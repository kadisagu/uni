package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка вступительного испытания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamVariantGen extends EntityBase
 implements INaturalIdentifiable<EnrExamVariantGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrExamVariant";
    public static final String ENTITY_NAME = "enrExamVariant";
    public static final int VERSION_HASH = 261272805;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET_VARIANT = "examSetVariant";
    public static final String L_EXAM_SET_ELEMENT = "examSetElement";
    public static final String P_PASS_MARK_AS_LONG = "passMarkAsLong";
    public static final String P_PRIORITY = "priority";
    public static final String P_PASS_MARK_AS_STRING = "passMarkAsString";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private EnrExamSetVariant _examSetVariant;     // Настроенный набор ВИ
    private EnrExamSetElement _examSetElement;     // ВИ
    private long _passMarkAsLong;     // Зачетный балл
    private int _priority;     // Приоритет ВИ в наборе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Настроенный набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetVariant getExamSetVariant()
    {
        return _examSetVariant;
    }

    /**
     * @param examSetVariant Настроенный набор ВИ. Свойство не может быть null.
     */
    public void setExamSetVariant(EnrExamSetVariant examSetVariant)
    {
        dirty(_examSetVariant, examSetVariant);
        _examSetVariant = examSetVariant;
    }

    /**
     * @return ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetElement getExamSetElement()
    {
        return _examSetElement;
    }

    /**
     * @param examSetElement ВИ. Свойство не может быть null.
     */
    public void setExamSetElement(EnrExamSetElement examSetElement)
    {
        dirty(_examSetElement, examSetElement);
        _examSetElement = examSetElement;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл. Свойство не может быть null.
     */
    @NotNull
    public long getPassMarkAsLong()
    {
        return _passMarkAsLong;
    }

    /**
     * @param passMarkAsLong Зачетный балл. Свойство не может быть null.
     */
    public void setPassMarkAsLong(long passMarkAsLong)
    {
        dirty(_passMarkAsLong, passMarkAsLong);
        _passMarkAsLong = passMarkAsLong;
    }

    /**
     * @return Приоритет ВИ в наборе. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет ВИ в наборе. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamVariantGen)
        {
            if (withNaturalIdProperties)
            {
                setExamSetVariant(((EnrExamVariant)another).getExamSetVariant());
                setExamSetElement(((EnrExamVariant)another).getExamSetElement());
            }
            setPassMarkAsLong(((EnrExamVariant)another).getPassMarkAsLong());
            setPriority(((EnrExamVariant)another).getPriority());
        }
    }

    public INaturalId<EnrExamVariantGen> getNaturalId()
    {
        return new NaturalId(getExamSetVariant(), getExamSetElement());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamVariantGen>
    {
        private static final String PROXY_NAME = "EnrExamVariantNaturalProxy";

        private Long _examSetVariant;
        private Long _examSetElement;

        public NaturalId()
        {}

        public NaturalId(EnrExamSetVariant examSetVariant, EnrExamSetElement examSetElement)
        {
            _examSetVariant = ((IEntity) examSetVariant).getId();
            _examSetElement = ((IEntity) examSetElement).getId();
        }

        public Long getExamSetVariant()
        {
            return _examSetVariant;
        }

        public void setExamSetVariant(Long examSetVariant)
        {
            _examSetVariant = examSetVariant;
        }

        public Long getExamSetElement()
        {
            return _examSetElement;
        }

        public void setExamSetElement(Long examSetElement)
        {
            _examSetElement = examSetElement;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamVariantGen.NaturalId) ) return false;

            EnrExamVariantGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamSetVariant(), that.getExamSetVariant()) ) return false;
            if( !equals(getExamSetElement(), that.getExamSetElement()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamSetVariant());
            result = hashCode(result, getExamSetElement());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamSetVariant());
            sb.append("/");
            sb.append(getExamSetElement());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamVariantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamVariant.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamVariant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examSetVariant":
                    return obj.getExamSetVariant();
                case "examSetElement":
                    return obj.getExamSetElement();
                case "passMarkAsLong":
                    return obj.getPassMarkAsLong();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examSetVariant":
                    obj.setExamSetVariant((EnrExamSetVariant) value);
                    return;
                case "examSetElement":
                    obj.setExamSetElement((EnrExamSetElement) value);
                    return;
                case "passMarkAsLong":
                    obj.setPassMarkAsLong((Long) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examSetVariant":
                        return true;
                case "examSetElement":
                        return true;
                case "passMarkAsLong":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examSetVariant":
                    return true;
                case "examSetElement":
                    return true;
                case "passMarkAsLong":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examSetVariant":
                    return EnrExamSetVariant.class;
                case "examSetElement":
                    return EnrExamSetElement.class;
                case "passMarkAsLong":
                    return Long.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamVariant> _dslPath = new Path<EnrExamVariant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamVariant");
    }
            

    /**
     * @return Настроенный набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getExamSetVariant()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
    {
        return _dslPath.examSetVariant();
    }

    /**
     * @return ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getExamSetElement()
     */
    public static EnrExamSetElement.Path<EnrExamSetElement> examSetElement()
    {
        return _dslPath.examSetElement();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getPassMarkAsLong()
     */
    public static PropertyPath<Long> passMarkAsLong()
    {
        return _dslPath.passMarkAsLong();
    }

    /**
     * @return Приоритет ВИ в наборе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getPassMarkAsString()
     */
    public static SupportedPropertyPath<String> passMarkAsString()
    {
        return _dslPath.passMarkAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrExamVariant> extends EntityPath<E>
    {
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariant;
        private EnrExamSetElement.Path<EnrExamSetElement> _examSetElement;
        private PropertyPath<Long> _passMarkAsLong;
        private PropertyPath<Integer> _priority;
        private SupportedPropertyPath<String> _passMarkAsString;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Настроенный набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getExamSetVariant()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
        {
            if(_examSetVariant == null )
                _examSetVariant = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT, this);
            return _examSetVariant;
        }

    /**
     * @return ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getExamSetElement()
     */
        public EnrExamSetElement.Path<EnrExamSetElement> examSetElement()
        {
            if(_examSetElement == null )
                _examSetElement = new EnrExamSetElement.Path<EnrExamSetElement>(L_EXAM_SET_ELEMENT, this);
            return _examSetElement;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getPassMarkAsLong()
     */
        public PropertyPath<Long> passMarkAsLong()
        {
            if(_passMarkAsLong == null )
                _passMarkAsLong = new PropertyPath<Long>(EnrExamVariantGen.P_PASS_MARK_AS_LONG, this);
            return _passMarkAsLong;
        }

    /**
     * @return Приоритет ВИ в наборе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrExamVariantGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getPassMarkAsString()
     */
        public SupportedPropertyPath<String> passMarkAsString()
        {
            if(_passMarkAsString == null )
                _passMarkAsString = new SupportedPropertyPath<String>(EnrExamVariantGen.P_PASS_MARK_AS_STRING, this);
            return _passMarkAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EnrExamVariantGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariant#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrExamVariantGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrExamVariant.class;
        }

        public String getEntityName()
        {
            return "enrExamVariant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPassMarkAsString();

    public abstract String getShortTitle();

    public abstract String getTitle();
}
