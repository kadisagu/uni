package ru.tandemservice.unienr14.request.entity;

/**
 * @author vdanilov
 */
public interface IEnrEntrantSingleProofBenefitStatement extends IEnrEntrantBenefitStatement {

    IEnrEntrantBenefitProofDocument getMainProof();
    void setMainProof(IEnrEntrantBenefitProofDocument mainProof);

}
