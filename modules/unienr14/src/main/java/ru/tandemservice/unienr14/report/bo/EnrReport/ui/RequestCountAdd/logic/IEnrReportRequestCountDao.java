/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd.EnrReportRequestCountAddUI;

/**
 * @author oleyba
 * @since 5/20/14
 */
public interface IEnrReportRequestCountDao extends INeedPersistenceSupport
{
    Long createReport(EnrReportRequestCountAddUI model);
}