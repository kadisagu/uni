/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantOnlineTreated.ui.AddEdit;

import com.beust.jcommander.internal.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.UIDefines;

import java.util.List;

/**
 * @author Denis Katkov
 * @since 17.06.2016
 */
@Configuration
public class EnrEntrantOnlineTreatedAddEdit extends BusinessComponentManager
{
    public static final String ENR_ENTRANT_ONLINE_TREATED_DS = "enrEntrantOnlineTreatedDS";
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENR_ENTRANT_ONLINE_TREATED_DS, enrEntrantOnlineTreatedDSHandler()))
                .create();
    }

    @Bean
    public SimpleTitledComboDataSourceHandler enrEntrantOnlineTreatedDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> recordListWrapped = Lists.newArrayList();
                recordListWrapped.add(new DataWrapper(0L, "Допускается, такие онлайн-абитуриенты подсвечиваются серым цветом"));
                recordListWrapped.add(new DataWrapper(1L, "Не допускается, такие онлайн-абитуриенты в списке для выбора не отображаются"));
                context.put(UIDefines.COMBO_OBJECT_LIST, recordListWrapped);
                return super.execute(input, context);
            }
        };
    }
}