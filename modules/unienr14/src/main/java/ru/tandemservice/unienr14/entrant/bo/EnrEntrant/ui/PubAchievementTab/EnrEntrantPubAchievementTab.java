/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAchievementTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Configuration
public class EnrEntrantPubAchievementTab extends BusinessComponentManager
{
    public static final String INDIVIDUAL_ACHIEVEMENTS_DS = "individualAchievementsDS";

    @Bean
    public ColumnListExtPoint enrIndividualAchievementsDSColumns()
    {
        final IStyleResolver markStyleResolver = rowEntity -> {
            DataWrapper wrapper = (DataWrapper) rowEntity;
            EnrEntrantAchievement achievement = wrapper.getWrapped();
            Long mark = achievement.getType().isMarked() ? achievement.getMarkAsLong() : achievement.getType().getAchievementMarkAsLong();
            return achievement.getRatingMarkAsLong() != mark ? "background-color:#ffcccc" : null;
        };

        IPublisherLinkResolver documentResolver = new SimplePublisherLinkResolver("doc.id")
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                DataWrapper wrapper = (DataWrapper) entity;
                EnrEntrantAchievement achievement = wrapper.getWrapped();
                if (achievement.getDocument() instanceof EnrPersonEduDocumentRel)
                {
                    EnrPersonEduDocumentRel rel = ((EnrPersonEduDocumentRel) achievement.getDocument());
                    PersonEduDocument eduDocument = rel.getEduDocument();
                    ISecureRoleContext context = EnrPersonLegacyUtils.getSecureRoleContext(rel.getEntrant());
                    return EnrPersonLegacyUtils.getParametersMap(eduDocument, context);
                }
                else
                    return super.getParameters(entity);
            }
        };

        return columnListExtPointBuilder(INDIVIDUAL_ACHIEVEMENTS_DS)
                .addColumn(textColumn("requestType", EnrEntrantAchievement.type().achievementKind().requestType().shortTitle()))
                .addColumn(textColumn("title", "title"))
                .addColumn(publisherColumn("request", EnrEntrantAchievement.L_REQUEST).entityListProperty(EnrEntrantAchievement.L_REQUEST).visible("ui:showRequestColumn"))
                .addColumn(textColumn("mark", "mark").formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).styleResolver(markStyleResolver))
                .addColumn(publisherColumn("doc", "doc").publisherLinkResolver(documentResolver).formatter(new PropertyFormatter(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE)))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), EDIT_LISTENER).permissionKey("enr14EntrantPubAchievementTabEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), DELETE_LISTENER, new FormattedMessage("individualAchievementsDS.delete.alert", "title")).permissionKey("enr14EntrantPubAchievementTabDelete"))
                .create();
    }


    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(INDIVIDUAL_ACHIEVEMENTS_DS, enrIndividualAchievementsDSColumns(), achievementsDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler achievementsDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long entrantId = context.getNotNull("entrantId");

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "a");
                builder.column(property("a"));
                builder.where(eq(property("a", EnrEntrantAchievement.entrant().id()), value(entrantId)));

                builder.order(property("a", EnrEntrantAchievement.type().achievementKind().requestType().code()));
                builder.order(property("a", EnrEntrantAchievement.type().achievementKind().title()));
                builder.order(property("a", EnrEntrantAchievement.id()));

                input.setCountRecord(5);
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).preserveCountRecord().pageable(false).build().transform((EnrEntrantAchievement achievement) -> {
                    DataWrapper wrapper = new DataWrapper(achievement);
                    IEnrEntrantAchievementProofDocument document = achievement.getDocument();
                    IEntity doc = document instanceof EnrPersonEduDocumentRel ? ((EnrPersonEduDocumentRel) document).getEduDocument() : document;

                    wrapper.put("title", achievement.getType().getAchievementKind().getTitle());
                    wrapper.put("mark", achievement.getType().isMarked() ? achievement.getMark() : achievement.getType().getAchievementMark());
                    wrapper.put("doc", doc);
                    return wrapper;
                });
            }
        };
    }
}



    
