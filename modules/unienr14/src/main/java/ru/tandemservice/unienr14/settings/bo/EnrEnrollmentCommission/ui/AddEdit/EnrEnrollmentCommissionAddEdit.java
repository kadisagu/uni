/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.presenter.*;
import org.tandemframework.caf.ui.config.*;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class EnrEnrollmentCommissionAddEdit extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
