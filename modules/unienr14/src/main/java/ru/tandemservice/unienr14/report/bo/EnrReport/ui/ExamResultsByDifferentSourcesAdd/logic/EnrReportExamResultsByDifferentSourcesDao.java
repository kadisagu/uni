/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.logic;

import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.EnrReportExamResultsByDifferentSourcesAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.*;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 01.07.2014
 */
public class EnrReportExamResultsByDifferentSourcesDao extends UniBaseDao implements IEnrReportExamResultsByDifferentSourcesDao {

    // Заглушка для строк с филиалами, для 22 ячеек, идущих за первой, в которую вносится название филиала.
    private static final List<String> ROW_CAP_22_ELEMENTS = Arrays.asList("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    private static final List<String> ROW_CAP_21_ELEMENTS = Arrays.asList("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

    @Override
    public long createReport(EnrReportExamResultsByDifferentSourcesAddUI model) {
        EnrReportExamResultsByDifferentSources report = new EnrReportExamResultsByDifferentSources();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportExamResultsByDifferentSources.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportExamResultsByDifferentSources.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportExamResultsByDifferentSources.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportExamResultsByDifferentSources.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportExamResultsByDifferentSources.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportExamResultsByDifferentSources.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportExamResultsByDifferentSources.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportExamResultsByDifferentSources.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportExamResultsByDifferentSources.P_PROGRAM_SET, "title");
        DatabaseFile content = new DatabaseFile();

        report.setStage(model.getStageSelector().getStage().getTitle());
        report.setExamResultSource(model.getExamResultSource().getTitle());

        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        report.setGroupByOrgUnits(model.isGroupByOrgUnits() ? "Да" : "Нет");
        report.setShowProgramSubjectGroups(model.isShowProgramSubjectGroups() ? "Да" : "Нет");

        try {
            content.setContent(buildReport(model));
        } catch (IOException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        } catch (JXLException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        content.setFilename("EnrReportExamResultsByDifferentSources.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportExamResultsByDifferentSourcesAddUI model) throws JXLException, IOException
    {
        // документ
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");


        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property("vvi", EnrChosenEntranceExam.requestedCompetition()), property("reqComp")))
                .where(ne(property(EnrChosenEntranceExam.maxMarkForm().passForm().code().fromAlias("vvi")), value(EnrExamPassFormCodes.OLYMPIAD)))
                .column(property("vvi"));

        if (model.getExamResultSource().getId() == 0L)
            requestedCompDQL.where(eq(property(EnrChosenEntranceExam.maxMarkForm().passForm().internal().fromAlias("vvi")), value(Boolean.FALSE)));
        else {
            requestedCompDQL
                    .where(eq(property(EnrChosenEntranceExam.maxMarkForm().passForm().internal().fromAlias("vvi")), value(Boolean.TRUE)))
                    .where(le(property(EnrChosenEntranceExam.maxMarkForm().markSource().markAsLong().fromAlias("vvi")), value(100000)));
        }



        // Запрос
        List<EnrChosenEntranceExam> vviList = getList(requestedCompDQL);

        requestedCompDQL.resetColumns();


        // присоединяем наборы испытаний, чтобы впоследствии добавить все дисциплины ВИ по направлению
        requestedCompDQL
                .joinEntity("vvi", DQLJoinType.inner, EnrExamVariant.class, "examVar", eq(property("examVar"), property(EnrChosenEntranceExam.actualExam().fromAlias("vvi"))))
                .joinEntity("examVar", DQLJoinType.inner, EnrExamSetVariant.class, "examSetVar", eq(property("examSetVar"), property(EnrExamVariant.examSetVariant().fromAlias("examVar"))))
                .joinEntity("examSetVar", DQLJoinType.inner, EnrExamSet.class, "examSet", eq(property(EnrExamSetVariant.examSet().fromAlias("examSetVar")), property("examSet")))
                .joinEntity("examSet", DQLJoinType.inner, EnrExamSetDiscipline.class, "dis", eq(property("examSet"), property(EnrExamSetDiscipline.examSet().fromAlias("dis"))))
                .column(property("vvi"))
                .column(property("dis"));


        List<Object[]> disciplines = getList(requestedCompDQL);

        Map<EduProgramSubject, Set<EnrDiscipline>> disciplineMap = SafeMap.get(HashSet.class);

        for (Object[] row : disciplines)
        {
            EnrExamSetDiscipline discipline = (EnrExamSetDiscipline)row[1];
            EnrChosenEntranceExam exam = (EnrChosenEntranceExam) row[0];
            EduProgramSubject subject = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();

            disciplineMap.get(subject).add(discipline.getDiscipline().getDiscipline());

        }

        Comparator<OrgUnit> topDependentOrgUnitNullSafeComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                if (o1 == o2) { return 0; }
                if (null == o1) { return  1; }
                if (null == o2) { return -1; }
                int i = Boolean.compare(null == o1.getParent(), null == o2.getParent());
                if (0 != i) { return -i; }

                return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
            }
        };
        Comparator<EduProgramKind> programKindComparator = new Comparator<EduProgramKind>() {
            @Override
            public int compare(EduProgramKind o1, EduProgramKind o2) {
                return o1.getCode().compareTo(o2.getCode());
            }
        };
        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };


        Map<OrgUnit, Map<OrgUnit, Map<EduProgramKind, Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>>>>> dataMap = new TreeMap<>(topDependentOrgUnitNullSafeComparator);

        for (EnrChosenEntranceExam exam : vviList)
        {
            EduProgramKind programKind = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramKind();
            EduProgramSubject programSubject = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            OrgUnit orgUnit = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
            OrgUnit formative = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
            EnrDiscipline discipline = exam.getDiscipline().getDiscipline();

            if (!model.isGroupByOrgUnits()) {
                orgUnit = null;
                formative = null;
            }

            final Map<OrgUnit, Map<EduProgramKind, Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>>>> orgUnitMap = SafeMap.safeGet(dataMap, orgUnit, TreeMap.class, new OrgUnitPrintTiltleComparator());
            final Map<EduProgramKind, Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>>> eduProgramKindMap = SafeMap.safeGet(orgUnitMap, formative, TreeMap.class, programKindComparator);
            final Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>> eduProgramSubjectMap = SafeMap.safeGet(eduProgramKindMap, programKind, TreeMap.class, subjectComparator);
            if (!eduProgramSubjectMap.containsKey(programSubject)) {
                eduProgramSubjectMap.put(programSubject, new TreeMap<EnrDiscipline, Set<EnrChosenEntranceExam>>(ITitled.TITLED_COMPARATOR));
                Map<EnrDiscipline, Set<EnrChosenEntranceExam>> enrDisciplineSetMap = eduProgramSubjectMap.get(programSubject);
                for (EnrDiscipline discFromSubject : disciplineMap.get(programSubject)) {
                    if (!enrDisciplineSetMap.containsKey(discFromSubject))
                        enrDisciplineSetMap.put(discFromSubject, new HashSet<EnrChosenEntranceExam>());
                }
            }
            Map<EnrDiscipline, Set<EnrChosenEntranceExam>> enrDisciplineSetMap = eduProgramSubjectMap.get(programSubject);

            final Set<EnrChosenEntranceExam> exams = SafeMap.safeGet(enrDisciplineSetMap, discipline, HashSet.class);
            exams.add(exam);
        }




        WritableSheet sheet = workbook.createSheet("отчет", 0);

        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat formativeOUFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setBackground(Colour.GREY_40_PERCENT);
        formativeOUFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        formativeOUFormat.setBackground(Colour.GREY_25_PERCENT);

        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        WritableFont subjectFont = new WritableFont(WritableFont.createFont("Calibri"), 10);
        WritableCellFormat subjectFormat = new WritableCellFormat(contentFont);
        subjectFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableFont groupFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        WritableFont groupSubjectFont = new WritableFont(WritableFont.createFont("Calibri"), 10);
        groupFont.setBoldStyle(WritableFont.BOLD);
        groupFont.setColour(Colour.GRAY_50);
        groupSubjectFont.setBoldStyle(WritableFont.BOLD);
        groupSubjectFont.setColour(Colour.GRAY_50);
        WritableCellFormat groupFormat = new WritableCellFormat(groupFont);
        groupFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat groupSubjectFormat = new WritableCellFormat(groupSubjectFont);
        groupSubjectFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        int index = printHeader(model, sheet);



        List<String[]> groupedSubjectsTable = new ArrayList<>();
        List<Boolean> groupSubjectRowMarker = new ArrayList<>();
        List<String[]> groupDetailsTable = new ArrayList<>();
        Map<EnrDiscipline, Set<EnrEntrant>[]> groupDisciplinesCountedValues = new TreeMap<>(ITitled.TITLED_COMPARATOR);
        Map<EnrDiscipline, Set<EnrEntrant>> groupDisciplinesTotalValues = new HashMap<>();
        Map<EnrDiscipline, PairKey<Integer, Integer>> groupDisciplinesAverageValues = new HashMap<>();
        String prevGroup = null;


        // Запоняем таблицу
        for (Map.Entry<OrgUnit, Map<OrgUnit, Map<EduProgramKind, Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>>>>> orgUnitEntry : dataMap.entrySet())
        {



            for (Map.Entry<OrgUnit, Map<EduProgramKind, Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>>>> formativeEntry : orgUnitEntry.getValue().entrySet())
            {
                if (model.isGroupByOrgUnits())
                {
                    StringBuilder orgUnitTitleBuilder = new StringBuilder();
                    orgUnitTitleBuilder.append(formativeEntry.getKey().getPrintTitle()).append(" (").append(orgUnitEntry.getKey().getPrintTitle()).append(")");
                    List<String> orgUnitRow = new ArrayList<>();
                    orgUnitRow.add(orgUnitTitleBuilder.toString());
                    orgUnitRow.addAll(ROW_CAP_22_ELEMENTS);
                    ExcelReportUtils.printrow(sheet, orgUnitRow, index++, formativeOUFormat);
                }


                for (Map.Entry<EduProgramKind, Map<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>>> programKindEntry : formativeEntry.getValue().entrySet())
                {
                    for (Map.Entry<EduProgramSubject, Map<EnrDiscipline, Set<EnrChosenEntranceExam>>> subjectEntry : programKindEntry.getValue().entrySet())
                    {
                        if (prevGroup == null)
                            prevGroup = subjectEntry.getKey().getGroupTitle();

                        Set<EnrEntrant> totalOnSubject = new HashSet<>();


                        if (model.isShowProgramSubjectGroups())
                        {
                            // при смене группы записываем в документ содержимое предыдущей группы и сбрасываем массивы со строками и пр. данными
                            if (!prevGroup.equals(subjectEntry.getKey().getGroupTitle()))
                            {
                                Set<EnrEntrant> totalOnGroup = new HashSet<>();
                                List<Boolean> detailsSubjectRowMarker = new ArrayList<>();
                                for (Map.Entry<EnrDiscipline, Set<EnrEntrant>[]> disciplineEntry : groupDisciplinesCountedValues.entrySet())
                                {
                                    List<String> row = new ArrayList<>();
                                    row.add("   " + disciplineEntry.getKey().getTitle());
                                    detailsSubjectRowMarker.add(true);
                                    if (groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getSecond() != 0) {
                                        row.add(String.valueOf(groupDisciplinesTotalValues.get(disciplineEntry.getKey()).size()));
                                        row.add(String.valueOf(groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getFirst() / groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getSecond()));
                                        totalOnGroup.addAll(groupDisciplinesTotalValues.get(disciplineEntry.getKey()));
                                        for (int i = 0; i < disciplineEntry.getValue().length; i++) {
                                            if (disciplineEntry.getValue()[i].size() > 0)
                                                row.add(String.valueOf(disciplineEntry.getValue()[i].size()));
                                            else row.add("");
                                        }
                                    }
                                    else row.addAll(ROW_CAP_22_ELEMENTS);
                                    groupDetailsTable.add(row.toArray(new String[row.size()]));
                                }
                                List<String> groupRow = new ArrayList<>();
                                groupRow.add(prevGroup);
                                groupRow.add(String.valueOf(totalOnGroup.size()));
                                groupRow.addAll(ROW_CAP_21_ELEMENTS);
                                groupDetailsTable.add(0, groupRow.toArray(new String[groupRow.size()]));
                                detailsSubjectRowMarker.add(0, false);
                                for (int i = 0; i < groupDetailsTable.size(); i++) {
                                    if (detailsSubjectRowMarker.get(i))
                                        ExcelReportUtils.printrow(sheet, groupDetailsTable.get(i), index++, groupSubjectFormat);
                                    else
                                        ExcelReportUtils.printrow(sheet, groupDetailsTable.get(i), index++, groupFormat);
                                }

                                for (int i = 0; i < groupedSubjectsTable.size(); i++) {
                                    if (groupSubjectRowMarker.get(i))
                                        ExcelReportUtils.printrow(sheet, groupedSubjectsTable.get(i), index++, subjectFormat);
                                    else
                                        ExcelReportUtils.printrow(sheet, groupedSubjectsTable.get(i), index++, contentFormat);
                                }
                                groupDetailsTable.clear();
                                groupDisciplinesAverageValues.clear();
                                groupedSubjectsTable.clear();
                                groupDisciplinesCountedValues.clear();
                                groupDisciplinesTotalValues.clear();
                                prevGroup = subjectEntry.getKey().getGroupTitle();
                                groupSubjectRowMarker.clear();

                            }
                        }

                        List<String[]> subjectTable = new ArrayList<>();
                        List<Boolean> subjectRowMarker = new ArrayList<>();

                        // заполняем дисциплины по направлению
                        for (Map.Entry<EnrDiscipline, Set<EnrChosenEntranceExam>> disciplineEntry : subjectEntry.getValue().entrySet())
                        {
                            List<String> row = new ArrayList<>();
                            Set<EnrEntrant>[] values = new HashSet[20];
                            for (int i = 0; i < values.length; i++) {
                                values[i] = new HashSet<>();
                            }
                            Set<EnrEntrant> totalEntrants = new HashSet<>();
                            int average = 0;
                            int count = 0;
                            for (EnrChosenEntranceExam exam : disciplineEntry.getValue())
                            {
                                if (exam.getMarkAsDouble() < 1) {
                                    values[0].add(exam.getRequestedCompetition().getRequest().getEntrant());
                                    count++;
                                    totalEntrants.add(exam.getRequestedCompetition().getRequest().getEntrant());
                                }
                                else {
                                    int interval = (int)((double)exam.getMarkAsDouble());
                                    average += interval;
                                    count++;
                                    interval = interval - 1;
                                    EnrRequestedCompetition competition = exam.getRequestedCompetition();
                                    EnrEntrantRequest request = competition.getRequest();
                                    EnrEntrant entrant = request.getEntrant();
                                    totalEntrants.add(exam.getRequestedCompetition().getRequest().getEntrant());

                                    values[interval / 5].add(entrant);
                                }
                            }

                            if (groupDisciplinesCountedValues.containsKey(disciplineEntry.getKey())) {
                                for (int i = 0; i < groupDisciplinesCountedValues.get(disciplineEntry.getKey()).length; i++) {
                                    groupDisciplinesCountedValues.get(disciplineEntry.getKey())[i].addAll(values[i]);
                                }
                                groupDisciplinesTotalValues.get(disciplineEntry.getKey()).addAll(totalEntrants);
                            }
                            else {
                                groupDisciplinesCountedValues.put(disciplineEntry.getKey(), values);
                                groupDisciplinesTotalValues.put(disciplineEntry.getKey(), totalEntrants);
                            }

                            row.add("   " + disciplineEntry.getKey().getTitle());

                            if (disciplineEntry.getValue().size() > 0) {

                                row.add(String.valueOf(totalEntrants.size()));
                                row.add(String.valueOf(average / count));

                                for (int i = 0; i < values.length; i++) {
                                    if (values[i].size() > 0)
                                        row.add(String.valueOf(values[i].size()));
                                    else row.add("");
                                }
                                totalOnSubject.addAll(totalEntrants);
                            } else {
                                row.addAll(ROW_CAP_22_ELEMENTS);
                            }

                            subjectTable.add(row.toArray(new String[row.size()]));
                            subjectRowMarker.add(true);
                            if (groupDisciplinesAverageValues.containsKey(disciplineEntry.getKey())) {
                                average += groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getFirst();
                                count += groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getSecond();
                            }
                            groupDisciplinesAverageValues.put(disciplineEntry.getKey(), new PairKey<Integer, Integer>(average, count));
                        }

                        // subjectTable - таблица под одно направление

                        List<String> subjectRow = new ArrayList<>();
                        subjectRow.add(subjectEntry.getKey().getTitleWithCode());
                        subjectRow.add(String.valueOf(totalOnSubject.size()));
                        subjectRow.addAll(ROW_CAP_21_ELEMENTS);
                        subjectTable.add(0, subjectRow.toArray(new String[subjectRow.size()]));
                        subjectRowMarker.add(0, false);
                        groupedSubjectsTable.addAll(subjectTable);
                        groupSubjectRowMarker.addAll(subjectRowMarker);

                        if (!model.isShowProgramSubjectGroups())
                        {
                            for (int i = 0; i < groupedSubjectsTable.size(); i++) {
                                if (i == 0)
                                    ExcelReportUtils.printrow(sheet, groupedSubjectsTable.get(i), index++, contentFormat);
                                else
                                    ExcelReportUtils.printrow(sheet, groupedSubjectsTable.get(i), index++, subjectFormat);
                            }
                            groupedSubjectsTable.clear();
                        }
                    }
                }

                if (model.isShowProgramSubjectGroups()) {
                    Set<EnrEntrant> totalOnGroup = new HashSet<>();
                    List<Boolean> detailsSubjectRowMarker = new ArrayList<>();
                    for (Map.Entry<EnrDiscipline, Set<EnrEntrant>[]> disciplineEntry : groupDisciplinesCountedValues.entrySet())
                    {
                        List<String> row = new ArrayList<>();
                        row.add("   " + disciplineEntry.getKey().getTitle());
                        detailsSubjectRowMarker.add(true);
                        if (groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getSecond() != 0) {
                            row.add(String.valueOf(groupDisciplinesTotalValues.get(disciplineEntry.getKey()).size()));
                            row.add(String.valueOf(groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getFirst() / groupDisciplinesAverageValues.get(disciplineEntry.getKey()).getSecond()));
                            totalOnGroup.addAll(groupDisciplinesTotalValues.get(disciplineEntry.getKey()));
                            for (int i = 0; i < disciplineEntry.getValue().length; i++) {
                                if (disciplineEntry.getValue()[i].size() > 0)
                                    row.add(String.valueOf(disciplineEntry.getValue()[i].size()));
                                else row.add("");
                            }
                        }
                        else row.addAll(ROW_CAP_22_ELEMENTS);
                        groupDetailsTable.add(row.toArray(new String[row.size()]));
                    }
                    List<String> groupRow = new ArrayList<>();
                    groupRow.add(prevGroup);
                    groupRow.add(String.valueOf(totalOnGroup.size()));
                    groupRow.addAll(ROW_CAP_21_ELEMENTS);
                    groupDetailsTable.add(0, groupRow.toArray(new String[groupRow.size()]));
                    detailsSubjectRowMarker.add(0, false);
                    for (int i = 0; i < groupDetailsTable.size(); i++) {
                        if (detailsSubjectRowMarker.get(i))
                            ExcelReportUtils.printrow(sheet, groupDetailsTable.get(i), index++, groupSubjectFormat);
                        else
                            ExcelReportUtils.printrow(sheet, groupDetailsTable.get(i), index++, groupFormat);
                    }

                    for (int i = 0; i < groupedSubjectsTable.size(); i++) {
                        if (groupSubjectRowMarker.get(i))
                            ExcelReportUtils.printrow(sheet, groupedSubjectsTable.get(i), index++, subjectFormat);
                        else
                            ExcelReportUtils.printrow(sheet, groupedSubjectsTable.get(i), index++, contentFormat);
                    }
                    groupDetailsTable.clear();
                    groupedSubjectsTable.clear();
                    groupDisciplinesCountedValues.clear();
                    groupDisciplinesTotalValues.clear();
                    prevGroup = null;
                    groupSubjectRowMarker.clear();
                    groupDisciplinesAverageValues.clear();
                }


            }
        }







        workbook.write();
        workbook.close();

        byte[] bytes = out.toByteArray();
        out.close();


        return bytes;
    }



    private int printHeader(EnrReportExamResultsByDifferentSourcesAddUI model, WritableSheet sheet) throws JXLException, IOException
    {
        int rowIndex = 0;
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);

        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat filterTitlesFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        filterTitlesFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        headerFormat.setWrap(true);

        CellMergeMap mergeMap = new CellMergeMap();
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        StringBuilder labelBuilder = new StringBuilder();

        if (model.getExamResultSource().getId() == 1L)
            labelBuilder.append("Сведения по результатам ВИ по материалам ОУ на ");
        else labelBuilder.append("Сведения по результатам ЕГЭ на ");

        labelBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

        sheet.addCell(new Label( 0, 0, labelBuilder.toString(), labelFormat));

        FilterParametersPrinter filterParametersPrinter = new FilterParametersPrinter();
        filterParametersPrinter.setParallelSelector(model.getParallelSelector());
        filterParametersPrinter.setStageSelector(model.getStageSelector());

        filterParametersPrinter.addVariableParameter("По результатам ВИ", model.getExamResultSource().getTitle());
        filterParametersPrinter.addVariableParameter("Группировать по подразделениям", model.isGroupByOrgUnits() ? "Да" : "Нет");
        filterParametersPrinter.addVariableParameter("Выделять укрупненные группы направлений подготовки", model.isShowProgramSubjectGroups() ?  "Да" : "Нет");

        List<String[]> hTable = filterParametersPrinter.getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());


        for (int i = 0; i < hTable.size(); i++) {
            sheet.addCell(new Label(0, i+2, hTable.get(i)[0], filterTitlesFormat));
            sheet.addCell(new Label(1, i+2, hTable.get(i)[1], contentFormat));
            rowIndex = i+2;
        }

        rowIndex+=2;

        int c = 0;

        List<String> headerL1 = new ArrayList<>();
        List<String> headerL2 = new ArrayList<>();

        headerL1.add("Направление подготовки (специальность) / Дисциплина");
        headerL2.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 1);

        headerL1.add("Всего чел.");
        headerL2.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 1);

        headerL1.add("Средний балл");
        headerL2.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 1);

        if (model.getExamResultSource().getId() == 0L)
            headerL1.add("Количество абитуриентов с результатами единого государственного экзамена, распределенное по интервалам баллов (чел)");
        else
            headerL1.add("Количество абитуриентов с результатами вступительных испытаний, распределенное по интервалам баллов (чел)");
        mergeMap.addMapping(c, rowIndex, c+19, rowIndex);


        headerL2.add("0-5");
        headerL2.add("6-10");
        headerL2.add("11-15");
        headerL2.add("16-20");
        headerL2.add("21-25");
        headerL2.add("26-30");
        headerL2.add("31-35");
        headerL2.add("36-40");
        headerL2.add("41-45");
        headerL2.add("46-50");
        headerL2.add("51-55");
        headerL2.add("56-60");
        headerL2.add("61-65");
        headerL2.add("66-70");
        headerL2.add("71-75");
        headerL2.add("76-80");
        headerL2.add("81-85");
        headerL2.add("86-90");
        headerL2.add("91-95");
        headerL2.add("96-100");

        ExcelReportUtils.setWidths(new int[]{
                60,
                15,
                15,
        }, sheet);


        ExcelReportUtils.printrow(sheet, headerL1, rowIndex++, headerFormat);
        ExcelReportUtils.printrow(sheet, headerL2, rowIndex++, headerFormat);


        mergeMap.makeMerges(sheet);

        return rowIndex;
    }
}
