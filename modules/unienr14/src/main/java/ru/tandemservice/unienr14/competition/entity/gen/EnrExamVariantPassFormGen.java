package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма сдачи вступительного испытания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamVariantPassFormGen extends EntityBase
 implements INaturalIdentifiable<EnrExamVariantPassFormGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm";
    public static final String ENTITY_NAME = "enrExamVariantPassForm";
    public static final int VERSION_HASH = -191769208;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_VARIANT = "examVariant";
    public static final String L_PASS_FORM = "passForm";

    private EnrExamVariant _examVariant;     // ВИ
    private EnrExamPassForm _passForm;     // Форма сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamVariant getExamVariant()
    {
        return _examVariant;
    }

    /**
     * @param examVariant ВИ. Свойство не может быть null.
     */
    public void setExamVariant(EnrExamVariant examVariant)
    {
        dirty(_examVariant, examVariant);
        _examVariant = examVariant;
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     */
    @NotNull
    public EnrExamPassForm getPassForm()
    {
        return _passForm;
    }

    /**
     * @param passForm Форма сдачи. Свойство не может быть null.
     */
    public void setPassForm(EnrExamPassForm passForm)
    {
        dirty(_passForm, passForm);
        _passForm = passForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamVariantPassFormGen)
        {
            if (withNaturalIdProperties)
            {
                setExamVariant(((EnrExamVariantPassForm)another).getExamVariant());
                setPassForm(((EnrExamVariantPassForm)another).getPassForm());
            }
        }
    }

    public INaturalId<EnrExamVariantPassFormGen> getNaturalId()
    {
        return new NaturalId(getExamVariant(), getPassForm());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamVariantPassFormGen>
    {
        private static final String PROXY_NAME = "EnrExamVariantPassFormNaturalProxy";

        private Long _examVariant;
        private Long _passForm;

        public NaturalId()
        {}

        public NaturalId(EnrExamVariant examVariant, EnrExamPassForm passForm)
        {
            _examVariant = ((IEntity) examVariant).getId();
            _passForm = ((IEntity) passForm).getId();
        }

        public Long getExamVariant()
        {
            return _examVariant;
        }

        public void setExamVariant(Long examVariant)
        {
            _examVariant = examVariant;
        }

        public Long getPassForm()
        {
            return _passForm;
        }

        public void setPassForm(Long passForm)
        {
            _passForm = passForm;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamVariantPassFormGen.NaturalId) ) return false;

            EnrExamVariantPassFormGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamVariant(), that.getExamVariant()) ) return false;
            if( !equals(getPassForm(), that.getPassForm()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamVariant());
            result = hashCode(result, getPassForm());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamVariant());
            sb.append("/");
            sb.append(getPassForm());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamVariantPassFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamVariantPassForm.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamVariantPassForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examVariant":
                    return obj.getExamVariant();
                case "passForm":
                    return obj.getPassForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examVariant":
                    obj.setExamVariant((EnrExamVariant) value);
                    return;
                case "passForm":
                    obj.setPassForm((EnrExamPassForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examVariant":
                        return true;
                case "passForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examVariant":
                    return true;
                case "passForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examVariant":
                    return EnrExamVariant.class;
                case "passForm":
                    return EnrExamPassForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamVariantPassForm> _dslPath = new Path<EnrExamVariantPassForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamVariantPassForm");
    }
            

    /**
     * @return ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm#getExamVariant()
     */
    public static EnrExamVariant.Path<EnrExamVariant> examVariant()
    {
        return _dslPath.examVariant();
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm#getPassForm()
     */
    public static EnrExamPassForm.Path<EnrExamPassForm> passForm()
    {
        return _dslPath.passForm();
    }

    public static class Path<E extends EnrExamVariantPassForm> extends EntityPath<E>
    {
        private EnrExamVariant.Path<EnrExamVariant> _examVariant;
        private EnrExamPassForm.Path<EnrExamPassForm> _passForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm#getExamVariant()
     */
        public EnrExamVariant.Path<EnrExamVariant> examVariant()
        {
            if(_examVariant == null )
                _examVariant = new EnrExamVariant.Path<EnrExamVariant>(L_EXAM_VARIANT, this);
            return _examVariant;
        }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm#getPassForm()
     */
        public EnrExamPassForm.Path<EnrExamPassForm> passForm()
        {
            if(_passForm == null )
                _passForm = new EnrExamPassForm.Path<EnrExamPassForm>(L_PASS_FORM, this);
            return _passForm;
        }

        public Class getEntityClass()
        {
            return EnrExamVariantPassForm.class;
        }

        public String getEntityName()
        {
            return "enrExamVariantPassForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
