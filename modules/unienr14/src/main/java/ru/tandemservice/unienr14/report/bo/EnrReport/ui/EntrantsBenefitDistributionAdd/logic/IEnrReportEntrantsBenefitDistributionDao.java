/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd.EnrReportEntrantsBenefitDistributionAddUI;

/**
 * @author rsizonenko
 * @since 09.07.2014
 */
public interface IEnrReportEntrantsBenefitDistributionDao extends INeedPersistenceSupport {
    long createReport(EnrReportEntrantsBenefitDistributionAddUI model);
}
