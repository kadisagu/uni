package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.settings.entity.gen.EnrCampaignDisciplineGen;

/**
 * Дисциплина, используемая в рамках ПК
 */
public class EnrCampaignDiscipline extends EnrCampaignDisciplineGen
{
    public EnrCampaignDiscipline() { }

    public EnrCampaignDiscipline(EnrEnrollmentCampaign enrollmentCampaign, EnrDiscipline discipline) {
        setEnrollmentCampaign(enrollmentCampaign);
        setDiscipline(discipline);
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getDiscipline() == null) {
            return this.getClass().getSimpleName();
        }
        return getDiscipline().getTitle();
    }

    @Override
    public String getShortTitle() {
        return getDiscipline().getShortTitle();
    }
}