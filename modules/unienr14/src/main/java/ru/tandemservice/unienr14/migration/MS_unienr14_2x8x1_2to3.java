package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrCompetitionType

		// созданы обязательные свойства publicTitle, priority
		{
			// создать колонку
			tool.createColumn("enr14_c_comp_type_t", new DBColumn("publictitle_p", DBType.createVarchar(255)));
			tool.createColumn("enr14_c_comp_type_t", new DBColumn("priority_p", DBType.INTEGER));

			// задать значения
			tool.executeUpdate("update enr14_c_comp_type_t set publictitle_p=?, priority_p=? where code_p=?", "без вступительных испытаний", 1, "01");
			tool.executeUpdate("update enr14_c_comp_type_t set publictitle_p=?, priority_p=? where code_p=?", "квота лиц с особыми правами", 2, "02");
			tool.executeUpdate("update enr14_c_comp_type_t set publictitle_p=?, priority_p=? where code_p=?", "целевой прием", 3, "03");
			tool.executeUpdate("update enr14_c_comp_type_t set publictitle_p=?, priority_p=? where code_p=?", "общий конкурс", 4, "04");
			tool.executeUpdate("update enr14_c_comp_type_t set publictitle_p=?, priority_p=? where code_p=?", "без вступительных испытаний", 5, "05");
			tool.executeUpdate("update enr14_c_comp_type_t set publictitle_p=?, priority_p=? where code_p=?", "общий конкурс", 6, "06");

			// сделать колонки NOT NULL
			tool.setColumnNullable("enr14_c_comp_type_t", "publictitle_p", false);
			tool.setColumnNullable("enr14_c_comp_type_t", "priority_p", false);
		}
    }
}