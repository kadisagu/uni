/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd.logic;

import com.ibm.icu.text.*;
import jxl.JXLException;
import jxl.Workbook;
import jxl.format.*;
import jxl.format.Alignment;
import jxl.format.BoldStyle;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd.EnrReportAdmissionPlansAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.*;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans;
import ru.tandemservice.unienr14.report.entity.EnrReportForEnrollmentCommission;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author rsizonenko
 * @since 17.10.2014
 */
public class EnrReportAdmissionPlansDao extends UniBaseDao implements IEnrReportAdmissionPlansDao {

    public Long createReport(EnrReportAdmissionPlansAddUI model)
    {
        EnrReportAdmissionPlans report = new EnrReportAdmissionPlans();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setStage(model.getStageSelector().getStage().getTitle());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportForEnrollmentCommission.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportForEnrollmentCommission.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportForEnrollmentCommission.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportForEnrollmentCommission.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportForEnrollmentCommission.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportForEnrollmentCommission.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportForEnrollmentCommission.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportForEnrollmentCommission.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        report.setFirstWaveDateFrom(model.getFirstWaveDateFrom());
        report.setFirstWaveDateTo(model.getFirstWaveDateTo());
        report.setSecondWaveDateFrom(model.getSecondWaveDateFrom());
        report.setSecondWaveDateTo(model.getSecondWaveDateTo());
        report.setThirdWaveDateFrom(model.getThirdWaveDateFrom());
        report.setThirdWaveDateTo(model.getThirdWaveDateTo());

        DatabaseFile content = new DatabaseFile();

        try {
            content.setContent(buildReport(model));
        } catch (JXLException | IOException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }
        content.setFilename("EnrReportAdmissionPlans.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportAdmissionPlansAddUI model) throws JXLException, IOException{

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);
        WritableSheet sheet = workbook.createSheet("ReportSheet", 0);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());


        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");

        // дополняем запрос нужными нам ВВИ и ВВИ-ф
        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.left, EnrChosenEntranceExam.class, "vvi", eq(property("reqComp"), property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi"))));

        // Поднимем запросом дисциплины
        DQLSelectBuilder stateExamDisciplinesSB = new DQLSelectBuilder().fromDataSource(requestedCompDQL.buildQuery(), "ege")
                .column(property(EnrChosenEntranceExam.discipline().discipline().fromAlias("vvi").fromAlias("ege")))
                .where(eq(property(EnrChosenEntranceExam.maxMarkForm().passForm().internal().fromAlias("vvi").fromAlias("ege")), value(Boolean.FALSE)))
                .distinct();

        DQLSelectBuilder internalExamDisciplinesSB = new DQLSelectBuilder().fromDataSource(requestedCompDQL.buildQuery(), "inter")
                .column(property(EnrChosenEntranceExam.discipline().discipline().fromAlias("vvi").fromAlias("inter")))
                .where(eq(property(EnrChosenEntranceExam.maxMarkForm().passForm().internal().fromAlias("vvi").fromAlias("inter")), value(Boolean.TRUE)))
                .distinct();


        List<EnrDiscipline> stateDisciplines = getList(stateExamDisciplinesSB);
        List<EnrDiscipline> internalDisciplines = getList(internalExamDisciplinesSB);

        final Comparator<EnrDiscipline> disciplineComparator = new Comparator<EnrDiscipline>() {
            @Override
            public int compare(EnrDiscipline o1, EnrDiscipline o2) {
                return o1.getCode().compareTo(o2.getCode());
            }
        };
        Collections.sort(stateDisciplines, disciplineComparator);
        Collections.sort(internalDisciplines, disciplineComparator);

        requestedCompDQL.joinEntity("reqComp", DQLJoinType.left, EnrEnrollmentExtract.class, "extract", eq(property(EnrEnrollmentExtract.entity().fromAlias("extract")), property("reqComp")));
        requestedCompDQL.column(property("vvi")).column(property("extract")).column(property("reqComp"));

        List<Object[]> fromQueryData = getList(requestedCompDQL);


        Comparator<EduProgramSubject> groupComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {

                // Сравниваем поколение перечня
                int res = Boolean.compare(o1 instanceof EduProgramSubject2013, o2 instanceof  EduProgramSubject2013);
                if (res != 0) return -res;
                res = Boolean.compare(o1 instanceof EduProgramSubject2009, o2 instanceof  EduProgramSubject2009);
                if (res != 0) return -res;
                res = Boolean.compare(o1 instanceof EduProgramSubjectOkso, o2 instanceof  EduProgramSubjectOkso);
                if (res != 0) return -res;

                // сравниваем вид ОП

                final EduProgramKind o1Kind = o1.getSubjectIndex().getProgramKind();
                final EduProgramKind o2Kind = o2.getSubjectIndex().getProgramKind();
                res = Boolean.compare(o1Kind.isProgramBachelorDegree() || o1Kind.isProgramSpecialistDegree(), o2Kind.isProgramBachelorDegree() || o2Kind.isProgramSpecialistDegree());
                if (res != 0) return -res;
                res = o1Kind.getCode().compareToIgnoreCase(o2Kind.getCode());
                if (res != 0) return res;

                // сравниваем укрупненную группу

                res = o1.getGroupTitle().compareToIgnoreCase(o2.getGroupTitle());
                return res;
            }
        };

        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                return o1.getSubjectCode().compareToIgnoreCase(o2.getSubjectCode());
            }
        };

        Comparator<EnrProgramSetOrgUnit> programSetOrgUnitComparator = new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                return o1.getProgramSet().getTitle().compareToIgnoreCase(o2.getProgramSet().getTitle());
            }
        };

        Comparator<EnrRequestedCompetition> reqCompComparator = new Comparator<EnrRequestedCompetition>() {
            @Override
            public int compare(EnrRequestedCompetition o1, EnrRequestedCompetition o2) {

                return o1.getRequest().getEntrant().getId().compareTo(o2.getRequest().getEntrant().getId());
            }
        };

        Map<EduProgramSubject, Map<EduProgramSubject, Map<EnrProgramSetOrgUnit, Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>>>>> dataMap = new TreeMap<>(groupComparator);

        for (Object[] row : fromQueryData) {
            EnrChosenEntranceExam exam = (EnrChosenEntranceExam)row[0];
            EnrEnrollmentExtract extract = ((EnrEnrollmentExtract) row[1]);
            EnrRequestedCompetition reqComp = ((EnrRequestedCompetition) row[2]);
            EnrProgramSetOrgUnit programSetOU = reqComp.getCompetition().getProgramSetOrgUnit();
            EduProgramSubject subject = programSetOU.getProgramSet().getProgramSubject();
            PairKey<EnrEnrollmentExtract, EnrRequestedCompetition> pairKey = new PairKey<>(extract, reqComp);

            Map<EduProgramSubject, Map<EnrProgramSetOrgUnit, Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>>>> programSubjectMap = SafeMap.safeGet(dataMap, subject, TreeMap.class, subjectComparator);
            Map<EnrProgramSetOrgUnit, Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>>> programSetOUMap = SafeMap.safeGet(programSubjectMap, subject, TreeMap.class, programSetOrgUnitComparator);
            Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>> extractMap = SafeMap.safeGet(programSetOUMap, programSetOU, HashMap.class);
            Set<EnrChosenEntranceExam> vviSet = SafeMap.safeGet(extractMap, pairKey, HashSet.class);
            if (exam != null)
                vviSet.add(exam);
        }

        List<PairKey<Integer, String[]>> tTable = new ArrayList<>();

        WritableFont contentFont = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont subjectFont = new WritableFont(WritableFont.ARIAL, 10);
        subjectFont.setColour(Colour.GRAY_50);
        WritableFont groupFont = new WritableFont(WritableFont.ARIAL, 10);
        groupFont.setBoldStyle(WritableFont.BOLD);
        groupFont.setColour(Colour.GRAY_50);
        WritableCellFormat programSetFormat = new WritableCellFormat(contentFont);
        WritableCellFormat subjectFormat = new WritableCellFormat(subjectFont);
        WritableCellFormat groupFormat = new WritableCellFormat(groupFont);
        programSetFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        subjectFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        groupFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        programSetFormat.setWrap(true);
        subjectFormat.setWrap(true);
        groupFormat.setWrap(true);

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat format = new DecimalFormat("#0.0", otherSymbols);

        int index = printHeader(model, stateDisciplines, internalDisciplines, sheet);


        int rowNum = 1;
        for (Map.Entry<EduProgramSubject, Map<EduProgramSubject, Map<EnrProgramSetOrgUnit, Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>>>>> groupEntry : dataMap.entrySet()) {

            EduProgramSubject group = groupEntry.getKey();

            Set<EnrRequestedCompetition> onGroupBudget = new TreeSet<>(reqCompComparator);
            Set<EnrRequestedCompetition> onGroupTarget = new TreeSet<>(reqCompComparator);
            Set<EnrRequestedCompetition> onGroupContract = new TreeSet<>(reqCompComparator);
            Set<EnrRequestedCompetition> groupEnrolledFirstWave = new TreeSet<>(reqCompComparator);
            Set<EnrRequestedCompetition> groupEnrolledSecondWave = new TreeSet<>(reqCompComparator);
            Set<EnrRequestedCompetition> groupEnrolledThirdWave = new TreeSet<>(reqCompComparator);
            Map<EnrDiscipline, Map<EnrEntrant, Double>> groupStateExamMaxMarks = new HashMap<>();
            Map<EnrDiscipline, Map<EnrEntrant, Double>> groupInternalExamMaxMarks = new HashMap<>();
            int onGroupMinPlan = 0;
            int onGroupTargetPlan = 0;
            int onGroupContractPlan = 0;

            List<PairKey<Integer, String[]>> groupTablePart = new ArrayList<>();


            for (Map.Entry<EduProgramSubject, Map<EnrProgramSetOrgUnit, Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>>>> subjectEntry : groupEntry.getValue().entrySet()) {

                EduProgramSubject subject = subjectEntry.getKey();

                Set<EnrRequestedCompetition> onSubjectBudget = new TreeSet<>(reqCompComparator);
                Set<EnrRequestedCompetition> onSubjectTarget = new TreeSet<>(reqCompComparator);
                Set<EnrRequestedCompetition> onSubjectContract = new TreeSet<>(reqCompComparator);
                Set<EnrRequestedCompetition> subjectEnrolledFirstWave = new TreeSet<>(reqCompComparator);
                Set<EnrRequestedCompetition> subjectEnrolledSecondWave = new TreeSet<>(reqCompComparator);
                Set<EnrRequestedCompetition> subjectEnrolledThirdWave = new TreeSet<>(reqCompComparator);
                Map<EnrDiscipline, Map<EnrEntrant, Double>> subjStateExamMaxMarks = new HashMap<>();
                Map<EnrDiscipline, Map<EnrEntrant, Double>> subjInternalExamMaxMarks = new HashMap<>();
                int onSubjectMinPlan = 0;
                int onSubjectTargetPlan = 0;
                int onSubjectContractPlan = 0;

                List<PairKey<Integer, String[]>> subjectTablePart = new ArrayList<>();


                for (Map.Entry<EnrProgramSetOrgUnit, Map<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>>> programSetEntry : subjectEntry.getValue().entrySet()) {

                    EnrProgramSetOrgUnit programSetOrgUnit = programSetEntry.getKey();
                    EnrProgramSetBase programSet = programSetOrgUnit.getProgramSet();

                    Set<EnrRequestedCompetition> onProgramSetBudget = new TreeSet<>(reqCompComparator);
                    Set<EnrRequestedCompetition> onProgramSetTarget = new TreeSet<>(reqCompComparator);
                    Set<EnrRequestedCompetition> onProgramSetContract = new TreeSet<>(reqCompComparator);
                    Set<EnrRequestedCompetition> pSetEnrolledFirstWave = new TreeSet<>(reqCompComparator);
                    Set<EnrRequestedCompetition> pSetEnrolledSecondWave = new TreeSet<>(reqCompComparator);
                    Set<EnrRequestedCompetition> pSetEnrolledThirdWave = new TreeSet<>(reqCompComparator);
                    Map<EnrDiscipline, Map<EnrEntrant, Double>> stateExamMaxMarks = new HashMap<>();
                    Map<EnrDiscipline, Map<EnrEntrant, Double>> internalExamMaxMarks = new HashMap<>();

                    for (Map.Entry<PairKey<EnrEnrollmentExtract, EnrRequestedCompetition>, Set<EnrChosenEntranceExam>> pairEntry : programSetEntry.getValue().entrySet()) {
                        EnrEnrollmentExtract extract = pairEntry.getKey().getFirst();
                        EnrRequestedCompetition reqComp = pairEntry.getKey().getSecond();
                        Map<EnrDiscipline, Double> onReqCompStateMarks = new HashMap<>();
                        Map<EnrDiscipline, Double> onReqCompInterMarks = new HashMap<>();
                        final EnrEntrant entrant = reqComp.getRequest().getEntrant();

                        if (EnrReportStageSelector.enrollmentResultsCheckByPriority(reqComp.getState().getPriority()) && extract != null && extract.getOrder() != null && extract.getOrder().getCommitDate() != null){
                            Long commitTime = extract.getOrder().getCommitDate().getTime();

                            if (model.getFirstWaveDateFrom().getTime() < commitTime && model.getFirstWaveDateTo().getTime() > commitTime)
                                pSetEnrolledFirstWave.add(reqComp);
                            if (model.getSecondWaveDateFrom().getTime() < commitTime && model.getSecondWaveDateTo().getTime() > commitTime)
                                pSetEnrolledSecondWave.add(reqComp);
                            if (model.getThirdWaveDateFrom().getTime() < commitTime && model.getThirdWaveDateTo().getTime() > commitTime)
                                pSetEnrolledThirdWave.add(reqComp);
                        }

                        if (reqComp.getCompetition().getType().getCompensationType().isBudget()) {
                            onProgramSetBudget.add(reqComp);
                            if (reqComp instanceof EnrRequestedCompetitionTA)
                                onProgramSetTarget.add(reqComp);
                        }
                        else
                        {
                            onProgramSetContract.add(reqComp);
                        }

                        for (EnrChosenEntranceExam exam : pairEntry.getValue()) {
                            if (exam.getMaxMarkForm() == null || exam.getMaxMarkForm().getPassForm() == null) continue;
                            if (exam.getMaxMarkForm().getPassForm().isInternal())
                            {
                                final EnrDiscipline discipline = exam.getDiscipline().getDiscipline();
                                final Double markInMap = onReqCompInterMarks.get(discipline);
                                final Double markAsDouble = exam.getMarkAsDouble();
                                if (markInMap == null || markInMap.compareTo(markAsDouble) < 0)
                                    onReqCompInterMarks.put(discipline, markAsDouble);
                            }
                            else
                            {
                                final EnrDiscipline discipline = exam.getDiscipline().getDiscipline();
                                final Double markInMap = onReqCompStateMarks.get(discipline);
                                final Double markAsDouble = exam.getMarkAsDouble();
                                if (markInMap == null || markInMap.compareTo(markAsDouble) < 0)
                                    onReqCompStateMarks.put(discipline, markAsDouble);
                            }
                        }

                        for (Map.Entry<EnrDiscipline, Double> entry : onReqCompStateMarks.entrySet()) {
                            Map<EnrEntrant, Double> marks = SafeMap.safeGet(stateExamMaxMarks, entry.getKey(), HashMap.class);
                            marks.put(entrant, entry.getValue());
                        }

                        for (Map.Entry<EnrDiscipline, Double> entry : onReqCompInterMarks.entrySet()) {
                            Map<EnrEntrant, Double> marks = SafeMap.safeGet(internalExamMaxMarks, entry.getKey(), HashMap.class);
                            marks.put(entrant, entry.getValue());
                        }
                    }

                    final int ministerialPlan = programSetOrgUnit.getMinisterialPlan();
                    final int targetAdmPlan = programSetOrgUnit.getTargetAdmPlan();
                    final int contractPlan = programSetOrgUnit.getContractPlan();
                    onSubjectMinPlan += ministerialPlan;
                    onSubjectTargetPlan += targetAdmPlan;
                    onSubjectContractPlan += contractPlan;

                    final int budgets = onProgramSetBudget.size();
                    final int targets = onProgramSetTarget.size();
                    final int contracts = onProgramSetContract.size();
                    onSubjectBudget.addAll(onProgramSetBudget);
                    onSubjectTarget.addAll(onProgramSetTarget);
                    onSubjectContract.addAll(onProgramSetContract);

                    subjectEnrolledFirstWave.addAll(pSetEnrolledFirstWave);
                    subjectEnrolledSecondWave.addAll(pSetEnrolledSecondWave);
                    subjectEnrolledThirdWave.addAll(pSetEnrolledThirdWave);

                    List<String> row = new ArrayList<>();

                    row.add(String.valueOf(rowNum++)); // номер
                    row.add(subject.getTitleWithCode()); // направление
                    row.add(programSet.getTitle()); // ОП
                    row.add(String.valueOf(ministerialPlan)); // план на бюджет
                    row.add(String.valueOf(budgets)); // выбранных конкурсов бюджет
                    row.add(String.valueOf(targetAdmPlan)); // план на целевой
                    row.add(String.valueOf(targets)); // вк целевой
                    row.add(String.valueOf(contractPlan)); // план по договору
                    row.add(String.valueOf(contracts)); // вк договор

                    // егэ дисциплины
                    if (!stateDisciplines.isEmpty()) {
                        for (EnrDiscipline stateDiscipline : stateDisciplines) {
                            Map<EnrEntrant, Double> map = SafeMap.safeGet(stateExamMaxMarks, stateDiscipline, HashMap.class);
                            Map<EnrEntrant, Double> onSubjectMap = SafeMap.safeGet(subjStateExamMaxMarks, stateDiscipline, HashMap.class);
                            onSubjectMap.putAll(map);
                            row.add(format.format(calcAverage(map)));
                        }
                    } else row.add("");

                    // 4 пустых
                    for (int i = 0; i < 4; i++) {
                        row.add("");
                    }

                    // внутренние экзамены
                    if (!internalDisciplines.isEmpty())
                    {
                        for (EnrDiscipline internalDiscipline : internalDisciplines) {
                            Map<EnrEntrant, Double> map = SafeMap.safeGet(stateExamMaxMarks, internalDiscipline, HashMap.class);
                            Map<EnrEntrant, Double> onSubjectMap = SafeMap.safeGet(subjInternalExamMaxMarks, internalDiscipline, HashMap.class);
                            onSubjectMap.putAll(map);
                            row.add(format.format(calcAverage(map)));
                        }
                    } else row.add("");

                    // Волны
                    row.add(String.valueOf(pSetEnrolledFirstWave.size()));
                    row.add(String.valueOf(pSetEnrolledSecondWave.size()));
                    row.add(String.valueOf(pSetEnrolledThirdWave.size()));

                    row.add("");

                    // Конкурс
                    if (ministerialPlan > 0)
                        row.add(format.format(budgets * 1.0 / ministerialPlan));
                    else row.add("");
                    if (targetAdmPlan > 0)
                        row.add(format.format(targets * 1.0 / targetAdmPlan));
                    else row.add("");
                    if (contractPlan > 0)
                        row.add(format.format(contracts * 1.0 / contractPlan));
                    else row.add("");

                    subjectTablePart.add(new PairKey<Integer, String[]>(1, row.toArray(new String[row.size()])));

                }

                onGroupMinPlan += onSubjectMinPlan;
                onGroupTargetPlan += onSubjectTargetPlan;
                onGroupContractPlan += onSubjectContractPlan;

                final int budgets = onSubjectBudget.size();
                final int targets = onSubjectTarget.size();
                final int contracts = onSubjectContract.size();

                onGroupBudget.addAll(onSubjectBudget);
                onGroupTarget.addAll(onSubjectTarget);
                onGroupContract.addAll(onSubjectContract);

                groupEnrolledFirstWave.addAll(subjectEnrolledFirstWave);
                groupEnrolledSecondWave.addAll(subjectEnrolledSecondWave);
                groupEnrolledThirdWave.addAll(subjectEnrolledThirdWave);

                List<String> row = new ArrayList<>();

                row.add(""); // номер
                row.add(subject.getTitleWithCode()); // направление
                row.add(""); // ОП
                row.add(String.valueOf(onSubjectMinPlan)); // план на бюджет
                row.add(String.valueOf(budgets)); // выбранных конкурсов бюджет
                row.add(String.valueOf(onSubjectTargetPlan)); // план на целевой
                row.add(String.valueOf(targets)); // вк целевой
                row.add(String.valueOf(onSubjectContractPlan)); // план по договору
                row.add(String.valueOf(contracts)); // вк договор

                // егэ дисциплины
                if (!stateDisciplines.isEmpty())
                {
                    for (EnrDiscipline stateDiscipline : stateDisciplines) {
                        Map<EnrEntrant, Double> map = SafeMap.safeGet(subjStateExamMaxMarks, stateDiscipline, HashMap.class);
                        Map<EnrEntrant, Double> onGroupMap = SafeMap.safeGet(subjStateExamMaxMarks, stateDiscipline, HashMap.class);
                        onGroupMap.putAll(map);
                        row.add(format.format(calcAverage(map)));
                    }
                } else row.add("");

                // 4 пустых
                for (int i = 0; i < 4; i++) {
                    row.add("");
                }

                // внутренние экзамены
                if (!internalDisciplines.isEmpty())
                {
                    for (EnrDiscipline internalDiscipline : internalDisciplines) {
                        Map<EnrEntrant, Double> map = SafeMap.safeGet(subjInternalExamMaxMarks, internalDiscipline, HashMap.class);
                        Map<EnrEntrant, Double> onGroupMap = SafeMap.safeGet(groupInternalExamMaxMarks, internalDiscipline, HashMap.class);
                        onGroupMap.putAll(map);
                        row.add(format.format(calcAverage(map)));
                    }
                } else row.add("");

                // Волны
                row.add(String.valueOf(subjectEnrolledFirstWave.size()));
                row.add(String.valueOf(subjectEnrolledSecondWave.size()));
                row.add(String.valueOf(subjectEnrolledThirdWave.size()));

                row.add("");

                // Конкурс
                if (onSubjectMinPlan > 0)
                    row.add(format.format(budgets * 1.0 / onSubjectMinPlan));
                else row.add("");
                if (onSubjectTargetPlan > 0)
                    row.add(format.format(targets * 1.0 / onSubjectTargetPlan));
                else row.add("");
                if (onSubjectContractPlan > 0)
                    row.add(format.format(contracts * 1.0 / onSubjectContractPlan));
                else row.add("");

                groupTablePart.add(new PairKey<Integer, String[]>(2, row.toArray(new String[row.size()])));

                groupTablePart.addAll(subjectTablePart);
            }


            final int budgets = onGroupBudget.size();
            final int targets = onGroupTarget.size();
            final int contracts = onGroupContract.size();

            List<String> row = new ArrayList<>();

            row.add(""); // номер
            row.add(group.getGroupTitle()); // группа
            row.add(""); // ОП
            row.add(String.valueOf(onGroupMinPlan)); // план на бюджет
            row.add(String.valueOf(budgets)); // выбранных конкурсов бюджет
            row.add(String.valueOf(onGroupTargetPlan)); // план на целевой
            row.add(String.valueOf(targets)); // вк целевой
            row.add(String.valueOf(onGroupContractPlan)); // план по договору
            row.add(String.valueOf(contracts)); // вк договор

            // егэ дисциплины
            if (!stateDisciplines.isEmpty())
            {
                for (EnrDiscipline stateDiscipline : stateDisciplines) {
                    Map<EnrEntrant, Double> map = SafeMap.safeGet(groupStateExamMaxMarks, stateDiscipline, HashMap.class);
                    row.add(format.format(calcAverage(map)));
                }
            } else row.add("");

            // 4 пустых
            for (int i = 0; i < 4; i++) {
                row.add("");
            }

            // внутренние экзамены
            if (!internalDisciplines.isEmpty())
            {
                for (EnrDiscipline internalDiscipline : internalDisciplines) {
                    Map<EnrEntrant, Double> map = SafeMap.safeGet(groupInternalExamMaxMarks, internalDiscipline, HashMap.class);
                    row.add(format.format(calcAverage(map)));
                }
            } else row.add("");

            // Волны
            row.add(String.valueOf(groupEnrolledFirstWave.size()));
            row.add(String.valueOf(groupEnrolledSecondWave.size()));
            row.add(String.valueOf(groupEnrolledThirdWave.size()));

            row.add("");

            // Конкурс
            if (onGroupMinPlan > 0)
                row.add(format.format(budgets * 1.0 / onGroupMinPlan));
            else row.add("");
            if (onGroupTargetPlan > 0)
                row.add(format.format(targets * 1.0 / onGroupTargetPlan));
            else row.add("");
            if (onGroupContractPlan > 0)
                row.add(format.format(contracts * 1.0 / onGroupContractPlan));
            else row.add("");

            tTable.add(new PairKey<Integer, String[]>(3, row.toArray(new String[row.size()])));
            tTable.addAll(groupTablePart);

        }

        for (PairKey<Integer, String[]> pair : tTable) {
            String[] row = pair.getSecond();
            switch (pair.getFirst()) {
                case (1) :   ExcelReportUtils.printRowHideNulls(sheet, row, index++, programSetFormat); break;
                case (2) :   ExcelReportUtils.printRowHideNulls(sheet, row, index++, subjectFormat); break;
                case (3) :   ExcelReportUtils.printRowHideNulls(sheet, row, index++, groupFormat); break;
            }
        }



        workbook.write();
        workbook.close();

        return out.toByteArray();

    }


    private static int printHeader(EnrReportAdmissionPlansAddUI model, List<EnrDiscipline> stateDisciplines, List<EnrDiscipline> internalDisciplines, WritableSheet sheet) throws JXLException, IOException
    {
        int nextFreeRow = 0;
        int stateCol = 0;
        int internalCol = 0;
        WritableFont labelFont = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 10);

        WritableFont contentFont = new WritableFont(WritableFont.ARIAL, 10);

        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat headerSecondFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);
        headerSecondFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerSecondFormat.setAlignment(Alignment.CENTRE);
        headerSecondFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerSecondFormat.setWrap(true);
        headerSecondFormat.setOrientation(Orientation.PLUS_90);
        WritableCellFormat titleFormat = new WritableCellFormat(headerFont);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        List<String[]> hTable = new FilterParametersPrinter()
                .setParallelSelector(model.getParallelSelector())
                .setDateSelector(model.getDateSelector())
                .setStageSelector(model.getStageSelector())
                .addVariableParameter("Первая волна зачисления с", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFirstWaveDateFrom()))
                .addVariableParameter("Первая волна зачисления по", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFirstWaveDateTo()))
                .addVariableParameter("Вторая волна зачисления с", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getSecondWaveDateFrom()))
                .addVariableParameter("Вторая волна зачисления по", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getSecondWaveDateTo()))
                .addVariableParameter("Третья волна зачисления с", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getThirdWaveDateFrom()))
                .addVariableParameter("Третья волна зачисления по", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getThirdWaveDateTo()))
                .getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo(), model.getEnrollmentCampaign());

        sheet.addCell(new Label(1, 0, "Сводка о планах приема, средних баллах, результатах зачисления, " + new SimpleDateFormat("dd.MM.yyyy").format(new Date()), labelFormat));

        for (int i = 0; i < hTable.size(); i++) {
            sheet.addCell(new Label(1, i+2, hTable.get(i)[0], titleFormat));
            sheet.addCell(new Label(2, i+2, hTable.get(i)[1], contentFormat));
            nextFreeRow = i+3;
        }



        List<String> firstLineHC = new ArrayList<>();
        List<String> secondLineHC = new ArrayList<>();

        CellMergeMap mergeMap = new CellMergeMap();
        int startCol;
        int col = 0;
        int row = nextFreeRow;

        firstLineHC.add("№ п/п");
        secondLineHC.add("");
        mergeMap.addMapping(col, row, col++, row+1);

        firstLineHC.add("Укрупненная группа / Направления подготовки");
        secondLineHC.add("");
        mergeMap.addMapping(col, row, col++, row+1);

        firstLineHC.add("Образовательная программа");
        secondLineHC.add("");
        mergeMap.addMapping(col, row, col++, row+1);

        firstLineHC.add("Бюджетные места");
        secondLineHC.add("Количество мест");
        firstLineHC.add("");
        secondLineHC.add("Количество заявлений");
        mergeMap.addMapping(col, row, ++col, row);
        col++;


        firstLineHC.add("из них целевой прием");
        secondLineHC.add("Количество мест");
        firstLineHC.add("");
        secondLineHC.add("Количество заявлений");
        mergeMap.addMapping(col, row, ++col, row);
        col++;

        firstLineHC.add("Места с полным возмещением затрат");
        secondLineHC.add("Количество мест");
        firstLineHC.add("");
        secondLineHC.add("Количество заявлений");
        mergeMap.addMapping(col, row, ++col, row);
        col++;

        firstLineHC.add("Средние баллы по ЕГЭ по направлениям подготовки (по специальностям)");

        startCol = col;
        stateCol = col;
        boolean firstIteration = true;

        if (!stateDisciplines.isEmpty()) {
            for (EnrDiscipline discipline : stateDisciplines) {
                secondLineHC.add(discipline.getTitle());
                if (!firstIteration) {
                    firstLineHC.add("");
                    col++;
                }
                firstIteration = false;
            }
        } else secondLineHC.add("");

        mergeMap.addMapping(startCol, row, col++, row);


        firstLineHC.add("Средние баллы по дополнительным испытаниям по направлениям подготовки (по специальностям)");
        secondLineHC.add("");
        firstLineHC.add("");
        secondLineHC.add("");
        firstLineHC.add("");
        secondLineHC.add("");
        firstLineHC.add("");
        secondLineHC.add("");

        mergeMap.addMapping(col, row, col+3, row);
        col += 4;

        firstLineHC.add("Средние баллы по вступительным испытаниям, форму и перечень которых вузы определяют самостоятельно");

        startCol = col;
        internalCol = col;

        if (!internalDisciplines.isEmpty()) {
            firstIteration = true;
            for (EnrDiscipline discipline : internalDisciplines) {
                secondLineHC.add(discipline.getTitle());
                if (!firstIteration) {
                    firstLineHC.add("");
                    col++;
                }
                firstIteration = false;
            }
        } else secondLineHC.add("");

        mergeMap.addMapping(startCol, row, col++, row);



        firstLineHC.add("Количество зачисленных абитуриентов");
        secondLineHC.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFirstWaveDateTo()));
        firstLineHC.add("");
        secondLineHC.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getSecondWaveDateTo()));
        firstLineHC.add("");
        secondLineHC.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getThirdWaveDateTo()));

        mergeMap.addMapping(col, row, col+2, row);
        col += 3;

        int reverseThatColumn = col;
        int reverseThatColumnRow = row;

        firstLineHC.add("");
        secondLineHC.add("");

        mergeMap.addMapping(col, row, col++, row + 1);

        firstLineHC.add("Конкурс (количество поданных заявлений на одно место)");
        secondLineHC.add("бюджет");
        firstLineHC.add("");
        secondLineHC.add("целевой прием");
        firstLineHC.add("");
        secondLineHC.add("по договору");

        mergeMap.addMapping(col, row, col + 2, row);


        mergeMap.makeMerges(sheet);

        col += 3;

        List<Integer> colSizes = new ArrayList<>();
        colSizes.add(5);
        colSizes.add(35);
        colSizes.add(35);

        int stateColSize = stateDisciplines.size() > 4 ? 6 : stateDisciplines.size() == 0 ? 24 : 24 / stateDisciplines.size();
        int internalColSize = internalDisciplines.size() > 4 ? 6 : internalDisciplines.size() == 0 ? 24 : 24 / internalDisciplines.size();

        int lastStateCol = stateCol + stateDisciplines.size();
        int lastInternalCol = internalCol + internalDisciplines.size();


        for (int i = 3; i < col; i++) {
            if (i == stateCol || (i > stateCol && i < lastStateCol))
                colSizes.add(stateColSize);
            else if (i == internalCol || (i > internalCol && i < lastInternalCol))
                colSizes.add(internalColSize);
            else
                colSizes.add(6);
        }



        for (int i = 0; i < colSizes.size(); i++) {
            sheet.setColumnView(i, colSizes.get(i));
        }
        sheet.setRowView(row, 1850);

        ExcelReportUtils.printrow(sheet, firstLineHC, row++, headerFormat);

        sheet.setRowView(row, 1950);

        ExcelReportUtils.printrow(sheet, secondLineHC, row, headerSecondFormat);


        sheet.addCell(new Label(reverseThatColumn, reverseThatColumnRow, "Количество мест по допол-му приему", headerSecondFormat));


        return row + 1;
    }

    private double calcAverage(Map<EnrEntrant, Double> values)
    {
        if (values.size() == 0)
            return 0.0;
        double average = 0.0;
        int count = 0;
        for (Map.Entry<EnrEntrant, Double> entry : values.entrySet()) {

            final Double value = entry.getValue();
            if (value > 0) {
                average += value;
                count++;
            }
        }
        return (average / count);
    }

}
