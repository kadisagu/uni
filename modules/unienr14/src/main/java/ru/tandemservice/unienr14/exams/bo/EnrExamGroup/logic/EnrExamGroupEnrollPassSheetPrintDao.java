/**
 *$Id: EnrExamGroupEnrollPassSheetPrintDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 08.07.13
 */
public class EnrExamGroupEnrollPassSheetPrintDao extends UniBaseDao implements IEnrExamGroupEnrollPassSheetPrintDao
{
    @Override
    public RtfDocument createEnrPassSheetRtf(long examGroupId)
    {
        final EnrExamGroup examGroup = getNotNull(EnrExamGroup.class, examGroupId);

        if (validate(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET).hasErrors())
            return null;

        final RtfDocument document = getDocument(EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET);

        injectModifier(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET).modify(document);
        injectTableModifier(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET).modify(document);

        return document;
    }

    @Override
    public RtfDocument createEnrPassSheetCodedRtf(long examGroupId)
    {
        final EnrExamGroup examGroup = getNotNull(EnrExamGroup.class, examGroupId);

        if (validate(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED).hasErrors())
            return null;

        final RtfDocument document = getDocument(EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED);

        injectModifier(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED).modify(document);
        injectTableModifier(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED).modify(document);

        return document;
    }

    @Override
    public RtfDocument createEnrPassSheetMarksRtf(long examGroupId)
    {
        final EnrExamGroup examGroup = getNotNull(EnrExamGroup.class, examGroupId);

        if (validate(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS).hasErrors())
            return null;

        final RtfDocument document = getDocument(EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS);

        injectModifier(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS).modify(document);
        injectTableModifier(examGroup, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS).modify(document);

        return document;
    }

    protected ErrorCollector validate(EnrExamGroup examGroup, String sheetCode)
    {
        final ErrorCollector errorCollector = ContextLocal.getErrorCollector();

        if (!existsEntity(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.L_EXAM_GROUP, examGroup))
            errorCollector.add(EnrExamGroupManager.instance().getProperty("examGroup.print.emptyException"));

        return errorCollector;
    }

    protected RtfDocument getDocument(String examGroupPrintFormCode)
    {
        final EnrScriptItem scriptItem = getCatalogItem(EnrScriptItem.class, examGroupPrintFormCode);
         return new RtfReader().read(scriptItem.getCurrentTemplate());
    }

    protected RtfInjectModifier injectModifier(final EnrExamGroup examGroup, String sheetType)
    {
        final RtfInjectModifier modifier = new RtfInjectModifier();

        /* подготавливаем данные БД */
        final List<EnrExamScheduleEvent> examScheduleEventList = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "e").column(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("e")))
                .where(eq(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("e")), value(examGroup)))
                .order(property(EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().fromAlias("e")))
                .createStatement(getSession()).list();

        final Set<String> uniqCommission = new LinkedHashSet<>();
        final Set<Date> uniqpassDate = new LinkedHashSet<>();
        final RtfString commissionRtf = new RtfString();
        final StringBuilder passDateBuilder = new StringBuilder();
        for (EnrExamScheduleEvent scheduleEvent : examScheduleEventList)
        {
            if (!commissionRtf.toList().isEmpty())
                commissionRtf.par();
            if (uniqCommission.add(scheduleEvent.getCommission()))
                commissionRtf.append(scheduleEvent.getCommission());

            if (passDateBuilder.length() > 0)
                passDateBuilder.append(", ");
            if (uniqpassDate.add(scheduleEvent.getScheduleEvent().getDurationBegin()))
                passDateBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleEvent.getScheduleEvent().getDurationBegin()));
        }

        OrgUnit territorialOrgUnit = examGroup.getTerritorialOrgUnit();

        /* метки */
        modifier.put("accTitle", territorialOrgUnit.getPrintTitle());
        modifier.put("examGroupTitle", examGroup.getTitle());
        modifier.put("DFS", examGroup.getDiscipline().getTitle() + " (" + examGroup.getPassForm().getTitle() + ")");
        modifier.put("commission", commissionRtf);
        modifier.put("passDate", passDateBuilder.toString());

        return modifier;
    }

    protected RtfTableModifier injectTableModifier(final EnrExamGroup examGroup, String sheetType)
    {
        final RtfTableModifier tableModifier = new RtfTableModifier();

        /* подготавливаем данные БД */
        final List<EnrExamPassDiscipline> discList = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "d").column(property("d"))
                .where(eq(property(EnrExamPassDiscipline.examGroup().fromAlias("d")), value(examGroup)))
                .createStatement(getSession()).list();

        int number = 1;
        final List<String[]> table = new ArrayList<>();
        for (EnrExamPassDiscipline disc : discList)
        {
            String[] line;
            if (EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET.equalsIgnoreCase(sheetType))
                line = writeEnrPassSheetLine(disc);
            else if (EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED.equalsIgnoreCase(sheetType))
                line = writeEnrPassSheetCodedLine(disc);
            else if (EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS.equalsIgnoreCase(sheetType))
                line = writeEnrPassSheetMarksLine(disc);
            else
                throw new IllegalStateException();

            line[0] = String.valueOf(number++);

            table.add(line);
        }

        /* метки */
        tableModifier.put("T", table.toArray(new String[table.size()][]));

        return tableModifier;
    }

    protected String[] writeEnrPassSheetLine(EnrExamPassDiscipline disc)
    {
        return new String[]{"", disc.getCode(), disc.getEntrant().getPerson().getFullFio()};
    }

    protected String[] writeEnrPassSheetCodedLine(EnrExamPassDiscipline disc)
    {
        return new String[]{"", disc.getCode()};
    }

    protected String[] writeEnrPassSheetMarksLine(EnrExamPassDiscipline disc)
    {
        return new String[]{"", disc.getCode(), disc.getEntrant().getPerson().getFullFio(), disc.getAbsenceNote() != null ? disc.getAbsenceNote().getShortTitle() : disc.getMarkAsString()};
    }
}
