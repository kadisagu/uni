/* $Id:$ */
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.IEnrPAQuotaFreeWrapper;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/30/14
 */
public class EnrProgramAllocationDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_ALLOCATION_ID = "allocation";
    public static final String VIEW_PROP_FORMING_DATE = "formingDate";
    public static final String VIEW_PROP_ADD_DISABLED = "addDisabled";
    public static final String VIEW_PROP_EDIT_DISABLED = "editDisabled";
    public static final String VIEW_PROP_DELETE_DISABLED = "deleteDisabled";
    public static final String VIEW_PROP_QUOTA = "quota";


    public EnrProgramAllocationDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrProgramSetOrgUnit.class, "m");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("m"));
        dql.where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("m")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))));
        dql.where(or(
            exists(new DQLSelectBuilder()
                .fromEntity(EnrRequestedProgram.class, "p").column("p")
                .joinPath(DQLJoinType.inner, EnrRequestedProgram.requestedCompetition().fromAlias("p"), "req")
                .where(eq(property("req", EnrRequestedCompetition.competition().programSetOrgUnit()), property("m")))
                .where(eq(property("req", EnrRequestedCompetition.competition().allowProgramPriorities()), value(Boolean.TRUE)))
                .where(eq(property("req", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                .buildQuery()
            ),
            exists(new DQLSelectBuilder()
                .fromEntity(EnrProgramAllocation.class, "a").column("a")
                .where(eq(property("a", EnrProgramAllocation.programSetOrgUnit()), property("m")))
                .buildQuery()
            )
        ));

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(isPageable())
            .build();

        Map<Long, EnrProgramAllocation> allocMap = new HashMap<>();
        for (EnrProgramAllocation alloc : DataAccessServices.dao().getList(EnrProgramAllocation.class, EnrProgramAllocation.programSetOrgUnit().programSet().enrollmentCampaign().s(), (Object) context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))) {
            allocMap.put(alloc.getProgramSetOrgUnit().getId(), alloc);
        }

        for (DataWrapper wrapper: DataWrapper.wrap(output)) {
            EnrProgramAllocation alloc = allocMap.get(wrapper.getId());
            wrapper.setProperty(VIEW_PROP_ADD_DISABLED, alloc != null);
            wrapper.setProperty(VIEW_PROP_EDIT_DISABLED, alloc == null);
            wrapper.setProperty(VIEW_PROP_DELETE_DISABLED, alloc == null);
            wrapper.setProperty(VIEW_PROP_FORMING_DATE, alloc == null ? null : alloc.getFormingDate());
            wrapper.setProperty(VIEW_PROP_ALLOCATION_ID, alloc == null ? null : alloc.getId());

            if (alloc != null) {
                IEnrPAQuotaFreeWrapper freeQuota = EnrProgramAllocationManager.instance().dao().getFreeQuota(alloc.getId());
                Map<Long, String> quotaMap = EnrProgramAllocationManager.instance().dao().getQuotaHTMLDescription(freeQuota);
                wrapper.setProperty(VIEW_PROP_QUOTA, quotaMap.get(0L));
            } else {
                wrapper.setProperty(VIEW_PROP_QUOTA, "");
            }
        }

        return output;
    }
}


