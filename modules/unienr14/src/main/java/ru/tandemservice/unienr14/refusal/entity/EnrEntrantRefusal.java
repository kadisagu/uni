package ru.tandemservice.unienr14.refusal.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.refusal.entity.gen.EnrEntrantRefusalGen;

/**
 * Отказ в приеме документов
 */
public class EnrEntrantRefusal extends EnrEntrantRefusalGen
{
    @Override
    @EntityDSLSupport(parts = {EnrEntrantRefusalGen.P_LAST_NAME, EnrEntrantRefusalGen.P_FIRST_NAME, EnrEntrantRefusalGen.P_MIDDLE_NAME})
    public String getFullName(){
        StringBuilder fullName = new StringBuilder(getLastName()).append(" ").append(getFirstName());
        if (getMiddleName() != null) {
            fullName.append(" ").append(getMiddleName());
        }
        return fullName.toString();
    }

}