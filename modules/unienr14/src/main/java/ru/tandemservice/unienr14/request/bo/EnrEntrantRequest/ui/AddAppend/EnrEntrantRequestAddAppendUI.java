/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.EnrCompetitionManager;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.ExternalOrgUnitDSHandler;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.ICompetitionAddonCustomizer;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EnrRequestedCompetitionWrapper;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/24/13
 */
@Input({
    @Bind(key = "entrantId", binding = "entrant.id", required = true),
    @Bind(key = "entrantRequestId", binding = "entrantRequest.id")
})
public class EnrEntrantRequestAddAppendUI extends UIPresenter
{
    public boolean isInWizard() {
        return SimpleWizardUIPresenter.isInWizard(this) || EnrEntrantRequestAddWizardWizardUI.isInWizard(this);
    }

    private EnrEntrant entrant = new EnrEntrant();
    private EnrEntrantRequest entrantRequest = new EnrEntrantRequest();

    // выбор конкурсов
    private List<EnrCompetitionType> competitionTypeList;
    private List<EnrEduLevelRequirement> eduLevelRequirementList;
    private EduProgramForm form;
    private List<EduProgramSubject> subjectList;
    private EnrOrgUnit orgUnit;
    private OrgUnit formativeOrgUnit;

    private StaticListDataSource<EnrCompetition> possibleCompetitionDS;
    private boolean tooManyPossibleCompetitions;

    public boolean enrollmentCommissionRequired = false;

    // список выбранных конкурсов
    private List<EnrRequestedCompetitionWrapper> requestedCompetitionList;
    private EnrRequestedCompetitionWrapper currentRow;
    private EnrRequestedCompetitionWrapper editedRow;
    private Integer editedRowIndex;

    // параметры работы формы
    private boolean regDateDisabled = false;
    private boolean regDateVisible = true;  // todo надо ли это? в старом абитуриенте был вообще ключ в property-файле
    private boolean regNumberVisible = true;

    private boolean initialized = false;
    private boolean eduLevelRequirementVisible;

    private List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs;

    // true, false - значения, null - не показывать фильтрацию
    private Boolean stateExamRestriction = null;
    private Set<Long> filteredStateExamCompetitionIds;
    private boolean onlyCrimea = false;

    @Override
    public void onComponentRefresh()
    {
        prepareFormData();
        setInitialized(true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEntrant().getEnrollmentCampaign());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_PERSON, getEntrant().getPerson());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE, getEntrantRequest().getType());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST, getEntrantRequest().isReceiveEduLevelFirst());
        dataSource.put("entrant", getEntrant().getId());

        dataSource.put(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS, (Boolean.TRUE.equals(getStateExamRestriction()) ? getFilteredStateExamCompetitionIds() : null));
        dataSource.put(EnrEntrantRequestManager.BIND_FORM, getForm());
        dataSource.put(EnrEntrantRequestManager.BIND_ORG_UNIT, getOrgUnit());
        dataSource.put(EnrEntrantRequestManager.BIND_FORMATIVE_ORG_UNIT, getFormativeOrgUnit());
        dataSource.put(EnrEntrantRequestManager.BIND_SUBJECT_LIST, getSubjectList());
        dataSource.put(EnrEntrantRequestManager.BIND_COMPETITION_TYPE_LIST, getCompetitionTypeList());
        dataSource.put(EnrEntrantRequestManager.BIND_EDU_LEVEL_REQ_LIST, getEduLevelRequirementList());
        dataSource.put(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL, getEntrantRequest().getEduDocument() != null ? getEntrantRequest().getEduDocument().getEduLevel() : null);

        if (editedRow != null)
        {
            dataSource.put(EnrEntrantRequestManager.BIND_TARGET_ADMISSION_KIND, getEditedRow().getTargetAdmissionKind());
            dataSource.put(EnrEntrantRequestManager.BIND_PROGRAM_SET, getEditedRow().getCompetition().getProgramSetOrgUnit().getProgramSet());
            dataSource.put(EnrTargetAdmissionManager.BIND_PROGRAM_SET_ORG_UNIT, getEditedRow().getCompetition().getProgramSetOrgUnit());
            if (getEditedRow().isTargetContractProgramSet())
                dataSource.put(ExternalOrgUnitDSHandler.PROGRAM_SET, Collections.singletonList(getEditedRow().getCompetition().getProgramSetOrgUnit().getProgramSet()));
        }

        if (EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_EXCLUSIVE.equals(dataSource.getName()) || EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_EXCLUSIVE.equals(dataSource.getName()))
            dataSource.put(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE, EnrBenefitTypeCodes.EXCLUSIVE);
        if (EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_PREFERENCE.equals(dataSource.getName()) || EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_PREFERENCE.equals(dataSource.getName()))
            dataSource.put(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE, EnrBenefitTypeCodes.PREFERENCE);
        if (EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_NO_EXAM.equals(dataSource.getName()) || EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_NO_EXAM.equals(dataSource.getName()))
            dataSource.put(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE, EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS);

    }

    // actions

    public void onChangeRequestType()
    {
        prepareEduLevelRequirement();
        prepareStateExamFilter();
        preparePossibleDSColumns();
        prepareRequestedCompetitionList();
        onRefreshSelectionParams();
    }

    public void onRefreshSelectionParams() {
        onRefreshPossibleDS();
    }

    public void onRefreshPossibleDS() {

        if (getPossibleCompetitionDS() == null || getEntrantRequest().getType() == null)
            return;

        getPossibleCompetitionDS().setupRows(Collections.<EnrCompetition>emptyList());
        final String alias = "c";
        final DQLSelectBuilder dql = new EnrEntrantRequestManager.CompetitionDQLBuilder()
                .campaign(getEntrant().getEnrollmentCampaign())
                .requestType(getEntrantRequest().getType())
                .receiveEduLevelFirst(getEntrantRequest().isReceiveEduLevelFirst())
                .eduDocumentLevel(getEntrantRequest().getEduDocument() != null ? getEntrantRequest().getEduDocument().getEduLevel() : null)
                .programForm(getForm())
                .orgUnit(getOrgUnit())
                .formativeOrgUnit(getFormativeOrgUnit())
                .subjectList(getSubjectList())
                .eduLevelRequirementList(getEduLevelRequirementList())
                .competitionTypeList(getCompetitionTypeList())
                .onlyCrimea(isOnlyCrimea())
                .build(alias);

        dql.column(alias);
        dql.order(property(EnrCompetition.programSetOrgUnit().programSet().title().fromAlias(alias)));
        dql.order(property(EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().fullTitle().fromAlias(alias)));
        dql.order(property(EnrCompetition.programSetOrgUnit().programSet().programForm().code().fromAlias(alias)));
        dql.order(property(EnrCompetition.type().code().fromAlias(alias)));
        dql.order(property(EnrCompetition.eduLevelRequirement().code().fromAlias(alias)));

        if (getRequestedCompetitionList() != null && !getRequestedCompetitionList().isEmpty()) {
            dql.where(notIn(property(EnrCompetition.id().fromAlias("c")), CollectionUtils.collect(getRequestedCompetitionList(), wrapper -> wrapper.getCompetition().getId())));
        }

        // Доп. логика проектнх слоев
        for (IUIAddon addon : getConfig().getAddons().values())
        {
            if (addon instanceof ICompetitionAddonCustomizer) {
                ((ICompetitionAddonCustomizer) addon).customizeCompetitionDQL(dql, alias);
            }
        }

        List<EnrCompetition> result = dql.createStatement(_uiSupport.getSession()).list();
        if (Boolean.TRUE.equals(getStateExamRestriction())) {
            final Set<Long> filteredCompetitionIds = getFilteredStateExamCompetitionIds(); // из очень много, в in запихивать нельзя
            if (null != filteredCompetitionIds) {
                CollectionUtils.filter(result, object -> filteredCompetitionIds.contains(object.getId()));
            }
        }

        getPossibleCompetitionDS().setupRows(result);
        setTooManyPossibleCompetitions(getPossibleCompetitionDS().getRowList().size() > 30);

        clearSelectedDirections();
    }

    public void onClickSelect() {

        List<EnrRequestedCompetitionWrapper> justSelectedDirections = new ArrayList<>();

        final Collection<IEntity> objectList = ((CheckboxColumn)getPossibleCompetitionDS().getColumn("select")).getSelectedObjects();

        for (IEntity entity : objectList) {
            {
                EnrCompetition competition = IUniBaseDao.instance.get().getNotNull(EnrCompetition.class, entity.getId());

                EnrRequestedCompetitionWrapper wrapper = new EnrRequestedCompetitionWrapper(competition.getId(), competition.getTitle());
                wrapper.setCompetition(competition);

                justSelectedDirections.add(wrapper);
            }
        }

        Set<Long> selectedSubjectIds = new HashSet<>();
        selectedSubjectIds.addAll(CollectionUtils.collect(CollectionUtils.union(justSelectedDirections, getRequestedCompetitionList()), wrapper -> {
            EduProgramSubject programSubject = wrapper.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            if (programSubject.getSubjectIndex().getProgramKind().isProgramBachelorDegree() || programSubject.getSubjectIndex().getProgramKind().isProgramSpecialistDegree())
                return programSubject.getId();
            return null;
        }));
        selectedSubjectIds.remove(null);
        if (selectedSubjectIds.size() > 3) {
            throw new ApplicationException("Невозможно добавить выбранные конкурсы: абитуриент может подать заявления не более чем на три направления (специальности) по совокупности всех выбранных образовательных программ бакалавриата и специалитета.");
        }

        getRequestedCompetitionList().addAll(justSelectedDirections);

        onRefreshPossibleDS();
    }

    public void onClickApply() {
        validate();
        if (getUserContext().getErrorCollector().hasErrors())
            return;

        try {
            if (saveOrUpdateEntrantRequest()) return;

            deactivate(
                new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), getEntrantRequest().getId())
                .add("entrantRequestId", getEntrantRequest().getId())
                .buildMap()

            );
        }
        catch (Exception e) {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    // действия со списком выбранных направлений

    public void onClickEditRow()
    {
        if (getEditedRow() != null)
            return;

        EnrRequestedCompetitionWrapper selectedRow = findRow();
        EnrRequestedCompetitionWrapper row = new EnrRequestedCompetitionWrapper(selectedRow);
        setEditedRow(row);
        setEditedRowIndex(getRequestedCompetitionList().indexOf(row));
    }

    public void onClickDeleteRow()
    {
        EnrRequestedCompetitionWrapper row = findRow();
        if (null != row)
            getRequestedCompetitionList().remove(row);
        onRefreshPossibleDS();
    }

    public void onClickMoveRowUp()
    {
        List<EnrRequestedCompetitionWrapper> rows = getRequestedCompetitionList();
        EnrRequestedCompetitionWrapper baseRow = findRow();

        int index = rows.indexOf(baseRow);
        if (index > 0) { Collections.swap(rows, index-1, index); }
    }

    public void onClickMoveRowDown()
    {
        List<EnrRequestedCompetitionWrapper> rows = getRequestedCompetitionList();
        EnrRequestedCompetitionWrapper baseRow = findRow();

        int index = rows.indexOf(baseRow);
        if (index < rows.size()-1) { Collections.swap(rows, index, index+1); }
    }


    public void onClickSaveRow()
    {
        if (getEditedRowIndex() != null && getRequestedCompetitionList().size() > getEditedRowIndex() && getEditedRowIndex() >= 0)
            getRequestedCompetitionList().set(getEditedRowIndex(), getEditedRow());

        setEditedRow(null);
    }

    public void onClickCancelEdit()
    {
        setEditedRowIndex(null);
        setEditedRow(null);
    }

    // presenter

    public boolean isIdentityCardDisabled() { return false; }
    public boolean isEduDocumentDisabled() { return false; }
    public boolean isRequestTypeDisabled() { return false; }

    public String getFormTitle() {
        if (isAppendForm())
            return "Добавление выбранных конкурсов в заявление №" + getEntrantRequest().getStringNumber();
        else
            return "Добавление заявления";
    }

    public boolean isAppendForm() {
        return getEntrantRequest().getId() != null;
    }

    public boolean isShowRequestDataBlock() {
        return !isAppendForm();
    }

    public boolean isCurrentRowFirst() {
        EnrRequestedCompetitionWrapper currentRow = getCurrentRow();
        if (null == currentRow) { return false; }

        List<EnrRequestedCompetitionWrapper> rowList = getRequestedCompetitionList();
        if (rowList.isEmpty()) { return false; }

        return currentRow.equals(rowList.get(0));
    }

    public boolean isCurrentRowLast() {
        EnrRequestedCompetitionWrapper currentRow = getCurrentRow();
        if (null == currentRow) { return false; }

        List<EnrRequestedCompetitionWrapper> rowList = getRequestedCompetitionList();
        if (rowList.isEmpty()) { return false; }

        return currentRow.equals(rowList.get(rowList.size()-1));
    }

    public boolean isEditMode()
    {
        return getEditedRow() != null;
    }

    public boolean isCurrentRowInEditMode()
    {
        return getEditedRow() != null && getEditedRow().equals(getCurrentRow());
    }

    public boolean isRequestTypeBS(){ return getEntrantRequest().getType() != null && getEntrantRequest().getType().getCode().equals(EnrRequestTypeCodes.BS); }

    public boolean isEduDocumentLevelSredneeObsthee(){ return getEntrantRequest().getEduDocument() != null && getEntrantRequest().getEduDocument().getEduLevel() != null && getEntrantRequest().getEduDocument().getEduLevel().getCode().equals(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE); }

    // utils

    private void clearSelectedDirections() {
        ((CheckboxColumn) getPossibleCompetitionDS().getColumn("select")).setSelectedObjects(new ArrayList<>());
    }

    private EnrRequestedCompetitionWrapper findRow()
    {
        final Long id = getListenerParameterAsLong();
        return CollectionUtils.find(getRequestedCompetitionList(), object -> object.getId().equals(id));
    }

    protected void validate()
    {
        if (!CollectionUtils.exists(getRequestedCompetitionList(), EnrRequestedCompetitionWrapper::isFromCurrentRequest)) {
            throw new ApplicationException("Необходимо выбрать хотя бы один конкурс.", true);
        }


        Set<Long> selectedSubjectIds = new HashSet<>();
        selectedSubjectIds.addAll(CollectionUtils.collect(getRequestedCompetitionList(), wrapper -> {
            EduProgramSubject programSubject = wrapper.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            if (programSubject.getSubjectIndex().getProgramKind().isProgramBachelorDegree() || programSubject.getSubjectIndex().getProgramKind().isProgramSpecialistDegree())
                return programSubject.getId();
            return null;
        }));
        selectedSubjectIds.remove(null);
        if (selectedSubjectIds.size() > 3) {
            throw new ApplicationException("Невозможно добавить выбранные конкурсы: абитуриент может подать заявления не более чем на три направления (специальности) по совокупности всех выбранных образовательных программ бакалавриата и специалитета.");
        }


        for (EnrRequestedCompetitionWrapper wrapper : getRequestedCompetitionList()) {
            wrapper.validate(getUserContext().getErrorCollector());
        }
    }

    protected boolean saveOrUpdateEntrantRequest()
    {
        EnrEntrantRequest entrantRequest = getEntrantRequest();
        if (isAppendForm()) {
            EnrEntrantRequestManager.instance().dao().doAppendDirections(entrantRequest, getRequestedCompetitionList());
        } else {
            EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getEntrantRequest().getType(), getEntrant().getEnrollmentCampaign());
            if (!entrantRequest.isFiledByTrustee())
                entrantRequest.setTrusteeDetails(null);

            if (entrantRequest.getBenefitCategory() != null && getRequestBenefitProofDocs().isEmpty())
            {
                ContextLocal.getErrorCollector().add("Выберите документы, подтверждающие особое право.", "requestBenefitProofDocs");
                return true;
            }

            if (!isEduDocumentLevelSredneeObsthee()) {
                entrantRequest.setSpecialSchool(false);
                entrantRequest.setEveningSchool(false);
            }
            IPrincipalContext userContext = UserContext.getInstance().getPrincipalContext();
            entrantRequest.setAcceptPrincipalContext(userContext);
            entrantRequest.setAcceptRequest(userContext.getLoginedTitle());

            entrantRequest = EnrEntrantRequestManager.instance().dao().doCreateEntrantRequest(getEntrant(), entrantRequest, getRequestBenefitProofDocs(), getRequestedCompetitionList());
        }
        setEntrantRequest(entrantRequest);

        return false;
    }

    /* видимость и параметры фильтрации конкурсов по ЕГЭ (только для БС) */
    private void prepareStateExamFilter()
    {
        this.setFilteredStateExamCompetitionIds(null);
        this.setStateExamRestriction(null);

        EnrRequestType type = getEntrantRequest().getType();
        if (null == type) { return; }

        if (!EnrRequestTypeCodes.BS.equals(type.getCode())) { return; }

        if (null == getStateExamRestriction()) {
            // если значения нет, то заполняем из настройки (если оно null то значение так и не появится)
            this.setStateExamRestriction(getEntrant().getEnrollmentCampaign().getSettings().getStateExamRestriction());
        }

        if (null != this.getStateExamRestriction()) {
            // если выбирать нужно, то загружаем фильтр конкурсов
            setFilteredStateExamCompetitionIds(EnrCompetitionManager.instance().stateExamFilterDao().getFilteredBSCompetitions(getEntrant()));
        }

    }

    /* видимость поля "Огр. на ур. образ." зависит от вида заявления */
    private void prepareEduLevelRequirement()
    {
        final String reqTypeCode = getEntrantRequest().getType() != null ? getEntrantRequest().getType().getCode() : null;
        setEduLevelRequirementVisible(EnrRequestTypeCodes.BS.equals(reqTypeCode) || EnrRequestTypeCodes.SPO.equals(reqTypeCode));
        if (!isEduLevelRequirementVisible()) {
            setEduLevelRequirementList(null);
        }
    }

    /* видимость колонки "Огр. на ур. образ." зависит от вида заявления */
    private void preparePossibleDSColumns()
    {
        getPossibleCompetitionDS().clearColumns();
        getPossibleCompetitionDS().addColumn(new CheckboxColumn("select", ""));
        //getPossibleCompetitionDS().addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrCompetition.programSetOrgUnit().programSet().programSubject().titleWithCode().s()).setClickable(false).setOrderable(false));
        getPossibleCompetitionDS().addColumn(new SimpleColumn("Набор ОП для приема", EnrCompetition.programSetOrgUnit().programSet().title().s()).setClickable(false).setOrderable(false));
        getPossibleCompetitionDS().addColumn(new SimpleColumn("Филиал", EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()).setClickable(false).setOrderable(false));
        getPossibleCompetitionDS().addColumn(new SimpleColumn("Форм. подр.", EnrCompetition.programSetOrgUnit().formativeOrgUnit().shortTitleWithTopEmphasized().s()).setClickable(false).setOrderable(false));
        getPossibleCompetitionDS().addColumn(new SimpleColumn("Форма обучения", EnrCompetition.programSetOrgUnit().programSet().programForm().title().s()).setClickable(false).setOrderable(false));
        getPossibleCompetitionDS().addColumn(new SimpleColumn("На базе", EnrCompetition.eduLevelRequirement().shortTitle().s()).setClickable(false).setOrderable(false));
        getPossibleCompetitionDS().addColumn(new SimpleColumn("Вид приема", EnrCompetition.type().shortTitle().s()).setClickable(false).setOrderable(false));
    }

    /* список выбранных по всем заявлениям конкурсов зависит от вида заявления */
    private void prepareRequestedCompetitionList()
    {
        setRequestedCompetitionList(new ArrayList<>());
        List<EnrRequestedCompetition> requestedCompetitions = IUniBaseDao.instance.get().getList(
            new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "rc")
            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(getEntrant())))
            .where(eq(property("rc", EnrRequestedCompetition.request().type()), value(getEntrantRequest().getType())))
            .where(eq(property("rc", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
            .order(property("rc", EnrRequestedCompetition.priority().s())));

        for (EnrRequestedCompetition reqComp : requestedCompetitions) {
            getRequestedCompetitionList().add(new EnrRequestedCompetitionWrapper(reqComp, getEntrantRequest()));
        }
    }

    protected void prepareFormData()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));

        getSupport().getSession().refresh(getEntrant().getEnrollmentCampaign().getSettings());

        setRegNumberVisible(getEntrant().getEnrollmentCampaign().getSettings().isNumberOnRequestManual());

        // поле даты добавления отсутствует на форме добавления направлений в заявление -
        // поэтому берем сразу ключ с карточки абитуриента, поскольку поле будет только на форме добавления заявления,
        // а она вызывается оттуда
        setRegDateDisabled(!CoreServices.securityService().check(getEntrant(), getUserContext().getPrincipalContext(), "editRequestRegDate_enrEntrantPubRequestTab"));

        setEnrollmentCommissionRequired(!EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));

        if (isAppendForm()) {
            setEntrantRequest(IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, getEntrantRequest().getId()));
        }
        else {
            if (!isInitialized()) {
                // В дате добавления заявления обрезаем миллисекунды, т.к. из-за них возникают проблемы с ФИСом
                // И, например, на форме редактирования они просто обнуляются автоматом datePicker'ом, даже дату/время никто не менял.
                // см. DEV-8116
                getEntrantRequest().setRegDate(DateUtils.truncate(new Date(), Calendar.SECOND));
                getEntrantRequest().setIdentityCard(getEntrant().getPerson().getIdentityCard());
                getEntrantRequest().setEnrollmentCommission(EnrEnrollmentCommissionManager.instance().dao().getDefaultCommission(isEnrollmentCommissionRequired()));
                if(!isInWizard())
                {
                    getEntrantRequest().setType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getEntrant().getEnrollmentCampaign()));
                }
            }
        }



        setRequestBenefitProofDocs(new ArrayList<>());
        StaticListDataSource<EnrCompetition> possibleDS = new StaticListDataSource<>();
        setPossibleCompetitionDS(possibleDS);

        possibleDS.setupRows(Collections.<EnrCompetition>emptyList());

        prepareEduLevelRequirement();
        prepareStateExamFilter();
        preparePossibleDSColumns();
        prepareRequestedCompetitionList();
        onRefreshSelectionParams();
    }


    public boolean isForeignIdentityCard()
    {
        return getEntrantRequest().getIdentityCard() != null && getEntrantRequest().getIdentityCard().getCitizenship().getCode() != 0;
    }

    public void onChangeEduDoc()
    {
        getEntrantRequest().setGeneralEduSchool(getEntrantRequest().getEduDocument() != null && getEntrantRequest().getEduDocument().getEduLevel().getHierarhyParent().getCode().equals(EduLevelCodes.OBTSHEE_OBRAZOVANIE));
        onRefreshPossibleDS();
    }

    public void onChangeEnrollmentCommission()
    {
        EnrEnrollmentCommissionManager.instance().dao().saveDefaultCommission(getEntrantRequest().getEnrollmentCommission());
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public List<EnrCompetitionType> getCompetitionTypeList()
    {
        return competitionTypeList;
    }

    public void setCompetitionTypeList(List<EnrCompetitionType> competitionTypeList)
    {
        this.competitionTypeList = competitionTypeList;
    }

    public boolean isRegDateVisible()
    {
        return regDateVisible;
    }

    public void setRegDateVisible(boolean regDateVisible)
    {
        this.regDateVisible = regDateVisible;
    }

    public boolean isRegNumberVisible()
    {
        return regNumberVisible;
    }

    public void setRegNumberVisible(boolean regNumberVisible)
    {
        this.regNumberVisible = regNumberVisible;
    }

    public boolean isRegDateDisabled()
    {
        return regDateDisabled;
    }

    public void setRegDateDisabled(boolean regDateDisabled)
    {
        this.regDateDisabled = regDateDisabled;
    }

    public EnrEntrantRequest getEntrantRequest()
    {
        return entrantRequest;
    }

    public void setEntrantRequest(EnrEntrantRequest entrantRequest)
    {
        this.entrantRequest = entrantRequest;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    public void setInitialized(boolean initialized)
    {
        this.initialized = initialized;
    }

    public Integer getEditedRowIndex()
    {
        return editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        this.editedRowIndex = editedRowIndex;
    }

    public EnrRequestedCompetitionWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(EnrRequestedCompetitionWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public EnrRequestedCompetitionWrapper getEditedRow()
    {
        return editedRow;
    }

    public void setEditedRow(EnrRequestedCompetitionWrapper editedRow)
    {
        this.editedRow = editedRow;
    }

    public List<EnrEduLevelRequirement> getEduLevelRequirementList()
    {
        return eduLevelRequirementList;
    }

    public void setEduLevelRequirementList(List<EnrEduLevelRequirement> eduLevelRequirementList)
    {
        this.eduLevelRequirementList = eduLevelRequirementList;
    }

    public EduProgramForm getForm()
    {
        return form;
    }

    public void setForm(EduProgramForm form)
    {
        this.form = form;
    }

    public EnrOrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public OrgUnit getFormativeOrgUnit(){ return formativeOrgUnit; }
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit){ this.formativeOrgUnit = formativeOrgUnit; }

    public StaticListDataSource<EnrCompetition> getPossibleCompetitionDS()
    {
        return possibleCompetitionDS;
    }

    public void setPossibleCompetitionDS(StaticListDataSource<EnrCompetition> possibleCompetitionDS)
    {
        this.possibleCompetitionDS = possibleCompetitionDS;
    }

    public List<EnrRequestedCompetitionWrapper> getRequestedCompetitionList()
    {
        return requestedCompetitionList;
    }

    public void setRequestedCompetitionList(List<EnrRequestedCompetitionWrapper> requestedCompetitionList)
    {
        this.requestedCompetitionList = requestedCompetitionList;
    }

    public List<EduProgramSubject> getSubjectList()
    {
        return subjectList;
    }

    public void setSubjectList(List<EduProgramSubject> subjectList)
    {
        this.subjectList = subjectList;
    }

    public boolean isTooManyPossibleCompetitions()
    {
        return tooManyPossibleCompetitions;
    }

    public void setTooManyPossibleCompetitions(boolean tooManyPossibleCompetitions)
    {
        this.tooManyPossibleCompetitions = tooManyPossibleCompetitions;
    }

    public Boolean getStateExamRestriction()
    {
        return this.stateExamRestriction;
    }

    public void setStateExamRestriction(Boolean stateExamRestriction)
    {
        this.stateExamRestriction = stateExamRestriction;
    }

    public boolean isFilterStateExamResultVisible()
    {
        return null != getStateExamRestriction();
    }

    public Set<Long> getFilteredStateExamCompetitionIds()
    {
        return this.filteredStateExamCompetitionIds;
    }

    public void setFilteredStateExamCompetitionIds(Set<Long> filteredStateExamCompetitionIds)
    {
        this.filteredStateExamCompetitionIds = filteredStateExamCompetitionIds;
    }


    public boolean isEduLevelRequirementVisible(){ return this.eduLevelRequirementVisible; }
    public void setEduLevelRequirementVisible(boolean eduLevelRequirementVisible){ this.eduLevelRequirementVisible = eduLevelRequirementVisible; }

    public List<IEnrEntrantBenefitProofDocument> getRequestBenefitProofDocs(){ return requestBenefitProofDocs; }
    public void setRequestBenefitProofDocs(List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs){ this.requestBenefitProofDocs = requestBenefitProofDocs; }


    public boolean isEnrollmentCommissionRequired()
    {
        return enrollmentCommissionRequired;
    }

    public void setEnrollmentCommissionRequired(boolean enrollmentCommissionRequired)
    {
        this.enrollmentCommissionRequired = enrollmentCommissionRequired;
    }

    public boolean isOnlyCrimea()
    {
        return onlyCrimea;
    }

    public void setOnlyCrimea(boolean onlyCrimea)
    {
        this.onlyCrimea = onlyCrimea;
    }
}
