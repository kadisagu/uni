/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrChosenEntranceExamNumberFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EdiSpecialExamConditions.EnrEntrantEdiSpecialExamConditions;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExams;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.IncludeEntrant.EnrExamGroupIncludeEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.AppealAddEdit.EnrExamPassDisciplineAppealAddEdit;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.AppealAddEdit.EnrExamPassDisciplineAppealAddEditUI;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.MarkEdit.EnrExamPassDisciplineMarkEdit;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.RetakeAdd.EnrExamPassDisciplineRetakeAdd;
import ru.tandemservice.unienr14.rating.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit.EnrEnrollmentCampaignAddEdit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/5/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required=true),
    @Bind(key = EnrEntrantPubExamTabUI.PARAM_INLINE, binding = "inline")
})
public class EnrEntrantPubExamTabUI extends UIPresenter
{
    public static final String PARAM_INLINE = "inline";

    private EnrEntrant entrant = new EnrEntrant();

    private List<CompetitionRowWrapper> rowList;
    private CompetitionRowWrapper currentRow;

    private boolean inline;
    public boolean isInline() { return this.inline; }
    public void setInline(boolean inline) { this.inline = inline; }

    private StaticListDataSource<EnrExamPassDiscipline> examListDS;
    private StaticListDataSource<EnrExamPassAppeal> appealDS;

    private boolean entranceExamIncorrect;
    private boolean examListIncorrect;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setEntranceExamIncorrect(EnrEntrantManager.instance().dao().isEntranceExamIncorrect(getEntrant()));
        setExamListIncorrect(EnrEntrantManager.instance().dao().isExamListIncorrect(getEntrant()));
        prepareChosenEntranceExamList();
        prepareExamList();
        prepareAppealList();
    }

    public void onClickEditSpecialExamConditions()
    {
        getActivationBuilder().asRegionDialog(EnrEntrantEdiSpecialExamConditions.class).parameter(IUIPresenter.PUBLISHER_ID, getEntrant().getId()).activate();
    }

    public void onClickChooseEntranceExams()
    {
        _uiActivation.asRegion(EnrEntrantEditChosenExams.class).top()
        .parameter("entrantId", getEntrant().getId())
        .activate();
    }

    public void onClickDistributeExamList()
    {
        EnrExamGroupManager.instance().distributionDao().doDistributeEntrantExamPassDisciplines(getEntrant());

        List<EnrExamPassDiscipline> list = new DQLSelectBuilder()
        .fromEntity(EnrExamPassDiscipline.class, "x")
        .where(isNull(property(EnrExamPassDiscipline.examGroup().fromAlias("x"))))
        .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("x")), value(getEntrant())))
        .order(property(EnrExamPassDiscipline.discipline().discipline().title().fromAlias("x")))
        .order(property(EnrExamPassDiscipline.passForm().title().fromAlias("x")))
        .createStatement(getSupport().getSession()).list();

        if (!list.isEmpty()) {
            //  "Для ВИ (названия ВИ) нет подходящих вариантов в расписании.". Названия ВИ = дисциплина (форма сдачи), если их несколько, то через запятую их выводим.
            StringBuilder sb = new StringBuilder();
            for (EnrExamPassDiscipline d: list) {
                if (sb.length() > 0) { sb.append(", "); }
                sb.append(d.getTitle());
            }
            ContextLocal.getInfoCollector().add("Для ВИ " + sb.toString() + " нет подходящих вариантов в расписании.");
        }

    }

    public void onClickRefreshExamList() {
        EnrExamPassDisciplineManager.instance().dao().doRefreshExamList(getEntrant());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickMark() {
        _uiActivation
        .asRegionDialog(EnrExamPassDisciplineMarkEdit.class)
        .parameter(PUBLISHER_ID, getListenerParameterAsLong())
        .activate();

        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickExamListRowDelete() {
        EnrExamPassDisciplineManager.instance().dao().deleteExamPassDiscipline(getListenerParameterAsLong());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickPrintEnrollmentExamSheet()
    {
        EnrScriptItem scriptItem = null;
        if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_INTERNAL_EXAMS.equals(entrant.getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET);

        }
        else if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_ALL_EXAMS.equals(entrant.getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET_ALL_EXAMS);

        }
        else
        {
            throw new UnsupportedOperationException();
        }

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, getEntrant().getId()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, getEntrant().getId());
    }

    public void onClickSetExamGroup() {
        _uiActivation
        .asRegionDialog(EnrExamGroupIncludeEntrant.class)
        .parameter(PUBLISHER_ID, getListenerParameterAsLong())
        .activate();

        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickAddRetake() {
        _uiActivation
        .asRegionDialog(EnrExamPassDisciplineRetakeAdd.class)
        .parameter(PUBLISHER_ID, getEntrant().getId())
        .activate();

        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickAddAppeal() {
        _uiActivation
        .asRegionDialog(EnrExamPassDisciplineAppealAddEdit.class)
        .parameter(EnrExamPassDisciplineAppealAddEditUI.BIND_DSICIPLINE_ID, getListenerParameterAsLong())
        .activate();

        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickEditAppeal() {
        _uiActivation
        .asRegionDialog(EnrExamPassDisciplineAppealAddEdit.class)
        .parameter(PUBLISHER_ID, getListenerParameterAsLong())
        .activate();

        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickDeleteAppeal() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    // presenter

    public boolean isDisplayAnyError() {
        return isEntranceExamIncorrect() || isExamListIncorrect();
    }


    // utils

    private void prepareExamList()
    {
        setExamListDS(new StaticListDataSource<>());
        getExamListDS().setMinCountRow(5);

        getExamListDS().setupRows(IUniBaseDao.instance.get().getList(EnrExamPassDiscipline.class,
            EnrExamPassDiscipline.entrant(), getEntrant(),
            EnrExamPassDiscipline.discipline().discipline().title().s(), EnrExamPassDiscipline.passForm().code().s(), EnrExamPassDiscipline.retake().s()));
        final Set<Long> actualDisciplines = EnrExamPassDisciplineManager.instance().dao().getActualExamPassDisciplineIds(getEntrant());

        AbstractColumn titleColumn = new SimpleColumn("Вступительное испытание", EnrExamPassDiscipline.title()).setClickable(false).setOrderable(false);
        titleColumn.setStyleResolver(rowEntity -> actualDisciplines.contains(rowEntity.getId()) || ((EnrExamPassDiscipline)rowEntity).isRetake() ? null : ("color: " + UniDefines.COLOR_CELL_ERROR));
        getExamListDS().addColumn(titleColumn);

        getExamListDS().addColumn(new SimpleColumn("Филиал", EnrExamPassDiscipline.territorialOrgUnit().shortTitleWithTopEmphasized().s()).setClickable(false).setOrderable(false));
        getExamListDS().addColumn(new PublisherLinkColumn("Экзам. группа", "title", EnrExamPassDiscipline.examGroup()).setOrderable(false));
        getExamListDS().addColumn(new SimpleColumn("Расписание для ВИ", EnrExamPassDiscipline.examScheduleTitle().s(), NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
        getExamListDS().addColumn(new SimpleColumn("Оценка", EnrExamPassDiscipline.examResult().s()).setClickable(false).setOrderable(false));

        if (getEntrant().isAccessible()) {
            getExamListDS().addColumn(new ActionColumn("Включить ВИ в ЭГ", "clone", "onClickSetExamGroup")
            .setPermissionKey("enr14EntrantPubChosenExamTabSetExamGroup"));
            getExamListDS().addColumn(new ActionColumn("Выставить оценку", "edit_mark", "onClickMark")
            .setPermissionKey("enr14EntrantPubChosenExamTabEditMark"));
            getExamListDS().addColumn(new ActionColumn("Добавить апелляцию", "add", "onClickAddAppeal")
            .setPermissionKey("enr14EntrantPubChosenExamTabAddAppeal"));
            getExamListDS().addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickExamListRowDelete", "Удалить дисциплину для сдачи «{0}» ({1})?", EnrExamPassDiscipline.discipline().title().s(), EnrExamPassDiscipline.passForm().title().s())
            .setPermissionKey("enr14EntrantPubChosenExamTabDeleteExam"));
        }
    }

    private void prepareAppealList()
    {
        setAppealDS(new StaticListDataSource<>());

        getAppealDS().setupRows(IUniBaseDao.instance.get().getList(EnrExamPassAppeal.class,
            EnrExamPassAppeal.examPassDiscipline().entrant(), getEntrant(),
            EnrExamPassAppeal.examPassDiscipline().discipline().discipline().title().s(), EnrExamPassAppeal.examPassDiscipline().passForm().code().s()));

        getAppealDS().setMinCountRow(getAppealDS().getRowList().size() + 1);

        AbstractColumn titleColumn = new SimpleColumn("Вступительное испытание", EnrExamPassAppeal.examPassDiscipline().title()).setClickable(false).setOrderable(false);
        getAppealDS().addColumn(titleColumn);

        getAppealDS().addColumn(new SimpleColumn("Балл", EnrExamPassAppeal.markAsString().s()).setClickable(false).setOrderable(false));
        getAppealDS().addColumn(new SimpleColumn("Дата подачи", EnrExamPassAppeal.appealDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        getAppealDS().addColumn(new SimpleColumn("Дата решения", EnrExamPassAppeal.decisionDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        getAppealDS().addColumn(new SimpleColumn("Решение", EnrExamPassAppeal.decisionText().s(), StringLimitFormatter.NON_BROKEN_WORDS_WITH_NEW_LINE_SUPPORT).setClickable(false).setOrderable(false));

        if (getEntrant().isAccessible()) {
            getAppealDS().addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditAppeal")
            .setPermissionKey("enr14EntrantPubChosenExamTabEditAppeal"));
            getAppealDS().addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteAppeal", "Удалить апелляцию по дисциплине для сдачи «{0}» ({1})?", EnrExamPassAppeal.examPassDiscipline().discipline().title().s(), EnrExamPassAppeal.examPassDiscipline().passForm().title().s())
            .setPermissionKey("enr14EntrantPubChosenExamTabDeleteAppeal"));
        }
    }

    private void prepareChosenEntranceExamList()
    {
        setRowList(new ArrayList<>());

        Map<EnrRequestedCompetition, Map<EnrChosenEntranceExam, List<EnrChosenEntranceExamForm>>> map = new HashMap<>();
        for (EnrRequestedCompetition rc : IUniBaseDao.instance.get().getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant(), getEntrant())) {
            if (rc.getRequest().isTakeAwayDocument()) continue;
            map.put(rc, null);
        }
        for (EnrChosenEntranceExam exam : IUniBaseDao.instance.get().getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().request().entrant(), getEntrant())) {
            if (exam.getRequestedCompetition().getRequest().isTakeAwayDocument()) continue;
            SafeMap.safeGet(map, exam.getRequestedCompetition(), HashMap.class).put(exam, new ArrayList<>());
        }
        for (EnrChosenEntranceExamForm examForm : IUniBaseDao.instance.get().getList(EnrChosenEntranceExamForm.class, EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant(), getEntrant())) {
            if (examForm.getChosenEntranceExam().getRequestedCompetition().getRequest().isTakeAwayDocument()) continue;
            map.get(examForm.getChosenEntranceExam().getRequestedCompetition()).get(examForm.getChosenEntranceExam()).add(examForm);
        }

        Map<EnrChosenEntranceExam, EnrEntrantMarkSourceBenefit> olympiadMap = new HashMap<>();
        for (EnrEntrantMarkSourceBenefit olympiad : IUniBaseDao.instance.get().getList(EnrEntrantMarkSourceBenefit.class, EnrEntrantMarkSourceBenefit.entrant(), getEntrant())) {
            olympiadMap.put(olympiad.getChosenEntranceExamForm().getChosenEntranceExam(), olympiad);
        }

        Map<EnrRequestedCompetition, EnrRatingItem> ratingMap = new HashMap<>();
        for (EnrRatingItem rating : IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.entrant(), getEntrant())) {
            ratingMap.put(rating.getRequestedCompetition(), rating);
        }

        for (Map.Entry<EnrRequestedCompetition, Map<EnrChosenEntranceExam, List<EnrChosenEntranceExamForm>>> e: map.entrySet()) {
            CompetitionRowWrapper wrapper = new CompetitionRowWrapper(e.getKey());
            getRowList().add(wrapper);

            EnrRatingItem rating = ratingMap.get(e.getKey());
            if (rating != null && !rating.isStatusEntranceExamsCorrect()) {
                wrapper.choiceUnactual = true;
            }

            if (e.getValue() == null)
                continue;

            StringBuilder examSetTitle = new StringBuilder();
            List<EnrChosenEntranceExam> examList = new ArrayList<>(e.getValue().keySet());
            Collections.sort(examList, EnrChosenEntranceExamNumberFormatter.COMPARATOR);
            EnrChosenEntranceExamNumberFormatter numberFormatter = new EnrChosenEntranceExamNumberFormatter(examList);
            for (EnrChosenEntranceExam exam : examList) {
                if (examSetTitle.length() != 0) examSetTitle.append("\n");
                examSetTitle.append(numberFormatter.format(exam));
                examSetTitle.append(exam.getDiscipline().getTitle());

                EnrEntrantMarkSourceBenefit olympiad = olympiadMap.get(exam);
                if (null != olympiad) {
                    examSetTitle.append(" (").append(olympiad.getMainProof().getExtendedTitle()).append(")");
                    continue;
                }

                List<EnrChosenEntranceExamForm> forms = new ArrayList<>(e.getValue().get(exam));
                if (!forms.isEmpty()) {
                    examSetTitle.append(" (");
                    Collections.sort(forms, new EntityComparator<>(new EntityOrder(EnrChosenEntranceExamForm.passForm().code())));
                    examSetTitle.append(UniStringUtils.join(forms, EnrChosenEntranceExamForm.passForm().title().s(), ", "));
                    examSetTitle.append(")");
                }
            }
            String examsTitle = NewLineFormatter.NOBR_IN_LINES.format(examSetTitle.toString());
            // todo check
            if (false) {
                examsTitle = "<div style=\"color: " + UniDefines.COLOR_CELL_ERROR + ";\">" + examsTitle + "</div>";
            }
            wrapper.setExamsTitle(examsTitle);
        }

        Collections.sort(getRowList(), (o1, o2) -> o1.getCompetition().getPriority() - o2.getCompetition().getPriority());
    }

    // inner classes

    @SuppressWarnings({"serial", "unused"})
    private static class CompetitionRowWrapper extends IdentifiableWrapper<EnrRequestedCompetition> {

        private String examsTitle = "";
        private EnrRequestedCompetition competition;
        private boolean choiceUnactual;

        private CompetitionRowWrapper(EnrRequestedCompetition i) throws ClassCastException {
            super(i);
            competition = i;
        }

        public String getExamsTitle() { return examsTitle; }
        public void setExamsTitle(String examsTitle) { this.examsTitle = examsTitle; }
        public String getRowTitle() { return getCompetition().getTitle(); }
        public String getRequestTitle() { return getCompetition().getRequest().getStringNumber(); }
        public EnrRequestedCompetition getCompetition() { return competition; }
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public List<CompetitionRowWrapper> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<CompetitionRowWrapper> rowList)
    {
        this.rowList = rowList;
    }

    public CompetitionRowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(CompetitionRowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public StaticListDataSource<EnrExamPassDiscipline> getExamListDS()
    {
        return examListDS;
    }

    public void setExamListDS(StaticListDataSource<EnrExamPassDiscipline> examListDS)
    {
        this.examListDS = examListDS;
    }

    public boolean isEntranceExamIncorrect()
    {
        return entranceExamIncorrect;
    }

    public void setEntranceExamIncorrect(boolean entranceExamIncorrect)
    {
        this.entranceExamIncorrect = entranceExamIncorrect;
    }

    public boolean isExamListIncorrect()
    {
        return examListIncorrect;
    }

    public void setExamListIncorrect(boolean examListIncorrect)
    {
        this.examListIncorrect = examListIncorrect;
    }

    public StaticListDataSource<EnrExamPassAppeal> getAppealDS()
    {
        return appealDS;
    }

    public void setAppealDS(StaticListDataSource<EnrExamPassAppeal> appealDS)
    {
        this.appealDS = appealDS;
    }
}
