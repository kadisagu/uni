/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.HorizontalSummaryEnrollmentResultsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.HorizontalSummaryEnrollmentResultsAdd.EnrReportHorizontalSummaryEnrollmentResultsAddUI;

/**
 * @author rsizonenko
 * @since 02.09.2014
 */
public interface IEnrReportHorizontalSummaryEnrollmentResultsDao extends INeedPersistenceSupport {
    long createReport(EnrReportHorizontalSummaryEnrollmentResultsAddUI model);
}
