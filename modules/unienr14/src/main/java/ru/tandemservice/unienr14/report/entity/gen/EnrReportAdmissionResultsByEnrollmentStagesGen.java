package ru.tandemservice.unienr14.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Результаты приема по этапам зачисления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrReportAdmissionResultsByEnrollmentStagesGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages";
    public static final String ENTITY_NAME = "enrReportAdmissionResultsByEnrollmentStages";
    public static final int VERSION_HASH = -824892381;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_PROGRAM_SET = "programSet";
    public static final String P_PARALLEL = "parallel";
    public static final String P_FIRST_STAGE_DATE_FROM = "firstStageDateFrom";
    public static final String P_FIRST_STAGE_DATE_TO = "firstStageDateTo";
    public static final String P_SECOND_STAGE_DATE_FROM = "secondStageDateFrom";
    public static final String P_SECOND_STAGE_DATE_TO = "secondStageDateTo";
    public static final String P_EXT_STAGE_DATE_FROM = "extStageDateFrom";
    public static final String P_EXT_STAGE_DATE_TO = "extStageDateTo";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _requestType;     // Вид заявления
    private String _compensationType;     // Вид возмещения затрат
    private String _programForm;     // Форма обучения
    private String _enrOrgUnit;     // Филиал
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _programSubject;     // Направление, спец., профессия
    private String _eduProgram;     // Образовательная программа
    private String _programSet;     // Набор образовательных программ
    private String _parallel;     // Поступающие параллельно
    private Date _firstStageDateFrom;     // Первый этап зачисления с
    private Date _firstStageDateTo;     // Первый этап зачисления по
    private Date _secondStageDateFrom;     // Второй этап зачисления с
    private Date _secondStageDateTo;     // Второй этап зачисления по
    private Date _extStageDateFrom;     // Дополнительный прием с
    private Date _extStageDateTo;     // Дополнительный прием по

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид заявления.
     */
    @Length(max=255)
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма обучения.
     */
    @Length(max=255)
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Филиал.
     */
    public String getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал.
     */
    public void setEnrOrgUnit(String enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, спец., профессия.
     */
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, спец., профессия.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Образовательная программа.
     */
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Набор образовательных программ.
     */
    public String getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор образовательных программ.
     */
    public void setProgramSet(String programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Поступающие параллельно.
     */
    public String getParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Поступающие параллельно.
     */
    public void setParallel(String parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    /**
     * @return Первый этап зачисления с. Свойство не может быть null.
     */
    @NotNull
    public Date getFirstStageDateFrom()
    {
        return _firstStageDateFrom;
    }

    /**
     * @param firstStageDateFrom Первый этап зачисления с. Свойство не может быть null.
     */
    public void setFirstStageDateFrom(Date firstStageDateFrom)
    {
        dirty(_firstStageDateFrom, firstStageDateFrom);
        _firstStageDateFrom = firstStageDateFrom;
    }

    /**
     * @return Первый этап зачисления по. Свойство не может быть null.
     */
    @NotNull
    public Date getFirstStageDateTo()
    {
        return _firstStageDateTo;
    }

    /**
     * @param firstStageDateTo Первый этап зачисления по. Свойство не может быть null.
     */
    public void setFirstStageDateTo(Date firstStageDateTo)
    {
        dirty(_firstStageDateTo, firstStageDateTo);
        _firstStageDateTo = firstStageDateTo;
    }

    /**
     * @return Второй этап зачисления с. Свойство не может быть null.
     */
    @NotNull
    public Date getSecondStageDateFrom()
    {
        return _secondStageDateFrom;
    }

    /**
     * @param secondStageDateFrom Второй этап зачисления с. Свойство не может быть null.
     */
    public void setSecondStageDateFrom(Date secondStageDateFrom)
    {
        dirty(_secondStageDateFrom, secondStageDateFrom);
        _secondStageDateFrom = secondStageDateFrom;
    }

    /**
     * @return Второй этап зачисления по. Свойство не может быть null.
     */
    @NotNull
    public Date getSecondStageDateTo()
    {
        return _secondStageDateTo;
    }

    /**
     * @param secondStageDateTo Второй этап зачисления по. Свойство не может быть null.
     */
    public void setSecondStageDateTo(Date secondStageDateTo)
    {
        dirty(_secondStageDateTo, secondStageDateTo);
        _secondStageDateTo = secondStageDateTo;
    }

    /**
     * @return Дополнительный прием с. Свойство не может быть null.
     */
    @NotNull
    public Date getExtStageDateFrom()
    {
        return _extStageDateFrom;
    }

    /**
     * @param extStageDateFrom Дополнительный прием с. Свойство не может быть null.
     */
    public void setExtStageDateFrom(Date extStageDateFrom)
    {
        dirty(_extStageDateFrom, extStageDateFrom);
        _extStageDateFrom = extStageDateFrom;
    }

    /**
     * @return Дополнительный прием по. Свойство не может быть null.
     */
    @NotNull
    public Date getExtStageDateTo()
    {
        return _extStageDateTo;
    }

    /**
     * @param extStageDateTo Дополнительный прием по. Свойство не может быть null.
     */
    public void setExtStageDateTo(Date extStageDateTo)
    {
        dirty(_extStageDateTo, extStageDateTo);
        _extStageDateTo = extStageDateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrReportAdmissionResultsByEnrollmentStagesGen)
        {
            setEnrollmentCampaign(((EnrReportAdmissionResultsByEnrollmentStages)another).getEnrollmentCampaign());
            setDateFrom(((EnrReportAdmissionResultsByEnrollmentStages)another).getDateFrom());
            setDateTo(((EnrReportAdmissionResultsByEnrollmentStages)another).getDateTo());
            setRequestType(((EnrReportAdmissionResultsByEnrollmentStages)another).getRequestType());
            setCompensationType(((EnrReportAdmissionResultsByEnrollmentStages)another).getCompensationType());
            setProgramForm(((EnrReportAdmissionResultsByEnrollmentStages)another).getProgramForm());
            setEnrOrgUnit(((EnrReportAdmissionResultsByEnrollmentStages)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrReportAdmissionResultsByEnrollmentStages)another).getFormativeOrgUnit());
            setProgramSubject(((EnrReportAdmissionResultsByEnrollmentStages)another).getProgramSubject());
            setEduProgram(((EnrReportAdmissionResultsByEnrollmentStages)another).getEduProgram());
            setProgramSet(((EnrReportAdmissionResultsByEnrollmentStages)another).getProgramSet());
            setParallel(((EnrReportAdmissionResultsByEnrollmentStages)another).getParallel());
            setFirstStageDateFrom(((EnrReportAdmissionResultsByEnrollmentStages)another).getFirstStageDateFrom());
            setFirstStageDateTo(((EnrReportAdmissionResultsByEnrollmentStages)another).getFirstStageDateTo());
            setSecondStageDateFrom(((EnrReportAdmissionResultsByEnrollmentStages)another).getSecondStageDateFrom());
            setSecondStageDateTo(((EnrReportAdmissionResultsByEnrollmentStages)another).getSecondStageDateTo());
            setExtStageDateFrom(((EnrReportAdmissionResultsByEnrollmentStages)another).getExtStageDateFrom());
            setExtStageDateTo(((EnrReportAdmissionResultsByEnrollmentStages)another).getExtStageDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrReportAdmissionResultsByEnrollmentStagesGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrReportAdmissionResultsByEnrollmentStages.class;
        }

        public T newInstance()
        {
            return (T) new EnrReportAdmissionResultsByEnrollmentStages();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "requestType":
                    return obj.getRequestType();
                case "compensationType":
                    return obj.getCompensationType();
                case "programForm":
                    return obj.getProgramForm();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "eduProgram":
                    return obj.getEduProgram();
                case "programSet":
                    return obj.getProgramSet();
                case "parallel":
                    return obj.getParallel();
                case "firstStageDateFrom":
                    return obj.getFirstStageDateFrom();
                case "firstStageDateTo":
                    return obj.getFirstStageDateTo();
                case "secondStageDateFrom":
                    return obj.getSecondStageDateFrom();
                case "secondStageDateTo":
                    return obj.getSecondStageDateTo();
                case "extStageDateFrom":
                    return obj.getExtStageDateFrom();
                case "extStageDateTo":
                    return obj.getExtStageDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "programSet":
                    obj.setProgramSet((String) value);
                    return;
                case "parallel":
                    obj.setParallel((String) value);
                    return;
                case "firstStageDateFrom":
                    obj.setFirstStageDateFrom((Date) value);
                    return;
                case "firstStageDateTo":
                    obj.setFirstStageDateTo((Date) value);
                    return;
                case "secondStageDateFrom":
                    obj.setSecondStageDateFrom((Date) value);
                    return;
                case "secondStageDateTo":
                    obj.setSecondStageDateTo((Date) value);
                    return;
                case "extStageDateFrom":
                    obj.setExtStageDateFrom((Date) value);
                    return;
                case "extStageDateTo":
                    obj.setExtStageDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "requestType":
                        return true;
                case "compensationType":
                        return true;
                case "programForm":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "eduProgram":
                        return true;
                case "programSet":
                        return true;
                case "parallel":
                        return true;
                case "firstStageDateFrom":
                        return true;
                case "firstStageDateTo":
                        return true;
                case "secondStageDateFrom":
                        return true;
                case "secondStageDateTo":
                        return true;
                case "extStageDateFrom":
                        return true;
                case "extStageDateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "requestType":
                    return true;
                case "compensationType":
                    return true;
                case "programForm":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "eduProgram":
                    return true;
                case "programSet":
                    return true;
                case "parallel":
                    return true;
                case "firstStageDateFrom":
                    return true;
                case "firstStageDateTo":
                    return true;
                case "secondStageDateFrom":
                    return true;
                case "secondStageDateTo":
                    return true;
                case "extStageDateFrom":
                    return true;
                case "extStageDateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "requestType":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "programForm":
                    return String.class;
                case "enrOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "programSet":
                    return String.class;
                case "parallel":
                    return String.class;
                case "firstStageDateFrom":
                    return Date.class;
                case "firstStageDateTo":
                    return Date.class;
                case "secondStageDateFrom":
                    return Date.class;
                case "secondStageDateTo":
                    return Date.class;
                case "extStageDateFrom":
                    return Date.class;
                case "extStageDateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrReportAdmissionResultsByEnrollmentStages> _dslPath = new Path<EnrReportAdmissionResultsByEnrollmentStages>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrReportAdmissionResultsByEnrollmentStages");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getEnrOrgUnit()
     */
    public static PropertyPath<String> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getProgramSet()
     */
    public static PropertyPath<String> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getParallel()
     */
    public static PropertyPath<String> parallel()
    {
        return _dslPath.parallel();
    }

    /**
     * @return Первый этап зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getFirstStageDateFrom()
     */
    public static PropertyPath<Date> firstStageDateFrom()
    {
        return _dslPath.firstStageDateFrom();
    }

    /**
     * @return Первый этап зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getFirstStageDateTo()
     */
    public static PropertyPath<Date> firstStageDateTo()
    {
        return _dslPath.firstStageDateTo();
    }

    /**
     * @return Второй этап зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getSecondStageDateFrom()
     */
    public static PropertyPath<Date> secondStageDateFrom()
    {
        return _dslPath.secondStageDateFrom();
    }

    /**
     * @return Второй этап зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getSecondStageDateTo()
     */
    public static PropertyPath<Date> secondStageDateTo()
    {
        return _dslPath.secondStageDateTo();
    }

    /**
     * @return Дополнительный прием с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getExtStageDateFrom()
     */
    public static PropertyPath<Date> extStageDateFrom()
    {
        return _dslPath.extStageDateFrom();
    }

    /**
     * @return Дополнительный прием по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getExtStageDateTo()
     */
    public static PropertyPath<Date> extStageDateTo()
    {
        return _dslPath.extStageDateTo();
    }

    public static class Path<E extends EnrReportAdmissionResultsByEnrollmentStages> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _enrOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _programSet;
        private PropertyPath<String> _parallel;
        private PropertyPath<Date> _firstStageDateFrom;
        private PropertyPath<Date> _firstStageDateTo;
        private PropertyPath<Date> _secondStageDateFrom;
        private PropertyPath<Date> _secondStageDateTo;
        private PropertyPath<Date> _extStageDateFrom;
        private PropertyPath<Date> _extStageDateTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getEnrOrgUnit()
     */
        public PropertyPath<String> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getProgramSet()
     */
        public PropertyPath<String> programSet()
        {
            if(_programSet == null )
                _programSet = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getParallel()
     */
        public PropertyPath<String> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<String>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_PARALLEL, this);
            return _parallel;
        }

    /**
     * @return Первый этап зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getFirstStageDateFrom()
     */
        public PropertyPath<Date> firstStageDateFrom()
        {
            if(_firstStageDateFrom == null )
                _firstStageDateFrom = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_FIRST_STAGE_DATE_FROM, this);
            return _firstStageDateFrom;
        }

    /**
     * @return Первый этап зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getFirstStageDateTo()
     */
        public PropertyPath<Date> firstStageDateTo()
        {
            if(_firstStageDateTo == null )
                _firstStageDateTo = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_FIRST_STAGE_DATE_TO, this);
            return _firstStageDateTo;
        }

    /**
     * @return Второй этап зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getSecondStageDateFrom()
     */
        public PropertyPath<Date> secondStageDateFrom()
        {
            if(_secondStageDateFrom == null )
                _secondStageDateFrom = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_SECOND_STAGE_DATE_FROM, this);
            return _secondStageDateFrom;
        }

    /**
     * @return Второй этап зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getSecondStageDateTo()
     */
        public PropertyPath<Date> secondStageDateTo()
        {
            if(_secondStageDateTo == null )
                _secondStageDateTo = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_SECOND_STAGE_DATE_TO, this);
            return _secondStageDateTo;
        }

    /**
     * @return Дополнительный прием с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getExtStageDateFrom()
     */
        public PropertyPath<Date> extStageDateFrom()
        {
            if(_extStageDateFrom == null )
                _extStageDateFrom = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_EXT_STAGE_DATE_FROM, this);
            return _extStageDateFrom;
        }

    /**
     * @return Дополнительный прием по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages#getExtStageDateTo()
     */
        public PropertyPath<Date> extStageDateTo()
        {
            if(_extStageDateTo == null )
                _extStageDateTo = new PropertyPath<Date>(EnrReportAdmissionResultsByEnrollmentStagesGen.P_EXT_STAGE_DATE_TO, this);
            return _extStageDateTo;
        }

        public Class getEntityClass()
        {
            return EnrReportAdmissionResultsByEnrollmentStages.class;
        }

        public String getEntityName()
        {
            return "enrReportAdmissionResultsByEnrollmentStages";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
