/**
 *$Id: IEnrCampDisciplineGroupDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
public interface IEnrCampDisciplineGroupDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет\обновляет группу дисциплин.
     * Сохраняет\обновляет список дисциплин группы.
     * @param group Группа дисциплин
     * @param disciplineList список дисциплин
     * @return id группы дисциплин
     */
    Long saveOrUpdateDiscGroup(EnrCampaignDisciplineGroup group, List<EnrCampaignDiscipline> disciplineList);
}
