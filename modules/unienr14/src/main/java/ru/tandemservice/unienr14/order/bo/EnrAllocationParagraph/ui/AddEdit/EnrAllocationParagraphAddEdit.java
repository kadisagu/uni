/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.CheckboxDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.RadioDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic.EnrParagraphEntrantSelectionDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 8/8/14
 */
@Configuration
public class EnrAllocationParagraphAddEdit extends BusinessComponentManager
{
    public static final String BIND_EDIT_FORM = "editForm";
    public static final String BIND_ONLY_ALLOCATED = "onlyAllocated";
    public static final String BIND_ONLY_EDU_PROGRAM = "onlyEduProgram";
    public static final String BIND_FORM = "eduForm";
    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String BIND_SUBJECT = "programSubject";
    public static final String BIND_PROGRAM_SET = "programSet";
    public static final String BIND_PARAGRAPH = "paragraph";
    public static final String BIND_PROGRAM_SET_ITEM = "programSetItem";

    public static final String DS_ENTRANT = "entrantDS";
    public static final String DS_FORM = "formDS";
    public static final String DS_ORG_UNIT = "orgUnitDS";
    public static final String DS_FORMATIVE_ORG_UNIT = "formativeOrgUnitDS";
    public static final String DS_SUBJECT = "subjectDS";
    public static final String DS_PROGRAM_SET = "programSetDS";
    public static final String DS_PROGRAM_SET_ITEM = "programSetItemDS";


    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String RADIOBOX_COLUMN = "radiobox";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(DS_ENTRANT, entrantDS(), entrantDSHandler()))
                .addDataSource(selectDS(DS_FORM, eduProgramFormDSHandler()))
                .addDataSource(selectDS(DS_ORG_UNIT, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(DS_FORMATIVE_ORG_UNIT, formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
                .addDataSource(selectDS(DS_SUBJECT, subjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(DS_PROGRAM_SET, programSetDSHandler()))
                .addDataSource(selectDS(DS_PROGRAM_SET_ITEM, programSetItemDSHandler()))
                .create();
    }

    /**
     * Создается точка расширения "Список выбранных конкурсов на форме добавления/редактирования параграфа", единая
     * для всех форм добавления/редактирования параграфов о зачислении
     *
     * @return точка расширения "Список выбранных конкурсов на форме добавления/редактирования параграфа"
     */
    @Bean
    public ColumnListExtPoint entrantDS()
    {
        return ColumnListExtPoint.with(getName(), "entrantDS")
                .addColumn(CheckboxDSColumn.with().name(CHECKBOX_COLUMN).create())
                .addColumn(RadioDSColumn.with().name(RADIOBOX_COLUMN).visible("ui:order.printFormType.selectHeadman").create())
                .addColumn(TextDSColumn.with().name("fio").path(EnrRequestedCompetition.request().entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name(EnrParagraphEntrantSelectionDSHandler.CUSTOM_STATES).path(EnrParagraphEntrantSelectionDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()).create())
                .addColumn(TextDSColumn.with().name("sex").path(EnrRequestedCompetition.request().entrant().person().identityCard().sex().shortTitle()).create())
                .addColumn(TextDSColumn.with().name("passport").path(EnrRequestedCompetition.request().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(publisherColumn("enrollExtract", EnrParagraphEntrantSelectionDSHandler.ENROLL_EXTRACT).formatter(new IFormatter<EnrEnrollmentExtract>()
                {
                    @Override
                    public String format(EnrEnrollmentExtract source)
                    {
                        return source.getShortTitle();
                    }
                }).publisherLinkResolver(new IPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return ((EnrEnrollmentExtract) ((DataWrapper) entity).getProperty(EnrParagraphEntrantSelectionDSHandler.ENROLL_EXTRACT)).getParagraph().getOrder().getId();
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EnrOrderPub.class.getSimpleName();
                    }
                }))
                .addColumn(TextDSColumn.with().name("competition").path(EnrRequestedCompetition.competition().title()).create())
                .addColumn(TextDSColumn.with().name("state").path(EnrRequestedCompetition.state().stateDaemonSafe()).create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> entrantDSHandler()
    {
        return new EnrParagraphEntrantSelectionDSHandler(getName());
    }

    /* Выбор конкурсов */


    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                boolean editForm = context.getBoolean(BIND_EDIT_FORM, false);

                boolean onlyAllocated = editForm ? false : context.getBoolean(BIND_ONLY_ALLOCATED, false);
                boolean onlyEduProgram = editForm ? false : context.getBoolean(BIND_ONLY_EDU_PROGRAM, false);

                EnrAllocationParagraph paragraph = context.get(BIND_PARAGRAPH);

                DQLSelectBuilder allocDQL = new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                        .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r")));

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
                        .where(eq(property(EnrCompetition.type().compensationType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)));

                if (onlyAllocated)
                    compDQL.where(exists(allocDQL.buildQuery()));

                DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
                extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));
                List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());
                extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));
                compDQL.where(notExists(extrDQL.buildQuery()));


                if (onlyEduProgram)
                {
                    DQLSelectBuilder reqProgDQL = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp");
                    reqProgDQL.where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r")));
                    compDQL.where(exists(reqProgDQL.buildQuery()));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EduProgramForm.code())
                .filter(EduProgramForm.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                boolean editForm = context.getBoolean(BIND_EDIT_FORM, false);

                boolean onlyAllocated = editForm ? false : context.getBoolean(BIND_ONLY_ALLOCATED, false);
                boolean onlyEduProgram = editForm ? false : context.getBoolean(BIND_ONLY_EDU_PROGRAM, false);

                EnrAllocationParagraph paragraph = context.get(BIND_PARAGRAPH);
                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
                        .where(eq(property(EnrCompetition.type().compensationType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().orgUnit().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)));

                if (onlyAllocated)
                {
                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                            .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r"))).buildQuery()));
                }
                DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
                extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));
                List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());
                extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));
                compDQL.where(notExists(extrDQL.buildQuery()));


                if (onlyEduProgram)
                {

                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                            .where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r"))).buildQuery()));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                boolean editForm = context.getBoolean(BIND_EDIT_FORM, false);

                boolean onlyAllocated = editForm ? false : context.getBoolean(BIND_ONLY_ALLOCATED, false);
                boolean onlyEduProgram = editForm ? false : context.getBoolean(BIND_ONLY_EDU_PROGRAM, false);

                EnrAllocationParagraph paragraph = context.get(BIND_PARAGRAPH);
                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
                        .where(eq(property(EnrCompetition.type().compensationType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().formativeOrgUnit().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)));

                if (onlyAllocated)
                {
                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                            .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r"))).buildQuery()));
                }
                DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
                extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));
                List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());
                extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));
                compDQL.where(notExists(extrDQL.buildQuery()));


                if (onlyEduProgram)
                {

                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                            .where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r"))).buildQuery()));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .filter(OrgUnit.fullTitle())
                .order(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                boolean editForm = context.getBoolean(BIND_EDIT_FORM, false);

                boolean onlyAllocated = editForm ? false : context.getBoolean(BIND_ONLY_ALLOCATED, false);
                boolean onlyEduProgram = editForm ? false : context.getBoolean(BIND_ONLY_EDU_PROGRAM, false);

                EnrAllocationParagraph paragraph = context.get(BIND_PARAGRAPH);
                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
                        .where(eq(property(EnrCompetition.type().compensationType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)));

                if (onlyAllocated)
                {
                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                            .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r"))).buildQuery()));
                }
                DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
                extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));
                List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());
                extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));
                compDQL.where(notExists(extrDQL.buildQuery()));


                if (onlyEduProgram)
                {

                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                            .where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r"))).buildQuery()));
                }

                if (formativeOrgUnit != null)
                {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }
                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EduProgramSubject.code())
                .order(EduProgramSubject.title())
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSetDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrProgramSetBase.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                boolean editForm = context.getBoolean(BIND_EDIT_FORM, false);

                boolean onlyAllocated = editForm ? false : context.getBoolean(BIND_ONLY_ALLOCATED, false);
                boolean onlyEduProgram = editForm ? false : context.getBoolean(BIND_ONLY_EDU_PROGRAM, false);

                EnrAllocationParagraph paragraph = context.get(BIND_PARAGRAPH);
                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
                        .where(eq(property(EnrCompetition.type().compensationType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                        .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)));

                if (onlyAllocated)
                {
                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                            .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r"))).buildQuery()));
                }

                DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
                extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));
                List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());
                extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));
                compDQL.where(notExists(extrDQL.buildQuery()));


                if (onlyEduProgram)
                {

                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                            .where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r"))).buildQuery()));
                }


                if (formativeOrgUnit != null)
                {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }
                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EnrProgramSetBase.title())
                .filter(EnrProgramSetBase.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSetItemDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrProgramSetItem.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                boolean editForm = context.getBoolean(BIND_EDIT_FORM, false);

                boolean onlyAllocated = editForm ? false : context.getBoolean(BIND_ONLY_ALLOCATED, false);
                boolean onlyEduProgram = editForm ? false : context.getBoolean(BIND_ONLY_EDU_PROGRAM, false);

                EnrAllocationParagraph paragraph = context.get(BIND_PARAGRAPH);
                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
                        .where(eq(property(EnrCompetition.type().compensationType().fromAlias("comp")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().fromAlias("comp")), property(alias, EnrProgramSetItem.programSet())))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                        .where(eq(property("r", EnrRequestedCompetition.state().code()), commonValue(EnrEntrantStateCodes.ENROLLED)))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet()), commonValue(context.get(BIND_PROGRAM_SET))));

                if (onlyAllocated)
                {
                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                            .where(eq(property(alias, EnrProgramSetItem.id()), property("alloc", EnrProgramAllocationItem.programSetItem().id())))
                            .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r")))
                            .where(eq(property("alloc", EnrProgramAllocationItem.programSetItem().programSet()), property("comp", EnrCompetition.programSetOrgUnit().programSet())))
                            .buildQuery()));

                }

                DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
                extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));
                List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());
                extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));
                compDQL.where(notExists(extrDQL.buildQuery()));


                if (onlyEduProgram)
                {

                    compDQL.where(exists(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                            .where(eq(property("rp", EnrRequestedProgram.programSetItem().id()), property(alias, EnrProgramSetItem.id())))
                            .where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r")))
                            .where(eq(property("rp", EnrRequestedProgram.programSetItem().programSet()), property("comp", EnrCompetition.programSetOrgUnit().programSet())))
                            .buildQuery()));
                }


                if (formativeOrgUnit != null)
                {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }
                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EnrProgramSetItem.program().title())
                .filter(EnrProgramSetItem.program().title())
                .pageable(true);
    }
}



    