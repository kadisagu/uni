package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактный параграф на абитуриентов (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrAbstractParagraphGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph";
    public static final String ENTITY_NAME = "enrAbstractParagraph";
    public static final int VERSION_HASH = -2034038859;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_NUMBER = "number";

    private EnrAbstractOrder _order;     // Абстрактный приказ на абитуриентов (2014)
    private int _number;     // Номер параграфа в приказе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     */
    @NotNull
    public EnrAbstractOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     */
    public void setOrder(EnrAbstractOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер параграфа в приказе. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrAbstractParagraphGen)
        {
            setOrder(((EnrAbstractParagraph)another).getOrder());
            setNumber(((EnrAbstractParagraph)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrAbstractParagraphGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrAbstractParagraph.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrAbstractParagraph is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrAbstractOrder) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrAbstractOrder.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrAbstractParagraph> _dslPath = new Path<EnrAbstractParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrAbstractParagraph");
    }
            

    /**
     * @return Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph#getOrder()
     */
    public static EnrAbstractOrder.Path<EnrAbstractOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends EnrAbstractParagraph> extends EntityPath<E>
    {
        private EnrAbstractOrder.Path<EnrAbstractOrder> _order;
        private PropertyPath<Integer> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph#getOrder()
     */
        public EnrAbstractOrder.Path<EnrAbstractOrder> order()
        {
            if(_order == null )
                _order = new EnrAbstractOrder.Path<EnrAbstractOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EnrAbstractParagraphGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return EnrAbstractParagraph.class;
        }

        public String getEntityName()
        {
            return "enrAbstractParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
