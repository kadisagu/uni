/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievement.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrPersonEduDocumentRelGen;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id"),
    @Bind(key = "entrantId", binding = "entrantId")
})
public class EnrEntrantAchievementAddEditUI extends UIPresenter
{
    private EntityHolder<EnrEntrantAchievement> _entityHolder = new EntityHolder<>(new EnrEntrantAchievement());

    private Long _entrantId;
    private EnrRequestType _requestType;
    private EnrEntrantDocumentViewWrapper _achievementDocument;

    @Override
    public void onComponentRefresh()
    {
        getEntityHolder().refresh();
        if (isAddForm())
        {
            getAchievement().setEntrant(DataAccessServices.dao().<EnrEntrant>getNotNull(getEntrantId()));
            setRequestType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getAchievement().getEntrant().getEnrollmentCampaign()));
        }
        else
        {
            setRequestType(getAchievement().getType().getAchievementKind().getRequestType());
        }

        IEnrEntrantAchievementProofDocument document = getAchievement().getDocument();
        if (null != document)
            _achievementDocument = EnrEntrantDocumentViewWrapper.wrap(document);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getAchievement().getEntrant().getEnrollmentCampaign());
        dataSource.put(EnrEntrantAchievementAddEdit.BIND_ENTRANT_ID, getAchievement().getEntrant().getId());
        if(getRequestType() != null)
        {
            dataSource.put(EnrEntrantAchievementAddEdit.BIND_REQUEST_TYPE_ID, getRequestType().getId());
        }
    }

    public boolean isAddForm() {
        return getEntityHolder().getId() == null;
    }

    public void onClickApply()
    {
        if(isAddForm())
        {
            EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getRequestType(), getAchievement().getEntrant().getEnrollmentCampaign());
        }

        if(getAchievementDocument() != null)
        {
            IEntity entity = getAchievementDocument().getEntity();
            if (entity instanceof PersonEduDocument)
            {
                PersonEduDocument eduDocument = (PersonEduDocument) entity;
                EnrPersonEduDocumentRel rel = DataAccessServices.dao().getByNaturalId(new EnrPersonEduDocumentRelGen.NaturalId(getAchievement().getEntrant(), eduDocument));
                if (null == rel)
                    DataAccessServices.dao().saveOrUpdate(rel = new EnrPersonEduDocumentRel(getAchievement().getEntrant(), eduDocument));
                getAchievement().setDocument(rel);
            } else if (entity instanceof IEnrEntrantAchievementProofDocument)
                getAchievement().setDocument((IEnrEntrantAchievementProofDocument) entity);
            else
                throw new IllegalArgumentException();
        }
        else
        {
            getAchievement().setDocument(null);
        }

        if(getAchievement().validateMark(ContextLocal.getErrorCollector(), "mark").hasErrors()
                || getAchievement().validateDocument(ContextLocal.getErrorCollector(), "document").hasErrors())
        {
            return;
        }

        EnrEntrantManager.instance().dao().saveOrUpdateAchievement(getAchievement());
        deactivate();

    }

    // Getters && Setters
    public EnrEntrantAchievement getAchievement()
    {
        return getEntityHolder().getValue();
    }

    public void setAchievementDocument(EnrEntrantDocumentViewWrapper achievementDocument)
    {
        _achievementDocument = achievementDocument;
    }

    public EnrEntrantDocumentViewWrapper getAchievementDocument()
    {
        return _achievementDocument;
    }

    public EntityHolder<EnrEntrantAchievement> getEntityHolder(){ return _entityHolder; }
    public void setEntityHolder(EntityHolder<EnrEntrantAchievement> entityHolder){ _entityHolder = entityHolder; }

    public Long getEntrantId(){ return _entrantId; }
    public void setEntrantId(Long entrantId){ _entrantId = entrantId; }

    public EnrRequestType getRequestType(){ return _requestType; }
    public void setRequestType(EnrRequestType requestType){ _requestType = requestType; }

    public boolean isShowMark()
    {
        return getAchievement().getType() != null && getAchievement().getType().isMarked();
    }
}