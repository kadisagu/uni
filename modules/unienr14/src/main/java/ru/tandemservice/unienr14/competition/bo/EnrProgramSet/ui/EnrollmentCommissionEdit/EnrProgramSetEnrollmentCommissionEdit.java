/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

/**
 * @author Alexey Lopatin
 * @since 03.06.2015
 */
@Configuration
public class EnrProgramSetEnrollmentCommissionEdit extends BusinessComponentManager
{
    public static final String ENROLLMENT_COMMISSION_DS = EnrEnrollmentCommissionManager.DS_ENROLLMENT_COMMISSION;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ENROLLMENT_COMMISSION_DS, getName(), EnrEnrollmentCommission.permissionSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(DQLExpressions.eq(DQLExpressions.property(alias, EnrEnrollmentCommission.enabled()), DQLExpressions.value(Boolean.TRUE))))
                ))
                .create();
    }
}
