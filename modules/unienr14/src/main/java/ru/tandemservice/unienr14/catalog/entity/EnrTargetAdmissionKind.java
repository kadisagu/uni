package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrTargetAdmissionKindGen;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Вид целевого приема
 */
public class EnrTargetAdmissionKind extends EnrTargetAdmissionKindGen
{
    /**
     * Используется в текущей ПК - выбранная пользователем ПК в фильтрах и селекторах в модуле хранится в настройках пользователя.
     * @return true, если используется в ПК, иначе false
     */
    @Override
    @EntityDSLSupport
    public boolean isUseInEnrCamp()
    {
        EnrEnrollmentCampaign defaultCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();

        if (defaultCampaign == null)
            return false;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "ck").column(property(EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("ck")))
                .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().fromAlias("ck")), value(defaultCampaign)))
                .where(eq(property(EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("ck")), value(this)));

        return ISharedBaseDao.instance.get().existsEntity(dql.buildQuery());
    }
}