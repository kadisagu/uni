/* $Id: EcProfileDistributionEntrantAddUI.java 33636 2014-04-16 04:31:39Z nfedorovskih $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.EntrantAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.EnrPAItemWrapper;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.EnrPAQuotaManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.IEnrPAQuotaFreeWrapper;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Input({
    @Bind(key = EnrProgramAllocationEntrantAddUI.ALLOCATION_ID, binding = "allocation.id")
})
public class EnrProgramAllocationEntrantAddUI extends UIPresenter
{
    public static final String ALLOCATION_ID = "allocationId";

    private EnrProgramAllocation _allocation = new EnrProgramAllocation();

    private ISelectModel competitionTypeList;
    private EnrCompetitionType _competitionType;

    private List<EnrPAItemWrapper> itemList;
    private EnrPAItemWrapper item;

    private boolean _hasDefaultChecked;

    @Override
    public void onComponentRefresh()
    {
        setAllocation(DataAccessServices.dao().get(EnrProgramAllocation.class, getAllocation().getId()));
        setCompetitionTypeList(new LazySimpleSelectModel<>(DataAccessServices.dao().getList(EnrCompetitionType.class, EnrCompetitionType.code().s())));
        onChangeCompetitionType();
    }

    public void onChangeCompetitionType() {

        setItemList(new ArrayList<>());
        setItem(null);
        setHasDefaultChecked(false);

        if (getCompetitionType() == null) return;

        setItemList(EnrProgramAllocationManager.instance().dao().getChoiceSource(getAllocation(), getCompetitionType()));

        // обновляем рейтинг абитуриентов
        doRefreshEntrantRateList();
    }

    public void onRateRefresh()
    {
        // произошел выбор чекбокса или выбор из селекта
        // пока мы сидели на форме мог измениться план приема или еще что-нибудь влияющее на рейтинг
        // нужно пересчитать рейтинг, потом принудительно выделить тех абитуриентов и те направления,
        // которые пришли в POST'е, уменьшая при этом план, если нельзя выделить абитуриента или направление,
        // то пропускаем такого абитуриента, после чего пробежаться по всем не выделенным абитуриентам расставляя
        // им остатки по планам

        _hasDefaultChecked = true;

        // обновляем рейтинг
        doRefreshEntrantRateList();
    }

    public void onClickSave()
    {
        // сохраняем рекомендованных абитуриентов
        // пока мы сидели на форме, теоретически могло что угодно измениться: план, суммы баллов, наборы экзаменов
        // но мы все равно сохраняем ровно то, что выбрали на форме
        // превышение планов тут не проверяется, оно покажется уже в публикаторе распределения после сохранения

        Map<EnrRequestedCompetition, EnrProgramSetItem> choiceResult = new HashMap<>();
        for (EnrPAItemWrapper item : getItemList()) {
            if (item.isSelected() && item.getProgram() != null) choiceResult.put(item.getEntrant(), item.getProgram());
        }
        EnrProgramAllocationManager.instance().dao().addItems(getAllocation(), choiceResult);

        deactivate();
    }

    // presenter

    public boolean isShowTaColumn() {
        return getCompetitionType() != null && EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(getCompetitionType().getCode());
    }

    public String getCheckboxFieldId() {
        if (null == getItem()) return "";
        return "c." + getItem().getId();
    }

    public String getProgramSelectId() {
        if (null == getItem()) return "";
        return "p." + getItem().getId();
    }

    // utils

    private void doRefreshEntrantRateList()
    {
        // свободные места в распределении
        IEnrPAQuotaFreeWrapper freeQuotaDTO = EnrProgramAllocationManager.instance().dao().getFreeQuota(getAllocation().getId());

        // получаем алгоритм заполнения планов приема
        EnrPAQuotaManager quotaManager = new EnrPAQuotaManager(freeQuotaDTO);

        // общее число строк
        int len = getItemList().size();

        int i = 0;

        Set<Long> chosenEntrantSet = new HashSet<>();
        for (EnrPAItemWrapper item : getItemList()) {
            if (item.isSelected()) chosenEntrantSet.add(item.getId());
            item.setSelected(false);
        }

        if (_hasDefaultChecked) {
            // число выбранных абитуриентов
            int checkedCount = chosenEntrantSet.size();

            // если есть выбранные по умолчанию абитуриенты и направления, то только их и пытаемся выбрать в рейтинге
            while (i < len && checkedCount > 0)
            {
                EnrPAItemWrapper item = getItemList().get(i);

                if (chosenEntrantSet.contains(item.getId())) {
                    checkedCount--;
                    EnrProgramSetItem choiceResult = quotaManager.doChoose(item.getProgramList(), item.getProgram());
                    item.setProgram(choiceResult);
                    item.setSelected(choiceResult != null);
                }

                i++;
            }
        } else {
            // общее число свободных мест
            int freeTotal = quotaManager.getFreeTotal();

            while (i < len && freeTotal > 0)
            {
                EnrPAItemWrapper item = getItemList().get(i);

                EnrProgramSetItem choiceResult = quotaManager.doChoose(item.getProgramList(), null);

                if (choiceResult != null) {
                    freeTotal--;
                    item.setProgram(choiceResult);
                    item.setSelected(true);
                }

                i++;
            }
        }

        while (i < len) {
            item.setProgram(null);
            item.setSelected(false);
            i++;
        }
    }

    // getters and setters

    public EnrCompetitionType getCompetitionType()
    {
        return _competitionType;
    }

    public void setCompetitionType(EnrCompetitionType competitionType)
    {
        _competitionType = competitionType;
    }

    public ISelectModel getCompetitionTypeList()
    {
        return competitionTypeList;
    }

    public void setCompetitionTypeList(ISelectModel competitionTypeList)
    {
        this.competitionTypeList = competitionTypeList;
    }

    public EnrPAItemWrapper getItem()
    {
        return item;
    }

    public void setItem(EnrPAItemWrapper item)
    {
        this.item = item;
    }

    public List<EnrPAItemWrapper> getItemList()
    {
        return itemList;
    }

    public void setItemList(List<EnrPAItemWrapper> itemList)
    {
        this.itemList = itemList;
    }

    public EnrProgramAllocation getAllocation()
    {
        return _allocation;
    }

    public void setAllocation(EnrProgramAllocation allocation)
    {
        _allocation = allocation;
    }

    public boolean isHasDefaultChecked()
    {
        return _hasDefaultChecked;
    }

    public void setHasDefaultChecked(boolean hasDefaultChecked)
    {
        _hasDefaultChecked = hasDefaultChecked;
    }
}
