package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import java.util.*;

/**
 * @author vdanilov
 */
@Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888")
public abstract class EnrSelectionContext {

    public static final Predicate<EnrSelectionItem> SELECTED_ITEMS_PREDICATE = EnrSelectionItem::isSelected;

    // ключ группы
    @SuppressWarnings("serial")
    public class GroupKey extends MultiKey
    {
        private final EnrCompetition competition;
        private final EnrCampaignTargetAdmissionKind targetAdmissionKind;

        public EnrCompetition getCompetition() { return this.competition; }
        public EnrCampaignTargetAdmissionKind getTargetAdmissionKind() { return this.targetAdmissionKind; }

        public Long getTaKindId() {
            if (null == this.getTargetAdmissionKind()) { return null; }
            return this.getTargetAdmissionKind().getTargetAdmissionKind().getId();
        }

        public String getCompositeId() {
            Long taKindId = getTaKindId();
            if (null == taKindId) { return Long.toString(getCompetition().getId(), 32); }
            return Long.toString(getCompetition().getId(), 32) + "#" + Long.toString(taKindId, 32);

        }

        // если группа без ЦП
        public GroupKey(final EnrCompetition competition) {
            super(competition.getId(), null);
            this.competition = competition;
            this.targetAdmissionKind = null;
        }

        // если группа по ЦП
        public GroupKey(final EnrCompetition competition, final EnrCampaignTargetAdmissionKind targetAdmissionKind) {
            super(competition.getId(), targetAdmissionKind.getId());
            this.competition = competition;
            this.targetAdmissionKind = targetAdmissionKind;
        }

        public String getTitle() {
            if (null == this.getTargetAdmissionKind()) {
                return this.getCompetition().getTitle();
            }
            return this.getCompetition().getTitle()+", ЦП="+this.getTargetAdmissionKind().getTitle();
        }
    }

    protected boolean debug;
    public boolean isDebug() { return debug; }

    protected boolean targetAdmissionCompetition;
    public boolean isTargetAdmissionCompetition() { return this.targetAdmissionCompetition; }

    private final Map<GroupKey, EnrSelectionGroup> stepItemGroupMap = new LinkedHashMap<>();
    private final Map<GroupKey, EnrSelectionGroup> stepItemGroupMapRo = Collections.unmodifiableMap(this.stepItemGroupMap);
    public Map<GroupKey, EnrSelectionGroup> getStepItemGroupMap() { return this.stepItemGroupMapRo; }

    private Set<EnrSelectionItem> preSelectedStepItems = null;
    public Set<EnrSelectionItem> setPreSelectedStepItems(final Collection<EnrSelectionItem> preSelectedStepItems) {
        return (this.preSelectedStepItems = new LinkedHashSet<>(preSelectedStepItems));
    }
    public Set<EnrSelectionItem> getPreSelectedStepItems() {
        return preSelectedStepItems;
    }

    private int _changeNumber;
    public int getChangeNumber() { return this._changeNumber; }
    protected void change() { this._changeNumber ++; }

    public boolean isCheckPrevEnrollmentDuplicates() { return true; }



    // действие: обновить состояние после инициализации
    public void afterInitCallback()
    {
        if (this.debug) {
            for(final EnrSelectionGroup group: this.getStepItemGroupMap().values()) {
                Debug.begin("group = " + group.getKey().getTitle());
                Debug.message("plan = " + group.getCompetitionPlan());
                Debug.message("extract = " + group.getCompetitionExtractSize());
                Debug.end();
            }
        }
    }

    public Collection<EnrSelectionItem> getStepItemWrappers(final EnrEntrant entrant) {
        return this.getStepItemWrappers4OtherGroups(entrant, null);
    }

    public Collection<EnrSelectionItem> getStepItemWrappers4OtherGroups(final EnrEntrant entrant, final EnrSelectionGroup excludeGroup) {
        final List<EnrSelectionItem> items = new ArrayList<>();
        for (final EnrSelectionGroup group: this.getStepItemGroupMap().values()) {
            if (group.equals(excludeGroup)) { continue; }
            final EnrSelectionItem item = group.getItemMap().get(entrant);
            if (null != item) { items.add(item); }
        }
        return items;
    }


    // действие: выбрать абитуриентов (выбираем всех, кто isSelectionAllowed, пока не наберется limit)
    public void executeSelection() {
        this.executeSelection(EnrSelectionContext.this::isSelectionAllowed);
    }

    /**
     * Выбирает абитуриентов, пока это возможно
     * @param selectionAllowedPredicate условия предварительной выборки абитуриентов
     */
    protected void executeSelection(final Predicate<EnrSelectionItem> selectionAllowedPredicate)
    {
        int i = 0;
        while (true) {
            // сохраняем число изменений (до начала операций)
            int savedChangeNumber = this.getChangeNumber();

            // поехали
            Debug.begin("iteration-" + String.valueOf(i++));
            try
            {
                for (final EnrSelectionGroup group: this.getStepItemGroupMap().values()) {
                    Debug.begin(group.getKey().getTitle());
                    try {
                        final int limit = this.calculateGroupLimit(group);
                        if (limit > 0) {
                            Debug.message("limit=" + limit);

                            // фильтруем тех, кого можно выбирать (с доп.условиями)
                            final Iterator<EnrSelectionItem> it = Iterators.filter(
                                group.getItemMap().values().iterator(),
                                selectionAllowedPredicate
                            );

                            // пробуем их выбрать
                            if (this.tryToSelectItems(group, it, limit) > 0) {
                                savedChangeNumber = -1; // что-то изменилось, явно отмечаем это
                                // если перезачисление освободит место с этого же конкурса - ничего страшного, мы снова его обработаем в следующей итерации цикла
                                // если перезачисление освободит место с друого гонкурса - тоже все хорошо, в следующей итерации цикла он будет обработан
                            }
                        }
                    } finally {
                        Debug.end();
                    }
                }
            } finally {
                Debug.end();
            }

            // если ничего не изменилось - выходим
            if (savedChangeNumber == this.getChangeNumber()) {
                break;
            }
        }
    }

    // действие: сохранить в базу результат выбора
    public void applyResults() {

    }

    /** @return ключ группы (конкурс+цп, если надо) */
    protected GroupKey getKey(final EnrRequestedCompetition requestedCompetition)
    {
        if (this.isTargetAdmissionCompetition()) {
            if (requestedCompetition instanceof EnrRequestedCompetitionTA) {
                final EnrRequestedCompetitionTA rcTa = (EnrRequestedCompetitionTA)requestedCompetition;
                return new GroupKey(rcTa.getCompetition(), rcTa.getTargetAdmissionKind());
            }
        }
        return new GroupKey(requestedCompetition.getCompetition());
    }

    /** @return группа */
    protected EnrSelectionGroup getGroup(final IEnrEnrollmentStepItem stepItem)
    {
        final GroupKey key = this.getKey(stepItem.getEntity().getRequestedCompetition());
        EnrSelectionGroup group = this.stepItemGroupMap.get(key);
        if (null == group) { this.stepItemGroupMap.put(key, group = new EnrSelectionGroup(key, this)); }
        return group;
    }

    /** @return true, если элемент можно выбрать */
    public boolean isSelectionAllowed(final EnrSelectionItem selectionItem)
    {
        // если уже выбран или уже игнорируется, то не выбираем
        if (selectionItem.isSelected()) { return false; }
        if (selectionItem.isSkipped()) { return false; }
        return true;
    }

    // по EnrEnrollmentStepItemOrderInfo находит группу (не создает новых)
    public EnrSelectionGroup getStepItemGroup(final IEnrSelectionItemPrevEnrollmentInfo orderInfo) {
        final GroupKey orderInfoGroupKey = this.getKey(orderInfo.getRequestedCompetition());
        return this.getStepItemGroupMap().get(orderInfoGroupKey);
    }


    /** @return число абитуриентов, которых можно еще выбрать (по умолчанию: пересчитанное число мест (за вычитом уже зачисленных) - число выбранных + число освобожденных) */
    protected int calculateGroupLimit(final EnrSelectionGroup group)
    {
        // берем пересчитанное число оставшихся мест
        long plan = group.getCompetitionPlan();

        // отнимаем места, занятые зачисленными абитуриентами на данный конкурс (за исключением перезачисленных и предвыбранных)
        plan -= group.getNonCoveredCompetitionExtractCount();

        // отнимаем места, занятые выбранными и/или предвыбранными абитуриентами (на этот конкурс)
        plan -= group.getPreAndSelectedEntrantCount();

        // возвращаем оставшееся число мест
        return (int)Math.max(0, plan);
    }

    /**
     * по всем абитуриентам из списка пробует их выбрать (до достижения указанного предела)
     * @return число выбранных
     */
    protected int tryToSelectItems(final EnrSelectionGroup group, final Iterator<EnrSelectionItem> selectionAllowedIt, int limit)
    {
        int result = 0;
        while ((limit > 0) && selectionAllowedIt.hasNext()) {
            if (this.tryToSelectItem(selectionAllowedIt.next())) {
                limit--;
                result++;
            }
        }
        return result;
    }

    /**
     * пробует выбрать абитуриента (проверяет, не выбран ли он где-то, обрабатывает конфликты выбора)
     * @return true, если абитуриент выбран
     */
    protected boolean tryToSelectItem(final EnrSelectionItem selectionItem)
    {
        // ЭТАП-0: проверка, что мы его вообще можем выбирать
        if (!this.isSelectionAllowed(selectionItem)) { return false; }

        // ЭТАП-1: первым делом, проверяем, не выбран ли он уже где-то (если выбран - это надо обработать)
        {
            final Collection<EnrSelectionItem> currentSelection = this.getCurrentSelection(selectionItem.getEntrant(), input -> Objects.equals(
                input.getRequestedCompetition().isParallel(),
                selectionItem.getRequestedCompetition().isParallel()
            ));

            if (currentSelection.size() > 0)
            {
                // если даный абитуриент в конкурсе уже выбран (что странно) - пропускаем
                if (currentSelection.contains(selectionItem)) { return false; }

                // если приоритет в уже выбранном элементе не ниже (хотябы по одному), то игнорируем переданный элемент (и сразу выходим)
                {
                    final Iterator<EnrSelectionItem> it = Iterators.filter(currentSelection.iterator(), input -> (input.getRequestedCompetition().getPriority() <= selectionItem.getRequestedCompetition().getPriority()));
                    if (it.hasNext()) {
                        selectionItem.skip(new EnrSelectionSkipReason.AlreadySelectedWithHigherPriority(it.next()));
                        return false;
                    }
                }

                // исключаем выбранных с худшими приоритетами (они все такие, т.к. пункт выше не сработал)
                for (final EnrSelectionItem currentSelected : currentSelection) {
                    currentSelected.skip(new EnrSelectionSkipReason.ToBeSelectedWithHigherPriority(selectionItem));
                }
            }
        }

        // ЭТАП-2: сохраняем выбор и освобождаем места при перевыборе (если таковые есть)
        return this.selectStepItem(selectionItem);
    }


    /**
     * принудительно выбирает абитуриента (без проверки того, что он выбран на другие конекрсы с соотв. приоритетами)
     * освобождает места, связанные с перезачислением
     * проверяется толко то, что абитуриент еще не выбран, что он включен в шаг и проверки того, что его можно выбирать
     * @return true, если абитуриент выбран
     */
    protected boolean selectStepItem(final EnrSelectionItem selectionItem)
    {
        // еще раз проверяем (т.к. метод может быть вызван для любого абитуриента из вне)
        if (!this.isSelectionAllowed(selectionItem)) { return false; }

        // выбираем абитуриента
        selectionItem.select();

        // освобождаем вакантные места в группах (только в тех, где на момент создания шага была резолюция «перезачислить» - т.е. освободить место)
        for (final IEnrSelectionItemPrevEnrollmentInfo orderInfo : selectionItem.getReenrollFromList())
        {
            final EnrSelectionGroup orderInfoGroup = this.getStepItemGroup(orderInfo);
            if (null == orderInfoGroup) {
                // конкурс, с которого абитуриента надо перезачислить не в текущем шаге - место не освобождается (число мест пересчитывать не надо)
                continue;
            }

            // освобождаем вакантное место (здесь проверять не надо, потому что он не обязтельно зачислен или рекомендован)
            final boolean registered = orderInfoGroup.reenroll(orderInfo, selectionItem);
            if (registered) {
                if (this.debug) {
                    Debug.message(
                        "xx " + selectionItem.getEntrantFio() + ": освобождение вакантного места «" + orderInfoGroup.getKey().getTitle() + "»"
                    );
                }
            }
        }

        // абитуриента выбрали
        return true;
    }

    /** @return not-null { stepItemWrapper i | i.entrant=entrant && i.selected=true && predicate(i)=true } */
    protected Collection<EnrSelectionItem> getCurrentSelection(final EnrEntrant entrant, final Predicate<EnrSelectionItem> predicate)
    {
        final List<EnrSelectionItem> result = new ArrayList<>(4); // как правило, там 1-2 элемента
        for (final EnrSelectionGroup group : this.getStepItemGroupMap().values()) {
            final EnrSelectionItem selectionItem = group.getItemMap().get(entrant);
            if (null == selectionItem) { continue; }
            if (!selectionItem.isSelected()) { continue; }
            if (!entrant.equals(selectionItem.getEntrant())) { continue; }
            if (!predicate.apply(selectionItem)) { continue; }
            result.add(selectionItem);
        }
        return result;
    }
}
