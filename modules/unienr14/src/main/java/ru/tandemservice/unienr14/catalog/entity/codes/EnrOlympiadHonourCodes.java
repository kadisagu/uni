package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Степень отличия в дипломе участника олимпиады"
 * Имя сущности : enrOlympiadHonour
 * Файл data.xml : unienr.olymp.catalog.data.xml
 */
public interface EnrOlympiadHonourCodes
{
    /** Константа кода (code) элемента : Победитель (title) */
    String WINNER = "1";
    /** Константа кода (code) элемента : Призер (title) */
    String PRIZE_WINNER = "2";

    Set<String> CODES = ImmutableSet.of(WINNER, PRIZE_WINNER);
}
