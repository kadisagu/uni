package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.rating.entity.gen.EnrChosenEntranceExamFormGen;

/**
 * Выбранная форма сдачи (ВВИ-ф) (2013)
 */
public class EnrChosenEntranceExamForm extends EnrChosenEntranceExamFormGen implements IEntityDebugTitled
{
    public EnrChosenEntranceExamForm()
    {
    }

    public EnrChosenEntranceExamForm(EnrChosenEntranceExam chosenExam, EnrExamPassForm passForm)
    {
        setChosenEntranceExam(chosenExam);
        setPassForm(passForm);
    }

    @Override
    public String getEntityDebugTitle() {
        return getChosenEntranceExam().getEntityDebugTitle() + ": " + getPassForm().getTitle();
    }

    @EntityDSLSupport(parts = EnrChosenEntranceExamForm.L_MARK_SOURCE+"."+EnrEntrantMarkSource.P_MARK_AS_LONG)
    @Override
    public long getMarkAsLong() {
        final EnrEntrantMarkSource markSource = getMarkSource();
        return (null == markSource ? 0L : markSource.getMarkAsLong());
    }

    @EntityDSLSupport(parts = EnrChosenEntranceExamForm.P_MARK_AS_LONG)
    @Override
    public String getMarkAsString() {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
    }

    @EntityDSLSupport(parts = EnrChosenEntranceExamForm.P_MARK_AS_LONG)
    @Override
    public double getMarkAsDoubleNullSafe() {
        final EnrEntrantMarkSource markSource = getMarkSource();
        return (null == markSource ? 0 : markSource.getMarkAsDouble());
    }
}