/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.CheckboxDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.RadioDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic.EnrParagraphEntrantSelectionDSHandler;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/8/14
 */
@Configuration
public class EnrEnrollmentParagraphAddEdit extends BusinessComponentManager
{
    public static final String BIND_REQUEST_TYPE = "requestType";
    public static final String BIND_COMPENSATION_TYPE = "compensationType";
    public static final String BIND_FORM = "eduForm";
    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String BIND_SUBJECT = "programSubject";
    public static final String BIND_PROGRAM_SET_LIST = "programSet";
    public static final String BIND_COMPETITION_TYPE = "competitionType";
    public static final String BIND_COMPETITION_LIST = "competition";
    public static final String BIND_ONLY_ENROLLED_MARKED = "onlyEnrolledMarked";
    public static final String BIND_TARGET_ADMISIION_KIND_LIST = "targetAdmissionKindList";
    public static final String BIND_CITIZENSHIP_ID = "citizenshipId";
    public static final String BIND_PARALLEL = "parallel";
    public static final String BIND_PARAGRAPH = "paragraph";
    public static final String BIND_ORIGINAL_HAND_IN = "originalHandIn";
    public static final String BIND_ENROLLMENT_ACCEPTED = "enrollmentAccepted";
    public static final String BIND_ENROLLMENT_AVAILABLE = "enrollmentAvailable";
    public static final String BIND_CUSTOM_STATE = "customStateList";

    public static final String DS_ENTRANT = "entrantDS";
    public static final String DS_FORM = "formDS";
    public static final String DS_ORG_UNIT = "orgUnitDS";
    public static final String DS_FORMATIVE_ORG_UNIT = "formativeOrgUnitDS";
    public static final String DS_SUBJECT = "subjectDS";
    public static final String DS_COMPETITION_TYPE = "competitionTypeDS";
    public static final String DS_PROGRAM_SET = "programSetDS";
    public static final String DS_COMPETITION = "competitionDS";
    public static final String DS_TARGET_ADMISSION_KIND = "targetAdmissionKindDS";
    public static final String DS_CUSTOM_STATE = "statusDS";

    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String RADIOBOX_COLUMN = "radiobox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_ENTRANT, entrantDS(), entrantDSHandler()))
            .addDataSource(selectDS(DS_FORM, eduProgramFormDSHandler()))
            .addDataSource(selectDS(DS_ORG_UNIT, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addDataSource(selectDS(DS_FORMATIVE_ORG_UNIT, formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS(DS_SUBJECT, subjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(selectDS(DS_COMPETITION_TYPE, competitionTypeDSHandler()))
            .addDataSource(selectDS(DS_PROGRAM_SET, programSetDSHandler()))
            .addDataSource(selectDS(DS_COMPETITION, competitionDSHandler()))
            .addDataSource(selectDS(DS_TARGET_ADMISSION_KIND, taKindDSHandler()))
             .addDataSource(selectDS(DS_CUSTOM_STATE, customStateDS()))
            .create();
    }

    /**
     * Создается точка расширения "Список выбранных конкурсов на форме добавления/редактирования параграфа", единая
     * для всех форм добавления/редактирования параграфов о зачислении
     *
     * @return точка расширения "Список выбранных конкурсов на форме добавления/редактирования параграфа"
     */
    @Bean
    public ColumnListExtPoint entrantDS()
    {
        return ColumnListExtPoint.with(getName(), "entrantDS")
            .addColumn(CheckboxDSColumn.with().name(CHECKBOX_COLUMN).create())
            .addColumn(RadioDSColumn.with().name(RADIOBOX_COLUMN).visible("ui:order.printFormType.selectHeadman").create())
            .addColumn(TextDSColumn.with().name("fio").path(EnrRequestedCompetition.request().entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
            .addColumn(TextDSColumn.with().name(EnrParagraphEntrantSelectionDSHandler.CUSTOM_STATES).path(EnrParagraphEntrantSelectionDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()).create())
            .addColumn(TextDSColumn.with().name("sex").path(EnrRequestedCompetition.request().entrant().person().identityCard().sex().shortTitle()).create())
            .addColumn(TextDSColumn.with().name("passport").path(EnrRequestedCompetition.request().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
            .addColumn(TextDSColumn.with().name("eduLevel").path(EnrRequestedCompetition.request().eduDocument().eduLevel().shortTitle()).create())
            .addColumn(TextDSColumn.with().name("competition").path(EnrRequestedCompetition.competition().title()).formatter(NoWrapFormatter.INSTANCE).create())
            .addColumn(TextDSColumn.with().name("conditions").path(EnrRequestedCompetition.parametersTitle()).formatter(NoWrapFormatter.INSTANCE).create())
            .addColumn(TextDSColumn.with().name("state").path(EnrRequestedCompetition.state().stateDaemonSafe()).create())
            .addColumn(TextDSColumn.with().name("priority").path(EnrRequestedCompetition.priority()).create())
            .addColumn(booleanColumn("original", EnrRequestedCompetition.originalDocumentHandedIn()).create())
            .addColumn(booleanColumn("accepted", EnrRequestedCompetition.accepted()).create())
            .addColumn(booleanColumn("enrollmentAvailable", EnrRequestedCompetition.enrollmentAvailable()).create())
            .addColumn(textColumn("otherExtracts", EnrParagraphEntrantSelectionDSHandler.OTHER_EXTRACTS).formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> entrantDSHandler()
    {
        return new EnrParagraphEntrantSelectionDSHandler(getName());
    }

    /* Выбор конкурсов */

    @Bean
    public IDefaultComboDataSourceHandler competitionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetitionType.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                    .where(eq(property(EnrCompetition.type().fromAlias("comp")), property(alias)))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                    .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                    .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                    .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                    ;

                if (formativeOrgUnit != null) {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
            .where(EnrCompetitionType.compensationType(), BIND_COMPENSATION_TYPE)
            .order(EnrCompetitionType.code())
            .filter(EnrCompetitionType.title())
            .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), property(alias)))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property("comp", EnrCompetition.requestType()), value(context.<EnrRequestType>get(BIND_REQUEST_TYPE))));

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
        .order(EduProgramForm.code())
        .filter(EduProgramForm.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                    .where(eq(property(EnrCompetition.programSetOrgUnit().orgUnit().fromAlias("comp")), property(alias)))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                    .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))));

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                    .where(eq(property(EnrCompetition.programSetOrgUnit().formativeOrgUnit().fromAlias("comp")), property(alias)))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property("comp", EnrCompetition.requestType()), value(context.<EnrRequestType>get(BIND_REQUEST_TYPE))))
                    .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                    .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))));

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
        .filter(OrgUnit.fullTitle())
        .order(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias("comp")), property(alias)))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                    .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                    .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))));

                if (formativeOrgUnit != null) {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
        .order(EduProgramSubject.code())
        .order(EduProgramSubject.title())
        .filter(EduProgramSubject.code())
        .filter(EduProgramSubject.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSetDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrProgramSetBase.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "comp")
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().fromAlias("comp")), property(alias)))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                    .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                    .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                    .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                    .where(eq(property("comp", EnrCompetition.type()), commonValue(context.get(BIND_COMPETITION_TYPE))))
                    ;

                if (formativeOrgUnit != null) {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
        .order(EnrProgramSetBase.title())
        .filter(EnrProgramSetBase.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetition.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);
                List programSetList = context.get(BIND_PROGRAM_SET_LIST);

                dql
                    .where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .where(eq(property(alias, EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                    .where(eq(property(alias, EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                    .where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().programForm()), commonValue(context.get(BIND_FORM))))
                    .where(eq(property(alias, EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                    .where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                    .where(eq(property(alias, EnrCompetition.type()), commonValue(context.get(BIND_COMPETITION_TYPE))))
                ;

                if (formativeOrgUnit != null) {
                    dql.where(eq(property(alias, EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }
                if (programSetList != null && !programSetList.isEmpty()) {
                    dql.where(in(property(alias, EnrCompetition.programSetOrgUnit().programSet()), programSetList));
                }


                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
                    .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(BIND_PARALLEL))))
                    .where(eq(property(EnrRequestedCompetition.competition().fromAlias("r")), property(alias)));

                if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED))
                    ));
                } else {
                    compDQL.where(or(
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                        eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
                    ));
                }


                dql.where(exists(compDQL.buildQuery()));
            }
        }
            .order(EnrCompetition.programSetOrgUnit().programSet().title())
            .order(EnrCompetition.eduLevelRequirement().code())
            .filter(EnrCompetition.programSetOrgUnit().programSet().title())
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler taKindDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EnrTargetAdmissionKind.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        EnrCampaignTargetAdmissionKind.class,
                        EnrCampaignTargetAdmissionKind.L_TARGET_ADMISSION_KIND, property(alias),
                        EnrCampaignTargetAdmissionKind.L_ENROLLMENT_CAMPAIGN, context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                ));
            }
        };

        handler.order(EnrTargetAdmissionKind.title());
        handler.filter(EnrTargetAdmissionKind.title());
        return handler;
    }

    @Bean
    public IDefaultComboDataSourceHandler customStateDS()
    {
       return EnrEntrantCustomStateType.defaultSelectDSHandler(getName());
    }
}