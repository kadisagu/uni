/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.logic;

import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentPropertyWrapper;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentTypeSettingsConfig;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.entity.PersonDocumentRoleRel;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantBaseDocumentGen;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;

/**
 * @author nvankov
 * @since 27.03.2017
 */
public class EnrEntrantBaseDocumentDao extends UniBaseDao implements IEnrEntrantBaseDocumentDao
{
    @Override
    public void modifyConfig(PersonDocumentTypeSettingsConfig config, PersonRole personRole)
    {
        if(personRole instanceof EnrEntrant)
        {
            EnrEntrant enrEntrant = (EnrEntrant) personRole;
            PersonDocumentType documentType = config.getDocumentType();
            if(documentType == null)
            {
                return;
            }

            EnrCampaignEntrantDocument campaignEntrantDocument = getByNaturalId(new EnrCampaignEntrantDocument.NaturalId(documentType, enrEntrant.getEnrollmentCampaign()));

            if(campaignEntrantDocument == null)
            {
                return;
            }

            if(campaignEntrantDocument.isSendFIS())
            {
                PersonDocumentPropertyWrapper propertyWrapper = config.getPropMap().get(PersonDocument.P_ISSUANCE_DATE);

                if(!propertyWrapper.isRequired() && !propertyWrapper.isUnmodifiableRequired())
                {
                    config.getPropMap().put(PersonDocument.P_ISSUANCE_DATE,
                            new PersonDocumentPropertyWrapper(propertyWrapper.getProperty(), propertyWrapper.isUsed(),
                                    propertyWrapper.isUnmodifiableUsed(), true, false));
                    config.getRequiredProperties().add(PersonDocument.P_ISSUANCE_DATE);
                }

                propertyWrapper = config.getPropMap().get(PersonDocument.P_ISSUANCE_PLACE);

                if(!propertyWrapper.isRequired() && !propertyWrapper.isUnmodifiableRequired())
                {
                    config.getPropMap().put(PersonDocument.P_ISSUANCE_PLACE,
                            new PersonDocumentPropertyWrapper(propertyWrapper.getProperty(), propertyWrapper.isUsed(),
                                    propertyWrapper.isUnmodifiableUsed(),true, false));

                    config.getRequiredProperties().add(PersonDocument.P_ISSUANCE_PLACE);
                }
            }
        }
    }

    @Override
    public void onSaveOrUpdate(Long documentId, PersonRole personRole)
    {
        if(personRole instanceof EnrEntrant)
        {
            EnrEntrant entrant = (EnrEntrant) personRole;
            PersonDocument document = get(documentId);

            PersonDocumentRoleRel roleRel = getByNaturalId(new PersonDocumentRoleRel.NaturalId(document, entrant));

            if (roleRel == null)
            {
                throw new IllegalStateException("Document without PersonDocumentRoleRel");
            }

            EnrEntrantBaseDocument entrantBaseDocument = getByNaturalId(new EnrEntrantBaseDocumentGen.NaturalId(roleRel));

            if(entrantBaseDocument == null)
            {
                entrantBaseDocument = new EnrEntrantBaseDocument();
                entrantBaseDocument.setEntrant(entrant);
                entrantBaseDocument.setDocRelation(roleRel);
                entrantBaseDocument.setDocumentType(roleRel.getDocument().getDocumentType());

                save(entrantBaseDocument);
            }
        }
    }

    @Override
    public PersonRole getPersonRole(ISecureRoleContext secureRoleContext)
    {
        return null;
    }
}
