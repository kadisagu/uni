/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.logic.EduProgramHigherProfComboDSHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/4/14
 */
@Configuration
public class EnrProgramSetAddEdit extends BusinessComponentManager
{
    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_FORM = "eduProgramForm";
    public static final String PARAM_SUBJECT = "eduProgramSubject";
    public static final String PARAM_ADDED_ORG_UNIT_IDS = "addedOrgUnitIds";
    public static final String PARAM_REQUEST_TYPE = "requestType";

    public static final String EXTERNAL_OU_DS = "externalOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS("programFormDS", programFormDSHandler()))
                .addDataSource(selectDS("programHigherDS", programHigherDSHandler()).addColumn(EduProgramProf.titleAndConditionsShort().s()))
                .addDataSource(selectDS("programSecondaryDS", programSecondaryDSHandler()).addColumn(EduProgramProf.titleWithCodeAndConditionsShortWithForm().s()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EduProgramManager.EDU_PROGRAM_KIND_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(exists(
                                    EnrRequestType2eduProgramKindRel.class,
                                    EnrRequestType2eduProgramKindRel.requestType().s(), context.get(PARAM_REQUEST_TYPE),
                                    EnrRequestType2eduProgramKindRel.programKind().s(), property(alias)
                        )))))
                .addDataSource(selectDS("orgUnitDS", orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(EXTERNAL_OU_DS, ExternalOrgUnitManager.instance().externalOrgUnitComboDSHandler()).addColumn(ExternalOrgUnit.P_TITLE_WITH_LEGAL_FORM))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                if (null == campaign)
                {
                    dql.where(isNull(alias + ".id"));

                } else
                {
                    dql.where(exists(new DQLSelectBuilder()
                            .fromEntity(EduProgramHigherProf.class, "pr").column("pr.id")
                            .where(eq(property(EduProgramHigherProf.year().fromAlias("pr")), value(campaign.getEducationYear())))
                            .where(eq(property(EduProgramHigherProf.programSubject().fromAlias("pr")), property(alias)))
                            .buildQuery()));
                }
            }
        }
                .where(EduProgramSubject.subjectIndex().programKind(), PARAM_PROGRAM_KIND)
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EduProgramHigherProfComboDSHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(EduProgramHigherProf.class, "edu")
                        .column(value(1))
                        .where(eq(property("edu", EduProgram.form()), property(alias, EduProgramForm.id())));

                sub.where(exists(addWhereConditions("edu", sub, context).buildQuery()));
            }
        }
                .order(EduProgramForm.code())
                .filter(EduProgramForm.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final Collection ids = context.get(PARAM_ADDED_ORG_UNIT_IDS);
                if (!CollectionUtils.isEmpty(ids))
                {
                    dql.where(notIn(property(alias + ".id"), ids));
                }
            }
        }
            .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSecondaryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSecondaryProf.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                if (null == campaign)
                {
                    dql.where(isNull(alias + ".id"));

                } else
                {
                    dql.where(eq(property(EduProgramSecondaryProf.year().fromAlias(alias)), value(campaign.getEducationYear())));
                }
            }
        }
            .order(EduProgramProf.programSubject().code())
            .order(EduProgram.title())
            .filter(EduProgramProf.programSubject().code())
            .filter(EduProgram.title())
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programHigherDSHandler()
    {
        return new EduProgramHigherProfComboDSHandler(getName(), EduProgramHigherProf.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                addWhereConditions(alias, dql, context);
            }
        }
                .where(EduProgramHigherProf.form(), PARAM_FORM)
                .order(EduProgram.title())
                .filter(EduProgram.title());
    }
}