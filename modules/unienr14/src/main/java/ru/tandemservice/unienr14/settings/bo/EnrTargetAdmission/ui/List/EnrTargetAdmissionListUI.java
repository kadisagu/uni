/**
 *$Id: EnrTargetAdmissionListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.util.TargetAdmissionKindDataWrapper;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class EnrTargetAdmissionListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        // заполнение полей
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public boolean isNothingSelected()
    {
        return getEnrEnrollmentCampaign() == null;
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEnrollmentCampaign());
    }

    public void onToogleTargetAdmissionUse()
    {
        EnrTargetAdmissionManager.instance().dao().doToogleTargetAdmissionKindUse(DataAccessServices.dao().<EnrTargetAdmissionKind>getNotNull(getListenerParameterAsLong()), getEnrEnrollmentCampaign());
    }

    public void onToogleTargetAdmissionNotUse()
    {
        EnrTargetAdmissionManager.instance().dao().doToogleTargetAdmissionKindNotUse(DataAccessServices.dao().<EnrTargetAdmissionKind>getNotNull(getListenerParameterAsLong()), getEnrEnrollmentCampaign());
    }

    public void onToogleTargetAdmissionStateInterestTrue()
    {
        EnrTargetAdmissionManager.instance().dao().doToogleTargetAdmissionStateInterest(DataAccessServices.dao().<EnrTargetAdmissionKind>getNotNull(getListenerParameterAsLong()), getEnrEnrollmentCampaign(), true);
    }

    public void onToogleTargetAdmissionStateInterestFalse()
    {
        EnrTargetAdmissionManager.instance().dao().doToogleTargetAdmissionStateInterest(DataAccessServices.dao().<EnrTargetAdmissionKind>getNotNull(getListenerParameterAsLong()), getEnrEnrollmentCampaign(), false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrEnrollmentCampaign());
    }

    private Long _editedRowId;
    private ExternalOrgUnit _currentExtOrgUnit;

    public void onClickEditRow()
    {
        _editedRowId = getListenerParameterAsLong();

    }

    public void onClickSaveRow()
    {
        EnrTargetAdmissionManager.instance().dao().updateTAKindExtOrgUnits(_editedRowId, getEnrEnrollmentCampaign(), getConfig().getDataSource(EnrTargetAdmissionList.TARGET_ADMISSION_SEARCH_DS).<TargetAdmissionKindDataWrapper>getRecordById(_editedRowId).getExternalOrgUnits());
        _editedRowId = null;
    }

    public void onClickCancelEdit()
    {
        _editedRowId = null;
    }


    // Getters & Setters

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public ExternalOrgUnit getCurrentExtOrgUnit(){ return _currentExtOrgUnit; }
    public void setCurrentExtOrgUnit(ExternalOrgUnit currentExtOrgUnit){ _currentExtOrgUnit = currentExtOrgUnit; }

    public boolean isEditMode(){ return  _editedRowId != null; }

    public boolean isStateInterestDisabled(){ return  _editedRowId != null || !getCurrentRow().isUseInEnrCampaign(); }


    public boolean isCurrentRowInEditMode(){ return _editedRowId != null && getCurrentRow().getId().equals(_editedRowId); }

    public TargetAdmissionKindDataWrapper getCurrentRow(){ return getConfig().getDataSource(EnrTargetAdmissionList.TARGET_ADMISSION_SEARCH_DS).getCurrent(); }
}
