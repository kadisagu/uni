/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubScienceDataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.bo.Person.ui.AcademicDegreeAddEdit.PersonAcademicDegreeAddEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.AcademicStatusAddEdit.PersonAcademicStatusAddEdit;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;

import java.util.List;

/**
 * @author oleyba
 * @since 4/22/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantPubScienceDataTabUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();
    private ISecureRoleContext secureRoleContext;

    private DynamicListDataSource<PersonAcademicDegree> _personScienceDegreeDataSource;
    private DynamicListDataSource<PersonAcademicStatus> _personScienceStatusDataSource;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setSecureRoleContext(EnrPersonLegacyUtils.getSecureRoleContext(getEntrant()));

        prepareScienceDegreeDataSource();
        prepareScienceStatusDataSource();
    }

    public void onClickAddScienceDegree()
    {
        _uiActivation.asRegionDialog(PersonAcademicDegreeAddEdit.class.getSimpleName())
                .parameter("personId", getPerson().getId())
                .parameter("personAcademicDegreeId", null)
                .activate();
    }

    public void onClickPrintScienceDegreeFile()
    {
        PersonAcademicDegree item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        byte[] content = item.getDiplomFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл диплома пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(item.getDiplomFileType()).fileName(item.getDiplomFileName()).document(content), false);
    }

    public void onClickEditScienceDegree()
    {
        _uiActivation.asRegionDialog(PersonAcademicDegreeAddEdit.class.getSimpleName())
                .parameter("personId", null)
                .parameter("personAcademicDegreeId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickAddScienceStatus()
    {
        _uiActivation.asRegionDialog(PersonAcademicStatusAddEdit.class.getSimpleName())
                .parameter("personId", getPerson().getId())
                .parameter("personAcademicStatusId", null)
                .activate();
    }

    public void onClickPrintScienceStatusFile()
    {
        PersonAcademicStatus item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        byte[] content = item.getCertificateFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл аттестата пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(item.getCertificateFileType()).fileName(item.getCertificateFileName()).document(content), false);
    }

    public void onClickEditScienceStatus()
    {
        _uiActivation.asRegionDialog(PersonAcademicStatusAddEdit.class.getSimpleName())
                .parameter("personId", null)
                .parameter("personAcademicStatusId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteScienceStatus()
    {
        PersonAcademicStatus item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        DataAccessServices.dao().delete(item);
        _personScienceStatusDataSource.refresh();
    }

    public void onClickDeleteScienceDegree()
    {
        PersonAcademicDegree item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        DataAccessServices.dao().delete(item);
        _personScienceDegreeDataSource.refresh();
    }

    // presenter

    public Person getPerson() {
        return getEntrant().getPerson();
    }

    // utils

    private void prepareScienceDegreeDataSource()
    {
        DynamicListDataSource<PersonAcademicDegree> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), context -> {
            prepareCustomDataSource(getPersonScienceDegreeDataSource(), PersonAcademicDegree.ENTITY_CLASS, PersonAcademicDegree.L_PERSON);
        }, 5);

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Ученая степень", PersonAcademicDegree.academicDegree().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap()
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                        .add(ISecureRoleContext.SECURE_ROLE_CONTEXT, getSecureRoleContext());
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Отрасль науки", PersonAcademicDegree.scienceBranch().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата присуждения", PersonAcademicDegree.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintScienceDegreeFile", "Печатать файл диплома").setDisabledProperty(PersonAcademicDegree.P_PRINTING_DISABLED).setPermissionKey("printFilePersonScienceDegree_enrEntrant"));
        if (getSecureRoleContext().isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditScienceDegree").setPermissionKey("editPersonScienceDegree_enrEntrant"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteScienceDegree", "Удалить ученую степень?").setPermissionKey("deletePersonScienceDegree_enrEntrant"));
        }

        setPersonScienceDegreeDataSource(dataSource);
    }

    private void prepareScienceStatusDataSource()
    {
        DynamicListDataSource<PersonAcademicStatus> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), context -> {
            prepareCustomDataSource(getPersonScienceStatusDataSource(), PersonAcademicStatus.ENTITY_CLASS, PersonAcademicStatus.L_PERSON);
        }, 5);

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Ученое звание", PersonAcademicStatus.academicStatus().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap()
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                        .add(ISecureRoleContext.SECURE_ROLE_CONTEXT, getSecureRoleContext());
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Дата присуждения", PersonAcademicStatus.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintScienceStatusFile", "Печатать файл аттестата").setDisabledProperty(PersonAcademicStatus.P_PRINTING_DISABLED).setPermissionKey("printFilePersonScienceStatus_enrEntrant"));
        if (getSecureRoleContext().isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditScienceStatus").setPermissionKey("editPersonScienceStatus_enrEntrant"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteScienceStatus", "Удалить ученое звание?").setPermissionKey("deletePersonScienceStatus_enrEntrant"));
        }
        setPersonScienceStatusDataSource(dataSource);
    }

    private <T extends IEntity> void prepareCustomDataSource(DynamicListDataSource<T> dataSource, String entityClass, String property)
    {
        MQBuilder builder = new MQBuilder(entityClass, "o");
        builder.addJoin("o", property, "e");

        builder.add(MQExpression.eq("e", "id", getPerson().getId()));

        OrderDescriptionRegistry orderSettings = new OrderDescriptionRegistry("o");
        orderSettings.applyOrder(builder, dataSource.getEntityOrder());

        List<T> entities = ISharedBaseDao.instance.get().getList(builder);
        int count = Math.max(entities.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
        dataSource.createPage(entities);
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return secureRoleContext;
    }

    public void setSecureRoleContext(ISecureRoleContext secureRoleContext)
    {
        this.secureRoleContext = secureRoleContext;
    }

    public DynamicListDataSource<PersonAcademicDegree> getPersonScienceDegreeDataSource()
    {
        return _personScienceDegreeDataSource;
    }

    public void setPersonScienceDegreeDataSource(DynamicListDataSource<PersonAcademicDegree> personScienceDegreeDataSource)
    {
        _personScienceDegreeDataSource = personScienceDegreeDataSource;
    }

    public DynamicListDataSource<PersonAcademicStatus> getPersonScienceStatusDataSource()
    {
        return _personScienceStatusDataSource;
    }

    public void setPersonScienceStatusDataSource(DynamicListDataSource<PersonAcademicStatus> personScienceStatusDataSource)
    {
        _personScienceStatusDataSource = personScienceStatusDataSource;
    }
}


