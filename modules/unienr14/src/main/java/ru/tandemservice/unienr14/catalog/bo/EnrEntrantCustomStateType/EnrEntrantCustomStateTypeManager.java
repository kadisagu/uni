/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEntrantCustomStateType;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author nvankov
 * @since 8/15/14
 */
@Configuration
public class EnrEntrantCustomStateTypeManager extends BusinessObjectManager
{
    public static EnrEntrantCustomStateTypeManager instance()
    {
        return instance(EnrEntrantCustomStateTypeManager.class);
    }
}



    