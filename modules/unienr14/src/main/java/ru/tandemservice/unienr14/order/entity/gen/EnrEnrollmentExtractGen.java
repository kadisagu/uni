package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка приказа о зачислении абитуриентов (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentExtractGen extends EnrAbstractExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract";
    public static final String ENTITY_NAME = "enrEnrollmentExtract";
    public static final int VERSION_HASH = -1037049446;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY = "entity";
    public static final String P_GROUP_TITLE = "groupTitle";
    public static final String P_EXCLUDE_LOWER_PRIORITY_COMPETITIONS = "excludeLowerPriorityCompetitions";
    public static final String L_STUDENT = "student";
    public static final String P_ORDER_INFO = "orderInfo";

    private EnrRequestedCompetition _entity;     // Выбранный конкурс
    private String _groupTitle;     // Зачислить в группу
    private boolean _excludeLowerPriorityCompetitions = true;     // Исключать абитуриента из конкурсов с более низким приоритетом
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrRequestedCompetition getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntity(EnrRequestedCompetition entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Зачислить в группу.
     */
    @Length(max=255)
    public String getGroupTitle()
    {
        return _groupTitle;
    }

    /**
     * @param groupTitle Зачислить в группу.
     */
    public void setGroupTitle(String groupTitle)
    {
        dirty(_groupTitle, groupTitle);
        _groupTitle = groupTitle;
    }

    /**
     * Если значение выставленно в true, то демон IEnrRatingDaemonDao#doUpdateExcludedByStatus будет исключать из конкурса все выбранные конкурсы данного абитуриента с приоритетами ниже текущего.
     *
     * @return Исключать абитуриента из конкурсов с более низким приоритетом. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludeLowerPriorityCompetitions()
    {
        return _excludeLowerPriorityCompetitions;
    }

    /**
     * @param excludeLowerPriorityCompetitions Исключать абитуриента из конкурсов с более низким приоритетом. Свойство не может быть null.
     */
    public void setExcludeLowerPriorityCompetitions(boolean excludeLowerPriorityCompetitions)
    {
        dirty(_excludeLowerPriorityCompetitions, excludeLowerPriorityCompetitions);
        _excludeLowerPriorityCompetitions = excludeLowerPriorityCompetitions;
    }

    /**
     * @return Студент. Свойство должно быть уникальным.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство должно быть уникальным.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEnrollmentExtractGen)
        {
            setEntity(((EnrEnrollmentExtract)another).getEntity());
            setGroupTitle(((EnrEnrollmentExtract)another).getGroupTitle());
            setExcludeLowerPriorityCompetitions(((EnrEnrollmentExtract)another).isExcludeLowerPriorityCompetitions());
            setStudent(((EnrEnrollmentExtract)another).getStudent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentExtractGen> extends EnrAbstractExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentExtract.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return obj.getEntity();
                case "groupTitle":
                    return obj.getGroupTitle();
                case "excludeLowerPriorityCompetitions":
                    return obj.isExcludeLowerPriorityCompetitions();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entity":
                    obj.setEntity((EnrRequestedCompetition) value);
                    return;
                case "groupTitle":
                    obj.setGroupTitle((String) value);
                    return;
                case "excludeLowerPriorityCompetitions":
                    obj.setExcludeLowerPriorityCompetitions((Boolean) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                        return true;
                case "groupTitle":
                        return true;
                case "excludeLowerPriorityCompetitions":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return true;
                case "groupTitle":
                    return true;
                case "excludeLowerPriorityCompetitions":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return EnrRequestedCompetition.class;
                case "groupTitle":
                    return String.class;
                case "excludeLowerPriorityCompetitions":
                    return Boolean.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentExtract> _dslPath = new Path<EnrEnrollmentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentExtract");
    }
            

    /**
     * @return Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getEntity()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Зачислить в группу.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getGroupTitle()
     */
    public static PropertyPath<String> groupTitle()
    {
        return _dslPath.groupTitle();
    }

    /**
     * Если значение выставленно в true, то демон IEnrRatingDaemonDao#doUpdateExcludedByStatus будет исключать из конкурса все выбранные конкурсы данного абитуриента с приоритетами ниже текущего.
     *
     * @return Исключать абитуриента из конкурсов с более низким приоритетом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#isExcludeLowerPriorityCompetitions()
     */
    public static PropertyPath<Boolean> excludeLowerPriorityCompetitions()
    {
        return _dslPath.excludeLowerPriorityCompetitions();
    }

    /**
     * @return Студент. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getOrderInfo()
     */
    public static SupportedPropertyPath<String> orderInfo()
    {
        return _dslPath.orderInfo();
    }

    public static class Path<E extends EnrEnrollmentExtract> extends EnrAbstractExtract.Path<E>
    {
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _entity;
        private PropertyPath<String> _groupTitle;
        private PropertyPath<Boolean> _excludeLowerPriorityCompetitions;
        private Student.Path<Student> _student;
        private SupportedPropertyPath<String> _orderInfo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getEntity()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> entity()
        {
            if(_entity == null )
                _entity = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Зачислить в группу.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getGroupTitle()
     */
        public PropertyPath<String> groupTitle()
        {
            if(_groupTitle == null )
                _groupTitle = new PropertyPath<String>(EnrEnrollmentExtractGen.P_GROUP_TITLE, this);
            return _groupTitle;
        }

    /**
     * Если значение выставленно в true, то демон IEnrRatingDaemonDao#doUpdateExcludedByStatus будет исключать из конкурса все выбранные конкурсы данного абитуриента с приоритетами ниже текущего.
     *
     * @return Исключать абитуриента из конкурсов с более низким приоритетом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#isExcludeLowerPriorityCompetitions()
     */
        public PropertyPath<Boolean> excludeLowerPriorityCompetitions()
        {
            if(_excludeLowerPriorityCompetitions == null )
                _excludeLowerPriorityCompetitions = new PropertyPath<Boolean>(EnrEnrollmentExtractGen.P_EXCLUDE_LOWER_PRIORITY_COMPETITIONS, this);
            return _excludeLowerPriorityCompetitions;
        }

    /**
     * @return Студент. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract#getOrderInfo()
     */
        public SupportedPropertyPath<String> orderInfo()
        {
            if(_orderInfo == null )
                _orderInfo = new SupportedPropertyPath<String>(EnrEnrollmentExtractGen.P_ORDER_INFO, this);
            return _orderInfo;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentExtract.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getOrderInfo();
}
