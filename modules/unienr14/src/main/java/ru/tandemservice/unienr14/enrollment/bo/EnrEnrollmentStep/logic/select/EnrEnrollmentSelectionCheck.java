/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.collections15.CollectionUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

import java.util.*;

/**
 * @author oleyba
 * @since 7/16/15
 */
public class EnrEnrollmentSelectionCheck
{
    private final EnrSelectionGroup selectionGroup;

    private final List<String> errors = new ArrayList<>(4);
    private final List<IEnrSelectionPrevEnrollmentFact> extractsMarkEnrolledByOther;
    private final List<EnrSelectionItem> stepItemsAvailable;
    private final List<EnrSelectionItem> stepItemsMarkEnrolledByThis;
    private final List<EnrSelectionItem> stepItemsMarkEnrolledByOther;
    private final List<EnrSelectionItem> stepItemsMarkEnrolledByThisOrOtherAndAvailable;
    private final int planBalance;
    private final int entrantBalance;

    public EnrEnrollmentSelectionCheck(final EnrSelectionGroup selectionGroup, final Predicate<EnrSelectionItem> available)
    {
        this.selectionGroup = selectionGroup;

        final Collection<EnrSelectionItem> allItems = this.getSelectionGroup().getItemMap().values();

        this.extractsMarkEnrolledByOther = new ArrayList<>(this.getSelectionGroup().getCompetitionExtractUsedByOtherGroups(input -> input.getStepItem().isShouldBeEnrolled()));

        this.stepItemsAvailable = new ArrayList<>(Collections2.filter(allItems, available));

        // даже те, кого было нельзя
        this.stepItemsMarkEnrolledByThis = new ArrayList<>(Collections2.filter(allItems, input -> input.getStepItem().isShouldBeEnrolled()));

        // даже те, кого было нельзя
        this.stepItemsMarkEnrolledByOther = new ArrayList<>(this.getSelectionGroup().getAllowdStepItemsUsedByOtherGroups(input -> input.getStepItem().isShouldBeEnrolled()));

        final Set<EnrSelectionItem> s = new HashSet<>();
        s.addAll(this.stepItemsMarkEnrolledByThis);
        s.addAll(this.stepItemsMarkEnrolledByOther);

        // только допущенные
        this.stepItemsMarkEnrolledByThisOrOtherAndAvailable = new ArrayList<>(Collections2.filter(s, stepItemsAvailable::contains));

        this.planBalance =
            (
                this.getSelectionGroup().getCompetitionPlan()
                    - this.getSelectionGroup().getCompetitionExtractSize()
                    + this.extractsMarkEnrolledByOther.size()
                    - this.stepItemsMarkEnrolledByThis.size()
            );

        this.entrantBalance = (
            this.stepItemsAvailable.size()
                - this.stepItemsMarkEnrolledByThisOrOtherAndAvailable.size()
        );


        // messages

        // план
        if (this.planBalance < 0) {
            this.errors.add("Превышен план приема.");
        }

        // остатки места, но есть абитуриентны
        if (this.planBalance > 0 && this.entrantBalance > 0) {
            this.errors.add("Остались нераспределенные места.");
        }

        // рекомендован, но не доступен для рекомендации
        {
            final Collection<EnrSelectionItem> nonAvailableEnrollment = CollectionUtils.subtract(this.stepItemsMarkEnrolledByThis, this.stepItemsAvailable);
            for (final EnrSelectionItem w: nonAvailableEnrollment) {
                this.errors.add("Абитуриент выбран к зачислению с нарушением правил выбора: " + w.getEntrantFio());
            }
        }

        // рекомендованные параллельщики
        for (final EnrSelectionItem w: this.stepItemsMarkEnrolledByThis) {
            if (!w.getRequestedCompetition().isParallel()) { continue; }

            boolean existsNonParallel = false;
            if (!existsNonParallel) {
                for (final IEnrSelectionItemPrevEnrollmentInfo info: w.getAddEnrollList()) {
                    if (info.getRequestedCompetition().isParallel()) { continue; }
                    if (info.isCancelled()) { continue; }
                    existsNonParallel = true;
                    break;
                }
            }
            if (!existsNonParallel) {
                for (final EnrSelectionGroup otherGroup: selectionGroup.getOtherStepItemGroups()) {
                    final IEnrSelectionPrevEnrollmentFact otherExtract = otherGroup.getCompetitionExtractMap().get(w.getEntrant());
                    if (null == otherExtract) { continue; }
                    if (otherExtract.getRequestedCompetition().isParallel()) { continue; }
                    existsNonParallel = true;
                    break;
                }
            }
            if (!existsNonParallel) {
                for (final EnrSelectionGroup otherGroup: selectionGroup.getOtherStepItemGroups()) {
                    final EnrSelectionItem otherItem = otherGroup.getItemMap().get(w.getEntrant());
                    if (null == otherItem) { continue; }
                    if (otherItem.getRequestedCompetition().isParallel()) { continue; }
                    if (!otherItem.getStepItem().isShouldBeEnrolled()) { continue; }
                    existsNonParallel = true;
                    break;
                }
            }

            if (!existsNonParallel) {
                this.errors.add("Абитуриент выбран к зачислению на параллельное обучение при отсутствии рекомендации/зачисления на основное: " + w.getEntrantFio());
            }
        }

        // повторно рекомендованные
        {
            final Collection<EnrSelectionItem> duplicateEnrollment = CollectionUtils.intersection(this.stepItemsMarkEnrolledByThis, this.stepItemsMarkEnrolledByOther);
            for (final EnrSelectionItem w: duplicateEnrollment) {
                if (w.getRequestedCompetition().isParallel()) {
                    this.errors.add("Абитуриент выбран к зачислению более одного раза (на параллельное обучение): " + w.getEntrantFio());
                } else {
                    this.errors.add("Абитуриент выбран к зачислению более одного раза: " + w.getEntrantFio());
                }
            }
        }

        // пропущен
        if (this.planBalance <= 0) {
            final Set<EnrSelectionItem> set = new HashSet<>(this.stepItemsAvailable);
            set.removeAll(this.stepItemsMarkEnrolledByThis);

            // убираем всех, кто выбран на лучший приоритет в другие конкурсы
            for (final Iterator<EnrSelectionItem> it = set.iterator(); it.hasNext(); ) {
                final EnrSelectionItem item = it.next();
                boolean enrolledOther = false;
                final Collection<EnrSelectionItem> selectionItemsOtherGroup = getContext().getStepItemWrappers4OtherGroups(item.getEntrant(), selectionGroup);
                for (final EnrSelectionItem otherItem: selectionItemsOtherGroup) {
                    if (item.getRequestedCompetition().isParallel() != otherItem.getRequestedCompetition().isParallel()) { continue; }
                    if (!otherItem.getStepItem().isShouldBeEnrolled()) { continue; }
                    if (otherItem.getRequestedCompetition().getPriority() < item.getRequestedCompetition().getPriority()) {
                        enrolledOther = true;
                        break;
                    }
                }
                if (enrolledOther) {
                    it.remove();
                }
            }

            // убираем всех, у которых нет выбранных после него
            {
                for (final Iterator<EnrSelectionItem> it = set.iterator(); it.hasNext(); ) {
                    final EnrSelectionItem w = it.next();

                    final Iterator<EnrSelectionItem> tmp = allItems.iterator();
                    while (tmp.hasNext()) {
                        final EnrSelectionItem tmpItem = tmp.next();
                        if (w.equals(tmpItem)) { break; }
                    }

                    boolean error = false;
                    while (tmp.hasNext()) {
                        final EnrSelectionItem tmpItem = tmp.next();
                        if (tmpItem.getStepItem().isShouldBeEnrolled()) {
                            error = true;
                            break;
                        }
                    }

                    if (!error) {
                        it.remove();
                    }
                }
            }

            // остальных показываем
            for (final EnrSelectionItem w: set) {
                this.errors.add("Пропущен: " + w.getEntrantFio());
            }
        }

        // В конкурсе с нулевым или отрицательным числом свободных мест
        // еще есть подходящие для выбора абитуриенты
        // с абсолютной позицией (рангом)
        // как у отмеченного к зачислению с наибольшей абсолютной позицией (рангом).
        if (this.planBalance <= 0) {
            final Set<EnrSelectionItem> set = new HashSet<>(this.stepItemsAvailable);
            set.removeAll(this.stepItemsMarkEnrolledByThis);

            // убираем всех, кто выбран в другие конкурсы с более высоким приоритетом
            for (final Iterator<EnrSelectionItem> it = set.iterator(); it.hasNext(); ) {
                final EnrSelectionItem item = it.next();
                boolean enrolledOther = false;
                final Collection<EnrSelectionItem> selectionItemsOtherGroup = getContext().getStepItemWrappers4OtherGroups(item.getEntrant(), selectionGroup);
                for (final EnrSelectionItem otherItem: selectionItemsOtherGroup) {
                    if (item.getRequestedCompetition().isParallel() != otherItem.getRequestedCompetition().isParallel()) { continue; }
                    if (!otherItem.getStepItem().isShouldBeEnrolled()) { continue; }
                    if (otherItem.getRequestedCompetition().getPriority() < item.getRequestedCompetition().getPriority()) {
                        enrolledOther = true;
                        break;
                    }
                }
                if (enrolledOther) {
                    it.remove();
                }
            }

            int rank = stepItemsMarkEnrolledByThis.isEmpty() ? -1 : Collections.max(CollectionUtils.collect(stepItemsMarkEnrolledByThis, enrSelectionItem -> enrSelectionItem.getRatingItem().getPositionAbsolute()));
            for (EnrSelectionItem item : set) {
                if (item.getRatingItem().getPositionAbsolute() == rank) {
                    this.errors.add("Полупроходная позиция: " + item.getEntrantFio());
                }
            }
        }
    }

    public void crossCheck(EnrEnrollmentSelectionCheck other) {
        // зачислен с нарушением приоритетов
        /* абитуриент зачислен в текущем конкурсе (А) с приоритетом N
           абитуриент не зачислен в конкурсе (B) с приоритетом M<N, с тем же признаком «Параллельно»
           в конкурсе В одновременно:
             не осталось нераспределенных мест
             есть абитуриент ниже по рейтингу и он зачислен, либо в конкурсе В остались свободные места. */

        if (other.planBalance > 0 && other.entrantBalance > 0) {
            return;
        }

        for (final EnrSelectionItem a: this.stepItemsMarkEnrolledByThis) {
            boolean error = false;
            boolean passedWithHigherPriority = false;
            
            int pN = a.getRequestedCompetition().getPriority();
            int pM = 0;

            for (Map.Entry<EnrEntrant, EnrSelectionItem> e : other.selectionGroup.getItemMap().entrySet()) {
                EnrSelectionItem b = e.getValue();
                if (passedWithHigherPriority && b.getStepItem().isShouldBeEnrolled()) {
                    error = true;
                }

                if (a.getEntrant().equals(b.getEntrant())
                    && a.getRequestedCompetition().isParallel() == b.getRequestedCompetition().isParallel()
                    && a.getRequestedCompetition().getPriority() > b.getRequestedCompetition().getPriority()
                    && other.stepItemsAvailable.contains(b)) {
                    passedWithHigherPriority = true;
                    pM = b.getRequestedCompetition().getPriority();
                }
            }

            if (passedWithHigherPriority && other.planBalance > 0) {
                error = true;
            }

            if (error) {
                this.errors.add(String.format("Абитуриент %s зачислен с нарушением приоритетов (%s при возможности зачисления на %s).", a.getEntrantFio(), pN, pM));
            }
        }
    }

    // accessors

    public EnrSelectionGroup getSelectionGroup() { return this.selectionGroup; }
    public EnrSelectionContext getContext() { return getSelectionGroup().getContext(); }

    public int getEntrantBalance()
    {
        return entrantBalance;
    }

    public List<String> getErrors()
    {
        return errors;
    }

    public List<IEnrSelectionPrevEnrollmentFact> getExtractsMarkEnrolledByOther()
    {
        return extractsMarkEnrolledByOther;
    }

    public int getPlanBalance()
    {
        return planBalance;
    }

    public List<EnrSelectionItem> getStepItemsAvailable()
    {
        return stepItemsAvailable;
    }

    public List<EnrSelectionItem> getStepItemsMarkEnrolledByOther()
    {
        return stepItemsMarkEnrolledByOther;
    }

    public List<EnrSelectionItem> getStepItemsMarkEnrolledByThis()
    {
        return stepItemsMarkEnrolledByThis;
    }

    public List<EnrSelectionItem> getStepItemsMarkEnrolledByThisOrOtherAndAvailable()
    {
        return stepItemsMarkEnrolledByThisOrOtherAndAvailable;
    }
}
