/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemPubUI;
import ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.EnrEnrollmentOrderParagraphPrintFormTypeManager;
import ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.AddEdit.EnrEnrollmentOrderParagraphPrintFormTypeAddEdit;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType;

/**
 * @author azhebko
 * @since 18.07.2014
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true)})
public class EnrEnrollmentOrderParagraphPrintFormTypePubUI extends BaseCatalogItemPubUI
{
    private String _parallelTitle;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        String parallelPropertyKey;
        Boolean parallel = ((EnrEnrollmentOrderParagraphPrintFormType) getCatalogItem()).getParallel();
        if (parallel == null) parallelPropertyKey = "parallelDS.id.all";
        else parallelPropertyKey = parallel ? "parallelDS.id.parallel" : "parallelDS.id.notParallel";

        _parallelTitle = EnrEnrollmentOrderParagraphPrintFormTypeManager.instance().getProperty(parallelPropertyKey);
    }

    @Override
    public void onClickEditItem()
    {
        getActivationBuilder().asRegion(EnrEnrollmentOrderParagraphPrintFormTypeAddEdit.class)
            .parameter(IUIPresenter.PUBLISHER_ID, getCatalogItem().getId())
            .activate();
    }

    public void onClickDownloadTemplate()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(getCatalogItem().getTitle() + ".rtf").document(CommonBaseUtil.getTemplateContent((EnrEnrollmentOrderParagraphPrintFormType) getCatalogItem())), false);
    }

    public String getParallelTitle(){ return _parallelTitle; }
}