package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип вступительного испытания"
 * Имя сущности : enrExamType
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrExamTypeCodes
{
    /** Константа кода (code) элемента : Обычное (title) */
    String USUAL = "1";
    /** Константа кода (code) элемента : Профильной направленности (title) */
    String PROFILE = "2";
    /** Константа кода (code) элемента : Творческой и (или) профессиональной направленности (title) */
    String CREATIVE_PROFESSIONAL = "3";

    Set<String> CODES = ImmutableSet.of(USUAL, PROFILE, CREATIVE_PROFESSIONAL);
}
