package ru.tandemservice.unienr14.ws.campaignInfo;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
@WebService
public class EnrCampaignInfoService
{
    public EnrCampaignInfoEnvironmentNode getEnrollmentEnvironment(@WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        return IEnrCampaignInfoServiceDao.INSTANCE.get().getEnrollmentEnvironmentData(enrollmentCampaignTitle);
    }
}
