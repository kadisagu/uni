package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.ui.BasicAddEdit.EnrCatalogOrdersBasicAddEdit;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.ui.ReasonAddEdit.EnrCatalogOrdersReasonAddEdit;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/**
 * Причина приказа по абитуриентам
 */
public class EnrOrderReason extends EnrOrderReasonGen implements IDynamicCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() { return EnrCatalogOrdersReasonAddEdit.class.getSimpleName(); }
        };
    }

}