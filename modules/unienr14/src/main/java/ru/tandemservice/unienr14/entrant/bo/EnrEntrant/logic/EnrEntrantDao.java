/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonDocumentRoleRel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.rating.entity.*;
import ru.tandemservice.unienr14.rating.entity.gen.EnrChosenEntranceExamFormGen;
import ru.tandemservice.unienr14.rating.entity.gen.EnrChosenEntranceExamGen;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/10/13
 */
public class EnrEntrantDao extends UniBaseDao implements IEnrEntrantDao
{
    /**
     * При добавлении/редактировании заявления абитуриента и при добавлении/изменении направления или направлений из
     * заявления, API функция должна захватить лок NAME-enrollmentCampaignId до конца транзакции.
     * Это обеспечит уровень изоляции SERIALIZABLE для EntrantRequest и RequestedEnrollmentDirection в рамках приемной
     * кампании, на котором основаны все ограничения приемной кампании.
     * Hint: проверки на эти ограничения требуют отсутствия фантомных вставок
     *
     * @param session сессия
     * @param entrant абитуриент
     */
    public static void lockEnrollmentCampaign(Session session, EnrEntrant entrant)
    {
        NamedSyncInTransactionCheckLocker.register(session, "enrollmentCampaignLock-" + entrant.getEnrollmentCampaign().getId());
        lockEntrant(session, entrant);
    }

    /**
     * При изменении данных абитуриента, для которых не требуется лок на всю приемную кампанию
     *
     * @param session сессия
     * @param entrant абитуриент
     */
    public static void lockEntrant(Session session, EnrEntrant entrant)
    {
        NamedSyncInTransactionCheckLocker.register(session, "entrantLock-" + entrant.getId());
    }

    @Override
    public String getUniqueEntrantNumber(EnrEntrant entrant)
    {
        return INumberQueueDAO.instance.get().getNextNumber(EnrEntrantManager.instance().entrantNumberGenerationRule(), entrant);
    }

    @Override
    public void deleteEntrant(EnrEntrant entrant)
    {
        if (existsEntity(EnrEntrantRequest.class, EnrEntrantRequest.L_ENTRANT, entrant)) {
            throw new ApplicationException("Нельзя удалять абитуриента с заявлениями.");
        }

        for (EnrExamPassDiscipline discipline : getList(EnrExamPassDiscipline.class, EnrExamPassDiscipline.entrant(), entrant)) {
            if (discipline.getExamGroup() != null) {
                throw new ApplicationException("Нельзя удалять абитуриента, если его дисциплины для сдачи включены в экзам. группу.");
            }
        }

        // обнуляем ссылку в онлайн-абитуриенте, если необходимо
        // todo заюзать cascade="orphan"
        EnrOnlineEntrant onlineEntrant = get(EnrOnlineEntrant.class, EnrOnlineEntrant.entrant().s(), entrant);
        if (onlineEntrant != null)
        {
            onlineEntrant.setEntrant(null);
            getSession().update(onlineEntrant);
        }

        // идиотский фикс по задаче DEV-5132
        new DQLDeleteBuilder(EnrEntrantBenefitProof.class)
        .where(in(
            "id",
            new DQLSelectBuilder()
            .fromEntity(EnrEntrantBenefitProof.class, "p").column("p.id")
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(IEnrEntrantBenefitStatement.class, "s")
                .where(eq(property("s.id"), property("p", EnrEntrantBenefitProof.benefitStatement())))
                .buildQuery()
            ))
            .buildQuery()
        ))
        .createStatement(getSession()).execute();
        getSession().flush();

        // удаляем абитуриента через апи функцию удаления категории персоны
        PersonManager.instance().dao().deletePersonRole(entrant.getId());
    }

    @Override
    public void doChangeEntrantArchival(EnrEntrant entrant)
    {
        Session session = getSession();

        if (entrant.isArchival())
        {
            entrant.setArchival(false);
        } else
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
            builder.column(property("rc", EnrRequestedCompetition.state()));
            builder.distinct();
            builder.where(eq(property("rc", EnrRequestedCompetition.request().entrant().id()), value(entrant.getId())));
            builder.where(notIn(property("rc", EnrRequestedCompetition.state().code()), Lists.newArrayList(EnrEntrantStateCodes.EXAMS_PASSED, EnrEntrantStateCodes.OUT_OF_COMPETITION, EnrEntrantStateCodes.ACTIVE)));
            builder.top(1);
            builder.order(property("rc", EnrRequestedCompetition.state().priority()));

            EnrEntrantState state = builder.createStatement(session).uniqueResult();

            if (state != null)
            {
                throw new ApplicationException("Нельзя списать абитуриента в архив в состоянии «" + state.getTitle() + "».");
            }
            entrant.setArchival(true);
        }
        session.update(entrant);
    }

    @Override
    public void doNormalizePriorityPerEntrantAndRequestType(EnrEntrant entrant, EnrRequestType requestType)
    {
        getSession().flush();
        CommonManager.instance().commonPriorityDao().doNormalizePriority(
            EnrRequestedCompetition.class, EnrRequestedCompetition.P_PRIORITY,
            EnrRequestedCompetition.request().entrant(), entrant,
            EnrRequestedCompetition.request().takeAwayDocument(), Boolean.FALSE,
            EnrRequestedCompetition.request().type(), requestType
        );

        List<EnrRequestedCompetition> requestedCompetitions = getList(
            new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "c")
            .where(eq(property("c", EnrRequestedCompetition.request().entrant()), value(entrant)))
            .where(eq(property("c", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
            .where(eq(property("c", EnrRequestedCompetition.request().type()), value(requestType))));

        for (EnrRequestedCompetition rcg : requestedCompetitions) {
            CommonManager.instance().commonPriorityDao().doNormalizePriority(
                EnrRequestedProgram.class, EnrRequestedProgram.P_PRIORITY,
                EnrRequestedProgram.requestedCompetition(), rcg
            );
        }
    }

    @Override
    public void doNormalizePriorityPerEntrant(EnrEntrant entrant)
    {
        for (EnrRequestType type : getList(EnrRequestType.class))
            doNormalizePriorityPerEntrantAndRequestType(entrant, type);
    }

    @Override
    public void saveChosenExams(EnrEntrant entrant, Map<EnrRequestedCompetition, List<IChosenExamWrapper>> chosenExamMap, EnrInternalExamReason internalExamReason)
    {
        Set<Long> savedChosenExamIds = new HashSet<>();
        Set<Long> savedChosenPassFormIds = new HashSet<>();
        Set<Long> savedOlympiadSources = new HashSet<>();

        // todo optimize it if you can

        EnrExamPassForm passFormOlympiad = getCatalogItem(EnrExamPassForm.class, EnrExamPassFormCodes.OLYMPIAD);

        for (Map.Entry<EnrRequestedCompetition, List<IChosenExamWrapper>> entry : chosenExamMap.entrySet()) {
            EnrRequestedCompetition requestedCompetition = entry.getKey();
            //            EnrCompetition competition = requestedCompetition.getCompetition();

            for (IChosenExamWrapper wrapper : entry.getValue()) {

                EnrChosenEntranceExam chosenExam = getByNaturalId(new EnrChosenEntranceExamGen.NaturalId(requestedCompetition, wrapper.getDiscipline()));
                if (null == chosenExam)
                    chosenExam = new EnrChosenEntranceExam(requestedCompetition, wrapper.getDiscipline());
                chosenExam.setActualExam(wrapper.getActualExam());
                saveOrUpdate(chosenExam);
                savedChosenExamIds.add(chosenExam.getId());

                for (EnrExamPassForm passForm : wrapper.getSelectedPassForms()) {
                    EnrChosenEntranceExamForm chosenPassForm = getByNaturalId(new EnrChosenEntranceExamFormGen.NaturalId(chosenExam, passForm));
                    if (null == chosenPassForm)
                        chosenPassForm = new EnrChosenEntranceExamForm(chosenExam, passForm);
                    saveOrUpdate(chosenPassForm);
                    savedChosenPassFormIds.add(chosenPassForm.getId());
                }

                if (null != wrapper.getDiplomaCategory()) {
                    if (null == wrapper.getDiploma()) { throw new IllegalStateException(/*внутренний косяк формы*/); }

                    EnrChosenEntranceExamForm chosenPassForm = getByNaturalId(new EnrChosenEntranceExamFormGen.NaturalId(chosenExam, passFormOlympiad));
                    if (null == chosenPassForm)
                        chosenPassForm = new EnrChosenEntranceExamForm(chosenExam, passFormOlympiad);
                    saveOrUpdate(chosenPassForm);
                    savedChosenPassFormIds.add(chosenPassForm.getId());

                    List<EnrEntrantMarkSourceBenefit> sourceList = getList(EnrEntrantMarkSourceBenefit.class, EnrEntrantMarkSourceBenefit.chosenEntranceExamForm(), chosenPassForm);
                    EnrEntrantMarkSourceBenefit source = sourceList != null && !sourceList.isEmpty() ? sourceList.get(0) : new EnrEntrantMarkSourceBenefit(); // если есть, берем первый, остальные будут удалены
                    source.setChosenEntranceExamForm(chosenPassForm);
                    source.setBenefitCategory(wrapper.getDiplomaCategory());
                    source.setMainProof(wrapper.getDiploma());
                    saveOrUpdate(source);

                    boolean proofExists = false;
                    for (EnrEntrantBenefitProof proof : getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement(), source)) {
                        if (source.getMainProof().equals(proof.getDocument())) {
                            proofExists = true;
                        } else {
                            delete(proof);
                        }
                    }

                    if (!proofExists) {
                        EnrEntrantBenefitProof proof = new EnrEntrantBenefitProof(source, source.getMainProof());
                        save(proof);
                    }

                    savedOlympiadSources.add(source.getId());
                }
            }
        }

        for (EnrEntrantMarkSourceBenefit source : getList(EnrEntrantMarkSourceBenefit.class, EnrEntrantMarkSourceBenefit.entrant(), entrant)) {
            if (!savedOlympiadSources.contains(source.getId())) {
                for (EnrEntrantBenefitProof proof : getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement(), source)) {
                    delete(proof);
                }
                delete(source);
            }
        }

        for (EnrChosenEntranceExamForm passForm : getList(EnrChosenEntranceExamForm.class, EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant(), entrant)) {
            if (passForm.getChosenEntranceExam().getRequestedCompetition().getRequest().isTakeAwayDocument())
                continue;
            if (!savedChosenPassFormIds.contains(passForm.getId()))
                delete(passForm);
        }

        for (EnrChosenEntranceExam exam : getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().request().entrant(), entrant)) {
            if (exam.getRequestedCompetition().getRequest().isTakeAwayDocument())
                continue;
            if (!savedChosenExamIds.contains(exam.getId()))
                delete(exam);
        }

        Set<EnrEntrantRequest> requestList = new HashSet<>(CollectionUtils.collect(chosenExamMap.keySet(), enrRequestedCompetition -> enrRequestedCompetition.getRequest()));

        for (EnrEntrantRequest request: requestList)
        {
            request.setInternalExamReason(internalExamReason);
            update(request);
        }
    }

    @Override
    public EnrEntrant findSuitableEntrant(final EnrEnrollmentCampaign campaign, Person person) {
        return new DQLSelectBuilder()
        .fromEntity(EnrEntrant.class, "e")
        .where(eq(property(EnrEntrant.enrollmentCampaign().fromAlias("e")), value(campaign)))
        .where(eq(property(EnrEntrant.person().fromAlias("e")), value(person)))
        .order(property(EnrEntrant.id().fromAlias("e")))
        .createStatement(getSession())
        .setMaxResults(1)
        .uniqueResult();
    }

    @Override
    public Long doSaveEntrantWizardData(EnrEntrant entrant, AddressBase regAddressDTO, AddressBase factAddressDTO)
    {
        // зы. session.flush здесь художественно расставленны потому что PersonRole.getPrincipal был написан каким-то психом

        final Session session = getSession();
        final Person person = entrant.getPerson();
        final IdentityCard identityCard = person.getIdentityCard();

        if (null != regAddressDTO) {
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(identityCard, regAddressDTO, IdentityCard.L_ADDRESS);
        }
        if (null != factAddressDTO) {
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(person, factAddressDTO, Person.L_ADDRESS);
        }

        if (null == identityCard.getId()) {
            identityCard.setPhoto(new DatabaseFile());
            session.save(identityCard.getPhoto()); //сохраняем фотку
        }

        session.saveOrUpdate(identityCard); //сохраняем удостоверение личности
        session.saveOrUpdate(person.getContactData());
        session.saveOrUpdate(person); //cохраняем персону
        session.flush();

        identityCard.setPerson(person);
        session.saveOrUpdate(identityCard); //сохраняем персону в удостоверении личности
        session.flush();

        lock("entrant.person="+person.getId());

        if (null == entrant.getId()) {
            EnrEntrant tmp = findSuitableEntrant(entrant.getEnrollmentCampaign(), person);
            if (null != tmp) {
                // TODO: tmp.update(entrant) - только то, что на форме
                entrant = tmp;
            } else {
                entrant.setPersonalNumber(getUniqueEntrantNumber(entrant));
                entrant.setPerson(person);
                entrant.setPrincipal((Principal)PersonManager.instance().dao().createPrincipalContext(entrant).getPrincipal());
                PersonManager.instance().dao().savePersonRole(entrant, person);
            }
        }
        session.saveOrUpdate(entrant);
        session.flush();

        return entrant.getId();
    }

    @Override
    public boolean isEntranceExamIncorrect(EnrEntrant entrant)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrRatingItem.class, "r").column("r.id")
        .where(eq(property(EnrRatingItem.statusEntranceExamsCorrect().fromAlias("r")), value(Boolean.FALSE)))
        .where(eq(property(EnrRatingItem.requestedCompetition().request().takeAwayDocument().fromAlias("r")), value(Boolean.FALSE)))
        .where(eq(property(EnrRatingItem.entrant().fromAlias("r")), value(entrant)));
        return existsEntity(dql.buildQuery());
    }

    @Override
    public boolean isProfileNotSelected(EnrEntrant entrant)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrRequestedCompetition.class, "r").column("r.id")
        .where(eq(property(EnrRequestedCompetition.request().entrant().fromAlias("r")), value(entrant)))
        .where(eq(property(EnrRequestedCompetition.competition().allowProgramPriorities().fromAlias("r")), value(Boolean.TRUE)))
        .where(ne(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)))

        .joinEntity("r", DQLJoinType.left, EnrRequestedProgram.class, "selected", eq(property(EnrRequestedProgram.requestedCompetition().fromAlias("selected")), property("r")))
        .where(isNull(property(EnrRequestedProgram.priority().fromAlias("selected"))))
        ;

        return existsEntity(dql.buildQuery());
    }

    @Override
    public boolean isOlympiadNotUsed(EnrEntrant entrant)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrOlympiadDiploma.class, "d").column("d.id")
                .where(exists(new DQLSelectBuilder()
                        .fromEntity(PersonDocumentRoleRel.class, "rr")
                        .where(eq(property("rr", PersonDocumentRoleRel.document().id()), property("d.id")))
                        .where(eq(property("rr", PersonDocumentRoleRel.role().id()), value(entrant.getId())))
                        .buildQuery()))

        // DEV-4832: TODO: возможно, это надо убрать
        .joinEntity("d", DQLJoinType.left, EnrEntrantMarkSourceBenefit.class, "src", eq(property(EnrEntrantMarkSourceBenefit.mainProof().fromAlias("src")), property("d")))
        .where(isNull(property("src.id")))

        .joinEntity("d", DQLJoinType.left, EnrEntrantBenefitProof.class, "proof", eq(property(EnrEntrantBenefitProof.document().fromAlias("proof")), property("d")))
        .where(isNull(property("proof.id")))
        ;

        return existsEntity(dql.buildQuery());
    }

    @Override
    public boolean isExamListIncorrect(EnrEntrant entrant)
    {
        DQLSelectBuilder allExamPassDisc = new DQLSelectBuilder()
        .fromEntity(EnrExamPassDiscipline.class, "d").column("d.id")
        .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("d")), value(entrant)))
        .where(eq(property(EnrExamPassDiscipline.retake().fromAlias("d")), value(Boolean.FALSE)))
        ;

        DQLSelectBuilder correctExamPassDisc = new DQLSelectBuilder()
        .fromEntity(EnrChosenEntranceExamForm.class, "ch")
        .joinPath(DQLJoinType.inner, EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("ch"), "e")
        .joinPath(DQLJoinType.inner, EnrChosenEntranceExam.requestedCompetition().request().entrant().fromAlias("e"), "entrant")
        .joinEntity("ch", DQLJoinType.inner, EnrExamPassDiscipline.class, "d", and(
            eq(property(EnrExamPassDiscipline.discipline().fromAlias("d")), property(EnrChosenEntranceExam.discipline().fromAlias("e"))),
            eq(property(EnrExamPassDiscipline.passForm().fromAlias("d")), property(EnrChosenEntranceExamForm.passForm().fromAlias("ch"))),
            eq(property(EnrExamPassDiscipline.entrant().fromAlias("d")), property("entrant")),
            eq(property(EnrExamPassDiscipline.retake().fromAlias("d")), value(Boolean.FALSE))
        ))

        .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().fromAlias("ch")), value(entrant)))
        .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().takeAwayDocument().fromAlias("ch")), value(Boolean.FALSE)))

        .column("d.id")
        .predicate(DQLPredicateType.distinct)
        ;

        DQLSelectBuilder chosenInternal = new DQLSelectBuilder()
        .fromEntity(EnrChosenEntranceExamForm.class, "ch")
        .column(property(EnrChosenEntranceExamForm.chosenEntranceExam().discipline().id().fromAlias("ch")))
        .column(property(EnrChosenEntranceExamForm.passForm().id().fromAlias("ch")))
        .predicate(DQLPredicateType.distinct)
        .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().fromAlias("ch")), value(entrant)))
        .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().takeAwayDocument().fromAlias("ch")), value(Boolean.FALSE)))
        .where(eq(property(EnrChosenEntranceExamForm.passForm().internal().fromAlias("ch")), value(Boolean.TRUE)))
        ;

        int allExamPassDiscCount = getCount(allExamPassDisc);
        int correctExamPassDiscCount = getCount(correctExamPassDisc);
        int chosenInternalCount = getCount(chosenInternal);

        return allExamPassDiscCount != correctExamPassDiscCount || allExamPassDiscCount != chosenInternalCount;
    }

    @Override
    public void updatePreparationData(EnrEntrant entrant, List<EnrAccessCourse> accessCourseList, List<EnrAccessDepartment> accessDepartmentList)
    {
        // EnrAccessCourse
        List<EnrAccessCourse> accessCourseForSave = new ArrayList<>(accessCourseList);
        for (EnrEntrantAccessCourse item : getList(EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.L_ENTRANT, entrant))
        {
            if (!accessCourseList.contains(item.getCourse()))
                delete(item);
            else
                accessCourseForSave.remove(item.getCourse());
        }
        for (EnrAccessCourse accessCourse : accessCourseForSave)
        {
            EnrEntrantAccessCourse item = new EnrEntrantAccessCourse();
            item.setEntrant(entrant);
            item.setCourse(accessCourse);
            save(item);
        }

        // EnrAccessDepartment
        List<EnrAccessDepartment> accessDepartmentForSave = new ArrayList<>(accessDepartmentList);
        for (EnrEntrantAccessDepartment item : getList(EnrEntrantAccessDepartment.class, EnrEntrantAccessDepartment.L_ENTRANT, entrant))
        {
            if (!accessDepartmentList.contains(item.getAccessDepartment()))
                delete(item);
            else
                accessDepartmentForSave.remove(item.getAccessDepartment());
        }
        for (EnrAccessDepartment accessDepartment : accessDepartmentForSave)
        {
            EnrEntrantAccessDepartment item = new EnrEntrantAccessDepartment();
            item.setEntrant(entrant);
            item.setAccessDepartment(accessDepartment);
            save(item);
        }
    }


    @Override
    public void saveSelectedProfilesAndAllPriorities(EnrEntrant entrant, EnrRequestType requestType, Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap)
    {
        Set<Long> updated = new HashSet<>();

        // будем отражать приоритеты относительно нуля, а потом обратно

        List<Long> programIdsToDelete = new ArrayList<>();
        List<IEntity> objectsToUpdate = new ArrayList<>();

        for (EnrRequestedCompetition direction : priorityMap.keySet()) {
            direction.setPriority(-direction.getPriority());
            objectsToUpdate.add(direction);
        }

        // теперь отразим и сохраним
        Map<EnrRequestedProgram.NaturalId, EnrRequestedProgram> programMap = new HashMap<>();
        for (Map.Entry<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> entry : priorityMap.entrySet()) {
            for (Map.Entry<EnrProgramSetItem, Integer> subEntry : entry.getValue().entrySet()) {
                EnrRequestedProgram requestedProgram = getByNaturalId(new EnrRequestedProgram.NaturalId(entry.getKey(), subEntry.getKey()));
                if (null == requestedProgram) {
                    requestedProgram = new EnrRequestedProgram(entry.getKey(), subEntry.getKey());
                }
                Integer priority = subEntry.getValue();
                programMap.put(new EnrRequestedProgram.NaturalId(entry.getKey(), subEntry.getKey()), requestedProgram);

                if (priority != null)
                {
                    requestedProgram.setPriority(-priority);
                    objectsToUpdate.add(requestedProgram);
                }
                else if (null != requestedProgram.getId())
                    programIdsToDelete.add(requestedProgram.getId());
            }
        }

        for (IEntity entity : objectsToUpdate) {
            if (null != entity.getId() && !programIdsToDelete.contains(entity.getId()))
            {
                saveOrUpdate(entity);
                updated.add(entity.getId());
            }
        }

        getSession().flush();

        for (Long programId : programIdsToDelete) {
            delete(programId);
        }
        getSession().flush();

        // удалим профили, которые не выбрали в этот раз

        List<EnrRequestedProgram> requestedPrograms = getList(
            new DQLSelectBuilder()
            .fromEntity(EnrRequestedProgram.class, "p")
            .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().entrant()), value(entrant)))
            .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().takeAwayDocument()), value(Boolean.FALSE)))
            .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().type()), value(requestType))));

        for (EnrRequestedProgram program : requestedPrograms) {
            if (!programIdsToDelete.contains(program.getId()) && !updated.contains(program.getId())) {
                delete(program);
            }
        }

        getSession().flush();

        // теперь обратно

        for (Map.Entry<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> entry : priorityMap.entrySet()) {
            for (Map.Entry<EnrProgramSetItem, Integer> subEntry : entry.getValue().entrySet()) {
                EnrRequestedProgram requestedProgram = programMap.get(new EnrRequestedProgram.NaturalId(entry.getKey(), subEntry.getKey()));

                if (!programIdsToDelete.contains(requestedProgram.getId()) && requestedProgram.getPriority() != 0)
                {
                    requestedProgram.setPriority(-requestedProgram.getPriority());
                    saveOrUpdate(requestedProgram);
                    updated.add(requestedProgram.getId());
                }
            }
        }

        for (EnrRequestedCompetition competition : priorityMap.keySet()) {
            competition.setPriority(-competition.getPriority());
            update(competition);
        }

        getSession().flush();

        // нормализуем приоритеты
        doNormalizePriorityPerEntrantAndRequestType(entrant, requestType);
    }

    @Override
    public void doSaveBenefitNoExamsData(EnrRequestedCompetitionNoExams requestedCompetition, EnrProgramSetItem programSetItem, EnrBenefitCategory benefitCategory, List<IEnrEntrantBenefitProofDocument> documentList) {
        requestedCompetition.setProgramSetItem(programSetItem);
        doSaveBenefitData(requestedCompetition, benefitCategory, documentList);
    }

    @Override
    public void doSaveBenefitMarkSourceData(EnrEntrantMarkSourceBenefit markSourceBenefit, EnrBenefitCategory benefitCategory, IEnrEntrantBenefitProofDocument mainProof) {
        markSourceBenefit.setMainProof(mainProof);
        doSaveBenefitData(markSourceBenefit, benefitCategory, Collections.singletonList(mainProof));
    }

    @Override
    public void doSaveBenefitData(final IEnrEntrantBenefitStatement statement, EnrBenefitCategory benefitCategory, List<IEnrEntrantBenefitProofDocument> documentList)
    {
        Session session = lock("IEnrEntrantBenefitStatement.id=" + statement.getId());
        statement.setBenefitCategory(benefitCategory);
        session.saveOrUpdate(statement);

        new MergeAction.SessionMergeAction<IEnrEntrantBenefitProofDocument, EnrEntrantBenefitProof>() {
            @Override protected IEnrEntrantBenefitProofDocument key(final EnrEntrantBenefitProof source) { return source.getDocument(); }
            @Override protected void fill(final EnrEntrantBenefitProof target, final EnrEntrantBenefitProof source) { target.update(source, false);  }
            @Override protected EnrEntrantBenefitProof buildRow(final EnrEntrantBenefitProof source) { return source; }
        }.merge(
            getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement().s(), statement),
            org.apache.commons.collections15.CollectionUtils.collect(documentList, document -> new EnrEntrantBenefitProof(statement, document))
        );
    }

    @Override
    public List<IEnrEntrantBenefitProofDocument> getBenefitStatementProofDocs(IEnrEntrantBenefitStatement statement)
    {
        return new DQLSelectBuilder()
        .fromEntity(EnrEntrantBenefitProof.class, "bp")
        .column(property("bp", EnrEntrantBenefitProof.document()))
        .where(eq(property("bp", EnrEntrantBenefitProof.benefitStatement()), value(statement)))
        .order(property("bp", EnrEntrantBenefitProof.document().documentType().title()))
        .order(property("bp", EnrEntrantBenefitProof.document().seria()))
        .order(property("bp", EnrEntrantBenefitProof.document().number()))
        .createStatement(getSession()).list();
    }

    @Override
    public void updateEntrantData(EnrEntrant entrant, List<EnrSourceInfoAboutUniversity> sourceInfoList) {

        update(entrant);

        List<EnrSourceInfoAboutUniversity> sourceInfoAboutUniversityForSave = new ArrayList<>(sourceInfoList);
        for (EnrEntrantInfoAboutUniversity item : getList(EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.L_ENTRANT, entrant))
        {
            if (!sourceInfoList.contains(item.getSourceInfo()))
                getSession().delete(item);
            else
                sourceInfoAboutUniversityForSave.remove(item.getSourceInfo());
        }
        for (EnrSourceInfoAboutUniversity sourceInfoAboutUniversity : sourceInfoAboutUniversityForSave)
        {
            EnrEntrantInfoAboutUniversity item = new EnrEntrantInfoAboutUniversity();
            item.setEntrant(entrant);
            item.setSourceInfo(sourceInfoAboutUniversity);
            getSession().save(item);
        }
    }

    @Override
    public void saveOrUpdateCustomState(EnrEntrantCustomState enrEntrantCustomState)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantCustomState.class, "st")
                .column("st.id")
                .where(eq(property("st", EnrEntrantCustomState.entrant()), value(enrEntrantCustomState.getEntrant())))
                .where(eq(property("st", EnrEntrantCustomState.customState()), value(enrEntrantCustomState.getCustomState())));

        if (null != enrEntrantCustomState.getId())
            builder.where(ne(property("st"), value(enrEntrantCustomState)));

        FilterUtils.applyIntersectPeriodFilterNullSafe(
                builder, "st",  EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE,
                enrEntrantCustomState.getBeginDate(), enrEntrantCustomState.getEndDate());

        if (existsEntity(builder.buildQuery()))
            throw new ApplicationException("Данный дополнительный статус «" + enrEntrantCustomState.getCustomState().getTitle() + "» пересекается по срокам действия с уже имеющимся аналогичным статусом абитуриента.");

        saveOrUpdate(enrEntrantCustomState);
    }

    @Override
    public Map<EnrEntrant, List<EnrEntrantCustomState>> getActiveCustomStatesMap(Collection<EnrEntrant> entrants, Date date)
    {
        Map<EnrEntrant, List<EnrEntrantCustomState>> result = SafeMap.get(ArrayList.class);
        if (null == entrants || entrants.isEmpty()) return result;

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantCustomState.class, "st").column("st")
                .where(in(property("st", EnrEntrantCustomState.entrant()), entrants))
                .order(property("st", EnrEntrantCustomState.customState().shortTitle()))
                .order(property("st", EnrEntrantCustomState.beginDate()))
                .order(property("st", EnrEntrantCustomState.endDate()));

        FilterUtils.applyInPeriodFilterNullSafe(builder, "st", EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE, date);

        for (EnrEntrantCustomState customState : builder.createStatement(getSession()).<EnrEntrantCustomState>list())
        {
            result.get(customState.getEntrant()).add(customState);
        }
        return result;
    }

    private final DaoCacheFacade.CacheEntryDefinition<Long, Boolean> CACHE_IS_ORG_UNIT_FORMING_OR_ENR_ORG_UNIT = new DaoCacheFacade.CacheEntryDefinition<Long, Boolean>("EnrOrgUnitDao.isShowEnrOrgUnitTab", 128, false) {
        @Override public void fill(final Map<Long, Boolean> cache, final Collection<Long> ids) {

            for (final Long id: ids) {
                cache.put(id, this.isOrgUnitFormingOrEnrOrgUnit(id));
            }
        }

        private Boolean isOrgUnitFormingOrEnrOrgUnit(Long id)
        {
            return existsEntity(EnrOrgUnit.class, EnrOrgUnit.institutionOrgUnit().orgUnit().s(), id) ||
                    existsEntity(
                            OrgUnitToKindRelation.class,
                            OrgUnitToKindRelation.L_ORG_UNIT, id,
                            OrgUnitToKindRelation.orgUnitKind().code().s(), Arrays.asList(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)
                    );
        }
    };

    @Override
    public boolean isOrgUnitFormingOrEnrOrgUnit(OrgUnit orgUnit)
    {
        return DaoCacheFacade.getEntry(CACHE_IS_ORG_UNIT_FORMING_OR_ENR_ORG_UNIT).getRecords(Collections.singleton(orgUnit.getId())).get(orgUnit.getId());
    }

    @Override
    public List<IEntity> getSecLocalEntitiesForEntrant(Long entrantId)
    {
        return getList(new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "ou")
                .where(exists(new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "c")
                        .where(or(
                                eq(property("c", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit()), property("ou")),
                                eq(property("c", EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit()), property("ou"))))
                        .where(eq(property("c", EnrRequestedCompetition.request().entrant().id()), value(entrantId)))
                        .buildQuery()))
        );
    }

    @Override
    public void saveOrUpdateAchievement(EnrEntrantAchievement achievement)
    {
        achievement.setRatingMarkAsLong(achievement.getType().isMarked() ?
            Math.min(achievement.getMarkAsLong(), achievement.getType().getAchievementMarkAsLong())
            : achievement.getType().getAchievementMarkAsLong());
        saveOrUpdate(achievement);
        EnrEntrantDaemonBean.DAEMON.registerAfterCompleteWakeUp(getSession());
    }
}
