/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.ui.BasicAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.EnrCatalogOrdersManager;

/**
 * @author oleyba
 * @since 7/7/14
 */
@Configuration
public class EnrCatalogOrdersBasicAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("orderTypeDS", EnrCatalogOrdersManager.instance().orderTypeDSHandler()))
            .create();
    }
}