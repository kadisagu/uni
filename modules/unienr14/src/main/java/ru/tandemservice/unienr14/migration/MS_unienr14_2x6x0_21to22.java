package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_21to22 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrReportEntrantRegistrationJournal

		// таблица сущности переименована
		{
			// переименовать таблицу
            if (tool.tableExists("enr14_rep_entrant_reg_journal"))
			tool.renameTable("enr14_rep_entrant_reg_journal", "enr14_rep_entr_reg_journal_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrReportForEnrollmentCommission

		// таблица сущности переименована
		{
			// переименовать таблицу
            if (tool.tableExists("enr_14_rep_for_enr_commission"))
			tool.renameTable("enr_14_rep_for_enr_commission", "enr14_rep_for_enr_comm_t");

		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrReportDailyRequestsCumulative

        // таблица сущности переименована
        {
            // переименовать таблицу
            if (tool.tableExists("enr_14_rep_daily_req_cumul_t"))
            tool.renameTable("enr_14_rep_daily_req_cumul_t", "enr14_rep_daily_req_cumul_t");

        }


    }
}