package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 17.04.13
 */
@Configuration
public class EnrCampaignDisciplineList extends BusinessComponentManager {

    public static final String CAMP_DISCIPLINE_LIST_DS = "campDisciplineListDS";
    public static final String ENR_STATE_EXAM_SUBJECT_DS = "enrStateExamSubjectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENR_STATE_EXAM_SUBJECT_DS, enrStateExamSubjectComboDSHandler()))
                .addDataSource(searchListDS(CAMP_DISCIPLINE_LIST_DS, campDisciplineListDS(), campDisciplineListDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrStateExamSubjectComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrStateExamSubject.class);
    }

    @Bean
    public ColumnListExtPoint campDisciplineListDS()
    {
        return this.columnListExtPointBuilder(CAMP_DISCIPLINE_LIST_DS)
                .addColumn(textColumn("title", EnrDiscipline.title()))
                .addColumn(checkboxColumn("used").controlInHeader(false).listener("onChangeSubject"))
                .addColumn(blockColumn("stateExamSubject","stateExamSubjectBlockColumn"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> campDisciplineListDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrDiscipline.class)
                .order(EnrDiscipline.title(), OrderDirection.asc)
                .pageable(false);
    }
}
