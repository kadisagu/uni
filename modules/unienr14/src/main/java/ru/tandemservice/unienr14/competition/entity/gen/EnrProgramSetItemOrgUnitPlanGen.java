package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План приема по ОП в наборе ОП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetItemOrgUnitPlanGen extends EntityBase
 implements IEnrollmentStudentSettings, INaturalIdentifiable<EnrProgramSetItemOrgUnitPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan";
    public static final String ENTITY_NAME = "enrProgramSetItemOrgUnitPlan";
    public static final int VERSION_HASH = -159207611;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET_ITEM = "programSetItem";
    public static final String L_PROGRAM_SET_ORG_UNIT = "programSetOrgUnit";
    public static final String P_PLAN = "plan";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_PROGRAM_SET = "programSet";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_PROGRAM = "program";

    private EnrProgramSetItem _programSetItem;     // Образовательная программа в наборе
    private EnrProgramSetOrgUnit _programSetOrgUnit;     // Подразделение, ведущее прием
    private int _plan;     // План приема
    private EducationOrgUnit _educationOrgUnit;     // НПП
    private EnrProgramSetBase _programSet;     // Набор ОП
    private EnrOrgUnit _orgUnit;     // Подразделение, ведущее прием
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private EduProgramHigherProf _program;     // ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Образовательная программа в наборе. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetItem getProgramSetItem()
    {
        return _programSetItem;
    }

    /**
     * @param programSetItem Образовательная программа в наборе. Свойство не может быть null.
     */
    public void setProgramSetItem(EnrProgramSetItem programSetItem)
    {
        dirty(_programSetItem, programSetItem);
        _programSetItem = programSetItem;
    }

    /**
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetOrgUnit getProgramSetOrgUnit()
    {
        return _programSetOrgUnit;
    }

    /**
     * @param programSetOrgUnit Подразделение, ведущее прием. Свойство не может быть null.
     */
    public void setProgramSetOrgUnit(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        dirty(_programSetOrgUnit, programSetOrgUnit);
        _programSetOrgUnit = programSetOrgUnit;
    }

    /**
     * @return План приема. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan План приема. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    /**
     * Временное поле. Констрейнта на совпадение formativeOrgUnit нет, потому что в НПП это поле можно менять.
     *
     * @return НПП.
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit НПП.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Набор ОП.
     *
     * Это формула "programSetOrgUnit.programSet".
     */
    public EnrProgramSetBase getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор ОП.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgramSet(EnrProgramSetBase programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Подразделение, ведущее прием.
     *
     * Это формула "programSetOrgUnit.orgUnit".
     */
    public EnrOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение, ведущее прием.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Формирующее подразделение.
     *
     * Это формула "programSetOrgUnit.formativeOrgUnit".
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return ОП.
     *
     * Это формула "programSetItem.program".
     */
    public EduProgramHigherProf getProgram()
    {
        return _program;
    }

    /**
     * @param program ОП.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgram(EduProgramHigherProf program)
    {
        dirty(_program, program);
        _program = program;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramSetItemOrgUnitPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSetItem(((EnrProgramSetItemOrgUnitPlan)another).getProgramSetItem());
                setProgramSetOrgUnit(((EnrProgramSetItemOrgUnitPlan)another).getProgramSetOrgUnit());
            }
            setPlan(((EnrProgramSetItemOrgUnitPlan)another).getPlan());
            setEducationOrgUnit(((EnrProgramSetItemOrgUnitPlan)another).getEducationOrgUnit());
            setProgramSet(((EnrProgramSetItemOrgUnitPlan)another).getProgramSet());
            setOrgUnit(((EnrProgramSetItemOrgUnitPlan)another).getOrgUnit());
            setFormativeOrgUnit(((EnrProgramSetItemOrgUnitPlan)another).getFormativeOrgUnit());
            setProgram(((EnrProgramSetItemOrgUnitPlan)another).getProgram());
        }
    }

    public INaturalId<EnrProgramSetItemOrgUnitPlanGen> getNaturalId()
    {
        return new NaturalId(getProgramSetItem(), getProgramSetOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EnrProgramSetItemOrgUnitPlanGen>
    {
        private static final String PROXY_NAME = "EnrProgramSetItemOrgUnitPlanNaturalProxy";

        private Long _programSetItem;
        private Long _programSetOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrProgramSetItem programSetItem, EnrProgramSetOrgUnit programSetOrgUnit)
        {
            _programSetItem = ((IEntity) programSetItem).getId();
            _programSetOrgUnit = ((IEntity) programSetOrgUnit).getId();
        }

        public Long getProgramSetItem()
        {
            return _programSetItem;
        }

        public void setProgramSetItem(Long programSetItem)
        {
            _programSetItem = programSetItem;
        }

        public Long getProgramSetOrgUnit()
        {
            return _programSetOrgUnit;
        }

        public void setProgramSetOrgUnit(Long programSetOrgUnit)
        {
            _programSetOrgUnit = programSetOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrProgramSetItemOrgUnitPlanGen.NaturalId) ) return false;

            EnrProgramSetItemOrgUnitPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSetItem(), that.getProgramSetItem()) ) return false;
            if( !equals(getProgramSetOrgUnit(), that.getProgramSetOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSetItem());
            result = hashCode(result, getProgramSetOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSetItem());
            sb.append("/");
            sb.append(getProgramSetOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetItemOrgUnitPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetItemOrgUnitPlan.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetItemOrgUnitPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSetItem":
                    return obj.getProgramSetItem();
                case "programSetOrgUnit":
                    return obj.getProgramSetOrgUnit();
                case "plan":
                    return obj.getPlan();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "programSet":
                    return obj.getProgramSet();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "program":
                    return obj.getProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSetItem":
                    obj.setProgramSetItem((EnrProgramSetItem) value);
                    return;
                case "programSetOrgUnit":
                    obj.setProgramSetOrgUnit((EnrProgramSetOrgUnit) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "programSet":
                    obj.setProgramSet((EnrProgramSetBase) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((EnrOrgUnit) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "program":
                    obj.setProgram((EduProgramHigherProf) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSetItem":
                        return true;
                case "programSetOrgUnit":
                        return true;
                case "plan":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "programSet":
                        return true;
                case "orgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "program":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSetItem":
                    return true;
                case "programSetOrgUnit":
                    return true;
                case "plan":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "programSet":
                    return true;
                case "orgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "program":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSetItem":
                    return EnrProgramSetItem.class;
                case "programSetOrgUnit":
                    return EnrProgramSetOrgUnit.class;
                case "plan":
                    return Integer.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "programSet":
                    return EnrProgramSetBase.class;
                case "orgUnit":
                    return EnrOrgUnit.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "program":
                    return EduProgramHigherProf.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetItemOrgUnitPlan> _dslPath = new Path<EnrProgramSetItemOrgUnitPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetItemOrgUnitPlan");
    }
            

    /**
     * @return Образовательная программа в наборе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgramSetItem()
     */
    public static EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
    {
        return _dslPath.programSetItem();
    }

    /**
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgramSetOrgUnit()
     */
    public static EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
    {
        return _dslPath.programSetOrgUnit();
    }

    /**
     * @return План приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    /**
     * Временное поле. Констрейнта на совпадение formativeOrgUnit нет, потому что в НПП это поле можно менять.
     *
     * @return НПП.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Набор ОП.
     *
     * Это формула "programSetOrgUnit.programSet".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgramSet()
     */
    public static EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Подразделение, ведущее прием.
     *
     * Это формула "programSetOrgUnit.orgUnit".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Формирующее подразделение.
     *
     * Это формула "programSetOrgUnit.formativeOrgUnit".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return ОП.
     *
     * Это формула "programSetItem.program".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgram()
     */
    public static EduProgramHigherProf.Path<EduProgramHigherProf> program()
    {
        return _dslPath.program();
    }

    public static class Path<E extends EnrProgramSetItemOrgUnitPlan> extends EntityPath<E>
    {
        private EnrProgramSetItem.Path<EnrProgramSetItem> _programSetItem;
        private EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> _programSetOrgUnit;
        private PropertyPath<Integer> _plan;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private EnrProgramSetBase.Path<EnrProgramSetBase> _programSet;
        private EnrOrgUnit.Path<EnrOrgUnit> _orgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EduProgramHigherProf.Path<EduProgramHigherProf> _program;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Образовательная программа в наборе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgramSetItem()
     */
        public EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
        {
            if(_programSetItem == null )
                _programSetItem = new EnrProgramSetItem.Path<EnrProgramSetItem>(L_PROGRAM_SET_ITEM, this);
            return _programSetItem;
        }

    /**
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgramSetOrgUnit()
     */
        public EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
        {
            if(_programSetOrgUnit == null )
                _programSetOrgUnit = new EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit>(L_PROGRAM_SET_ORG_UNIT, this);
            return _programSetOrgUnit;
        }

    /**
     * @return План приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrProgramSetItemOrgUnitPlanGen.P_PLAN, this);
            return _plan;
        }

    /**
     * Временное поле. Констрейнта на совпадение formativeOrgUnit нет, потому что в НПП это поле можно менять.
     *
     * @return НПП.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Набор ОП.
     *
     * Это формула "programSetOrgUnit.programSet".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgramSet()
     */
        public EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
        {
            if(_programSet == null )
                _programSet = new EnrProgramSetBase.Path<EnrProgramSetBase>(L_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Подразделение, ведущее прием.
     *
     * Это формула "programSetOrgUnit.orgUnit".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Формирующее подразделение.
     *
     * Это формула "programSetOrgUnit.formativeOrgUnit".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return ОП.
     *
     * Это формула "programSetItem.program".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan#getProgram()
     */
        public EduProgramHigherProf.Path<EduProgramHigherProf> program()
        {
            if(_program == null )
                _program = new EduProgramHigherProf.Path<EduProgramHigherProf>(L_PROGRAM, this);
            return _program;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetItemOrgUnitPlan.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetItemOrgUnitPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
