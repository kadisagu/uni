/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd.logic;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd.EnrReportEnrollmentCommissionMeetingProtocolAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEnrollmentCommissionMeetingProtocol;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 06.08.2014
 */
public class EnrReportEnrollmentCommissionMeetingProtocolDao extends UniBaseDao implements IEnrReportEnrollmentCommissionMeetingProtocolDao {
    private static final String T1 = "T1";
    private static final String T2 = "T2";
    private static final String T3 = "T3";
    private static final String T4 = "T4";
    private static final String T5 = "T5";
    private static final String T6 = "T6";
    private static final String T7 = "T7";

    private static final Integer T4_SUM_COLUMN_NUMBER = 9;

    @Override
    public Long createReport(EnrReportEnrollmentCommissionMeetingProtocolAddUI model)
    {
        EnrReportEnrollmentCommissionMeetingProtocol report = new EnrReportEnrollmentCommissionMeetingProtocol();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEnrollmentCommissionMeetingProtocol.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEnrollmentCommissionMeetingProtocol.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEnrollmentCommissionMeetingProtocol.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEnrollmentCommissionMeetingProtocol.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEnrollmentCommissionMeetingProtocol.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEnrollmentCommissionMeetingProtocol.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEnrollmentCommissionMeetingProtocol.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEnrollmentCommissionMeetingProtocol.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEnrollmentCommissionMeetingProtocol.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive()) {
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        }
        if (model.isDoNotShowCommissionResolution())
            report.setEnrollmentCommissionResolution("Не заполнять");
        else report.setEnrollmentCommissionResolution("Заполнять");


        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model, EnrScriptItemCodes.REPORT_ENROLLMENT_COMMISSION_MEETING_PROTOCOL, true));
        content.setFilename("EnrReportEnrollmentCommissionMeetingProtocol.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }



    public byte[] buildReport(EnrReportEnrollmentCommissionMeetingProtocolAddUI model, String templateCatalogCode)
    {
        return buildReport(model, templateCatalogCode, false);
    }

    protected byte[] buildReport(final EnrReportEnrollmentCommissionMeetingProtocolAddUI model, String templateCatalogCode, boolean competitionReportForm)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, templateCatalogCode);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // Выбираем абитуриентов, подходящих под параметры отчета

        final Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();

        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, filterAddon, true, competitionReportForm);

        requestedCompDQL.order(property(EnrRatingItem.position().fromAlias(requestedCompDQL.rating())));
        requestedCompDQL.column(requestedCompDQL.rating());

        Set<Long> entrantsIds = Sets.newHashSet();

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(getSession()).<EnrRatingItem>list()) {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
            nonEmptyBlocks.add(ratingItem.getCompetition().getProgramSetOrgUnit());
            if(competitionReportForm)
                entrantsIds.add(ratingItem.getEntrant().getId());
        }

        //{entrant.id -> {requestedCompetition.id -> priority}}
        final Map<Long, Map<Long, Integer>> recommendedReqCompMap = Maps.newHashMap();

        if(competitionReportForm)
        {
            BatchUtils.execute(entrantsIds, 100, elements -> {
                DQLSelectBuilder rcSubBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
                rcSubBuilder.column(property("rc", EnrRequestedCompetition.request().entrant().id()), "entrantId");
                rcSubBuilder.column(DQLFunctions.max(property("rc", EnrRequestedCompetition.priority())), "minPriority");
                rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.request().entrant().enrollmentCampaign().id()), value(model.getEnrollmentCampaign().getId())));
                rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.RECOMMENDED)));
                rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
                rcSubBuilder.where(in(property("rc", EnrRequestedCompetition.request().entrant().id()), elements));
                rcSubBuilder.group(property("rc", EnrRequestedCompetition.request().entrant().id()));


                DQLSelectBuilder rcBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "c");
                rcBuilder.column(property("c", EnrRequestedCompetition.id()));
                rcBuilder.column(property("c", EnrRequestedCompetition.priority()));
                rcBuilder.column(property("c", EnrRequestedCompetition.request().entrant().id()));
                rcBuilder.joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().fromAlias("c"), "entrant");
                rcBuilder.joinDataSource("c", DQLJoinType.inner, rcSubBuilder.buildQuery(), "ds", eq(property("ds.entrantId"), property("entrant", EnrEntrant.id())));
                rcBuilder.where(eq(property("ds.minPriority"), property("c", EnrRequestedCompetition.priority())));

                for (Object[] row : rcBuilder.createStatement(getSession()).<Object[]>list()) {
                    Long competitionId = (Long) row[0];
                    Integer priority = (Integer) row[1];
                    Long entrantId = (Long) row[2];

                    recommendedReqCompMap.put(entrantId, Maps.<Long, Integer>newHashMap());
                    recommendedReqCompMap.get(entrantId).put(competitionId, priority);
                }
            });
        }

        // Еще нам нужны будут баллы по ВИ абитуриентов

        Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap = SafeMap.get(HashMap.class);

        EnrEntrantDQLSelectBuilder examDQL = prepareEntrantDQL(model, filterAddon, false, competitionReportForm);
        examDQL.fromEntity(EnrChosenEntranceExam.class, "exam");
        examDQL.where(eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("exam")), property(examDQL.reqComp())));
        examDQL.column("exam");
        for (EnrChosenEntranceExam exam : examDQL.createStatement(getSession()).<EnrChosenEntranceExam>list()) {
            if (exam.getActualExam() != null && exam.getMarkAsDouble() != null) {
                entrantExamMap.get(exam.getRequestedCompetition()).put(exam.getActualExam(), exam);
            }
        }

        Map<EnrRequestedCompetition, EnrOrder> orderMap = new HashMap<>();
        EnrEntrantDQLSelectBuilder orderDQL = prepareEntrantDQL(model, filterAddon, false, competitionReportForm);
        orderDQL.fromEntity(EnrEnrollmentExtract.class, "e");
        orderDQL.where(eq(property(EnrEnrollmentExtract.entity().fromAlias("e")), property(orderDQL.reqComp())));
        orderDQL.column(orderDQL.reqComp());
        orderDQL.column(property(EnrEnrollmentExtract.paragraph().order().fromAlias("e")));
        for (Object[] row : orderDQL.createStatement(getSession()).<Object[]>list()) {
            orderMap.put((EnrRequestedCompetition) row[0], (EnrOrder) row[1]);
        }

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов (не путать с массивом выбранных конкурсов, о котором уже было выше написано).

        Collection<EnrCompetition> competitions = filterAddon.getFilteredList();

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition()) {
            competitions = entrantMap.keySet();
        }

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList) {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // Сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());
        Collections.sort(blockList, (o1, o2) -> {
            int result;
            result = - Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
            if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
            if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
            return result;
        });

        // подгрузим наборы ВИ наших конкурсов

        Set<Long> examSetVariantIds = new HashSet<>();
        for (EnrCompetition competition : competitions) {
            examSetVariantIds.add(competition.getExamSetVariant().getId());
        }

        Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(examSetVariantIds);

        // подгрузим образовательные программы наборов

        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(blockList, EnrProgramSetOrgUnit::getProgramSet);

        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets)) {
            programMap.get(item.getProgramSet()).add(item.getProgram());
        }
        for (EnrProgramSetBase programSetBase : programSets) {
            if (programSetBase instanceof EnrProgramSetSecondary) {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary)programSetBase).getProgram());
            }
        }

        // определим, какие данные выводить, исходя из параметров отчета

        boolean printMinisterial = true;
        boolean printContract = true;
        CompensationType compensationType = (CompensationType) filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        if (filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).isEnableCheckboxChecked() && null != compensationType) {
            printMinisterial = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            printContract = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        }

        // сразу заменим метку даты

        new RtfInjectModifier().put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);

        // берем из шаблона таблицы

        Map<String, RtfTable> headerTables = new HashMap<>();
        headerTables.put(EnrRequestTypeCodes.BS, UniRtfUtil.removeTable(document, "BS", true));
        headerTables.put(EnrRequestTypeCodes.MASTER, UniRtfUtil.removeTable(document, "MASTER", true));
        RtfTable higher = UniRtfUtil.removeTable(document, "HIGHER", true);
        headerTables.put(EnrRequestTypeCodes.HIGHER, higher);
        headerTables.put(EnrRequestTypeCodes.POSTGRADUATE, higher);
        RtfTable residency = UniRtfUtil.removeTable(document, "RESIDENCY", true);
        headerTables.put(EnrRequestTypeCodes.TRAINEESHIP, residency);
        headerTables.put(EnrRequestTypeCodes.INTERNSHIP, residency);
        headerTables.put(EnrRequestTypeCodes.SPO, UniRtfUtil.removeTable(document, "SPO", true));

        Map<String, RtfTable> contentTables = new HashMap<>();
        contentTables.put(T1, UniRtfUtil.removeTable(document, T1, false));
        contentTables.put(T2, UniRtfUtil.removeTable(document, T2, false));
        contentTables.put(T3, UniRtfUtil.removeTable(document, T3, false));
        contentTables.put(T4, UniRtfUtil.removeTable(document, T4, false));
        contentTables.put(T5, UniRtfUtil.removeTable(document, T5, false));
        contentTables.put(T6, UniRtfUtil.removeTable(document, T6, false));
        contentTables.put(T7, UniRtfUtil.removeTable(document, T7, false));

        // и чистим шаблон, чтобы не было ненужных переводов строк
        document.getElementList().clear();

        // выводим блоки отчета
        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList) {

            // сначала выводим заголовочную таблицу блока

            List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);

            String tableCode = programSetOrgUnit.getProgramSet().getRequestType().getCode();
            if (EnrRequestTypeCodes.HIGHER.equals(tableCode))
                switch (programSetOrgUnit.getProgramSet().getProgramKind().getCode())
                {
                    case EduProgramKindCodes.PROGRAMMA_INTERNATURY : tableCode = EnrRequestTypeCodes.INTERNSHIP; break;
                    case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_: tableCode = EnrRequestTypeCodes.POSTGRADUATE; break;
                    case EduProgramKindCodes.PROGRAMMA_ORDINATURY: tableCode = EnrRequestTypeCodes.TRAINEESHIP; break;
                }

            RtfTable headerTable = headerTables.get(tableCode).getClone();
            document.getElementList().add(headerTable);

            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
            boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);
            boolean skipProgramsRow = (model.isSkipProgramSetTitle() || model.isReplaceProgramSetTitle()) && programs.size() == 1;
            boolean skipProgramSetRow = model.isSkipProgramSetTitle();
            if (skipProgramsRow && headerTable.getRowList().size() >= 6) {
                headerTable.getRowList().remove(5);
            }
            if (skipProgramSetRow && headerTable.getRowList().size() >= 4) {
                headerTable.getRowList().remove(3);
            }

            List<EnrCompetition> enrCompetitions = blockMap.get(programSetOrgUnit);

            boolean hasRecommendedOther = false;
            if (competitionReportForm)
            {
                for (EnrCompetition competition : enrCompetitions)
                {
                    if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0)
                    {
                        // если прием по ЦП отдельно, делим списки по видам ЦП
                        for (EnrTargetAdmissionPlan taPlan : taPlans)
                        {
                            List<EnrRatingItem> entrantList = new ArrayList<>();
                            for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class))
                            {
                                if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA) entrant.getRequestedCompetition()).getTargetAdmissionKind()))
                                {
                                    entrantList.add(entrant);
                                }
                            }
                            if (model.isSkipEmptyCompetition() && entrantList.isEmpty())
                            {
                                continue;
                            }
                            if (entrantList != null)
                            {
                                for (EnrRatingItem entrant : entrantList)
                                {
                                    Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                                    if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                                    {
                                        hasRecommendedOther = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                        if (entrantList != null)
                        {
                            for (EnrRatingItem entrant : entrantList)
                            {
                                Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                                {
                                    hasRecommendedOther = true;
                                }
                            }
                        }
                    }
                }
            }

            fillHeaderData(document, programSetOrgUnit, programs, printMinisterial, printContract, printOP, competitionReportForm, enrCompetitions, hasRecommendedOther);

            // теперь списки по каждому конкурсу
            MutableInt ministerialRequestTotal = new MutableInt(0); MutableInt contractRequestTotal = new MutableInt(0);
            for (EnrCompetition competition : enrCompetitions) {

                IEnrExamSetDao.IExamSetSettings examSet = examSetContent.get(competition.getExamSetVariant().getId());

                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0) {
                    // если прием по ЦП отдельно, делим списки по видам ЦП
                    for (EnrTargetAdmissionPlan taPlan : taPlans) {
                        List<EnrRatingItem> entrantList = new ArrayList<>();
                        for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class)) {
                            if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA)entrant.getRequestedCompetition()).getTargetAdmissionKind())) {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty()) {
                            continue;
                        }
                        final String tableHeader = getTableHeader(competition, taPlan, entrantList.size(), competitionReportForm);
                        printContentTable(competition, entrantList, document, tableHeader,
                                ministerialRequestTotal, contractRequestTotal,
                                contentTables, examSet, entrantExamMap, orderMap,
                                model.isPrintEntrantNumberColumn(), recommendedReqCompMap, competitionReportForm, !model.isDoNotShowCommissionResolution());
                    }
                } else {
                    List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    final String tableHeader = getTableHeader(competition, null, entrantList.size(), competitionReportForm);
                    printContentTable(competition, entrantList, document, tableHeader,
                            ministerialRequestTotal, contractRequestTotal,
                            contentTables, examSet, entrantExamMap, orderMap, model.isPrintEntrantNumberColumn(),recommendedReqCompMap, competitionReportForm,
                            !model.isDoNotShowCommissionResolution());
                }
            }

            // выводим в заголовочную таблицу информацию по числу поданных заявлений

            //numberOfRequest - число заявлений: сумма на бюджетные конкурсы (сумма по всем табличкам бюджетных конкурсов), сумма по конкурсам по договору; если отчет строится только по бюджету - строки по договору не будет, если отчет по договору - строки по бюджету не будет; если КЦП 0 (план на бюджет не задан) - строки по бюджету не будет, если по договору план 0 (не задан) - строки по договору не будет
            //Подано заявлений:
            //    на бюджет (КЦП) – 101
            //    на места с оплатой стоимости обучения – 49

            RtfString requestTotal = new RtfString();
            if (printMinisterial) {
                requestTotal.append("    на бюджет (КЦП) – ").append(String.valueOf(ministerialRequestTotal.intValue()));
            }
            if (printContract) {
                if (requestTotal.toList().size() != 0) requestTotal.par();
                requestTotal.append("    на места с оплатой стоимости обучения – ").append(String.valueOf(contractRequestTotal.intValue()));
            }
            new RtfInjectModifier().put("requestsTotal", requestTotal).modify(document);

            if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1) {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
        }

        return RtfUtil.toByteArray(document);
    }

    private EnrEntrantDQLSelectBuilder prepareEntrantDQL(EnrReportEnrollmentCommissionMeetingProtocolAddUI model, EnrCompetitionFilterAddon filterAddon, boolean fetch, boolean competitionReportForm)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign())
                ;

        if (competitionReportForm) {
            requestedCompDQL.where(eq(property(requestedCompDQL.rating(), EnrRatingItem.statusRatingPositive()), value(Boolean.TRUE)));
        }

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL.where(in(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.state()), model.getReqCompStates()));

        if (model.getParallelSelector().isParallelOnly()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.getParallelSelector().isSkipParallel()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        return requestedCompDQL;
    }

    private List<EnrTargetAdmissionPlan> getTaPlans(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        return programSetOrgUnit.getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition() ?
                getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit, EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s())
                : null;
    }

    private String getTableHeader(EnrCompetition competition, EnrTargetAdmissionPlan taPlan, int size, boolean competitionReportForm)
    {
        StringBuilder header = new StringBuilder();
        header.append(competition.getType().getPrintTitle());
        if (null != taPlan) {
            header.append(" – ").append(taPlan.getEnrCampaignTAKind().getTitle());
        }
        header.append(" (заявлений – ").append(size);
        if(competitionReportForm && !EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(competition.getType().getCode()) && !EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(competition.getType().getCode()))
        {
            int plan = 0;
            if (null != taPlan)
            {
                EnrTargetAdmissionCompetitionPlan tACompPlan = getByNaturalId(new EnrTargetAdmissionCompetitionPlan.NaturalId(taPlan, competition));
                plan = tACompPlan == null? taPlan.getPlan() : tACompPlan.getPlan();
            }
            else
                plan = competition.getPlan();

            header.append(", число мест — ").append(plan);
        }
        header.append(")");
        return header.toString();
    }

    private void printContentTable(EnrCompetition competition, List<EnrRatingItem> entrantList, RtfDocument document, final String tableHeader,
                                   MutableInt ministerialRequestTotal, MutableInt contractRequestTotal,
                                   Map<String, RtfTable> contentTables,
                                   final IEnrExamSetDao.IExamSetSettings examSet,
                                   final Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap,
                                   final Map<EnrRequestedCompetition, EnrOrder> orderMap,
                                   final boolean printEntrantNumber,
                                   Map<Long, Map<Long, Integer>> recommendedReqCompMap, final boolean competitionReportForm,
                                   boolean showResolution)
    {
        final String tableName = chooseRtfTable(competition);
        RtfTable contentTable = contentTables.get(tableName).getClone();

        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(contentTable);


        Set<Person> entrants = new HashSet<>();

        for (EnrRatingItem ratingItem : entrantList) {
            entrants.add(ratingItem.getRequestedCompetition().getRequest().getEntrant().getPerson());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "lang")
                .where(in(property(PersonForeignLanguage.person().fromAlias("lang")), entrants))
                .column(property("lang"));

        List<PersonForeignLanguage> fromQueryLangs = getList(builder);
        Map<Person, Set<PersonForeignLanguage>> langsMap = SafeMap.get(HashSet.class);

        for (PersonForeignLanguage lang : fromQueryLangs) {
            langsMap.get(lang.getPerson()).add(lang);
        }

        final List<String[]> tableContent = new ArrayList<>();
        int number = 1;
        if (entrantList != null) {
            for (EnrRatingItem entrant : entrantList) {
                List<Double> marks = new ArrayList<>();
                for (IEnrExamSetDao.IExamSetElementSettings exam : examSet.getElementList()) {
                    EnrChosenEntranceExam chosenEntranceExam = entrantExamMap.get(entrant.getRequestedCompetition()).get(exam.getCgExam());
                    marks.add(chosenEntranceExam == null ? null : chosenEntranceExam.getMarkAsDouble());
                }

                boolean printStarForState = false;
                if(competitionReportForm )
                {
                    Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                    if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                    {
                        printStarForState = true;
                    }
                }

                tableContent.add(printEntrant(number++, entrant,
                                              tableName, marks,
                                              orderMap.get(entrant.getRequestedCompetition()),
                                              printEntrantNumber,
                                              competitionReportForm, printStarForState,
                                              langsMap.get(entrant.getRequestedCompetition().getRequest().getEntrant().getPerson()),
                                              showResolution));

                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    ministerialRequestTotal.increment();
                }
                if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    contractRequestTotal.increment();
                }
            }
        }

        if(tableContent.isEmpty())
            tableContent.add(new String[1]);

        new RtfTableModifier()
                .put(tableName, tableContent.toArray(new String[tableContent.size()][]))
                .put(tableName, new RtfRowIntercepterBase() {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex)
                    {
                        if (examSet.getElementList().isEmpty()) return;
                        if (!T2.equals(tableName) && !T4.equals(tableName) && !T6.equals(tableName) && !T7.equals(tableName)) return;

                        final int[] scales = new int[examSet.getElementList().size()];
                        Arrays.fill(scales, 1);

                        // разбиваем ячейку заголовка таблицы для названий оценок на нужное число элементов
                        RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), T4_SUM_COLUMN_NUMBER, (newCell, index) -> {
                            String content = examSet.getElementList().get(index).getElement().getShortTitle();
                            newCell.getElementList().addAll(new RtfString().append(IRtfData.QC).append(content).toList());
                        }, scales);
                        RtfUtil.splitRow(table.getRowList().get(currentRowIndex), T4_SUM_COLUMN_NUMBER, null, scales);
                    }
                    // todo сделать утильный класс
                    @Override public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                        List<IRtfElement> list = new ArrayList<>();
                        IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                        text.setRaw(true);
                        list.add(text);
                        return list;
                    }
                    @Override public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                        newRowList.get(0).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());
                    }
                })
                .modify(document);
    }

    private String[] printEntrant(int number,
                                  EnrRatingItem ratingItem,
                                  String tableName,
                                  List<Double> marks,
                                  EnrOrder enrOrder,
                                  boolean printEntrantNumber,
                                  final boolean competitionReportForm,
                                  boolean printStartForState,
                                  Set<PersonForeignLanguage> langs,
                                  boolean showResolution)
    {
        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();
        Long idEntrant = reqComp.getRequest().getEntrant().getId();

        List<String> row = new ArrayList<>();
        // номер
        row.add(String.valueOf(number));
        // Личный номер абитуриента
        if (printEntrantNumber)
            row.add(reqComp.getRequest().getEntrant().getPersonalNumber());
        // фио
        row.add(reqComp.getRequest().getEntrant().getFullFio());

        // Дата рождения
        row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(reqComp.getRequest().getIdentityCard().getBirthDate()));

        // Адрес регистрации
        row.add(reqComp.getRequest().getIdentityCard().getAddress() == null ? "Адрес не указан" : reqComp.getRequest().getIdentityCard().getAddress().getTitleWithFlat());

        // Документ об образовании
        row.add(reqComp.getRequest().getEduDocument().getDisplayableTitle());

        // Ин. язык
        row.add(UniStringUtils.join(langs, PersonForeignLanguage.language().title().s(), " \\line "));

        // сдан оригинал
        row.add(YesNoFormatter.INSTANCE.format(reqComp.isOriginalDocumentHandedIn()));

        // Согл. на зач.
        if (T3.equals(tableName) || T4.equals(tableName) || T1.equals(tableName) || T2.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
        {
            row.add(YesNoFormatter.INSTANCE.format(reqComp.isAccepted()));
        }

        // категория
        if (T1.equals(tableName) || T3.equals(tableName)) {
            if (reqComp instanceof IEnrEntrantBenefitStatement) {
                row.add(((IEnrEntrantBenefitStatement)reqComp).getBenefitCategory().getShortTitle());
            } else row.add("");
        }
        // ср. балл
        if (T5.equals(tableName)) {
            Double avgMark = reqComp.getRequest().getEduDocument().getAvgMarkAsDouble();
            row.add(avgMark == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(avgMark));
        }
        // сумма и результаты
        if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName)) {
            row.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ratingItem.getTotalMarkAsDouble()));
            for (Double mark : marks) {
                row.add(mark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
            }
            if (marks.isEmpty()) row.add(""); // иначе таблица поедет
        }

        if (T6.equals(tableName) || T7.equals(tableName)) {
            Double avgMark = reqComp.getRequest().getEduDocument().getAvgMarkAsDouble();
            row.add(avgMark == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(avgMark));
        }

        // Баллы за ИД
        NumberFormat format = new DecimalFormat("#0.##");
        row.add(format.format(ratingItem.getAchievementMarkAsLong()/1000.0));

        // Решение комиссии
        if (showResolution)
        {
            switch (reqComp.getState().getCode())
            {
                case EnrEntrantStateCodes.RECOMMENDED : row.add("Рекомендован"); break;
                case EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED :
                case EnrEntrantStateCodes.IN_ORDER : row.add("К зачислению"); break;
                case EnrEntrantStateCodes.ENROLLED : row.add("Зачислен"); break;
            }
        }

        return row.toArray(new String[row.size()]);
    }

    private void fillHeaderData(RtfDocument document, EnrProgramSetOrgUnit programSetOrgUnit, List<EduProgramProf> programs, boolean printMinisterial, boolean printContract, boolean printOP, boolean competitionReportForm, List<EnrCompetition> enrCompetitions, boolean hasRecommendedOther)
    {
        Collections.sort(programs, (o1, o2) -> o1.getTitleAndConditionsShort().compareTo(o2.getTitleAndConditionsShort()));

        String duration = null; boolean diffDuration = false;
        for (EduProgram program : programs) {
            if (null == duration) {
                duration = program.getDuration().getTitle();
            } else {
                diffDuration = diffDuration || !program.getDuration().getTitle().equals(duration);
            }
        }
        String programConditions = null; boolean diffConditions = false;
        for (EduProgramProf program : programs) {
            if (null == programConditions) {
                programConditions = StringUtils.trimToEmpty(program.getImplConditionsShort());
            } else {
                diffConditions = diffConditions || !StringUtils.trimToEmpty(program.getImplConditionsShort()).equals(programConditions);
            }
        }

        OrgUnit filial = programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
                .put("shortTitle", filial.getParent() == null ? "" : filial.getParent().getPrintTitle())
                .put("filial", filial.getPrintTitle())
                .put("formOrgUnit", programSetOrgUnit.getFormativeOrgUnit() == null || programSetOrgUnit.getFormativeOrgUnit().equals(filial) ? "" : programSetOrgUnit.getFormativeOrgUnit().getPrintTitle())
                .put("eduSubjectKind", programSetOrgUnit.getProgramSet().getProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_DATIVE).toLowerCase())
                .put("eduSubject", programSetOrgUnit.getProgramSet().getProgramSubject().getTitleWithCode())
                .put("programForm", programSetOrgUnit.getProgramSet().getProgramForm().getTitle())
                .put("duration", diffDuration ? "" : duration)
                .put("programTrait", diffConditions? "" : programConditions)
                .put("eduProgramKind", programSetOrgUnit.getProgramSet().getProgramKind().getShortTitle())
                .put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
                .put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getPrintTitle())
        ;

        RtfString eduProgramTitles = new RtfString();
        for (EduProgramProf program : programs) {
            if (eduProgramTitles.toList().size() > 0) eduProgramTitles.par();
            eduProgramTitles.append(program.getShortTitle() + " - " + program.getTitleAndConditionsShort());
        }
        modifier.put("eduProgram", eduProgramTitles);

        //numberOfPlaces - число мест для приема (для комбинации набор ОП, филиал); не печатаем те строчки, в которых план 0 (не задан); если план с квотой ЦП бьется на подквоты (более чем на одну), то выводить детализацию по видам ЦП; например, если есть КЦП и нет квоты особых прав и ЦП, то будет строка "Число мест на бюджет (КЦП) - 50"; если отчет строится только по бюджету - строки с планом по договору не будет, если отчет по договору - строк по бюджету не будет (включая расшифровку по квотам и видам ЦП)
        //Число мест на бюджет (КЦП) – 50, из них:
        //       квота лиц с особыми правами – 5
        //       квота целевого приема – 10
        //(из них по видам целевого приема: Южный фед. округ – 2, ФГУП Спецмнотаж – 3, МО Курганской обл. – 1, Тюменский район – 3)
        //Число мест с оплатой стоимости обучения – 100

        boolean printMinisterialPlan = printMinisterial && programSetOrgUnit.getMinisterialPlan() > 0;
        boolean printExclusivePlan = EnrRequestTypeCodes.BS.equals(programSetOrgUnit.getProgramSet().getRequestType().getCode());
        boolean printTaPlan = programSetOrgUnit.getTargetAdmPlan() > 0;
        boolean printContractPlan = printContract && programSetOrgUnit.getContractPlan() > 0;

        List<String> planLines = new ArrayList<>();
        if (printMinisterialPlan) {
            StringBuilder minPlan = new StringBuilder();
            minPlan.append("Число мест на бюджет (КЦП) – ").append(String.valueOf(programSetOrgUnit.getMinisterialPlan()));
            if (printExclusivePlan || printTaPlan) {
                minPlan.append(", из них:");
            }
            planLines.add(minPlan.toString());

            if (printExclusivePlan) {
                int plan;
                if(competitionReportForm)
                {
                    Collection<EnrCompetition> exclusiveCompetitions = Collections2.filter(enrCompetitions, input -> EnrCompetitionTypeCodes.EXCLUSIVE.equals(input.getType().getCode()));

                    plan = getSum(CommonBaseEntityUtil.<Integer>getPropertiesList(exclusiveCompetitions, EnrCompetition.plan()));
                }
                else
                    plan = programSetOrgUnit.getExclusivePlan();

                planLines.add("       квота лиц с особыми правами - " + String.valueOf(plan));
            }
            if (printTaPlan) {
                int plan;
                if(competitionReportForm)
                {
                    Collection<EnrCompetition> taCompetitions = Collections2.filter(enrCompetitions, input -> EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(input.getType().getCode()));

                    plan = getSum(CommonBaseEntityUtil.<Integer>getPropertiesList(taCompetitions, EnrCompetition.plan()));
                }
                else
                    plan = programSetOrgUnit.getTargetAdmPlan();

                planLines.add("       квота целевого приема – " + String.valueOf(plan));
                List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);
                if (taPlans != null && taPlans.size() > 0) {
                    planLines.add("(из них по видам целевого приема: " + UniStringUtils.join(taPlans, EnrTargetAdmissionPlan.planString().s(), ", ") + ")");
                }
            }
        }
        if (printContractPlan) {
            planLines.add("Число мест с оплатой стоимости обучения – " + String.valueOf(programSetOrgUnit.getContractPlan()));
        }

        RtfString plan = new RtfString();
        for (String planLine : planLines) {
            if (planLines.indexOf(planLine) > 0) plan.par();
            plan.append(planLine);
        }

        modifier.put("plan", plan);

        if(hasRecommendedOther)
        {
            modifier.put("hasRecommended", "* - рекомендован в рамках другого конкурсного списка по более высокому приоритету");
        }
        else
        {
            UniRtfUtil.deleteRowsWithLabels(document, Lists.newArrayList("hasRecommended"));
        }

        modifier.modify(document);

    }

    private int getSum(List<Integer> integerList)
    {
        if(integerList == null || integerList.isEmpty()) return 0;
        int sum = 0;
        for(Integer integer : integerList)
        {
            sum += integer;
        }
        return sum;
    }

//    private RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
//    {
//        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);
//
//        if (removeLabel) {
//            for (RtfRow row : table.getRowList()) {
//                for (RtfCell cell : row.getCellList()) {
//                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null) {
//                        UniRtfUtil.fillCell(cell, "");
//                    }
//                }
//            }
//        }
//
//        document.getElementList().remove(table);
//
//        return table;
//    }

    private String chooseRtfTable(EnrCompetition competition) {
        String requestTypeCode = competition.getRequestType().getCode();
        String compTypeCode = competition.getType().getCode();
        // Бакалавриат, специалитет:
        if (EnrRequestTypeCodes.BS.equals(requestTypeCode)) {
            // Без ВИ в рамках КЦП - "T1"
            if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compTypeCode)) return T1;
            // В рамках квоты лиц, имеющих особые права - "T2"
            if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(compTypeCode)) return T2;
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // Без ВИ по договору - T3
            if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compTypeCode)) return T3;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // Магистратура:
        if (EnrRequestTypeCodes.MASTER.equals(requestTypeCode)) {
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // Аспирантура:
        if ((EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) && EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind().getCode()))
                || EnrRequestTypeCodes.POSTGRADUATE.equals(requestTypeCode)) {
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode) || EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }

        // Ординатура, Интернатура
        if(EnrRequestTypeCodes.TRAINEESHIP.equals(requestTypeCode) || EnrRequestTypeCodes.INTERNSHIP.equals(requestTypeCode)
                || (EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) &&
                (EduProgramKindCodes.PROGRAMMA_ORDINATURY.equals(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind().getCode()) ||
                        EduProgramKindCodes.PROGRAMMA_INTERNATURY.equals(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind().getCode()))))
        {
            // Общий конкурс - T6
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode) || EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T6;
            // По договору - T7
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T7;
        }

        // СПО:
        if (EnrRequestTypeCodes.SPO.equals(requestTypeCode)) {
            // Общий конкурс - T5
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T5;
            // По договору - T5
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T5;
        }
        throw new IllegalArgumentException();
    }
}