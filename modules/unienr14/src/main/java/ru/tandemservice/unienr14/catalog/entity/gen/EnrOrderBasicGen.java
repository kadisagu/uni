package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание приказа по абитуриентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrderBasicGen extends EntityBase
 implements INaturalIdentifiable<EnrOrderBasicGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic";
    public static final String ENTITY_NAME = "enrOrderBasic";
    public static final int VERSION_HASH = 1533865888;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_ORDER_TYPE = "orderType";
    public static final String P_COMMENTABLE = "commentable";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EnrOrderType _orderType;     // Тип приказа по абитуриентам
    private boolean _commentable;     // Комментируемо
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderType getOrderType()
    {
        return _orderType;
    }

    /**
     * @param orderType Тип приказа по абитуриентам. Свойство не может быть null.
     */
    public void setOrderType(EnrOrderType orderType)
    {
        dirty(_orderType, orderType);
        _orderType = orderType;
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommentable()
    {
        return _commentable;
    }

    /**
     * @param commentable Комментируемо. Свойство не может быть null.
     */
    public void setCommentable(boolean commentable)
    {
        dirty(_commentable, commentable);
        _commentable = commentable;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOrderBasicGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrOrderBasic)another).getCode());
            }
            setOrderType(((EnrOrderBasic)another).getOrderType());
            setCommentable(((EnrOrderBasic)another).isCommentable());
            setTitle(((EnrOrderBasic)another).getTitle());
        }
    }

    public INaturalId<EnrOrderBasicGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrOrderBasicGen>
    {
        private static final String PROXY_NAME = "EnrOrderBasicNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrOrderBasicGen.NaturalId) ) return false;

            EnrOrderBasicGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrderBasicGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrderBasic.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrderBasic();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "orderType":
                    return obj.getOrderType();
                case "commentable":
                    return obj.isCommentable();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "orderType":
                    obj.setOrderType((EnrOrderType) value);
                    return;
                case "commentable":
                    obj.setCommentable((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "orderType":
                        return true;
                case "commentable":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "orderType":
                    return true;
                case "commentable":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "orderType":
                    return EnrOrderType.class;
                case "commentable":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrderBasic> _dslPath = new Path<EnrOrderBasic>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrderBasic");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#getOrderType()
     */
    public static EnrOrderType.Path<EnrOrderType> orderType()
    {
        return _dslPath.orderType();
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#isCommentable()
     */
    public static PropertyPath<Boolean> commentable()
    {
        return _dslPath.commentable();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrOrderBasic> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EnrOrderType.Path<EnrOrderType> _orderType;
        private PropertyPath<Boolean> _commentable;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrOrderBasicGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#getOrderType()
     */
        public EnrOrderType.Path<EnrOrderType> orderType()
        {
            if(_orderType == null )
                _orderType = new EnrOrderType.Path<EnrOrderType>(L_ORDER_TYPE, this);
            return _orderType;
        }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#isCommentable()
     */
        public PropertyPath<Boolean> commentable()
        {
            if(_commentable == null )
                _commentable = new PropertyPath<Boolean>(EnrOrderBasicGen.P_COMMENTABLE, this);
            return _commentable;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrOrderBasicGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrOrderBasic.class;
        }

        public String getEntityName()
        {
            return "enrOrderBasic";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
