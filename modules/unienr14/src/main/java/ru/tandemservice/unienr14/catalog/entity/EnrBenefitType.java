package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;

import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrBenefitTypeGen;

/**
 * Вид особого права при поступлении
 */
public class EnrBenefitType extends EnrBenefitTypeGen implements IDynamicCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name){
        return new EntityComboDataSourceHandler(name, EnrBenefitType.class)
                .order(EnrBenefitType.title())
                .filter(EnrBenefitType.title());
    }
}