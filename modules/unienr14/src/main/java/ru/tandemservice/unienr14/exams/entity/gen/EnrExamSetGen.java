package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор вступительных испытаний (ВИ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamSetGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.exams.entity.EnrExamSet";
    public static final String ENTITY_NAME = "enrExamSet";
    public static final int VERSION_HASH = -850784308;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_KEY = "key";
    public static final String P_ELEMENTS_TITLE = "elementsTitle";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private String _key;     // Ключ набора (генерируется по составу набора)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Ключ набора (генерируется по составу набора). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getKey()
    {
        return _key;
    }

    /**
     * @param key Ключ набора (генерируется по составу набора). Свойство не может быть null.
     */
    public void setKey(String key)
    {
        dirty(_key, key);
        _key = key;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamSetGen)
        {
            setEnrollmentCampaign(((EnrExamSet)another).getEnrollmentCampaign());
            setKey(((EnrExamSet)another).getKey());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamSetGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamSet.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamSet();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "key":
                    return obj.getKey();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "key":
                    obj.setKey((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "key":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "key":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "key":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamSet> _dslPath = new Path<EnrExamSet>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamSet");
    }
            

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSet#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Ключ набора (генерируется по составу набора). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSet#getKey()
     */
    public static PropertyPath<String> key()
    {
        return _dslPath.key();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSet#getElementsTitle()
     */
    public static SupportedPropertyPath<String> elementsTitle()
    {
        return _dslPath.elementsTitle();
    }

    public static class Path<E extends EnrExamSet> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _key;
        private SupportedPropertyPath<String> _elementsTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSet#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Ключ набора (генерируется по составу набора). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSet#getKey()
     */
        public PropertyPath<String> key()
        {
            if(_key == null )
                _key = new PropertyPath<String>(EnrExamSetGen.P_KEY, this);
            return _key;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSet#getElementsTitle()
     */
        public SupportedPropertyPath<String> elementsTitle()
        {
            if(_elementsTitle == null )
                _elementsTitle = new SupportedPropertyPath<String>(EnrExamSetGen.P_ELEMENTS_TITLE, this);
            return _elementsTitle;
        }

        public Class getEntityClass()
        {
            return EnrExamSet.class;
        }

        public String getEntityName()
        {
            return "enrExamSet";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getElementsTitle();
}
