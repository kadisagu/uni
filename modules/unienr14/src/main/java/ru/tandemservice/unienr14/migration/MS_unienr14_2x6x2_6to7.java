package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.tableExists("enr14_c_enr_step_kind_t")) {

            DBTable dbt = new DBTable("enr14_c_enr_step_kind_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);
		}

        short stepKindEntityCode = tool.entityCodes().ensure("enrEnrollmentStepKind");

        if (!tool.columnExists("enr14_enr_step_t", "kind_id")) {
			tool.createColumn("enr14_enr_step_t", new DBColumn("kind_id", DBType.LONG));

            Statement statement = tool.getConnection().createStatement();
            final ResultSet rs = statement.executeQuery("select id from enr14_c_enr_step_kind_t where code_p = 'rec_and_adhere'");
            Long defaultKind = rs.next() ? rs.getLong(1) : null;
            if (defaultKind == null) {
                defaultKind = EntityIDGenerator.generateNewId(stepKindEntityCode);
                tool.executeUpdate("insert into enr14_c_enr_step_kind_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)", defaultKind, stepKindEntityCode, "rec_and_adhere", "rec_and_adhere");
            }

			tool.executeUpdate("update enr14_enr_step_t set kind_id=? where kind_id is null", defaultKind);
			tool.setColumnNullable("enr14_enr_step_t", "kind_id", false);

		}

        if (tool.columnExists("enr14_enr_step_item_t", "passedexams_p")) {
            tool.dropColumn("enr14_enr_step_item_t", "passedexams_p");
        }
    }
}