/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.EnrReportEntrantsAgeDistributionAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsAgeDistribution;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 18.06.2014
 */
public class EnrReportEntrantsAgeDistributionDao extends UniBaseDao implements IEnrReportEntrantsAgeDistributionDao
{
    @Override
    public long createReport(EnrReportEntrantsAgeDistributionAddUI model) {

        EnrReportEntrantsAgeDistribution report = new EnrReportEntrantsAgeDistribution();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setCalculationAgeDate(model.getCalculationAgeDate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsAgeDistribution.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsAgeDistribution.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsAgeDistribution.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsAgeDistribution.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantsAgeDistribution.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantsAgeDistribution.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantsAgeDistribution.P_PROGRAM_SET, "title");

        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantsAgeDistribution.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantsAgeDistributionAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_AGE_DISTRIBUTION);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Дата

        new RtfInjectModifier().put("dateForm", new SimpleDateFormat("dd.MM.yyyy").format(model.getCalculationAgeDate())).modify(document);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }


        requestedCompDQL
                .where(and(ne(property("reqComp", EnrRequestedCompetition.state().priority()), value(8)), ne(property("reqComp", EnrRequestedCompetition.state().priority()), value(11))))
                .column(property("reqComp"));



        List<EnrRequestedCompetition> acceptanceStage = getList(requestedCompDQL);

        requestedCompDQL.where(and(le(property("reqComp", EnrRequestedCompetition.state().priority()), value(10)), or(le(property("reqComp", EnrRequestedCompetition.state().priority()), value(5)), ge(property("reqComp", EnrRequestedCompetition.state().priority()), value(9)))));

        List<EnrRequestedCompetition> examPassStage = getList(requestedCompDQL);

        requestedCompDQL.where(eq(property("reqComp", EnrRequestedCompetition.state().priority()), value(1)));

        List<EnrRequestedCompetition> enrollStage = getList(requestedCompDQL);

        RtfTable t1TableElement =(RtfTable) UniRtfUtil.findElement(document.getElementList(), "T1");
        RtfElement filial =(RtfElement) UniRtfUtil.findElement(document.getElementList(), "filial");
        document.getElementList().remove(t1TableElement);
        document.getElementList().remove(filial);

        Comparator<OrgUnit> orgUnitComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
            }
        };

        Comparator<OrgUnit> orgUnitTopDependentComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                int i = Boolean.compare(null == o1.getParent(), null == o2.getParent());
                if (0 != i) { return -i; }

                return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
            }
        };

        Map<OrgUnit, Map<OrgUnit, Set<EnrRequestedCompetition>>> acceptanceMap = new TreeMap<>(orgUnitTopDependentComparator);
        Map<OrgUnit, Map<OrgUnit, Set<EnrRequestedCompetition>>> examPassMap = new TreeMap<>(orgUnitTopDependentComparator);
        Map<OrgUnit, Map<OrgUnit, Set<EnrRequestedCompetition>>> enrollMap = new TreeMap<>(orgUnitTopDependentComparator);

        boolean firstIteration = true;

        // заполнение карт подразделение - формирующее - набор выбранных конкурсов

        for (EnrRequestedCompetition reqComp : acceptanceStage)
        {
            if (!acceptanceMap.containsKey(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit()))
            {
                acceptanceMap.put(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit(), new TreeMap<OrgUnit, Set<EnrRequestedCompetition>>(orgUnitComparator));
            }
            if (!acceptanceMap.get(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit())
                              .containsKey(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit()))
                acceptanceMap.get(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit())
                    .put(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit(), new HashSet<EnrRequestedCompetition>());
            acceptanceMap.get(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit())
                    .get(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit())
                    .add(reqComp);
        }

        for (EnrRequestedCompetition reqComp : examPassStage)
        {
            if (!examPassMap.containsKey(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit()))
            {
                examPassMap.put(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit(), new TreeMap<OrgUnit, Set<EnrRequestedCompetition>>(orgUnitComparator));
            }
            if (!examPassMap.get(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit())
                              .containsKey(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit()))
                examPassMap.get(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit())
                    .put(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit(), new HashSet<EnrRequestedCompetition>());
            examPassMap.get(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit())
                    .get(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit())
                    .add(reqComp);
        }

        for (EnrRequestedCompetition reqComp : enrollStage)
        {
            OrgUnit orgUnit = reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
            OrgUnit effectiveFormativeOrgUnit = reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();

            Map<OrgUnit, Set<EnrRequestedCompetition>> orgUnitMap = SafeMap.safeGet(enrollMap, orgUnit, TreeMap.class, orgUnitComparator);
            Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(orgUnitMap, effectiveFormativeOrgUnit, HashSet.class);
            requestedCompetitionSet.add(reqComp);
        }

        // заполнение таблиц

        for (Map.Entry<OrgUnit, Map<OrgUnit, Set<EnrRequestedCompetition>>> entry : acceptanceMap.entrySet())
            for (Map.Entry<OrgUnit, Set<EnrRequestedCompetition>> pair : entry.getValue().entrySet())
            {

                StringBuilder builder = new StringBuilder().append(pair.getKey().getPrintTitle()).append(" (").append(entry.getKey().getPrintTitle()).append(")");

                if (!firstIteration)
                {
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                }

                document.getElementList().add(filial.getClone());
                new RtfInjectModifier().put("filial", builder.toString()).modify(document);

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                Set<IdentityCard> acceptanceBudget = new HashSet<>();
                Set<IdentityCard> acceptanceCommercial = new HashSet<>();
                Set<IdentityCard> examPassBudget = new HashSet<>();
                Set<IdentityCard> examPassCommercial = new HashSet<>();
                Set<IdentityCard> enrollBudget = new HashSet<>();
                Set<IdentityCard> enrollCommercial = new HashSet<>();

                for (EnrRequestedCompetition reqComp : pair.getValue())
                {
                    if (reqComp.getCompetition().getType().getCompensationType().isBudget())
                        acceptanceBudget.add(reqComp.getRequest().getIdentityCard());
                    else acceptanceCommercial.add(reqComp.getRequest().getIdentityCard());
                }

                if (examPassMap.containsKey(entry.getKey()))
                    if (examPassMap.get(entry.getKey()).containsKey(pair.getKey()))
                        for (EnrRequestedCompetition reqComp : examPassMap.get(entry.getKey()).get(pair.getKey()))
                        {
                            if (reqComp.getCompetition().getType().getCompensationType().isBudget())
                                examPassBudget.add(reqComp.getRequest().getIdentityCard());
                            else examPassCommercial.add(reqComp.getRequest().getIdentityCard());
                        }

                if (enrollMap.containsKey(entry.getKey()))
                    if (enrollMap.get(entry.getKey()).containsKey(pair.getKey()))
                        for (EnrRequestedCompetition reqComp : enrollMap.get(entry.getKey()).get(pair.getKey()))
                        {
                            if (reqComp.getCompetition().getType().getCompensationType().isBudget())
                                enrollBudget.add(reqComp.getRequest().getIdentityCard());
                            else enrollCommercial.add(reqComp.getRequest().getIdentityCard());
                        }


                List<String[]> t1Table = buildContent(acceptanceBudget, acceptanceCommercial, "Численность абитуриентов", model.getCalculationAgeDate());
                List<String[]> t2Table = buildContent(examPassBudget, examPassCommercial, "Успешно прошли вступительные испытания", model.getCalculationAgeDate());
                List<String[]> t3Table = buildContent(enrollBudget, enrollCommercial, "Всего зачислено", model.getCalculationAgeDate());

                document.getElementList().add(t1TableElement.getClone());

                RtfTableModifier tableModifier = new RtfTableModifier();
                tableModifier.put("T1", t1Table.toArray(new String[t1Table.size()][]));
                tableModifier.put("T1", intercepter);
                tableModifier.put("T2", t2Table.toArray(new String[t2Table.size()][]));
                tableModifier.put("T2", intercepter);
                tableModifier.put("T3", t3Table.toArray(new String[t3Table.size()][]));
                tableModifier.put("T3", intercepter);
                tableModifier.modify(document);

                firstIteration = false;

            }

        RtfTableModifier tableModifier = new RtfTableModifier();
        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("H", new RtfRowIntercepterRawText());
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }



    private List<String[]> buildContent(Collection<IdentityCard> budgetCards, Collection<IdentityCard> commercialCards, String stage, Date calculationAgeDate)
    {
        List<String[]> content = new ArrayList<>();


        Integer[] budget = {0,0,0,0,0,0,0,0,0};
        Integer[] commercial = {0,0,0,0,0,0,0,0,0};
        Integer[] total =  {0,0,0,0,0,0,0,0,0};
        Integer[] budgetFem ={0,0,0,0,0,0,0,0,0};
        Integer[] commercialFem = {0,0,0,0,0,0,0,0,0};
        Integer[] totalFem = {0,0,0,0,0,0,0,0,0};

        for (IdentityCard card : budgetCards)
        {
            checkAgeAddToContent(card, budget, calculationAgeDate);
            if (!card.getSex().isMale())
                checkAgeAddToContent(card, budgetFem, calculationAgeDate);
        }


        for (IdentityCard card : commercialCards)
        {
            checkAgeAddToContent(card, commercial, calculationAgeDate);
            if (!card.getSex().isMale())
                checkAgeAddToContent(card, commercialFem, calculationAgeDate);
        }

        for (int i = 0; i < 9; i++) {
            total[i] = budget[i] + commercial [i];
            totalFem[i] = budgetFem[i] + commercialFem[i];
        }


        content.add(new String[]{
                    stage,
                    "Бюджет",
                    String.valueOf(budget[0]),
                    String.valueOf(budget[1]),
                    String.valueOf(budget[2]),
                    String.valueOf(budget[3]),
                    String.valueOf(budget[4]),
                    String.valueOf(budget[5]),
                    String.valueOf(budget[6]),
                    String.valueOf(budget[7]),
                    String.valueOf(budget[8]),
        });

        content.add(new String[]{
                    "",
                    "Договор",
                    String.valueOf(commercial[0]),
                    String.valueOf(commercial[1]),
                    String.valueOf(commercial[2]),
                    String.valueOf(commercial[3]),
                    String.valueOf(commercial[4]),
                    String.valueOf(commercial[5]),
                    String.valueOf(commercial[6]),
                    String.valueOf(commercial[7]),
                    String.valueOf(commercial[8]),
        });

        content.add(new String[]{
                    "",
                    "Всего",
                    String.valueOf(total[0]),
                    String.valueOf(total[1]),
                    String.valueOf(total[2]),
                    String.valueOf(total[3]),
                    String.valueOf(total[4]),
                    String.valueOf(total[5]),
                    String.valueOf(total[6]),
                    String.valueOf(total[7]),
                    String.valueOf(total[8]),
        });

        content.add(new String[]{
                    "В том числе женщин",
                    "Бюджет",
                    String.valueOf(budgetFem[0]),
                    String.valueOf(budgetFem[1]),
                    String.valueOf(budgetFem[2]),
                    String.valueOf(budgetFem[3]),
                    String.valueOf(budgetFem[4]),
                    String.valueOf(budgetFem[5]),
                    String.valueOf(budgetFem[6]),
                    String.valueOf(budgetFem[7]),
                    String.valueOf(budgetFem[8]),
        });

        content.add(new String[]{
                "",
                    "Договор",
                    String.valueOf(commercialFem[0]),
                    String.valueOf(commercialFem[1]),
                    String.valueOf(commercialFem[2]),
                    String.valueOf(commercialFem[3]),
                    String.valueOf(commercialFem[4]),
                    String.valueOf(commercialFem[5]),
                    String.valueOf(commercialFem[6]),
                    String.valueOf(commercialFem[7]),
                    String.valueOf(commercialFem[8]),
        });

        content.add(new String[]{
                    "",
                    "Всего",
                    String.valueOf(totalFem[0]),
                    String.valueOf(totalFem[1]),
                    String.valueOf(totalFem[2]),
                    String.valueOf(totalFem[3]),
                    String.valueOf(totalFem[4]),
                    String.valueOf(totalFem[5]),
                    String.valueOf(totalFem[6]),
                    String.valueOf(totalFem[7]),
                    String.valueOf(totalFem[8]),
        });

        return content;
    }


    // чекает возраст и увеличивает соответствующий элемент в массиве
    private void checkAgeAddToContent(IdentityCard card, Integer[] array, Date calculationAgeDate)
    {
        Date checkDate = card.getBirthDate();
        if (checkDate != null) {
            int age = calculationAgeDate == null ? card.getAge() : CoreDateUtils.getFullYearsAge(checkDate, calculationAgeDate);
            if (age <= 17)
                array[1]++;
            else if (age >= 24)
                array[8]++;
            else
                switch (age) {
                    case 18:
                        array[2]++; break;
                    case 19:
                        array[3]++; break;
                    case 20:
                        array[4]++; break;
                    case 21:
                        array[5]++; break;
                    case 22:
                        array[6]++; break;
                    case 23:
                        array[7]++; break;
                }

            array[0]++;
        }
    }


    // Перехватчик мерджит первый коламн под объединения
    private IRtfRowIntercepter intercepter = new IRtfRowIntercepter() {
        @Override
        public void beforeModify(RtfTable table, int currentRowIndex) {

        }

        @Override
        public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
            if (rowIndex % 3 == 0)
            {
                if (colIndex == 0)
                    cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
            }

            else
            if (colIndex == 0)
                cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
            return null;

        }

        @Override
        public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

        }
    };
}

