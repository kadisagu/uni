package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * см. DEV-5250
 * В миграции из DEV-4853 были удалены выбранные конкурсы на неправильно созданные конкурсы, и остались их хвосты в персистентном интерфейсе.
 * Почему все свалилось сейчас, а не после миграции - TF-658.
 * Нужно написать новую миграцию, которая погрохает все хвосты - т.е. все benefitProof, у которых больше нет benefitStatement. В транке и в бранче.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_27to28 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.0")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        /*
         * VIEW:
         * CREATE OR REPLACE VIEW ienrentrantbenefitstatement_v AS
         *     SELECT a0.id, a0.discriminator, a0.entrant_id, a0.benefitcategory_id
         *     FROM enr14_request_t a0
         * UNION ALL
         *     SELECT a0.id, a0.discriminator, ( SELECT fdef_1.entrant_id FROM enr14_requested_comp_t JOIN enr14_request_t fdef_1 ON fdef_1.id = enr14_requested_comp_t.request_id WHERE enr14_requested_comp_t.id = a1.id) AS entrant_id, a1.benefitcategory_id
         *     FROM enr14_requested_comp_t a0
         *     JOIN enr14_requested_comp_ne_t a1 ON a1.id = a0.id
         * UNION ALL
         *     SELECT a0.id, a0.discriminator, ( SELECT fdef_1.entrant_id FROM enr14_requested_comp_t JOIN enr14_request_t fdef_1 ON fdef_1.id = enr14_requested_comp_t.request_id WHERE enr14_requested_comp_t.id = a1.id) AS entrant_id, a1.benefitcategory_id
         *     FROM enr14_requested_comp_t a0
         *     JOIN enr14_requested_comp_ex_t a1 ON a1.id = a0.id;
         */

        /*
         * CONSTRAINT:
         * select distinct enr14_entrant_benefit_proof_t.benefitstatement_id as xpr_0
         * from enr14_entrant_benefit_proof_t
         * where enr14_entrant_benefit_proof_t.benefitstatement_id is not null and enr14_entrant_benefit_proof_t.benefitstatement_id not in (
         *  select ienrentrantbenefitstatement_v.id as xpr_1 from ienrentrantbenefitstatement_v
         * )
         */

        tool.executeUpdate(
            "delete from enr14_entrant_benefit_proof_t where id in (" +
            "select x.id from enr14_entrant_benefit_proof_t x " +
            "where x.benefitstatement_id is not null " +
            "and x.benefitstatement_id not in (select t1.id from enr14_request_t t1) " +
            "and x.benefitstatement_id not in (select t2.id from enr14_requested_comp_ne_t t2) " +
            "and x.benefitstatement_id not in (select t3.id from enr14_requested_comp_ex_t t3) " +
            ")"
        );
    }
}