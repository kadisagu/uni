/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author azhebko
 * @since 01.08.2014
 */
@Configuration
public class EnrProgramSetItemOrgUnitPlanEdit extends BusinessComponentManager
{
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .create();
    }
}