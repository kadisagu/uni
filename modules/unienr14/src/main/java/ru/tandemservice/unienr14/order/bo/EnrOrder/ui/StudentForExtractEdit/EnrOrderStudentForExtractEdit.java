/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.StudentForExtractEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.*;

/**
 * @author nvankov
 * @since 8/18/14
 */
@Configuration
public class EnrOrderStudentForExtractEdit extends BusinessComponentManager
{
    public static final String STUDENT_DS = "studentDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(STUDENT_DS, studentDSHandler()).addColumn("student", null, new IFormatter<Student>()
                {
                    @Override
                    public String format(Student source)
                    {
                        // через запятую личный номер, академ. группа, сокр. назв. форм. подр. НПП, сокр. назв. терр. подр. НПП, ФУТС в полном формате
                        StringBuilder builder = new StringBuilder().append(source.getPersonalNumber());
                        if(source.getGroup() != null) builder.append(", ").append(source.getGroup().getTitle());
                        if(source.getEducationOrgUnit() != null)
                        {
                            builder.append(", ").append(source.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle()).append(", ")
                                    .append(source.getEducationOrgUnit().getTerritorialOrgUnit().getShortTitle()).append(", ")
                                    .append(source.getEducationOrgUnit().getDevelopCombinationFullTitle());
                        }

                        return builder.toString();
                    }
                }))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler studentDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Student.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long extractId = context.getNotNull("extractId");
                EnrAbstractExtract extract = DataAccessServices.dao().get(extractId);
                if(extract instanceof EnrEnrollmentExtract)
                {
                    dql.where(DQLExpressions.exists(
                            new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ee")
                                    .where(DQLExpressions.eq(
                                            DQLExpressions.property("ee", EnrEnrollmentExtract.entity().request().entrant().person().id()),
                                            DQLExpressions.property(alias, Student.person().id())
                                    ))
                                    .where(DQLExpressions.eq(DQLExpressions.property("ee", EnrEnrollmentExtract.id()), DQLExpressions.value(extractId)))
                                    .where(DQLExpressions.eq(
                                            DQLExpressions.property("ee", EnrEnrollmentExtract.entity().competition().programSetOrgUnit().programSet().programSubject()),
                                            DQLExpressions.property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject())
                                    ))
                                    .buildQuery()));
                    dql.where(DQLExpressions.isNotNull(DQLExpressions.property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject())));
                    dql.where(DQLExpressions.notExists(
                                    new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ee")
                                            .where(DQLExpressions.eq(
                                                    DQLExpressions.property("ee", EnrEnrollmentExtract.student().id()),
                                                    DQLExpressions.property(alias, Student.id())
                                            ))
                                            .where(DQLExpressions.ne(DQLExpressions.property("ee", EnrEnrollmentExtract.id()), DQLExpressions.value(extractId)))
                                            .buildQuery())
                    );
                }
                else if(extract instanceof EnrAllocationExtract)
                {
                    dql.where(DQLExpressions.exists(
                            new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ae")
                                    .where(DQLExpressions.eq(
                                            DQLExpressions.property("ae", EnrAllocationExtract.entity().request().entrant().person().id()),
                                            DQLExpressions.property(alias, Student.person().id())
                                    ))
                                    .where(DQLExpressions.eq(DQLExpressions.property("ae", EnrAllocationExtract.id()), DQLExpressions.value(extractId)))
                                    .where(DQLExpressions.eq(
                                            DQLExpressions.property("ae", EnrAllocationExtract.entity().competition().programSetOrgUnit().programSet().programSubject()),
                                            DQLExpressions.property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject())
                                    ))
                                    .buildQuery()));
                    dql.where(DQLExpressions.isNotNull(DQLExpressions.property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject())));
                    dql.where(DQLExpressions.notExists(
                                    new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ae")
                                            .where(DQLExpressions.eq(
                                                    DQLExpressions.property("ae", EnrAllocationExtract.student().id()),
                                                    DQLExpressions.property(alias, Student.id())
                                            ))
                                            .where(DQLExpressions.ne(DQLExpressions.property("ae", EnrAllocationExtract.id()), DQLExpressions.value(extractId)))
                                            .buildQuery())
                    );
                }
                else if(extract instanceof EnrEnrollmentMinisteryExtract)
                {
                    dql.where(DQLExpressions.exists(
                            new DQLSelectBuilder().fromEntity(EnrEnrollmentMinisteryExtract.class, "eme")
                                    .where(DQLExpressions.eq(
                                            DQLExpressions.property("eme", EnrEnrollmentMinisteryExtract.entrantRequest().entrant().person().id()),
                                            DQLExpressions.property(alias, Student.person().id())
                                    ))
                                    .where(DQLExpressions.eq(DQLExpressions.property("eme", EnrEnrollmentMinisteryExtract.id()), DQLExpressions.value(extractId)))
                                    .where(DQLExpressions.eq(
                                            DQLExpressions.property("eme", EnrEnrollmentMinisteryExtract.entrantRequest().competition().programSetOrgUnit().programSet().programSubject()),
                                            DQLExpressions.property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject())
                                    ))
                                    .buildQuery()));
                    dql.where(DQLExpressions.isNotNull(DQLExpressions.property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject())));
                    dql.where(DQLExpressions.notExists(
                                    new DQLSelectBuilder().fromEntity(EnrEnrollmentMinisteryExtract.class, "eme")
                                            .where(DQLExpressions.eq(
                                                    DQLExpressions.property("eme", EnrEnrollmentMinisteryExtract.student().id()),
                                                    DQLExpressions.property(alias, Student.id())
                                            ))
                                            .where(DQLExpressions.ne(DQLExpressions.property("eme", EnrEnrollmentMinisteryExtract.id()), DQLExpressions.value(extractId)))
                                            .buildQuery())
                    );
                }
                else throw new IllegalStateException();
            }
        }
                .order(Student.person().identityCard().fullFio())
                .filter(Student.person().identityCard().fullFio());

    }
}



    