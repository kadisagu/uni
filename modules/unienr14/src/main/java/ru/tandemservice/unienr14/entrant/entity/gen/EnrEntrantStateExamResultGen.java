package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrSecondWaveStateExamPlace;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Результат ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantStateExamResultGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantStateExamResultGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult";
    public static final String ENTITY_NAME = "enrEntrantStateExamResult";
    public static final int VERSION_HASH = -1355992697;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_SUBJECT = "subject";
    public static final String P_YEAR = "year";
    public static final String L_SECOND_WAVE_NO_MARK_RELATION = "secondWaveNoMarkRelation";
    public static final String P_SECOND_WAVE = "secondWave";
    public static final String L_SECOND_WAVE_EXAM_PLACE = "secondWaveExamPlace";
    public static final String P_MARK_AS_LONG = "markAsLong";
    public static final String P_DOCUMENT_NUMBER = "documentNumber";
    public static final String P_DOCUMENT_ISSUANCE_DATE = "documentIssuanceDate";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_VERIFIED_BY_USER = "verifiedByUser";
    public static final String P_VERIFIED_BY_USER_COMMENT = "verifiedByUserComment";
    public static final String P_STATE_CHECK_STATUS = "stateCheckStatus";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_ABOVE_PASSING_MARK = "abovePassingMark";
    public static final String P_FOUND_IN_FIS = "foundInFis";
    public static final String P_EDIT_DISABLED = "editDisabled";
    public static final String P_MARK = "mark";
    public static final String P_TITLE = "title";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrStateExamSubject _subject;     // Предмет
    private int _year;     // Год получения балла
    private EnrEntrantStateExamResult _secondWaveNoMarkRelation;     // Результат ЕГЭ в доп.сроки без балла
    private boolean _secondWave;     // Направлен на ЕГЭ в доп. сроки
    private EnrSecondWaveStateExamPlace _secondWaveExamPlace;     // ППЭ для сдачи в доп. сроки
    private Long _markAsLong;     // Балл
    private String _documentNumber;     // Номер свидетельства
    private Date _documentIssuanceDate;     // Дата выдачи свидетельства
    private boolean _accepted;     // Зачтен
    private boolean _verifiedByUser;     // Проверен вручную
    private String _verifiedByUserComment;     // Комментарий к проверке результата вручную
    private String _stateCheckStatus;     // Статус проверки в ФИС
    private Date _registrationDate;     // Дата добавления
    private Date _modificationDate;     // Дата изменения
    private boolean _abovePassingMark;     // Балл выше порогового
    private boolean _foundInFis;     // Найден в ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Предмет. Свойство не может быть null.
     */
    @NotNull
    public EnrStateExamSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет. Свойство не может быть null.
     */
    public void setSubject(EnrStateExamSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Год получения балла. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год получения балла. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * Указывает на результат ЕГЭ второй волны без балла того же абитуриента по тому же предмету, что и указанны в текущей строке.
     * Предполагается, что ЕГЭ второй волны без балла для абитуриента и предмета только одна.
     * Данная связь заполняется только для результатов ЕГЭ с баллами (для результатов ЕГЭ без баллов поле будет сброшено).
     * Связь разрывается, если у объекта (по ссылке) появляется балл.
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshSource4StateExam.
     *
     * @return Результат ЕГЭ в доп.сроки без балла.
     */
    public EnrEntrantStateExamResult getSecondWaveNoMarkRelation()
    {
        return _secondWaveNoMarkRelation;
    }

    /**
     * @param secondWaveNoMarkRelation Результат ЕГЭ в доп.сроки без балла.
     */
    public void setSecondWaveNoMarkRelation(EnrEntrantStateExamResult secondWaveNoMarkRelation)
    {
        dirty(_secondWaveNoMarkRelation, secondWaveNoMarkRelation);
        _secondWaveNoMarkRelation = secondWaveNoMarkRelation;
    }

    /**
     * @return Направлен на ЕГЭ в доп. сроки. Свойство не может быть null.
     */
    @NotNull
    public boolean isSecondWave()
    {
        return _secondWave;
    }

    /**
     * @param secondWave Направлен на ЕГЭ в доп. сроки. Свойство не может быть null.
     */
    public void setSecondWave(boolean secondWave)
    {
        dirty(_secondWave, secondWave);
        _secondWave = secondWave;
    }

    /**
     * @return ППЭ для сдачи в доп. сроки.
     */
    public EnrSecondWaveStateExamPlace getSecondWaveExamPlace()
    {
        return _secondWaveExamPlace;
    }

    /**
     * @param secondWaveExamPlace ППЭ для сдачи в доп. сроки.
     */
    public void setSecondWaveExamPlace(EnrSecondWaveStateExamPlace secondWaveExamPlace)
    {
        dirty(_secondWaveExamPlace, secondWaveExamPlace);
        _secondWaveExamPlace = secondWaveExamPlace;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Балл.
     */
    public Long getMarkAsLong()
    {
        return _markAsLong;
    }

    /**
     * @param markAsLong Балл.
     */
    public void setMarkAsLong(Long markAsLong)
    {
        dirty(_markAsLong, markAsLong);
        _markAsLong = markAsLong;
    }

    /**
     * @return Номер свидетельства.
     */
    @Length(max=255)
    public String getDocumentNumber()
    {
        return _documentNumber;
    }

    /**
     * @param documentNumber Номер свидетельства.
     */
    public void setDocumentNumber(String documentNumber)
    {
        dirty(_documentNumber, documentNumber);
        _documentNumber = documentNumber;
    }

    /**
     * @return Дата выдачи свидетельства.
     */
    public Date getDocumentIssuanceDate()
    {
        return _documentIssuanceDate;
    }

    /**
     * @param documentIssuanceDate Дата выдачи свидетельства.
     */
    public void setDocumentIssuanceDate(Date documentIssuanceDate)
    {
        dirty(_documentIssuanceDate, documentIssuanceDate);
        _documentIssuanceDate = documentIssuanceDate;
    }

    /**
     * @return Зачтен. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Зачтен. Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * @return Проверен вручную. Свойство не может быть null.
     */
    @NotNull
    public boolean isVerifiedByUser()
    {
        return _verifiedByUser;
    }

    /**
     * @param verifiedByUser Проверен вручную. Свойство не может быть null.
     */
    public void setVerifiedByUser(boolean verifiedByUser)
    {
        dirty(_verifiedByUser, verifiedByUser);
        _verifiedByUser = verifiedByUser;
    }

    /**
     * @return Комментарий к проверке результата вручную.
     */
    public String getVerifiedByUserComment()
    {
        return _verifiedByUserComment;
    }

    /**
     * @param verifiedByUserComment Комментарий к проверке результата вручную.
     */
    public void setVerifiedByUserComment(String verifiedByUserComment)
    {
        dirty(_verifiedByUserComment, verifiedByUserComment);
        _verifiedByUserComment = verifiedByUserComment;
    }

    /**
     * @return Статус проверки в ФИС.
     */
    @Length(max=255)
    public String getStateCheckStatus()
    {
        return _stateCheckStatus;
    }

    /**
     * @param stateCheckStatus Статус проверки в ФИС.
     */
    public void setStateCheckStatus(String stateCheckStatus)
    {
        dirty(_stateCheckStatus, stateCheckStatus);
        _stateCheckStatus = stateCheckStatus;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Балл выше порогового. Свойство не может быть null.
     */
    @NotNull
    public boolean isAbovePassingMark()
    {
        return _abovePassingMark;
    }

    /**
     * @param abovePassingMark Балл выше порогового. Свойство не может быть null.
     */
    public void setAbovePassingMark(boolean abovePassingMark)
    {
        dirty(_abovePassingMark, abovePassingMark);
        _abovePassingMark = abovePassingMark;
    }

    /**
     * @return Найден в ФИС. Свойство не может быть null.
     */
    @NotNull
    public boolean isFoundInFis()
    {
        return _foundInFis;
    }

    /**
     * @param foundInFis Найден в ФИС. Свойство не может быть null.
     */
    public void setFoundInFis(boolean foundInFis)
    {
        dirty(_foundInFis, foundInFis);
        _foundInFis = foundInFis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantStateExamResultGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrEntrantStateExamResult)another).getEntrant());
                setSubject(((EnrEntrantStateExamResult)another).getSubject());
                setYear(((EnrEntrantStateExamResult)another).getYear());
            }
            setSecondWaveNoMarkRelation(((EnrEntrantStateExamResult)another).getSecondWaveNoMarkRelation());
            setSecondWave(((EnrEntrantStateExamResult)another).isSecondWave());
            setSecondWaveExamPlace(((EnrEntrantStateExamResult)another).getSecondWaveExamPlace());
            setMarkAsLong(((EnrEntrantStateExamResult)another).getMarkAsLong());
            setDocumentNumber(((EnrEntrantStateExamResult)another).getDocumentNumber());
            setDocumentIssuanceDate(((EnrEntrantStateExamResult)another).getDocumentIssuanceDate());
            setAccepted(((EnrEntrantStateExamResult)another).isAccepted());
            setVerifiedByUser(((EnrEntrantStateExamResult)another).isVerifiedByUser());
            setVerifiedByUserComment(((EnrEntrantStateExamResult)another).getVerifiedByUserComment());
            setStateCheckStatus(((EnrEntrantStateExamResult)another).getStateCheckStatus());
            setRegistrationDate(((EnrEntrantStateExamResult)another).getRegistrationDate());
            setModificationDate(((EnrEntrantStateExamResult)another).getModificationDate());
            setAbovePassingMark(((EnrEntrantStateExamResult)another).isAbovePassingMark());
            setFoundInFis(((EnrEntrantStateExamResult)another).isFoundInFis());
        }
    }

    public INaturalId<EnrEntrantStateExamResultGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getSubject(), getYear());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantStateExamResultGen>
    {
        private static final String PROXY_NAME = "EnrEntrantStateExamResultNaturalProxy";

        private Long _entrant;
        private Long _subject;
        private int _year;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, EnrStateExamSubject subject, int year)
        {
            _entrant = ((IEntity) entrant).getId();
            _subject = ((IEntity) subject).getId();
            _year = year;
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getSubject()
        {
            return _subject;
        }

        public void setSubject(Long subject)
        {
            _subject = subject;
        }

        public int getYear()
        {
            return _year;
        }

        public void setYear(int year)
        {
            _year = year;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantStateExamResultGen.NaturalId) ) return false;

            EnrEntrantStateExamResultGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getSubject(), that.getSubject()) ) return false;
            if( !equals(getYear(), that.getYear()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getSubject());
            result = hashCode(result, getYear());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getSubject());
            sb.append("/");
            sb.append(getYear());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantStateExamResultGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantStateExamResult.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantStateExamResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "subject":
                    return obj.getSubject();
                case "year":
                    return obj.getYear();
                case "secondWaveNoMarkRelation":
                    return obj.getSecondWaveNoMarkRelation();
                case "secondWave":
                    return obj.isSecondWave();
                case "secondWaveExamPlace":
                    return obj.getSecondWaveExamPlace();
                case "markAsLong":
                    return obj.getMarkAsLong();
                case "documentNumber":
                    return obj.getDocumentNumber();
                case "documentIssuanceDate":
                    return obj.getDocumentIssuanceDate();
                case "accepted":
                    return obj.isAccepted();
                case "verifiedByUser":
                    return obj.isVerifiedByUser();
                case "verifiedByUserComment":
                    return obj.getVerifiedByUserComment();
                case "stateCheckStatus":
                    return obj.getStateCheckStatus();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "modificationDate":
                    return obj.getModificationDate();
                case "abovePassingMark":
                    return obj.isAbovePassingMark();
                case "foundInFis":
                    return obj.isFoundInFis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "subject":
                    obj.setSubject((EnrStateExamSubject) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "secondWaveNoMarkRelation":
                    obj.setSecondWaveNoMarkRelation((EnrEntrantStateExamResult) value);
                    return;
                case "secondWave":
                    obj.setSecondWave((Boolean) value);
                    return;
                case "secondWaveExamPlace":
                    obj.setSecondWaveExamPlace((EnrSecondWaveStateExamPlace) value);
                    return;
                case "markAsLong":
                    obj.setMarkAsLong((Long) value);
                    return;
                case "documentNumber":
                    obj.setDocumentNumber((String) value);
                    return;
                case "documentIssuanceDate":
                    obj.setDocumentIssuanceDate((Date) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "verifiedByUser":
                    obj.setVerifiedByUser((Boolean) value);
                    return;
                case "verifiedByUserComment":
                    obj.setVerifiedByUserComment((String) value);
                    return;
                case "stateCheckStatus":
                    obj.setStateCheckStatus((String) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "abovePassingMark":
                    obj.setAbovePassingMark((Boolean) value);
                    return;
                case "foundInFis":
                    obj.setFoundInFis((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "subject":
                        return true;
                case "year":
                        return true;
                case "secondWaveNoMarkRelation":
                        return true;
                case "secondWave":
                        return true;
                case "secondWaveExamPlace":
                        return true;
                case "markAsLong":
                        return true;
                case "documentNumber":
                        return true;
                case "documentIssuanceDate":
                        return true;
                case "accepted":
                        return true;
                case "verifiedByUser":
                        return true;
                case "verifiedByUserComment":
                        return true;
                case "stateCheckStatus":
                        return true;
                case "registrationDate":
                        return true;
                case "modificationDate":
                        return true;
                case "abovePassingMark":
                        return true;
                case "foundInFis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "subject":
                    return true;
                case "year":
                    return true;
                case "secondWaveNoMarkRelation":
                    return true;
                case "secondWave":
                    return true;
                case "secondWaveExamPlace":
                    return true;
                case "markAsLong":
                    return true;
                case "documentNumber":
                    return true;
                case "documentIssuanceDate":
                    return true;
                case "accepted":
                    return true;
                case "verifiedByUser":
                    return true;
                case "verifiedByUserComment":
                    return true;
                case "stateCheckStatus":
                    return true;
                case "registrationDate":
                    return true;
                case "modificationDate":
                    return true;
                case "abovePassingMark":
                    return true;
                case "foundInFis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "subject":
                    return EnrStateExamSubject.class;
                case "year":
                    return Integer.class;
                case "secondWaveNoMarkRelation":
                    return EnrEntrantStateExamResult.class;
                case "secondWave":
                    return Boolean.class;
                case "secondWaveExamPlace":
                    return EnrSecondWaveStateExamPlace.class;
                case "markAsLong":
                    return Long.class;
                case "documentNumber":
                    return String.class;
                case "documentIssuanceDate":
                    return Date.class;
                case "accepted":
                    return Boolean.class;
                case "verifiedByUser":
                    return Boolean.class;
                case "verifiedByUserComment":
                    return String.class;
                case "stateCheckStatus":
                    return String.class;
                case "registrationDate":
                    return Date.class;
                case "modificationDate":
                    return Date.class;
                case "abovePassingMark":
                    return Boolean.class;
                case "foundInFis":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantStateExamResult> _dslPath = new Path<EnrEntrantStateExamResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantStateExamResult");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getSubject()
     */
    public static EnrStateExamSubject.Path<EnrStateExamSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Год получения балла. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * Указывает на результат ЕГЭ второй волны без балла того же абитуриента по тому же предмету, что и указанны в текущей строке.
     * Предполагается, что ЕГЭ второй волны без балла для абитуриента и предмета только одна.
     * Данная связь заполняется только для результатов ЕГЭ с баллами (для результатов ЕГЭ без баллов поле будет сброшено).
     * Связь разрывается, если у объекта (по ссылке) появляется балл.
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshSource4StateExam.
     *
     * @return Результат ЕГЭ в доп.сроки без балла.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getSecondWaveNoMarkRelation()
     */
    public static EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult> secondWaveNoMarkRelation()
    {
        return _dslPath.secondWaveNoMarkRelation();
    }

    /**
     * @return Направлен на ЕГЭ в доп. сроки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isSecondWave()
     */
    public static PropertyPath<Boolean> secondWave()
    {
        return _dslPath.secondWave();
    }

    /**
     * @return ППЭ для сдачи в доп. сроки.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getSecondWaveExamPlace()
     */
    public static EnrSecondWaveStateExamPlace.Path<EnrSecondWaveStateExamPlace> secondWaveExamPlace()
    {
        return _dslPath.secondWaveExamPlace();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Балл.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getMarkAsLong()
     */
    public static PropertyPath<Long> markAsLong()
    {
        return _dslPath.markAsLong();
    }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getDocumentNumber()
     */
    public static PropertyPath<String> documentNumber()
    {
        return _dslPath.documentNumber();
    }

    /**
     * @return Дата выдачи свидетельства.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getDocumentIssuanceDate()
     */
    public static PropertyPath<Date> documentIssuanceDate()
    {
        return _dslPath.documentIssuanceDate();
    }

    /**
     * @return Зачтен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * @return Проверен вручную. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isVerifiedByUser()
     */
    public static PropertyPath<Boolean> verifiedByUser()
    {
        return _dslPath.verifiedByUser();
    }

    /**
     * @return Комментарий к проверке результата вручную.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getVerifiedByUserComment()
     */
    public static PropertyPath<String> verifiedByUserComment()
    {
        return _dslPath.verifiedByUserComment();
    }

    /**
     * @return Статус проверки в ФИС.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getStateCheckStatus()
     */
    public static PropertyPath<String> stateCheckStatus()
    {
        return _dslPath.stateCheckStatus();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Балл выше порогового. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isAbovePassingMark()
     */
    public static PropertyPath<Boolean> abovePassingMark()
    {
        return _dslPath.abovePassingMark();
    }

    /**
     * @return Найден в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isFoundInFis()
     */
    public static PropertyPath<Boolean> foundInFis()
    {
        return _dslPath.foundInFis();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isEditDisabled()
     */
    public static SupportedPropertyPath<Boolean> editDisabled()
    {
        return _dslPath.editDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getMark()
     */
    public static SupportedPropertyPath<Integer> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrEntrantStateExamResult> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrStateExamSubject.Path<EnrStateExamSubject> _subject;
        private PropertyPath<Integer> _year;
        private EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult> _secondWaveNoMarkRelation;
        private PropertyPath<Boolean> _secondWave;
        private EnrSecondWaveStateExamPlace.Path<EnrSecondWaveStateExamPlace> _secondWaveExamPlace;
        private PropertyPath<Long> _markAsLong;
        private PropertyPath<String> _documentNumber;
        private PropertyPath<Date> _documentIssuanceDate;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Boolean> _verifiedByUser;
        private PropertyPath<String> _verifiedByUserComment;
        private PropertyPath<String> _stateCheckStatus;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Boolean> _abovePassingMark;
        private PropertyPath<Boolean> _foundInFis;
        private SupportedPropertyPath<Boolean> _editDisabled;
        private SupportedPropertyPath<Integer> _mark;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getSubject()
     */
        public EnrStateExamSubject.Path<EnrStateExamSubject> subject()
        {
            if(_subject == null )
                _subject = new EnrStateExamSubject.Path<EnrStateExamSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Год получения балла. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(EnrEntrantStateExamResultGen.P_YEAR, this);
            return _year;
        }

    /**
     * Указывает на результат ЕГЭ второй волны без балла того же абитуриента по тому же предмету, что и указанны в текущей строке.
     * Предполагается, что ЕГЭ второй волны без балла для абитуриента и предмета только одна.
     * Данная связь заполняется только для результатов ЕГЭ с баллами (для результатов ЕГЭ без баллов поле будет сброшено).
     * Связь разрывается, если у объекта (по ссылке) появляется балл.
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshSource4StateExam.
     *
     * @return Результат ЕГЭ в доп.сроки без балла.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getSecondWaveNoMarkRelation()
     */
        public EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult> secondWaveNoMarkRelation()
        {
            if(_secondWaveNoMarkRelation == null )
                _secondWaveNoMarkRelation = new EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult>(L_SECOND_WAVE_NO_MARK_RELATION, this);
            return _secondWaveNoMarkRelation;
        }

    /**
     * @return Направлен на ЕГЭ в доп. сроки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isSecondWave()
     */
        public PropertyPath<Boolean> secondWave()
        {
            if(_secondWave == null )
                _secondWave = new PropertyPath<Boolean>(EnrEntrantStateExamResultGen.P_SECOND_WAVE, this);
            return _secondWave;
        }

    /**
     * @return ППЭ для сдачи в доп. сроки.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getSecondWaveExamPlace()
     */
        public EnrSecondWaveStateExamPlace.Path<EnrSecondWaveStateExamPlace> secondWaveExamPlace()
        {
            if(_secondWaveExamPlace == null )
                _secondWaveExamPlace = new EnrSecondWaveStateExamPlace.Path<EnrSecondWaveStateExamPlace>(L_SECOND_WAVE_EXAM_PLACE, this);
            return _secondWaveExamPlace;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Балл.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getMarkAsLong()
     */
        public PropertyPath<Long> markAsLong()
        {
            if(_markAsLong == null )
                _markAsLong = new PropertyPath<Long>(EnrEntrantStateExamResultGen.P_MARK_AS_LONG, this);
            return _markAsLong;
        }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getDocumentNumber()
     */
        public PropertyPath<String> documentNumber()
        {
            if(_documentNumber == null )
                _documentNumber = new PropertyPath<String>(EnrEntrantStateExamResultGen.P_DOCUMENT_NUMBER, this);
            return _documentNumber;
        }

    /**
     * @return Дата выдачи свидетельства.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getDocumentIssuanceDate()
     */
        public PropertyPath<Date> documentIssuanceDate()
        {
            if(_documentIssuanceDate == null )
                _documentIssuanceDate = new PropertyPath<Date>(EnrEntrantStateExamResultGen.P_DOCUMENT_ISSUANCE_DATE, this);
            return _documentIssuanceDate;
        }

    /**
     * @return Зачтен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(EnrEntrantStateExamResultGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * @return Проверен вручную. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isVerifiedByUser()
     */
        public PropertyPath<Boolean> verifiedByUser()
        {
            if(_verifiedByUser == null )
                _verifiedByUser = new PropertyPath<Boolean>(EnrEntrantStateExamResultGen.P_VERIFIED_BY_USER, this);
            return _verifiedByUser;
        }

    /**
     * @return Комментарий к проверке результата вручную.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getVerifiedByUserComment()
     */
        public PropertyPath<String> verifiedByUserComment()
        {
            if(_verifiedByUserComment == null )
                _verifiedByUserComment = new PropertyPath<String>(EnrEntrantStateExamResultGen.P_VERIFIED_BY_USER_COMMENT, this);
            return _verifiedByUserComment;
        }

    /**
     * @return Статус проверки в ФИС.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getStateCheckStatus()
     */
        public PropertyPath<String> stateCheckStatus()
        {
            if(_stateCheckStatus == null )
                _stateCheckStatus = new PropertyPath<String>(EnrEntrantStateExamResultGen.P_STATE_CHECK_STATUS, this);
            return _stateCheckStatus;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EnrEntrantStateExamResultGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EnrEntrantStateExamResultGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Балл выше порогового. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isAbovePassingMark()
     */
        public PropertyPath<Boolean> abovePassingMark()
        {
            if(_abovePassingMark == null )
                _abovePassingMark = new PropertyPath<Boolean>(EnrEntrantStateExamResultGen.P_ABOVE_PASSING_MARK, this);
            return _abovePassingMark;
        }

    /**
     * @return Найден в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isFoundInFis()
     */
        public PropertyPath<Boolean> foundInFis()
        {
            if(_foundInFis == null )
                _foundInFis = new PropertyPath<Boolean>(EnrEntrantStateExamResultGen.P_FOUND_IN_FIS, this);
            return _foundInFis;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#isEditDisabled()
     */
        public SupportedPropertyPath<Boolean> editDisabled()
        {
            if(_editDisabled == null )
                _editDisabled = new SupportedPropertyPath<Boolean>(EnrEntrantStateExamResultGen.P_EDIT_DISABLED, this);
            return _editDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getMark()
     */
        public SupportedPropertyPath<Integer> mark()
        {
            if(_mark == null )
                _mark = new SupportedPropertyPath<Integer>(EnrEntrantStateExamResultGen.P_MARK, this);
            return _mark;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrEntrantStateExamResultGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrEntrantStateExamResult.class;
        }

        public String getEntityName()
        {
            return "enrEntrantStateExamResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isEditDisabled();

    public abstract Integer getMark();

    public abstract String getTitle();
}
