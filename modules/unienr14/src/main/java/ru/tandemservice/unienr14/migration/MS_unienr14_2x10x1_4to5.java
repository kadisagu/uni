package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность customerTargetContractTraining

        // создана новая сущность
        if (!tool.tableExists("cstmrtrgtcntrcttrnng_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("cstmrtrgtcntrcttrnng_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_b079913b"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("programset_id", DBType.LONG).setNullable(false),
                                      new DBColumn("externalorgunit_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("customerTargetContractTraining");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrProgramSetBase

        // создано обязательное свойство targetContractTraining
        if (!tool.columnExists("enr14_program_set_base_t", "targetcontracttraining_p"))
        {
            // создать колонку
            tool.createColumn("enr14_program_set_base_t", new DBColumn("targetcontracttraining_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultTargetContractTraining = false;
            tool.executeUpdate("update enr14_program_set_base_t set targetcontracttraining_p=? where targetcontracttraining_p is null", defaultTargetContractTraining);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_program_set_base_t", "targetcontracttraining_p", false);

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrRequestedCompetition

        // создано свойство contractCommonDate
        if (!tool.columnExists("enr14_requested_comp_t", "contractcommondate_p"))
        {
            // создать колонку
            tool.createColumn("enr14_requested_comp_t", new DBColumn("contractcommondate_p", DBType.DATE));

        }

        // создано свойство contractCommonNumber
        if (!tool.columnExists("enr14_requested_comp_t", "contractcommonnumber_p"))
        {
            // создать колонку
            tool.createColumn("enr14_requested_comp_t", new DBColumn("contractcommonnumber_p", DBType.createVarchar(255)));

        }

        // создано свойство externalOrgUnit
        if (!tool.columnExists("enr14_requested_comp_t", "externalorgunit_id"))
        {
            // создать колонку
            tool.createColumn("enr14_requested_comp_t", new DBColumn("externalorgunit_id", DBType.LONG));

        }


    }
}