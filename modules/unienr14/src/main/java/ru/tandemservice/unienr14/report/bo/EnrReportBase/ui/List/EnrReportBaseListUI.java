/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/14/14
 */
@State({
    @Bind(key = EnrReportBaseListUI.BIND_REPORT_DESC, binding = "reportDescName")
})
public class EnrReportBaseListUI<T extends IEnrReport> extends UIPresenter
{
    public static final String BIND_REPORT_DESC = "reportDesc";

    private String reportDescName;
    private IEnrStorableReportDesc reportDesc;

    private EnrEnrollmentCampaign _enrollmentCampaign;

    private DynamicListDataSource<T> reportDS;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setReportDesc(EnrReportBaseManager.instance().storableReportDescExtPoint().getItem(getReportDescName()));
        prepareReportDS();
    }

    public void onClickAddReport() {
        _uiActivation.asRegion(getReportDesc().getAddFormComponent()).top().activate();
    }

    public void onClickDeleteReport() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickPrint() {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getListenerParameterAsLong()), true);
    }

    public void onClickSearch() {
        getReportDS().refresh();
        DataSettingsFacade.saveSettings(getSettings());
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickClear() {
        getSettings().clear();
        onClickSearch();
    }

    // presenter

    public String getSettingsKey() {
        return getReportDesc().getReportClass().getSimpleName() + "List.";
    }

    // util

    private void prepareReportDS()
    {
        if (getReportDS() != null) return;

        DynamicListDataSource<T> dataSource = new DynamicListDataSource<>(this, component -> {
            final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(getReportDesc().getReportClass(), "d");
            final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("d"));
            orderDescriptionRegistry.applyOrder(dql, getReportDS().getEntityOrder());
            dql.where(eq(property("d." + IEnrReport.L_ENROLLMENT_CAMPAIGN), value(getEnrollmentCampaign())));
            UniBaseUtils.createPage(getReportDS(), dql, _uiSupport.getSession());
        });
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new PublisherLinkColumn("Дата формирования", IEnrReport.P_FORMING_DATE)
            .setResolver(new DefaultPublisherLinkResolver() {
                @Override public String getComponentName(IEntity entity) {
                    return EnrReportBasePub.class.getSimpleName();
                }
            })
            .setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME)

        );
        dataSource.addColumn(new SimpleColumn("Отчетный период", IEnrReport.P_PERIOD_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrint")
            .defaultIndicator(new IndicatorColumn.Item("printer", "Печать"))
            .setImageHeader(false)
            .setDisableSecondSubmit(false)
            .setPermissionKey("enr14StorableReportPrint")
            .setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", IEnrReport.P_FORMING_DATE)
            .setPermissionKey("enr14StorableReportDelete"));
        dataSource.setOrder(IEnrReport.P_FORMING_DATE, OrderDirection.desc);
        setReportDS(dataSource);
    }

    // getters and setters

    public IEnrStorableReportDesc getReportDesc()
    {
        return reportDesc;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public DynamicListDataSource<T> getReportDS()
    {
        return reportDS;
    }

    public void setReportDS(DynamicListDataSource<T> reportDS)
    {
        this.reportDS = reportDS;
    }

    public void setReportDesc(IEnrStorableReportDesc reportDesc)
    {
        this.reportDesc = reportDesc;
    }

    public String getReportDescName()
    {
        return reportDescName;
    }

    public void setReportDescName(String reportDescName)
    {
        this.reportDescName = reportDescName;
    }
}