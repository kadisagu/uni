package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrReportCompetitionList

		// создано свойство origEduDocument
		{
			// создать колонку
			tool.createColumn("enr14_rep_comp_list_t", new DBColumn("origedudocument_p", DBType.createVarchar(255)));

		}

		// создано свойство consentEnrollment
		{
			// создать колонку
			tool.createColumn("enr14_rep_comp_list_t", new DBColumn("consentenrollment_p", DBType.createVarchar(255)));

		}

		// создано свойство finalAgreementEnrollment
		{
			// создать колонку
			tool.createColumn("enr14_rep_comp_list_t", new DBColumn("finalagreementenrollment_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrReportRatingList

		// создано свойство origEduDocument
		{
			// создать колонку
			tool.createColumn("enr14_rep_rating_list_t", new DBColumn("origedudocument_p", DBType.createVarchar(255)));

		}

		// создано свойство consentEnrollment
		{
			// создать колонку
			tool.createColumn("enr14_rep_rating_list_t", new DBColumn("consentenrollment_p", DBType.createVarchar(255)));

		}

		// создано свойство finalAgreementEnrollment
		{
			// создать колонку
			tool.createColumn("enr14_rep_rating_list_t", new DBColumn("finalagreementenrollment_p", DBType.createVarchar(255)));

		}


    }
}