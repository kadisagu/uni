/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;

import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

/**
 * @author oleyba
 * @since 5/9/13
 */
@Configuration
public class EnrEntrantRequestEdit extends BusinessComponentManager
{
    public static final String DS_BENEFIT_CATEGORY_PREFERENCE = EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_PREFERENCE;
    public static final String DS_BENEFIT_PROOF_PREFERENCE_DOC = EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_PREFERENCE;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addDataSource(EnrEntrantBaseDocumentManager.instance().eduDocumentDSConfig())
        .addDataSource(EnrEntrantBaseDocumentManager.instance().identityCardDSConfig())
        .addDataSource(selectDS(DS_BENEFIT_CATEGORY_PREFERENCE, EnrCatalogCommonsManager.instance().benefitCategoryDSHandler()))
        .addDataSource(selectDS(DS_BENEFIT_PROOF_PREFERENCE_DOC, EnrEntrantRequestManager.instance().benefitProofDocDSHandler()).addColumn(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
        .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
        .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
        .addDataSource(EnrEntrantRequestManager.instance().originalSubmissionAndReturnWayDSConfig())
        .create();
    }
}