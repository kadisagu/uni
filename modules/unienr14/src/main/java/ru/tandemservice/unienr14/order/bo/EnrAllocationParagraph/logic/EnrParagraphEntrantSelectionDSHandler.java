/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic;

import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit.EnrAllocationParagraphAddEdit;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/7/14
 */
public class EnrParagraphEntrantSelectionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String ENROLL_EXTRACT = "enrollExtract";
    public static final String CUSTOM_STATES = "customStates";

    public EnrParagraphEntrantSelectionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrRequestedCompetition.class, "r");

        boolean onlyAllocated = context.getBoolean(EnrAllocationParagraphAddEdit.BIND_ONLY_ALLOCATED, false);
        boolean onlyEduProgram = context.getBoolean(EnrAllocationParagraphAddEdit.BIND_ONLY_EDU_PROGRAM, false);

        EnrAllocationParagraph paragraph = context.get(EnrAllocationParagraphAddEdit.BIND_PARAGRAPH);

        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("r"))
            .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "c")
            .where(eq(property(EnrCompetition.requestType().fromAlias("c")), value(((EnrOrder) paragraph.getOrder()).getRequestType())))
            .where(eq(property(EnrCompetition.type().compensationType().fromAlias("c")), value(((EnrOrder) paragraph.getOrder()).getCompensationType())))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().programForm()), commonValue(context.get(EnrAllocationParagraphAddEdit.BIND_FORM))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(EnrAllocationParagraphAddEdit.BIND_ORG_UNIT))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(EnrAllocationParagraphAddEdit.BIND_SUBJECT))))
            .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), commonValue(context.get(EnrAllocationParagraphAddEdit.BIND_FORMATIVE_ORG_UNIT))))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet()), commonValue(context.get(EnrAllocationParagraphAddEdit.BIND_PROGRAM_SET))))
        ;

        EnrProgramSetItem programSetItem = context.get(EnrAllocationParagraphAddEdit.BIND_PROGRAM_SET_ITEM);

        if(programSetItem != null)
        {
            dql.where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet()), value(programSetItem.getProgramSet())));
        }


        DQLSelectBuilder extrDQL = new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ex");
        extrDQL.where(eq(property("ex", EnrAllocationExtract.entity()), property("r")));

        List<Long> exIds = CommonBaseEntityUtil.getIdList(paragraph.getExtractList());

        extrDQL.where(notIn(property("ex", EnrAllocationExtract.id()), exIds));

        dql.where(notExists(extrDQL.buildQuery()));

        if (onlyAllocated)
        {
            DQLSelectBuilder allocDQL = new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "alloc")
                    .where(eq(property("alloc", EnrProgramAllocationItem.entrant()), property("r")));
            if(programSetItem != null)
                allocDQL.where(eq(property("alloc", EnrProgramAllocationItem.programSetItem()), value(programSetItem)));
            dql.where(exists(allocDQL.buildQuery()));
        }

        if (onlyEduProgram)
        {
            DQLSelectBuilder reqProgDQL = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                .where(eq(property("rp", EnrRequestedProgram.requestedCompetition()), property("r")));
            if(programSetItem != null)
                reqProgDQL.where(eq(property("rp", EnrRequestedProgram.programSetItem()), value(programSetItem)));
            dql.where(exists(reqProgDQL.buildQuery()));
        }

        orderDescriptionRegistry.applyOrder(dql, input.getEntityOrder());

        List<EnrRequestedCompetition> requestedCompetitions = dql.createStatement(context.getSession()).list();
        Collection<EnrEntrant> entrants = CollectionUtils.collect(requestedCompetitions, enrRequestedCompetition -> enrRequestedCompetition.getRequest().getEntrant());

        final Map<Long, EnrEnrollmentExtract> enrollExtractMap = Maps.newHashMap();
        BatchUtils.execute(requestedCompetitions, 100, elements -> {
            for(EnrEnrollmentExtract enrollmentExtract : new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ee")
                    .where(in(property("ee", EnrEnrollmentExtract.entity()), elements))
                    .createStatement(context.getSession()).<EnrEnrollmentExtract>list())
            {
                enrollExtractMap.put(enrollmentExtract.getRequestedCompetition().getId(), enrollmentExtract);
            }
        });

        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrants, new Date());

        List<DataWrapper> result = new ArrayList<>();
        for (EnrRequestedCompetition requestedCompetition: requestedCompetitions)
        {
            DataWrapper record = new DataWrapper(requestedCompetition);
            record.setProperty(ENROLL_EXTRACT, enrollExtractMap.get(requestedCompetition.getId()));
            record.setProperty(CUSTOM_STATES, customStatesMap.get(requestedCompetition.getRequest().getEntrant()));
            result.add(record);
        }

        DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build().ordering(new EntityComparator(
            new EntityOrder(EnrRequestedCompetition.request().entrant().person().fullFio(), OrderDirection.asc),
            new EntityOrder(EnrRequestedCompetition.id(), OrderDirection.asc)));

        output.setCountRecord(Math.max(1, output.getRecordList().size()));

        return output;
    }

}
