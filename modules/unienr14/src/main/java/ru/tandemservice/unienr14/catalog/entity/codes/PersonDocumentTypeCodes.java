package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип документа персоны"
 * Имя сущности : personDocumentType
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface PersonDocumentTypeCodes
{
    /** Константа кода (code) элемента : Удостоверение личности (title) */
    String INDENTITY_CARD = "1";
    /** Константа кода (code) элемента : Документ о полученном образовании (title) */
    String EDU_INSTITUTION = "4";
    /** Константа кода (code) элемента : Диплом участника олимпиады (title) */
    String OLYMP_DIPLOMA = "2";
    /** Константа кода (code) элемента : Справка об установлении инвалидности (title) */
    String DISABLED_CERT = "5";
    /** Константа кода (code) элемента : Заключение об отсутствии противопоказаний для обучения (title) */
    String CAN_RECEIVE_EDUCATION_CERT = "6";
    /** Константа кода (code) элемента : Заключение психолого-медико-педагогической комиссии (title) */
    String PSY_CERT = "7";
    /** Константа кода (code) элемента : Документ, подтверждающий сиротство (title) */
    String ORPHAN = "2016_08";
    /** Константа кода (code) элемента : Документ, подтверждающий принадлежность к ветеранам боевых действий (title) */
    String COMBAT_VETERANS = "2016_09";
    /** Константа кода (code) элемента : Диплом победителя/призера в области спорта (title) */
    String SPORTS_DIPLOMA = "2016_10";
    /** Константа кода (code) элемента : Документ, подтверждающий принадлежность к соотечественникам (title) */
    String BELONGING_NATION = "2016_11";

    Set<String> CODES = ImmutableSet.of(INDENTITY_CARD, EDU_INSTITUTION, OLYMP_DIPLOMA, DISABLED_CERT, CAN_RECEIVE_EDUCATION_CERT, PSY_CERT, ORPHAN, COMBAT_VETERANS, SPORTS_DIPLOMA, BELONGING_NATION);
}
