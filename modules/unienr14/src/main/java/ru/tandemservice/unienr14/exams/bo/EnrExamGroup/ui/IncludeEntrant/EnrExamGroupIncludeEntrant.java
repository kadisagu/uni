/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.IncludeEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/7/14
 */
@Configuration
public class EnrExamGroupIncludeEntrant extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EXAM_GROUP_SET_DS = "examGroupSetDS";
	public static final String GROUP_SCHEDULE_EVENT_DS =  "groupScheduleEventDS";

    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_EXAM_GROUP_SET = "examGroupSet";
    public static final String BIND_ENTRANT = "entrant";
    public static final String BIND_DISCIPLINE = "discipline";
    public static final String BIND_PASS_FORM = "passForm";
    public static final String BIND_PREVIOUS_EXAM_GROUP = "prevExamGroup";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addDataSource(selectDS(EXAM_GROUP_SET_DS, examGroupSetSelectDSHandler()).addColumn(EnrExamGroupSet.periodTitle().s()))
 			.addDataSource(CommonBaseStaticSelectDataSource.selectDS(GROUP_SCHEDULE_EVENT_DS, getName(), EnrExamGroupScheduleEvent.defaultSelectDSHandler(getName())))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSetSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroupSet.class)
            .where(EnrExamGroupSet.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
            .order(EnrExamGroupSet.beginDate())
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EnrEntrant entrant = context.get(BIND_ENTRANT);
                if (entrant == null)
                    dql.where(isNull(alias + ".id"));
            }
        }
            .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }
}