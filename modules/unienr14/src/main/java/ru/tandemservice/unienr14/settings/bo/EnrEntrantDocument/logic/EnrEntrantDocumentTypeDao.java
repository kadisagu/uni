/**
 *$Id: EnrEntrantDocumentTypeDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.gen.EnrCampaignEntrantDocumentGen;

/**
 * @author Alexander Shaburov
 * @since 30.04.13
 */
public class EnrEntrantDocumentTypeDao extends UniBaseDao implements IEnrEntrantDocumentTypeDao
{

    @Override
    public Long doToggleUsed(Long documentTypeId, Long enrollmentCampaignId, boolean used)
    {
        lock("enrCampaignEntrantDocument."+enrollmentCampaignId+"."+documentTypeId);

        final EnrEnrollmentCampaign enrollmentCampaign = getNotNull(enrollmentCampaignId);
        final PersonDocumentType documentType = getNotNull(documentTypeId);
        EnrCampaignEntrantDocument campaignEntrantDocument = getByNaturalId(new EnrCampaignEntrantDocumentGen.NaturalId(documentType, enrollmentCampaign));

        if (used) {
            if (campaignEntrantDocument == null) {
                campaignEntrantDocument = new EnrCampaignEntrantDocument(enrollmentCampaign, documentType);

                if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(documentType.getCode()) && (2016 <= enrollmentCampaign.getEducationYear().getIntValue()))
                {
                    campaignEntrantDocument.setAchievement(true);
                }
            }
            saveOrUpdate(campaignEntrantDocument);
            return campaignEntrantDocument.getId();
        } else {
            if (campaignEntrantDocument != null) {
                delete(campaignEntrantDocument);
            }
            return null;
        }
    }

    @Override
    public Long doToggleProperty(Long documentTypeId, Long enrollmentCampaignId, String propertyName, boolean propertyValue)
    {
        final Long campaignEntrantDocumentId = doToggleUsed(documentTypeId, enrollmentCampaignId, true);
        final EnrCampaignEntrantDocument campaignEntrantDocument = getNotNull(campaignEntrantDocumentId);
        campaignEntrantDocument.setProperty(propertyName, Boolean.valueOf(propertyValue));
        saveOrUpdate(campaignEntrantDocument);
        return campaignEntrantDocument.getId();
    }


    /** { enrEntrantDocumentType.code -> { enrBenefitType.code }} */
    private static final Map<String, Collection<String>> DEFAULT_DOC_TYPE_SETTINGS;
    static {
        Map<String, Collection<String>> map = new LinkedHashMap<String, Collection<String>>();

        map.put(PersonDocumentTypeCodes.INDENTITY_CARD, Arrays.<String>asList());
        map.put(PersonDocumentTypeCodes.EDU_INSTITUTION, Arrays.<String>asList());

        // DEV-4832: элементу «Диплом олимпиады» ставим "да" в поля «Подтверждает особое право - без ВИ» и «Подтверждает особое право - 100 баллов»
        map.put(PersonDocumentTypeCodes.OLYMP_DIPLOMA, Arrays.<String>asList(EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS, EnrBenefitTypeCodes.MAX_EXAM_RESULT));

        // DEV-4832: элементам «Справка об установлении инвалидности», «Заключение об отсутствии противопоказаний для обучения», «Заключение психолого-медико-педагогической комиссии» - ставим "да" в поле «Подтверждает особое право - квота» и в поле «Подтверждает особое право - преим. право», "нет" во все остальные
        map.put(PersonDocumentTypeCodes.DISABLED_CERT, Arrays.<String>asList(EnrBenefitTypeCodes.EXCLUSIVE, EnrBenefitTypeCodes.PREFERENCE));
        map.put(PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT, Arrays.<String>asList(EnrBenefitTypeCodes.EXCLUSIVE, EnrBenefitTypeCodes.PREFERENCE));
        map.put(PersonDocumentTypeCodes.PSY_CERT, Arrays.<String>asList(EnrBenefitTypeCodes.EXCLUSIVE, EnrBenefitTypeCodes.PREFERENCE));

        //DEV-9573: Элементам «Документ, подтверждающий сиротство», «Документ, подтверждающий принадлежность к ветеранам боевых действий» - ставим "да" в поле «Подтверждает особое право - квота» и в поле «Подтверждает особое право - преим. право»
        // Элементу «Диплом победителя/призера в области спорта»- ставим "да" в поле «Подтверждает особое право - без ВИ» и в поле «Подтверждает особое право - 100 баллов»
        map.put(PersonDocumentTypeCodes.ORPHAN, Arrays.asList(EnrBenefitTypeCodes.EXCLUSIVE, EnrBenefitTypeCodes.PREFERENCE));
        map.put(PersonDocumentTypeCodes.COMBAT_VETERANS, Arrays.asList(EnrBenefitTypeCodes.EXCLUSIVE, EnrBenefitTypeCodes.PREFERENCE));
        map.put(PersonDocumentTypeCodes.SPORTS_DIPLOMA, Arrays.asList(EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS, EnrBenefitTypeCodes.MAX_EXAM_RESULT));
        map.put(PersonDocumentTypeCodes.BELONGING_NATION, Arrays.asList());

        DEFAULT_DOC_TYPE_SETTINGS = Collections.unmodifiableMap(map);
    }


    @Override
    public void doSetEnrCampDefaultSettings(Long enrollmentCampaignId)
    {
        final EnrEnrollmentCampaign enrCamp = getNotNull(enrollmentCampaignId);

        final Set<String> existDocTypeCodeList = new HashSet<String>(
        new DQLSelectBuilder()
        .fromEntity(EnrCampaignEntrantDocument.class, "b")
        .column(property(EnrCampaignEntrantDocument.documentType().code().fromAlias("b")))
        .where(eq(property(EnrCampaignEntrantDocument.enrollmentCampaign().id().fromAlias("b")), value(enrollmentCampaignId)))
        .createStatement(getSession()).<String>list()
        );

        for (Map.Entry<String, Collection<String>> e: DEFAULT_DOC_TYPE_SETTINGS.entrySet()) {
            if (!existDocTypeCodeList.contains(e.getKey())) {
                save(new EnrCampaignEntrantDocument(enrCamp, getCatalogItem(PersonDocumentType.class, e.getKey()), e.getValue()));
            }
        }
    }
}
