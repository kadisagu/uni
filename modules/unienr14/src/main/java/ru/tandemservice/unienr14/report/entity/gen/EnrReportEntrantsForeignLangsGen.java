package ru.tandemservice.unienr14.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список абитуриентов, изучающих иностранные языки (по подразделениям)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrReportEntrantsForeignLangsGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs";
    public static final String ENTITY_NAME = "enrReportEntrantsForeignLangs";
    public static final int VERSION_HASH = -223108240;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_ENROLLMENT_STAGE = "enrollmentStage";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_COMPETITION_TYPE = "competitionType";
    public static final String P_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_PROGRAM_SET = "programSet";
    public static final String P_SPLITTED = "splitted";
    public static final String P_PARALLEL = "parallel";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления добавлены с
    private Date _dateTo;     // Заявления добавлены по
    private String _enrollmentStage;     // Стадия приемной кампании
    private String _requestType;     // Вид заявления
    private String _compensationType;     // Вид возмещения затрат
    private String _programForm;     // Форма обучения
    private String _competitionType;     // Вид приема
    private String _enrOrgUnit;     // Филиал
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _programSubject;     // Направление, спец., профессия
    private String _eduProgram;     // Образовательная программа
    private String _programSet;     // Набор образовательных программ
    private String _splitted;     // Группировать абитуриентов по направлениям приема на подразделении
    private String _parallel;     // Поступающие параллельно

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления добавлены с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления добавлены по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentStage()
    {
        return _enrollmentStage;
    }

    /**
     * @param enrollmentStage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentStage(String enrollmentStage)
    {
        dirty(_enrollmentStage, enrollmentStage);
        _enrollmentStage = enrollmentStage;
    }

    /**
     * @return Вид заявления.
     */
    @Length(max=255)
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма обучения.
     */
    @Length(max=255)
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Вид приема.
     */
    @Length(max=255)
    public String getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Вид приема.
     */
    public void setCompetitionType(String competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Филиал.
     */
    public String getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал.
     */
    public void setEnrOrgUnit(String enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, спец., профессия.
     */
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, спец., профессия.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Образовательная программа.
     */
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Набор образовательных программ.
     */
    public String getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор образовательных программ.
     */
    public void setProgramSet(String programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Группировать абитуриентов по направлениям приема на подразделении.
     */
    @Length(max=255)
    public String getSplitted()
    {
        return _splitted;
    }

    /**
     * @param splitted Группировать абитуриентов по направлениям приема на подразделении.
     */
    public void setSplitted(String splitted)
    {
        dirty(_splitted, splitted);
        _splitted = splitted;
    }

    /**
     * @return Поступающие параллельно.
     */
    public String getParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Поступающие параллельно.
     */
    public void setParallel(String parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrReportEntrantsForeignLangsGen)
        {
            setEnrollmentCampaign(((EnrReportEntrantsForeignLangs)another).getEnrollmentCampaign());
            setDateFrom(((EnrReportEntrantsForeignLangs)another).getDateFrom());
            setDateTo(((EnrReportEntrantsForeignLangs)another).getDateTo());
            setEnrollmentStage(((EnrReportEntrantsForeignLangs)another).getEnrollmentStage());
            setRequestType(((EnrReportEntrantsForeignLangs)another).getRequestType());
            setCompensationType(((EnrReportEntrantsForeignLangs)another).getCompensationType());
            setProgramForm(((EnrReportEntrantsForeignLangs)another).getProgramForm());
            setCompetitionType(((EnrReportEntrantsForeignLangs)another).getCompetitionType());
            setEnrOrgUnit(((EnrReportEntrantsForeignLangs)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrReportEntrantsForeignLangs)another).getFormativeOrgUnit());
            setProgramSubject(((EnrReportEntrantsForeignLangs)another).getProgramSubject());
            setEduProgram(((EnrReportEntrantsForeignLangs)another).getEduProgram());
            setProgramSet(((EnrReportEntrantsForeignLangs)another).getProgramSet());
            setSplitted(((EnrReportEntrantsForeignLangs)another).getSplitted());
            setParallel(((EnrReportEntrantsForeignLangs)another).getParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrReportEntrantsForeignLangsGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrReportEntrantsForeignLangs.class;
        }

        public T newInstance()
        {
            return (T) new EnrReportEntrantsForeignLangs();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "enrollmentStage":
                    return obj.getEnrollmentStage();
                case "requestType":
                    return obj.getRequestType();
                case "compensationType":
                    return obj.getCompensationType();
                case "programForm":
                    return obj.getProgramForm();
                case "competitionType":
                    return obj.getCompetitionType();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "eduProgram":
                    return obj.getEduProgram();
                case "programSet":
                    return obj.getProgramSet();
                case "splitted":
                    return obj.getSplitted();
                case "parallel":
                    return obj.getParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "enrollmentStage":
                    obj.setEnrollmentStage((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((String) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "programSet":
                    obj.setProgramSet((String) value);
                    return;
                case "splitted":
                    obj.setSplitted((String) value);
                    return;
                case "parallel":
                    obj.setParallel((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "enrollmentStage":
                        return true;
                case "requestType":
                        return true;
                case "compensationType":
                        return true;
                case "programForm":
                        return true;
                case "competitionType":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "eduProgram":
                        return true;
                case "programSet":
                        return true;
                case "splitted":
                        return true;
                case "parallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "enrollmentStage":
                    return true;
                case "requestType":
                    return true;
                case "compensationType":
                    return true;
                case "programForm":
                    return true;
                case "competitionType":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "eduProgram":
                    return true;
                case "programSet":
                    return true;
                case "splitted":
                    return true;
                case "parallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "enrollmentStage":
                    return String.class;
                case "requestType":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "programForm":
                    return String.class;
                case "competitionType":
                    return String.class;
                case "enrOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "programSet":
                    return String.class;
                case "splitted":
                    return String.class;
                case "parallel":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrReportEntrantsForeignLangs> _dslPath = new Path<EnrReportEntrantsForeignLangs>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrReportEntrantsForeignLangs");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEnrollmentStage()
     */
    public static PropertyPath<String> enrollmentStage()
    {
        return _dslPath.enrollmentStage();
    }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Вид приема.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getCompetitionType()
     */
    public static PropertyPath<String> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEnrOrgUnit()
     */
    public static PropertyPath<String> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getProgramSet()
     */
    public static PropertyPath<String> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Группировать абитуриентов по направлениям приема на подразделении.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getSplitted()
     */
    public static PropertyPath<String> splitted()
    {
        return _dslPath.splitted();
    }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getParallel()
     */
    public static PropertyPath<String> parallel()
    {
        return _dslPath.parallel();
    }

    public static class Path<E extends EnrReportEntrantsForeignLangs> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _enrollmentStage;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _competitionType;
        private PropertyPath<String> _enrOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _programSet;
        private PropertyPath<String> _splitted;
        private PropertyPath<String> _parallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrReportEntrantsForeignLangsGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrReportEntrantsForeignLangsGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEnrollmentStage()
     */
        public PropertyPath<String> enrollmentStage()
        {
            if(_enrollmentStage == null )
                _enrollmentStage = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_ENROLLMENT_STAGE, this);
            return _enrollmentStage;
        }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Вид приема.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getCompetitionType()
     */
        public PropertyPath<String> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEnrOrgUnit()
     */
        public PropertyPath<String> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getProgramSet()
     */
        public PropertyPath<String> programSet()
        {
            if(_programSet == null )
                _programSet = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Группировать абитуриентов по направлениям приема на подразделении.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getSplitted()
     */
        public PropertyPath<String> splitted()
        {
            if(_splitted == null )
                _splitted = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_SPLITTED, this);
            return _splitted;
        }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs#getParallel()
     */
        public PropertyPath<String> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<String>(EnrReportEntrantsForeignLangsGen.P_PARALLEL, this);
            return _parallel;
        }

        public Class getEntityClass()
        {
            return EnrReportEntrantsForeignLangs.class;
        }

        public String getEntityName()
        {
            return "enrReportEntrantsForeignLangs";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
