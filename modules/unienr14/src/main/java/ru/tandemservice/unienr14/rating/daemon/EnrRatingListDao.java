package ru.tandemservice.unienr14.rating.daemon;

import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitCategoryCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantAchievementKindCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrTieBreakRatingRuleCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings;

import java.nio.channels.IllegalSelectorException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

/**
 * @author vdanilov
 */
public class EnrRatingListDao extends UniBaseDao implements IEnrRatingListDao {

    protected Session lock() {
        return this.lock(EnrRatingItem.class.getName());
    }

    @Override
    public boolean doPrepareRatingItems()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingListDao.doPrepareRatingItems");
        try {
            final Session session = this.lock();
            final MutableInt updates = new MutableInt(0);

            final DQLSelectBuilder dataSource = new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "rc")
            .column(property(EnrRequestedCompetition.id().fromAlias("rc")), "id")
            .column(property(EnrRequestedCompetition.request().entrant().id().fromAlias("rc")), "entrant_id")
            .column(property(EnrRequestedCompetition.competition().id().fromAlias("rc")), "competition_id")
            .where(isNull(property(EnrRequestedCompetition.request().takeAwayDocumentDate().fromAlias("rc"))))
            .where(eq(property(EnrRequestedCompetition.request().entrant().archival().fromAlias("rc")), value(Boolean.FALSE)));

            // перебрасываем ссылку на активный выбранный конкурс, если вдруг не совпадает (признак корректности выставляется при смене номера)
            Debug.begin("update-link");
            try {
                final DQLUpdateBuilder updateDql = new DQLUpdateBuilder(EnrRatingItem.class)
                .fromDataSource(dataSource.buildQuery(), "ds")
                .where(eq(property(EnrRatingItem.entrant().id()), property("ds.entrant_id")))
                .where(eq(property(EnrRatingItem.competition().id()), property("ds.competition_id")))
                .where(ne(property(EnrRatingItem.L_REQUESTED_COMPETITION), property("ds.id"))) // только, если не совпадает
                .set(EnrRatingItem.L_REQUESTED_COMPETITION, property("ds.id"))
                .set(EnrRatingItem.P_VALUE_HASH, value(EntrantRatingData.NO_HASH)); // если что-то меняем, сбрасываем хэш

                updates.add(executeAndClear(updateDql, session));
            } finally {
                Debug.end();
            }

            // сбрасываем актуальность выбранных конкурсов (если они более неактуальны - ссылка уже переброшена)
            Debug.begin("clear-active");
            try {
                final DQLUpdateBuilder updateDql = new DQLUpdateBuilder(EnrRatingItem.class)
                .fromEntity(EnrRatingItem.class, "ri")
                .where(eq(property("ri.id"), property("id")))
                .where(or(
                    isNotNull(property(EnrRatingItem.requestedCompetition().request().takeAwayDocumentDate().fromAlias("ri"))),
                    eq(property(EnrRatingItem.requestedCompetition().request().entrant().archival().fromAlias("ri")), value(Boolean.TRUE))
                ))
                .where(eq(property(EnrRatingItem.P_REQUESTED_COMPETITION_ACTIVE), value(Boolean.TRUE))) // только, если не совпадает
                .set(EnrRatingItem.P_REQUESTED_COMPETITION_ACTIVE, value(Boolean.FALSE))
                .set(EnrRatingItem.P_VALUE_HASH, value(EntrantRatingData.NO_HASH)); // если что-то меняем, сбрасываем хэш

                updates.add(executeAndClear(updateDql, session));
            } finally {
                Debug.end();
            }

            // добавляем все, чего не хватает (добавляем неактивными, признак активности изменится при пересчете рейтинга)
            Debug.begin("insert-new");
            try {
                final List<Object[]> rows = new DQLSelectBuilder()
                .fromDataSource(dataSource.buildQuery(), "ds")
                .column(/*0*/property("ds.id"))
                .column(/*1*/property("ds.entrant_id"))
                .column(/*2*/property("ds.competition_id"))
                .where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EnrRatingItem.class, "x")
                    .column(property("x.id"))
                    .where(eq(property(EnrRatingItem.entrant().id().fromAlias("x")), property("ds.entrant_id")))
                    .where(eq(property(EnrRatingItem.competition().id().fromAlias("x")), property("ds.competition_id")))
                    .buildQuery()
                ))
                .createStatement(session)
                .list();


                // insert (предполагается, что кроме этого места EnrRatingItem не создается больше нигде, так что коллизий быть не должно)
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                try {
                    BatchUtils.execute(rows, 256, new BatchUtils.Action<Object[]>() {
                        private final EnrRatingItem fakeRatingItem = new EnrRatingItem();
                        private int position = Integer.MAX_VALUE; // глобальная уникальная, потом пересчитаем

                        @Override public void execute(final Collection<Object[]> rows) {

                            // здесь будем собирать результаты (это по крайней мере в 10 раз быстрее, чем hibernate-insert)
                            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(EnrRatingItem.class);

                            // добавляем данные
                            for (final Object[] row: rows) {

                                insertBuilder = insertBuilder
                                .valuesFrom(this.fakeRatingItem)
                                .value(EnrRatingItem.L_REQUESTED_COMPETITION, row[0])
                                .value(EnrRatingItem.L_ENTRANT, row[1])
                                .value(EnrRatingItem.L_COMPETITION, row[2])
                                .value(EnrRatingItem.P_REQUESTED_COMPETITION_ACTIVE, Boolean.FALSE)
                                .value(EnrRatingItem.P_VALUE_HASH, EntrantRatingData.NO_HASH)
                                .value(EnrRatingItem.P_POSITION_ABSOLUTE, Integer.MAX_VALUE)
                                .value(EnrRatingItem.P_POSITION, this.position-- /* глобальная уникальность, потом пересчитаем */)
                                .addBatch();
                            }

                            // сохраняем все в базу (внимание, здесь могут отработать не все записи)
                            final int count = insertBuilder.createStatement(session).execute();
                            if (count != rows.size()) { Debug.message("expected="+rows.size()+", count="+count); }
                            updates.add(count);
                        }
                    });

                    // чистим сессию
                    session.flush();
                    session.clear();

                } finally {
                    eventLock.release();
                }

            } finally {
                Debug.end();
            }

            return updates.intValue() > 0;

        } finally {
            Debug.end();
        }
    }

    @Override
    public List<EntrantRatingCompetitionData> getAllCompetitionIds()
    {
        Debug.begin("EnrRatingListDao.getAllCompetitionIds");
        try {

            final Session session = this.getSession();

            final List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "c")
                .where(exists(EnrRequestedCompetition.class, EnrRequestedCompetition.competition().s(), property("c")))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().open()), value(Boolean.TRUE)))
                .joinPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("c"), "ec")
                .joinEntity("c", DQLJoinType.left, EnrEntrantAchievementSettings.class, "s", and(
                    eq(property("c", EnrCompetition.requestType()), property("s", EnrEntrantAchievementSettings.requestType())),
                    eq(property("ec"), property("s", EnrEntrantAchievementSettings.enrollmentCampaign()))
                ))
                .column(/*0*/property(EnrCompetition.id().fromAlias("c")))
                .column(/*1*/property(EnrCompetition.requestType().code().fromAlias("c")))
                .column(/*2*/property(EnrCompetition.type().code().fromAlias("c")))
                .column(/*3*/property(EnrEntrantAchievementSettings.maxTotalAchievementMarkAsLong().fromAlias("s")))
                .column(/*4*/property(EnrEnrollmentCampaign.settings().tieBreakRatingRule().code().fromAlias("ec")))
                .column(/*5*/property(EnrCompetition.programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().code().fromAlias("c")))
                .createStatement(session).list();

            final List<EntrantRatingCompetitionData> result = new ArrayList<>(rows.size());
            for (final Object[] row: rows) {
                final EntrantRatingCompetitionData data = new EntrantRatingCompetitionData((Long)row[0], (String)row[1], (String)row[2], (String)row[4], (String) row[5]);

                if (row[3] == null)
                    throw new IllegalStateException(); // база неконсистентна. Настройки инд. достижений должны быть для всех видов заявлений и всех ПК.

                data.setTotalAchievementMarkLimit((Long) row[3]);
                result.add(data);
            }
            return result;
        } finally {
            Debug.end();
        }
    }

    @Override
    public List<EntrantRatingData> getRatingItemDataList(final EntrantRatingCompetitionData competitionData)
    {
        // поехали
        // Debug.stopLogging(); // здесь 4 одинаковых запросy - не надо их логировать
        try {
            // элементы настроенного набора ВИ для конкурса { examVariant.id -> index }
            final Map<Long, Integer> orderedExamIndexMap = this.getOrderedExamIndexMap(competitionData);

            // подготавливаем данные для сортировки
            final Map<Long, EntrantRatingData> ratingDataMap = this.getRatingDataList(competitionData, orderedExamIndexMap);
            final List<EntrantRatingData> ratingDataList = new ArrayList<EntrantRatingData>(ratingDataMap.values());

            // сортируем (глобально, без разницы, сданы ли все ВИ, зачислен ли абитуриент, сдал ли оригинал докобраза)
            final Comparator<EntrantRatingData> nonUniqComparator = this.buildNonUniqueComparator(competitionData, orderedExamIndexMap.size());

            // уникальный сортировщик (сортирует, сохраняя текущее положение элементов, если нет - то все новые в конец)
            final Comparator<EntrantRatingData> uniqComparator = new Comparator<EntrantRatingData>() {

                @Override
                public int compare(final EntrantRatingData o1, final EntrantRatingData o2)
                {
                    int i;

                    // сначала сортируем по правилам сортировки
                    if (0 != (i = nonUniqComparator.compare(o1, o2))) { return i; }

                    // затем, по текущему положению в базе
                    final int p1 = o1.getDatabaseIndex();
                    final int p2 = o2.getDatabaseIndex();
                    if (0 != (i = Integer.compare(p1, p2))) { return i; }

                    // если одинаковые, то сортируем по id (чтобы порядок был одинаковым)
                    // это нужно, чтобы объекты сортировались каждый раз одинаково
                    return o1.getRequestedCompetitionId().compareTo(o2.getRequestedCompetitionId());
                }
            };

            // сортируем
            Collections.sort(ratingDataList, uniqComparator);

            // выставляем номера
            {
                // данне для вычисления абсолютной позиции
                int positionUniqueNumber = 1;
                int positionNonUniqueNumber = 1;
                EntrantRatingData prev = (ratingDataList.isEmpty() ? null : ratingDataList.get(0));

                // сначала добавляем все, что есть в данных
                for (final EntrantRatingData data: ratingDataList) {

                    // пересчитываем абсолютную (неуникальную позицию), ее можно сохранять сразу же в базу
                    // если абитуриенты одинаковые с точки зрения рейтинга, оставляем абсолютный (неуникальный) номер тем же,
                    // иначе - увеличиваем на 1
                    {
                        final int nonUniqueCmp = nonUniqComparator.compare(prev, data);
                        if (nonUniqueCmp < 0) { positionNonUniqueNumber++; }
                        else if (nonUniqueCmp > 0) {
                            // записи должны следовать в порядке возрастания (по компаратору)
                            throw new IllegalStateException();
                        }
                        prev = data;
                    }

                    // выставляем номера
                    data.setPositionNonUnique(positionNonUniqueNumber);
                    data.setPositionUnique(positionUniqueNumber++);
                }
            }

            // размер
            //Debug.message("size="+ratingDataList.size());

            // возвращаем список
            return ratingDataList;
        } finally {
            // Debug.resumeLogging();
        }
    }

    // при каждом старте приложения будет новый базовый хэш для каждого часа - будет полный пересчет один раз в час при старте
    private static final long basehash_system_start = (System.currentTimeMillis()/1000/60/60);

    /**
     * обновляет поле databaseValueHash (на основе basehash_system_start и data.calculateValueHash)
     * @param dataList список элементов, полученных в {@link IEnrRatingListDao#getRatingItemDataList(EntrantRatingCompetitionData)}
     * @return список элементов, которые нужно обновить (пустой список, если обновлять нечего)
     */
    public static List<EntrantRatingData> setupValueHash(final List<EntrantRatingData> dataList)
    {
        final ArrayList<EntrantRatingData> updateList = new ArrayList<>();
        {
            final List<EntrantRatingData> possibleUpdateList = new ArrayList<>();

            // база хэша меняется каждые 12 часов (1 раз в 12 часов все записи принудительно пересчитываются)
            final long basehash_current_time = (System.currentTimeMillis()/1000/60/60/12);
            final long basehash = basehash_system_start ^ basehash_current_time;

            for (final EntrantRatingData data: dataList) {
                final long hash = data.calculateValueHash(basehash);
                if (hash != data.getDatabaseValueHash() || EntrantRatingData.NO_HASH == data.getDatabaseValueHash())
                {
                    // это существующий элемент, но с новым хэшем - обновляем ему хэш и сохраняем в список обновляемых
                    data.setDatabaseValueHash(hash);
                    updateList.add(data);
                }
                else
                {
                    // это существующий элемент, его хэш совпал - мы его откладываем (далее решим, что с ними делать)
                    data.setDatabaseValueHash(hash);
                    possibleUpdateList.add(data);
                }
            }

            // если изменения были, то все отложенные элементы принудительно обновляются
            // иначе, считаем, что весь список корректен (вероятность, что все элементы поменялись так, что их хэши не изменились - почти нулевая)
            if (updateList.size() > 0) {
                updateList.addAll(possibleUpdateList);
            }
        }
        updateList.trimToSize();
        return updateList;
    }


    private final DQLValuesStructure RATING_ITEM_UPDATE_STRUCT;
    {
        final Set<String> ignoreProperties = new HashSet<String>(Arrays.asList(EnrRatingItem.L_ENTRANT, EnrRatingItem.L_COMPETITION, EnrRatingItem.L_REQUESTED_COMPETITION));
        final IEntityMeta ratingItemMeta = EntityRuntime.getMeta(EnrRatingItem.class);
        final DQLValuesStructureBuilder ratingItemStructBuilder = new DQLValuesStructureBuilder();
        ratingItemStructBuilder.property(EntityBase.P_ID, PropertyType.LONG);
        for(final String propertyName : ratingItemMeta.getPropertyNames()) {
            if (ignoreProperties.contains(propertyName)) { continue; }
            final IPropertyMeta property = ratingItemMeta.getProperty(propertyName);
            if (ratingItemMeta.getSystemFields().contains(property)) { continue; }
            if (property instanceof IFieldPropertyMeta) {
                final IFieldPropertyMeta field = (IFieldPropertyMeta) property;
                if (field.isCalculatedField()) { continue; }
                if (!field.isMutable()) { continue; }
                ratingItemStructBuilder.property(propertyName, field.getPropertyType());
            }
        }
        this.RATING_ITEM_UPDATE_STRUCT = ratingItemStructBuilder.build();
    }


    @Override
    public boolean doSaveRatingList(final List<EntrantRatingData> updateList)
    {
        EnrEntrantDaemonBean.DAEMON.check();
        if (updateList.isEmpty()) { return false; }
        //Debug.begin("EnrRatingListDao.doSaveRatingList(id="+competitionId+", sz="+updateList.size()+")");
        try {
            final Session session = this.lock();

            // выставляем блокировку на логирование данных (их адово много)
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            try {
                // update
                BatchUtils.execute(updateList, 256, new BatchUtils.Action<EntrantRatingData>() {
                    @Override public void execute(final Collection<EntrantRatingData> elements) {

                        // здесь будем собирать результаты (это по крайней мере в 10 раз быстрее, чем hibernate-update)
                        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(EnrRatingListDao.this.RATING_ITEM_UPDATE_STRUCT);

                        // обновляем данные
                        int expected = 0;
                        for (final EntrantRatingData data: elements) {
                            final EnrRatingItem item = new EnrRatingItem();
                            item.setId(data.getRatingItemId());
                            item.setValueHash(data.getDatabaseValueHash());
                            if (EnrRatingListDao.this.updateRatingItem(item, data, session)) {
                                valuesBuilder.rowValuesFrom(item);
                                expected ++;
                            }
                        }

                        // формируем запрос
                        final DQLUpdateBuilder updateBuilder = new DQLUpdateBuilder(EnrRatingItem.class);
                        updateBuilder.fromDataSource(valuesBuilder.build(), "src");
                        updateBuilder.where(eq(property(EntityBase.P_ID), property("src", EntityBase.P_ID)));
                        for (final String property: EnrRatingListDao.this.RATING_ITEM_UPDATE_STRUCT.getProperties()) {
                            if (EntityBase.P_ID.equals(property)) { continue; }
                            updateBuilder.set(property, property("src", property));
                        }

                        // сохраняем все в базу (внимание, здесь могут отработать не все записи)
                        final int count = updateBuilder.createStatement(session).execute();
                        if (count != expected) { Debug.message("expected="+expected+", count="+count); }
                    }
                });

            } finally {
                eventLock.release();
            }

            return true;
        } finally {
            //Debug.end();
        }
    }

    protected boolean updateRatingItem(final EnrRatingItem item, final EntrantRatingData data, final Session session)
    {
        item.setRequestedCompetitionActive(data.isRequestedCompetitionActive());
        item.setPositionAbsolute(data.getPositionNonUnique());
        item.setPosition(data.getPositionUnique());
        item.setAchievementMarkAsLong(data.getAchievementMark());

        // XXXXX //
        // здесь можно сохранять в item всякие данные о сданных ВИ, сумме баллов и прочее счастье

        final boolean noExam = (null != data.getNoExamWrapper()) || 0 == data.getMarkArray().length || data.getCompetitionData().isNoExam();

        if (noExam)
        {
            // для без ВИ сразу true
            item.setStatusEntranceExamsCorrect(true);
            item.setStatusRatingComplete(true);
            item.setStatusRatingPositive(Boolean.TRUE);
        }
        else
        {
            // если есть ВИ, то начинаем шаманство
            final boolean entranceExamsCorrect = data.isAllExamPresent() && (!data.isDuplicateExamExists()) && (!data.isMarkError());
            item.setStatusEntranceExamsCorrect(entranceExamsCorrect);

            final boolean ratingComplete = data.isAllMarkPresent() && data.isAllMarkCompleteOrPassed() && (!data.isMarkError());
            item.setStatusRatingComplete(ratingComplete);

            if (entranceExamsCorrect)
            {
                // мы можем что-то говорить о рейтинге
                final boolean nonPassedCompleteMarkExists = data.isNonPassedCompleteMarkExists();
                if (nonPassedCompleteMarkExists)
                {
                    // если есть ВИ (со всеми сданными ВВИ-ф), по которому балл ниже порогового - то сразу false
                    item.setStatusRatingPositive(Boolean.FALSE);
                }
                else if (ratingComplete)
                {
                    // если рейтинг завершен (есть все оценки) - выводим true, если все оценки не ниже пороговых
                    // TODO: если nonPassedCompleteMarkExists=false и ratingComplete=true, то isNonPassedMarkExists должен быть false
                    // TODO: поэтому item.setStatusRatingPositive(Boolean.valueOf(!data.isNonPassedMarkExists())); можно заменить на Boolean.TRUE
                    item.setStatusRatingPositive(Boolean.valueOf(!data.isNonPassedMarkExists()));
                }
                else
                {
                    // иначе null - пока что не все сдано
                    item.setStatusRatingPositive(null);
                }
            }
            else
            {
                // рейтинг некорректен - null
                item.setStatusRatingPositive(null);
            }
        }

        if (noExam) {
            item.setTotalMarkAsLong(0L);
        } else {
            item.setTotalMarkAsLong(data.getMarkSum());
        }

        // XXXXX //

        return true;
    }

    @SuppressWarnings("serial")
    protected static Map<String, Integer> NO_EXAM_CODE_PRIORITY = new LinkedHashMap<String, Integer>() {{
        int n = 0;
        // члены сборных команд;
        this.put(EnrBenefitCategoryCodes.BEZ_V_I_CHLENY_SBORNYH_KOMAND, n++);

        // победители всероссийской олимпиады школьников;
        this.put(EnrBenefitCategoryCodes.BEZ_V_I_POBEDITELI_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV, n++);

        // призеры всероссийской олимпиады школьников;
        this.put(EnrBenefitCategoryCodes.BEZ_V_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV, n++);

        // чемпионы и призеры в области спорта;
        this.put(EnrBenefitCategoryCodes.BEZ_V_I_CHEMPIONY_I_PRIZERY_V_OBLASTI_SPORTA, n++);

        // победители олимпиад школьников;
        this.put(EnrBenefitCategoryCodes.BEZ_V_I_POBEDITELI_OLIMPIAD_SHKOLNIKOV, n++);

        // призеры олимпиад школьников.
        this.put(EnrBenefitCategoryCodes.BEZ_V_I_PRIZERY_OLIMPIAD_SHKOLNIKOV, n++);
    }};

    private static int safe(final Integer i) { return (null == i ? Integer.MAX_VALUE : i.intValue()); }

    /** @return правила сортировки (сортируем все, кого можно сортировать - по максимуму) */
    protected Comparator<EntrantRatingData> buildNonUniqueComparator(final EntrantRatingCompetitionData competitionData, final int examSetSize)
    {
        final String requestTypeCode = competitionData.getRequestTypeCode();
        final String eduProgramKindCode = competitionData.getEduProgramKindCode();

        // СПО
        if (EnrRequestTypeCodes.SPO.equals(requestTypeCode))
        {
            return (o1, o2) -> {
                // по убыванию среднего балла аттестата
                {
                    final int i = Long.compare(o1.getEduInstitutionAvgMark(), o2.getEduInstitutionAvgMark());
                    if (0 != i) { return -i; }
                }

                // по убыванию баллов за инд. достижения
                {
                    final int i = Long.compare(o1.getAchievementMark(), o2.getAchievementMark());
                    if (0 != i) { return -i; }
                }

                // если все одинаковое, возвращаем 0
                return 0;
            };
        }

        // БС
        if (EnrRequestTypeCodes.BS.equals(requestTypeCode))
        {
            final boolean noExam = competitionData.isNoExam();
            final boolean preepmtionEnabled = true;

            // БезВИ
            if (noExam) {
                return (o1, o2) -> {
                    final EntrantRatingData.NoExamWrapper noExam1 = o1.getNoExamWrapper();
                    final EntrantRatingData.NoExamWrapper noExam2 = o2.getNoExamWrapper();

                    // наличие данных БЕЗ-ВИ (если данных нет - в конец списка)
                    if (null == noExam1 || null == noExam2) {
                        return Boolean.compare(null == noExam1, null == noExam2);
                    }

                    // по типу БЕЗ-ВИ
                    {
                        final int p1 = safe(NO_EXAM_CODE_PRIORITY.get(noExam1.getBenefitCategoryCode()));
                        final int p2 = safe(NO_EXAM_CODE_PRIORITY.get(noExam2.getBenefitCategoryCode()));
                        final int i = Integer.compare(p1, p2);
                        if (0 != i) { return i; }
                    }

                    // по убыванию баллов за инд. достижения
                    {
                        final int i = Long.compare(o1.getAchievementMark(), o2.getAchievementMark());
                        if (0 != i) { return -i; }
                    }

                    // по преимущественному праву
                    {
                        final int i = Boolean.compare(o1.isPreemtive(), o2.isPreemtive());
                        if (0 != i) { return -i; }
                    }

                    // если все одинаковое, возвращаем 0
                    return 0;
                };
            }

            // обычная сотритовка (учитывая данные ВИ)
            return (o1, o2) -> {
                // по убыванию конкурсного балла (сумма баллов по ВИ + сумма ИД)
                {
                    final int i = Long.compare(o1.getMarkSum(), o2.getMarkSum());
                    if (0 != i) { return -i; }
                }

                // в зависимости от настройки ранжирования
                if (EnrTieBreakRatingRuleCodes.SUM_THEN_PRIORITY.equals(competitionData.getTieBreakRuleCode()) || EnrTieBreakRatingRuleCodes.SUM.equals(competitionData.getTieBreakRuleCode())) {
                    final int i = Long.compare(o1.getMarkSum() - o1.getAchievementMark(), o2.getMarkSum() - o2.getAchievementMark());
                    if (0 != i) { return -i; }
                }
                if (EnrTieBreakRatingRuleCodes.SUM_THEN_PRIORITY.equals(competitionData.getTieBreakRuleCode()) || EnrTieBreakRatingRuleCodes.PRIORITY.equals(competitionData.getTieBreakRuleCode())) {
                    for (int j=0; j<examSetSize; j++) {
                        final int i = Long.compare(o1.getMark(j), o2.getMark(j));
                        if (0 != i) { return -i; }
                    }
                }

                // по преимущественному праву
                {
                    final int i = Boolean.compare(o1.isPreemtive(), o2.isPreemtive());
                    if (0 != i) { return -i; }
                }

                // если все одинаковое, возвращаем 0
                return 0;
            };
        }

        // Одинатура, Интернатура
        if (EnrRequestTypeCodes.TRAINEESHIP.equals(requestTypeCode) ||
                EnrRequestTypeCodes.INTERNSHIP.equals(requestTypeCode) ||
            (EnrRequestTypeCodes.HIGHER.equals(requestTypeCode)
                        && (EduProgramKindCodes.PROGRAMMA_INTERNATURY.equals(eduProgramKindCode) || EduProgramKindCodes.PROGRAMMA_ORDINATURY.equals(eduProgramKindCode))))
        {
            return (o1, o2) -> {
                // по убыванию конкурсного балла (сумма баллов по ВИ, без баллов аттестата и индивидуальных достижений)
                {
                    final int i = Long.compare(o1.getMarkSum(), o2.getMarkSum());
                    if (0 != i) { return -i; }
                }

                // по убыванию среднего балла аттестата
                {
                    final int i = Long.compare(o1.getEduInstitutionAvgMark(), o2.getEduInstitutionAvgMark());
                    if (0 != i) { return -i; }
                }

                // по убыванию баллов за инд. достижения
                {
                    final int i = Long.compare(o1.getAchievementMark(), o2.getAchievementMark());
                    if (0 != i) { return -i; }
                }

                // если все одинаковое, возвращаем 0
                return 0;
            };
        }

        // Аспирантура
        if (EnrRequestTypeCodes.POSTGRADUATE.equals(requestTypeCode) ||
                (EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) && EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(eduProgramKindCode)))
        {
            return (o1, o2) -> {
                // по убыванию конкурсного балла (сумма баллов по ВИ, без баллов аттестата и индивидуальных достижений)
                {
                    final int i = Long.compare(o1.getMarkSum(), o2.getMarkSum());
                    if (0 != i) { return -i; }
                }

                // по баллам за первое ВИ (с наивысшим приоритетом) в настроенном наборе ВИ для набора ОП
                if(examSetSize > 0)
                {
                    final int i = Long.compare(o1.getMark(0), o2.getMark(0));
                    if (0 != i) { return -i; }
                }

                // по убыванию баллов за инд. достижения
                {
                    final int i = Long.compare(o1.getAchievementMark(), o2.getAchievementMark());
                    if (0 != i) { return -i; }
                }

                // если все одинаковое, возвращаем 0
                return 0;
            };
        }

        // Магистратура
        if (EnrRequestTypeCodes.MASTER.equals(requestTypeCode))
        {
            return (o1, o2) -> {
                // по убыванию конкурсного балла (сумма баллов по ВИ, без баллов аттестата и индивидуальных достижений)
                {
                    final int i = Long.compare(o1.getMarkSum(), o2.getMarkSum());
                    if (0 != i) { return -i; }
                }

                // по баллам по отдельным ВИ в порядке приоритета
                for (int j=0; j<examSetSize; j++) {
                    final int i = Long.compare(o1.getMark(j), o2.getMark(j));
                    if (0 != i) { return -i; }
                }

                // по убыванию баллов за инд. достижения
                {
                    final int i = Long.compare(o1.getAchievementMark(), o2.getAchievementMark());
                    if (0 != i) { return -i; }
                }

                // если все одинаковое, возвращаем 0
                return 0;
            };
        }

        // что-то неизвестное
        throw new IllegalStateException(competitionData.toString());
    }


    protected Session lockRatingList(final EnrCompetition competition) {
        return this.lock("ratingList."+competition.getId());
    }

    /** @return { examVariant.id -> index } order by examVariant.priority */
    protected Map<Long, Integer> getOrderedExamIndexMap(final EntrantRatingCompetitionData competitionData)
    {
        // exam.id -> index
        final Map<Long, Integer> orderedExamIndexMap = new HashMap<>();
        if (competitionData.isNoExam()) { return orderedExamIndexMap; }

        // exam.id order by exam.priority
        final List<Long> orderedExamIdList = new DQLSelectBuilder()
        .fromEntity(EnrCompetition.class, "c")
        .where(eq(property("c.id"), value(competitionData.getCompetitionId())))
        .fromEntity(EnrExamVariant.class, "e").column(property("e.id"))
        .where(eq(property(EnrExamVariant.examSetVariant().fromAlias("e")), property(EnrCompetition.examSetVariant().fromAlias("c"))))
        .order(property(EnrExamVariant.priority().fromAlias("e")))
        .order(property(EnrExamVariant.id().fromAlias("e")))
        .createStatement(this.getSession()).list();

        int index = 0;
        for (final Long examId: orderedExamIdList) {
            if (null != orderedExamIndexMap.put(examId, index++)) {
                throw new IllegalStateException();
            }
        }

        return orderedExamIndexMap;
    }

    /** @return { entrant.id -> EntrantRatingData} данные для рейтингового списка */
    protected Map<Long, EntrantRatingData> getRatingDataList(final EntrantRatingCompetitionData competitionData, final Map<Long, Integer> orderedExamIndexMap)
    {
        final Session session = this.getSession();
        final Long competitionId = competitionData.getCompetitionId();

        // entrant.id -> data
        final Map<Long, EntrantRatingData> resultMap = new HashMap<>();

        // requestedCompetition.id -> entrant.id (в рамках конкурса оно уникально)
        final Map<Long, Long> reqComp2EntrantMap = new HashMap<>();

        // по всем записям
        {
            final DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder()
                .fromEntity(EnrRatingItem.class, "ri")
                .joinPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().fromAlias("ri"), "rqc")
                .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("rqc"), "rq")
                .where(eq(property(EnrRequestedCompetition.competition().id().fromAlias("rqc")), value(competitionId)))
                .predicate(DQLPredicateType.distinct)
            );

            final int row_ri_id       = dql.column(property(EnrRatingItem.id().fromAlias("ri")));
            final int row_reqcomp_id  = dql.column(property(EnrRequestedCompetition.id().fromAlias("rqc")));
            final int row_entrant_id  = dql.column(property(EnrEntrantRequest.entrant().id().fromAlias("rq")));
            //int row_comp_id     = dql.column(property(EnrRequestedCompetition.competition().id().fromAlias("rq")));

            final int row_ri_valuehash = dql.column(property(EnrRatingItem.valueHash().fromAlias("ri")));
            final int row_ri_position  = dql.column(property(EnrRatingItem.position().fromAlias("ri")));

            final int row_req_takeaway_date = dql.column(property(EnrEntrantRequest.takeAwayDocumentDate().fromAlias("rq")));
            final int row_entrant_archival = dql.column(property(EnrEntrantRequest.entrant().archival().fromAlias("rq")));

            final int row_benefit_id    = dql.column(property(EnrEntrantRequest.benefitCategory().id().fromAlias("rq")));
            final int row_achieve_sum   = dql.column(property(EnrEntrantRequest.entrantAchievementMarkSum().fromAlias("rq")));
            final int row_institution_avg_mark = dql.column(property(EnrEntrantRequest.eduDocument().avgMarkAsLong().fromAlias("rq")));

            final Iterable<Object[]> rows = scrollRows(dql.getDql().createStatement(session));

            final int markSize = orderedExamIndexMap.size();
            for(final Object[] row: rows) {
                final Long ritemId   = (Long)row[row_ri_id];
                final Long reqCompId = (Long)row[row_reqcomp_id];
                final Long entrantId = (Long)row[row_entrant_id];
                //final Long compId    = (Long)row[row_comp_id];

                if (null != reqComp2EntrantMap.put(reqCompId, entrantId)) {
                    throw new IllegalSelectorException();
                }

                final EntrantRatingData data = new EntrantRatingData(competitionData, ritemId, reqCompId, markSize);
                if (null != resultMap.put(entrantId, data)) { throw new IllegalStateException(); }

                final Long databaseValueHash = (Long)row[row_ri_valuehash];
                data.setDatabaseValueHash(databaseValueHash);

                final Integer databaseIndex = (Integer)row[row_ri_position];
                data.setDatabaseIndex(databaseIndex);

                final boolean preemptive = (null != row[row_benefit_id]);
                data.setPreemtive(preemptive);

                final Long eduInstitutionAvgMarkObject = (Long)row[row_institution_avg_mark];
                final long eduInstitutionAvgMark = (null == eduInstitutionAvgMarkObject ? 0L : eduInstitutionAvgMarkObject);
                data.setEduInstitutionAvgMark(eduInstitutionAvgMark);

                long achievementSum = Math.min((Long) row[row_achieve_sum], competitionData.getTotalAchievementMarkLimit());
                data.setAchievementMark(achievementSum);

                if (EnrRequestTypeCodes.BS.equals(competitionData.getRequestTypeCode()) || EnrRequestTypeCodes.MASTER.equals(competitionData.getRequestTypeCode())) {
                    data.addAchievementSumToMarkSum();
                }

                final Date takeAwayRequestDate = (Date)row[row_req_takeaway_date];
                final boolean entrantArchival = Boolean.TRUE.equals(row[row_entrant_archival]);
                final boolean isActive = (null == takeAwayRequestDate) && !entrantArchival;
                data.setRequestedCompetitionActive(isActive);
            }
        }

        // признак «без ВИ» (таких мало - делаем отдельный запрос, но только если вид конкурса действительно БЕЗ-ВИ)
        if (competitionData.isNoExam()) {
            final Iterable<Object[]> rows = scrollRows(
                new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetitionNoExams.class, "rqc")
                .column(/*0*/property(EnrRequestedCompetitionNoExams.id().fromAlias("rqc")))
                .column(/*1*/property(EnrRequestedCompetitionNoExams.benefitCategory().code().fromAlias("rqc")))
                .where(eq(property(EnrRequestedCompetitionNoExams.competition().id().fromAlias("rqc")), value(competitionId)))
                .createStatement(session)
            );

            for(final Object[] row: rows) {
                final Long reqCompId = (Long)row[0];
                final Long entrantId = reqComp2EntrantMap.get(reqCompId);
                if (null == entrantId) { throw new IllegalStateException(); }

                final EntrantRatingData data = resultMap.get(entrantId);
                if (null == data) { throw new IllegalStateException(); }
                if (null != data.getNoExamWrapper()) { throw new IllegalStateException(); }

                final String benefitCategoryCode = (String)row[1];
                data.setNoExamWrapper(new EntrantRatingData.NoExamWrapper(benefitCategoryCode));
            }
        }

        // ВВИ и оценки по ним (этих очень много, и их не приджоинить к основному запросу)
        if (orderedExamIndexMap.size() > 0 && !competitionData.isNoExam())
        {
            final Iterable<Object[]> rows = scrollRows(
                new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExam.class, "exam")
                .column(/*0*/property(EnrChosenEntranceExam.requestedCompetition().id().fromAlias("exam")))
                .column(/*1*/property(EnrChosenEntranceExam.actualExam().id().fromAlias("exam")))
                .column(/*2*/property(EnrChosenEntranceExam.actualExam().passMarkAsLong().fromAlias("exam")))

                .joinPath(DQLJoinType.left, EnrChosenEntranceExam.maxMarkForm().fromAlias("exam"), "examForm")
                .joinPath(DQLJoinType.left, EnrChosenEntranceExamForm.markSource().fromAlias("examForm"), "markSource")
                .column(/*3*/property(EnrEntrantMarkSource.markAsLong().fromAlias("markSource")))

                .column(/*4*/property(EnrChosenEntranceExam.statusMaxMarkComplete().fromAlias("exam")))
                .where(eq(property(EnrChosenEntranceExam.requestedCompetition().competition().id().fromAlias("exam")), value(competitionId)))
                .createStatement(session)
            );

            for (final Object[] row: rows)
            {
                final Long reqCompId = (Long)row[0];
                final Long entrantId = reqComp2EntrantMap.get(reqCompId);
                if (null == entrantId) { continue; }

                final EntrantRatingData data = resultMap.get(entrantId);
                if (null == data) { continue; }

                final Long actualExamId = (Long)row[1];
                final Integer markIndex = orderedExamIndexMap.get(actualExamId);
                if (null == markIndex) {
                    // ВВИ нет в наборе - регистрируем ошибку, оценку игнорируем
                    data.registerMarkError();
                    continue;
                }

                // регистритуем ВВИ
                data.registerExam(markIndex);

                Long markAsLong = (Long)row[3];
                Boolean statusMaxMarkComplete = (Boolean)row[4];

                // если это null, значит есть ВВИ, но оценки по нему нет
                if (null != markAsLong)
                {
                    // если оценки заполнены с ошибками - обнуляем и регистрируем ошибку
                    if (markAsLong < 0) {
                        markAsLong = 0L;
                        data.registerMarkError();
                    }

                    Long passMarkAsLong = (Long)row[2];
                    // оно гарантированно не null, а если null - то это ошибка
                    // если оценки заполнены с ошибками - обнуляем и регистрируем ошибку
                    if ((null == passMarkAsLong) || (passMarkAsLong < 0)) {
                        passMarkAsLong = 0L;
                        data.registerMarkError();
                    }

                    // регистрируем оценку
                    boolean mark_pass = (markAsLong >= passMarkAsLong);
                    boolean mark_complete = Boolean.TRUE.equals(statusMaxMarkComplete);
                    data.registerMark(markIndex, markAsLong, mark_pass, mark_complete);
                }

            }
        }

        // возвращаем результат
        return resultMap;
    }

}
