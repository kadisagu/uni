/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.Export;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.EnrStateExamResultManager;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author azhebko
 * @since 11.06.2014
 */
@Configuration
public class EnrStateExamResultExport extends BusinessComponentManager
{
    public final static String IDENTITY_CARD_TYPE_DS = "icTypeDS";
    public static final String ENR_ORDER_DS = "enrOrderDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(IDENTITY_CARD_TYPE_DS, EnrStateExamResultManager.instance().icTypeDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ENR_ORDER_DS, getName(), EnrOrder.defaultSelectDSHandler(getName())))
                .create();
    }
}