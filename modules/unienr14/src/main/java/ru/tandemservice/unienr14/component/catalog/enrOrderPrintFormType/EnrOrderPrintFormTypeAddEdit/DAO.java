/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypeAddEdit;

import com.google.common.collect.ImmutableList;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;

/**
 * @author azhebko
 * @since 14.07.2014
 */
public class DAO extends DefaultScriptCatalogAddEditDAO<EnrOrderPrintFormType, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setOrderTypeModel(new LazySimpleSelectModel<>(ImmutableList.of(
                this.getByNaturalId(new EnrOrderType.NaturalId(EnrOrderTypeCodes.ENROLLMENT)),
                this.getByNaturalId(new EnrOrderType.NaturalId(EnrOrderTypeCodes.CANCEL)))
        ).setSearchFromStart(false));
    }

    @Override
    protected boolean isAddingScriptAllowed(){ return true; }

    @Override
    public void update(Model model)
    {
        if (model.isAddForm())
            model.getCatalogItem().setCatalogCode(this.getByCode(EnrOrderPrintFormType.class, EnrOrderPrintFormTypeCodes.BASE_ENROLLMENT).getCatalogCode());

        super.update(model);
    }
}