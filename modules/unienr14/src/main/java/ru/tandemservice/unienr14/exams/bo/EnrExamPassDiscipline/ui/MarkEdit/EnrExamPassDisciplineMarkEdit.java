/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.MarkEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;

/**
 * @author oleyba
 * @since 5/9/13
 */
@Configuration
public class EnrExamPassDisciplineMarkEdit extends BusinessComponentManager
{
    public static final String DS_ABSENCE_NOTE = "absenceNoteDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS(DS_ABSENCE_NOTE, absenceNoteSelectDSHandler()))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> absenceNoteSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAbsenceNote.class)
            .order(EnrAbsenceNote.code());
    }
}