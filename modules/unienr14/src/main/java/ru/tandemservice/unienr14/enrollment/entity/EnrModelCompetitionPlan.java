package ru.tandemservice.unienr14.enrollment.entity;

import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrPlanZeroFixer;
import ru.tandemservice.unienr14.enrollment.entity.gen.*;

/** @see ru.tandemservice.unienr14.enrollment.entity.gen.EnrModelCompetitionPlanGen */
public class EnrModelCompetitionPlan extends EnrModelCompetitionPlanGen implements EnrPlanZeroFixer.IPlanOwner
{
    public void updatePlanFromBase()
    {
        setPlan(getStep().getKind().isUsePercentage() ? (int) Math.ceil(0.0001 * getStep().getPercentageAsLong() * getBasePlan()) : getBasePlan());
    }

    @Override
    public String getEduLevelReqCode()
    {
        return getCompetition().getEduLevelRequirement().getCode();
    }

    @Override
    public String getCompTypeCode()
    {
        return getCompetition().getType().getCode();
    }

    @Override
    public Object planBalanceKey()
    {
        return getCompetition().getType().getCode();
    }

    @Override
    public int getFixPlan()
    {
        return getBasePlan();
    }

    @Override
    public void setFixPlan(int plan)
    {
        setBasePlan(plan);
    }
}