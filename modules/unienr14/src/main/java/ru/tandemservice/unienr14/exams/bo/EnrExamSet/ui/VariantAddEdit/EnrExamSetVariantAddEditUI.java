/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.VariantAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.EnrExamSetDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.ExamSetVariantKind;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/5/14
 */
@Input({
    @Bind(key = EnrExamSetVariantAddEditUI.BIND_OWNER_ID, binding = "ownerId", required = true),
    @Bind(key = EnrExamSetVariantAddEditUI.BIND_VARIANT_KIND, binding = "variantKind", required = true)
})
public class EnrExamSetVariantAddEditUI extends UIPresenter
{
    public static final String BIND_VARIANT_KIND = "variantKind";
    public static final String BIND_OWNER_ID = "ownerId";

    private Long ownerId;
    private ExamSetVariantKind variantKind;

    private EnrProgramSetBase owner;

    private EnrExamSet examSet;

    private DynamicListDataSource<EnrExamSet> examSetDS;

    private ISelectModel examSetFilterModel;

    private List<ExamSetFilter> examSetFilterList;
    private ExamSetFilter currentExamSetFilter;

    private boolean selectionInProcess = false;

    private EnrExamSetDao.IExamSetSettings examSetSettings;
    private EnrExamSetDao.IExamSetElementSettings currentExamSetElementSettings;

    private List<EnrExamPassForm> passFormList;
    private EnrExamPassForm currentPassForm;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setOwner(IUniBaseDao.instance.get().getNotNull(EnrProgramSetBase.class, getOwnerId()));

        setPassFormList(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(EnrExamPassForm.class));

        initExamSetFilters();
        initExamSetDS();

        EnrExamSetVariant currentVariant = getVariantKind().getVariant(getOwner());
        if (currentVariant != null) {
            setExamSet(currentVariant.getExamSet());
            setExamSetSettings(EnrExamSetManager.instance().dao().getExamSetContent(Collections.singleton(currentVariant.getId())).get(currentVariant.getId()));
        }
        else {
            setSelectionInProcess(true);
        }
    }

    public void onClickSelect() {
        EnrExamSet selectedSet = (EnrExamSet) ((RadioButtonColumn) getExamSetDS().getColumn(0)).getSelectedEntity();
        if (selectedSet == null) {
            throw new ApplicationException("Выберите набор ВИ для настройки.");
        }

        getVariantKind().validateSelection(getOwner(), selectedSet);

        if (getExamSet() == null) {
            onClickSelectNull();
        }

        setExamSet(selectedSet);
        setSelectionInProcess(false);
        if (getExamSetSettings() == null || getExamSetSettings().getExamSet() == null || !getExamSetSettings().getExamSet().equals(getExamSet())) {
            setExamSetSettings(EnrExamSetManager.instance().dao().getNewExamSettings(getExamSet()));
        }
    }

    public void onClickSelectNull() {
        setExamSet(null);
        setExamSetSettings(null);
        setSelectionInProcess(false);
    }

    public void onClickStartSelection() {
        setExamSet(null);
        setExamSetSettings(null);
        setSelectionInProcess(true);
        //        if (getExamSet() != null) {
        //        }
    }

    public void onClickApply() {
        if (getExamSetSettings() != null) {

            for (EnrExamSetDao.IExamSetElementSettings elementSettings : getExamSetSettings().getElementList()) {
                if (!CollectionUtils.exists(elementSettings.getPassFormMap().values(), use -> use)) {
                    throw new ApplicationException("Для вступительного испытания «" + elementSettings.getElement().getTitle() + "» не выбрана ни одна форма сдачи: необходимо указать хотя бы одну разрешенную форму.");
                }
            }

            EnrExamSetManager.instance().dao().saveExamSetSettings(getExamSetSettings(), getOwner(), getVariantKind());
        }
        deactivate();
    }

    // presenter

    public boolean isExamSetNotSelected() {
        return getExamSet() == null;
    }

    public boolean isExamSetEmpty() {
        return getExamSet() != null && getExamSetSettings() != null && getExamSetSettings().getElementList().isEmpty();
    }

    public boolean isCurrentEntityPassFormEnabled() {
        return getCurrentExamSetElementSettings().getPassFormMap().get(getCurrentPassForm());
    }

    public void setCurrentEntityPassFormEnabled(boolean enabled) {
        getCurrentExamSetElementSettings().getPassFormMap().put(getCurrentPassForm(), enabled);
    }

    public String getPassFormInputId() {
        return "passMark." + getCurrentExamSetElementSettings().getElement().getId().hashCode();
    }

    public String getPassMarkInputId() {
        return "passForm." + getCurrentExamSetElementSettings().getElement().getId().hashCode() + "." + getCurrentPassForm().getCode();
    }

    public String getPriorityInputId() {
        return "priority." + getCurrentExamSetElementSettings().getElement().getId().hashCode() + "." + getCurrentPassForm().getCode();
    }

    // utils

    private void initExamSetFilters()
    {
        if (null != getExamSetFilterModel())
            return;

        Set<IdentifiableWrapper<IEntity>> examSetElementValueWrappers = new HashSet<>();
        for (EnrExamSetElement element : IUniBaseDao.instance.get().getList(EnrExamSetElement.class, EnrExamSetElement.examSet().enrollmentCampaign(), getOwner().getEnrollmentCampaign())) {
            examSetElementValueWrappers.add(new IdentifiableWrapper<>(element.getValue().getId(), element.getValue().getTitle()));
        }
        List<IdentifiableWrapper<IEntity>> wrapperList = new ArrayList<>(examSetElementValueWrappers);
        Collections.sort(wrapperList, ITitled.TITLED_COMPARATOR);
        setExamSetFilterModel(new LazySimpleSelectModel<>(wrapperList));

        setExamSetFilterList(new ArrayList<>());
        for (int i = 1; i <=3; i++)
            getExamSetFilterList().add(new ExamSetFilter(i));
    }

    private void initExamSetDS()
    {
        if (null != getExamSetDS())
            return;

        DynamicListDataSource<EnrExamSet> dataSource = new DynamicListDataSource<>(this, component -> {
            DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrExamSet.class, "s")
            .where(eq(property(EnrExamSet.enrollmentCampaign().fromAlias("s")), value(getOwner().getEnrollmentCampaign())))
            .column("s")
            ;

            List<Long> filterValueIds = new ArrayList<>();
            for (ExamSetFilter filter : getExamSetFilterList()) {
                if (filter.getValue() != null) {
                    filterValueIds.add(filter.getValue().getId());
                }
            }

            for (Long filterId : filterValueIds) {
                dql.where(exists(new DQLSelectBuilder()
                .fromEntity(EnrExamSetElement.class, "e")
                .where(eq(property(EnrExamSetElement.value().id().fromAlias("e")), value(filterId)))
                .where(eq(property(EnrExamSetElement.examSet().id().fromAlias("e")), property("s.id")))
                .column(property(EnrExamSetElement.examSet().id().fromAlias("e")))
                .buildQuery()));
            }

            List<EnrExamSet> list = IUniBaseDao.instance.get().getList(dql);
            final Map<EnrExamSet, List<EnrExamSetElement>> examSet2ElementsMap = EnrExamSetManager.instance().dao().examSet2ElementsMap(list.toArray(new EnrExamSet[list.size()]));
            Collections.sort(list, EnrExamSet.getComparator(examSet2ElementsMap));
            UniBaseUtils.createPage(getExamSetDS(), list);
        });

        dataSource.addColumn(new RadioButtonColumn());

        dataSource.addColumn(new SimpleColumn("Набор ВИ", EnrExamSet.P_ELEMENTS_TITLE).setFormatter(NewLineFormatter.NOBR_IN_LINES).setOrderable(false).setClickable(false));

        dataSource.setCountRow(10);
        setExamSetDS(dataSource);
    }

    // getters and setters

    public EnrProgramSetBase getOwner()
    {
        return owner;
    }

    public void setOwner(EnrProgramSetBase owner)
    {
        this.owner = owner;
    }

    public Long getOwnerId()
    {
        return ownerId;
    }

    public void setOwnerId(Long ownerId)
    {
        this.ownerId = ownerId;
    }

    public EnrExamSet getExamSet()
    {
        return examSet;
    }

    public void setExamSet(EnrExamSet examSet)
    {
        this.examSet = examSet;
    }

    public DynamicListDataSource<EnrExamSet> getExamSetDS()
    {
        return examSetDS;
    }

    public void setExamSetDS(DynamicListDataSource<EnrExamSet> examSetDS)
    {
        this.examSetDS = examSetDS;
    }

    public ISelectModel getExamSetFilterModel()
    {
        return examSetFilterModel;
    }

    public void setExamSetFilterModel(ISelectModel examSetFilterModel)
    {
        this.examSetFilterModel = examSetFilterModel;
    }

    public List<ExamSetFilter> getExamSetFilterList()
    {
        return examSetFilterList;
    }

    public void setExamSetFilterList(List<ExamSetFilter> examSetFilterList)
    {
        this.examSetFilterList = examSetFilterList;
    }

    public ExamSetFilter getCurrentExamSetFilter()
    {
        return currentExamSetFilter;
    }

    public void setCurrentExamSetFilter(ExamSetFilter currentExamSetFilter)
    {
        this.currentExamSetFilter = currentExamSetFilter;
    }

    public boolean isSelectionInProcess()
    {
        return selectionInProcess;
    }

    public void setSelectionInProcess(boolean selectionInProcess)
    {
        this.selectionInProcess = selectionInProcess;
    }

    public IEnrExamSetDao.IExamSetSettings getExamSetSettings()
    {
        return examSetSettings;
    }

    public void setExamSetSettings(IEnrExamSetDao.IExamSetSettings examSetSettings)
    {
        this.examSetSettings = examSetSettings;
    }

    public IEnrExamSetDao.IExamSetElementSettings getCurrentExamSetElementSettings()
    {
        return currentExamSetElementSettings;
    }

    public void setCurrentExamSetElementSettings(IEnrExamSetDao.IExamSetElementSettings currentExamSetElementSettings)
    {
        this.currentExamSetElementSettings = currentExamSetElementSettings;
    }

    public List<EnrExamPassForm> getPassFormList()
    {
        return passFormList;
    }

    public void setPassFormList(List<EnrExamPassForm> passFormList)
    {
        this.passFormList = passFormList;
    }

    public EnrExamPassForm getCurrentPassForm()
    {
        return currentPassForm;
    }

    public void setCurrentPassForm(EnrExamPassForm currentPassForm)
    {
        this.currentPassForm = currentPassForm;
    }

    public ExamSetVariantKind getVariantKind()
    {
        return variantKind;
    }

    public void setVariantKind(ExamSetVariantKind variantKind)
    {
        this.variantKind = variantKind;
    }

    // inner classes

    public class ExamSetFilter
    {
        private int id;
        private IdentifiableWrapper<IEntity> value;

        private ExamSetFilter(int id) { this.id = id; }
        public IdentifiableWrapper<IEntity> getValue() { return value; }
        public void setValue(IdentifiableWrapper<IEntity> value) { this.value = value; }
        public String getId() { return "currentExamSetFilter" + id; }
    }

    public class VariantUsage extends IdentifiableWrapper
    {
        private String propertyName;
        private boolean use;

        private VariantUsage(Long id, String title, String fieldName, boolean use) {
            super(id, title);
            propertyName = fieldName;
            this.use = use;
        }

        public String getPropertyName() { return propertyName; }
        public boolean isUse() { return use; }
        public void setUse(boolean use) { this.use = use; }
    }
}
