/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.StudentEnrollment;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.competition.entity.gen.IEnrollmentStudentSettingsGen;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/15/14
 */
public class EnrStudentEnrollmentSettingsDsHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EnrStudentEnrollmentSettingsDsHandler(String ownerId)
    {
        super(ownerId, IEnrollmentStudentSettingsGen.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(IEnrollmentStudentSettings.class, "i");
        final DQLSelectBuilder builder = orderDescriptionRegistry.buildDQLSelectBuilder().column("i")
            .joinPath(DQLJoinType.inner, IEnrollmentStudentSettingsGen.programSet().fromAlias("i"), "s")
            .joinPath(DQLJoinType.inner, IEnrollmentStudentSettingsGen.orgUnit().fromAlias("i"), "ou");

        final EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final EnrRequestType requestType = context.get(EnrSettingsStudentEnrollment.BIND_REQUEST_TYPE);
        final EduProgramForm programForm = context.get(EnrSettingsStudentEnrollment.BIND_PROGRAM_FORM);
        final List<EnrOrgUnit> orgUnits = context.get(EnrSettingsStudentEnrollment.BIND_ORG_UNITS);
        final List<EduProgramSubject> programSubjects = context.get(EnrSettingsStudentEnrollment.BIND_PROGRAM_SUBJECTS);
        final String subjectCode = context.get(EnrSettingsStudentEnrollment.BIND_SUBJECT_CODE);
        final String title = context.get(EnrSettingsStudentEnrollment.BIND_PROGRAM_SET_TITLE);

        builder.where(eq(property("s", EnrProgramSetBase.enrollmentCampaign()), value(campaign)));

        if (orgUnits != null && !orgUnits.isEmpty()) {
            builder.where(in(property("ou"), orgUnits));
        }

        if (programSubjects != null && !programSubjects.isEmpty()) {
            builder.where(in(property("s", EnrProgramSetBase.programSubject()), programSubjects));
        }

        if (programForm != null) {
            builder.where(eq(property("s", EnrProgramSetBase.programForm()), value(programForm)));
        }

        if (requestType == null) {
            builder.where(nothing());
        }
        else {
            builder.joinEntity("s", DQLJoinType.inner, requestType.getProgramSetClass(), "e", eq(property("e", EnrProgramSetBase.id()), property("s", EnrProgramSetBase.id())));
            builder.where(exists(
                    EnrRequestType2eduProgramKindRel.class,
                    EnrRequestType2eduProgramKindRel.requestType().s(), requestType,
                    EnrRequestType2eduProgramKindRel.programKind().s(), property("s", EnrProgramSetBase.programSubject().subjectIndex().programKind())
            ));
        }

        FilterUtils.applySimpleLikeFilter(builder, "s", EnrProgramSetBase.programSubject().code(), subjectCode);
        FilterUtils.applySimpleLikeFilter(builder, "s", EnrProgramSetBase.title(), title);

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(orderDescriptionRegistry).pageable(true).build();

         wrap(output, context);

        return output;
    }

    protected List<DataWrapper> wrap(final DSOutput output, ExecutionContext context)
    {
        for (DataWrapper wrapper : DataWrapper.wrap(output)) {
            wrapper.set("disabled", ((IEnrollmentStudentSettings)wrapper.getWrapped()).getEducationOrgUnit() == null);
        }

        return output.getRecordList();
    }


}

