package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrReportSelectedEduProgramsSummary

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("enr14_rep_sel_edu_programs_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                    new DBColumn("datefrom_p", DBType.TIMESTAMP).setNullable(false),
                    new DBColumn("dateto_p", DBType.TIMESTAMP).setNullable(false),
                    new DBColumn("stage_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("requesttype_p", DBType.TEXT).setNullable(false),
                    new DBColumn("compensationtype_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("programform_p", DBType.TEXT).setNullable(false),
                    new DBColumn("competitiontype_p", DBType.TEXT),
                    new DBColumn("enrorgunit_p", DBType.TEXT),
                    new DBColumn("formativeorgunit_p", DBType.TEXT),
                    new DBColumn("programsubject_p", DBType.TEXT),
                    new DBColumn("eduprogram_p", DBType.TEXT),
                    new DBColumn("programset_p", DBType.TEXT),
                    new DBColumn("parallel_p", DBType.createVarchar(255)),
                    new DBColumn("rowformingtype_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("includedetailsheets_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("firstpriorityonly_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrReportSelectedEduProgramsSummary");

        }


    }
}