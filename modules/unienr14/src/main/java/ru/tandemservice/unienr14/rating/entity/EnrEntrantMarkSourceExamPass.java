package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unienr14.rating.entity.gen.*;

/**
 * Основание балла (балл по ДДС) (2013)
 *
 * Содзается и обновляется демоном на основе ДДС абитуриента IEnrRatingDaemonDao#doRefreshSource4ExamPass (создание оснований по данным ДДС абитуриента).
 */
public class EnrEntrantMarkSourceExamPass extends EnrEntrantMarkSourceExamPassGen
{
    public EnrEntrantMarkSourceExamPass() {}

    public EnrEntrantMarkSourceExamPass(EnrChosenEntranceExamForm chosenEntranceExamForm, EnrExamPassDiscipline examPassDiscipline) {
        this.setChosenEntranceExamForm(chosenEntranceExamForm);
        this.setExamPassDiscipline(examPassDiscipline);
    }

    public EnrEntrantMarkSourceExamPass(EnrChosenEntranceExamForm chosenEntranceExamForm, EnrExamPassDiscipline examPassDiscipline, long markAsLong) {
        this(chosenEntranceExamForm, examPassDiscipline);
        this.setMarkAsLong(markAsLong);
    }

    @Override
    public String getInfoString()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getExamPassDiscipline().getMarkAsDouble()) + " — " + getExamPassDiscipline().getPassForm().getFullTitle() + (this.getExamPassDiscipline().isRetake() ? ", пересдача" : "");
    }

    @Override
    public String getDocumentTitleForPrint()
    {
        return ""; // todo DEV-2931
    }
}