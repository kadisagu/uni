/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrWSExport.ui.Orders;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author oleyba
 * @since 7/20/14
 */
@Configuration
public class EnrWSExportOrders extends BusinessComponentManager
{
    public static final String ENR_ENROLLMENT_CAMPAIGN_DS = "enrEnrollmentCampaignDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS(ENR_ENROLLMENT_CAMPAIGN_DS, EnrEnrollmentCampaignManager.instance().enrCampaignDSHandler()))
            .create();
    }
}