/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsSecondFormRegJournalAdd.logic;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsSecondFormRegJournalAdd.EnrReportEntrantsSecondFormRegJournalAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.OrgUnitPrintTiltleComparator;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.RtfBackslashScreener;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsSecondFormRegJournal;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author rsizonenko
 * @since 25.06.2014
 */
public class EnrReportEntrantsSecondFormRegJournalDao extends UniBaseDao implements IEnrReportEntrantsSecondFormRegJournalDao {
    @Override
    public long createReport(EnrReportEntrantsSecondFormRegJournalAddUI model) {
        EnrReportEntrantsSecondFormRegJournal report = new EnrReportEntrantsSecondFormRegJournal();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsSecondFormRegJournal.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantsSecondFormRegJournal.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsSecondFormRegJournal.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantsSecondFormRegJournal.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsSecondFormRegJournal.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsSecondFormRegJournal.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantsSecondFormRegJournal.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantsSecondFormRegJournal.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantsSecondFormRegJournal.P_PROGRAM_SET, "title");
        DatabaseFile content = new DatabaseFile();



        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantsSecondFormRegJournal.zip");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_ZIP);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }
    
    private byte[] buildReport(EnrReportEntrantsSecondFormRegJournalAddUI model)
    {

        // rtf template
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_SECOND_DORM_REG_JOURNAL);

        // output stream for zip
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();

        // zip
        ZipOutputStream zos = new ZipOutputStream(bytesOut);



        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL
                .column(property("reqComp"));

        List<EnrRequestedCompetition> fromQueryRequestedCompetitions = getList(requestedCompDQL);

        requestedCompDQL
                .resetColumns()
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property("reqComp", EnrRequestedCompetition.id()), property("vvi", EnrChosenEntranceExam.requestedCompetition().id())))
                .joinEntity("vvi", DQLJoinType.inner, EnrChosenEntranceExamForm.class, "vvif", eq(property("vvi"), property(EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("vvif"))))
                .column(property("vvif"));

        List<EnrChosenEntranceExamForm> fromQueryVviList = getList(requestedCompDQL);

        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExamForm>> vviMap = new HashMap<>();

        for (EnrChosenEntranceExamForm examForm : fromQueryVviList)
        {
            if (!vviMap.containsKey(examForm.getChosenEntranceExam().getRequestedCompetition()))
                vviMap.put(examForm.getChosenEntranceExam().getRequestedCompetition(), new TreeSet<EnrChosenEntranceExamForm>(new Comparator<EnrChosenEntranceExamForm>() {
                    @Override
                    public int compare(EnrChosenEntranceExamForm o1, EnrChosenEntranceExamForm o2) {
                        return o1.getChosenEntranceExam().getDiscipline().getDiscipline().getTitle().compareTo(o2.getChosenEntranceExam().getDiscipline().getDiscipline().getTitle());
                    }
                }));
            vviMap.get(examForm.getChosenEntranceExam().getRequestedCompetition()).add(examForm);
        }

        // comp-s
        Comparator<OrgUnit> topDependentOrgUnitComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                int i = Boolean.compare(null == o1.getParent(), null == o2.getParent());
                if (0 != i) { return -i; }

                return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
            }
        };

        Comparator<EduProgramSubject> subjComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };

        Comparator<EduProgramForm> formComparator = new Comparator<EduProgramForm>() {
            @Override
            public int compare(EduProgramForm o1, EduProgramForm o2) {
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };

        Comparator<EnrRequestedCompetition> requestedCompetitionComparator = new Comparator<EnrRequestedCompetition>() {
            @Override
            public int compare(EnrRequestedCompetition o1, EnrRequestedCompetition o2) {

                int res = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getRequest().getIdentityCard().getFullFio(), o2.getRequest().getIdentityCard().getFullFio());
                if (res != 0) return res;


                res = Boolean.compare(o1.getCompetition().getType().getCompensationType().isBudget(), o2.getCompetition().getType().getCompensationType().isBudget());
                if (res != 0) return -res;

                EnrBenefitCategory category1 = o1.getRequest().getBenefitCategory();
                EnrBenefitCategory category2 = o2.getRequest().getBenefitCategory();
                // Если закончились предметы сравнения - сравниваем по ID, чтобы не терялись выбранные конкурсы
                if (category1 == category2) { return o1.getId().compareTo(o2.getId()); }
                if (null == category1) { return 1; }
                if (null == category2) { return -1; }

                res = o1.getRequest().getBenefitCategory().getBenefitType().getTitle().compareToIgnoreCase(o2.getRequest().getBenefitCategory().getBenefitType().getTitle());
                if (res != 0) return res;
                // Если закончились предметы сравнения - сравниваем по ID, чтобы не терялись выбранные конкурсы
                return o1.getId().compareTo(o2.getId());
            }
        };





        Map<OrgUnit, Map<OrgUnit, Map<EduProgramForm, Map<EduProgramSubject, Set<EnrRequestedCompetition>>>>> dataMap = new TreeMap<>(topDependentOrgUnitComparator);

        // Forming dataMap: OrgUnit->Formative->EduProgram->EduProgSubj->Set<requestedComps>
        for (EnrRequestedCompetition reqComp  : fromQueryRequestedCompetitions)
        {
            OrgUnit orgUnit = reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
            OrgUnit formative = reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
            EduProgramForm programForm = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm();
            EduProgramSubject programSubject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();

            Map<OrgUnit, Map<EduProgramForm, Map<EduProgramSubject, Set<EnrRequestedCompetition>>>> orgUnitMap = SafeMap.safeGet(dataMap, orgUnit, TreeMap.class, new OrgUnitPrintTiltleComparator());
            Map<EduProgramForm, Map<EduProgramSubject, Set<EnrRequestedCompetition>>> eduProgramFormMap = SafeMap.safeGet(orgUnitMap, formative, TreeMap.class, formComparator);
            Map<EduProgramSubject, Set<EnrRequestedCompetition>> eduProgramSubjectMap = SafeMap.safeGet(eduProgramFormMap, programForm, TreeMap.class, subjComparator);
            Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(eduProgramSubjectMap, programSubject, HashSet.class);
            requestedCompetitionSet.add(reqComp);
        }

        // forming documents, adding to zip archive
        for (Map.Entry<OrgUnit, Map<OrgUnit, Map<EduProgramForm, Map<EduProgramSubject, Set<EnrRequestedCompetition>>>>> orgUnitEntry : dataMap.entrySet())
        {
            for (Map.Entry<OrgUnit, Map<EduProgramForm, Map<EduProgramSubject, Set<EnrRequestedCompetition>>>> formativeEntry : orgUnitEntry.getValue().entrySet())
            {
                for (Map.Entry<EduProgramForm, Map<EduProgramSubject, Set<EnrRequestedCompetition>>> programFormEntry : formativeEntry.getValue().entrySet())
                {
                    for (Map.Entry<EduProgramSubject, Set<EnrRequestedCompetition>> subjectEntry : programFormEntry.getValue().entrySet())
                    {
                        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


                        int num = 1;
                        List<String[]> tTable = new ArrayList<>();

                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).setValue(programFormEntry.getKey());
                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).setEnableCheckboxChecked(true);

                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT).setValue(orgUnitEntry.getKey());
                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT).setEnableCheckboxChecked(true);

                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT).setValue(formativeEntry.getKey());
                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT).setEnableCheckboxChecked(true);

                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT).setValue(subjectEntry.getKey());
                        model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT).setEnableCheckboxChecked(true);



                        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());


                        for (EnrRequestedCompetition reqComp : subjectEntry.getValue())
                        {

                            List<String> row = new ArrayList<>();
                            row.add(String.valueOf(num++));
                            StringBuilder fullName = new StringBuilder()
                                    .append(reqComp.getRequest().getIdentityCard().getLastName())
                                    .append("\\line")
                                    .append(reqComp.getRequest().getIdentityCard().getFirstName());
                            if (reqComp.getRequest().getIdentityCard().getMiddleName() != null)
                                fullName.append("\\line").append(reqComp.getRequest().getIdentityCard().getMiddleName());
                            row.add(new RtfBackslashScreener().screenBackslashes(fullName.toString()));

                            row.add(new DateFormatter(DateFormatter.PATTERN_DEFAULT).format(reqComp.getRequest().getIdentityCard().getBirthDate()));

                            row.add(reqComp.getRequest().getEduDocument().getDisplayableTitle());

                            StringBuilder addressAndPhone = new StringBuilder();
                            if (reqComp.getRequest().getIdentityCard().getAddress() != null)
                                addressAndPhone.append(reqComp.getRequest().getIdentityCard().getAddress().getShortTitleWithSettlement()).append("\\line");
                            if (reqComp.getRequest().getEntrant().getPerson().getContactData().getPhoneDefault() != null)
                                addressAndPhone.append(reqComp.getRequest().getEntrant().getPerson().getContactData().getPhoneDefault());

                            row.add(new RtfBackslashScreener().screenBackslashes(addressAndPhone.toString()));

                            row.add(reqComp.getRequest().getIdentityCard().getCitizenship().getShortTitle());

                            boolean olimp = false;
                            EnrChosenEntranceExam olimpVi = new EnrChosenEntranceExam();

                            if (vviMap.containsKey(reqComp)) {
                                StringBuilder egeVvif = new StringBuilder();
                                StringBuilder innerVvif = new StringBuilder();
                                for (EnrChosenEntranceExamForm examForm : vviMap.get(reqComp))
                                {
                                    if (examForm.getPassForm() != null) {
                                        if (examForm.getPassForm().isInternal()) {
                                            innerVvif.append(examForm.getChosenEntranceExam().getDiscipline().getShortTitle()).append(" - ");
                                            if (examForm.getMarkSource() != null)
                                                innerVvif.append(examForm.getMarkSource().getMarkAsDouble().intValue()).append("\\line");
                                            else innerVvif.append("\\line");
                                        }
                                        else if (examForm.getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM)) {
                                            egeVvif.append(examForm.getChosenEntranceExam().getDiscipline().getShortTitle()).append(" - ");
                                            if (examForm.getMarkSource() == null)
                                                egeVvif.append("\\line");
                                            else
                                                egeVvif.append(examForm.getMarkSource().getMarkAsDouble().intValue()).append("\\line");
                                        }

                                        if (examForm.getChosenEntranceExam().getMaxMarkForm()!=null && examForm.getChosenEntranceExam().getMaxMarkForm().getPassForm().getCode().equals(EnrExamPassFormCodes.OLYMPIAD)) {
                                            olimp = true;
                                            olimpVi = examForm.getChosenEntranceExam();
                                        }
                                    }
                                }
                                row.add(new RtfBackslashScreener().screenBackslashes(egeVvif.toString()));
                                row.add(new RtfBackslashScreener().screenBackslashes(innerVvif.toString()));
                            }
                            else {
                                row.add("");
                                row.add("");
                            }
                            row.add(reqComp.getCompetition().getType().getCompensationType().getShortTitle());

                            StringBuilder benefitBuilder = new StringBuilder();



                            if (reqComp.getCompetition().isNoExams())
                                benefitBuilder.append(((EnrRequestedCompetitionNoExams)reqComp).getBenefitCategory().getBenefitType().getShortTitle());
                            if (reqComp.getCompetition().isExclusive()) {
                                if (benefitBuilder.length() > 0)
                                    benefitBuilder.append("\\line ");
                                benefitBuilder.append(((EnrRequestedCompetitionExclusive) reqComp).getBenefitCategory().getBenefitType().getShortTitle());
                            }
                            if (reqComp.getRequest().getBenefitCategory() != null) {
                                if (benefitBuilder.length() > 0)
                                    benefitBuilder.append("\\line ");
                                benefitBuilder.append(reqComp.getRequest().getBenefitCategory().getBenefitType().getShortTitle());
                            }
                            if (olimp && ((EnrEntrantMarkSourceBenefit) olimpVi.getMarkSource()).getBenefitCategory()!=null) {
                                    if (benefitBuilder.length() > 0)
                                        benefitBuilder.append("\\line ");
                                    benefitBuilder.append(((EnrEntrantMarkSourceBenefit) olimpVi.getMarkSource()).getBenefitCategory().getBenefitType().getShortTitle());
                            }
                            row.add(benefitBuilder.toString());

                            if (reqComp.getCompetition().isTargetAdmission())
                                row.add("да");
                            else row.add("");

                            row.add("");
                            tTable.add(row.toArray(new String[row.size()]));
                        }

                        RtfTableModifier modifier = new RtfTableModifier();
                        modifier.put("T", tTable.toArray(new String[tTable.size()][]));
                        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
                        modifier.put("T", new RtfRowIntercepterRawText());
                        modifier.put("H", new RtfRowIntercepterRawText());
                        modifier.put("D", new String[][]{{"Дата формирования", new DateFormatter(DateFormatter.PATTERN_DEFAULT).format(new Date())}});
                        modifier.modify(document);

                        StringBuilder filename = new StringBuilder()
                                .append(formativeEntry.getKey().getShortTitle())
                                .append(" (")
                                .append(orgUnitEntry.getKey().getShortTitle())
                                .append(") ")
                                .append(subjectEntry.getKey().getSubjectCode())
                                .append(" ")
                                .append(subjectEntry.getKey().getShortTitle())
                                .append(" (")
                                .append(programFormEntry.getKey().getTitle())
                                .append(")");

                        if (filename.length() > 120)
                            filename.setLength(120);
                        filename
                                .append(".rtf");
                        String latinFileName = CoreStringUtils.transliterate(filename.toString());
                        ZipEntry zipEntry = new ZipEntry(latinFileName);
                        try {
                            zos.putNextEntry(zipEntry);
                            zos.write(RtfUtil.toByteArray(document));
                            zos.closeEntry();
                        } catch (IOException e)
                        {
                            CoreExceptionUtils.getRuntimeException(e);
                        }

                    }
                }
            }
        }

        byte[] result = null;

        try{
            zos.finish();
            zos.close();
            result = bytesOut.toByteArray();
            bytesOut.close();

        } catch (IOException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }

        return result;
    }
}
