package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCompetitionTypeGen extends EntityBase
 implements INaturalIdentifiable<EnrCompetitionTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType";
    public static final String ENTITY_NAME = "enrCompetitionType";
    public static final int VERSION_HASH = -1849020274;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String P_PUBLIC_TITLE = "publicTitle";
    public static final String P_PRIORITY = "priority";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_TITLE = "title";
    public static final String P_PRIORITY_FOR_PROGRAM_ALLOC = "priorityForProgramAlloc";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _printTitle;     // Печатное название
    private String _publicTitle;     // Публичное название
    private int _priority;     // Приоритет
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Печатное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название. Свойство не может быть null.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Публичное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPublicTitle()
    {
        return _publicTitle;
    }

    /**
     * @param publicTitle Публичное название. Свойство не может быть null.
     */
    public void setPublicTitle(String publicTitle)
    {
        dirty(_publicTitle, publicTitle);
        _publicTitle = publicTitle;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCompetitionTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrCompetitionType)another).getCode());
            }
            setShortTitle(((EnrCompetitionType)another).getShortTitle());
            setPrintTitle(((EnrCompetitionType)another).getPrintTitle());
            setPublicTitle(((EnrCompetitionType)another).getPublicTitle());
            setPriority(((EnrCompetitionType)another).getPriority());
            setCompensationType(((EnrCompetitionType)another).getCompensationType());
            setTitle(((EnrCompetitionType)another).getTitle());
        }
    }

    public INaturalId<EnrCompetitionTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrCompetitionTypeGen>
    {
        private static final String PROXY_NAME = "EnrCompetitionTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCompetitionTypeGen.NaturalId) ) return false;

            EnrCompetitionTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCompetitionTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCompetitionType.class;
        }

        public T newInstance()
        {
            return (T) new EnrCompetitionType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "printTitle":
                    return obj.getPrintTitle();
                case "publicTitle":
                    return obj.getPublicTitle();
                case "priority":
                    return obj.getPriority();
                case "compensationType":
                    return obj.getCompensationType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "publicTitle":
                    obj.setPublicTitle((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "printTitle":
                        return true;
                case "publicTitle":
                        return true;
                case "priority":
                        return true;
                case "compensationType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "printTitle":
                    return true;
                case "publicTitle":
                    return true;
                case "priority":
                    return true;
                case "compensationType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "publicTitle":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "compensationType":
                    return CompensationType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCompetitionType> _dslPath = new Path<EnrCompetitionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCompetitionType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Публичное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPublicTitle()
     */
    public static PropertyPath<String> publicTitle()
    {
        return _dslPath.publicTitle();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPriorityForProgramAlloc()
     */
    public static SupportedPropertyPath<Integer> priorityForProgramAlloc()
    {
        return _dslPath.priorityForProgramAlloc();
    }

    public static class Path<E extends EnrCompetitionType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _printTitle;
        private PropertyPath<String> _publicTitle;
        private PropertyPath<Integer> _priority;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<Integer> _priorityForProgramAlloc;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrCompetitionTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrCompetitionTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(EnrCompetitionTypeGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Публичное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPublicTitle()
     */
        public PropertyPath<String> publicTitle()
        {
            if(_publicTitle == null )
                _publicTitle = new PropertyPath<String>(EnrCompetitionTypeGen.P_PUBLIC_TITLE, this);
            return _publicTitle;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrCompetitionTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrCompetitionTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType#getPriorityForProgramAlloc()
     */
        public SupportedPropertyPath<Integer> priorityForProgramAlloc()
        {
            if(_priorityForProgramAlloc == null )
                _priorityForProgramAlloc = new SupportedPropertyPath<Integer>(EnrCompetitionTypeGen.P_PRIORITY_FOR_PROGRAM_ALLOC, this);
            return _priorityForProgramAlloc;
        }

        public Class getEntityClass()
        {
            return EnrCompetitionType.class;
        }

        public String getEntityName()
        {
            return "enrCompetitionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract int getPriorityForProgramAlloc();
}
