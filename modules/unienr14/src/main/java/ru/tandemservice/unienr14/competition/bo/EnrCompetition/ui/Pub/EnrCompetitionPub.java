/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.resolver.DefaultLinkResolver;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrRatingItemDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

/**
 * @author oleyba
 * @since 3/17/14
 */
@Configuration
public class EnrCompetitionPub extends BusinessComponentManager
{
    public static final String ITEM_DS = "itemDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(ITEM_DS, ratingDSColumns(), ratingDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint ratingDSColumns()
    {
        return columnListExtPointBuilder(ITEM_DS)
            .addColumn(textColumn("position", EnrRatingItem.position()))
            .addColumn(textColumn("rank", EnrRatingItem.positionAbsolute()))
            .addColumn(publisherColumn("fio", EnrEntrant.person().fullFio())
                .entityListProperty(EnrRatingItem.requestedCompetition().request().entrant().s())
                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EnrEntrantPub.class.getSimpleName();
                    }

                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return new ParametersMap()
                            .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrEntrant) entity).getId())
                            .add("selectedTab", "ratingTab");
                    }
                }))
            .addColumn(textColumn(EnrRatingItemDSHandler.VIEW_PROP_CUSTOM_STATES, EnrRatingItemDSHandler.VIEW_PROP_CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
            .addColumn(textColumn("state", EnrRatingItem.requestedCompetition().state().stateDaemonSafe()))
            .addColumn(publisherColumn("otherOrders", "title")
                .entityListProperty(EnrRatingItemDSHandler.VIEW_PROP_PREV_ORDERS)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return entity.getId() < 0 ? null : EnrOrderPub.class.getSimpleName();
                    }
                }))
            .addColumn(publisherColumn("thisOrder", "title")
                .entityListProperty(EnrRatingItemDSHandler.VIEW_PROP_CURRENT_ORDER)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(DefaultLinkResolver.with()
                    .component(EnrOrderPub.class)
                    .primaryKeyAsParameter(PublisherActivator.PUBLISHER_ID_KEY)
                    .create()))
            .addColumn(headerColumn("competitionMarkHead")
                               .addSubColumn(textColumn("totalMark", "ui:totalMark").create())
                               .addSubColumn(textColumn("entranceExam", "ui:entranceExamSum").create())
                               .addSubColumn(textColumn("achievementMark", EnrRatingItem.achievementMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).create())
                               .visible("ui:bakSpecMag"))
            .addColumn(textColumn("eduInstitutionAvgMarkSpo", EnrRatingItem.requestedCompetition().request().eduDocument().avgMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("ui:spo"))
            .addColumn(textColumn("competitionMarkSum", EnrRatingItem.totalMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("mvel:!presenter.bakSpecMag"))
            .addColumn(textColumn("eduInstitutionAvgMarkTrInt", EnrRatingItem.requestedCompetition().request().eduDocument().avgMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("ui:trainInter"))

            .addColumn(textColumn("achievementMarkSum", EnrRatingItem.achievementMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("mvel:!presenter.bakSpecMag"))
            //.addColumn(textColumn("ratingInfo", EnrRatingItemDSHandler.VIEW_PROPERTY_RATING_INFO)) // todo
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> ratingDSHandler()
    {
        return new EnrRatingItemDSHandler(getName());
    }
}