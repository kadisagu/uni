package ru.tandemservice.unienr14.programAlloc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абитуриент в распределении по ОП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramAllocationItemGen extends EntityBase
 implements INaturalIdentifiable<EnrProgramAllocationItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem";
    public static final String ENTITY_NAME = "enrProgramAllocationItem";
    public static final int VERSION_HASH = -1792195657;
    private static IEntityMeta ENTITY_META;

    public static final String L_ALLOCATION = "allocation";
    public static final String L_ENTRANT = "entrant";
    public static final String L_PROGRAM_SET_ITEM = "programSetItem";

    private EnrProgramAllocation _allocation;     // Распределение по ОП
    private EnrRequestedCompetition _entrant;     // Выбранный конкурс
    private EnrProgramSetItem _programSetItem;     // ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Распределение по ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramAllocation getAllocation()
    {
        return _allocation;
    }

    /**
     * @param allocation Распределение по ОП. Свойство не может быть null.
     */
    public void setAllocation(EnrProgramAllocation allocation)
    {
        dirty(_allocation, allocation);
        _allocation = allocation;
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Выбранный конкурс. Свойство не может быть null.
     */
    public void setEntrant(EnrRequestedCompetition entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetItem getProgramSetItem()
    {
        return _programSetItem;
    }

    /**
     * @param programSetItem ОП. Свойство не может быть null.
     */
    public void setProgramSetItem(EnrProgramSetItem programSetItem)
    {
        dirty(_programSetItem, programSetItem);
        _programSetItem = programSetItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramAllocationItemGen)
        {
            if (withNaturalIdProperties)
            {
                setAllocation(((EnrProgramAllocationItem)another).getAllocation());
                setEntrant(((EnrProgramAllocationItem)another).getEntrant());
            }
            setProgramSetItem(((EnrProgramAllocationItem)another).getProgramSetItem());
        }
    }

    public INaturalId<EnrProgramAllocationItemGen> getNaturalId()
    {
        return new NaturalId(getAllocation(), getEntrant());
    }

    public static class NaturalId extends NaturalIdBase<EnrProgramAllocationItemGen>
    {
        private static final String PROXY_NAME = "EnrProgramAllocationItemNaturalProxy";

        private Long _allocation;
        private Long _entrant;

        public NaturalId()
        {}

        public NaturalId(EnrProgramAllocation allocation, EnrRequestedCompetition entrant)
        {
            _allocation = ((IEntity) allocation).getId();
            _entrant = ((IEntity) entrant).getId();
        }

        public Long getAllocation()
        {
            return _allocation;
        }

        public void setAllocation(Long allocation)
        {
            _allocation = allocation;
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrProgramAllocationItemGen.NaturalId) ) return false;

            EnrProgramAllocationItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getAllocation(), that.getAllocation()) ) return false;
            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getAllocation());
            result = hashCode(result, getEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getAllocation());
            sb.append("/");
            sb.append(getEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramAllocationItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramAllocationItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramAllocationItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "allocation":
                    return obj.getAllocation();
                case "entrant":
                    return obj.getEntrant();
                case "programSetItem":
                    return obj.getProgramSetItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "allocation":
                    obj.setAllocation((EnrProgramAllocation) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrRequestedCompetition) value);
                    return;
                case "programSetItem":
                    obj.setProgramSetItem((EnrProgramSetItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "allocation":
                        return true;
                case "entrant":
                        return true;
                case "programSetItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "allocation":
                    return true;
                case "entrant":
                    return true;
                case "programSetItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "allocation":
                    return EnrProgramAllocation.class;
                case "entrant":
                    return EnrRequestedCompetition.class;
                case "programSetItem":
                    return EnrProgramSetItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramAllocationItem> _dslPath = new Path<EnrProgramAllocationItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramAllocationItem");
    }
            

    /**
     * @return Распределение по ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem#getAllocation()
     */
    public static EnrProgramAllocation.Path<EnrProgramAllocation> allocation()
    {
        return _dslPath.allocation();
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem#getEntrant()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem#getProgramSetItem()
     */
    public static EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
    {
        return _dslPath.programSetItem();
    }

    public static class Path<E extends EnrProgramAllocationItem> extends EntityPath<E>
    {
        private EnrProgramAllocation.Path<EnrProgramAllocation> _allocation;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _entrant;
        private EnrProgramSetItem.Path<EnrProgramSetItem> _programSetItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Распределение по ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem#getAllocation()
     */
        public EnrProgramAllocation.Path<EnrProgramAllocation> allocation()
        {
            if(_allocation == null )
                _allocation = new EnrProgramAllocation.Path<EnrProgramAllocation>(L_ALLOCATION, this);
            return _allocation;
        }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem#getEntrant()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem#getProgramSetItem()
     */
        public EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
        {
            if(_programSetItem == null )
                _programSetItem = new EnrProgramSetItem.Path<EnrProgramSetItem>(L_PROGRAM_SET_ITEM, this);
            return _programSetItem;
        }

        public Class getEntityClass()
        {
            return EnrProgramAllocationItem.class;
        }

        public String getEntityName()
        {
            return "enrProgramAllocationItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
