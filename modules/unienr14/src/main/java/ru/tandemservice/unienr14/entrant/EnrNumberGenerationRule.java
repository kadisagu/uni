package ru.tandemservice.unienr14.entrant;

import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.shared.commonbase.base.util.DefaultNumberGenerationRule;

/**
 * @author vdanilov
 */
public class EnrNumberGenerationRule<T> extends DefaultNumberGenerationRule<T> {

    public static final String PREFIX_ENR14 = "enr14.";
    public EnrNumberGenerationRule(Class<T> klass, PropertyPath<String> numberProperty, String[] keyProperties) {
        super(PREFIX_ENR14, klass, numberProperty, keyProperties);
    }
    public EnrNumberGenerationRule(final Class<T> klass, final String... keyProperties) {
        this(klass, new PropertyPath<String>("number"), keyProperties);
    }
}
