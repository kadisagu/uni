package ru.tandemservice.unienr14.settings.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.settings.entity.gen.EnrEntrantAchievementSettingsGen;

/** @see ru.tandemservice.unienr14.settings.entity.gen.EnrEntrantAchievementSettingsGen */
public class EnrEntrantAchievementSettings extends EnrEntrantAchievementSettingsGen
{
    public EnrEntrantAchievementSettings()
    {
    }

    public EnrEntrantAchievementSettings(EnrRequestType requestType, EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.setRequestType(requestType);
        this.setEnrollmentCampaign(enrollmentCampaign);
    }

    @EntityDSLSupport
    @Override
    public double getMaxTotalAchievementMark()
    {
        return 0.001d * getMaxTotalAchievementMarkAsLong();
    }

    public void setMaxTotalAchievementMark(double value)
    {
        setMaxTotalAchievementMarkAsLong((long) (value * 1000));
    }

    public String getMaxTotalAchievementMarkFieldTitle()
    {
        return "Максимальная сумма баллов за индивидуальные достижения абитуриента";
    }

    public ErrorCollector validate(ErrorCollector errorCollector)
    {
        if (getMaxTotalAchievementMark() > EnrEntrantAchievementKind.MARK_LIMIT && getRequestType().getCode().equals(EnrRequestTypeCodes.BS))
        {
            errorCollector.add("Для вида заявления «" + getRequestType().getTitle() + "» " +
                                       StringUtils.uncapitalize(getMaxTotalAchievementMarkFieldTitle()) + " должна быть не больше " +
                                       EnrEntrantAchievementKind.MARK_LIMIT + ".", "maxTotalAchievementMark");
        }
        return errorCollector;
    }
}
