/**
 *$Id: OrderVisaItemsWrapper.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateAddEdit;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic.IEnrEnrollmentOrderVisaTemplate;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Обертка для шаблона Виз приказов о зачислении.
 * Агрегирует данные по Шаблону: Группу и список Виз этой группы
 *
 * @author Alexander Shaburov
 * @since 01.08.13
 */
public class OrderVisaItemsWrapper implements IEnrEnrollmentOrderVisaTemplate.IOrderVisaItemsWrapper, IIdentifiable
{
    private final Long _id;

    private Integer _number;
    private GroupsMemberVising _group;
    private List<PossibleVisaWrapper> _visaList;

    public OrderVisaItemsWrapper()
    {
        _id = Integer.valueOf(System.identityHashCode(this)).longValue();
    }

    public OrderVisaItemsWrapper(Integer number)
    {
        _number = number;
        _id = Integer.valueOf(System.identityHashCode(this)).longValue();
    }

    public OrderVisaItemsWrapper(OrderVisaItemsWrapper wrapper, Integer number)
    {
        this();
        _number = number;
        _group = wrapper.getGroup();
        _visaList = wrapper.getVisaList();
    }

    public OrderVisaItemsWrapper(GroupsMemberVising group, List<EnrCampaignOrderVisaItem> visaList, Integer number)
    {
        this();
        _number = number;
        _group = group;

        Collections.sort(visaList, new EntityComparator<EnrCampaignOrderVisaItem>(new EntityOrder(EnrCampaignOrderVisaItem.priority())));
        _visaList = wrapPossibleVisaList(CommonBaseEntityUtil.<IPossibleVisa>getPropertiesList(visaList, EnrCampaignOrderVisaItem.possibleVisa().s()));

    }

    public List<PossibleVisaWrapper> wrapPossibleVisaList(List<IPossibleVisa> visaList)
    {
        if (visaList == null)
            return null;

        if (visaList.isEmpty())
            return new ArrayList<>();

        final List<PossibleVisaWrapper> wrapperList = new ArrayList<>();
        int priority = 1;
        for (IPossibleVisa visa : visaList)
            wrapperList.add(new PossibleVisaWrapper(priority++, visa));

        return wrapperList;
    }



    public void sortVisaList()
    {
        Collections.sort(_visaList, new Comparator<PossibleVisaWrapper>()
        {
            @Override
            public int compare(PossibleVisaWrapper o1, PossibleVisaWrapper o2)
            {
                return Integer.compare(o1.getPriority(), o2.getPriority());
            }
        });
    }

    // Getters & Setters

    public String getNumber()
    {
        return String.valueOf(_number);
    }

    public String getGroupStr()
    {
        return _group.getTitle();
    }

    public List<IPossibleVisa> getPossibleVisaList()
    {
        if (null == _visaList)
            return null;

        final List<IPossibleVisa> possibleVisaList = new ArrayList<>();
        for (OrderVisaItemsWrapper.PossibleVisaWrapper wrapper : _visaList)
            possibleVisaList.add(wrapper.getPossibleVisa());

        return possibleVisaList;
    }

    public void setPossibleVisaList(List<IPossibleVisa> possibleVisaList)
    {
        _visaList = wrapPossibleVisaList(possibleVisaList);
    }

    public class PossibleVisaWrapper
    {
        private int _priority;
        private final IPossibleVisa _possibleVisa;

        public PossibleVisaWrapper(int priority, IPossibleVisa possibleVisa)
        {
            _priority = priority;
            _possibleVisa = possibleVisa;
        }

        public Long getId() { return _possibleVisa.getId(); }

        public int getPriority() { return _priority; }
        public void setPriority(int priority) { _priority = priority; }

        public IPossibleVisa getPossibleVisa() { return _possibleVisa; }
    }

    // Accessors

    @Override
    public Long getId()
    {
        return _id;
    }

    public void setNumber(Integer number)
    {
        _number = number;
    }

    @Override
    public GroupsMemberVising getGroup()
    {
        return _group;
    }

    public void setGroup(GroupsMemberVising group)
    {
        _group = group;
    }

    @Override
    public List<PossibleVisaWrapper> getVisaList()
    {
        return _visaList;
    }
}
