package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент набора ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamSetElementGen extends EntityBase
 implements INaturalIdentifiable<EnrExamSetElementGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.exams.entity.EnrExamSetElement";
    public static final String ENTITY_NAME = "enrExamSetElement";
    public static final int VERSION_HASH = 2029799765;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET = "examSet";
    public static final String L_VALUE = "value";
    public static final String L_TYPE = "type";
    public static final String P_NUMBER = "number";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private EnrExamSet _examSet;     // Набор ВИ
    private IEnrExamSetElementValue _value;     // Значение
    private EnrExamType _type;     // Тип
    private int _number;     // Порядковый номер испытания в наборе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSet getExamSet()
    {
        return _examSet;
    }

    /**
     * @param examSet Набор ВИ. Свойство не может быть null.
     */
    public void setExamSet(EnrExamSet examSet)
    {
        dirty(_examSet, examSet);
        _examSet = examSet;
    }

    /**
     * @return Значение. Свойство не может быть null.
     */
    @NotNull
    public IEnrExamSetElementValue getValue()
    {
        return _value;
    }

    /**
     * @param value Значение. Свойство не может быть null.
     */
    public void setValue(IEnrExamSetElementValue value)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && value!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrExamSetElementValue.class);
            IEntityMeta actual =  value instanceof IEntity ? EntityRuntime.getMeta((IEntity) value) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Тип. Свойство не может быть null.
     */
    @NotNull
    public EnrExamType getType()
    {
        return _type;
    }

    /**
     * @param type Тип. Свойство не может быть null.
     */
    public void setType(EnrExamType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Порядковый номер испытания в наборе. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер испытания в наборе. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamSetElementGen)
        {
            if (withNaturalIdProperties)
            {
                setExamSet(((EnrExamSetElement)another).getExamSet());
                setValue(((EnrExamSetElement)another).getValue());
            }
            setType(((EnrExamSetElement)another).getType());
            setNumber(((EnrExamSetElement)another).getNumber());
        }
    }

    public INaturalId<EnrExamSetElementGen> getNaturalId()
    {
        return new NaturalId(getExamSet(), getValue());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamSetElementGen>
    {
        private static final String PROXY_NAME = "EnrExamSetElementNaturalProxy";

        private Long _examSet;
        private Long _value;

        public NaturalId()
        {}

        public NaturalId(EnrExamSet examSet, IEnrExamSetElementValue value)
        {
            _examSet = ((IEntity) examSet).getId();
            _value = ((IEntity) value).getId();
        }

        public Long getExamSet()
        {
            return _examSet;
        }

        public void setExamSet(Long examSet)
        {
            _examSet = examSet;
        }

        public Long getValue()
        {
            return _value;
        }

        public void setValue(Long value)
        {
            _value = value;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamSetElementGen.NaturalId) ) return false;

            EnrExamSetElementGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamSet(), that.getExamSet()) ) return false;
            if( !equals(getValue(), that.getValue()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamSet());
            result = hashCode(result, getValue());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamSet());
            sb.append("/");
            sb.append(getValue());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamSetElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamSetElement.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamSetElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examSet":
                    return obj.getExamSet();
                case "value":
                    return obj.getValue();
                case "type":
                    return obj.getType();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examSet":
                    obj.setExamSet((EnrExamSet) value);
                    return;
                case "value":
                    obj.setValue((IEnrExamSetElementValue) value);
                    return;
                case "type":
                    obj.setType((EnrExamType) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examSet":
                        return true;
                case "value":
                        return true;
                case "type":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examSet":
                    return true;
                case "value":
                    return true;
                case "type":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examSet":
                    return EnrExamSet.class;
                case "value":
                    return IEnrExamSetElementValue.class;
                case "type":
                    return EnrExamType.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamSetElement> _dslPath = new Path<EnrExamSetElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamSetElement");
    }
            

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getExamSet()
     */
    public static EnrExamSet.Path<EnrExamSet> examSet()
    {
        return _dslPath.examSet();
    }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getValue()
     */
    public static IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Тип. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getType()
     */
    public static EnrExamType.Path<EnrExamType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Порядковый номер испытания в наборе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrExamSetElement> extends EntityPath<E>
    {
        private EnrExamSet.Path<EnrExamSet> _examSet;
        private IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue> _value;
        private EnrExamType.Path<EnrExamType> _type;
        private PropertyPath<Integer> _number;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getExamSet()
     */
        public EnrExamSet.Path<EnrExamSet> examSet()
        {
            if(_examSet == null )
                _examSet = new EnrExamSet.Path<EnrExamSet>(L_EXAM_SET, this);
            return _examSet;
        }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getValue()
     */
        public IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue> value()
        {
            if(_value == null )
                _value = new IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue>(L_VALUE, this);
            return _value;
        }

    /**
     * @return Тип. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getType()
     */
        public EnrExamType.Path<EnrExamType> type()
        {
            if(_type == null )
                _type = new EnrExamType.Path<EnrExamType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Порядковый номер испытания в наборе. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EnrExamSetElementGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EnrExamSetElementGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetElement#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrExamSetElementGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrExamSetElement.class;
        }

        public String getEntityName()
        {
            return "enrExamSetElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getShortTitle();

    public abstract String getTitle();
}
