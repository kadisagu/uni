package ru.tandemservice.unienr14.enrollment.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.enrollment.entity.gen.*;

import java.util.Date;

/** @see ru.tandemservice.unienr14.enrollment.entity.gen.EnrModelStepGen */
public class EnrModelStep extends EnrModelStepGen
{
    public EnrModelStep()
    {
    }

    public EnrModelStep(Date date, EnrEnrollmentStepKind defKind)
    {
        setEnrollmentDate(date);
        setKind(defKind);
    }

    public String getDateStr() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getEnrollmentDate());
    }

    public boolean isBasedOnRealStep() {
        return getBaseRealStep() != null;
    }
}