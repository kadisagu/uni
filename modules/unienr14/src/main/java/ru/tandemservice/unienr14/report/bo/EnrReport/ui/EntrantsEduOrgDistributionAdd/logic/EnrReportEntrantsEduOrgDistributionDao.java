/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd.logic;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd.EnrReportEntrantsEduOrgDistributionAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.RtfBackslashScreener;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsEduOrgDistribution;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 19.06.2014
 */
public class EnrReportEntrantsEduOrgDistributionDao extends UniBaseDao implements IEnrReportEntrantsEduOrgDistributionDao
{
    @Override
    public long createReport(EnrReportEntrantsEduOrgDistributionAddUI model) {
        EnrReportEntrantsEduOrgDistribution report = new EnrReportEntrantsEduOrgDistribution();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        if (model.isFilterByEduDocumentKinds())
            report.setEduDocumentKind(UniStringUtils.join(model.getEduDocumentKindList(), "title", "; "));

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsEduOrgDistribution.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantsEduOrgDistribution.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsEduOrgDistribution.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantsEduOrgDistribution.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsEduOrgDistribution.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsEduOrgDistribution.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantsEduOrgDistribution.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantsEduOrgDistribution.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantsEduOrgDistribution.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantsEduOrgDistribution.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();



    }


    private byte[] buildReport(EnrReportEntrantsEduOrgDistributionAddUI model)
    {

        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_EDU_ORG_DISTRIBUTION);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Дата

        new RtfInjectModifier().put("dateForm", new SimpleDateFormat("dd.MM.yyyy").format(new Date())).modify(document);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        requestedCompDQL
                .where(eq(property("request", EnrEntrantRequest.takeAwayDocument()), value(Boolean.FALSE)));

        if (model.isFilterByEduDocumentKinds())
            requestedCompDQL
                    .where(in(property("request", EnrEntrantRequest.eduDocument().eduDocumentKind()), model.getEduDocumentKindList()));


        requestedCompDQL.column(property("reqComp"));
        List<EnrRequestedCompetition> reqCompList = getList(requestedCompDQL);


        // Комаратор стран. Двигает Россию в конец списка
        Comparator<AddressCountry> countryComparator = new Comparator<AddressCountry>() {
            @Override
            public int compare(AddressCountry o1, AddressCountry o2) {
                if (o1==null)
                    if (o2==null)
                        return 0;
                    else
                        return 1000;
                else if (o2 == null)
                    return -1000;
                int result = 0;
                if (o1.getCode() == 0)
                    result +=10000;
                if (o2.getCode() == 0)
                    result -=10000;
                result +=o1.getTitle().compareToIgnoreCase(o2.getTitle());
                return result;
            }
        };

        // Компаратор адрес-айтемов. Сравнивает по тайтлу с типом.
        Comparator<AddressItem> itemComparator = new Comparator<AddressItem>() {
            @Override
            public int compare(AddressItem o1, AddressItem o2) {
                if (o1==null)
                    if (o2==null)
                        return 0;
                    else
                        return 1000;
                else if (o2 == null)
                    return -1000;
                return o1.getTitleWithType().compareToIgnoreCase(o2.getTitleWithType());
            }
        };


        Comparator<String> stringComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        };



        Map<AddressCountry, Map<AddressItem, Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>>>> dataMap = new TreeMap<>(countryComparator);


        for (EnrRequestedCompetition reqComp : reqCompList)
        {
            String eduOrgTitle = reqComp.getRequest().getEduDocument().getEduOrganization();
            if (reqComp.getRequest().getEduDocument().getEduOrganizationAddressItem() != null)
            {
                AddressCountry country = reqComp.getRequest().getEduDocument().getEduOrganizationAddressItem().getCountry();
                AddressItem settlement = reqComp.getRequest().getEduDocument().getEduOrganizationAddressItem();
                AddressItem region = settlement.getParent();

                while (region != null)
                {
                    if (region.getParent() != null)
                        region = region.getParent();
                    else break;
                }
                Map<AddressItem, Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>>> regionMap = SafeMap.safeGet(dataMap, country, TreeMap.class, itemComparator);
                Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>> settlementMap = SafeMap.safeGet(regionMap, region, TreeMap.class, itemComparator);
                Map<String, Set<EnrRequestedCompetition>> eduOrgMap = SafeMap.safeGet(settlementMap, settlement, TreeMap.class, stringComparator);
                Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(eduOrgMap, eduOrgTitle, HashSet.class);
                requestedCompetitionSet.add(reqComp);

            }
            else
            {
                Map<AddressItem, Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>>> regionMap = SafeMap.safeGet(dataMap, null, TreeMap.class, itemComparator);
                Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>> settlementMap = SafeMap.safeGet(regionMap, null, TreeMap.class, itemComparator);
                Map<String, Set<EnrRequestedCompetition>> eduOrgMap = SafeMap.safeGet(settlementMap, null, TreeMap.class, stringComparator);
                Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(eduOrgMap, eduOrgTitle, HashSet.class);
                requestedCompetitionSet.add(reqComp);
            }
        }


        List<String[]> tTable = new ArrayList<>();

        int[] totalValues = {0,0,0};


        for (Map.Entry<AddressCountry, Map<AddressItem, Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>>>>  t1Entry : dataMap.entrySet())
        {
            List<String[]> t1Table = new ArrayList<>();
            Integer[] t1Values = {0,0,0};

            for (Map.Entry<AddressItem, Map<AddressItem, Map<String, Set<EnrRequestedCompetition>>>> t2Entry : t1Entry.getValue().entrySet()) {

                List<String[]> t2Table = new ArrayList<>();
                Integer[] t2Values = {0,0,0};


                for (Map.Entry<AddressItem, Map<String, Set<EnrRequestedCompetition>>> t3Entry : t2Entry.getValue().entrySet()) {

                    List<String[]> t3Table = new ArrayList<>();
                    Integer[] t3Values = {0,0,0};
                    for (Map.Entry<String, Set<EnrRequestedCompetition>> t4Entry : t3Entry.getValue().entrySet()) {

                        HashSet<EnrEntrantRequest> examPassStage = new HashSet<>();
                        HashSet<EnrEntrantRequest> enrollStage = new HashSet<>();
                        HashSet<EnrEntrantRequest> requests = new HashSet<>();
                        for (EnrRequestedCompetition reqComp : t4Entry.getValue()) {
                            requests.add(reqComp.getRequest());
                            if (EnrReportStageSelector.examPassedCheckByPriority(reqComp.getState().getPriority()))
                                examPassStage.add(reqComp.getRequest());
                            if (EnrReportStageSelector.enrollmentResultsCheckByPriority(reqComp.getState().getPriority()))
                                enrollStage.add(reqComp.getRequest());
                        }

                        t3Table.add(new String[]{
                                "t4",
                                t4Entry.getKey(),
                                String.valueOf(requests.size()),
                                String.valueOf(examPassStage.size()),
                                String.valueOf(enrollStage.size())
                        });

                        t3Values[0] += requests.size();
                        t3Values[1] += examPassStage.size();
                        t3Values[2] += enrollStage.size();

                    }

                    t3Table.add(0, new String[]{
                            "t3",
                            (t3Entry.getKey() == null ? "Населенный пункт не указан" : t3Entry.getKey().getTitleWithType()),
                            String.valueOf(t3Values[0]),
                            String.valueOf(t3Values[1]),
                            String.valueOf(t3Values[2]),
                    });

                    t2Table.addAll(t3Table);
                    t2Values[0] += t3Values[0];
                    t2Values[1] += t3Values[1];
                    t2Values[2] += t3Values[2];
                }

                t2Table.add(0, new String[]{
                        "t2",
                        (t2Entry.getKey() == null ? "Регион не указан" : t2Entry.getKey().getTitleWithType()),
                        String.valueOf(t2Values[0]),
                        String.valueOf(t2Values[1]),
                        String.valueOf(t2Values[2]),
                });

                t1Table.addAll(t2Table);
                t1Values[0] += t2Values[0];
                t1Values[1] += t2Values[1];
                t1Values[2] += t2Values[2];

            }

            t1Table.add(0, new String[]{
                    "t1",
                    (t1Entry.getKey() == null ? "Страна не указана" : t1Entry.getKey().getTitle()),
                    String.valueOf(t1Values[0]),
                    String.valueOf(t1Values[1]),
                    String.valueOf(t1Values[2]),
            });
            tTable.addAll(t1Table);
            totalValues[0] += t1Values[0];
            totalValues[1] += t1Values[1];
            totalValues[2] += t1Values[2];
        }

        tTable.add(new String[]{
                "t0",
           "ИТОГО",
            String.valueOf(totalValues[0]),
            String.valueOf(totalValues[1]),
            String.valueOf(totalValues[2])
        });

        List<String[]> formedTTable = new ArrayList<>();

        final List<Integer> colors = new ArrayList<>();

        for (String[] row : tTable)
        {
            formedTTable.add(new String[]{
                    row[1],
                    row[2],
                    row[3],
                    row[4]
            });
            if (row[0].equals("t1"))
                colors.add(15);
            else if (row[0].equals("t2"))
                colors.add(16);
            if (row[0].equals("t3"))
                colors.add(17);
            else if (row[0].equals("t4"))
                colors.add(0);
            else if (row[0].equals("t0"))
                colors.add(20);
        }


        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        if (model.getParallelSelector().isParallelActive())
            hTable.add(new String[]{"Поступающие параллельно", model.getParallelSelector().getParallel().getTitle()});
        if (model.isFilterByEduDocumentKinds())
            hTable.add(new String[]{"Вид документа о полученном образовании", new RtfBackslashScreener().screenBackslashes(UniStringUtils.join(model.getEduDocumentKindList(), EduDocumentKind.title().s(), "\\line "))});
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("H", new RtfRowIntercepterRawText());

        tableModifier.put("T", formedTTable.toArray(new String[formedTTable.size()][]));

        tableModifier.put("T", new IRtfRowIntercepter() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {

            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {


                cell.setBackgroundColorIndex(colors.get(rowIndex));

                if (colors.get(rowIndex)!=0) {
                    if (colors.get(rowIndex)!=20)
                        cell.setTextAlignment(291);
                    RtfString string = new RtfString();
                    string
                            .boldBegin()
                            .append(value)
                            .boldEnd();
                    return string.toList();
                }
                else return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                for (int i = 0; i < colors.size(); i++) {
                    if (colors.get(i)!=0 && colors.get(i)!=20)
                        newRowList.get(i+1).getCellList().get(0).append(1288);
                }
            }
        });

        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }
}
