package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Число мест для зачисления по виду ЦП в модели
 *
 * Число мест для зачисления по конкурсу в рамках шага.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrModelTargetAdmissionPlanGen extends EntityBase
 implements INaturalIdentifiable<EnrModelTargetAdmissionPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan";
    public static final String ENTITY_NAME = "enrModelTargetAdmissionPlan";
    public static final int VERSION_HASH = 822193012;
    private static IEntityMeta ENTITY_META;

    public static final String L_MODEL = "model";
    public static final String L_COMPETITION = "competition";
    public static final String L_TARGET_ADMISSION_PLAN = "targetAdmissionPlan";
    public static final String P_PLAN = "plan";

    private EnrEnrollmentModel _model;     // Модель
    private EnrCompetition _competition;     // Конкурс (комбинация условий поступления) для приема
    private EnrTargetAdmissionPlan _targetAdmissionPlan;     // План приема по виду ЦП
    private int _plan;     // Число мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Модель. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentModel getModel()
    {
        return _model;
    }

    /**
     * @param model Модель. Свойство не может быть null.
     */
    public void setModel(EnrEnrollmentModel model)
    {
        dirty(_model, model);
        _model = model;
    }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * @return План приема по виду ЦП. Свойство не может быть null.
     */
    @NotNull
    public EnrTargetAdmissionPlan getTargetAdmissionPlan()
    {
        return _targetAdmissionPlan;
    }

    /**
     * @param targetAdmissionPlan План приема по виду ЦП. Свойство не может быть null.
     */
    public void setTargetAdmissionPlan(EnrTargetAdmissionPlan targetAdmissionPlan)
    {
        dirty(_targetAdmissionPlan, targetAdmissionPlan);
        _targetAdmissionPlan = targetAdmissionPlan;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Число мест. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrModelTargetAdmissionPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setModel(((EnrModelTargetAdmissionPlan)another).getModel());
                setCompetition(((EnrModelTargetAdmissionPlan)another).getCompetition());
                setTargetAdmissionPlan(((EnrModelTargetAdmissionPlan)another).getTargetAdmissionPlan());
            }
            setPlan(((EnrModelTargetAdmissionPlan)another).getPlan());
        }
    }

    public INaturalId<EnrModelTargetAdmissionPlanGen> getNaturalId()
    {
        return new NaturalId(getModel(), getCompetition(), getTargetAdmissionPlan());
    }

    public static class NaturalId extends NaturalIdBase<EnrModelTargetAdmissionPlanGen>
    {
        private static final String PROXY_NAME = "EnrModelTargetAdmissionPlanNaturalProxy";

        private Long _model;
        private Long _competition;
        private Long _targetAdmissionPlan;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentModel model, EnrCompetition competition, EnrTargetAdmissionPlan targetAdmissionPlan)
        {
            _model = ((IEntity) model).getId();
            _competition = ((IEntity) competition).getId();
            _targetAdmissionPlan = ((IEntity) targetAdmissionPlan).getId();
        }

        public Long getModel()
        {
            return _model;
        }

        public void setModel(Long model)
        {
            _model = model;
        }

        public Long getCompetition()
        {
            return _competition;
        }

        public void setCompetition(Long competition)
        {
            _competition = competition;
        }

        public Long getTargetAdmissionPlan()
        {
            return _targetAdmissionPlan;
        }

        public void setTargetAdmissionPlan(Long targetAdmissionPlan)
        {
            _targetAdmissionPlan = targetAdmissionPlan;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrModelTargetAdmissionPlanGen.NaturalId) ) return false;

            EnrModelTargetAdmissionPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getModel(), that.getModel()) ) return false;
            if( !equals(getCompetition(), that.getCompetition()) ) return false;
            if( !equals(getTargetAdmissionPlan(), that.getTargetAdmissionPlan()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getModel());
            result = hashCode(result, getCompetition());
            result = hashCode(result, getTargetAdmissionPlan());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getModel());
            sb.append("/");
            sb.append(getCompetition());
            sb.append("/");
            sb.append(getTargetAdmissionPlan());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrModelTargetAdmissionPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrModelTargetAdmissionPlan.class;
        }

        public T newInstance()
        {
            return (T) new EnrModelTargetAdmissionPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "model":
                    return obj.getModel();
                case "competition":
                    return obj.getCompetition();
                case "targetAdmissionPlan":
                    return obj.getTargetAdmissionPlan();
                case "plan":
                    return obj.getPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "model":
                    obj.setModel((EnrEnrollmentModel) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "targetAdmissionPlan":
                    obj.setTargetAdmissionPlan((EnrTargetAdmissionPlan) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "model":
                        return true;
                case "competition":
                        return true;
                case "targetAdmissionPlan":
                        return true;
                case "plan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "model":
                    return true;
                case "competition":
                    return true;
                case "targetAdmissionPlan":
                    return true;
                case "plan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "model":
                    return EnrEnrollmentModel.class;
                case "competition":
                    return EnrCompetition.class;
                case "targetAdmissionPlan":
                    return EnrTargetAdmissionPlan.class;
                case "plan":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrModelTargetAdmissionPlan> _dslPath = new Path<EnrModelTargetAdmissionPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrModelTargetAdmissionPlan");
    }
            

    /**
     * @return Модель. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getModel()
     */
    public static EnrEnrollmentModel.Path<EnrEnrollmentModel> model()
    {
        return _dslPath.model();
    }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * @return План приема по виду ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getTargetAdmissionPlan()
     */
    public static EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan> targetAdmissionPlan()
    {
        return _dslPath.targetAdmissionPlan();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    public static class Path<E extends EnrModelTargetAdmissionPlan> extends EntityPath<E>
    {
        private EnrEnrollmentModel.Path<EnrEnrollmentModel> _model;
        private EnrCompetition.Path<EnrCompetition> _competition;
        private EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan> _targetAdmissionPlan;
        private PropertyPath<Integer> _plan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Модель. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getModel()
     */
        public EnrEnrollmentModel.Path<EnrEnrollmentModel> model()
        {
            if(_model == null )
                _model = new EnrEnrollmentModel.Path<EnrEnrollmentModel>(L_MODEL, this);
            return _model;
        }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * @return План приема по виду ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getTargetAdmissionPlan()
     */
        public EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan> targetAdmissionPlan()
        {
            if(_targetAdmissionPlan == null )
                _targetAdmissionPlan = new EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan>(L_TARGET_ADMISSION_PLAN, this);
            return _targetAdmissionPlan;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelTargetAdmissionPlan#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrModelTargetAdmissionPlanGen.P_PLAN, this);
            return _plan;
        }

        public Class getEntityClass()
        {
            return EnrModelTargetAdmissionPlan.class;
        }

        public String getEntityName()
        {
            return "enrModelTargetAdmissionPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
