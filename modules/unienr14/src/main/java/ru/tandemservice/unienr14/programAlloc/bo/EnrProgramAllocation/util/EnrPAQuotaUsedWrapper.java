/* $Id: EcgpQuotaUsedDTO.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EnrPAQuotaUsedWrapper implements IEnrPAQuotaUsedWrapper
{
    private int _totalUsed;
    private Map<Long, Integer> _usedMap;

    public EnrPAQuotaUsedWrapper(int totalUsed, Map<Long, Integer> usedMap)
    {
        _totalUsed = totalUsed;
        _usedMap = usedMap;
    }

    // Getters

    public int getTotalUsed()
    {
        return _totalUsed;
    }

    public Map<Long, Integer> getUsedMap()
    {
        return _usedMap;
    }
}
