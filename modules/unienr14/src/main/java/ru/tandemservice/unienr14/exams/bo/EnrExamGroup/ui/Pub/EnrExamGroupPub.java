/**
 *$Id: EnrExamGroupPub.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupPubEntrantSearchDSHandler;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * @author Alexander Shaburov
 * @since 31.05.13
 */
@Configuration
public class EnrExamGroupPub extends BusinessComponentManager
{
    public static final String EXAM_GROUP_ENTRANT_SEARCH_DS = "enrExamGroupEntrantSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EXAM_GROUP_ENTRANT_SEARCH_DS, enrExamGroupEntrantSearchDSColumns(), enrExamGroupEntrantSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint enrExamGroupEntrantSearchDSColumns()
    {
        final IStyleResolver fioColorSR = rowEntity -> {
            DataWrapper wrapper = (DataWrapper) rowEntity;
            final Boolean invalid = (Boolean) wrapper.getProperty(EnrExamGroupPubEntrantSearchDSHandler.V_PROP_INVALID_ENTRANT);
            return invalid ? "background-color:#ffcccc" : null;
        };

        final IFormatter markFormatter = new DoubleFormatter(2)
        {
            @Override
            public String format(Object source)
            {
                String value = String.valueOf(source);
                try
                {
                    Number number = NumberFormat.getInstance().parse(value);
                    return super.format(number);
                }
                catch (ParseException e)
                {
                    return value;
                }
            }
        };

        return columnListExtPointBuilder(EXAM_GROUP_ENTRANT_SEARCH_DS)
                .addColumn(textColumn("personalNumber", EnrExamPassDiscipline.entrant().personalNumber()).order())
                .addColumn(publisherColumn("fio", EnrExamPassDiscipline.entrant().person().fullFio()).styleResolver(fioColorSR).primaryKeyPath(EnrExamPassDiscipline.entrant().id()).parameters("ui:entrantPubLinkParametersMap").order())
                .addColumn(textColumn("idcardNumber", EnrExamPassDiscipline.entrant().person().fullIdentityCardNumber()))
                .addColumn(dateColumn("setDate", EnrExamPassDiscipline.examGroupSetDate()).order())
                .addColumn(textColumn("examGroupPosition", EnrExamPassDiscipline.examGroupPosition()).visible(Debug.isEnabled()))
                .addColumn(textColumn("mark", EnrExamPassDiscipline.examResultFull()).formatter(markFormatter))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickExcludeEntrant").permissionKey("enr14ExamGroupExcludeEntrant").alert(new FormattedMessage("enrExamGroupEntrantSearchDS.delete.alert", EnrExamPassDiscipline.entrant().person().fio().s())))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> enrExamGroupEntrantSearchDSHandler()
    {
        return new EnrExamGroupPubEntrantSearchDSHandler(getName());
    }
}
