/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.CompetitionListAdd;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.EnrReportRatingListAddUI;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/25/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entity.id")
})
public class EnrReportCompetitionListAddUI extends EnrReportRatingListAddUI
{
    private static EnrEntrantState TAKE_DOUMENTS_AWAY_STATE = DataAccessServices.dao().getByCode(EnrEntrantState.class, EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY);

    private List reqCompStates = new ArrayList<>();

    public void onClickApply() {
        if (getDateFrom().compareTo(getDateTo()) > 0) {
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");
            return;
        }

        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                setReportId(EnrReportManager.instance().ratingListDao().createCompetitionListReport(EnrReportCompetitionListAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }



    public void onChangeValuesInUtil()
    {
        setReqCompStates(Collections.<EnrEntrantState>emptyList());

        @SuppressWarnings("unchecked")
        final List<EnrEntrantState> values =  getReqCompStateModel().getValues(null);

        if (values.size() == 0) {
            return;
        }

        values.remove(TAKE_DOUMENTS_AWAY_STATE);
        setReqCompStates(values);
    }

    @Override
    protected void configUtil(EnrCompetitionFilterAddon util) {
        super.configUtil(util);
        util.configListener("onChangeValuesInUtil");
        util.configBaseUpdateOnChange("reqCompStates");
    }

    // getters and setters

    public CommonFilterSelectModel getReqCompStateModel()
    {
        return new CommonFilterSelectModel() {

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o) {

                final DQLSelectBuilder reqCompDQL = new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "reqComp")
                        .column(value(1))
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("reqComp"),"comp")
                        .where(eq(property("reqComp", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(getEnrollmentCampaign())))
                        .where(eq(property("reqComp", EnrRequestedCompetition.state()), property("st")));

                getCompetitionFilterAddon().applyFilters(reqCompDQL, "comp");

                final DQLSelectBuilder stateDQL = new DQLSelectBuilder().fromEntity(EnrEntrantState.class, "st");
                stateDQL.column(property("st"));
                stateDQL.where(notIn(property("st", EnrEntrantState.code()), EnrEntrantStateCodes.ENTRANT_ARCHIVED, EnrEntrantStateCodes.OUT_OF_COMPETITION, EnrEntrantStateCodes.ACTIVE));

                FilterUtils.applyLikeFilter(stateDQL, filter, EnrEntrantState.title().fromAlias("st"));
                FilterUtils.applySelectFilter(stateDQL, "st", EnrEntrantState.id(), o);

                stateDQL.where(exists(reqCompDQL.buildQuery()));

                stateDQL.order(property("st", EnrEntrantState.priority()));

                return new DQLListResultBuilder(stateDQL);
            }

        };
    }


    public List getReqCompStates() {
        return reqCompStates;
    }

    public void setReqCompStates(List reqCompStates) {
        this.reqCompStates = reqCompStates;
    }
}