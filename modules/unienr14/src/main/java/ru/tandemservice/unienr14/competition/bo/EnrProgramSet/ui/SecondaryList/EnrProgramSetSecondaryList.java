/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.SecondaryList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.*;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.EnrProgramSetSecondaryDSHandler;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.Pub.EnrProgramSetPub;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/18/14
 */
@Configuration
public class EnrProgramSetSecondaryList extends BusinessComponentManager
{
    public static final String BIND_PROGRAM_FORM = "programForm";
    public static final String BIND_ORG_UNITS = "orgUnits";
    public static final String BIND_SUBJECT_CODE = "subjectCode";
    public static final String BIND_PROGRAM_SUBJECTS = "programSubjects";
    public static final String BIND_TITLE = "title";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
            .addDataSource(selectDS("orgUnitDS", orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(searchListDS("competitionSecondarySearchDS", competitionSecondarySearchDSColumns(), competitionSecondarySearchDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint competitionSecondarySearchDSColumns()
    {
        return columnListExtPointBuilder("competitionSecondarySearchDS")
                .addColumn(publisherColumn("title", EnrProgramSetSecondary.title()).order()
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(EnrProgramSetSecondary.id()).setComponent(EnrProgramSetPub.class)))
                .addColumn(textColumn("programSubject", EnrProgramSetSecondary.program().programSubject().titleWithCode()).order())
                .addColumn(textColumn("orgUnit", EnrProgramSetSecondaryDSHandler.VIEW_PROP_OU)
                                   .formatter(new CollectionFormatter(new PropertyFormatter(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))))
                .addColumn(textColumn("developForm", EnrProgramSetSecondary.program().form().title().s()).order())
                .addColumn(textColumn("baseLevel", EnrProgramSetSecondary.program().baseLevel().title().s()).order())
                .addColumn(textColumn("inDepthStudy", EnrProgramSetSecondary.program().inDepthStudy().s()).formatter(YesNoFormatter.INSTANCE).order())
                .addColumn(textColumn("exams", EnrProgramSetSecondaryDSHandler.VIEW_PROP_EXAMS).formatter(NewLineFormatter.NOBR_IN_LINES))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditCompetitionSecondary").permissionKey("enr14CompetitionSecondaryListEditComp"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteCompetitionSecondary").alert(new FormattedMessage("competitionSecondarySearchDS.delete.alert", EnrProgramSetSecondary.program().programSubject().titleWithCode().s())).permissionKey("enr14CompetitionSecondaryListDeleteComp"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> competitionSecondarySearchDSHandler()
    {
        return new EnrProgramSetSecondaryDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EduProgramForm programForm = context.get(BIND_PROGRAM_FORM);

                DQLSelectBuilder programSetBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetSecondary.class, "ps").column(property("ps", EnrProgramSetBase.id()));
                EnrProgramSetSecondaryDSHandler.applyFilters(programSetBuilder, "ps", campaign, null, null, null);

                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                        .where(in(property("psou", EnrProgramSetOrgUnit.programSet().id()), programSetBuilder.buildQuery()))
                        .where(eq(property("psou", EnrProgramSetOrgUnit.orgUnit().id()), property(alias, EnrOrgUnit.id())))
                        .buildQuery()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                //EduProgramForm programForm = context.get(BIND_PROGRAM_FORM);
                List<EnrOrgUnit> orgUnits = context.get(BIND_ORG_UNITS);

                DQLSelectBuilder programSetBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetSecondary.class, "ps").column(property("ps", EnrProgramSetBase.id()));
                EnrProgramSetSecondaryDSHandler.applyFilters(programSetBuilder, "ps", campaign, null, orgUnits, null);

                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetSecondary.class, "pss")
                        .where(in(property("pss", EnrProgramSetSecondary.id()), programSetBuilder.buildQuery()))
                        .where(eq(property("pss", EnrProgramSetSecondary.programSubject().id()), property(alias, EduProgramSubject.id())))
                        .buildQuery()
                ));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title());
    }
}