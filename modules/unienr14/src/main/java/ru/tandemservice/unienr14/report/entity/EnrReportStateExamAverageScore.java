package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.EnrReportStateExamAverageScoreAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportStateExamAverageScoreGen;

import java.util.Arrays;
import java.util.List;

/**
 * Сводка о средних баллах абитуриентов, поступающих по ЕГЭ
 */
public class EnrReportStateExamAverageScore extends EnrReportStateExamAverageScoreGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportStateExamAverageScore";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(
            P_STAGE,
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET
    );

    public static IEnrStorableReportDesc getDescription() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportStateExamAverageScore.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportStateExamAverageScoreAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводка о средних баллах абитуриентов, поступающих по ЕГЭ»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводка о средних баллах абитуриентов, поступающих по ЕГЭ»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }

}