/* $Id: EntrantPrintBlock.java 33259 2014-03-27 11:59:44Z azhebko $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.entrant;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintColumnIndex;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 31.08.2011
 */
public class EntrantPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentCampaign = new ReportPrintCheckbox();
    private IReportPrintCheckbox _entrantPersonalNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _state = new ReportPrintCheckbox();
    private IReportPrintCheckbox _specialExamConditions = new ReportPrintCheckbox();

    private IReportPrintCheckbox _armyData = new ReportPrintCheckbox();


    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 32; i++)
            ids.add("chEntrant" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {
        modify(dql, printInfo, Collections.emptyList());
    }

    public void modify(ReportDQL dql, final IReportPrintInfo printInfo, List<IReportPrintBlock> additionalBlocks) {

        // добавляем колонку удостоверения личности
        int i = dql.addLastAggregateColumn(Person.identityCard());

        // соединяем абитуриента через left join, потому что мог быть выбран фильтр "не является абитуриентом"
        // если абитуриент был ранее соединен по inner join, то останется inner join
        String entrantAlias = dql.leftJoinEntity(EnrEntrant.class, EnrEntrant.person());

        // добавляем колонку entrant, агрегируем в список
        final int entrantIndex = dql.addListAggregateColumn(entrantAlias);

        // добавляем стандартные колонки персоны
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintColumnIndex());
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("fullFio", i, IdentityCard.fullFio()));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("sex", i, IdentityCard.sex().shortTitle()));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("birthDate", i, IdentityCard.birthDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("cardType", i, IdentityCard.cardType().shortTitle()));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("seria", i, IdentityCard.seria()));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("number", i, IdentityCard.number()));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("issuanceDate", i, IdentityCard.issuanceDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("issuancePlace", i, IdentityCard.issuancePlace()));


        // дополнительные колонки из персон
        for (IReportPrintBlock additionalBlock : additionalBlocks) {
            additionalBlock.modify(dql, printInfo);
        }

        // колонки абитуриента
        if (_enrollmentCampaign.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("enrollmentCampaign", entrantIndex, EnrEntrant.enrollmentCampaign().title()));

        if (_entrantPersonalNumber.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("entrantPersonalNumber", entrantIndex, EnrEntrant.personalNumber()));

        if (_state.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("state", entrantIndex, EnrEntrant.state().title()));

        if (_specialExamConditions.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("specialExamConditions", entrantIndex, null, new IFormatter<EnrEntrant>()
            {
                @Override
                public String format(EnrEntrant source)
                {
                    String specialExamConditionsDetails = StringUtils.trimToNull(source.getSpecialExamConditionsDetails());
                    return source.isNeedSpecialExamConditions() ? "нуждается" + (specialExamConditionsDetails == null ? "" : " (" + specialExamConditionsDetails + ")") : "не нуждается";
                }
            }));





        if (_armyData.isActive())
        {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("armyData", entrantIndex, null, new IFormatter<EnrEntrant>()
            {
                @Override
                public String format(EnrEntrant source)
                {
                    if (source.isPassArmy()) {
                        return CommonBaseStringUtil.joinWithSeparator(", ",
                                "да",
                                CommonBaseStringUtil.joinWithSeparator(
                                        " ",
                                        source.getBeginArmy() != null ? "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getBeginArmy()) : "",
                                        source.getEndArmy() != null ? "по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndArmy()) : ""
                                ),
                                source.getEndArmyYear() != null && source.getEndArmyYear() != 0 ? "уволен в запас в " + String.valueOf(source.getEndArmyYear()) + " г." : ""
                        );
                    } else {
                        return "нет";
                    }
                }
            }));
        }
    }
    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public IReportPrintCheckbox getEntrantPersonalNumber()
    {
        return _entrantPersonalNumber;
    }

    public IReportPrintCheckbox getState()
    {
        return _state;
    }

    public IReportPrintCheckbox getSpecialExamConditions(){ return _specialExamConditions; }

    public IReportPrintCheckbox getArmyData()
    {
        return _armyData;
    }
}
