/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/17/13
 */
@Configuration
public class EnrEnrollmentStepAdd extends BusinessComponentManager
{
    public static final String REQUEST_TYPE = "requestType";

    public static final String ENROLLMENT_DATE_DS = "enrollmentDateDS";
    public static final String ENROLLMENT_STEP_KIND_DS = "enrollmentStepKindDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrRequestTypeManager.instance().requestTypeEnrCampaignDSConfig())
            .addDataSource(selectDS(ENROLLMENT_DATE_DS, enrollmentDateDSHandler()))
            .addDataSource(selectDS(ENROLLMENT_STEP_KIND_DS, EnrEnrollmentStepManager.instance().enrollmentStepKindDSHandler()))
            .create();
    }



    @Bean
    public IDefaultComboDataSourceHandler enrollmentDateDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> result = new ArrayList<>();

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EnrCampaignEnrollmentStage.class, "s")
                        .column(property("s", EnrCampaignEnrollmentStage.date()))
                        .where(eq(property("s", EnrCampaignEnrollmentStage.requestType()), commonValue(context.get(REQUEST_TYPE))))
                        .where(eq(property("s", EnrCampaignEnrollmentStage.enrollmentCampaign()), value(context.<IIdentifiable>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(likeUpper(DQLFunctions.cast(property("s", EnrCampaignEnrollmentStage.date()), PropertyType.TRIMMEDSTRING), value(CoreStringUtils.escapeLike(input.getComboFilterByValue()))))
                        .order(property("s", EnrCampaignEnrollmentStage.date()))
                        .distinct();

                if (input.getPrimaryKeys() == null || input.getPrimaryKeys().isEmpty()) {
                    builder.top(UniDao.MAX_ROWS);
                }

                long id = 1;
                for (Date date: IUniBaseDao.instance.get().<Date>getList(builder)) {
                    DataWrapper wrapper = new DataWrapper(id++, DateFormatter.DEFAULT_DATE_FORMATTER.format(date));
                    wrapper.setProperty("date", date);
                    result.add(wrapper);
                }

                context.put(UIDefines.COMBO_OBJECT_LIST, result);
                return super.execute(input, context);
            }
        };
    }
}