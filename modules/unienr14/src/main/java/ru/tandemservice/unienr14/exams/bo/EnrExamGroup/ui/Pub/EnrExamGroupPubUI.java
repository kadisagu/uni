/**
 *$Id: EnrExamGroupPubUI.java 34410 2014-05-23 15:14:07Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupPubEntrantSearchDSHandler;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.AddEdit.EnrExamGroupAddEdit;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetMarks.EnrExamGroupSetMarks;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetScheduleEventTime.EnrExamGroupSetScheduleEventTime;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 31.05.13
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id", required = true)
})
public class EnrExamGroupPubUI extends UIPresenter
{
    private EntityHolder<EnrExamGroup> _entityHolder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh(EnrExamGroup.class);
    }

    public void onClickSetMarks()
    {
        if (!ISharedBaseDao.instance.get().existsEntity(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.L_EXAM_GROUP, _entityHolder.getValue()))
            throw new ApplicationException(getConfig().getProperty("setMarks.error.noEvent"));

        getActivationBuilder().asRegion(EnrExamGroupSetMarks.class)
                .parameter("examGroupId", _entityHolder.getId())
                .activate();
    }

    public void onClickEdit()
    {
        getActivationBuilder().asRegion(EnrExamGroupAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, _entityHolder.getId())
                .activate();
    }

    public void onClickChooseExamTime()
    {
        if (EnrExamGroupManager.instance().groupDao().hasExamPassDiscipline(_entityHolder.getId()))
            throw new ApplicationException(getConfig().getProperty("setScheduleEventTime.fillException"));

        getActivationBuilder().asRegion(EnrExamGroupSetScheduleEventTime.class)
                .parameter("examGroupId", _entityHolder.getId())
                .activate();
    }

    public void onClickExcludeEntrant()
    {
        EnrExamGroupManager.instance().groupDao().doExcludeEntrant(getListenerParameterAsLong());
    }

    public void onClickPrintEnrPassSheet()
    {
        final RtfDocument document = EnrExamGroupManager.instance().enrPassSheetPrintDao().createEnrPassSheetRtf(_entityHolder.getId());
        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("enrollmentPassSheet.rtf").document(document), true);
    }

    public void onClickPrintEnrPassSheetCoded()
    {
        final RtfDocument document = EnrExamGroupManager.instance().enrPassSheetPrintDao().createEnrPassSheetCodedRtf(_entityHolder.getId());
        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("enrollmentPassSheetCoded.rtf").document(document), true);
    }

    public void onClickPrintEnrPassSheetMarks()
    {
        final RtfDocument document = EnrExamGroupManager.instance().enrPassSheetPrintDao().createEnrPassSheetMarksRtf(_entityHolder.getId());
        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("enrollmentPassSheetMarks.rtf").document(document), true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrExamGroupPubEntrantSearchDSHandler.BIND_EXAM_GROUP, _entityHolder.getValue());
    }

    // Getters & Setters

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker", _entityHolder.getValue().getTitle());
    }

    public String getExamGroupOccupSizeStr()
    {
        return DataAccessServices.dao().getCount(EnrExamPassDiscipline.class, EnrExamPassDiscipline.examGroup().s(), _entityHolder.getValue()) + "/" + _entityHolder.getValue().getSize();
    }

    public String getExamGroupStatusStr()
    {
        if (_entityHolder.getValue().isClosed())
            return getConfig().getProperty("ui.status.closed");
        else
            return getConfig().getProperty("ui.status.opened");
    }

    public String getEventListStr()
    {
        final List<EnrExamGroupScheduleEvent> eventList = DataAccessServices.dao().getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup(), _entityHolder.getValue(), EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());

        final StringBuilder string = new StringBuilder();

        for (EnrExamGroupScheduleEvent event : eventList)
        {
            if (string.length() > 0)
                string.append("\n");

            string
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getExamScheduleEvent().getScheduleEvent().getDurationBegin()))
                    .append(", ")
                    .append(DateFormatter.DATE_FORMATTER_TIME.format(event.getExamScheduleEvent().getScheduleEvent().getDurationBegin()))
                    .append("-")
                    .append(DateFormatter.DATE_FORMATTER_TIME.format(event.getExamScheduleEvent().getScheduleEvent().getDurationEnd()))
                    .append(", ")
                    .append(event.getExamScheduleEvent().getExamRoom().getPlace().getTitleWithLocation());
            if (event.getExamScheduleEvent().getCommission() != null)
                string.append(", ").append(getConfig().getProperty("commission")).append(": ").append(event.getExamScheduleEvent().getCommission());
        }

        return string.toString();
    }

    public Map<String, Object> getEntrantPubLinkParametersMap()
    {
        return new ParametersMap().add("selectedTab", "requestTab");
    }

    // Accessors

    public EntityHolder<EnrExamGroup> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrExamGroup> entityHolder)
    {
        _entityHolder = entityHolder;
    }
}
