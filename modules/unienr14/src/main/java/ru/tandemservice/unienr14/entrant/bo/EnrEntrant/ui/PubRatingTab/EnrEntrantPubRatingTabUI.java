/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRatingTab;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrChosenEntranceExamNumberFormatter;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/13/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantPubRatingTabUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();

    private Map<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> requestTypeMap;

    private EnrRequestType currentRequestType;
    private RatingRowWrapper currentRow;

    private boolean entranceExamIncorrect;
    private boolean examListIncorrect;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setEntranceExamIncorrect(EnrEntrantManager.instance().dao().isEntranceExamIncorrect(getEntrant()));
        setExamListIncorrect(EnrEntrantManager.instance().dao().isExamListIncorrect(getEntrant()));

        requestTypeMap = Maps.newTreeMap(new Comparator<EnrRequestType>()
        {
            @Override
            public int compare(EnrRequestType t1, EnrRequestType t2)
            {
                return t1.getPriority() - t2.getPriority();
            }
        });

        List<EnrEntrantAchievement> entrantAchievementList = IUniBaseDao.instance.get().getList(EnrEntrantAchievement.class, EnrEntrantAchievement.entrant(), getEntrant());
        Map<EnrRequestType, List<EnrEntrantAchievement>> entrantAchievementMap = Maps.newHashMap();
        for (EnrEntrantAchievement achievement : entrantAchievementList)
        {
            EnrRequestType type = achievement.getType().getAchievementKind().getRequestType();
            if (!entrantAchievementMap.containsKey(type))
                entrantAchievementMap.put(type, Lists.<EnrEntrantAchievement>newArrayList());
            entrantAchievementMap.get(type).add(achievement);
        }

        for (EnrRequestedCompetition requestedCompetition : IUniBaseDao.instance.get().getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant(), getEntrant())) {
            if (requestedCompetition.getRequest().isTakeAwayDocument())
                continue;

            RatingRowWrapper wrapper = new RatingRowWrapper(requestedCompetition);

            EnrRequestType type = requestedCompetition.getRequest().getType();
            List<EnrEntrantAchievement> achievements = entrantAchievementMap.get(type);
            if (null != achievements)
                wrapper.setEntrantAchievementList(achievements);

            Map<EnrRequestedCompetition, RatingRowWrapper> map = Maps.newHashMap();
            map.put(requestedCompetition, wrapper);

            if (!requestTypeMap.containsKey(type))
                requestTypeMap.put(type, Lists.<Map<EnrRequestedCompetition, RatingRowWrapper>>newArrayList());
            requestTypeMap.get(type).add(map);
        }

        List<EnrRatingItem> ratingItemList = IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.entrant(), getEntrant());
        List<EnrChosenEntranceExam> examList = IUniBaseDao.instance.get().getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().request().entrant(), getEntrant());
        List<EnrEnrollmentExtract> extractList = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity().request().entrant(), getEntrant());

        List<EnrEnrollmentStepItem> stepItemList = new DQLSelectBuilder().fromEntity(EnrEnrollmentStepItem.class, "s").column("s")
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().request().entrant().fromAlias("s")), value(getEntrant())))
                .where(ne(property(EnrEnrollmentStepItem.step().state().code().fromAlias("s")), value(EnrEnrollmentStepStateCodes.CLOSED)))
                .createStatement(_uiSupport.getSession()).list();

        for (Map.Entry<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> entry : requestTypeMap.entrySet())
        {
            for (Map<EnrRequestedCompetition, RatingRowWrapper> map : entry.getValue())
            {
                for (EnrRatingItem rating : ratingItemList)
                {
                    RatingRowWrapper wrapper = map.get(rating.getRequestedCompetition());
                    if (null != wrapper) wrapper.setRating(rating);
                }
                for (EnrChosenEntranceExam exam : examList)
                {
                    RatingRowWrapper wrapper = map.get(exam.getRequestedCompetition());
                    if (null != wrapper) wrapper.getExams().add(exam);
                }
                for (EnrEnrollmentExtract extract : extractList)
                {
                    RatingRowWrapper wrapper = map.get(extract.getEntity());
                    if (null != wrapper) {
                        wrapper.setExtract(extract);
                        if (extract.isCancelled()) {
                            EnrCancelExtract cancelExtract = IUniBaseDao.instance.get().get(EnrCancelExtract.class, EnrCancelExtract.entity(), extract);
                            wrapper.setCancelExtract(cancelExtract);
                        }
                    }
                }
                for (EnrEnrollmentStepItem stepItem : stepItemList)
                {
                    RatingRowWrapper wrapper = map.get(stepItem.getEntity().getRequestedCompetition());
                    if (null != wrapper) wrapper.getEnrollmentItemList().add(stepItem);
                }
            }
        }
    }

    // inner classes

    public static class RatingRowWrapper implements ITitled {

        private EnrRequestedCompetition requestedCompetition;
        private EnrRatingItem rating;
        private Set<EnrChosenEntranceExam> exams = new HashSet<>();
        private EnrEnrollmentExtract extract;
        private List<EnrEnrollmentStepItem> enrollmentItemList = Lists.newArrayList();
        private List<EnrEntrantAchievement> entrantAchievementList = Lists.newArrayList();
        private EnrEnrollmentStepItem enrollmentItem;
        private EnrCancelExtract cancelExtract;

        private String markEntranceExam;
        private String markEntrantAchievement;

        private RatingRowWrapper(EnrRequestedCompetition requestedCompetition) {
            this.requestedCompetition = requestedCompetition;
        }

        public Set<EnrChosenEntranceExam> getExams() { return exams; }
        public EnrRatingItem getRating() { return rating; }
        public void setRating(EnrRatingItem rating) { this.rating = rating; }
        public List<EnrEnrollmentStepItem> getEnrollmentItemList() { return enrollmentItemList; }
        public void setEnrollmentItemList(List<EnrEnrollmentStepItem> enrollmentItemList) { this.enrollmentItemList = enrollmentItemList; }
        public List<EnrEntrantAchievement> getEntrantAchievementList() { return this.entrantAchievementList; }
        public void setEntrantAchievementList(List<EnrEntrantAchievement> entrantAchievementList) { this.entrantAchievementList = entrantAchievementList; }
        public EnrEnrollmentStepItem getEnrollmentItem() { return enrollmentItem; }
        public void setEnrollmentItem(EnrEnrollmentStepItem enrollmentItem) { this.enrollmentItem = enrollmentItem; }
        public EnrEnrollmentExtract getExtract() { return extract; }
        public void setExtract(EnrEnrollmentExtract extract) { this.extract = extract; }

        public EnrRequestedCompetition getRequestedCompetition() { return requestedCompetition; }
        public EnrCompetition getCompetition() { return requestedCompetition.getCompetition(); }
        @Override public String getTitle() { return requestedCompetition.getTitle(); }

        public String getMarkEntranceExam() {
            if (null != markEntranceExam) return markEntranceExam;
            if (getExams().isEmpty()) {
                return markEntranceExam = (getCompetition().getExamSetVariant().getExamSet().isEmptySet() || getCompetition().isNoExams()) ? "не предусмотрены ВИ" : "не выбраны ВИ";
            }

            StringBuilder b = new StringBuilder();
            List<EnrChosenEntranceExam> examList = new ArrayList<>(exams);
            Collections.sort(examList, EnrChosenEntranceExamNumberFormatter.COMPARATOR);
            EnrChosenEntranceExamNumberFormatter numberFormatter = new EnrChosenEntranceExamNumberFormatter(examList);
            for (EnrChosenEntranceExam exam : examList) {
                if (b.length() != 0) b.append("\n");
                b.append(numberFormatter.format(exam));
                b.append(exam.getDiscipline().getTitle()).append(": ");
                b.append(exam.getMarkSource() == null ? "—" : exam.getMarkSource().getInfoString());
            }

            markEntranceExam = NewLineFormatter.SIMPLE.format(b.toString());

            if (getRating()!= null && !getRating().isStatusEntranceExamsCorrect()) {
                markEntranceExam = "<div style=\"color: " + UniDefines.COLOR_CELL_ERROR + ";\">" + markEntranceExam + "</div>";
            }

            return markEntranceExam;
        }

        public String getMarkEntrantAchievement()
        {
            if (null != markEntrantAchievement) return markEntrantAchievement;
            if (getEntrantAchievementList().isEmpty()) return markEntrantAchievement = "";

            List<EnrEntrantAchievement> achievements = Lists.newArrayList(entrantAchievementList);
            Collections.sort(achievements, new EntityComparator<EnrEntrantAchievement>(new EntityOrder(EnrEntrantAchievement.mark(), OrderDirection.desc)));

            StringBuilder b = new StringBuilder();

            int i = 0;
            for (EnrEntrantAchievement achievement : achievements)
            {
                if(achievement.getType().isForRequest() && !getRequestedCompetition().getRequest().equals(achievement.getRequest()))
                {
                    continue;
                }

                EnrEntrantAchievementType type = achievement.getType();
                long mark = type.isMarked() ? achievement.getRatingMarkAsLong() : type.getAchievementMarkAsLong();

                if (b.length() != 0) b.append("\n");
                b.append(++i);
                b.append(". ");
                b.append(type.getAchievementKind().getShortTitle());
                b.append(" — ");
                b.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark * 0.001d));
            }
            return markEntrantAchievement = NewLineFormatter.SIMPLE.format(b.toString());
        }

        public boolean isShowOrder() {
            return getExtract() != null;
        }

        public String getEnrollmentString() {
            if (getEnrollmentItem() == null) return "";
            if (getEnrollmentItem().isRecommended()) return "Рекомендован к зачислению на " + getEnrollmentItem().getStep().getDateStr();
            if (getEnrollmentItem().isIncluded()) return "Включен в конкурсный список на " + getEnrollmentItem().getStep().getDateStr();
            return "Не включен в список шага зачисления на " + getEnrollmentItem().getStep().getDateStr();
        }

        public String getEduInstitutionAvgMark()
        {
            if(getRating() == null) return "";
            PersonEduDocument eduDocument = getRating().getRequestedCompetition().getRequest().getEduDocument();
            return eduDocument != null && eduDocument.getAvgMarkAsDouble() != null ? LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE.format(eduDocument.getAvgMarkAsLong()) : "";
        }

        public Map getEnrollmentPubParams() {
            if (getEnrollmentItem() == null) return null;
            ParametersMap params = new ParametersMap()
                .add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, getEnrollmentItem().getStep().getId())
                .add(EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, getEnrollmentItem().getEntity().getCompetition().getId());
            if (getCompetition().getProgramSetOrgUnit().getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()
                && getEnrollmentItem().getEntity().getRequestedCompetition() instanceof EnrRequestedCompetitionTA) {
                EnrRequestedCompetitionTA reqComp = (EnrRequestedCompetitionTA) getEnrollmentItem().getEntity().getRequestedCompetition();
                params.add(EnrEnrollmentStepCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, reqComp.getTargetAdmissionKind().getTargetAdmissionKind().getId());
            }
            return params;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RatingRowWrapper wrapper = (RatingRowWrapper) o;
            return requestedCompetition.equals(wrapper.requestedCompetition);
        }

        public EnrCancelExtract getCancelExtract()
        {
            return cancelExtract;
        }

        public void setCancelExtract(EnrCancelExtract cancelExtract)
        {
            this.cancelExtract = cancelExtract;
        }
    }

    public void onClickRefresh() {
        EnrEntrantDaemonBean.DAEMON.wakeUpAndWaitDaemon(60);
        this.getSupport().setRefreshScheduled(true);
    }

    // presenter

    public String getDaemonStatus(final String message) {
        final Long date = EnrEntrantDaemonBean.DAEMON.getCompleteStatus();
        if (null != date) {
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        }
        return message;
    }

    public int getCurrentRowNumber() {
        return getRowList().indexOf(getCurrentRow()) + 1;
    }

    public boolean isDisplayAnyError() {
        return isEntranceExamIncorrect() || isExamListIncorrect();
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public RatingRowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(RatingRowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public EnrRatingItem getCurrentRowRating()
    {
        return getCurrentRow().getRating();
    }

    public boolean isBakSpecMag()
    {
        String code = getCurrentRequestType().getCode();
        return EnrRequestTypeCodes.BS.equals(code) || EnrRequestTypeCodes.MASTER.equals(code);
    }

    public boolean isTrainInter()
    {
        String code = getCurrentRequestType().getCode();
        return EnrRequestTypeCodes.INTERNSHIP.equals(code) || EnrRequestTypeCodes.TRAINEESHIP.equals(code);
    }

    public boolean isSpo()
    {
        return EnrRequestTypeCodes.SPO.equals(getCurrentRequestType().getCode());
    }

    public List<RatingRowWrapper> getRowList()
    {
        List<RatingRowWrapper> rows = Lists.newArrayList();
        for (Map<EnrRequestedCompetition, RatingRowWrapper> map : getRequestTypeMap().get(getCurrentRequestType()))
        {
            rows.addAll(map.values());
        }

        Collections.sort(rows, new Comparator<RatingRowWrapper>()
        {
            @Override
            public int compare(RatingRowWrapper o1, RatingRowWrapper o2)
            {
                return o1.getRequestedCompetition().getPriority() - o2.getRequestedCompetition().getPriority();
            }
        });
        return rows;
    }

    public Set<EnrRequestType> getRequestTypeList()
    {
        return getRequestTypeMap().keySet();
    }

    public Map<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> getRequestTypeMap()
    {
        return requestTypeMap;
    }

    public void setRequestTypeMap(Map<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> requestTypeMap)
    {
        this.requestTypeMap = requestTypeMap;
    }

    public EnrRequestType getCurrentRequestType()
    {
        return currentRequestType;
    }

    public void setCurrentRequestType(EnrRequestType currentRequestType)
    {
        this.currentRequestType = currentRequestType;
    }

    public boolean isEntranceExamIncorrect()
    {
        return entranceExamIncorrect;
    }

    public void setEntranceExamIncorrect(boolean entranceExamIncorrect)
    {
        this.entranceExamIncorrect = entranceExamIncorrect;
    }

    public boolean isExamListIncorrect()
    {
        return examListIncorrect;
    }

    public void setExamListIncorrect(boolean examListIncorrect)
    {
        this.examListIncorrect = examListIncorrect;
    }
}