/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.EditParams;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class EnrIndividualAchievementEditParamsUI extends UIPresenter
{
    private EntityHolder<EnrEntrantAchievementSettings> _entityHolder = new EntityHolder<>();
    private double _maxTotalAchievementMark;

    @Override
    public void onComponentRefresh()
    {
        getEntityHolder().refresh();
        setMaxTotalAchievementMark(getAchievementSettings().getMaxTotalAchievementMark());
    }

    // Listeners

    public void onClickApply()
    {
        getAchievementSettings().setMaxTotalAchievementMark(getMaxTotalAchievementMark());
        if (!getAchievementSettings().validate(ContextLocal.getErrorCollector()).hasErrors())
        {
            DataAccessServices.dao().update(getAchievementSettings());
            deactivate();
        }
        EnrEntrantDaemonBean.DAEMON.wakeUpDaemon();
    }

    // Getters && Setters

    public double getMaxTotalAchievementMark()
    {
        return _maxTotalAchievementMark;
    }

    public void setMaxTotalAchievementMark(double maxTotalAchievementMark)
    {
        _maxTotalAchievementMark = maxTotalAchievementMark;
    }

    public EntityHolder<EnrEntrantAchievementSettings> getEntityHolder(){ return _entityHolder; }
    public EnrEntrantAchievementSettings getAchievementSettings(){ return getEntityHolder().getValue(); }
}