package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Способ ранжирования абитуриентов при равенстве суммы конкурсных баллов (бакалавриат, специалитет, магистратура)"
 * Имя сущности : enrTieBreakRatingRule
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrTieBreakRatingRuleCodes
{
    /** Константа кода (code) элемента : По убыванию суммы конкурсных баллов, начисленных по результатам вступительных испытаний, затем по убыванию количества баллов, начисленных по результатам отдельных вступительных испытаний, в соответствии с приоритетностью вступительных испытаний (title) */
    String SUM_THEN_PRIORITY = "1";
    /** Константа кода (code) элемента : По убыванию суммы конкурсных баллов, начисленных по результатам вступительных испытаний (title) */
    String SUM = "2";
    /** Константа кода (code) элемента : По убыванию количества баллов, начисленных по результатам отдельных вступительных испытаний, в соответствии с приоритетностью вступительных испытаний (title) */
    String PRIORITY = "3";

    Set<String> CODES = ImmutableSet.of(SUM_THEN_PRIORITY, SUM, PRIORITY);
}
