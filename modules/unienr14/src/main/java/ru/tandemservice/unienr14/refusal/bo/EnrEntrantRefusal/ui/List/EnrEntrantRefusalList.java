/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.ui.List;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.logic.EnrEntrantRefusalListDSHandler;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author rsizonenko
 * @since 20.05.2014.
 */

@Configuration
public class EnrEntrantRefusalList extends BusinessComponentManager
{
    public static final String ENR_ORG_UNIT = "enrOrgUnit";
    public static final String ENR_ENTRANT_REFUSAL_COMPETITION_DATA = "enrEntrantRefusalCompetitionData";
    public static final String ENR_ENTRANT_REFUSAL_DATE = "enrEntrantRefusalDate";
    public static final String ENR_ENTRANT_REFUSAL_REASON = "enrEntrantRefusalReason";
    public static final String ENR_ENTRANT_REFUSAL_COMMENT = "enrEntrantRefusalComment";
    public static final String ENR_ENTRANT_REFUSAL_FULL_NAME = "enrEntrantRefusalFullName";

    public static final String ENR_ENROLLMENT_CAMPAIGN = "enrEnrollmentCampaign";
    public static final String ENR_ENROLLMENT_CAMPAIGN_DS = "enrEnrollmentCampaignDS";
    public static final String ENR_ORG_UNIT_DS = "enrOrgUnitDS";
    public static final String ENR_ENTRANT_REFUSAL_LIST_DS = "enrEntrantRefusalListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENR_ENROLLMENT_CAMPAIGN_DS, EnrEnrollmentCampaignManager.instance().enrCampaignDSHandler()))
                .addDataSource(selectDS(ENR_ORG_UNIT_DS, enrOrgUnitDSHandler())
                        .addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(searchListDS(ENR_ENTRANT_REFUSAL_LIST_DS, enrEntrantRefusalListDSColumns(), enrEntrantRefusalListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint enrEntrantRefusalListDSColumns(){
        return columnListExtPointBuilder(ENR_ENTRANT_REFUSAL_LIST_DS)
                .addColumn(textColumn(ENR_ENTRANT_REFUSAL_FULL_NAME, "fullName").order().required(true))
                .addColumn(textColumn(ENR_ENTRANT_REFUSAL_COMPETITION_DATA, EnrEntrantRefusal.competitionData().s()))
                .addColumn(textColumn(ENR_ENTRANT_REFUSAL_REASON, EnrEntrantRefusal.refusalReason().title()).required(true))
                .addColumn(textColumn(ENR_ENTRANT_REFUSAL_COMMENT, EnrEntrantRefusal.comment().s()))
                .addColumn(textColumn(ENR_ORG_UNIT, EnrEntrantRefusal.enrOrgUnit().title()))
                .addColumn(textColumn(ENR_ENTRANT_REFUSAL_DATE, EnrEntrantRefusal.registrationDate().s()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEnrEntrantRefusal").permissionKey("enr14EntrantRefusalEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEnrEntrantRefusal", new FormattedMessage("enrEntrantRefusalListDS.deleteAlert", EnrEntrantRefusal.fullName())).permissionKey("enr14EntrantRefusalDelete"))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrOrgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(in(property(alias, EnrOrgUnit.id()), new DQLSelectBuilder()
                    .fromEntity(EnrEntrantRefusal.class, "er")
                    .column(property("er", EnrEntrantRefusal.enrOrgUnit().id()))
                    .where(eq(property("er", EnrEntrantRefusal.enrOrgUnit().enrollmentCampaign()), commonValue(context.get(ENR_ENROLLMENT_CAMPAIGN))))
                    .buildQuery()));
            }
        }
            .where(EnrOrgUnit.enrollmentCampaign(), ENR_ENROLLMENT_CAMPAIGN)
            .pageable(true);
    }

    @Bean
    public IDefaultSearchDataSourceHandler enrEntrantRefusalListDSHandler()
    {
        return new EnrEntrantRefusalListDSHandler(getName());
    }

}
