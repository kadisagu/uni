package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид особого права при поступлении"
 * Имя сущности : enrBenefitType
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrBenefitTypeCodes
{
    /** Константа кода (code) элемента : Без вступительных испытаний (title) */
    String NO_ENTRANCE_EXAMS = "1";
    /** Константа кода (code) элемента : В пределах квоты (title) */
    String EXCLUSIVE = "2";
    /** Константа кода (code) элемента : Преимущественное право (title) */
    String PREFERENCE = "3";
    /** Константа кода (code) элемента : 100 баллов за вступительное испытание (title) */
    String MAX_EXAM_RESULT = "4";

    Set<String> CODES = ImmutableSet.of(NO_ENTRANCE_EXAMS, EXCLUSIVE, PREFERENCE, MAX_EXAM_RESULT);
}
