package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание балла (балл по апелляции ДДС)
 *
 * Создается и обновляется демоном на основе ДДС абитуриента IEnrRatingDaemonDao#doRefreshSource4Appeal (создание оснований по апелляциям ДДС абитуриента).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantMarkSourceAppealGen extends EnrEntrantMarkSource
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceAppeal";
    public static final String ENTITY_NAME = "enrEntrantMarkSourceAppeal";
    public static final int VERSION_HASH = 111474489;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_PASS_APPEAL = "examPassAppeal";

    private EnrExamPassAppeal _examPassAppeal;     // Апелляция ДДС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Апелляция ДДС.
     */
    public EnrExamPassAppeal getExamPassAppeal()
    {
        return _examPassAppeal;
    }

    /**
     * @param examPassAppeal Апелляция ДДС.
     */
    public void setExamPassAppeal(EnrExamPassAppeal examPassAppeal)
    {
        dirty(_examPassAppeal, examPassAppeal);
        _examPassAppeal = examPassAppeal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEntrantMarkSourceAppealGen)
        {
            setExamPassAppeal(((EnrEntrantMarkSourceAppeal)another).getExamPassAppeal());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantMarkSourceAppealGen> extends EnrEntrantMarkSource.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantMarkSourceAppeal.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantMarkSourceAppeal();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "examPassAppeal":
                    return obj.getExamPassAppeal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "examPassAppeal":
                    obj.setExamPassAppeal((EnrExamPassAppeal) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examPassAppeal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examPassAppeal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "examPassAppeal":
                    return EnrExamPassAppeal.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantMarkSourceAppeal> _dslPath = new Path<EnrEntrantMarkSourceAppeal>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantMarkSourceAppeal");
    }
            

    /**
     * @return Апелляция ДДС.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceAppeal#getExamPassAppeal()
     */
    public static EnrExamPassAppeal.Path<EnrExamPassAppeal> examPassAppeal()
    {
        return _dslPath.examPassAppeal();
    }

    public static class Path<E extends EnrEntrantMarkSourceAppeal> extends EnrEntrantMarkSource.Path<E>
    {
        private EnrExamPassAppeal.Path<EnrExamPassAppeal> _examPassAppeal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Апелляция ДДС.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceAppeal#getExamPassAppeal()
     */
        public EnrExamPassAppeal.Path<EnrExamPassAppeal> examPassAppeal()
        {
            if(_examPassAppeal == null )
                _examPassAppeal = new EnrExamPassAppeal.Path<EnrExamPassAppeal>(L_EXAM_PASS_APPEAL, this);
            return _examPassAppeal;
        }

        public Class getEntityClass()
        {
            return EnrEntrantMarkSourceAppeal.class;
        }

        public String getEntityName()
        {
            return "enrEntrantMarkSourceAppeal";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
