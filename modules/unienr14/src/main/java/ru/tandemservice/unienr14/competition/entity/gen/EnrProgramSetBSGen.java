package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор ОП для приема (бакалавры и специалисты)
 *
 * Определяет перечень ОП, на который формируются конкурсы, для приема на бакалавриат и специалитет.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetBSGen extends EnrProgramSetBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS";
    public static final String ENTITY_NAME = "enrProgramSetBS";
    public static final int VERSION_HASH = -435525654;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET_VARIANT_BASE = "examSetVariantBase";
    public static final String L_EXAM_SET_VARIANT_SEC = "examSetVariantSec";
    public static final String L_EXAM_SET_VARIANT_HIGH = "examSetVariantHigh";
    public static final String L_EXAM_SET_VARIANT_MIN_BASE = "examSetVariantMinBase";
    public static final String L_EXAM_SET_VARIANT_CON_BASE = "examSetVariantConBase";
    public static final String L_EXAM_SET_VARIANT_MIN_PROF = "examSetVariantMinProf";
    public static final String L_EXAM_SET_VARIANT_CON_PROF = "examSetVariantConProf";

    private EnrExamSetVariant _examSetVariantBase;     // Набор ВИ на базе СОО
    private EnrExamSetVariant _examSetVariantSec;     // Набор ВИ на базе СПО
    private EnrExamSetVariant _examSetVariantHigh;     // Набор ВИ на базе ВО
    private EnrExamSetVariant _examSetVariantMinBase;     // Набор ВИ для приема на бюджет на основе СОО (старый)
    private EnrExamSetVariant _examSetVariantConBase;     // Набор ВИ для приема на контракт на основе СОО (старый)
    private EnrExamSetVariant _examSetVariantMinProf;     // Набор ВИ для приема на бюджет на основе ПО (старый)
    private EnrExamSetVariant _examSetVariantConProf;     // Набор ВИ для приема на контракт на основе ПО (старый)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ВИ на базе СОО.
     */
    public EnrExamSetVariant getExamSetVariantBase()
    {
        return _examSetVariantBase;
    }

    /**
     * @param examSetVariantBase Набор ВИ на базе СОО.
     */
    public void setExamSetVariantBase(EnrExamSetVariant examSetVariantBase)
    {
        dirty(_examSetVariantBase, examSetVariantBase);
        _examSetVariantBase = examSetVariantBase;
    }

    /**
     * @return Набор ВИ на базе СПО.
     */
    public EnrExamSetVariant getExamSetVariantSec()
    {
        return _examSetVariantSec;
    }

    /**
     * @param examSetVariantSec Набор ВИ на базе СПО.
     */
    public void setExamSetVariantSec(EnrExamSetVariant examSetVariantSec)
    {
        dirty(_examSetVariantSec, examSetVariantSec);
        _examSetVariantSec = examSetVariantSec;
    }

    /**
     * @return Набор ВИ на базе ВО.
     */
    public EnrExamSetVariant getExamSetVariantHigh()
    {
        return _examSetVariantHigh;
    }

    /**
     * @param examSetVariantHigh Набор ВИ на базе ВО.
     */
    public void setExamSetVariantHigh(EnrExamSetVariant examSetVariantHigh)
    {
        dirty(_examSetVariantHigh, examSetVariantHigh);
        _examSetVariantHigh = examSetVariantHigh;
    }

    /**
     * @return Набор ВИ для приема на бюджет на основе СОО (старый).
     */
    public EnrExamSetVariant getExamSetVariantMinBase()
    {
        return _examSetVariantMinBase;
    }

    /**
     * @param examSetVariantMinBase Набор ВИ для приема на бюджет на основе СОО (старый).
     */
    public void setExamSetVariantMinBase(EnrExamSetVariant examSetVariantMinBase)
    {
        dirty(_examSetVariantMinBase, examSetVariantMinBase);
        _examSetVariantMinBase = examSetVariantMinBase;
    }

    /**
     * @return Набор ВИ для приема на контракт на основе СОО (старый).
     */
    public EnrExamSetVariant getExamSetVariantConBase()
    {
        return _examSetVariantConBase;
    }

    /**
     * @param examSetVariantConBase Набор ВИ для приема на контракт на основе СОО (старый).
     */
    public void setExamSetVariantConBase(EnrExamSetVariant examSetVariantConBase)
    {
        dirty(_examSetVariantConBase, examSetVariantConBase);
        _examSetVariantConBase = examSetVariantConBase;
    }

    /**
     * @return Набор ВИ для приема на бюджет на основе ПО (старый).
     */
    public EnrExamSetVariant getExamSetVariantMinProf()
    {
        return _examSetVariantMinProf;
    }

    /**
     * @param examSetVariantMinProf Набор ВИ для приема на бюджет на основе ПО (старый).
     */
    public void setExamSetVariantMinProf(EnrExamSetVariant examSetVariantMinProf)
    {
        dirty(_examSetVariantMinProf, examSetVariantMinProf);
        _examSetVariantMinProf = examSetVariantMinProf;
    }

    /**
     * @return Набор ВИ для приема на контракт на основе ПО (старый).
     */
    public EnrExamSetVariant getExamSetVariantConProf()
    {
        return _examSetVariantConProf;
    }

    /**
     * @param examSetVariantConProf Набор ВИ для приема на контракт на основе ПО (старый).
     */
    public void setExamSetVariantConProf(EnrExamSetVariant examSetVariantConProf)
    {
        dirty(_examSetVariantConProf, examSetVariantConProf);
        _examSetVariantConProf = examSetVariantConProf;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrProgramSetBSGen)
        {
            setExamSetVariantBase(((EnrProgramSetBS)another).getExamSetVariantBase());
            setExamSetVariantSec(((EnrProgramSetBS)another).getExamSetVariantSec());
            setExamSetVariantHigh(((EnrProgramSetBS)another).getExamSetVariantHigh());
            setExamSetVariantMinBase(((EnrProgramSetBS)another).getExamSetVariantMinBase());
            setExamSetVariantConBase(((EnrProgramSetBS)another).getExamSetVariantConBase());
            setExamSetVariantMinProf(((EnrProgramSetBS)another).getExamSetVariantMinProf());
            setExamSetVariantConProf(((EnrProgramSetBS)another).getExamSetVariantConProf());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetBSGen> extends EnrProgramSetBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetBS.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetBS();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariantBase":
                    return obj.getExamSetVariantBase();
                case "examSetVariantSec":
                    return obj.getExamSetVariantSec();
                case "examSetVariantHigh":
                    return obj.getExamSetVariantHigh();
                case "examSetVariantMinBase":
                    return obj.getExamSetVariantMinBase();
                case "examSetVariantConBase":
                    return obj.getExamSetVariantConBase();
                case "examSetVariantMinProf":
                    return obj.getExamSetVariantMinProf();
                case "examSetVariantConProf":
                    return obj.getExamSetVariantConProf();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "examSetVariantBase":
                    obj.setExamSetVariantBase((EnrExamSetVariant) value);
                    return;
                case "examSetVariantSec":
                    obj.setExamSetVariantSec((EnrExamSetVariant) value);
                    return;
                case "examSetVariantHigh":
                    obj.setExamSetVariantHigh((EnrExamSetVariant) value);
                    return;
                case "examSetVariantMinBase":
                    obj.setExamSetVariantMinBase((EnrExamSetVariant) value);
                    return;
                case "examSetVariantConBase":
                    obj.setExamSetVariantConBase((EnrExamSetVariant) value);
                    return;
                case "examSetVariantMinProf":
                    obj.setExamSetVariantMinProf((EnrExamSetVariant) value);
                    return;
                case "examSetVariantConProf":
                    obj.setExamSetVariantConProf((EnrExamSetVariant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariantBase":
                        return true;
                case "examSetVariantSec":
                        return true;
                case "examSetVariantHigh":
                        return true;
                case "examSetVariantMinBase":
                        return true;
                case "examSetVariantConBase":
                        return true;
                case "examSetVariantMinProf":
                        return true;
                case "examSetVariantConProf":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariantBase":
                    return true;
                case "examSetVariantSec":
                    return true;
                case "examSetVariantHigh":
                    return true;
                case "examSetVariantMinBase":
                    return true;
                case "examSetVariantConBase":
                    return true;
                case "examSetVariantMinProf":
                    return true;
                case "examSetVariantConProf":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariantBase":
                    return EnrExamSetVariant.class;
                case "examSetVariantSec":
                    return EnrExamSetVariant.class;
                case "examSetVariantHigh":
                    return EnrExamSetVariant.class;
                case "examSetVariantMinBase":
                    return EnrExamSetVariant.class;
                case "examSetVariantConBase":
                    return EnrExamSetVariant.class;
                case "examSetVariantMinProf":
                    return EnrExamSetVariant.class;
                case "examSetVariantConProf":
                    return EnrExamSetVariant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetBS> _dslPath = new Path<EnrProgramSetBS>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetBS");
    }
            

    /**
     * @return Набор ВИ на базе СОО.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantBase()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantBase()
    {
        return _dslPath.examSetVariantBase();
    }

    /**
     * @return Набор ВИ на базе СПО.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantSec()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantSec()
    {
        return _dslPath.examSetVariantSec();
    }

    /**
     * @return Набор ВИ на базе ВО.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantHigh()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantHigh()
    {
        return _dslPath.examSetVariantHigh();
    }

    /**
     * @return Набор ВИ для приема на бюджет на основе СОО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantMinBase()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantMinBase()
    {
        return _dslPath.examSetVariantMinBase();
    }

    /**
     * @return Набор ВИ для приема на контракт на основе СОО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantConBase()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantConBase()
    {
        return _dslPath.examSetVariantConBase();
    }

    /**
     * @return Набор ВИ для приема на бюджет на основе ПО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantMinProf()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantMinProf()
    {
        return _dslPath.examSetVariantMinProf();
    }

    /**
     * @return Набор ВИ для приема на контракт на основе ПО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantConProf()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantConProf()
    {
        return _dslPath.examSetVariantConProf();
    }

    public static class Path<E extends EnrProgramSetBS> extends EnrProgramSetBase.Path<E>
    {
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantBase;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantSec;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantHigh;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantMinBase;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantConBase;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantMinProf;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantConProf;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ВИ на базе СОО.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantBase()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantBase()
        {
            if(_examSetVariantBase == null )
                _examSetVariantBase = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_BASE, this);
            return _examSetVariantBase;
        }

    /**
     * @return Набор ВИ на базе СПО.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantSec()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantSec()
        {
            if(_examSetVariantSec == null )
                _examSetVariantSec = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_SEC, this);
            return _examSetVariantSec;
        }

    /**
     * @return Набор ВИ на базе ВО.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantHigh()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantHigh()
        {
            if(_examSetVariantHigh == null )
                _examSetVariantHigh = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_HIGH, this);
            return _examSetVariantHigh;
        }

    /**
     * @return Набор ВИ для приема на бюджет на основе СОО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantMinBase()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantMinBase()
        {
            if(_examSetVariantMinBase == null )
                _examSetVariantMinBase = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_MIN_BASE, this);
            return _examSetVariantMinBase;
        }

    /**
     * @return Набор ВИ для приема на контракт на основе СОО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantConBase()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantConBase()
        {
            if(_examSetVariantConBase == null )
                _examSetVariantConBase = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_CON_BASE, this);
            return _examSetVariantConBase;
        }

    /**
     * @return Набор ВИ для приема на бюджет на основе ПО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantMinProf()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantMinProf()
        {
            if(_examSetVariantMinProf == null )
                _examSetVariantMinProf = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_MIN_PROF, this);
            return _examSetVariantMinProf;
        }

    /**
     * @return Набор ВИ для приема на контракт на основе ПО (старый).
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS#getExamSetVariantConProf()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantConProf()
        {
            if(_examSetVariantConProf == null )
                _examSetVariantConProf = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_CON_PROF, this);
            return _examSetVariantConProf;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetBS.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetBS";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
