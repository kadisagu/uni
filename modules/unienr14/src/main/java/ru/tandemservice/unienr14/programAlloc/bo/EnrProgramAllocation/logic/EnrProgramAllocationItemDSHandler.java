/* $Id:$ */
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.Pub.EnrProgramAllocationPub;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/30/14
 */
public class EnrProgramAllocationItemDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_TA_KIND = "targetAdmissionKind";
    public static final String VIEW_PROP_FINAL_MARK = "finalMark";
    public static final String VIEW_PROP_PRIORITIES = "priorities";
    public static final String VIEW_PROP_PRIORITIES_SHORT = "priorities";
    public static final String VIEW_PROP_POSITION = "position";
    public static final String VIEW_PROP_FULL_FIO = "fullFio";
    private static final String VIEW_PROP_COMP_TYPE_PRIORITY = "compTypePriority";

    public EnrProgramAllocationItemDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrProgramAllocation allocation = DataAccessServices.dao().getNotNull(EnrProgramAllocation.class, (Long) context.get(EnrProgramAllocationPub.PARAM_ALLOCATION_ID));

        List<EnrProgramAllocationItem> items = DataAccessServices.dao().getList(EnrProgramAllocationItem.class, EnrProgramAllocationItem.allocation().s(), allocation);

        List<EnrRatingItem> ratingItems = new DQLSelectBuilder()
            .fromEntity(EnrRatingItem.class, "r").column("r")
            .joinEntity("r", DQLJoinType.inner, EnrProgramAllocationItem.class, "m", eq(property(EnrRatingItem.requestedCompetition().fromAlias("r")), property(EnrProgramAllocationItem.entrant().fromAlias("m"))))
            .where(eq(property(EnrProgramAllocationItem.allocation().fromAlias("m")), value(allocation)))
            .createStatement(context.getSession()).list();
        Map<Long, EnrRatingItem> ratingMap = new HashMap<>();
        for (EnrRatingItem r : ratingItems) {
            ratingMap.put(r.getRequestedCompetition().getId(), r);
        }

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "e")
            .joinEntity("e", DQLJoinType.inner, EnrProgramAllocationItem.class, "m", eq(property(EnrRequestedProgram.requestedCompetition().fromAlias("e")), property(EnrProgramAllocationItem.entrant().fromAlias("m"))))
            .where(eq(property(EnrProgramAllocationItem.allocation().fromAlias("m")), value(allocation)))
            .column(property(EnrRequestedProgram.requestedCompetition().id().fromAlias("e")))
            .column(property(EnrRequestedProgram.programSetItem().program().fromAlias("e")))
            .order(property(EnrRequestedProgram.priority().fromAlias("e")));

        Map<Long, List<String>> prioritiesMap = new HashMap<>();
        Map<Long, List<String>> prioritiesShortMap = new HashMap<>();
        for (Object[] row : dql.createStatement(context.getSession()).<Object[]>list())
        {
            Long entrantId = (Long) row[0];
            EduProgram program = (EduProgram) row[1];

            List<String> set = prioritiesMap.get(entrantId);
            if (set == null)
                prioritiesMap.put(entrantId, set = new ArrayList<>());
            set.add(program.getFullTitle());

            List<String> shortSet = prioritiesShortMap.get(entrantId);
            if (shortSet == null)
                prioritiesShortMap.put(entrantId, shortSet = new ArrayList<>());
            shortSet.add(program.getShortTitle());
        }

        IEnrProgramAllocationDao dao = EnrProgramAllocationManager.instance().dao();

        List<DataWrapper> wrappers = new ArrayList<>();
        for (EnrProgramAllocationItem item : items) {
            DataWrapper wrapper = new DataWrapper(item);
            wrappers.add(wrapper);
            wrapper.put(VIEW_PROP_TA_KIND, item.getTargetAdmissionKind());
            wrapper.put(VIEW_PROP_FULL_FIO, item.getEntrant().getRequest().getEntrant().getFullFio());

            EnrRatingItem ratingItem = ratingMap.get(item.getEntrant().getId());
            if (null != ratingItem) {
                wrapper.put(VIEW_PROP_FINAL_MARK, ratingItem.getTotalMarkAsString());
                wrapper.put(VIEW_PROP_POSITION, ratingItem.getPosition());
            } else {
                wrapper.put(VIEW_PROP_FINAL_MARK, "");
                wrapper.put(VIEW_PROP_POSITION, Integer.MAX_VALUE);
            }

            wrapper.put(VIEW_PROP_COMP_TYPE_PRIORITY, item.getEntrant().getCompetition().getType().getPriorityForProgramAlloc());

            List<String> priorityList = prioritiesMap.get(item.getEntrant().getId());
            wrapper.put(VIEW_PROP_PRIORITIES, priorityList == null ? null : StringUtils.join(priorityList, ", "));

            List<String> shortPriorityList = prioritiesShortMap.get(item.getEntrant().getId());
            wrapper.put(VIEW_PROP_PRIORITIES_SHORT, shortPriorityList == null ? null : StringUtils.join(shortPriorityList, ", "));
        }
        Collections.sort(wrappers, new EntityComparator<DataWrapper>(new EntityOrder(VIEW_PROP_COMP_TYPE_PRIORITY), new EntityOrder(VIEW_PROP_POSITION), new EntityOrder(VIEW_PROP_FULL_FIO)));

        DSOutput output = new DSOutput(input);
        output.setTotalSize(wrappers.size());
        List<Object> records = new ArrayList<>();
        records.addAll(wrappers.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), wrappers.size())));
        output.setRecordList(records);
        return output;
    }

}



