/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.logic;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/18/13
 */
public class EnrEntrantDQLSelectBuilder extends DQLSelectBuilder
{
    private static final String RATING = "ratingItem";
    private static final String REQ_COMP = "reqComp";
    private static final String ENTRANT = "entrant";
    private static final String REQUEST = "request";
    private static final String COMP = "comp";
    private static final String PROGRAM_SET_OU = "psOu";
    private static final String PROGRAM_SET = "ps";

    private Set<String> innerAliases = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
        REQ_COMP, ENTRANT, REQUEST, COMP)));
    private Map<String, String> aliasMap = new HashMap<>();

    public EnrEntrantDQLSelectBuilder() {
        this(false, false);
    }

    public EnrEntrantDQLSelectBuilder(boolean fetch) {
        this(fetch, false);
    }

    public EnrEntrantDQLSelectBuilder(boolean fetch, boolean startFromRating)
    {
        if (startFromRating) {
            fromEntity(EnrRatingItem.class, RATING);
            if (fetch) {
                fetchPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().fromAlias(RATING), REQ_COMP);
            } else {
                joinPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().fromAlias(RATING), REQ_COMP);
            }
        } else {
            fromEntity(EnrRequestedCompetition.class, REQ_COMP);
        }
        if (fetch)
            fetchPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias(REQ_COMP), REQUEST)
            .fetchPath(DQLJoinType.inner, EnrEntrantRequest.entrant().fromAlias(REQUEST), ENTRANT, true)
            .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias(REQ_COMP), COMP)
            .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().fromAlias(COMP), PROGRAM_SET_OU)
            .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().fromAlias(PROGRAM_SET_OU), PROGRAM_SET)
            ;
        else
            joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias(REQ_COMP), REQUEST)
            .joinPath(DQLJoinType.inner, EnrEntrantRequest.entrant().fromAlias(REQUEST), ENTRANT, true)
            .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias(REQ_COMP), COMP)
            .joinPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().fromAlias(COMP), PROGRAM_SET_OU)
            .joinPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().fromAlias(PROGRAM_SET_OU), PROGRAM_SET)
            ;
    }

    private Map<String, String> getAliasMap() { return aliasMap; }

    public String alias(String candidate) {
        if (getAliasMap().keySet().contains(candidate))
            return getAliasMap().get(candidate);
        if (innerAliases.contains(candidate)) {
            int i = 1;
            while (getAliasMap().values().contains(candidate + i)) i++;
            getAliasMap().put(candidate, candidate + i);
            return candidate + i;
        }
        getAliasMap().put(candidate, candidate);
        return candidate;
    }


    public String rating() { return RATING;}
    public String competition() { return COMP;}
    public String programSetOu() { return PROGRAM_SET_OU;}
    public String programSet() { return PROGRAM_SET;}
    
    public String reqComp() { return REQ_COMP; }
    public String request() { return REQUEST;}
    public String entrant() { return ENTRANT; }
    

    public EnrEntrantDQLSelectBuilder notArchiveOnly() {
        where(eq(property(EnrEntrant.archival().fromAlias(entrant())), value(Boolean.FALSE)));
        return this;
    }

    public EnrEntrantDQLSelectBuilder defaultActiveFilter() {
        where(eq(property(EnrEntrant.archival().fromAlias(entrant())), value(Boolean.FALSE)));
        where(ne(property(EnrRequestedCompetition.state().code().fromAlias(reqComp())), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));
        where(ne(property(EnrRequestedCompetition.state().code().fromAlias(reqComp())), value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)));
        return this;
    }

    public EnrEntrantDQLSelectBuilder filter(EnrEnrollmentCampaign enrollmentCampaign) {
        return (EnrEntrantDQLSelectBuilder) where(eq(property(EnrEntrant.enrollmentCampaign().fromAlias(entrant())), value(enrollmentCampaign)));
    }
    
    public EnrEntrantDQLSelectBuilder filter(Date dateFrom, Date dateTo) {
        FilterUtils.applyBetweenFilter(this, request(), EnrEntrantRequest.regDate().s(), dateFrom, dateTo);
        return this;
    }

    public EnrEntrantDQLSelectBuilder filter(CompensationType compensationType) {
        if (compensationType != null)
            where(eq(property(EnrCompetition.type().compensationType().fromAlias(competition())), value(compensationType)));
        return this; 
    }

    public EnrEntrantDQLSelectBuilder filter(EnrCompetitionType competitionType)
    {
        where(eq(property(EnrCompetition.type().fromAlias(competition())), value(competitionType)));
        return this;
    }

    public static interface Initializer {
        void init(EnrEntrantDQLSelectBuilder dql);
    }
    
    public static EnrEntrantDQLSelectBuilder get(Initializer initializer, boolean fetch) {
        EnrEntrantDQLSelectBuilder dql = new EnrEntrantDQLSelectBuilder(fetch);
        initializer.init(dql);
        return dql;
    }

    public static EnrEntrantDQLSelectBuilder get(Initializer initializer) {
        EnrEntrantDQLSelectBuilder dql = new EnrEntrantDQLSelectBuilder(false);
        initializer.init(dql);
        return dql;
    }
}
