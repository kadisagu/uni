/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub.EnrEnrollmentModelCompetitionInStepPub;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Configuration
public class EnrEnrollmentModelCompetitionPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS("itemDS", competitionDSColumns(), competitionDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder("itemDS")
            .addColumn(textColumn("position", EnrRatingItem.position()))
            .addColumn(textColumn("absPos", EnrRatingItem.positionAbsolute()))
            .addColumn(publisherColumn("fio", EnrEntrant.person().identityCard().fullFio())
                .entityListProperty(EnrRatingItem.requestedCompetition().request().entrant().s())
                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity) { return EnrEntrantPub.class.getSimpleName(); }

                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return new ParametersMap()
                            .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrEntrant) entity).getId())
                            .add("selectedTab", "ratingTab");
                    }
                }))
            .addColumn(textColumn("totalMark", EnrRatingItem.totalMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE))
            .addColumn(textColumn("priority", EnrRatingItem.requestedCompetition().priority().s()))
            .addColumn(booleanColumn("parallel", EnrRatingItem.requestedCompetition().parallel().s()))
            .addColumn(textColumn("included", EnrModelEntrantDSHandler.VIEW_PROP_INCLUDED))
            .addColumn(textColumn("state", EnrRatingItem.requestedCompetition().state().stateDaemonSafe()))
            .addColumn(textColumn("originalIn", EnrModelEntrantDSHandler.VIEW_PROP_ORIGINAL_IN))
            .addColumn(textColumn("accepted", EnrModelEntrantDSHandler.VIEW_PROP_ACCEPTED))
            .addColumn(textColumn("enrollmentAvailable", EnrModelEntrantDSHandler.VIEW_PROP_ENROLLMENT_AVAILABLE))
            .addColumn(publisherColumn("enrolled", "title")
                .entityListProperty(EnrModelEntrantDSHandler.VIEW_PROP_ENROLLED)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(new IPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return ((EnrModelEntrantDSHandler.EnrolledWrapper) entity).getPublisherParameters();
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EnrEnrollmentModelCompetitionInStepPub.class.getSimpleName();
                    }
                }))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> competitionDSHandler()
    {
        return new EnrModelEntrantDSHandler(getName());
    }
}