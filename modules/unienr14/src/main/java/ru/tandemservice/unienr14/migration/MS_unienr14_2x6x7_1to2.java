package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x7_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantRequest

		//  создано обязательное свойство internationalTreatyContractor
		{
			if( tool.tableExists("enr14_request_t") && !tool.columnExists("enr14_request_t", "ntrntnltrtycntrctr_p") ) {

                tool.createColumn("enr14_request_t", new DBColumn("ntrntnltrtycntrctr_p", DBType.BOOLEAN));
                // задать значение по умолчанию
                java.lang.Boolean defaultInternationalTreatyContractor = false;
                tool.executeUpdate("update enr14_request_t set ntrntnltrtycntrctr_p=? where ntrntnltrtycntrctr_p is null", defaultInternationalTreatyContractor);

                // сделать колонку NOT NULL
                tool.setColumnNullable("enr14_request_t", "ntrntnltrtycntrctr_p", false);
            }

		}


    }
}