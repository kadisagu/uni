/* $Id: EcOrderDao.java 23770 2012-08-09 04:27:26Z vdanilov $ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetItemOrgUnitPlanGen;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@SuppressWarnings("unchecked")
public class EnrOrderDao extends UniBaseDao implements IEnrOrderDao
{

    @Override
    public EnrOrder saveOrUpdateOrder(EnrOrder order, Date createDate, boolean addForm, String executor)
    {
        if (!addForm && !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + order.getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        if (executor != null)
            order.setExecutor(executor);

        if (!addForm) {
            Calendar c1 = Calendar.getInstance();
            c1.setTime(createDate);
            c1.set(Calendar.SECOND, 0);
            c1.set(Calendar.MILLISECOND, 0);
            Calendar c2 = Calendar.getInstance();
            c2.setTime(order.getCreateDate());
            c2.set(Calendar.SECOND, 0);
            c2.set(Calendar.MILLISECOND, 0);
            if (!c1.equals(c2))
                order.setCreateDate(CoreDateUtils.getDateWithMiliseconds(createDate));
        } else {
            order.setCreateDate(createDate);
        }

        EnrOrderManager.instance().dao().checkOrderNumber(order);

        saveOrUpdate(order);

        return order;
    }

    @Override
    public void deleteOrder(Long orderId)
    {
        EnrOrder order = getNotNull(EnrOrder.class, orderId);
        if (!OrderStatesCodes.FORMING.equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + order.getState().getTitle() + "», удаление запрещено.");
        }

        // очищаем старост
        for (EnrEnrollmentParagraph paragraph : getList(EnrEnrollmentParagraph.class, EnrEnrollmentParagraph.order(), order)) {
            if (paragraph.getGroupManager() != null) {
                paragraph.setGroupManager(null);
            }
        }

        for (EnrAllocationParagraph paragraph : getList(EnrAllocationParagraph.class, EnrAllocationParagraph.order(), order)) {
            if (paragraph.getGroupManager() != null) {
                paragraph.setGroupManager(null);
            }
        }

        // удаляем приказ со всеми параграфами и выписками
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }


    @Override
    public void updateParagraphPriority(Long paragraphId, int direction)
    {
        final Session session = getSession();

        EnrAbstractParagraph paragraph = getNotNull(EnrAbstractParagraph.class, paragraphId);
        List<EnrAbstractParagraph> list = getList(EnrAbstractParagraph.class, EnrAbstractParagraph.order().s(), paragraph.getOrder(), EnrAbstractParagraph.number().s());
        int i = 0;
        while (i < list.size() && !list.get(i).equals(paragraph))
            i++;
        if (direction == -1 && i == 0)
            return;
        if (direction == 1 && i == list.size() - 1)
            return;

        EnrAbstractParagraph prev = list.get(i + direction);

        int currNumber = paragraph.getNumber();
        int prevNumber = prev.getNumber();

        prev.setNumber(Integer.MIN_VALUE);
        session.update(prev);
        session.flush();

        paragraph.setNumber(prevNumber);
        session.update(paragraph);
        session.flush();

        prev.setNumber(currNumber);
        session.update(paragraph);
        session.flush();
    }

    @Override
    public void deleteParagraph(Long paragraphId)
    {
        EnrAbstractParagraph paragraph = get(EnrAbstractParagraph.class, paragraphId);

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(paragraph.getOrder().getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + paragraph.getOrder().getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        Session session = getSession();

        // удаляем старосту из параграфа, если он есть
        if (paragraph instanceof EnrEnrollmentParagraph && ((EnrEnrollmentParagraph) paragraph).getGroupManager() != null)
        {
            ((EnrEnrollmentParagraph) paragraph).setGroupManager(null);
            session.update(paragraph);
            session.flush();
        }

        if (paragraph instanceof EnrAllocationParagraph && ((EnrAllocationParagraph) paragraph).getGroupManager() != null)
        {
            ((EnrAllocationParagraph) paragraph).setGroupManager(null);
            session.update(paragraph);
            session.flush();
        }


        // удаляем параграф со всеми выписками
        MoveDaoFacade.getMoveDao().deleteParagraph(paragraph, null);
    }

    @Override
    public void deleteExtract(EnrAbstractExtract extract)
    {
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(extract.getParagraph().getOrder().getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + extract.getParagraph().getOrder().getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        EnrAbstractParagraph paragraph = (EnrAbstractParagraph) extract.getParagraph();
        if (paragraph.getExtractCount() == 1)
            throw new ApplicationException("Параграф не может быть пустым.");

        delete(extract);

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО (сравнение реализовано в выписке)
        List<EnrAbstractExtract> extractList = getList(EnrAbstractExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        Collections.sort(extractList);

        // присваиваем уникальные номера выпискам
        int counter = -1;
        for (EnrAbstractExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            getSession().update(enrollmentExtract);
        }
        getSession().flush();

        // нумеруем выписки с единицы
        counter = 1;
        for (EnrAbstractExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            update(enrollmentExtract);
        }
    }

    @Override
    public void saveEnrollmentOrderText(EnrOrder order)
    {
        ScriptItem script = order.getPrintFormType();
            Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(script,
                    IScriptExecutor.TEMPLATE_VARIABLE, script.getCurrentTemplate(),
                    IScriptExecutor.OBJECT_VARIABLE, order.getId(),
                    IScriptExecutor.PRINT_OBJECT_VARIABLE, order,
                    IScriptExecutor.PRINT_FILE_NAME, "EnrOrder.rtf");

            // ничего не напечаталось
            if (result == null)
                throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов не создал печатную форму.");

            Object document = result.get(IScriptExecutor.DOCUMENT);
            if (!(document instanceof byte[]))
                throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов не создал печатную форму.");

            Object fileNameObject = result.get(IScriptExecutor.FILE_NAME);
            String fileName = fileNameObject == null ? "EnrOrder.rtf" : fileNameObject.toString();
            if (!fileName.endsWith(".rtf"))
                throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов создал неверное имя файла. Имя файла должно иметь расширение rtf.");

        DatabaseFile printForm = order.getPrintForm();
        if (null == printForm) printForm = new DatabaseFile();
            printForm.setContent((byte[]) document);
            printForm.setFilename(fileName);
            printForm.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        getSession().saveOrUpdate(printForm);

        order.setPrintForm(printForm);
        update(order);
    }

    @Override
    public void checkOrderNumber(EnrAbstractOrder order)
    {
        String number = order.getNumber();
        if (number == null) return;
        number = number.trim();
        Date date = order.getCreateDate();
        if (date == null)
            throw new ApplicationException("Нельзя проверить номер приказа. Не указана дата формирования.");

        MQBuilder builder = new MQBuilder(EnrAbstractOrder.ENTITY_NAME, "s");
        builder.add(UniMQExpression.eqYear("s", IAbstractOrder.P_CREATE_DATE, CoreDateUtils.getYear(date)));
        builder.add(MQExpression.eq("s", IAbstractOrder.P_NUMBER, number));
        if (order.getId() != null)
            builder.add(MQExpression.notEq("s", "id", order.getId()));

        List<EnrAbstractOrder> list = builder.getResultList(getSession());
        if (!list.isEmpty())
            throw new ApplicationException("Номер приказа должен быть уникален в рамках календарного года среди всех приказов по абитуриентам.");
    }

    @Override
    public void getDownloadPrintForm(Long enrollmentOrderId)
    {
        EnrOrder enrollmentOrder = getNotNull(EnrOrder.class, enrollmentOrderId);

        String stateCode = enrollmentOrder.getState().getCode();

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
        || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
        || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // приказ проведен => печатная форма сохранена
            if (enrollmentOrder.getPrintForm() == null)
                throw new IllegalStateException("Print form not saved for EnrOrder: id=" + enrollmentOrderId + ", stateCode = " + stateCode);

            // загружаем сохраненную печатную форму
            if(enrollmentOrder.getPrintFormType().isPrintPdf())
            {
                byte[] content = enrollmentOrder.getPrintForm().getContent();
                if (content == null)
                    throw new ApplicationException("Файл печатной формы пуст.");
                BusinessComponentUtils.downloadDocument(new ReportRenderer(enrollmentOrder.getPrintForm().getFilename().split("\\.")[0] + ".pdf", content, true), false);
            }
            else
            {
                if (enrollmentOrder.getPrintForm().getContent() == null)
                    throw new ApplicationException("Файл печатной формы пуст.");
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(enrollmentOrder.getPrintForm()), false);
            }
        } else {

            if(enrollmentOrder.getPrintFormType().isPrintPdf())
            {
                Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(enrollmentOrder.getPrintFormType(),
                        IScriptExecutor.TEMPLATE_VARIABLE, enrollmentOrder.getPrintFormType().getCurrentTemplate(),
                        IScriptExecutor.OBJECT_VARIABLE, enrollmentOrderId,
                        IScriptExecutor.PRINT_OBJECT_VARIABLE, enrollmentOrder
                );

                byte[] document = (byte[]) result.get(IScriptExecutor.DOCUMENT);
                String fileName = (String) result.get(IScriptExecutor.FILE_NAME);

                BusinessComponentUtils.downloadDocument(new ReportRenderer(fileName.split("\\.")[0] + ".pdf", document, true), false);
            }
            else
            {
                CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(enrollmentOrder.getPrintFormType(),
                        IScriptExecutor.TEMPLATE_VARIABLE, enrollmentOrder.getPrintFormType().getCurrentTemplate(),
                        IScriptExecutor.OBJECT_VARIABLE, enrollmentOrderId,
                        IScriptExecutor.PRINT_OBJECT_VARIABLE, enrollmentOrder,
                        IScriptExecutor.PRINT_FILE_NAME, "EnrOrder.rtf");
            }
        }
    }

    @Override
    public List<EnrOrderBasic> getReasonToBasicsList(EnrOrderReason enrollmentOrderReason)
    {
        MQBuilder builder = new MQBuilder(EnrOrderReasonToBasicsRel.ENTITY_CLASS, "rel", new String[]{EnrOrderReasonToBasicsRel.L_BASIC});
        builder.add(MQExpression.eq("rel", EnrOrderReasonToBasicsRel.L_REASON, enrollmentOrderReason));
        builder.addOrder("rel", EnrOrderReasonToBasicsRel.L_BASIC + "." + EnrOrderBasic.P_TITLE);
        return builder.getResultList(getSession());
    }


    @Override
    public void doSendToCoordination(List<Long> selectedIds, IPersistentPersonable initiator)
    {
        List<EnrOrder> orders = new MQBuilder(EnrOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).<EnrOrder>getResultList(getSession());
        for (EnrOrder order : orders)
        {
            VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
            if (visaTask != null)
                throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");
            if (StringUtils.isEmpty(order.getNumber()))
                throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");
            if (order.getCommitDate() == null)
                throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");
            if (order.getParagraphCount() == 0)
                throw new ApplicationException("Нельзя отправить приказ на согласование без параграфов.");
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE))
                throw new ApplicationException(orders.size() > 1 ? "Не все выбранные приказы можно отправить на согласование." : "Приказ нельзя отправить на согласование, так как он не в состоянии «Формируется».");
        }
        for (EnrOrder order : orders)
        {
            //2. надо сохранить печатную форму приказа
            EnrOrderManager.instance().dao().saveEnrollmentOrderText(order);

            //3. отправляем на согласование
            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(order, initiator);
            sendToCoordinationService.execute();
        }
    }

    @Override
    public void doCommit(List<Long> selectedIds)
    {
        synchronized (this)
        {
            if (NamedSyncInTransactionCheckLocker.hasLock("enrollmentOrderCommitSync"))
                throw new ApplicationException("Нельзя провести приказ параллельно с проведением другого приказа о зачислении. Повторите попытку проведения через несколько минут.");
            NamedSyncInTransactionCheckLocker.register(getSession(), "enrollmentOrderCommitSync");
        }

        List<EnrOrder> orders = new MQBuilder(EnrOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).<EnrOrder>getResultList(getSession());
        for (EnrOrder order : orders)
        {

            if (order.getNumber() == null)
                throw new ApplicationException("Нельзя проводить приказ без номера.");
            if (order.getCommitDate() == null)
                throw new ApplicationException("Нельзя проводить приказ без даты приказа.");
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED))
                throw new ApplicationException(orders.size() > 1 ? "Не все выбранные приказы можно провести." : "Приказ нельзя провести, так как он не в состоянии «Согласован».");
        }

        OrderStates stateOrderFinished = UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
        ExtractStates stateExtractFinished = UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED);
        for (EnrOrder order : orders)
        {
            //обновляем печатную форму
            EnrOrderManager.instance().dao().saveEnrollmentOrderText(order);
            MoveDaoFacade.getMoveDao().doCommitOrder(order, stateOrderFinished, stateExtractFinished, null);
        }
    }

    @Override
    public void doSendToFormative(Long orderId)
    {
        EnrOrder order = getNotNull(EnrOrder.class, orderId);

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(order);

        ExtractStates stateInOrder = getCatalogItem(ExtractStates.class, ExtractStatesCodes.IN_ORDER);
        for (EnrAbstractExtract extract : getList(EnrAbstractExtract.class, EnrAbstractExtract.paragraph().order().s(), order)) {
            extract.setState(stateInOrder);
            update(extract);
        }
    }

    @Override
    public void doReject(EnrOrder order)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(order, UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(order);
            touchService.execute();
        }
    }

    @Override
    public void doRollback(EnrOrder order)
    {
        MoveDaoFacade.getMoveDao().doRollbackOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED), null);
    }

    @Override
    public void doExecuteAcceptVisaOrderService(IAbstractOrder order)
    {
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED));
        update(order);

        ExtractStates stateAccepted = getCatalogItem(ExtractStates.class, ExtractStatesCodes.ACCEPTED);
        for (EnrAbstractExtract extract : getList(EnrAbstractExtract.class, EnrAbstractExtract.paragraph().order().s(), order)) {
            extract.setState(stateAccepted);
            update(extract);
        }
    }

    @Override
    public void doExecuteRejectVisaOrderService(IAbstractOrder order)
    {
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_REJECTED));
        update(order);

        ExtractStates stateInOrder = getCatalogItem(ExtractStates.class, ExtractStatesCodes.IN_ORDER);
        for (EnrAbstractExtract extract : getList(EnrAbstractExtract.class, EnrAbstractExtract.paragraph().order().s(), order)) {
            extract.setState(stateInOrder);
            update(extract);
        }
    }

    @Override
    public void doCommit(EnrAbstractExtract extract)
    {
        doCancelEnrollmentExtract(extract, true);
    }

    @Override
    public void doRollback(EnrAbstractExtract extract)
    {
        doCancelEnrollmentExtract(extract, false);
    }

    private void doCancelEnrollmentExtract(EnrAbstractExtract extract, boolean commit)
    {
        if (!(extract instanceof EnrCancelExtract)) return;
        EnrCancelExtract cancelExtract = (EnrCancelExtract) extract;
        List<EnrCancelExtract> chain = new ArrayList<>();
        EnrAbstractExtract targetExtract = cancelExtract;
        boolean cancel = !commit;
        while (targetExtract instanceof EnrCancelExtract) {
            cancel = !cancel;
            chain.add((EnrCancelExtract) targetExtract);
            targetExtract = ((EnrCancelExtract) targetExtract).getEntity();
            targetExtract.setCancelled(cancel);
            update(targetExtract);
        }
        if (!commit) {
            chain.remove(0);
        }
        if (targetExtract instanceof EnrEnrollmentExtract) {
            EnrEnrollmentExtract enrollmentExtract = (EnrEnrollmentExtract) targetExtract;
            enrollmentExtract.setCancelled(cancel);
            boolean excludeLowerPriorityCompetitions = !cancel || ((EnrCancelParagraph) chain.get(chain.size() - 1).getParagraph()).isCancelExcludeLowerPriorityCompetitions();
            enrollmentExtract.setExcludeLowerPriorityCompetitions(excludeLowerPriorityCompetitions);
            update(targetExtract);
        }
        if (targetExtract instanceof EnrAllocationExtract) {
            EnrAllocationExtract allocationExtract = (EnrAllocationExtract) targetExtract;
            allocationExtract.setCancelled(cancel);
            update(targetExtract);
        }
    }

    @Override
    public String getDefaultOrderBasicText(String enrOrderTypeCode)
    {
        return null;
    }

    @Override
    public void doCreateStudents(List<Long> selectedIds)
    {
        synchronized (this)
        {
            if (NamedSyncInTransactionCheckLocker.hasLock("enrollmentOrderStudentCreateLock"))
                throw new ApplicationException("В данный момент уже выполняется создание студентов. Повторите попытку через несколько минут.");
            NamedSyncInTransactionCheckLocker.register(getSession(), "enrollmentOrderStudentCreateLock");
        }

        List<EnrOrder> orders = new MQBuilder(EnrOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).<EnrOrder>getResultList(getSession());
        int studentCount = 0;
        for (EnrOrder order : orders)
        {
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)) continue;
            if (order.getType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT))
            {
                studentCount = studentCount + doCreateStudentsByEnrollOrder(order);
            }
            else if (order.getType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT_MIN))
            {
                studentCount = studentCount + doCreateStudentsByEnrollMinOrder(order);
            }
            else
                continue;

        }

        UserContext userContext = UserContext.getInstance();
        if (null != userContext) {
            userContext.getInfoCollector().add("Создано студентов: " + studentCount + ".");
        }
    }

    private int doCreateStudentsByEnrollOrder(EnrOrder order)
    {
        StudentStatus studentStatus = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);
        Course course = DevelopGridDAO.getCourseMap().get(1);
        StudentCategory category = getCatalogItem(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);

        List<EnrEnrollmentExtract> extracts = getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().order(), order);

        Map<EnrEnrollmentExtract, EnrAllocationExtract> allocationExtractMap = new HashMap<>();
        List<Object[]> rows = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
            .where(eq(property("e", EnrEnrollmentExtract.paragraph().order()), value(order)))
            .fromEntity(EnrAllocationExtract.class, "a").column("a")
            .where(eq(property("a", EnrAllocationExtract.entity()), property("e", EnrEnrollmentExtract.entity())))
            .createStatement(getSession()).list();
        for (Object[] row : rows) {
            allocationExtractMap.put((EnrEnrollmentExtract) row[0], (EnrAllocationExtract) row[1]);
        }

        Map<EnrEnrollmentExtract, Set<EnrEntrantCustomState>> customStateMap = SafeMap.get(HashSet.class);
        rows = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
            .where(eq(property("e", EnrEnrollmentExtract.paragraph().order()), value(order)))
            .fromEntity(EnrEntrantCustomState.class, "c").column("c")
            .where(eq(property("c", EnrEntrantCustomState.entrant()), property("e", EnrEnrollmentExtract.entity().request().entrant())))
            .createStatement(getSession()).list();
        for (Object[] row : rows) {
            customStateMap.get((EnrEnrollmentExtract) row[0]).add((EnrEntrantCustomState) row[1]);
        }

        int studentCount = 0;
        for (EnrEnrollmentExtract extract : extracts) {
            if (extract.getStudent() != null) continue;
            if (extract.isCancelled()) continue;
            EnrRequestedCompetition requestedCompetition = extract.getRequestedCompetition();
            EnrCompetition competition = requestedCompetition.getCompetition();

            EnrAllocationExtract allocationExtract = allocationExtractMap.get(extract);
            if (allocationExtract != null && allocationExtract.isCancelled()) allocationExtract = null;
            if (allocationExtract != null && allocationExtract.getStudent() != null) {
                throw new ApplicationException("В выписке о распределении по ОП уже указан студент: " + allocationExtract.getTitle());
            }
            if (allocationExtract != null && !ExtractStatesCodes.FINISHED.equals(allocationExtract.getState().getCode())) {
                throw new ApplicationException("Выписка о распределении по ОП не проведена: " + allocationExtract.getTitle());
            }

            EnrProgramSetOrgUnit psOu = competition.getProgramSetOrgUnit();
            EducationOrgUnit orgUnit = psOu.getEducationOrgUnit();
            if (allocationExtract == null && orgUnit == null) {
                throw new ApplicationException("Не задана настройка НПП для создания студентов: " + psOu.getTitle());
            }

            Date splitToSpecializationOrderDate = null;
            String splitToSpecializationOrderNumber = null;

            if (allocationExtract != null) {
                EnrAllocationParagraph paragraph = (EnrAllocationParagraph) allocationExtract.getParagraph();
                EnrProgramSetItemOrgUnitPlan plan = getByNaturalId(new EnrProgramSetItemOrgUnitPlanGen.NaturalId(paragraph.getEnrProgramSetItem(), psOu));
                if (plan == null) {
                    throw new ApplicationException("Не задан план приема по ОП: " + paragraph.getEnrProgramSetItem().getProgram().getTitle() + ", " + psOu.getTitle());
                }
                if (plan.getEducationOrgUnit() == null) {
                    throw new ApplicationException("Не задана настройка НПП для создания студентов: " + paragraph.getEnrProgramSetItem().getProgram().getTitle() + ", " + psOu.getTitle());
                }
                orgUnit = plan.getEducationOrgUnit();
                splitToSpecializationOrderDate = paragraph.getOrder().getCommitDate();
                splitToSpecializationOrderNumber = paragraph.getOrder().getNumber();
            }

            EnrEntrantRequest request = requestedCompetition.getRequest();

            Student student = new Student();
            student.setPerson(request.getEntrant().getPerson());
            student.setEducationOrgUnit(orgUnit);
            student.setDevelopPeriodAuto(orgUnit.getDevelopPeriod());
            student.setEntranceYear(order.getEnrollmentCampaign().getEducationYear().getIntValue());
            student.setCompensationType(competition.getType().getCompensationType());
            student.setStatus(studentStatus);
            student.setCourse(course);
            student.setEduDocument(request.getEduDocument());
            student.setEduDocumentOriginalHandedIn(requestedCompetition.isOriginalDocumentHandedIn());
            if (competition.isTargetAdmission()) {
                EnrRequestedCompetitionTA taComp = (EnrRequestedCompetitionTA) requestedCompetition;
                student.setTargetAdmission(true);
                student.setTargetAdmissionOrgUnit(taComp.getTargetAdmissionOrgUnit());
            }
            student.setStudentCategory(category);
            save(student);
            studentCount++;

            Set<EnrEntrantCustomState> entrantCustomStates = customStateMap.get(extract);
            for (EnrEntrantCustomState state : entrantCustomStates) {
                if (state.getCustomState().getStudentCustomState() == null) continue;
                StudentCustomState studentState = new StudentCustomState();
                studentState.setStudent(student);
                studentState.setCustomState(state.getCustomState().getStudentCustomState());
                studentState.setBeginDate(state.getBeginDate());
                studentState.setEndDate(state.getEndDate());
                studentState.setDescription(state.getDescription());
                save(studentState);
            }

            OrderData orderData = new OrderData();
            orderData.setStudent(student);
            orderData.setEduEnrollmentOrderNumber(order.getNumber());
            orderData.setEduEnrollmentOrderDate(order.getCommitDate());
            orderData.setEduEnrollmentOrderEnrDate(order.getActionDate());
            orderData.setSplitToSpecializationOrderNumber(splitToSpecializationOrderNumber);
            orderData.setSplitToSpecializationOrderDate(splitToSpecializationOrderDate);
            this.save(orderData);

            extract.setStudent(student);
            update(extract);

            if (allocationExtract != null) {
                allocationExtract.setStudent(student);
                update(extract);
            }
        }

        return studentCount;
    }

    protected int doCreateStudentsByEnrollMinOrder(EnrOrder order)
    {
        StudentStatus studentStatus = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);
        Course course = DevelopGridDAO.getCourseMap().get(1);
        StudentCategory category = getCatalogItem(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);

        List<EnrEnrollmentMinisteryExtract> extracts = getList(EnrEnrollmentMinisteryExtract.class, EnrEnrollmentMinisteryExtract.paragraph().order(), order);

        Map<EnrEnrollmentMinisteryExtract, Set<EnrEntrantCustomState>> customStateMap = SafeMap.get(HashSet.class);
        List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentMinisteryExtract.class, "e").column("e")
                .where(eq(property("e", EnrEnrollmentMinisteryExtract.paragraph().order()), value(order)))
                .fromEntity(EnrEntrantCustomState.class, "c").column("c")
                .where(eq(property("c", EnrEntrantCustomState.entrant()), property("e", EnrEnrollmentMinisteryExtract.entrantRequest().entrant())))
                .createStatement(getSession()).list();
        for (Object[] row : rows) {
            customStateMap.get((EnrEnrollmentMinisteryExtract) row[0]).add((EnrEntrantCustomState) row[1]);
        }

        int studentCount = 0;
        for (EnrEnrollmentMinisteryExtract extract : extracts) {
            if (extract.getStudent() != null) continue;
            if (extract.isCancelled()) continue;

            EnrEntrantForeignRequest request = extract.getEntrantRequest();
            EnrCompetition competition = extract.getCompetition();
            EnrProgramSetOrgUnit psOu = competition.getProgramSetOrgUnit();
            EducationOrgUnit orgUnit = psOu.getEducationOrgUnit();

            if(orgUnit == null)
                throw new ApplicationException("Не задана настройка НПП для создания студентов: " + psOu.getTitle());

            Student student = new Student();
            student.setPerson(request.getEntrant().getPerson());
            student.setEducationOrgUnit(orgUnit);
            student.setDevelopPeriodAuto(orgUnit.getDevelopPeriod());
            student.setEntranceYear(order.getEnrollmentCampaign().getEducationYear().getIntValue());
            student.setCompensationType(getByCode(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
            student.setStatus(studentStatus);
            student.setCourse(course);
            student.setEduDocument(request.getEduDocument());

            EnrEntrantOriginalDocumentStatus original = getByNaturalId(new EnrEntrantOriginalDocumentStatusGen.NaturalId(request.getEntrant(), request.getEduDocument()));
            student.setEduDocumentOriginalHandedIn(original != null);
            student.setTargetAdmission(false);

            student.setStudentCategory(category);
            save(student);
            studentCount++;

            Set<EnrEntrantCustomState> entrantCustomStates = customStateMap.get(extract);
            for (EnrEntrantCustomState state : entrantCustomStates) {
                if (state.getCustomState().getStudentCustomState() == null) continue;
                StudentCustomState studentState = new StudentCustomState();
                studentState.setStudent(student);
                studentState.setCustomState(state.getCustomState().getStudentCustomState());
                studentState.setBeginDate(state.getBeginDate());
                studentState.setEndDate(state.getEndDate());
                studentState.setDescription(state.getDescription());
                save(studentState);
            }

            OrderData orderData = new OrderData();
            orderData.setStudent(student);
            orderData.setEduEnrollmentOrderNumber(order.getNumber());
            orderData.setEduEnrollmentOrderDate(order.getCommitDate());
            orderData.setEduEnrollmentOrderEnrDate(order.getActionDate());
            this.save(orderData);

            extract.setStudent(student);
            update(extract);
        }

        return studentCount;
    }

}
