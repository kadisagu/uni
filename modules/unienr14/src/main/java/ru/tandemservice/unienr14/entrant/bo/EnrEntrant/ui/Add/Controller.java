/* $Id: Controller.java 32113 2014-01-27 09:14:01Z hudson $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.PersonIdentityCardEditInline;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class Controller extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Controller<IDAO, Model, EnrEntrant>
{
    public static final String IDENTITY_CARD_REGION = "identityCardRegion";

    @Override
    protected void startNewInstancePublisher(IBusinessComponent component, EnrEntrant newInstance)
    {
        activateInRoot(component, new PublisherActivator(newInstance, new ParametersMap()
                .add("selectedTab", null)
                .add("selectedDataTab", null)
        ));
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        component.createChildRegion(IDENTITY_CARD_REGION, new ComponentActivator(PersonIdentityCardEditInline.class.getSimpleName(),
                new ParametersMap()
                        .add(PersonIdentityCardEditInline.PERSON, getModel(component).getPerson())));
    }
}
