package ru.tandemservice.unienr14.migration;

import java.util.Arrays;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * изменение приоритетов в enrEntrantState
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_18to19 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        /**
        <item id="1" constant-name="active">
            <property name="priority" value="6"/>
        </item>
        <item id="2" constant-name="out of competition">
            <property name="priority" value="7"/>
        </item>
        <item id="3" constant-name="enrolled">
            <property name="priority" value="1"/>
        </item>
        <item id="4" constant-name="exams passed">
            <property name="priority" value="5"/>
        </item>
        <item id="5" constant-name="recommended">
            <property name="priority" value="4"/>
        </item>
        <item id="6" constant-name="in order">
            <property name="priority" value="2"/>
        </item>
        <item id="7" constant-name="take documents away">
            <property name="priority" value="8"/>
        </item>
        <item id="8" constant-name="excluded by reject">
            <property name="priority" value="10"/>
        </item>
        <item id="9" constant-name="excluded by extract">
            <property name="priority" value="9"/>
        </item>
        <item id="10" constant-name="select should be enrolled">
            <property name="priority" value="3"/>
        </item>
         */

        for (int i: Arrays.asList(1, 0)) {
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+6,  "1");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+7,  "2");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+1,  "3");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+5,  "4");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+4,  "5");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+2,  "6");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+8,  "7");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+10, "8");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+9,  "9");
            tool.executeUpdate("update enr14_c_entrant_state_t set priority_p=? where code_p=?", (i*100)+3,  "10");
        }


    }
}