package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_unienr14_2x8x1_22to23 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrDiscipline
        SQLSelectQuery selectQuery = new SQLSelectQuery()
                .from(SQLFrom.table("enr14_c_discipline_t", "d"))
                .column("d.id")
                .column("d.code_p")
                .where("code_p<>'6'")
                .where("title_p='История'");

        List<Object[]> items = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectQuery));

        if(items != null && !items.isEmpty())
        {
            if(items.size() > 1) throw new RuntimeException("Элементов справочника «Дисциплины вступительных испытаний» с названием «История» больше 1-го.");

            Long id = (Long) items.get(0)[0];
            String code = (String) items.get(0)[1];

            tool.dropConstraint("enr14_c_discipline_t", "uk_naturalid_enrdiscipline");

            tool.executeUpdate("update enr14_c_discipline_t set code_p=? where code_p=?", code, "6");
            tool.executeUpdate("update enr14_c_discipline_t set code_p=? where id=?", "6", id);
        }
    }
}