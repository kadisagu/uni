package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка приказа о зачислении по направлению Минобрнауки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentMinisteryExtractGen extends EnrAbstractExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract";
    public static final String ENTITY_NAME = "enrEnrollmentMinisteryExtract";
    public static final int VERSION_HASH = -261506353;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String L_STUDENT = "student";

    private EnrEntrantForeignRequest _entrantRequest;     // Направление на поступление иностранного абитуриента
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление на поступление иностранного абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEntrantForeignRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Направление на поступление иностранного абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrantRequest(EnrEntrantForeignRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Студент.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEnrollmentMinisteryExtractGen)
        {
            setEntrantRequest(((EnrEnrollmentMinisteryExtract)another).getEntrantRequest());
            setStudent(((EnrEnrollmentMinisteryExtract)another).getStudent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentMinisteryExtractGen> extends EnrAbstractExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentMinisteryExtract.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentMinisteryExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entrantRequest":
                    obj.setEntrantRequest((EnrEntrantForeignRequest) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequest":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequest":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequest":
                    return EnrEntrantForeignRequest.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentMinisteryExtract> _dslPath = new Path<EnrEnrollmentMinisteryExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentMinisteryExtract");
    }
            

    /**
     * @return Направление на поступление иностранного абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract#getEntrantRequest()
     */
    public static EnrEntrantForeignRequest.Path<EnrEntrantForeignRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Студент.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends EnrEnrollmentMinisteryExtract> extends EnrAbstractExtract.Path<E>
    {
        private EnrEntrantForeignRequest.Path<EnrEntrantForeignRequest> _entrantRequest;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление на поступление иностранного абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract#getEntrantRequest()
     */
        public EnrEntrantForeignRequest.Path<EnrEntrantForeignRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EnrEntrantForeignRequest.Path<EnrEntrantForeignRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Студент.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentMinisteryExtract.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentMinisteryExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
