/**
 *$Id: EnrEnrollmentCampaignAddEdit.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCampaignType;
import ru.tandemservice.unienr14.catalog.entity.EnrLevelBudgetFinancing;
import ru.tandemservice.unienr14.catalog.entity.EnrTieBreakRatingRule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
@Configuration
public class EnrEnrollmentCampaignAddEdit extends BusinessComponentManager
{
    public static final String EDUCATION_YEAR_SELECT_DS = EducationCatalogsManager.DS_EDU_YEAR;
    public static final String ENR_ORG_UNIT_SELECT_DS = "enrOrgUnitDS";
    public static final String TARGET_ADMISSION_CONTRACT_SELECT_DS = "targetAdmissionContractDS";
    public static final String DIRECTION_COMBINED_IN_CG_SELECT_DS = "directionCompetitionGroupDS";
    public static final String STATE_EXAM_RESTRICTION_DS = "stateExamRestrictionDS";
    public static final String NUMBER_ON_REQUEST_SELECT_DS = "numberOnRequestDS";
    public static final String EXAM_GROUP_TITLE_FORMING_SELECT_DS = "examGroupTitleFormingDS";
    public static final String STATE_EXAM_ACCEPTED_SELECT_DS = "stateExamAcceptedDS";
    public static final String ORIGIN_DOC_FOR_PRELIMINARY_SELECT_DS = "originDocForPreliminaryDS";
    public static final String ORIGIN_DOC_FOR_ORDER_SELECT_DS = "originDocForOrderDS";
    public static final String ENROLL_PARAGRAPH_PER_EDU_ORG_UNIT_SELECT_DS = "enrollParagraphPerEduOrgUnitDS";
    public static final String TARGET_ADMISSION_COMPETITION_DS = "targetAdmissionCompetitionDS";
    public static final String EXAM_LIST_PRINT_TYPE_DS = "examListPrintTypeDS";
    public static final String LEVEL_BUDGET_FINANCING_DS = "levelBudgetFinancingDS";
    public static final String ACCEPT_PEOPLE_RESIDING_IN_CRIMEA_DS = "acceptPeopleResidingInCrimeaDS";
    public static final String ENROLLMENT_CAMPAIGN_TYPE_DS = "enrollmentCampaignTypeDS";

    public static final Long STATE_EXAM_RESTRICTION_NONE = 1L;
    public static final Long STATE_EXAM_RESTRICTION_FALSE = 2L;
    public static final Long STATE_EXAM_RESTRICTION_TRUE = 3L;

//
    public static final Long EXAM_LIST_PRINT_TYPE_INTERNAL_EXAMS = 1L;
    public static final Long EXAM_LIST_PRINT_TYPE_ALL_EXAMS = 2L;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(eduYearDSConfig())
            .addDataSource(selectDS(ENR_ORG_UNIT_SELECT_DS, enrOrgUnitDSHandler()).addColumn(EduInstitutionOrgUnit.orgUnit().fullTitle().s()))
            .addDataSource(selectDS(TARGET_ADMISSION_CONTRACT_SELECT_DS, targetAdmissionContractDSHandler()))
            .addDataSource(selectDS(DIRECTION_COMBINED_IN_CG_SELECT_DS, directionCombinedInCGDSHandler()))
            .addDataSource(selectDS(STATE_EXAM_RESTRICTION_DS, stateExamRestrictionDSSHandler()))
            .addDataSource(selectDS(NUMBER_ON_REQUEST_SELECT_DS, numberOnRequestDSHandler()))
            .addDataSource(selectDS(EXAM_GROUP_TITLE_FORMING_SELECT_DS, examGroupTitleFormingDSHandler()))
            .addDataSource(selectDS(STATE_EXAM_ACCEPTED_SELECT_DS, stateExamAcceptedDSHandler()))
            .addDataSource(selectDS(ORIGIN_DOC_FOR_PRELIMINARY_SELECT_DS, originDocForPreliminaryDSHandler()))
            .addDataSource(selectDS(ORIGIN_DOC_FOR_ORDER_SELECT_DS, originDocForOrderDSHandler()))
            .addDataSource(selectDS(ENROLL_PARAGRAPH_PER_EDU_ORG_UNIT_SELECT_DS, enrollParagraphPerEduOrgUnitDSHandler()))
            .addDataSource(selectDS(TARGET_ADMISSION_COMPETITION_DS, targetAdmissionCompetitionDSHandler()))
            .addDataSource(selectDS(EXAM_LIST_PRINT_TYPE_DS, examListPrintTypeDSHandler()))
            .addDataSource(selectDS(LEVEL_BUDGET_FINANCING_DS, levelBudgetFinancingDS()))
            .addDataSource(selectDS(ACCEPT_PEOPLE_RESIDING_IN_CRIMEA_DS, acceptPeopleResidingInCrimeaDS()))
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ENROLLMENT_CAMPAIGN_TYPE_DS, getName(), EnrEnrollmentCampaignType.defaultSelectDSHandler(getName())))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> enrollParagraphPerEduOrgUnitDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .yesTitle("enrollParagraphPerEduOrgUnit.yes")
            .noTitle("enrollParagraphPerEduOrgUnit.not");
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> originDocForOrderDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .yesTitle("originDocForOrder.need")
            .noTitle("originDocForOrder.notNeed");
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> originDocForPreliminaryDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .yesTitle("originDocForPreliminary.need")
            .noTitle("originDocForPreliminary.notNeed");
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> stateExamAcceptedDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .yesTitle("stateExamCertificate.accepted")
            .noTitle("stateExamCertificate.notAccepted");
    }

    @Bean
    protected IDefaultComboDataSourceHandler examGroupTitleFormingDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .noTitle("examGroupTitleForming.auto")
            .yesTitle("examGroupTitleForming.manual")
            .noFirst();
    }

    @Bean
    public IDefaultComboDataSourceHandler numberOnRequestDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .noTitle("numberOnRequest.auto")
            .yesTitle("numberOnRequest.manual")
            .noFirst();
    }

    @Bean
    public IDefaultComboDataSourceHandler directionCombinedInCGDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .yesTitle("directionCompetitionGroup.combined")
            .noTitle("directionCompetitionGroup.notCombined");
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionContractDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .yesTitle("targetAdmissionContract.use")
            .noTitle("targetAdmissionContract.notUse");
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamRestrictionDSSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
            .addRecord(STATE_EXAM_RESTRICTION_NONE, "stateExamRestriction.none")
            .addRecord(STATE_EXAM_RESTRICTION_FALSE, "stateExamRestriction.false")
            .addRecord(STATE_EXAM_RESTRICTION_TRUE, "stateExamRestriction.true");
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionCompetitionDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
            .noTitle("targetAdmissionCompetition.common")
            .yesTitle("targetAdmissionCompetition.individual")
            .noFirst();
    }



    @Bean
    public IDefaultComboDataSourceHandler examListPrintTypeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
            .addRecord(EXAM_LIST_PRINT_TYPE_ALL_EXAMS, "examList.printType.allExams")
            .addRecord(EXAM_LIST_PRINT_TYPE_INTERNAL_EXAMS, "examList.printType.internalExams");
    }

    @Bean
    public IDefaultComboDataSourceHandler enrOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduInstitutionOrgUnit.class)
            .filter(EduInstitutionOrgUnit.orgUnit().fullTitle())
            .order(EduInstitutionOrgUnit.orgUnit().fullTitle())
            .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> levelBudgetFinancingDS()
    {
        return new EntityComboDataSourceHandler(getName(), EnrLevelBudgetFinancing.class)
            .order(EnrLevelBudgetFinancing.priority());
    }

    @Bean
    public IDefaultComboDataSourceHandler acceptPeopleResidingInCrimeaDS()
    {
        return new TwinComboDataSourceHandler(getName())
                .noFirst();
    }

    @Bean
    public UIDataSourceConfig eduYearDSConfig()
    {
        return SelectDSConfig.with(EducationCatalogsManager.DS_EDU_YEAR, getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(eduYearDSHandler())
        .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource())
        .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(ge(property(alias, EducationYear.intValue()), value(2015))));

    }
}
