/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList.EnrProgramSetEnrollmentCommissionListUI.*;

/**
 * @author Alexey Lopatin
 * @since 02.06.2015
 */
public class EnrProgramSetEnrollmentCommissionDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EnrProgramSetEnrollmentCommissionDSHandler(String ownerId)
    {
        super(ownerId, EnrProgramSetOrgUnit.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, "epr").column("epr")
                .joinPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().fromAlias("epr"), "s")
                .joinPath(DQLJoinType.inner, EnrProgramSetOrgUnit.orgUnit().fromAlias("epr"), "ou");

        EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        EnrRequestType requestType = context.get(PARAM_REQUEST_TYPE);
        Boolean checkPSEnrollmentCommission = context.get(PARAM_CHECK_PS_ENROLLMENT_COMMISSION);
        List<EduProgramForm> programForms = context.get(PARAM_PROGRAM_FORMS);
        List<EnrOrgUnit> orgUnits = context.get(PARAM_ORG_UNITS);
        List<OrgUnit> formativeOrgUnits = context.get(PARAM_FORMATIVE_ORG_UNITS);
        List<EduProgramSubject> programSubjects = context.get(PARAM_PROGRAM_SUBJECTS);
        List<EnrEnrollmentCommission> enrollmentCommissions = context.get(PARAM_ENROLLMENT_COMMISSIONS);
        String subjectCode = context.get(PARAM_SUBJECT_CODE);
        String programSetTitle = context.get(PARAM_PROGRAM_SET_TITLE);

        if (null == campaign || null == requestType)
        {
            builder.where(nothing());
            return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
        }

        builder.joinEntity("s", DQLJoinType.inner, requestType.getProgramSetClass(), "ps", eq(property("ps", EnrProgramSetBase.id()), property("s", EnrProgramSetBase.id())));
        builder.where(exists(
                EnrRequestType2eduProgramKindRel.class,
                EnrRequestType2eduProgramKindRel.requestType().s(), requestType,
                EnrRequestType2eduProgramKindRel.programKind().s(), property("s", EnrProgramSetBase.programSubject().subjectIndex().programKind())
        ));

        if (null != checkPSEnrollmentCommission && checkPSEnrollmentCommission)
        {
            Collection<Long> ids = UniBaseDao.ids(EnrEnrollmentCommissionManager.instance().dao().getProgramSetOrgUnit2EnrollmentCommissionMap(campaign, requestType).keySet());
            builder.where(notIn(property("epr", EnrProgramSetOrgUnit.id()), ids));
        }
        else
        {
            if (null != enrollmentCommissions && !enrollmentCommissions.isEmpty())
            {
                List<Long> orgUnitIds = Lists.newArrayList();
                Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> orgUnit2SetECMap = EnrEnrollmentCommissionManager.instance().dao().getProgramSetOrgUnit2EnrollmentCommissionMap(campaign, requestType);

                for (Map.Entry<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> entry : orgUnit2SetECMap.entrySet())
                {
                    EnrProgramSetEnrollmentCommission prSetEC = entry.getValue();
                    if (enrollmentCommissions.contains(prSetEC.getEnrollmentCommission()))
                    {
                        orgUnitIds.add(entry.getKey().getId());
                    }
                }
                builder.where(orgUnitIds.isEmpty() ? nothing() : in(property("epr", EnrProgramSetOrgUnit.id()), orgUnitIds));
            }
        }
        if (null != programForms && !programForms.isEmpty())
        {
            builder.where(in(property("s", EnrProgramSetBase.programForm()), programForms));
        }

        EnrProgramSetSearchDSHandler.applyFilters(builder, "s", campaign, requestType, null, orgUnits, formativeOrgUnits, programSubjects, null);
        FilterUtils.applySimpleLikeFilter(builder, "s", EnrProgramSetBase.programSubject().code(), subjectCode);
        FilterUtils.applySimpleLikeFilter(builder, "s", EnrProgramSetBase.title(), programSetTitle);

        List<EnrProgramSetOrgUnit> resultList = builder.createStatement(context.getSession()).list();
        Collections.sort(resultList, COMPARATOR);

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }

    private static final Comparator<EnrProgramSetOrgUnit> COMPARATOR = (o1, o2) -> {
        int result = EnrOrgUnit.ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR.compare(o1.getOrgUnit(), o2.getOrgUnit());

        if (result == 0)
        {
            OrgUnit ou1 = o1.getFormativeOrgUnit();
            OrgUnit ou2 = o2.getFormativeOrgUnit();
            int i = Boolean.compare(ou1.isTop(), ou2.isTop());
            result = 0 != i ? -i : ou1.getTitle().compareTo(ou2.getTitle());
        }

        EnrProgramSetBase ps1 = o1.getProgramSet();
        EnrProgramSetBase ps2 = o2.getProgramSet();
        if (result == 0)
            result = ps1.getProgramForm().getTitle().compareTo(ps2.getProgramForm().getTitle());
        if (result == 0)
            result = ps1.getProgramSubject().getSubjectCode().compareTo(ps2.getProgramSubject().getSubjectCode());
        if (result == 0)
            result = ps1.getProgramSubject().getTitle().compareTo(ps2.getProgramSubject().getTitle());
        if (result == 0)
            result = ps1.getTitle().compareTo(ps2.getTitle());

        return result;
    };
}
