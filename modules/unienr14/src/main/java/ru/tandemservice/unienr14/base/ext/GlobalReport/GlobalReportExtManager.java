/* $Id:$ */
package ru.tandemservice.unienr14.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unienr14.base.ext.GlobalReport.ui.List.EnrGlobalReportListAddon;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.View.EnrReportPersonView;

/**
 * @author oleyba
 * @since 12/11/13
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("enr14EntrantReportPerson", new GlobalReportDefinition("unienr14", "enr14EntrantReportPerson", EnrReportPersonView.class.getSimpleName()))
                .create();
    }
}