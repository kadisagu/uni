/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic.EnrOlympiadDiplomaDao;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic.IEnrOlympiadDiplomaDao;

/**
 * @author oleyba
 * @since 5/3/13
 */
@Configuration
public class EnrOlympiadDiplomaManager extends BusinessObjectManager
{
    public static EnrOlympiadDiplomaManager instance()
    {
        return instance(EnrOlympiadDiplomaManager.class);
    }

    @Bean
    public IEnrOlympiadDiplomaDao dao()
    {
        return new EnrOlympiadDiplomaDao();
    }
}
