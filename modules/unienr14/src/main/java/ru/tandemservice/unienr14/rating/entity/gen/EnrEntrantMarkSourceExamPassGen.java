package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceExamPass;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание балла (балл по ДДС)
 *
 * Создается и обновляется демоном на основе ДДС абитуриента IEnrRatingDaemonDao#doRefreshSource4ExamPass (создание оснований по данным ДДС абитуриента).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantMarkSourceExamPassGen extends EnrEntrantMarkSource
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceExamPass";
    public static final String ENTITY_NAME = "enrEntrantMarkSourceExamPass";
    public static final int VERSION_HASH = 1628996231;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_PASS_DISCIPLINE = "examPassDiscipline";

    private EnrExamPassDiscipline _examPassDiscipline;     // Дисциплина для сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина для сдачи.
     */
    public EnrExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    /**
     * @param examPassDiscipline Дисциплина для сдачи.
     */
    public void setExamPassDiscipline(EnrExamPassDiscipline examPassDiscipline)
    {
        dirty(_examPassDiscipline, examPassDiscipline);
        _examPassDiscipline = examPassDiscipline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEntrantMarkSourceExamPassGen)
        {
            setExamPassDiscipline(((EnrEntrantMarkSourceExamPass)another).getExamPassDiscipline());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantMarkSourceExamPassGen> extends EnrEntrantMarkSource.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantMarkSourceExamPass.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantMarkSourceExamPass();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "examPassDiscipline":
                    return obj.getExamPassDiscipline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "examPassDiscipline":
                    obj.setExamPassDiscipline((EnrExamPassDiscipline) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examPassDiscipline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examPassDiscipline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "examPassDiscipline":
                    return EnrExamPassDiscipline.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantMarkSourceExamPass> _dslPath = new Path<EnrEntrantMarkSourceExamPass>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantMarkSourceExamPass");
    }
            

    /**
     * @return Дисциплина для сдачи.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceExamPass#getExamPassDiscipline()
     */
    public static EnrExamPassDiscipline.Path<EnrExamPassDiscipline> examPassDiscipline()
    {
        return _dslPath.examPassDiscipline();
    }

    public static class Path<E extends EnrEntrantMarkSourceExamPass> extends EnrEntrantMarkSource.Path<E>
    {
        private EnrExamPassDiscipline.Path<EnrExamPassDiscipline> _examPassDiscipline;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина для сдачи.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceExamPass#getExamPassDiscipline()
     */
        public EnrExamPassDiscipline.Path<EnrExamPassDiscipline> examPassDiscipline()
        {
            if(_examPassDiscipline == null )
                _examPassDiscipline = new EnrExamPassDiscipline.Path<EnrExamPassDiscipline>(L_EXAM_PASS_DISCIPLINE, this);
            return _examPassDiscipline;
        }

        public Class getEntityClass()
        {
            return EnrEntrantMarkSourceExamPass.class;
        }

        public String getEntityName()
        {
            return "enrEntrantMarkSourceExamPass";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
