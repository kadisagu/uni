/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd;/**
 * @author rsizonenko
 * @since 09.06.2014
 */

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

public class EnrReportEntrantRegistrationJournalAddUI extends UIPresenter {

    private EnrEnrollmentCampaign enrollmentCampaign;
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private boolean filterByEnrCommission = false;
    private boolean disabledEnrollmentCommissionCheckbox = false;
    private boolean groupByEnrCommission;

    private List<EnrEnrollmentCommission> _enrollmentCommissionList;

    public boolean isPrintEntrantNumberColumn()
    {
        // В РНИМУ нужет ровно тот же отчет, только плюс колонка с номерами абитуриента
        return false;
    }

    // From presenter
    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDisabledEnrollmentCommissionCheckbox(!EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));

        if(isDisabledEnrollmentCommissionCheckbox())
            setFilterByEnrCommission(true);

        configUtil(getCompetitionFilterAddon());
    }

    // validate
    private void validate()
    {
        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    // Listeners
    public void onClickApply() {
        validate();
        Long reportId = EnrReportManager.instance().entrantRegistrationJournalDao().createReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        dateSelector.refreshDates(getEnrollmentCampaign());
    }

    // Utils
    private void configUtil(EnrCompetitionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }


    // getters-setters
    public List<EnrEnrollmentCommission> getEnrollmentCommissionList()
    {
        return _enrollmentCommissionList;
    }

    public void setEnrollmentCommissionList(List<EnrEnrollmentCommission> enrollmentCommissionList)
    {
        _enrollmentCommissionList = enrollmentCommissionList;
    }

    public boolean isGroupByEnrCommission() {
        return groupByEnrCommission;
    }

    public void setGroupByEnrCommission(boolean groupByEnrCommission) {
        this.groupByEnrCommission = groupByEnrCommission;
    }

    public boolean isFilterByEnrCommission() {
        return filterByEnrCommission;
    }

    public void setFilterByEnrCommission(boolean filterByEnrCommission) {
        this.filterByEnrCommission = filterByEnrCommission;
    }

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isDisabledEnrollmentCommissionCheckbox()
    {
        return disabledEnrollmentCommissionCheckbox;
    }

    public void setDisabledEnrollmentCommissionCheckbox(boolean disabledEnrollmentCommissionCheckbox)
    {
        this.disabledEnrollmentCommissionCheckbox = disabledEnrollmentCommissionCheckbox;
    }
}