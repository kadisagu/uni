package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы приемных кампаний"
 * Имя сущности : enrEnrollmentCampaignType
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEnrollmentCampaignTypeCodes
{
    /** Константа кода (code) элемента : Прием на обучение по программам бакалавриата/специалитета (title) */
    String BS = "1";
    /** Константа кода (code) элемента : Прием на обучение по программам магистратуры (title) */
    String MASTER = "2";
    /** Константа кода (code) элемента : Прием на обучение по программам СПО (title) */
    String SPO = "3";
    /** Константа кода (code) элемента : Прием на обучение по программам подготовки кадров высшей квалификации (title) */
    String HIGHER = "4";
    /** Константа кода (code) элемента : Прием иностранных абитуриентов по направлениям Минобрнауки РФ (title) */
    String ENTRANT_FOREIGN = "5";

    Set<String> CODES = ImmutableSet.of(BS, MASTER, SPO, HIGHER, ENTRANT_FOREIGN);
}
