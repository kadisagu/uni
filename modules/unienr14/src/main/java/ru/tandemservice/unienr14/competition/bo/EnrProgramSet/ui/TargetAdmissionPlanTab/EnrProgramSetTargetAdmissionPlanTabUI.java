/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.TargetAdmissionPlanTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.TargetAdmissionPlanEdit.EnrProgramSetTargetAdmissionPlanEdit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrTargetAdmissionPlanWrapper;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 04.05.2014
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "programSetHolder.id"))
public class EnrProgramSetTargetAdmissionPlanTabUI extends UIPresenter
{
    private EntityHolder<EnrProgramSetBase> _programSetHolder = new EntityHolder<>();

    private List<ProgramSetOrgUnitWrapper> _orgUnitList;
    private ProgramSetOrgUnitWrapper _currentOrgUnit;
    private EnrTargetAdmissionPlanWrapper _currentTaKindRow;
    private EnrCompetition currentTaCompetition;

    @Override
    public void onComponentRefresh()
    {
        getProgramSetHolder().refresh(EnrProgramSetBase.class);
        setOrgUnitList(getBlockList());
    }

    // Listeners
    public void onClickEditPlan()
    {
        getActivationBuilder().asCurrent(EnrProgramSetTargetAdmissionPlanEdit.class).parameter("programSetOrgUnit", getListenerParameterAsLong()).activate();
    }

    // presenter

    public String getPlanStyle()
    {
        Integer plan = getCurrentTaKindRow().getPlan(getCurrentTaCompetition().getId());
        if (null == plan) return "";
        Integer calculatedPlan = getCurrentTaKindRow().getCalculatedPlan(getCurrentTaCompetition().getId());
        return plan.equals(calculatedPlan) ? "" : "color: red;";
    }

    public String getPlanValue()
    {
        Integer plan = getCurrentTaKindRow().getPlan(getCurrentTaCompetition().getId());
        if (null == plan) return "";
        Integer calculatedPlan = getCurrentTaKindRow().getCalculatedPlan(getCurrentTaCompetition().getId());
        return plan + (plan.equals(calculatedPlan) ? "" : " (задан вручную)");
    }

    // data access

    protected List<EnrProgramSetTargetAdmissionPlanTabUI.ProgramSetOrgUnitWrapper> getBlockList()
    {
        List<EnrProgramSetOrgUnit> programSetOrgUnits = DataAccessServices.dao().getList(new DQLSelectBuilder()
            .fromEntity(EnrProgramSetOrgUnit.class, "psou")
            .column(property("psou"))
            .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().fromAlias("psou"))
            .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().id()), value(getProgramSetHolder().getId())))
            .where(or(
                eq(property("psou", EnrProgramSetOrgUnit.targetAdmOpen()), value(Boolean.TRUE)),
                in(property("psou", EnrProgramSetOrgUnit.id()), new DQLSelectBuilder()
                    .fromEntity(EnrTargetAdmissionPlan.class, "tap")
                    .column(property("tap", EnrTargetAdmissionPlan.programSetOrgUnit().id()))
                    .where(eq(property("tap", EnrTargetAdmissionPlan.programSetOrgUnit().programSet().id()), value(getProgramSetHolder().getId())))
                    .buildQuery()))));

        Collections.sort(programSetOrgUnits, new Comparator<EnrProgramSetOrgUnit>()
        {
            @Override
            public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
            {
                return EnrOrgUnit.ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR.compare(o1.getOrgUnit(), o2.getOrgUnit());
            }
        });

        List<EnrProgramSetTargetAdmissionPlanTabUI.ProgramSetOrgUnitWrapper> result = new ArrayList<>();
        for (EnrProgramSetOrgUnit programSetOrgUnit: programSetOrgUnits) {

            List<EnrCompetition> taCompetitionList = DataAccessServices.dao().getList(new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "c")
                .where(eq(property("c", EnrCompetition.programSetOrgUnit()), value(programSetOrgUnit)))
                .where(eq(property("c", EnrCompetition.type().code()), value(EnrCompetitionTypeCodes.TARGET_ADMISSION)))
                .order(property("c", EnrCompetition.eduLevelRequirement().priority())));

            List<EnrTargetAdmissionPlanWrapper> rowList = EnrProgramSetManager.instance().dao().getProgramSetOrgUnitTAPlans(programSetOrgUnit.getId(), false);
            int totalPlan = 0;
            for (EnrTargetAdmissionPlanWrapper w : rowList) {
                totalPlan += w.getPlan();
            }

            result.add(new EnrProgramSetTargetAdmissionPlanTabUI.ProgramSetOrgUnitWrapper(programSetOrgUnit, totalPlan, rowList, taCompetitionList));
        }

        return result;
    }


    /** Набор ОП на подразделении, ведущем прием по ЦП. */
    public static class ProgramSetOrgUnitWrapper extends DataWrapper
    {
        private EnrProgramSetOrgUnit _programSetOrgUnit;
        private int _totalPlan;
        private List<EnrTargetAdmissionPlanWrapper> _rowList;
        private List<EnrCompetition> taCompetitionList;
        private String _warn;

        public ProgramSetOrgUnitWrapper(EnrProgramSetOrgUnit programSetOrgUnit, int totalPlan, List<EnrTargetAdmissionPlanWrapper> rowList, List<EnrCompetition> taCompetitionList)
        {
            super(programSetOrgUnit);
            _programSetOrgUnit = programSetOrgUnit;
            _totalPlan = totalPlan;
            _rowList = rowList;
            this.taCompetitionList = taCompetitionList;

            StringBuilder warnRaw = new StringBuilder();

            if (getTotalPlan() != getProgramSetOrgUnit().getTargetAdmPlan()) {
                warnRaw.append("<div>Суммарный план приема по видам ЦП (").append(getTotalPlan()).append(") отличается от общей квоты ЦП (").append(getProgramSetOrgUnit().getTargetAdmPlan()).append(").</div>");
            }

            for (EnrCompetition taComp : taCompetitionList)
            {
                int sum = 0;
                for (EnrTargetAdmissionPlanWrapper planWrapper : rowList)
                {
                    Integer plan = planWrapper.getPlan(taComp.getId());
                    sum += plan != null ? plan : 0;
                }
                if (sum != taComp.getPlan()) {
                    warnRaw.append("<div>Для базового уровня «").append(taComp.getEduLevelRequirement().getShortTitle())
                            .append("» суммарное число мест по видам ЦП (").append(sum).append(") отличается от числа мест по конкурсу целевого приема (")
                            .append(taComp.getPlan()).append(").</div>");
                }
            }

            _warn = warnRaw.toString();
        }

        public EnrProgramSetOrgUnit getProgramSetOrgUnit(){ return _programSetOrgUnit; }
        public int getTotalPlan(){ return _totalPlan; }
        public List<EnrTargetAdmissionPlanWrapper> getRowList(){ return _rowList; }
        public List<EnrCompetition> getTaCompetitionList() { return taCompetitionList; }

        public String getCaption(){ return "Планы приема по видам ЦП (" + getProgramSetOrgUnit().getOrgUnit().getDepartmentTitle() + ", квота ЦП: " + getProgramSetOrgUnit().getTargetAdmPlan() + ")"; }

        public boolean isHasWarnings(){ return StringUtils.isNotEmpty(_warn); }

        public String getWarningMessage(){ return _warn; }
    }


    // Getters && Setters

    public EntityHolder<EnrProgramSetBase> getProgramSetHolder(){ return _programSetHolder; }
    public EnrProgramSetBase getProgramSet(){ return getProgramSetHolder().getValue(); }

    public List<ProgramSetOrgUnitWrapper> getOrgUnitList(){ return _orgUnitList; }
    public void setOrgUnitList(List<ProgramSetOrgUnitWrapper> orgUnitList){ _orgUnitList = orgUnitList; }

    public ProgramSetOrgUnitWrapper getCurrentOrgUnit(){ return _currentOrgUnit; }
    public void setCurrentOrgUnit(ProgramSetOrgUnitWrapper currentOrgUnit){ _currentOrgUnit = currentOrgUnit; }

    public EnrTargetAdmissionPlanWrapper getCurrentTaKindRow(){ return _currentTaKindRow; }
    public void setCurrentTaKindRow(EnrTargetAdmissionPlanWrapper currentTaKindRow){ _currentTaKindRow = currentTaKindRow; }

    public EnrCompetition getCurrentTaCompetition()
    {
        return currentTaCompetition;
    }

    public void setCurrentTaCompetition(EnrCompetition currentTaCompetition)
    {
        this.currentTaCompetition = currentTaCompetition;
    }

    public boolean isEmptyProgramSetTaPlan() {
        int sum = 0;
        for (ProgramSetOrgUnitWrapper orgUnitWrapper : getOrgUnitList()) {
            sum += orgUnitWrapper.getProgramSetOrgUnit().getTargetAdmPlan();
        }
        return sum <= 0;
    }
}