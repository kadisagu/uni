package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Базовые уровни образования поступающих"
 * Имя сущности : enrEduLevelRequirement
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEduLevelRequirementCodes
{
    /** Константа кода (code) элемента : Уровень не указан (title) */
    String NO = "1";
    /** Константа кода (code) элемента : Среднее общее образование (title) */
    String SOO = "2";
    /** Константа кода (code) элемента : Профессиональное образование (title) */
    String PO = "3";
    /** Константа кода (code) элемента : Основное общее образование (title) */
    String OOO = "4";
    /** Константа кода (code) элемента : Среднее профессиональное образование (title) */
    String SPO = "5";
    /** Константа кода (code) элемента : Высшее образование (title) */
    String VO = "6";
    /** Константа кода (code) элемента : Высшее образование (специалитет, магистратура) (title) */
    String VO_SPEC_MAG = "7";
    /** Константа кода (code) элемента : Среднее общее или профессиональное образование (title) */
    String SOO_AND_PO = "8";
    /** Константа кода (code) элемента : Среднее общее или среднее профессиональное образование (title) */
    String SOO_AND_SPO = "9";
    /** Константа кода (code) элемента : Среднее общее или высшее образование (title) */
    String SOO_AND_VO = "10";

    Set<String> CODES = ImmutableSet.of(NO, SOO, PO, OOO, SPO, VO, VO_SPEC_MAG, SOO_AND_PO, SOO_AND_SPO, SOO_AND_VO);
}
