/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionWrapper;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;

/**
 * @author oleyba
 * @since 3/27/15
 */
@State({
    @Bind(key = EnrEnrollmentModelCompetitionInStepPubUI.PARAM_STEP, binding = "step.id", required = true),
    @Bind(key = EnrEnrollmentModelCompetitionInStepPubUI.PARAM_COMPETITION, binding = "competitionHolder.id", required = true),
    @Bind(key = EnrEnrollmentModelCompetitionInStepPubUI.PARAM_TARGET_ADMISSION_KIND, binding = "taKind.id")
})
public class EnrEnrollmentModelCompetitionInStepPubUI extends UIPresenter
{
    public static final String PARAM_STEP = "step";
    public static final String PARAM_COMPETITION = "competition";
    public static final String PARAM_TARGET_ADMISSION_KIND = "taKind";

    private EnrModelStep step = new EnrModelStep();
    private EntityHolder<EnrCompetition> competitionHolder = new EntityHolder<>();
    private EnrTargetAdmissionKind taKind = new EnrTargetAdmissionKind();

    private EnrModelCompetitionWrapper wrapper;

    private IUIDataSource itemDS;

    @Override
    public void onComponentRefresh()
    {
        setStep(IUniBaseDao.instance.get().getNotNull(EnrModelStep.class, getStep().getId()));
        if (null != getTaKind() && null != getTaKind().getId()) {
            setTaKind(IUniBaseDao.instance.get().get(EnrTargetAdmissionKind.class, getTaKind().getId()));
        } else {
            setTaKind(null);
        }
        getCompetitionHolder().refresh();

        wrapper = EnrEnrollmentModelManager.instance().dao().calcStats(getStep(), getStep().getModel(), getCompetition(), getTaKind());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrModelStepItemDSHandler.BIND_ENR_STEP, getStep());
        dataSource.put(EnrModelStepItemDSHandler.BIND_COMPETITION, getCompetition());
        dataSource.put(EnrModelStepItemDSHandler.BIND_TA_KIND, getTaKind());
    }

    // actions

    public void onInclude() {
        // todo нужно проверять, что включать вообще можно
        EnrModelStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setIncluded(true);
        IUniBaseDao.instance.get().update(item);
    }

    public void onExclude() {
        EnrModelStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setIncluded(false);
        IUniBaseDao.instance.get().update(item);
    }

    public void onSetEnrolled() {
        EnrModelStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setEnrolled(true);
        IUniBaseDao.instance.get().update(item);
    }

    public void onClearEnrolled() {
        EnrModelStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setEnrolled(false);
        IUniBaseDao.instance.get().update(item);
    }

    public void onSetEnrollmentAvailable() {
        EnrModelStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setEnrollmentAvailable(true);
        IUniBaseDao.instance.get().update(item);
    }

    public void onClearEnrollmentAvailable() {
        EnrModelStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setEnrollmentAvailable(false);
        IUniBaseDao.instance.get().update(item);
    }

    // presenter

    public IEntity getCurrentItem() {
        return getItemDS().getCurrent();
    }

    public IUIDataSource getItemDS()
    {
        if (itemDS == null)
            itemDS = getConfig().getDataSource("itemDS"); // todo сделать константу
        return itemDS;
    }

    public EnrCompetition getCompetition()
    {
        return getCompetitionHolder().getValue();
    }

    public boolean isEditAllowed() {
        return getStep().getBaseRealStep() == null;
    }

    public boolean isEditNotAllowed() {
        return !isEditAllowed();
    }

    // getters and setters

    public EntityHolder<EnrCompetition> getCompetitionHolder()
    {
        return competitionHolder;
    }

    public EnrModelStep getStep()
    {
        return step;
    }

    public void setStep(EnrModelStep step)
    {
        this.step = step;
    }

    public EnrTargetAdmissionKind getTaKind()
    {
        return taKind;
    }

    public void setTaKind(EnrTargetAdmissionKind taKind)
    {
        this.taKind = taKind;
    }

    public EnrModelCompetitionWrapper getWrapper()
    {
        return wrapper;
    }
}