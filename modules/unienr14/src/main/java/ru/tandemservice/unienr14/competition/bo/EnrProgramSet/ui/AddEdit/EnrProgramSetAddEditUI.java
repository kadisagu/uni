/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.Pub.EnrProgramSetPub;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrProgramSetOrgUnitWrapper;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author oleyba
 * @since 2/4/14
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programSetHolder.id"),
        @Bind(key = EnrProgramSetAddEditUI.SECONDARY, binding = "programSetSecondary", required = true)
})
@Input({
        @Bind(key = EnrProgramSetAddEditUI.REQUEST_TYPE_BIND, binding = "requestType")
})
public class EnrProgramSetAddEditUI extends UIPresenter
{
    public static final String SECONDARY = "secondary";
    public static final String REQUEST_TYPE_BIND = "requestType";

    private EntityHolder<EnrProgramSetBase> _programSetHolder = new EntityHolder<>();

    private EnrEnrollmentCampaign _enrCampaign;
    private EnrRequestType _requestType;
    private ISelectModel _formativeOrgUnitModel;
    private List<EnrProgramSetOrgUnitWrapper> _rowList;

    private EnrProgramSetOrgUnitWrapper _currentRow;
    private EnrProgramSetOrgUnitWrapper _editedRow;
    private Integer _editedRowIndex;
    private boolean _rowAdded;

    private boolean _competitionOpen;
    private boolean _programSetSecondary;
    private boolean _targetContract;
    private boolean _crimea;

    private EduProgramKind _programKind;
    private List<EduProgramHigherProf> _programList = new ArrayList<>();
    private EduProgramSubject _programSubject;
    private EduProgramForm _programForm;
    private String _programTitle;
    private EduProgramSecondaryProf _program;

    private List<ExternalOrgUnit> _externalOrgUnits;
    private String programPrintTitle;

    @Override
    public void onComponentRefresh()
    {
        if (isEditForm())
        {
            getProgramSetHolder().setValue(IUniBaseDao.instance.get().getNotNull(EnrProgramSetBase.class, getProgramSetHolder().getId()));
            setEnrCampaign(getProgramSet().getEnrollmentCampaign());
            setRequestType(getProgramSet().getRequestType());
            setCrimea(getProgramSet().isAcceptPeopleResidingInCrimea());


            if (isProgramSetSecondary())
            {
                setProgram(((EnrProgramSetSecondary) getProgramSet()).getProgram());
                setProgramTitle(getProgramSet().getTitle());
                setProgramPrintTitle(getProgramSet().getPrintTitle());
            } else
            {
                setProgramKind(getProgramSet().getProgramSubject().getSubjectIndex().getProgramKind());
                setProgramSubject(getProgramSet().getProgramSubject());
                setProgramForm(getProgramSet().getProgramForm());
                setProgramTitle(getProgramSet().getTitle());
                setProgramPrintTitle(getProgramSet().getPrintTitle());
                setProgramList(new ArrayList<>());
                for (EnrProgramSetItem item : IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), getProgramSet()))
                    getProgramList().add(item.getProgram());

                setTargetContract(getProgramSet().isTargetContractTraining());
                setExternalOrgUnits(EnrProgramSetManager.instance().dao().getExternalOrgUnitsForProgramSet(getProgramSet()));
            }

        } else
        {
            setEnrCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
            if (null == getEnrCampaign()){ throw new IllegalStateException("Set default enrCampaign before activating this form."); }
            getEnrCampaign().checkOpen();
        }

        setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        setCompetitionOpen(isEditForm() && ISharedBaseDao.instance.get().existsEntity(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().s(), getProgramSet()));

        if (getRowList() == null)
            fillRowList();

        if (!isProgramSetSecondary())
        {
            if (getRequestType() == null) {
                throw new IllegalStateException();
            }

            onChangeProgramKind();
        }
    }

    private void fillRowList()
    {
        setRowList(new ArrayList<>());
        if (isEditForm())
        {
            getRowList().addAll(CollectionUtils.collect(
                    IUniBaseDao.instance.get().getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet(), getProgramSet(), EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().fullTitle().s()),
                    EnrProgramSetOrgUnitWrapper::new));
        }

        setEditedRow(null);
        setEditedRowIndex(null);
        setRowAdded(false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrCampaign());
        dataSource.put(EnrProgramSetAddEdit.PARAM_FORM, getProgramForm());
        dataSource.put(EnrProgramSetAddEdit.PARAM_SUBJECT, getProgramSubject());
        dataSource.put(EnrProgramSetAddEdit.PARAM_PROGRAM_KIND, getProgramKind());
        dataSource.put(EnrProgramSetAddEdit.PARAM_REQUEST_TYPE, getRequestType());

        List<Long> ids = new ArrayList<>();
        for (EnrProgramSetOrgUnitWrapper row: getRowList())
        {
            if (row.getOrgUnit() != null) ids.add(row.getOrgUnit().getId());
        }

        if (getCurrentRow() != null && getCurrentRow().getOrgUnit() != null)
        {
            ids.remove(getCurrentRow().getOrgUnit().getId());
        }

        dataSource.put(EnrProgramSetAddEdit.PARAM_ADDED_ORG_UNIT_IDS, ids);
    }

    // Listeners
    public void onClickApply()
    {
        EnrProgramSetBase programSet = getProgramSet();
        boolean addForm = isAddForm();
        if (addForm)
        {
            if (isProgramSetSecondary())
            {
                programSet = new EnrProgramSetSecondary();

            } else {
                switch (getProgramKind().getCode())
                {
                    case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA:
                    case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV:
                        programSet = new EnrProgramSetBS();
                        break;
                    case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY:
                        programSet = new EnrProgramSetMaster();
                        break;
                    case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_:
                        programSet = new EnrProgramSetHigher();
                        break;
                    case EduProgramKindCodes.PROGRAMMA_ORDINATURY:
                        programSet = new EnrProgramSetHigher();
                        break;
                    case EduProgramKindCodes.PROGRAMMA_INTERNATURY:
                        programSet = new EnrProgramSetHigher();
                        break;
                    default:
                        throw new IllegalStateException(); // можно сохранять только бакалавров, магистров, специалистов и кадров высшей квалификации
                }
            }
            programSet.setRequestType(getRequestType());
        }


//        if(!getProgramList().isEmpty())
//        {
//            EduProgramOrientation defOrientation = getProgramList().get(0).getProgramOrientation();
//            boolean notEqualOrientations = false;
//            for (EduProgramHigherProf programHigherProf : getProgramList())
//            {
//                if(!Objects.equals(defOrientation, programHigherProf.getProgramOrientation()))
//                {
//                    notEqualOrientations = true;
//                    break;
//                }
//            }
//            if(notEqualOrientations)
//                throw new ApplicationException("Ориентации образовательных программ в наборе должны совпадать.");
//        }

        if(getRowList() == null || getRowList().isEmpty())
            throw new ApplicationException("Введите план приема.");
        else
        {
            for(EnrProgramSetOrgUnitWrapper wrapper : getRowList())
            {
                if (isTargetContract() && (wrapper.getMinisterialPlan() > 0 || wrapper.getExclusivePlan() > 0 || wrapper.getTargetAdmPlan() > 0))
                    throw new ApplicationException(getConfig().getProperty("programSetOU.validate.targetContractPlan"));
                else if(wrapper.getMinisterialPlan() + wrapper.getExclusivePlan() + wrapper.getTargetAdmPlan() + wrapper.getContractPlan() <= 0)
                    throw new ApplicationException("Введите план приема.");
            }
        }

        EnrProgramSetManager.instance().dao().saveOrUpdateProgramSet(updateProgramSet(programSet), getProgramList(), getRowList());

        EnrProgramSetManager.instance().dao().saveOrUpdateCustomerTargetContractTraining(programSet, isTargetContract(), getExternalOrgUnits());

        if (addForm) {
            _uiActivation.asDesktopRoot(EnrProgramSetPub.class).parameter(PUBLISHER_ID, programSet.getId()).activate();
        }
        deactivate();
    }

    /* Обновляет набор ОП (специальность), добавляет базовые свойства. */
    private <T extends EnrProgramSetBase> T updateProgramSet(T programSet)
    {
        programSet.setEnrollmentCampaign(getEnrCampaign());
        programSet.setTitle(getProgramTitle());
        programSet.setPrintTitle(getProgramPrintTitle());
        if (isProgramSetSecondary())
        {
            EnrProgramSetSecondary ps = (EnrProgramSetSecondary) programSet;
            ps.setProgram(getProgram());
            programSet.setProgramSubject(ps.getProgram().getProgramSubject());
            programSet.setProgramForm(ps.getProgram().getForm());
            programSet.setMethodDivCompetitions(IUniBaseDao.instance.get().getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.SEC_NO_DIV));

        } else {
            programSet.setProgramSubject(getProgramSubject());
            programSet.setProgramForm(getProgramForm());
            if (isAddForm() && isProgramSetBS()) {
                programSet.setMethodDivCompetitions(IUniBaseDao.instance.get().getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_NO_DIV));
            } else if(isAddForm() && isProgramSetHigher()) {
                final String methodCode = EnrProgramSetHigher.getRequiredDivMethodCode(getRequestType().getCode());
                programSet.setMethodDivCompetitions(IUniBaseDao.instance.get().getCatalogItem(EnrMethodDivCompetitions.class, methodCode));
            } else if(isAddForm() && isProgramSetMaster()) {
                programSet.setMethodDivCompetitions(IUniBaseDao.instance.get().getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.MASTER_NO_DIV));
            }
            if (programSet.getMethodDivCompetitions() == null) {
                throw new IllegalStateException();
            }

            programSet.setTargetContractTraining(isTargetContract());
        }
        programSet.setAcceptPeopleResidingInCrimea(isCrimea());
        return programSet;
    }

    public void onClickAddRow()
    {
        if (getEditedRow() != null)
            return;

        setEditedRow(new EnrProgramSetOrgUnitWrapper(0L, ""));

        getRowList().add(getEditedRow());
        setEditedRowIndex(getRowList().size() - 1);

        setRowAdded(true);
    }

    public void onClickEditRow()
    {
        if (getEditedRow() != null)
            return;

        EnrProgramSetOrgUnitWrapper baseRow = findRow();

        setEditedRow(baseRow.getCopy());
        setEditedRowIndex(getRowList().indexOf(baseRow));
        setRowAdded(false);
    }

    public void onClickDeleteRow()
    {
        EnrProgramSetOrgUnitWrapper row = findRow();
        if (null != row)
            getRowList().remove(row);
    }

    public void onClickSaveRow()
    {
        ErrorCollector errorCollector = validateEditRow();
        if (errorCollector != null && errorCollector.hasErrors())
            return;

        if (getEditedRowIndex() != null && getRowList().size() > getEditedRowIndex() && getEditedRowIndex() >= 0) {
            getRowList().set(getEditedRowIndex(), getEditedRow().getCopy());
        }

        setEditedRowIndex(null);
        setEditedRow(null);
        setRowAdded(false);
    }

    public void onClickCancelEdit()
    {
        if (isRowAdded() && !getRowList().isEmpty())
            getRowList().remove(getRowList().size() - 1);

        setEditedRowIndex(null);
        setEditedRow(null);
        setRowAdded(false);
    }

    public void refreshTitles()
    {
        if (isAddForm()) {
            if(isProgramSetSecondary()) {
                setProgramTitle(getProgram().getProgramSubject().getTitleWithCode());
                setProgramPrintTitle(getProgram().getProgramSubject().getTitleWithCode());
            }else if(getProgramSubject() != null){
                setProgramTitle(getProgramSubject().getTitleWithCode());
                setProgramPrintTitle(getProgramSubject().getTitleWithCode());
            }
        }
    }

    /*
        public void onClickMoveRowUp()
        {
            List<EnrProgramSetOrgUnitWrapper> rows = getRowList();
            EnrProgramSetOrgUnitWrapper baseRow = findRow();

            int index = rows.indexOf(baseRow);
            if (index > 0) { Collections.swap(rows, index - 1, index); }
        }

        public void onClickMoveRowDown()
        {
            List<EnrProgramSetOrgUnitWrapper> rows = getRowList();
            EnrProgramSetOrgUnitWrapper baseRow = findRow();

            int index = rows.indexOf(baseRow);
            if (index < rows.size()-1) { Collections.swap(rows, index, index+1); }
        }

        public boolean isCurrentRowFirst()
        {
            EnrProgramSetOrgUnitWrapper currentRow = getCurrentRow();
            if (null == currentRow) { return false; }

            List<EnrProgramSetOrgUnitWrapper> rowList = getRowList();
            if (rowList.isEmpty()) { return false; }

            return currentRow.equals(rowList.get(0));
        }

        public boolean isCurrentRowLast()
        {
            EnrProgramSetOrgUnitWrapper currentRow = getCurrentRow();
            if (null == currentRow) { return false; }

            List<EnrProgramSetOrgUnitWrapper> rowList = getRowList();
            if (rowList.isEmpty()) { return false; }

            return currentRow.equals(rowList.get(rowList.size()-1));
        }
    */

    public void onChangeProgramKind()
    {
        if (isProgramSetSecondary())
        {
            for (EnrProgramSetOrgUnitWrapper row: getRowList())
            {
                row.setExclusivePlan(0);
                row.setTargetAdmPlan(0);
            }
        }
        else if (isProgramSetMaster() || isProgramSetHigher())
        {
            for (EnrProgramSetOrgUnitWrapper row: getRowList())
            {
                row.setExclusivePlan(0);
            }
        }
    }

    private ErrorCollector validateEditRow()
    {
        EnrProgramSetOrgUnitWrapper editedRow = getEditedRow();
        if (editedRow == null) return null;
        final ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (isTargetContract() && (editedRow.getTargetAdmPlan() > 0 || editedRow.getExclusivePlan() > 0 || editedRow.getMinisterialPlan() > 0))
            errorCollector.add(getConfig().getProperty("programSetOU.validate.targetContractPlan"), "ministerialPlan", "targetAdmPlan", "exclusivePlan");

        if(editedRow.getMinisterialPlan() + editedRow.getExclusivePlan() + editedRow.getTargetAdmPlan() + editedRow.getContractPlan() <= 0)
                errorCollector.add(getConfig().getProperty("programSetOU.validate.nullPlan"), "contractPlan", "ministerialPlan");
        else if ((editedRow.getTargetAdmPlan() > 0 || editedRow.getExclusivePlan() > 0) && editedRow.getMinisterialPlan() == 0)
            errorCollector.add(getConfig().getProperty("programSetOU.validate.minPlan"), "ministerialPlan");
        else
        {
            if (isProgramSetMaster() || isProgramSetHigher())
            {
                if (editedRow.getTargetAdmPlan() > editedRow.getMinisterialPlan())
                    errorCollector.add(getConfig().getProperty("programSetOU.validate.minPlanToTa"), "ministerialPlan", "targetAdmPlan");

            } else if (isProgramSetBS()) {

                if (editedRow.getTargetAdmPlan() + editedRow.getExclusivePlan() > editedRow.getMinisterialPlan())
                    errorCollector.add(getConfig().getProperty("programSetOU.validate.minPlanToTAAndExclusive"), "ministerialPlan", "exclusivePlan", "targetAdmPlan");
            }
        }

        return errorCollector;
    }

    private EnrProgramSetOrgUnitWrapper findRow()
    {
        final Long id = getListenerParameterAsLong();
        return CollectionUtils.find(getRowList(), object -> object.getId().equals(id));
    }


    // Getters && Setters
    public EntityHolder<EnrProgramSetBase> getProgramSetHolder(){ return _programSetHolder; }
    public EnrProgramSetBase getProgramSet(){ return getProgramSetHolder().getValue(); }

    public EnrEnrollmentCampaign getEnrCampaign(){ return _enrCampaign; }
    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign){ _enrCampaign = enrCampaign; }

    public ISelectModel getFormativeOrgUnitModel(){ return _formativeOrgUnitModel; }
    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel){ _formativeOrgUnitModel = formativeOrgUnitModel; }

    public List<EnrProgramSetOrgUnitWrapper> getRowList(){
        return _rowList;
    }
    public void setRowList(List<EnrProgramSetOrgUnitWrapper> rowList){ _rowList = rowList; }

    public EnrProgramSetOrgUnitWrapper getCurrentRow(){ return _currentRow; }
    public void setCurrentRow(EnrProgramSetOrgUnitWrapper currentRow){ _currentRow = currentRow; }

    public EnrProgramSetOrgUnitWrapper getEditedRow(){ return _editedRow; }
    public void setEditedRow(EnrProgramSetOrgUnitWrapper editedRow){ _editedRow = editedRow; }

    public Integer getEditedRowIndex(){ return _editedRowIndex; }
    public void setEditedRowIndex(Integer editedRowIndex){ _editedRowIndex = editedRowIndex; }

    public boolean isRowAdded(){ return _rowAdded; }
    public void setRowAdded(boolean rowAdded){ _rowAdded = rowAdded; }

    public boolean isCompetitionOpen(){ return _competitionOpen; }
    public void setCompetitionOpen(boolean competitionOpen){ _competitionOpen = competitionOpen; }

    public boolean isProgramSetSecondary(){ return _programSetSecondary; }
    public void setProgramSetSecondary(boolean programSetSecondary){ _programSetSecondary = programSetSecondary; }

    public EduProgramKind getProgramKind(){ return _programKind; }
    public void setProgramKind(EduProgramKind programKind){ _programKind = programKind; }

    public List<EduProgramHigherProf> getProgramList(){ return _programList; }
    public void setProgramList(List<EduProgramHigherProf> programList){ _programList = programList; }

    public EduProgramSubject getProgramSubject(){ return _programSubject; }
    public void setProgramSubject(EduProgramSubject programSubject){ _programSubject = programSubject; }

    public EduProgramForm getProgramForm(){ return _programForm; }
    public void setProgramForm(EduProgramForm programForm){ _programForm = programForm; }

    public String getProgramTitle(){ return _programTitle; }
    public void setProgramTitle(String programTitle){ _programTitle = programTitle; }

    public EduProgramSecondaryProf getProgram(){ return _program; }
    public void setProgram(EduProgramSecondaryProf program){ _program = program; }

    public EnrRequestType getRequestType(){ return _requestType; }
    public void setRequestType(EnrRequestType requestType){ _requestType = requestType; }

    // Util
    private boolean isAddForm(){ return getProgramSetHolder().getId() == null; }

    /* Форма редактирования. Набор ОП уже есть. */
    public boolean isEditForm(){ return !isAddForm(); }


    public String getSticker()
    {
        return isProgramSetSecondary() ?
                (isAddForm() ? getConfig().getProperty("ui.sticker.secondary.add") : getConfig().getProperty("ui.sticker.secondary.edit")) :
                (isAddForm() ? getConfig().getProperty("ui.sticker.add") : getConfig().getProperty("ui.sticker.edit"));
    }

    public boolean isEditMode() { return getEditedRow() != null; }

    public boolean isCurrentRowInEditMode() { return getEditedRow() != null && getEditedRowIndex().equals(getRowList().indexOf(getCurrentRow())); }

    public boolean isProgramSetBS(){ return getProgramKind() != null && getProgramKind().isBS(); }

    public boolean isProgramSetMaster(){ return getProgramKind() != null && (getProgramKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY)); }

    public boolean isProgramSetHigher()
    {
        return getProgramKind() != null && getProgramKind().isHigher();
    }

    public boolean isTargetContract()
    {
        return _targetContract;
    }

    public void setTargetContract(boolean targetContract)
    {
        _targetContract = targetContract;
    }

    public List<ExternalOrgUnit> getExternalOrgUnits()
    {
        return _externalOrgUnits;
    }

    public void setExternalOrgUnits(List<ExternalOrgUnit> externalOrgUnits)
    {
        _externalOrgUnits = externalOrgUnits;
    }

    public boolean isCrimea()
    {
        return _crimea;
    }

    public void setCrimea(boolean crimea)
    {
        _crimea = crimea;
    }

    public String getRenderPage()
    {
        StringBuilder result = new StringBuilder(EnrProgramSetAddEditUI.class.getPackage().getName() + ".page.");
        if (getProgramKind() == null || isProgramSetSecondary())
            result.append("ProgramSetSecondary");

        if (isProgramSetBS())
            result.append("ProgramSetBS");

        if (isProgramSetMaster() || isProgramSetHigher())
            result.append("ProgramSetMasterAndHigher");

        return result.toString();
    }

    public boolean isDivCompetitionsMethodsDisabled()
    {
        return isEditForm() && IUniBaseDao.instance.get().existsEntity(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().id().s(), getProgramSet().getId());
    }

    public String getProgramPrintTitle()
    {
        return programPrintTitle;
    }

    public void setProgramPrintTitle(String programPrintTitle)
    {
        this.programPrintTitle = programPrintTitle;
    }
}