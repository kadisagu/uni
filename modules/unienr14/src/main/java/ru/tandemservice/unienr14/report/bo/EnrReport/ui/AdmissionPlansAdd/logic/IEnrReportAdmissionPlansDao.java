/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd.EnrReportAdmissionPlansAddUI;

/**
 * @author rsizonenko
 * @since 17.10.2014
 */
public interface IEnrReportAdmissionPlansDao extends INeedPersistenceSupport {
    Long createReport(EnrReportAdmissionPlansAddUI model);
}
