package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип контекста действия роли"
 * Имя сущности : localRoleScope
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface LocalRoleScopeCodes
{
    /** Константа кода (code) элемента : Подразделение (title) */
    String ORG_UNIT = "orgUnit";
    /** Константа кода (code) элемента : Приемная кампания (title) */
    String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    /** Константа кода (code) элемента : Отборочная комиссия (title) */
    String ENROLLMENT_COMMISSION = "enrollmentCommission";

    Set<String> CODES = ImmutableSet.of(ORG_UNIT, ENROLLMENT_CAMPAIGN, ENROLLMENT_COMMISSION);
}
