/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;

import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionDSHandler;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.Pub.EnrProgramSetPub;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;

/**
 * @author vdanilov
 */
@Configuration
public class EnrExamSetPub extends BusinessComponentManager
{
    public static final String COMPETITION_DS = "competitionDS";
    public static final String COMPETITION_TYPE_SELECT_DS = "competitionTypeSelectDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(selectDS(COMPETITION_TYPE_SELECT_DS, competitionTypeSelectDSHandler()))
        .addDataSource(searchListDS(COMPETITION_DS, competitionDSColumns(), competitionDSHandler()))
        .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder(COMPETITION_DS)
        .addColumn(publisherColumn("programSet", EnrCompetition.programSetOrgUnit().programSet().title())
            .publisherLinkResolver(new SimplePublisherLinkResolver(EnrCompetition.programSetOrgUnit().programSet().id())
            .setComponent(EnrProgramSetPub.class))
        )
        .addColumn(textColumn("competitionType", EnrCompetition.type().shortTitle().s()))
        .addColumn(textColumn("programSubject", EnrCompetition.programSetOrgUnit().programSet().programSubject().titleWithCode().s()))
        .addColumn(textColumn("orgUnit", EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
        .addColumn(textColumn("formativeOrgUnit", EnrCompetition.programSetOrgUnit().formativeOrgUnit().shortTitleWithTopEmphasized().s()))
        .addColumn(textColumn("developForm", EnrCompetition.programSetOrgUnit().programSet().programForm().shortTitle().s()))
        .addColumn(textColumn("programKind", EnrCompetition.programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().shortTitle().s()))
        .addColumn(textColumn("eduLevelRequirement", EnrCompetition.eduLevelRequirement().shortTitle().s()))
        .addColumn(textColumn("exams", EnrCompetitionDSHandler.VIEW_PROP_EXAMS).formatter(NewLineFormatter.NOBR_IN_LINES))
        .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public IBusinessHandler<DSInput, DSOutput> competitionDSHandler()
    {
        return new EnrCompetitionDSHandler(getName()).setPageable(false);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> competitionTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetitionType.class)
        .order(EnrCompetitionType.code())
        .filter(EnrCompetitionType.title())
        .pageable(true);
    }
}
