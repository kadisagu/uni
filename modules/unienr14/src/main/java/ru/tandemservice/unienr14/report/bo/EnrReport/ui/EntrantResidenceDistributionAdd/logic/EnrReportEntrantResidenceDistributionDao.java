/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantResidenceDistributionAdd.logic;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantResidenceDistributionAdd.EnrReportEntrantResidenceDistributionAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantResidenceDistribution;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 20.06.2014
 */
public class EnrReportEntrantResidenceDistributionDao extends UniBaseDao implements IEnrReportEntrantResidenceDistributionDao
{
    @Override
    public long createReport(EnrReportEntrantResidenceDistributionAddUI model) {
        EnrReportEntrantResidenceDistribution report = new EnrReportEntrantResidenceDistribution();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setSettlementDetalization(model.isSettlementDetalization() ? "Да" : "Нет");
        report.setSplitByProgramSubjects(model.isSplitByProgramSubjects() ? "Да" : "Нет");
        report.setStage(model.getStageSelector().getStage().getTitle());
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantResidenceDistribution.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantResidenceDistribution.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantResidenceDistribution.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantResidenceDistribution.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantResidenceDistribution.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantResidenceDistribution.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantResidenceDistribution.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantResidenceDistribution.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantResidenceDistribution.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantResidenceDistributionAddUI model)
    {



        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_RESIDENCE_DISTRIBUTION);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Дата

        new RtfInjectModifier().put("dateForm", new SimpleDateFormat("dd.MM.yyyy").format(new Date())).modify(document);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");

        requestedCompDQL
              //  .where(le(property("reqComp", EnrRequestedCompetition.state().priority()), value(model.getStageSelector().getStage().getId())))
                .column(property("reqComp"));

        List<EnrRequestedCompetition> reqCompList = getList(requestedCompDQL);


        // Компаратор адрес-айтемов. Сравнивает по тайтлу с типом. Регионы иностранных гос-в объединяет в одну запись. Россию смещает вниз по таблице
        Comparator<AddressItem> parentItemComparator = new Comparator<AddressItem>() {
            @Override
            public int compare(AddressItem o1, AddressItem o2) {
                if (o1==null)
                    if (o2==null)
                        return 0;
                    else
                        return 1000;
                else if (o2 == null)
                    return -1000;
                int result = 0;
                if (o1.getCountry().getCode() != 0)
                    if (o2.getCountry().getCode() != 0)
                        return o1.getCountry().getTitle().compareToIgnoreCase(o2.getCountry().getTitle());

                if (o1.getCountry().getCode() != 0)
                    return -10000;
                if (o2.getCountry().getCode() != 0)
                    return 10000;

                return result + o1.getTitleWithType().compareToIgnoreCase(o2.getTitleWithType());
            }
        };

        Comparator<AddressItem> itemComparator = new Comparator<AddressItem>() {
            @Override
            public int compare(AddressItem o1, AddressItem o2) {
                if (o1==null)
                    if (o2==null)
                        return 0;
                    else
                        return 1000;
                else if (o2 == null)
                    return -1000;
                return o1.getTitleWithType().compareToIgnoreCase(o2.getTitleWithType());
            }
        };

        // Достаем из дока повторяющие надписи и таблицы
        RtfElement countryRegion = (RtfElement)UniRtfUtil.findElement(document.getElementList(), "countryRegion");
        RtfElement programSubject = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "programSubject");
        RtfTable tTableElement = (RtfTable)UniRtfUtil.findElement(document.getElementList(), "T");

        // И удаляем их
        document.getElementList().remove(countryRegion);
        document.getElementList().remove(programSubject);
        document.getElementList().remove(tTableElement);

        // print formDate
        new RtfInjectModifier().put("formDate", new DateFormatter(DateFormatter.PATTERN_DEFAULT).format(new Date())).modify(document);


        // Карта с данными Регион(Страна) - Нас.пункт - Набор выбранных конурсов
        Map<AddressItem, Map<AddressItem, Set<EnrRequestedCompetition>>> residenceMap = new TreeMap<>(parentItemComparator);

        for (EnrRequestedCompetition reqComp : reqCompList)
        {

            AddressBase address = reqComp.getRequest().getEntrant().getPerson().getIdentityCard().getAddress();
            if (address instanceof AddressDetailed) {

                AddressItem settlement = ((AddressDetailed)reqComp.getRequest().getEntrant().getPerson().getIdentityCard().getAddress()).getSettlement();

                AddressItem region = settlement;
                while (region != null)
                {
                    if (region.getParent() != null)
                        region = region.getParent();
                    else break;
                }

                if (settlement == null) {
                    if (reqComp.getRequest().getIdentityCard().getCitizenship().getCode() != 0 && reqComp.getRequest().getIdentityCard().getCitizenship().getCode() != -1)
                    {
                        region = new AddressItem();
                        region.setCountry((AddressCountry)reqComp.getRequest().getIdentityCard().getCitizenship());
                    }
                }

                Map<AddressItem, Set<EnrRequestedCompetition>> addressItemSetMap = SafeMap.safeGet(residenceMap, region, TreeMap.class, itemComparator);
                Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(addressItemSetMap, settlement, HashSet.class);
                requestedCompetitionSet.add(reqComp);
            }
            else {
                if (reqComp.getRequest().getIdentityCard().getCitizenship().getCode() != 0 && reqComp.getRequest().getIdentityCard().getCitizenship().getCode() != -1)
                {
                    AddressItem region = new AddressItem();
                    region.setCountry((AddressCountry)reqComp.getRequest().getIdentityCard().getCitizenship());
                    Map<AddressItem, Set<EnrRequestedCompetition>> addressItemSetMap = SafeMap.safeGet(residenceMap, region, TreeMap.class, itemComparator);
                    Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(addressItemSetMap, null, HashSet.class);
                    requestedCompetitionSet.add(reqComp);
                }
                else {
                    Map<AddressItem, Set<EnrRequestedCompetition>> addressItemSetMap = SafeMap.safeGet(residenceMap, null, TreeMap.class, itemComparator);
                    Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(addressItemSetMap, null, HashSet.class);
                    requestedCompetitionSet.add(reqComp);
                }
            }
        }

        int totalCount = 0;
        int totalBudget = 0;
        int totalCommercial = 0;
        List<String[]> tTable = new ArrayList<>();
        List<PairKey<String, Integer[]>> tValues = new ArrayList<>();

        NumberFormat formatter = new DecimalFormat("#0.00");

        // Заполняем основную таблицу
        for (Map.Entry<AddressItem, Map<AddressItem, Set<EnrRequestedCompetition>>> regionEntry : residenceMap.entrySet())
        {
            Set<EnrEntrantRequest> budgetRequests = new HashSet<>();
            Set<EnrEntrantRequest> commercialRequests = new HashSet<>();

            for (Map.Entry<AddressItem, Set<EnrRequestedCompetition>> settlementEntry : regionEntry.getValue().entrySet())
            {
                for (EnrRequestedCompetition reqComp : settlementEntry.getValue())
                {
                    if (reqComp.getCompetition().getType().getCompensationType().isBudget())
                        budgetRequests.add(reqComp.getRequest());
                    else commercialRequests.add(reqComp.getRequest());
                }
            }

            StringBuilder regionName = new StringBuilder();
            if (regionEntry.getKey() == null)
                regionName.append("Адрес не указан");
            else regionName.append(regionEntry.getKey().getCountry().getCode() == 0 ? regionEntry.getKey().getTitleWithType() : regionEntry.getKey().getCountry().getTitle());

            tValues.add(new PairKey<String, Integer[]>(
                    regionName.toString(),
                    new Integer[]{
                            budgetRequests.size(),
                            commercialRequests.size(),
                            budgetRequests.size() + commercialRequests.size()
                    }
            ));

            totalBudget += budgetRequests.size();
            totalCommercial += commercialRequests.size();
            totalCount += budgetRequests.size() + commercialRequests.size();
        }

        for (PairKey<String, Integer[]> pairKey : tValues)
        {
            tTable.add(new String[]{
                    pairKey.getFirst(),
                    String.valueOf(pairKey.getSecond()[0]),
                    String.valueOf(pairKey.getSecond()[1]),
                    String.valueOf(pairKey.getSecond()[2]),
                    formatter.format(percentage(pairKey.getSecond()[2], totalCount)) + "%",
            });
        }
        tTable.add(new String[]{
                "ИТОГО:",
                String.valueOf(totalBudget),
                String.valueOf(totalCommercial),
                String.valueOf(totalCount),
                "100%"
        });

        document.getElementList().add(tTableElement.getClone());
        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("T", tTable.toArray(new String[tTable.size()][]));
        modifier.put("T", makeLastCellBoldIntercepter);
        // Н-таблица
        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        hTable.add(0, new String[]{
                "Стадия приемной кампании",
                model.getStageSelector().getStage().getTitle()
        });
        if (model.getParallelSelector().isParallelActive())
            hTable.add(new String[]{
                    "Поступающие параллельно",
                    model.getParallelSelector().getParallel().getTitle()
            });

        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
        modifier.put("H", new RtfRowIntercepterRawText());
        modifier.modify(document);


        // Детализация по регионам
        if (model.isSettlementDetalization()) {
            for (Map.Entry<AddressItem, Map<AddressItem, Set<EnrRequestedCompetition>>> regionEntry : residenceMap.entrySet()) {
                if (regionEntry.getKey() == null)
                    continue;
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(programSubject.getClone());

                totalBudget = 0;
                totalCommercial = 0;

                int totalLocal = 0;
                float totalPercent = 0;

                List<String[]> t1Table = new ArrayList<>();
                document.getElementList().add(tTableElement.getClone());
                new RtfInjectModifier().put("programSubject", "Детализация по населенному пункту: " + (regionEntry.getKey().getCountry().getCode() == 0 ? regionEntry.getKey().getTitleWithType() : regionEntry.getKey().getCountry().getTitle())).modify(document);

                for (Map.Entry<AddressItem, Set<EnrRequestedCompetition>> settlementEntry : regionEntry.getValue().entrySet()) {
                    Set<EnrEntrantRequest> budgetRequests = new HashSet<>();
                    Set<EnrEntrantRequest> commercialRequests = new HashSet<>();

                    for (EnrRequestedCompetition reqComp : settlementEntry.getValue()) {
                        if (reqComp.getCompetition().getType().getCompensationType().isBudget())
                            budgetRequests.add(reqComp.getRequest());
                        else commercialRequests.add(reqComp.getRequest());
                    }

                    int summ = budgetRequests.size() + commercialRequests.size();
                    float percent = percentage(summ, totalCount);



                    t1Table.add(new String[]{
                            settlementEntry.getKey() == null ? "Адрес не указан" : settlementEntry.getKey().getTitleWithType(),
                            String.valueOf(budgetRequests.size()),
                            String.valueOf(commercialRequests.size()),
                            String.valueOf(summ),
                            formatter.format(percent) + "%"
                    });

                    totalBudget += budgetRequests.size();
                    totalCommercial += commercialRequests.size();
                    totalLocal += summ;
                    totalPercent += percent;
                }

                t1Table.add(new String[]{
                        "ИТОГО:",
                        String.valueOf(totalBudget),
                        String.valueOf(totalCommercial),
                        String.valueOf(totalLocal),
                        formatter.format(totalPercent)+ "%"
                });

                RtfTableModifier t1modifier = new RtfTableModifier();
                t1modifier.put("T", t1Table.toArray(new String[t1Table.size()][]));
                t1modifier.put("T", makeLastCellBoldIntercepter);
                t1modifier.modify(document);

            }
        }

        final Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                return o1.getSubjectCode().compareToIgnoreCase(o2.getSubjectCode());
            }
        };

        if (model.isSplitByProgramSubjects())
        {
            Map<EduProgramSubject, Map<AddressItem, Set<EnrRequestedCompetition>>> subjectMap = new TreeMap<>(subjectComparator);

            for (Map.Entry<AddressItem, Map<AddressItem, Set<EnrRequestedCompetition>>> regionEntry : residenceMap.entrySet())
                for (Map.Entry<AddressItem, Set<EnrRequestedCompetition>> settlementEntry : regionEntry.getValue().entrySet())
                    for (EnrRequestedCompetition reqComp : settlementEntry.getValue()) {

                        AddressItem region = regionEntry.getKey();
                        if (region != null && region.getCountry().getCode()!=0)
                        {
                            if (settlementEntry.getKey() != null)
                                region = settlementEntry.getKey();
                        }
                        EduProgramSubject subject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
                        if (!subjectMap.containsKey(subject))
                            subjectMap.put(subject, new TreeMap<AddressItem, Set<EnrRequestedCompetition>>(parentItemComparator));
                        if (!subjectMap.get(subject).containsKey(region))
                            subjectMap.get(subject).put(region, new HashSet<EnrRequestedCompetition>());
                        subjectMap.get(subject).get(region).add(reqComp);
                    }

            for (Map.Entry<EduProgramSubject, Map<AddressItem, Set<EnrRequestedCompetition>>> subjectEntry : subjectMap.entrySet())
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(programSubject.getClone());

                List<String[]> t2Table = new ArrayList<>();
                List<PairKey<String, Integer[]>> t2FormingContent = new ArrayList<>();
                document.getElementList().add(tTableElement.getClone());
                new RtfInjectModifier().put("programSubject", subjectEntry.getKey().getTitleWithCode()).modify(document);
                totalBudget = 0;
                totalCommercial = 0;

                int totalLocal = 0;

                for (Map.Entry<AddressItem, Set<EnrRequestedCompetition>> settlementEntry : subjectEntry.getValue().entrySet())
                {
                    Set<EnrEntrantRequest> budgetRequests = new HashSet<>();
                    Set<EnrEntrantRequest> commercialRequests = new HashSet<>();


                    for (EnrRequestedCompetition reqComp : settlementEntry.getValue())
                    {
                        if (reqComp.getCompetition().getType().getCompensationType().isBudget())
                            budgetRequests.add(reqComp.getRequest());
                        else commercialRequests.add(reqComp.getRequest());
                    }
                    StringBuilder builder = new StringBuilder();

                    if (settlementEntry.getKey() != null && settlementEntry.getKey().getCountry().getCode() != 0) {
                        builder.append(settlementEntry.getKey().getCountry().getTitle());
                        builder.append(", ");
                    }
                    if (settlementEntry.getKey() == null)
                        builder.append("Россия, адрес не указан");
                    else if (settlementEntry.getKey().getTitleWithType() == null || settlementEntry.getKey().getTitleWithType().isEmpty())
                        builder.append("адрес не указан");
                    else builder.append(settlementEntry.getKey().getTitleWithType());

                    int summ = budgetRequests.size() + commercialRequests.size();
                    t2FormingContent.add(new PairKey<String, Integer[]>(
                            builder.toString(),
                            new Integer[]{
                                    (budgetRequests.size()),
                                    (commercialRequests.size()),
                                    (summ),
                            }
                    ));


                    totalBudget += budgetRequests.size();
                    totalCommercial += commercialRequests.size();
                    totalLocal += summ;
                }
                for (PairKey<String, Integer[]> pair : t2FormingContent)
                {
                    t2Table.add(new String[]{
                            pair.getFirst(),
                            String.valueOf(pair.getSecond()[0]),
                            String.valueOf(pair.getSecond()[1]),
                            String.valueOf(pair.getSecond()[2]),
                            formatter.format(percentage(pair.getSecond()[2], totalLocal)) + "%"
                    });
                }

                t2Table.add(new String[]{
                        "ИТОГО:",
                        String.valueOf(totalBudget),
                        String.valueOf(totalCommercial),
                        String.valueOf(totalLocal),
                        "100%"
                });

                RtfTableModifier t2modifier = new RtfTableModifier();
                t2modifier.put("T", t2Table.toArray(new String[t2Table.size()][]));
                t2modifier.put("T", makeLastCellBoldIntercepter);
                t2modifier.modify(document);

            }


        }

        return RtfUtil.toByteArray(document);
    }



    // Делает надпись Итого болдовой
    IRtfRowIntercepter makeLastCellBoldIntercepter = new IRtfRowIntercepter() {
        @Override
        public void beforeModify(RtfTable table, int currentRowIndex) {

        }

        @Override
        public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {

            if (colIndex == 0 && value != null && value.equals("ИТОГО:"))
            {
                RtfString string = new RtfString();
                string
                        .boldBegin()
                        .append(value)
                        .boldEnd();
                return string.toList();
            }

            return null;
        }

        @Override
        public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

        }
    };

    private static float percentage(int a, int b)
    {
        return (a*100.0f/b);
    }
}
