package ru.tandemservice.unienr14.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка по результатам ВИ по материалам ОУ или по ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrReportExamResultsByDifferentSourcesGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources";
    public static final String ENTITY_NAME = "enrReportExamResultsByDifferentSources";
    public static final int VERSION_HASH = -354395417;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_STAGE = "stage";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_COMPETITION_TYPE = "competitionType";
    public static final String P_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_PROGRAM_SET = "programSet";
    public static final String P_PARALLEL = "parallel";
    public static final String P_EXAM_RESULT_SOURCE = "examResultSource";
    public static final String P_GROUP_BY_ORG_UNITS = "groupByOrgUnits";
    public static final String P_SHOW_PROGRAM_SUBJECT_GROUPS = "showProgramSubjectGroups";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления добавлены с
    private Date _dateTo;     // Заявления добавлены по
    private String _stage;     // Стадия приемной кампании
    private String _requestType;     // Вид заявления
    private String _compensationType;     // Вид возмещения затрат
    private String _programForm;     // Форма обучения
    private String _competitionType;     // Вид приема
    private String _enrOrgUnit;     // Филиал
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _programSubject;     // Направление, спец., профессия
    private String _eduProgram;     // Образовательная программа
    private String _programSet;     // Набор образовательных программ
    private String _parallel;     // Поступающие параллельно
    private String _examResultSource;     // По результатам ВИ
    private String _groupByOrgUnits;     // Группировать по подразделениям
    private String _showProgramSubjectGroups;     // Выделять укрупненные группы направлений подготовки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления добавлены с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления добавлены по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStage()
    {
        return _stage;
    }

    /**
     * @param stage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setStage(String stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return Вид заявления.
     */
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Вид приема.
     */
    public String getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Вид приема.
     */
    public void setCompetitionType(String competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Филиал.
     */
    public String getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал.
     */
    public void setEnrOrgUnit(String enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, спец., профессия.
     */
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, спец., профессия.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Образовательная программа.
     */
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Набор образовательных программ.
     */
    public String getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор образовательных программ.
     */
    public void setProgramSet(String programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Поступающие параллельно.
     */
    @Length(max=255)
    public String getParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Поступающие параллельно.
     */
    public void setParallel(String parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    /**
     * @return По результатам ВИ.
     */
    @Length(max=255)
    public String getExamResultSource()
    {
        return _examResultSource;
    }

    /**
     * @param examResultSource По результатам ВИ.
     */
    public void setExamResultSource(String examResultSource)
    {
        dirty(_examResultSource, examResultSource);
        _examResultSource = examResultSource;
    }

    /**
     * @return Группировать по подразделениям.
     */
    @Length(max=255)
    public String getGroupByOrgUnits()
    {
        return _groupByOrgUnits;
    }

    /**
     * @param groupByOrgUnits Группировать по подразделениям.
     */
    public void setGroupByOrgUnits(String groupByOrgUnits)
    {
        dirty(_groupByOrgUnits, groupByOrgUnits);
        _groupByOrgUnits = groupByOrgUnits;
    }

    /**
     * @return Выделять укрупненные группы направлений подготовки.
     */
    @Length(max=255)
    public String getShowProgramSubjectGroups()
    {
        return _showProgramSubjectGroups;
    }

    /**
     * @param showProgramSubjectGroups Выделять укрупненные группы направлений подготовки.
     */
    public void setShowProgramSubjectGroups(String showProgramSubjectGroups)
    {
        dirty(_showProgramSubjectGroups, showProgramSubjectGroups);
        _showProgramSubjectGroups = showProgramSubjectGroups;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrReportExamResultsByDifferentSourcesGen)
        {
            setEnrollmentCampaign(((EnrReportExamResultsByDifferentSources)another).getEnrollmentCampaign());
            setDateFrom(((EnrReportExamResultsByDifferentSources)another).getDateFrom());
            setDateTo(((EnrReportExamResultsByDifferentSources)another).getDateTo());
            setStage(((EnrReportExamResultsByDifferentSources)another).getStage());
            setRequestType(((EnrReportExamResultsByDifferentSources)another).getRequestType());
            setCompensationType(((EnrReportExamResultsByDifferentSources)another).getCompensationType());
            setProgramForm(((EnrReportExamResultsByDifferentSources)another).getProgramForm());
            setCompetitionType(((EnrReportExamResultsByDifferentSources)another).getCompetitionType());
            setEnrOrgUnit(((EnrReportExamResultsByDifferentSources)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrReportExamResultsByDifferentSources)another).getFormativeOrgUnit());
            setProgramSubject(((EnrReportExamResultsByDifferentSources)another).getProgramSubject());
            setEduProgram(((EnrReportExamResultsByDifferentSources)another).getEduProgram());
            setProgramSet(((EnrReportExamResultsByDifferentSources)another).getProgramSet());
            setParallel(((EnrReportExamResultsByDifferentSources)another).getParallel());
            setExamResultSource(((EnrReportExamResultsByDifferentSources)another).getExamResultSource());
            setGroupByOrgUnits(((EnrReportExamResultsByDifferentSources)another).getGroupByOrgUnits());
            setShowProgramSubjectGroups(((EnrReportExamResultsByDifferentSources)another).getShowProgramSubjectGroups());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrReportExamResultsByDifferentSourcesGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrReportExamResultsByDifferentSources.class;
        }

        public T newInstance()
        {
            return (T) new EnrReportExamResultsByDifferentSources();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "stage":
                    return obj.getStage();
                case "requestType":
                    return obj.getRequestType();
                case "compensationType":
                    return obj.getCompensationType();
                case "programForm":
                    return obj.getProgramForm();
                case "competitionType":
                    return obj.getCompetitionType();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "eduProgram":
                    return obj.getEduProgram();
                case "programSet":
                    return obj.getProgramSet();
                case "parallel":
                    return obj.getParallel();
                case "examResultSource":
                    return obj.getExamResultSource();
                case "groupByOrgUnits":
                    return obj.getGroupByOrgUnits();
                case "showProgramSubjectGroups":
                    return obj.getShowProgramSubjectGroups();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "stage":
                    obj.setStage((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((String) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "programSet":
                    obj.setProgramSet((String) value);
                    return;
                case "parallel":
                    obj.setParallel((String) value);
                    return;
                case "examResultSource":
                    obj.setExamResultSource((String) value);
                    return;
                case "groupByOrgUnits":
                    obj.setGroupByOrgUnits((String) value);
                    return;
                case "showProgramSubjectGroups":
                    obj.setShowProgramSubjectGroups((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "stage":
                        return true;
                case "requestType":
                        return true;
                case "compensationType":
                        return true;
                case "programForm":
                        return true;
                case "competitionType":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "eduProgram":
                        return true;
                case "programSet":
                        return true;
                case "parallel":
                        return true;
                case "examResultSource":
                        return true;
                case "groupByOrgUnits":
                        return true;
                case "showProgramSubjectGroups":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "stage":
                    return true;
                case "requestType":
                    return true;
                case "compensationType":
                    return true;
                case "programForm":
                    return true;
                case "competitionType":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "eduProgram":
                    return true;
                case "programSet":
                    return true;
                case "parallel":
                    return true;
                case "examResultSource":
                    return true;
                case "groupByOrgUnits":
                    return true;
                case "showProgramSubjectGroups":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "stage":
                    return String.class;
                case "requestType":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "programForm":
                    return String.class;
                case "competitionType":
                    return String.class;
                case "enrOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "programSet":
                    return String.class;
                case "parallel":
                    return String.class;
                case "examResultSource":
                    return String.class;
                case "groupByOrgUnits":
                    return String.class;
                case "showProgramSubjectGroups":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrReportExamResultsByDifferentSources> _dslPath = new Path<EnrReportExamResultsByDifferentSources>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrReportExamResultsByDifferentSources");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getStage()
     */
    public static PropertyPath<String> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Вид приема.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getCompetitionType()
     */
    public static PropertyPath<String> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getEnrOrgUnit()
     */
    public static PropertyPath<String> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getProgramSet()
     */
    public static PropertyPath<String> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getParallel()
     */
    public static PropertyPath<String> parallel()
    {
        return _dslPath.parallel();
    }

    /**
     * @return По результатам ВИ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getExamResultSource()
     */
    public static PropertyPath<String> examResultSource()
    {
        return _dslPath.examResultSource();
    }

    /**
     * @return Группировать по подразделениям.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getGroupByOrgUnits()
     */
    public static PropertyPath<String> groupByOrgUnits()
    {
        return _dslPath.groupByOrgUnits();
    }

    /**
     * @return Выделять укрупненные группы направлений подготовки.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getShowProgramSubjectGroups()
     */
    public static PropertyPath<String> showProgramSubjectGroups()
    {
        return _dslPath.showProgramSubjectGroups();
    }

    public static class Path<E extends EnrReportExamResultsByDifferentSources> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _stage;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _competitionType;
        private PropertyPath<String> _enrOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _programSet;
        private PropertyPath<String> _parallel;
        private PropertyPath<String> _examResultSource;
        private PropertyPath<String> _groupByOrgUnits;
        private PropertyPath<String> _showProgramSubjectGroups;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrReportExamResultsByDifferentSourcesGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrReportExamResultsByDifferentSourcesGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getStage()
     */
        public PropertyPath<String> stage()
        {
            if(_stage == null )
                _stage = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_STAGE, this);
            return _stage;
        }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Вид приема.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getCompetitionType()
     */
        public PropertyPath<String> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getEnrOrgUnit()
     */
        public PropertyPath<String> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getProgramSet()
     */
        public PropertyPath<String> programSet()
        {
            if(_programSet == null )
                _programSet = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getParallel()
     */
        public PropertyPath<String> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_PARALLEL, this);
            return _parallel;
        }

    /**
     * @return По результатам ВИ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getExamResultSource()
     */
        public PropertyPath<String> examResultSource()
        {
            if(_examResultSource == null )
                _examResultSource = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_EXAM_RESULT_SOURCE, this);
            return _examResultSource;
        }

    /**
     * @return Группировать по подразделениям.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getGroupByOrgUnits()
     */
        public PropertyPath<String> groupByOrgUnits()
        {
            if(_groupByOrgUnits == null )
                _groupByOrgUnits = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_GROUP_BY_ORG_UNITS, this);
            return _groupByOrgUnits;
        }

    /**
     * @return Выделять укрупненные группы направлений подготовки.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportExamResultsByDifferentSources#getShowProgramSubjectGroups()
     */
        public PropertyPath<String> showProgramSubjectGroups()
        {
            if(_showProgramSubjectGroups == null )
                _showProgramSubjectGroups = new PropertyPath<String>(EnrReportExamResultsByDifferentSourcesGen.P_SHOW_PROGRAM_SUBJECT_GROUPS, this);
            return _showProgramSubjectGroups;
        }

        public Class getEntityClass()
        {
            return EnrReportExamResultsByDifferentSources.class;
        }

        public String getEntityName()
        {
            return "enrReportExamResultsByDifferentSources";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
