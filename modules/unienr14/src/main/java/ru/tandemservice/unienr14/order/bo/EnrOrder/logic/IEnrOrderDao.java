/* $Id: IEcOrderDao.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPubUI;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.IAbstractOrder;

import java.util.Date;
import java.util.List;

/**
 * API для работы с движением абитуриентов
 *
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public interface IEnrOrderDao extends INeedPersistenceSupport
{
    void deleteOrder(Long orderId);

    void deleteParagraph(Long paragraphId);

    void deleteExtract(EnrAbstractExtract extract);

    /**
     * Сохраняет печатную форму приказа о зачислении
     *
     * @param order приказ о зачислении
     */
    void saveEnrollmentOrderText(EnrOrder order);

    /**
     * Проверяет на уникальность номер приказа в рамках текущего календарного года
     *
     * @param order абитуриентский приказ
     */
    void checkOrderNumber(EnrAbstractOrder order);

    /**
     * Скачивает печатную форму приказа о зачислении абитуриентов
     *
     * @param enrollmentOrderId идентификатор приказа о зачислении
     */
    void getDownloadPrintForm(Long enrollmentOrderId);

    /**
     * @param enrollmentOrderReason причина для приказа о зачислении
     * @return список оснований для данной причины
     */
    List<EnrOrderBasic> getReasonToBasicsList(EnrOrderReason enrollmentOrderReason);

    EnrOrder saveOrUpdateOrder(EnrOrder order, Date createDate, boolean addForm, String executor);

    void doSendToCoordination(List<Long> selectedIds, IPersistentPersonable initiator);

    void doCommit(List<Long> selectedIds);

    void doSendToFormative(Long orderId);

    void doReject(EnrOrder order);

    void doRollback(EnrOrder order);

    void updateParagraphPriority(Long paragraphId, int direction);

    void doExecuteAcceptVisaOrderService(IAbstractOrder order);

    void doExecuteRejectVisaOrderService(IAbstractOrder order);

    void doCommit(EnrAbstractExtract extract);

    void doRollback(EnrAbstractExtract extract);

    String getDefaultOrderBasicText(String enrOrderTypeCode);

    void doCreateStudents(List<Long> selectedIds);
}