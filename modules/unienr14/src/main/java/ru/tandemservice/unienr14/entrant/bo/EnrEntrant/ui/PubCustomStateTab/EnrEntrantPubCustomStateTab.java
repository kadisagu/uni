/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubCustomStateTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.ui.ColorRowCustomizer;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantCustomStateSearchDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;

/**
 * @author oleyba
 * @since 6/11/14
 */
@Configuration
public class EnrEntrantPubCustomStateTab extends BusinessComponentManager
{
    public static final String CUSTOM_STATE_LIST_DS = "customStateDS";

    @Bean
    public ColumnListExtPoint customStateListCL()
    {
        return columnListExtPointBuilder(CUSTOM_STATE_LIST_DS)
                .addColumn(textColumn("title", EnrEntrantCustomState.customState().title()).required(true).order())
                .addColumn(dateColumn("beginDate", EnrEntrantCustomState.beginDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().width("250px"))
                .addColumn(dateColumn("endDate", EnrEntrantCustomState.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().width("250px"))
                .addColumn(textColumn("desc", EnrEntrantCustomState.description()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("enr14EntrantPubCustomStatesTabEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.customStatusDeleteAlert").permissionKey("enr14EntrantPubCustomStatesTabDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CUSTOM_STATE_LIST_DS, customStateListCL(), customStateListDSHandler())
                                       .rowCustomizer(new ColorRowCustomizer<>().coloredProperty(EnrEntrantCustomState.customState().s())))
                .addDataSource(selectDS("customStateTypeDS", EnrEntrantCustomStateCIComboDS()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler customStateListDSHandler()
    {
        return new EnrEntrantCustomStateSearchDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler EnrEntrantCustomStateCIComboDS()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrEntrantCustomStateType.class);
    }

}