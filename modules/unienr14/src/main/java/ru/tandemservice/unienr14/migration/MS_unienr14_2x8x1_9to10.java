package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOnlineEntrantStatement

		// удалено свойство takeEdu
		{
			// удалить колонку
			tool.dropColumn("enronlineentrantstatement_t", "takeedu_p");
		}

		// удалено свойство wantEdu
		{
			// удалить колонку
			tool.dropColumn("enronlineentrantstatement_t", "wantedu_p");

		}

		// создано свойство haveEduLevel
        if(!tool.columnExists("enronlineentrantstatement_t", "haveedulevel_p"))
		{
			// создать колонку
			tool.createColumn("enronlineentrantstatement_t", new DBColumn("haveedulevel_p", DBType.createVarchar(255)));

		}

		// создано свойство wantRequestType
        if(!tool.columnExists("enronlineentrantstatement_t", "wantrequesttype_p"))
		{
			// создать колонку
			tool.createColumn("enronlineentrantstatement_t", new DBColumn("wantrequesttype_p", DBType.createVarchar(255)));

		}


    }
}