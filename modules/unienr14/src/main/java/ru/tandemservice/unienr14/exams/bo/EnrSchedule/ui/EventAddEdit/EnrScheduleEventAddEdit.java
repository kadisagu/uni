/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.ui.EventAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;

/**
 * @author oleyba
 * @since 5/16/13
 */
@Configuration
public class EnrScheduleEventAddEdit extends BusinessComponentManager
{
    public static final String EXAM_ROOM_SELECT_DS = "examRoomSelectDS";
    public static final String EXAM_GROUP_SET_SELECT_DS = "examGroupSetSelectDS";
    public static final String EXAM_GROUP_SELECT_DS = "examGroupSelectDS";

    public static final String BIND_TERR_ORG_UNIT = "territorialOrgUnit";
    public static final String BIND_EXAM_GROUP_SET = "examGroupSet";
    public static final String BIND_DISCIPLINE = "campaignDiscipline";
    public static final String BIND_PASS_FORM = "passForm";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EXAM_ROOM_SELECT_DS, examRoomSelectDSHandler()).addColumn(EnrCampaignExamRoom.place().titleWithLocation().s()))
                .addDataSource(selectDS(EXAM_GROUP_SET_SELECT_DS, examGroupSetSelectDSHandler()).addColumn(EnrExamGroupSet.periodTitle().s()))
                .addDataSource(selectDS(EXAM_GROUP_SELECT_DS, examGroupSelectDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroup.class)
                .where(EnrExamGroup.examGroupSet(), BIND_EXAM_GROUP_SET)
                .where(EnrExamGroup.discipline(), BIND_DISCIPLINE)
                .where(EnrExamGroup.passForm(), BIND_PASS_FORM)
                .where(EnrExamGroup.territorialOrgUnit(), BIND_TERR_ORG_UNIT)
                .where(EnrExamGroup.days(), 1)
                .order(EnrExamGroup.title())
                .filter(EnrExamGroup.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSetSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroupSet.class)
                .where(EnrExamGroupSet.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .order(EnrExamGroupSet.beginDate())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examRoomSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignExamRoom.class)
                .where(EnrCampaignExamRoom.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .where(EnrCampaignExamRoom.responsibleOrgUnit(), BIND_TERR_ORG_UNIT)
                .order(EnrCampaignExamRoom.place().title())
                .filter(EnrCampaignExamRoom.place().title())
                .pageable(true);
    }

}
