/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.PlanEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;

/**
 * @author nvankov
 * @since 7/28/14
 */
@Input({
        @Bind(key=UIPresenter.PUBLISHER_ID, binding="competition.id", required=true)
})
public class EnrCompetitionPlanEditUI extends UIPresenter
{
    private EnrCompetition _competition = new EnrCompetition();

    @Override
    public void onComponentRefresh()
    {
        _competition = DataAccessServices.dao().getNotNull(_competition.getId());
    }

    public void onClickApply()
    {
        DataAccessServices.dao().update(_competition);
        deactivate();
    }

    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    public void setCompetition(EnrCompetition competition)
    {
        _competition = competition;
    }
}
