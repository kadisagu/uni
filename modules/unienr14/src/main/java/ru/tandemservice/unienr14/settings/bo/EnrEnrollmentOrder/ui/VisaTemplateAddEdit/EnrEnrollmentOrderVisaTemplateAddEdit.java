/**
 *$Id: EnrEnrollmentOrderVisaTemplateAddEdit.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.EnrEnrollmentOrderManager;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;

import java.util.List;
import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
@Configuration
public class EnrEnrollmentOrderVisaTemplateAddEdit extends BusinessComponentManager
{
    public static final String GROUP_SELECT_DS = "groupSelectDS";
    public static final String POSSIBLE_VISA_SELECT_DS = "possibleVisaSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        final IFormatter<IPossibleVisa> visaSelectDSOrgUnitFormatter = new IFormatter<IPossibleVisa>()
        {
            @Override
            public String format(IPossibleVisa source)
            {
                if (source instanceof EmployeePostPossibleVisa)
                    return ((EmployeePostPossibleVisa) source).getEntity().getOrgUnit().getExtendedShortTitle2();
                else if (source instanceof StudentPossibleVisa)
                    return EnrEnrollmentOrderManager.instance().getProperty("visaTemplateAddEdit.visaSelectDS.orgUnit.student", ((StudentPossibleVisa) source).getEntity().getEducationOrgUnit().getFormativeOrgUnit().getExtendedShortTitle2(), ((StudentPossibleVisa) source).getEntity().getCourse().getTitle());
                else
                    return "";
            }
        };

        return presenterExtPointBuilder()
                .addDataSource(selectDS(GROUP_SELECT_DS, groupSelectDSHandler()))
                .addDataSource(selectDS(POSSIBLE_VISA_SELECT_DS, visaSelectDSHandler())
                        .addColumn("visaSelectDS.fullFio", IPossibleVisaGen.entity().person().fullFio().s())
                        .addColumn("visaSelectDS.title", IPossibleVisaGen.title().s())
                        .addColumn("visaSelectDS.personOwnerTitle", IPossibleVisaGen.PERSON_OWNER_TITLE)
                        .addColumn("visaSelectDS.orgUnit", null, visaSelectDSOrgUnitFormatter))
                        .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> visaSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), IPossibleVisa.class)
        {
            @Override
            @SuppressWarnings("unchecked")
            protected List<IEntity> sortSelectedValues(List<IEntity> list, Set primaryKeys)
            {
                return UniBaseDao.sort(list, primaryKeys);
            }
        }
                .order(IPossibleVisaGen.title())
                .filter(IPossibleVisaGen.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> groupSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), GroupsMemberVising.class)
                .order(GroupsMemberVising.title())
                .filter(GroupsMemberVising.title())
                .pageable(true);
    }
}
