package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Конфигурация для скриптовой печати модуля «(Новый) Абитуриент»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrScriptItemGen extends ScriptItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrScriptItem";
    public static final String ENTITY_NAME = "enrScriptItem";
    public static final int VERSION_HASH = 678448072;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRINT_PDF = "printPdf";
    public static final String P_PRINTABLE_TO_PDF = "printableToPdf";

    private boolean _printPdf = false;     // Печатать в pdf
    private boolean _printableToPdf = false;     // Предусмотрена печать в pdf

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатать в pdf. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintPdf()
    {
        return _printPdf;
    }

    /**
     * @param printPdf Печатать в pdf. Свойство не может быть null.
     */
    public void setPrintPdf(boolean printPdf)
    {
        dirty(_printPdf, printPdf);
        _printPdf = printPdf;
    }

    /**
     * @return Предусмотрена печать в pdf. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintableToPdf()
    {
        return _printableToPdf;
    }

    /**
     * @param printableToPdf Предусмотрена печать в pdf. Свойство не может быть null.
     */
    public void setPrintableToPdf(boolean printableToPdf)
    {
        dirty(_printableToPdf, printableToPdf);
        _printableToPdf = printableToPdf;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrScriptItemGen)
        {
            setPrintPdf(((EnrScriptItem)another).isPrintPdf());
            setPrintableToPdf(((EnrScriptItem)another).isPrintableToPdf());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrScriptItemGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrScriptItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrScriptItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "printPdf":
                    return obj.isPrintPdf();
                case "printableToPdf":
                    return obj.isPrintableToPdf();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "printPdf":
                    obj.setPrintPdf((Boolean) value);
                    return;
                case "printableToPdf":
                    obj.setPrintableToPdf((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "printPdf":
                        return true;
                case "printableToPdf":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "printPdf":
                    return true;
                case "printableToPdf":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "printPdf":
                    return Boolean.class;
                case "printableToPdf":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrScriptItem> _dslPath = new Path<EnrScriptItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrScriptItem");
    }
            

    /**
     * @return Печатать в pdf. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrScriptItem#isPrintPdf()
     */
    public static PropertyPath<Boolean> printPdf()
    {
        return _dslPath.printPdf();
    }

    /**
     * @return Предусмотрена печать в pdf. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrScriptItem#isPrintableToPdf()
     */
    public static PropertyPath<Boolean> printableToPdf()
    {
        return _dslPath.printableToPdf();
    }

    public static class Path<E extends EnrScriptItem> extends ScriptItem.Path<E>
    {
        private PropertyPath<Boolean> _printPdf;
        private PropertyPath<Boolean> _printableToPdf;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатать в pdf. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrScriptItem#isPrintPdf()
     */
        public PropertyPath<Boolean> printPdf()
        {
            if(_printPdf == null )
                _printPdf = new PropertyPath<Boolean>(EnrScriptItemGen.P_PRINT_PDF, this);
            return _printPdf;
        }

    /**
     * @return Предусмотрена печать в pdf. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrScriptItem#isPrintableToPdf()
     */
        public PropertyPath<Boolean> printableToPdf()
        {
            if(_printableToPdf == null )
                _printableToPdf = new PropertyPath<Boolean>(EnrScriptItemGen.P_PRINTABLE_TO_PDF, this);
            return _printableToPdf;
        }

        public Class getEntityClass()
        {
            return EnrScriptItem.class;
        }

        public String getEntityName()
        {
            return "enrScriptItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
