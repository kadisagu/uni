/**
 *$Id: EnrExamRoomManager.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamRoom.logic.EnrExamRoomDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamRoom.logic.IEnrExamRoomDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.05.13
 */
@Configuration
public class EnrExamRoomManager extends BusinessObjectManager
{
    public static EnrExamRoomManager instance()
    {
        return instance(EnrExamRoomManager.class);
    }

    @Bean
    public IEnrExamRoomDao dao()
    {
        return new EnrExamRoomDao();
    }

    /**
     * Территория - подразделения из всех, являющимихся терр. подр. в НП выбранной ПК.
     */
    @Bean
    public IBusinessHandler<DSInput, DSOutput> terrOrgUnitSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

                final DQLSelectBuilder terrOrgUnitIdsBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, "d")
                    .column(property(EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().id().fromAlias("d")))
                    .where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("d")), value(enrCamp)));

                dql.where(in(property(OrgUnit.id().fromAlias(alias)), terrOrgUnitIdsBuilder.buildQuery()));
            }
        }
            .order(OrgUnit.territorialFullTitle())
            .filter(OrgUnit.territorialFullTitle())
            .pageable(true);
    }
}
