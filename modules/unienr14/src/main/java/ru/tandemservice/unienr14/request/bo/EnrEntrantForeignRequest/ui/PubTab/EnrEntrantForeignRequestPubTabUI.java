/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.PubTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.AddEdit.EnrEntrantForeignRequestAddEdit;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;

/**
 * @author nvankov
 * @since 7/15/14
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantForeignRequestPubTabUI extends UIPresenter
{
    private EnrEntrant _entrant = new EnrEntrant();
    private EnrEntrantForeignRequest _request;

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    @Override
    public void onComponentRefresh()
    {
        _entrant = DataAccessServices.dao().get(_entrant.getId());
        _request = DataAccessServices.dao().getByNaturalId(new EnrEntrantForeignRequest.NaturalId(_entrant));
    }

    public void onClickEdit()
    {
        _uiActivation.asRegion(EnrEntrantForeignRequestAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, _request.getId())
                .activate();
    }
}
