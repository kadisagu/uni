package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

/**
 * Причины пропуска абитуриента в шаге зачисления
 * @author vdanilov
 */
public abstract class EnrSelectionSkipReason {

    /** исключен из обработки, т.к. перезачислен на другой конкурс (этого быть не может) */
    public static class OrderInfoSkipReenrollSolution extends EnrSelectionSkipReason {
        private final IEnrSelectionItemPrevEnrollmentInfo prevEnrollmentInfo;
        private final EnrSelectionItem reenrollTo;
        public OrderInfoSkipReenrollSolution(IEnrSelectionItemPrevEnrollmentInfo prevEnrollmentInfo, EnrSelectionItem reenrollTo) {
            this.prevEnrollmentInfo = prevEnrollmentInfo;
            this.reenrollTo = reenrollTo;
        }
        @Override protected String toInfoString() { return "Абитуриент будет перевыбран ("+this.prevEnrollmentInfo.getInfo()+") на: " + this.reenrollTo.getCompetitionTitle(); }
    }

    /** исключен из обработки, т.к. при разрешении конфликта указано «пропустить» */
    public static class OrderInfoSkipConflictSolution extends EnrSelectionSkipReason {
        private final IEnrSelectionItemPrevEnrollmentInfo orderInfo;
        public OrderInfoSkipConflictSolution(final IEnrSelectionItemPrevEnrollmentInfo orderInfo) {
            this.orderInfo = orderInfo;
        }
        @Override protected String toInfoString() { return "Абитуриент уже зачислен (резолюция - пропустить): " + this.orderInfo.getInfo(); }
    }

    /** исключен из обработки, т.к. поле «включен» в абитуриенте шага зачисления установлено в «нет» */
    public static class NotIncluded extends EnrSelectionSkipReason {
        @Override protected String toInfoString() { return "Абитуриент не включен в шаг зачисления"; }
    }

    /** исключен из обработки т.к. забрал документы */
    public static class TakeAwayDocument extends EnrSelectionSkipReason {
        @Override protected String toInfoString() { return "Абитуриент забрал документы"; }
    }

    /** исключен из обработки т.к. абитуриент был агхивирован */
    public static class Archival extends EnrSelectionSkipReason {
        @Override protected String toInfoString() { return "Абитуриент в архиве"; }
    }

    /** исключен из обработки т.к. будет выбран с конкурсом с более высоким абитуриентом */
    public static class ToBeSelectedWithHigherPriority extends EnrSelectionSkipReason {
        private final EnrSelectionItem reselectTo;
        public ToBeSelectedWithHigherPriority(EnrSelectionItem reselectTo) { this.reselectTo = reselectTo; }
        @Override protected String toInfoString() { return "Абитуриент будет выбран с более высоким приоритетом: " + this.reselectTo.getCompetitionTitle(); }
    }

    /** исключен из обработки т.к. уже выбран конкурс с более высоким абитуриентом */
    public static class AlreadySelectedWithHigherPriority extends EnrSelectionSkipReason {
        private final EnrSelectionItem selectedItem;
        public AlreadySelectedWithHigherPriority(EnrSelectionItem selectedItem) { this.selectedItem = selectedItem; }
        @Override protected String toInfoString() { return "Абитуриент уже выбран с более высоким приоритетом: " + this.selectedItem.getCompetitionTitle(); }

    }

    private EnrSelectionSkipReason next;
    public EnrSelectionSkipReason then(final EnrSelectionSkipReason next) {
        if (null == next) { return this; }
        if (null == this.next) { this.next = next; }
        else { this.next = this.next.then(next); }
        return this;
    }

    protected String toInfoString() {
        return getClass().getSimpleName();
    }

    public String getInfo() {
        final StringBuilder sb = new StringBuilder(this.toInfoString());
        if (null != this.next) {
            sb.append("; ").append(this.next.getInfo());
        }
        return sb.toString();
    }


}
