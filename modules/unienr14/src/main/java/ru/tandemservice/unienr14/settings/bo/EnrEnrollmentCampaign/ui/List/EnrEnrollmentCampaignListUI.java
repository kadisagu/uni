/**
 *$Id: EnrEnrollmentCampaignListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit.EnrEnrollmentCampaignAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.CopyForm.EnrEnrollmentCampaignCopyForm;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 08.04.13
 */
@Input({
        @Bind(key = EnrEnrollmentCampaignListUI.BIND_AFTER_COPY_INFO, binding = "afterCopyInformation")
})
public class EnrEnrollmentCampaignListUI extends UIPresenter
{
    public static final String BIND_AFTER_COPY_INFO = "copyInfo";
    public static final String PROP_EDU_YEAR = "eduYear";

    private String _afterCopyInformation;
    private boolean _afterCopyInformationIsShowed = false;

    @Override
    public void onComponentRefresh()
    {
        if (_afterCopyInformationIsShowed)
            setAfterCopyInformation(null);

        _afterCopyInformationIsShowed = getAfterCopyInformation() != null;
    }

    public void onClickAddEnrollmentCampaign()
    {
        getActivationBuilder().asCurrent(EnrEnrollmentCampaignAddEdit.class)
                .activate();
    }

    public void onClickEditEnrollmentCampaign()
    {
        getActivationBuilder().asRegion(EnrEnrollmentCampaignAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().deleteCampaign(DataAccessServices.dao().<EnrEnrollmentCampaign>get(getListenerParameterAsLong()));
    }

    public void onToggleOpen()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().doChangeOpen(getListenerParameterAsLong());
    }

    public void onClickCopyEnrollmentCampaign()
    {
        setAfterCopyInformation(null);
        getActivationBuilder().asRegionDialog(EnrEnrollmentCampaignCopyForm.class)
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        Object eduYear = getSettings().get(PROP_EDU_YEAR);
        if (null != eduYear)
            dataSource.put(PROP_EDU_YEAR, eduYear);
    }

    public String getAfterCopyInformation()
    {
        return _afterCopyInformation;
    }

    public void setAfterCopyInformation(String afterCopyInformation)
    {
        if (afterCopyInformation == null)
            _afterCopyInformationIsShowed = false;

        _afterCopyInformation = afterCopyInformation;
    }
}
