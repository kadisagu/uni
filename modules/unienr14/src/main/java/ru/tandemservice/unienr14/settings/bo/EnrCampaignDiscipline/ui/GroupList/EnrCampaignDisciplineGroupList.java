/**
 *$Id: EnrCampaignDisciplineGroupList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.GroupList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic.EnrCampDisciplineGroupSearchDSHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
@Configuration
public class EnrCampaignDisciplineGroupList extends BusinessComponentManager
{
    public static final String CAMP_DISCIPLINE_GROUP_SEARCH_DS = "campDisciplineGroupDS";
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        .addDataSource(searchListDS(CAMP_DISCIPLINE_GROUP_SEARCH_DS, campDisciplineGroupDSColumns(), campDisciplineGroupDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint campDisciplineGroupDSColumns()
    {
        IColumnListExtPointBuilder columns = columnListExtPointBuilder(CAMP_DISCIPLINE_GROUP_SEARCH_DS)
        .addColumn(textColumn("title", EnrCampaignDisciplineGroup.title()).order())
        .addColumn(textColumn("shortTitle", EnrCampaignDisciplineGroup.shortTitle()))
        .addColumn(textColumn("disciplines", "disciplines").formatter( CollectionFormatter.ROW_FORMATTER));

        if (Debug.isDisplay()) {
            columns.addColumn(textColumn("defaultPassMark", EnrCampaignDisciplineGroup.defaultPassMarkAsLong()).formatter(EnrEntrantManager.DEFAULT_MARK_FORMATTER));
        }

        return columns
        .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditCompDisciplineGroup"))
        .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteCompDisciplineGroup", new FormattedMessage("campDisciplineGroupDS.delete.alert", EnrCampaignDisciplineGroup.title().s())))
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> campDisciplineGroupDSHandler()
    {
        return new EnrCampDisciplineGroupSearchDSHandler(getName());
    }
}
