/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public class EnrReportStageSelector {

    private IdentifiableWrapper stage;
    private List<IdentifiableWrapper> stageList;

    private boolean stageActive;

    public EnrReportStageSelector() {
        setStageList(Arrays.asList(new IdentifiableWrapper(6L, "По ходу приема документов"),
                                   new IdentifiableWrapper(4L, "По результатам сдачи ВИ"),
                                   new IdentifiableWrapper(2L, "По результатам зачисления")));
    }

    public IdentifiableWrapper getStage() {
        return stage;
    }

    public void setStage(IdentifiableWrapper stage) {
        this.stage = stage;
    }

    public List<IdentifiableWrapper> getStageList() {
        return stageList;
    }

    public void setStageList(List<IdentifiableWrapper> stageList) {
        this.stageList = stageList;
    }

    public boolean isStageActive() {
        return stageActive;
    }

    public void setStageActive(boolean stageActive) {
        this.stageActive = stageActive;
    }

    /** Применяет фильтр по стадии приемной кампании.
     * @param alias алиас выбранного конкурса (по умолчанию в фильтре "reqComp")
    * */
    public DQLSelectBuilder applyFilter(EnrEntrantDQLSelectBuilder builder, String alias)
    {
        switch (stage.getId().intValue())
        {
            case (6) : builder
                            .where(and(ne(property(alias, EnrRequestedCompetition.state().priority()), value(8)), ne(property(alias, EnrRequestedCompetition.state().priority()), value(11)))); break;
            case (4) : builder
                            .where(and(le(property(alias, EnrRequestedCompetition.state().priority()), value(10)), or(le(property(alias, EnrRequestedCompetition.state().priority()), value(5)), ge(property(alias, EnrRequestedCompetition.state().priority()), value(9))))); break;
            case (2) : builder
                            .where(eq(property(alias, EnrRequestedCompetition.state().priority()), value(1))); break;
        }
        return builder;
    }

    /** селектор, в котором выбрано положение "По ходу приема документов"
     * */
    public static EnrReportStageSelector docAcceptanceStageFilter()
    {
        EnrReportStageSelector selector = new EnrReportStageSelector();
        selector.setStage(new IdentifiableWrapper(6L, "По ходу приема документов"));
        return selector;
    }

    /** селектор, в котором выбрано положение "По результатам сдачи ВИ"
     * */
    public static EnrReportStageSelector examPassResultsStageFilter()
    {
        EnrReportStageSelector selector = new EnrReportStageSelector();
        selector.setStage(new IdentifiableWrapper(4L, "По результатам сдачи ВИ"));
        return selector;
    }

    /** селектор, в котором выбрано положение "По результатам зачисления"
     * */
    public static EnrReportStageSelector enrollmentResultsStageFilter()
    {
        EnrReportStageSelector selector = new EnrReportStageSelector();
        selector.setStage(new IdentifiableWrapper(2L, "По результатам зачисления"));
        return selector;
    }

    public static boolean enrollmentResultsCheckByPriority(int priority)
    {
        return priority == 1;
    }


    public static boolean examPassedCheckByPriority(int priority)
    {
        return priority != 11 && (priority < 6 || priority > 8);
    }


    public static boolean docAcceptanceCheckByPriority(int priority)
    {
        return priority != 11 && priority!=8;
    }


}
