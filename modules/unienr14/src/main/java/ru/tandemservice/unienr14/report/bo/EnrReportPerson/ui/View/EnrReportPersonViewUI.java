/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import ru.tandemservice.unienr14.base.ext.ReportPerson.ui.Add.ReportPersonAddExt;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;

/**
 * @author oleyba
 * @since 5/12/14
 */

public class EnrReportPersonViewUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        getActivationBuilder().asDesktopRoot(ReportPersonAdd.class)
            .parameter(ReportPersonAddUI.TAB_LIST_VISIBLE, ReportPersonAddExt.ENTRANT_TAB)
            .parameter(ReportPersonAddUI.SCHEET_LIST_VISIBLE, EnrReportPersonAddUI.ENTRANT_SCHEET)
            .parameter(ReportPersonAddUI.SCHEET_LIST_VISIBLE, EnrReportPersonAddUI.ENTRANT_SCHEET)
            .parameter(ReportPersonAddUI.COMPONENT_TITLE, "Форма добавления отчета «Выборка абитуриентов»")
            .parameter(ReportPersonAddUI.PERSON_SCHEET, false)
            .activate();
    }

    // getters and setters
}