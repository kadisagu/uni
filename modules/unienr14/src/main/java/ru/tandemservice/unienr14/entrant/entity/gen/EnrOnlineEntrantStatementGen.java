package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявление онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantStatementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement";
    public static final String ENTITY_NAME = "enrOnlineEntrantStatement";
    public static final int VERSION_HASH = 216349675;
    private static IEntityMeta ENTITY_META;

    public static final String P_HAVE_EDU_LEVEL = "haveEduLevel";
    public static final String P_WANT_REQUEST_TYPE = "wantRequestType";
    public static final String P_EXCLUSIVE_RIGHTS = "exclusiveRights";
    public static final String P_EXCLUSIVE_RIGHTS_WITHOUT_EXAMS = "exclusiveRightsWithoutExams";
    public static final String P_EXCLUSIVE_RIGHTS_BUDGET = "exclusiveRightsBudget";
    public static final String P_EXCLUSIVE_RIGHTS_EMPTIVE_ENROLL = "exclusiveRightsEmptiveEnroll";
    public static final String P_HAVE_AGREEMENT_TARGET_EDU = "haveAgreementTargetEdu";
    public static final String P_AGREEMENT_NUMBER = "agreementNumber";
    public static final String P_AGREEMENT_DATE = "agreementDate";
    public static final String P_AGREEMENT_ORG = "agreementOrg";
    public static final String P_AGREEMENT_EDU_PROGRAMM = "agreementEduProgramm";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private String _haveEduLevel;     // Полученное образование (eduLevel)
    private String _wantRequestType;     // Программы (enrRequestType)
    private boolean _exclusiveRights = false;     // У меня есть особые права
    private boolean _exclusiveRightsWithoutExams = false;     // Право на прием без вступительных испытаний
    private boolean _exclusiveRightsBudget = false;     // Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты
    private boolean _exclusiveRightsEmptiveEnroll = false;     // Преимущественное право зачисления
    private boolean _haveAgreementTargetEdu = false;     // Заключен договор о целевом обучении
    private String _agreementNumber;     // Номер договора
    private Date _agreementDate;     // Дата заключения
    private String _agreementOrg;     // Данные об организации
    private String _agreementEduProgramm;     // Данные об образовательной программе
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Полученное образование (eduLevel).
     */
    @Length(max=255)
    public String getHaveEduLevel()
    {
        return _haveEduLevel;
    }

    /**
     * @param haveEduLevel Полученное образование (eduLevel).
     */
    public void setHaveEduLevel(String haveEduLevel)
    {
        dirty(_haveEduLevel, haveEduLevel);
        _haveEduLevel = haveEduLevel;
    }

    /**
     * @return Программы (enrRequestType).
     */
    @Length(max=255)
    public String getWantRequestType()
    {
        return _wantRequestType;
    }

    /**
     * @param wantRequestType Программы (enrRequestType).
     */
    public void setWantRequestType(String wantRequestType)
    {
        dirty(_wantRequestType, wantRequestType);
        _wantRequestType = wantRequestType;
    }

    /**
     * @return У меня есть особые права. Свойство не может быть null.
     */
    @NotNull
    public boolean isExclusiveRights()
    {
        return _exclusiveRights;
    }

    /**
     * @param exclusiveRights У меня есть особые права. Свойство не может быть null.
     */
    public void setExclusiveRights(boolean exclusiveRights)
    {
        dirty(_exclusiveRights, exclusiveRights);
        _exclusiveRights = exclusiveRights;
    }

    /**
     * @return Право на прием без вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public boolean isExclusiveRightsWithoutExams()
    {
        return _exclusiveRightsWithoutExams;
    }

    /**
     * @param exclusiveRightsWithoutExams Право на прием без вступительных испытаний. Свойство не может быть null.
     */
    public void setExclusiveRightsWithoutExams(boolean exclusiveRightsWithoutExams)
    {
        dirty(_exclusiveRightsWithoutExams, exclusiveRightsWithoutExams);
        _exclusiveRightsWithoutExams = exclusiveRightsWithoutExams;
    }

    /**
     * @return Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты. Свойство не может быть null.
     */
    @NotNull
    public boolean isExclusiveRightsBudget()
    {
        return _exclusiveRightsBudget;
    }

    /**
     * @param exclusiveRightsBudget Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты. Свойство не может быть null.
     */
    public void setExclusiveRightsBudget(boolean exclusiveRightsBudget)
    {
        dirty(_exclusiveRightsBudget, exclusiveRightsBudget);
        _exclusiveRightsBudget = exclusiveRightsBudget;
    }

    /**
     * @return Преимущественное право зачисления. Свойство не может быть null.
     */
    @NotNull
    public boolean isExclusiveRightsEmptiveEnroll()
    {
        return _exclusiveRightsEmptiveEnroll;
    }

    /**
     * @param exclusiveRightsEmptiveEnroll Преимущественное право зачисления. Свойство не может быть null.
     */
    public void setExclusiveRightsEmptiveEnroll(boolean exclusiveRightsEmptiveEnroll)
    {
        dirty(_exclusiveRightsEmptiveEnroll, exclusiveRightsEmptiveEnroll);
        _exclusiveRightsEmptiveEnroll = exclusiveRightsEmptiveEnroll;
    }

    /**
     * @return Заключен договор о целевом обучении. Свойство не может быть null.
     */
    @NotNull
    public boolean isHaveAgreementTargetEdu()
    {
        return _haveAgreementTargetEdu;
    }

    /**
     * @param haveAgreementTargetEdu Заключен договор о целевом обучении. Свойство не может быть null.
     */
    public void setHaveAgreementTargetEdu(boolean haveAgreementTargetEdu)
    {
        dirty(_haveAgreementTargetEdu, haveAgreementTargetEdu);
        _haveAgreementTargetEdu = haveAgreementTargetEdu;
    }

    /**
     * @return Номер договора.
     */
    @Length(max=255)
    public String getAgreementNumber()
    {
        return _agreementNumber;
    }

    /**
     * @param agreementNumber Номер договора.
     */
    public void setAgreementNumber(String agreementNumber)
    {
        dirty(_agreementNumber, agreementNumber);
        _agreementNumber = agreementNumber;
    }

    /**
     * @return Дата заключения.
     */
    public Date getAgreementDate()
    {
        return _agreementDate;
    }

    /**
     * @param agreementDate Дата заключения.
     */
    public void setAgreementDate(Date agreementDate)
    {
        dirty(_agreementDate, agreementDate);
        _agreementDate = agreementDate;
    }

    /**
     * @return Данные об организации.
     */
    @Length(max=255)
    public String getAgreementOrg()
    {
        return _agreementOrg;
    }

    /**
     * @param agreementOrg Данные об организации.
     */
    public void setAgreementOrg(String agreementOrg)
    {
        dirty(_agreementOrg, agreementOrg);
        _agreementOrg = agreementOrg;
    }

    /**
     * @return Данные об образовательной программе.
     */
    @Length(max=255)
    public String getAgreementEduProgramm()
    {
        return _agreementEduProgramm;
    }

    /**
     * @param agreementEduProgramm Данные об образовательной программе.
     */
    public void setAgreementEduProgramm(String agreementEduProgramm)
    {
        dirty(_agreementEduProgramm, agreementEduProgramm);
        _agreementEduProgramm = agreementEduProgramm;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantStatementGen)
        {
            setHaveEduLevel(((EnrOnlineEntrantStatement)another).getHaveEduLevel());
            setWantRequestType(((EnrOnlineEntrantStatement)another).getWantRequestType());
            setExclusiveRights(((EnrOnlineEntrantStatement)another).isExclusiveRights());
            setExclusiveRightsWithoutExams(((EnrOnlineEntrantStatement)another).isExclusiveRightsWithoutExams());
            setExclusiveRightsBudget(((EnrOnlineEntrantStatement)another).isExclusiveRightsBudget());
            setExclusiveRightsEmptiveEnroll(((EnrOnlineEntrantStatement)another).isExclusiveRightsEmptiveEnroll());
            setHaveAgreementTargetEdu(((EnrOnlineEntrantStatement)another).isHaveAgreementTargetEdu());
            setAgreementNumber(((EnrOnlineEntrantStatement)another).getAgreementNumber());
            setAgreementDate(((EnrOnlineEntrantStatement)another).getAgreementDate());
            setAgreementOrg(((EnrOnlineEntrantStatement)another).getAgreementOrg());
            setAgreementEduProgramm(((EnrOnlineEntrantStatement)another).getAgreementEduProgramm());
            setOnlineEntrant(((EnrOnlineEntrantStatement)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantStatementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantStatement.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantStatement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "haveEduLevel":
                    return obj.getHaveEduLevel();
                case "wantRequestType":
                    return obj.getWantRequestType();
                case "exclusiveRights":
                    return obj.isExclusiveRights();
                case "exclusiveRightsWithoutExams":
                    return obj.isExclusiveRightsWithoutExams();
                case "exclusiveRightsBudget":
                    return obj.isExclusiveRightsBudget();
                case "exclusiveRightsEmptiveEnroll":
                    return obj.isExclusiveRightsEmptiveEnroll();
                case "haveAgreementTargetEdu":
                    return obj.isHaveAgreementTargetEdu();
                case "agreementNumber":
                    return obj.getAgreementNumber();
                case "agreementDate":
                    return obj.getAgreementDate();
                case "agreementOrg":
                    return obj.getAgreementOrg();
                case "agreementEduProgramm":
                    return obj.getAgreementEduProgramm();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "haveEduLevel":
                    obj.setHaveEduLevel((String) value);
                    return;
                case "wantRequestType":
                    obj.setWantRequestType((String) value);
                    return;
                case "exclusiveRights":
                    obj.setExclusiveRights((Boolean) value);
                    return;
                case "exclusiveRightsWithoutExams":
                    obj.setExclusiveRightsWithoutExams((Boolean) value);
                    return;
                case "exclusiveRightsBudget":
                    obj.setExclusiveRightsBudget((Boolean) value);
                    return;
                case "exclusiveRightsEmptiveEnroll":
                    obj.setExclusiveRightsEmptiveEnroll((Boolean) value);
                    return;
                case "haveAgreementTargetEdu":
                    obj.setHaveAgreementTargetEdu((Boolean) value);
                    return;
                case "agreementNumber":
                    obj.setAgreementNumber((String) value);
                    return;
                case "agreementDate":
                    obj.setAgreementDate((Date) value);
                    return;
                case "agreementOrg":
                    obj.setAgreementOrg((String) value);
                    return;
                case "agreementEduProgramm":
                    obj.setAgreementEduProgramm((String) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "haveEduLevel":
                        return true;
                case "wantRequestType":
                        return true;
                case "exclusiveRights":
                        return true;
                case "exclusiveRightsWithoutExams":
                        return true;
                case "exclusiveRightsBudget":
                        return true;
                case "exclusiveRightsEmptiveEnroll":
                        return true;
                case "haveAgreementTargetEdu":
                        return true;
                case "agreementNumber":
                        return true;
                case "agreementDate":
                        return true;
                case "agreementOrg":
                        return true;
                case "agreementEduProgramm":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "haveEduLevel":
                    return true;
                case "wantRequestType":
                    return true;
                case "exclusiveRights":
                    return true;
                case "exclusiveRightsWithoutExams":
                    return true;
                case "exclusiveRightsBudget":
                    return true;
                case "exclusiveRightsEmptiveEnroll":
                    return true;
                case "haveAgreementTargetEdu":
                    return true;
                case "agreementNumber":
                    return true;
                case "agreementDate":
                    return true;
                case "agreementOrg":
                    return true;
                case "agreementEduProgramm":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "haveEduLevel":
                    return String.class;
                case "wantRequestType":
                    return String.class;
                case "exclusiveRights":
                    return Boolean.class;
                case "exclusiveRightsWithoutExams":
                    return Boolean.class;
                case "exclusiveRightsBudget":
                    return Boolean.class;
                case "exclusiveRightsEmptiveEnroll":
                    return Boolean.class;
                case "haveAgreementTargetEdu":
                    return Boolean.class;
                case "agreementNumber":
                    return String.class;
                case "agreementDate":
                    return Date.class;
                case "agreementOrg":
                    return String.class;
                case "agreementEduProgramm":
                    return String.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantStatement> _dslPath = new Path<EnrOnlineEntrantStatement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantStatement");
    }
            

    /**
     * @return Полученное образование (eduLevel).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getHaveEduLevel()
     */
    public static PropertyPath<String> haveEduLevel()
    {
        return _dslPath.haveEduLevel();
    }

    /**
     * @return Программы (enrRequestType).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getWantRequestType()
     */
    public static PropertyPath<String> wantRequestType()
    {
        return _dslPath.wantRequestType();
    }

    /**
     * @return У меня есть особые права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRights()
     */
    public static PropertyPath<Boolean> exclusiveRights()
    {
        return _dslPath.exclusiveRights();
    }

    /**
     * @return Право на прием без вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRightsWithoutExams()
     */
    public static PropertyPath<Boolean> exclusiveRightsWithoutExams()
    {
        return _dslPath.exclusiveRightsWithoutExams();
    }

    /**
     * @return Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRightsBudget()
     */
    public static PropertyPath<Boolean> exclusiveRightsBudget()
    {
        return _dslPath.exclusiveRightsBudget();
    }

    /**
     * @return Преимущественное право зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRightsEmptiveEnroll()
     */
    public static PropertyPath<Boolean> exclusiveRightsEmptiveEnroll()
    {
        return _dslPath.exclusiveRightsEmptiveEnroll();
    }

    /**
     * @return Заключен договор о целевом обучении. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isHaveAgreementTargetEdu()
     */
    public static PropertyPath<Boolean> haveAgreementTargetEdu()
    {
        return _dslPath.haveAgreementTargetEdu();
    }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementNumber()
     */
    public static PropertyPath<String> agreementNumber()
    {
        return _dslPath.agreementNumber();
    }

    /**
     * @return Дата заключения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementDate()
     */
    public static PropertyPath<Date> agreementDate()
    {
        return _dslPath.agreementDate();
    }

    /**
     * @return Данные об организации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementOrg()
     */
    public static PropertyPath<String> agreementOrg()
    {
        return _dslPath.agreementOrg();
    }

    /**
     * @return Данные об образовательной программе.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementEduProgramm()
     */
    public static PropertyPath<String> agreementEduProgramm()
    {
        return _dslPath.agreementEduProgramm();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantStatement> extends EntityPath<E>
    {
        private PropertyPath<String> _haveEduLevel;
        private PropertyPath<String> _wantRequestType;
        private PropertyPath<Boolean> _exclusiveRights;
        private PropertyPath<Boolean> _exclusiveRightsWithoutExams;
        private PropertyPath<Boolean> _exclusiveRightsBudget;
        private PropertyPath<Boolean> _exclusiveRightsEmptiveEnroll;
        private PropertyPath<Boolean> _haveAgreementTargetEdu;
        private PropertyPath<String> _agreementNumber;
        private PropertyPath<Date> _agreementDate;
        private PropertyPath<String> _agreementOrg;
        private PropertyPath<String> _agreementEduProgramm;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Полученное образование (eduLevel).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getHaveEduLevel()
     */
        public PropertyPath<String> haveEduLevel()
        {
            if(_haveEduLevel == null )
                _haveEduLevel = new PropertyPath<String>(EnrOnlineEntrantStatementGen.P_HAVE_EDU_LEVEL, this);
            return _haveEduLevel;
        }

    /**
     * @return Программы (enrRequestType).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getWantRequestType()
     */
        public PropertyPath<String> wantRequestType()
        {
            if(_wantRequestType == null )
                _wantRequestType = new PropertyPath<String>(EnrOnlineEntrantStatementGen.P_WANT_REQUEST_TYPE, this);
            return _wantRequestType;
        }

    /**
     * @return У меня есть особые права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRights()
     */
        public PropertyPath<Boolean> exclusiveRights()
        {
            if(_exclusiveRights == null )
                _exclusiveRights = new PropertyPath<Boolean>(EnrOnlineEntrantStatementGen.P_EXCLUSIVE_RIGHTS, this);
            return _exclusiveRights;
        }

    /**
     * @return Право на прием без вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRightsWithoutExams()
     */
        public PropertyPath<Boolean> exclusiveRightsWithoutExams()
        {
            if(_exclusiveRightsWithoutExams == null )
                _exclusiveRightsWithoutExams = new PropertyPath<Boolean>(EnrOnlineEntrantStatementGen.P_EXCLUSIVE_RIGHTS_WITHOUT_EXAMS, this);
            return _exclusiveRightsWithoutExams;
        }

    /**
     * @return Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRightsBudget()
     */
        public PropertyPath<Boolean> exclusiveRightsBudget()
        {
            if(_exclusiveRightsBudget == null )
                _exclusiveRightsBudget = new PropertyPath<Boolean>(EnrOnlineEntrantStatementGen.P_EXCLUSIVE_RIGHTS_BUDGET, this);
            return _exclusiveRightsBudget;
        }

    /**
     * @return Преимущественное право зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isExclusiveRightsEmptiveEnroll()
     */
        public PropertyPath<Boolean> exclusiveRightsEmptiveEnroll()
        {
            if(_exclusiveRightsEmptiveEnroll == null )
                _exclusiveRightsEmptiveEnroll = new PropertyPath<Boolean>(EnrOnlineEntrantStatementGen.P_EXCLUSIVE_RIGHTS_EMPTIVE_ENROLL, this);
            return _exclusiveRightsEmptiveEnroll;
        }

    /**
     * @return Заключен договор о целевом обучении. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#isHaveAgreementTargetEdu()
     */
        public PropertyPath<Boolean> haveAgreementTargetEdu()
        {
            if(_haveAgreementTargetEdu == null )
                _haveAgreementTargetEdu = new PropertyPath<Boolean>(EnrOnlineEntrantStatementGen.P_HAVE_AGREEMENT_TARGET_EDU, this);
            return _haveAgreementTargetEdu;
        }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementNumber()
     */
        public PropertyPath<String> agreementNumber()
        {
            if(_agreementNumber == null )
                _agreementNumber = new PropertyPath<String>(EnrOnlineEntrantStatementGen.P_AGREEMENT_NUMBER, this);
            return _agreementNumber;
        }

    /**
     * @return Дата заключения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementDate()
     */
        public PropertyPath<Date> agreementDate()
        {
            if(_agreementDate == null )
                _agreementDate = new PropertyPath<Date>(EnrOnlineEntrantStatementGen.P_AGREEMENT_DATE, this);
            return _agreementDate;
        }

    /**
     * @return Данные об организации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementOrg()
     */
        public PropertyPath<String> agreementOrg()
        {
            if(_agreementOrg == null )
                _agreementOrg = new PropertyPath<String>(EnrOnlineEntrantStatementGen.P_AGREEMENT_ORG, this);
            return _agreementOrg;
        }

    /**
     * @return Данные об образовательной программе.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getAgreementEduProgramm()
     */
        public PropertyPath<String> agreementEduProgramm()
        {
            if(_agreementEduProgramm == null )
                _agreementEduProgramm = new PropertyPath<String>(EnrOnlineEntrantStatementGen.P_AGREEMENT_EDU_PROGRAMM, this);
            return _agreementEduProgramm;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantStatement.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantStatement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
