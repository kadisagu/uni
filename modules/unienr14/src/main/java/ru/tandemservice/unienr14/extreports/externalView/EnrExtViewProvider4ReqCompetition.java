/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

/**
 * @author oleyba
 * @since 8/23/13
 */
public class EnrExtViewProvider4ReqCompetition extends SimpleDQLExternalViewConfig
{

    private static final java.util.List<String> BUDGET_COMP_TYPES = Arrays.asList(EnrCompetitionTypeCodes.MINISTERIAL, EnrCompetitionTypeCodes.EXCLUSIVE, EnrCompetitionTypeCodes.TARGET_ADMISSION, EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL);

    @Override
    protected DQLSelectBuilder buildDqlQuery() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "req")
            .joinPath(DQLJoinType.left, EnrRequestedCompetition.state().fromAlias("req"), "req_state")

            .joinEntity("req", DQLJoinType.left, EnrRequestedCompetitionTA.class, "req_ta", eq(property("req_ta"), property("req")))
            .joinPath(DQLJoinType.left, EnrRequestedCompetitionTA.targetAdmissionKind().fromAlias("req_ta"), "req_ta_kind_rel")
            .joinPath(DQLJoinType.left, EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("req_ta_kind_rel"), "req_ta_kind")
            .joinPath(DQLJoinType.left, EnrRequestedCompetitionTA.targetAdmissionOrgUnit().fromAlias("req_ta"), "req_ta_ou")

            .joinEntity("req", DQLJoinType.left, EnrRequestedCompetitionExclusive.class, "req_excl", eq(property("req_excl"), property("req")))
            .joinPath(DQLJoinType.left, EnrRequestedCompetitionExclusive.benefitCategory().fromAlias("req_excl"), "benefit_cat_excl")

            .joinEntity("req", DQLJoinType.left, EnrRequestedCompetitionNoExams.class, "req_no_exam", eq(property("req_no_exam"), property("req")))
            .joinPath(DQLJoinType.left, EnrRequestedCompetitionNoExams.benefitCategory().fromAlias("req_no_exam"), "benefit_cat_no_exam")

            .joinPath(DQLJoinType.left, EnrRequestedCompetition.competition().fromAlias("req"), "comp")
            .joinPath(DQLJoinType.left, EnrCompetition.programSetOrgUnit().fromAlias("comp"), "ps_ou")
            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.programSet().fromAlias("ps_ou"), "ps")
            .joinPath(DQLJoinType.left, EnrProgramSetBase.programSubject().fromAlias("ps"), "subject")
            .joinPath(DQLJoinType.left, EduProgramSubject.subjectIndex().fromAlias("subject"), "s_index")
            .joinPath(DQLJoinType.left, EduProgramSubjectIndex.programKind().fromAlias("s_index"), "program_kind")

            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.formativeOrgUnit().fromAlias("ps_ou"), "form_ou")
            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.orgUnit().fromAlias("ps_ou"), "enr_ou")
            .joinPath(DQLJoinType.left, EnrOrgUnit.institutionOrgUnit().fromAlias("enr_ou"), "inst_ou")
            .joinPath(DQLJoinType.left, EduInstitutionOrgUnit.orgUnit().fromAlias("inst_ou"), "enr_ou_ou")

            .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("req"), "request")
            .joinPath(DQLJoinType.inner, EnrEntrantRequest.entrant().fromAlias("request"), "entrant")

            .joinPath(DQLJoinType.left, EnrEntrantRequest.benefitCategory().fromAlias("request"), "benefit_cat_pref")

            .joinEntity("req", DQLJoinType.left, EnrRatingItem.class, "rating_e", eq(property(EnrRatingItem.requestedCompetition().fromAlias("rating_e")), property("req")))
            .joinPath(DQLJoinType.left, EnrEntrantRequest.eduInstDocOriginalRef().fromAlias("request"), "doc_status")

            .joinPath(DQLJoinType.left, EnrEntrantRequest.eduDocument().fromAlias("request"), "edu_doc")
            .joinPath(DQLJoinType.left, PersonEduDocument.eduDocumentKind().fromAlias("edu_doc"), "edu_doc_kind")
            .joinPath(DQLJoinType.left, PersonEduDocument.eduLevel().fromAlias("edu_doc"), "edu_level")
            .joinPath(DQLJoinType.left, PersonEduDocument.graduationHonour().fromAlias("edu_doc"), "grad_honour")
            .joinPath(DQLJoinType.left, PersonEduDocument.eduOrganizationAddressItem().fromAlias("edu_doc"), "edu_doc_address_item")
            .joinPath(DQLJoinType.left, AddressItem.country().fromAlias("edu_doc_address_item"), "edu_doc_address_country")

            .joinPath(DQLJoinType.inner, EnrEntrantRequest.identityCard().fromAlias("request"), "idc")
            .joinEntity("idc", DQLJoinType.left, AddressCountry.class, "citizenship_country", eq(property("idc", IdentityCard.citizenship().id()), property("citizenship_country", AddressCountry.id())))
//                .joinPath(DQLJoinType.left, IdentityCard.citizenship().fromAlias("idc"), "citizenship_country")

            .joinEntity("req", DQLJoinType.left, EnrEnrollmentExtract.class, "enr_extract", eq(property(EnrEnrollmentExtract.entity().fromAlias("enr_extract")), property("req")))
            .joinEntity("enr_extract", DQLJoinType.left, EnrEnrollmentParagraph.class, "enr_par", eq(property(EnrEnrollmentExtract.paragraph().fromAlias("enr_extract")), property("enr_par")))
            .joinEntity("enr_par", DQLJoinType.left, EnrOrder.class, "enr_order", eq(property(EnrEnrollmentParagraph.order().fromAlias("enr_par")), property("enr_order")))
            .joinPath(DQLJoinType.left, EnrOrder.state().fromAlias("enr_order"), "enr_order_state")

            .joinEntity("enr_extract", DQLJoinType.left, EnrCancelExtract.class, "cancel_extract", eq(property(EnrCancelExtract.entity().fromAlias("cancel_extract")), property("enr_extract")))
            .joinEntity("cancel_extract", DQLJoinType.left, EnrCancelParagraph.class, "cancel_par", eq(property(EnrCancelExtract.paragraph().fromAlias("cancel_extract")), property("cancel_par")))
            .joinEntity("cancel_par", DQLJoinType.left, EnrOrder.class, "cancel_order", eq(property(EnrCancelParagraph.order().fromAlias("cancel_par")), property("cancel_order")))
            .joinPath(DQLJoinType.left, EnrOrder.state().fromAlias("cancel_order"), "cancel_order_state")

            .joinEntity("ps", DQLJoinType.left, EnrProgramSetSecondary.class, "secondaryPS", eq(property(EnrProgramSetBase.id().fromAlias("ps")), property(EnrProgramSetSecondary.id().fromAlias("secondaryPS"))))
            .joinPath(DQLJoinType.left, EnrProgramSetSecondary.program().fromAlias("secondaryPS"), "secondaryProgram")
            .joinPath(DQLJoinType.left, EduProgramSecondaryProf.baseLevel().fromAlias("secondaryProgram"), "baseLevel")
            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.program().fromAlias("ps_ou"), "program")
            .joinPath(DQLJoinType.left, EduProgram.eduProgramTrait().fromAlias("program"), "trait")


        ;

        column(dql, property(EnrRequestedCompetition.id().fromAlias("req")), "id").comment("uid выбранного конкурса");
        column(dql, property(EnrRequestedCompetition.priority().fromAlias("req")), "priority").comment("приоритет");
        booleanIntColumn(dql, EnrRequestedCompetition.profileEducation().fromAlias("req"), "profileEducation").comment("получил профильное образование");
        booleanIntColumn(dql, EnrRequestedCompetition.acceleratedLearning().fromAlias("req"), "acceleratedLearning").comment("индивидуальное ускоренное обучение (1 - да, 0 - нет)");
        booleanIntColumn(dql, EnrRequestedCompetition.parallel().fromAlias("req"), "parallel").comment("абитуриент хочет быть зачисленным на данный конкурс параллельно основному зачислению (1 - да, 0 - нет), всегда 0 для конкурсов, где вид приема не предусматривает договора на обучение");
//        booleanIntColumn(dql, EnrRequestedCompetition.acceptedContract().fromAlias("req"), "acceptedContract").comment("вместо этой колонки следует использовать колонку accepted. в данный момент эта колонка оставлена для совместимости с уже разработанными отчетами, но в дальнейшем будет удалена.");
        column(dql,  new DQLCaseExpressionBuilder(property("req", EnrRequestedCompetition.competition().type().code()))
                .when(value(EnrCompetitionTypeCodes.CONTRACT), cast(property("req", EnrRequestedCompetition.accepted()), PropertyType.INT))
                .when(value(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT), cast(property("req", EnrRequestedCompetition.accepted()), PropertyType.INT))
                .otherwise(value(0))
                .build(), "acceptedContract").comment("вместо этой колонки следует использовать колонку accepted. в данный момент эта колонка оставлена для совместимости с уже разработанными отчетами, но в дальнейшем будет удалена.");
        booleanIntColumn(dql, EnrRequestedCompetition.accepted().fromAlias("req"), "accepted").comment("согласие на зачисление (1 - да, 0 - нет)");
        booleanIntColumn(dql, EnrRequestedCompetition.enrollmentAvailable().fromAlias("req"), "enrollmentAvailable").comment("согласие на зачисление (1 - да, 0 - нет)");

        column(dql, property(EnrEntrantState.title().fromAlias("req_state")), "state").comment("состояние выбранного конкурса");
        column(dql, property(EnrEntrantState.code().fromAlias("req_state")), "stateCode").comment("код состояния выбранного конкурса");

        booleanIntExprColumn(dql, or(
                                     eq(property(EnrCompetition.type().code().fromAlias("comp")), value(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)),
                                     eq(property(EnrCompetition.type().code().fromAlias("comp")), value(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL))),
        "statusNoEntranceExams").comment("поступает без ВИ (1 - да, 0 - нет)");
        booleanIntExprColumn(dql, eq(property(EnrCompetition.type().code().fromAlias("comp")), value(EnrCompetitionTypeCodes.EXCLUSIVE)),
                             "statusExclusive").comment("поступает по квоте особых прав (1 - да, 0 - нет)");

        column(dql, property(EnrTargetAdmissionKind.title().fromAlias("req_ta_kind")), "targetAdmissionKind").comment("вид целевого приема");
        column(dql, property(EnrTargetAdmissionKind.code().fromAlias("req_ta_kind")), "targetAdmissionKindCode").comment("код вида целевого приема");
        column(dql, property(ExternalOrgUnit.title().fromAlias("req_ta_ou")), "targetAdmissionOrgUnit").comment("организация целевого приема");

        // отсюда начался копипаст из EnrExtViewProvider4Competition

        column(dql, property("comp", EnrCompetition.id()), "competitionId").comment("uid конкурса");
        column(dql, property("ps", EnrProgramSetBase.enrollmentCampaign().title()), "enrollmentCampaign").comment("приемная кампания");
        column(dql, property("ps", EnrProgramSetBase.enrollmentCampaign().educationYear().intValue()), "enrollmentCampaignYear").comment("год приема (календарный)");
        column(dql, property("ps", EnrProgramSetBase.enrollmentCampaign().educationYear().title()), "enrollmentCampaignEduYear").comment("год приема (учебный)");

        column(dql, property("comp", EnrCompetition.plan()), "admissionPlan").comment("план приема");

        column(dql, property("ps", EnrProgramSetBase.id()), "programSetId").comment("uid набора ОП");
        column(dql, property("ps", EnrProgramSetBase.title()), "programSetTitle").comment("название набора ОП");

        column(dql, property("ps_ou", EnrProgramSetOrgUnit.id()), "programSetOuId").comment("uid подразделения, ведущего прием по набору ОП");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.ministerialPlan()), "programSetOuMinisterialPlan").comment("КЦП по набору ОП для подразделения, ведущего прием");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.targetAdmPlan()), "programSetOuTargetAdmPlan").comment("план приема по ЦП по набору ОП для подразделения, ведущего прием");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.exclusivePlan()), "programSetOuExclusivePlan").comment("план приема по квоте по набору ОП для подразделения, ведущего прием");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.contractPlan()), "programSetOuContractPlan").comment("план приема по договору по набору ОП для подразделения, ведущего прием");

        column(dql, property("enr_ou", EnrOrgUnit.id()), "enrOrgUnitId").comment("uid филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.fullTitle()), "enrOrgUnitFullTitle").comment("полное название филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.shortTitle()), "enrOrgUnitShortTitle").comment("сокращенное название филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.territorialFullTitle()), "enrOrgUnitTerrFullTitle").comment("полное терр. название филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.territorialShortTitle()), "enrOrgUnitTerrShortTitle").comment("сокращенное терр. название филиала в рамках ПК");

        column(dql, property("form_ou", OrgUnit.id()), "formativeOrgUnitId").comment("uid формирующего подразделения");
        column(dql, property("form_ou", OrgUnit.fullTitle()), "formativeOrgUnitFullTitle").comment("полное название формирующего подразделения");
        column(dql, property("form_ou", OrgUnit.shortTitle()), "formativeOrgUnitShortTitle").comment("сокращенное название формирующего подразделения");

        column(dql, property("comp", EnrCompetition.type().title()), "competitionType").comment("вид приема");
        column(dql, property("comp", EnrCompetition.type().code()), "competitionTypeCode").comment("код вида приема");

        column(dql, property("comp", EnrCompetition.requestType().title()), "requestType").comment("вид заявления");
        column(dql, property("comp", EnrCompetition.requestType().code()), "requestTypeCode").comment("код вида заявления");

        column(dql, property("comp", EnrCompetition.type().compensationType().title()), "compensationType").comment("вид возм. затрат");
        column(dql, property("comp", EnrCompetition.type().compensationType().code()), "compensationTypeCode").comment("код вида возм. затрат");

        column(dql, property("comp", EnrCompetition.eduLevelRequirement().title()), "eduLevelRequirement").comment("ограничение по уровню образования поступающих");
        column(dql, property("comp", EnrCompetition.eduLevelRequirement().code()), "eduLevelRequirementCode").comment("код ограничения по уровню образования поступающих");

        column(dql, property("ps", EnrProgramSetBase.programForm().title()), "programForm").comment("форма обучения");
        column(dql, property("ps", EnrProgramSetBase.programForm().code()), "programFormCode").comment("код формы обучения");

        column(dql, property("ps", EnrProgramSetBase.programSubject().title()), "programSubject").comment("направление (профессия, специальность)");
        column(dql, property("ps", EnrProgramSetBase.programSubject().subjectCode()), "programSubjectCode").comment("код направления (профессии, специальности)");

        column(dql, property("program_kind", EduProgramKind.title()), "programKind").comment("вид обр. программы");
        column(dql, property("program_kind", EduProgramKind.code()), "programKindCode").comment("код вида обр. программы");
        column(dql, property("program_kind", EduProgramKind.shortTitle()), "programKindShortTitle").comment("сокр. название вида обр. программы");

        column(dql, property("program_kind", EduProgramKind.eduLevel().title()), "levelType").comment("уровень образования");
        column(dql, property("program_kind", EduProgramKind.eduLevel().code()), "levelTypeCode").comment("код уровня образования");

        booleanIntColumn(dql, EduProgramKind.programBachelorDegree().fromAlias("program_kind"), "bachelor").comment("ОП по программе бакалавриата (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programHigherProf().fromAlias("program_kind"), "high").comment("ОП по программе высшего образования (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programMasterDegree().fromAlias("program_kind"), "master").comment("ОП по программе магистратуры (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programSecondaryProf().fromAlias("program_kind"), "middle").comment("ОП по программе СПО (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programSpecialistDegree().fromAlias("program_kind"), "specialty").comment("ОП по программе специалитета (1 - да, 0 - нет)");

        booleanIntColumn(dql, EduProgramSecondaryProf.inDepthStudy().fromAlias("secondaryProgram"), "inDepthStudy").comment("Углубленное изучение для ОП по программам СПО (1 - да, 0 - нет)");
        column(dql, property("baseLevel", EduLevel.code()), "baseLevelCode").comment("Код базы образования");
        column(dql, property("trait", EduProgramTrait.code()), "programTraitCode").comment("Код особенности реализации");


        column(dql,
                new DQLCaseExpressionBuilder(property("program_kind", EduProgramKind.code()))
                        .when(value(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA), cast(value("бакалавр"), PropertyType.STRING))
                        .when(value(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV), cast(value("специалист"), PropertyType.STRING))
                        .when(value(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY), cast(value("магистр"), PropertyType.STRING))
                        .otherwise(cast(value("-"), PropertyType.STRING))
                        .build(),
                "qualification"
        ).comment("квалификация (не для всех видов программ, разделение бакалавров на академ. и прикл. пока не поддерживается)");

        // а здесь закончился копипаст из EnrExtViewProvider4Competition

        column(dql, property(EnrRequestedCompetition.request().id().fromAlias("req")), "entrantRequestId").comment("uid заявления");

        booleanIntExprColumn(dql, and(
                isNotNull(property("doc_status", "id")),
                eq(property(EnrRequestedCompetition.originalDocumentHandedIn().fromAlias("req")), value(true))
        ), "originalDocumentHandedIn").comment("сдан оригинал док. об образ. (1 - да, 0 - нет)");

        column(dql, property(EnrEntrantOriginalDocumentStatus.registrationDate().fromAlias("doc_status")), "originalDocumentInDate").comment("дата сдачи оригинала док. об образ.");
        column(dql, property(EnrEntrantOriginalDocumentStatus.takeAwayDate().fromAlias("doc_status")), "originalDocumentOutDate").comment("дата возврата оригинала док. об образ.");

        column(dql, EnrExtViewUtil.asLongColumn(EnrRatingItem.totalMarkAsLong().fromAlias("rating_e")), "rating").comment("суммарный балл по сдаче ВИ");
        booleanIntColumn(dql, EnrRatingItem.statusEntranceExamsCorrect().fromAlias("rating_e"), "statusEntranceExamsCorrect").comment("корректно выбраны ВИ (1 - да, 0 - нет)");
        booleanIntColumn(dql, EnrRatingItem.statusRatingComplete().fromAlias("rating_e"), "statusRatingComplete").comment("сданы все ВИ (1 - да, 0 - нет)");
        booleanIntColumn(dql, EnrRatingItem.statusRatingPositive().fromAlias("rating_e"), "statusRatingPositive").comment("сданы все ВИ выше мин. балла (1 - да, 0 - нет)");
        column(dql, property(EnrRatingItem.position().fromAlias("rating_e")), "ratingPosition").comment("позиция в конкурсе");
        column(dql, property(EnrRatingItem.positionAbsolute().fromAlias("rating_e")), "ratingPositionAbsolute").comment("ранг в конкурсе");

        column(dql, diffyears(createDate(year(property(EnrEntrantRequest.regDate().fromAlias("request"))), value(1), value(1)), property(IdentityCard.birthDate().fromAlias("idc"))), "age").comment("полных лет (на момент подачи заявления)");
        column(dql, property(EnrEntrantRequest.regDate().fromAlias("request")), "regDate").comment("дата подачи заявления");
        column(dql, year(property(EnrEntrantRequest.regDate().fromAlias("request"))), "regDateYear").comment("год подачи заявления");
        column(dql, property(EnrEntrantRequest.regNumber().fromAlias("request")), "regNumber").comment("регистрационный номер заявления");
        booleanIntColumn(dql, EnrEntrantRequest.takeAwayDocument().fromAlias("request"), "takeAwayDocument").comment("абитуриент забрал документы и отозвал заявление (1 - да, 0 - нет)");
        column(dql, property(EnrEntrantRequest.takeAwayDocumentDate().fromAlias("request")), "takeAwayDocumentDate").comment("дата отзыва заявления абитуриентом");

        booleanIntColumn(dql, EnrEntrantRequest.eveningSchool().fromAlias("request"), "eveningSchool").comment("выпускник вечерней (сменной) образовательной организации (1 - да, 0 - нет)");
        booleanIntColumn(dql, EnrEntrantRequest.specialSchool().fromAlias("request"), "specialSchool").comment("выпускник специальной (коррекционной) образовательной организации (1 - да, 0 - нет)");
        column(dql, property(AddressCountry.title().fromAlias("edu_doc_address_country")), "eduInstCountry").comment("страна, в которой получен док. об образ.");
        column(dql, property(AddressCountry.digitalCode().fromAlias("edu_doc_address_country")), "eduInstCountryCode").comment("код страны, в которой получен док. об образ.");

        column(dql, property(EduLevel.title().fromAlias("edu_level")), "eduLevel").comment("уровень образования (по документу об образовании, актуальный)");
        column(dql, property(EduLevel.code().fromAlias("edu_level")), "eduLevelCode").comment("код уровня образования (по документу об образовании, актуальный)");
        column(dql, property(PersonEduDocument.documentEducationLevel().fromAlias("edu_doc")), "eduDocumentEduLevel").comment("уровень образования (по документу об образовании, как указан в документе)");

        column(dql, property(EduDocumentKind.title().fromAlias("edu_doc_kind")), "eduDocumentKind").comment("вид документа об образовании");
        column(dql, property(EduDocumentKind.code().fromAlias("edu_doc_kind")), "eduDocumentKindCode").comment("код вида документа об образовании");
        column(dql, property(GraduationHonour.title().fromAlias("grad_honour")), "graduationHonour").comment("степень отличия в документе об образовании");

        column(dql, EnrExtViewUtil.asLongColumn(PersonEduDocument.avgMarkAsLong().fromAlias("edu_doc")), "eduDocumentAverageMark").comment("средний балл по документу об образовании");
        column(dql, property(PersonEduDocument.issuanceDate().fromAlias("edu_doc")), "eduDocumentIssuanceDate").comment("дата выдачи документа об образовании");
        column(dql, property(PersonEduDocument.yearEnd().fromAlias("edu_doc")), "eduDocumentYearEnd").comment("год окончания по документу об образовании");

        column(dql, property(IdentityCard.birthDate().fromAlias("idc")), "birthDate").comment("дата рождения");
        column(dql, year(property(IdentityCard.birthDate().fromAlias("idc"))), "birthDateYear").comment("год рождения");
        column(dql, property(AddressCountry.title().fromAlias("citizenship_country")), "citizenship").comment("гражданство");
        column(dql, property(AddressCountry.digitalCode().fromAlias("citizenship_country")), "citizenshipCode").comment("код страны гражданства");
        column(dql, property(IdentityCard.firstName().fromAlias("idc")), "firstName").comment("имя");
        column(dql, property(IdentityCard.lastName().fromAlias("idc")), "lastName").comment("фамилия");
        column(dql, property(IdentityCard.middleName().fromAlias("idc")), "middleName").comment("отчество");
        column(dql, property(IdentityCard.fullFio().fromAlias("idc")), "fullFio").comment("ФИО");
        column(dql, property(IdentityCard.sex().title().fromAlias("idc")), "sex").comment("пол");
        column(dql, property(IdentityCard.sex().code().fromAlias("idc")), "sexCode").comment("код пола");

        booleanIntColumn(dql, property(EnrEntrantRequest.internationalTreatyContractor().fromAlias("request")), "interTreatyContractor").comment("Абитуриент, поступающий по международному договору (1 - да, 0 - нет)");
        booleanIntColumn(dql, property(EnrEntrantRequest.generalEduSchool().fromAlias("request")), "generalEducation").comment("Выпускник общеобразовательной организации");

        column(dql, property(EnrEntrant.id().fromAlias("entrant")), "entrantId").comment("uid абитуриента");
        booleanIntColumn(dql, EnrEntrant.archival().fromAlias("entrant"), "archival").comment("абитуриент в архиве (1 - да, 0 - нет)");
        column(dql, property(EnrEntrant.beginArmy().fromAlias("entrant")), "beginArmy").comment("дата начала службы в армии");
        column(dql, property(EnrEntrant.endArmy().fromAlias("entrant")), "endArmy").comment("дата окончания службы в армии");
        column(dql, property(EnrEntrant.endArmyYear().fromAlias("entrant")), "endArmyYear").comment("год окончания службы в армии");
        booleanIntColumn(dql, EnrEntrant.passArmy().fromAlias("entrant"), "passArmy").comment("служил в армии (1 - да, 0 - нет)");
        column(dql, value(Boolean.FALSE), "passProfileEducation").comment("временное, будет удалено");
        column(dql, property(EnrEntrant.personalNumber().fromAlias("entrant")), "personalNumber").comment("личный номер абитуриента");

        booleanIntExprColumn(dql, exists(new DQLSelectBuilder()
            .fromEntity(EnrEntrantAccessCourse.class, "ac").column("ac.id")
            .where(eq(property("entrant", "id"), property("ac", EnrEntrantAccessCourse.entrant().s())))
            .buildQuery()), "accessCoursesCount").comment("обучался на подготовительных курсах (1 - да, 0 - нет)");

        booleanIntColumn(dql, EnrEntrant.needSpecialExamConditions().fromAlias("entrant"), "needSpecialExamConditions").comment("нуждается в спец. условиях для прохождения ВИ (1 - да, 0 - нет)");

        column(dql, property(EnrEntrant.enrollmentCampaign().dateFrom().fromAlias("entrant")), "dateFrom").comment("дата начала приемной кампании");
        column(dql, year(property(EnrEntrant.enrollmentCampaign().dateFrom().fromAlias("entrant"))), "dateFromYear").comment("год начала приемной кампании");
        column(dql, property(EnrEntrant.enrollmentCampaign().dateTo().fromAlias("entrant")), "dateTo").comment("дата окончания приемной кампании");
        column(dql, year(property(EnrEntrant.enrollmentCampaign().dateTo().fromAlias("entrant"))), "dateToYear").comment("год окончания приемной кампании");

        column(dql, property(EnrBenefitCategory.shortTitle().fromAlias("benefit_cat_excl")), "benefitCategoryExclusive").comment("категория, дающая особое право на поступление по квоте");
        column(dql, property(EnrBenefitCategory.code().fromAlias("benefit_cat_excl")), "benefitCategoryExclusiveCode").comment("код категории, дающей особое право на поступление по квоте");

        column(dql, property(EnrBenefitCategory.shortTitle().fromAlias("benefit_cat_no_exam")), "benefitCategoryNoExam").comment("категория, дающая особое право на поступление без ВИ");
        column(dql, property(EnrBenefitCategory.code().fromAlias("benefit_cat_no_exam")), "benefitCategoryNoExamCode").comment("код категории, дающей особое право на поступление без ВИ");

        column(dql, property(EnrBenefitCategory.shortTitle().fromAlias("benefit_cat_pref")), "benefitCategoryPreference").comment("категория, дающая преимущественное право поступления");
        column(dql, property(EnrBenefitCategory.code().fromAlias("benefit_cat_pref")), "benefitCategoryPreferenceCode").comment("код категории, дающей преимущественное право поступления");

        column(dql, property(EnrOrder.id().fromAlias("enr_order")), "enrOrderId").comment("uid приказа о зачислении");
        column(dql, property(EnrOrder.number().fromAlias("enr_order")), "enrOrderNumber").comment("номер приказа о зачислении");
        column(dql, property(EnrOrder.commitDate().fromAlias("enr_order")), "enrOrderDate").comment("дата приказа о зачислении");
        column(dql, property(OrderStates.title().fromAlias("enr_order_state")), "enrOrderState").comment("состояние приказа о зачислении");
        column(dql, property(OrderStates.code().fromAlias("enr_order_state")), "enrOrderStateCode").comment("код состояния приказа о зачислении");
        booleanIntColumn(dql, property(EnrEnrollmentExtract.cancelled().fromAlias("enr_extract")), "enrExtractCancelled").comment("выписка о зачислении отменена (1 - да, 0 - нет)");

        column(dql, property(EnrOrder.id().fromAlias("cancel_order")), "cancelOrderId").comment("uid приказа об отмене зачисления");
        column(dql, property(EnrOrder.number().fromAlias("cancel_order")), "cancelOrderNumber").comment("номер приказа об отмене зачисления");
        column(dql, property(EnrOrder.commitDate().fromAlias("cancel_order")), "cancelOrderDate").comment("дата приказа об отмене зачисления");
        column(dql, property(OrderStates.title().fromAlias("cancel_order_state")), "cancelOrderState").comment("состояние приказа об отмене зачисления");
        column(dql, property(OrderStates.code().fromAlias("cancel_order_state")), "cancelOrderStateCode").comment("код состояния приказа об отмене зачисления");

        return dql;
    }
}

