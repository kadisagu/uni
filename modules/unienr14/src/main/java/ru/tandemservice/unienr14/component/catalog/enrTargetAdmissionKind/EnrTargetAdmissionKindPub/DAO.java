/**
 *$Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrTargetAdmissionKind.EnrTargetAdmissionKindPub;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class DAO extends DefaultCatalogPubDAO<EnrTargetAdmissionKind, Model> implements IDAO
{
    @Override
    protected void applyFilters(Model model, MQBuilder builder)
    {
        super.applyFilters(model, builder);

        builder.addOrder("ci", EnrTargetAdmissionKind.priority(), OrderDirection.asc);
    }

    @Override
    public void doUp(Long id)
    {
        final EnrTargetAdmissionKind entity = get(EnrTargetAdmissionKind.class, id);

        final EnrTargetAdmissionKind nearEntity = new DQLSelectBuilder().fromEntity(EnrTargetAdmissionKind.class, "b").column(property("b"))
                .where(lt(property(EnrTargetAdmissionKind.priority().fromAlias("b")), value(entity.getPriority())))
                .order(property(EnrTargetAdmissionKind.priority().fromAlias("b")), OrderDirection.desc)
                .createStatement(getSession()).setMaxResults(1).uniqueResult();

        if (nearEntity == null)
            return;

        final int priority = entity.getPriority();
        final int nearPriority = nearEntity.getPriority();

        entity.setPriority(-1);
        getSession().update(entity);
        getSession().flush();

        nearEntity.setPriority(priority);
        getSession().update(nearEntity);
        getSession().flush();

        entity.setPriority(nearPriority);
        getSession().update(entity);
        getSession().flush();
    }

    @Override
    public void doDown(Long id)
    {
        final EnrTargetAdmissionKind entity = get(EnrTargetAdmissionKind.class, id);

        final EnrTargetAdmissionKind nearEntity = new DQLSelectBuilder().fromEntity(EnrTargetAdmissionKind.class, "b").column(property("b"))
                .where(gt(property(EnrTargetAdmissionKind.priority().fromAlias("b")), value(entity.getPriority())))
                .order(property(EnrTargetAdmissionKind.priority().fromAlias("b")), OrderDirection.asc)
                .createStatement(getSession()).setMaxResults(1).uniqueResult();

        if (nearEntity == null)
            return;

        final int priority = entity.getPriority();
        final int nearPriority = nearEntity.getPriority();

        entity.setPriority(-1);
        getSession().update(entity);
        getSession().flush();

        nearEntity.setPriority(priority);
        getSession().update(nearEntity);
        getSession().flush();

        entity.setPriority(nearPriority);
        getSession().update(entity);
        getSession().flush();
    }
}
