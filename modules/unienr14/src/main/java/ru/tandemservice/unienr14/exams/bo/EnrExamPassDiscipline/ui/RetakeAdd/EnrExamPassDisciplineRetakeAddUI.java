/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.RetakeAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

/**
 * @author oleyba
 * @since 6/21/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrExamPassDisciplineRetakeAddUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();
    private EnrExamPassDiscipline exam;
    
    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    public void onClickApply() {
        EnrExamPassDisciplineManager.instance().dao().doCreateRetake(getExam());
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrExamPassDisciplineRetakeAdd.BIND_ENTRANT, getEntrant());
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public EnrExamPassDiscipline getExam()
    {
        return exam;
    }

    public void setExam(EnrExamPassDiscipline exam)
    {
        this.exam = exam;
    }
}