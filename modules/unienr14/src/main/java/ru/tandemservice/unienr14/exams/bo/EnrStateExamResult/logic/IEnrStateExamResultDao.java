/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 4/22/13
 */
public interface IEnrStateExamResultDao extends INeedPersistenceSupport
{
    /**
     * Меняет свойство "Зачтено" результата ЕГЭ на противоположное значение
     * @param id идентификатор результата
     */
    void doChangeResultChecked(Long id);

    /**
     * Меняет свойство "Результат проверен вручную" результата ЕГЭ на противоположное значение
     * @param id идентификатор результата
     */
    void doChangeResultVerifiedByUser(Long id);


    /**
     * Находит перечень конкурсов из ПК абитуриента, ВИ которыз покрываются результатами ЕГЭ абитуриента
     * @param entrant абитуриент
     * @return конкурсы
     */
    List<EnrCompetition> getCoveredCompetitions(EnrEntrant entrant);

    /* Возвращает данные УЛ абитуриентов данной ПК. */
    byte[] getEntrantStateExamResultExportContent(EnrEnrollmentCampaign enrCampaign, Date requestRegDateFrom, Date requestRegDateTo, boolean includeForeignCitizens, DataWrapper icType, List<EnrOrder> enrOrders) throws UnsupportedEncodingException;

    /* Импортирует данные ФИС ЕГЭ, возвращает файл результата импорта. */
    byte[] updateStateExamResultsAndGetImportResultsContent(EnrEnrollmentCampaign enrCampaign, InputStream inputStream) throws Exception;
}
