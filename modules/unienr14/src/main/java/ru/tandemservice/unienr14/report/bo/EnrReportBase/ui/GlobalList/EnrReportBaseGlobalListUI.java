/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.GlobalList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.evaluation.IExpressionEvaluationService;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.ViewWrapper.EnrReportBaseViewWrapperUI;

import java.util.*;

/**
 * @author oleyba
 * @since 5/12/14
 */
public class EnrReportBaseGlobalListUI extends UIPresenter
{
    private Map<String, EnrReportBaseManager.IEnrReportDefinition> reportMap = new HashMap<>();

    private List<EnrReportBaseManager.IEnrReportDefinition> reportList;
    private EnrReportBaseManager.IEnrReportDefinition currentReport;

    @Override
    public void onComponentActivate()
    {
        final IExpressionEvaluationService evalService = CoreServices.expressionEvaluationService();
        final Map<String, EnrReportBaseManager.IEnrReportDefinition> itemMap = EnrReportBaseManager.instance().reportListExtPoint().getItems();

        this.getReportMap().clear();

        for (final Map.Entry<String, EnrReportBaseManager.IEnrReportDefinition> entry : itemMap.entrySet())
        {
            final EnrReportBaseManager.IEnrReportDefinition reportDefinition = entry.getValue();

            final String visibleExpression = StringUtils.trimToNull(reportDefinition.getVisibleExpression());
            if (null != visibleExpression) {
                try {
                    if (!Boolean.TRUE.equals(evalService.evaluateExpression(visibleExpression))) {
                        continue;
                    }
                } catch (final Exception t) {
                    CoreExceptionUtils.logException("EnrReportListViewUI.IEnrReportDefinition.getVisibleExpression", t);
                    continue;
                }
            }
            getReportMap().put(reportDefinition.getName(), reportDefinition);
        }

        setReportList(new ArrayList<EnrReportBaseManager.IEnrReportDefinition>(getReportMap().values()));
        Collections.sort(getReportList(), ITitled.TITLED_COMPARATOR);
    }

    public void onClickViewReport() {
        getReportMap().get(this.<String>getListenerParameter()).onClickViewReport(this.<String>getListenerParameter(), _uiActivation);
    }

    // Calculate

    public boolean isAllowPermission()
    {
        return CoreServices.securityService().check(CoreServices.securityService().getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), getCurrentReport().getName());
    }

    public String getCurrentReportNumber() {
        return String.valueOf(getReportList().indexOf(getCurrentReport()) + 1);
    }

    public Map getCurrentReportParameters() {
        return new ParametersMap().add(EnrReportBaseViewWrapperUI.REPORT_NAME_KEY, getCurrentReport().getName());
    }

    // Getters & Setters

    public EnrReportBaseManager.IEnrReportDefinition getCurrentReport()
    {
        return currentReport;
    }

    public void setCurrentReport(EnrReportBaseManager.IEnrReportDefinition currentReport)
    {
        this.currentReport = currentReport;
    }

    public void setReportList(List<EnrReportBaseManager.IEnrReportDefinition> reportList)
    {
        this.reportList = reportList;
    }

    public Map<String, EnrReportBaseManager.IEnrReportDefinition> getReportMap()
    {
        return reportMap;
    }

    public void setReportMap(Map<String, EnrReportBaseManager.IEnrReportDefinition> reportMap)
    {
        this.reportMap = reportMap;
    }

    public List<EnrReportBaseManager.IEnrReportDefinition> getReportList()
    {
        return reportList;
    }
}