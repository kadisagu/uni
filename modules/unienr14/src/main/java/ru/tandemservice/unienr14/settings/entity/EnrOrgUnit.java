package ru.tandemservice.unienr14.settings.entity;

import java.util.Comparator;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.settings.entity.gen.EnrOrgUnitGen;

/**
 * Филиал в рамках ПК
 *
 * Подразделение, аккредитованное осуществлять прием.
 * Существует в рамках приемной кампании учебного года, т.е. создается, если есть аккредитация на прием в этом учебном году.
 */
public class EnrOrgUnit extends EnrOrgUnitGen implements ITitled
{
    public EnrOrgUnit() { }

    public EnrOrgUnit(EnrEnrollmentCampaign campaign, EduInstitutionOrgUnit institutionOrgUnit) {
        setEnrollmentCampaign(campaign);
        setInstitutionOrgUnit(institutionOrgUnit);
    }

    @Override
    @EntityDSLSupport(parts = EnrOrgUnit.L_INSTITUTION_ORG_UNIT + "." + EduInstitutionOrgUnit.L_ORG_UNIT + "." + OrgUnit.P_FULL_TITLE)
    public String getTitle()
    {
        if (getInstitutionOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return getInstitutionOrgUnit().getOrgUnit().getFullTitle();
    }

    @Override
    @EntityDSLSupport
    public String getDepartmentTitle(){
        return getInstitutionOrgUnit().getOrgUnit().getShortTitleWithTopEmphasized();
    }

    public static final Comparator<EnrOrgUnit> ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR = new Comparator<EnrOrgUnit>() {
        @Override public int compare(EnrOrgUnit o1, EnrOrgUnit o2) {
            final int i = Boolean.compare(o1.isTop(), o2.isTop());
            if (0 != i) { return -i; }
            return o1.getDepartmentTitle().compareTo(o2.getDepartmentTitle());
        }
    };

    public static final Comparator<Object> ENR_ORG_UNIT_DEPARTMENT_TITLE_UNCHECKED_COMPARATOR = new Comparator<Object>() {
        @Override public int compare(Object o1, Object o2) {
            if (o1 instanceof EnrOrgUnit && o2 instanceof EnrOrgUnit) {
                return ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR.compare((EnrOrgUnit) o1, (EnrOrgUnit) o2);
            }
            return 0;
        }
    };


    public boolean isTop() {
        return getInstitutionOrgUnit().getOrgUnit().isTop();
    }

    public String getLoginSafe() {
        if (getLogin() == null)
            return ApplicationRuntime.getProperty("enr14.fis.login");
        return getLogin();
    }

    public String getPasswordSafe() {
        if (getPassword() == null)
            return ApplicationRuntime.getProperty("enr14.fis.password");
        return getPassword();
    }
}