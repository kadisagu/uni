/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;

/**
 * @author oleyba
 * @since 5/3/13
 */
public class EnrEntrantDocumentViewWrapper extends ViewWrapper<IEntity>
{
    public static final String VIEW_PROPERTY_DOCUMENT_TYPE = "enrDocumentType";
    public static final String VIEW_PROPERTY_DOCUMENT_TYPE_TITLE = "enrDocumentTypeTitle";
    public static final String VIEW_PROPERTY_SERIA_AND_NUMBER = "seriaAndNumber";
    public static final String VIEW_PROPERTY_ISSUANCE_DATE = "issuanceDate";
    public static final String VIEW_PROPERTY_INFO = "info";
    public static final String VIEW_PROPERTY_REG_DATE = "regDate";
    public static final String VIEW_PROPERTY_REQUEST_ATTACHMENTS = "requests";
    public static final String VIEW_PROPERTY_WITHOUT_SCAN_COPY = "withoutScanCopy";

    public EnrEntrantDocumentViewWrapper(EnrEntrantBaseDocument document)
    {
        super(document);
        setViewProperty("title", document.getDocumentType().getTitle());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE, document.getDocumentType());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE, document.getDocumentType().getTitle());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER, UniStringUtils.joinWithSeparator(" ", document.getSeria(), document.getNumber()));
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, document.getIssuanceDate());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO, document.getInfoString());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, document.getRegistrationDate());
    }

//    public EnrEntrantDocumentViewWrapper(EnrOlympiadDiploma document)
//    {
//        super(document);
//        setViewProperty("title", document.getDocumentType().getTitle());
//        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE, document.getDocumentType());
//        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE, document.getDocumentType().getTitle());
//        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER, document.getSeriaAndNumber());
//        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, document.getIssuanceDate());
//        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO, document.getInfoString());
//        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, document.getRegistrationDate());
//    }

    public EnrEntrantDocumentViewWrapper(PersonEduDocument doc)
    {
        super(doc);
        setViewProperty("title", doc.getFullNumber());
        PersonDocumentType enrDocumentType = IUniBaseDao.instance.get().getCatalogItem(PersonDocumentType.class, PersonDocumentTypeCodes.EDU_INSTITUTION);
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE, enrDocumentType);
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE, doc.getDocumentKindTitle());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER, UniStringUtils.joinWithSeparator(" ", doc.getSeria(), doc.getNumber()));
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, doc.getIssuanceDate());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO, "Год окончания: " + doc.getYearEnd() + ", обр. орг: " + doc.getEduOrganization() + " (" + doc.getEduOrganizationAddressItem().getAddressTitle() + ")");
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, doc.getCreationDate());
    }

    public EnrEntrantDocumentViewWrapper(IdentityCard idc)
    {
        super(idc);
        setViewProperty("title", idc.getShortTitle());
        PersonDocumentType enrDocumentType = IUniBaseDao.instance.get().getCatalogItem(PersonDocumentType.class, PersonDocumentTypeCodes.INDENTITY_CARD);
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE, enrDocumentType);
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE, enrDocumentType.getTitle());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER, UniStringUtils.joinWithSeparator(" ", idc.getSeria(), idc.getNumber()));
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, idc.getIssuanceDate());
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO, getInfoString(idc));
        setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, idc.getCreationDate());
    }

    public static EnrEntrantDocumentViewWrapper wrap(IEnrEntrantRequestAttachable document)
    {
//        if (document instanceof EnrOlympiadDiploma)
//            return new EnrEntrantDocumentViewWrapper((EnrOlympiadDiploma) document);
        if (document instanceof EnrEntrantBaseDocument)
            return new EnrEntrantDocumentViewWrapper((EnrEntrantBaseDocument) document);
        if (document instanceof EnrPersonEduDocumentRel)
            return new EnrEntrantDocumentViewWrapper(((EnrPersonEduDocumentRel) document).getEduDocument());
        throw new IllegalArgumentException();
    }

    /**
     * DEV-2878
     * @param idc док. об образовании
     * @return информация о документе
     */
    public static String getInfoString(IdentityCard idc)
    {
        return idc.getCardType().getTitle() + (idc.isActive() ? " (основной)" : "");
    }
}
