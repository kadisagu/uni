/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument;

import com.google.common.collect.ImmutableList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.IPersonDocumentContextDao;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.IPersonDocumentContextManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.logic.EnrEntrantBaseDocumentDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.logic.IEnrEntrantBaseDocumentDao;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes.*;

/**
 * @author oleyba
 * @since 4/29/13
 */
@Configuration
public class EnrEntrantBaseDocumentManager extends BusinessObjectManager implements IPersonDocumentContextManager
{
    public static final String BIND_PERSON = "person";
    public static final String BIND_REQUEST_TYPE = "requestType";
    public static final String BIND_DOCUMENT_KIND = "documentKind";
    public static final String BIND_RECEIVE_EDU_LEVEL_FIRST = "receiveEduLevelFirst";

    public static final String DS_IDENTITY_CARD = "identityCardDS";
    public static final String DS_EDU_INSTITUTION = "eduDocumentDS";
    
    public static EnrEntrantBaseDocumentManager instance()
    {
        return instance(EnrEntrantBaseDocumentManager.class);
    }

    @Bean
    public IEnrEntrantBaseDocumentDao dao()
    {
        return new EnrEntrantBaseDocumentDao();
    }

    @Bean
    public UIDataSourceConfig identityCardDSConfig()
    {
        return SelectDSConfig.with(DS_IDENTITY_CARD, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.identityCardDSHandler())
            .addColumn(IdentityCard.displayableTitle().s())
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> identityCardDSHandler() {
        return new EntityComboDataSourceHandler(getName(), IdentityCard.class){

            @Override @SuppressWarnings("unchecked") protected List<IEntity> sortOptions(List<IEntity> list) {
                Collections.sort(list, new EntityComparator<>()
                    .addFirst(new EntityOrder(IdentityCard.active(), OrderDirection.desc))
                    .addLast(new EntityOrder(IdentityCard.creationDate(), OrderDirection.desc)));
                return list;
            }

            @Override protected boolean isExternalSortRequired() { return true; }
        }
            .where(IdentityCard.person(), BIND_PERSON)
            .filter(IdentityCard.seria())
            .filter(IdentityCard.number())
            .filter(IdentityCard.cardType().title())
            .pageable(false)
        ;
    }

    @Bean
    public UIDataSourceConfig eduDocumentDSConfig()
    {
        return SelectDSConfig.with(DS_EDU_INSTITUTION, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.eduDocumentDSHandler())
            .addColumn(PersonEduDocument.displayableTitle().s())
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduDocumentDSHandler() {
        return new EntityComboDataSourceHandler(getName(), PersonEduDocument.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrRequestType requestType = context.get(BIND_REQUEST_TYPE);
                Boolean receiveEduLevelFirst = context.get(BIND_RECEIVE_EDU_LEVEL_FIRST);
                if (null == requestType || receiveEduLevelFirst == null) {
                    dql.where(nothing());
                } else {
                    DQLSelectBuilder levelDQLFilter = getEduLevelDQLFilter(
                            "el",
                            property(alias, PersonEduDocument.eduDocumentKind().code()),
                            property(alias, PersonEduDocument.eduDocumentKind().canCertifyQualificationSpec()),
                            property(alias, PersonEduDocument.certifyQualificationSpec()),
                            requestType.getCode(),
                            receiveEduLevelFirst
                    );
                    levelDQLFilter.column(value(1));
                    levelDQLFilter.where(eq(property(alias, PersonEduDocument.eduLevel()), property("el")));
                    dql.where(exists(levelDQLFilter.buildQuery()));
                }
            }
        }
            .where(PersonEduDocument.person(), BIND_PERSON)
            .order(PersonEduDocument.issuanceDate(), OrderDirection.desc)
            .order(PersonEduDocument.creationDate(), OrderDirection.desc)
            .filter(PersonEduDocument.seria())
            .filter(PersonEduDocument.number())
            .filter(PersonEduDocument.eduDocumentKind().title())
        ;
    }

    /**
     * DQL запрос отбора EduLevel по условиям приема.
     * Для фильтрации документов об образовании по уровню. Это может быть документ об образовани персоны или онлайн-абитуриента.
     * Применяется при выборе документа об образовании на форме добавления/редактирования заявления
     * и на форме выбора конкурсов в мастере добавления заявления.
     *
     * @param eduLevAlias            алиас, который следует присвоить виду образования
     * @param eduDocKindCodeProperty поле с кодом вида документа (поле-ссылка на EduDocumentKind.code). Если null, то в условии не участвует.
     * @param canCertifyQualificationSpec поле «Может подтверждать квалификацию дипломированного специалиста»  (поле-ссылка на EduDocumentKind.canCertifyQualificationSpec). Если null, то в условии не участвует.
     * @param certifyQualificationSpec  поле «Подтверждает квалификацию дипломированного специалиста» (поле булево значение). Если null, то в условии не участвует.
     * @param requestTypeCode        код вида заявления абитуриента
     * @param receiveEduLevelFirst   флаг "Получает образование данного уровня впервые"
     * @return dql-запрос к EduLevel отфильтрованный в соответствии с видом заявления и флагом о первом поступлении (в соответствии с правилами приему)
     */
    public static DQLSelectBuilder getEduLevelDQLFilter(String eduLevAlias, IDQLExpression eduDocKindCodeProperty, IDQLExpression canCertifyQualificationSpec, IDQLExpression certifyQualificationSpec,
                                                        String requestTypeCode, boolean receiveEduLevelFirst)
    {
        // DEV-6939, DEV-7543, DEV-7954
        // всё это зло и некрасиво. Но началось с аналитики, поэтому исправлять надо сначала в головах, потом в коде.

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduLevel.class, eduLevAlias);
        final List<String> allowedCodes;
        IDQLExpression additionalExpr = null;

        switch (requestTypeCode)
        {
            case EnrRequestTypeCodes.BS:
            {
                if (receiveEduLevelFirst)
                    allowedCodes =  ImmutableList.of(SREDNEE_OBTSHEE_OBRAZOVANIE,
                                                     SREDNEE_PROFESSIONALNOE_OBRAZOVANIE,
                                                     VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);
                else
                    allowedCodes = ImmutableList.of(SREDNEE_OBTSHEE_OBRAZOVANIE,
                                                    SREDNEE_PROFESSIONALNOE_OBRAZOVANIE,
                                                    VYSSHEE_OBRAZOVANIE_BAKALAVRIAT,
                                                    VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA,
                                                    VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);
                break;
            }
            case EnrRequestTypeCodes.SPO:
            {
                if (receiveEduLevelFirst)
                {
                    allowedCodes = ImmutableList.of(OSNOVNOE_OBTSHEE_OBRAZOVANIE,
                                                    SREDNEE_OBTSHEE_OBRAZOVANIE,
                                                    VYSSHEE_OBRAZOVANIE_BAKALAVRIAT,
                                                    VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA,
                                                    VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);

                    // Среднее профессиональное образование, если вид документа об образовании — «Диплом о начальном профессиональном образовании»
                    additionalExpr = and(
                            eq(property(eduLevAlias, EduLevel.code()), value(SREDNEE_PROFESSIONALNOE_OBRAZOVANIE)),
                            eduDocKindCodeProperty != null ? eq(eduDocKindCodeProperty, value(EduDocumentKindCodes.DIPLOM_NPO)) : null
                    );
                }
                else
                    allowedCodes = ImmutableList.of(OSNOVNOE_OBTSHEE_OBRAZOVANIE,
                                                    SREDNEE_OBTSHEE_OBRAZOVANIE,
                                                    SREDNEE_PROFESSIONALNOE_OBRAZOVANIE,
                                                    VYSSHEE_OBRAZOVANIE_BAKALAVRIAT,
                                                    VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA,
                                                    VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);
                break;
            }
            case EnrRequestTypeCodes.MASTER:
            {
                if (receiveEduLevelFirst)
                {
                    allowedCodes = ImmutableList.of(VYSSHEE_OBRAZOVANIE_BAKALAVRIAT,
                                                    VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);

                    // Высшее образование - специалитет, магистратура, если вид документа об образовании — «Диплом специалиста» и поле «квалификация» (qualification)
                    // документа содержит фразу «дипломированный специалист» (искать вне зависимости от регистра)
                    additionalExpr = and(
                            eq(property(eduLevAlias, EduLevel.code()), value(VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA)),
                            canCertifyQualificationSpec != null ? eq(canCertifyQualificationSpec, value(Boolean.TRUE)) : null,
                            certifyQualificationSpec != null ? eq(certifyQualificationSpec, value(Boolean.TRUE)) : null
                    );
                }
                else
                {
                    allowedCodes = ImmutableList.of(VYSSHEE_OBRAZOVANIE_BAKALAVRIAT,
                                                    VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA,
                                                    VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);
                }
                break;
            }
            case EnrRequestTypeCodes.HIGHER:
            case EnrRequestTypeCodes.POSTGRADUATE:
            case EnrRequestTypeCodes.TRAINEESHIP:
            case EnrRequestTypeCodes.INTERNSHIP:
            {
                if (receiveEduLevelFirst)
                    allowedCodes = ImmutableList.of(VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA);
                else
                    allowedCodes = ImmutableList.of(VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA,
                                                    VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);
                break;
            }
            default:
                throw new IllegalArgumentException();
        }

        dql.where(or(
                in(property(eduLevAlias, EduLevel.code()), allowedCodes),
                additionalExpr
        ));
        return dql;
    }

    @Bean
    @Override
    public IPersonDocumentContextDao contextDao()
    {
        return dao();
    }

}
