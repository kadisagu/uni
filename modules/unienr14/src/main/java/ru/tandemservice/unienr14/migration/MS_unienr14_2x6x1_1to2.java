package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrCampaignEntrantDocument

        // создано обязательное свойство benefitNoExams
        {
            // создать колонку
            tool.createColumn("enr14_camp_entrant_doc_t", new DBColumn("benefitnoexams_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitnoexams_p=? where benefitnoexams_p is null", Boolean.FALSE);
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitnoexams_p=? where documenttype_id in (select id from enr14_c_entrant_doc_type_t where code_p in (?))", Boolean.TRUE, "2"); // PersonDocumentTypeCodes.OLYMP_DIPLOMA = "2"

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_camp_entrant_doc_t", "benefitnoexams_p", false);

        }

        // создано обязательное свойство benefitExclusive
        {
            // создать колонку
            tool.createColumn("enr14_camp_entrant_doc_t", new DBColumn("benefitexclusive_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitexclusive_p=? where benefitexclusive_p is null", Boolean.FALSE);
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitexclusive_p=? where documenttype_id in (select id from enr14_c_entrant_doc_type_t where code_p in (?,?,?))", Boolean.TRUE, "5", "6", "7"); // PersonDocumentTypeCodes.DISABLED_CERT = "5", PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT = "6", PersonDocumentTypeCodes.PSY_CERT = "7"

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_camp_entrant_doc_t", "benefitexclusive_p", false);

        }

        // создано обязательное свойство benefitPreference
        {
            // создать колонку
            tool.createColumn("enr14_camp_entrant_doc_t", new DBColumn("benefitpreference_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitpreference_p=? where benefitpreference_p is null", Boolean.FALSE);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_camp_entrant_doc_t", "benefitpreference_p", false);

        }

        // создано обязательное свойство benefitMaxMark
        {
            // создать колонку
            tool.createColumn("enr14_camp_entrant_doc_t", new DBColumn("benefitmaxmark_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitmaxmark_p=? where benefitmaxmark_p is null", Boolean.FALSE);
            tool.executeUpdate("update enr14_camp_entrant_doc_t set benefitmaxmark_p=? where documenttype_id in (select id from enr14_c_entrant_doc_type_t where code_p in (?))", Boolean.TRUE, "2"); // PersonDocumentTypeCodes.OLYMP_DIPLOMA = "2"

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_camp_entrant_doc_t", "benefitmaxmark_p", false);

        }

        // удалено свойство benefit
        {
            // удалить колонку
            tool.dropColumn("enr14_camp_entrant_doc_t", "benefit_p");
        }
    }
}