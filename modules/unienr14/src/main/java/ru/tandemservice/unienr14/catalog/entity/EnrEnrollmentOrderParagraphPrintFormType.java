package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/**
 * Печатные формы параграфов приказов о зачислении
 */
public class EnrEnrollmentOrderParagraphPrintFormType extends EnrEnrollmentOrderParagraphPrintFormTypeGen implements ITemplateDocument
{
    @Override public byte[] getContent(){ return CommonBaseUtil.getTemplateContent(this); }
}