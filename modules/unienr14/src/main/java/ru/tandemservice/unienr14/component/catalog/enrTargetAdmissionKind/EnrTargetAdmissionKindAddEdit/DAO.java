/**
 *$Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrTargetAdmissionKind.EnrTargetAdmissionKindAddEdit;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class DAO extends DefaultCatalogAddEditDAO<EnrTargetAdmissionKind, Model> implements IDAO
{
    @Override
    public void update(Model model)
    {
        EnrTargetAdmissionKind catalogItem = model.getCatalogItem();

        if (catalogItem.getId() == null)
        {
            final List<Integer> priorityList = new DQLSelectBuilder().fromEntity(EnrTargetAdmissionKind.class, "b").column(property(EnrTargetAdmissionKind.priority().fromAlias("b")))
                    .createStatement(getSession()).list();

            int priority = 1;
            while (priorityList.contains(priority))
                priority++;

            catalogItem.setPriority(priority);
        }

        super.update(model);
    }
}
