/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 6/25/14
 */
public class EnrExtViewProvider4ReqProgram extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrRequestedProgram.class, "rp");

        column(dql, property("rp", EnrRequestedProgram.id()), "itemId").comment("uid записи");
        column(dql, property("rp", EnrRequestedProgram.requestedCompetition().id()), "reqCompId").comment("uid выбранного конкурса");
        column(dql, property("rp", EnrRequestedProgram.programSetItem().id()), "programSetItemId").comment("uid образовательной программы в наборе");
        column(dql, property("rp", EnrRequestedProgram.priority()), "priority").comment("приоритет");
        column(dql, property("rp", EnrRequestedProgram.regDate()), "regDate").comment("дата регистрации");

        return dql;
    }
}


