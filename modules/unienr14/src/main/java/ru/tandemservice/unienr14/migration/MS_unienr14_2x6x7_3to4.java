package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x7_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetition

		// создано обязательное свойство acceleratedLearning
		{
			// создать колонку
			tool.createColumn("enr14_requested_comp_t", new DBColumn("acceleratedlearning_p", DBType.BOOLEAN));


			// задать значение по умолчанию
			java.lang.Boolean defaultAcceleratedLearning = false;
			tool.executeUpdate("update enr14_requested_comp_t set acceleratedlearning_p=? where acceleratedlearning_p is null", defaultAcceleratedLearning);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_requested_comp_t", "acceleratedlearning_p", false);

		}


    }
}