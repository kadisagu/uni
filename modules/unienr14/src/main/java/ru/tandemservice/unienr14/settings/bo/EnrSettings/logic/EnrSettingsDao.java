/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.logic;

import com.beust.jcommander.internal.Lists;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.competition.entity.gen.IEnrollmentStudentSettingsGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/15/14
 */
public class EnrSettingsDao extends UniBaseDao implements IEnrSettingsDao
{
    @Override
    public void doAutoFillEduOu(EnrEnrollmentCampaign campaign)
    {
        List<IEnrollmentStudentSettings> settingsList = new DQLSelectBuilder()
            .fromEntity(IEnrollmentStudentSettings.class, "i")
            .where(eq(property("i", IEnrollmentStudentSettingsGen.programSet().enrollmentCampaign()), value(campaign)))
            .where(isNull(property("i", IEnrollmentStudentSettingsGen.educationOrgUnit())))
            .createStatement(getSession()).list();

        DevelopCondition developCondition = DataAccessServices.dao().get(DevelopCondition.class, DevelopCondition.code(), DevelopConditionCodes.FULL_PERIOD);

        for (IEnrollmentStudentSettings settings : settingsList) {
            doFillEduOu(settings, developCondition);
        }
    }

    @SuppressWarnings("unchecked")
    private void doFillEduOu(IEnrollmentStudentSettings settings, DevelopCondition developCondition) {
        EnrProgramSetBase programSet = settings.getProgramSet();
        OrgUnit terrOu = settings.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();

        EduProgramProf prof;
        EducationLevels educationLevel;
        List<EducationOrgUnit> eduOuList;
        OrgUnit ownerOrgUnit = settings.getFormativeOrgUnit();
        //boolean setSpecialization = false;

//        if (EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII.equals(programSet.getProgramKind().getEduLevel().getCode())) {
//            return;
//        }

        if (programSet instanceof EnrProgramSetSecondary) {
            prof = ((EnrProgramSetSecondary) programSet).getProgram();
            EduProgramSecondaryProf programSecondaryProf = ((EnrProgramSetSecondary) programSet).getProgram();
//            educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4Middle(prof.getProgramSubject(), programSecondaryProf.isInDepthStudy());
            ownerOrgUnit = prof.getOwnerOrgUnit().getOrgUnit();
            eduOuList = getEducationOrgUnits(programSecondaryProf, settings.getFormativeOrgUnit(), terrOu, ownerOrgUnit, true);
        } else if (settings.getProgram() != null) {
            prof = settings.getProgram();
//            educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4High(prof.getProgramSubject(), settings.getProgram().getProgramSpecialization());
            ownerOrgUnit = prof.getOwnerOrgUnit().getOrgUnit();
            eduOuList = getEducationOrgUnits(settings.getProgram(), settings.getFormativeOrgUnit(), terrOu, ownerOrgUnit, true);
        } else {
//            List<EnrProgramSetItemOrgUnitPlan> plans = getList(EnrProgramSetItemOrgUnitPlan.class, EnrProgramSetItemOrgUnitPlan.programSet(), programSet);
//            if (!plans.isEmpty()) return;
//
            List<EnrProgramSetItem> items = getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSet, EnrProgramSetItem.program().shortTitle().s());
            if (items.isEmpty() || items.size() > 1) return;
            EduProgramHigherProf higherProf = items.get(0).getProgram();
            prof = higherProf;
//            educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4High(prof.getProgramSubject(), higherProf.getProgramSpecialization());
            ownerOrgUnit = prof.getOwnerOrgUnit().getOrgUnit();
            eduOuList = getEducationOrgUnits(higherProf, settings.getFormativeOrgUnit(), terrOu, ownerOrgUnit, true);

//            items.remove(0);
//
//            Set<OrgUnit> ownerOrgUnits = new HashSet<>(CollectionUtils.collect(items, programSetItem -> programSetItem.getProgram().getOwnerOrgUnit().getOrgUnit()));
//            if (ownerOrgUnits.size() == 1) ownerOrgUnit = ownerOrgUnits.iterator().next();
//            for (EnrProgramSetItem item : items) {
//                if (!prof.getDuration().equals(item.getProgram().getDuration())) return;
//                if (!higherProf.getProgramQualification().equals(item.getProgram().getProgramQualification())) return;
//                if (!Objects.equals(higherProf.getProgramOrientation(), item.getProgram().getProgramOrientation())) return;
//                if (prof.getEduProgramTrait() == null && item.getProgram().getEduProgramTrait() != null) return;
//                if (prof.getEduProgramTrait() != null && !prof.getEduProgramTrait().equals(item.getProgram().getEduProgramTrait())) return;
//            }
//
//            educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4High(prof.getProgramSubject(), null);
//            eduOuList = getEducationOrgUnits(higherProf, terrOu, ownerOrgUnit, true);
        }

        if (eduOuList == null) return;

        if (eduOuList.size() == 1) {
            updateSettingsItem(settings, eduOuList.get(0));
            return;
        } else if (eduOuList.size() > 1) {
            return;
        }

        DQLSelectBuilder eduHsDql = new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "e")
                .where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(ownerOrgUnit)))
                .where(eq(property("e", EducationLevelsHighSchool.educationLevel().eduProgramSubject()), value(prof.getProgramSubject())))
                .order(property("e", EducationLevelsHighSchool.displayableTitle().s()));

        if (prof instanceof EduProgramSecondaryProf) {
            eduHsDql.where(eq(property("e", EducationLevelsHighSchool.educationLevel().qualification().code()), value(((EduProgramSecondaryProf) prof).isInDepthStudy() ? QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O : QualificationsCodes.BAZOVYY_UROVEN_S_P_O)));
        }
        else
        {
            eduHsDql.fetchPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().eduProgramSpecialization().fromAlias("e"), "espec")
                    .where(eq(property("e", EducationLevelsHighSchool.assignedQualification()), value(((EduProgramHigherProf) prof).getProgramQualification())))
                    .where(eq(property("e", EducationLevelsHighSchool.programOrientation()), value(((EduProgramHigherProf) prof).getProgramOrientation())))
                    .where(or(
                            instanceOf("espec", EduProgramSpecializationRoot.class),
                            eq(property("e", EducationLevelsHighSchool.educationLevel().eduProgramSpecialization()), value(((EduProgramHigherProf) prof).getProgramSpecialization()))

                    ));
        }

        List<EducationLevelsHighSchool> eduHsList = eduHsDql.createStatement(getSession()).list();
        if (eduHsList.size() > 1) return;

        EducationLevelsHighSchool eduHs = null;

        if (eduHsList.size() == 1) {
            eduHs = eduHsList.get(0);
        } else if (eduHsList.isEmpty())
        {
            eduHs = new EducationLevelsHighSchool();
            eduHs.setOrgUnit(ownerOrgUnit);
//            eduHs.setTitle(prof.getProgramSubject().getTitle());
//            eduHs.setShortTitle(prof.getShortTitle());
            eduHs.setCode("eduHs."+prof.getProgramSubject().getCode()+"."+Long.toString(System.currentTimeMillis(),32));
            eduHs.setAssignedQualification(prof.getProgramQualification());

            if (prof instanceof EduProgramHigherProf) {
                eduHs.setProgramOrientation(((EduProgramHigherProf) prof).getProgramOrientation());
                eduHs.setEducationLevel(UniEduProgramManager.instance().syncDao().createOrGetEduLvl4High(prof.getProgramSubject(),  ((EduProgramHigherProf) prof).getProgramSpecialization() instanceof EduProgramSpecializationRoot ? null : ((EduProgramHigherProf) prof).getProgramSpecialization()));
            }
            else {
                eduHs.setEducationLevel(UniEduProgramManager.instance().syncDao().createOrGetEduLvl4Middle(prof.getProgramSubject(), ((EduProgramSecondaryProf) prof).isInDepthStudy()));
            }
            eduHs.setAllowStudents(eduHs.getEducationLevel().getLevelType().isAllowStudents());
            eduHs.setTitle(eduHs.getEducationLevel().getTitle());
            eduHs.setShortTitle(eduHs.getEducationLevel().getShortTitle() == null? eduHs.getEducationLevel().getTitle() : eduHs.getEducationLevel().getShortTitle());

            UniEduProgramManager.instance().syncDao().doSaveEducationLevelsHighSchool(eduHs);
        }

        List<DevelopForm> developFormList = IUniBaseDao.instance.get().getList(DevelopForm.class, DevelopForm.programForm(), prof.getForm(), DevelopForm.code().s());
        List<DevelopTech> developTechList = IUniBaseDao.instance.get().getList(DevelopTech.class, DevelopTech.programTrait(), prof.getEduProgramTrait(), DevelopTech.code().s());
        List<DevelopPeriod> developPeriodList = IUniBaseDao.instance.get().getList(DevelopPeriod.class, DevelopPeriod.eduProgramDuration(), prof.getDuration(), DevelopPeriod.P_PRIORITY);

        if (developFormList.size() != 1 || developTechList.size() != 1 || developPeriodList.size() != 1) return;

        EducationOrgUnit eduOu = new EducationOrgUnit();
        eduOu.setEducationLevelHighSchool(eduHs);
        eduOu.setFormativeOrgUnit(settings.getFormativeOrgUnit());
        eduOu.setTerritorialOrgUnit(terrOu);
        eduOu.setDevelopCondition(developCondition);
        eduOu.setDevelopForm(developFormList.get(0));
        eduOu.setDevelopTech(developTechList.get(0));
        eduOu.setDevelopPeriod(developPeriodList.get(0));

        eduOu.setCode(0);
        eduOu.setShortTitle(prof.getShortTitle());

        EducationOrgUnit sameKeyEduOu = UniEduProgramManager.instance().dao().getWithSameKey(eduOu);
        if (sameKeyEduOu != null) {
            eduOu = sameKeyEduOu;
        } else {
            save(eduOu);
        }

        updateSettingsItem(settings, eduOu);

        getSession().flush();
    }

    public List<EducationOrgUnit> getEduOuList(IEnrollmentStudentSettings settings) {
        //берем Набор ОП для приема
        EnrProgramSetBase programSet = settings.getProgramSet();

        List<EducationOrgUnit> orgUnitList;

        if (programSet instanceof EnrProgramSetSecondary) {
            EduProgramSecondaryProf programSecondaryProf = ((EnrProgramSetSecondary) programSet).getProgram();
            Set<EducationOrgUnit> orgUnits = new HashSet<>();
            orgUnits.addAll(getEducationOrgUnits(programSecondaryProf, null, null, programSecondaryProf.getOwnerOrgUnit().getOrgUnit(), false));
            orgUnits.addAll(getEducationOrgUnits(programSecondaryProf, null, null, settings.getFormativeOrgUnit(), false));
            orgUnitList = Lists.newArrayList();
            orgUnitList.addAll(orgUnits);

        } else if (settings.getProgram() != null) {
            Set<EducationOrgUnit> orgUnits = new HashSet<>();
            orgUnits.addAll(getEducationOrgUnits(settings.getProgram(), null, null, settings.getProgram().getOwnerOrgUnit().getOrgUnit(), false));
            orgUnits.addAll(getEducationOrgUnits(settings.getProgram(), null, null, settings.getFormativeOrgUnit(), false));
            orgUnitList = new ArrayList<>();
            orgUnitList.addAll(orgUnits);
        } else {
            Set<EducationOrgUnit> orgUnits = new HashSet<>();

            List<EnrProgramSetItem> items = getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSet, EnrProgramSetItem.program().shortTitle().s());
            for (EnrProgramSetItem item : items) {
                orgUnits.addAll(getEducationOrgUnits(item.getProgram(), null, null, item.getProgram().getOwnerOrgUnit().getOrgUnit(), false));
            }

            if (!items.isEmpty()) {
                orgUnits.addAll(getEducationOrgUnits(items.get(0).getProgram(), null, null, settings.getFormativeOrgUnit(), false));
            }

            orgUnitList = new ArrayList<>();
            orgUnitList.addAll(orgUnits);
        }

        Collections.sort(orgUnitList, new EntityComparator<>(
                new EntityOrder(EducationOrgUnit.title().s()),
                new EntityOrder(EducationOrgUnit.formativeOrgUnit().shortTitle().s()),
                new EntityOrder(EducationOrgUnit.territorialOrgUnit().shortTitle().s()),
                new EntityOrder(EducationOrgUnit.developForm().code().s()),
                new EntityOrder(EducationOrgUnit.developCondition().code().s()),
                new EntityOrder(EducationOrgUnit.developTech().code().s()),
                new EntityOrder(EducationOrgUnit.developPeriod().priority().s()),
                new EntityOrder(EducationOrgUnit.id().s())
        ));
        return orgUnitList;
    }

    @Override
    public boolean checkAccountingRulesAgreementBudget(EnrEnrollmentCampaign campaign)
    {
        return existsEntity(new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                .column(property("rc", EnrRequestedCompetition.request().type().id()))
                .column(property("rc", EnrRequestedCompetition.request().entrant().id()))
                .column(DQLFunctions.count(property("rc", EnrRequestedCompetition.id())))
                .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(true)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().enrollmentCampaign()), value(campaign)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(UniDefines.COMPENSATION_TYPE_BUDGET)))
                .group(property("rc", EnrRequestedCompetition.request().type().id()))
                .group(property("rc", EnrRequestedCompetition.request().entrant().id()))
                .having(gt(DQLFunctions.count(property("rc", EnrRequestedCompetition.id())), value(1))).buildQuery());
    }

    @Override
    public boolean checkAccountingRulesAgreementContract(EnrEnrollmentCampaign campaign)
    {
        return existsEntity(new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                .column(property("rc", EnrRequestedCompetition.request().type().id()))
                .column(property("rc", EnrRequestedCompetition.request().entrant().id()))
                .column(DQLFunctions.count(property("rc", EnrRequestedCompetition.id())))
                .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(true)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().enrollmentCampaign()), value(campaign)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
                .group(property("rc", EnrRequestedCompetition.request().type().id()))
                .group(property("rc", EnrRequestedCompetition.request().entrant().id()))
                .having(gt(DQLFunctions.count(property("rc", EnrRequestedCompetition.id())), value(1))).buildQuery());
    }

    @Override
    public void updateSettingsItem(IEnrollmentStudentSettings settings, EducationOrgUnit eduOU)
    {
        // У НПВ выставляем признак набора студентов
        if (!eduOU.getEducationLevelHighSchool().isAllowStudents())
        {
            eduOU.getEducationLevelHighSchool().setAllowStudents(true);
            update(eduOU.getEducationLevelHighSchool());
        }
        //НПП должно быть используемым
        if (!eduOU.isUsed())
        {
            eduOU.setUsed(true);
            update(eduOU);
        }

        settings.setEducationOrgUnit(eduOU);
        update(settings);
    }

    private List<EducationOrgUnit> getEducationOrgUnits(EduProgramHigherProf program, OrgUnit formOu, OrgUnit terrOu, OrgUnit ownerOu, boolean isAutoFill)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
            .fetchPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization().fromAlias("e"), "espec")
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(ownerOu)))
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()), value(program.getProgramSubject())))
            .where(eq(property("e", EducationOrgUnit.developForm().programForm()), value(program.getForm())))
            .where(eq(property("e", EducationOrgUnit.developPeriod().eduProgramDuration()), value(program.getDuration())))
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().assignedQualification()), value(program.getProgramQualification())))
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().programOrientation()), value(program.getProgramOrientation())));

        if(isAutoFill && formOu != null)
        {
            dql.where(eq(property("e", EducationOrgUnit.formativeOrgUnit()), value(formOu)));
        }

        if (program.getEduProgramTrait() != null) {
            dql.where(eq(property("e", EducationOrgUnit.developTech().programTrait()), value(program.getEduProgramTrait())));
        } else {
            dql.where(isNull(property("e", EducationOrgUnit.developTech().programTrait())));
        }

        if(isAutoFill)
        {
            dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization()), value(program.getProgramSpecialization())));
        }
        else
        {
            dql.where(or(
                    instanceOf("espec", EduProgramSpecializationRoot.class),
                    eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization()), value(program.getProgramSpecialization()))

            ));
        }

        if (null != terrOu) {
            dql.where(eq(property("e", EducationOrgUnit.territorialOrgUnit()), value(terrOu)));
        }
        return dql.createStatement(getSession()).list();
    }

    //Получение НПП
    private List<EducationOrgUnit> getEducationOrgUnits(EduProgramSecondaryProf program, OrgUnit formOu, OrgUnit terrOu, OrgUnit ownerOu, boolean isAutoFill)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(ownerOu)))
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()), value(program.getProgramSubject())))
            .where(eq(property("e", EducationOrgUnit.developForm().programForm()), value(program.getForm())))
            .where(eq(property("e", EducationOrgUnit.developPeriod().eduProgramDuration()), value(program.getDuration())))
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().code()), value(program.isInDepthStudy() ? QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O : QualificationsCodes.BAZOVYY_UROVEN_S_P_O)));

        if(isAutoFill && formOu != null)
        {
            dql.where(eq(property("e", EducationOrgUnit.formativeOrgUnit()), value(formOu)));
        }

        if (program.getEduProgramTrait() != null) {
            dql.where(eq(property("e", EducationOrgUnit.developTech().programTrait()), value(program.getEduProgramTrait())));
        } else {
            dql.where(isNull(property("e", EducationOrgUnit.developTech().programTrait())));
        }

        if (null != terrOu) {
            dql.where(eq(property("e", EducationOrgUnit.territorialOrgUnit()), value(terrOu)));
        }

        return dql.createStatement(getSession()).list();
    }

    @Override
    public void doClearEduOu(Long id)
    {
        IEnrollmentStudentSettings settings = getNotNull(id);
        settings.setEducationOrgUnit(null);
        update(settings);
    }
}