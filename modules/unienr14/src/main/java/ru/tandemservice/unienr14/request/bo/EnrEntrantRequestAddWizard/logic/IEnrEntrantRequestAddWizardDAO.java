/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.SportType;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.EnrEntrantAchievementVO;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.EnrEntrantStateExamVO;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.PersonSportAchievementVO;

import java.util.List;

/**
 * @author nvankov
 * @since 6/9/14
 */
public interface IEnrEntrantRequestAddWizardDAO extends INeedPersistenceSupport
{
    List<PersonEduDocument> getEduDocumentsFilteredByRequestType(Long personId, String requestTypeCode, boolean receiveEduLevelFirst);

    List<EnrOnlineEntrantEduDocument> getOnlineEntrantEduDocumentsFilteredByRequestType(Long onlineEntrantId, String requestTypeCode, boolean receiveEduLevelFirst);

    List<IdentityCard> getIdentityCardsForPerson(Long personId);

    boolean eduDocAppliedForRequest(Long eduDocumentId);

    List<EnrEntrantStateExamVO> prepareStateExamList(EnrEntrantWizardState state);

    void saveStateExamList(EnrEntrantWizardState state, List<EnrEntrantStateExamVO> exams);

    List<EnrEntrantAchievementVO> prepareIndividualAchievementsList(EnrEntrantWizardState state);

    void saveIndividualAchievementsList(EnrEntrantWizardState state, List<EnrEntrantAchievementVO> achievements);

    List<PersonSportAchievementVO> prepareSportAchievementsList(EnrEntrantWizardState state);

    void saveSportAchievementsList(EnrEntrantWizardState state, List<PersonSportAchievementVO> achievements);

    List<SportType> listSportKinds(Long personId);
}
