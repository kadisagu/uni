/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ActionDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic.EnrAllocationExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author nvankov
 * @since 8/8/14
 */
@Configuration
public class EnrAllocationParagraphPub extends BusinessComponentManager
{
    public static final String ALLOCATION_EXTRACT_DS = "allocationExtractDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(ALLOCATION_EXTRACT_DS, allocationExtractDS(), allocationExtractDSHandler()))
                .create();
    }

    /**
     * Создается точка расширения "Список выписок о зачислении в карточке параграфа", единая
     * для всех публикаторов параграфов о зачислении
     *
     * @return точка расширения "Список выписок о зачислении в карточке параграфа"
     */
    @Bean
    public ColumnListExtPoint allocationExtractDS()
    {
        IColumnListExtPointBuilder columnBuilder = ColumnListExtPoint.with(getName(), ALLOCATION_EXTRACT_DS)
                .addColumn(publisherColumn("fio", EnrRequestedCompetition.request().entrant().person().fullFio())
                                .entityListProperty(EnrAllocationExtract.entity().s())
//                                .primaryKeyPath(EnrAbstractExtract.paragraph().order().id())
                                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                                {
                                    @Override
                                    public String getComponentName(IEntity entity) { return EnrEntrantPub.class.getSimpleName(); }

                                    @Override
                                    public Object getParameters(IEntity entity)
                                    {
                                        return new ParametersMap()
                                                .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrRequestedCompetition) entity).getRequest().getEntrant().getId())
                                                .add("selectedTab", "ratingTab");
                                    }
                                })
                                .create()
                )
                .addColumn(textColumn(EnrAllocationExtractDSHandler.CUSTOM_STATES, EnrAllocationExtractDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
                .addColumn(textColumn("sex", EnrAllocationExtract.entity().request().entrant().person().identityCard().sex().shortTitle()).create())
                .addColumn(textColumn("passport", EnrAllocationExtract.entity().request().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(publisherColumn("enrollExtract", EnrAllocationExtractDSHandler.ENROLL_EXTRACT).formatter(new IFormatter<EnrEnrollmentExtract>()
                {
                    @Override
                    public String format(EnrEnrollmentExtract source)
                    {
                        return source.getShortTitle();
                    }
                }).publisherLinkResolver(new IPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return ((EnrEnrollmentExtract)((DataWrapper)entity).getProperty(EnrAllocationExtractDSHandler.ENROLL_EXTRACT)).getParagraph().getOrder().getId();
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EnrOrderPub.class.getSimpleName();
                    }
                }))
                .addColumn(textColumn("competition", EnrAllocationExtract.entity().competition().title()).create());

        if (Debug.isEnabled()) {
            columnBuilder.addColumn(textColumn("extractState", EnrAllocationExtract.state().title()));
        }

        return columnBuilder
                .addColumn(booleanColumn("cancelled", EnrAllocationExtract.cancelled()))
                .addColumn(publisherColumn("cancelExtract", "shortTitle")
                                .entityListProperty(EnrAllocationExtractDSHandler.CANCEL_EXTRACT)
//                                .primaryKeyPath(EnrAbstractExtract.paragraph().order().id())
                                .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                                    @Override public String getComponentName(IEntity entity) { return EnrOrderPub.class.getSimpleName(); }
                                    @Override public Object getParameters(IEntity entity) { return ((EnrCancelExtract) entity).getParagraph().getOrder().getId(); }
                                })
                                .create()
                )
                .addColumn(publisherColumn("otherExtracts", "title")
                        .entityListProperty(EnrAllocationExtractDSHandler.OTHER_EXTRACTS)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(publisherColumn("student", EnrEnrollmentExtract.student().personalNumber())
                    .primaryKeyPath(EnrEnrollmentExtract.student().id().s()))
                .addColumn(ActionDSColumn.with().name("setStudent").icon(CommonDefines.ICON_CHOOSE)
                        .listener("onClickSetStudent").visible("ui:chooseStudentVisible")
                        .permissionKey("ui:secModel.chooseStudent"))
//                        .permissionKey("ui:secModel.deleteExtract")
//                        .disabled(EnrAllocationExtractDSHandler.NO_EXCLUDE))
                .addColumn(ActionDSColumn.with().name("exclude").icon(CommonDefines.ICON_DELETE)
                                .listener("onClickDeleteExtract")
                                .alert(FormattedMessage.with().template("enrollmentExtractDS.exclude.alert").parameter(EnrAllocationExtract.entity().request().entrant().person().identityCard().fullFio().s()).create())
                                .permissionKey("ui:secModel.deleteExtract")
                                .disabled(EnrAllocationExtractDSHandler.NO_EXCLUDE).create()
                )
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> allocationExtractDSHandler()
    {
        return new EnrAllocationExtractDSHandler(getName());
    }
}



    