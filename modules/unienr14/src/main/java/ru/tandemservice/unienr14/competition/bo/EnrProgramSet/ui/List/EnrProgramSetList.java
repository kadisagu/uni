/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.EnrProgramSetSearchDSHandler;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/4/14
 */
@Configuration
public class EnrProgramSetList extends BusinessComponentManager
{
    public static final String EXAM_SET_SEARCH_DS = "programSetSearchDS";

    public static final String BIND_REQUEST_TYPE = "requestType";
    public static final String BIND_PROGRAM_FORM = "programForm";
    public static final String BIND_ORG_UNITS = "orgUnits";
    public static final String BIND_SUBJECT_CODE = "subjectCode";
    public static final String BIND_PROGRAM_SUBJECTS = "programSubjects";
    public static final String BIND_EDU_PROGRAMS = "eduPrograms";
    public static final String BIND_PROGRAM_SET_TITLE = "programSetTitle";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_2_Config())
            .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
            .addDataSource(selectDS("orgUnitDS", orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(selectDS("eduProgramDS", eduProgramDSHandler()))
            .addDataSource(searchListDS(EXAM_SET_SEARCH_DS, programSetSearchDSColumns(), programSetSearchDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint programSetSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_SET_SEARCH_DS)
            .addColumn(checkboxColumn("select"))
            .addColumn(publisherColumn("title", EnrProgramSetBS.title().s()).order())
            .addColumn(textColumn("programSubject", EnrProgramSetBS.programSubject().titleWithCode().s()).order())
            // todo добавить колонку
            // .addColumn(textColumn("programs", EnrProgramSetSearchDSHandler.PROP_PROGRAMS).formatter(CollectionFormatter.INSTANCE))
            .addColumn(textColumn("developForm", EnrProgramSetBS.programForm().title().s()).order())
            .addColumn(textColumn("programKind", EnrProgramSetBS.programSubject().subjectIndex().programKind().shortTitle().s()).order())
            // todo добавить колонку
            // .addColumn(textColumn("orgUnits", EnrProgramSetSearchDSHandler.PROP_ORG_UNITS).formatter(CollectionFormatter.INSTANCE))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditProgramSet").permissionKey("enr14ProgramSetListEditSet").visible("ui:campaign.greaterThan2014"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteProgramSet").alert(new FormattedMessage("programSetSearchDS.delete.alert", EnrProgramSetBS.title().s())).permissionKey("enr14ProgramSetListDeleteSet"))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSetSearchDSHandler()
    {
        return new EnrProgramSetSearchDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EnrRequestType requestType = context.get(BIND_REQUEST_TYPE);

                DQLSelectBuilder programSetBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps").column(property("ps", EnrProgramSetBase.id()));
                EnrProgramSetSearchDSHandler.applyFilters(programSetBuilder, "ps", campaign, requestType, null, null, null, null, null);

                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                        .where(in(property("psou", EnrProgramSetOrgUnit.programSet().id()), programSetBuilder.buildQuery()))
                        .where(eq(property("psou", EnrProgramSetOrgUnit.orgUnit().id()), property(alias, EnrOrgUnit.id())))
                        .buildQuery()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EnrRequestType requestType = context.get(BIND_REQUEST_TYPE);
                List<EnrOrgUnit> orgUnits = context.get(BIND_ORG_UNITS);

                DQLSelectBuilder programSetBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps").column(property("ps", EnrProgramSetBase.id()));
                EnrProgramSetSearchDSHandler.applyFilters(programSetBuilder, "ps", campaign, requestType, null, orgUnits, null, null, null);

                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetItem.class, "psi")
                        .where(in(property("psi", EnrProgramSetItem.programSet().id()), programSetBuilder.buildQuery()))
                        .where(eq(property("psi", EnrProgramSetItem.program().programSubject().id()), property(alias, EduProgramSubject.id())))
                        .buildQuery()
                ));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramHigherProf.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EnrRequestType requestType = context.get(BIND_REQUEST_TYPE);
                List<EnrOrgUnit> orgUnits = context.get(BIND_ORG_UNITS);
                List<EduProgramSubject> programSubjects = context.get(BIND_PROGRAM_SUBJECTS);

                DQLSelectBuilder programSetBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps").column(property("ps", EnrProgramSetBase.id()));
                EnrProgramSetSearchDSHandler.applyFilters(programSetBuilder, "ps", campaign, requestType, null, orgUnits, null, programSubjects, null);

                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetItem.class, "psi")
                        .where(in(property("psi", EnrProgramSetItem.programSet().id()), programSetBuilder.buildQuery()))
                        .where(eq(property("psi", EnrProgramSetItem.program().id()), property(alias, EduProgramHigherProf.id())))
                        .buildQuery()
                ));
            }
        }
            .filter(EduProgramHigherProf.title())
            .order(EduProgramHigherProf.title());
    }
}