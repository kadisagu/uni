/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
