/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.EnrReportStateExamEnrollmentResultsAddUI;

/**
 * @author rsizonenko
 * @since 22.07.2014
 */
public interface IEnrReportStateExamEnrollmentResultsDao extends INeedPersistenceSupport {
    long createReport(EnrReportStateExamEnrollmentResultsAddUI model);
}
