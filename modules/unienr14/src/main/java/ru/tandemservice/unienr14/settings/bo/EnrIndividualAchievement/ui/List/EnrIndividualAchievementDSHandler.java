/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 4/11/14
 */
public class EnrIndividualAchievementDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrIndividualAchievementDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrCampaignId = context.getNotNull(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        Long requestTypeId = context.getNotNull("requestType");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantAchievementType.class, "a");
        builder.column("a");
        builder.where(eq(property("a", EnrEntrantAchievementType.enrollmentCampaign().id()), value(enrCampaignId)));
        builder.where(eq(property("a", EnrEntrantAchievementType.achievementKind().requestType().id()), value(requestTypeId)));

        input.setCountRecord(5);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).preserveCountRecord().order().pageable(false).build();
    }
}
