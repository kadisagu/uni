/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.shared.person.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic.EnrOlympiadDiplomaDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit.EnrOlympiadDiplomaAddEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit.EnrOlympiadDiplomaAddEditUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;

/**
 * @author oleyba
 * @since 5/3/13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrant.id", required = true)
})
public class EnrOlympiadDiplomaListUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrOlympiadDiplomaDSHandler.BIND_ENTRANT, getEntrant());
    }

    public boolean isCurrentScanCopyDisabled()
    {
        EnrOlympiadDiploma diploma = _uiSupport.getDataSourceCurrentValue(EnrOlympiadDiplomaList.DS_DIPLOMA);
        return diploma == null || diploma.getScanCopy() == null;
    }

    public void onClickAdd() {
        _uiActivation.asRegionDialog(EnrOlympiadDiplomaAddEdit.class.getSimpleName())
//            .parameter(EnrOlympiadDiplomaAddEditUI.BIND_ENTRANT_ID, getEntrant().getId()) TODO: SH-1904
            .parameter(PUBLISHER_ID, null)
            .activate();
    }

    public void onClickDownloadScanCopy()
    {
        EnrOlympiadDiploma diploma = DataAccessServices.dao().get(getListenerParameterAsLong());
        if(diploma.getScanCopy() != null)
        {
            try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.PERSON_DOCUMENTS_SERVICE_NAME))
            {

                RemoteDocumentDTO scanCopy = service.get(diploma.getScanCopy());
                if (scanCopy != null)
                {
                    BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(scanCopy.getFileType()).fileName(scanCopy.getFileName()).document(scanCopy.getContent()), true);
                }
            }
        }
    }

    public void onClickEdit() {
        _uiActivation.asRegionDialog(EnrOlympiadDiplomaAddEdit.class.getSimpleName())
//            .parameter(EnrOlympiadDiplomaAddEditUI.BIND_ENTRANT_ID, getEntrant().getId()) TODO: SH-1904
            .parameter(PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }

    public void onClickDelete() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // presenter


    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }
}
