/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;

import java.util.Collection;

/**
 * @author oleyba
 * @since 5/16/13
 */
public interface IEnrScheduleDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет\обновляет событие в расписании.
     * @param examEvent событие в расписании ВИ
     * @return id сохраненного события
     */
    Long saveOrUpdateExamEvent(EnrExamScheduleEvent examEvent);

    /**
     * Добавляет для События в расписании ЭГ-ы.
     * Обновляет связи по текущему списку ЭГ: нужные - создаются, ненужные - удаляются, ничего не обновляется.
     * @param event событие
     * @param examGroups ЭГ-ы, которые указываются для этого События
     */
    void doAttachExamGroupEvent(EnrExamScheduleEvent event, Collection<EnrExamGroup> examGroups);

    void deleteEvent(Long eventId);
}
