package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Виды заявлений абитуриента"
 * Имя сущности : enrRequestType
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrRequestTypeCodes
{
    /** Константа кода (code) элемента : Программы бакалавриата, специалитета (title) */
    String BS = "1";
    /** Константа кода (code) элемента : Программы магистратуры (title) */
    String MASTER = "2";
    /** Константа кода (code) элемента : Направления подготовки кадров высшей квалификации (title) */
    String HIGHER = "3";
    /** Константа кода (code) элемента : Среднее профессиональное образование (title) */
    String SPO = "4";
    /** Константа кода (code) элемента : Программы аспирантуры (адъюнктуры) (title) */
    String POSTGRADUATE = "5";
    /** Константа кода (code) элемента : Программы ординатуры (title) */
    String TRAINEESHIP = "6";
    /** Константа кода (code) элемента : Программы интернатуры (title) */
    String INTERNSHIP = "7";

    Set<String> CODES = ImmutableSet.of(BS, MASTER, HIGHER, SPO, POSTGRADUATE, TRAINEESHIP, INTERNSHIP);
}
