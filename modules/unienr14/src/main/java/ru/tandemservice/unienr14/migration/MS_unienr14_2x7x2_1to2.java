package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentStepKind

		// создано обязательное свойство allowed
		{
			// создать колонку
			tool.createColumn("enr14_c_enr_step_kind_t", new DBColumn("allowed_p", DBType.BOOLEAN));

			// задать значение по умолчанию
            tool.executeUpdate("update enr14_c_enr_step_kind_t set allowed_p=? where allowed_p is null", (Boolean) false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_c_enr_step_kind_t", "allowed_p", false);

		}


    }
}