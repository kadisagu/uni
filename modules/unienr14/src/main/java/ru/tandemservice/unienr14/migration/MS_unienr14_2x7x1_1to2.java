package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrMethodDivCompetitions

		// создана новая сущность
		{
			if(!tool.tableExists("enr14_c_meth_div_comp_t"))
			{
				// создать таблицу
				DBTable dbt = new DBTable("enr14_c_meth_div_comp_t",
						new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
						new DBColumn("discriminator", DBType.SHORT).setNullable(false),
						new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
						new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
						new DBColumn("description_p", DBType.TEXT).setNullable(false),
						new DBColumn("enrrequesttype_id", DBType.LONG).setNullable(false),
						new DBColumn("title_p", DBType.createVarchar(1200))
				);
				tool.createTable(dbt);
			}
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrMethodDivCompetitions");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetBase

		// создано обязательное свойство methodDivCompetitions
		{
			// создать колонку
			tool.createColumn("enr14_program_set_base_t", new DBColumn("methoddivcompetitions_id", DBType.LONG));

			final short enrMethodDivCompetitionsCode = tool.entityCodes().get("enrMethodDivCompetitions");

			Long spoReqType = (Long) tool.getUniqueResult("select id from enr14_c_request_type_t where code_p='4'"); // EnrRequestTypeCodes.SPO = 4

			java.lang.Long spoMethodDivCompetitions = EntityIDGenerator.generateNewId(enrMethodDivCompetitionsCode);
			tool.executeUpdate(
					"insert into enr14_c_meth_div_comp_t(id, discriminator, code_p, title_p, shorttitle_p, description_p, enrrequesttype_id) values(?, ?, ?, ?, ?, ?, ?)",
					spoMethodDivCompetitions,
					enrMethodDivCompetitionsCode,
					"no_div_spo",
					"Без деления",
					"без деления",
					"Отдельные конкурсы в зависимости от уровня образования поступающих не проводятся",
					spoReqType); // String SPO = "4"

			java.lang.Long masterMethodDivCompetitions = EntityIDGenerator.generateNewId(enrMethodDivCompetitionsCode);
			tool.executeUpdate(
					"insert into enr14_c_meth_div_comp_t(id, discriminator, code_p, title_p, shorttitle_p, description_p, enrrequesttype_id) values(?, ?, ?, ?, ?, ?, ?)",
					masterMethodDivCompetitions,
					enrMethodDivCompetitionsCode,
					"no_div_master",
					"Без деления",
					"без деления",
					"Отдельные конкурсы в зависимости от уровня образования поступающих не проводятся",
					(Long) tool.getUniqueResult("select id from enr14_c_request_type_t where code_p='2'")); // EnrRequestTypeCodes.MASTER = "2"

			java.lang.Long higherMethodDivCompetitions = EntityIDGenerator.generateNewId(enrMethodDivCompetitionsCode);
			tool.executeUpdate(
					"insert into enr14_c_meth_div_comp_t(id, discriminator, code_p, title_p, shorttitle_p, description_p, enrrequesttype_id) values(?, ?, ?, ?, ?, ?, ?)",
					higherMethodDivCompetitions,
					enrMethodDivCompetitionsCode,
					"no_div_higher",
					"Без деления",
					"без деления",
					"Отдельные конкурсы в зависимости от уровня образования поступающих не проводятся",
					(Long) tool.getUniqueResult("select id from enr14_c_request_type_t where code_p='3'")); // EnrRequestTypeCodes.HIGHER = "3"

			java.lang.Long defaultBsMethodDivCompetitions = EntityIDGenerator.generateNewId(enrMethodDivCompetitionsCode);
			tool.executeUpdate(
					"insert into enr14_c_meth_div_comp_t(id, discriminator, code_p, title_p, shorttitle_p, description_p, enrrequesttype_id) values(?, ?, ?, ?, ?, ?, ?)",
					defaultBsMethodDivCompetitions,
					enrMethodDivCompetitionsCode,
					"no_div_bs",
					"Без деления",
					"без деления",
					"Отдельные конкурсы в зависимости от уровня образования поступающих не проводятся",
					(Long) tool.getUniqueResult("select id from enr14_c_request_type_t where code_p='1'")); // EnrRequestTypeCodes.BS = "1"

			java.lang.Long bsMidProfMethodDivCompetitions = EntityIDGenerator.generateNewId(enrMethodDivCompetitionsCode);
			tool.executeUpdate(
					"insert into enr14_c_meth_div_comp_t(id, discriminator, code_p, title_p, shorttitle_p, description_p, enrrequesttype_id) values(?, ?, ?, ?, ?, ?, ?)",
					bsMidProfMethodDivCompetitions,
					enrMethodDivCompetitionsCode,
					"bs_soo_po",
					"СОО отдельно от ПО",
					"СОО, ПО",
					"Отдельные конкурсы на базе среднего общего и профессионального образования",
					(Long) tool.getUniqueResult("select id from enr14_c_request_type_t where code_p='1'")); // EnrRequestTypeCodes.BS = "1"

			tool.executeUpdate("update enr14_program_set_base_t set methoddivcompetitions_id=? where methoddivcompetitions_id is null and id in (select id from enr14_program_set_sec_t)", spoMethodDivCompetitions);
			tool.executeUpdate("update enr14_program_set_base_t set methoddivcompetitions_id=? where methoddivcompetitions_id is null and id in (select id from enr14_program_set_master_t)", masterMethodDivCompetitions);
			tool.executeUpdate("update enr14_program_set_base_t set methoddivcompetitions_id=? where methoddivcompetitions_id is null and id in (select id from enr14_program_set_higher_t)", higherMethodDivCompetitions);

			ISQLTranslator translator = tool.getDialect().getSQLTranslator();

			SQLSelectQuery subQuery = new SQLSelectQuery().from(
                    SQLFrom.table("enr14_competition_t", "comp")
                            .innerJoin(SQLFrom.table("enr14_program_set_ou_t", "psou"), "comp.programsetorgunit_id=psou.id")
                            .innerJoin(SQLFrom.table("enr14_program_set_base_t", "ps"), "ps.id=psou.programset_id")
                            .innerJoin(SQLFrom.table("enr14_c_lev_req_t", "lr"), "lr.id=comp.edulevelrequirement_id")
                            .innerJoin(SQLFrom.table("enr14_c_comp_type_t", "ct"), "ct.id=comp.type_id")
            )
					.where("lr.code_p='2' and(ct.code_p='04' or ct.code_p='06') and psb.id=ps.id") // EnrEduLevelRequirementCodes.SOO = 2, EnrCompetitionTypeCodes.MINISTERIAL = 04, EnrCompetitionTypeCodes.CONTRACT = 06
					.column("comp.id");

            SQLSelectQuery bsExistsQuery = new SQLSelectQuery().from(
                    SQLFrom.table("enr14_program_set_bs_t", "bs")
            )
                    .where("bs.id=psb.id");

            SQLUpdateQuery updateBsMidProfQuery = new SQLUpdateQuery("enr14_program_set_base_t", "psb")
                    .where("exists(" + translator.toSql(bsExistsQuery) + ")")
                    .where("exists(" + translator.toSql(subQuery) + ")")
                    .where("psb.methoddivcompetitions_id is null")
                    .set("methoddivcompetitions_id", "?");

			tool.executeUpdate(translator.toSql(updateBsMidProfQuery), bsMidProfMethodDivCompetitions);

			SQLUpdateQuery updateDefaultBSQuery = new SQLUpdateQuery("enr14_program_set_base_t", "psb")
                    .where("exists(" + translator.toSql(bsExistsQuery) + ")")
                    .where("not exists(" + translator.toSql(subQuery) + ")")
                    .where("psb.methoddivcompetitions_id is null")
                    .set("methoddivcompetitions_id", "?");

			tool.executeUpdate(translator.toSql(updateDefaultBSQuery), defaultBsMethodDivCompetitions);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_program_set_base_t", "methoddivcompetitions_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetBS

		// создано свойство enrExamSetVariantMidBase
		// создано свойство enrExamSetVariantMidProfBase
		// создано свойство enrExamSetVariantHighBase
		{
			// создать колонку
			tool.createColumn("enr14_program_set_bs_t", new DBColumn("enrexamsetvariantmidbase_id", DBType.LONG));

			tool.executeUpdate("update enr14_program_set_bs_t set enrexamsetvariantmidbase_id=examsetvariantminbase_id where enrexamsetvariantmidbase_id is null");

			// создать колонку
			tool.createColumn("enr14_program_set_bs_t", new DBColumn("nrexmstvrntmdprfbs_id", DBType.LONG));

			tool.executeUpdate("update enr14_program_set_bs_t set nrexmstvrntmdprfbs_id=examsetvariantminprof_id where nrexmstvrntmdprfbs_id is null");

			// создать колонку
			tool.createColumn("enr14_program_set_bs_t", new DBColumn("enrexamsetvarianthighbase_id", DBType.LONG));

			tool.executeUpdate("update enr14_program_set_bs_t set enrexamsetvarianthighbase_id=examsetvariantminprof_id where enrexamsetvarianthighbase_id is null");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetMaster

		// создано обязательное свойство examSetVariant
		{
			// создать колонку
			tool.createColumn("enr14_program_set_master_t", new DBColumn("examsetvariant_id", DBType.LONG));

			tool.executeUpdate("update enr14_program_set_master_t set examsetvariant_id=examsetvariantmin_id where examsetvariant_id is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_program_set_master_t", "examsetvariant_id", false);

		}
    }
}