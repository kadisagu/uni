package ru.tandemservice.unienr14.enrollment.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unienr14.enrollment.entity.gen.EnrEnrollmentStepItemOrderInfoGen;

/**
 * Информация о предыдущих зачислениях абитуриента для шага зачисления
 *
 * Создается при создании шага зачисления на каждую выписку, по которой абитуриент зачислен, и это зачисление актуально, т.е. приказ не отменен так или иначе, на момент создания шага зачисления.
 */
public class EnrEnrollmentStepItemOrderInfo extends EnrEnrollmentStepItemOrderInfoGen implements ITitled
{
    @Override
    public String getTitle()
    {
        if (getItem() == null) {
            return this.getClass().getSimpleName();
        }
        return "Информация о предыдущем зачислении: " + getItem().getEntity().getEntrant().getFio() + ", " + getOrderInfo() + ", шаг зачисления " + getItem().getStep().getDateStr();
    }
}