/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubDAO;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubModel;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.IEnrScriptItem;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public class DAO<T extends ICatalogItem & IScriptItem & IEnrScriptItem, Model extends ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemPub.Model<T>> extends DefaultScriptCatalogPubDAO<T, Model> implements IDAO<T, Model>
{
    public void doTogglePrintPdfScript(Long itemId)
    {
        EnrScriptItem scriptItem = get(EnrScriptItem.class, itemId);
        if(scriptItem.isPrintPdf())
            scriptItem.setPrintPdf(false);
        else
            scriptItem.setPrintPdf(true);
        update(scriptItem);
    }
}