/**
 *$Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrOlympiad.EnrOlympiadAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOlympiadTypeCodes;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 14.05.13
 */
public class DAO extends DefaultCatalogAddEditDAO<EnrOlympiad, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setEduYearModel(EducationCatalogsManager.getEducationYearModel());
        model.setOlympiadTypeModel(new LazySimpleSelectModel<EnrOlympiadType>(EnrOlympiadType.class).setSortProperty(EnrOlympiadType.code().s()));

        if (model.isAddForm()) {
            EnrEnrollmentCampaign defaultCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
            if (defaultCampaign != null)
                model.getCatalogItem().setEduYear(defaultCampaign.getEducationYear());
            model.getCatalogItem().setOlympiadType(get(EnrOlympiadType.class, EnrOlympiadType.code(), EnrOlympiadTypeCodes.OTHER));
        }
    }
}
