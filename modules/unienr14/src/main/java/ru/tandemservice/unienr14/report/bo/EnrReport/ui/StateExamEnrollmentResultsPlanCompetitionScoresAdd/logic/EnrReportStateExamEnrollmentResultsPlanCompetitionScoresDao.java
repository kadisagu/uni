package ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsPlanCompetitionScoresAdd.logic;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsPlanCompetitionScoresAdd.EnrReportStateExamEnrollmentResultsPlanCompetitionScoresAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportStateExamEnrollmentResults;
import ru.tandemservice.unienr14.report.entity.EnrReportStateExamEnrollmentResultsPlanCompetitionScores;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ilunin
 * @since 19.09.2014
 */
public class EnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao extends UniBaseDao implements IEnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao
{
    final Integer GROUP = 1;
    final Integer SUBJECT = 2;
    final Integer PROGRAM_SET = 3;

    /**
     * Найти наименьшую оценку среди рейтингов абитуриентов
     *
     * @param lowest  текущая наименьшая оценка
     * @param ratings рейтинги абитуриентов
     * @return новая наименьшая оценка
     */
    private static long findLowestMark(Long lowest, Collection<EnrRatingItem> ratings)
    {
        for (EnrRatingItem rating : ratings)
        {
            if (lowest == null)
                lowest = rating.getTotalMarkAsLong();
            lowest = Math.min(lowest, rating.getTotalMarkAsLong());
        }
        return lowest;
    }

    // посчитать план приема по конкурсам
    private static long calculateReceptionPlan(Set<EnrProgramSetOrgUnit> orgUnits, boolean budget)
    {
        long result = 0;
        for (EnrProgramSetOrgUnit orgUnit : orgUnits)
            if (budget)     // если "бюджет", то КЦП (ministerialPlan)
                result += orgUnit.getMinisterialPlan();
            else            // если "договор", то план по договору
                result += orgUnit.getContractPlan();
        return result;
    }

    @Override
    public long createReport(EnrReportStateExamEnrollmentResultsPlanCompetitionScoresAddUI model)
    {
        EnrReportStateExamEnrollmentResultsPlanCompetitionScores report = new EnrReportStateExamEnrollmentResultsPlanCompetitionScores();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportStateExamEnrollmentResults.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportStateExamEnrollmentResults.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportStateExamEnrollmentResults.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportStateExamEnrollmentResults.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportStateExamEnrollmentResults.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportStateExamEnrollmentResults.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportStateExamEnrollmentResults.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportStateExamEnrollmentResults.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportOrgUnitEnrollmentResults.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportStateExamEnrollmentResultsPlanCompetitionScoresAddUI model)
    {
        final NumberFormat formatter = new DecimalFormat("#0.00");

        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_STATE_EXAM_ENROLLMENT_RESULTS_PLAN_COMPETITION_SCORES);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly() // абитуриент не архивен
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo()) // заявления в указанном диапазоне дат
                .filter(model.getEnrollmentCampaign()); // фильтр "Приемная кампания"
        // фильтр параметров: "Вид заявления" "Вид возмещения" "Форма обучения" "Вид приема" "Филиал"
        // "Формирующее подр." "Направление, спец., профессия" "Образовательная программа"Набор образовательных программ"
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());
        // фильтр "Поступающие параллельно"
        if (model.getParallelSelector().isParallelActive())
        {
            // выбрано "учитывать только их" - выбираются только конкурсы с признаком поступления параллельно
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            // выбрано "не учитывать" - отсекаются выбранные конкурсы с признаком поступления параллельно
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        requestedCompDQL.joinEntity(requestedCompDQL.reqComp(), DQLJoinType.left, EnrRatingItem.class, "rating", eq(property(EnrRatingItem.requestedCompetition().fromAlias("rating")), property(requestedCompDQL.reqComp())));
        DQLSelectColumnNumerator numerator = new DQLSelectColumnNumerator(requestedCompDQL);
        int reqCompColumnIndex = numerator.column(property(requestedCompDQL.reqComp()));
        int ratingColumnIndex = numerator.column(property("rating"));

        Comparator<EduProgramSubject> groupComparator = new Comparator<EduProgramSubject>()
        {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2)
            {

                // сначала 2013 года
                int res = Boolean.compare(o1 instanceof EduProgramSubject2013, o2 instanceof EduProgramSubject2013);
                if (res != 0) return -res;

                // затем 2009
                res = Boolean.compare(o1 instanceof EduProgramSubject2009, o2 instanceof EduProgramSubject2009);
                if (res != 0) return -res;

                // затем 2005 (оксо)
                res = Boolean.compare(o1 instanceof EduProgramSubjectOkso, o2 instanceof EduProgramSubjectOkso);
                if (res != 0) return -res;

                String o1KindCode = o1.getEduProgramKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV) ? EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : o1.getEduProgramKind().getCode();
                String o2KindCode = o2.getEduProgramKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV) ? EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : o2.getEduProgramKind().getCode();
                res = o1KindCode.compareTo(o2KindCode);
                if (res != 0) return res;

                res = o1.getSubjectIndex().getPriority() - o2.getSubjectIndex().getPriority();
                if (res != 0) return res;

                res = o1.getSubjectIndex().getCode().compareTo(o2.getSubjectIndex().getCode());
                if (res != 0) return res;

                return o1.getGroupTitle().compareToIgnoreCase(o2.getGroupTitle());
            }
        };

        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>()
        {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2)
            {
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };


        // дерево: "укрупнённая группа" -> "направление" -> "набор ОП" -> "конкурс" -> "рейтинг абитуриента"
        Map<EduProgramSubject, Map<EduProgramSubject, Map<EnrProgramSetBase, Multimap<EnrRequestedCompetition, EnrRatingItem>>>> dataMap = new TreeMap<>(groupComparator);
        HashSet<EnrProgramSetBase> programSets = new HashSet<>();

        for (Object[] row : this.<Object[]>getList(requestedCompDQL))
        {
            EnrRequestedCompetition reqComp = (EnrRequestedCompetition) row[reqCompColumnIndex];
            EnrRatingItem rating = (EnrRatingItem) row[ratingColumnIndex];

            EduProgramSubject subject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            EnrProgramSetBase programSet = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet();
            programSets.add(programSet);

            Map<EduProgramSubject, Map<EnrProgramSetBase, Multimap<EnrRequestedCompetition, EnrRatingItem>>> groupTree = dataMap.get(subject);
            Map<EnrProgramSetBase, Multimap<EnrRequestedCompetition, EnrRatingItem>> subjectTree = null;
            Multimap<EnrRequestedCompetition, EnrRatingItem> programSetTree = null;
            if (groupTree == null)
                dataMap.put(subject, groupTree = new TreeMap<>(subjectComparator));
            else
                subjectTree = groupTree.get(subject);

            if (subjectTree == null)
                groupTree.put(subject, subjectTree = new TreeMap<>(ITitled.TITLED_COMPARATOR));
            else
                programSetTree = subjectTree.get(programSet);

            if (programSetTree == null)
                subjectTree.put(programSet, programSetTree = HashMultimap.create());
            else
                programSetTree = subjectTree.get(programSet);

            if (rating != null)
                programSetTree.put(reqComp, rating);
        }

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, "programSetItem")
                .where(in(property(EnrProgramSetItem.programSet().fromAlias("programSetItem")), programSets))
                .column(property("programSetItem"));

        List<EnrProgramSetItem> fromQueryProgramSetItems = getList(builder);

        Map<EnrProgramSetBase, Set<EnrProgramSetItem>> programSetItemsMap = SafeMap.get(HashSet.class);

        for (EnrProgramSetItem programSetItem : fromQueryProgramSetItems)
            programSetItemsMap.get(programSetItem.getProgramSet()).add(programSetItem);

        boolean budget = ((CompensationType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget();

        List<String[]> tTable = new ArrayList<>();
        List<Integer> rowsQuantifier = new ArrayList<>();
        // обработка "укрупнённой группы"
        for (Map.Entry<EduProgramSubject, Map<EduProgramSubject, Map<EnrProgramSetBase, Multimap<EnrRequestedCompetition, EnrRatingItem>>>> groupEntry : dataMap.entrySet())
        {
            // строки "укрупнённой группы" + строки группы "направление"
            List<String[]> groupTablePart = new ArrayList<>();
            // зачисленные абитуриенты уровня "укрупнённая группа"
            Set<EnrEntrant> groupEnrolledEntrants = new HashSet<>();
            // все абитуриенты подавшие заявки уровня "укрупнённая группа"
            Set<EnrEntrant> groupAllEntrants = new HashSet<>();
            // организации ведущие прием (для подсчёта плана приема)
            Set<EnrProgramSetOrgUnit> groupOrgUnits = new HashSet<>();
            List<Integer> groupQualifiersPart = new ArrayList<>();

            // обработка группы "направления подготовки"
            for (Map.Entry<EduProgramSubject, Map<EnrProgramSetBase, Multimap<EnrRequestedCompetition, EnrRatingItem>>> subjectEntry : groupEntry.getValue().entrySet())
            {
                // строки группы "направление" + строки группы "набор ОП"
                List<String[]> subjectTablePart = new ArrayList<>();
                // зачисленные абитуриенты уровня "направление"
                Set<EnrEntrant> subjectEnrolledEntrants = new HashSet<>();
                // все абитуриенты подавшие заявки уровня "направление"
                Set<EnrEntrant> subjectAllEntrants = new HashSet<>();
                // организации ведущие прием (для подсчёта плана приема)
                Set<EnrProgramSetOrgUnit> subjectOrgUnits = new HashSet<>();

                // обработка группы "набор ОП"
                for (Map.Entry<EnrProgramSetBase, Multimap<EnrRequestedCompetition, EnrRatingItem>> programSetEntry : subjectEntry.getValue().entrySet())
                {
                    Set<EnrEntrant> programSetEnrolledEntrants = new HashSet<>(); // зачисленные абитуриенты уровня "набор ОП"
                    Set<EnrEntrant> programSetAllEntrants = new HashSet<>(); // все абитуриенты подавшие заявки уровня "набор ОП"
                    Set<EnrProgramSetOrgUnit> programSetOrgUnits = new HashSet<>(); // организации ведущие прием (для подсчёта плана приема)

                    final EnrProgramSetBase key = programSetEntry.getKey();

                    final String methodDivCode = key.getMethodDivCompetitions().getCode();

                    boolean showDetails = !(methodDivCode.equals(EnrMethodDivCompetitionsCodes.BS_NO_DIV) ||
                            methodDivCode.equals(EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV) ||
                            methodDivCode.equals(EnrMethodDivCompetitionsCodes.INTERNSHIP_NO_DIV) ||
                            methodDivCode.equals(EnrMethodDivCompetitionsCodes.MASTER_NO_DIV) ||
                            methodDivCode.equals(EnrMethodDivCompetitionsCodes.POSTGRADUATE_NO_DIV) ||
                            methodDivCode.equals(EnrMethodDivCompetitionsCodes.SEC_NO_DIV) ||
                            methodDivCode.equals(EnrMethodDivCompetitionsCodes.TRAINEESHIP_NO_DIV));

                    Map<EnrEduLevelRequirement, Long> lowestMap = new HashMap<>();

                    // обработка конкурсов
                    for (Map.Entry<EnrRequestedCompetition, Collection<EnrRatingItem>> reqCompEntry : programSetEntry.getValue().asMap().entrySet())
                    {
                        EnrRequestedCompetition reqComp = reqCompEntry.getKey();
                        EnrEntrant entrant = reqComp.getRequest().getEntrant();
                        Collection<EnrRatingItem> ratings = reqCompEntry.getValue();

                        if (reqComp.getState().getCode().equals(EnrEntrantStateCodes.ENROLLED)) // конкурс имеет состояние стадии ПК - "по результатам зачисления"
                        {
                            programSetEnrolledEntrants.add(entrant);
                            EnrCompetition comp = reqComp.getCompetition();
                            // вид приема: если бюджет - то "общий конкурс", иначе - "по договору"
                            String competitionType = budget ? EnrCompetitionTypeCodes.MINISTERIAL : EnrCompetitionTypeCodes.CONTRACT;

                            if (comp.getType().getCode().equals(competitionType))
                            {
                                Long prevLowest = lowestMap.get(comp.getEduLevelRequirement());
                                lowestMap.put(comp.getEduLevelRequirement(), findLowestMark(prevLowest, ratings));
                            }
                        }
                        programSetAllEntrants.add(entrant);
                        programSetOrgUnits.add(reqComp.getCompetition().getProgramSetOrgUnit());
                    }
                    // добавить результаты в группу верхнего уровня
                    subjectAllEntrants.addAll(programSetAllEntrants);
                    subjectEnrolledEntrants.addAll(programSetEnrolledEntrants);
                    subjectOrgUnits.addAll(programSetOrgUnits);

                    // посчитать план приема уровня группы "набор ОП"
                    long receptionPlan = calculateReceptionPlan(programSetOrgUnits, budget);

                    StringBuilder sb = new StringBuilder();
                    if (lowestMap.size() > 0)
                    {
                        if (showDetails) {
                            for (Map.Entry<EnrEduLevelRequirement, Long> entry : lowestMap.entrySet()) {
                                if (entry.getValue() == null || entry.getValue().equals(0L))
                                    continue;
                                if (sb.length() > 0) sb.append(" / ");
                                sb.append(entry.getKey().getShortTitle() + " - " + (entry.getValue() / 1000));
                            }
                        }
                        else {
                            final Long min = Collections.min(lowestMap.values());
                            if (min != 0)
                                sb.append(min/1000);
                        }
                    }

                    if (sb.length() == 0)
                        sb.append("-");

                    // составить строку проходного балла
                    String passingScore = sb.toString();
                    // вычислить заголовок "набора ОП"
                    EnrProgramSetBase programSet = key;

                    // строка уровня "набор ОП"
                    ReportRow programSetRow = new ReportRow(programSet.getTitle(), receptionPlan, programSetAllEntrants.size(), programSetEnrolledEntrants.size(), passingScore, formatter);

                    subjectTablePart.add(programSetRow.cells);

                }
                // добавить результаты в группу верхнего уровня
                groupAllEntrants.addAll(subjectAllEntrants);
                groupEnrolledEntrants.addAll(subjectEnrolledEntrants);
                groupOrgUnits.addAll(subjectOrgUnits);
                // посчитать план приема уровня группы "направление"
                long subjectReceptionPlan = calculateReceptionPlan(subjectOrgUnits, budget);
                // строка уровня "направление"
                ReportRow subjectRow = new ReportRow(subjectEntry.getKey().getTitleWithCode(), subjectReceptionPlan, subjectAllEntrants.size(), subjectEnrolledEntrants.size(), "", formatter);

                groupTablePart.add(subjectRow.cells);
                groupTablePart.addAll(subjectTablePart);
                groupQualifiersPart.add(SUBJECT);
                addQualifiers(groupQualifiersPart, PROGRAM_SET, subjectTablePart.size());
            }
            // посчитать план приема уровня группы "укрупнённая группа"
            long groupReceptionPlan = calculateReceptionPlan(groupOrgUnits, budget);
            // строка уровня "укрупнённая группа"
            ReportRow groupRow = new ReportRow(groupEntry.getKey().getGroupTitle(), groupReceptionPlan, groupAllEntrants.size(), groupEnrolledEntrants.size(), "", formatter);

            tTable.add(groupRow.cells);
            tTable.addAll(groupTablePart);
            rowsQuantifier.add(GROUP);
            rowsQuantifier.addAll(groupQualifiersPart);
        }

        RtfTableModifier modifier = new RtfTableModifier();

        FilterParametersPrinter filterParametersPrinter = new FilterParametersPrinter();
        filterParametersPrinter.setParallelSelector(model.getParallelSelector());
        List<String[]> hTable = filterParametersPrinter.getTableForNonRtf(filterAddon, model.getDateSelector());

        int color = document.getHeader().getColorTable().addColor(150, 150, 150);

        modifier.put("T", tTable.toArray(new String[tTable.size()][]));

        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
        modifier.put("H", new RtfRowIntercepterRawText());

        modifier.put("T", new RowsDecorator(rowsQuantifier, color));

        modifier.modify(document);

        new RtfInjectModifier().put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(model.getDateSelector().getDateFrom())).modify(document);

        return RtfUtil.toByteArray(document);
    }

    void addQualifiers(List<Integer> qualifiers, Integer qualifier, int count)
    {
        for (int i = 0; i < count; i++)
            qualifiers.add(qualifier);
    }

    class RowsDecorator implements IRtfRowIntercepter
    {
        List<Integer> rowsQuantifier;
        int colorIndex;

        public RowsDecorator(List<Integer> rowsQuantifier, int colorIndex)
        {
            this.rowsQuantifier = rowsQuantifier;
            this.colorIndex = colorIndex;
        }

        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {
        }

        @SuppressWarnings("NumberEquality")
        @Override
        public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
        {
            Integer type = rowsQuantifier.get(rowIndex);

            if (type == GROUP || type == SUBJECT)
            {
                RtfString string = new RtfString();
                string.color(colorIndex);
                if (type == GROUP)
                    string.boldBegin().append(value).boldEnd();
                else
                    string.append(value);
                return string.toList();
            }
            return null;
        }

        @Override
        public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
        {
        }
    }

    // строка отчета
    class ReportRow
    {
        final String[] cells = new String[5];

        /**
         * @param title                название элемента (укр. группы или направления или набора ОП)
         * @param planEntrantCount     план приема
         * @param requestCount         количество заявок от студентов
         * @param enrolledEntrantCount число зачисленных студентов
         * @param passingScore         проходной балл
         */
        ReportRow(String title, long planEntrantCount, int requestCount, int enrolledEntrantCount, String passingScore, NumberFormat competitionFormatter)
        {
            cells[0] = title;
            cells[1] = String.valueOf(planEntrantCount);
            double competition = planEntrantCount == 0 ? 0 : (double) requestCount / (double) planEntrantCount;
            cells[2] = competition == 0 ? "-" : competitionFormatter.format(competition);
            cells[3] = String.valueOf(enrolledEntrantCount);
            cells[4] = passingScore;
        }
    }
}
