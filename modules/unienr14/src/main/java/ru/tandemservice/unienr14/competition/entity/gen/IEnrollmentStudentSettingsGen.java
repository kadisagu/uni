package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;

/**
 * Настройка НПП для создания студентов
 *
 * Настройка НПП для создания студента по зачисленному абитуриенту.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrollmentStudentSettingsGen extends InterfaceStubBase
 implements IEnrollmentStudentSettings{
    public static final int VERSION_HASH = -29997057;

    public static final String L_PROGRAM_SET = "programSet";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_PROGRAM = "program";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";

    private EnrProgramSetBase _programSet;
    private EnrOrgUnit _orgUnit;
    private OrgUnit _formativeOrgUnit;
    private EduProgramHigherProf _program;
    private EducationOrgUnit _educationOrgUnit;


    public EnrProgramSetBase getProgramSet()
    {
        return _programSet;
    }

    public void setProgramSet(EnrProgramSetBase programSet)
    {
        _programSet = programSet;
    }


    public EnrOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }


    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }


    public EduProgramHigherProf getProgram()
    {
        return _program;
    }

    public void setProgram(EduProgramHigherProf program)
    {
        _program = program;
    }


    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        _educationOrgUnit = educationOrgUnit;
    }

    private static final Path<IEnrollmentStudentSettings> _dslPath = new Path<IEnrollmentStudentSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings");
    }
            

    /**
     * @return Набор ОП.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getProgramSet()
     */
    public static EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Подразделение, ведущее прием.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return ОП.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getProgram()
     */
    public static EduProgramHigherProf.Path<EduProgramHigherProf> program()
    {
        return _dslPath.program();
    }

    /**
     * @return НПП.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    public static class Path<E extends IEnrollmentStudentSettings> extends EntityPath<E>
    {
        private EnrProgramSetBase.Path<EnrProgramSetBase> _programSet;
        private EnrOrgUnit.Path<EnrOrgUnit> _orgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EduProgramHigherProf.Path<EduProgramHigherProf> _program;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ОП.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getProgramSet()
     */
        public EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
        {
            if(_programSet == null )
                _programSet = new EnrProgramSetBase.Path<EnrProgramSetBase>(L_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Подразделение, ведущее прием.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return ОП.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getProgram()
     */
        public EduProgramHigherProf.Path<EduProgramHigherProf> program()
        {
            if(_program == null )
                _program = new EduProgramHigherProf.Path<EduProgramHigherProf>(L_PROGRAM, this);
            return _program;
        }

    /**
     * @return НПП.
     * @see ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

        public Class getEntityClass()
        {
            return IEnrollmentStudentSettings.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
