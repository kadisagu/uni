/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic.EnrStateExamResultDao;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic.IEnrStateExamResultDao;

/**
 * @author oleyba
 * @since 6/6/14
 */
@Configuration
public class EnrStateExamResultManager extends BusinessObjectManager
{
    public static EnrStateExamResultManager instance()
    {
        return instance(EnrStateExamResultManager.class);
    }

    @Bean
    public IEnrStateExamResultDao dao()
    {
        return new EnrStateExamResultDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler icTypeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(icTypeItemListExtPoint());
    }

    public static final Long IC_PASSPORT_RF = 1L;
    public static final Long IC_OTHER = 2L;

    @Bean
    public ItemListExtPoint<DataWrapper> icTypeItemListExtPoint()
    {
        return itemList(DataWrapper.class).
                add(IC_PASSPORT_RF.toString(), new DataWrapper(IC_PASSPORT_RF, "ui.icPassportRF")).
                add(IC_OTHER.toString(), new DataWrapper(IC_OTHER, "ui.icOther")).
                create();
    }

}
