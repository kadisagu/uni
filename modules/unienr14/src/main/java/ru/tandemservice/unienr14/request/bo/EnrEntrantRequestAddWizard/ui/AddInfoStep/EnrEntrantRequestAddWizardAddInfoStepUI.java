/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AddInfoStep;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.person.base.bo.Person.util.WorkPlaceSelectModel;
import org.tandemframework.shared.person.base.bo.Person.util.WorkPostSelectModel;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.base.entity.PersonSportAchievement;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguageSkill;
import org.tandemframework.shared.person.catalog.entity.SportRank;
import org.tandemframework.shared.person.catalog.entity.SportType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.EnrEntrantRequestAddWizardManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.PersonSportAchievementVO;

import java.util.*;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardAddInfoStepUI extends UIPresenter implements IWizardStep
{
    private EnrEntrantWizardState _state;

    private Person person;
    private EnrEntrant _entrant;

    private boolean _withForeignLanguage;
    private boolean _withForeignLanguageDisabled = false;
    private List<PersonForeignLanguage> languageList;
    private PersonForeignLanguage currentLanguage;
    private PersonForeignLanguage mainLanguage;
    private ISelectModel languageModel = new LazySimpleSelectModel<>(ForeignLanguage.class);
    private ISelectModel skillModel = new LazySimpleSelectModel<>(ForeignLanguageSkill.class);

    private List<PersonSportAchievementVO> _sportAchievements = Lists.newArrayList();
    private PersonSportAchievementVO _currentSportAchievement;
    private boolean _withSportAchievementsDisabled = false;
    private boolean _withSportAchievements;
    private List<SportRank> _sportRankList;

    private List<EnrAccessCourse> _accessCourseList;
    private List<EnrAccessDepartment> _accessDepartmentList;

    private ISelectModel _workPlaceModel;
    private ISelectModel _workPostModel;

    private ISelectModel _sourceInfoAboutUniversityListModel;

    private List<EnrSourceInfoAboutUniversity> _sourceInfoAboutUniversityList;

    public List<PersonSportAchievementVO> getSportAchievements()
    {
        return _sportAchievements;
    }

    public void setSportAchievements(List<PersonSportAchievementVO> sportAchievements)
    {
        _sportAchievements = sportAchievements;
    }

    public PersonSportAchievementVO getCurrentSportAchievement()
    {
        return _currentSportAchievement;
    }

    public void setCurrentSportAchievement(PersonSportAchievementVO currentSportAchievement)
    {
        _currentSportAchievement = currentSportAchievement;
    }

    public boolean isWithSportAchievements()
    {
        return _withSportAchievements;
    }

    public void setWithSportAchievements(boolean withSportAchievements)
    {
        _withSportAchievements = withSportAchievements;
    }

    public List<SportRank> getSportRankList()
    {
        return _sportRankList;
    }

    public void setSportRankList(List<SportRank> sportRankList)
    {
        _sportRankList = sportRankList;
    }

    public List<EnrAccessCourse> getAccessCourseList()
    {
        return _accessCourseList;
    }

    public void setAccessCourseList(List<EnrAccessCourse> accessCourseList)
    {
        _accessCourseList = accessCourseList;
    }

    public List<EnrAccessDepartment> getAccessDepartmentList()
    {
        return _accessDepartmentList;
    }

    public void setAccessDepartmentList(List<EnrAccessDepartment> accessDepartmentList)
    {
        _accessDepartmentList = accessDepartmentList;
    }

    public ISelectModel getWorkPlaceModel()
    {
        return _workPlaceModel;
    }

    public void setWorkPlaceModel(ISelectModel workPlaceModel)
    {
        _workPlaceModel = workPlaceModel;
    }

    public ISelectModel getWorkPostModel()
    {
        return _workPostModel;
    }

    public void setWorkPostModel(ISelectModel workPostModel)
    {
        _workPostModel = workPostModel;
    }

    public ISelectModel getSourceInfoAboutUniversityListModel()
    {
        return _sourceInfoAboutUniversityListModel;
    }

    public void setSourceInfoAboutUniversityListModel(ISelectModel sourceInfoAboutUniversityListModel)
    {
        _sourceInfoAboutUniversityListModel = sourceInfoAboutUniversityListModel;
    }

    public List<EnrSourceInfoAboutUniversity> getSourceInfoAboutUniversityList()
    {
        return _sourceInfoAboutUniversityList;
    }

    public void setSourceInfoAboutUniversityList(List<EnrSourceInfoAboutUniversity> sourceInfoAboutUniversityList)
    {
        _sourceInfoAboutUniversityList = sourceInfoAboutUniversityList;
    }

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    public boolean isWithForeignLanguage()
    {
        return _withForeignLanguage;
    }

    public void setWithForeignLanguage(boolean withForeignLanguage)
    {
        _withForeignLanguage = withForeignLanguage;
    }

    @Override
    public void onComponentRefresh()
    {
        _entrant = DataAccessServices.dao().getNotNull(_state.getEntrantId());
        setPerson(IUniBaseDao.instance.get().getNotNull(Person.class, getState().getPersonId()));
        setLanguageList(IUniBaseDao.instance.get().getList(PersonForeignLanguage.class, PersonForeignLanguage.person(), getPerson(), PersonForeignLanguage.language().title().s()));
        if(getLanguageList().isEmpty())
        {
            _withForeignLanguageDisabled = false;
            PersonForeignLanguage language = newLanguage();
            language.setLanguage(IUniBaseDao.instance.get().getByCode(ForeignLanguage.class, "12"));
            getLanguageList().add(language);
        }
        _withForeignLanguage = true;
        for (PersonForeignLanguage language : getLanguageList()) {
            if (language.isMain()) {
                setMainLanguage(language);
            }
            if(language.getId() != null && !getState().getCreatedObjectIds().contains(language.getId()))
                _withForeignLanguageDisabled = true;
        }
        _sportAchievements = EnrEntrantRequestAddWizardManager.instance().dao().prepareSportAchievementsList(_state);
        if(_sportAchievements.isEmpty())
        {
            _sportAchievements.add(newSportAchievement());
            _withSportAchievements = false;
            _withSportAchievementsDisabled = false;
        }
        else
        {
            for(PersonSportAchievementVO achievement : _sportAchievements)
            {
                if(!achievement.isCreatedInCurrentWizard())
                {
                    _withSportAchievementsDisabled = true;
                    break;
                }
            }
            _withSportAchievements = true;
        }

        setSportRankList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(SportRank.class));

        _accessCourseList = new ArrayList<>();
        for (EnrEntrantAccessCourse item : DataAccessServices.dao().getList(EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.entrant().id(), _state.getEntrantId()))
            _accessCourseList.add(item.getCourse());
        Collections.sort(_accessCourseList, ITitled.TITLED_COMPARATOR);

        _accessDepartmentList = new ArrayList<>();
        for (EnrEntrantAccessDepartment item : DataAccessServices.dao().getList(EnrEntrantAccessDepartment.class, EnrEntrantAccessDepartment.entrant().id(), _state.getEntrantId()))
            _accessDepartmentList.add(item.getAccessDepartment());
        Collections.sort(_accessDepartmentList, ITitled.TITLED_COMPARATOR);

        setWorkPlaceModel(new WorkPlaceSelectModel());
        setWorkPostModel(new WorkPostSelectModel());

        setSourceInfoAboutUniversityListModel(new LazySimpleSelectModel<>(EnrSourceInfoAboutUniversity.class).setSearchFromStart(false));
        setSourceInfoAboutUniversityList(new ArrayList<>());
        for (EnrEntrantInfoAboutUniversity item : IUniBaseDao.instance.get().getList(EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.L_ENTRANT, _entrant))
            getSourceInfoAboutUniversityList().add(item.getSourceInfo());
    }

    public Integer getCurrentSportAchievementIndex()
    {
        return _sportAchievements.indexOf(_currentSportAchievement);
    }

    public boolean isShowAddSportAchievementButton()
    {
        return _sportAchievements.size() == getCurrentSportAchievementIndex() + 1;
    }

    public boolean isWithForeignLanguageDisabled()
    {
        return _withForeignLanguageDisabled;
    }

    public boolean isWithSportAchievementsDisabled()
    {
        return _withSportAchievementsDisabled;
    }

    // actions

    public void onAddLanguage() {
        getLanguageList().add(newLanguage());
    }

    public void onDeleteLanguage() {
        PersonForeignLanguage deleted = null;
        for (PersonForeignLanguage language : getLanguageList()) {
            if (getLanguageId(language).equals(getListenerParameter())) {
                deleted = language;
            }
        }
        getLanguageList().remove(deleted);
        if(getLanguageList().isEmpty())
            getLanguageList().add(newLanguage());
    }

    private PersonForeignLanguage newLanguage()
    {
        PersonForeignLanguage language = new PersonForeignLanguage();
        language.setPerson(getPerson());
        return language;
    }

    public void onAddSportAchievement()
    {
        _sportAchievements.add(newSportAchievement());
        Collections.sort(_sportAchievements, PersonSportAchievementVO.COMPARATOR);
    }

    public void onDeleteSportAchievement()
    {
        Integer index = getListenerParameter();
        if (index != null) {
            PersonSportAchievementVO value = _sportAchievements.get(index);
            if(value.isCreatedInCurrentWizard() && value.getSportAchievement().getId() != null) {
                Long deleteId = value.getSportAchievement().getId();
                DataAccessServices.dao().delete(deleteId);
                _state.getCreatedObjectIds().remove(deleteId);
            }
            _sportAchievements.remove(index.intValue());
            if(_sportAchievements.isEmpty()) {
                _sportAchievements.add(newSportAchievement());
            }
            Collections.sort(_sportAchievements, PersonSportAchievementVO.COMPARATOR);
        }
    }

    private PersonSportAchievementVO newSportAchievement()
    {
        PersonSportAchievement achievement = new PersonSportAchievement();
        achievement.setPerson(person);
        return new PersonSportAchievementVO(achievement, true);
    }

    @Override
    public void saveStepData()
    {
        Set<ForeignLanguage> languages = new HashSet<>(CollectionUtils.collect(getLanguageList(), PersonForeignLanguage::getLanguage));
        if (getLanguageList().size() > languages.size()) {
            throw new ApplicationException("Один и тот же иностранный язык выбран несколько раз.");
        }

        if(_withSportAchievements)
            validateSportAchievements();

        IUniBaseDao.instance.get().doInTransaction(session -> {
            session.update(getPerson());

            if(_withForeignLanguage)
            {
                List<PersonForeignLanguage> existingLanguages = IUniBaseDao.instance.get().getList(PersonForeignLanguage.class, PersonForeignLanguage.person(), getPerson(), PersonForeignLanguage.language().title().s());

                for (PersonForeignLanguage language : getLanguageList()) {
                    if (getMainLanguage() != null && getLanguageId(getMainLanguage()).equals(getLanguageId(language))) {
                        language.setMain(true);
                    }
                }

                setLanguageList(new MergeAction.SessionMergeAction<ForeignLanguage, PersonForeignLanguage>()
                {

                    @Override
                    protected PersonForeignLanguage buildRow(PersonForeignLanguage source)
                    {
                        return source;
                    }

                    @Override
                    protected ForeignLanguage key(PersonForeignLanguage source)
                    {
                        return source.getLanguage();
                    }

                    @Override
                    protected void fill(PersonForeignLanguage target, PersonForeignLanguage source)
                    {
                        target.update(source, false);
                    }
                }.merge(
                        existingLanguages,
                        getLanguageList()));

                List<Long> newIds = UniBaseUtils.getIdList(getLanguageList());
                newIds.removeAll(UniBaseUtils.getIdList(existingLanguages));
                getState().getCreatedObjectIds().addAll(newIds);
            }
            else
            {
                List<Long> deleteIdsList = Lists.newArrayList();
                for(PersonForeignLanguage language : getLanguageList())
                {
                    if(language.getId() != null && getState().getCreatedObjectIds().contains(language.getId()))
                        deleteIdsList.add(language.getId());

                }
                if (!deleteIdsList.isEmpty())
                    new DQLDeleteBuilder(PersonForeignLanguage.class)
                            .where(DQLExpressions.in(DQLExpressions.property(PersonForeignLanguage.id()), deleteIdsList))
                            .createStatement(session).execute();
            }

            if(_withSportAchievements)
                EnrEntrantRequestAddWizardManager.instance().dao().saveSportAchievementsList(_state, _sportAchievements);
            else
            {
                for(PersonSportAchievementVO sportAchievementVO : _sportAchievements)
                {
                    List<Long> deleteIdsList = Lists.newArrayList();
                    if(sportAchievementVO.getSportAchievement().getId() != null && sportAchievementVO.isCreatedInCurrentWizard())
                        deleteIdsList.add(sportAchievementVO.getSportAchievement().getId());
                    if (!deleteIdsList.isEmpty())
                        new DQLDeleteBuilder(PersonSportAchievement.class)
                                .where(DQLExpressions.in(DQLExpressions.property(PersonSportAchievement.id()), deleteIdsList))
                                .createStatement(session).execute();
                }
            }

            EnrEntrantManager.instance().dao().updatePreparationData(_entrant, _accessCourseList, _accessDepartmentList);
            EnrEntrantManager.instance().dao().updateEntrantData(_entrant, getSourceInfoAboutUniversityList());
            return null;
        });
    }

    private void validateSportAchievements()
    {
        Set<Long> sportTypesIds = Sets.newHashSet();
        for(PersonSportAchievementVO sportAchievement : _sportAchievements)
        {
            SportType sportType = sportAchievement.getSportAchievement().getSportKind();
            if(sportType != null)
            {
                if(sportTypesIds.contains(sportType.getId()))
                    throw new ApplicationException("У персоны не может быть несколько спортивных достижений одного вида.");
                else
                    sportTypesIds.add(sportType.getId());
            }
        }
    }

    public void onChangePassArmy()
    {
        if(!_entrant.isPassArmy())
        {
            _entrant.setEndArmyYear(null);
            _entrant.setBeginArmy(null);
            _entrant.setEndArmy(null);
        }
    }

    // presenter

    public boolean isCurrentLanguageDisabled() {
        return ! (getCurrentLanguage().getId() == null || getState().getCreatedObjectIds().contains(getCurrentLanguage().getId()));
    }

    public boolean isShowAddLanguageButton() {
        return getLanguageList().indexOf(getCurrentLanguage()) == getLanguageList().size() - 1;
    }

    public String getLanguageFieldId() {
        return "language." + getCurrentLanguageId();
    }

    public String getSkillFieldId() {
        return "skill." + getCurrentLanguageId();
    }

    public String getCurrentLanguageId() {
        return getLanguageId(getCurrentLanguage());
    }

    // utils

    public String getLanguageId(PersonForeignLanguage language) {
        return String.valueOf(System.identityHashCode(language));
    }

    // getters and setters

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public PersonForeignLanguage getCurrentLanguage()
    {
        return currentLanguage;
    }

    public List<PersonForeignLanguage> getLanguageList()
    {
        return languageList;
    }

    public ISelectModel getLanguageModel()
    {
        return languageModel;
    }

    public PersonForeignLanguage getMainLanguage()
    {
        return mainLanguage;
    }

    public Person getPerson()
    {
        return person;
    }

    public ISelectModel getSkillModel()
    {
        return skillModel;
    }

    public void setCurrentLanguage(PersonForeignLanguage currentLanguage)
    {
        this.currentLanguage = currentLanguage;
    }

    public void setLanguageList(List<PersonForeignLanguage> languageList)
    {
        this.languageList = languageList;
    }

    public void setMainLanguage(PersonForeignLanguage mainLanguage)
    {
        this.mainLanguage = mainLanguage;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }
}
