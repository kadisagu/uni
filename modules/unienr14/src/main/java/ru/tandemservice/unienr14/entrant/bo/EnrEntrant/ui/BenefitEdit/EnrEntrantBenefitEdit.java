/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.BenefitEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;

/**
 * @author oleyba
 * @since 2/26/14
 */
@Configuration
public class EnrEntrantBenefitEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("benefitCategoryDS", EnrCatalogCommonsManager.instance().benefitCategoryDSHandler()))
            .addDataSource(selectDS("documentDS", EnrEntrantManager.instance().benefitProofDocumentDSHandler()).addColumn(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
            .addDataSource(selectDS("programSetItemDS", EnrEntrantManager.instance().noExamsProgramSetItemDSHandler()).addColumn(EnrProgramSetItem.program().title().s()))
            .create();
    }


}