/* $Id: Controller.java 32254 2014-02-04 09:18:39Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubLogTab;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author ekachanova
 */
public class Controller extends ru.tandemservice.uni.component.log.EntityLogViewBase.Controller
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = component.getModel();
        model.setSettings(UniBaseUtils.getDataSettings(component, "logView.entrant.filter."));
        getDao().prepare(model);
        prepareDataSource(component);
    }
}
