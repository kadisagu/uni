package ru.tandemservice.unienr14.ws.campaignInfo;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
public interface IEnrCampaignInfoServiceDao
{
    public static final SpringBeanCache<IEnrCampaignInfoServiceDao> INSTANCE = new SpringBeanCache<IEnrCampaignInfoServiceDao>(IEnrCampaignInfoServiceDao.class.getName());

    public EnrCampaignInfoEnvironmentNode getEnrollmentEnvironmentData(String enrollmentCampaignTitle);
}
