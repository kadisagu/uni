/* $Id: controllerPub.vm 24257 2012-09-28 10:50:28Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.component.catalog.enrDiscipline.EnrDisciplinePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IRawFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;

/**
 * @author AutoGenerator
 * Created on 10.04.2013 
 */
public class Controller extends DefaultCatalogPubController<EnrDiscipline, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<EnrDiscipline> dataSource)
    {
        final IRawFormatter formatter = (IRawFormatter) source ->
                "<div title=\"Зачетный балл подставляется по умолчанию в наборе ВИ для КГ, где может быть изменен пользователем.\">"
                + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(0.001 * (Long)source)+  "</div>";

        AbstractColumn markColumn = new SimpleColumn(EnrDiscipline.P_DEFAULT_PASS_MARK_AS_LONG, "Зачетный балл по умолчанию", EnrDiscipline.P_DEFAULT_PASS_MARK_AS_LONG, formatter).setClickable(false).setOrderable(false);
        dataSource.addColumn(markColumn);
    }
}
