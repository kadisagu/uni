package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_unienr14_2x8x0_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetHigher

		// создано обязательное свойство requestType
		{
			// создать колонку
			tool.createColumn("enr14_program_set_higher_t", new DBColumn("requesttype_id", DBType.LONG));

			java.lang.Long defaultRequestType = MigrationUtils.getIdByCode(tool, "enr14_c_request_type_t", "3");
			tool.executeUpdate("update enr14_program_set_higher_t set requesttype_id=? where requesttype_id is null", defaultRequestType);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_program_set_higher_t", "requesttype_id", false);
		}
    }
}