/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import jxl.JXLException;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.lang.math.NumberUtils;

import java.util.List;

/**
 * @author rsizonenko
 * @since 01.07.2014
 */
public class ExcelReportUtils {
    // выводит строку в документ
    public static void printrow(WritableSheet sheet, String[] rowContent, int rowIndex, WritableCellFormat format) throws JXLException
    {
        for (int i = 0; i < rowContent.length; i++) {
            if (NumberUtils.isNumber(rowContent[i]))
                sheet.addCell(new Number(i, rowIndex, Double.parseDouble(rowContent[i]), format));
            else
                sheet.addCell(new Label(i, rowIndex, rowContent[i], format));
        }
    }



    // выводит строку в документ
    public static void printrow(WritableSheet sheet, List<String> rowContent, int rowIndex, WritableCellFormat format) throws JXLException
    {
        for (int i = 0; i < rowContent.size(); i++) {
            String value = rowContent.get(i);
            if (NumberUtils.isNumber(value))
                sheet.addCell(new Number(i, rowIndex, Double.parseDouble(value), format));
            else
                sheet.addCell(new Label(i, rowIndex, value, format));
        }
    }

    // выводит строку в документ, но вместо 0 делает пустые ячейки
    public static void printRowHideNulls(WritableSheet sheet, List<String> rowContent, int rowIndex, WritableCellFormat format) throws JXLException
    {
        for (int i = 0; i < rowContent.size(); i++) {
            String value = rowContent.get(i);
            if (value.equals("0") || value.equals("0.0"))
                sheet.addCell(new Label(i, rowIndex, "", format));
            else
            {
                if (NumberUtils.isNumber(value))
                    sheet.addCell(new Number(i, rowIndex, Double.parseDouble(value), format));
                else
                    sheet.addCell(new Label(i, rowIndex, value, format));
            }
        }
    }

    // выводит строку в документ
    public static void printRowHideNulls(WritableSheet sheet, String[] rowContent, int rowIndex, WritableCellFormat format) throws JXLException
    {
        for (int i = 0; i < rowContent.length; i++) {
            final String value = rowContent[i];
            if (value.equals("0") || value.equals("0.0"))
                sheet.addCell(new Label(i, rowIndex, "", format));
            else
            {
                if (NumberUtils.isNumber(value))
                    sheet.addCell(new Number(i, rowIndex, Double.parseDouble(value), format));
                else
                    sheet.addCell(new Label(i, rowIndex, value, format));
            }
        }
    }

    // устанавливает ширину ячеек
    public static void setWidths(int[] widths, WritableSheet sheet)
    {
        for (int i = 0; i < widths.length; i++) {
            sheet.setColumnView(i, widths[i]);
        }
    }
}
