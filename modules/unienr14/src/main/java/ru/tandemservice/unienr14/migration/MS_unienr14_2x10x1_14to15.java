package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.Timestamp;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x1_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetition

		// создано свойство acceptedDate
		{
			// создать колонку
			tool.createColumn("enr14_requested_comp_t", new DBColumn("accepteddate_p", DBType.TIMESTAMP));

            // заполняем датой подачи заявления
            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_requested_comp_t", "rc")
                    .innerJoin(SQLFrom.table("enr14_request_t", "r"), "r.id=rc.request_id")
            )
                    .column("rc.id")
                    .column("r.regdate_p")
                    .where("accepted_p=?");

            List<Object[]> dataList = tool.executeQuery(
                    MigrationUtils.processor(Long.class, Timestamp.class),
                    translator.toSql(selectQuery), true);

            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_requested_comp_t")
                    .set("accepteddate_p", "?")
                    .where("id=?");

            for(Object[] obj : dataList)
            {
                Long id = (Long) obj[0];
                Timestamp regDate = (Timestamp) obj[1];

                tool.executeUpdate(translator.toSql(updateQuery), regDate, id);
            }

		}
    }
}