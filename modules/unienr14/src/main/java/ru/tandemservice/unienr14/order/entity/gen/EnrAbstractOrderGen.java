package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактный приказ на абитуриентов (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrAbstractOrderGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrAbstractOrder";
    public static final String ENTITY_NAME = "enrAbstractOrder";
    public static final int VERSION_HASH = -1123885199;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_CREATE_DATE = "createDate";
    public static final String L_STATE = "state";
    public static final String P_COMMIT_DATE = "commitDate";
    public static final String P_ACTION_DATE = "actionDate";
    public static final String P_COMMIT_DATE_SYSTEM = "commitDateSystem";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";
    public static final String P_EXECUTOR = "executor";
    public static final String P_READONLY = "readonly";

    private String _number;     // Номер
    private Date _createDate;     // Дата формирования
    private OrderStates _state;     // Состояние приказа
    private Date _commitDate;     // Дата подписания
    private Date _actionDate;     // Дата вступления в силу
    private Date _commitDateSystem;     // Дата проведения
    private String _textParagraph;     // Текстовый параграф
    private String _executor;     // Исполнитель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null и должно быть уникальным.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние приказа. Свойство не может быть null.
     */
    public void setState(OrderStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата подписания.
     */
    public Date getCommitDate()
    {
        return _commitDate;
    }

    /**
     * @param commitDate Дата подписания.
     */
    public void setCommitDate(Date commitDate)
    {
        dirty(_commitDate, commitDate);
        _commitDate = commitDate;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getActionDate()
    {
        return _actionDate;
    }

    /**
     * @param actionDate Дата вступления в силу.
     */
    public void setActionDate(Date actionDate)
    {
        dirty(_actionDate, actionDate);
        _actionDate = actionDate;
    }

    /**
     * @return Дата проведения.
     */
    public Date getCommitDateSystem()
    {
        return _commitDateSystem;
    }

    /**
     * @param commitDateSystem Дата проведения.
     */
    public void setCommitDateSystem(Date commitDateSystem)
    {
        dirty(_commitDateSystem, commitDateSystem);
        _commitDateSystem = commitDateSystem;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrAbstractOrderGen)
        {
            setNumber(((EnrAbstractOrder)another).getNumber());
            setCreateDate(((EnrAbstractOrder)another).getCreateDate());
            setState(((EnrAbstractOrder)another).getState());
            setCommitDate(((EnrAbstractOrder)another).getCommitDate());
            setActionDate(((EnrAbstractOrder)another).getActionDate());
            setCommitDateSystem(((EnrAbstractOrder)another).getCommitDateSystem());
            setTextParagraph(((EnrAbstractOrder)another).getTextParagraph());
            setExecutor(((EnrAbstractOrder)another).getExecutor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrAbstractOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrAbstractOrder.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrAbstractOrder is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "createDate":
                    return obj.getCreateDate();
                case "state":
                    return obj.getState();
                case "commitDate":
                    return obj.getCommitDate();
                case "actionDate":
                    return obj.getActionDate();
                case "commitDateSystem":
                    return obj.getCommitDateSystem();
                case "textParagraph":
                    return obj.getTextParagraph();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "state":
                    obj.setState((OrderStates) value);
                    return;
                case "commitDate":
                    obj.setCommitDate((Date) value);
                    return;
                case "actionDate":
                    obj.setActionDate((Date) value);
                    return;
                case "commitDateSystem":
                    obj.setCommitDateSystem((Date) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "createDate":
                        return true;
                case "state":
                        return true;
                case "commitDate":
                        return true;
                case "actionDate":
                        return true;
                case "commitDateSystem":
                        return true;
                case "textParagraph":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "createDate":
                    return true;
                case "state":
                    return true;
                case "commitDate":
                    return true;
                case "actionDate":
                    return true;
                case "commitDateSystem":
                    return true;
                case "textParagraph":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "createDate":
                    return Date.class;
                case "state":
                    return OrderStates.class;
                case "commitDate":
                    return Date.class;
                case "actionDate":
                    return Date.class;
                case "commitDateSystem":
                    return Date.class;
                case "textParagraph":
                    return String.class;
                case "executor":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrAbstractOrder> _dslPath = new Path<EnrAbstractOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrAbstractOrder");
    }
            

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getState()
     */
    public static OrderStates.Path<OrderStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата подписания.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getCommitDate()
     */
    public static PropertyPath<Date> commitDate()
    {
        return _dslPath.commitDate();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getActionDate()
     */
    public static PropertyPath<Date> actionDate()
    {
        return _dslPath.actionDate();
    }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getCommitDateSystem()
     */
    public static PropertyPath<Date> commitDateSystem()
    {
        return _dslPath.commitDateSystem();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#isReadonly()
     */
    public static SupportedPropertyPath<Boolean> readonly()
    {
        return _dslPath.readonly();
    }

    public static class Path<E extends EnrAbstractOrder> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _createDate;
        private OrderStates.Path<OrderStates> _state;
        private PropertyPath<Date> _commitDate;
        private PropertyPath<Date> _actionDate;
        private PropertyPath<Date> _commitDateSystem;
        private PropertyPath<String> _textParagraph;
        private PropertyPath<String> _executor;
        private SupportedPropertyPath<Boolean> _readonly;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EnrAbstractOrderGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(EnrAbstractOrderGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getState()
     */
        public OrderStates.Path<OrderStates> state()
        {
            if(_state == null )
                _state = new OrderStates.Path<OrderStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата подписания.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getCommitDate()
     */
        public PropertyPath<Date> commitDate()
        {
            if(_commitDate == null )
                _commitDate = new PropertyPath<Date>(EnrAbstractOrderGen.P_COMMIT_DATE, this);
            return _commitDate;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getActionDate()
     */
        public PropertyPath<Date> actionDate()
        {
            if(_actionDate == null )
                _actionDate = new PropertyPath<Date>(EnrAbstractOrderGen.P_ACTION_DATE, this);
            return _actionDate;
        }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getCommitDateSystem()
     */
        public PropertyPath<Date> commitDateSystem()
        {
            if(_commitDateSystem == null )
                _commitDateSystem = new PropertyPath<Date>(EnrAbstractOrderGen.P_COMMIT_DATE_SYSTEM, this);
            return _commitDateSystem;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(EnrAbstractOrderGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(EnrAbstractOrderGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractOrder#isReadonly()
     */
        public SupportedPropertyPath<Boolean> readonly()
        {
            if(_readonly == null )
                _readonly = new SupportedPropertyPath<Boolean>(EnrAbstractOrderGen.P_READONLY, this);
            return _readonly;
        }

        public Class getEntityClass()
        {
            return EnrAbstractOrder.class;
        }

        public String getEntityName()
        {
            return "enrAbstractOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isReadonly();
}
