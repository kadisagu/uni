package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetOrgUnitGen;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * Набор ОП для приема на подразделении
 *
 * Позволяет открыть конкурсы на подразделении
 */
public class EnrProgramSetOrgUnit extends EnrProgramSetOrgUnitGen implements IEntityDebugTitled, ITitled
{
    public EnrProgramSetOrgUnit() { }

    public EnrProgramSetOrgUnit(EnrProgramSetBase programSet, EnrOrgUnit orgUnit) {
        setProgramSet(programSet);
        setOrgUnit(orgUnit);
    }

    /** @return форм.подразделение, либо ведущее прием подр., если форм.подразделение явно не указано  */
    public OrgUnit getEffectiveFormativeOrgUnit() {
        OrgUnit ou = getFormativeOrgUnit();
        if (null != ou) { return ou; }
        return getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
    }

    @Override
    public String getEntityDebugTitle() {
        return getProgramSet().getTitle() + " ("+getOrgUnit().getTitle()+")";
    }

    @Override
    public String getTitle() {
        if (getProgramSet() == null) {
            return this.getClass().getSimpleName();
        }
        return getProgramSet().getTitle() + " ("+getOrgUnit().getDepartmentTitle()+")";
    }

    @Override
    public String getProgramTitle()
    {
        if (getProgramSet() instanceof EnrProgramSetSecondary) {
            return ((EnrProgramSetSecondary)getProgramSet()).getProgram().getTitle();
        }
        return "";
    }

    public int getMinisterialWithoutExclusiveAndTA() {
        return getMinisterialPlan() - getExclusivePlan() - getTargetAdmPlan();
    }
}