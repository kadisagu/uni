/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.Date;

/**
 * @author nvankov
 * @since 4/15/15
 */
public class EnrExamPassDisciplineWrapper extends DataWrapper implements IEnrExamPassDisciplineDao.IExamPassDisciplineMarkData
{
    private EnrExamPassDiscipline _discipline;
    private EnrExamPassAppeal _examPassAppeal;
    private Long _mark;
    private Date _markDate;
    private EnrAbsenceNote _absenceNote;
    public boolean _markDisabled = false;

    public EnrExamPassDisciplineWrapper(EnrExamPassDiscipline discipline, EnrExamPassAppeal examPassAppeal)
    {
        setId(discipline.getId());
        _discipline = discipline;
        _mark = discipline.getMarkAsLong();
        _markDate = discipline.getMarkDate();
        _absenceNote = discipline.getAbsenceNote();
        if(_absenceNote != null)
            _markDisabled = true;
        _examPassAppeal = examPassAppeal;
    }

    public EnrExamPassDiscipline getDiscipline()
    {
        return _discipline;
    }

    public String getAppeal()
    {
        return _examPassAppeal != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_examPassAppeal.getMarkAsDouble()) : "";
    }

    public String getRetake()
    {
        return YesNoFormatter.INSTANCE.format(_discipline.isRetake());
    }

    public Long getMark()
    {
        return _mark;
    }

    public void setMark(Long mark)
    {
        _mark = mark;
    }

    public Date getMarkDate()
    {
        return _markDate;
    }

    public void setMarkDate(Date markDate)
    {
        _markDate = markDate;
    }

    public EnrAbsenceNote getAbsenceNote()
    {
        return _absenceNote;
    }

    public void setAbsenceNote(EnrAbsenceNote absenceNote)
    {
        _absenceNote = absenceNote;
    }

    public boolean isMarkDisabled()
    {
        return _markDisabled;
    }

    public void setMarkDisabled(boolean markDisabled)
    {
        _markDisabled = markDisabled;
    }

    public boolean isMarked()
    {
        return _absenceNote != null || _mark != null && _mark > 0;
    }

    @Override
    public Long getMarkAsLong()
    {
        return getMark();
    }
}
