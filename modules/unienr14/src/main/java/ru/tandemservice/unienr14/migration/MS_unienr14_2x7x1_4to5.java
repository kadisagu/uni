package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEduLevelRequirement

		// создано обязательное свойство priority
		{
			// создать колонку
			tool.createColumn("enr14_c_lev_req_t", new DBColumn("priority_p", DBType.INTEGER));

            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 10, "1");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 2, "2");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 9, "3");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 1, "4");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 6, "5");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 7, "6");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 8, "7");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 5, "8");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 3, "9");
            tool.executeUpdate("update enr14_c_lev_req_t set priority_p=? where code_p = ?", 4, "10");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_c_lev_req_t", "priority_p", false);

		}


    }
}