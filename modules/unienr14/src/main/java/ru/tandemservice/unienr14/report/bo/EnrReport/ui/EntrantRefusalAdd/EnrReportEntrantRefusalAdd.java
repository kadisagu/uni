/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantRefusalReason;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 28.05.2014
 */

@Configuration
public class EnrReportEntrantRefusalAdd extends BusinessComponentManager {

    public static final String ENR_ORG_UNIT_DS = "enrOrgUnitDS";

    public static final String ENR_ENTRANT_REFUSAL_REASON_DS = "enrEntrantRefusalReasonDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENR_ORG_UNIT_DS, enrOrgUnitDSHandler())
                    .addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(ENR_ENTRANT_REFUSAL_REASON_DS, enrEntrantRefusalReasonHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrOrgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(in(property(alias, EnrOrgUnit.id()), new DQLSelectBuilder()
                        .fromEntity(EnrEntrantRefusal.class, "er")
                        .column(property("er", EnrEntrantRefusal.enrOrgUnit().id()))
                        .where(eq(property("er", EnrEntrantRefusal.enrOrgUnit().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .buildQuery()));
            }
        }
                .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .order(EnrOrgUnit.institutionOrgUnit().orgUnit().title())
                .pageable(true);
    }


    @Bean
    public IDefaultComboDataSourceHandler enrEntrantRefusalReasonHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantRefusalReason.class)
                .filter(EnrEntrantRefusalReason.title())
                .order(EnrEntrantRefusalReason.title())
                .pageable(true);
    }
}
