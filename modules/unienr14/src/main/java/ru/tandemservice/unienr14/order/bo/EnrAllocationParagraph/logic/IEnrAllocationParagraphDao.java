/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author oleyba
 * @since 7/8/14
 */
public interface IEnrAllocationParagraphDao extends INeedPersistenceSupport
{
    void saveOrUpdateParagraph(EnrOrder order, EnrAllocationParagraph paragraph, List<EnrRequestedCompetition> preStudents, EnrRequestedCompetition manager, String groupTitle);

    int doCreateOrders(Set<Long> programSetOrgUnitIds);

    /** Прочие выписки абитуриентов (кроме данного параграфа). */
    Collection<EnrAbstractExtract> getOtherExtracts(Collection<EnrEntrant> entrants, @Nullable EnrAbstractParagraph paragraph);
}