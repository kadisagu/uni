/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.Pub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic.EnrEnrollmentExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.StudentForExtractEdit.EnrOrderStudentForExtractEdit;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.List;

/**
 * @author oleyba
 * @since 7/8/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "paragraphId")
})
public class EnrEnrollmentParagraphPubUI extends UIPresenter
{
    private Long _paragraphId;
    private EnrEnrollmentParagraph _paragraph;
    private CommonPostfixPermissionModel _secModel;

    private DataWrapper params = new DataWrapper();

    @Override
    public void onComponentRefresh()
    {
        setParagraph(DataAccessServices.dao().getNotNull(EnrEnrollmentParagraph.class, getParagraphId()));
        setSecModel(new CommonPostfixPermissionModel("enrOrder"));

        List<EnrEnrollmentExtract> extracts = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph(), getParagraph(), EnrEnrollmentExtract.number().s());
        getParams().put("programSet", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrEnrollmentExtract.entity().competition().programSetOrgUnit().programSet().title().s(), ";"));
        getParams().put("competitionType", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrEnrollmentExtract.entity().competition().type().title().s(), ";"));
        getParams().put("competition", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrEnrollmentExtract.entity().competition().title().s(), ";"));
        getParams().put("groupTitle", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrEnrollmentExtract.groupTitle().s(), ";"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EnrEnrollmentParagraphPub.ENROLLMENT_EXTRACT_DS.equals(dataSource.getName())) {
            dataSource.put(EnrEnrollmentExtractDSHandler.ENROLLMENT_PARAGRAPH, _paragraph);
        }
    }

    public boolean isCanAnnul()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(_paragraph.getOrder().getState().getCode());
    }

    public boolean isChooseStudentVisible()
    {
        return OrderStatesCodes.FINISHED.equals(_paragraph.getOrder().getState().getCode());
    }

    public String getFormTitle()
    {
        return "Параграф приказа «" + getParagraph().getOrder().getType().getTitle() + "»";
    }

    // Listeners

    public void onClickEditParagraph()
    {
        _uiActivation.asRegion(EnrEnrollmentParagraphAddEdit.class)
            .parameter(EnrEnrollmentParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getParagraph().getId())
            .activate();
    }

    public void onClickDeleteParagraph()
    {
        EnrOrderManager.instance().dao().deleteParagraph(getParagraph().getId());
        deactivate();
    }

    public void onClickSetStudent()
    {
        _uiActivation.asRegionDialog(EnrOrderStudentForExtractEdit.class).parameter("extractId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteExtract()
    {
        EnrEnrollmentExtract extract = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());

        EnrOrderManager.instance().dao().deleteExtract(extract);
    }

    // Getters & Setters

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public EnrEnrollmentParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrEnrollmentParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public DataWrapper getParams()
    {
        return params;
    }

    public void setParams(DataWrapper params)
    {
        this.params = params;
    }
}