/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.ui.List.EnrEntrantRefusalList;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 20.05.2014.
 */

public class EnrEntrantRefusalListDSHandler extends DefaultSearchDataSourceHandler {

    public EnrEntrantRefusalListDSHandler(String ownerID)
    {
        super(ownerID);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EnrEntrantRefusal.class, "er")
            .setOrders(EnrEntrantRefusalList.ENR_ENTRANT_REFUSAL_FULL_NAME, new OrderDescription("er", EnrEntrantRefusal.fullName()))
            .setOrders(EnrEntrantRefusalList.ENR_ENTRANT_REFUSAL_DATE, new OrderDescription("er", EnrEntrantRefusal.registrationDate()));

        DQLSelectBuilder builder = orderDescription.buildDQLSelectBuilder();

        EnrEnrollmentCampaign enrollmentCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        EnrOrgUnit orgUnit = context.get("orgUnit");
        Date dateFrom = context.get("registeredFromFilter");
        Date dateTo = context.get("registeredToFilter");
        String lastName = context.get("lastName");
        String firstName = context.get("firstName");
        String middleName = context.get("middleName");

        if (dateFrom != null || dateTo != null)
            FilterUtils.applyBetweenFilter(builder, "er", EnrEntrantRefusal.registrationDate().s(), dateFrom, dateTo);

        if (enrollmentCampaign != null)
            builder.where(eq(property("er", EnrEntrantRefusal.enrollmentCampaign()), value(enrollmentCampaign)));

        if (orgUnit != null)
            builder.where(eq(property("er", EnrEntrantRefusal.enrOrgUnit()), value(orgUnit)));

        if (firstName != null)
            FilterUtils.applySimpleLikeFilter(builder, "er", EnrEntrantRefusal.firstName(), firstName);

        if (lastName != null)
            FilterUtils.applySimpleLikeFilter(builder, "er", EnrEntrantRefusal.lastName(), lastName);

        if (middleName != null)
            FilterUtils.applySimpleLikeFilter(builder, "er", EnrEntrantRefusal.middleName(), middleName);

        orderDescription.applyOrder(builder, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();

    }

}
