package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина набора ВИ
 *
 * Сводные данные по дисциплинам, входящим в набор ВИ.
 * Обновляется демоном IEnrExamSetDaemonBean и listener-ами на основе Элементов набора ВИ (enrExamSetElement).
 * Данная сущность обеспечивает уникальность набора дисциплин внутри ВИ (включая содержимое групп).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamSetDisciplineGen extends EntityBase
 implements INaturalIdentifiable<EnrExamSetDisciplineGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline";
    public static final String ENTITY_NAME = "enrExamSetDiscipline";
    public static final int VERSION_HASH = 413105350;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET = "examSet";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_SOURCE = "source";

    private EnrExamSet _examSet;     // Набор ВИ
    private EnrCampaignDiscipline _discipline;     // Дисциплина ПК
    private EnrExamSetElement _source;     // Источник дисциплины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSet getExamSet()
    {
        return _examSet;
    }

    /**
     * @param examSet Набор ВИ. Свойство не может быть null.
     */
    public void setExamSet(EnrExamSet examSet)
    {
        dirty(_examSet, examSet);
        _examSet = examSet;
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина ПК. Свойство не может быть null.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * Элемент набора ВИ, на основе которого получена данная дисциплина.
     *
     * @return Источник дисциплины. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetElement getSource()
    {
        return _source;
    }

    /**
     * @param source Источник дисциплины. Свойство не может быть null.
     */
    public void setSource(EnrExamSetElement source)
    {
        dirty(_source, source);
        _source = source;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamSetDisciplineGen)
        {
            if (withNaturalIdProperties)
            {
                setExamSet(((EnrExamSetDiscipline)another).getExamSet());
                setDiscipline(((EnrExamSetDiscipline)another).getDiscipline());
            }
            setSource(((EnrExamSetDiscipline)another).getSource());
        }
    }

    public INaturalId<EnrExamSetDisciplineGen> getNaturalId()
    {
        return new NaturalId(getExamSet(), getDiscipline());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamSetDisciplineGen>
    {
        private static final String PROXY_NAME = "EnrExamSetDisciplineNaturalProxy";

        private Long _examSet;
        private Long _discipline;

        public NaturalId()
        {}

        public NaturalId(EnrExamSet examSet, EnrCampaignDiscipline discipline)
        {
            _examSet = ((IEntity) examSet).getId();
            _discipline = ((IEntity) discipline).getId();
        }

        public Long getExamSet()
        {
            return _examSet;
        }

        public void setExamSet(Long examSet)
        {
            _examSet = examSet;
        }

        public Long getDiscipline()
        {
            return _discipline;
        }

        public void setDiscipline(Long discipline)
        {
            _discipline = discipline;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamSetDisciplineGen.NaturalId) ) return false;

            EnrExamSetDisciplineGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamSet(), that.getExamSet()) ) return false;
            if( !equals(getDiscipline(), that.getDiscipline()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamSet());
            result = hashCode(result, getDiscipline());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamSet());
            sb.append("/");
            sb.append(getDiscipline());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamSetDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamSetDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamSetDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examSet":
                    return obj.getExamSet();
                case "discipline":
                    return obj.getDiscipline();
                case "source":
                    return obj.getSource();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examSet":
                    obj.setExamSet((EnrExamSet) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "source":
                    obj.setSource((EnrExamSetElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examSet":
                        return true;
                case "discipline":
                        return true;
                case "source":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examSet":
                    return true;
                case "discipline":
                    return true;
                case "source":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examSet":
                    return EnrExamSet.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "source":
                    return EnrExamSetElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamSetDiscipline> _dslPath = new Path<EnrExamSetDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamSetDiscipline");
    }
            

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline#getExamSet()
     */
    public static EnrExamSet.Path<EnrExamSet> examSet()
    {
        return _dslPath.examSet();
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * Элемент набора ВИ, на основе которого получена данная дисциплина.
     *
     * @return Источник дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline#getSource()
     */
    public static EnrExamSetElement.Path<EnrExamSetElement> source()
    {
        return _dslPath.source();
    }

    public static class Path<E extends EnrExamSetDiscipline> extends EntityPath<E>
    {
        private EnrExamSet.Path<EnrExamSet> _examSet;
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrExamSetElement.Path<EnrExamSetElement> _source;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline#getExamSet()
     */
        public EnrExamSet.Path<EnrExamSet> examSet()
        {
            if(_examSet == null )
                _examSet = new EnrExamSet.Path<EnrExamSet>(L_EXAM_SET, this);
            return _examSet;
        }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * Элемент набора ВИ, на основе которого получена данная дисциплина.
     *
     * @return Источник дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline#getSource()
     */
        public EnrExamSetElement.Path<EnrExamSetElement> source()
        {
            if(_source == null )
                _source = new EnrExamSetElement.Path<EnrExamSetElement>(L_SOURCE, this);
            return _source;
        }

        public Class getEntityClass()
        {
            return EnrExamSetDiscipline.class;
        }

        public String getEntityName()
        {
            return "enrExamSetDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
