package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рейтинг абитуриента по конкурсу
 *
 * Определяет положение абитуриента в списке по конкурсу (согласно правилам ранжирования).
 * В список попадают все неархивные абитуриенты с активными (не отозванными) заявлениями.
 * При утрате актуальности заявления,
 * Набор объектов и ключевые поля создаются и обновляются демоном IEnrRatingDaemonDao#doUpdateRatingList.
 * Рейтинг и положение в списке обновляется через IEnrRatingListDao тем же демоном.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrRatingItemGen extends EntityBase
 implements INaturalIdentifiable<EnrRatingItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrRatingItem";
    public static final String ENTITY_NAME = "enrRatingItem";
    public static final int VERSION_HASH = -1696603237;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMPETITION = "competition";
    public static final String L_ENTRANT = "entrant";
    public static final String P_VALUE_HASH = "valueHash";
    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";
    public static final String P_REQUESTED_COMPETITION_ACTIVE = "requestedCompetitionActive";
    public static final String P_STATUS_ENTRANCE_EXAMS_CORRECT = "statusEntranceExamsCorrect";
    public static final String P_STATUS_RATING_COMPLETE = "statusRatingComplete";
    public static final String P_STATUS_RATING_POSITIVE = "statusRatingPositive";
    public static final String P_TOTAL_MARK_AS_LONG = "totalMarkAsLong";
    public static final String P_ACHIEVEMENT_MARK_AS_LONG = "achievementMarkAsLong";
    public static final String P_POSITION_ABSOLUTE = "positionAbsolute";
    public static final String P_POSITION = "position";
    public static final String L_EXCLUDED_BY_STEP_ITEM = "excludedByStepItem";
    public static final String L_EXCLUDED_BY_EXTRACT = "excludedByExtract";
    public static final String L_EXCLUDED_BY_EXTRACT_MINISTERIAL = "excludedByExtractMinisterial";
    public static final String P_ACHIEVEMENT_MARK_AS_STRING = "achievementMarkAsString";
    public static final String P_ENTRANCE_SUM_AS_STRING_REPLACING_ZERO = "entranceSumAsStringReplacingZero";
    public static final String P_TITLE = "title";
    public static final String P_TOTAL_MARK_AS_STRING = "totalMarkAsString";
    public static final String P_TOTAL_MARK_AS_STRING_REPLACING_ZERO = "totalMarkAsStringReplacingZero";

    private EnrCompetition _competition;     // Конкурс
    private EnrEntrant _entrant;     // Абитуриент
    private long _valueHash = 0l;     // Хэш всех значимых полей
    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс абитуриента
    private boolean _requestedCompetitionActive = false;     // Выбранный конкурс абитуриента активный
    private boolean _statusEntranceExamsCorrect = false;     // Набор выбранных вступительных испытаний (ВВИ) корректен
    private boolean _statusRatingComplete = false;     // Сданы все выбранные вступительные испытания (ВВИ)
    private Boolean _statusRatingPositive;     // По всем вступительным испытаниям балл не ниже порогового
    private long _totalMarkAsLong = 0l;     // Итоговый балл
    private long _achievementMarkAsLong = 0l;     // Сумма баллов за индивидуальные достижения с учетом ограничений
    private int _positionAbsolute;     // Положение в списке, определяемое порядком ранжирования
    private int _position;     // Положение (уникальное) в списке, с учетом ручного ранжирования
    private EnrEnrollmentStepItem _excludedByStepItem;     // Исключен из конкурса (не принес оригиналы документов)
    private EnrEnrollmentExtract _excludedByExtract;     // Исключен из конкурса (зачислен с более высоким приоритетом)
    private EnrEnrollmentExtract _excludedByExtractMinisterial;     // Исключен из конкурса (зачислен по квоте или без ВИ)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * Хэш всех значимых полей записи. 0 - отсутствие хэша (требуется перегенерация).
     * Используется для внутренних нужд демона (пересчитывается на основе всех значимых для рейтинга данных абитуриента).
     * Обновляются демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Хэш всех значимых полей. Свойство не может быть null.
     */
    @NotNull
    public long getValueHash()
    {
        return _valueHash;
    }

    /**
     * @param valueHash Хэш всех значимых полей. Свойство не может быть null.
     */
    public void setValueHash(long valueHash)
    {
        dirty(_valueHash, valueHash);
        _valueHash = valueHash;
    }

    /**
     * Ссылка на соответствующий выбранный конкурс активного (не отозванного) заявления неархивного абитуриента.
     * Если заявление отзывается, то поле перебрасывается на соответствующий выбранный конкурс активного заявления абитуриента.
     * Если таковых заявлений нет - остается ссылкой на последний выбранный (более неактивный) конкурс.
     * Обновляется демоном IEnrRatingDaemonDao#doPrepareRatingItems.
     *
     * @return Выбранный конкурс абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    /**
     * true, если requestedCompetition находится в не отозванном заявлении неархивного абитуриента.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Выбранный конкурс абитуриента активный. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequestedCompetitionActive()
    {
        return _requestedCompetitionActive;
    }

    /**
     * @param requestedCompetitionActive Выбранный конкурс абитуриента активный. Свойство не может быть null.
     */
    public void setRequestedCompetitionActive(boolean requestedCompetitionActive)
    {
        dirty(_requestedCompetitionActive, requestedCompetitionActive);
        _requestedCompetitionActive = requestedCompetitionActive;
    }

    /**
     * true, если «без ВИ» или пустой набор.
     * true, если все настроенные ВИ в конкурсе покрыты ВВИ, все ВВИ актуальны, среди ВВИ нет одинаковых настроенных ВИ.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Набор выбранных вступительных испытаний (ВВИ) корректен. Свойство не может быть null.
     */
    @NotNull
    public boolean isStatusEntranceExamsCorrect()
    {
        return _statusEntranceExamsCorrect;
    }

    /**
     * @param statusEntranceExamsCorrect Набор выбранных вступительных испытаний (ВВИ) корректен. Свойство не может быть null.
     */
    public void setStatusEntranceExamsCorrect(boolean statusEntranceExamsCorrect)
    {
        dirty(_statusEntranceExamsCorrect, statusEntranceExamsCorrect);
        _statusEntranceExamsCorrect = statusEntranceExamsCorrect;
    }

    /**
     * true, если без ВИ или пустой набор.
     * true, если по всем актуальным ВВИ либо сданы все ВВИ-ф, либо есть балл не ниже порогового.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Сданы все выбранные вступительные испытания (ВВИ). Свойство не может быть null.
     */
    @NotNull
    public boolean isStatusRatingComplete()
    {
        return _statusRatingComplete;
    }

    /**
     * @param statusRatingComplete Сданы все выбранные вступительные испытания (ВВИ). Свойство не может быть null.
     */
    public void setStatusRatingComplete(boolean statusRatingComplete)
    {
        dirty(_statusRatingComplete, statusRatingComplete);
        _statusRatingComplete = statusRatingComplete;
    }

    /**
     * true  - рейтинг корректен и по всем ВВИ есть итоговые баллы не ниже порогового (для «без ВИ» и пустого набора значение всегда true).
     * false - рейтинг корректен, существует ВВИ (в котором сданы все ВВИ-ф) с итоговым баллом ниже порогового.
     * null  - статус рейтинга не определен (он либо рейтинг некорректен, либо сданы не все ВВИ).
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return По всем вступительным испытаниям балл не ниже порогового.
     */
    public Boolean getStatusRatingPositive()
    {
        return _statusRatingPositive;
    }

    /**
     * @param statusRatingPositive По всем вступительным испытаниям балл не ниже порогового.
     */
    public void setStatusRatingPositive(Boolean statusRatingPositive)
    {
        dirty(_statusRatingPositive, statusRatingPositive);
        _statusRatingPositive = statusRatingPositive;
    }

    /**
     * Хранится со смещением в три знака для дробной части (для «без ВИ» и пустого набора значение всегда 0).
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Итоговый балл. Свойство не может быть null.
     */
    @NotNull
    public long getTotalMarkAsLong()
    {
        return _totalMarkAsLong;
    }

    /**
     * @param totalMarkAsLong Итоговый балл. Свойство не может быть null.
     */
    public void setTotalMarkAsLong(long totalMarkAsLong)
    {
        dirty(_totalMarkAsLong, totalMarkAsLong);
        _totalMarkAsLong = totalMarkAsLong;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Является суммой баллов за индивидуальные достижения с учетом ограничений.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Сумма баллов за индивидуальные достижения с учетом ограничений. Свойство не может быть null.
     */
    @NotNull
    public long getAchievementMarkAsLong()
    {
        return _achievementMarkAsLong;
    }

    /**
     * @param achievementMarkAsLong Сумма баллов за индивидуальные достижения с учетом ограничений. Свойство не может быть null.
     */
    public void setAchievementMarkAsLong(long achievementMarkAsLong)
    {
        dirty(_achievementMarkAsLong, achievementMarkAsLong);
        _achievementMarkAsLong = achievementMarkAsLong;
    }

    /**
     * «Абсолютное» положение в списке, определяемое для всех абитуриентов сравнением только данных ранжирования абитуриентов.
     * Совпадает для абитуриентов с одинаковыми параметрами.
     * Пересчитывается по указанному порядку начиная с 1.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Положение в списке, определяемое порядком ранжирования. Свойство не может быть null.
     */
    @NotNull
    public int getPositionAbsolute()
    {
        return _positionAbsolute;
    }

    /**
     * @param positionAbsolute Положение в списке, определяемое порядком ранжирования. Свойство не может быть null.
     */
    public void setPositionAbsolute(int positionAbsolute)
    {
        dirty(_positionAbsolute, positionAbsolute);
        _positionAbsolute = positionAbsolute;
    }

    /**
     * «Скорректированное» (уникальное) положение в списке, определяется уточнением абсолютного положения пользователем.
     * Возрастает при увеличении абсолютной позиции (в рамках одинаковой абсолютной позиции может меняться пользователем произвольно).
     * Пересчитывается по порядку начиная с 1, порядок определяется сортировкой всех абитуриентов по positionAbsolute, затем по текущему значению positionCorrected.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Положение (уникальное) в списке, с учетом ручного ранжирования. Свойство не может быть null.
     */
    @NotNull
    public int getPosition()
    {
        return _position;
    }

    /**
     * @param position Положение (уникальное) в списке, с учетом ручного ранжирования. Свойство не может быть null.
     */
    public void setPosition(int position)
    {
        dirty(_position, position);
        _position = position;
    }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (отказался от зачисления путем не принесения оригиналов документов).
     * Ссылка на абитуриента в конкурсном списке (данного абитуриента в списке по данному конкурса), в рамках которого абитуриент был исключен из текущего конкурса (не принес оригиналы документов).
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Формирование приказов».
     *
     * @return Исключен из конкурса (не принес оригиналы документов).
     */
    public EnrEnrollmentStepItem getExcludedByStepItem()
    {
        return _excludedByStepItem;
    }

    /**
     * @param excludedByStepItem Исключен из конкурса (не принес оригиналы документов).
     */
    public void setExcludedByStepItem(EnrEnrollmentStepItem excludedByStepItem)
    {
        dirty(_excludedByStepItem, excludedByStepItem);
        _excludedByStepItem = excludedByStepItem;
    }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (был зачислен на конкурс с более высоким приоритетом).
     * Ссылка на действующую выписку из приказа о зачислении (данного абитуриента на конкурс с более высоким приоритетом), на основании которой абитуриент был исключен из текущего конкурса (зачислен с более высоким приоритетом).
     * Если выписок более одной, то ссылка будет на выписку с приоритетом наиболее близким к приоритету текущего выбранного конкурса.
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Зачисление завершено» (если шаг зачисления есть), либо по данным согласованных выписок (если шага зачисления нет).
     *
     * @return Исключен из конкурса (зачислен с более высоким приоритетом).
     */
    public EnrEnrollmentExtract getExcludedByExtract()
    {
        return _excludedByExtract;
    }

    /**
     * @param excludedByExtract Исключен из конкурса (зачислен с более высоким приоритетом).
     */
    public void setExcludedByExtract(EnrEnrollmentExtract excludedByExtract)
    {
        dirty(_excludedByExtract, excludedByExtract);
        _excludedByExtract = excludedByExtract;
    }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (был зачислен на конкурс по квоте или без ВИ того же подр. в наборе ОП).
     * Ссылка на действующую выписку из приказа о зачислении, на основании которой абитуриент был исключен из текущего конкурса.
     * Если выписок более одной, то ссылка будет на выписку с приоритетом наиболее близким к приоритету текущего выбранного конкурса.
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Зачисление завершено» (если шаг зачисления есть), либо по данным согласованных выписок (если шага зачисления нет).
     *
     * @return Исключен из конкурса (зачислен по квоте или без ВИ).
     */
    public EnrEnrollmentExtract getExcludedByExtractMinisterial()
    {
        return _excludedByExtractMinisterial;
    }

    /**
     * @param excludedByExtractMinisterial Исключен из конкурса (зачислен по квоте или без ВИ).
     */
    public void setExcludedByExtractMinisterial(EnrEnrollmentExtract excludedByExtractMinisterial)
    {
        dirty(_excludedByExtractMinisterial, excludedByExtractMinisterial);
        _excludedByExtractMinisterial = excludedByExtractMinisterial;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrRatingItemGen)
        {
            if (withNaturalIdProperties)
            {
                setCompetition(((EnrRatingItem)another).getCompetition());
                setEntrant(((EnrRatingItem)another).getEntrant());
            }
            setValueHash(((EnrRatingItem)another).getValueHash());
            setRequestedCompetition(((EnrRatingItem)another).getRequestedCompetition());
            setRequestedCompetitionActive(((EnrRatingItem)another).isRequestedCompetitionActive());
            setStatusEntranceExamsCorrect(((EnrRatingItem)another).isStatusEntranceExamsCorrect());
            setStatusRatingComplete(((EnrRatingItem)another).isStatusRatingComplete());
            setStatusRatingPositive(((EnrRatingItem)another).getStatusRatingPositive());
            setTotalMarkAsLong(((EnrRatingItem)another).getTotalMarkAsLong());
            setAchievementMarkAsLong(((EnrRatingItem)another).getAchievementMarkAsLong());
            setPositionAbsolute(((EnrRatingItem)another).getPositionAbsolute());
            setPosition(((EnrRatingItem)another).getPosition());
            setExcludedByStepItem(((EnrRatingItem)another).getExcludedByStepItem());
            setExcludedByExtract(((EnrRatingItem)another).getExcludedByExtract());
            setExcludedByExtractMinisterial(((EnrRatingItem)another).getExcludedByExtractMinisterial());
        }
    }

    public INaturalId<EnrRatingItemGen> getNaturalId()
    {
        return new NaturalId(getCompetition(), getEntrant());
    }

    public static class NaturalId extends NaturalIdBase<EnrRatingItemGen>
    {
        private static final String PROXY_NAME = "EnrRatingItemNaturalProxy";

        private Long _competition;
        private Long _entrant;

        public NaturalId()
        {}

        public NaturalId(EnrCompetition competition, EnrEntrant entrant)
        {
            _competition = ((IEntity) competition).getId();
            _entrant = ((IEntity) entrant).getId();
        }

        public Long getCompetition()
        {
            return _competition;
        }

        public void setCompetition(Long competition)
        {
            _competition = competition;
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrRatingItemGen.NaturalId) ) return false;

            EnrRatingItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getCompetition(), that.getCompetition()) ) return false;
            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCompetition());
            result = hashCode(result, getEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCompetition());
            sb.append("/");
            sb.append(getEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrRatingItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrRatingItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrRatingItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "competition":
                    return obj.getCompetition();
                case "entrant":
                    return obj.getEntrant();
                case "valueHash":
                    return obj.getValueHash();
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
                case "requestedCompetitionActive":
                    return obj.isRequestedCompetitionActive();
                case "statusEntranceExamsCorrect":
                    return obj.isStatusEntranceExamsCorrect();
                case "statusRatingComplete":
                    return obj.isStatusRatingComplete();
                case "statusRatingPositive":
                    return obj.getStatusRatingPositive();
                case "totalMarkAsLong":
                    return obj.getTotalMarkAsLong();
                case "achievementMarkAsLong":
                    return obj.getAchievementMarkAsLong();
                case "positionAbsolute":
                    return obj.getPositionAbsolute();
                case "position":
                    return obj.getPosition();
                case "excludedByStepItem":
                    return obj.getExcludedByStepItem();
                case "excludedByExtract":
                    return obj.getExcludedByExtract();
                case "excludedByExtractMinisterial":
                    return obj.getExcludedByExtractMinisterial();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "valueHash":
                    obj.setValueHash((Long) value);
                    return;
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
                case "requestedCompetitionActive":
                    obj.setRequestedCompetitionActive((Boolean) value);
                    return;
                case "statusEntranceExamsCorrect":
                    obj.setStatusEntranceExamsCorrect((Boolean) value);
                    return;
                case "statusRatingComplete":
                    obj.setStatusRatingComplete((Boolean) value);
                    return;
                case "statusRatingPositive":
                    obj.setStatusRatingPositive((Boolean) value);
                    return;
                case "totalMarkAsLong":
                    obj.setTotalMarkAsLong((Long) value);
                    return;
                case "achievementMarkAsLong":
                    obj.setAchievementMarkAsLong((Long) value);
                    return;
                case "positionAbsolute":
                    obj.setPositionAbsolute((Integer) value);
                    return;
                case "position":
                    obj.setPosition((Integer) value);
                    return;
                case "excludedByStepItem":
                    obj.setExcludedByStepItem((EnrEnrollmentStepItem) value);
                    return;
                case "excludedByExtract":
                    obj.setExcludedByExtract((EnrEnrollmentExtract) value);
                    return;
                case "excludedByExtractMinisterial":
                    obj.setExcludedByExtractMinisterial((EnrEnrollmentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "competition":
                        return true;
                case "entrant":
                        return true;
                case "valueHash":
                        return true;
                case "requestedCompetition":
                        return true;
                case "requestedCompetitionActive":
                        return true;
                case "statusEntranceExamsCorrect":
                        return true;
                case "statusRatingComplete":
                        return true;
                case "statusRatingPositive":
                        return true;
                case "totalMarkAsLong":
                        return true;
                case "achievementMarkAsLong":
                        return true;
                case "positionAbsolute":
                        return true;
                case "position":
                        return true;
                case "excludedByStepItem":
                        return true;
                case "excludedByExtract":
                        return true;
                case "excludedByExtractMinisterial":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "competition":
                    return true;
                case "entrant":
                    return true;
                case "valueHash":
                    return true;
                case "requestedCompetition":
                    return true;
                case "requestedCompetitionActive":
                    return true;
                case "statusEntranceExamsCorrect":
                    return true;
                case "statusRatingComplete":
                    return true;
                case "statusRatingPositive":
                    return true;
                case "totalMarkAsLong":
                    return true;
                case "achievementMarkAsLong":
                    return true;
                case "positionAbsolute":
                    return true;
                case "position":
                    return true;
                case "excludedByStepItem":
                    return true;
                case "excludedByExtract":
                    return true;
                case "excludedByExtractMinisterial":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "competition":
                    return EnrCompetition.class;
                case "entrant":
                    return EnrEntrant.class;
                case "valueHash":
                    return Long.class;
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
                case "requestedCompetitionActive":
                    return Boolean.class;
                case "statusEntranceExamsCorrect":
                    return Boolean.class;
                case "statusRatingComplete":
                    return Boolean.class;
                case "statusRatingPositive":
                    return Boolean.class;
                case "totalMarkAsLong":
                    return Long.class;
                case "achievementMarkAsLong":
                    return Long.class;
                case "positionAbsolute":
                    return Integer.class;
                case "position":
                    return Integer.class;
                case "excludedByStepItem":
                    return EnrEnrollmentStepItem.class;
                case "excludedByExtract":
                    return EnrEnrollmentExtract.class;
                case "excludedByExtractMinisterial":
                    return EnrEnrollmentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrRatingItem> _dslPath = new Path<EnrRatingItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrRatingItem");
    }
            

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * Хэш всех значимых полей записи. 0 - отсутствие хэша (требуется перегенерация).
     * Используется для внутренних нужд демона (пересчитывается на основе всех значимых для рейтинга данных абитуриента).
     * Обновляются демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Хэш всех значимых полей. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getValueHash()
     */
    public static PropertyPath<Long> valueHash()
    {
        return _dslPath.valueHash();
    }

    /**
     * Ссылка на соответствующий выбранный конкурс активного (не отозванного) заявления неархивного абитуриента.
     * Если заявление отзывается, то поле перебрасывается на соответствующий выбранный конкурс активного заявления абитуриента.
     * Если таковых заявлений нет - остается ссылкой на последний выбранный (более неактивный) конкурс.
     * Обновляется демоном IEnrRatingDaemonDao#doPrepareRatingItems.
     *
     * @return Выбранный конкурс абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    /**
     * true, если requestedCompetition находится в не отозванном заявлении неархивного абитуриента.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Выбранный конкурс абитуриента активный. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#isRequestedCompetitionActive()
     */
    public static PropertyPath<Boolean> requestedCompetitionActive()
    {
        return _dslPath.requestedCompetitionActive();
    }

    /**
     * true, если «без ВИ» или пустой набор.
     * true, если все настроенные ВИ в конкурсе покрыты ВВИ, все ВВИ актуальны, среди ВВИ нет одинаковых настроенных ВИ.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Набор выбранных вступительных испытаний (ВВИ) корректен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#isStatusEntranceExamsCorrect()
     */
    public static PropertyPath<Boolean> statusEntranceExamsCorrect()
    {
        return _dslPath.statusEntranceExamsCorrect();
    }

    /**
     * true, если без ВИ или пустой набор.
     * true, если по всем актуальным ВВИ либо сданы все ВВИ-ф, либо есть балл не ниже порогового.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Сданы все выбранные вступительные испытания (ВВИ). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#isStatusRatingComplete()
     */
    public static PropertyPath<Boolean> statusRatingComplete()
    {
        return _dslPath.statusRatingComplete();
    }

    /**
     * true  - рейтинг корректен и по всем ВВИ есть итоговые баллы не ниже порогового (для «без ВИ» и пустого набора значение всегда true).
     * false - рейтинг корректен, существует ВВИ (в котором сданы все ВВИ-ф) с итоговым баллом ниже порогового.
     * null  - статус рейтинга не определен (он либо рейтинг некорректен, либо сданы не все ВВИ).
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return По всем вступительным испытаниям балл не ниже порогового.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getStatusRatingPositive()
     */
    public static PropertyPath<Boolean> statusRatingPositive()
    {
        return _dslPath.statusRatingPositive();
    }

    /**
     * Хранится со смещением в три знака для дробной части (для «без ВИ» и пустого набора значение всегда 0).
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Итоговый балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTotalMarkAsLong()
     */
    public static PropertyPath<Long> totalMarkAsLong()
    {
        return _dslPath.totalMarkAsLong();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Является суммой баллов за индивидуальные достижения с учетом ограничений.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Сумма баллов за индивидуальные достижения с учетом ограничений. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getAchievementMarkAsLong()
     */
    public static PropertyPath<Long> achievementMarkAsLong()
    {
        return _dslPath.achievementMarkAsLong();
    }

    /**
     * «Абсолютное» положение в списке, определяемое для всех абитуриентов сравнением только данных ранжирования абитуриентов.
     * Совпадает для абитуриентов с одинаковыми параметрами.
     * Пересчитывается по указанному порядку начиная с 1.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Положение в списке, определяемое порядком ранжирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getPositionAbsolute()
     */
    public static PropertyPath<Integer> positionAbsolute()
    {
        return _dslPath.positionAbsolute();
    }

    /**
     * «Скорректированное» (уникальное) положение в списке, определяется уточнением абсолютного положения пользователем.
     * Возрастает при увеличении абсолютной позиции (в рамках одинаковой абсолютной позиции может меняться пользователем произвольно).
     * Пересчитывается по порядку начиная с 1, порядок определяется сортировкой всех абитуриентов по positionAbsolute, затем по текущему значению positionCorrected.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Положение (уникальное) в списке, с учетом ручного ранжирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getPosition()
     */
    public static PropertyPath<Integer> position()
    {
        return _dslPath.position();
    }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (отказался от зачисления путем не принесения оригиналов документов).
     * Ссылка на абитуриента в конкурсном списке (данного абитуриента в списке по данному конкурса), в рамках которого абитуриент был исключен из текущего конкурса (не принес оригиналы документов).
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Формирование приказов».
     *
     * @return Исключен из конкурса (не принес оригиналы документов).
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getExcludedByStepItem()
     */
    public static EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem> excludedByStepItem()
    {
        return _dslPath.excludedByStepItem();
    }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (был зачислен на конкурс с более высоким приоритетом).
     * Ссылка на действующую выписку из приказа о зачислении (данного абитуриента на конкурс с более высоким приоритетом), на основании которой абитуриент был исключен из текущего конкурса (зачислен с более высоким приоритетом).
     * Если выписок более одной, то ссылка будет на выписку с приоритетом наиболее близким к приоритету текущего выбранного конкурса.
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Зачисление завершено» (если шаг зачисления есть), либо по данным согласованных выписок (если шага зачисления нет).
     *
     * @return Исключен из конкурса (зачислен с более высоким приоритетом).
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getExcludedByExtract()
     */
    public static EnrEnrollmentExtract.Path<EnrEnrollmentExtract> excludedByExtract()
    {
        return _dslPath.excludedByExtract();
    }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (был зачислен на конкурс по квоте или без ВИ того же подр. в наборе ОП).
     * Ссылка на действующую выписку из приказа о зачислении, на основании которой абитуриент был исключен из текущего конкурса.
     * Если выписок более одной, то ссылка будет на выписку с приоритетом наиболее близким к приоритету текущего выбранного конкурса.
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Зачисление завершено» (если шаг зачисления есть), либо по данным согласованных выписок (если шага зачисления нет).
     *
     * @return Исключен из конкурса (зачислен по квоте или без ВИ).
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getExcludedByExtractMinisterial()
     */
    public static EnrEnrollmentExtract.Path<EnrEnrollmentExtract> excludedByExtractMinisterial()
    {
        return _dslPath.excludedByExtractMinisterial();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getAchievementMarkAsString()
     */
    public static SupportedPropertyPath<String> achievementMarkAsString()
    {
        return _dslPath.achievementMarkAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getEntranceSumAsStringReplacingZero()
     */
    public static SupportedPropertyPath<String> entranceSumAsStringReplacingZero()
    {
        return _dslPath.entranceSumAsStringReplacingZero();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTotalMarkAsString()
     */
    public static SupportedPropertyPath<String> totalMarkAsString()
    {
        return _dslPath.totalMarkAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTotalMarkAsStringReplacingZero()
     */
    public static SupportedPropertyPath<String> totalMarkAsStringReplacingZero()
    {
        return _dslPath.totalMarkAsStringReplacingZero();
    }

    public static class Path<E extends EnrRatingItem> extends EntityPath<E>
    {
        private EnrCompetition.Path<EnrCompetition> _competition;
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PropertyPath<Long> _valueHash;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;
        private PropertyPath<Boolean> _requestedCompetitionActive;
        private PropertyPath<Boolean> _statusEntranceExamsCorrect;
        private PropertyPath<Boolean> _statusRatingComplete;
        private PropertyPath<Boolean> _statusRatingPositive;
        private PropertyPath<Long> _totalMarkAsLong;
        private PropertyPath<Long> _achievementMarkAsLong;
        private PropertyPath<Integer> _positionAbsolute;
        private PropertyPath<Integer> _position;
        private EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem> _excludedByStepItem;
        private EnrEnrollmentExtract.Path<EnrEnrollmentExtract> _excludedByExtract;
        private EnrEnrollmentExtract.Path<EnrEnrollmentExtract> _excludedByExtractMinisterial;
        private SupportedPropertyPath<String> _achievementMarkAsString;
        private SupportedPropertyPath<String> _entranceSumAsStringReplacingZero;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _totalMarkAsString;
        private SupportedPropertyPath<String> _totalMarkAsStringReplacingZero;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * Хэш всех значимых полей записи. 0 - отсутствие хэша (требуется перегенерация).
     * Используется для внутренних нужд демона (пересчитывается на основе всех значимых для рейтинга данных абитуриента).
     * Обновляются демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Хэш всех значимых полей. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getValueHash()
     */
        public PropertyPath<Long> valueHash()
        {
            if(_valueHash == null )
                _valueHash = new PropertyPath<Long>(EnrRatingItemGen.P_VALUE_HASH, this);
            return _valueHash;
        }

    /**
     * Ссылка на соответствующий выбранный конкурс активного (не отозванного) заявления неархивного абитуриента.
     * Если заявление отзывается, то поле перебрасывается на соответствующий выбранный конкурс активного заявления абитуриента.
     * Если таковых заявлений нет - остается ссылкой на последний выбранный (более неактивный) конкурс.
     * Обновляется демоном IEnrRatingDaemonDao#doPrepareRatingItems.
     *
     * @return Выбранный конкурс абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

    /**
     * true, если requestedCompetition находится в не отозванном заявлении неархивного абитуриента.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Выбранный конкурс абитуриента активный. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#isRequestedCompetitionActive()
     */
        public PropertyPath<Boolean> requestedCompetitionActive()
        {
            if(_requestedCompetitionActive == null )
                _requestedCompetitionActive = new PropertyPath<Boolean>(EnrRatingItemGen.P_REQUESTED_COMPETITION_ACTIVE, this);
            return _requestedCompetitionActive;
        }

    /**
     * true, если «без ВИ» или пустой набор.
     * true, если все настроенные ВИ в конкурсе покрыты ВВИ, все ВВИ актуальны, среди ВВИ нет одинаковых настроенных ВИ.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Набор выбранных вступительных испытаний (ВВИ) корректен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#isStatusEntranceExamsCorrect()
     */
        public PropertyPath<Boolean> statusEntranceExamsCorrect()
        {
            if(_statusEntranceExamsCorrect == null )
                _statusEntranceExamsCorrect = new PropertyPath<Boolean>(EnrRatingItemGen.P_STATUS_ENTRANCE_EXAMS_CORRECT, this);
            return _statusEntranceExamsCorrect;
        }

    /**
     * true, если без ВИ или пустой набор.
     * true, если по всем актуальным ВВИ либо сданы все ВВИ-ф, либо есть балл не ниже порогового.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Сданы все выбранные вступительные испытания (ВВИ). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#isStatusRatingComplete()
     */
        public PropertyPath<Boolean> statusRatingComplete()
        {
            if(_statusRatingComplete == null )
                _statusRatingComplete = new PropertyPath<Boolean>(EnrRatingItemGen.P_STATUS_RATING_COMPLETE, this);
            return _statusRatingComplete;
        }

    /**
     * true  - рейтинг корректен и по всем ВВИ есть итоговые баллы не ниже порогового (для «без ВИ» и пустого набора значение всегда true).
     * false - рейтинг корректен, существует ВВИ (в котором сданы все ВВИ-ф) с итоговым баллом ниже порогового.
     * null  - статус рейтинга не определен (он либо рейтинг некорректен, либо сданы не все ВВИ).
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return По всем вступительным испытаниям балл не ниже порогового.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getStatusRatingPositive()
     */
        public PropertyPath<Boolean> statusRatingPositive()
        {
            if(_statusRatingPositive == null )
                _statusRatingPositive = new PropertyPath<Boolean>(EnrRatingItemGen.P_STATUS_RATING_POSITIVE, this);
            return _statusRatingPositive;
        }

    /**
     * Хранится со смещением в три знака для дробной части (для «без ВИ» и пустого набора значение всегда 0).
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Итоговый балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTotalMarkAsLong()
     */
        public PropertyPath<Long> totalMarkAsLong()
        {
            if(_totalMarkAsLong == null )
                _totalMarkAsLong = new PropertyPath<Long>(EnrRatingItemGen.P_TOTAL_MARK_AS_LONG, this);
            return _totalMarkAsLong;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Является суммой баллов за индивидуальные достижения с учетом ограничений.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Сумма баллов за индивидуальные достижения с учетом ограничений. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getAchievementMarkAsLong()
     */
        public PropertyPath<Long> achievementMarkAsLong()
        {
            if(_achievementMarkAsLong == null )
                _achievementMarkAsLong = new PropertyPath<Long>(EnrRatingItemGen.P_ACHIEVEMENT_MARK_AS_LONG, this);
            return _achievementMarkAsLong;
        }

    /**
     * «Абсолютное» положение в списке, определяемое для всех абитуриентов сравнением только данных ранжирования абитуриентов.
     * Совпадает для абитуриентов с одинаковыми параметрами.
     * Пересчитывается по указанному порядку начиная с 1.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Положение в списке, определяемое порядком ранжирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getPositionAbsolute()
     */
        public PropertyPath<Integer> positionAbsolute()
        {
            if(_positionAbsolute == null )
                _positionAbsolute = new PropertyPath<Integer>(EnrRatingItemGen.P_POSITION_ABSOLUTE, this);
            return _positionAbsolute;
        }

    /**
     * «Скорректированное» (уникальное) положение в списке, определяется уточнением абсолютного положения пользователем.
     * Возрастает при увеличении абсолютной позиции (в рамках одинаковой абсолютной позиции может меняться пользователем произвольно).
     * Пересчитывается по порядку начиная с 1, порядок определяется сортировкой всех абитуриентов по positionAbsolute, затем по текущему значению positionCorrected.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateRatingList.
     *
     * @return Положение (уникальное) в списке, с учетом ручного ранжирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getPosition()
     */
        public PropertyPath<Integer> position()
        {
            if(_position == null )
                _position = new PropertyPath<Integer>(EnrRatingItemGen.P_POSITION, this);
            return _position;
        }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (отказался от зачисления путем не принесения оригиналов документов).
     * Ссылка на абитуриента в конкурсном списке (данного абитуриента в списке по данному конкурса), в рамках которого абитуриент был исключен из текущего конкурса (не принес оригиналы документов).
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Формирование приказов».
     *
     * @return Исключен из конкурса (не принес оригиналы документов).
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getExcludedByStepItem()
     */
        public EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem> excludedByStepItem()
        {
            if(_excludedByStepItem == null )
                _excludedByStepItem = new EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem>(L_EXCLUDED_BY_STEP_ITEM, this);
            return _excludedByStepItem;
        }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (был зачислен на конкурс с более высоким приоритетом).
     * Ссылка на действующую выписку из приказа о зачислении (данного абитуриента на конкурс с более высоким приоритетом), на основании которой абитуриент был исключен из текущего конкурса (зачислен с более высоким приоритетом).
     * Если выписок более одной, то ссылка будет на выписку с приоритетом наиболее близким к приоритету текущего выбранного конкурса.
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Зачисление завершено» (если шаг зачисления есть), либо по данным согласованных выписок (если шага зачисления нет).
     *
     * @return Исключен из конкурса (зачислен с более высоким приоритетом).
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getExcludedByExtract()
     */
        public EnrEnrollmentExtract.Path<EnrEnrollmentExtract> excludedByExtract()
        {
            if(_excludedByExtract == null )
                _excludedByExtract = new EnrEnrollmentExtract.Path<EnrEnrollmentExtract>(L_EXCLUDED_BY_EXTRACT, this);
            return _excludedByExtract;
        }

    /**
     * Если поле заполнено, то абитуриент считается исключенным (был зачислен на конкурс по квоте или без ВИ того же подр. в наборе ОП).
     * Ссылка на действующую выписку из приказа о зачислении, на основании которой абитуриент был исключен из текущего конкурса.
     * Если выписок более одной, то ссылка будет на выписку с приоритетом наиболее близким к приоритету текущего выбранного конкурса.
     * Заполняется демоном IEnrRatingDaemonDao#doUpdateExcludedByStatus в момент перехода шага зачисления в состояние «Зачисление завершено» (если шаг зачисления есть), либо по данным согласованных выписок (если шага зачисления нет).
     *
     * @return Исключен из конкурса (зачислен по квоте или без ВИ).
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getExcludedByExtractMinisterial()
     */
        public EnrEnrollmentExtract.Path<EnrEnrollmentExtract> excludedByExtractMinisterial()
        {
            if(_excludedByExtractMinisterial == null )
                _excludedByExtractMinisterial = new EnrEnrollmentExtract.Path<EnrEnrollmentExtract>(L_EXCLUDED_BY_EXTRACT_MINISTERIAL, this);
            return _excludedByExtractMinisterial;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getAchievementMarkAsString()
     */
        public SupportedPropertyPath<String> achievementMarkAsString()
        {
            if(_achievementMarkAsString == null )
                _achievementMarkAsString = new SupportedPropertyPath<String>(EnrRatingItemGen.P_ACHIEVEMENT_MARK_AS_STRING, this);
            return _achievementMarkAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getEntranceSumAsStringReplacingZero()
     */
        public SupportedPropertyPath<String> entranceSumAsStringReplacingZero()
        {
            if(_entranceSumAsStringReplacingZero == null )
                _entranceSumAsStringReplacingZero = new SupportedPropertyPath<String>(EnrRatingItemGen.P_ENTRANCE_SUM_AS_STRING_REPLACING_ZERO, this);
            return _entranceSumAsStringReplacingZero;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrRatingItemGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTotalMarkAsString()
     */
        public SupportedPropertyPath<String> totalMarkAsString()
        {
            if(_totalMarkAsString == null )
                _totalMarkAsString = new SupportedPropertyPath<String>(EnrRatingItemGen.P_TOTAL_MARK_AS_STRING, this);
            return _totalMarkAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrRatingItem#getTotalMarkAsStringReplacingZero()
     */
        public SupportedPropertyPath<String> totalMarkAsStringReplacingZero()
        {
            if(_totalMarkAsStringReplacingZero == null )
                _totalMarkAsStringReplacingZero = new SupportedPropertyPath<String>(EnrRatingItemGen.P_TOTAL_MARK_AS_STRING_REPLACING_ZERO, this);
            return _totalMarkAsStringReplacingZero;
        }

        public Class getEntityClass()
        {
            return EnrRatingItem.class;
        }

        public String getEntityName()
        {
            return "enrRatingItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getAchievementMarkAsString();

    public abstract String getEntranceSumAsStringReplacingZero();

    public abstract String getTitle();

    public abstract String getTotalMarkAsString();

    public abstract String getTotalMarkAsStringReplacingZero();
}
