/**
 *$Id: ExamSetElementWrapper.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit;

import org.tandemframework.core.common.IIdentifiable;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

/**
 * @author Alexander Shaburov
 * @since 17.04.13
 */
public class ExamSetElementWrapper implements IEnrExamSetDao.IExamSetElementWrapper, IIdentifiable
{
    private final Long _id;

    private IEnrExamSetElementValue _elementValue;
    private EnrExamType _examType;

    public ExamSetElementWrapper()
    {
        _id = Integer.valueOf(System.identityHashCode(this)).longValue();
    }

    public ExamSetElementWrapper(ExamSetElementWrapper other)
    {
        this();
        _elementValue = other.getElementValue();
        _examType = other.getExamType();
    }

    public ExamSetElementWrapper(EnrExamSetElement examSetElement)
    {
        this();
        _elementValue = examSetElement.getValue();
        _examType = examSetElement.getType();
    }

    /* Generated */

    @Override
    public Long getId()
    {
        return _id;
    }

    @Override
    public IEnrExamSetElementValue getElementValue()
    {
        return _elementValue;
    }

    public void setElementValue(IEnrExamSetElementValue elementValue)
    {
        _elementValue = elementValue;
    }

    @Override
    public EnrExamType getExamType()
    {
        return _examType;
    }

    public void setExamType(EnrExamType examType)
    {
        _examType = examType;
    }
}
