package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x1_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignSettings

        // создано обязательное свойство limitAchievementMarkWithEduDocMark
        {
            // создать колонку
            if (!tool.columnExists("enr14_campaign_settings_t", "lmtachvmntmrkwtheddcmrk_p")) {
                tool.createColumn("enr14_campaign_settings_t", new DBColumn("lmtachvmntmrkwtheddcmrk_p", DBType.BOOLEAN));
            }

            // задать значение по умолчанию
            java.lang.Boolean defaultLimitAchievementMarkWithEduDocMark = Boolean.FALSE;
            tool.executeUpdate("update enr14_campaign_settings_t set lmtachvmntmrkwtheddcmrk_p=? where lmtachvmntmrkwtheddcmrk_p is null", defaultLimitAchievementMarkWithEduDocMark);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_campaign_settings_t", "lmtachvmntmrkwtheddcmrk_p", false);
        }


    }
}