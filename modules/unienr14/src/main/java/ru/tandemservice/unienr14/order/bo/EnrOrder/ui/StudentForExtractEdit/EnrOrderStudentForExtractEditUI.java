/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.StudentForExtractEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract;

/**
 * @author nvankov
 * @since 8/18/14
 */

@State({
        @Bind(key = "extractId", binding = "extractId", required = true)
})
public class EnrOrderStudentForExtractEditUI extends UIPresenter
{
    private Long _extractId;
    private EnrAbstractExtract _extract;

    @Override
    public void onComponentRefresh()
    {
        _extract = DataAccessServices.dao().get(_extractId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EnrOrderStudentForExtractEdit.STUDENT_DS.equals(dataSource.getName()))
        {
            dataSource.put("extractId", _extractId);
        }
    }

    public Student getStudent()
    {
        if(_extract instanceof EnrEnrollmentExtract)
            return ((EnrEnrollmentExtract) _extract).getStudent();
        else if(_extract instanceof EnrAllocationExtract)
            return ((EnrAllocationExtract) _extract).getStudent();
        else if(_extract instanceof EnrEnrollmentMinisteryExtract)
            return ((EnrEnrollmentMinisteryExtract) _extract).getStudent();
        else
            throw new IllegalStateException();
    }

    public void setStudent(Student student)
    {
        if(_extract instanceof EnrEnrollmentExtract)
            ((EnrEnrollmentExtract) _extract).setStudent(student);
        else if(_extract instanceof EnrAllocationExtract)
            ((EnrAllocationExtract) _extract).setStudent(student);
        else if(_extract instanceof EnrEnrollmentMinisteryExtract)
            ((EnrEnrollmentMinisteryExtract) _extract).setStudent(student);
        else
            throw new IllegalStateException();
    }

    public void onClickApply()
    {
        DataAccessServices.dao().update(_extract);
        deactivate();
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }
}
