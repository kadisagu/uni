package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * DEV-5350: Изменить структуру данных - теперь ссылки должны быть переброшены на реальные выписки о зачислении. Все шаги зачисления вместе с их содержимым нужно удалить. Также удалить из схемы данных и из базы все временные сущности по приказам о зачислении.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_24to25 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Теперь ссылки должны быть переброшены на реальные выписки о зачислении.
        tool.table("enr14_enr_step_item_ordinfo_t").foreignKeys().clear(); // ссылка теперь на другую сущность

        // Все шаги зачисления вместе с их содержимым нужно удалить.
        tool.executeUpdate("delete from enr14_enr_step_item_ordinfo_t");
        tool.executeUpdate("delete from enr14_enr_step_item_t");
        tool.executeUpdate("delete from enr14_enr_step_stage_t");
        tool.executeUpdate("delete from enr14_enr_step_t");

        // Также удалить из схемы данных и из базы все временные сущности по приказам о зачислении.
        tool.executeUpdate("delete from enr14_mock_extract_t");
        tool.executeUpdate("delete from enr14_mock_order_t");

    }
}