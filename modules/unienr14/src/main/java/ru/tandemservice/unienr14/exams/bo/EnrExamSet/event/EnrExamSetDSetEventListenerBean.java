package ru.tandemservice.unienr14.exams.bo.EnrExamSet.event;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;

import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.daemon.IEnrExamSetDaemonDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;


/**
 * @author vdanilov
 */
public class EnrExamSetDSetEventListenerBean {



    // при изменении группы проверяет, что ее нет ни в одном наборе (в том числе при удалении самой группы - т.к. в ней каскадно удаляются элементы)
    public static class EnrCampaignDisciplineGroupElementChangeListener implements IDSetEventListener {
        public static final EnrCampaignDisciplineGroupElementChangeListener INSTANCE = new EnrCampaignDisciplineGroupElementChangeListener();
        @Override public void onEvent(final DSetEvent event)
        {
            final DQLExecutionContext context = event.getContext();
            final List<Object[]> rows = new DQLSelectBuilder()
            .fromEntity(EnrCampaignDisciplineGroupElement.class, "ge")
            .where(in(property("ge.id"), event.getMultitude().getInExpression()))
            .fromEntity(EnrExamSetElement.class, "se")
            .where(eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("ge")), property(EnrExamSetElement.value().fromAlias("se"))))
            .column(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("ge")))
            .column(property(EnrExamSetElement.examSet().id().fromAlias("se")))
            .predicate(DQLPredicateType.distinct)
            .createStatement(context).list();

            if (rows.isEmpty()) { return; }

            // собираем мапу
            final Map<Long, List<Long>> map = SafeMap.get(ArrayList.class);
            for (final Object[] row: rows) {
                map.get((Long)row[0]).add((Long)row[1]);
            }

            if (1 == map.size()) {
                // если там всего лишь одна группа, то выводим сообщение с указанием группы и наборов ви
                throw getDetailException(context, map.entrySet().iterator().next());
            }

            // иначе (какое-то массовое действи) выводим общее сообщение
            final ApplicationException exception = new ApplicationException(EnrExamSetManager.instance().getProperty("error.group-content-change.group-is-in-use.title"));
            if (Debug.isEnabled()) {
                try {
                    for (final Map.Entry<Long, List<Long>> e: map.entrySet()) {
                        exception.addSuppressed(getDetailException(context, e));
                    }
                } catch (final Throwable t) {
                    // do nothing
                }
            }
            throw exception;
        }

        protected ApplicationException getDetailException(DQLExecutionContext context, final Map.Entry<Long, List<Long>> e)
        {
            final StringBuilder sb = new StringBuilder();
            final String disciplineGroupTitle = getDisciplineGroupTitle(context, e.getKey());

            for (final Long examSetId: e.getValue()) {
                // поидее так делать нельзя (у нас listener и в нем нет сессии)
                final EnrExamSet examSet = DataAccessServices.dao().get(EnrExamSet.class, examSetId);
                if (sb.length() > 0) { sb.append(", "); }
                sb.append("«").append(EnrExamSetManager.instance().examSetElementTitle(examSet.getElementList(), ", ")).append("»");
            }

            final ApplicationException detailException = new ApplicationException(
                EnrExamSetManager.instance().getProperty(
                    "error.group-content-change.group-is-in-use.detail",
                    disciplineGroupTitle,
                    sb.toString()
                )
            );
            return detailException;
        }

        protected String getDisciplineGroupTitle(DQLExecutionContext context, final Long disciplineId)
        {
            final EnrCampaignDisciplineGroup disciplineGroup = DataAccessServices.dao().get(EnrCampaignDisciplineGroup.class, disciplineId);
            if (null != disciplineGroup) { return disciplineGroup.getTitle(); }

            // вах, у нас нет группы - что это значит...
            // а это значит, что у нас уже удалена группа (на уровне hibernate, но в базе она еще есть)
            // придется извлекать руками

            return new DQLSelectBuilder()
            .fromEntity(EnrCampaignDisciplineGroup.class, "tmp")
            .column(property(EnrCampaignDisciplineGroup.title().fromAlias("tmp")))
            .where(eq(property("tmp.id"), value(disciplineId)))
            .createStatement(context).<String>uniqueResult();
        }
    }

    // при изменении группы или элементов набора ВИ, проверяет что дисциплины не повторяются
    public static class EnrExamSetUniqueDisciplineListListener extends ParamTransactionCompleteListener<Boolean>  {
        public static final EnrExamSetUniqueDisciplineListListener INSTANCE = new EnrExamSetUniqueDisciplineListListener();

        @Override
        public Collection<Long> getIds(final DSetEvent event) {
            final Class entityClass = event.getMultitude().getEntityMeta().getEntityClass();

            if (EnrExamSetElement.class.isAssignableFrom(entityClass))
            {
                // если это элемент набора ВИ, берем оттуда набор ВИ
                return new DQLSelectBuilder()
                .predicate(DQLPredicateType.distinct)
                .fromEntity(EnrExamSetElement.class, "el")
                .where(in(property("el.id"), event.getMultitude().getInExpression()))
                .column(property(EnrExamSetElement.examSet().id().fromAlias("el")))
                .createStatement(event.getContext()).list();
            }

            if (EnrCampaignDisciplineGroupElement.class.isAssignableFrom(entityClass))
            {
                // если это элемент группы, берем все наборы, связанные с группой
                return new DQLSelectBuilder()
                .predicate(DQLPredicateType.distinct)
                .fromEntity(EnrCampaignDisciplineGroupElement.class, "gel")
                .where(in(property("gel.id"), event.getMultitude().getInExpression()))
                .fromEntity(EnrExamSetElement.class, "el")
                .where(eq(property(EnrExamSetElement.value().fromAlias("el")), property(EnrCampaignDisciplineGroupElement.group().fromAlias("gel"))))
                .column(property(EnrExamSetElement.examSet().id().fromAlias("el")))
                .createStatement(event.getContext()).list();
            }

            throw new IllegalArgumentException(entityClass.getName());
        }

        @Override
        public Boolean beforeCompletion(final Session session, final Collection<Long> ids) {
            // маленькое число обънктов за раз, потому что запрос внутри будет сложным
            // очень маловероятно, что после завершения транзакции поменяются очень много наборов

            final List<ApplicationException> exceptionList = new ArrayList<>();
            BatchUtils.execute(ids, 64, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> ids) {

                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(EnrExamSetElement.class, "el")
                    .column(property(EnrExamSetElement.examSet().id().fromAlias("el")))
                    .group(property(EnrExamSetElement.examSet().id().fromAlias("el")))

                    .joinEntity("el", DQLJoinType.left, EnrCampaignDisciplineGroupElement.class, "gel", and(
                        eq(property(EnrExamSetElement.value().fromAlias("el")), property(EnrCampaignDisciplineGroupElement.group().fromAlias("gel")))
                    ))
                    .joinEntity("gel", DQLJoinType.left, EnrCampaignDiscipline.class, "dsc", or(
                        eq(property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("gel")), property("dsc")),
                        eq(property(EnrExamSetElement.value().fromAlias("el")), property("dsc"))
                    ))

                    .where(in(property(EnrExamSetElement.examSet().id().fromAlias("el")), ids))
                    .where(isNotNull("dsc.id"))


                    .column(property("dsc.id"))
                    .group(property("dsc.id"))

                    .column(count("el.id"))
                    .having(gt(count("el.id"), value(1)))

                    .createStatement(session).list();

                    for (final Object[] row: rows) {
                        final EnrExamSet examSet = DataAccessServices.dao().get(EnrExamSet.class, (Long)row[0]);
                        final EnrCampaignDiscipline dsc = DataAccessServices.dao().get(EnrCampaignDiscipline.class, (Long)row[1]);
                        exceptionList.add(
                            new ApplicationException(EnrExamSetManager.instance().getProperty(
                                "error.group-content-change.group-discipline-is-not-unique.detail",
                                EnrExamSetManager.instance().examSetElementTitle(examSet.getElementList(), ", "),
                                dsc.getTitle()
                            ))
                        );
                    }
                }
            });

            if (exceptionList.size() > 0) {
                if (1 == exceptionList.size()) {
                    // если вдруг так случилось, и исключение у нас одно, то его и выводим
                    throw exceptionList.iterator().next();
                }

                // если же ошибок много, то выводим общую фразу, что все плохо
                final ApplicationException exception = new ApplicationException(EnrExamSetManager.instance().getProperty("error.group-content-change.group-discipline-is-not-unique.title"));
                for (final ApplicationException e: exceptionList) {
                    exception.addSuppressed(e);
                }
                throw exception;
            }


            // если все ок, то вызываем в той же транзакции обновление дисциплин
            return IEnrExamSetDaemonDao.instance.get().doUpdateExamSetDisciplines();
        }
    }


    public void init() {


        // проверка изменений
        {
            // при изменении группы проверять, что ее нет ни в одном наборе
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElementChangeListener.INSTANCE);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElementChangeListener.INSTANCE);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElementChangeListener.INSTANCE);
        }

        // проверка уникальности
        {
            // при изменении набора ВИ проверять уникальность дисциплин в наборе ВИ
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrExamSetElement.class, EnrExamSetUniqueDisciplineListListener.INSTANCE);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrExamSetElement.class, EnrExamSetUniqueDisciplineListListener.INSTANCE);

            // при изменении набора группы проверять уникальность дисциплин во всех наборах ВИ
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrCampaignDisciplineGroupElement.class, EnrExamSetUniqueDisciplineListListener.INSTANCE);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrCampaignDisciplineGroupElement.class, EnrExamSetUniqueDisciplineListListener.INSTANCE);
        }



    }

}
