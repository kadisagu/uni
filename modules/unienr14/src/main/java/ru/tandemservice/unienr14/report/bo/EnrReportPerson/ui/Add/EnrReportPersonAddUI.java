package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.print.contacts.ContactsPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.print.education.EducationPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.print.person.PersonPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.armyData.ArmyDataParam;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.egeData.EgeDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.egeData.EgeDataParam;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.enrollmentData.EnrollmentDataParam;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantData.EntrantDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantData.EntrantDataParam;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataParam;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.passDisciplineData.PassDisciplineDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.passDisciplineData.PassDisciplineDataParam;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.contacts.EnrContactsPrintBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.education.EnrEducationPrintBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.entrant.EntrantPrintBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.request.EntrantRequestPrintBlock;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
@Input({
        @Bind(key = "summaryReport", binding = "summaryReport")
})
public class EnrReportPersonAddUI extends UIPresenter implements IReportDQLModifierOwner, ReportPersonAddUI.IChildPresenter
{
    public static final String ENTRANT_SCHEET = "enr14EntrantScheet";

    private Boolean _summaryReport;// true, если отчет вызывает как "Выборка персон"

    // tab flag
    private boolean _entrantScheet;

    private String _selectedTab = EnrReportPersonAdd.ENTRANT_DATA_TAB;
    private EntrantDataParam _entrantData = new EntrantDataParam(this);
    private ArmyDataParam _armyData = new ArmyDataParam();
    private EgeDataParam _egeData = new EgeDataParam();
    private IReportDQLModifier _enrollmentData = new EnrollmentDataParam();
    private PassDisciplineDataParam _passDisciplineData = new PassDisciplineDataParam();
    private EntrantRequestDataParam _entrantRequestData = new EntrantRequestDataParam(this);

    // double row data
    private boolean _doubleEntrantRowData = true;

    // print blocks
    private IReportPrintBlock _entrantPrintBlock = new EntrantPrintBlock();
    private IReportPrintBlock _entrantRequestPrintBlock = new EntrantRequestPrintBlock();

    private IReportPrintBlock _personPrintBlock = new PersonPrintBlock();
    private IReportPrintBlock _educationPrintBlock = new EnrEducationPrintBlock();
    private IReportPrintBlock _contactsPrintBlock = new EnrContactsPrintBlock();


    @Override
    public void onComponentActivate()
    {
        // выбираем по умолчанию приемную кампанию
        _entrantData.getEnrollmentCampaign().setActive(true);

        if(_entrantData.getEnrollmentCampaign().isActive() && _entrantData.getEnrollmentCampaign().getData() == null)
        {
            _entrantData.getEnrollmentCampaign().setData(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        }
        final EnrCompetitionFilterAddon entrantRequestUtil = (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
        configUtil(entrantRequestUtil);
        entrantRequestUtil.setRefresh(false);
    }

    @Override
    public void onComponentRefresh()
    {
        _entrantScheet = ((ReportPersonAddUI) _uiSupport.getParentUI()).isScheetVisible(ENTRANT_SCHEET);

        // является абитуриентом
        _entrantData.getEntrant().setActive(true);
        if(_entrantData.getEntrant().isActive() && _entrantData.getEntrant().getData() == null)
        {
            _entrantData.getEntrant().setData(TwinComboDataSourceHandler.getYesOption());
        }
        // выбираем по умолчанию неархивных абитуриентов
        _entrantData.getEntrantArchival().setActive(true);

        if(_entrantData.getEntrantArchival().isActive() && _entrantData.getEntrantArchival().getData() == null)
        {
            _entrantData.getEntrantArchival().setData(TwinComboDataSourceHandler.getNoOption());
        }
    }

    public void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true);

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_WITH_MISS_EMPTY_VALUES_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public void onChangeEnrollmentCampaign()
    {
        final EnrCompetitionFilterAddon entrantRequestUtil = (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
        configUtilWhere(entrantRequestUtil);
    }

    public void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        EnrEnrollmentCampaign campaign = _entrantData.getEnrollmentCampaign().getData();

        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), campaign));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        _entrantData.getEnrollmentCampaign().putParamIfActive(dataSource, EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

        if (_entrantScheet)
        {
            EntrantDataBlock.onBeforeDataSourceFetch(dataSource, _entrantData);
            EgeDataBlock.onBeforeDataSourceFetch(dataSource, _egeData);
            EntrantRequestDataBlock.onBeforeDataSourceFetch(dataSource, _entrantRequestData);
            PassDisciplineDataBlock.onBeforeDataSourceFetch(dataSource, _entrantData.getEnrollmentCampaign());
        }

        EntrantRequestDataBlock.onBeforeDataSourceFetch(dataSource, _entrantData);
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        if (_entrantScheet)
        {
            _entrantData.validateReportParams(errorCollector);
            _enrollmentData.validateReportParams(errorCollector);
            _armyData.validateReportParams(errorCollector);
            _egeData.validateReportParams(errorCollector);
            _passDisciplineData.validateReportParams(errorCollector);
            _entrantRequestData.validateReportParams(errorCollector);
        }
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_entrantScheet)
        {
            printInfo.addSheet(ENTRANT_SCHEET);

            printInfo.setDoubleRowData(ENTRANT_SCHEET, _doubleEntrantRowData);

            // фильтры модифицируют запросы
            _entrantData.modify(dql, printInfo);
            _enrollmentData.modify(dql, printInfo);
            _armyData.modify(dql, printInfo);
            _egeData.modify(dql, printInfo);
            _passDisciplineData.modify(dql, printInfo);
            _entrantRequestData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
            ((EntrantPrintBlock) _entrantPrintBlock).modify(dql, printInfo, Arrays.asList(
                    _educationPrintBlock,
                    _contactsPrintBlock
            ));
            _entrantRequestPrintBlock.modify(dql, printInfo);
        }
    }

    @Override
    public void onClickApplyAction()
    {
        final EnrCompetitionFilterAddon entrantRequestUtil = (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
        entrantRequestUtil.saveSettings();
    }

    // Getters

    public EntrantDataParam getEntrantData()
    {
        return _entrantData;
    }

    public IReportDQLModifier getEnrollmentData() { return _enrollmentData; }

    public ArmyDataParam getArmyData()
    {
        return _armyData;
    }

    public EgeDataParam getEgeData()
    {
        return _egeData;
    }

    public PassDisciplineDataParam getPassDisciplineData()
    {
        return _passDisciplineData;
    }

    public EntrantRequestDataParam getEntrantRequestData()
    {
        return _entrantRequestData;
    }

    public IReportPrintBlock getEntrantPrintBlock()
    {
        return _entrantPrintBlock;
    }

    public IReportPrintBlock getEntrantRequestPrintBlock()
    {
        return _entrantRequestPrintBlock;
    }

    // Getters & Setters

    public boolean isEntrantScheet()
    {
        return _entrantScheet;
    }

    public void setEntrantScheet(boolean entrantScheet)
    {
        _entrantScheet = entrantScheet;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public boolean isTrue()
    {
        return true;
    }

    public Boolean getSummaryReport()
    {
        return _summaryReport;
    }

    public void setSummaryReport(Boolean summaryReport)
    {
        _summaryReport = summaryReport;
    }

    public boolean isDoubleEntrantRowData()
    {
        return _doubleEntrantRowData;
    }

    public void setDoubleEntrantRowData(boolean doubleEntrantRowData)
    {
        _doubleEntrantRowData = doubleEntrantRowData;
    }

    public IReportPrintBlock getPersonPrintBlock() {
        return _personPrintBlock;
    }

    public IReportPrintBlock getEnrEducationPrintBlock() {
        return _educationPrintBlock;
    }

    public IReportPrintBlock getEnrContactsPrintBlock() {
        return _contactsPrintBlock;
    }
}
