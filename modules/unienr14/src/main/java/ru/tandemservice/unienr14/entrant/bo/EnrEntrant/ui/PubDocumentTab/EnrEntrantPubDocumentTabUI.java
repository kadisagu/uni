/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author oleyba
 * @since 4/22/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id"),
    @Bind(key = "selectedDataTab")
})
public class EnrEntrantPubDocumentTabUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();
    private String selectedDataTab;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    public String getRegionName()
    {
        return EnrEntrantPubDocumentTab.TAB_PANEL_REGION_NAME;
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public String getSelectedDataTab()
    {
        return selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        this.selectedDataTab = selectedDataTab;
    }
}
