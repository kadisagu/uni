package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.FamilyStatus;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Онлайн-абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant";
    public static final String ENTITY_NAME = "enrOnlineEntrant";
    public static final int VERSION_HASH = 363466266;
    private static IEntityMeta ENTITY_META;

    public static final String P_REG_ADDRESS = "regAddress";
    public static final String P_SAME_ADDRESS = "sameAddress";
    public static final String P_FACT_ADDRESS = "factAddress";
    public static final String P_NEED_DORM = "needDorm";
    public static final String P_CONTACT_PHONE = "contactPhone";
    public static final String P_MOBILE_PHONE = "mobilePhone";
    public static final String P_WORK_PHONE = "workPhone";
    public static final String P_EMAIL = "email";
    public static final String P_CHILDS = "childs";
    public static final String P_CURRENT_WORK = "currentWork";
    public static final String P_CURRENT_POST = "currentPost";
    public static final String P_ARMY_START_DATE = "armyStartDate";
    public static final String P_ARMY_END_DATE = "armyEndDate";
    public static final String P_ARMY_END_YEAR = "armyEndYear";
    public static final String L_IDENTITY_CARD = "identityCard";
    public static final String L_FAMILY_STATUS = "familyStatus";
    public static final String L_FOREIGN_LANGUAGE = "foreignLanguage";
    public static final String P_ONLINE_ENTRANT = "onlineEntrant";
    public static final String P_PERSONAL_NUMBER = "personalNumber";
    public static final String P_REG_DATE = "regDate";
    public static final String L_ENR_ENROLLMENT_CAMPAIGN = "enrEnrollmentCampaign";
    public static final String L_ENTRANT = "entrant";
    public static final String L_ID_CARD_SCAN_COPY = "idCardScanCopy";
    public static final String L_IND_ACHIEV_SCAN_COPY = "indAchievScanCopy";
    public static final String L_OTHER_DOC_SCAN_COPY = "otherDocScanCopy";

    private String _regAddress;     // Адрес регистрации
    private boolean _sameAddress = false;     // Адрес регистрации и фактический адрес совпадают
    private String _factAddress;     // Фактический адрес
    private boolean _needDorm = false;     // Нуждается в общежитии
    private String _contactPhone;     // Контактный телефон
    private String _mobilePhone;     // Мобильный телефон
    private String _workPhone;     // Рабочий телефон
    private String _email;     // E-mail
    private Integer _childs;     // Количество детей
    private String _currentWork;     // Место работы в настоящее время
    private String _currentPost;     // Должность
    private Date _armyStartDate;     // Дата начала службы
    private Date _armyEndDate;     // Дата окончания службы
    private Integer _armyEndYear;     // Год увольнения в запас
    private IdentityCard _identityCard;     // Удостоверение личности
    private FamilyStatus _familyStatus;     // Семейное положение
    private ForeignLanguage _foreignLanguage;     // Иностранный язык
    private long _onlineEntrant;     // Id из онлайн-регистрации
    private int _personalNumber;     // Регистрационный номер
    private Date _regDate;     // Дата регистрации
    private EnrEnrollmentCampaign _enrEnrollmentCampaign;     // Приемная кампания
    private EnrEntrant _entrant;     // Абитуриент
    private DatabaseFile _idCardScanCopy;     // Скан-копии документов УЛ
    private DatabaseFile _indAchievScanCopy;     // Скан-копии документов, подтверждающих ИД
    private DatabaseFile _otherDocScanCopy;     // Скан-копии иных документов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Адрес регистрации.
     */
    @Length(max=255)
    public String getRegAddress()
    {
        return _regAddress;
    }

    /**
     * @param regAddress Адрес регистрации.
     */
    public void setRegAddress(String regAddress)
    {
        dirty(_regAddress, regAddress);
        _regAddress = regAddress;
    }

    /**
     * @return Адрес регистрации и фактический адрес совпадают. Свойство не может быть null.
     */
    @NotNull
    public boolean isSameAddress()
    {
        return _sameAddress;
    }

    /**
     * @param sameAddress Адрес регистрации и фактический адрес совпадают. Свойство не может быть null.
     */
    public void setSameAddress(boolean sameAddress)
    {
        dirty(_sameAddress, sameAddress);
        _sameAddress = sameAddress;
    }

    /**
     * @return Фактический адрес.
     */
    @Length(max=255)
    public String getFactAddress()
    {
        return _factAddress;
    }

    /**
     * @param factAddress Фактический адрес.
     */
    public void setFactAddress(String factAddress)
    {
        dirty(_factAddress, factAddress);
        _factAddress = factAddress;
    }

    /**
     * @return Нуждается в общежитии. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedDorm()
    {
        return _needDorm;
    }

    /**
     * @param needDorm Нуждается в общежитии. Свойство не может быть null.
     */
    public void setNeedDorm(boolean needDorm)
    {
        dirty(_needDorm, needDorm);
        _needDorm = needDorm;
    }

    /**
     * @return Контактный телефон.
     */
    @Length(max=255)
    public String getContactPhone()
    {
        return _contactPhone;
    }

    /**
     * @param contactPhone Контактный телефон.
     */
    public void setContactPhone(String contactPhone)
    {
        dirty(_contactPhone, contactPhone);
        _contactPhone = contactPhone;
    }

    /**
     * @return Мобильный телефон.
     */
    @Length(max=255)
    public String getMobilePhone()
    {
        return _mobilePhone;
    }

    /**
     * @param mobilePhone Мобильный телефон.
     */
    public void setMobilePhone(String mobilePhone)
    {
        dirty(_mobilePhone, mobilePhone);
        _mobilePhone = mobilePhone;
    }

    /**
     * @return Рабочий телефон.
     */
    @Length(max=255)
    public String getWorkPhone()
    {
        return _workPhone;
    }

    /**
     * @param workPhone Рабочий телефон.
     */
    public void setWorkPhone(String workPhone)
    {
        dirty(_workPhone, workPhone);
        _workPhone = workPhone;
    }

    /**
     * @return E-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return Количество детей.
     */
    public Integer getChilds()
    {
        return _childs;
    }

    /**
     * @param childs Количество детей.
     */
    public void setChilds(Integer childs)
    {
        dirty(_childs, childs);
        _childs = childs;
    }

    /**
     * @return Место работы в настоящее время.
     */
    @Length(max=255)
    public String getCurrentWork()
    {
        return _currentWork;
    }

    /**
     * @param currentWork Место работы в настоящее время.
     */
    public void setCurrentWork(String currentWork)
    {
        dirty(_currentWork, currentWork);
        _currentWork = currentWork;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getCurrentPost()
    {
        return _currentPost;
    }

    /**
     * @param currentPost Должность.
     */
    public void setCurrentPost(String currentPost)
    {
        dirty(_currentPost, currentPost);
        _currentPost = currentPost;
    }

    /**
     * @return Дата начала службы.
     */
    public Date getArmyStartDate()
    {
        return _armyStartDate;
    }

    /**
     * @param armyStartDate Дата начала службы.
     */
    public void setArmyStartDate(Date armyStartDate)
    {
        dirty(_armyStartDate, armyStartDate);
        _armyStartDate = armyStartDate;
    }

    /**
     * @return Дата окончания службы.
     */
    public Date getArmyEndDate()
    {
        return _armyEndDate;
    }

    /**
     * @param armyEndDate Дата окончания службы.
     */
    public void setArmyEndDate(Date armyEndDate)
    {
        dirty(_armyEndDate, armyEndDate);
        _armyEndDate = armyEndDate;
    }

    /**
     * @return Год увольнения в запас.
     */
    public Integer getArmyEndYear()
    {
        return _armyEndYear;
    }

    /**
     * @param armyEndYear Год увольнения в запас.
     */
    public void setArmyEndYear(Integer armyEndYear)
    {
        dirty(_armyEndYear, armyEndYear);
        _armyEndYear = armyEndYear;
    }

    /**
     * @return Удостоверение личности.
     */
    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    /**
     * @param identityCard Удостоверение личности.
     */
    public void setIdentityCard(IdentityCard identityCard)
    {
        dirty(_identityCard, identityCard);
        _identityCard = identityCard;
    }

    /**
     * @return Семейное положение.
     */
    public FamilyStatus getFamilyStatus()
    {
        return _familyStatus;
    }

    /**
     * @param familyStatus Семейное положение.
     */
    public void setFamilyStatus(FamilyStatus familyStatus)
    {
        dirty(_familyStatus, familyStatus);
        _familyStatus = familyStatus;
    }

    /**
     * @return Иностранный язык.
     */
    public ForeignLanguage getForeignLanguage()
    {
        return _foreignLanguage;
    }

    /**
     * @param foreignLanguage Иностранный язык.
     */
    public void setForeignLanguage(ForeignLanguage foreignLanguage)
    {
        dirty(_foreignLanguage, foreignLanguage);
        _foreignLanguage = foreignLanguage;
    }

    /**
     * @return Id из онлайн-регистрации. Свойство не может быть null.
     */
    @NotNull
    public long getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Id из онлайн-регистрации. Свойство не может быть null.
     */
    public void setOnlineEntrant(long onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     */
    @NotNull
    public int getPersonalNumber()
    {
        return _personalNumber;
    }

    /**
     * @param personalNumber Регистрационный номер. Свойство не может быть null.
     */
    public void setPersonalNumber(int personalNumber)
    {
        dirty(_personalNumber, personalNumber);
        _personalNumber = personalNumber;
    }

    /**
     * @return Дата регистрации.
     */
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата регистрации.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return _enrEnrollmentCampaign;
    }

    /**
     * @param enrEnrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrEnrollmentCampaign(EnrEnrollmentCampaign enrEnrollmentCampaign)
    {
        dirty(_enrEnrollmentCampaign, enrEnrollmentCampaign);
        _enrEnrollmentCampaign = enrEnrollmentCampaign;
    }

    /**
     * @return Абитуриент.
     */
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Скан-копии документов УЛ.
     */
    public DatabaseFile getIdCardScanCopy()
    {
        return _idCardScanCopy;
    }

    /**
     * @param idCardScanCopy Скан-копии документов УЛ.
     */
    public void setIdCardScanCopy(DatabaseFile idCardScanCopy)
    {
        dirty(_idCardScanCopy, idCardScanCopy);
        _idCardScanCopy = idCardScanCopy;
    }

    /**
     * @return Скан-копии документов, подтверждающих ИД.
     */
    public DatabaseFile getIndAchievScanCopy()
    {
        return _indAchievScanCopy;
    }

    /**
     * @param indAchievScanCopy Скан-копии документов, подтверждающих ИД.
     */
    public void setIndAchievScanCopy(DatabaseFile indAchievScanCopy)
    {
        dirty(_indAchievScanCopy, indAchievScanCopy);
        _indAchievScanCopy = indAchievScanCopy;
    }

    /**
     * @return Скан-копии иных документов.
     */
    public DatabaseFile getOtherDocScanCopy()
    {
        return _otherDocScanCopy;
    }

    /**
     * @param otherDocScanCopy Скан-копии иных документов.
     */
    public void setOtherDocScanCopy(DatabaseFile otherDocScanCopy)
    {
        dirty(_otherDocScanCopy, otherDocScanCopy);
        _otherDocScanCopy = otherDocScanCopy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantGen)
        {
            setRegAddress(((EnrOnlineEntrant)another).getRegAddress());
            setSameAddress(((EnrOnlineEntrant)another).isSameAddress());
            setFactAddress(((EnrOnlineEntrant)another).getFactAddress());
            setNeedDorm(((EnrOnlineEntrant)another).isNeedDorm());
            setContactPhone(((EnrOnlineEntrant)another).getContactPhone());
            setMobilePhone(((EnrOnlineEntrant)another).getMobilePhone());
            setWorkPhone(((EnrOnlineEntrant)another).getWorkPhone());
            setEmail(((EnrOnlineEntrant)another).getEmail());
            setChilds(((EnrOnlineEntrant)another).getChilds());
            setCurrentWork(((EnrOnlineEntrant)another).getCurrentWork());
            setCurrentPost(((EnrOnlineEntrant)another).getCurrentPost());
            setArmyStartDate(((EnrOnlineEntrant)another).getArmyStartDate());
            setArmyEndDate(((EnrOnlineEntrant)another).getArmyEndDate());
            setArmyEndYear(((EnrOnlineEntrant)another).getArmyEndYear());
            setIdentityCard(((EnrOnlineEntrant)another).getIdentityCard());
            setFamilyStatus(((EnrOnlineEntrant)another).getFamilyStatus());
            setForeignLanguage(((EnrOnlineEntrant)another).getForeignLanguage());
            setOnlineEntrant(((EnrOnlineEntrant)another).getOnlineEntrant());
            setPersonalNumber(((EnrOnlineEntrant)another).getPersonalNumber());
            setRegDate(((EnrOnlineEntrant)another).getRegDate());
            setEnrEnrollmentCampaign(((EnrOnlineEntrant)another).getEnrEnrollmentCampaign());
            setEntrant(((EnrOnlineEntrant)another).getEntrant());
            setIdCardScanCopy(((EnrOnlineEntrant)another).getIdCardScanCopy());
            setIndAchievScanCopy(((EnrOnlineEntrant)another).getIndAchievScanCopy());
            setOtherDocScanCopy(((EnrOnlineEntrant)another).getOtherDocScanCopy());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrant.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "regAddress":
                    return obj.getRegAddress();
                case "sameAddress":
                    return obj.isSameAddress();
                case "factAddress":
                    return obj.getFactAddress();
                case "needDorm":
                    return obj.isNeedDorm();
                case "contactPhone":
                    return obj.getContactPhone();
                case "mobilePhone":
                    return obj.getMobilePhone();
                case "workPhone":
                    return obj.getWorkPhone();
                case "email":
                    return obj.getEmail();
                case "childs":
                    return obj.getChilds();
                case "currentWork":
                    return obj.getCurrentWork();
                case "currentPost":
                    return obj.getCurrentPost();
                case "armyStartDate":
                    return obj.getArmyStartDate();
                case "armyEndDate":
                    return obj.getArmyEndDate();
                case "armyEndYear":
                    return obj.getArmyEndYear();
                case "identityCard":
                    return obj.getIdentityCard();
                case "familyStatus":
                    return obj.getFamilyStatus();
                case "foreignLanguage":
                    return obj.getForeignLanguage();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
                case "personalNumber":
                    return obj.getPersonalNumber();
                case "regDate":
                    return obj.getRegDate();
                case "enrEnrollmentCampaign":
                    return obj.getEnrEnrollmentCampaign();
                case "entrant":
                    return obj.getEntrant();
                case "idCardScanCopy":
                    return obj.getIdCardScanCopy();
                case "indAchievScanCopy":
                    return obj.getIndAchievScanCopy();
                case "otherDocScanCopy":
                    return obj.getOtherDocScanCopy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "regAddress":
                    obj.setRegAddress((String) value);
                    return;
                case "sameAddress":
                    obj.setSameAddress((Boolean) value);
                    return;
                case "factAddress":
                    obj.setFactAddress((String) value);
                    return;
                case "needDorm":
                    obj.setNeedDorm((Boolean) value);
                    return;
                case "contactPhone":
                    obj.setContactPhone((String) value);
                    return;
                case "mobilePhone":
                    obj.setMobilePhone((String) value);
                    return;
                case "workPhone":
                    obj.setWorkPhone((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "childs":
                    obj.setChilds((Integer) value);
                    return;
                case "currentWork":
                    obj.setCurrentWork((String) value);
                    return;
                case "currentPost":
                    obj.setCurrentPost((String) value);
                    return;
                case "armyStartDate":
                    obj.setArmyStartDate((Date) value);
                    return;
                case "armyEndDate":
                    obj.setArmyEndDate((Date) value);
                    return;
                case "armyEndYear":
                    obj.setArmyEndYear((Integer) value);
                    return;
                case "identityCard":
                    obj.setIdentityCard((IdentityCard) value);
                    return;
                case "familyStatus":
                    obj.setFamilyStatus((FamilyStatus) value);
                    return;
                case "foreignLanguage":
                    obj.setForeignLanguage((ForeignLanguage) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((Long) value);
                    return;
                case "personalNumber":
                    obj.setPersonalNumber((Integer) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "enrEnrollmentCampaign":
                    obj.setEnrEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "idCardScanCopy":
                    obj.setIdCardScanCopy((DatabaseFile) value);
                    return;
                case "indAchievScanCopy":
                    obj.setIndAchievScanCopy((DatabaseFile) value);
                    return;
                case "otherDocScanCopy":
                    obj.setOtherDocScanCopy((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "regAddress":
                        return true;
                case "sameAddress":
                        return true;
                case "factAddress":
                        return true;
                case "needDorm":
                        return true;
                case "contactPhone":
                        return true;
                case "mobilePhone":
                        return true;
                case "workPhone":
                        return true;
                case "email":
                        return true;
                case "childs":
                        return true;
                case "currentWork":
                        return true;
                case "currentPost":
                        return true;
                case "armyStartDate":
                        return true;
                case "armyEndDate":
                        return true;
                case "armyEndYear":
                        return true;
                case "identityCard":
                        return true;
                case "familyStatus":
                        return true;
                case "foreignLanguage":
                        return true;
                case "onlineEntrant":
                        return true;
                case "personalNumber":
                        return true;
                case "regDate":
                        return true;
                case "enrEnrollmentCampaign":
                        return true;
                case "entrant":
                        return true;
                case "idCardScanCopy":
                        return true;
                case "indAchievScanCopy":
                        return true;
                case "otherDocScanCopy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "regAddress":
                    return true;
                case "sameAddress":
                    return true;
                case "factAddress":
                    return true;
                case "needDorm":
                    return true;
                case "contactPhone":
                    return true;
                case "mobilePhone":
                    return true;
                case "workPhone":
                    return true;
                case "email":
                    return true;
                case "childs":
                    return true;
                case "currentWork":
                    return true;
                case "currentPost":
                    return true;
                case "armyStartDate":
                    return true;
                case "armyEndDate":
                    return true;
                case "armyEndYear":
                    return true;
                case "identityCard":
                    return true;
                case "familyStatus":
                    return true;
                case "foreignLanguage":
                    return true;
                case "onlineEntrant":
                    return true;
                case "personalNumber":
                    return true;
                case "regDate":
                    return true;
                case "enrEnrollmentCampaign":
                    return true;
                case "entrant":
                    return true;
                case "idCardScanCopy":
                    return true;
                case "indAchievScanCopy":
                    return true;
                case "otherDocScanCopy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "regAddress":
                    return String.class;
                case "sameAddress":
                    return Boolean.class;
                case "factAddress":
                    return String.class;
                case "needDorm":
                    return Boolean.class;
                case "contactPhone":
                    return String.class;
                case "mobilePhone":
                    return String.class;
                case "workPhone":
                    return String.class;
                case "email":
                    return String.class;
                case "childs":
                    return Integer.class;
                case "currentWork":
                    return String.class;
                case "currentPost":
                    return String.class;
                case "armyStartDate":
                    return Date.class;
                case "armyEndDate":
                    return Date.class;
                case "armyEndYear":
                    return Integer.class;
                case "identityCard":
                    return IdentityCard.class;
                case "familyStatus":
                    return FamilyStatus.class;
                case "foreignLanguage":
                    return ForeignLanguage.class;
                case "onlineEntrant":
                    return Long.class;
                case "personalNumber":
                    return Integer.class;
                case "regDate":
                    return Date.class;
                case "enrEnrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "entrant":
                    return EnrEntrant.class;
                case "idCardScanCopy":
                    return DatabaseFile.class;
                case "indAchievScanCopy":
                    return DatabaseFile.class;
                case "otherDocScanCopy":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrant> _dslPath = new Path<EnrOnlineEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrant");
    }
            

    /**
     * @return Адрес регистрации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getRegAddress()
     */
    public static PropertyPath<String> regAddress()
    {
        return _dslPath.regAddress();
    }

    /**
     * @return Адрес регистрации и фактический адрес совпадают. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#isSameAddress()
     */
    public static PropertyPath<Boolean> sameAddress()
    {
        return _dslPath.sameAddress();
    }

    /**
     * @return Фактический адрес.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getFactAddress()
     */
    public static PropertyPath<String> factAddress()
    {
        return _dslPath.factAddress();
    }

    /**
     * @return Нуждается в общежитии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#isNeedDorm()
     */
    public static PropertyPath<Boolean> needDorm()
    {
        return _dslPath.needDorm();
    }

    /**
     * @return Контактный телефон.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getContactPhone()
     */
    public static PropertyPath<String> contactPhone()
    {
        return _dslPath.contactPhone();
    }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getMobilePhone()
     */
    public static PropertyPath<String> mobilePhone()
    {
        return _dslPath.mobilePhone();
    }

    /**
     * @return Рабочий телефон.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getWorkPhone()
     */
    public static PropertyPath<String> workPhone()
    {
        return _dslPath.workPhone();
    }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return Количество детей.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getChilds()
     */
    public static PropertyPath<Integer> childs()
    {
        return _dslPath.childs();
    }

    /**
     * @return Место работы в настоящее время.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getCurrentWork()
     */
    public static PropertyPath<String> currentWork()
    {
        return _dslPath.currentWork();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getCurrentPost()
     */
    public static PropertyPath<String> currentPost()
    {
        return _dslPath.currentPost();
    }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getArmyStartDate()
     */
    public static PropertyPath<Date> armyStartDate()
    {
        return _dslPath.armyStartDate();
    }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getArmyEndDate()
     */
    public static PropertyPath<Date> armyEndDate()
    {
        return _dslPath.armyEndDate();
    }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getArmyEndYear()
     */
    public static PropertyPath<Integer> armyEndYear()
    {
        return _dslPath.armyEndYear();
    }

    /**
     * @return Удостоверение личности.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getIdentityCard()
     */
    public static IdentityCard.Path<IdentityCard> identityCard()
    {
        return _dslPath.identityCard();
    }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getFamilyStatus()
     */
    public static FamilyStatus.Path<FamilyStatus> familyStatus()
    {
        return _dslPath.familyStatus();
    }

    /**
     * @return Иностранный язык.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getForeignLanguage()
     */
    public static ForeignLanguage.Path<ForeignLanguage> foreignLanguage()
    {
        return _dslPath.foreignLanguage();
    }

    /**
     * @return Id из онлайн-регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getOnlineEntrant()
     */
    public static PropertyPath<Long> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getPersonalNumber()
     */
    public static PropertyPath<Integer> personalNumber()
    {
        return _dslPath.personalNumber();
    }

    /**
     * @return Дата регистрации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getEnrEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrEnrollmentCampaign()
    {
        return _dslPath.enrEnrollmentCampaign();
    }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Скан-копии документов УЛ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getIdCardScanCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> idCardScanCopy()
    {
        return _dslPath.idCardScanCopy();
    }

    /**
     * @return Скан-копии документов, подтверждающих ИД.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getIndAchievScanCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> indAchievScanCopy()
    {
        return _dslPath.indAchievScanCopy();
    }

    /**
     * @return Скан-копии иных документов.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getOtherDocScanCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> otherDocScanCopy()
    {
        return _dslPath.otherDocScanCopy();
    }

    public static class Path<E extends EnrOnlineEntrant> extends EntityPath<E>
    {
        private PropertyPath<String> _regAddress;
        private PropertyPath<Boolean> _sameAddress;
        private PropertyPath<String> _factAddress;
        private PropertyPath<Boolean> _needDorm;
        private PropertyPath<String> _contactPhone;
        private PropertyPath<String> _mobilePhone;
        private PropertyPath<String> _workPhone;
        private PropertyPath<String> _email;
        private PropertyPath<Integer> _childs;
        private PropertyPath<String> _currentWork;
        private PropertyPath<String> _currentPost;
        private PropertyPath<Date> _armyStartDate;
        private PropertyPath<Date> _armyEndDate;
        private PropertyPath<Integer> _armyEndYear;
        private IdentityCard.Path<IdentityCard> _identityCard;
        private FamilyStatus.Path<FamilyStatus> _familyStatus;
        private ForeignLanguage.Path<ForeignLanguage> _foreignLanguage;
        private PropertyPath<Long> _onlineEntrant;
        private PropertyPath<Integer> _personalNumber;
        private PropertyPath<Date> _regDate;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrEnrollmentCampaign;
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private DatabaseFile.Path<DatabaseFile> _idCardScanCopy;
        private DatabaseFile.Path<DatabaseFile> _indAchievScanCopy;
        private DatabaseFile.Path<DatabaseFile> _otherDocScanCopy;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Адрес регистрации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getRegAddress()
     */
        public PropertyPath<String> regAddress()
        {
            if(_regAddress == null )
                _regAddress = new PropertyPath<String>(EnrOnlineEntrantGen.P_REG_ADDRESS, this);
            return _regAddress;
        }

    /**
     * @return Адрес регистрации и фактический адрес совпадают. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#isSameAddress()
     */
        public PropertyPath<Boolean> sameAddress()
        {
            if(_sameAddress == null )
                _sameAddress = new PropertyPath<Boolean>(EnrOnlineEntrantGen.P_SAME_ADDRESS, this);
            return _sameAddress;
        }

    /**
     * @return Фактический адрес.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getFactAddress()
     */
        public PropertyPath<String> factAddress()
        {
            if(_factAddress == null )
                _factAddress = new PropertyPath<String>(EnrOnlineEntrantGen.P_FACT_ADDRESS, this);
            return _factAddress;
        }

    /**
     * @return Нуждается в общежитии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#isNeedDorm()
     */
        public PropertyPath<Boolean> needDorm()
        {
            if(_needDorm == null )
                _needDorm = new PropertyPath<Boolean>(EnrOnlineEntrantGen.P_NEED_DORM, this);
            return _needDorm;
        }

    /**
     * @return Контактный телефон.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getContactPhone()
     */
        public PropertyPath<String> contactPhone()
        {
            if(_contactPhone == null )
                _contactPhone = new PropertyPath<String>(EnrOnlineEntrantGen.P_CONTACT_PHONE, this);
            return _contactPhone;
        }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getMobilePhone()
     */
        public PropertyPath<String> mobilePhone()
        {
            if(_mobilePhone == null )
                _mobilePhone = new PropertyPath<String>(EnrOnlineEntrantGen.P_MOBILE_PHONE, this);
            return _mobilePhone;
        }

    /**
     * @return Рабочий телефон.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getWorkPhone()
     */
        public PropertyPath<String> workPhone()
        {
            if(_workPhone == null )
                _workPhone = new PropertyPath<String>(EnrOnlineEntrantGen.P_WORK_PHONE, this);
            return _workPhone;
        }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(EnrOnlineEntrantGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return Количество детей.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getChilds()
     */
        public PropertyPath<Integer> childs()
        {
            if(_childs == null )
                _childs = new PropertyPath<Integer>(EnrOnlineEntrantGen.P_CHILDS, this);
            return _childs;
        }

    /**
     * @return Место работы в настоящее время.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getCurrentWork()
     */
        public PropertyPath<String> currentWork()
        {
            if(_currentWork == null )
                _currentWork = new PropertyPath<String>(EnrOnlineEntrantGen.P_CURRENT_WORK, this);
            return _currentWork;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getCurrentPost()
     */
        public PropertyPath<String> currentPost()
        {
            if(_currentPost == null )
                _currentPost = new PropertyPath<String>(EnrOnlineEntrantGen.P_CURRENT_POST, this);
            return _currentPost;
        }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getArmyStartDate()
     */
        public PropertyPath<Date> armyStartDate()
        {
            if(_armyStartDate == null )
                _armyStartDate = new PropertyPath<Date>(EnrOnlineEntrantGen.P_ARMY_START_DATE, this);
            return _armyStartDate;
        }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getArmyEndDate()
     */
        public PropertyPath<Date> armyEndDate()
        {
            if(_armyEndDate == null )
                _armyEndDate = new PropertyPath<Date>(EnrOnlineEntrantGen.P_ARMY_END_DATE, this);
            return _armyEndDate;
        }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getArmyEndYear()
     */
        public PropertyPath<Integer> armyEndYear()
        {
            if(_armyEndYear == null )
                _armyEndYear = new PropertyPath<Integer>(EnrOnlineEntrantGen.P_ARMY_END_YEAR, this);
            return _armyEndYear;
        }

    /**
     * @return Удостоверение личности.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getIdentityCard()
     */
        public IdentityCard.Path<IdentityCard> identityCard()
        {
            if(_identityCard == null )
                _identityCard = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD, this);
            return _identityCard;
        }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getFamilyStatus()
     */
        public FamilyStatus.Path<FamilyStatus> familyStatus()
        {
            if(_familyStatus == null )
                _familyStatus = new FamilyStatus.Path<FamilyStatus>(L_FAMILY_STATUS, this);
            return _familyStatus;
        }

    /**
     * @return Иностранный язык.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getForeignLanguage()
     */
        public ForeignLanguage.Path<ForeignLanguage> foreignLanguage()
        {
            if(_foreignLanguage == null )
                _foreignLanguage = new ForeignLanguage.Path<ForeignLanguage>(L_FOREIGN_LANGUAGE, this);
            return _foreignLanguage;
        }

    /**
     * @return Id из онлайн-регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getOnlineEntrant()
     */
        public PropertyPath<Long> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new PropertyPath<Long>(EnrOnlineEntrantGen.P_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getPersonalNumber()
     */
        public PropertyPath<Integer> personalNumber()
        {
            if(_personalNumber == null )
                _personalNumber = new PropertyPath<Integer>(EnrOnlineEntrantGen.P_PERSONAL_NUMBER, this);
            return _personalNumber;
        }

    /**
     * @return Дата регистрации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(EnrOnlineEntrantGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getEnrEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrEnrollmentCampaign()
        {
            if(_enrEnrollmentCampaign == null )
                _enrEnrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENR_ENROLLMENT_CAMPAIGN, this);
            return _enrEnrollmentCampaign;
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Скан-копии документов УЛ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getIdCardScanCopy()
     */
        public DatabaseFile.Path<DatabaseFile> idCardScanCopy()
        {
            if(_idCardScanCopy == null )
                _idCardScanCopy = new DatabaseFile.Path<DatabaseFile>(L_ID_CARD_SCAN_COPY, this);
            return _idCardScanCopy;
        }

    /**
     * @return Скан-копии документов, подтверждающих ИД.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getIndAchievScanCopy()
     */
        public DatabaseFile.Path<DatabaseFile> indAchievScanCopy()
        {
            if(_indAchievScanCopy == null )
                _indAchievScanCopy = new DatabaseFile.Path<DatabaseFile>(L_IND_ACHIEV_SCAN_COPY, this);
            return _indAchievScanCopy;
        }

    /**
     * @return Скан-копии иных документов.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant#getOtherDocScanCopy()
     */
        public DatabaseFile.Path<DatabaseFile> otherDocScanCopy()
        {
            if(_otherDocScanCopy == null )
                _otherDocScanCopy = new DatabaseFile.Path<DatabaseFile>(L_OTHER_DOC_SCAN_COPY, this);
            return _otherDocScanCopy;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrant.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
