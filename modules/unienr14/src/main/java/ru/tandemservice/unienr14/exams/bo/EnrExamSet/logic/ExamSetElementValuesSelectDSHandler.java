/**
 *$Id: ExamSetElementValuesSelectDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.exams.entity.gen.IEnrExamSetElementValueGen;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author Alexander Shaburov
 * @since 18.04.13
 */
public class ExamSetElementValuesSelectDSHandler extends DefaultComboDataSourceHandler
{
    public ExamSetElementValuesSelectDSHandler(String ownerId)
    {
        super(ownerId, IEnrExamSetElementValue.class);
    }

    public static final String PROP_ENR_CAMP = "enrCamp";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(PROP_ENR_CAMP) == null ? EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign() : context.<EnrEnrollmentCampaign>get(PROP_ENR_CAMP);
        final Set primaryKeys = input.getPrimaryKeys();

        final DQLSelectBuilder discBuilder = new DQLSelectBuilder().fromEntity(EnrCampaignDiscipline.class, "d")
                .column(property(EnrCampaignDiscipline.id().fromAlias("d")))
                .column(property(EnrCampaignDiscipline.discipline().title().fromAlias("d")))
                .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("d")), value(enrCamp)));

        final DQLSelectBuilder gropuBuilder = new DQLSelectBuilder().fromEntity(EnrCampaignDisciplineGroup.class, "g")
                .column(property(EnrCampaignDisciplineGroup.id().fromAlias("g")))
                .column(property(EnrCampaignDisciplineGroup.title().fromAlias("g")))
                .where(eq(property(EnrCampaignDisciplineGroup.enrollmentCampaign().fromAlias("g")), value(enrCamp)));

        if (primaryKeys != null &&  !primaryKeys.isEmpty())
        {
            discBuilder.where(in(property(EnrCampaignDiscipline.id().fromAlias("d")), primaryKeys));
            gropuBuilder.where(in(property(EnrCampaignDisciplineGroup.id().fromAlias("g")), primaryKeys));
        }

        if (StringUtils.isNotEmpty(input.getComboFilterByValue()))
        {
            discBuilder.where(like(upper(property(EnrCampaignDiscipline.discipline().title().fromAlias("d"))), value(CoreStringUtils.escapeLike(input.getComboFilterByValue()))));
            gropuBuilder.where(like(upper(property(EnrCampaignDisciplineGroup.title().fromAlias("g"))), value(CoreStringUtils.escapeLike(input.getComboFilterByValue()))));
        }

        final List<Object[]> selectResultList = discBuilder.union(gropuBuilder.buildSelectRule()).createStatement(context.getSession()).list();
        List<Long> sortedIds = UniBaseDao.<Long>getSortedIds(selectResultList, new Comparator<String>()
        {
            @Override
            public int compare(String o1, String o2)
            {
                return o1.compareTo(o2);
            }
        });
        sortedIds = sortedIds.size() > 50 ? sortedIds.subList(0, 51) : sortedIds;

        List<IEnrExamSetElementValue> resultList = new DQLSelectBuilder().fromEntity(IEnrExamSetElementValue.class, "v").column(property("v"))
                .where(in(property(IEnrExamSetElementValueGen.id().fromAlias("v")), sortedIds))
                .createStatement(context.getSession()).list();

        resultList = UniBaseDao.sort(resultList, sortedIds);

        DSOutput output = ListOutputBuilder.get(input, resultList).build();
        if (selectResultList.size() > 50)
            output.setTotalSize(selectResultList.size());
        return output;
    }
}
