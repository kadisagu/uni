/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.EnrReportEntrantListAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantList;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/14/14
 */
public class EnrReportEntrantListDao extends UniBaseDao implements IEnrReportEntrantListDao
{

    private static final String T1 = "T1";
    private static final String T3 = "T3";
    private static final String T2 = "T2";
    private static final String T4 = "T4";

    @Override
    public Long createReport(EnrReportEntrantListAddUI model)
    {
        EnrReportEntrantList report = new EnrReportEntrantList();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }



    private byte[] buildReport(EnrReportEntrantListAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_LIST);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // Выбираем абитуриентов, подходящих под параметры отчета

        Map<EnrCompetition, List<EnrRequestedCompetition>> entrantMap = new HashMap<>();

        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
            .notArchiveOnly()
            .filter(model.getDateFrom(), model.getDateTo())
            .filter(model.getEnrollmentCampaign())
            ;

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.isSkipParallel()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        // Сразу сортируем
        if (model.isSortByRequestNumber()) {
            requestedCompDQL.order(property(EnrEntrantRequest.regNumber().fromAlias(requestedCompDQL.request())));
        } else {
            requestedCompDQL.order(property(EnrEntrant.person().identityCard().fullFio().fromAlias(requestedCompDQL.entrant())));
        }

        requestedCompDQL.column(requestedCompDQL.reqComp());

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        List<EnrRequestedCompetition> requestedCompetitions = requestedCompDQL.createStatement(getSession()).list();

        for (EnrRequestedCompetition requestedCompetition : requestedCompetitions) {
            SafeMap.safeGet(entrantMap, requestedCompetition.getCompetition(), ArrayList.class).add(requestedCompetition);
            nonEmptyBlocks.add(requestedCompetition.getCompetition().getProgramSetOrgUnit());
        }

        Map<Long, Set<EnrChosenEntranceExamForm>> chosenExams = Maps.newHashMap();

        BatchUtils.execute(requestedCompetitions, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrChosenEntranceExamForm.class, "ce")
                    .where(or(
                            eq(property("ce", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().type().code()), value(EnrRequestTypeCodes.BS)),
                            eq(property("ce", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().type().code()), value(EnrRequestTypeCodes.MASTER))
                    ))
                    .where(not(eq(property("ce", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().competition().type().code()), value(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL))))
                    .where(not(eq(property("ce", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().competition().type().code()), value(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT))))
                    .where(in(property("ce", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition()), elements));

            for(EnrChosenEntranceExamForm examForm : builder.createStatement(getSession()).<EnrChosenEntranceExamForm>list())
            {
                SafeMap.safeGet(chosenExams, examForm.getChosenEntranceExam().getRequestedCompetition().getId(), HashSet.class).add(examForm);
            }
        });

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов (не путать с массивом выбранных конкурсов, о котором уже было выше написано).

        Collection<EnrCompetition> competitions = filterAddon.getFilteredList();

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition()) {
            competitions = entrantMap.keySet();
        }

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList) {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // Сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());
        Collections.sort(blockList, (o1, o2) -> {
            int result;
            result = - Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
            if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
            if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
            return result;
        });


        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(blockList, EnrProgramSetOrgUnit::getProgramSet);

        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets)) {
            programMap.get(item.getProgramSet()).add(item.getProgram());
        }
        for (EnrProgramSetBase programSetBase : programSets) {
            if (programSetBase instanceof EnrProgramSetSecondary) {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary)programSetBase).getProgram());
            }
        }

        boolean printMinisterial = true;
        boolean printContract = true;
        CompensationType compensationType = (CompensationType) filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        if (null != compensationType && filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).isEnableCheckboxChecked()) {
            printMinisterial = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            printContract = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        }

        new RtfInjectModifier().put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);

        Map<String, RtfTable> headerTables = new HashMap<>();
        headerTables.put(EnrRequestTypeCodes.BS, UniRtfUtil.removeTable(document, "BS", true));
        headerTables.put(EnrRequestTypeCodes.MASTER, UniRtfUtil.removeTable(document, "MASTER", true));
        RtfTable higher = UniRtfUtil.removeTable(document, "HIGHER", true);
        headerTables.put(EnrRequestTypeCodes.HIGHER, higher);
        headerTables.put(EnrRequestTypeCodes.POSTGRADUATE, higher);
        RtfTable residency = UniRtfUtil.removeTable(document, "RESIDENCY", true);
        headerTables.put(EnrRequestTypeCodes.TRAINEESHIP, residency);
        headerTables.put(EnrRequestTypeCodes.INTERNSHIP, residency);
        headerTables.put(EnrRequestTypeCodes.SPO, UniRtfUtil.removeTable(document, "SPO", true));

        Map<String, RtfTable> contentTables = new HashMap<>();
        contentTables.put(T1, UniRtfUtil.removeTable(document, T1, false));
        contentTables.put(T2, UniRtfUtil.removeTable(document, T2, false));
        contentTables.put(T3, UniRtfUtil.removeTable(document, T3, false));
        contentTables.put(T4, UniRtfUtil.removeTable(document, T4, false));

        document.getElementList().clear();

        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList) {

            List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);

            String tableCode = programSetOrgUnit.getProgramSet().getRequestType().getCode();
            if (EnrRequestTypeCodes.HIGHER.equals(tableCode))
                switch (programSetOrgUnit.getProgramSet().getProgramKind().getCode())
                {
                    case EduProgramKindCodes.PROGRAMMA_INTERNATURY : tableCode = EnrRequestTypeCodes.INTERNSHIP; break;
                    case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_: tableCode = EnrRequestTypeCodes.POSTGRADUATE; break;
                    case EduProgramKindCodes.PROGRAMMA_ORDINATURY: tableCode = EnrRequestTypeCodes.TRAINEESHIP; break;
                }

            RtfTable headerTable = headerTables.get(tableCode).getClone();
            document.getElementList().add(headerTable);

            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
            boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);
            boolean skipProgramsRow = (model.isSkipProgramSetTitle() || model.isReplaceProgramSetTitle()) && programs.size() == 1;
            boolean skipProgramSetRow = model.isSkipProgramSetTitle();
            if (skipProgramsRow && headerTable.getRowList().size() >= 6) {
                headerTable.getRowList().remove(5);
            }
            if (skipProgramSetRow && headerTable.getRowList().size() >= 4) {
                headerTable.getRowList().remove(3);
            }

            List<EnrCompetition> enrCompetitions = blockMap.get(programSetOrgUnit);

            boolean hasNoExams = checkNoExams(enrCompetitions);
            boolean hasBSExams = checkBSExams(enrCompetitions);


            fillHeaderData(document, programSetOrgUnit, programs, printMinisterial, printContract, printOP, hasNoExams, hasBSExams);

            MutableInt ministerialRequestTotal = new MutableInt(0); MutableInt contractRequestTotal = new MutableInt(0);
            for (EnrCompetition competition : enrCompetitions) {

                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0) {
                    for (EnrTargetAdmissionPlan taPlan : taPlans) {
                        List<EnrRequestedCompetition> entrantList = new ArrayList<>();
                        for (EnrRequestedCompetition entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class)) {
                            if (entrant instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA)entrant).getTargetAdmissionKind())) {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty()) {
                            continue;
                        }
                        final String tableHeader = getTableHeader(competition, taPlan, entrantList.size());
                        printContentTable(competition, entrantList, chosenExams, document, tableHeader, ministerialRequestTotal, contractRequestTotal, contentTables, model.isPrintPriority());
                    }
                } else {
                    List<EnrRequestedCompetition> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    final String tableHeader = getTableHeader(competition, null, entrantList.size());
                    printContentTable(competition, entrantList, chosenExams, document, tableHeader, ministerialRequestTotal, contractRequestTotal, contentTables, model.isPrintPriority());
                }
            }

            //numberOfRequest - число заявлений: сумма на бюджетные конкурсы (сумма по всем табличкам бюджетных конкурсов), сумма по конкурсам по договору; если отчет строится только по бюджету - строки по договору не будет, если отчет по договору - строки по бюджету не будет; если КЦП 0 (план на бюджет не задан) - строки по бюджету не будет, если по договору план 0 (не задан) - строки по договору не будет
            //Подано заявлений:
            //    на бюджет (КЦП) – 101
            //    на места с оплатой стоимости обучения – 49

            RtfString requestTotal = new RtfString();
            if (printMinisterial) {
                requestTotal.append("    на бюджет (КЦП) \u2013 ").append(String.valueOf(ministerialRequestTotal.intValue()));
            }
            if (printContract) {
                if (requestTotal.toList().size() != 0) requestTotal.par();
                requestTotal.append("    на места с оплатой стоимости обучения \u2013 ").append(String.valueOf(contractRequestTotal.intValue()));
            }
            new RtfInjectModifier().put("requestsTotal", requestTotal).modify(document);

            if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1) {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
        }

        return RtfUtil.toByteArray(document);
    }

    private boolean checkNoExams(List<EnrCompetition> enrCompetitions)
    {
        return CollectionUtils.exists(enrCompetitions, enrCompetition
                -> EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(enrCompetition.getCompTypeCode())
                || EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(enrCompetition.getCompTypeCode()));
    }

    private boolean checkBSExams(List<EnrCompetition> enrCompetitions)
    {
        return CollectionUtils.exists(enrCompetitions, enrCompetition
                -> (EnrRequestTypeCodes.BS.equals(enrCompetition.getRequestType().getCode())
                    || EnrRequestTypeCodes.MASTER.equals(enrCompetition.getRequestType().getCode()))
                && !EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(enrCompetition.getCompTypeCode())
                && !EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(enrCompetition.getCompTypeCode()));
    }

    private List<EnrTargetAdmissionPlan> getTaPlans(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        return programSetOrgUnit.getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition() ?
                    getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit, EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s())
                    : null;
    }

    private String getTableHeader(EnrCompetition competition, EnrTargetAdmissionPlan taPlan, int size)
    {
        StringBuilder header = new StringBuilder();
        header.append(competition.getType().getPrintTitle());
        if (null != taPlan) {
            header.append(" – ").append(taPlan.getEnrCampaignTAKind().getTitle());
        }
        header.append(" (подано заявлений – ").append(size).append(")");
        return header.toString();
    }

    private void printContentTable(EnrCompetition competition, List<EnrRequestedCompetition> entrantList, Map<Long, Set<EnrChosenEntranceExamForm>> chosenExams,
                       RtfDocument document, final String tableHeader, MutableInt ministerialRequestTotal, MutableInt contractRequestTotal,
                       Map<String, RtfTable> contentTables, boolean printPriority)
    {
        String tableName = chooseRtfTable(competition);
        RtfTable contentTable = contentTables.get(tableName).getClone();

        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(contentTable);

        final List<String[]> tableContent = new ArrayList<>();
        int number = 1;
        if (entrantList != null) {
            for (EnrRequestedCompetition entrant : entrantList) {
                tableContent.add(printEntrant(number++, entrant, chosenExams.get(entrant.getId()), tableName, printPriority));
                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    ministerialRequestTotal.increment();
                }
                if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    contractRequestTotal.increment();
                }
            }
        }

        new RtfTableModifier()
            .put(tableName, tableContent.toArray(new String[tableContent.size()][]))
            .put(tableName, new RtfRowIntercepterBase() {
                // todo сделать утильный класс
                @Override public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                    List<IRtfElement> list = new ArrayList<>();
                    IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                    text.setRaw(true);
                    list.add(text);
                    return list;
                }
                @Override public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                    newRowList.get(startIndex - 3).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());
                }

                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    if (!printPriority)
                    {
                        int column = 0;
                        switch (tableName)
                        {
                            case T1:
                            case T3:
                                column=3;
                                break;
                            case T2:
                            case T4:
                                column=2;
                                break;
                        }
                        RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), column); //ячейка в строке с меткой T
                        RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), column); //ячейка в шапке
                    }
                }
            })
            .modify(document);
    }

    private String[] printEntrant(int number, EnrRequestedCompetition entrantReqComp, Set<EnrChosenEntranceExamForm> enrChosenEntranceExamForms, String tableName, boolean printPriority)
    {
        List<String> row = new ArrayList<>();
        row.add(String.valueOf(number));
        row.add(entrantReqComp.getRequest().getEntrant().getFullFio());
        if (T1.equals(tableName) || T3.equals(tableName)) {
            if (entrantReqComp instanceof IEnrEntrantBenefitStatement) {
                row.add(((IEnrEntrantBenefitStatement)entrantReqComp).getBenefitCategory().getShortTitle());
            } else row.add("");
        }
        if (printPriority)
            row.add(String.valueOf(entrantReqComp.getPriority()));
        row.add(YesNoFormatter.INSTANCE.format(entrantReqComp.isOriginalDocumentHandedIn()));
        row.add(YesNoFormatter.INSTANCE.format(entrantReqComp.isAccepted()));
        row.add(entrantReqComp.getRequest().getStringNumber());
        row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantReqComp.getRequest().getRegDate()));
        StringBuilder info = new StringBuilder();
        if (entrantReqComp instanceof EnrRequestedCompetitionNoExams) {
            info.append("Выбранная ОП: ").append(((EnrRequestedCompetitionNoExams)entrantReqComp).getProgramSetItem().getProgram().getShortTitle());
        }
        if (entrantReqComp.getRequest().isTakeAwayDocument()) {
            if (info.length() > 0) info.append("\\par ");
            info.append("Забрал документы (").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantReqComp.getRequest().getTakeAwayDocumentDate())).append(")");
        }
        if (entrantReqComp instanceof EnrRequestedCompetitionExclusive) {
            if (info.length() > 0) info.append("\\par ");
            info.append("Особое право: ").append(((EnrRequestedCompetitionExclusive)entrantReqComp).getBenefitCategory().getShortTitle());
        }
        if (entrantReqComp.getRequest().getBenefitCategory() != null) {
            if (info.length() > 0) info.append("\\par ");
            info.append("Преимущественное право: ").append(entrantReqComp.getRequest().getBenefitCategory().getShortTitle());
        }
        if((EnrRequestTypeCodes.BS.equals(entrantReqComp.getRequest().getType().getCode())
                || EnrRequestTypeCodes.MASTER.equals(entrantReqComp.getRequest().getType().getCode()))
                && !EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(entrantReqComp.getCompetition().getCompTypeCode())
                && !EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(entrantReqComp.getCompetition().getCompTypeCode())
                && enrChosenEntranceExamForms != null
                && !enrChosenEntranceExamForms.isEmpty())
        {
            boolean existState = CollectionUtils.exists(enrChosenEntranceExamForms, enrChosenEntranceExamForm -> !enrChosenEntranceExamForm.getPassForm().isInternal());
            boolean existInternal = CollectionUtils.exists(enrChosenEntranceExamForms, enrChosenEntranceExamForm -> enrChosenEntranceExamForm.getPassForm().isInternal());;
            if (info.length() > 0) info.append("\\par ");
            info.append("поступает на основании результатов: ");
            if(existState && existInternal)
            {
                info.append("ЕГЭ и ВИ ОО");
            }
            else if(existInternal)
            {
                info.append("ВИ ОО");
            }
            else
            {
                info.append("ЕГЭ");
            }
        }

        row.add(info.toString());
        addCustomColumn(row, entrantReqComp, tableName);
            
        return row.toArray(new String[row.size()]);
    }

    private void fillHeaderData(RtfDocument document, EnrProgramSetOrgUnit programSetOrgUnit, List<EduProgramProf> programs, boolean printMinisterial, boolean printContract, boolean printOP, boolean hasNoExams, boolean hasBSExams)
    {
        Collections.sort(programs, (o1, o2) -> o1.getTitleAndConditionsShort().compareTo(o2.getTitleAndConditionsShort()));

        String duration = null; boolean diffDuration = false;
        for (EduProgram program : programs) {
            if (null == duration) {
                duration = program.getDuration().getTitle();
            } else {
                diffDuration = diffDuration || !program.getDuration().getTitle().equals(duration);
            }
        }
        String programConditions = null; boolean diffConditions = false;
        for (EduProgramProf program : programs) {
            if (null == programConditions) {
                programConditions = StringUtils.trimToEmpty(program.getImplConditionsShort());
            } else {
                diffConditions = diffConditions || !StringUtils.trimToEmpty(program.getImplConditionsShort()).equals(programConditions);
            }
        }

        OrgUnit filial = programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
            .put("filial", filial.getPrintTitle())
            .put("formOrgUnit", programSetOrgUnit.getFormativeOrgUnit() == null || programSetOrgUnit.getFormativeOrgUnit().equals(filial) ? "" : programSetOrgUnit.getFormativeOrgUnit().getPrintTitle())
            .put("eduSubjectKind", programSetOrgUnit.getProgramSet().getProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_NOMINATIVE))
            .put("eduSubject", programSetOrgUnit.getProgramSet().getProgramSubject().getTitleWithCode())
            .put("programForm", programSetOrgUnit.getProgramSet().getProgramForm().getTitle())
            .put("duration", diffDuration ? "" : duration)
            .put("programTrait", diffConditions? "" : programConditions)
            .put("eduProgramKind", programSetOrgUnit.getProgramSet().getProgramKind().getShortTitle())
            .put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
            .put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getPrintTitle())
            ;

        RtfString eduProgramTitles = new RtfString();
        for (EduProgramProf program : programs) {
            if (eduProgramTitles.toList().size() > 0) eduProgramTitles.par();
            eduProgramTitles.append(program.getShortTitle() + " \u2013 " + program.getTitleAndConditionsShort());
        }
        modifier.put("eduProgram", eduProgramTitles);

        //numberOfPlaces - число мест для приема (для комбинации набор ОП, филиал); не печатаем те строчки, в которых план 0 (не задан); если план с квотой ЦП бьется на подквоты (более чем на одну), то выводить детализацию по видам ЦП; например, если есть КЦП и нет квоты особых прав и ЦП, то будет строка "Число мест на бюджет (КЦП) - 50"; если отчет строится только по бюджету - строки с планом по договору не будет, если отчет по договору - строк по бюджету не будет (включая расшифровку по квотам и видам ЦП)
        //Число мест на бюджет (КЦП) – 50, из них:
        //       квота лиц с особыми правами – 5
        //       квота целевого приема – 10
        //(из них по видам целевого приема: Южный фед. округ – 2, ФГУП Спецмнотаж – 3, МО Курганской обл. – 1, Тюменский район – 3)
        //Число мест с оплатой стоимости обучения – 100

        RtfString plan = new RtfString();
        if (printMinisterial) {
            plan.append("Число мест на бюджет (КЦП) – ").append(String.valueOf(programSetOrgUnit.getMinisterialPlan()));
            boolean newLineNeeded = true;
            if (EnrRequestTypeCodes.BS.equals(programSetOrgUnit.getProgramSet().getRequestType().getCode())) {
                plan.append(", из них:").par().append("       квота лиц с особыми правами \u2013 ").append(String.valueOf(programSetOrgUnit.getExclusivePlan())).par();
                newLineNeeded = false;
            }
            if (programSetOrgUnit.getTargetAdmPlan() > 0) {
                if (newLineNeeded) plan.append(", из них:").par();
                plan.append("       квота целевого приема \u2013 ").append(String.valueOf(programSetOrgUnit.getTargetAdmPlan()));
                List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);
                if (taPlans != null && taPlans.size() > 0) {
                    plan.par().append("(из них по видам целевого приема: ");
                    plan.append(UniStringUtils.join(taPlans, EnrTargetAdmissionPlan.planString().s(), ", "));
                }
            }
        }
        if (printContract) {
            plan.par().append("Число мест с оплатой стоимости обучения \u2013 ").append(String.valueOf(programSetOrgUnit.getContractPlan()));
        }
        modifier.put("plan", plan);

        if(hasNoExams)
        {
            modifier.put("withoutEntrExam", new RtfString().par().boldBegin().append("ВИ").boldEnd().append(" \u2013 вступительные испытания"));
        }
        else
        {
            modifier.put("withoutEntrExam", "");
        }

        if(hasBSExams)
        {
            modifier.put("resultBase", new RtfString().par().boldBegin().append("ЕГЭ").boldEnd().append(" \u2013 единый государственный экзамен, ").boldBegin().append("ВИ ОО").boldEnd().append(" \u2013 вступительное испытание, проводимое организацией самостоятельно"));
        }
        else
        {
            modifier.put("resultBase", "");
        }

        modifier.modify(document);
    }


    private String chooseRtfTable(EnrCompetition competition) {
        String requestTypeCode = competition.getRequestType().getCode();        
        String compTypeCode = competition.getType().getCode();
        // Бакалавриат, специалитет:
        if (EnrRequestTypeCodes.BS.equals(requestTypeCode)) {
            // Без ВИ в рамках КЦП - "T1"
            if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compTypeCode)) return T1;
            // В рамках квоты лиц, имеющих особые права - "T1"
            if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(compTypeCode)) return T1;
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // Без ВИ по договору - T3
            if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compTypeCode)) return T3;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // Магистратура:
        if (EnrRequestTypeCodes.MASTER.equals(requestTypeCode)) {
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // Аспирантура:
        if (EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) || EnrRequestTypeCodes.POSTGRADUATE.equals(requestTypeCode) || EnrRequestTypeCodes.TRAINEESHIP.equals(requestTypeCode) || EnrRequestTypeCodes.INTERNSHIP.equals(requestTypeCode)) {
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode) || EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // СПО:
        if (EnrRequestTypeCodes.SPO.equals(requestTypeCode)) {
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // По договору - T2
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T2;
        }
        throw new IllegalArgumentException();
    }

    protected  void addCustomColumn(List<String> row, EnrRequestedCompetition entrant, String tableName)
    {
        //Добавление дополнительных колонок в проекте
    }
}