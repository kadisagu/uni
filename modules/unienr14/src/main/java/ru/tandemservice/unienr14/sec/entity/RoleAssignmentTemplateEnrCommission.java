package ru.tandemservice.unienr14.sec.entity;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.sec.entity.gen.*;

/** @see ru.tandemservice.unienr14.sec.entity.gen.RoleAssignmentTemplateEnrCommissionGen */
public class RoleAssignmentTemplateEnrCommission extends RoleAssignmentTemplateEnrCommissionGen
{
    public RoleAssignmentTemplateEnrCommission()
    {
    }

    public RoleAssignmentTemplateEnrCommission(IPrincipalContext principalContext, RoleConfigTemplate role, EnrEnrollmentCommission enrollmentCommission)
    {
        setPrincipalContext(principalContext);
        setRoleConfig(role);
        setEnrollmentCommission(enrollmentCommission);
    }

    @Override
    public ILocalRoleContext getLocalRoleContext()
    {
        return getEnrollmentCommission();
    }
}