/**
 *$Id: EnrExamGroupManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.*;

/**
 * @author Alexander Shaburov
 * @since 17.05.13
 */
@Configuration
public class EnrExamGroupManager extends BusinessObjectManager
{
    public static EnrExamGroupManager instance()
    {
        return instance(EnrExamGroupManager.class);
    }

    @Bean
    public IEnrExamGroupDao groupDao()
    {
        return new EnrExamGroupDao();
    }

    @Bean
    public IEnrExamGroupDistributionDao distributionDao()
    {
        return new EnrExamGroupDistributionDao();
    }

    @Bean
    public IEnrExamGroupSetDao setDao()
    {
        return new EnrExamGroupSetDao();
    }

    @Bean
    public IEnrExamGroupEnrollPassSheetPrintDao enrPassSheetPrintDao()
    {
        return new EnrExamGroupEnrollPassSheetPrintDao();
    }
}
