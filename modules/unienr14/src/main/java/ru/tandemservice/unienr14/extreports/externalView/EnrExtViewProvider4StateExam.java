/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unienr14.catalog.entity.EnrSecondWaveStateExamPlace;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/27/13
 */
public class EnrExtViewProvider4StateExam extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrEntrantStateExamResult.class, "result")
            .joinPath(DQLJoinType.left, EnrEntrantStateExamResult.secondWaveExamPlace().fromAlias("result"), "s_wave_place");
        
        column(dql, property("result", EnrEntrantStateExamResult.id()), "resultId").comment("uid результата ЕГЭ");
        column(dql, property("result", EnrEntrantStateExamResult.entrant().id()), "entrantId").comment("uid абитуриента");
        column(dql, property("result", EnrEntrantStateExamResult.year()), "year").comment("год сдачи");

        column(dql, property("result", EnrEntrantStateExamResult.subject().title()), "subject").comment("предмет ЕГЭ");
        column(dql, property("result", EnrEntrantStateExamResult.subject().shortTitle()), "subjectShortTitle").comment("сокр. название предмета ЕГЭ");
        column(dql, property("result", EnrEntrantStateExamResult.subject().code()), "subjectCode").comment("код предмета ЕГЭ");

        booleanIntColumn(dql, EnrEntrantStateExamResult.secondWave().fromAlias("result"), "secondWave").comment("направлен на ЕГЭ в доп. сроки (1 - да, 0 - нет)");
        column(dql, property("s_wave_place",  EnrSecondWaveStateExamPlace.title()), "secondWaveExamPlace").comment("ППЭ для сдачи в доп. сроки");
        
        column(dql, EnrExtViewUtil.asLongColumn(EnrEntrantStateExamResult.markAsLong().fromAlias("result")), "mark").comment("балл");

        column(dql, property("result", EnrEntrantStateExamResult.documentNumber()), "certNumber").comment("номер свидетельства");
        column(dql, property("result", EnrEntrantStateExamResult.documentIssuanceDate()), "certIssuanceDate").comment("дата выдачи свидетельства");


        booleanIntColumn(dql, EnrEntrantStateExamResult.accepted().fromAlias("result"), "accepted").comment("зачтен");

        booleanIntColumn(dql, EnrEntrantStateExamResult.verifiedByUser().fromAlias("result"), "verifiedByUser").comment("проверен вручную");
        column(dql, property("result", EnrEntrantStateExamResult.verifiedByUserComment()), "verifiedByUserComment").comment("комментарий к проверке результата вручную");

        column(dql, new DQLCaseExpressionBuilder()
            .when(isNotNull(property("result", EnrEntrantStateExamResult.stateCheckStatus())), value(1))
            .otherwise(value(0))
            .build(),
            "verifiedByFis"
        ).comment("проверен в ФИС (не обязательно при этом подтвержден ФИС, смотрите на статус проверки в ФИС)");
        column(dql, property("result", EnrEntrantStateExamResult.stateCheckStatus()), "verifiedByFisStatus").comment("статус проверки в ФИС");

        column(dql, property("result", EnrEntrantStateExamResult.registrationDate()), "registrationDate").comment("дата создания результата в системе");

        return dql;
    }
}