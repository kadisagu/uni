/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrRequestType.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

/**
 * @author nvankov
 * @since 16.06.2016
 */
public interface IEnrRequestTypeDao extends INeedPersistenceSupport
{
    /**
     * Выбранный пользователем вид заявления в рамках ПК в фильтрах и селекторах в модуле хранится в настройках пользователя, как общий для всех страниц модуля.
     * Этот метод позволяет получить такой вид заявления для текущего пользователя.
     * <p/>
     * Если вы используете такой вид заявления в списке в фильтре, то при применении фильтров сохраняйте новый выбор пользователя.
     * Если вы используете такой вид заявления в селекторе, сохраняйте при обновлении селектора.
     * <p/>
     * Для сохранения используйте метод
     *
     * @return выбранный пользователем вид заявления (может быть null).
     * @see IEnrRequestTypeDao#saveDefaultRequestType(ru.tandemservice.unienr14.catalog.entity.EnrRequestType, ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign)
     * <p/>
     * (DEV-9539)
     * *
     */
    public EnrRequestType getDefaultRequestType(EnrEnrollmentCampaign enrollmentCampaign);


    List<EnrRequestType> getRequestTypeList(EnrEnrollmentCampaign enrollmentCampaign);

    /**
     * Выбранный пользователем вид заявления в рамках ПК в фильтрах и селекторах в модуле хранится в настройках пользователя, как общая для всех страниц модуля.
     * Этот метод позволяет сохранить новый выбор пользователя в настройки.
     * <p/>
     * Если вы используете такой вид заявления в списке в фильтре, то при применении фильтров сохраняйте новый выбор пользователя.
     * Если вы используете такой вид заявления в селекторе, сохраняйте при обновлении селектора.
     * <p/>
     * Чтобы получить сохраненное значение, используйте
     *
     * @param requestType выбранная пользователем ПК
     * @param campaign ПК в рамках которой будет сохранен вид заявления
     * @see ru.tandemservice.unienr14.catalog.bo.EnrRequestType.logic.IEnrRequestTypeDao#getDefaultRequestType(ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign)
     * <p/>
     * (DEV-9539)
     */
    public void saveDefaultRequestType(EnrRequestType requestType, EnrEnrollmentCampaign campaign);
}
