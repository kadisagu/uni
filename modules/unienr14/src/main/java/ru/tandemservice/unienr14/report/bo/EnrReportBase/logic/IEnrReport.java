/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.logic;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.IStorableReport;

import java.util.Date;

/**
 * @author oleyba
 * @since 5/14/14
 */
public interface IEnrReport extends IStorableReport, IEntity
{
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_PERIOD_TITLE = "periodTitle";

    // todo сделать из IEnrReport персистентный интерфейс
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign"; // должно быть персистентным

    IEnrStorableReportDesc getDesc();

    Date getFormingDate();
    String getPeriodTitle();
}
