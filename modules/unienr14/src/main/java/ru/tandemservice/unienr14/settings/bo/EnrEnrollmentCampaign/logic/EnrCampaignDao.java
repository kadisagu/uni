/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.organization.sec.bo.Sec.util.RoleAssignmentCacheSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.base.bo.EducationYear.logic.IEducationYearDao;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentCampaignTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrProgramSetOrgUnitWrapper;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit.ExamSetElementWrapper;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCampaign;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.EnrEntrantDocumentManager;
import ru.tandemservice.unienr14.settings.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/9/13
 */
public class EnrCampaignDao extends UniBaseDao implements IEnrCampaignDao
{

    public static final String SETTINGS_KEY_PREFIX_DEFAULT_ENR_CAMPAIGN = "enrDefaultEnrCampaign.";
    public static final String SETTINGS_KEY_DEFAULT_ENR_CAMPAIGN = "campaign";

    @Override
    public EnrEnrollmentCampaign getDefaultCampaign()
    {
        String settingsOwner = DataSettingsFacade.getSettingsOwner();
        if (null == settingsOwner)
            return null;

        IDataSettings settings = DataSettingsFacade.getSettings(settingsOwner, SETTINGS_KEY_PREFIX_DEFAULT_ENR_CAMPAIGN);
        Object campaign = settings.get(SETTINGS_KEY_DEFAULT_ENR_CAMPAIGN);

        List<EnrEnrollmentCampaign> enrollmentCampaignList = EnrEnrollmentCampaignManager.instance().enrCampaignPermissionsDao().getEnrollmentCampaignList(UserContext.getInstance().getPrincipalContext());

        if(enrollmentCampaignList.isEmpty()) return null;

        if(campaign == null)
        {
            EnrEnrollmentCampaign enrollmentCampaign = enrollmentCampaignList.get(0);
            saveDefaultCampaign(enrollmentCampaign);
            return enrollmentCampaign;
        }

        if(campaign instanceof EnrEnrollmentCampaign)
        {
            EnrEnrollmentCampaign enrollmentCampaign = (EnrEnrollmentCampaign) campaign;
            if(!enrollmentCampaignList.contains(enrollmentCampaign))
            {
                enrollmentCampaign = enrollmentCampaignList.get(0);
                saveDefaultCampaign(enrollmentCampaign);
            }

            return enrollmentCampaign;

        }

        return null;
    }

    @Override
    public void saveDefaultCampaign(EnrEnrollmentCampaign campaign)
    {
        String settingsOwner = DataSettingsFacade.getSettingsOwner();
        if (null == settingsOwner)
            return;

        IDataSettings settings = DataSettingsFacade.getSettings(settingsOwner, SETTINGS_KEY_PREFIX_DEFAULT_ENR_CAMPAIGN);
        settings.set(SETTINGS_KEY_DEFAULT_ENR_CAMPAIGN, campaign);
        DataSettingsFacade.saveSettings(settings);
    }

    @Override
    public Long saveOrUpdateCampaign(final EnrEnrollmentCampaign entity, final List<EduInstitutionOrgUnit> enrOrgUnitList)
    {
        if (validateCampaign(entity).hasErrors())
            return null;

        final Session session = getSession();

        EnrEnrollmentCampaignSettings settings = entity.getSettings();

        if (settings == null)
        {
            settings = new EnrEnrollmentCampaignSettings();
            entity.setSettings(settings);
        }

//        /*
//            Если при первом сохранении была ошибка, то у объекта EnrEnrollmentCampaignSettings id останется проставленным,
//            но в базе его не будет - упадет ошибка при повторном сохранении, хибернат будет думать, что объект есть в базе.
//            Поэтому если настройки есть, то проверяем есть ли у них id, если есть то пытаемся достать их из базы
//            - если в базе нет, то id надо занулить, что бы хибернат не думал, что объект уже сохранен.
//         */
//        else if (entity.getSettings().getId() != null)
//        {
//            if (getSession().get(EnrEnrollmentCampaignSettings.class, entity.getSettings().getId()) == null)
//                entity.getSettings().setId(null);
//        }

        session.saveOrUpdate(entity.getSettings());
        session.saveOrUpdate(entity);

        new MergeAction.SessionMergeAction<EduInstitutionOrgUnit, EnrOrgUnit>() {
            @Override protected EduInstitutionOrgUnit key(final EnrOrgUnit source) {
                return source.getInstitutionOrgUnit();
            }
            @Override protected void fill(final EnrOrgUnit target, final EnrOrgUnit source) {

            }
            @Override protected EnrOrgUnit buildRow(final EnrOrgUnit source) {
                return new EnrOrgUnit(entity, source.getInstitutionOrgUnit());
            }
        }.merge(
            getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), entity),
            CollectionUtils.collect(enrOrgUnitList, eduInstitutionOrgUnit -> { return new EnrOrgUnit(entity, eduInstitutionOrgUnit); }
            ));

        EnrEntrantDocumentManager.instance().typeDao().doSetEnrCampDefaultSettings(entity.getId());
        // todo сохраняем настройки для Типов приказов по умолчанию
        // EnrEnrollmentOrderManager.instance().orderTypeDao().doSetEnrCampDefaultSettings(entity.getId());

        if (!existsEntity(EnrEntrantAchievementSettings.class, EnrEntrantAchievementSettings.L_ENROLLMENT_CAMPAIGN, entity.getId()))
        {
            // Создаем умолчанию для индивидуальных достижений (для каждого вида зявления), если таких еще не создано
            for (EnrRequestType requestType : getList(EnrRequestType.class))
            {
                EnrEntrantAchievementSettings achievementSettings = new EnrEntrantAchievementSettings(requestType, entity);
                // initialize values
                achievementSettings.setMaxTotalAchievementMarkAsLong(0);
                session.save(achievementSettings);
            }
        }

        return entity.getId();
    }

    @Override
    public ErrorCollector validateCampaign(EnrEnrollmentCampaign entity)
    {
        final ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (entity.getDateFrom().getTime() >= entity.getDateTo().getTime())
            errorCollector.add(EnrEnrollmentCampaignManager.instance().getProperty("enrollmentCampaign.validate.error.date"), "dateFrom", "dateTo");

        return errorCollector;
    }

    @Override
    public void deleteCampaign(EnrEnrollmentCampaign entity)
    {
        delete(entity);
    }

    @Override
    public List<EnrEnrollmentCampaign> getEnrollmentCampaignList()
    {
        List<EnrEnrollmentCampaign> campaigns = getList(EnrEnrollmentCampaign.class);
        Collections.sort(campaigns, EnrEnrollmentCampaignManager.ENR_CAMPAIGN_COMPARATOR);
        return campaigns;
    }

    @Override
    public Long updateEnrCampSettings(EnrEnrollmentCampaign entity, IEnrCampSettings settings)
    {
        update(entity.getSettings());
        return entity.getSettings().getId();
    }

    @Override
    public EnrEnrollmentCampaign copyEnrollmentCampaign(EnrEnrollmentCampaign srcCampaign, EnrEnrollmentCampaign newCampaignContainer)
    {
        Preconditions.checkNotNull(srcCampaign);
        Preconditions.checkNotNull(newCampaignContainer.getTitle());
        Preconditions.checkNotNull(newCampaignContainer.getEducationYear());
        Preconditions.checkNotNull(newCampaignContainer.getDateFrom());
        Preconditions.checkNotNull(newCampaignContainer.getDateTo());

        final Session session = getSession();

        /* Скопировать ПК на следующий учебный год, ее настройки и филиалы в рамках ПК */

        // новые настройки
        final EnrEnrollmentCampaignSettings newSettings = new EnrEnrollmentCampaignSettings();
        newSettings.update(srcCampaign.getSettings(), false);
        session.save(newSettings);

        // новая ПК
        final EnrEnrollmentCampaign newCampaign = new EnrEnrollmentCampaign();
        newCampaign.update(srcCampaign, false);          // Копируем [неизвестные на данный момент] поля исходной ПК
        newCampaign.update(newCampaignContainer, false); // Копируем поля, введенные на форме копирования ПК
        newCampaign.setSettings(newSettings);            // Заполняем поле с настройками ПК ссылкой на копию настроек
        if (saveOrUpdateCampaign(newCampaign, ImmutableList.<EduInstitutionOrgUnit>of()) == null) {
            session.delete(newSettings);
            return null;
        }

        // Филиалы новой ПК
        final Map<EnrOrgUnit, EnrOrgUnit> src2destEnrOrgUnitMap = new HashMap<>();
        for (EnrOrgUnit srcEnrOrgUnit : getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), srcCampaign))
        {
            final EnrOrgUnit newEnrOrgUnit = new EnrOrgUnit(newCampaign, srcEnrOrgUnit.getInstitutionOrgUnit());
            newEnrOrgUnit.update(srcEnrOrgUnit, false);
            session.save(newEnrOrgUnit);

            if (src2destEnrOrgUnitMap.put(srcEnrOrgUnit, newEnrOrgUnit) != null)
                throw new IllegalStateException();
        }

        /* В скопированную ПК скопировать все: */

        // Дисциплины ВИ в ПК
        final Map<EnrDiscipline, EnrCampaignDiscipline> newDisciplinesMap = new HashMap<>();
        final Map<IEnrExamSetElementValue, IEnrExamSetElementValue> src2destExamSetValuesMap = new HashMap<>();
        for (EnrCampaignDiscipline srcDiscipline : getList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.enrollmentCampaign(), srcCampaign))
        {
            final EnrCampaignDiscipline newDiscipline = new EnrCampaignDiscipline(newCampaign, srcDiscipline.getDiscipline());
            newDiscipline.update(srcDiscipline, false);
            session.save(newDiscipline);

            if (src2destExamSetValuesMap.put(srcDiscipline, newDiscipline) != null)
                throw new IllegalStateException();

            if (newDisciplinesMap.put(srcDiscipline.getDiscipline(), newDiscipline) != null)
                throw new IllegalStateException("Duplicate discipline in enrollment campaign");
        }

        // Группы дисциплин ВИ в ПК
        for (EnrCampaignDisciplineGroup srcDisciplineGroup : getList(EnrCampaignDisciplineGroup.class, EnrCampaignDisciplineGroup.enrollmentCampaign(), srcCampaign))
        {
            final EnrCampaignDisciplineGroup newDisciplineGroup = new EnrCampaignDisciplineGroup();
            newDisciplineGroup.update(srcDisciplineGroup, false);
            newDisciplineGroup.setEnrollmentCampaign(newCampaign);
            session.save(newDisciplineGroup);

            if (src2destExamSetValuesMap.put(srcDisciplineGroup, newDisciplineGroup) != null)
                throw new IllegalStateException();

            // Дисциплины в группе ВИ
            for (EnrCampaignDisciplineGroupElement srcDisciplineGroupElement : getList(EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElement.group(), srcDisciplineGroup))
            {
                final EnrCampaignDiscipline newEnrCampaignDiscipline = newDisciplinesMap.get(srcDisciplineGroupElement.getDiscipline().getDiscipline());
                final EnrCampaignDisciplineGroupElement newDisciplineGroupElement = new EnrCampaignDisciplineGroupElement(newEnrCampaignDiscipline, newDisciplineGroup);
                newDisciplineGroupElement.update(srcDisciplineGroupElement, false);
                session.save(newDisciplineGroupElement);
            }
        }

        // Виды ЦП в рамках ПК
        final Map<EnrCampaignTargetAdmissionKind, EnrCampaignTargetAdmissionKind> src2destTAKindMap = new HashMap<>();
        for (EnrCampaignTargetAdmissionKind srcEnrCampaignTaKind : getList(EnrCampaignTargetAdmissionKind.class, EnrCampaignTargetAdmissionKind.enrollmentCampaign(), srcCampaign))
        {
            final EnrCampaignTargetAdmissionKind newEnrCampaignTaKind = new EnrCampaignTargetAdmissionKind(newCampaign, srcEnrCampaignTaKind.getTargetAdmissionKind());
            newEnrCampaignTaKind.update(srcEnrCampaignTaKind, false);
            session.save(newEnrCampaignTaKind);

            if (src2destTAKindMap.put(srcEnrCampaignTaKind, newEnrCampaignTaKind) != null)
                throw new IllegalStateException();

            // Организации ЦП
            for (EnrTargetAdmissionOrgUnit srcEnrTaOrgUnit : getList(EnrTargetAdmissionOrgUnit.class, EnrTargetAdmissionOrgUnit.enrCampaignTAKind(), srcEnrCampaignTaKind))
            {
                final EnrTargetAdmissionOrgUnit newEnrTaOrgUnit = new EnrTargetAdmissionOrgUnit(newEnrCampaignTaKind, srcEnrTaOrgUnit.getExternalOrgUnit());
                newEnrTaOrgUnit.update(srcEnrTaOrgUnit, false);
                session.save(newEnrTaOrgUnit);
            }
        }

        // Наборы ВИ
        final IEnrExamSetDao examSetDao = EnrExamSetManager.instance().dao();
        for (EnrExamSet srcExamSet : getList(EnrExamSet.class, EnrExamSet.enrollmentCampaign(), srcCampaign))
        {
            final EnrExamSet newExamSet = new EnrExamSet();
            newExamSet.update(srcExamSet, false);
            newExamSet.setEnrollmentCampaign(newCampaign);
            session.save(newExamSet);

            final List<ExamSetElementWrapper> examSetElementWrappers = new ArrayList<>();
            for (EnrExamSetElement srcExamSetElement : getList(EnrExamSetElement.class, EnrExamSetElement.examSet(), srcExamSet, EnrExamSetElement.P_NUMBER))
            {
                final IEnrExamSetElementValue newExamSetElementValue = src2destExamSetValuesMap.get(srcExamSetElement.getValue());
                final ExamSetElementWrapper newExamSetElementWrapper = new ExamSetElementWrapper();
                newExamSetElementWrapper.setElementValue(newExamSetElementValue);
                newExamSetElementWrapper.setExamType(srcExamSetElement.getType());
                examSetElementWrappers.add(newExamSetElementWrapper);
            }
            examSetDao.saveOrUpdateExamSet(newExamSet, examSetElementWrappers);
        }

        // Дисциплины наборов ВИ
        // листенер EnrExamSetUniqueDisciplineListListener срабатывает и создает дисциплины наборов ВИ

        // Наборы ОП
        final IEducationYearDao eduYearDao = EducationYearManager.instance().dao();
        for (EnrProgramSetBase srcProgramSet : getList(EnrProgramSetBase.class, EnrProgramSetBase.enrollmentCampaign(), srcCampaign))
        {
            final EnrProgramSetBase newProgramSet;
            try
            {
                newProgramSet = srcProgramSet.getClass().newInstance();
            }
            catch (InstantiationException | IllegalAccessException e)
            {
                throw new RuntimeException(e);
            }
            newProgramSet.update(srcProgramSet, false);
            newProgramSet.setEnrollmentCampaign(newCampaign);

            // Копии ОП
            final List<EduProgramHigherProf> newProgramList = new ArrayList<>();
            final Map<EduProgram, EduProgram> src2copyEduProgramMap = new HashMap<>();
            if (newProgramSet instanceof EnrProgramSetSecondary)
            {
                // Для специальности (профессии) для приема на СПО устанавливать ссылку на соответствующую копию ОП
                final EnrProgramSetSecondary newProgramSetSec = (EnrProgramSetSecondary) newProgramSet;
                final EnrProgramSetSecondary srcProgramSetSec = (EnrProgramSetSecondary) srcProgramSet;
                newProgramSetSec.setProgram(eduYearDao.getProgramCopy(srcProgramSetSec.getProgram(), newCampaign.getEducationYear()));

                // Копии ОП на следующий год нет - пропускаем (не сохраняем) этот набор ОП
                if (newProgramSetSec.getProgram() == null)
                    continue;

                if (src2copyEduProgramMap.put(srcProgramSetSec.getProgram(), newProgramSetSec.getProgram()) != null)
                    throw new IllegalStateException();
            }
            else
            {
                // Собираем копии ОП для учебного года новой ПК
                final List<EnrProgramSetItem> srcProgramSetItemList = getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), srcProgramSet);
                for (EnrProgramSetItem srcProgramSetItem : srcProgramSetItemList)
                {
                    final EduProgramHigherProf copy = eduYearDao.getProgramCopy(srcProgramSetItem.getProgram(), newCampaign.getEducationYear());
                    if (copy == null)
                        break;

                    newProgramList.add(copy);

                    if (src2copyEduProgramMap.put(srcProgramSetItem.getProgram(), copy) != null)
                        throw new IllegalStateException();
                }

                // Какой-то копии ОП на следующий год нет - пропускаем (не сохраняем) этот набор ОП
                if (srcProgramSetItemList.size() != newProgramList.size())
                    continue;


                if (newProgramSet instanceof EnrProgramSetBS)
                {
                    // Для наборов ОП (бак-спец) установить все его настройки ВИ в нулл
                    final EnrProgramSetBS newProgramSetBS = (EnrProgramSetBS) newProgramSet;
                    newProgramSetBS.setExamSetVariantBase(null);
                    newProgramSetBS.setExamSetVariantConBase(null);
                    newProgramSetBS.setExamSetVariantConProf(null);
                    newProgramSetBS.setExamSetVariantHigh(null);
                    newProgramSetBS.setExamSetVariantMinBase(null);
                    newProgramSetBS.setExamSetVariantMinProf(null);
                    newProgramSetBS.setExamSetVariantSec(null);

                    // устанавливаем способ деления ОП на без деления
                    newProgramSet.setMethodDivCompetitions(getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_NO_DIV));
                }
            }

            // Копии планов приема подразделений
            final List<EnrProgramSetOrgUnitWrapper> newProgramSetOrgUnitWrappers = new ArrayList<>();
            for (EnrProgramSetOrgUnit srcProgramSetOrgUnit : getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet(), srcProgramSet))
            {
                final EnrOrgUnit newEnrOrgUnit = src2destEnrOrgUnitMap.get(srcProgramSetOrgUnit.getOrgUnit());
                if (newEnrOrgUnit == null)
                    throw new IllegalStateException();

                final EnrProgramSetOrgUnitWrapper newEnrProgramSetOrgUnitWrapper = new EnrProgramSetOrgUnitWrapper(newEnrOrgUnit);
                newEnrProgramSetOrgUnitWrapper.setFormativeOrgUnit(srcProgramSetOrgUnit.getFormativeOrgUnit());
                newEnrProgramSetOrgUnitWrapper.setContractPlan(srcProgramSetOrgUnit.getContractPlan());
                newEnrProgramSetOrgUnitWrapper.setMinisterialPlan(srcProgramSetOrgUnit.getMinisterialPlan());
                newEnrProgramSetOrgUnitWrapper.setTargetAdmPlan(srcProgramSetOrgUnit.getTargetAdmPlan());
                newEnrProgramSetOrgUnitWrapper.setExclusivePlan(srcProgramSetOrgUnit.getExclusivePlan());

                newProgramSetOrgUnitWrappers.add(newEnrProgramSetOrgUnitWrapper);
            }

            EnrProgramSetManager.instance().dao().saveOrUpdateProgramSet(newProgramSet, newProgramList, newProgramSetOrgUnitWrappers);
            session.flush();

            // Планыы приема по ОП в наборе ОП
            for (EnrProgramSetItemOrgUnitPlan srcEnrProgramSetOrgUnitPlan : getList(EnrProgramSetItemOrgUnitPlan.class, EnrProgramSetItemOrgUnitPlan.programSetItem().programSet(), srcProgramSet))
            {
                final EduProgramHigherProf programHigherProfCopy = (EduProgramHigherProf) src2copyEduProgramMap.get(srcEnrProgramSetOrgUnitPlan.getProgramSetItem().getProgram());
                final EnrProgramSetItem newProgramSetItem = getByNaturalId(new EnrProgramSetItem.NaturalId(newProgramSet, programHigherProfCopy));

                final EnrOrgUnit newEnrOrgUnit = src2destEnrOrgUnitMap.get(srcEnrProgramSetOrgUnitPlan.getOrgUnit());
                final EnrProgramSetOrgUnit newProgramSetOrgUnit = getByNaturalId(new EnrProgramSetOrgUnit.NaturalId(newProgramSet, newEnrOrgUnit));

                final EnrProgramSetItemOrgUnitPlan newEnrProgramSetOrgUnitPlan = new EnrProgramSetItemOrgUnitPlan(newProgramSetItem, newProgramSetOrgUnit);

                newEnrProgramSetOrgUnitPlan.update(srcEnrProgramSetOrgUnitPlan, false); // Копируем план приема
                newEnrProgramSetOrgUnitPlan.setEducationOrgUnit(null); // ссылку educationOrgUnit заполнять не нужно
                // Внимание!
                // Не используйте поля объекта newEnrProgramSetOrgUnitPlan для логики - метод update в них записал данные из исходного объекта (в базу они не попадут, т.к. это формулы).
                session.save(newEnrProgramSetOrgUnitPlan);
            }

            // Планы по виду ЦП
            for (EnrTargetAdmissionPlan srcTargetAdmissionPlan : getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit().programSet(), srcProgramSet))
            {
                final EnrCampaignTargetAdmissionKind newCampaignTAKind = src2destTAKindMap.get(srcTargetAdmissionPlan.getEnrCampaignTAKind());
                final EnrOrgUnit newEnrOrgUnit = src2destEnrOrgUnitMap.get(srcTargetAdmissionPlan.getProgramSetOrgUnit().getOrgUnit());
                final EnrProgramSetOrgUnit newProgramSetOrgUnit = getByNaturalId(new EnrProgramSetOrgUnit.NaturalId(newProgramSet, newEnrOrgUnit));

                final EnrTargetAdmissionPlan newTargetAdmissionPlan = new EnrTargetAdmissionPlan();
                newTargetAdmissionPlan.update(srcTargetAdmissionPlan, false);
                newTargetAdmissionPlan.setEnrCampaignTAKind(newCampaignTAKind);
                newTargetAdmissionPlan.setProgramSetOrgUnit(newProgramSetOrgUnit);

                session.save(newTargetAdmissionPlan);
            }
        }

        return newCampaign;
    }

    @Override
    public EnrEntrantAchievementSettings getAchievementSettings(EnrEnrollmentCampaign campaign, EnrRequestType requestType)
    {
        Preconditions.checkNotNull(campaign);
        Preconditions.checkNotNull(requestType);

        final EnrEntrantAchievementSettings settings = getByNaturalId(new EnrEntrantAchievementSettings.NaturalId(campaign, requestType));

        if (settings == null)
            throw new IllegalStateException("Not found EnrEntrantAchievementSettings for campaign " + campaign.getId() + " and request type " + requestType.getId());

        return settings;
    }

    @Override
    public void doChangeOpen(Long ecId)
    {
        EnrEnrollmentCampaign campaign = getNotNull(EnrEnrollmentCampaign.class, ecId);
        if (campaign.isLessOrEqualThan2014() && !campaign.isOpen()) {
            throw new ApplicationException("Работа с ПК старше 2014 года приостановлена.");
        }
        campaign.setOpen(!campaign.isOpen());
        update(campaign);
    }

    @Override
    public void checkEnrollmentCampaignOpen(EnrEnrollmentCampaign campaign)
    {
        if (existsEntityByCondition(EnrEnrollmentCampaign.class, "ec", and(
            eq(property("ec", EnrEnrollmentCampaign.id()), value(campaign.getId())),
            eq(property("ec", EnrEnrollmentCampaign.open()), value(Boolean.FALSE))
        ))) {
            throw new ApplicationException("Действие не может быть выполнено, т.к. приемная кампания «" + campaign.getTitle() + "» закрыта.");
        }
    }
    
    public static final RoleAssignmentCacheSource RA_SRC_TEMPLATE_ENR_CAMPAIGN = new RoleAssignmentCacheSource("t_enr_ec")
    {
        @Override
        public IDQLSelectableQuery query()
        {
            return new DQLSelectBuilder()
                .fromEntity(RoleAssignmentTemplateEnrCampaign.class, "t_enr_ec_a")
                .column(property("t_enr_ec_a", RoleAssignmentTemplateEnrCampaign.principalContext().id()), "u_pc")
                .column(property("t_enr_ec_a", RoleAssignmentTemplateEnrCampaign.roleConfig().role().id()), "u_role")
                .column(property("t_enr_ec_a", RoleAssignmentTemplateEnrCampaign.enrollmentCampaign().id()), "u_entity")
                .buildQuery();
        }
    };

    public static final RoleAssignmentCacheSource RA_SRC_LOCAL_ENR_CAMPAIGN = new RoleAssignmentCacheSource("l_enr_ec")
    {
        @Override
        public IDQLSelectableQuery query()
        {
            return new DQLSelectBuilder()
                .fromEntity(RoleAssignmentLocalEnrCampaign.class, "t_enr_ec_a")
                .column(property("t_enr_ec_a", RoleAssignmentLocalEnrCampaign.principalContext().id()), "u_pc")
                .column(property("t_enr_ec_a", RoleAssignmentLocalEnrCampaign.roleConfig().role().id()), "u_role")
                .column(property("t_enr_ec_a", RoleAssignmentLocalEnrCampaign.roleConfig().enrollmentCampaign().id()), "u_entity")
                .buildQuery();
        }
    };
}
