/**
 *$Id: EnrEnrollmentOrderVisaTemplateAddEditUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.EnrEnrollmentOrderManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;
import ru.tandemservice.unienr14.settings.entity.EnrVisasTemplate;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
@Input({
        @Bind(key = "visaTemplateId", binding = "visaTemplateId")
})
public class EnrEnrollmentOrderVisaTemplateAddEditUI extends UIPresenter
{
    private Long _visaTemplateId;
    private EnrVisasTemplate _visaTemplate = new EnrVisasTemplate();

    private List<OrderVisaItemsWrapper> _rowList;
    private OrderVisaItemsWrapper _currentRow;
    private OrderVisaItemsWrapper.PossibleVisaWrapper _currentRowCurrentVisa;
    private OrderVisaItemsWrapper _editedRow;
    private Integer _editedRowIndex;
    private boolean _rowAdded;

    @Override
    public void onComponentRefresh()
    {
        if (_visaTemplateId != null)
            _visaTemplate = DataAccessServices.dao().getNotNull(EnrVisasTemplate.class, _visaTemplateId);
        
        refreshExamSetElemetList();
    }

    private void refreshExamSetElemetList()
    {
        _rowList = new ArrayList<>();
        if (null != _visaTemplate) {
            final Map<GroupsMemberVising,List<EnrCampaignOrderVisaItem>> visaMap = EnrEnrollmentOrderManager.instance().visaTemplateDao().getOrderVisasByTemplate(_visaTemplate);
            
            int number = 1;
            for (Map.Entry<GroupsMemberVising,List<EnrCampaignOrderVisaItem>> entry : visaMap.entrySet())
            {
                final OrderVisaItemsWrapper wrapper = new OrderVisaItemsWrapper(entry.getKey(), entry.getValue(), number++);
                _rowList.add(wrapper);
            }
        }

        _editedRow = null;
        _editedRowIndex = null;
        _rowAdded = false;
    }
    
    public void onClickApply()
    {
        EnrEnrollmentOrderManager.instance().visaTemplateDao().saveOrUpdateVisas(_rowList, _visaTemplate);
        deactivate();
    }

    public void onClickAddRow()
    {
        if (_editedRow != null)
            throw new IllegalStateException();

        final int number = _rowList.size() + 1;

        _editedRow = new OrderVisaItemsWrapper(number);

        _rowList.add(new OrderVisaItemsWrapper(number));
        _editedRowIndex = _rowList.size() - 1;

        _rowAdded = true;
    }

    public void onClickEditRow()
    {
        if (_editedRow != null)
            throw new IllegalStateException();

        final OrderVisaItemsWrapper baseRow = findRow(getListenerParameterAsLong());

        _editedRow = new OrderVisaItemsWrapper(baseRow, _rowList.indexOf(baseRow) + 1);
        _editedRowIndex = _rowList.indexOf(baseRow);
        _rowAdded = false;
    }

    public void onClickSaveRow()
    {
        if (_editedRowIndex != null && _rowList.size() > _editedRowIndex && _editedRowIndex >= 0)
            _rowList.set(_editedRowIndex, _editedRow);

        _editedRow = null;
        _rowAdded = false;
    }

    public void onClickDeleteRow()
    {
        final OrderVisaItemsWrapper row = findRow(getListenerParameterAsLong());
        if (null != row)
            _rowList.remove(row);

        int number = 1;
        for (OrderVisaItemsWrapper wrapper : _rowList)
            wrapper.setNumber(number++);
    }


    public void onClickMoveRowUp()
    {
        final Map<Long, Long> parameter = getListenerParameter();
        if (parameter.isEmpty())
            throw new IllegalStateException();
        final Map.Entry<Long, Long> entry = parameter.entrySet().iterator().next();

        final OrderVisaItemsWrapper baseRow = findRow(entry.getKey());
        final OrderVisaItemsWrapper.PossibleVisaWrapper baseVisa = findRowVisa(entry.getKey(), entry.getValue());

        final int index = baseRow.getVisaList().indexOf(baseVisa);
        final OrderVisaItemsWrapper.PossibleVisaWrapper nearVisa = baseRow.getVisaList().get(index - 1);

        final int priority = baseVisa.getPriority();
        final int nearPrioriry = nearVisa.getPriority();

        baseVisa.setPriority(nearPrioriry);
        nearVisa.setPriority(priority);

        baseRow.sortVisaList();
    }

    public void onClickMoveRowDown()
    {
        final Map<Long, Long> parameter = getListenerParameter();
        if (parameter.isEmpty())
            throw new IllegalStateException();
        final Map.Entry<Long, Long> entry = parameter.entrySet().iterator().next();

        final OrderVisaItemsWrapper baseRow = findRow(entry.getKey());
        final OrderVisaItemsWrapper.PossibleVisaWrapper baseVisa = findRowVisa(entry.getKey(), entry.getValue());

        final int index = baseRow.getVisaList().indexOf(baseVisa);
        final OrderVisaItemsWrapper.PossibleVisaWrapper nearVisa = baseRow.getVisaList().get(index + 1);

        final int priority = baseVisa.getPriority();
        final int nearPrioriry = nearVisa.getPriority();

        baseVisa.setPriority(nearPrioriry);
        nearVisa.setPriority(priority);

        baseRow.sortVisaList();
    }

    public void onClickCancelEdit()
    {
        if (_rowAdded && !_rowList.isEmpty())
            _rowList.remove(_rowList.size() - 1);

        _editedRowIndex = null;
        _editedRow = null;
        _rowAdded = false;
    }

    private OrderVisaItemsWrapper findRow(final Long id)
    {
        return CollectionUtils.find(_rowList, object -> object.getId().equals(id));
    }
    
    private OrderVisaItemsWrapper.PossibleVisaWrapper findRowVisa(final Long rowId, final Long visaId)
    {
        final OrderVisaItemsWrapper baseRow = findRow(rowId);

        return CollectionUtils.find(baseRow.getVisaList(), object -> object.getPossibleVisa().getId().equals(visaId));
    }


    // Getters & Setters

    @SuppressWarnings("SimplifiableIfStatement")
    public boolean isCurrentRowCurrentVisaFirst()
    {
        final OrderVisaItemsWrapper.PossibleVisaWrapper currentVisa = _currentRowCurrentVisa;
        if (null == currentVisa) { return false; }

        final List<OrderVisaItemsWrapper.PossibleVisaWrapper> visaList = _currentRow.getVisaList();
        if (visaList.isEmpty()) { return false; }

        return currentVisa.equals(visaList.get(0));
    }

    @SuppressWarnings("SimplifiableIfStatement")
    public boolean isCurrentRowCurrentVisaLast()
    {
        final OrderVisaItemsWrapper.PossibleVisaWrapper currentVisa = _currentRowCurrentVisa;
        if (null == currentVisa) { return false; }

        final List<OrderVisaItemsWrapper.PossibleVisaWrapper> visaList = _currentRow.getVisaList();
        if (visaList.isEmpty()) { return false; }

        return currentVisa.equals(visaList.get(visaList.size()-1));
    }

    private boolean isAddForm()
    {
        return _visaTemplateId == null;
    }

    public boolean isEditMode()
    {
        return _editedRow != null;
    }

    public boolean isCurrentRowInEditMode()
    {
        return _editedRow != null && _editedRowIndex.equals(_rowList.indexOf(_currentRow));
    }    

    public String getSticker()
    {
        if (isAddForm())
            return getConfig().getProperty("ui.sticker.add");
        else
            return getConfig().getProperty("ui.sticker.edit");
    }


    // Accessors

    public Long getVisaTemplateId()
    {
        return _visaTemplateId;
    }

    public void setVisaTemplateId(Long visaTemplateId)
    {
        _visaTemplateId = visaTemplateId;
    }

    public EnrVisasTemplate getVisaTemplate()
    {
        return _visaTemplate;
    }

    public void setVisaTemplate(EnrVisasTemplate visaTemplate)
    {
        _visaTemplate = visaTemplate;
    }

    public List<OrderVisaItemsWrapper> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<OrderVisaItemsWrapper> rowList)
    {
        _rowList = rowList;
    }

    public OrderVisaItemsWrapper getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(OrderVisaItemsWrapper currentRow)
    {
        _currentRow = currentRow;
    }

    public OrderVisaItemsWrapper.PossibleVisaWrapper getCurrentRowCurrentVisa()
    {
        return _currentRowCurrentVisa;
    }

    public void setCurrentRowCurrentVisa(OrderVisaItemsWrapper.PossibleVisaWrapper currentRowCurrentVisa)
    {
        _currentRowCurrentVisa = currentRowCurrentVisa;
    }

    public OrderVisaItemsWrapper getEditedRow()
    {
        return _editedRow;
    }

    public void setEditedRow(OrderVisaItemsWrapper editedRow)
    {
        _editedRow = editedRow;
    }

    public Integer getEditedRowIndex()
    {
        return _editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        _editedRowIndex = editedRowIndex;
    }

    public boolean isRowAdded()
    {
        return _rowAdded;
    }

    public void setRowAdded(boolean rowAdded)
    {
        _rowAdded = rowAdded;
    }
}
