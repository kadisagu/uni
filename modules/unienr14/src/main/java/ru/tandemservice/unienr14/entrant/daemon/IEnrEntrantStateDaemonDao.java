package ru.tandemservice.unienr14.entrant.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;
import org.tandemframework.shared.commonbase.base.util.Wiki;

/**
 * @author vdanilov
 */
public interface IEnrEntrantStateDaemonDao
{

    final SpringBeanCache<IEnrEntrantStateDaemonDao> instance = new SpringBeanCache<IEnrEntrantStateDaemonDao>(IEnrEntrantStateDaemonDao.class.getName());

    /**
     * Обновляет состояния абитуриента (на основе состояния ВКГ)
     * @return true, если что-то было изменено
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15012693")
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateEntrantStatus();

    /**
     * Обновляет состояние выбранных конкурсов (на основе данных системы)
     * @return true, если что-то было изменено
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15012693")
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateRequestedCompetitionStatus();
}
