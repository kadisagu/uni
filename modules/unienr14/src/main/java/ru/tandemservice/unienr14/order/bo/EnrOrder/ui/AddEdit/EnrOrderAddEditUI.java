/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 8/13/13
 */
@Input({@Bind(key = "orderId", binding = "order.id")})
public class EnrOrderAddEditUI extends UIPresenter
{
    private EnrOrder _order = new EnrOrder();

    private EmployeePost _employeePost;
    private Date _createDate;

    private ISelectModel _employeePostModel;

    private boolean _addForm;
    private boolean _enrollmentCampaignDisabled;
    private boolean _orderTypeDisabled;

    private boolean typeBasic;
    private boolean typeCommand;
    private boolean typeReasonAndBasic;

    @Override
    public void onComponentRefresh()
    {

        setEmployeePostModel(new OrderExecutorSelectModel());

        if (getOrder().getId() == null) {
            setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
            // форма добавления
            setAddForm(true);
            setCreateDate(new Date());
            getOrder().setRequestType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getEnrollmentCampaign()));
        } else {
            // форма редактирования
            setAddForm(false);
            setOrder(IUniBaseDao.instance.get().getNotNull(EnrOrder.class, getOrder().getId()));
            setCreateDate(getOrder().getCreateDate());
            setEnrollmentCampaignDisabled(getOrder().getParagraphCount() > 0);
            setOrderTypeDisabled(isEnrollmentCampaignDisabled());
            setEnrollmentCampaign(getOrder().getEnrollmentCampaign());
        }

        onChangeOrderType();
    }

    public void onChangeOrderType() {
        EnrOrderPrintFormType enrollmentOrderType = getOrder().getPrintFormType();
        setTypeBasic(enrollmentOrderType != null && enrollmentOrderType.isBasic());
        setTypeCommand(enrollmentOrderType != null && enrollmentOrderType.isCommand());
        setTypeReasonAndBasic(enrollmentOrderType != null && enrollmentOrderType.isReasonAndBasic());
        if (getOrder().getType() != null && StringUtils.isEmpty(getOrder().getOrderBasicText()))
        {
            getOrder().setOrderBasicText(EnrOrderManager.instance().dao().getDefaultOrderBasicText(getOrder().getType().getCode()));
        }
    }

    public void onClickApply()
    {
        if(isAddForm())
        {
            EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
            EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getOrder().getRequestType(), getEnrollmentCampaign());
        }
        EnrOrder order = getOrder();
        Date createDate = getCreateDate();
        boolean addForm = isAddForm();
        String executor = OrderExecutorSelectModel.getExecutor(getEmployeePost());

        EnrOrder enrOrder = EnrOrderManager.instance().dao().saveOrUpdateOrder(order, createDate, addForm, executor);

        deactivate();

        if (isAddForm())
            _uiActivation.asDesktopRoot(EnrOrderPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, enrOrder.getId())
                .activate();
    }

    // presenter


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrOrderAddEdit.PARAM_ORDER_TYPE, getOrder().getType());
        dataSource.put(EnrOrderAddEdit.PARAM_ORDER_REASON, getOrder().getReason());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getOrder().getEnrollmentCampaign());
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _order.getEnrollmentCampaign();
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _order.setEnrollmentCampaign(enrollmentCampaign);
    }

    public boolean isShowActionDate() {
        return getOrder().getType() != null;
    }

    // Getters & Setters

    public EnrOrderBasic getBasic()
    {
        return getOrder().getBasic();
    }

    public void setBasic(EnrOrderBasic basic)
    {
        getOrder().setBasic(basic);
    }

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public boolean isEnrollmentCampaignDisabled()
    {
        return _enrollmentCampaignDisabled;
    }

    public void setEnrollmentCampaignDisabled(boolean enrollmentCampaignDisabled)
    {
        _enrollmentCampaignDisabled = enrollmentCampaignDisabled;
    }

    public boolean isOrderTypeDisabled()
    {
        return _orderTypeDisabled;
    }

    public void setOrderTypeDisabled(boolean orderTypeDisabled)
    {
        _orderTypeDisabled = orderTypeDisabled;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public Date getCreateDate()
    {
        return _createDate;
    }

    public void setCreateDate(Date createDate)
    {
        _createDate = createDate;
    }

    public boolean isTypeBasic()
    {
        return typeBasic;
    }

    public void setTypeBasic(boolean typeBasic)
    {
        this.typeBasic = typeBasic;
    }

    public boolean isTypeCommand()
    {
        return typeCommand;
    }

    public void setTypeCommand(boolean typeCommand)
    {
        this.typeCommand = typeCommand;
    }

    public boolean isTypeReasonAndBasic()
    {
        return typeReasonAndBasic;
    }

    public void setTypeReasonAndBasic(boolean typeReasonAndBasic)
    {
        this.typeReasonAndBasic = typeReasonAndBasic;
    }
}
