package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.egeData.EgeDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.enrollmentData.EnrollmentDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantData.EntrantDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataBlock;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.passDisciplineData.PassDisciplineDataBlock;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.ExternalOrgUnitDSHandler;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
@Configuration
public class EnrReportPersonAdd extends BusinessComponentManager
{
    public static final String COMPETITION_FILTERS_ENTRANT_REQUEST = CommonFilterAddon.class.getSimpleName();

    // sub tab panels
    public static final String ENTRANT_TAB_PANEL = "enrEntrantTabPanel";

    // second level tabs
    public static final String ENTRANT_PRINT_TAB = "enrEntrantPrintTab";
    public static final String ENTRANT_DATA_TAB = "enrEntrantDataTab";
    public static final String ENTRANT_REQUEST_DATA_TAB = "enrEntrantRequestDataTab";

    // tab block lists
    public static final String ENTRANT_DATA_BLOCK_LIST = "enrEntrantDataBlockList";
    public static final String ENTRANT_REQUEST_DATA_BLOCK_LIST = "enrEntrantRequestDataBlockList";
    public static final String PRELIMINARY_DATA_BLOCK_LIST = "enrPreliminaryDataBlockList";

    // block names
    public static final String ENTRANT_DATA = "enrEntrantData";
    public static final String BENEFIT_DATA = "enrBenefitData";
    public static final String ARMY_DATA = "armyData";
    public static final String EGE_DATA = "enrEgeData";
    public static final String ENTRANT_REQUEST_DATA = "enrEntrantRequestData";
    public static final String PRELIMINARY_DATA = "enrPreliminaryData";
    public static final String PASS_DISCIPLINE = "enrPassDisciplineData";
    public static final String ENROLLMENT_DATA = "enrollmentData";

    // print tab block lists
    public static final String ENTRANT_SCHEET_REQUIRED_PART_BLOCK_LIST = "enrEntrantScheetRequiredPartBlockListExtPoint";
    public static final String ENTRANT_SCHEET_ADDITIONAL_PART_BLOCK_LIST = "enrEntrantScheetAdditionalPartBlockListExtPoint";
    public static final String ENTRANT_REQUEST_SCHEET_BLOCK_LIST = "enrEntrantRequestScheetBlockListExtPoint";

    public static final String ENTRANT_PRINT_BLOCK_LIST = "enrEntrantPrintBlockList";

    // prefetch object name
    public static final String PREFETCH_ENROLLMENT_CAMPAIGN = "enrEnrollmentCampaign";
    public static final String PREFETCH_ENR_RATING_DATA = "enrEnrRatingDataUtil";
    public static final String PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP = "enrStateExamSubjectMarkMap";
    public static final String PREFETCH_PRE_STUDENT_MAP = "enrPreStudentMap";
    public static final String PREFETCH_ENROLLMENT_EXTRACT_MAP = "enrEnrollmentExtractMap";
    public static final String PREFETCH_CHOSEN_EXAM_MAP = "enrEnrollmentExtractMap";
    public static final String PREFETCH_DIRECTION_OLYMPIAD_MAP = "enrDirectionOlympiadMap";
    public static final String PREFETCH_OLYMPIAD_MARK_SOURCE_MAP = "enrOlympiadMarkSourceMap";
    public static final String PREFETCH_ENTRANT_EXAMGROUP_MAP = "enrEntrantExamGroupMap";
    public static final String PREFETCH_PARTNER_EDU_INSTITUTION = "passPartnerEduInstitutionMap";
    public static final String PREFETCH_STATE_EXAM_RESULTS = "stateExamResultMap";
    public static final String PREFETCH_TOTAL_DATA_ENTRANTS = "countEntrants";
    public static final String PREFETCH_TOTAL_DATA_REQUESTS = "countRequests";
    public static final String PREFETCH_TOTAL_DATA_COMPETITIONS = "countCompetitions";
    public static final String PREFETCH_ACHIEVEMENTS = "enrEntrantAchievement";

    // print tab block names
    public static final String PERSON_SCHEET_PERSON_BLOCK_LIST = "personScheetPersonBlockList";
    public static final String PERSON_SCHEET_EDUCATION_BLOCK_LIST = "personScheetEducationBlockList";
    public static final String PERSON_SCHEET_CONTACTS_BLOCK_LIST = "personScheetContactsBlockList";



    @Bean
    public TabPanelExtPoint enrEntrantTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(ENTRANT_TAB_PANEL)
                .addTab(htmlTab(ENTRANT_DATA_TAB, "EntrantDataTab"))
                .addTab(htmlTab(ENTRANT_REQUEST_DATA_TAB, "EntrantRequestDataTab"))
                .addTab(htmlTab(ENTRANT_PRINT_TAB, "EntrantPrintTab"))
                .create();
    }

    @Bean
    public BlockListExtPoint enrEntrantDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(ENTRANT_DATA, "block/entrantData/EntrantData"))
                .addBlock(htmlBlock(ARMY_DATA, "block/armyData/ArmyData"))
                .addBlock(htmlBlock(EGE_DATA, "block/egeData/EgeData"))
                .addBlock(htmlBlock(PASS_DISCIPLINE, "block/passDisciplineData/PassDisciplineData"))
                .addBlock(htmlBlock(ENROLLMENT_DATA, "block/enrollmentData/EnrollmentData"))
                .create();
    }

    @Bean
    public BlockListExtPoint enrEntrantPrintBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_PRINT_BLOCK_LIST)
                .create();
    }

    @Bean
    public BlockListExtPoint enrEntrantRequestDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_REQUEST_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(ENTRANT_REQUEST_DATA, "block/entrantRequestData/EntrantRequestData"))
                .create();
    }

    @Bean
    public BlockListExtPoint enrPreliminaryDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(PRELIMINARY_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(PRELIMINARY_DATA, "block/preliminaryData/PreliminaryData"))
                .create();
    }

    @Bean
    public BlockListExtPoint enrEntrantScheetBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_SCHEET_REQUIRED_PART_BLOCK_LIST)
                .addBlock(htmlBlock(ENTRANT_SCHEET_REQUIRED_PART_BLOCK_LIST, "print/entrant/RequiredPart"))
                .addBlock(htmlBlock(PERSON_SCHEET_EDUCATION_BLOCK_LIST, "print/education/Template"))
                .addBlock(htmlBlock(PERSON_SCHEET_CONTACTS_BLOCK_LIST, "print/contacts/Template"))
                .addBlock(htmlBlock(ENTRANT_SCHEET_ADDITIONAL_PART_BLOCK_LIST, "print/entrant/AdditionalPart"))
                .addBlock(htmlBlock(ENTRANT_REQUEST_SCHEET_BLOCK_LIST, "print/request/Template"))
                .create();
    }

    // EntrantDataBlock
    @Bean
    public IDefaultComboDataSourceHandler accessCoursesDSHandler()
    {
        return EntrantDataBlock.createAccessCoursesDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler infoSourceDSHandler()
    {
        return EntrantDataBlock.createInfoSourceDS(getName());
    }


    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateDSHandler()
    {
        return EntrantDataBlock.createEntrantCustomStateDS(getName());
    }

    // EnrollmentData

    @Bean
    public IDefaultComboDataSourceHandler enrollmentOrderStateDSHandler()
    {
        return EnrollmentDataBlock.createEnrollmentOrderStateDS(this.getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler cancelledExtractsDSHandler()
    {
        return EnrollmentDataBlock.createCancelledExtractsDS(this.getName());
    }

    // EgeData

    @Bean
    public IDefaultComboDataSourceHandler certificateIssuanceYearDSHandler()
    {
        return EgeDataBlock.createCertificateIssuanceYearDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamSubjectDSHandler()
    {
        return EgeDataBlock.createStateExamSubjectDS(getName());
    }

    // PassDisciplineData

    @Bean
    public IDefaultComboDataSourceHandler educationSubjectDSHandler()
    {
        return PassDisciplineDataBlock.createEducationSubjectDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectPassFormDS()
    {
        return PassDisciplineDataBlock.createSubjectPassFormDS(getName());
    }

    // EntrantRequestData

    @Bean
    public IDefaultComboDataSourceHandler competitionTypeDSHandler()
    {
        return EntrantRequestDataBlock.createCompetitionTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler()
    {
        return EntrantRequestDataBlock.createCompensationTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionKindDSHandler()
    {
        return EntrantRequestDataBlock.createTargetAdmissionKindDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return EntrantRequestDataBlock.createEntrantStateDS(getName());
    }


    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> benefitTypeDSHandler()
    {
        return EntrantRequestDataBlock.createBenefitTypeDS(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> benefitCategoryDSHandler()
    {
        return EntrantRequestDataBlock.createBenefitCategoryDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler targetContractOrgUnitsDS()
    {
        return new ExternalOrgUnitDSHandler(getName())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
//                super.prepareConditions(ep);
                EnrEnrollmentCampaign campaign = ep.context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                List<EnrEnrollmentCommission> commissions = ep.context.get("enrCommissions");

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rComp")
                        .where(eq(property("rComp", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(campaign)));
                if (commissions != null && !commissions.isEmpty())
                    subBuilder.where(in(property("rComp", EnrRequestedCompetition.request().enrollmentCommission()), commissions));
                subBuilder.where(in(property("rComp", EnrRequestedCompetition.competition().type().code()), Lists.newArrayList(EnrCompetitionTypeCodes.CONTRACT, EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)))
                        .column(property("rComp", EnrRequestedCompetition.externalOrgUnit().id()));

                ep.dqlBuilder.where(in(property("e", ExternalOrgUnit.id()), subBuilder.buildQuery()));
            }
        };
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(EntrantDataBlock.ACCESS_COURSES_DS, accessCoursesDSHandler()))
                .addDataSource(selectDS(EntrantDataBlock.INFO_SOURCE_DS, infoSourceDSHandler()))
                .addDataSource(selectDS(EntrantDataBlock.ENTRANT_CUSTOM_STATE_DS, entrantCustomStateDSHandler()))
                .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
                .addDataSource(selectDS(EnrollmentDataBlock.ENROLLMENT_ORDER_STATE_DS, enrollmentOrderStateDSHandler()))
                .addDataSource(selectDS(EnrollmentDataBlock.CANCELLED_EXTRACTS_DS, cancelledExtractsDSHandler()))
                .addDataSource(selectDS(EntrantRequestDataBlock.BENEFIT_TYPE_DS, benefitTypeDSHandler()))
                .addDataSource(selectDS(EntrantRequestDataBlock.BENEFIT_CATEGORY_DS, benefitCategoryDSHandler()).addColumn(EnrBenefitCategory.titleWithType().s()))
                .addDataSource(selectDS(EgeDataBlock.CERTIFICATE_ISSUANCE_YEAR_DS, certificateIssuanceYearDSHandler()))
                .addDataSource(selectDS(EgeDataBlock.STATE_EXAM_SUBJECT_DS, stateExamSubjectDSHandler()))
                .addDataSource(selectDS(PassDisciplineDataBlock.EDUCATION_SUBJECT_DS, educationSubjectDSHandler()))
                .addDataSource(selectDS(PassDisciplineDataBlock.SUBJECT_PASS_FORM_DS, subjectPassFormDS()))
                .addDataSource(selectDS(EntrantRequestDataBlock.COMPETITION_TYPE_DS, competitionTypeDSHandler()))
                .addDataSource(selectDS(EntrantRequestDataBlock.COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .addDataSource(selectDS(EntrantRequestDataBlock.TARGET_ADMISSION_KIND_DS, targetAdmissionKindDSHandler()))
                .addDataSource(selectDS(EntrantRequestDataBlock.ENTRANT_STATE_DS, entrantStateDSHandler()))
                .addDataSource(EnrEntrantRequestManager.instance().originalSubmissionAndReturnWayDSConfig())
                .addDataSource(selectDS(EntrantRequestDataBlock.ACCEPT_REQUEST_DS, EnrEntrantRequestManager.instance().acceptRequestDSHandler()))
                .addDataSource(selectDS(EntrantRequestDataBlock.TARGET_CONTRACT_ORGUNIT_DS, targetContractOrgUnitsDS()).addColumn(ExternalOrgUnit.titleWithLegalForm().s()))
                .create();
    }
}
