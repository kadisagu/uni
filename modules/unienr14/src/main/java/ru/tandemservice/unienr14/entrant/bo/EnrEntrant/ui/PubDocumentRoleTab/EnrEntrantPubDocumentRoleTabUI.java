/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentRoleTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardAddEdit.PersonIdentityCardAddEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.PersonIdentityCardEditInline;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Add.PersonDocumentAdd;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.RoleTab.PersonDocumentRoleTabUI;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEditUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author nvankov
 * @since 07.11.2016
 */
@Input({
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = "personRoleModel")
})
public class EnrEntrantPubDocumentRoleTabUI extends PersonDocumentRoleTabUI
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(EnrEntrantPubDocumentRoleTab.PARAM_ENTRANT_REQUEST, getSettings().get(EnrEntrantPubDocumentRoleTab.PARAM_ENTRANT_REQUEST));
    }

    public void onClickAddEduInstitution()
    {
        _uiActivation.asRegionDialog(EnrEntrantEduDocumentAddEdit.class.getSimpleName())
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, null)
                .parameter(EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, getEntrant().getId())
                .parameter(PersonEduDocumentAddEditUI.BIND_PERSON, getPerson().getId())
                .activate();
        _uiSupport.setRefreshScheduled(true);
    }

    @Override
    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(PersonDocumentAdd.class)
//                .parameter(PersonDocumentManager.BIND_PERSON_ID, getPerson().getId())
//                .parameter(PersonDocumentManager.BIND_CONTEXT_KEY, PersonDocumentUtil.key(getEntrant().getClass()))
                .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getPersonRole().getId())
                .activate();
        _uiSupport.setRefreshScheduled(true);
    }

    @Override
    public void onEditEntityFromList()
    {

        IEntity document = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        if (document instanceof PersonEduDocument)
        {
            _uiActivation.asRegionDialog(EnrEntrantEduDocumentAddEdit.class)
                    .parameter(EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, getEntrant().getId())
                    .parameter(PersonEduDocumentAddEditUI.BIND_PERSON, getEntrant().getPerson().getId())
                    .parameter(PublisherActivator.PUBLISHER_ID_KEY, document.getId())
                    .activate();
        } else if (document instanceof PersonDocument)
        {
            _uiActivation.asRegionDialog(PersonDocumentAdd.class)
                    .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                    .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getPersonRole().getId())
                    .activate();
        } else if (document instanceof IdentityCard)
        {
            _uiActivation.asRegionDialog(PersonIdentityCardAddEdit.class)
                    .parameter(UIPresenter.PUBLISHER_ID, getEntrant().getId())
                    .parameter(PersonIdentityCardEditInline.IDENTITY_CARD_ID, document.getId())
                    .parameter("birthDateCheck", getPersonRoleModel().isHasBirthDateCheck())
                    .activate();
        }
        _uiSupport.setRefreshScheduled(true);
    }

    @Override
    public void onDeleteEntityFromList()
    {
        IEntity document = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        if (document instanceof PersonEduDocument) {
            PersonEduDocumentManager.instance().dao().deleteDocument(getListenerParameterAsLong());
        }
        else if (document instanceof PersonDocument) {
            PersonDocumentManager.instance().dao().doUnlinkDocument(getListenerParameterAsLong(), getEntrant());

        }
        else if (document instanceof IdentityCard) {
            PersonManager.instance().dao().deleteIdentityCard(getListenerParameterAsLong());
        }
        _uiSupport.setRefreshScheduled(true);
    }

    public EnrEntrant getEntrant()
    {
        return (EnrEntrant) getPersonRole();
    }
}
