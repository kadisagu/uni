/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 30.07.2014
 */
public class CitizenshipModel extends CommonSingleSelectModel
{
    public static final Long NO_RUSSIAN_CITIZENSHIP = 0L;

    @Override
    public Object getPrimaryKey(Object value)
    {
        return value instanceof Long ? value : super.getPrimaryKey(value);
    }

    @Override
    protected IListResultBuilder createBuilder(String filter, Object o)
    {
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressCountry.class, "b").column(property("b"));

        if (o != null)
        {
            if (NO_RUSSIAN_CITIZENSHIP.equals(o))
            {
                final List<DataWrapper> list = new ArrayList<>();
                final DataWrapper element = new DataWrapper();
                element.setId(NO_RUSSIAN_CITIZENSHIP);
                element.setTitle("Не гражданин РФ");
                list.add(0, element);
                return new SimpleListResultBuilder<>(list);
            }
            else
            {
                builder.where(eq(property(AddressCountry.id().fromAlias("b")), commonValue(o, PropertyType.LONG)));
                return new SimpleListResultBuilder<>(Collections.singleton(new DataWrapper(IUniBaseDao.instance.get().getList(builder).iterator().next())));
            }
        }

        if (StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property(AddressCountry.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter))));

        builder.order(property(AddressCountry.title().fromAlias("b")));

        final List<DataWrapper> patchedList = new ArrayList<>();
        final boolean addNoCitizenship = StringUtils.containsIgnoreCase("Не гражданин РФ", filter);
        if (addNoCitizenship)
        {
            final DataWrapper element = new DataWrapper();
            element.setId(NO_RUSSIAN_CITIZENSHIP);
            element.setTitle("Не гражданин РФ");
            patchedList.add(0, element);
        }
        final DQLListResultBuilder<IEntity> resultBuilder = new DQLListResultBuilder<>(builder, addNoCitizenship ? 49 : 50);
        for (IEntity entity : resultBuilder.findOptions().getObjects())
            patchedList.add(new DataWrapper(entity));

        return new AbstractListResultBuilder()
        {
            final ListResult _result = new ListResult<>(patchedList, IUniBaseDao.instance.get().getCount(builder) + (addNoCitizenship ? 1 : 0));

            @Override
            public ListResult findOptions()
            {
                return _result;
            }

            @Override
            public List getValues()
            {
                if( _result.getObjects().size()!=_result.getMaxCount() )
                {
                    // похоже, запрос был с ограничением количества записей и в результате часть значений была потеряна
                    // данный запрос не должен содержать ограничение записей, иначе в Мультиселект нельзя будет добавить больше значений
                    throw new RuntimeException("Some values are lost. Objects:" + _result.getObjects().size() + " maxCount:" + _result.getMaxCount());
                }
                return _result.getObjects();
            }
        };
    }
}