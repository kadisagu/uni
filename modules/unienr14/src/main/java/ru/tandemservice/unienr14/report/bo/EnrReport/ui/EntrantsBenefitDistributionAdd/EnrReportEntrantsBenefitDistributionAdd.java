/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd;/**
 * @author rsizonenko
 * @since 09.07.2014
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

@Configuration
public class EnrReportEntrantsBenefitDistributionAdd extends BusinessComponentManager {

    public static final String ENR_BENEFIT_TYPE_DS = "enrBenefitTypeDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(ENR_BENEFIT_TYPE_DS, enrBenefitTypeHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrBenefitTypeHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrBenefitType.class)
                .filter(EnrBenefitType.title())
                .order(EnrBenefitType.title())
                .pageable(true);
    }
}
