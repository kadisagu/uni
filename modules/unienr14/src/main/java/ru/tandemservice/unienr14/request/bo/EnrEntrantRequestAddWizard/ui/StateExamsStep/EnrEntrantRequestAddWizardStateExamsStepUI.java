/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.StateExamsStep;

import java.util.Collections;
import java.util.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unienr14.catalog.entity.EnrSecondWaveStateExamPlace;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.EnrEntrantRequestAddWizardManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.EnrEntrantStateExamVO;

import com.google.common.collect.Lists;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardStateExamsStepUI extends UIPresenter implements IWizardStep
{
    private EnrEntrantWizardState _state;

    private EnrEntrant _entrant;
    private EnrOnlineEntrant _onlineEntrant;

    private List<EnrEntrantStateExamVO> _stateExamList = Lists.newArrayList();
    private EnrEntrantStateExamVO _currentStateExam;

    private ISelectModel _secondWaveExamPlaceModel;

    @Override
    public void onComponentRefresh()
    {
        _entrant = DataAccessServices.dao().getNotNull(_state.getEntrantId());
        if(_state.getOnlineEntrantId() != null)
            _onlineEntrant = DataAccessServices.dao().get(_state.getOnlineEntrantId());
        setSecondWaveExamPlaceModel(new LazySimpleSelectModel<EnrSecondWaveStateExamPlace>(EnrSecondWaveStateExamPlace.class));
        _stateExamList = EnrEntrantRequestAddWizardManager.instance().dao().prepareStateExamList(_state);
    }

    // actions

    @Override
    public void saveStepData()
    {
        validateStateExamList();
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
        EnrEntrantRequestAddWizardManager.instance().dao().saveStateExamList(_state, getStateExamList());
    }

    private void validateStateExamList()
    {
        int top = getEntrant().getEnrollmentCampaign().getEducationYear().getIntValue();
        for(EnrEntrantStateExamVO stateExamVO : getStateExamList())
        {
            if(!(top - 4 <= stateExamVO.getResult().getYear() &&  stateExamVO.getResult().getYear() <= top))
                _uiSupport.error("Год сдачи ЕГЭ не может быть меньше " + (top-4) + "г. или больше " + top + "г.", getIdYearField(getStateExamList().indexOf(stateExamVO)));
        }
    }

    public void onChangeSecondWave()
    {
        Integer index = getListenerParameter();
        if(index != null)
        {
            EnrEntrantStateExamVO value = _stateExamList.get(index);
            if(!value.getResult().isSecondWave())
            {
                value.getResult().setSecondWaveExamPlace(null);
            }
        }
    }

    public void onAdd()
    {
        Integer index = getListenerParameter();
        if(index != null)
        {
            EnrEntrantStateExamVO value = _stateExamList.get(index);
            _stateExamList.add(getNewValue(value.getSubject(), true));
        }
        Collections.sort(_stateExamList, EnrEntrantStateExamVO.COMPARATOR);
    }

    public void onDelete()
    {
        Integer index = getListenerParameter();
        if (index != null) {
            EnrEntrantStateExamVO value = _stateExamList.get(index);
            if(value.isCreatedInCurrentWizard() && value.getResult().getId() != null) {
                DataAccessServices.dao().delete(value.getResult().getId());
            }
            EnrStateExamSubject subject = value.getSubject();
            _stateExamList.remove(index.intValue());

            boolean containValueWithSameSubject = false;
            for(EnrEntrantStateExamVO examValue : _stateExamList) {
                if(examValue.getSubject().equals(subject)) {
                    containValueWithSameSubject = true;
                    break;
                }
            }
            if(!containValueWithSameSubject) {
                _stateExamList.add(index, getNewValue(subject, false));
            }
            Collections.sort(_stateExamList, EnrEntrantStateExamVO.COMPARATOR);
        }
    }

    // presenter

    public Integer getCurrentIndex()
    {
        return _stateExamList.indexOf(_currentStateExam);
    }

    public String getCurrentIdYearField()
    {
        return getIdYearField(getCurrentIndex());
    }

    public String getIdYearField(Integer index)
    {
        return "year_" + getCurrentIndex();
    }

    // utils

    private EnrEntrantStateExamVO getNewValue(EnrStateExamSubject subject, boolean selected)
    {
        EnrEntrantStateExamResult stateExamResult = new EnrEntrantStateExamResult();
        stateExamResult.setSubject(subject);
        stateExamResult.setEntrant(_entrant);
        stateExamResult.setYear(_entrant.getEnrollmentCampaign().getEducationYear().getIntValue());
        return new EnrEntrantStateExamVO(selected, subject, stateExamResult, true);
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public List<EnrEntrantStateExamVO> getStateExamList()
    {
        return _stateExamList;
    }

    public void setStateExamList(List<EnrEntrantStateExamVO> stateExamList)
    {
        _stateExamList = stateExamList;
    }

    public EnrEntrantStateExamVO getCurrentStateExam()
    {
        return _currentStateExam;
    }

    public void setCurrentStateExam(EnrEntrantStateExamVO currentStateExam)
    {
        _currentStateExam = currentStateExam;
    }

    public ISelectModel getSecondWaveExamPlaceModel()
    {
        return _secondWaveExamPlaceModel;
    }

    public void setSecondWaveExamPlaceModel(ISelectModel secondWaveExamPlaceModel)
    {
        _secondWaveExamPlaceModel = secondWaveExamPlaceModel;
    }

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }


}
