/**
 *$Id: EnrExamRoomListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.exams.bo.EnrExamRoom.ui.AddEdit.EnrExamRoomAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * @author Alexander Shaburov
 * @since 20.05.13
 */
public class EnrExamRoomListUI extends UIPresenter
{
    public static final String SETTINGS_ORG_UNIT = "orgUnit";

    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickAddExamRoom()
    {
        getActivationBuilder().asRegion(EnrExamRoomAddEdit.class)
                .parameter("territorialOrgUnitId", getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getId())
                .activate();
    }

    public void onClickEditExamRoom()
    {
        getActivationBuilder().asRegion(EnrExamRoomAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteExamRoom()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getSettings().<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
        dataSource.put(EnrExamRoomList.BIND_TERR_ORG_UNIT, getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
    }

    public EnrOrgUnit getOrgUnit(){ return getSettings().get(SETTINGS_ORG_UNIT); }

    // Getters & Setters

    public boolean isNothingSelected()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) == null || getSettings().get(SETTINGS_ORG_UNIT) == null;
    }
}
