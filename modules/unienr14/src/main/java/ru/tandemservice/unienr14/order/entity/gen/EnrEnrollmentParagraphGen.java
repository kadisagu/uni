package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа о зачислении абитуриентов (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentParagraphGen extends EnrAbstractParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph";
    public static final String ENTITY_NAME = "enrEnrollmentParagraph";
    public static final int VERSION_HASH = -1385777441;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_PROGRAM_FORM = "programForm";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String P_PARALLEL = "parallel";
    public static final String L_GROUP_MANAGER = "groupManager";

    private EnrOrgUnit _enrOrgUnit;     // Филиал
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private EduProgramSubject _programSubject;     // Направление, профессия, специальность
    private EduProgramForm _programForm;     // Форма обучения
    private EnrCompetitionType _competitionType;     // Вид приема
    private boolean _parallel;     // Параллельное обучение
    private EnrEnrollmentExtract _groupManager;     // Староста

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Филиал. Свойство не может быть null.
     */
    @NotNull
    public EnrOrgUnit getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал. Свойство не может быть null.
     */
    public void setEnrOrgUnit(EnrOrgUnit enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, профессия, специальность. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, профессия, специальность. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(EduProgramForm programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetitionType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Вид приема. Свойство не может быть null.
     */
    public void setCompetitionType(EnrCompetitionType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Параллельное обучение. Свойство не может быть null.
     */
    @NotNull
    public boolean isParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Параллельное обучение. Свойство не может быть null.
     */
    public void setParallel(boolean parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    /**
     * @return Староста.
     */
    public EnrEnrollmentExtract getGroupManager()
    {
        return _groupManager;
    }

    /**
     * @param groupManager Староста.
     */
    public void setGroupManager(EnrEnrollmentExtract groupManager)
    {
        dirty(_groupManager, groupManager);
        _groupManager = groupManager;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEnrollmentParagraphGen)
        {
            setEnrOrgUnit(((EnrEnrollmentParagraph)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrEnrollmentParagraph)another).getFormativeOrgUnit());
            setProgramSubject(((EnrEnrollmentParagraph)another).getProgramSubject());
            setProgramForm(((EnrEnrollmentParagraph)another).getProgramForm());
            setCompetitionType(((EnrEnrollmentParagraph)another).getCompetitionType());
            setParallel(((EnrEnrollmentParagraph)another).isParallel());
            setGroupManager(((EnrEnrollmentParagraph)another).getGroupManager());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentParagraphGen> extends EnrAbstractParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programForm":
                    return obj.getProgramForm();
                case "competitionType":
                    return obj.getCompetitionType();
                case "parallel":
                    return obj.isParallel();
                case "groupManager":
                    return obj.getGroupManager();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((EnrOrgUnit) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "programForm":
                    obj.setProgramForm((EduProgramForm) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((EnrCompetitionType) value);
                    return;
                case "parallel":
                    obj.setParallel((Boolean) value);
                    return;
                case "groupManager":
                    obj.setGroupManager((EnrEnrollmentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "programForm":
                        return true;
                case "competitionType":
                        return true;
                case "parallel":
                        return true;
                case "groupManager":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "programForm":
                    return true;
                case "competitionType":
                    return true;
                case "parallel":
                    return true;
                case "groupManager":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrOrgUnit":
                    return EnrOrgUnit.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "programForm":
                    return EduProgramForm.class;
                case "competitionType":
                    return EnrCompetitionType.class;
                case "parallel":
                    return Boolean.class;
                case "groupManager":
                    return EnrEnrollmentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentParagraph> _dslPath = new Path<EnrEnrollmentParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentParagraph");
    }
            

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getEnrOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, профессия, специальность. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getCompetitionType()
     */
    public static EnrCompetitionType.Path<EnrCompetitionType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Параллельное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#isParallel()
     */
    public static PropertyPath<Boolean> parallel()
    {
        return _dslPath.parallel();
    }

    /**
     * @return Староста.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getGroupManager()
     */
    public static EnrEnrollmentExtract.Path<EnrEnrollmentExtract> groupManager()
    {
        return _dslPath.groupManager();
    }

    public static class Path<E extends EnrEnrollmentParagraph> extends EnrAbstractParagraph.Path<E>
    {
        private EnrOrgUnit.Path<EnrOrgUnit> _enrOrgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduProgramForm.Path<EduProgramForm> _programForm;
        private EnrCompetitionType.Path<EnrCompetitionType> _competitionType;
        private PropertyPath<Boolean> _parallel;
        private EnrEnrollmentExtract.Path<EnrEnrollmentExtract> _groupManager;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getEnrOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, профессия, специальность. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> programForm()
        {
            if(_programForm == null )
                _programForm = new EduProgramForm.Path<EduProgramForm>(L_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getCompetitionType()
     */
        public EnrCompetitionType.Path<EnrCompetitionType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new EnrCompetitionType.Path<EnrCompetitionType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Параллельное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#isParallel()
     */
        public PropertyPath<Boolean> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<Boolean>(EnrEnrollmentParagraphGen.P_PARALLEL, this);
            return _parallel;
        }

    /**
     * @return Староста.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph#getGroupManager()
     */
        public EnrEnrollmentExtract.Path<EnrEnrollmentExtract> groupManager()
        {
            if(_groupManager == null )
                _groupManager = new EnrEnrollmentExtract.Path<EnrEnrollmentExtract>(L_GROUP_MANAGER, this);
            return _groupManager;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentParagraph.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
