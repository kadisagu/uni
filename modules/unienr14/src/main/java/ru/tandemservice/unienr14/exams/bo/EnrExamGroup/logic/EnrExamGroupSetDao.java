/**
 *$Id: EnrExamGroupSetDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 17.05.13
 */
public class EnrExamGroupSetDao extends UniBaseDao implements IEnrExamGroupSetDao
{
    @Override
    public Long saveOrUpdate(EnrExamGroupSet examGroupSet)
    {
        if (examGroupSet.getId() == null)
            examGroupSet.setOpen(true);

        if (examGroupSet.getBeginDate().getTime() > examGroupSet.getEndDate().getTime())
            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroupSet.add.dateException"));

        if (examGroupSet.getId() == null && EnrExamGroupManager.instance().setDao().hasOpened(examGroupSet.getEnrollmentCampaign().getId()))
            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroupSet.add.hasOpenedException"));

        final List<Object[]> setList = new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b")
                .column(property(EnrExamGroupSet.beginDate().fromAlias("b")))
                .column(property(EnrExamGroupSet.endDate().fromAlias("b")))
                .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(examGroupSet.getEnrollmentCampaign())))
                .where(ne(property(EnrExamGroupSet.id().fromAlias("b")), value(examGroupSet.getId())))
                .createStatement(getSession()).list();

        for (Object[] objects : setList)
        {
            final Date beginDate = (Date) objects[0];
            final Date endDate = (Date) objects[1];

            if (CommonBaseDateUtil.isBetween(beginDate, examGroupSet.getBeginDate(), examGroupSet.getEndDate()) || CommonBaseDateUtil.isBetween(endDate, examGroupSet.getBeginDate(), examGroupSet.getEndDate()))
                throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroupSet.add.dateCressesException"));
            else {
                if (CommonBaseDateUtil.isBetween(examGroupSet.getBeginDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(examGroupSet.getEndDate(), beginDate, endDate))
                    throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroupSet.add.dateCressesException"));
            }
        }

        getSession().saveOrUpdate(examGroupSet);

        return examGroupSet.getId();
    }

    @Override
    public void doToggleOpen(Long examGroupSetId)
    {
        final EnrExamGroupSet examGroupSet = getNotNull(EnrExamGroupSet.class, examGroupSetId);

        if (examGroupSet.isOpen())
        {
            examGroupSet.setOpen(false);
            update(examGroupSet);
        }
        else
        {
            final Long openCount = new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property(EnrExamGroupSet.id().fromAlias("b")))
                    .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(examGroupSet.getEnrollmentCampaign())))
                    .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                    .where(ne(property(EnrExamGroupSet.id().fromAlias("b")), value(examGroupSetId)))
                    .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

            if (openCount > 0)
                throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroupSet.toggleOpen.hasOpenedException"));

            examGroupSet.setOpen(true);
            update(examGroupSet);
        }
    }

    @Override
    public boolean hasOpened(Long enrollmentCampaignId)
    {
        final Long openCount = new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property(EnrExamGroupSet.id().fromAlias("b")))
                    .where(eq(property(EnrExamGroupSet.enrollmentCampaign().id().fromAlias("b")), value(enrollmentCampaignId)))
                    .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                    .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        return openCount > 0;
    }

    @Override
    public EnrExamGroupSet getDefaultForExamPassDisc(Long enrollmentCampaignId)
    {
        List<EnrExamGroupSet> setList = new DQLSelectBuilder()
            .fromEntity(EnrExamGroupSet.class, "b").column("b")
            .order(property(EnrExamGroupSet.open().fromAlias("b")))
            .order(property(EnrExamGroupSet.id().fromAlias("b")))
            .where(eq(property(EnrExamGroupSet.enrollmentCampaign().id().fromAlias("b")), value(enrollmentCampaignId)))
            .createStatement(getSession()).list();
        return setList.isEmpty() ? null : setList.get(setList.size() - 1);
    }
}
