package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина вступительного испытания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrDisciplineGen extends EntityBase
 implements INaturalIdentifiable<EnrDisciplineGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrDiscipline";
    public static final String ENTITY_NAME = "enrDiscipline";
    public static final int VERSION_HASH = 1203108520;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DEFAULT_PASS_MARK_AS_LONG = "defaultPassMarkAsLong";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private long _defaultPassMarkAsLong;     // Зачетный балл по умолчанию
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     */
    @NotNull
    public long getDefaultPassMarkAsLong()
    {
        return _defaultPassMarkAsLong;
    }

    /**
     * @param defaultPassMarkAsLong Зачетный балл по умолчанию. Свойство не может быть null.
     */
    public void setDefaultPassMarkAsLong(long defaultPassMarkAsLong)
    {
        dirty(_defaultPassMarkAsLong, defaultPassMarkAsLong);
        _defaultPassMarkAsLong = defaultPassMarkAsLong;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrDisciplineGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrDiscipline)another).getCode());
            }
            setShortTitle(((EnrDiscipline)another).getShortTitle());
            setDefaultPassMarkAsLong(((EnrDiscipline)another).getDefaultPassMarkAsLong());
            setTitle(((EnrDiscipline)another).getTitle());
        }
    }

    public INaturalId<EnrDisciplineGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrDisciplineGen>
    {
        private static final String PROXY_NAME = "EnrDisciplineNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrDisciplineGen.NaturalId) ) return false;

            EnrDisciplineGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EnrDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "defaultPassMarkAsLong":
                    return obj.getDefaultPassMarkAsLong();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "defaultPassMarkAsLong":
                    obj.setDefaultPassMarkAsLong((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "defaultPassMarkAsLong":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "defaultPassMarkAsLong":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "defaultPassMarkAsLong":
                    return Long.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrDiscipline> _dslPath = new Path<EnrDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrDiscipline");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getDefaultPassMarkAsLong()
     */
    public static PropertyPath<Long> defaultPassMarkAsLong()
    {
        return _dslPath.defaultPassMarkAsLong();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrDiscipline> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Long> _defaultPassMarkAsLong;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrDisciplineGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrDisciplineGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getDefaultPassMarkAsLong()
     */
        public PropertyPath<Long> defaultPassMarkAsLong()
        {
            if(_defaultPassMarkAsLong == null )
                _defaultPassMarkAsLong = new PropertyPath<Long>(EnrDisciplineGen.P_DEFAULT_PASS_MARK_AS_LONG, this);
            return _defaultPassMarkAsLong;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrDiscipline#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrDisciplineGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrDiscipline.class;
        }

        public String getEntityName()
        {
            return "enrDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
