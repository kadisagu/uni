package ru.tandemservice.unienr14.catalog.entity;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrInternalExamReasonGen;

import java.util.Collection;
import java.util.Set;

/**
 * Категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ
 */
public class EnrInternalExamReason extends EnrInternalExamReasonGen implements IDynamicCatalogItem
{
    private static final Set<String> HIDDEN_FIELDS = ImmutableSet.of(EnrInternalExamReason.P_USER_CODE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public boolean isActiveToggleEnabled(ICatalogItem item) { return false; }
            @Override public Collection<String> getHiddenFields() { return HIDDEN_FIELDS; }
        };
    }
}