/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionPlanFormula;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrPlanZeroFixer;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrTargetAdmissionCompetitionPlanFormula;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionGroup;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionItem;
import ru.tandemservice.unienr14.enrollment.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/27/15
 */
public class EnrEnrollmentModelDao extends UniBaseDao implements IEnrEnrollmentModelDao
{
    public static final Set<String> INCLUDED_STATE_CODES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
        EnrEntrantStateCodes.EXAMS_PASSED,
        EnrEntrantStateCodes.RECOMMENDED,
        EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED
    )));

    @Override
    public Long doCreateModel(Long campaignId, EnrRequestType requestType, String title, List<EnrModelStep> stepList)
    {
        if (stepList.isEmpty()) {
            throw new ApplicationException("Не настроены даты для зачисления.");
        }

        EnrEnrollmentModel model = new EnrEnrollmentModel();
        model.setTitle(title);
        model.setRequestType(requestType);
        model.setEnrollmentCampaign(get(EnrEnrollmentCampaign.class, campaignId));
        save(model);

        boolean copyFromEnrollmentData = true;

        for (EnrModelStep step : stepList) {
            step.setModel(model);

            EnrEnrollmentStep enrollmentStep = new DQLSimple<>(EnrEnrollmentStep.class)
                .where(EnrEnrollmentStep.enrollmentCampaign(), model.getEnrollmentCampaign())
                .where(EnrEnrollmentStep.enrollmentDate(), step.getEnrollmentDate())
                .where(EnrEnrollmentStep.requestType(), requestType)
                .where(EnrEnrollmentStep.state().code(), EnrEnrollmentStepStateCodes.CLOSED)
                .get();
            copyFromEnrollmentData = copyFromEnrollmentData && enrollmentStep != null;

            step.setBaseRealStep(copyFromEnrollmentData ? enrollmentStep : null);
            save(step);

            if (copyFromEnrollmentData) {

                for (EnrEnrollmentStepStage enrollmentStepStage : getList(EnrEnrollmentStepStage.class, EnrEnrollmentStepStage.enrollmentStep(), enrollmentStep)) {
                    EnrModelStepStage stepStage = new EnrModelStepStage();
                    stepStage.setEnrollmentStage(enrollmentStepStage.getEnrollmentStage());
                    stepStage.setStep(step);
                    save(stepStage);
                }

                Set<Long> enrolled = new HashSet<>(new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentStepItem.class, "s").column("s.id")
                    .where(eq(property("s", EnrEnrollmentStepItem.step()), value(enrollmentStep)))
                    .where(eq(property("s", EnrEnrollmentStepItem.shouldBeEnrolled()), value(Boolean.TRUE)))
                    .fromEntity(EnrEnrollmentExtract.class, "e")
                    .where(eq(property("s", EnrEnrollmentStepItem.entity().requestedCompetition()), property("e", EnrEnrollmentExtract.entity())))
                    .where(eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                    .where(eq(property("e", EnrEnrollmentExtract.cancelled()), value(Boolean.FALSE)))
                    .where(eq(property("e", EnrEnrollmentExtract.entity().request().entrant().state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                    .createStatement(getSession()).<Long>list());

                Set<Long> reenrolled = new HashSet<>(new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentStepItem.class, "s").column("s.id")
                    .where(eq(property("s", EnrEnrollmentStepItem.step()), value(enrollmentStep)))
                    .fromEntity(EnrEnrollmentExtract.class, "e")
                    .where(eq(property("s", EnrEnrollmentStepItem.entity().requestedCompetition()), property("e", EnrEnrollmentExtract.entity())))
                    .where(eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                    .where(eq(property("e", EnrEnrollmentExtract.cancelled()), value(Boolean.TRUE)))
                    .where(eq(property("e", EnrEnrollmentExtract.entity().request().entrant().state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                    .createStatement(getSession()).<Long>list());

                Set<EnrCompetition> competitions = new HashSet<>();

                for (EnrEnrollmentStepItem enrollmentStepItem : getList(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.step(), enrollmentStep))
                {
                    competitions.add(enrollmentStepItem.getEntity().getRequestedCompetition().getCompetition());

                    EnrModelStepItem stepItem = new EnrModelStepItem();
                    stepItem.setStep(step);
                    stepItem.setEntity(enrollmentStepItem.getEntity());

                    stepItem.setAccepted(enrollmentStepItem.isAccepted());
                    stepItem.setEnrollmentAvailable(enrollmentStepItem.isEnrollmentAvailable());
                    stepItem.setEnrolled(enrolled.contains(enrollmentStepItem.getId()));
                    stepItem.setIncluded(enrollmentStepItem.isIncluded());
                    stepItem.setOriginalIn(enrollmentStepItem.isOriginalIn());
                    stepItem.setReEnrolled(reenrolled.contains(enrollmentStepItem.getId()));

                    save(stepItem);
                }

                getSession().flush();

                for (EnrCompetition competition : competitions) {
                    EnrModelCompetitionPlan plan = new EnrModelCompetitionPlan();
                    plan.setStep(step);
                    plan.setCompetition(competition);
                    plan.setBasePlan(competition.getPlan());
                    plan.updatePlanFromBase();
                    save(plan);

                    if (model.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {
                        List<EnrTargetAdmissionCompetitionPlan> plans = getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition(), competition);
                        for (EnrTargetAdmissionCompetitionPlan taPlan : plans) {
                            save(new EnrModelTargetAdmissionPlan(model, taPlan));
                        }
                    }
                }

                continue;
            }

            List<EnrCampaignEnrollmentStage> stages = new DQLSelectBuilder()
                .fromEntity(EnrCampaignEnrollmentStage.class, "s")
                .where(eq(property(EnrCampaignEnrollmentStage.enrollmentCampaign().id().fromAlias("s")), value(campaignId)))
                .where(eq(property(EnrCampaignEnrollmentStage.requestType().fromAlias("s")), value(requestType)))
                .where(eq(property(EnrCampaignEnrollmentStage.date().fromAlias("s")), valueDate(step.getEnrollmentDate())))
                .createStatement(getSession()).list();

            if (stages.isEmpty()) {
                throw new ApplicationException("В выбранную дату стадий зачисления не предусмотрено.");
            }

            boolean usePercentage = EnrEnrollmentStepKindCodes.NO_REC_PERCENTAGE.equals(step.getKind().getCode());
            if (usePercentage) {
                for (EnrCampaignEnrollmentStage stage : stages) {
                    if (!EnrCompetitionTypeCodes.MINISTERIAL.equals(stage.getCompetitionType().getCode()) && !EnrCompetitionTypeCodes.CONTRACT.equals(stage.getCompetitionType().getCode())) {
                        throw new ApplicationException("В варианте зачисления «на процент от числа мест» могут участвовать только конкурсы с видом приема «Общий конкурс» и «По договору».");
                    }
                }
            }

            for (EnrCampaignEnrollmentStage stage : stages) {
                EnrModelStepStage stepStage = new EnrModelStepStage();
                stepStage.setEnrollmentStage(stage);
                stepStage.setStep(step);
                save(stepStage);
            }

            DQLSelectBuilder competitionBuilder = new DQLSelectBuilder()

                .fromEntity(EnrCompetition.class, "c").column("c.id")
                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().id().fromAlias("c")), value(campaignId)))

                .fromEntity(EnrCampaignEnrollmentStage.class, "s")
                .where(in(property("s"), stages))

                .where(eq(property(EnrCompetition.type().fromAlias("c")), property(EnrCampaignEnrollmentStage.competitionType().fromAlias("s"))))
                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("c")), property(EnrCampaignEnrollmentStage.programForm().fromAlias("s"))))
                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().acceptPeopleResidingInCrimea().fromAlias("c")), property(EnrCampaignEnrollmentStage.acceptPeopleResidingInCrimea().fromAlias("s"))))
                .where(eq(property(EnrCompetition.requestType().fromAlias("c")), property(EnrCampaignEnrollmentStage.requestType().fromAlias("s"))));

            List<EnrRatingItem> ratingItems = new DQLSelectBuilder()
                .fromEntity(EnrRatingItem.class, "i")
                .where(in(property(EnrRatingItem.requestedCompetition().competition().fromAlias("i")), competitionBuilder.buildQuery()))
                .createStatement(getSession()).list();

            Set<EnrCompetition> competitions = new HashSet<>();

            for (EnrRatingItem ratingItem : ratingItems)
            {
                EnrRequestedCompetition requestedCompetition = ratingItem.getRequestedCompetition();
                competitions.add(requestedCompetition.getCompetition());

                EnrModelStepItem stepItem = new EnrModelStepItem();
                stepItem.setStep(step);
                stepItem.setEntity(ratingItem);
                boolean archival = requestedCompetition.getRequest().getEntrant().isArchival();
                boolean takeAwayDocument = requestedCompetition.getRequest().isTakeAwayDocument();
                boolean includedState = INCLUDED_STATE_CODES.contains(requestedCompetition.getState().getCode());
                stepItem.setIncluded(!archival && !takeAwayDocument && includedState);
                stepItem.setOriginalIn(ratingItem.isOriginalIn());
                stepItem.setAccepted(requestedCompetition.isAccepted());
                stepItem.setEnrollmentAvailable(requestedCompetition.isEnrollmentAvailable());

                save(stepItem);
            }

            getSession().flush();

            for (EnrCompetition competition : competitions) {
                EnrModelCompetitionPlan plan = new EnrModelCompetitionPlan();
                plan.setStep(step);
                plan.setCompetition(competition);
                plan.setBasePlan(competition.getPlan());
                plan.updatePlanFromBase();
                save(plan);

                if (model.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {
                    List<EnrTargetAdmissionCompetitionPlan> plans = getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition(), competition);
                    for (EnrTargetAdmissionCompetitionPlan taPlan : plans) {
                        save(new EnrModelTargetAdmissionPlan(model, taPlan));
                    }
                }
            }
        }

        return model.getId();
    }

    @Override
    public void calcStats(EnrModelStep step, EnrEnrollmentModel model, Map<MultiKey, EnrModelCompetitionWrapper> wrapperMap)
    {
        if (step == null && model == null)
            throw new IllegalArgumentException();

        if (model == null) model = step.getModel();

        boolean ta = model.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition();

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrModelStepItem.class, "i")
            .fetchPath(DQLJoinType.inner, EnrModelStepItem.entity().fromAlias("i"), "r")
            .where(eq(property("i", EnrModelStepItem.step().model()), value(model)));

        if (step != null) dql.where(eq(property("i", EnrModelStepItem.step()), value(step)));

        Map<MultiKey, Set<EnrModelStepItem>> itemMap = SafeMap.get(HashSet.class);

        for (EnrModelStepItem item : this.<EnrModelStepItem>getList(dql)) {
            EnrCompetition competition = item.getEntity().getCompetition();
            EnrRequestedCompetitionTA taComp = ta && competition.isTargetAdmission() ? (EnrRequestedCompetitionTA) item.getEntity().getRequestedCompetition() : null;
            itemMap.get(EnrModelCompetitionWrapper.key(competition, taComp == null ? null : taComp.getTargetAdmissionKind().getTargetAdmissionKind())).add(item);
        }

        dql = new DQLSelectBuilder()
            .fromEntity(EnrChosenEntranceExam.class, "e")
            .fromEntity(EnrModelStepItem.class, "i")
            .where(eq(property("i", EnrModelStepItem.step().model()), value(model)))
            .where(eq(property("i", EnrModelStepItem.entity().requestedCompetition()), property("e", EnrChosenEntranceExam.requestedCompetition())))
            .where(eq(property("e", EnrChosenEntranceExam.maxMarkForm().passForm().code()), value(EnrExamPassFormCodes.STATE_EXAM)))
            .where(eq(property("i", EnrModelStepItem.enrolled()), value(Boolean.TRUE)))
            .where(eq(property("i", EnrModelStepItem.entity().statusEntranceExamsCorrect()), value(Boolean.TRUE)))
            .where(eq(property("i", EnrModelStepItem.entity().statusRatingComplete()), value(Boolean.TRUE)))
            .where(eq(property("i", EnrModelStepItem.entity().statusRatingPositive()), value(Boolean.TRUE)))
            .where(isNotNull(property("e", EnrChosenEntranceExam.maxMarkForm().markSource())))
            .column("i.id")
            .column("e.id")
            .column(property("e", EnrChosenEntranceExam.maxMarkForm().markSource().markAsLong()))
            .predicate(DQLPredicateType.distinct)
        ;

        if (step != null) dql.where(eq(property("i", EnrModelStepItem.step()), value(step)));

        Map<Long, List<Long>> markMap = SafeMap.get(ArrayList.class);

        for (Object[] row : this.<Object[]>getList(dql)) {
            markMap.get((Long) row[0]).add((Long) row[2]);
        }

        Set<Long> enrolled = new HashSet<>();
        for (Map.Entry<MultiKey, EnrModelCompetitionWrapper> e : wrapperMap.entrySet()) {
            EnrModelCompetitionWrapper wrapper = e.getValue();
            long markSum = 0;
            int markCount = 0;
            for (EnrModelStepItem item : itemMap.get(e.getKey())) {
                if (item.isEnrolled() && item.isReEnrolled()) {
                    wrapper.incReEnrolled();
                }
                if (!item.isEnrolled() || item.isReEnrolled()) {
                    continue;
                }
                if (enrolled.add(item.getEntity().getId())) {
                    wrapper.incEnrolled();
                }
                for (Long mark : markMap.get(item.getId())) {
                    markSum += mark;
                    markCount++;
                }
                wrapper.updatePassRating(item);
            }
            wrapper.calcAvgStateExamMark(markSum, markCount);
        }

        dql = new DQLSelectBuilder()
            .fromEntity(EnrModelCompetitionPlan.class, "plan")
            .where(eq(property("plan", EnrModelCompetitionPlan.step().model()), value(model)))
            .order(property("plan", EnrModelCompetitionPlan.step().enrollmentDate()), OrderDirection.asc)
        ;
        if (step != null) dql.where(eq(property("plan", EnrModelCompetitionPlan.step()), value(step)));

        for (EnrModelCompetitionPlan plan : DataAccessServices.dao().<EnrModelCompetitionPlan>getList(dql)) {
            EnrModelCompetitionWrapper wrapper = wrapperMap.get(EnrModelCompetitionWrapper.key(plan.getCompetition(), null));
            if (null != wrapper) wrapper.setPlan(plan.getPlan());
        }

        dql = new DQLSelectBuilder()
            .fromEntity(EnrModelTargetAdmissionPlan.class, "plan")
            .where(eq(property("plan", EnrModelTargetAdmissionPlan.model()), value(model)))
        ;
        for (EnrModelTargetAdmissionPlan plan : DataAccessServices.dao().<EnrModelTargetAdmissionPlan>getList(dql)) {
            EnrModelCompetitionWrapper wrapper = wrapperMap.get(EnrModelCompetitionWrapper.key(plan.getCompetition(), plan.getTargetAdmissionPlan().getEnrCampaignTAKind().getTargetAdmissionKind()));
            if (null != wrapper) wrapper.setPlan(plan.getPlan());
        }
    }

    @Override
    public EnrModelCompetitionWrapper calcStats(EnrModelStep step, EnrEnrollmentModel model, EnrCompetition competition, EnrTargetAdmissionKind taKind)
    {
        EnrModelCompetitionWrapper wrapper = new EnrModelCompetitionWrapper(null, model, competition, taKind);
        Map<MultiKey, EnrModelCompetitionWrapper> wrapperMap = new HashMap<>(1);
        wrapperMap.put(wrapper.key(), wrapper);
        calcStats(step, model, wrapperMap);
        return wrapper;
    }

    @Override
    public void doDelete(Long modelId)
    {
        delete(modelId);
    }

    @Override
    public EnrModellingContext prepareContext(EnrModelStep step, boolean debug)
    {
        final EnrModellingContext context = new EnrModellingContext(step, debug);
        Debug.begin("prepareEnrollmentContext");
        try {
            DebugUtils.debug(
                new DebugUtils.Section("init")
                {
                    @Override
                    public void execute()
                    {
                        EnrEnrollmentModelDao.this.doInitContext(context);
                    }
                }
            );
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
        return context;
    }

    @Override
    public void doEnroll(EnrEnrollmentModel model)
    {
        List<EnrModelStep> steps = getList(EnrModelStep.class, EnrModelStep.model(), model, EnrModelStep.enrollmentDate().s());
        if (steps.isEmpty()) {
            return;
        }

        EnrModelStep firstStep = steps.remove(0);
        if (!firstStep.isBasedOnRealStep()) {
            calculatePlans(firstStep, true);
            enroll(firstStep);
        }

        if (!firstStep.isBasedOnRealStep()) {
            calculatePlans(firstStep, false);
        }

        for (EnrModelStep step : steps) {
            if (step.isBasedOnRealStep()) {
                continue;
            }
            calculatePlans(step, false);
            enroll(step);
        }
    }

    @Override
    public void doAllEnrollmentAvailable(EnrEnrollmentModel model)
    {
        for (EnrModelStepItem item : new DQLSimple<>(EnrModelStepItem.class)
            .where(EnrModelStepItem.step().model(), model)
            .where(EnrModelStepItem.step().baseRealStep(), null)
            .list()) {
            item.setEnrollmentAvailable(true);
            update(item);
        }
    }

    @Override
    public void doRefreshOriginal(EnrEnrollmentModel model)
    {
        for (EnrModelStepItem item : new DQLSimple<>(EnrModelStepItem.class)
            .where(EnrModelStepItem.step().model(), model)
            .where(EnrModelStepItem.step().baseRealStep(), null)
            .list()) {
            item.setOriginalIn(item.getEntity().isOriginalIn());
            item.setAccepted(item.getEntity().getRequestedCompetition().isAccepted());
            item.setEnrollmentAvailable(item.getEntity().getRequestedCompetition().isEnrollmentAvailable());
            update(item);
        }
    }

    /** инициализирует данные о списке абитуриентов, существующих ранее приказах, планы по конкурсам */
    protected void doInitContext(final EnrModellingContext context)
    {
        // сначала обрабатываем строки шага
        Debug.begin("stepItems");
        try {
            // preload
            Debug.begin("preload");
            try {
                // конкурсы
                this.getList(
                    new DQLSelectBuilder()
                        .fromEntity(EnrCompetition.class, "c")
                        .where(exists(
                            new DQLSelectBuilder().fromEntity(EnrModelStepItem.class, "si").column(property("si.id"))
                                .where(eq(property(EnrModelStepItem.step().fromAlias("si")), value(context.getStep())))
                                .where(eq(property(EnrModelStepItem.entity().requestedCompetition().competition().fromAlias("si")), property("c")))
                                .buildQuery()
                        ))
                );

                // выбранные конкурсы
                this.getList(
                    new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "rc")
                        .where(exists(
                            new DQLSelectBuilder().fromEntity(EnrModelStepItem.class, "si").column(property("si.id"))
                                .where(eq(property(EnrModelStepItem.step().fromAlias("si")), value(context.getStep())))
                                .where(eq(property(EnrModelStepItem.entity().requestedCompetition().fromAlias("si")), property("rc")))
                                .buildQuery()
                        ))
                );
            } finally {
                Debug.end();
            }

            final boolean reenrollByPriorityEnabled = context.getStep().getModel().getEnrollmentCampaign().getSettings().isReenrollByPriorityEnabled();

            List<EnrModelStepItem> items = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "si").column(property("si"))
                .fetchPath(DQLJoinType.inner, EnrModelStepItem.entity().fromAlias("si"), "ri")
                .where(eq(property(EnrModelStepItem.step().fromAlias("si")), value(context.getStep())))
                .order(property(EnrRatingItem.position().fromAlias("ri")))
                .createStatement(this.getSession()).list();
            Map<EnrEntrant, List<EnrModelStepItem>> itemMap = SafeMap.get(ArrayList.class);
            for (EnrModelStepItem item : items) {
                itemMap.get(item.getEntity().getEntrant()).add(item);
            }

            final Map<EnrModelStepItem, EnrSelectionItem> wrapperMap = context.setupStepItemList(items);

            // previous enrollment
            Debug.begin("orders");
            try {
                final List<EnrModelStepItem> prevItems = new DQLSelectBuilder()
                    .fromEntity(EnrModelStepItem.class, "oi").column(property("oi"))
                    .where(eq(property(EnrModelStepItem.step().model().fromAlias("oi")), value(context.getStep().getModel())))
                    .where(lt(property(EnrModelStepItem.step().enrollmentDate().fromAlias("oi")), value(context.getStep().getEnrollmentDate(), PropertyType.DATE)))
                    .createStatement(this.getSession()).list();

                for (EnrModelStepItem prevItem : prevItems) {
                    if (!prevItem.isEnrolled()) continue;
                    for (EnrModelStepItem item : itemMap.get(prevItem.getEntity().getEntrant())) {
                        EnrModelStepItemPrevEnrollmentInfo prevEnrollmentInfo = new EnrModelStepItemPrevEnrollmentInfo(prevItem, item, reenrollByPriorityEnabled);
                        prevEnrollmentInfo.apply(wrapperMap.get(item));
                    }
                }
            } finally {
                Debug.end();
            }


        } finally {
            Debug.end();
        }

        Map<MultiKey, Integer> planMap = new HashMap<>();
        for (EnrModelCompetitionPlan plan : getList(EnrModelCompetitionPlan.class, EnrModelCompetitionPlan.step(), context.getStep())) {
            planMap.put(new MultiKey(plan.getCompetition().getId(), null), plan.getPlan());
        }
        for (EnrModelTargetAdmissionPlan plan : getList(EnrModelTargetAdmissionPlan.class, EnrModelTargetAdmissionPlan.model(), context.getStep().getModel())) {
            planMap.put(new MultiKey(plan.getCompetition().getId(), plan.getTargetAdmissionPlan().getEnrCampaignTAKind().getId()), plan.getPlan());
        }


        // планы приема (по всем группам)
        Debug.begin("setupPlan");
        try {
            final Set<String> generalCompetitionCodes = new HashSet<>(Arrays.asList(EnrCompetitionTypeCodes.MINISTERIAL, EnrCompetitionTypeCodes.CONTRACT));
            final Collection<EnrSelectionGroup> selectionGroups = context.getStepItemGroupMap().values();
            for (final EnrSelectionGroup group: selectionGroups) {
                final EnrCompetition comp = group.getKey().getCompetition();
                final EnrCampaignTargetAdmissionKind taKind = group.getKey().getTargetAdmissionKind();
                final Integer plan = planMap.get(new MultiKey(comp.getId(), (generalCompetitionCodes.contains(comp.getType().getCode()) || null == taKind) ? null : taKind.getId()));
                if (null == plan) {
                    throw new ApplicationException("Не задано число мест по конкурсу: " + comp.getTitle() + ".");
                }
                group.setupPlan(plan);
            }
        } finally {
            Debug.end();
        }

        // вызываем логику внутри контекста
        context.afterInitCallback();
    }

    private void enroll(EnrModelStep step)
    {
        this.lock("entrantSelectionContext." +step.getModel().getId());
        Debug.begin("doExecuteSelection");
        try {

            final EnrModellingContext modellingContext = prepareContext(step, Debug.isDisplay());
            modellingContext.executeSelection();
            modellingContext.applyResults();

//            byte[] xml = EnrXmlEnrollmentUtils.toXml(modellingContext.buildEnrXmlEnrollmentStepState());
//            step.setXml(xml);
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
    }

    private void calculatePlans(EnrModelStep step, boolean preEnroll) {
        IEnrCompetitionDao.PlanCalculationRule correctionRule = preEnroll ? IEnrCompetitionDao.PlanCalculationRule.BY_PASSED : IEnrCompetitionDao.PlanCalculationRule.BY_PASSED_AND_FREE;

        PlanCalculationContext context = new PlanCalculationContext(step, correctionRule);

        List<EnrModelCompetitionPlan> competitions = new DQLSelectBuilder()
            .fromEntity(EnrModelCompetitionPlan.class, "c").column(property("c"))
            .where(eq(property("c", EnrModelCompetitionPlan.step()), value(step)))
            .createStatement(getSession()).<EnrModelCompetitionPlan>list();

        Map<EnrProgramSetOrgUnit, List<EnrModelCompetitionPlan>> compPlanMap = new HashMap<>();
        for (EnrModelCompetitionPlan c : competitions) {
            SafeMap.safeGet(compPlanMap, c.getCompetition().getProgramSetOrgUnit(), ArrayList.class).add(c);
            c.setBasePlan(EnrCompetitionPlanFormula.calculatePlan(c.getCompetition(), context.getContext(c.getCompetition().getProgramSetOrgUnit())));
        }

        for (Map.Entry<EnrProgramSetOrgUnit, List<EnrModelCompetitionPlan>> e : compPlanMap.entrySet()) {
            EnrPlanZeroFixer.fix(e.getValue(), context.getContext(e.getKey()), new EntityComparator<EnrModelCompetitionPlan>(new EntityOrder(EnrModelCompetitionPlan.basePlan(), OrderDirection.desc)));
        }

        for (EnrModelCompetitionPlan c : competitions) {
            c.updatePlanFromBase();
            update(c);

        }

        // обновляем планы по видам ЦП для конкурсов

        List<EnrModelTargetAdmissionPlan> taCompPlans = getList(EnrModelTargetAdmissionPlan.class, EnrModelTargetAdmissionPlan.model(), step.getModel());
        Map<EnrProgramSetOrgUnit, List<EnrModelTargetAdmissionPlan>> taCompPlanMap = new HashMap<>();
        for (EnrModelTargetAdmissionPlan taPlan : taCompPlans) {
            taPlan.setPlan(EnrTargetAdmissionCompetitionPlanFormula.calculatePlan(taPlan, context.getContext(taPlan.getCompetition().getProgramSetOrgUnit())));
            update(taPlan);
            SafeMap.safeGet(taCompPlanMap, taPlan.getCompetition().getProgramSetOrgUnit(), ArrayList.class).add(taPlan);
        }

        for (Map.Entry<EnrProgramSetOrgUnit, List<EnrModelTargetAdmissionPlan>> e : taCompPlanMap.entrySet()) {
            EnrPlanZeroFixer.fix(e.getValue(), context.getContext(e.getKey()), new EntityComparator<EnrModelTargetAdmissionPlan>(new EntityOrder(EnrModelTargetAdmissionPlan.plan(), OrderDirection.desc)));
        }

        getSession().flush();
    }



    class PlanCalculationContext {
        final IEnrCompetitionDao.PlanCalculationRule correctionRule;
        final EnrModelStep step;

        Map<MultiKey, Integer> passedCountMap;
        Map<MultiKey, Integer> enrolledCountMap;

        public PlanCalculationContext(EnrModelStep step, IEnrCompetitionDao.PlanCalculationRule correctionRule)
        {
            this.step = step;
            this.correctionRule = correctionRule;
            initPassed();
            initEnrolled();
        }

        public EnrCompetitionPlanFormula.IPlanCalculationContext getContext(final EnrProgramSetOrgUnit psOu) {
            return new EnrCompetitionPlanFormula.IPlanCalculationContext() {
                @Override public IEnrCompetitionDao.PlanCalculationRule correctionRule() {
                    return correctionRule;
                }

                @Override
                public int s(String compType, String level) {
                    Integer count = passedCountMap.get(new MultiKey(psOu.getId(), compType, level));
                    return null == count ? 0 : count;
                }

                @Override
                public int e(String compType) {
                    Integer count = enrolledCountMap.get(new MultiKey(psOu.getId(), compType, null));
                    return null == count ? 0 : count;
                }

                @Override
                public int e(String compType, String level) {
                    Integer count = enrolledCountMap.get(new MultiKey(psOu.getId(), compType, level));
                    return null == count ? 0 : count;
                }

                @Override
                public int e(String taKind, String compType, String level)
                {
                    Integer count = enrolledCountMap.get(new MultiKey(psOu.getId(), taKind, compType, level));
                    return null == count ? 0 : count;
                }
            };
        }

        private void initEnrolled() {
            enrolledCountMap = new HashMap<>();
            // число абитуриентов, зачисленных по данному конкурсу в предыдущих шагах
            List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i")
                .joinPath(DQLJoinType.inner, EnrModelStepItem.entity().requestedCompetition().fromAlias("i"), "r")
                .where(eq(property("i", EnrModelStepItem.enrolled()), value(Boolean.TRUE)))
                //.where(lt(property("i", EnrModelStepItem.step().enrollmentDate()), valueDate(step.getEnrollmentDate())))
                .where(eq(property("i", EnrModelStepItem.step().model()), value(step.getModel())))
                .column(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
                .column(property("r", EnrRequestedCompetition.competition().type().code()))
                .column(property("r", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
                .group(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
                .group(property("r", EnrRequestedCompetition.competition().type().code()))
                .group(property("r", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
                .column(DQLFunctions.count(DQLPredicateType.distinct, property("r", EnrRequestedCompetition.id())))
                .createStatement(getSession()).list();
            for (Object[] row : rows) {
                Long psOuId = (Long) row[0];
                String type = (String) row[1];
                String level = (String) row[2];
                int count = ((Number) row[3]).intValue();
                enrolledCountMap.put(new MultiKey(psOuId, type, level), count);
                MultiKey totalKey = new MultiKey(psOuId, type, null);
                Integer total = enrolledCountMap.get(totalKey); if (null == total) total = 0;
                enrolledCountMap.put(totalKey, total + count);
            }

            rows = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i")
                .where(eq(property("i", EnrModelStepItem.enrolled()), value(Boolean.TRUE)))
                //.where(lt(property("i", EnrModelStepItem.step().enrollmentDate()), valueDate(step.getEnrollmentDate())))
                .where(eq(property("i", EnrModelStepItem.step().model()), value(step.getModel())))
                .fromEntity(EnrRequestedCompetitionTA.class, "r")
                .where(eq(property(EnrModelStepItem.entity().requestedCompetition().fromAlias("i")), property("r")))
                .column(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
                .column(property("r", EnrRequestedCompetition.competition().type().code()))
                .column(property("r", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
                .column(property("r", EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().code()))
                .group(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
                .group(property("r", EnrRequestedCompetition.competition().type().code()))
                .group(property("r", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
                .group(property("r", EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().code()))
                .column(DQLFunctions.count(DQLPredicateType.distinct, property("r", EnrRequestedCompetition.id())))
                .createStatement(getSession()).list();
            for (Object[] row : rows) {
                Long psOuId = (Long) row[0];
                String type = (String) row[1];
                String level = (String) row[2];
                String taKind = (String) row[3];
                int count = ((Number) row[4]).intValue();
                enrolledCountMap.put(new MultiKey(psOuId, taKind, type, level), count);
            }
        }

        private void initPassed() {
            passedCountMap = new HashMap<>();
            // число абитуриентов, сдавших все ВИ
            // 1. в рейтинге признак «По всем вступительным испытаниям балл не ниже порогового» = да
            // 2. в рейтинге признак «Выбранный конкурс абитуриента активный» = да
            List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i")
                .joinPath(DQLJoinType.inner, EnrModelStepItem.entity().fromAlias("i"), "r")
                .where(eq(property("i", EnrModelStepItem.step().model()), value(step.getModel())))
                .where(eq(property("r", EnrRatingItem.statusRatingPositive()), value(Boolean.TRUE)))
                .where(eq(property("r", EnrRatingItem.requestedCompetitionActive()), value(Boolean.TRUE)))
                .column(property("r", EnrRatingItem.competition().programSetOrgUnit().id()))
                .column(property("r", EnrRatingItem.competition().type().code()))
                .column(property("r", EnrRatingItem.competition().eduLevelRequirement().code()))
                .group(property("r", EnrRatingItem.competition().programSetOrgUnit().id()))
                .group(property("r", EnrRatingItem.competition().type().code()))
                .group(property("r", EnrRatingItem.competition().eduLevelRequirement().code()))
                .column(DQLFunctions.count(DQLPredicateType.distinct, property("r", EnrRequestedCompetition.id())))
                .createStatement(getSession()).list();
            for (Object[] row : rows) {
                Long psOuId = (Long) row[0];
                String type = (String) row[1];
                String level = (String) row[2];
                int count = ((Number) row[3]).intValue();
                MultiKey key = new MultiKey(psOuId, type, level);
                Integer total = passedCountMap.get(key); if (null == total) total = 0;
                passedCountMap.put(key, total + count);
            }
        }
    }
}