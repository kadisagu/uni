package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Категория лиц, обладающих особыми правами при поступлении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrBenefitCategoryGen extends EntityBase
 implements INaturalIdentifiable<EnrBenefitCategoryGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory";
    public static final String ENTITY_NAME = "enrBenefitCategory";
    public static final int VERSION_HASH = -1902246296;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_BENEFIT_TYPE = "benefitType";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DESCRIPTION = "description";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_TYPE = "titleWithType";

    private String _code;     // Системный код
    private EnrBenefitType _benefitType;     // Вид особого права
    private String _shortTitle;     // Сокращенное название
    private String _description;     // Описание
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Вид особого права. Свойство не может быть null.
     */
    @NotNull
    public EnrBenefitType getBenefitType()
    {
        return _benefitType;
    }

    /**
     * @param benefitType Вид особого права. Свойство не может быть null.
     */
    public void setBenefitType(EnrBenefitType benefitType)
    {
        dirty(_benefitType, benefitType);
        _benefitType = benefitType;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrBenefitCategoryGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrBenefitCategory)another).getCode());
            }
            setBenefitType(((EnrBenefitCategory)another).getBenefitType());
            setShortTitle(((EnrBenefitCategory)another).getShortTitle());
            setDescription(((EnrBenefitCategory)another).getDescription());
            setTitle(((EnrBenefitCategory)another).getTitle());
        }
    }

    public INaturalId<EnrBenefitCategoryGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrBenefitCategoryGen>
    {
        private static final String PROXY_NAME = "EnrBenefitCategoryNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrBenefitCategoryGen.NaturalId) ) return false;

            EnrBenefitCategoryGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrBenefitCategoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrBenefitCategory.class;
        }

        public T newInstance()
        {
            return (T) new EnrBenefitCategory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "benefitType":
                    return obj.getBenefitType();
                case "shortTitle":
                    return obj.getShortTitle();
                case "description":
                    return obj.getDescription();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "benefitType":
                    obj.setBenefitType((EnrBenefitType) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "benefitType":
                        return true;
                case "shortTitle":
                        return true;
                case "description":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "benefitType":
                    return true;
                case "shortTitle":
                    return true;
                case "description":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "benefitType":
                    return EnrBenefitType.class;
                case "shortTitle":
                    return String.class;
                case "description":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrBenefitCategory> _dslPath = new Path<EnrBenefitCategory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrBenefitCategory");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Вид особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getBenefitType()
     */
    public static EnrBenefitType.Path<EnrBenefitType> benefitType()
    {
        return _dslPath.benefitType();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getTitleWithType()
     */
    public static SupportedPropertyPath<String> titleWithType()
    {
        return _dslPath.titleWithType();
    }

    public static class Path<E extends EnrBenefitCategory> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EnrBenefitType.Path<EnrBenefitType> _benefitType;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _description;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrBenefitCategoryGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Вид особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getBenefitType()
     */
        public EnrBenefitType.Path<EnrBenefitType> benefitType()
        {
            if(_benefitType == null )
                _benefitType = new EnrBenefitType.Path<EnrBenefitType>(L_BENEFIT_TYPE, this);
            return _benefitType;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrBenefitCategoryGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EnrBenefitCategoryGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrBenefitCategoryGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory#getTitleWithType()
     */
        public SupportedPropertyPath<String> titleWithType()
        {
            if(_titleWithType == null )
                _titleWithType = new SupportedPropertyPath<String>(EnrBenefitCategoryGen.P_TITLE_WITH_TYPE, this);
            return _titleWithType;
        }

        public Class getEntityClass()
        {
            return EnrBenefitCategory.class;
        }

        public String getEntityName()
        {
            return "enrBenefitCategory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithType();
}
