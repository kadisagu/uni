package ru.tandemservice.unienr14.extreports.ext.ExtReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.ExtReportManager;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.IExtReportContextDefinition;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.OrgUnitExtReportContextDefinition;
import ru.tandemservice.unienr14.extreports.entity.codes.ExtReportContextCodes;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author vdanilov
 */
@Configuration
public class ExtReportExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private ExtReportManager _extReportManager;

    @Bean
    public ItemListExtension<IExtReportContextDefinition> contextDefinitionListExtension()
    {
        IItemListExtensionBuilder<IExtReportContextDefinition> ext = this.itemListExtension(_extReportManager.contextDefinitionList());

        // Аккредитованное ОУ
        ext.add(ExtReportContextCodes.ENR_ORG_UNIT_CONTEXT, new OrgUnitExtReportContextDefinition(ExtReportContextCodes.ENR_ORG_UNIT_CONTEXT) {
            @Override public boolean isExtReportContext(IEntity entity) {
                if (!OrgUnit.class.isInstance(entity)) { return false; }
                return EnrEnrollmentCampaignManager.instance().isEnrOrgUnit((OrgUnit)entity);
            }
        });

        return ext.create();
    }


}
