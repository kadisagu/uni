package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantGen;

import java.util.Collection;

/**
 * Абитуриент
 */
public class EnrEntrant extends EnrEntrantGen implements ISecLocalEntityOwner
{
    @Override
    public String getTitle()
    {
        if (getPerson() == null) {
            return this.getClass().getSimpleName();
        }
        return getFullTitle();
    }

    @Override
    public String getFullTitle()
    {
        return getPerson().getIdentityCard().getFio() + ": " + getContextTitle();
    }

    @Override
    public String getContextTitle()
    {
        return "Абитуриент (прием " + getEnrollmentCampaign().getTitle() + ")";
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return EnrEntrantManager.instance().dao().getSecLocalEntitiesForEntrant(this.getId());
    }

    /**
     * @return Можно ли редактировать персональные данные
     */
    public boolean isAccessible() {
        return !isArchival();
    }
}