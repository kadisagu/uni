/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.IndAchievementsStep;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.EnrEntrantRequestAddWizardManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.EnrEntrantAchievementVO;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardIndAchievementsStepUI extends UIPresenter implements IWizardStep
{
    private EnrEntrantWizardState _state;

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    private EnrEntrant _entrant;
    private EnrEntrantRequest _enrEntrantRequest;
    private List<EnrEntrantAchievementVO> _individualAchievements = Lists.newArrayList();
    private EnrEntrantAchievementVO _currentIndividualAchievement;
    private boolean _haveAchievements;
    private boolean _showRequestColumn;

    public List<EnrEntrantAchievementVO> getIndividualAchievements()
    {
        return _individualAchievements;
    }

    public void setIndividualAchievements(List<EnrEntrantAchievementVO> individualAchievements)
    {
        _individualAchievements = individualAchievements;
    }

    public EnrEntrantAchievementVO getCurrentIndividualAchievement()
    {
        return _currentIndividualAchievement;
    }

    public void setCurrentIndividualAchievement(EnrEntrantAchievementVO currentIndividualAchievement)
    {
        _currentIndividualAchievement = currentIndividualAchievement;
    }

    public boolean isHaveAchievements()
    {
        return _haveAchievements;
    }

    public void setHaveAchievements(boolean haveAchievements)
    {
        _haveAchievements = haveAchievements;
    }

    @Override
    public void onComponentRefresh()
    {
        _entrant = DataAccessServices.dao().getNotNull(_state.getEntrantId());
        _enrEntrantRequest = DataAccessServices.dao().getNotNull(_state.getEntrantRequestId());
        _individualAchievements = EnrEntrantRequestAddWizardManager.instance().dao().prepareIndividualAchievementsList(_state);
        _haveAchievements = !_individualAchievements.isEmpty();
        _showRequestColumn = EnrEntrantRequestManager.instance().dao().isShowRequestColumn(_entrant.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantRequestAddWizardIndAchievementsStep.BIND_ENTRANT_ID, _state.getEntrantId());
        dataSource.put(EnrEntrantRequestAddWizardIndAchievementsStep.BIND_REQUEST_TYPE_ID, _state.getRequestTypeId());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _state.getEnrollmentCampaignId());
    }

    public boolean isShowAddButton()
    {
        return _individualAchievements.size() == getCurrentIndex() + 1;
    }

    public boolean isShowRequestColumn()
    {
        return _showRequestColumn;
    }

    public Integer getCurrentIndex()
    {
        return _individualAchievements.indexOf(_currentIndividualAchievement);
    }

    public void onChangeHaveAchievements()
    {
        if(_haveAchievements && _individualAchievements.isEmpty())
            _individualAchievements.add(getNewValue());
    }

    public void onAdd()
    {
        _individualAchievements.add(getNewValue());
        Collections.sort(_individualAchievements, EnrEntrantAchievementVO.COMPARATOR);
    }

    public void onChangeAchievement()
    {
        String id = getListenerParameter();
        EnrEntrantAchievementVO achievementVO = null;
        for(EnrEntrantAchievementVO ach : getIndividualAchievements())
        {
            if(ach.getId().equals(id))
            {
                achievementVO = ach;
                break;
            }
        }

        if(achievementVO != null && achievementVO.getAchievement().getType() != null && achievementVO.getAchievement().getType().isForRequest() && achievementVO.getAchievement().getRequest() == null)
        {
            achievementVO.getAchievement().setRequest(_enrEntrantRequest);
        }
    }

    public void onDelete()
    {
        Integer index = getListenerParameter();
        if (index != null) {
            EnrEntrantAchievementVO value = _individualAchievements.get(index);
            if(value.isCreatedInCurrentWizard() && value.getAchievement().getId() != null) {
                DataAccessServices.dao().delete(value.getAchievement().getId());
            }
            _individualAchievements.remove(index.intValue());
            if(_individualAchievements.isEmpty()) {
                _individualAchievements.add(getNewValue());
            }
            Collections.sort(_individualAchievements, EnrEntrantAchievementVO.COMPARATOR);
        }
    }

    private EnrEntrantAchievementVO getNewValue()
    {
        EnrEntrantAchievement achievement = new EnrEntrantAchievement();
        achievement.setEntrant(_entrant);
        return new EnrEntrantAchievementVO(achievement, null, true);
    }

    public void applyStep()
    {
        if(_haveAchievements)
        {
            ErrorCollector errorCollector = ContextLocal.getErrorCollector();
            for (EnrEntrantAchievementVO vo : _individualAchievements)
            {
                if(!vo.getAchievement().getType().isForRequest())
                {
                    vo.getAchievement().setRequest(null);
                }

                vo.getAchievement().validateMark(errorCollector, vo.getMarkFieldId());
                vo.getAchievement().validateDocument(vo.getDocumentWrapper() == null, errorCollector, vo.getDocumentFieldId());
                vo.getAchievement().validateRequest(errorCollector, vo.getRequestFieldId());
            }

            if (!errorCollector.hasErrors())
                EnrEntrantRequestAddWizardManager.instance().dao().saveIndividualAchievementsList(_state, _individualAchievements);
        }
    }

    @Override
    public void saveStepData()
    {
        applyStep();
    }
}
