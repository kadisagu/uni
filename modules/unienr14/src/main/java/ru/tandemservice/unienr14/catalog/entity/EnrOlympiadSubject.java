package ru.tandemservice.unienr14.catalog.entity;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

import java.util.Collection;
import java.util.Set;

/**
 * Профильная дисциплина олимпиады﻿
 */
public class EnrOlympiadSubject extends EnrOlympiadSubjectGen implements IDynamicCatalogItem
{

    private static final Set<String> HIDDEN_FIELDS = ImmutableSet.of(P_USER_CODE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_FIELDS; }

        };
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrOlympiadSubject.class)
                .where(EnrOlympiadSubject.enabled(), Boolean.TRUE)
                .titleProperty(EnrOlympiadSubject.P_TITLE)
                .filter(EnrOlympiadSubject.title())
                .order(EnrOlympiadSubject.title());
    }
}