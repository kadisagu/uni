package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unienr14.catalog.bo.EnrEduLevelRequirement.ui.ItemPub.EnrEduLevelRequirementItemPub;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrEduLevelRequirementGen;

import java.util.Collection;
import java.util.Collections;

/**
 * Базовые уровни образования поступающих
 */
public class EnrEduLevelRequirement extends EnrEduLevelRequirementGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getViewComponentName() {
                return EnrEduLevelRequirementItemPub.class.getSimpleName();
            }
        };
    }

    @Override
    public Collection<String> getDeclinableProperties()
    {
        return Collections.singletonList(P_TITLE);
    }
}