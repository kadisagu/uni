/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic;

import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.mq.UniDQLExpressions;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/1/14
 */
public class EnrEnrollmentMinisteryParagraphDao extends UniBaseDao implements IEnrEnrollmentMinisteryParagraphDao
{
    @Override
    public void saveOrUpdateParagraph(EnrOrder order, EnrEnrollmentMinisteryParagraph paragraph, List<EnrEntrantForeignRequest> preStudents)
    {
        // В один и тот же приказ нельзя одновременно добавлять параграф
        NamedSyncInTransactionCheckLocker.register(getSession(), "EnrOrder" + order.getId());

        // V A L I D A T E

        if (preStudents == null || preStudents.isEmpty())
            throw new ApplicationException("Параграф не может быть пустым.");

        final Session session = getSession();
        session.refresh(order);

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + order.getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        List<EnrEntrantForeignRequest> list = new ArrayList<>(preStudents);
        Collections.sort(list, (o1, o2) -> {
            return 0; // TODO:!!!
        });

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (EnrEntrantForeignRequest req : list)
        {
            final EnrEntrant entrant = req.getEntrant();

            // абитуриент не должен быть в другом приказе
            EnrEnrollmentMinisteryExtract enrollmentExtract = get(EnrEnrollmentMinisteryExtract.class, EnrEnrollmentMinisteryExtract.L_ENTRANT_REQUEST, req);
            if (enrollmentExtract != null && !(paragraph != null && paragraph.equals(enrollmentExtract.getParagraph())))
            {
                String orderNumber = enrollmentExtract.getParagraph().getOrder().getNumber();
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» уже в приказе" + (StringUtils.isEmpty(orderNumber) ? "" : " №" + orderNumber) + ".");
            }

            // данные приказа должны совпадать с данными абитуриента
            if (!order.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Приемная кампания у абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!order.getRequestType().equals(req.getCompetition().getRequestType()))
                throw new ApplicationException("Вид заявления у конкурса абитуриента по направлению министерства «" + req.getCompetition().getTitle() + "» отличается от указанного в приказе.");
            if (!order.getCompensationType().equals(req.getCompetition().getType().getCompensationType()))
                throw new ApplicationException("Вид возмещения затрат у конкурса абитуриента по направлению министерства «" + req.getEntrant().getFullFio() + "» отличается от указанного в приказе.");

            // EnrOrgUnit orgUnit, EduProgramForm form, EduProgramSubject subject, EnrCompetitionType competitionType

            // признаки должны соответствовать тому, что выбрано на форме
            if (!paragraph.getEnrOrgUnit().equals(req.getCompetition().getProgramSetOrgUnit().getOrgUnit()))
                throw new ApplicationException("Филиал у конкурса абитуриента по направлению министерства «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
            if (!paragraph.getProgramForm().equals(req.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm()))
                throw new ApplicationException("Форма оубчения у конкурса абитуриента по направлению министерства «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!paragraph.getProgramSubject().equals(req.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject()))
                throw new ApplicationException("Направление (профессия, специальность) у конкурса абитуриента по направлению министерства «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
            if (!paragraph.getCompetitionType().equals(req.getCompetition().getType()))
                throw new ApplicationException("Вид приема у конкурса абитуриента по направлению министерства «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");


        }

        if (paragraph.getId() != null)
        {
            // E D I T   P A R A G R A P H
            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (IAbstractExtract extract : paragraph.getExtractList()) {
                EnrEnrollmentMinisteryExtract enrollmentExtract = (EnrEnrollmentMinisteryExtract) extract;
                if (!list.remove(enrollmentExtract.getEntity())) {
                    session.delete(enrollmentExtract);
                }
            }
        }
        else
        {
            // C R E A T E   P A R A G R A P H
            paragraph.setOrder(order);
            paragraph.setNumber(order.getParagraphCount() + 1);  // номер параграфа с единицы
            session.save(paragraph);
        }

        int counter = -1;
        for (EnrEntrantForeignRequest preStudent : list)
        {
            // создаем выписку
            EnrEnrollmentMinisteryExtract extract = new EnrEnrollmentMinisteryExtract();

            // в preStudent уже есть все данные для проведения выписки
            extract.setEntity(preStudent);
            extract.setParagraph(paragraph);
            extract.setCreateDate(order.getCreateDate());
            extract.setNumber(counter--); // нет номера. перенумеруем в конце метода update
            extract.setState(getByCode(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));

            // сохраняем выписку
            session.save(extract);
        }

        // перенумеруем все выписки, так чтобы сортировка была по ФИО
        session.flush(); // flush перед запросом, чтобы исзлечь ВСЕ выписки
        List<EnrEnrollmentMinisteryExtract> extractList = getList(EnrEnrollmentMinisteryExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        extractList.sort(CommonCollator.comparing(e -> e.getEntity().getEntrant().getFullFio()));

        // присваиваем уникальные номера выпискам
        counter = -1 - extractList.size(); /* заведомо неиспользованные номера */
        for (EnrEnrollmentMinisteryExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            session.update(enrollmentExtract);
        }
        session.flush();

        // нумеруем выписки с единицы, расставляем имена групп зачисления, вычисляем выписку старосты
        counter = 1;
        for (EnrEnrollmentMinisteryExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            session.update(enrollmentExtract);
        }
        session.update(paragraph);
    }

    @Override
    public void doCreateOrders(EnrEnrollmentCampaign enrEnrollmentCampaign)
    {
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "EnrEnrMinOrdersCreate-" + enrEnrollmentCampaign.getId());

        List<EnrEntrantForeignRequest> requests = new DQLSelectBuilder()
                .fromEntity(EnrEntrantForeignRequest.class, "i")
                .where(eq(property("i", EnrEntrantForeignRequest.entrant().enrollmentCampaign()), value(enrEnrollmentCampaign)))
                .where(notExists(new DQLSelectBuilder().fromEntity(EnrEnrollmentMinisteryExtract.class, "e")
                        .where(eq(property("e", EnrEnrollmentMinisteryExtract.entrantRequest().id()), property("i", EnrEntrantForeignRequest.id())))
                        .buildQuery()))
                .createStatement(session).list();
        Map<MultiKey, Map<EnrCompetition, List<EnrEntrantForeignRequest>>> orderMap = Maps.newHashMap();

        for(EnrEntrantForeignRequest request : requests)
        {
            EnrCompetition competition = request.getCompetition();

            // по одному приказу на каждую комбинацию:
            // вид заявления, вид возмещения затрат, дата зачисления
            MultiKey orderKey = new MultiKey(competition.getRequestType().getId(),
                    competition.getType().getCompensationType().getCode(),
                    CoreDateUtils.getDayFirstTimeMoment(request.getEnrollmentDate()));

            SafeMap.safeGet(SafeMap.safeGet(orderMap, orderKey, HashMap.class), competition, ArrayList.class)
                    .add(request);
        }

        EnrOrderType orderType = getCatalogItem(EnrOrderType.class, EnrOrderTypeCodes.ENROLLMENT_MIN);
        EnrOrderPrintFormType printFormType = getCatalogItem(EnrOrderPrintFormType.class, EnrOrderPrintFormTypeCodes.BASE_ENROLLMENT_MINISTERY);
        String orderBasicText = EnrOrderManager.instance().dao().getDefaultOrderBasicText(EnrOrderTypeCodes.ENROLLMENT_MIN);

        Calendar c = Calendar.getInstance(); c.setTime(new Date());
        Set<String> takenOrderNumbers = new HashSet<>(new DQLSelectBuilder()
                .fromEntity(EnrOrder.class, "e").column(property("e", EnrOrder.number()))
                .where(UniDQLExpressions.eqYear("e", EnrOrder.createDate().s(), c.get(Calendar.YEAR)))
                .createStatement(getSession()).<String>list());

        long orderNumberNext = 1;
        for (Map.Entry<MultiKey, Map<EnrCompetition, List<EnrEntrantForeignRequest>>> orderEntry : orderMap.entrySet())
        {
            EnrEntrantForeignRequest sample = orderEntry.getValue().values().iterator().next().get(0);

            EnrOrder order = new EnrOrder();
            orderNumberNext = findNumber(orderNumberNext, takenOrderNumbers);
            order.setNumber(String.valueOf(orderNumberNext));
            takenOrderNumbers.add(order.getNumber());
            order.setCommitDate(c.getTime());
            order.setActionDate(sample.getEnrollmentDate());
            order.setEnrollmentCampaign(sample.getEntrant().getEnrollmentCampaign());
            order.setCompensationType(sample.getCompetition().getType().getCompensationType());
            order.setRequestType(sample.getCompetition().getRequestType());
            order.setType(orderType);
            order.setPrintFormType(getPrintFormType(printFormType, sample));
            order.setOrderBasicText(orderBasicText);
            EnrOrderManager.instance().dao().saveOrUpdateOrder(order, new Date(), true, null);
            session.flush();

            for (Map.Entry<EnrCompetition, List<EnrEntrantForeignRequest>> paragraphEntry : orderEntry.getValue().entrySet())
            {
                EnrCompetition competition = paragraphEntry.getKey();

                EnrEnrollmentMinisteryParagraph paragraph = new EnrEnrollmentMinisteryParagraph();
                paragraph.setOrder(order);
                paragraph.setProgramForm(competition.getProgramSetOrgUnit().getProgramSet().getProgramForm());
                paragraph.setEnrOrgUnit(competition.getProgramSetOrgUnit().getOrgUnit());
                paragraph.setFormativeOrgUnit(competition.getProgramSetOrgUnit().getFormativeOrgUnit());
                paragraph.setProgramSubject(competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject());
                paragraph.setCompetitionType(competition.getType());
                saveOrUpdateParagraph(order, paragraph, paragraphEntry.getValue());
            }
        }

        if (orderMap.keySet().isEmpty()) {
            ContextLocal.getInfoCollector().add("Не найдено направлений Минобрнауки РФ, не включенных в приказы.");
        } else {

            ContextLocal.getInfoCollector().add("Сформировано приказов: " + orderMap.keySet().size()+ ".");
        }
    }

    private long findNumber(long orderNumberNext, Set<String> takenOrderNumbers)
    {
        while (takenOrderNumbers.contains(String.valueOf(orderNumberNext))) {
            orderNumberNext++;
        }
        return orderNumberNext;
    }

    protected EnrOrderPrintFormType getPrintFormType(EnrOrderPrintFormType defaultType, EnrEntrantForeignRequest sample) {
        return defaultType;
    }

    @Override
    public Collection<EnrAbstractExtract> getOtherExtracts(Collection<EnrEntrant> entrants, EnrAbstractParagraph paragraph)
    {
        return new DQLSelectBuilder()
                .fromEntity(EnrAbstractExtract.class, "e")
                .column(property("e"))
                .where(paragraph == null ? null : ne(property("e", EnrAbstractExtract.paragraph()), value(paragraph)))
                .where(or(
                        exists(new DQLSelectBuilder()
                                .fromEntity(EnrEnrollmentExtract.class, "ee")
                                .where(eq(property("ee", EnrEnrollmentExtract.id()), property("e", EnrAbstractExtract.id())))
                                .where(in(property("ee", EnrEnrollmentExtract.entity().request().entrant()), entrants))
                                .buildQuery()),
                        exists(new DQLSelectBuilder()
                                .fromEntity(EnrEnrollmentMinisteryExtract.class, "eme")
                                .where(eq(property("eme", EnrEnrollmentMinisteryExtract.id()), property("e", EnrAbstractExtract.id())))
                                .where(in(property("eme", EnrEnrollmentMinisteryExtract.entrantRequest().entrant()), entrants))
                                .buildQuery()),
                        exists(new DQLSelectBuilder()
                                .fromEntity(EnrAllocationExtract.class, "ae")
                                .where(eq(property("ae", EnrAllocationExtract.id()), property("e", EnrAbstractExtract.id())))
                                .where(in(property("ae", EnrAllocationExtract.entity().request().entrant()), entrants))
                                .buildQuery()),
                        exists(new DQLSelectBuilder()
                                .fromEntity(EnrCancelExtract.class, "ce")
                                .where(eq(property("ce", EnrCancelExtract.id()), property("e", EnrAbstractExtract.id())))
                                .where(in(property("ce", EnrCancelExtract.requestedCompetition().request().entrant()), entrants))
                                .buildQuery())))

                .order(property("e", EnrAbstractExtract.createDate()))
                .createStatement(this.getSession()).list();
    }
}
