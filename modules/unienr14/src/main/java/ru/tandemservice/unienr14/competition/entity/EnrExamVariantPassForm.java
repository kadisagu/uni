package ru.tandemservice.unienr14.competition.entity;

import java.util.Comparator;

import org.tandemframework.core.common.IEntityDebugTitled;

import ru.tandemservice.unienr14.competition.entity.gen.EnrExamVariantPassFormGen;

/**
 * Форма сдачи вступительного испытания
 */
public class EnrExamVariantPassForm extends EnrExamVariantPassFormGen implements IEntityDebugTitled
{
    public static enum Comparators implements Comparator<EnrExamVariantPassForm>
    {
        byFormCode() {
            @Override public int compare(EnrExamVariantPassForm o1, EnrExamVariantPassForm o2) {
                return o1.getPassForm().getCode().compareTo(o2.getPassForm().getCode());
            }
        }
        ;
    }

    @Override
    public String getEntityDebugTitle() {
        return getExamVariant().getTitle() + ": " + getPassForm().getTitle();
    }

}