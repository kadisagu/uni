/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.ui.ByRoom;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRegionComponentActivationBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.EnrScheduleManager;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic.EnrScheduleByRoomEventSearchDSHandler;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.ui.EventAddEdit.EnrScheduleEventAddEdit;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/16/13
 */
public class EnrScheduleByRoomUI extends UIPresenter
{
    public static final String SETTINGS_ORG_UNIT = "orgUnit";

    private EnrEnrollmentCampaign _enrollmentCampaign;

    @Override
    public void onComponentRefresh()
    {
        // ПК
        if(_enrollmentCampaign == null){
            _enrollmentCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        }
        // Даты проведения ВИ
        prepareExamGroupDateFilter();
        // нажимаем кнопку, что бы обновились фильтры, т.к. при подъеме значений из сеттингов они не обновляются, если поменялась ПК
        TapSupportUtils.addInitScript(new ClickButtonAction("search", false));
    }

    public void prepareExamGroupDateFilter()
    {
        if (getSettings().get("dateFromFilter") == null || getSettings().get("dateToFilter") == null)
        {
            EnrExamGroupSet examGroupSet;
            if (EnrExamGroupManager.instance().setDao().hasOpened(_enrollmentCampaign.getId()))
            {
                examGroupSet = (EnrExamGroupSet) DataAccessServices.dao().getList(
                        new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property("b"))
                                .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                                .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(_enrollmentCampaign))))
                        .get(0);
            }
            else
            {
                final List<EnrExamGroupSet> examGroupSetList = DataAccessServices.dao().getList(EnrExamGroupSet.class, EnrExamGroupSet.enrollmentCampaign(), _enrollmentCampaign, EnrExamGroupSet.id().s());
                examGroupSet = examGroupSetList.isEmpty() ? null : examGroupSetList.get(examGroupSetList.size() - 1);
            }
            if (examGroupSet != null)
            {
                if (getSettings().get("dateFromFilter") == null)
                    getSettings().set("dateFromFilter", examGroupSet.getBeginDate());
                if (getSettings().get("dateToFilter") == null)
                    getSettings().set("dateToFilter", examGroupSet.getEndDate());
            }
        }
    }

    public void onClickAddEvent()
    {
        final List<IEntity> examRoomList = getSettings().get("examRoomList");

        final IRegionComponentActivationBuilder activationBuilder = getActivationBuilder().asRegion(EnrScheduleEventAddEdit.class);

        if (CollectionUtils.isNotEmpty(examRoomList))
            activationBuilder.parameter("examRoomId", examRoomList.size() == 1 ? examRoomList.get(0).getId() : null);

        if (getListenerParameter() != null)
        {
            activationBuilder
                    .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                    .parameter("subAdd", true);
        }

        activationBuilder
                .parameter("terrOrgUnitId", getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getId())
                .activate();
    }

    public void onClickEditEvent()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "b").column(property(EnrExamGroupScheduleEvent.id().fromAlias("b")))
                .where(eq(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("b")), value(getListenerParameterAsLong())))
                .where(gt(property(EnrExamGroupScheduleEvent.examGroup().days().fromAlias("b")), value(1)));

        if (ISharedBaseDao.instance.get().existsEntity(dql.buildQuery()))
            throw new ApplicationException(getConfig().getProperty("editEvent.tooManyDaysExamGroupsException"));

        getActivationBuilder().asRegion(EnrScheduleEventAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteEvent()
    {
        EnrScheduleManager.instance().dao().deleteEvent(getListenerParameterAsLong());
    }

    public void onClickSearch()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrollmentCampaign);
        saveSettings();
        getConfig().getDataSource(EnrScheduleByRoom.SCHEDULE_EVENT_SEARCH_DS).doCleanupDataSource();
        getConfig().getDataSource(EnrScheduleByRoom.SCHEDULE_EVENT_SEARCH_DS).doPrepareDataSource();
    }

    public void onClickClearSettings()
    {
        Object orgUnit = getSettings().get(SETTINGS_ORG_UNIT); //не чистим филиал
        clearSettings();
        getSettings().set(SETTINGS_ORG_UNIT, orgUnit);
        saveSettings();
        prepareExamGroupDateFilter();
        getConfig().getDataSource(EnrScheduleByRoom.SCHEDULE_EVENT_SEARCH_DS).doCleanupDataSource();
        getConfig().getDataSource(EnrScheduleByRoom.SCHEDULE_EVENT_SEARCH_DS).doPrepareDataSource();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _enrollmentCampaign);
        dataSource.put(EnrScheduleByRoom.BIND_RESPONSIBLE_ORG_UNIT, getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
        dataSource.put(EnrScheduleByRoomEventSearchDSHandler.BIND_EXAM_ROOM_LIST, getSettings().get("examRoomList"));
        dataSource.put(EnrScheduleByRoomEventSearchDSHandler.BIND_DATE_FROM, getSettings().get("dateFromFilter"));
        dataSource.put(EnrScheduleByRoomEventSearchDSHandler.BIND_DATE_TO, getSettings().get("dateToFilter"));
    }


    // Getters and Setters

    public boolean isScheduleEventSearchListEmpty()
    {
        return getConfig().getDataSource(EnrScheduleByRoom.SCHEDULE_EVENT_SEARCH_DS).getRecords().size() == 0;
    }

    public String getScheduleEventSearchListEmptyMessage()
    {
        final Date dateFrom = getSettings().get("dateFromFilter");
        final Date dateTo = getSettings().get("dateToFilter");

        if (dateFrom == null && dateTo == null)
            return getConfig().getProperty("tooltip.emptyScheduleEvent");

        StringBuilder periodStr = new StringBuilder();
        if (dateFrom != null)
        {
            periodStr.append("с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom));
        }
        if (dateTo != null)
        {
            if (periodStr.length() > 0)
                periodStr.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo));
            else
                periodStr.append("по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo));
        }

        return getConfig().getProperty("tooltip.emptyScheduleEventOnDate", periodStr.toString());
    }

    public void onClickPrintEntrantExamRegList()
    {
        final CheckboxColumn checkboxColumn = (CheckboxColumn) ((PageableSearchListDataSource) getConfig().getDataSource(EnrScheduleByRoom.SCHEDULE_EVENT_SEARCH_DS)).getLegacyDataSource().getColumn("checkbox");
        Collection<IEntity> selectedObjects = checkboxColumn.getSelectedObjects();

        if (CollectionUtils.isEmpty(selectedObjects)) {
            throw new ApplicationException("Невозможно напечатать журнал регистрации абитуриентов. Необходимо выбрать хотя бы одно событие расписания ВИ.");
        }
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANCE_EXAM_REG_LIST);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, "enrExamScheduleEventList", selectedObjects);
    }

    // Accessors
    public EnrOrgUnit getOrgUnit(){ return getSettings().get(SETTINGS_ORG_UNIT); }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isNothingSelected()
    {
        return getEnrollmentCampaign() == null || getOrgUnit() == null;
    }
}
