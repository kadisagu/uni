package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrant

		// создано обязательное свойство needSpecialExamConditions
		{
			// создать колонку
			tool.createColumn("enr14_entrant_t", new DBColumn("needspecialexamconditions_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14_entrant_t set needspecialexamconditions_p=? where needspecialexamconditions_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_entrant_t", "needspecialexamconditions_p", false);
		}

		// создано свойство specialExamConditionsDetails
		{
			// создать колонку
			tool.createColumn("enr14_entrant_t", new DBColumn("specialexamconditionsdetails_p", DBType.TEXT));
		}
    }
}