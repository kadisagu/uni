/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 3/27/15
 */
public interface IEnrEnrollmentModelDao extends INeedPersistenceSupport
{
    Long doCreateModel(Long campaignId, EnrRequestType requestType, String title, List<EnrModelStep> stepList);

    void doDelete(Long modelId);

    void calcStats(EnrModelStep step, EnrEnrollmentModel model, Map<MultiKey, EnrModelCompetitionWrapper> wrapperMap);

    EnrModelCompetitionWrapper calcStats(EnrModelStep step, EnrEnrollmentModel model, EnrCompetition competition, EnrTargetAdmissionKind taKind);

    EnrModellingContext prepareContext(EnrModelStep step, boolean debug);

    void doEnroll(EnrEnrollmentModel model);

    void doRefreshOriginal(EnrEnrollmentModel model);

    void doAllEnrollmentAvailable(EnrEnrollmentModel model);
}