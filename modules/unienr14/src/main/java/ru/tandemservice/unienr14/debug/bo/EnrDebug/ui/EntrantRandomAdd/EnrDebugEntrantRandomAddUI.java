/**
 *$Id$
 */
package ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EntrantRandomAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.EnrDebugManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Zhebko
 * @since 17.03.2014
 */
public class EnrDebugEntrantRandomAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private int _entrantsNumber = 100;

    public EnrEnrollmentCampaign getEnrollmentCampaign(){ return _enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign){ _enrollmentCampaign = enrollmentCampaign; }

    public int getEntrantsNumber(){ return _entrantsNumber; }
    public void setEntrantsNumber(int entrantsNumber){ _entrantsNumber = entrantsNumber; }

    public void onClickApply()
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try { EnrDebugManager.instance().dao().doFullEcRandomEntrant(getEnrollmentCampaign(), getEntrantsNumber()); }
        finally { eventLock.release(); }

        deactivate();
    }
}