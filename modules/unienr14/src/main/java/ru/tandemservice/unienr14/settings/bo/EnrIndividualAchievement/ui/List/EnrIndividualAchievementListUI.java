/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.AddEdit.EnrIndividualAchievementAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.EditParams.EnrIndividualAchievementEditParams;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings;

/**
 * @author nvankov
 * @since 4/11/14
 */
public class EnrIndividualAchievementListUI extends UIPresenter
{
    private EnrEntrantAchievementSettings _achievementSettings;

    @Override
    public void onComponentRefresh()
    {
        // заполнение полей
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        updateAchievementSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(getEnrEnrollmentCampaign() != null && getRequestType() != null)
            dataSource
                    .put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrEnrollmentCampaign().getId())
                    .put("requestType", getRequestType().getId());
    }

    private void updateAchievementSettings()
    {
        EnrEnrollmentCampaign campaign = getEnrEnrollmentCampaign();
        EnrRequestType requestType = getRequestType();
        if (campaign == null || requestType == null)
            setAchievementSettings(null);
        else
            setAchievementSettings(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getAchievementSettings(campaign, requestType));
    }

    // Listeners
    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEnrollmentCampaign());
        updateAchievementSettings();
    }

    public void onClickEditParams()
    {
         _uiActivation.asRegionDialog(EnrIndividualAchievementEditParams.class)
                 .parameter(UIPresenter.PUBLISHER_ID, getAchievementSettings().getId())
                 .activate();
    }

    public void onClickAddAchievement()
    {
        _uiActivation.asRegionDialog(EnrIndividualAchievementAddEdit.class).parameter("requestType", getRequestType().getId()).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EnrIndividualAchievementAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onChangeRequestType()
    {
        getSettings().save();
        updateAchievementSettings();
    }


    // Getters & Setters
    public boolean isNothingSelected(){ return getEnrEnrollmentCampaign() == null || getRequestType() == null; }

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign(){ return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN); }

    public EnrRequestType getRequestType(){return getSettings().get("requestType"); }

    public EnrEntrantAchievementSettings getAchievementSettings()
    {
        return _achievementSettings;
    }

    public void setAchievementSettings(EnrEntrantAchievementSettings achievementSettings)
    {
        _achievementSettings = achievementSettings;
    }
}
