/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author azhebko
 * @since 18.06.2014
 */
public class MS_unienr14_2x6x0_28to29 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.0")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // EnrEntrantDocumentType
        Statement statement = tool.getConnection().createStatement();
        statement.execute("select id from enr14_c_entrant_doc_type_t order by priority_p");
        ResultSet result = statement.getResultSet();

        List<Long> ids = new ArrayList<>();
        while (result.next())
            ids.add(result.getLong(1));

        PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update enr14_c_entrant_doc_type_t set priority_p = ? where id = ?");

        int p = -1;
        for (Long id: ids)
        {
            preparedStatement.setInt(1, p--);
            preparedStatement.setLong(2, id);
            preparedStatement.execute();
        }

        p = 1;
        for (Long id: ids)
        {
            preparedStatement.setInt(1, p++);
            preparedStatement.setLong(2, id);
            preparedStatement.execute();
        }
    }
}