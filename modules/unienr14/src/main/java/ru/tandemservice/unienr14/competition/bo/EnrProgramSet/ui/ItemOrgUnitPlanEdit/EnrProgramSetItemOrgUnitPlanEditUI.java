/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab.EnrProgramSetItemOrgUnitPlanTabUI;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;

import java.util.*;

/**
 * @author azhebko
 * @since 01.08.2014
 */
@Input(@Bind(key = "programSetOrgUnit", binding = "programSetOrgUnit.id", required = true))
public class EnrProgramSetItemOrgUnitPlanEditUI extends UIPresenter
{
    private EnrProgramSetOrgUnit _programSetOrgUnit = new EnrProgramSetOrgUnit();
    private Collection<EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper> _rows;
    private EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper _currentRow;

    @Override
    public void onComponentRefresh()
    {
        _programSetOrgUnit = IUniBaseDao.instance.get().getNotNull(EnrProgramSetOrgUnit.class, getProgramSetOrgUnit().getId());
        prepareRows();
    }

    private void prepareRows()
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();
        _rows = new ArrayList<>();

        //  psi --> plan
        Map<Long, Integer> planMap = new HashMap<>();
        for (EnrProgramSetItemOrgUnitPlan plan: dao.getList(EnrProgramSetItemOrgUnitPlan.class, EnrProgramSetItemOrgUnitPlan.programSetOrgUnit().id(), getProgramSetOrgUnit().getId()))
            planMap.put(plan.getProgramSetItem().getId(), plan.getPlan());

        List<EnrProgramSetItem> items = dao.getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet().id(), getProgramSetOrgUnit().getProgramSet().getId());
        Collections.sort(items, ITitled.TITLED_COMPARATOR);

        for (EnrProgramSetItem item: items)
        {
            Integer plan = planMap.get(item.getId());
            _rows.add(new EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper(item, plan == null ? 0 : plan));
        }
    }

    public void onClickApply()
    {
        Map<Long, Integer> planMap = new HashMap<>(getRows().size());
        for (EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper row: getRows())
            planMap.put(row.getId(), row.getPlan());

        EnrProgramSetManager.instance().dao().updateProgramSetOrgUnitPlans(getProgramSetOrgUnit(), planMap);
        this.deactivate();
    }

    // Getters && Setters
    public EnrProgramSetOrgUnit getProgramSetOrgUnit() { return _programSetOrgUnit; }
    public Collection<EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper> getRows() { return _rows; }

    public EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper getCurrentRow() { return _currentRow; }
    public void setCurrentRow(EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper currentRow) { _currentRow = currentRow; }
}