package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.EnrReportExamResultsByDifferentSourcesAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportExamResultsByDifferentSourcesGen;

import java.util.Arrays;
import java.util.List;

/**
 * Сводка по результатам ВИ по материалам ОУ или по ЕГЭ
 */
public class EnrReportExamResultsByDifferentSources extends EnrReportExamResultsByDifferentSourcesGen implements IEnrReport
{

    public static final String REPORT_KEY = "enr14ReportExamResultsByDifferentSources";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(
            P_STAGE,
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET,
            P_PARALLEL,
            P_EXAM_RESULT_SOURCE,
            P_GROUP_BY_ORG_UNITS,
            P_SHOW_PROGRAM_SUBJECT_GROUPS
    );

    public static IEnrStorableReportDesc getDescription() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportExamResultsByDifferentSources.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportExamResultsByDifferentSourcesAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводка по результатам ВИ по материалам ОУ или по ЕГЭ»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводка по результатам ВИ по материалам ОУ или по ЕГЭ»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}