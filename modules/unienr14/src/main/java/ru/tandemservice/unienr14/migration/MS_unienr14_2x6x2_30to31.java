/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author azhebko
 * @since 18.07.2014
 */
public class MS_unienr14_2x6x2_30to31 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, Long> code2idMap = new HashMap<>(4);
        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute("select a.id, b.code_p from enr14_c_order_form_type_t a inner join scriptitem_t b on a.id = b.id");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next())
                code2idMap.put(resultSet.getString(2), resultSet.getLong(1));
        }

        Long testId = code2idMap.get("test");
        Long cancelId = code2idMap.get("cancel");
        Long baseEnrId = code2idMap.get("base_enrollment");
        Long baseCancelEnrId = code2idMap.get("base_cancel_enrollment");

        if (testId != null)
        {
            // есть элемент справочника со старым кодом
            if (baseEnrId != null)
            {
                // есть элемент с новым кодом - на него могли создать приказ - приказы перекидываем на старый, новый удаляем
                tool.executeUpdate("update enr14_order_t set printformtype_id = ? where printformtype_id = ?", testId, baseEnrId);
                tool.executeUpdate("delete from enr14_c_order_form_type_t where id = ?", baseEnrId);
                tool.executeUpdate("delete from scriptitem_t where id = ?", baseEnrId);
            }

            // меняем код со старого на новый
            tool.executeUpdate("update scriptitem_t set code_p = ? where id = ?", "base_enrollment", testId);
        }

        if (cancelId != null)
        {
            // есть элемент справочника со старым кодом
            if (baseCancelEnrId != null)
            {
                // есть элемент с новым кодом - на него могли создать приказ - приказы перекидываем на старый, новый удаляем
                tool.executeUpdate("update enr14_order_t set printformtype_id = ? where printformtype_id = ?", cancelId, baseCancelEnrId);
                tool.executeUpdate("delete from enr14_c_order_form_type_t where id = ?", baseCancelEnrId);
                tool.executeUpdate("delete from scriptitem_t where id = ?", baseCancelEnrId);
            }

            // меняем код со старого на новый
            tool.executeUpdate("update scriptitem_t set code_p = ? where id = ?", "base_cancel_enrollment", cancelId);
        }
    }
}