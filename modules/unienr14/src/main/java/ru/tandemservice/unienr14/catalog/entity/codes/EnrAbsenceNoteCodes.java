package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Отметка в отношении отсутствующих абитуриентов"
 * Имя сущности : enrAbsenceNote
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrAbsenceNoteCodes
{
    /** Константа кода (code) элемента : Неявка (title) */
    String UNKNOWN = "1";
    /** Константа кода (code) элемента : Неявка по уваж. причине (title) */
    String VALID = "2";

    Set<String> CODES = ImmutableSet.of(UNKNOWN, VALID);
}
