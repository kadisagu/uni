package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приемная кампания
 *
 * Проводится в рамках аккредитованного подразделения.
 *
 * Ограничения:
 * - dql check constraint "enrCampaign_achievementSettingCount": Для всех видов зявлений должна быть настройка индивидуальных достижений ПК.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentCampaignGen extends EntityBase
 implements ILocalRoleContext{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign";
    public static final String ENTITY_NAME = "enrEnrollmentCampaign";
    public static final int VERSION_HASH = 46422188;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_TITLE = "title";
    public static final String L_SETTINGS = "settings";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_OPEN = "open";
    public static final String P_LOCAL_ROLE_CONTEXT_TITLE = "localRoleContextTitle";
    public static final String P_ENROLLMENT_PERIOD = "enrollmentPeriod";

    private EducationYear _educationYear;     // Учебный год
    private String _title;     // Название
    private EnrEnrollmentCampaignSettings _settings;     // Настройки ПК
    private Date _dateFrom;     // Дата начала приема
    private Date _dateTo;     // Дата окончания приема
    private boolean _open = true;     // Открыта
    private String _localRoleContextTitle; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Настройки ПК. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEnrollmentCampaignSettings getSettings()
    {
        return _settings;
    }

    /**
     * @param settings Настройки ПК. Свойство не может быть null и должно быть уникальным.
     */
    public void setSettings(EnrEnrollmentCampaignSettings settings)
    {
        dirty(_settings, settings);
        _settings = settings;
    }

    /**
     * @return Дата начала приема. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Дата начала приема. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Дата окончания приема. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Дата окончания приема. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Открыта. Свойство не может быть null.
     */
    @NotNull
    public boolean isOpen()
    {
        return _open;
    }

    /**
     * @param open Открыта. Свойство не может быть null.
     */
    public void setOpen(boolean open)
    {
        dirty(_open, open);
        _open = open;
    }

    /**
     * @return 
     *
     * Это формула "title".
     */
    // @Length(max=255)
    public String getLocalRoleContextTitle()
    {
        return _localRoleContextTitle;
    }

    /**
     * @param localRoleContextTitle 
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setLocalRoleContextTitle(String localRoleContextTitle)
    {
        dirty(_localRoleContextTitle, localRoleContextTitle);
        _localRoleContextTitle = localRoleContextTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentCampaignGen)
        {
            setEducationYear(((EnrEnrollmentCampaign)another).getEducationYear());
            setTitle(((EnrEnrollmentCampaign)another).getTitle());
            setSettings(((EnrEnrollmentCampaign)another).getSettings());
            setDateFrom(((EnrEnrollmentCampaign)another).getDateFrom());
            setDateTo(((EnrEnrollmentCampaign)another).getDateTo());
            setOpen(((EnrEnrollmentCampaign)another).isOpen());
            setLocalRoleContextTitle(((EnrEnrollmentCampaign)another).getLocalRoleContextTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentCampaignGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentCampaign.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentCampaign();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "title":
                    return obj.getTitle();
                case "settings":
                    return obj.getSettings();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "open":
                    return obj.isOpen();
                case "localRoleContextTitle":
                    return obj.getLocalRoleContextTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "settings":
                    obj.setSettings((EnrEnrollmentCampaignSettings) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "open":
                    obj.setOpen((Boolean) value);
                    return;
                case "localRoleContextTitle":
                    obj.setLocalRoleContextTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "title":
                        return true;
                case "settings":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "open":
                        return true;
                case "localRoleContextTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "title":
                    return true;
                case "settings":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "open":
                    return true;
                case "localRoleContextTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "title":
                    return String.class;
                case "settings":
                    return EnrEnrollmentCampaignSettings.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "open":
                    return Boolean.class;
                case "localRoleContextTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentCampaign> _dslPath = new Path<EnrEnrollmentCampaign>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentCampaign");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Настройки ПК. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getSettings()
     */
    public static EnrEnrollmentCampaignSettings.Path<EnrEnrollmentCampaignSettings> settings()
    {
        return _dslPath.settings();
    }

    /**
     * @return Дата начала приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Дата окончания приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Открыта. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#isOpen()
     */
    public static PropertyPath<Boolean> open()
    {
        return _dslPath.open();
    }

    /**
     * @return 
     *
     * Это формула "title".
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getLocalRoleContextTitle()
     */
    public static PropertyPath<String> localRoleContextTitle()
    {
        return _dslPath.localRoleContextTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getEnrollmentPeriod()
     */
    public static SupportedPropertyPath<String> enrollmentPeriod()
    {
        return _dslPath.enrollmentPeriod();
    }

    public static class Path<E extends EnrEnrollmentCampaign> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<String> _title;
        private EnrEnrollmentCampaignSettings.Path<EnrEnrollmentCampaignSettings> _settings;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<Boolean> _open;
        private PropertyPath<String> _localRoleContextTitle;
        private SupportedPropertyPath<String> _enrollmentPeriod;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrEnrollmentCampaignGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Настройки ПК. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getSettings()
     */
        public EnrEnrollmentCampaignSettings.Path<EnrEnrollmentCampaignSettings> settings()
        {
            if(_settings == null )
                _settings = new EnrEnrollmentCampaignSettings.Path<EnrEnrollmentCampaignSettings>(L_SETTINGS, this);
            return _settings;
        }

    /**
     * @return Дата начала приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrEnrollmentCampaignGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Дата окончания приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrEnrollmentCampaignGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Открыта. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#isOpen()
     */
        public PropertyPath<Boolean> open()
        {
            if(_open == null )
                _open = new PropertyPath<Boolean>(EnrEnrollmentCampaignGen.P_OPEN, this);
            return _open;
        }

    /**
     * @return 
     *
     * Это формула "title".
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getLocalRoleContextTitle()
     */
        public PropertyPath<String> localRoleContextTitle()
        {
            if(_localRoleContextTitle == null )
                _localRoleContextTitle = new PropertyPath<String>(EnrEnrollmentCampaignGen.P_LOCAL_ROLE_CONTEXT_TITLE, this);
            return _localRoleContextTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign#getEnrollmentPeriod()
     */
        public SupportedPropertyPath<String> enrollmentPeriod()
        {
            if(_enrollmentPeriod == null )
                _enrollmentPeriod = new SupportedPropertyPath<String>(EnrEnrollmentCampaignGen.P_ENROLLMENT_PERIOD, this);
            return _enrollmentPeriod;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentCampaign.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentCampaign";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getEnrollmentPeriod();
}
