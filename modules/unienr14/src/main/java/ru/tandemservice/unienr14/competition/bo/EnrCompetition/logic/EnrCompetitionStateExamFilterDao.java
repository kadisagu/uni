package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.IEnrStateExamResultMark;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author vdanilov
 */
public class EnrCompetitionStateExamFilterDao extends SharedBaseDao implements IEnrCompetitionStateExamFilterDao {




    @Override
    @SuppressWarnings("unchecked")
    public Set<Long> getFilteredBSCompetitions(final EnrEntrant entrant) {
        return this.getFilteredBSCompetitions(
            entrant.getEnrollmentCampaign(),
            (Collection)this.getList(EnrEntrantStateExamResult.class, EnrEntrantStateExamResult.entrant(), entrant)
        );
    }

    @Override
    public Set<Long> getFilteredBSCompetitions(final EnrEnrollmentCampaign ec, final Collection<IEnrStateExamResultMark> stateExamResultList)
    {
        // { stateExamSubject.id -> safe-max(markAsLong), для всех предметов там not-null }
        final Map<Long, Long> resultMap = this.prepareResultMap(stateExamResultList);

        // нужно оставить только те предметы, по которым
        // * форма сдачи ЕГЭ не предусмотрена (тогда абитуриент имеет право участвовать в испытаниях ВУЗа, а значит может выбирать такие конкурсы)
        // * форма сдачи ЕГЭ есть и в результатах абитуриента есть результат, которые его покрывает, либо ЕГЭ запланировано (иначе он сразу же выбывает из конкурса)

        // { enrExamVariant.id } для всех examSet из ec, которые должны быть покрыты, но не покрыты результатами ЕГЭ абитуриента
        final Set<Long> uncoveredExamVariantIds = this.getUncoveredExamVariantIds(ec, resultMap);

        // { enrCompetition.id } настройки наборов ВИ которых содержат элементы из uncoveredExamVariantIds
        final Set<Long> uncoveredCompetitionIds = this.getCompetitionByExamVariantIds(ec, uncoveredExamVariantIds);

        // берем все конкурсы (только БС) и выкидываем те, которые не покрыты
        final Set<Long> allCompetitionIds = new HashSet<Long>();
        allCompetitionIds.addAll(
            new DQLSelectBuilder()
            .fromEntity(EnrCompetition.class, "c")
            .where(eq(property(EnrCompetition.requestType().code().fromAlias("c")), value(EnrRequestTypeCodes.BS)))
            .where(eq(property(EnrCompetition.examSetVariant().examSet().enrollmentCampaign().fromAlias("c")), value(ec)))
            .column(property("c.id"))
            .createStatement(EnrCompetitionStateExamFilterDao.this.getSession())
            .<Long>list()
        );
        allCompetitionIds.removeAll(uncoveredCompetitionIds);

        // возвращаем (по идее, там будет именно то, что можно выбирать)
        return allCompetitionIds;
    }

    /**
     * находит все конкурсы из ec (все, кроме «без-ви») в настройках ВИ которых есть варианты вступительных испытаний из examVariantIds
     * @param ec
     * @param examVariantIds { enrExamVariant.id }
     * @return { enrCompetition.id } настройки наборов ВИ которых содержат элементы из examVariantIds
     */
    private Set<Long> getCompetitionByExamVariantIds(final EnrEnrollmentCampaign ec, final Set<Long> examVariantIds)
    {
        final Set<Long> competitionIds = new HashSet<>();
        BatchUtils.execute(examVariantIds, 128, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> examVariantIds) {
                competitionIds.addAll(
                    new DQLSelectBuilder()
                    .fromEntity(EnrCompetition.class, "c")
                    .where(notIn(EnrCompetition.type().code().fromAlias("c"), Arrays.asList(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT, EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL)))

                    .where(eq(property(EnrCompetition.examSetVariant().examSet().enrollmentCampaign().fromAlias("c")), value(ec)))

                    .fromEntity(EnrExamVariant.class, "ev")
                    .where(in(property("ev.id"), examVariantIds))

                    .where(eq(property(EnrCompetition.examSetVariant().fromAlias("c")), property(EnrExamVariant.examSetVariant().fromAlias("ev"))))

                    .column(property("c.id"))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(EnrCompetitionStateExamFilterDao.this.getSession())
                    .<Long>list()
                );
            }
        });
        return competitionIds;
    }

    /**
     * Среди всех наборов ec находит те варианты вступительных испытаний, которые не покрываются результатами ЕГЭ
     * @param ec
     * @param resultMap { stateExamSubject.id -> safe-max(markAsLong) }
     * @return { enrExamVariant.id }, для которых есть форма сдачи ЕГЭ, но у абитуриента нет результата, покрывающего ее (либо нет предмета, либо оценка ниже пороговой)
     */
    public Set<Long> getUncoveredExamVariantIds(final EnrEnrollmentCampaign ec, final Map<Long, Long> resultMap)
    {
        final List<Object[]> rows = new DQLSelectBuilder()
        .fromEntity(EnrExamVariant.class, "ev")
        .where(eq(property(EnrExamVariant.examSetVariant().examSet().enrollmentCampaign().fromAlias("ev")), value(ec)))
        .where(exists(
            new DQLSelectBuilder()
            .fromEntity(EnrExamVariantPassForm.class, "evpf").column(property("evpf.id"))
            .where(eq(property(EnrExamVariantPassForm.passForm().code().fromAlias("evpf")), value(EnrExamPassFormCodes.STATE_EXAM)))
            .where(eq(property(EnrExamVariantPassForm.examVariant().fromAlias("evpf")), property("ev.id")))
            .buildQuery()
        ))

        .fromEntity(EnrExamSetDiscipline.class, "esd")
        .where(eq(property(EnrExamVariant.examSetElement().fromAlias("ev")), property(EnrExamSetDiscipline.source().fromAlias("esd"))))

        .column(property(EnrExamVariant.id().fromAlias("ev")))
        .column(property(EnrExamVariant.passMarkAsLong().fromAlias("ev")))
        .column(property(EnrExamSetDiscipline.discipline().stateExamSubject().id().fromAlias("esd")))

        .createStatement(this.getSession())
        .list();

        final Set<Long> examVariantIds = new HashSet<>();
        final Set<Long> coveredExamVariantIds = new HashSet<>();

        for(final Object[] row: rows) {
            final Long examVariantId = (Long)row[0];
            final Long passMarkAsLong = (Long)row[1];
            final Long stateExamSubjectId = (Long)row[2];

            examVariantIds.add(examVariantId);

            final Long resultMark = resultMap.get(stateExamSubjectId);
            if (null != resultMark && resultMark.longValue() >= passMarkAsLong.longValue()) {
                coveredExamVariantIds.add(examVariantId);
            }
        }

        examVariantIds.removeAll(coveredExamVariantIds);
        return examVariantIds;
    }

    /**
     * Среди всех баллов по ЕГЭ по каждому предмету оставляет только максимальный балл,
     * если предмет есть, но балла еще нет (есть отложенный ЕГЭ) - делаем MAX_VALUE (это означает, что человек обладает потенциально бесконечным баллом)
     * @param stateExamResultList
     * @return { subject.id -> max-null-safe( r.markAsLong | r.subject = subject) }, где max-null-safe(null, ....) = Long.MAX_VALUE
     */
    private Map<Long, Long> prepareResultMap(final Collection<IEnrStateExamResultMark> stateExamResultList)
    {
        final Map<Long, Long> resultMap = new HashMap<>(stateExamResultList.size());
        for (final IEnrStateExamResultMark result: stateExamResultList)
        {
            final Long resultSubjectId = result.getSubject().getId();

            Long resultMark = result.getMarkAsLong();
            if (null == resultMark) { resultMark = Long.MAX_VALUE; }

            final Long value = resultMap.get(resultSubjectId);
            if (null != value) { resultMark = Math.max(value.longValue(), resultMark.longValue()); }

            resultMap.put(resultSubjectId, resultMark);
        }
        return resultMap;
    }

    private <T> Set<T> collect(final PropertyPath<T> resultPart, final PropertyPath<Long> filterIdPath, final Collection<Long> ids)
    {
        final Set<T> resultIds = new HashSet<>();
        BatchUtils.execute(ids, 256, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> ids) {
                resultIds.addAll(
                    new DQLSelectBuilder()
                    .fromEntity(EnrCompetition.class, "c")
                    .where(in(property(filterIdPath.fromAlias("c")), ids))
                    .column(property(resultPart.fromAlias("c")))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(EnrCompetitionStateExamFilterDao.this.getSession())
                    .<T>list()
                );
            }
        });
        return resultIds;
    }


    @Override
    public Set<Long> getEduProgramFormIds(final Collection<Long> competitionIds) {
        return this.collect(EnrCompetition.programSetOrgUnit().programSet().programForm().id(), EnrCompetition.id(), competitionIds);
    }

    @Override
    public Set<Long> getProgramSetOrgUnitOuIds(final Collection<Long> competitionIds) {
        return this.collect(EnrCompetition.programSetOrgUnit().orgUnit().id(), EnrCompetition.id(), competitionIds);
    }

    @Override
    public Set<Long> getProgramSetOrgUnitFormativeOuIds(final Collection<Long> competitionIds) {
        return this.collect(EnrCompetition.programSetOrgUnit().formativeOrgUnit().id(), EnrCompetition.id(), competitionIds);
    }

    @Override
    public Set<Long> getEduProgramSubjectIds(final Collection<Long> competitionIds) {
        return this.collect(EnrCompetition.programSetOrgUnit().programSet().programSubject().id(), EnrCompetition.id(), competitionIds);
    }

    @Override
    public Set<Long> getCompetitionTypeIds(Collection<Long> competitionIds)
    {
        return this.collect(EnrCompetition.type().id(), EnrCompetition.id(), competitionIds);
    }

    @Override
    public Set<Long> getCompetitionEduLevelRequirementIds(Collection<Long> competitionIds)
    {
        return this.collect(EnrCompetition.eduLevelRequirement().id(), EnrCompetition.id(), competitionIds);
    }
}
