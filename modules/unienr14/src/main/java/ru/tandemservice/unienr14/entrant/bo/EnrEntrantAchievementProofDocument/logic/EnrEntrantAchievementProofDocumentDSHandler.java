/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievementProofDocument.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.GraduationHonour2EduDocumentKind;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.entrant.entity.gen.IEnrEntrantAchievementProofDocumentGen;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * User: nvankov
 * Date: 5/30/16
 */
public class EnrEntrantAchievementProofDocumentDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String BIND_ENTRANT_ID = EnrEntrantManager.BIND_ENTRANT_ID;
    public static final String BIND_REQUEST_TYPE_ID = EnrEntrantRequestManager.BIND_REQUEST_TYPE_ID;

    public EnrEntrantAchievementProofDocumentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long entrantId = context.getNotNull(BIND_ENTRANT_ID);
        Long requestTypeId = context.get(BIND_REQUEST_TYPE_ID);
        if(requestTypeId == null)
        {
            return ListOutputBuilder.get(input, Collections.emptyList()).build();
        }

        EnrEntrant enrEntrant = DataAccessServices.dao().getNotNull(entrantId);
        EnrRequestType requestType = DataAccessServices.dao().getNotNull(requestTypeId);
        Set keySet = input.getPrimaryKeys();

        List<ViewWrapper> documents = Lists.newArrayList();

        DQLSelectBuilder baseDocumentBuilder = new DQLSelectBuilder().fromEntity(IEnrEntrantAchievementProofDocument.class, "pr")
                .column(property("pr"))
                .joinEntity("pr", DQLJoinType.left, EnrEntrantBaseDocument.class, "basedoc", eq(property("pr", IEnrEntrantAchievementProofDocumentGen.id()), property("basedoc", EnrEntrantBaseDocument.id())))
                .joinEntity("basedoc", DQLJoinType.left, EnrCampaignEntrantDocument.class, "bdoc", eq(property("basedoc", EnrEntrantBaseDocument.documentType()), property("bdoc", EnrCampaignEntrantDocument.documentType())))
                .joinEntity("pr", DQLJoinType.left, EnrOlympiadDiploma.class, "olymp", eq(property("pr", IEnrEntrantAchievementProofDocumentGen.id()), property("olymp", EnrOlympiadDiploma.id())))
                .joinEntity("olymp", DQLJoinType.left, EnrCampaignEntrantDocument.class, "oldoc", eq(property("olymp", EnrOlympiadDiploma.documentType()), property("oldoc", EnrCampaignEntrantDocument.documentType())))
                .where(not(instanceOf("pr", EnrPersonEduDocumentRel.class)))
                .where(or(
                        and(
                                isNotNull(property("bdoc")),
                                eq(property("bdoc", EnrCampaignEntrantDocument.achievement()), value(true))
                        ),
                        and(
                                isNotNull(property("oldoc")),
                                eq(property("oldoc", EnrCampaignEntrantDocument.achievement()), value(true))
                        )
                ))
                .where(eq(property("pr", EnrEntrantBaseDocument.entrant().id()), value(entrantId)));

        DQLSelectBuilder eduDocumentBuilder = new DQLSelectBuilder().fromEntity(PersonEduDocument.class, "edu")
                .where(eq(property("edu", PersonEduDocument.person()), value(enrEntrant.getPerson())));
        if(EnrRequestTypeCodes.BS.equals(requestType.getCode()))
        {
            eduDocumentBuilder.where(or(
                        and(
                                eq(property("edu", PersonEduDocument.eduLevel().code()), value(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE)),
                                or(
                                        eq(property("edu", PersonEduDocument.graduationHonour().code()), value(GraduationHonourCodes.GOLD_MEDAL)),
                                        eq(property("edu", PersonEduDocument.graduationHonour().code()), value(GraduationHonourCodes.SILVER_MEDAL)),
                                        eq(property("edu", PersonEduDocument.graduationHonour().code()), value(GraduationHonourCodes.RED_DIPLOMA))
                                )
                        ),
                        and(
                                eq(property("edu", PersonEduDocument.eduLevel().code()), value(EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE)),
                                eq(property("edu", PersonEduDocument.graduationHonour().code()), value(GraduationHonourCodes.RED_DIPLOMA))
                        )
                ));
        }
        else
        {
            eduDocumentBuilder.where(exists(new DQLSelectBuilder()
                    .fromEntity(GraduationHonour2EduDocumentKind.class, "dk")
                    .where(eq(property("dk", GraduationHonour2EduDocumentKind.documentKind()), property("edu", PersonEduDocument.eduDocumentKind())))
                    .where(eq(property("dk", GraduationHonour2EduDocumentKind.graduationHonour()), property("edu", PersonEduDocument.graduationHonour())))
                    .buildQuery()));
        }

        if (CollectionUtils.isNotEmpty(keySet))
            baseDocumentBuilder.where(in(property("pr.id"), keySet));
        if (CollectionUtils.isNotEmpty(keySet))
            eduDocumentBuilder.where(in(property("edu.id"), keySet));

        List<IEnrEntrantAchievementProofDocument> achievementProofDocuments = baseDocumentBuilder.createStatement(context.getSession()).list();
        List<PersonEduDocument> eduDocuments = eduDocumentBuilder.createStatement(context.getSession()).list();

        for (IEnrEntrantAchievementProofDocument document : achievementProofDocuments)
        {
            documents.add(new EnrEntrantDocumentViewWrapper((EnrEntrantBaseDocument) document));
//            if(document instanceof EnrOlympiadDiploma)
//            {
//                documents.add(new EnrEntrantDocumentViewWrapper((EnrOlympiadDiploma) document));
//            }
//            else if(document instanceof EnrEntrantBaseDocument)
//            {
//
//            }
        }

        for (PersonEduDocument document : eduDocuments)
        {
            documents.add(new EnrEntrantDocumentViewWrapper(document));
        }
        return ListOutputBuilder.get(input, documents).build();
    }
}
