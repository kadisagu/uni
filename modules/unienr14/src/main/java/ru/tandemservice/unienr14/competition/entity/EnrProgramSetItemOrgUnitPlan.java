package ru.tandemservice.unienr14.competition.entity;

import ru.tandemservice.unienr14.competition.entity.gen.*;

/**
 * План приема по ОП в наборе ОП
 */
public class EnrProgramSetItemOrgUnitPlan extends EnrProgramSetItemOrgUnitPlanGen
{
    public EnrProgramSetItemOrgUnitPlan()
    {
    }

    public EnrProgramSetItemOrgUnitPlan(EnrProgramSetItem programSetItem, EnrProgramSetOrgUnit programSetOrgUnit)
    {
        this.setProgramSetItem(programSetItem);
        this.setProgramSetOrgUnit(programSetOrgUnit);
    }

    @Override
    public String getTitle()
    {
        if (getProgramSet() == null) {
            return this.getClass().getSimpleName();
        }
        return getProgramSet().getTitle() + " (" + getOrgUnit().getDepartmentTitle() + ", " + getProgram().getTitle() + ")";
    }

    @Override
    public String getProgramTitle()
    {
        return getProgram().getTitle();
    }
}