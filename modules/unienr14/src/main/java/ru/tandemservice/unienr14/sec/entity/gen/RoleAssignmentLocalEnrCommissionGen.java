package ru.tandemservice.unienr14.sec.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import org.tandemframework.shared.organization.sec.entity.IRoleAssignment;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Назначение локальной роли на ОК принципалу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RoleAssignmentLocalEnrCommissionGen extends EntityBase
 implements IRoleAssignment, INaturalIdentifiable<RoleAssignmentLocalEnrCommissionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission";
    public static final String ENTITY_NAME = "roleAssignmentLocalEnrCommission";
    public static final int VERSION_HASH = -2029459078;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROLE_CONFIG = "roleConfig";
    public static final String L_PRINCIPAL_CONTEXT = "principalContext";

    private RoleConfigLocalEnrCommission _roleConfig;     // Конфигурация локальной роли для ОК
    private IPrincipalContext _principalContext;     // Контекст принципала

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Конфигурация локальной роли для ОК. Свойство не может быть null.
     */
    @NotNull
    public RoleConfigLocalEnrCommission getRoleConfig()
    {
        return _roleConfig;
    }

    /**
     * @param roleConfig Конфигурация локальной роли для ОК. Свойство не может быть null.
     */
    public void setRoleConfig(RoleConfigLocalEnrCommission roleConfig)
    {
        dirty(_roleConfig, roleConfig);
        _roleConfig = roleConfig;
    }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     */
    @NotNull
    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    /**
     * @param principalContext Контекст принципала. Свойство не может быть null.
     */
    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && principalContext!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  principalContext instanceof IEntity ? EntityRuntime.getMeta((IEntity) principalContext) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_principalContext, principalContext);
        _principalContext = principalContext;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RoleAssignmentLocalEnrCommissionGen)
        {
            if (withNaturalIdProperties)
            {
                setRoleConfig(((RoleAssignmentLocalEnrCommission)another).getRoleConfig());
                setPrincipalContext(((RoleAssignmentLocalEnrCommission)another).getPrincipalContext());
            }
        }
    }

    public INaturalId<RoleAssignmentLocalEnrCommissionGen> getNaturalId()
    {
        return new NaturalId(getRoleConfig(), getPrincipalContext());
    }

    public static class NaturalId extends NaturalIdBase<RoleAssignmentLocalEnrCommissionGen>
    {
        private static final String PROXY_NAME = "RoleAssignmentLocalEnrCommissionNaturalProxy";

        private Long _roleConfig;
        private Long _principalContext;

        public NaturalId()
        {}

        public NaturalId(RoleConfigLocalEnrCommission roleConfig, IPrincipalContext principalContext)
        {
            _roleConfig = ((IEntity) roleConfig).getId();
            _principalContext = ((IEntity) principalContext).getId();
        }

        public Long getRoleConfig()
        {
            return _roleConfig;
        }

        public void setRoleConfig(Long roleConfig)
        {
            _roleConfig = roleConfig;
        }

        public Long getPrincipalContext()
        {
            return _principalContext;
        }

        public void setPrincipalContext(Long principalContext)
        {
            _principalContext = principalContext;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof RoleAssignmentLocalEnrCommissionGen.NaturalId) ) return false;

            RoleAssignmentLocalEnrCommissionGen.NaturalId that = (NaturalId) o;

            if( !equals(getRoleConfig(), that.getRoleConfig()) ) return false;
            if( !equals(getPrincipalContext(), that.getPrincipalContext()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRoleConfig());
            result = hashCode(result, getPrincipalContext());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRoleConfig());
            sb.append("/");
            sb.append(getPrincipalContext());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RoleAssignmentLocalEnrCommissionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RoleAssignmentLocalEnrCommission.class;
        }

        public T newInstance()
        {
            return (T) new RoleAssignmentLocalEnrCommission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "roleConfig":
                    return obj.getRoleConfig();
                case "principalContext":
                    return obj.getPrincipalContext();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "roleConfig":
                    obj.setRoleConfig((RoleConfigLocalEnrCommission) value);
                    return;
                case "principalContext":
                    obj.setPrincipalContext((IPrincipalContext) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "roleConfig":
                        return true;
                case "principalContext":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "roleConfig":
                    return true;
                case "principalContext":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "roleConfig":
                    return RoleConfigLocalEnrCommission.class;
                case "principalContext":
                    return IPrincipalContext.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RoleAssignmentLocalEnrCommission> _dslPath = new Path<RoleAssignmentLocalEnrCommission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RoleAssignmentLocalEnrCommission");
    }
            

    /**
     * @return Конфигурация локальной роли для ОК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission#getRoleConfig()
     */
    public static RoleConfigLocalEnrCommission.Path<RoleConfigLocalEnrCommission> roleConfig()
    {
        return _dslPath.roleConfig();
    }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission#getPrincipalContext()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> principalContext()
    {
        return _dslPath.principalContext();
    }

    public static class Path<E extends RoleAssignmentLocalEnrCommission> extends EntityPath<E>
    {
        private RoleConfigLocalEnrCommission.Path<RoleConfigLocalEnrCommission> _roleConfig;
        private IPrincipalContextGen.Path<IPrincipalContext> _principalContext;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Конфигурация локальной роли для ОК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission#getRoleConfig()
     */
        public RoleConfigLocalEnrCommission.Path<RoleConfigLocalEnrCommission> roleConfig()
        {
            if(_roleConfig == null )
                _roleConfig = new RoleConfigLocalEnrCommission.Path<RoleConfigLocalEnrCommission>(L_ROLE_CONFIG, this);
            return _roleConfig;
        }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission#getPrincipalContext()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> principalContext()
        {
            if(_principalContext == null )
                _principalContext = new IPrincipalContextGen.Path<IPrincipalContext>(L_PRINCIPAL_CONTEXT, this);
            return _principalContext;
        }

        public Class getEntityClass()
        {
            return RoleAssignmentLocalEnrCommission.class;
        }

        public String getEntityName()
        {
            return "roleAssignmentLocalEnrCommission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
