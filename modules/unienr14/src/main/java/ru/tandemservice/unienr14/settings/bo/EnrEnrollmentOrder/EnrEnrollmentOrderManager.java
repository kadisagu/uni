/**
 *$Id: EnrEnrollmentOrderManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic.*;

/**
 * @author Alexander Shaburov
 * @since 19.06.13
 */
@Configuration
public class EnrEnrollmentOrderManager extends BusinessObjectManager
{
    public static EnrEnrollmentOrderManager instance()
    {
        return instance(EnrEnrollmentOrderManager.class);
    }

    @Bean
    public IEnrOrderPrintFormTypeDao orderTypeDao()
    {
        return new EnrOrderPrintFormTypeDao();
    }

    @Bean
    public IEnrEnrollmentOrderBasicDao basicDao()
    {
        return new EnrEnrollmentOrderBasicDao();
    }

    @Bean
    public IEnrEnrollmentOrderVisaTemplate visaTemplateDao()
    {
        return new EnrEnrollmentOrderVisaTemplateDao();
    }
}
