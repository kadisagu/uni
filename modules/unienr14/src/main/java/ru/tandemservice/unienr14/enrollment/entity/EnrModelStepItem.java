package ru.tandemservice.unienr14.enrollment.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.gen.*;

/** @see ru.tandemservice.unienr14.enrollment.entity.gen.EnrModelStepItemGen */
public class EnrModelStepItem extends EnrModelStepItemGen implements IEnrEnrollmentStepItem, IEntityDebugTitled, ITitled
{
    @Override
    public boolean isEntrantArchived()
    {
        return getEntity().getEntrant().isArchival();
    }

    @Override
    public boolean isEntrantRequestTakeAwayDocuments()
    {
        return getEntity().getRequestedCompetition().getRequest().isTakeAwayDocument();
    }

    @Override
    public boolean isRecommended()
    {
        return false;
    }

    @Override
    public boolean isShouldBeEnrolled()
    {
        return isEnrolled();
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getEntity().getRequestedCompetition().getEntityDebugTitle() + ", шаг " + getStep().getDateStr();
    }

    @Override
    public String getTitle()
    {
        return getEntityDebugTitle();
    }
}