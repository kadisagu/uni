/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;

/**
 * @author oleyba
 * @since 3/24/14
 */
@Configuration
public class EnrCampaignEnrollmentStageAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
            .addDataSource(selectDS("competitionTypeDS", competitionTypeDSHandler()).addColumn(EnrCompetitionType.title().s()))
            .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> competitionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetitionType.class)
        .order(EnrCompetitionType.code())
        .filter(EnrCompetitionType.title());
    }

}