package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import java.util.Collection;
import java.util.Set;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.IEnrStateExamResultMark;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author vdanilov
 */
public interface IEnrCompetitionStateExamFilterDao extends INeedPersistenceSupport {

    /**
     * Формирует перечень конкурсов бакалавриата и специалитета, покрытых результатами ЕГЭ
     * @param ec - ПК (конкурсы будут взяты из нее)
     * @param stateExamResultList результаты ЕГЭ (по ним будет проверяться покрытие)
     * @return { enrCompetition.id }, как правило, их очень много
     */
    Set<Long> getFilteredBSCompetitions(EnrEnrollmentCampaign ec, Collection<IEnrStateExamResultMark> stateExamResultList);

    /**
     * Формирует перечень конкурсов бакалавриата и специалитета, покрытых результатами ЕГЭ
     * @return { enrCompetition.id }, как правило, их очень много
     * @see IEnrCompetitionStateExamFilterDao#getFilteredBSCompetitions(EnrEnrollmentCampaign, Collection)
     */
    Set<Long> getFilteredBSCompetitions(EnrEntrant entrant);

    /**
     * @param competitionIds { enrCompetition.id }
     * @return distinct { enrCompetition.programSetOrgUnit.programSet.programForm.id }, форм существенно меньше, чем конкурсов
     */
    Set<Long> getEduProgramFormIds(Collection<Long> competitionIds);

    /**
     * @param competitionIds { enrCompetition.id }
     * @return distinct { enrCompetition.programSetOrgUnit.orgUnit.id }, подразделений существенно меньше, чем конкурсов
     */
    Set<Long> getProgramSetOrgUnitOuIds(Collection<Long> competitionIds);

    /**
     * @param competitionIds { enrCompetition.id }
     * @return distinct { enrCompetition.programSetOrgUnit.formativeOrgUnit.id }, подразделений существенно меньше, чем конкурсов
     */
    Set<Long> getProgramSetOrgUnitFormativeOuIds(Collection<Long> competitionIds);

    /**
     * @param competitionIds { enrCompetition.id }
     * @return distinct { enrCompetition.programSetOrgUnit.programSet.programSubject.id }, направлений существенно меньше, чем конкурсов
     */
    Set<Long> getEduProgramSubjectIds(Collection<Long> competitionIds);

    /**
     * @param competitionIds { enrCompetition.id }
     * @return distinct { enrCompetition.type.id }, типов существенно меньше, чем конкурсов
     */
    Set<Long> getCompetitionTypeIds(Collection<Long> competitionIds);

    /**
     * @param competitionIds { enrCompetition.id }
     * @return distinct { enrCompetition.eduLevelRequirement.id }
     */
    Set<Long> getCompetitionEduLevelRequirementIds(Collection<Long> competitionIds);
}
