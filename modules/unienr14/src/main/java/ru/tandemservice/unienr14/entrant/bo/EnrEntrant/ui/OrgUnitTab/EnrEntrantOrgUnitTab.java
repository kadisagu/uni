/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitList.EnrEntrantOrgUnitList;

/**
 * @author nvankov
 * @since 9/12/14
 */
@Configuration
public class EnrEntrantOrgUnitTab extends BusinessComponentManager
{
    public static final String ENR14_TAB_PANEL = "enr14TabPanel";

    public static final String ENR_ENTRANTS_TAB = "enr14EntrantsTab";
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public TabPanelExtPoint enr14TabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(ENR14_TAB_PANEL)
                .addTab(componentTab(ENR_ENTRANTS_TAB, EnrEntrantOrgUnitList.class).permissionKey("ui:secModel.orgUnit_viewEnr14EntrantsTab").securedObject("ui:orgUnit"))
                .create();
    }

}



    