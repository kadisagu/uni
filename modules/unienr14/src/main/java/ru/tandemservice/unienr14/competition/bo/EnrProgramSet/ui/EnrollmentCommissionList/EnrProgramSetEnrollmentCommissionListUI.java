/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList;

import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionEdit.EnrProgramSetEnrollmentCommissionEdit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionEdit.EnrProgramSetEnrollmentCommissionEditUI;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 02.06.2015
 */
public class EnrProgramSetEnrollmentCommissionListUI extends UIPresenter
{
    public static final String PARAM_REQUEST_TYPE = "requestType";
    public static final String PARAM_CHECK_PS_ENROLLMENT_COMMISSION = "checkProgramSetEnrollmentCommission";
    public static final String PARAM_PROGRAM_FORMS = "programForms";
    public static final String PARAM_ORG_UNITS = "orgUnits";
    public static final String PARAM_FORMATIVE_ORG_UNITS = "formativeOrgUnits";
    public static final String PARAM_PROGRAM_SUBJECTS = "programSubjects";
    public static final String PARAM_ENROLLMENT_COMMISSIONS = "enrollmentCommissions";
    public static final String PARAM_SUBJECT_CODE = "subjectCode";
    public static final String PARAM_PROGRAM_SET_TITLE = "programSetTitle";

    public static final String PARAM_CURRENT_ENROLLMENT_COMMISSION = "currentEnrollmentCommission";

    private EnrProgramSetOrgUnit _currentEditProgramSetOrgUnit;
    private EnrEnrollmentCommission _currentEditEnrollmentCommission;
    private Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> _orgUnit2SetECMap = Maps.newHashMap();
    private boolean _checkProgramSetEnrollmentCommission;

    @Override
    public void onComponentRefresh()
    {
        onRefreshValues();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getCampaign());
        dataSource.put(PARAM_REQUEST_TYPE, getRequestType());
        dataSource.put(PARAM_CHECK_PS_ENROLLMENT_COMMISSION, isCheckProgramSetEnrollmentCommission());
        dataSource.put(PARAM_PROGRAM_FORMS, getSettings().get(PARAM_PROGRAM_FORMS));
        dataSource.put(PARAM_ORG_UNITS, getSettings().get(PARAM_ORG_UNITS));
        dataSource.put(PARAM_FORMATIVE_ORG_UNITS, getSettings().get(PARAM_FORMATIVE_ORG_UNITS));
        dataSource.put(PARAM_PROGRAM_SUBJECTS, getSettings().get(PARAM_PROGRAM_SUBJECTS));
        dataSource.put(PARAM_ENROLLMENT_COMMISSIONS, getSettings().get(PARAM_ENROLLMENT_COMMISSIONS));
        dataSource.put(PARAM_SUBJECT_CODE, getSettings().get(PARAM_SUBJECT_CODE));
        dataSource.put(PARAM_PROGRAM_SET_TITLE, getSettings().get(PARAM_PROGRAM_SET_TITLE));

        if (dataSource.getName().equals(EnrProgramSetEnrollmentCommissionList.BLOCK_ENROLLMENT_COMMISSION_DS))
        {
            dataSource.put(PARAM_CURRENT_ENROLLMENT_COMMISSION, getCurrentEnrollmentCommission());
        }
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(EnrProgramSetEnrollmentCommissionList.PROGRAM_SET_ENROLLMENT_COMMISSION_DS).getOptionColumnSelectedObjects("check");
            selected.clear();
        }
    }

    // Listeners

    public void onClickEditEnrollmentCommission()
    {
        Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(EnrProgramSetEnrollmentCommissionList.PROGRAM_SET_ENROLLMENT_COMMISSION_DS).getOptionColumnSelectedObjects("check");
        if (selected.isEmpty())
            throw new ApplicationException("Выберите элементы списка для массового действия.");

        _uiActivation.asRegionDialog(EnrProgramSetEnrollmentCommissionEdit.class)
                .parameters(new ParametersMap()
                                    .add(EnrProgramSetEnrollmentCommissionEditUI.PARAM_CAMPAIGN, getCampaign())
                                    .add(EnrProgramSetEnrollmentCommissionEditUI.PARAM_REQUEST_TYPE, getRequestType())
                                    .add(EnrProgramSetEnrollmentCommissionEditUI.PARAM_PROGRAM_SET_ORG_UNIT_IDS, UniBaseDao.ids(selected)))
                .activate();
        selected.clear();
    }
    
    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getCampaign());
        onClickClear();
        onRefreshValues();
    }

    public void onChangeRequestType()
    {
        onClickClear();
        onRefreshValues();
    }

    public void onClickClear()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, PARAM_REQUEST_TYPE);
        _checkProgramSetEnrollmentCommission = false;
    }

    public void onClickEdit()
    {
        _currentEditProgramSetOrgUnit = getConfig().getDataSource(EnrProgramSetEnrollmentCommissionList.PROGRAM_SET_ENROLLMENT_COMMISSION_DS).getRecordById(getListenerParameterAsLong());

        EnrProgramSetEnrollmentCommission commission = getOrgUnit2SetECMap().get(_currentEditProgramSetOrgUnit);
        if (null != commission)
            _currentEditEnrollmentCommission = commission.getEnrollmentCommission();
    }

    public void onDeleteEntityFromList()
    {
        EnrProgramSetOrgUnit currentOrgUnit = getConfig().getDataSource(EnrProgramSetEnrollmentCommissionList.PROGRAM_SET_ENROLLMENT_COMMISSION_DS).getRecordById(getListenerParameterAsLong());
        EnrProgramSetEnrollmentCommission commission = getOrgUnit2SetECMap().get(currentOrgUnit);
        DataAccessServices.dao().delete(commission);
        onRefreshValues();
    }

    public void onClickSaveRow()
    {
        EnrEnrollmentCommissionManager.instance().dao().saveProgramSetEnrollmentCommission(getOrgUnit2SetECMap(), _currentEditProgramSetOrgUnit, _currentEditEnrollmentCommission);
        onRefreshValues();
    }

    public void onClickCancelEdit()
    {
        onRefreshValues();
    }

    public void onRefreshValues()
    {
        _orgUnit2SetECMap = EnrEnrollmentCommissionManager.instance().dao().getProgramSetOrgUnit2EnrollmentCommissionMap(getCampaign(), getRequestType());
        _currentEditProgramSetOrgUnit = null;
        _currentEditEnrollmentCommission = null;
    }

    // Utils

    public boolean isNothingSelected()
    {
        return getCampaign() == null || getRequestType() == null;
    }

    public boolean isEditMode()
    {
        return null != getCurrentEditProgramSetOrgUnit();
    }

    public boolean isProgramSetOrgUnitInEditMode()
    {
        return null != getCurrentEditProgramSetOrgUnit() && getCurrentProgramSetOrgUnit().getId().equals(getCurrentEditProgramSetOrgUnit().getId());
    }

    public boolean isDisabledDeleteEC()
    {
        return null == getOrgUnit2SetECMap().get(getCurrentProgramSetOrgUnit());
    }

    // Getters && Setters

    public EnrProgramSetOrgUnit getCurrentProgramSetOrgUnit()
    {
        return getSupport().getDataSourceCurrentValue(EnrProgramSetEnrollmentCommissionList.PROGRAM_SET_ENROLLMENT_COMMISSION_DS);
    }

    public EnrEnrollmentCommission getCurrentEnrollmentCommission()
    {
        Long orgUnitId = getCurrentProgramSetOrgUnit().getId();
        EnrProgramSetEnrollmentCommission commission = getOrgUnit2SetECMap().get(getCurrentProgramSetOrgUnit());
        return null != commission && orgUnitId.equals(commission.getProgramSetOrgUnit().getId()) ? commission.getEnrollmentCommission() : null;
    }

    public EnrEnrollmentCampaign getCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public EnrRequestType getRequestType()
    {
        return getSettings().get(PARAM_REQUEST_TYPE);
    }

    public EnrProgramSetOrgUnit getCurrentEditProgramSetOrgUnit()
    {
        return _currentEditProgramSetOrgUnit;
    }

    public void setCurrentEditProgramSetOrgUnit(EnrProgramSetOrgUnit currentEditProgramSetOrgUnit)
    {
        _currentEditProgramSetOrgUnit = currentEditProgramSetOrgUnit;
    }

    public EnrEnrollmentCommission getCurrentEditEnrollmentCommission()
    {
        return _currentEditEnrollmentCommission;
    }

    public void setCurrentEditEnrollmentCommission(EnrEnrollmentCommission currentEditEnrollmentCommission)
    {
        _currentEditEnrollmentCommission = currentEditEnrollmentCommission;
    }

    public Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> getOrgUnit2SetECMap()
    {
        return _orgUnit2SetECMap;
    }

    public void setOrgUnit2SetECMap(Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> orgUnit2SetECMap)
    {
        _orgUnit2SetECMap = orgUnit2SetECMap;
    }

    public boolean isCheckProgramSetEnrollmentCommission()
    {
        return _checkProgramSetEnrollmentCommission;
    }

    public void setCheckProgramSetEnrollmentCommission(boolean checkProgramSetEnrollmentCommission)
    {
        _checkProgramSetEnrollmentCommission = checkProgramSetEnrollmentCommission;
    }
}