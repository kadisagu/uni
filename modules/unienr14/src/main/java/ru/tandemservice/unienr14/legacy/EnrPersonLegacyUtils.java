/* $Id:$ */
package ru.tandemservice.unienr14.legacy;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;

/**
 * Класс для утилей интеграции с легаси компонентами в персоне,
 * типа карточек связанных с персоной документов
 *
 * @author oleyba
 * @since 5/4/13
 */
public class EnrPersonLegacyUtils
{
    /**
     * ресолвер для колонок со ссылками на карточки
     * @param context контекст проверки прав ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils#getSecureRoleContext
     * @return ресолвер
     */
    public static DefaultPublisherLinkResolver personRelatedStuffLinkResolver(final ISecureRoleContext context)
    {
        return new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                if(entity instanceof EnrEntrantDocumentViewWrapper && ((EnrEntrantDocumentViewWrapper) entity).getEntity() instanceof PersonDocument)
                {
                    return PersonDocumentUtil.getPub(((PersonDocument) ((EnrEntrantDocumentViewWrapper) entity).getEntity()).getDocumentType().getCode()).getSimpleName();
                }

                return super.getComponentName(entity);
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                return getParametersMap(entity, context);
            }
        };
    }

    public static DefaultPublisherLinkResolver personRelatedStuffLinkResolver(final EnrEntrant entrant) {
        return personRelatedStuffLinkResolver(getSecureRoleContext(entrant));
    }

    /**
     * @return Контекст нужен для компонентов работы с перс. данными
     * @param entrant абитуриент
     */
    public static ISecureRoleContext getSecureRoleContext(EnrEntrant entrant)
    {
        return SecureRoleContext.instance(entrant).hasBirthDateCheck(true).accessible(entrant.isAccessible());
    }

    /**
     * @return Набор параметров для перехода на карточку
     * @param entity сущность, для которой нужно открыть карточку
     * @param context контекст проверки прав ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils#getSecureRoleContext
     */
    public static ParametersMap getParametersMap(IEntity entity, ISecureRoleContext context)
    {
        ParametersMap map = new ParametersMap();
        if(entity instanceof EnrEntrantBaseDocument)
        {
            map.add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrEntrantBaseDocument) entity).getDocRelation().getDocument().getId());
        }
        else
        {
            map.add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
        }
        map.add("securedObjectId", context.getSecuredObject().getId());
        map.add("accessible", context.isAccessible());
        map.add("personRoleName", context.getPersonRoleName());
        map.add("personRoleModel", context); // без этой штуки не открывается публикатор id-карточки (оно там обязательное)
        return map;
    }
}
