// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.unienr14.ws.entrantrating;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Vasily Zhukov
 * @since 24.03.2011
 */
@WebService
public class EnrEntrantRatingService
{
    /**
     * Получение рейтинга абитуриентов по всем конкурсам
     *
     * @param enrollmentCampaignTitle Название приемной кампании
     * @return Рейтинг абитуриентов
     */
    public EnrEntrantRatingEnvironmentNode getEntrantRatingEnvironment(
            @WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle)
    {
        return IEnrEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(enrollmentCampaignTitle);
    }

    /**
     * Получение рейтинга абитуриентов по выбранному направлению подготовки (специальности)
     *
     * @param enrollmentCampaignTitle название приемной кампании
     * @param competitionId   Идентификатор конкурса
     * @return Рейтинг абитуриентов
     */
    public EnrEntrantRatingEnvironmentNode getEntrantRatingEnvironmentForEnrollmentDirection(
            @WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle,
            @WebParam(name = "competitionId") String competitionId)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        if (competitionId == null)
            throw new RuntimeException("WebParam 'enrollmentDirectionId' is not specified!");

        return IEnrEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(enrollmentCampaignTitle,competitionId);
    }
}
