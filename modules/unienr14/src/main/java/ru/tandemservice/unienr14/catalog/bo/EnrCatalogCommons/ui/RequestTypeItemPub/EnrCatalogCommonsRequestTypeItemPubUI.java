/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.ui.RequestTypeItemPub;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemPub.CatalogDynamicItemPubUI;

/**
 * @author Nikolay Fedorovskih
 * @since 02.04.2015
 */
public class EnrCatalogCommonsRequestTypeItemPubUI extends CatalogDynamicItemPubUI
{
    public static final String REQUEST_TYPE_ID_FILTER_PROPERTY = "itemId";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(REQUEST_TYPE_ID_FILTER_PROPERTY, getCatalogItem().getId());
    }
}