package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x4_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {
        // свойство refusedToBeEnrolled стало формулой
        if (!tool.columnExists("enr14_requested_comp_t", "refusedtobeenrolled_p")) {
            // удалить колонку
            tool.dropColumn("enr14_requested_comp_t", "refusedtobeenrolled_p");
        }

        if (!tool.columnExists("enr14_requested_comp_t", "refuseddate_p")) {
            // создать колонку
            tool.createColumn("enr14_requested_comp_t", new DBColumn("refuseddate_p", DBType.TIMESTAMP));

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_requested_comp_t")
                    .set("refuseddate_p", "accepteddate_p")
                    .where("refusedtobeenrolled_p=?")
                    .where("accepteddate_p is not null");

            tool.executeUpdate(translator.toSql(updateQuery), Boolean.TRUE);

            updateQuery = new SQLUpdateQuery("enr14_requested_comp_t")
                    .set("refuseddate_p", "regdate_p")
                    .where("refusedtobeenrolled_p=?")
                    .where("accepteddate_p is null");
            tool.prepareStatement(translator.toSql(updateQuery));
            tool.executeUpdate(translator.toSql(updateQuery), Boolean.TRUE);
        }
    }
}