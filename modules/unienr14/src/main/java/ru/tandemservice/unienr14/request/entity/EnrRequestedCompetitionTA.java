package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.gen.EnrRequestedCompetitionTAGen;

/**
 * Выбранный конкурс по ЦП
 */
public class EnrRequestedCompetitionTA extends EnrRequestedCompetitionTAGen
{
    public EnrRequestedCompetitionTA()
    {
    }

    public EnrRequestedCompetitionTA(EnrEntrantRequest request, EnrCompetition competition)
    {
        setRequest(request);
        setCompetition(competition);
    }

    @Override
    @EntityDSLSupport
    public String getParametersTitle()
    {
        StringBuilder contractStr = new StringBuilder();
        if (getContractNumber() != null || getContractDate() != null)
        {
            contractStr.append(", договор");
            if (getContractNumber() != null)
                contractStr.append(" №").append(getContractNumber());

            if (getContractDate() != null)
                contractStr.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractDate()));
        }

        return "ЦП: " + getTargetAdmissionKind().getTitle() + ", " + getTargetAdmissionOrgUnit().getTitleWithLegalForm() + contractStr.toString();
    }
}