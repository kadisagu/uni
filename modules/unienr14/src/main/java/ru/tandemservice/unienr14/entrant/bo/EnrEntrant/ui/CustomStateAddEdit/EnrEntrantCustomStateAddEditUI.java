/* $Id: EcEntrantCustomStateAddEditUI.java 26849 2013-04-05 09:59:31Z nvankov $ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.CustomStateAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;

import java.util.Date;

/**
 * @author nvankov
 * @since 4/3/13
 */
@Input ({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrantCustomStateId"),
    @Bind(key = "entrantId", binding = "entrantId")
})
public class EnrEntrantCustomStateAddEditUI extends UIPresenter
{
    private Long _entrantId;
    private Long _entrantCustomStateId;
    private EnrEntrantCustomState _entrantCustomState;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public Long getEntrantCustomStateId()
    {
        return _entrantCustomStateId;
    }

    public void setEntrantCustomStateId(Long entrantCustomStateId)
    {
        _entrantCustomStateId = entrantCustomStateId;
    }

    public EnrEntrantCustomState getEntrantCustomState()
    {
        return _entrantCustomState;
    }

    public void setEntrantCustomState(EnrEntrantCustomState EnrEntrantCustomState)
    {
        _entrantCustomState = EnrEntrantCustomState;
    }

    @Override
    public void onComponentRefresh()
    {
        if(isEditForm())
            _entrantCustomState = DataAccessServices.dao().get(_entrantCustomStateId);
        else
        {
            _entrantCustomState = new EnrEntrantCustomState();
            _entrantCustomState.setEntrant(DataAccessServices.dao().<EnrEntrant>get(_entrantId));
        }
    }

    public boolean isAddForm()
    {
        return null == _entrantCustomStateId;
    }

    public boolean isEditForm()
    {
        return !isAddForm();
    }

    public String getSticker()
    {
        if(isAddForm())
        {
            return _uiConfig.getProperty("ui.sticker.add");
        }
        else
        {
            return _uiConfig.getProperty("ui.sticker.edit");
        }
    }

    public void onClickApply()
    {
        Date beginDate = _entrantCustomState.getBeginDate();
        Date endDate = _entrantCustomState.getEndDate();
        if(null != beginDate && null != endDate) {
            if(beginDate.after(endDate)) {
                _uiSupport.error("«Дата начала действия статуса» должна быть не больше «Дата окончания действия статуса»", "beginDate", "endDate");
            }
        }
        if(!getUserContext().getErrorCollector().hasErrors()) {
            EnrEntrantManager.instance().dao().saveOrUpdateCustomState(_entrantCustomState);
            deactivate();
        }
    }
}
