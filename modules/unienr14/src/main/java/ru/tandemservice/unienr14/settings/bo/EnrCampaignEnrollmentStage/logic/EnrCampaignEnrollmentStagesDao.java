/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Calendar;
import java.util.Date;

/**
 * @author oleyba
 * @since 3/17/14
 */
public class EnrCampaignEnrollmentStagesDao extends UniBaseDao implements IEnrCampaignEnrollmentStagesDao
{
    @Override
    public void doAddDefaultDates(EnrEnrollmentCampaign enrEnrollmentCampaign)
    {
        if (enrEnrollmentCampaign.getEducationYear().getIntValue()!= 2016 )
            throw new ApplicationException("Установленные даты могут быть заполнены только для приемной кампании 2016/2017 года.");

        EnrRequestType requestType = getCatalogItem(EnrRequestType.class, EnrRequestTypeCodes.BS);
        Calendar calendar = Calendar.getInstance();

        calendar.set(2016, Calendar.JULY, 29);
        Date enrollmentDate = CommonBaseDateUtil.trimTime(calendar.getTime());
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.EXCLUSIVE),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.EXCLUSIVE),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.TARGET_ADMISSION),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.TARGET_ADMISSION),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
            enrollmentDate, false);

        calendar.set(2016, Calendar.AUGUST, 3);
        enrollmentDate = CommonBaseDateUtil.trimTime(calendar.getTime());
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
            enrollmentDate, false);


        calendar.set(2016, Calendar.AUGUST, 8);
        enrollmentDate = CommonBaseDateUtil.trimTime(calendar.getTime());
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
            enrollmentDate, false);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
            getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
            getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
            enrollmentDate, false);


        calendar.set(2016, Calendar.JULY, 19);
        enrollmentDate = CommonBaseDateUtil.trimTime(calendar.getTime());
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.EXCLUSIVE),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.EXCLUSIVE),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.TARGET_ADMISSION),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.TARGET_ADMISSION),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
                              enrollmentDate, true);

        calendar.set(2016, Calendar.JULY, 22);
        enrollmentDate = CommonBaseDateUtil.trimTime(calendar.getTime());
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
                              enrollmentDate, true);


        calendar.set(2016, Calendar.JULY, 27);
        enrollmentDate = CommonBaseDateUtil.trimTime(calendar.getTime());
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNAYA),
                              enrollmentDate, true);
        doSaveEnrollmentStage(enrEnrollmentCampaign, requestType,
                              getCatalogItem(EnrCompetitionType.class, EnrCompetitionTypeCodes.MINISTERIAL),
                              getCatalogItem(EduProgramForm.class, EduProgramFormCodes.OCHNO_ZAOCHNAYA),
                              enrollmentDate, true);

    }

    public void doSaveEnrollmentStage(EnrEnrollmentCampaign enrollmentCampaign, EnrRequestType requestType, EnrCompetitionType competitionType, EduProgramForm programForm, Date enrollmentDate, boolean isCrimea)
    {
        EnrCampaignEnrollmentStage stage = new EnrCampaignEnrollmentStage();
        stage.setEnrollmentCampaign(enrollmentCampaign);
        stage.setRequestType(requestType);
        stage.setCompetitionType(competitionType);
        stage.setProgramForm(programForm);
        stage.setDate(enrollmentDate);
        stage.setAcceptPeopleResidingInCrimea(isCrimea);
        EnrCampaignEnrollmentStage existing = getByNaturalId(stage.getNaturalId());
        if (existing == null) {
            save(stage);
        }
    }
}