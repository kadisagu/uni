/**
 *$Id: EnrExamGroupList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.EnrProgramSetSearchDSHandler;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupListDaysSelectDSHandler;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupListExamSelectDSHandler;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupSearchDSHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
@Configuration
public class EnrExamGroupList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String EXAM_GROUP_SET_SELECT_DS = "examGroupSetSelectDS";
    public static final String DISCIPLINE_SELECT_DS = "disciplineSelectDS";
    public static final String DAYS_SELECT_DS = "daysSelectDS";
    public static final String EXAM_GROUP_SEARCH_DS = "examGroupSearchDS";

    public static final String BIND_ORG_UNIT = "orgUnit"; // OrgUnit

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(selectDS("orgUnitDS", orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addDataSource(selectDS(EXAM_GROUP_SET_SELECT_DS, examGroupSetSelectDSHandler()).addColumn(EnrExamGroupSet.periodTitle().s()))
            .addDataSource(selectDS(DISCIPLINE_SELECT_DS, disciplineSelectDSHandler()))
            .addDataSource(selectDS(DAYS_SELECT_DS, daysSelectDSHandler()))
            .addDataSource(searchListDS(EXAM_GROUP_SEARCH_DS, examGroupSearchDSColumns(), examGroupSearchDSHandler()))
            .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ColumnListExtPoint examGroupSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_GROUP_SEARCH_DS)
                .addColumn(textColumn("title", EnrExamGroup.title()).clickable(true).order().required(true))
                .addColumn(textColumn("discPassFormTitle", EnrExamGroup.disciplineTitle()))
                .addColumn(textColumn("exmaPassDiscSize", EnrExamGroupSearchDSHandler.V_PROP_EXAM_PASS_DISC_SIZE))
                .addColumn(textColumn("event", EnrExamGroupSearchDSHandler.V_PROP_EVENT).formatter(NewLineFormatter.NOBR_IN_LINES))
                .addColumn(textColumn("days", EnrExamGroup.days()))
                .addColumn(booleanColumn("opened", EnrExamGroup.opened())
                    .trueLabel("examGroupSearchDS.opened.open")
                    .falseLabel("examGroupSearchDS.opened.closed"))
                .addColumn(actionColumn("enrPassSheet", new Icon("printer"), "onClickPrintEnrPassSheet").permissionKey("enr14ExamGroupListPrintEnrPassSheet"))
                .addColumn(actionColumn("enrPassSheetCoded", new Icon("printer"), "onClickPrintEnrPassSheetCoded").permissionKey("enr14ExamGroupListPrintEnrPassSheetCoded"))
                .addColumn(actionColumn("enrPassSheetMarks", new Icon("printer"), "onClickPrintEnrPassSheetMarks").permissionKey("enr14ExamGroupListPrintEnrPassSheetMarks"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditExamGroup").permissionKey("enr14ExamGroupListEditExamGroup"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteExamGroup").alert(new FormattedMessage("examGroupSearchDS.delete.alert", EnrExamGroup.title().s())).permissionKey("enr14ExamGroupListDeleteExamGroup"))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSearchDSHandler()
    {
        return new EnrExamGroupSearchDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> daysSelectDSHandler()
    {
        return new EnrExamGroupListDaysSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> disciplineSelectDSHandler()
    {
        return new EnrExamGroupListExamSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSetSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroupSet.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dql = super.query(alias, filter);

                if (StringUtils.isNotEmpty(filter))
                {
                    try
                    {
                        final Date date = new SimpleDateFormat("dd.MM.yyyy").parse(filter);
                        dql.where(or(
                                eq(property(EnrExamGroupSet.beginDate().fromAlias(alias)), value(date, PropertyType.DATE)),
                                eq(property(EnrExamGroupSet.endDate().fromAlias(alias)), value(date, PropertyType.DATE))));
                    }
                    catch (ParseException ignored)
                    {
                    }
                }

                return dql;
            }
        }
                .where(EnrExamGroupSet.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .order(EnrExamGroupSet.beginDate())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder psOuBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                    .column(property("psou", EnrProgramSetOrgUnit.orgUnit().id()))
                    .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))));

                dql.where(in(property(alias, EnrOrgUnit.id()), psOuBuilder.buildQuery()));
            }
        }
            .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }
}
