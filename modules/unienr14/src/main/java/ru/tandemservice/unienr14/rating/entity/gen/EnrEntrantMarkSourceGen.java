package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание балла
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantMarkSourceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource";
    public static final String ENTITY_NAME = "enrEntrantMarkSource";
    public static final int VERSION_HASH = -403841121;
    private static IEntityMeta ENTITY_META;

    public static final String L_CHOSEN_ENTRANCE_EXAM_FORM = "chosenEntranceExamForm";
    public static final String P_MARK_AS_LONG = "markAsLong";
    public static final String P_MARK_TIMESTAMP = "markTimestamp";
    public static final String P_MARK_AS_DOUBLE = "markAsDouble";
    public static final String P_MARK_AS_STRING = "markAsString";

    private EnrChosenEntranceExamForm _chosenEntranceExamForm;     // ВВИ-ф
    private long _markAsLong = 0l;     // Балл
    private Date _markTimestamp;     // Дата изменения балла

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ВВИ-ф.
     */
    public EnrChosenEntranceExamForm getChosenEntranceExamForm()
    {
        return _chosenEntranceExamForm;
    }

    /**
     * @param chosenEntranceExamForm ВВИ-ф.
     */
    public void setChosenEntranceExamForm(EnrChosenEntranceExamForm chosenEntranceExamForm)
    {
        dirty(_chosenEntranceExamForm, chosenEntranceExamForm);
        _chosenEntranceExamForm = chosenEntranceExamForm;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Обновляется демоном IEnrRatingDaemonDao для каждого класса основания отдельно.
     *
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public long getMarkAsLong()
    {
        return _markAsLong;
    }

    /**
     * @param markAsLong Балл. Свойство не может быть null.
     */
    public void setMarkAsLong(long markAsLong)
    {
        dirty(_markAsLong, markAsLong);
        _markAsLong = markAsLong;
    }

    /**
     * Дата изменения значения атрибута markAsLong.
     * Обновляется демоном IEnrRatingDaemonDao при изменении значения в markAsLong.
     *
     * @return Дата изменения балла. Свойство не может быть null.
     */
    @NotNull
    public Date getMarkTimestamp()
    {
        return _markTimestamp;
    }

    /**
     * @param markTimestamp Дата изменения балла. Свойство не может быть null.
     */
    public void setMarkTimestamp(Date markTimestamp)
    {
        dirty(_markTimestamp, markTimestamp);
        _markTimestamp = markTimestamp;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantMarkSourceGen)
        {
            setChosenEntranceExamForm(((EnrEntrantMarkSource)another).getChosenEntranceExamForm());
            setMarkAsLong(((EnrEntrantMarkSource)another).getMarkAsLong());
            setMarkTimestamp(((EnrEntrantMarkSource)another).getMarkTimestamp());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantMarkSourceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantMarkSource.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrEntrantMarkSource is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "chosenEntranceExamForm":
                    return obj.getChosenEntranceExamForm();
                case "markAsLong":
                    return obj.getMarkAsLong();
                case "markTimestamp":
                    return obj.getMarkTimestamp();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "chosenEntranceExamForm":
                    obj.setChosenEntranceExamForm((EnrChosenEntranceExamForm) value);
                    return;
                case "markAsLong":
                    obj.setMarkAsLong((Long) value);
                    return;
                case "markTimestamp":
                    obj.setMarkTimestamp((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "chosenEntranceExamForm":
                        return true;
                case "markAsLong":
                        return true;
                case "markTimestamp":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "chosenEntranceExamForm":
                    return true;
                case "markAsLong":
                    return true;
                case "markTimestamp":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "chosenEntranceExamForm":
                    return EnrChosenEntranceExamForm.class;
                case "markAsLong":
                    return Long.class;
                case "markTimestamp":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantMarkSource> _dslPath = new Path<EnrEntrantMarkSource>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantMarkSource");
    }
            

    /**
     * @return ВВИ-ф.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getChosenEntranceExamForm()
     */
    public static EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm> chosenEntranceExamForm()
    {
        return _dslPath.chosenEntranceExamForm();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Обновляется демоном IEnrRatingDaemonDao для каждого класса основания отдельно.
     *
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkAsLong()
     */
    public static PropertyPath<Long> markAsLong()
    {
        return _dslPath.markAsLong();
    }

    /**
     * Дата изменения значения атрибута markAsLong.
     * Обновляется демоном IEnrRatingDaemonDao при изменении значения в markAsLong.
     *
     * @return Дата изменения балла. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkTimestamp()
     */
    public static PropertyPath<Date> markTimestamp()
    {
        return _dslPath.markTimestamp();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkAsDouble()
     */
    public static SupportedPropertyPath<Double> markAsDouble()
    {
        return _dslPath.markAsDouble();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkAsString()
     */
    public static SupportedPropertyPath<String> markAsString()
    {
        return _dslPath.markAsString();
    }

    public static class Path<E extends EnrEntrantMarkSource> extends EntityPath<E>
    {
        private EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm> _chosenEntranceExamForm;
        private PropertyPath<Long> _markAsLong;
        private PropertyPath<Date> _markTimestamp;
        private SupportedPropertyPath<Double> _markAsDouble;
        private SupportedPropertyPath<String> _markAsString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ВВИ-ф.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getChosenEntranceExamForm()
     */
        public EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm> chosenEntranceExamForm()
        {
            if(_chosenEntranceExamForm == null )
                _chosenEntranceExamForm = new EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm>(L_CHOSEN_ENTRANCE_EXAM_FORM, this);
            return _chosenEntranceExamForm;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Обновляется демоном IEnrRatingDaemonDao для каждого класса основания отдельно.
     *
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkAsLong()
     */
        public PropertyPath<Long> markAsLong()
        {
            if(_markAsLong == null )
                _markAsLong = new PropertyPath<Long>(EnrEntrantMarkSourceGen.P_MARK_AS_LONG, this);
            return _markAsLong;
        }

    /**
     * Дата изменения значения атрибута markAsLong.
     * Обновляется демоном IEnrRatingDaemonDao при изменении значения в markAsLong.
     *
     * @return Дата изменения балла. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkTimestamp()
     */
        public PropertyPath<Date> markTimestamp()
        {
            if(_markTimestamp == null )
                _markTimestamp = new PropertyPath<Date>(EnrEntrantMarkSourceGen.P_MARK_TIMESTAMP, this);
            return _markTimestamp;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkAsDouble()
     */
        public SupportedPropertyPath<Double> markAsDouble()
        {
            if(_markAsDouble == null )
                _markAsDouble = new SupportedPropertyPath<Double>(EnrEntrantMarkSourceGen.P_MARK_AS_DOUBLE, this);
            return _markAsDouble;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource#getMarkAsString()
     */
        public SupportedPropertyPath<String> markAsString()
        {
            if(_markAsString == null )
                _markAsString = new SupportedPropertyPath<String>(EnrEntrantMarkSourceGen.P_MARK_AS_STRING, this);
            return _markAsString;
        }

        public Class getEntityClass()
        {
            return EnrEntrantMarkSource.class;
        }

        public String getEntityName()
        {
            return "enrEntrantMarkSource";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getMarkAsDouble();

    public abstract String getMarkAsString();
}
