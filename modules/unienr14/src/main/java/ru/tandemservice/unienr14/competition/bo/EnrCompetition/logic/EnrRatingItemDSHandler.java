/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/17/13
 */
public class EnrRatingItemDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String BIND_COMPETITION = "competition";

    public static final String VIEW_PROP_PREV_ORDERS = "prevOrders";
    public static final String VIEW_PROP_CURRENT_ORDER = "currentOrder";
    public static final String VIEW_PROP_CUSTOM_STATES = "customStates";
    public static final String VIEW_PROPERTY_RATING_INFO = "ratingInfo";

    public EnrRatingItemDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        EnrCompetition competition = context.get(BIND_COMPETITION);

        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrRatingItem.class, "i")
        .column(property("i"))
        .order(property(EnrRatingItem.position().fromAlias("i")))
        .where(eq(property(EnrRatingItem.competition().fromAlias("i")), value(competition)))
        ;
        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().build();

        List<EnrEntrant> entrantList = Lists.newArrayList();
        for (DataWrapper wrapper : DataWrapper.wrap(output))
            entrantList.add(((EnrRatingItem) wrapper.getWrapped()).getEntrant());
        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrantList, new Date());

        for (DataWrapper wrapper : DataWrapper.wrap(output)) {

            EnrRatingItem item = IUniBaseDao.instance.get().get(EnrRatingItem.class, wrapper.getId());

            List<IdentifiableWrapper> prevOrders = new ArrayList<>();
            List<IdentifiableWrapper> currentOrder = new ArrayList<>();
            for (EnrEnrollmentExtract extract :IUniBaseDao.instance.get().getList(
                EnrEnrollmentExtract.class,
                EnrEnrollmentExtract.entity().request().entrant(),
                item.getEntrant(),
                EnrEnrollmentExtract.paragraph().order().number().s(),
                EnrEnrollmentExtract.paragraph().order().commitDate().s(),
                EnrEnrollmentExtract.paragraph().order().id().s())
            ) {
                if (extract.getEntity().equals(item.getRequestedCompetition())) {
                    currentOrder.add(new IdentifiableWrapper(extract.getOrder().getId(), extract.getOrderInfo()));
                } else {
                    prevOrders.add(new IdentifiableWrapper(extract.getOrder().getId(), extract.getOrderInfo()));
                }
            }

            wrapper.setProperty(VIEW_PROP_PREV_ORDERS, prevOrders);
            wrapper.setProperty(VIEW_PROP_CURRENT_ORDER, currentOrder);
            wrapper.setProperty(VIEW_PROP_CUSTOM_STATES, customStatesMap.get(item.getEntrant()));
            wrapper.setProperty(VIEW_PROPERTY_RATING_INFO, ""); // todo
        }

        return output;
    }
}



