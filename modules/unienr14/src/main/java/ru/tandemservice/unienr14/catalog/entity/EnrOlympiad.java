package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrOlympiadGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Олимпиада
 */
public class EnrOlympiad extends EnrOlympiadGen implements org.tandemframework.common.catalog.entity.ICatalogItem
{
    public static final String BIND_OLYMPIAD_TYPE = "olympiadType";
    public static final String BIND_EDU_YEAR = "eduYear";

    @EntityDSLSupport
    public String getTitleWithYear()
    {
        return new StringBuilder()
                .append("(")
                .append(getEduYear().getTitle())
                .append((") "))
                .append(getTitle()).toString();
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {

        return new EntityComboDataSourceHandler(ownerId, EnrOlympiad.class)
            .customize((alias, dql, context, filter) ->
                       {
                           EducationYear eduYear = context.get(BIND_EDU_YEAR);
                           if (eduYear != null)
                               dql.where(between(property(alias, EnrOlympiad.eduYear().intValue()), value(eduYear.getIntValue() - 4), value(eduYear.getIntValue())));

                           return dql;
                       })
                .where(EnrOlympiad.olympiadType(), BIND_OLYMPIAD_TYPE)
                .titleProperty(EnrOlympiad.titleWithYear().s())
                .order(EnrOlympiad.eduYear().intValue(), OrderDirection.desc)
                .order(EnrOlympiad.title(), OrderDirection.asc)
                .filter(EnrOlympiad.eduYear().title())
                .filter(EnrOlympiad.title());
    }
}