/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.ITabPanelExtPointBuilder;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;

import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckEnrTab.EnrEnrollmentStepCheckEnrTab;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckRecTab.EnrEnrollmentStepCheckRecTab;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPub;

/**
 * @author oleyba
 * @since 12/17/13
 */
@Configuration
public class EnrEnrollmentStepPub extends BusinessComponentManager
{
    public static final String ACTION_BUTTON_LIST = "enrollmentStepActionButtons";
    public static final String COMMON_FILTER_ADDON = CommonFilterAddon.class.getSimpleName();

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addAddon(uiAddon(COMMON_FILTER_ADDON, EnrCompetitionFilterAddon.class))
        .addDataSource(searchListDS("competitionDS", competitionDSColumns(), competitionDSHandler()))
        .create();
    }

    @Bean
    public TabPanelExtPoint enrollmentStepPubTabPanelExtPoint()
    {

        return tabPanelExtPointBuilder("enrollmentStepPubTabPanel")
            .addTab(htmlTab("mainCardTab", "MainCardTab"))
            .addTab(componentTab("checkRecTab", EnrEnrollmentStepCheckRecTab.class).visible("ui:showRecommendationCheck"))
            .addTab(componentTab("checkEnrTab", EnrEnrollmentStepCheckEnrTab.class).visible("ui:showEnrollmentCheck"))
            .create();
    }

    @Bean
    public ButtonListExtPoint enrollmentStepActionButtons()
    {
        return buttonListExtPointBuilder(ACTION_BUTTON_LIST)
        .addButton(
            submitButton("refresh").listener("onClickRefresh")
            .visible("ui:step.refreshDataAllowed").permissionKey("enr14EnrollmentStepRefresh")
        )
        .addButton(
            submitButton("autoRecommend").listener("onClickAutoRecommend")
            .visible("ui:step.autoRecommendAllowed").permissionKey("enr14EnrollmentStepAutoRecommend")
        )
        .addButton(
            submitButton("allowManualRecommendation").listener("onClickAllowManualRecommendation")
            .visible("ui:step.allowManualRecommendationAllowed").permissionKey("enr14EnrollmentStepAllowManualRecommendation")
        )
        .addButton(
            submitButton("clearRecommendation").listener("onClickClearRecommendation")
            .visible("ui:step.clearRecommendationAllowed").permissionKey("enr14EnrollmentStepClearRecommendation")
        )
        .addButton(
            submitButton("autoMarkEnrolled").listener("onClickAutoMarkEnrolled")
            .visible("ui:step.autoMarkEnrolledAllowed").permissionKey("enr14EnrollmentStepAutoMarkEnrolled")
        )
        .addButton(
            submitButton("allowManualMarkEnrolled").listener("onClickAllowManualMarkEnrolled")
            .visible("ui:step.allowManualMarkEnrolledAllowed").permissionKey("enr14EnrollmentStepAllowManualMarkEnrolled")
        )
        .addButton(
            submitButton("clearMarkEnrolled").listener("onClickClearMarkEnrolled")
            .visible("ui:step.clearMarkEnrolledAllowed").permissionKey("enr14EnrollmentStepClearMarkEnrolled")
        )
        .addButton(
            submitButton("selectEnrollmentHack5696").listener("onClickSelectEnrollmentHack5696")
            .visible("ui:hack5696Visible").permissionKey("enr14EnrollmentStepAutoMarkEnrolled")
        )
        .addButton(
            submitButton("autoCreateOrders").listener("onClickAutoCreateOrders")
            .visible("ui:step.autoCreateOrdersAllowed").permissionKey("enr14EnrollmentStepCreateOrders")
        )
        .create();
    }

    @Bean
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder("competitionDS")
            .addColumn(publisherColumn("title", EnrCompetition.title())
                .publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override public Object getParameters(IEntity entity) { return ((EnrEnrollmentStepCompetitionDSHandler.CompetitionWrapper) entity).getPublisherParameters(); }
                    @Override public String getComponentName(IEntity entity) { return EnrEnrollmentStepCompetitionPub.class.getSimpleName(); }
                }))
            .addColumn(textColumn("plan", EnrEnrollmentStepCompetitionDSHandler.VIEW_PROP_PLAN).visible("ui:step.kind.usePercentage"))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickPlanEdit")
                .permissionKey("enr14EnrollmentStepPlanEdit")
                .visible("ui:step.kind.usePercentage")
                .disabled("ui:step.planEditDisabled")
            )
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> competitionDSHandler()
    {
        return new EnrEnrollmentStepCompetitionDSHandler(getName());
    }
}