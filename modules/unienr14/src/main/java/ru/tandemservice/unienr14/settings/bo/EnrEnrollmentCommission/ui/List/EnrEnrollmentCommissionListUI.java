/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit.EnrEnrollmentCommissionAddEdit;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
public class EnrEnrollmentCommissionListUI extends UIPresenter
{
    public static final String PARAM_TITLE = "title";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EnrEnrollmentCommissionList.ENROLLMENT_COMMISSION_DS))
        {
            dataSource.put(PARAM_TITLE, getSettings().get(PARAM_TITLE));
        }
    }

    public void onClickAddEnrollmentCommission()
    {
        _uiActivation.asRegionDialog(EnrEnrollmentCommissionAddEdit.class).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EnrEnrollmentCommissionAddEdit.class).parameter(PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickChangeEnable()
    {
        CatalogManager.instance().modifyDao().doChangeCatalogItemActivity(getListenerParameterAsLong());
    }
}
