/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.VisaList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 5/17/13
 */
@Configuration
public class EnrOrderVisaList extends BusinessComponentManager
{
    private static final String BUTTON_LIST = "buttonList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }

    @Bean
    public ButtonListExtPoint enrollmentVisaListButtonList() {
        return buttonListExtPointBuilder(BUTTON_LIST)
            .addButton(submitButton("addVisa", "onClickAddVisa").permissionKey("ui:secModel.addVisa"))
            .addButton(submitButton("addVisaGroup", "onClickAddVisaGroup").permissionKey("ui:secModel.addVisa"))
            .create();
    }
}