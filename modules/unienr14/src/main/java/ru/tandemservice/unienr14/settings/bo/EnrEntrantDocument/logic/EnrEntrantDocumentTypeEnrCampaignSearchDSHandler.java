/**
 *$Id: EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.person.base.entity.PersonDocumentTypeRight;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 30.04.13
 */
public class EnrEntrantDocumentTypeEnrCampaignSearchDSHandler extends DefaultSearchDataSourceHandler
{


    public EnrEntrantDocumentTypeEnrCampaignSearchDSHandler(String ownerId)
    {
        super(ownerId, PersonDocumentType.class);
    }

    public static final String PROP_USE_IN_ENR_CAMP = "useInEnrCamp";
    public static final String PROP_BENEFIT_NO_EXAMS = "benefitNoExams";
    public static final String PROP_BENEFIT_EXCLUSIVE = "benefitExclusive";
    public static final String PROP_BENEFIT_PREFERENCE = "benefitPreference";
    public static final String PROP_BENEFIT_MAX_MARK = "benefitMaxMark";
    public static final String PROP_COPY = "copy";
    public static final String PROP_ORIGINAL = "original";
    public static final String PROP_ACHIEVEMENT = "achievement";
    public static final String PROP_SEND_FIS = "sendFIS";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        if (enrCamp == null)
            return ListOutputBuilder.get(input, new ArrayList()).build();

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonDocumentType.class, "b").column(property("b"))
                .where(exists(new DQLSelectBuilder().fromEntity(PersonDocumentTypeRight.class, "tr")
                        .where(eq(property("tr", PersonDocumentTypeRight.documentType()), property("b")))
                        .where(eq(property("tr", PersonDocumentTypeRight.documentContext()), value(PersonDocumentUtil.key(EnrEntrant.class))))
                        .where(eq(property("tr", PersonDocumentTypeRight.viewAndLink()), value(true)))
                        .buildQuery()))
        .order(property(PersonDocumentType.priority().fromAlias("b")), OrderDirection.asc);

        filter(builder, context, "b");

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();

        return wrap(output, input, context);
    }

    /**
     * @param builder dql builder from EnrEntrantDocumentType
     */
    protected void filter(DQLSelectBuilder builder, ExecutionContext context, String alias)
    {

    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

        final List<EnrCampaignEntrantDocument> relationList = new DQLSelectBuilder().fromEntity(EnrCampaignEntrantDocument.class, "r").column(property("r"))
        .where(eq(property(EnrCampaignEntrantDocument.enrollmentCampaign().fromAlias("r")), value(enrCamp)))
        .createStatement(context.getSession()).list();

        final Map<PersonDocumentType, EnrCampaignEntrantDocument> docType2CampMap = new HashMap<>();
        for (EnrCampaignEntrantDocument campaignEntrantDocument : relationList)
            docType2CampMap.put(campaignEntrantDocument.getDocumentType(), campaignEntrantDocument);

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            EnrCampaignEntrantDocument campaignEntrantDocument = docType2CampMap.get(wrapper.<PersonDocumentType>getWrapped());

            if(campaignEntrantDocument == null)
            {
                campaignEntrantDocument = new EnrCampaignEntrantDocument(enrCamp, wrapper.getWrapped());
            }

            wrapper.setProperty(PROP_BENEFIT_NO_EXAMS, campaignEntrantDocument.isBenefitNoExams());
            wrapper.setProperty(PROP_BENEFIT_EXCLUSIVE, campaignEntrantDocument.isBenefitExclusive());
            wrapper.setProperty(PROP_BENEFIT_PREFERENCE, campaignEntrantDocument.isBenefitPreference());
            wrapper.setProperty(PROP_BENEFIT_MAX_MARK, campaignEntrantDocument.isBenefitMaxMark());
            wrapper.setProperty(PROP_COPY, campaignEntrantDocument.isCopyCanBeAccepted());
            wrapper.setProperty(PROP_ORIGINAL, campaignEntrantDocument.isOriginalCanBeAccepted());
            wrapper.setProperty(PROP_ACHIEVEMENT, campaignEntrantDocument.isAchievement());
            wrapper.setProperty(PROP_SEND_FIS, campaignEntrantDocument.isSendFIS());
        }

        return output;
    }
}
