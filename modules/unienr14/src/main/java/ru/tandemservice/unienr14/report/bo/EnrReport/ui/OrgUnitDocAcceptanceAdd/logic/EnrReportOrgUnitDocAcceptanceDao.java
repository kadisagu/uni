/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.logic;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.EnrReportOrgUnitDocAcceptanceAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs;
import ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author rsizonenko
 * @since 03.06.2014
 */
public class EnrReportOrgUnitDocAcceptanceDao extends UniBaseDao implements IEnrReportOrgUnitDocAcceptanceDao
{
    @Override
    public Long createReport(EnrReportOrgUnitDocAcceptanceAddUI model)
    {
        EnrReportOrgUnitDocAcceptance report = new EnrReportOrgUnitDocAcceptance();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsForeignLangs.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantsForeignLangs.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsForeignLangs.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsForeignLangs.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsForeignLangs.P_FORMATIVE_ORG_UNIT, "fullTitle");

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportOrgUnitDocAcceptance.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    public byte[] buildReport(EnrReportOrgUnitDocAcceptanceAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ORG_UNIT_DOC_ACCEPTANCE);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Дата

        Date formingDate = new Date();
        new RtfInjectModifier().put("dateForm", new SimpleDateFormat("dd.MM.yyyy").format(formingDate)).modify(document);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL
                .column(property("reqComp"));

        List<EnrRequestedCompetition> reqCompList = getList(requestedCompDQL);

        Comparator<EnrProgramSetOrgUnit> programSetOrgUnitComparator = new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                int result;
                result = o1.getEffectiveFormativeOrgUnit().getPrintTitle().compareTo(o2.getEffectiveFormativeOrgUnit().getPrintTitle());
                if (result != 0) return result;
                final OrgUnit o1OrgUnit = o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                final OrgUnit o2OrgUnit = o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                result = Boolean.compare(null == o1OrgUnit.getParent(), null == o2OrgUnit.getParent());
                if (result != 0) return -result;
                return o1OrgUnit.getPrintTitle().compareTo(o2OrgUnit.getPrintTitle());
            }
        };

        Comparator<EnrProgramSetBase> programSetBaseComparator = new Comparator<EnrProgramSetBase>() {
            @Override
            public int compare(EnrProgramSetBase o1, EnrProgramSetBase o2) {
                return o1.getProgramSubject().getCode().compareTo(o2.getProgramSubject().getCode());
            }
        };


        // Данные для таблицы

        Map<EnrProgramSetOrgUnit, Map<EnrProgramSetBase, Set<EnrEntrantRequest>>> dataMap = new TreeMap<>(programSetOrgUnitComparator);

        for (EnrRequestedCompetition reqComp : reqCompList)
        {
            EnrProgramSetOrgUnit programSetOrgUnit = reqComp.getCompetition().getProgramSetOrgUnit();
            EnrProgramSetBase programSetBase = programSetOrgUnit.getProgramSet();
            EnrEntrantRequest request = reqComp.getRequest();
            final Map<EnrProgramSetBase, Set<EnrEntrantRequest>> programSetBaseMap = SafeMap.safeGet(dataMap, programSetOrgUnit, TreeMap.class, programSetBaseComparator);
            final Set<EnrEntrantRequest> requests = SafeMap.safeGet(programSetBaseMap, programSetBase, HashSet.class);
            requests.add(request);
        }

        Integer[] totalRowFinal = new Integer[]{0,0,0,0};


        RtfTable tTable = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfTable fTable = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "F");
        final RtfElement filial = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "filial");
        document.getElementList().remove(tTable);
        document.getElementList().remove(fTable);
        document.getElementList().remove(filial);

        boolean notFirstIteration = false;

        // Заполнение Т-таблицы и Ф-таблицы

        for (Map.Entry<EnrProgramSetOrgUnit, Map<EnrProgramSetBase, Set<EnrEntrantRequest>>> orgUnitToProgramSetEntry : dataMap.entrySet())
        {
            List<Integer[]> tTableValues = new ArrayList<>();
            List<String[]> tTableContent = new ArrayList<>();

            new RtfInjectModifier().put("formativeOrgUnit", orgUnitToProgramSetEntry.getKey().getEffectiveFormativeOrgUnit().getPrintTitle());

            for (Map.Entry<EnrProgramSetBase, Set<EnrEntrantRequest>> programSetToRequestEntry : orgUnitToProgramSetEntry.getValue().entrySet())
            {
                int total = 0;
                int before = 0;
                int today = 0;
                int takedOut = 0;

                for (EnrEntrantRequest request : programSetToRequestEntry.getValue())
                {
                    total++;
                    if (DateUtils.isSameDay(request.getRegDate(), formingDate)) today++;
                    else before++;
                    if (request.isTakeAwayDocument()) takedOut++;
                }
                Integer[] intRow = new Integer[]{
                        total,
                        before,
                        today,
                        takedOut
                };
                String[] row = new String[]{
                        programSetToRequestEntry.getKey().getProgramSubject().getTitleWithCode(),
                        String.valueOf(total),
                        String.valueOf(before),
                        String.valueOf(today),
                        String.valueOf(takedOut)
                };
                tTableValues.add(intRow);
                tTableContent.add(row);
            }

            Integer[] totalRow = getTotalRow(tTableValues);
            tTableContent.add(new String[]{
                    "Итого:",
                    String.valueOf(totalRow[0]),
                    String.valueOf(totalRow[1]),
                    String.valueOf(totalRow[2]),
                    String.valueOf(totalRow[3])
            });

            totalRowFinal = sumOfRows(totalRow, totalRowFinal);
            StringBuilder builder = new StringBuilder(orgUnitToProgramSetEntry.getKey().getEffectiveFormativeOrgUnit().getPrintTitle());
            builder
                    .append(" (")
                    .append(orgUnitToProgramSetEntry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getPrintTitle())
                    .append(")");
            String[][] fTableContent = new String[][]{
                    {builder.toString()}
            };

            document.getElementList().add(fTable.getClone());

            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("F", fTableContent);
            modifier.put("T", tTableContent.toArray(new String[tTableContent.size()][]));
            modifier.modify(document);

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

        }

        document.getElementList().add(tTable.getClone());
        String[][] fTableContent = new String[][]{
                {"Итоги по отчету"}
        };
        String[][] tTableContent = new String[][]{
                {
                        "Итого:",
                        String.valueOf(totalRowFinal[0]),
                        String.valueOf(totalRowFinal[1]),
                        String.valueOf(totalRowFinal[2]),
                        String.valueOf(totalRowFinal[3])
                }
        };



        // Заполняем Н-таблицу
        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
        modifier.put("H", new RtfRowIntercepterRawText());
        modifier.put("T", tTableContent);
        modifier.put("F", fTableContent);
        modifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    // Утильные методы
    private Integer[] getTotalRow(List<Integer[]> table)
    {
        if (table.size() == 0 || table.get(0).length == 0)
            return new Integer[] {
                    0, 0, 0, 0
            };

        Integer[] totalRow = table.get(0);


        for (int k = 1; k < table.size(); k++)
        {
            for (int i = 0; i < totalRow.length; i++) {
                totalRow[i] += table.get(k)[i];
            }
        }

        return totalRow;
    }

    private Integer[] sumOfRows(Integer[] r1, Integer[] r2)
    {
        Integer[] sum = new Integer[r1.length];
        for (int i = 0; i < r1.length; i++) {
            sum[i] = r1[i] + r2[i];
        }
        return sum;
    }
}
