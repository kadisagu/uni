package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние шага зачисления"
 * Имя сущности : enrEnrollmentStepState
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEnrollmentStepStateCodes
{
    /** Константа кода (code) элемента : Сформирован (title) */
    String START = "start";
    /** Константа кода (code) элемента : Рекомендация зафиксирована (title) */
    String RECOMMENDED = "recommended";
    /** Константа кода (code) элемента : Формирование приказов (title) */
    String FORMING_ORDERS = "forming orders";
    /** Константа кода (code) элемента : Зачисление завершено (title) */
    String CLOSED = "closed";

    Set<String> CODES = ImmutableSet.of(START, RECOMMENDED, FORMING_ORDERS, CLOSED);
}
