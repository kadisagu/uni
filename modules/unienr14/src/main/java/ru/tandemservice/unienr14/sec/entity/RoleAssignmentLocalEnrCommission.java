package ru.tandemservice.unienr14.sec.entity;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import ru.tandemservice.unienr14.sec.entity.gen.*;

/** @see ru.tandemservice.unienr14.sec.entity.gen.RoleAssignmentLocalEnrCommissionGen */
public class RoleAssignmentLocalEnrCommission extends RoleAssignmentLocalEnrCommissionGen
{
    public RoleAssignmentLocalEnrCommission()
    {
    }

    public RoleAssignmentLocalEnrCommission(IPrincipalContext principalContext, RoleConfigLocalEnrCommission role)
    {
        setPrincipalContext(principalContext);
        setRoleConfig(role);
    }


    @Override
    public ILocalRoleContext getLocalRoleContext()
    {
        return getRoleConfig().getEnrollmentCommission();
    }
}