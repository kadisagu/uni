/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsByOrgUnitsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsByOrgUnitsAdd.EnrReportEnrollmentResultsByOrgUnitsAddUI;

/**
 * @author rsizonenko
 * @since 21.07.2014
 */
public interface IEnrReportEnrollmentResultsByOrgUnitsDao extends INeedPersistenceSupport {
    long createReport(EnrReportEnrollmentResultsByOrgUnitsAddUI model);
}
