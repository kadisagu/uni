package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;

/**
 * @author vdanilov
 */
public interface IEnrEntrantAchievementProofDocument extends IEntity, IEnrEntrantRequestAttachable {
    public EnrEntrant getEntrant();
}
