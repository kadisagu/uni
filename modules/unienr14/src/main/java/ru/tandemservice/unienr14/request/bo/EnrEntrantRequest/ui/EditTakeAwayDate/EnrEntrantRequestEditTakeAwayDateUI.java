/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.EditTakeAwayDate;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;

/**
 * @author oleyba
 * @since 6/3/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "request.id")
})
public class EnrEntrantRequestEditTakeAwayDateUI extends UIPresenter
{
    private EnrEntrantRequest request = new EnrEntrantRequest();

    @Override
    public void onComponentRefresh()
    {
        setRequest(IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, getRequest().getId()));
    }

    public void onClickApply() {
        IUniBaseDao.instance.get().update(getRequest());
        deactivate();
    }

    // getters and setters

    public EnrEntrantRequest getRequest()
    {
        return request;
    }

    public void setRequest(EnrEntrantRequest request)
    {
        this.request = request;
    }
}