/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.caf.IUIPresenterAdapter;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.component.IComponentRegionOwner;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AddInfoStep.EnrEntrantRequestAddWizardAddInfoStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AttachedDocsStep.EnrEntrantRequestAddWizardAttachedDocsStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.CompetitionsStep.EnrEntrantRequestAddWizardCompetitionsStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ContactsStep.EnrEntrantRequestAddWizardContactsStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.DocumentsStep.EnrEntrantRequestAddWizardDocumentsStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep.EnrEntrantRequestAddWizardExamsStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.IdentityCardStep.EnrEntrantRequestAddWizardIdentityCardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.IndAchievementsStep.EnrEntrantRequestAddWizardIndAchievementsStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.NextOfKinStep.EnrEntrantRequestAddWizardNextOfKinStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.PrioritiesStep.EnrEntrantRequestAddWizardPrioritiesStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.RequestTypeAndEduDocStep.EnrEntrantRequestAddWizardRequestTypeAndEduDocStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.StateExamsStep.EnrEntrantRequestAddWizardStateExamsStep;

/**
 * @author nvankov
 * @since 5/30/14
 */
public class EnrEntrantRequestAddWizardWizardUI extends UIPresenter
{
    public static final String BIND_PERSON = "personId";

    public static final String WIZARD_STEP_REGION = "wizardStep";
    public static final String PARAM_WIZARD_STATE = "state";

    public static final String IDENTITY_CARD_STEP_CODE = EnrEntrantRequestAddWizardIdentityCardStep.class.getSimpleName().toLowerCase();
    public static final String REQUEST_TYPE_AND_EDU_DOC_STEP_CODE = EnrEntrantRequestAddWizardRequestTypeAndEduDocStep.class.getSimpleName().toLowerCase();
    public static final String STATE_EXAMS_STEP_CODE = EnrEntrantRequestAddWizardStateExamsStep.class.getSimpleName().toLowerCase();
    public static final String DOCUMENTS_STEP_CODE = EnrEntrantRequestAddWizardDocumentsStep.class.getSimpleName().toLowerCase();
    public static final String NEXT_OF_KIN_STEP_CODE = EnrEntrantRequestAddWizardNextOfKinStep.class.getSimpleName().toLowerCase();
    public static final String ADD_INFO_STEP_CODE = EnrEntrantRequestAddWizardAddInfoStep.class.getSimpleName().toLowerCase();
    public static final String CONTACTS_STEP_CODE = EnrEntrantRequestAddWizardContactsStep.class.getSimpleName().toLowerCase();
    public static final String IND_ACHIEVEMENTS_STEP_CODE = EnrEntrantRequestAddWizardIndAchievementsStep.class.getSimpleName().toLowerCase();
    public static final String COMPETITIONS_STEP_CODE = EnrEntrantRequestAddWizardCompetitionsStep.class.getSimpleName().toLowerCase();
    public static final String EXAMS_STEP_CODE = EnrEntrantRequestAddWizardExamsStep.class.getSimpleName().toLowerCase();
    public static final String PRIORITIES_STEP_CODE = EnrEntrantRequestAddWizardPrioritiesStep.class.getSimpleName().toLowerCase();
    public static final String ATTACHED_DOCS_STEP_CODE = EnrEntrantRequestAddWizardAttachedDocsStep.class.getSimpleName().toLowerCase();

    public static final String STEP_NEXT = "next";
    public static final String STEP_PREVIOUS = "previous";
    public static final String STEP_CANCEL = "cancel";
    public static final String STEP_ACTIVATE_STEP = "activateStep";

    private EnrEntrantWizardState _state;
    private String _currentStepCode;
    private EnrEntrantWizardStep _currentStepData;

    @Override
    public void onComponentActivate()
    {
        fillSteps();
        activateStep(IDENTITY_CARD_STEP_CODE);
    }

    // actions

    public void onClickActivateOtherStep()
    {
        String stepName = getListenerParameter();
        activateStep(stepName);
    }

    public void onClickNext()
    {
        IWizardStep step = getCurrentStep();
        if (step == null) return;
        step.saveStepData();

        if (step instanceof UIPresenter) {
            // Сохранение данных в кастомизациях
            for (IUIAddon addon : ((UIPresenter) step).getConfig().getAddons().values())
            {
                if (addon instanceof IWizardStep) {
                    ((IWizardStep) addon).saveStepData();
                }
            }
        }

        if (ContextLocal.getErrorCollector().hasErrors()) return;
        if (getCurrentStepData() != null) {
            getState().activateNextStep(getCurrentStepData().getStepName());
        }
        EnrEntrantWizardStep nextStep = getNextStep();
        if (nextStep != null) {
            activateStep(nextStep.getStepName());
        }
    }

    public void onClickPrevious()
    {
        EnrEntrantWizardStep previousStep = getPreviousStep();
        if (previousStep != null) {
            activateStep(previousStep.getStepName());
        }
    }

    public void onClickCancel()
    {
        deactivate();
    }

    // presenter

    public boolean isHeaderStepSameAsCurrent() {
        return getCurrentStepForHeader() != null && getCurrentStepData() != null && getCurrentStepForHeader().getStepName().equals(getCurrentStepData().getStepName());
    }

    public boolean isHeaderStepIdcStep() {
        return getCurrentStepForHeader() != null && getCurrentStepForHeader().getStepName().equals(ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI.IDENTITY_CARD_STEP_CODE);
    }

    public boolean isShowWizardButtons() {
        return getCurrentStepData() == null || getCurrentStepData().isShowWizardButtons();
    }

    public boolean isPreviousButtonDisabled() {
        return getPreviousStep() == null || getPreviousStep().isDisabled();
    }

    public boolean isValidateOnClickNext() {
        return getCurrentStepData() == null || getCurrentStepData().isValidateOnClickNext();
    }

    public IWizardStep getCurrentStep()
    {
        IWizardStep step = (IWizardStep) _uiSupport.getChildUI(WIZARD_STEP_REGION);
        if(step == null) throw new RuntimeException("Can't find current wizard step");
        return step;
    }

    public EnrEntrantWizardStep getNextStep()
    {
        if(_currentStepData == null) return null;
        int index = getState().getStepsOrder().indexOf(_currentStepData.getStepName());
        if(index == -1) throw new RuntimeException("Current step not in steps");
        if(index + 1 > getState().getStepsOrder().size() - 1) return null;
        return getState().getSteps().get(getState().getStepsOrder().get(index+1));
    }

    public EnrEntrantWizardStep getPreviousStep()
    {
        if(_currentStepData == null) return null;
        int index = getState().getStepsOrder().indexOf(_currentStepData.getStepName());
        if(index == -1) throw new RuntimeException("Current step not in steps");
        if(index < 1) return null;
        return getState().getSteps().get(getState().getStepsOrder().get(index - 1));
    }

    public EnrEntrantWizardStep getCurrentStepForHeader()
    {
        return getState().getSteps().get(_currentStepCode);
    }

    // utils

    private void fillSteps()
    {
        _state = new EnrEntrantWizardState();
        getState().setIdentityCardStep(new EnrEntrantWizardStep(IDENTITY_CARD_STEP_CODE, EnrEntrantRequestAddWizardIdentityCardStep.class, "Удостоверение личности"));
        getState().setRequestTypeAndEduDocStep(new EnrEntrantWizardStep(REQUEST_TYPE_AND_EDU_DOC_STEP_CODE, EnrEntrantRequestAddWizardRequestTypeAndEduDocStep.class, "Вид заявления и образование"));
        getState().setStateExamsStep(new EnrEntrantWizardStep(STATE_EXAMS_STEP_CODE, EnrEntrantRequestAddWizardStateExamsStep.class, "Результаты ЕГЭ"));
        getState().setDocumentsStep(new EnrEntrantWizardStep(DOCUMENTS_STEP_CODE, EnrEntrantRequestAddWizardDocumentsStep.class, "Документы"));
        getState().setNextOfKinStep(new EnrEntrantWizardStep(NEXT_OF_KIN_STEP_CODE, EnrEntrantRequestAddWizardNextOfKinStep.class, "Родственники"));
        getState().setAddInfoStep(new EnrEntrantWizardStep(ADD_INFO_STEP_CODE, EnrEntrantRequestAddWizardAddInfoStep.class, "Доп. сведения"));
        getState().setContactsStep(new EnrEntrantWizardStep(CONTACTS_STEP_CODE, EnrEntrantRequestAddWizardContactsStep.class, "Контакты"));
        getState().setCompetitionsStep(new EnrEntrantWizardStep(COMPETITIONS_STEP_CODE, EnrEntrantRequestAddWizardCompetitionsStep.class, "Заявление, выбор конкурсов"));
        getState().setIndAchievementsStep(new EnrEntrantWizardStep(IND_ACHIEVEMENTS_STEP_CODE, EnrEntrantRequestAddWizardIndAchievementsStep.class, "Инд. достижения"));
        getState().setExamsStep(new EnrEntrantWizardStep(EXAMS_STEP_CODE, EnrEntrantRequestAddWizardExamsStep.class, "Выбор ВИ"));
        getState().setPrioritiesStep(new EnrEntrantWizardStep(PRIORITIES_STEP_CODE, EnrEntrantRequestAddWizardPrioritiesStep.class, "Приоритеты"));
        getState().setAttachedDocsStep(new EnrEntrantWizardStep(ATTACHED_DOCS_STEP_CODE, EnrEntrantRequestAddWizardAttachedDocsStep.class, "Приложенные к заявлению документы"));

        getState().getStepsOrder().add(IDENTITY_CARD_STEP_CODE);
        getState().getStepsOrder().add(REQUEST_TYPE_AND_EDU_DOC_STEP_CODE);
        getState().getStepsOrder().add(STATE_EXAMS_STEP_CODE);
        getState().getStepsOrder().add(DOCUMENTS_STEP_CODE);
        getState().getStepsOrder().add(NEXT_OF_KIN_STEP_CODE);
        getState().getStepsOrder().add(ADD_INFO_STEP_CODE);
        getState().getStepsOrder().add(CONTACTS_STEP_CODE);
        getState().getStepsOrder().add(COMPETITIONS_STEP_CODE);
        getState().getStepsOrder().add(IND_ACHIEVEMENTS_STEP_CODE);
        getState().getStepsOrder().add(EXAMS_STEP_CODE);
        getState().getStepsOrder().add(PRIORITIES_STEP_CODE);
        getState().getStepsOrder().add(ATTACHED_DOCS_STEP_CODE);

        getState().getSteps().put(getState().getIdentityCardStep().getStepName(), getState().getIdentityCardStep());
        getState().getSteps().put(getState().getRequestTypeAndEduDocStep().getStepName(), getState().getRequestTypeAndEduDocStep());
        getState().getSteps().put(getState().getStateExamsStep().getStepName(), getState().getStateExamsStep());
        getState().getSteps().put(getState().getDocumentsStep().getStepName(), getState().getDocumentsStep());
        getState().getSteps().put(getState().getNextOfKinStep().getStepName(), getState().getNextOfKinStep());
        getState().getSteps().put(getState().getAddInfoStep().getStepName(), getState().getAddInfoStep());
        getState().getSteps().put(getState().getContactsStep().getStepName(), getState().getContactsStep());
        getState().getSteps().put(getState().getCompetitionsStep().getStepName(), getState().getCompetitionsStep());
        getState().getSteps().put(getState().getIndAchievementsStep().getStepName(), getState().getIndAchievementsStep());
        getState().getSteps().put(getState().getExamsStep().getStepName(), getState().getExamsStep());
        getState().getSteps().put(getState().getPrioritiesStep().getStepName(), getState().getPrioritiesStep());
        getState().getSteps().put(getState().getAttachedDocsStep().getStepName(), getState().getAttachedDocsStep());

        getState().getRequestTypeAndEduDocStep().setDisabled(true);
        getState().getStateExamsStep().setDisabled(true);
        getState().getDocumentsStep().setDisabled(true);
        getState().getNextOfKinStep().setDisabled(true);
        getState().getAddInfoStep().setDisabled(true);
        getState().getContactsStep().setDisabled(true);
        getState().getCompetitionsStep().setDisabled(true);
        getState().getIndAchievementsStep().setDisabled(true);
        getState().getExamsStep().setDisabled(true);
        getState().getPrioritiesStep().setDisabled(true);
        getState().getAttachedDocsStep().setDisabled(true);

        getState().getIdentityCardStep().setShowWizardButtons(false);
        getState().getAttachedDocsStep().setShowWizardButtons(false);

        getState().getDocumentsStep().setValidateOnClickNext(false);
        getState().getNextOfKinStep().setValidateOnClickNext(false);
    }

    // public utils

    public void activateStep(String stepName)
    {
        if(getState().getSteps().get(stepName) == null) throw new RuntimeException("Can't find wizard step - " + stepName);
        _currentStepData = getState().getSteps().get(stepName);
        ParametersMap paramsMap = new ParametersMap().add(EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, _state);
        if(getState().getPersonId() != null) {
            paramsMap.put(BIND_PERSON, getState().getPersonId());
        }
        if(getState().getEntrantId() != null) {
            paramsMap.put("entrantId", getState().getEntrantId());
        }
        if (getState().getEntrantRequestId() != null) {
            paramsMap.put("requestId", getState().getEntrantRequestId());
        }
        _uiActivation.asRegion(_currentStepData.getComponentName(), WIZARD_STEP_REGION).parameters(paramsMap).activate();
    }

    /** @return true, если указанный компонент (презентер) находится внутри визарда  */
    public static boolean isInWizard(final IUIPresenter presenter) {
        return (null != findWizard(presenter.getConfig().getBusinessComponent()));
    }

    /** @return true, если указанный компонент находится внутри визарда  */
    public static boolean isInWizard(final IBusinessComponent component) {
        return (null != findWizard(component));
    }

    /**
     * по указанному компоненту находит визард, создержащий его
     * @return презентер визарда, null - если компонент вне визарда
     */
    public static EnrEntrantRequestAddWizardWizardUI findWizard(IBusinessComponent component) {
        while (null != component) {
            final IUIPresenterAdapter presenter = component.getPresenter();
            if (presenter instanceof EnrEntrantRequestAddWizardWizardUI) { return (EnrEntrantRequestAddWizardWizardUI) presenter; }

            final IComponentRegion parentRegion = component.getParentRegion();
            if (null == parentRegion) { return null; }

            final IComponentRegionOwner owner = parentRegion.getOwner();
            if (!(owner instanceof IBusinessComponent)) { return null; }

            component = (IBusinessComponent)owner;
        }
        return null;
    }

    // getters and setters

    public String getCurrentStepCode()
    {
        return _currentStepCode;
    }

    public void setCurrentStepCode(String currentStepCode)
    {
        _currentStepCode = currentStepCode;
    }

    public EnrEntrantWizardStep getCurrentStepData()
    {
        return _currentStepData;
    }

    public EnrEntrantWizardState getState()
    {
        return _state;
    }
}
