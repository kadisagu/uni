/* $Id$ */
package ru.tandemservice.unienr14.ws.onlineentrant;

import ru.tandemservice.unienr14.ws.ClientSoapFaultException;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * @author nvankov
 * @since 5/8/14
 */

@WebService
public class EnrOnlineEntrantService
{
    /**
     * Получение данных справочников
     *
     * @return Данные справочников
     */
    public CatalogsData getCatalogs(@WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle)
    {
        if(enrollmentCampaignTitle == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"enrollmentCampaignTitle\" is required");
        return IEnrOnlineEntrantServiceDao.INSTANCE.get().getCatalogs(enrollmentCampaignTitle);
    }

    /**
     * Создание/обновление онлайн-абитуриента
     *
     */
    public void createOrUpdateOnlineEntrant(@WebParam(name = "onlineEntrant") OnlineEntrantData onlineEntrant)
    {
        if(onlineEntrant == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"onlineEntrant\" is required");
        IEnrOnlineEntrantServiceDao.INSTANCE.get().createOrUpdateOnlineEntrant(onlineEntrant);
    }

    /**
     * Создан ли уже онлайн-абитуриент
     *
     * @return true, если существует
     */
    public boolean isOnlineEntrantExist(@WebParam(name = "onlineEntrantId") Long onlineEntrantId)
    {
        if(onlineEntrantId == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"onlineEntrantId\" is required");
        return IEnrOnlineEntrantServiceDao.INSTANCE.get().isOnlineEntrantExist(onlineEntrantId);
    }

    /**
     * Принято ли заявление онлайн-абитуриента
     *
     * @return true, если принято
     */
    public boolean isOnlineEntrantStatementAccepted(@WebParam(name = "onlineEntrantId") Long onlineEntrantId)
    {
        if(onlineEntrantId == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"onlineEntrantId\" is required");
        return IEnrOnlineEntrantServiceDao.INSTANCE.get().isOnlineEntrantStatementAccepted(onlineEntrantId);
    }

    /**
     * @return id абитуриента в уни, если он создан, иначе null
     */
    public String getEntrantUid(@WebParam(name = "onlineEntrantId") Long onlineEntrantId) {
        if(onlineEntrantId == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"onlineEntrantId\" is required");
        return IEnrOnlineEntrantServiceDao.INSTANCE.get().getEntrantId(onlineEntrantId);
    }

    /**
     * Получить данные онлайн-абитуриента
     *
     * @return Данные онлайн-абитуриента
     */
    public OnlineEntrantData getOnlineEntrant(@WebParam(name = "onlineEntrantId") Long onlineEntrantId, @WebParam(name = "fullData") boolean isFullData)
    {
        if(onlineEntrantId == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"onlineEntrantId\" is required");
        return IEnrOnlineEntrantServiceDao.INSTANCE.get().getOnlineEntrant(onlineEntrantId, isFullData);
    }

    /**
     * @return список id конкурсов абитуриента в уни, если они есть, иначе null
     */
    public List<String> getOnlineEntrantCompetitionsUids(@WebParam(name = "onlineEntrantId") Long onlineEntrantId) {
        if(onlineEntrantId == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA, "parameter \"onlineEntrantId\" is required");
        return IEnrOnlineEntrantServiceDao.INSTANCE.get().getOnlineEntrantCompetitionsUids(onlineEntrantId);
    }
}
