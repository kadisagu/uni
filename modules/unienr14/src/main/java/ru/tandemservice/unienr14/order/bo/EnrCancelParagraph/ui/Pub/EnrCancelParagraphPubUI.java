/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.Pub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic.EnrCancelExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit.EnrCancelParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit.EnrCancelParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelParagraph;

import java.util.List;

/**
 * @author oleyba
 * @since 7/10/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "paragraphId")
})
public class EnrCancelParagraphPubUI extends UIPresenter
{
    private Long _paragraphId;
    private EnrCancelParagraph _paragraph;
    private CommonPostfixPermissionModel _secModel;

    private DataWrapper params = new DataWrapper();

    @Override
    public void onComponentRefresh()
    {
        setParagraph(DataAccessServices.dao().getNotNull(EnrCancelParagraph.class, getParagraphId()));
        setSecModel(new CommonPostfixPermissionModel("enrOrder"));

        List<EnrCancelExtract> extracts = IUniBaseDao.instance.get().getList(EnrCancelExtract.class, EnrCancelExtract.paragraph(), getParagraph(), EnrCancelExtract.number().s());
        getParams().put("eduForm", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().programSetOrgUnit().programSet().programForm().title().s(), ";"));
        getParams().put("orgUnit", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().programSetOrgUnit().orgUnit().s() + ".departmentTitle", ";"));
        getParams().put("formativeOrgUnit", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().programSetOrgUnit().formativeOrgUnit().fullTitle().s(), ";"));
        getParams().put("programSubject", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().programSetOrgUnit().programSet().programSubject().titleWithCode().s(), ";"));
        getParams().put("programSet", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().programSetOrgUnit().programSet().title().s(), ";"));
        getParams().put("competitionType", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().type().title().s(), ";"));
        getParams().put("competition", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrCancelExtract.requestedCompetition().competition().title().s(), ";"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EnrCancelParagraphPub.DS_EXTRACT.equals(dataSource.getName())) {
            dataSource.put(EnrCancelExtractDSHandler.ENROLLMENT_PARAGRAPH, _paragraph);
        }
    }

    public boolean isShowExcludeLowerPriorityCompetitions()
    {
        return EnrOrderTypeCodes.ENROLLMENT.equals(getParagraph().getCancelledOrder().getType().getCode());
    }

    public String getFormTitle()
    {
        return "Параграф приказа «" + getParagraph().getOrder().getType().getTitle() + "»";
    }

    // Listeners

    public void onClickEditParagraph()
    {
        _uiActivation.asRegion(EnrCancelParagraphAddEdit.class)
            .parameter(EnrCancelParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getParagraph().getId())
            .activate();
    }

    public void onClickDeleteParagraph()
    {
        EnrOrderManager.instance().dao().deleteParagraph(getParagraph().getId());
        deactivate();
    }

    public void onClickDeleteExtract()
    {
        EnrCancelExtract extract = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());

        EnrOrderManager.instance().dao().deleteExtract(extract);
    }

    // Getters & Setters

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public EnrCancelParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrCancelParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public DataWrapper getParams()
    {
        return params;
    }

    public void setParams(DataWrapper params)
    {
        this.params = params;
    }   
}