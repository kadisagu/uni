/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.EnrReportOrgUnitDocAcceptanceAddUI;

/**
 * @author rsizonenko
 * @since 03.06.2014
 */
public interface IEnrReportOrgUnitDocAcceptanceDao extends INeedPersistenceSupport {
    Long createReport(EnrReportOrgUnitDocAcceptanceAddUI enrReportOrgUnitDocAcceptanceAddUI);
}
