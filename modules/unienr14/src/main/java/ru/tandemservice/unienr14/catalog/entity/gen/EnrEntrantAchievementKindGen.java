package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид индивидуального достижения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantAchievementKindGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantAchievementKindGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind";
    public static final String ENTITY_NAME = "enrEntrantAchievementKind";
    public static final int VERSION_HASH = -57141540;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DESCRIPTION = "description";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _description;     // Описание
    private EnrRequestType _requestType;     // Вид заявления
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantAchievementKindGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrEntrantAchievementKind)another).getCode());
            }
            setShortTitle(((EnrEntrantAchievementKind)another).getShortTitle());
            setDescription(((EnrEntrantAchievementKind)another).getDescription());
            setRequestType(((EnrEntrantAchievementKind)another).getRequestType());
            setTitle(((EnrEntrantAchievementKind)another).getTitle());
        }
    }

    public INaturalId<EnrEntrantAchievementKindGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantAchievementKindGen>
    {
        private static final String PROXY_NAME = "EnrEntrantAchievementKindNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantAchievementKindGen.NaturalId) ) return false;

            EnrEntrantAchievementKindGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantAchievementKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantAchievementKind.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantAchievementKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "description":
                    return obj.getDescription();
                case "requestType":
                    return obj.getRequestType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "description":
                        return true;
                case "requestType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "description":
                    return true;
                case "requestType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "description":
                    return String.class;
                case "requestType":
                    return EnrRequestType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantAchievementKind> _dslPath = new Path<EnrEntrantAchievementKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantAchievementKind");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrEntrantAchievementKind> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _description;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrEntrantAchievementKindGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrEntrantAchievementKindGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EnrEntrantAchievementKindGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrEntrantAchievementKindGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrEntrantAchievementKind.class;
        }

        public String getEntityName()
        {
            return "enrEntrantAchievementKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
