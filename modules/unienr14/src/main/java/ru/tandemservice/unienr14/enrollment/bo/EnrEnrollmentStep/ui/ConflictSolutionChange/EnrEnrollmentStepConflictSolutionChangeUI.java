/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.ConflictSolutionChange;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentConflictSolution;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 1/21/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "item.id")
})
public class EnrEnrollmentStepConflictSolutionChangeUI extends UIPresenter
{
    private EnrEnrollmentStepItem item = new EnrEnrollmentStepItem();

    private List<EnrEnrollmentStepItemOrderInfo> orders;
    private EnrEnrollmentStepItemOrderInfo currentOrder;

    private ISelectModel solutionModel;

    @Override
    public void onComponentRefresh()
    {
        setItem(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentStepItem.class, getItem().getId()));
        setSolutionModel(new LazySimpleSelectModel<EnrEnrollmentConflictSolution>(EnrEnrollmentConflictSolution.class));
        setOrders(new ArrayList<EnrEnrollmentStepItemOrderInfo>());
        for (EnrEnrollmentStepItemOrderInfo info : IUniBaseDao.instance.get().getList(EnrEnrollmentStepItemOrderInfo.class, EnrEnrollmentStepItemOrderInfo.item(), item, EnrEnrollmentStepItemOrderInfo.orderInfo().s())) {
            if (info.getExtract() != null && info.getExtract().getEntity().equals(getItem().getEntity().getRequestedCompetition()))
                continue;
            getOrders().add(info);
        }
        if (getOrders().isEmpty())
            throw new ApplicationException("Для данного абитуриента нет редактируемых данных по зачислению в рамках текущего шага.");
    }

    public void onClickApply() {
        // todo убрать это в dao
        for (EnrEnrollmentStepItemOrderInfo info : getOrders()) {
            IUniBaseDao.instance.get().update(info);
        }
        deactivate();
    }

    // presenter

    public String getItemTitle() {
        return "Абитуриент: " + getItem().getEntity().getRequestedCompetition().getRequest().getEntrant().getFio();
    }

    public String getCompetitionTitle() {
        return "Зачисление на конкурс: " + getItem().getEntity().getRequestedCompetition().getTitle();
    }

    // getters and setters

    public EnrEnrollmentStepItem getItem()
    {
        return item;
    }

    public void setItem(EnrEnrollmentStepItem item)
    {
        this.item = item;
    }

    public EnrEnrollmentStepItemOrderInfo getCurrentOrder()
    {
        return currentOrder;
    }

    public void setCurrentOrder(EnrEnrollmentStepItemOrderInfo currentOrder)
    {
        this.currentOrder = currentOrder;
    }

    public List<EnrEnrollmentStepItemOrderInfo> getOrders()
    {
        return orders;
    }

    public void setOrders(List<EnrEnrollmentStepItemOrderInfo> orders)
    {
        this.orders = orders;
    }

    public ISelectModel getSolutionModel()
    {
        return solutionModel;
    }

    public void setSolutionModel(ISelectModel solutionModel)
    {
        this.solutionModel = solutionModel;
    }
}