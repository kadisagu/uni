/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.ui.AchievementKindAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;

/**
 * @author Nikolay Fedorovskih
 * @since 25.02.2015
 */
@Configuration
public class EnrCatalogCommonsAchievementKindAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
                .create();
    }
}