package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetBase

		// создано обязательное свойство acceptPeopleResidingInCrimea
		{
			// создать колонку
			tool.createColumn("enr14_program_set_base_t", new DBColumn("acceptpeopleresidingincrimea_p", DBType.BOOLEAN));


			//установить равным признаку «Прием лиц, постоянно проживающих в Крыму» из настроек приемной кампании набора ОП
			tool.executeUpdate("update enr14_program_set_base_t " +
							   "set acceptpeopleresidingincrimea_p=" +
							   "(select s.acceptpeopleresidingincrimea_p from enr14_campaign_t c " +
								   "inner join enr14_campaign_settings_t s on c.settings_id = s.id " +
								   "where c.id = enrollmentCampaign_id) where acceptpeopleresidingincrimea_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_program_set_base_t", "acceptpeopleresidingincrimea_p", false);

		}


    }
}