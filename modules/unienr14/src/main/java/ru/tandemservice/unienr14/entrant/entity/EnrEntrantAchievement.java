package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantAchievementKindCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantAchievementGen;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;

/**
 * Индивидуальное достижение
 */
public class EnrEntrantAchievement extends EnrEntrantAchievementGen implements IFisUidByIdOwner, IEntityDebugTitled
{
    @Override
    public String getFisUidTitle()
    {
        return "Инд. достижение абитуриента: " + getType().getAchievementKind().getTitle() + ", " + getEntrant().getFullFio();
    }

    @EntityDSLSupport
    @Override
    public double getMark()
    {
        return 0.001d * getMarkAsLong();
    }

    public void setMark(double mark)
    {
        setMarkAsLong((long) (mark * 1000));
    }

    public ErrorCollector validateMark(ErrorCollector errorCollector, String markFieldId)
    {
        if (getType().isMarked() && getMark() > getType().getAchievementMark())
            errorCollector.add("Балл за индивидуальное достижение не должен превышать максимальный балл из настройки (" + getType().getAchievementMark() + ") для выбранного вида достижения.", markFieldId);

        return errorCollector;
    }

    public ErrorCollector validateDocument(ErrorCollector errorCollector, String documentFieldId)
    {
        return validateDocument(getDocument() == null, errorCollector, documentFieldId);
    }

    public ErrorCollector validateDocument(boolean documentIsNull, ErrorCollector errorCollector, String documentFieldId)
    {
        if(!(EnrEntrantAchievementKindCodes.BS_FINAL_COMPOSITION.equals(getType().getAchievementKind().getCode()) && EnrRequestTypeCodes.BS.equals(getType().getAchievementKind().getRequestType().getCode()))
                && documentIsNull)
        {
            errorCollector.add("Поле «Документ, подтверждающий индивидуальное достижение» должно быть заполнено.", documentFieldId);
        }

        return errorCollector;
    }

    public ErrorCollector validateRequest(ErrorCollector errorCollector, String requestFieldId)
    {

        if(getType().isForRequest() && getRequest() == null)
        {
            errorCollector.add("Поле «№ заявления» должно быть заполнено.", requestFieldId);
        }

        return errorCollector;
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getEntrant().getFio() + " " + getType().getAchievementKind().getShortTitle() + " " + getMarkAsLong() + " (в рейтинг " + getRatingMarkAsLong() + ")";
    }
}