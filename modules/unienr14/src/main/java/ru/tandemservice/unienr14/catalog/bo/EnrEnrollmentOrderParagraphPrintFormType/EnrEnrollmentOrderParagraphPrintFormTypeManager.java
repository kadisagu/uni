/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author azhebko
 * @since 17.07.2014
 */
@Configuration
public class EnrEnrollmentOrderParagraphPrintFormTypeManager extends BusinessObjectManager
{
    public static EnrEnrollmentOrderParagraphPrintFormTypeManager instance(){ return instance(EnrEnrollmentOrderParagraphPrintFormTypeManager.class); }
}