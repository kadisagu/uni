/* $Id: EcExtractComponentDao.java 20463 2011-10-25 05:43:26Z vdanilov $ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 25.05.2011
 */
public class EnrExtractComponentDao implements IExtractComponentDao<EnrAbstractExtract>
{
    @Override
    public void doCommit(EnrAbstractExtract extract, Map parameters)
    {
        EnrOrderManager.instance().dao().doCommit(extract);
    }

    @Override
    public void doRollback(EnrAbstractExtract extract, Map parameters)
    {
        EnrOrderManager.instance().dao().doRollback(extract);
    }
}
