package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_25to26 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrMockExtract

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("enr14_mock_extract_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrMockExtract");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrMockOrder

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("enr14_mock_order_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrMockOrder");

        }



    }
}