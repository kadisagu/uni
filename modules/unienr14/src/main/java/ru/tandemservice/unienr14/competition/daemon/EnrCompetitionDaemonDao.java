package ru.tandemservice.unienr14.competition.daemon;

import com.google.common.collect.Iterables;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.daemon.EnrSettingsDaemonBean;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public class EnrCompetitionDaemonDao extends UniBaseDao implements IEnrCompetitionDaemonDao {

    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    public static final Runnable SETTINGS_DAEMON_ACTIONS = () -> {

        // обновляем настроенные наборы ВИ по наборам ВИ
        try { IEnrCompetitionDaemonDao.instance.get().doUpdateExamSetVariant(); }
        catch (final Exception t) { Debug.exception(t.getMessage(), t); }

    };

    @Override
    protected void initDao()
    {
        // при изменении examSetVariant
        {
            IDSetEventListener wakeUpLitener = event -> {
                if (event.getEventType().isInsert() || event.getMultitude().getAffectedProperties().contains(EnrExamSetVariant.L_EXAM_SET)) {
                    EnrSettingsDaemonBean.DAEMON.registerAfterCompleteWakeUp(event);
                }
            };
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrExamSetVariant.class, wakeUpLitener);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrExamSetVariant.class, wakeUpLitener);
        }
    }

    @Override
    public boolean doUpdateExamSetVariant()
    {
        int updates = 0;
        Debug.begin("EnrCompetitionDaemonDao.doUpdateExamSetVariant");
        try {
            final Session session = lock(EnrExamVariant.class.getName());

            Debug.begin("delete");
            try {

                // удаляем все настроенные ВИ из настроенного набора, если она относится к другому набору
                // (например, если в настроенном наборе изменилась ссылка на набор)
                updates += executeAndClear(
                    new DQLDeleteBuilder(EnrExamVariant.class)
                    .where(ne(property(EnrExamVariant.examSetElement().examSet()), property(EnrExamVariant.examSetVariant().examSet())))
                );

            } finally {
                Debug.end();
            }


            Debug.begin("insert");
            try {
                final List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrExamSetVariant.class, "esv")
                .column(property("esv.id"), "esv_id")

                .fromEntity(EnrExamSetElement.class, "esl")
                .column(property("esl.id"), "esl_id")
                .column(property(EnrExamSetElement.value().id().fromAlias("esl")), "val_id")

                .where(eq(property(EnrExamSetVariant.examSet().fromAlias("esv")), property(EnrExamSetElement.examSet().fromAlias("esl"))))
                .where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EnrExamVariant.class, "rel").column(property("rel.id"))
                    .where(eq(property(EnrExamVariant.examSetVariant().fromAlias("rel")), property("esv")))
                    .where(eq(property(EnrExamVariant.examSetElement().fromAlias("rel")), property("esl")))
                    .buildQuery()
                ))
                .createStatement(session).list();

                if (rows.size() > 0) {

                    // IEnrExamSetElementValue.id -> defaultPassMarkAsLong
                    final Map<Long, Long> passMarkAsLongMap = new HashMap<>();
                    {
                        final Map<Class, Set<Long>> examSetElementValueIdsMap = SafeMap.get(HashSet.class);
                        for (final Object[] row: rows) {
                            final Long id = (Long)row[2];
                            examSetElementValueIdsMap.get(EntityRuntime.getMeta(id).getEntityClass()).add(id);
                        }

                        for (final Map.Entry<Class, Set<Long>> e: examSetElementValueIdsMap.entrySet()) {
                            for (List<Long> ids : Iterables.partition(e.getValue(), DQL.MAX_VALUES_ROW_NUMBER)) {
                                final List<Object[]> dscRows = new DQLSelectBuilder()
                                        .fromEntity(e.getKey(), "x").where(in(property("x", "id"), ids))
                                        .column(property("x", "id"), "value_id")
                                        .column(property("x", IEnrExamSetElementValue.P_DEFAULT_PASS_MARK_AS_LONG), "passMark")
                                        .createStatement(session).list();

                                for (final Object[] row: dscRows) {
                                    passMarkAsLongMap.putIfAbsent((Long) row[0], (Long) row[1]);
                                }
                            }
                        }
                    }

                    final Map<Long, EnrExamSetVariant> examSetVariantMap = getLoadCacheMap(EnrExamSetVariant.class);
                    final Map<Long, EnrExamSetElement> examSetElementMap = getLoadCacheMap(EnrExamSetElement.class);
                    for (final Object[] row: rows) {
                        final EnrExamSetVariant examSetVariant = examSetVariantMap.get((Long) row[0]);
                        final EnrExamSetElement examSetElement = examSetElementMap.get((Long) row[1]);
                        session.save(new EnrExamVariant(examSetVariant, examSetElement, passMarkAsLongMap.get((Long) row[2])));
                        updates++;
                    }

                    session.flush();
                }

            } finally {
                Debug.end();
            }

        } finally {
            Debug.end();
        }
        return updates > 0;
    }

}
