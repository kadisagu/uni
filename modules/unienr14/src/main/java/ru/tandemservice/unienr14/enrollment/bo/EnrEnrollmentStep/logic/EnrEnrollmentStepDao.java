/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentConflictSolution;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepState;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrEnrollmentStepEnrollmentContext;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrEnrollmentStepRecommendationContext;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrSelectionDao;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.EnrXmlEnrollmentUtils;
import ru.tandemservice.unienr14.enrollment.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

/**
 * @author oleyba
 * @since 12/17/13
 */
public class EnrEnrollmentStepDao extends UniBaseDao implements IEnrEnrollmentStepDao
{

    public static final String HACK_PROPERTY = "enr14.enrollment.recommendation.hack.allowed";
    public static final boolean isHachEnabled() {
        return Boolean.TRUE.equals(Boolean.valueOf(ApplicationRuntime.getProperty(EnrEnrollmentStepDao.HACK_PROPERTY)));
    }

    public static final Set<String> INCLUDED_STATE_CODES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
        EnrEntrantStateCodes.EXAMS_PASSED,
        EnrEntrantStateCodes.RECOMMENDED,
        EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED
    )));

    public static final Set<String> EXTRACT_STATUS_OK = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
        ExtractStatesCodes.ACCEPTED,
        ExtractStatesCodes.FINISHED
    )));


    @Override
    public void createNewStep(EnrEnrollmentCampaign campaign, Date enrollmentDate, EnrRequestType requestType, EnrEnrollmentStepKind stepKind, Long percentageAsLong)
    {
        lock("EnrEnrollmentStepDao.createNewStep."+campaign.getId());

        if (null == percentageAsLong) percentageAsLong = 10000L;

        List<EnrEnrollmentStep> steps = getList(
            new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentStep.class, "s")
            .order(property(EnrEnrollmentStep.enrollmentDate().fromAlias("s")))
            .where(eq(property(EnrEnrollmentStep.enrollmentCampaign().fromAlias("s")), value(campaign)))
            .where(eq(property(EnrEnrollmentStep.requestType().fromAlias("s")), value(requestType)))
            .where(ne(property(EnrEnrollmentStep.state().code().fromAlias("s")), value(EnrEnrollmentStepStateCodes.CLOSED)))
        );
        if (steps.size() > 0) {
            EnrEnrollmentStep step = steps.iterator().next();
            throw new ApplicationException("Перед созданием шага зачисления необходимо завершить работу с предыдущим шагом от «"+step.getDateStr()+"».");
        }

        try(final IEventServiceLock eventLock = CoreServices.eventService().lock()) {

            List<EnrCampaignEnrollmentStage> stages = new DQLSelectBuilder()
            .fromEntity(EnrCampaignEnrollmentStage.class, "s")
            .where(eq(property(EnrCampaignEnrollmentStage.enrollmentCampaign().fromAlias("s")), value(campaign)))
            .where(eq(property(EnrCampaignEnrollmentStage.requestType().fromAlias("s")), value(requestType)))
            .where(eq(property(EnrCampaignEnrollmentStage.date().fromAlias("s")), valueDate(enrollmentDate)))
            .createStatement(getSession()).list();

            if (stages.isEmpty()) {
                throw new ApplicationException("В выбранную дату стадий зачисления не предусмотрено.");
            }

            if (EnrEnrollmentStepKindCodes.NO_REC_PERCENTAGE.equals(stepKind.getCode())) {
                for (EnrCampaignEnrollmentStage stage : stages) {
                    if (!EnrCompetitionTypeCodes.MINISTERIAL.equals(stage.getCompetitionType().getCode()) && !EnrCompetitionTypeCodes.CONTRACT.equals(stage.getCompetitionType().getCode())) {
                        throw new ApplicationException("В варианте зачисления «на процент от числа мест» могут участвовать только конкурсы с видом приема «Общий конкурс» и «По договору».");
                    }
                }
            }

            EnrEnrollmentStep step = new EnrEnrollmentStep();
            step.setEnrollmentDate(enrollmentDate);
            step.setState(getCatalogItem(EnrEnrollmentStepState.class, EnrEnrollmentStepStateCodes.START));
            step.setEnrollmentCampaign(campaign);
            step.setRequestType(requestType);
            step.setKind(stepKind);
            step.setPercentageAsLong(percentageAsLong);
            save(step);

            for (EnrCampaignEnrollmentStage stage : stages) {
                EnrEnrollmentStepStage stepStage = new EnrEnrollmentStepStage();
                stepStage.setEnrollmentStage(stage);
                stepStage.setEnrollmentStep(step);
                save(stepStage);
            }

            DQLSelectBuilder competitionBuilder = new DQLSelectBuilder()

            .fromEntity(EnrCompetition.class, "c").column("c.id")
            .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("c")), value(campaign)))

            .fromEntity(EnrEnrollmentStepStage.class, "s")
            .where(eq(property(EnrEnrollmentStepStage.enrollmentStep().fromAlias("s")), value(step)))

            .where(eq(property(EnrCompetition.type().fromAlias("c")), property(EnrEnrollmentStepStage.enrollmentStage().competitionType().fromAlias("s"))))
            .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("c")), property(EnrEnrollmentStepStage.enrollmentStage().programForm().fromAlias("s"))))
            .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().acceptPeopleResidingInCrimea().fromAlias("c")), property(EnrEnrollmentStepStage.enrollmentStage().acceptPeopleResidingInCrimea().fromAlias("s"))))
            .where(eq(property(EnrCompetition.requestType().fromAlias("c")), property(EnrEnrollmentStepStage.enrollmentStage().requestType().fromAlias("s"))));

            List<EnrRatingItem> ratingItems = new DQLSelectBuilder()
            .fromEntity(EnrRatingItem.class, "i")
            .where(in(property(EnrRatingItem.requestedCompetition().competition().fromAlias("i")), competitionBuilder.buildQuery()))
            .createStatement(getSession()).list();

            Map<EnrEntrant, List<EnrEnrollmentExtract>> extractMap = SafeMap.get(ArrayList.class);
            {
                // !!! ВСЕ, не только согласованные (включая отмененные) !!!
                List<EnrEnrollmentExtract> allExtracts = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                .where(in(
                    property(EnrEnrollmentExtract.entity().request().entrant().id().fromAlias("e")),
                    new DQLSelectBuilder()
                    .fromEntity(EnrRatingItem.class, "i")
                    .column(property(EnrRatingItem.requestedCompetition().request().entrant().id().fromAlias("i")))
                    .where(in(property(EnrRatingItem.requestedCompetition().competition().fromAlias("i")), competitionBuilder.buildQuery()))
                    .buildQuery()
                ))
                .createStatement(getSession()).list();
                for (EnrEnrollmentExtract e: allExtracts) {
                    extractMap.get(e.getEntity().getRequest().getEntrant()).add(e);
                }
            }


            Set<EnrCompetition> competitions = new HashSet<>();

            Map<String, EnrEnrollmentConflictSolution> solutionMap = mapCatalog(getList(EnrEnrollmentConflictSolution.class));
            for (EnrRatingItem ratingItem : ratingItems)
            {
                EnrRequestedCompetition requestedCompetition = ratingItem.getRequestedCompetition();
                competitions.add(requestedCompetition.getCompetition());

                EnrEnrollmentStepItem stepItem = new EnrEnrollmentStepItem();
                stepItem.setStep(step);
                stepItem.setEntity(ratingItem);
                stepItem.setEntrantArchived(requestedCompetition.getRequest().getEntrant().isArchival());
                stepItem.setEntrantRequestTakeAwayDocuments(requestedCompetition.getRequest().isTakeAwayDocument());
                stepItem.setIncluded(INCLUDED_STATE_CODES.contains(requestedCompetition.getState().getCode()));
                stepItem.setOriginalIn(ratingItem.isOriginalIn());
                stepItem.setAccepted(requestedCompetition.isAccepted());
                stepItem.setEnrollmentAvailable(requestedCompetition.isEnrollmentAvailable());
                save(stepItem);

                List<EnrEnrollmentExtract> extracts = extractMap.get(requestedCompetition.getRequest().getEntrant());
                for (EnrEnrollmentExtract extract : extracts) {
                    if (!EXTRACT_STATUS_OK.contains(extract.getState().getCode())) {
                        throw new ApplicationException("Нельзя создать шаг зачисления, так как найдены выписки в состояниях, отличных от «Согласована», «Проведена».");
                    }
                    if (requestedCompetition.getCompetition().equals(extract.getEntity().getCompetition())) {
                        if (requestedCompetition.isParallel() != extract.getEntity().isParallel()) {
                            throw new ApplicationException("Нельзя создать шаг зачисления, так как признак параллельности в выписке «"+extract.getOrderInfo()+"»  отличается от признака параллельности в выбранном конкурсе абитуриента.");
                        }
                    }

                    EnrEnrollmentStepItemOrderInfo orderInfo = new EnrEnrollmentStepItemOrderInfo();
                    orderInfo.setItem(stepItem);

                    orderInfo.setExtract(extract);
                    orderInfo.setExtractEntity(extract.getEntity());
                    orderInfo.setExtractCancelled(extract.isCancelled());
                    orderInfo.setOrderInfo(extract.getOrderInfo());

                    String solutionCode = resolveEnrollmentConflict(campaign, ratingItem, extract);
                    orderInfo.setSolution(solutionMap.get(solutionCode));
                    save(orderInfo);
                }
            }

            getSession().flush();

            if (EnrEnrollmentStepKindCodes.NO_REC_PERCENTAGE.equals(stepKind.getCode())) {
                for (EnrCompetition competition : competitions) {
                    EnrEnrollmentStepCompetitionPlan plan = new EnrEnrollmentStepCompetitionPlan();
                    plan.setEnrollmentStep(step);
                    plan.setCompetition(competition);
                    plan.setPlan((int) Math.ceil(0.0001 * step.getPercentageAsLong() * competition.getPlan()));
                    save(plan);
                }
            }
        }
    }


    @Override
    public void doDelete(Long stepId)
    {
        EnrEnrollmentStep step = getNotNull(EnrEnrollmentStep.class, stepId);
        if (!EnrEnrollmentStepStateCodes.START.equals(step.getState().getCode())) {
            throw new ApplicationException("Удаление шага зачисления разрешено только для шагов в состоянии «Сформирован».");
        }
        delete(step);
    }

    @Override
    public void doMoveToNextState(EnrEnrollmentStep step)
    {
        if (!step.isNextStateAllowed()) {
            throw new ApplicationException("Текущее состояние шага зачисления - последнее, перевод далее не предусмотрен.");
        }

        switch (step.getState().getCode()) {
            case EnrEnrollmentStepStateCodes.START:
                doUpdateStepState(step, step.getKind().isUseRecommendation() ? EnrEnrollmentStepStateCodes.RECOMMENDED : EnrEnrollmentStepStateCodes.FORMING_ORDERS);
                return;
            case EnrEnrollmentStepStateCodes.RECOMMENDED:
                doUpdateStepState(step, EnrEnrollmentStepStateCodes.FORMING_ORDERS);
                return;
            case EnrEnrollmentStepStateCodes.FORMING_ORDERS:
                List<EnrEnrollmentExtract> extracts = new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentStepItem.class, "i")
                    .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), value(step)))
                    .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                    .where(eq(property(EnrEnrollmentExtract.entity().fromAlias("e")), property(EnrEnrollmentStepItem.entity().requestedCompetition().fromAlias("i"))))
                    .where(ne(property(EnrEnrollmentExtract.paragraph().order().state().code().fromAlias("e")), value(OrderStatesCodes.FINISHED)))
                    .createStatement(getSession()).setMaxResults(1).list();
                if (!extracts.isEmpty()) {
                    throw new ApplicationException("Не все приказы, в которые включены абитуриенты данного шага зачисления, проведены ("+ extracts.get(0).getOrderInfo() +").");
                }
//                for (EnrEnrollmentStepItem item : getList(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.step(), step)) {
//                    EnrEnrollmentExtract extract = get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity(), item.getEntity().getRequestedCompetition());
//                    if (extract != null && !OrderStatesCodes.FINISHED.equals(extract.getOrder().getState().getCode())) {
//                        throw new ApplicationException("Не все приказы, в которые включены абитуриенты данного шага зачисления, проведены ("+ extract.getOrderInfo() +").");
//                    }
//                }
                doUpdateStepState(step, EnrEnrollmentStepStateCodes.CLOSED);
                return;
        }

        throw new IllegalStateException();
    }

    @Override
    public void doMoveToPrevState(EnrEnrollmentStep step)
    {
        if (!step.isPrevStateAllowed()) {
            throw new ApplicationException("Текущее состояние шага зачисления - первое, откат назад не предусмотрен.");
        }

        switch (step.getState().getCode()) {
            case EnrEnrollmentStepStateCodes.RECOMMENDED:
                doUpdateStepState(step, EnrEnrollmentStepStateCodes.START);
                return;
            case EnrEnrollmentStepStateCodes.FORMING_ORDERS:
                doUpdateStepState(step, step.getKind().isUseRecommendation() ? EnrEnrollmentStepStateCodes.RECOMMENDED : EnrEnrollmentStepStateCodes.START);
                return;
            case EnrEnrollmentStepStateCodes.CLOSED:
                doUpdateStepState(step, EnrEnrollmentStepStateCodes.FORMING_ORDERS);
                return;
        }

        throw new IllegalStateException();
    }

    @Override
    public void doAllowManualMarkEnrolled(EnrEnrollmentStep step)
    {
        if (!step.isAllowManualMarkEnrolledAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        step.setAutoEnrolledMarked(false);
        saveOrUpdate(step);
    }

    @Override
    public void doAutoRecommend(EnrEnrollmentStep step)
    {
        if (!step.isAutoRecommendAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        doRefreshDataInternal(step);
        IEnrSelectionDao selectionDao = EnrEnrollmentStepManager.instance().selectionDao();
        selectionDao.doExecuteSelection(selectionDao.prepareRecommendationContext(step, Debug.isDisplay()));
        step.setAutoRecommended(true);
        saveOrUpdate(step);
    }

    @Override
    public void doAllowManualRecommendation(EnrEnrollmentStep step)
    {
        if (!step.isAllowManualRecommendationAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        step.setAutoRecommended(false);
        saveOrUpdate(step);
    }

    @Override
    public void doClearRecommendation(EnrEnrollmentStep step)
    {
        if (!step.isClearRecommendationAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        for (EnrEnrollmentStepItem item : getList(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.step(), step)) {
            if (item.isRecommended()) {
                item.setRecommended(false);
                saveOrUpdate(item);
            }
        }
        step.setAutoRecommended(false);
        saveOrUpdate(step);
    }

    @Override
    public void doAutoMarkEnrolled(EnrEnrollmentStep step)
    {
        if (!step.isAutoMarkEnrolledAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        doRefreshDataInternal(step);
        IEnrSelectionDao selectionDao = EnrEnrollmentStepManager.instance().selectionDao();
        selectionDao.doExecuteSelection(selectionDao.prepareEnrollmentContext(step, Debug.isDisplay()));
        step.setAutoEnrolledMarked(true);
        saveOrUpdate(step);
    }

    @Override
    public void doClearMarkEnrolled(EnrEnrollmentStep step)
    {
        if (!step.isClearMarkEnrolledAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        for (EnrEnrollmentStepItem item : getList(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.step(), step)) {
            if (item.isShouldBeEnrolled()) {
                item.setShouldBeEnrolled(false);
                saveOrUpdate(item);
            }
        }
        step.setAutoEnrolledMarked(false);
        saveOrUpdate(step);
    }

    @Override
    public void doRefreshData(EnrEnrollmentStep step)
    {
        if (!step.isRefreshDataAllowed()) {
            throw new ApplicationException("Данное действие не разрешено для состояния шага «" + step.getState().getTitle() + "».");
        }

        doRefreshDataInternal(step);
    }

    private void doRefreshDataInternal(EnrEnrollmentStep step)
    {
        for (EnrEnrollmentStepItem item : getList(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.step(), step)) {
            final EnrRequestedCompetition requestedCompetition = item.getEntity().getRequestedCompetition();
            item.setEntrantArchived(requestedCompetition.getRequest().getEntrant().isArchival());
            item.setEntrantRequestTakeAwayDocuments(requestedCompetition.getRequest().isTakeAwayDocument());
            item.setOriginalIn(item.getEntity().isOriginalIn());
            item.setAccepted(requestedCompetition.isAccepted());
            item.setEnrollmentAvailable(requestedCompetition.isEnrollmentAvailable());

            saveOrUpdate(item);
        }

        for (EnrEnrollmentStepItemOrderInfo info : getList(EnrEnrollmentStepItemOrderInfo.class, EnrEnrollmentStepItemOrderInfo.item().step(), step)) {
            info.setExtractEntity(info.getExtract().getEntity());
            info.setExtractCancelled(info.getExtract().isCancelled());
        }

    }

    // --------------

    /* @return EnrEnrollmentConflictSolution.code */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686876")
    private String resolveEnrollmentConflict(EnrEnrollmentCampaign campaign, EnrRatingItem ratingItem, EnrEnrollmentExtract extract)
    {
        EnrRequestedCompetition thisRc = ratingItem.getRequestedCompetition();
        EnrRequestedCompetition thatRc = extract.getEntity();

        boolean sameParallelFlag = (thisRc.isParallel() == thatRc.isParallel());                          // один и тот же признак «параллельноси»
        boolean sameRequestType = thisRc.getRequest().getType().equals(thatRc.getRequest().getType());    // один и тот же вид заявления

        boolean sameRequestAndParallel = sameParallelFlag && sameRequestType;
        if (!sameRequestAndParallel)
        {
            // зачислить повторно (выписки не влияют друг на друга)
            return EnrEnrollmentConflictSolutionCodes.ENROLL;
        }
        else
        {
            if (extract.isCancelled())
            {
                // если выписка отменена
                boolean sameRequestedCompetition = thisRc.equals(thatRc); // один и тот же ВК
                if (sameRequestedCompetition) { return EnrEnrollmentConflictSolutionCodes.SKIP; }
            }
            else
            {
                // если выписка не отменена
                boolean ecReenrollEnabled = campaign.getSettings().isReenrollByPriorityEnabled();    // для ПК разрешено перезачисление
                boolean extractPriorityHigher = (thisRc.getPriority() >= thatRc.getPriority());      // приоритет выписки выше (численно меньше)
                if (!ecReenrollEnabled) { return EnrEnrollmentConflictSolutionCodes.SKIP; }
                if (extractPriorityHigher) { return EnrEnrollmentConflictSolutionCodes.SKIP; }
            }

            // иначе, перезачислять
            return EnrEnrollmentConflictSolutionCodes.REENROLL;
        }
    }

    private void doUpdateStepXmlRecommendationState(EnrEnrollmentStep step, boolean needsValue) {
        if (needsValue) {
            if (null == step.getZipXmlRecommendationState()) {
                EnrEnrollmentStepRecommendationContext context = EnrEnrollmentStepManager.instance().selectionDao().prepareRecommendationContext(step, false);
                byte[] xml = EnrXmlEnrollmentUtils.toXml(context.buildEnrXmlEnrollmentStepState());
                step.setXmlRecommendationState(xml);
            }
        } else {
            step.setZipXmlRecommendationState(null);
        }
    }

    private void doUpdateStepXmlEnrollmentnState(EnrEnrollmentStep step, boolean needsValue) {
        if (needsValue) {
            if (null == step.getZipXmlEnrollmentState()) {
                EnrEnrollmentStepEnrollmentContext context = EnrEnrollmentStepManager.instance().selectionDao().prepareEnrollmentContext(step, false);
                byte[] xml = EnrXmlEnrollmentUtils.toXml(context.buildEnrXmlEnrollmentStepState());
                step.setXmlEnrollmentState(xml);
            }
        } else {
            step.setZipXmlEnrollmentState(null);
        }
    }

    /** {step.state.code -> [ xmlRec.isNotNull, xmlEnr.isNotNull ] } */
    private static final Map<String, Boolean[]> STATE_TO_XML_MAP = new HashMap<>();
    static {
        final Object[][] DATA = new Object[][] {
            { EnrEnrollmentStepStateCodes.START, false, false },
            { EnrEnrollmentStepStateCodes.RECOMMENDED, true, false },
            { EnrEnrollmentStepStateCodes.FORMING_ORDERS, true, true },
            { EnrEnrollmentStepStateCodes.CLOSED, true, true },
        };
        for (Object[] row: DATA) {
            STATE_TO_XML_MAP.put((String)row[0], new Boolean[] { (Boolean)row[1], (Boolean)row[2] });
        }
    }

    private void doUpdateStepState(EnrEnrollmentStep step, String stateCode)
    {
        step.setState(getCatalogItem(EnrEnrollmentStepState.class, stateCode));
        Boolean[] required = STATE_TO_XML_MAP.get(stateCode);
        doUpdateStepXmlRecommendationState(step, required[0]);
        doUpdateStepXmlEnrollmentnState(step, required[1]);
        saveOrUpdate(step);
    }


    @Override
    public void doRefreshNecessaryXml() {
        Session session = getSession();
        final List<Long> todoList = new ArrayList<Long>();
        for (Map.Entry<String, Boolean[]> e: STATE_TO_XML_MAP.entrySet()) {
            todoList.addAll(
                new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStep.class, "s")
                .column(property("s.id"))
                .where(eq(property(EnrEnrollmentStep.state().code().fromAlias("s")), value(e.getKey())))
                .where(or(
                    (e.getValue()[0] ? isNull(property(EnrEnrollmentStep.zipXmlRecommendationState().fromAlias("s"))) : isNotNull(property(EnrEnrollmentStep.zipXmlRecommendationState().fromAlias("s")))),
                    (e.getValue()[1] ? isNull(property(EnrEnrollmentStep.zipXmlEnrollmentState().fromAlias("s"))) : isNotNull(property(EnrEnrollmentStep.zipXmlEnrollmentState().fromAlias("s"))))
                ))
                .createStatement(session).<Long>list()
            );
        }

        for (Long id: todoList) {
            EnrEnrollmentStep step = get(EnrEnrollmentStep.class, id);
            doUpdateStepState(step, step.getState().getCode());
            session.flush();
            session.clear();
        }

    }


    @Override
    public void doSelectEnrollmentHack5696(EnrEnrollmentStep step) {
        if (!EnrEnrollmentStepDao.isHachEnabled()) { throw new IllegalStateException(); }
        if (!EnrEnrollmentStepStateCodes.RECOMMENDED.equals(step.getState().getCode())) { throw new IllegalStateException(); }
        if (!EnrEnrollmentStepKindCodes.REC_AND_ADHERE.equals(step.getKind().getCode())) { throw new IllegalStateException(); }

        doRefreshDataInternal(step);

        for (EnrEnrollmentStepItem item : getList(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.step(), step)) {
            boolean shouldBeEnrolled = item.isRecommended();
            if (shouldBeEnrolled && item.isEntrantArchived()) { shouldBeEnrolled = false; }
            if (shouldBeEnrolled && item.isEntrantRequestTakeAwayDocuments()) { shouldBeEnrolled = false; }
            if (shouldBeEnrolled && !item.isEnrollmentAvailable()) {
                shouldBeEnrolled = false;
            }

            item.setShouldBeEnrolled(shouldBeEnrolled);
            saveOrUpdate(item);
        }
    }


}