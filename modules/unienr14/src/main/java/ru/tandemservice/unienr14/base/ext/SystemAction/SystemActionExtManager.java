/**
 *$Id: SystemActionExtManager.java 26910 2013-04-10 05:45:04Z vdanilov $
 */
package ru.tandemservice.unienr14.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;

import ru.tandemservice.unienr14.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        IItemListExtensionBuilder<SystemActionDefinition> itemListExtension = itemListExtension(_systemActionManager.buttonListExtPoint());
        if (Debug.isDisplay() && Boolean.valueOf(ApplicationRuntime.getProperty("unienr14.randomData.allowed"))) {
            itemListExtension.add("unienr14_fillEduRandom", new SystemActionDefinition("unienr14", "fillEduRandom", "onClickFillEduRandom", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
            itemListExtension.add("unienr14_fillEcRandom", new SystemActionDefinition("unienr14", "fillEcRandom", "onClickFillEcRandom", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
            itemListExtension.add("unienr14_fillEcRandomEntrant", new SystemActionDefinition("unienr14", "fillEcRandomEntrant", "onClickFillEcRandomEntrant", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
            itemListExtension.add("unienr14_deleteEcEntrants", new SystemActionDefinition("unienr14", "deleteEcEntrants", "onClickDeleteEcEntrants", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
            itemListExtension.add("unienr14_deleteEnrollmentCampaign", new SystemActionDefinition("unienr14", "cleanEnrollmentCampaign", "onClickCleanEnrollmentCampaign", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        }
        itemListExtension.add("unienr14_exportCampaignInfo", new SystemActionDefinition("unienr14", "exportCampaignInfo", "onClickExportCampaignInfo", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unienr14_exportRatings", new SystemActionDefinition("unienr14", "exportRatings", "onClickExportRatings", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unienr14_exportOrders", new SystemActionDefinition("unienr14", "exportOrders", "onClickExportOrders", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unienr14_resetNumberQueue", new SystemActionDefinition("unienr14", "resetNumberQueue", "onClickResetNumberQueue", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        return itemListExtension.create();
    }
}
