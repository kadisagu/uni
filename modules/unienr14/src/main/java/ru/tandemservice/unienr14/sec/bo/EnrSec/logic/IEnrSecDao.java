/* $Id:$ */
package ru.tandemservice.unienr14.sec.bo.EnrSec.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;

/**
 * @author oleyba
 * @since 6/11/15
 */
public interface IEnrSecDao extends INeedPersistenceSupport
{
    Collection<IEntity> getLocalRoleContexts(EnrEntrant entity, String localRoleScope);

    Collection<IEntity> getLocalRoleContexts(EnrEntrantRequest entity, String localRoleScope);

    Collection<IEntity> getLocalRoleContexts(EnrRequestedCompetition entity, String localRoleScope);

    Collection<IEntity> getLocalRoleContexts(EnrEnrollmentCampaign entity, String localRoleScope);

    Collection<IEntity> getLocalRoleContexts(EnrEnrollmentCommission entity, String localRoleScope);
}