package ru.tandemservice.unienr14.sec.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import org.tandemframework.shared.organization.sec.entity.RoleConfig;
import ru.tandemservice.unienr14.sec.entity.gen.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.LocalRoleAddEdit.EnrEnrollmentCampaignLocalRoleAddEdit;

/** @see ru.tandemservice.unienr14.sec.entity.gen.RoleConfigLocalEnrCampaignGen */
public class RoleConfigLocalEnrCampaign extends RoleConfigLocalEnrCampaignGen
{
    @Override
    public ILocalRoleContext getRoleContext()
    {
        return getEnrollmentCampaign();
    }

    @Override
    public String getTitle()
    {
        return "Локальная роль на ПК";
    }

    @Override
    public void doRoleTitleUniqueCheck()
    {
        if (new DQLSimple<>(RoleConfigLocalEnrCampaign.class)
            .where(RoleConfigLocalEnrCampaign.role().title(), getTitle())
            .where(RoleConfigLocalEnrCampaign.enrollmentCampaign(), getEnrollmentCampaign())
            .whereNot(RoleConfig.role().id(), getRole().getId())
            .exists()) {
            throw new ApplicationException("Роль на ПК с таким названием уже существует.");
        }
    }

    @Override
    public String getFileExtension()
    {
        return "ec-pk";
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditFormBc()
    {
        return EnrEnrollmentCampaignLocalRoleAddEdit.class;
    }
}