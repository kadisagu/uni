/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List.EnrEntrantList;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 05.05.2016
 */
public class EntrantRequestAcceptPrincipalHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String PARAM_ENROLLMENT_COMMISSION = "enrCommissions";

    public EntrantRequestAcceptPrincipalHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        List<EnrEnrollmentCommission> commissions = context.get(EnrEntrantList.PARAM_ENROLLMENT_COMMISSION);

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "en")
                .column(DQLFunctions.min(property("en.id")))
                .column(property("en", EnrEntrantRequest.acceptRequest()))
                .distinct();

        String filter = input.getComboFilterByValue();
        if (StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property(EnrEntrantRequest.acceptRequest().fromAlias("en")), value(CoreStringUtils.escapeLike(filter))));
        builder.where(eq(property("en", EnrEntrantRequest.entrant().enrollmentCampaign()), value(campaign)));
        if (commissions != null && !commissions.isEmpty())
            builder.where(in(property("en", EnrEntrantRequest.enrollmentCommission()), commissions));
        builder.group(property("en", EnrEntrantRequest.acceptRequest()));
        builder.order(property(EnrEntrantRequest.acceptRequest().fromAlias("en")));

        final DQLListResultBuilder<Object[]> resultBuilder = new DQLListResultBuilder<>(builder, 50);
        int i=0;
        final List<DataWrapper> patchedList = new ArrayList<>();
        for (Object[] entry : resultBuilder.findOptions().getObjects())
        {
            if (entry != null && !StringUtils.isEmpty((String) entry[1]))
                patchedList.add(new DataWrapper((Long)entry[0], (String)entry[1]));
        }

        context.put(UIDefines.COMBO_OBJECT_LIST, patchedList);

        return super.execute(input, context);
    }

}