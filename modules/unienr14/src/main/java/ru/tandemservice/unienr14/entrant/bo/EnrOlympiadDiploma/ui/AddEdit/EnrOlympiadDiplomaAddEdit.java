/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.entity.*;

/**
 * @author oleyba
 * @since 5/3/13
 */
@Configuration
public class EnrOlympiadDiplomaAddEdit extends BusinessComponentManager
{
    public static final String DS_OLYMPIAD_TYPE = "olympiadTypeDS";
    public static final String DS_OLYMPIAD = "olympiadDS";
    public static final String DS_OLYMPIAD_HONOUR = "olympiadHonourDS";
    public static final String DS_OLYMPIAD_SUBJECT = "olympiadSubjectDS";
    public static final String DS_COUNTRY = "countryDS";
	public static final String DS_SETTLEMENT = "settlementDS";
    public static final String DS_TEAM_TYPE = "teamTypeDS";
    public static final String DS_PROFILE_OLIMPIAD = "olympiadProfileDisciplineDS";
    public static final String DS_STATE_EXAM_SUBJECT = "stateExamSubjectDS";

    public static Long RUSSIAN_TEAM_ID = 0L;
    public static Long UKRAINE_TEAM_ID = 1L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS(DS_OLYMPIAD_TYPE, olympiadTypeDSHandler()))
            .addDataSource(selectDS(DS_OLYMPIAD, olympiadDSHandler()).addColumn(EnrOlympiad.titleWithYear().s()))
            .addDataSource(selectDS(DS_OLYMPIAD_HONOUR, EnrCatalogCommonsManager.instance().olympiadHonourDSHandler()).addColumn(EnrOlympiadHonour.title().s()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_OLYMPIAD_SUBJECT, getName(), EnrOlympiadSubject.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(DS_COUNTRY, AddressBaseManager.instance().countryComboDSHandler()))
		        .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SETTLEMENT, getName(), AddressItem.settlementComboDSHandler(getName())))
                .addDataSource(selectDS(DS_TEAM_TYPE, teamTypeDSHandler()))
                .addDataSource(selectDS(DS_PROFILE_OLIMPIAD, profileOlympiadDSHandler()))
                .addDataSource(selectDS(DS_STATE_EXAM_SUBJECT, stateExamSubjectDS()))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> olympiadTypeDSHandler()
    {
        return EnrOlympiadType.defaultSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> olympiadDSHandler()
    {
        return EnrOlympiad.defaultSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> teamTypeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Lists.newArrayList(new IdentifiableWrapper(RUSSIAN_TEAM_ID, "Российской Федерации"),  new IdentifiableWrapper(UKRAINE_TEAM_ID, "Украины")));

    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> profileOlympiadDSHandler()
    {
        return EnrOlympiadProfileDiscipline.defaultSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> stateExamSubjectDS()
    {
        return EnrStateExamSubject.defaultSelectDSHandler(getName());
    }

}
