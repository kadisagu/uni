/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderSearchListDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.*;

/**
 * @author oleyba
 * @since 7/7/14
 */
public class EnrOrderListUI extends UIPresenter
{

    private EnrEnrollmentCampaign _enrollmentCampaign;

    private ISelectModel _orderTypeListModel;
    private List<OrderStates> _orderStateList;


    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setOrderTypeListModel(new LazySimpleSelectModel<>(EnrOrderType.class).setSortProperty(EnrOrderType.code().s()));
        setOrderStateList(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(OrderStates.class));

        CommonFilterAddon util = getFilterUtil();
        if (util != null)
        {
            util
                    .configDoubleWidthFilters(false)
                    .configUseEnableCheckbox(false)
                    .configSettings(this.getSettingsKey());

            util.clearFilterItems();
            util
                    .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                    .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

            configWhereFilters();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EnrOrderList.ORDER_LIST_DS))
        {
            dataSource.put(EnrOrderSearchListDSHandler.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
            dataSource.put(EnrOrderSearchListDSHandler.FILTER_UTIL, getFilterUtil());
            dataSource.put(EnrOrderSearchListDSHandler.CREATE_DATE_FILTER_NAME, getSettings().get(EnrOrderSearchListDSHandler.CREATE_DATE_FILTER_NAME));
            dataSource.put(EnrOrderSearchListDSHandler.COMMIT_DATE_FILTER_NAME, getSettings().get(EnrOrderSearchListDSHandler.COMMIT_DATE_FILTER_NAME));
            dataSource.put(EnrOrderSearchListDSHandler.NUMBER_FILTER_NAME, getSettings().get(EnrOrderSearchListDSHandler.NUMBER_FILTER_NAME));
            dataSource.put(EnrOrderSearchListDSHandler.ORDER_TYPE_FILTER_NAME, getSettings().get(EnrOrderSearchListDSHandler.ORDER_TYPE_FILTER_NAME));
            dataSource.put(EnrOrderSearchListDSHandler.ORDER_STATE_FILTER_NAME, getSettings().get(EnrOrderSearchListDSHandler.ORDER_STATE_FILTER_NAME));
        }
    }

    // actions

    public void onClickOrdersSearch()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickOrdersClear()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.clearSettings();

        clearSettings();
        configWhereFilters();
        onClickOrdersSearch();
    }

    // listeners

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        configWhereFilters();
    }

    public void onClickAddEnrollmentOrder()
    {
        _uiActivation.asRegion(EnrOrderAddEdit.class.getSimpleName()).activate();
    }

    public void onClickPrint()
    {
        EnrOrderManager.instance().dao().getDownloadPrintForm(getListenerParameterAsLong());
    }

    public void onClickEditEnrollmentOrder()
    {
        _uiActivation.asRegion(EnrOrderAddEdit.class.getSimpleName())
            .parameter("orderId",getListenerParameterAsLong())
            .activate();
    }

    public void onClickDeleteEnrollmentOrder()
    {
        EnrOrderManager.instance().dao().deleteOrder(getListenerParameterAsLong());
    }

    public void onClickSendCheckedToCoordination()
    {
        IPrincipalContext principalContext = getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable)&& !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");


        List<Long> selectedIds = UniBaseUtils.getIdList(((BaseSearchListDataSource) getConfig().getDataSource(EnrOrderList.ORDER_LIST_DS)).getOptionColumnSelectedObjects("selected"));;
        if (selectedIds.isEmpty())
            throw new ApplicationException("Выберите приказы для совершения данного действия.");
        EnrOrderManager.instance().dao().doSendToCoordination(selectedIds, principalContext instanceof Admin? null : (IPersistentPersonable) principalContext);
    }

    public void onClickCommitChecked()
    {
        List<Long> selectedIds = UniBaseUtils.getIdList(((BaseSearchListDataSource) getConfig().getDataSource(EnrOrderList.ORDER_LIST_DS)).getOptionColumnSelectedObjects("selected"));;
        if (selectedIds.isEmpty())
            throw new ApplicationException("Выберите приказы для совершения данного действия.");
        EnrOrderManager.instance().dao().doCommit(selectedIds);
    }

    public void onClickCreateStudents()
    {
        List<Long> selectedIds = UniBaseUtils.getIdList(((BaseSearchListDataSource) getConfig().getDataSource(EnrOrderList.ORDER_LIST_DS)).getOptionColumnSelectedObjects("selected"));;
        if (selectedIds.isEmpty())
            throw new ApplicationException("Выберите приказы для совершения данного действия.");
        EnrOrderManager.instance().dao().doCreateStudents(selectedIds);
    }

    // helpers

    private CommonFilterAddon getFilterUtil() { return (CommonFilterAddon) this.getConfig().getAddon(EnrOrderList.COMPETITION_FILTER_ADDON); }

    private void configWhereFilters()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
        {
            util.clearWhereFilter();
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
        }
    }

    public boolean isNothingSelected() { return null == getEnrollmentCampaign(); }

    // getters and setters

    public List<OrderStates> getOrderStateList()
    {
        return _orderStateList;
    }

    public void setOrderStateList(List<OrderStates> orderStateList)
    {
        _orderStateList = orderStateList;
    }

    public ISelectModel getOrderTypeListModel()
    {
        return _orderTypeListModel;
    }

    public void setOrderTypeListModel(ISelectModel orderTypeListModel)
    {
        _orderTypeListModel = orderTypeListModel;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}