/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;

/**
 * @author azhebko
 * @since 14.07.2014
 */
public class Model extends DefaultScriptCatalogAddEditModel<EnrOrderPrintFormType>
{
    private ISelectModel _orderTypeModel;

    public ISelectModel getOrderTypeModel(){ return _orderTypeModel; }
    public void setOrderTypeModel(ISelectModel orderTypeModel){ _orderTypeModel = orderTypeModel; }

    public boolean isUserScriptAllowed(){ return this.getCatalogItem().getOrderType() != null; }
}