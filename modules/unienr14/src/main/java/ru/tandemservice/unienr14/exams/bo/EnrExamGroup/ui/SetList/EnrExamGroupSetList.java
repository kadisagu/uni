/**
 *$Id: EnrExamGroupSetList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;

/**
 * @author Alexander Shaburov
 * @since 17.05.13
 */
@Configuration
public class EnrExamGroupSetList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String EXAM_GROUP_SET_SEARCH_DS = "examGroupSetSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(EXAM_GROUP_SET_SEARCH_DS, examGroupSetSearchDSColumns(), examGroupSetSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint examGroupSetSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_GROUP_SET_SEARCH_DS)
                .addColumn(textColumn("title", EnrExamGroupSet.periodTitle()))
                .addColumn(textColumn("size", EnrExamGroupSet.size()))
                .addColumn(toggleColumn("open", EnrExamGroupSet.open())
                        .permissionKey("enr14ExamGroupSetListCloseExamGroupSet")
                        .toggleOnListener("onToggleOpen")
                        .toggleOffListener("onToggleOpen"))

                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditExamGroupSet")
                    .permissionKey("enr14ExamGroupSetListEditExamGroupSet"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteExamGroupSet")
                    .alert(new FormattedMessage("examGroupSetSearchDS.delete.alert", EnrExamGroupSet.periodTitle()))
                    .permissionKey("enr14ExamGroupSetListDeleteExamGroupSet"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> examGroupSetSearchDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroupSet.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final DSOutput output = super.execute(input, context);
                // В таблице показываем всегда все строки +1 пустую
                output.setTotalSize(output.getCountRecord() + 1);
                output.setCountRecord(output.getCountRecord() + 1);

                return output;
            }
        }
                .where(EnrExamGroupSet.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .order(EnrExamGroupSet.id(), OrderDirection.desc)
                .pageable(false);
    }
}
