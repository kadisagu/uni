/**
 *$Id: EnrEnrollmentOrderReasonBasicSearchDSHandler.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.util.ToString;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
public class EnrEnrollmentOrderReasonBasicSearchDSHandler extends DefaultSearchDataSourceHandler
{

    public EnrEnrollmentOrderReasonBasicSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String NUMBER_COLUMN = "number";
    public static final String BASICS_TITLE_COLUMN = "basicsTitle";
    public static final String BASICS_CODE_COLUMN = "basicsCode";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder reasonDQL = new DQLSelectBuilder().fromEntity(EnrOrderReason.class, "r").column(property("r"));

        final DSOutput output = DQLSelectOutputBuilder.get(input, reasonDQL, context.getSession()).pageable(false).build();

        return wrap(output, context);
    }

    protected DSOutput wrap(DSOutput output, ExecutionContext context)
    {
        final List<Object[]> dataList = new DQLSelectBuilder().fromEntity(EnrOrderReasonToBasicsRel.class, "rel")
                .column(property(EnrOrderReasonToBasicsRel.reason().id().fromAlias("rel")))
                .column(property(EnrOrderReasonToBasicsRel.basic().fromAlias("rel")))
                .order(property(EnrOrderReasonToBasicsRel.basic().title().fromAlias("rel")))
                .createStatement(context.getSession()).list();

        Map<Long, List<EnrOrderBasic>> reason2CodesMap = SafeMap.get(ArrayList.class);
        for (Object[] objects : dataList)
        {
            final Long reasonId = (Long) objects[0];
            final EnrOrderBasic basicCode = (EnrOrderBasic) objects[1];

            reason2CodesMap.get(reasonId)
                    .add(basicCode);
        }

        int number = 1;
        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            wrapper.setProperty(NUMBER_COLUMN, number++);
            wrapper.setProperty(BASICS_TITLE_COLUMN, CoreStringUtils.join(reason2CodesMap.get(wrapper.getId()), new ToString<EnrOrderBasic>()
            {
                @Override
                public String toString(EnrOrderBasic value)
                {
                    return value.getTitle();
                }
            }, "\n"));
            wrapper.setProperty(BASICS_CODE_COLUMN, CommonBaseUtil.getPropertiesList(reason2CodesMap.get(wrapper.getId()), EnrOrderBasic.code().s()));
        }

        return output;
    }
}
