/**
 *$Id: EnrExamSetDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.daemon.EnrSettingsDaemonBean;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 16.04.13
 */
public class EnrExamSetDao extends UniBaseDao implements IEnrExamSetDao
{
    @Override
    public Map<EnrExamSet, List<EnrExamSetElement>> examSet2ElementsMap(EnrExamSet... examSets)
    {
        final List<EnrExamSet> examSetList = Arrays.asList(examSets);

        final Map<EnrExamSet, List<EnrExamSetElement>> examSet2ElementsMap = new HashMap<>();
        BatchUtils.execute(examSetList, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            final List<EnrExamSetElement> list = new DQLSelectBuilder().fromEntity(EnrExamSetElement.class, "e").column("e")
            .where(in(property(EnrExamSetElement.examSet().fromAlias("e")), elements))
            .createStatement(getSession()).list();

            for (EnrExamSetElement element : list)
                SafeMap.safeGet(examSet2ElementsMap, element.getExamSet(), ArrayList.class)
                .add(element);
        });

        for (List<EnrExamSetElement> list : examSet2ElementsMap.values())
            Collections.sort(list, EnrExamSetElement.Comparators.byNumber);

        return examSet2ElementsMap;
    }

    @Override
    public Long saveOrUpdateExamSet(EnrExamSet examSet, List<? extends IExamSetElementWrapper> elementWrapperList)
    {
        final List<EnrExamSetElement> elementList = new ArrayList<>(elementWrapperList.size());
        for (int i=0; i<elementWrapperList.size(); i++) {
            elementList.add(createExamSetElement(examSet, elementWrapperList.get(i), 1+i));
        }

        examSet.setKey(generateExamSetKey(elementList));
        saveOrUpdate(examSet);

        List<EnrExamSetElement> dbExamSetElementList = new DQLSelectBuilder().fromEntity(EnrExamSetElement.class, "e").column(property("e"))
        .where(eq(property(EnrExamSetElement.examSet().fromAlias("e")), value(examSet)))
        .order(property(EnrExamSetElement.number().fromAlias("e")))
        .createStatement(getSession()).list();

        {
            // сначала освобождаем старые номера (переномерация объекта случится ниже, там всем им выставятся нужные номера)
            // и убираем со всех профильность, что бы не падал uk_enr_exam_set_el_profile
            int number = Integer.MAX_VALUE;
            for (EnrExamSetElement e: dbExamSetElementList) {
                e.setNumber(number--);
            }
            getSession().flush();
        }

        new MergeAction.SessionMergeAction<INaturalId, EnrExamSetElement>() {
            @Override protected INaturalId key(EnrExamSetElement source) { return source.getNaturalId(); }
            @Override protected EnrExamSetElement buildRow(EnrExamSetElement source) { return source; }
            @Override protected void fill(EnrExamSetElement target, EnrExamSetElement source) { target.update(source, false); }
        }.merge(dbExamSetElementList, elementList);

        getSession().flush();

        // пробуждаем демона после сохранения
        EnrSettingsDaemonBean.DAEMON.registerAfterCompleteWakeUp(getSession());
        return examSet.getId();
    }

    @Override
    public EnrExamSet doGetEmptySet(EnrEnrollmentCampaign campaign)
    {
        EnrExamSet empty = new DQLSelectBuilder()
        .fromEntity(EnrExamSet.class, "s")
        .where(eq(property(EnrExamSet.enrollmentCampaign().fromAlias("s")), value(campaign)))
        .where(eq(property(EnrExamSet.key().fromAlias("s")), value("")))
        .createStatement(getSession()).uniqueResult();

        if (null != empty)
            return empty;

        empty = new EnrExamSet();
        empty.setEnrollmentCampaign(campaign);
        empty.setKey("");
        save(empty);
        return empty;
    }

    protected EnrExamSetElement createExamSetElement(EnrExamSet examSet, IExamSetElementWrapper wrapper, Number number)
    {
        final EnrExamSetElement element = new EnrExamSetElement();
        element.setExamSet(examSet);
        element.setNumber(number.intValue());
        element.setValue(wrapper.getElementValue());
        element.setType(wrapper.getExamType());
        return element;
    }

    /**
     * Генерирует ключ для Набора ВИ.
     * Правило генерации ключа:<p/>
     * - генерируем ключ ВИ:<p/>
     * -- для группы: берем коды дисциплин (по справочнику), входящих в группу, упорядочиваем лексикографически по возрастанию, конкатенируем через _, затем добавляем еще _ и код типа ВИ. Если группа профильная, то еще подчеркивание и букву p. Т.е получаем ключ вида 02_04_p<p/>
     * -- для дисциплины: аналогично группе, но дисциплина только одна, соответственно код дисциплины только один<p/>
     * - сортируем ключи ВИ лексикографически по возрастанию<p/>
     * - конкатенируем все ключи через ., т.е. получаем строку ключа вида 01.02_04_p.05<p/>
     * @param elementList список Элементов набора ВИ
     * @return ключ вида: 01.02_04_p.05
     */
    protected String generateExamSetKey(List<EnrExamSetElement> elementList)
    {
        final List<Object[]> disciplineGroupIdList = new DQLSelectBuilder().fromEntity(EnrCampaignDisciplineGroupElement.class, "ge")
        .column(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("ge")))
        .column(property(EnrCampaignDisciplineGroupElement.discipline().discipline().code().fromAlias("ge")))
        .where(in(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("ge")), CommonBaseUtil.getPropertiesList(elementList, EnrExamSetElement.value().id().s())))
        .createStatement(getSession()).list();

        final Map<Long, List<String>> groupId2DiscCodesMap = new HashMap<>();
        for (Object[] objects : disciplineGroupIdList)
        {
            final Long groupId = (Long) objects[0];
            final String discCode = (String) objects[1];

            SafeMap.safeGet(groupId2DiscCodesMap, groupId, LinkedList.class)
            .add(discCode);
        }

        for (List<String> list : groupId2DiscCodesMap.values())
            Collections.sort(list);

        final List<String> keyList = new ArrayList<>();
        for (EnrExamSetElement element : elementList)
        {
            StringBuilder elementKey = new StringBuilder();
            if (element.getValue() instanceof EnrCampaignDisciplineGroup)
            {
                final List<String> discCodeList = groupId2DiscCodesMap.get(element.getValue().getId());
                elementKey.append(CoreStringUtils.join(discCodeList, "_")).append("_").append(element.getType().getCode());
            }
            else if (element.getValue() instanceof EnrCampaignDiscipline)
            {
                elementKey
                .append(((EnrCampaignDiscipline) element.getValue()).getDiscipline().getCode())
                .append("_").append(element.getType().getCode());
            }

            keyList.add(elementKey.toString());
        }

        Collections.sort(keyList);
        return CoreStringUtils.join(keyList, ".");
    }

    @Override
    public Map<Long, IExamSetSettings> getExamSetContent(Collection<Long> variantIds)
    {
        final Session session = getSession();
        final List<EnrExamPassForm> forms = getCatalogItemListOrderByCode(EnrExamPassForm.class);
        final Map<Long, IExamSetSettings> result = new HashMap<>(variantIds.size());

        BatchUtils.execute(variantIds, 128, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> ids) {

                final Map<Long, Map<EnrExamVariant, List<EnrExamVariantPassForm>>> examDataMap = getExamData(ids);

                final Map<Long, List<EnrExamSetElement>> examSetMap = getExamSetData(ids);

                for (Long variantId : ids) {
                    List<EnrExamSetElement> examSetElements = examSetMap.get(variantId);

                    final EnrExamSetVariant examSetVariant = get(EnrExamSetVariant.class, variantId);

                    final ExamSetSettings settings = new ExamSetSettings(examSetVariant);
                    result.put(variantId, settings);

                    settings.setElementList(new ArrayList<>());

                    final Map<EnrExamVariant, List<EnrExamVariantPassForm>> cgExamMap = examDataMap.get(variantId);



                    if (examSetElements != null) {
                        for (EnrExamSetElement examSetElement : examSetElements) {

                            final Map<EnrExamPassForm, Boolean> elementPassFormMap = initPassFormMap(forms, false);

                            final StringBuilder elementTitle = new StringBuilder();
                            elementTitle.append(examSetElement.getTitle());

                            final EnrExamVariant exam = findExam(cgExamMap, examSetElement);

                            if (exam == null) {
                                elementTitle.append(" (не настроено)");
                                settings.setNotFullySetup(true);
                            }
                            else {
                                elementTitle.append(" (зач. балл - ").append(exam.getPassMarkAsString()).append("; фс");

                                // данные по формам
                                processPassForms(elementTitle, elementPassFormMap, cgExamMap.get(exam));

                                elementTitle.append(')');
                            }

                            final ExamSetElementSettings elementSettings = new ExamSetElementSettings(examSetElement, elementTitle.toString());
                            elementSettings.setPassFormMap(elementPassFormMap);
                            elementSettings.setPassMarkAsLong(exam == null ? 0 : exam.getPassMarkAsLong());
                            elementSettings.setPriority(exam == null ? examSetElement.getNumber() : exam.getPriority());
                            elementSettings.setCgExam(exam);
                            settings.getElementList().add(elementSettings);

                        }
                    }

                    Collections.sort(settings.getElementList(), (o1, o2) -> (o1.getPriority() - o2.getPriority()));

                    final StringBuilder examSetSettingsTitle = new StringBuilder();
                    for (IExamSetElementSettings elementSettings : settings.getElementList()) {
                        if (examSetSettingsTitle.length() > 0) { examSetSettingsTitle.append("\n"); }
                        examSetSettingsTitle.append(elementSettings.getTitle());
                    }

                    settings.setTitle(examSetSettingsTitle.toString());
                }
            }

            private Map<Long, List<EnrExamSetElement>> getExamSetData(Collection<Long> ids)
            {
                final Map<Long, List<EnrExamSetElement>> examSetMap = new HashMap<>();
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrExamSetElement.class, "e")
                .joinEntity("e", DQLJoinType.inner, EnrExamSetVariant.class, "c", eq(property(EnrExamSetVariant.examSet().fromAlias("c")), property(EnrExamSetElement.examSet().fromAlias("e"))))
                .where(in(property(EnrExamSetVariant.id().fromAlias("c")), ids))
                .column("e")
                .column("c.id")
                .order(property(EnrExamSetElement.number().fromAlias("e")))
                ;
                for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
                    EnrExamSetElement el = (EnrExamSetElement) row[0];
                    Long cgId = (Long) row[1];
                    SafeMap.safeGet(examSetMap, cgId, ArrayList.class).add(el);
                }
                for (EnrExamSetVariant cg : getList(EnrExamSetVariant.class, "id", ids)) {
                    if (!examSetMap.containsKey(cg.getId())) {
                        examSetMap.put(cg.getId(), Collections.<EnrExamSetElement>emptyList());
                    }
                }
                return examSetMap;
            }

            private Map<Long, Map<EnrExamVariant, List<EnrExamVariantPassForm>>> getExamData(Collection<Long> ids)
            {
                final Map<Long, Map<EnrExamVariant, List<EnrExamVariantPassForm>>> dataMap = SafeMap.get(HashMap.class);

                final List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrExamVariant.class, "e")
                .where(in(property(EnrExamVariant.examSetVariant().id().fromAlias("e")), ids))
                .joinEntity("e", DQLJoinType.left, EnrExamVariantPassForm.class, "f", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("f")), property("e")))
                .column(property("e"))
                .column("f")
                .createStatement(session).list();

                for (Object[] row: rows) {
                    EnrExamVariant exam = (EnrExamVariant)row[0];
                    EnrExamVariantPassForm form = (EnrExamVariantPassForm) row[1];
                    Map<EnrExamVariant, List<EnrExamVariantPassForm>> examMap = dataMap.get(exam.getExamSetVariant().getId());
                    if (null != form) { SafeMap.safeGet(examMap, exam, ArrayList.class).add(form); }
                }
                return dataMap;
            }

            private void processPassForms(StringBuilder elementTitle, Map<EnrExamPassForm, Boolean> elementPassFormMap, List<EnrExamVariantPassForm> passFormList)
            {
                if (passFormList.isEmpty()) {
                    elementTitle.append(" не заданы");
                } else {
                    Collections.sort(passFormList, EnrExamVariantPassForm.Comparators.byFormCode);
                    StringBuilder passForms = new StringBuilder();
                    for (EnrExamVariantPassForm passForm: passFormList) {
                        elementPassFormMap.put(passForm.getPassForm(), Boolean.TRUE);
                        if (passForms.length() > 0) { passForms.append(", "); }
                        passForms.append(passForm.getPassForm().getTitle());
                    }
                    elementTitle.append(" - ").append(passForms);
                }
            }

            private EnrExamVariant findExam(Map<EnrExamVariant, List<EnrExamVariantPassForm>> cgSettingsMap, EnrExamSetElement examSetElement)
            {
                for (EnrExamVariant i : cgSettingsMap.keySet()) {
                    if (i.getExamSetElement() != null && i.getExamSetElement().equals(examSetElement)) {
                        return i;
                    }
                }
                return null;
            }

        });
        return result;
    }

    private Map<EnrExamPassForm, Boolean> initPassFormMap(List<EnrExamPassForm> forms, boolean fillOlympiad)
    {
        final Map<EnrExamPassForm, Boolean> elementPassFormMap = new HashMap<>(forms.size());
        for (EnrExamPassForm passForm : forms) {
            elementPassFormMap.put(passForm, fillOlympiad && EnrExamPassFormCodes.OLYMPIAD.equals(passForm.getCode()) ? Boolean.TRUE : Boolean.FALSE);
        }
        return elementPassFormMap;
    }

    @Override
    public void saveExamSetSettings(IExamSetSettings examSetSettings, EnrProgramSetBase owner, ExamSetVariantKind variantKind)
    {
        EnrExamSet examSet = examSetSettings.getExamSet();

        EnrExamSetVariant examSetVariant = variantKind.getVariant(owner);
        if (examSetVariant == null) {
            examSetVariant = new EnrExamSetVariant();
            examSetVariant.setOwner(owner);
            examSetVariant.setExamSet(examSet);
            save(examSetVariant);
        }

        variantKind.setVariant(owner, examSetVariant);
        update(owner);

        updateExamSetVariant(examSetSettings, examSetVariant);

        new DQLDeleteBuilder(EnrExamSetVariant.class)
            .where(new DQLCanDeleteExpressionBuilder(EnrExamSetVariant.class, "id").getExpression())
            .createStatement(getSession())
            .execute();
    }

    @Override
    public ExamSetSettings getNewExamSettings(EnrExamSet examSet) {
        final ExamSetSettings settings = new ExamSetSettings(examSet);

        settings.setElementList(new ArrayList<>());

        final StringBuilder examSetSettingsTitle = new StringBuilder();
        final List<EnrExamPassForm> forms = getCatalogItemListOrderByCode(EnrExamPassForm.class);
        for (EnrExamSetElement examSetElement : getList(EnrExamSetElement.class, EnrExamSetElement.examSet(), examSet)) {

            final Map<EnrExamPassForm, Boolean> elementPassFormMap = initPassFormMap(forms, true);

            settings.setNotFullySetup(true);

            final ExamSetElementSettings elementSettings = new ExamSetElementSettings(examSetElement, examSetElement.getTitle() + " (не настроено)");
            elementSettings.setPassMarkAsLong(examSetElement.getValue().getDefaultPassMarkAsLong());
            elementSettings.setPriority(examSetElement.getNumber());
            elementSettings.setPassFormMap(elementPassFormMap);
            settings.getElementList().add(elementSettings);

            if (examSetSettingsTitle.length() > 0) { examSetSettingsTitle.append("\n"); }
            examSetSettingsTitle.append(elementSettings.getTitle());
        }

        settings.setTitle(examSetSettingsTitle.toString());

        return settings;
    }

    private void updateExamSetVariant(IExamSetSettings examSetSettings, EnrExamSetVariant examSetVariant)
    {
        examSetVariant.setExamSet(examSetSettings.getExamSet());
        update(examSetVariant);

        Set<Long> updatedExamIds = new HashSet<>();
        Set<Long> updatedPassFormIds = new HashSet<>();

        for (IExamSetElementSettings elementSettings : examSetSettings.getElementList()) {
            EnrExamVariant exam = elementSettings.getCgExam();
            if (exam == null) {
                exam = getByNaturalId(new EnrExamVariant.NaturalId(examSetVariant, elementSettings.getElement()));
            }
            if (exam == null) {
                exam = new EnrExamVariant(examSetVariant, elementSettings.getElement());
            }
            exam.setPassMarkAsLong(elementSettings.getPassMarkAsLong());
            exam.setPriority(- elementSettings.getPriority());
            saveOrUpdate(exam);
            updatedExamIds.add(exam.getId());

            for (Map.Entry<EnrExamPassForm, Boolean> entry : elementSettings.getPassFormMap().entrySet()) {
                EnrExamVariantPassForm examPassForm = null;
                if (exam.getId() != null)
                    examPassForm = getByNaturalId(new EnrExamVariantPassForm.NaturalId(exam, entry.getKey()));
                if (entry.getValue()) {
                    if (null == examPassForm) {
                        examPassForm = new EnrExamVariantPassForm();
                        examPassForm.setExamVariant(exam);
                        examPassForm.setPassForm(entry.getKey());
                    }
                    saveOrUpdate(examPassForm);
                    updatedPassFormIds.add(examPassForm.getId());
                }
            }
        }

        for (EnrExamVariantPassForm passForm : getList(EnrExamVariantPassForm.class, EnrExamVariantPassForm.examVariant().examSetVariant(), examSetVariant)) {
            if (!updatedPassFormIds.contains(passForm.getId()))
                delete(passForm);
        }

        for (EnrExamVariant exam : getList(EnrExamVariant.class, EnrExamVariant.examSetVariant(), examSetVariant)) {
            if (!updatedExamIds.contains(exam.getId()))
                delete(exam);
        }

        for (EnrExamVariant exam : getList(EnrExamVariant.class, EnrExamVariant.examSetVariant(), examSetVariant)) {
            exam.setPriority(- exam.getPriority());
        }

    }

    private static class ExamSetSettings implements IExamSetSettings
    {
        private EnrExamSet examSet;
        private List<IExamSetElementSettings> elementList = new ArrayList<>();
        private boolean notFullySetup;

        // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15010378
        // [название дисциплины] [[сокр. название типа ВИ], П] (зач. балл - [зач. балл]; фс - [возможные формы сдачи])
        // при этом:
        //    сокр. название типа ВИ не выводим, если тип = "Обычное"
        //    П - выводим, если ВИ имеет признак "профильное"
        //    возможные формы сдачи сортируются по коду справочника
        //    если возможные формы сдачи не заданы, то выводить «фс не заданы»
        private String title;

        private ExamSetSettings(EnrExamSetVariant variant)
        {
            this.examSet = variant.getExamSet();
        }

        private ExamSetSettings(EnrExamSet examSet)
        {
            this.examSet = examSet;
        }

        @Override public List<IExamSetElementSettings> getElementList() { return elementList; }
        public void setElementList(List<IExamSetElementSettings> elementList) { this.elementList = elementList; }
        @Override public boolean isNotFullySetup() { return notFullySetup; }
        public void setNotFullySetup(boolean notFullySetup) { this.notFullySetup = notFullySetup; }
        @Override public String getTitle() { return StringUtils.isEmpty(title) ? "Пустой набор" : title; }
        public void setTitle(String title) { this.title = title; }
        @Override public EnrExamSet getExamSet() { return examSet; }
        public void setExamSet(EnrExamSet examSet) { this.examSet = examSet; }
    }

    @SuppressWarnings("serial")
    private static class ExamSetElementSettings extends IdentifiableWrapper<EnrExamSetElement> implements IExamSetElementSettings
    {
        private EnrExamSetElement element;
        private EnrExamVariant cgExam;
        private long passMarkAsLong;
        private int priority;
        private Map<EnrExamPassForm, Boolean> passFormMap;

        public ExamSetElementSettings(EnrExamSetElement el, String title) throws ClassCastException
        {
            super(el.getId(), title);
            this.element = el;
        }

        @Override public EnrExamSetElement getElement() { return element; }
        public void setElement(EnrExamSetElement element) { this.element = element; }
        @Override public long getPassMarkAsLong() { return passMarkAsLong; }
        @Override public void setPassMarkAsLong(long passMarkAsLong) { this.passMarkAsLong = passMarkAsLong; }
        @Override public int getPriority() { return priority; }
        @Override public void setPriority(int priority) { this.priority = priority; }
        @Override public Map<EnrExamPassForm, Boolean> getPassFormMap() { return passFormMap; }
        public void setPassFormMap(Map<EnrExamPassForm, Boolean> passFormMap) { this.passFormMap = passFormMap; }
        @Override public EnrExamVariant getCgExam() { return cgExam; }
        public void setCgExam(EnrExamVariant cgExam) { this.cgExam = cgExam; }
    }
}
