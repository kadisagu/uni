/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 09.06.2015
 */
public class MS_unienr14_2x8x1_12to13 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.createColumn("enr14_program_set_base_t", new DBColumn("requesttype_id", DBType.LONG));

        // Переносим данные для кадров высшей квалификации
        final SQLUpdateQuery upd = new SQLUpdateQuery("enr14_program_set_base_t", "b")
                .set("requesttype_id", "h.requesttype_id")
                .from(SQLFrom.table("enr14_program_set_higher_t", "h"))
                .where("h.id=b.id");
        final int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
        if (Debug.isEnabled()) {
            System.out.println("MS_unienr14_2x8x1_11to12: " + ret + " moved from higher");
        }
        tool.dropColumn("enr14_program_set_higher_t", "requesttype_id");

        // Заполняем для остальных
        final Map<String, Long> codeMap = MigrationUtils.getCatalogCode2IdMap(tool, "enr14_c_request_type_t");
        tool.executeUpdate("update enr14_program_set_base_t set requesttype_id=? where id in (select id from enr14_program_set_bs_t)", codeMap.get("1"));
        tool.executeUpdate("update enr14_program_set_base_t set requesttype_id=? where id in (select id from enr14_program_set_master_t)", codeMap.get("2"));
        tool.executeUpdate("update enr14_program_set_base_t set requesttype_id=? where id in (select id from enr14_program_set_sec_t)", codeMap.get("4"));

        tool.setColumnNullable("enr14_program_set_base_t", "requesttype_id", false);
    }
}