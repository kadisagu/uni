/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ContactsStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardContactsStepUI extends UIPresenter implements IWizardStep
{
    public static final String REGION_ADDRESS = "addressRegion";

    private EnrEntrantWizardState _state;

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    private PersonContactData _contactData;

    public PersonContactData getContactData()
    {
        return _contactData;
    }

    public void setContactData(PersonContactData contactData)
    {
        _contactData = contactData;
    }

    @Override
    public void onComponentRefresh()
    {
        Person person = DataAccessServices.dao().get(_state.getPersonId());
        _contactData = person.getContactData();

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setInline(true);
        addressConfig.setAreaVisible(true);
        addressConfig.setEntityId(person.getId());
        addressConfig.setAddressProperty(Person.address().s());
        addressConfig.setWithoutFieldSet(false);
        addressConfig.setFourFieldsWidth(true);
        addressConfig.setFieldSetTitle("Фактический адрес");
        _uiActivation.asRegion(AddressBaseEditInline.class, REGION_ADDRESS).
                parameter(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig).
                activate();
    }

    private void applyStep()
    {
        if(StringUtils.isEmpty(_contactData.getPhoneDefault()) &&
                StringUtils.isEmpty(_contactData.getPhoneWork()) &&
                StringUtils.isEmpty(_contactData.getPhoneMobile()))
            _uiSupport.error("Необходимо указать хотя бы один телефон.", "phoneDefault",
                    "phoneWork",
                    "phoneMobile");
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();

        final AddressBaseEditInlineUI region = _uiSupport.getChildUI(REGION_ADDRESS);

        DataAccessServices.dao().doInTransaction(session -> {
            DataAccessServices.dao().update(_contactData);
            region.onSave();

            return null;
        });


    }

    @Override
    public void saveStepData()
    {
        applyStep();
    }
}
