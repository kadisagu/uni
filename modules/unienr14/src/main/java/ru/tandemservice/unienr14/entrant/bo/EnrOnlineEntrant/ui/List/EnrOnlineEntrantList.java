/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.logic.EnrOnlineEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.ui.Pub.EnrOnlineEntrantPub;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author nvankov
 * @since 5/19/14
 */
@Configuration
public class EnrOnlineEntrantList extends BusinessComponentManager
{
    public static final String ONLINE_ENTRANT_DS = "onlineEntrantDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(ONLINE_ENTRANT_DS, onlineEntrantCL(), onlineEntrantDS()))
                .create();
    }

    @Bean
    public ColumnListExtPoint onlineEntrantCL()
    {
        IColumnListExtPointBuilder extPointBuilder = columnListExtPointBuilder(ONLINE_ENTRANT_DS)
            .addColumn(textColumn("personalNumber", EnrOnlineEntrant.personalNumber()).required(true).order().width("100px"))
                .addColumn(publisherColumn("fio", EnrOnlineEntrant.identityCard().fullFio())
                        .permissionKey("menuEnr14OnlineEntrantPub")
                        .businessComponent(EnrOnlineEntrantPub.class)
                        .primaryKeyPath(EnrOnlineEntrant.id())
                        .required(true)
                        .order())
            .addColumn(textColumn("sex", EnrOnlineEntrant.identityCard().sex().shortTitle()))
            .addColumn(textColumn("passport", EnrOnlineEntrant.identityCard().fullNumber()))
            .addColumn(dateColumn("birthDate", EnrOnlineEntrant.identityCard().birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(dateColumn("regDate", EnrOnlineEntrant.regDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(publisherColumn("entrant", PersonRole.person().identityCard().fullFio())
                .entityListProperty(EnrOnlineEntrant.entrant().s())
                .parameters("mvel:['selectedTab':'requestTab']"))
            .addColumn(dateColumn("entrantRegDate", EnrOnlineEntrant.entrant().registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order());
        if (Debug.isEnabled()) {
            extPointBuilder.addColumn(actionColumn("clear", CommonDefines.ICON_CHOOSE, "onClearEntrantLink").permissionKey("denied_permission_key").alert(alert("Удалить связь с абитуриентом для онлайн-абитуриента «{0}»? Используйте эту функцию только для тестирования системы.", EnrOnlineEntrant.identityCard().fullFio().s())));
        }
        extPointBuilder.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("enr14OnlineEntrantListDelete").alert(alert("Удалить онлайн-абитуриента «{0}»?", EnrOnlineEntrant.identityCard().fullFio().s())));
        return extPointBuilder.create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler onlineEntrantDS()
    {
        return new EnrOnlineEntrantDSHandler(getName());
    }
}



    