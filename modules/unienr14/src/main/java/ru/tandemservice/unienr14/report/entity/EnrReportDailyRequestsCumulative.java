package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.EnrReportDailyRequestsCumulativeAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportDailyRequestsCumulativeGen;

import java.util.Arrays;
import java.util.List;

/**
 * Ежедневная сводка по заявлениям с нарастающим итогом
 */
public class EnrReportDailyRequestsCumulative extends EnrReportDailyRequestsCumulativeGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportDailyRequestsCumulative";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_SPLITTED,
        P_REQUEST_TYPE,
        P_PROGRAM_FORM,
        P_ENR_ORG_UNIT,
        P_FORMATIVE_ORG_UNIT,
        P_PROGRAM_SUBJECT,
        P_EDU_PROGRAM,
        P_PROGRAM_SET);

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportDailyRequestsCumulative.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportDailyRequestsCumulativeAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Ежедневная сводка по заявлениям с нарастающим итогом»"; }
            @Override public String getListTitle() { return "Список отчетов «Ежедневная сводка по заявлениям с нарастающим итогом»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}