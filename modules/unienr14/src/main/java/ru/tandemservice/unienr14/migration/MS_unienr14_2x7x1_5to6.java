package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// исправление MS_unienr14_2x7x1_1to2

        {
            tool.executeUpdate("update enr14_program_set_bs_t set examsetvariantsec_id=examsetvariantminprof_id where examsetvariantsec_id is null");
            tool.executeUpdate("update enr14_program_set_bs_t set examsetvarianthigh_id=examsetvariantminprof_id where examsetvarianthigh_id is null");
        }

        {
            Long spoMethodDivCompetitions = (Long) tool.getUniqueResult("select id from enr14_c_meth_div_comp_t where code_p='no_div_spo'"); // EnrMethodDivCompetitionsCodes.SEC_NO_DIV = "no_div_spo"
            Long masterMethodDivCompetitions = (Long) tool.getUniqueResult("select id from enr14_c_meth_div_comp_t where code_p='no_div_master'"); // EnrMethodDivCompetitionsCodes.MASTER_NO_DIV = "no_div_master"
            Long higherMethodDivCompetitions = (Long) tool.getUniqueResult("select id from enr14_c_meth_div_comp_t where code_p='no_div_higher'"); // EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV = "no_div_higher"

            tool.executeUpdate("update enr14_program_set_base_t set methoddivcompetitions_id=? where id in (select id from enr14_program_set_sec_t)", spoMethodDivCompetitions);
            tool.executeUpdate("update enr14_program_set_base_t set methoddivcompetitions_id=? where id in (select id from enr14_program_set_master_t)", masterMethodDivCompetitions);
            tool.executeUpdate("update enr14_program_set_base_t set methoddivcompetitions_id=? where id in (select id from enr14_program_set_higher_t)", higherMethodDivCompetitions);
        }
    }
}