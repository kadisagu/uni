/* $Id: EcgpQuotaDTO.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EnrPAQuotaWrapper implements IEnrPAQuotaWrapper
{
    private int _totalQuota;
    private boolean _totalQuotaValid;
    private Map<Long, Integer> _quotaMap;

    public EnrPAQuotaWrapper(int totalQuota, boolean totalQuotaValid, Map<Long, Integer> quotaMap)
    {
        _quotaMap = quotaMap;
        _totalQuotaValid = totalQuotaValid;
        _totalQuota = totalQuota;
    }

    // Getters

    public int getTotalQuota()
    {
        return _totalQuota;
    }

    public boolean isTotalQuotaValid()
    {
        return _totalQuotaValid;
    }

    public Map<Long, Integer> getQuotaMap()
    {
        return _quotaMap;
    }
}
