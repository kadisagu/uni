package ru.tandemservice.unienr14.rating.entity;

import java.util.List;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.gen.EnrExamPassDisciplineGen;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

/**
 * Дисциплина для сдачи
 */
public class EnrExamPassDiscipline extends EnrExamPassDisciplineGen implements ITitled
{
    public EnrExamPassDiscipline()
    {
    }

    public EnrExamPassDiscipline(EnrEntrant entrant, EnrCampaignDiscipline discipline, EnrExamPassForm passForm)
    {
        setEntrant(entrant);
        setDiscipline(discipline);
        setPassForm(passForm);
        setRetake(false);
    }

    public void syncAbsenceNoteAndMarkState()
    {
        if (null != getAbsenceNote()) {
            if (getAbsenceNote().isValid()) {
                setMarkAsLong(null);
                // MAYBE: setMarkDate(null);
            } else {
                setMarkAsLong(0L);
                // MAYBE: setMarkDate(null);
            }
        } else {
            if (null != getMarkAsLong()) {
                if (null == getMarkDate()) {
                    throw new ApplicationException("При выставлении балла необходимо указать дату сдачи.");
                }
            }
        }
    }




    @EntityDSLSupport(parts = {EnrExamPassDiscipline.L_DISCIPLINE + "." + EnrCampaignDiscipline.L_DISCIPLINE + "." + EnrDiscipline.P_TITLE, EnrExamPassDiscipline.L_PASS_FORM + "." + EnrExamPassForm.P_TITLE})
    @Override
    public String getTitle()
    {
        if (getDiscipline() == null) {
            return this.getClass().getSimpleName();
        }
        return getDisciplineAndPassFormTitle() + (isRetake() ? ", пересдача" : "");
    }

    @EntityDSLSupport(parts = {EnrExamPassDiscipline.L_DISCIPLINE + "." + EnrCampaignDiscipline.L_DISCIPLINE + "." + EnrDiscipline.P_TITLE, EnrExamPassDiscipline.L_PASS_FORM + "." + EnrExamPassForm.P_TITLE})
    @Override
    public String getDisciplineAndPassFormTitle()
    {
        return getDiscipline().getTitle() + " (" + getPassForm().getTitle() + ")";
    }

    @EntityDSLSupport(parts = EnrExamPassDiscipline.P_MARK_AS_LONG)
    @Override
    public String getMarkAsString() {
        return getMarkAsLong() == null ? "" : EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
    }

    @EntityDSLSupport(parts = EnrExamPassDiscipline.P_MARK_AS_LONG)
    @Override
    public Double getMarkAsDouble() {
        return getMarkAsLong() == null ? null : (double) getMarkAsLong() / (double) 1000;
    }

    @EntityDSLSupport
    @Override
    public String getExamResult() {
        if (getAbsenceNote() != null)
            return getAbsenceNote().getTitle();
        return getMarkAsLong() == null ? "" : EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
    }

    @EntityDSLSupport
    @Override
    public String getShortExamResult() {
        if (getAbsenceNote() != null)
            return getAbsenceNote().getShortTitle();
        return getMarkAsLong() == null ? "" : EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
    }

    @EntityDSLSupport
    @Override
    public String getExamResultFull() {
        String examResult;
        if (getAbsenceNote() != null)
            examResult = getAbsenceNote().getTitle();
        else
            examResult = getMarkAsLong() == null ? "" : EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
        return examResult + (isRetake() ? ", пересдача" : "");
    }

    /** шифр ДДС в ЭГ */
    @EntityDSLSupport
    @Override
    public String getCode()
    {
        if (getExamGroupPosition() == null || getExamGroup() == null) return Long.toHexString(getId());
        String number = getExamGroupPosition().toString();
        if (getExamGroupPosition() < 10)
            number = "000" + number;
        else if (getExamGroupPosition() < 100)
            number = "00" + number;
        else if (getExamGroupPosition() < 1000)
            number = "0" + number;
        return getExamGroup().getTitle() + number;
    }

    @Override
    @EntityDSLSupport
    public String getExamScheduleTitle() {
        if (getExamGroup() == null)
            return "";
        List<EnrExamGroupScheduleEvent> events = IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup(), getExamGroup(), EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());
        return UniStringUtils.join(events, EnrExamGroupScheduleEvent.P_SCHEDULE_TITLE, "\n");
    }

    public boolean isCanDeleteOnRefresh()
    {
        return getMarkAsLong() == null && getExamGroup() == null && !isRetake() && getAbsenceNote() == null;
    }

    public boolean isCanChangeExamGroup() {
        return getMarkAsLong() == null && getAbsenceNote() == null;
    }
}