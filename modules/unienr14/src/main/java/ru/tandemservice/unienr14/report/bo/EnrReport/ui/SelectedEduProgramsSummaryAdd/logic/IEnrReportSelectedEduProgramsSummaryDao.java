/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd.EnrReportSelectedEduProgramsSummaryAddUI;

/**
 * @author rsizonenko
 * @since 07.07.2014
 */
public interface IEnrReportSelectedEduProgramsSummaryDao extends INeedPersistenceSupport {
    long createReport(EnrReportSelectedEduProgramsSummaryAddUI model);
}
