/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author oleyba
 * @since 7/10/14
 */
public class RejectVisaOrderService extends ru.tandemservice.unimove.service.RejectVisaOrderService
{
    @Override
    protected void doValidate()
    {
        super.doValidate();
        if (IUniBaseDao.instance.get().getCount(EnrCancelExtract.class, EnrCancelExtract.entity().paragraph().order().s(), getOrder()) > 0) {
            throw new ApplicationException("Откат приказа невозможен, так как для данного приказа уже добавлены выписки об отмене приказа.");
        }
    }

    @Override
    protected void doExecute() throws Exception
    {
        EnrOrderManager.instance().dao().doExecuteRejectVisaOrderService(getOrder());
    }
}
