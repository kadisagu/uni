package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Организация ЦП
 */
public class EnrTargetAdmissionOrgUnit extends EnrTargetAdmissionOrgUnitGen
{
    public EnrTargetAdmissionOrgUnit()
    {

    }

    public EnrTargetAdmissionOrgUnit(EnrCampaignTargetAdmissionKind enrCampaignTAKind, ExternalOrgUnit externalOrgUnit)
    {
        this.setEnrCampaignTAKind(enrCampaignTAKind);
        this.setExternalOrgUnit(externalOrgUnit);
    }
}