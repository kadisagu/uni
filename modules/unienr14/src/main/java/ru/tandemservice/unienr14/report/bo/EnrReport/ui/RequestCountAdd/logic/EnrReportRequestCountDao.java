/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.utils.TemplateBasedComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd.EnrReportRequestCountAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.entity.EnrReportRequestCount;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/20/14
 */
public class EnrReportRequestCountDao extends UniBaseDao implements IEnrReportRequestCountDao
{

    private static final Map<String, String> levelRequierement;
    static {
        levelRequierement = new HashMap<>();
        for (String code : EnrEduLevelRequirementCodes.CODES) {
            levelRequierement.put(code, DataAccessServices.dao().getByCode(EnrEduLevelRequirement.class, code).getShortTitle());
        }
    }


    @Override
    public Long createReport(EnrReportRequestCountAddUI model)
    {
        EnrReportRequestCount report = new EnrReportRequestCount();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRequestCount.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRequestCount.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRequestCount.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRequestCount.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRequestCount.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRequestCount.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRequestCount.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("EnrReportRequestCount.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }



    private byte[] buildReport(EnrReportRequestCountAddUI model)
    {
        IDQLSelectableQuery competitionIds = model.getCompetitionFilterAddon().getEntityIdsFilteredBuilder("c").buildQuery();
        IDQLSelectableQuery programSetOUIds = new DQLSelectBuilder()
            .fromEntity(EnrCompetition.class, "comp")
            .column(property("comp", EnrCompetition.programSetOrgUnit().id()))
            .predicate(DQLPredicateType.distinct)
            .where(in(property("comp", EnrCompetition.id()), competitionIds)).buildQuery();

        IDQLSelectableQuery requestedCompetitionIds = new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "rc")
            .column(property("rc", EnrRequestedCompetition.id()))
            .where(eq(property("rc", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(model.getEnrollmentCampaign())))
            .where(betweenDays(EnrRequestedCompetition.request().regDate().fromAlias("rc"), model.getDateFrom(), model.getDateTo()))
            .where(in(property("rc", EnrRequestedCompetition.competition().id()), competitionIds))
            .where(eq(property("rc", EnrRequestedCompetition.request().entrant().archival()), value(Boolean.FALSE)))
            .where(model.isParallelActive() ? eq(property("rc", EnrRequestedCompetition.parallel()), value(model.isParallelOnly())) : null)
            .buildQuery();

        byte[] template = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_REQUEST_COUNT).getCurrentTemplate();
        RtfDocument t = new RtfReader().read(template);
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setSettings(t.getSettings());
        result.setHeader(t.getHeader());

        final int grayIndex = result.getHeader().getColorTable().addColor(128, 128, 128);
        final int blackIndex = result.getHeader().getColorTable().addColor(0, 0, 0);

        Map<Long, String> orgUnitMap = new LinkedHashMap<>();
        {
            List<OrgUnit> orgUnits = this.getList(new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "ou")
                .column(property("ou"))
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                    .where(eq(property("psou", EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().id()), property("ou", OrgUnit.id())))
                    .where(in(property("psou", EnrProgramSetOrgUnit.id()), programSetOUIds))
                    .buildQuery())));

            Collections.sort(orgUnits, (o1, o2) -> o1.isTop() || o2.isTop() ? -Boolean.compare(o1.isTop(), o2.isTop()) : o1.getTitle().compareTo(o2.getTitle()));

            for (OrgUnit orgUnit: orgUnits)
            {
                orgUnitMap.put(orgUnit.getId(), orgUnit.getNominativeCaseTitle() == null ? orgUnit.getFullTitle() : orgUnit.getNominativeCaseTitle());
            }
        }
        Map<Long, EnrProgramSetOrgUnit> programSetOUPlanMap = getProgramSetOUPlanMap(programSetOUIds);
        Map<Long, Map<String, Map<String, Integer>>> requestedCompetitionSummaryMap = getRequestedCompetitionSummaryMap(requestedCompetitionIds);
        Map<Long, Map<String, Integer>> takeDocumentsAwayMap = getTakeDocumentsAwayMap(requestedCompetitionIds);

        // Филиал -> Форма обучения -> Вид заявления -> список Наборов ОП на подразделении
        Map<Long, Map<String, Map<String, List<Long>>>> blockMap = new TreeMap<>(new TemplateBasedComparator<>(new ArrayList<>(orgUnitMap.keySet())));
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                /*0*/.column(property("psou", EnrProgramSetOrgUnit.id()))
                /*1*/.column(property("psou", EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().id()))
                /*2*/.column(property("psou", EnrProgramSetOrgUnit.programSet().programForm().code()))
                /*3*/.column(property("psou", EnrProgramSetOrgUnit.programSet().id()))
                .where(in(property("psou", EnrProgramSetOrgUnit.id()), programSetOUIds))
                .order(property("psou", EnrProgramSetOrgUnit.programSet().programSubject().title()))
                .order(property("psou", EnrProgramSetOrgUnit.programSet().title()));

            Map<Class<? extends EnrProgramSetBase>, String> psType2requestTypeMap = new HashMap<>(4);
            {
                psType2requestTypeMap.put(EnrProgramSetBS.class, EnrRequestTypeCodes.BS);
                psType2requestTypeMap.put(EnrProgramSetMaster.class, EnrRequestTypeCodes.MASTER);
                psType2requestTypeMap.put(EnrProgramSetHigher.class, EnrRequestTypeCodes.HIGHER);
                psType2requestTypeMap.put(EnrProgramSetSecondary.class, EnrRequestTypeCodes.SPO);
            }

            for (Object[] row : this.<Object[]>getList(builder))
            {
                Long programSetOUId = (Long) row[0];
                Long orgUnitId = (Long) row[1];
                String programFormCode = (String) row[2];
                Long programSetId = (Long) row[3];
                String requestTypeCode = psType2requestTypeMap.get(EntityRuntime.getMeta(programSetId).getEntityClass());

                Map<String, Map<String, List<Long>>> programFormMap = SafeMap.safeGet(blockMap, orgUnitId, TreeMap.class);
                Map<String, List<Long>> requestTypeMap = SafeMap.safeGet(programFormMap, programFormCode, TreeMap.class);
                SafeMap.safeGet(requestTypeMap, requestTypeCode, ArrayList.class).add(programSetOUId);
            }
        }

        Map<String, String> programFormTitleMap = new HashMap<>(EduProgramFormCodes.CODES.size());
        {
            for (EduProgramForm programForm: this.getList(EduProgramForm.class))
            {
                programFormTitleMap.put(programForm.getCode(), StringUtils.capitalize(programForm.getTitle()));
            }
        }

        Map<String, String> requestTypeTitleMap = new HashMap<>(EnrRequestTypeCodes.CODES.size());
        {
            for (EnrRequestType requestType: this.getList(EnrRequestType.class))
            {
                requestTypeTitleMap.put(requestType.getCode(), StringUtils.capitalize(requestType.getShortTitle()));
            }
        }

        IDQLSelectableQuery programSetIds = new DQLSelectBuilder()
            .fromEntity(EnrProgramSetOrgUnit.class, "psou")
            .column(property("psou", EnrProgramSetOrgUnit.programSet().id()))
            .where(in(property("psou", EnrProgramSetOrgUnit.id()), programSetOUIds))
            .predicate(DQLPredicateType.distinct)
            .buildQuery();

        // Набор ОП - > { Направление, Название набора ОП }
        Map<Long, String[]> programSetTitleWithSubjectMap = new HashMap<>();
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetBase.class, "ps", Boolean.TRUE)
                .fetchPath(DQLJoinType.inner, EnrProgramSetBase.programSubject().fromAlias("ps"))
                .column(property("ps"))
                .where(in(property("ps", EnrProgramSetBase.id()), programSetIds));

            for (EnrProgramSetBase programSet: this.<EnrProgramSetBase>getList(builder))
            {
                programSetTitleWithSubjectMap.put(programSet.getId(), new String[]{ programSet.getProgramSubject().getTitleWithCode(), programSet.getTitle() });
            }
        }

        Map<Long, EduProgramProf> programSet2SingleProgramMap = new HashMap<>();
        boolean replaceProgramSetBySingleProgramTitle = model.isReplaceProgramSetBySingleProgramTitle();
        boolean doNotDisplaySingleProgramTitle = model.isDoNotDisplaySingleProgramTitle();
        if (replaceProgramSetBySingleProgramTitle || doNotDisplaySingleProgramTitle)
        {
            DQLSelectBuilder singleProgramBuilder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, "item", Boolean.FALSE)
                .fetchPath(DQLJoinType.inner, EnrProgramSetItem.program().fromAlias("item"), Boolean.TRUE)
                .column("item")
                .where(in(property("item", EnrProgramSetItem.programSet().id()), new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetItem.class, "psi")
                    .column(property("psi", EnrProgramSetItem.programSet().id()))
                    .where(in(property("psi", EnrProgramSetItem.programSet().id()), programSetIds))
                    .group(property("psi", EnrProgramSetItem.programSet().id()))
                    .having(eq(DQLFunctions.count(property("psi", EnrProgramSetItem.program().id())), value(1))).buildQuery()));

            for (EnrProgramSetItem item: this.<EnrProgramSetItem>getList(singleProgramBuilder))
            {
                programSet2SingleProgramMap.put(item.getProgramSet().getId(), item.getProgram());
            }

            DQLSelectBuilder secondaryBuilder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetSecondary.class, "s")
                .fetchPath(DQLJoinType.inner, EnrProgramSetSecondary.program().fromAlias("s"), Boolean.TRUE)
                .column(property("s"))
                .where(in(property("s", EnrProgramSetSecondary.id()), programSetIds));

            for (EnrProgramSetSecondary programSet: this.<EnrProgramSetSecondary>getList(secondaryBuilder))
            {
                programSet2SingleProgramMap.put(programSet.getId(), programSet.getProgram());
            }
        }

        // перехватчик: Мердж строки-заголовка набора ОП, выделением серым отдельных уровней образования
        RtfRowIntercepterBase intercepter = new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (rowIndex % 2 == 0) // каждая четная строка - название Набора ОП
                {
                    cell.setMergeType(colIndex == 0 ? MergeType.HORIZONTAL_MERGED_FIRST : MergeType.HORIZONTAL_MERGED_NEXT);

                } else if (StringUtils.contains(value, "#_#"))
                {
                    List<String> lines = Arrays.asList(StringUtils.split(value, "#_#"));
                    RtfString rtfString = new RtfString()
                        .append(lines.get(0))
                        .par()
                        .color(grayIndex)
                        .append(lines.get(1));

                    if (lines.size() > 2)
                        for (int i = 2; i < lines.size(); i++) {
                            rtfString.par().append(lines.get(i));
                        }
                    rtfString.color(blackIndex);

                    return rtfString.toList();
                }

                return null;
            }
        };


        List<RtfDocument> pages = new ArrayList<>();

        // Поехали рисовать
        for (Map.Entry<Long, Map<String, Map<String, List<Long>>>> orgUnitEntry: blockMap.entrySet())
        {
            String orgUnitTitle = orgUnitMap.get(orgUnitEntry.getKey());
            for (Map.Entry<String, Map<String, List<Long>>> programFormEntry: orgUnitEntry.getValue().entrySet())
            {
                String programFormCode = programFormEntry.getKey();
                for (Map.Entry<String, List<Long>> requestTypeEntry: programFormEntry.getValue().entrySet())
                {
                    String requestTypeCode = requestTypeEntry.getKey();

                    List<String[]> rows = new ArrayList<>();
                    List<List<Integer>> totalNumbers = new ArrayList<>();
                    RtfDocument page = t.getClone();
                    {
                        RtfInjectModifier modifier = new RtfInjectModifier();
                        modifier.put("filial", orgUnitTitle);
                        modifier.put("programForm", programFormTitleMap.get(programFormCode));
                        modifier.put("requestType", requestTypeTitleMap.get(requestTypeCode));
                        modifier.modify(page);
                    }
                    {
                        for (Long programSetOrgUnitId: requestTypeEntry.getValue())
                        {
                            Long programSetId = programSetOUPlanMap.get(programSetOrgUnitId).getProgramSet().getId();
                            String[] titleParts = programSetTitleWithSubjectMap.get(programSetId);
                            String programSetTitle = titleParts[0];
                            EduProgramProf program = programSet2SingleProgramMap.get(programSetId);
                            if (program == null)
                            {
                                programSetTitle += " " + titleParts[1];

                            } else
                            {
                                if (!doNotDisplaySingleProgramTitle)
                                {
                                    programSetTitle += " " + program.getTitle();
                                }

                                programSetTitle += " " + program.getImplConditionsShort();
                            }

                            rows.add(new String[]{ programSetTitle, "", "", "", "", "", "", "", "", "", "", "", "", "" });

                            List<Integer> numbers = new ArrayList<>();
                            EnrProgramSetOrgUnit programSetOrgUnit = programSetOUPlanMap.get(programSetOrgUnitId);

                            final String methodDivCode = programSetOrgUnit.getProgramSet().getMethodDivCompetitions().getCode();

                            boolean showDetails = !(methodDivCode.equals(EnrMethodDivCompetitionsCodes.BS_NO_DIV) ||
                                    methodDivCode.equals(EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV) ||
                                    methodDivCode.equals(EnrMethodDivCompetitionsCodes.INTERNSHIP_NO_DIV) ||
                                    methodDivCode.equals(EnrMethodDivCompetitionsCodes.MASTER_NO_DIV) ||
                                    methodDivCode.equals(EnrMethodDivCompetitionsCodes.POSTGRADUATE_NO_DIV) ||
                                    methodDivCode.equals(EnrMethodDivCompetitionsCodes.SEC_NO_DIV) ||
                                    methodDivCode.equals(EnrMethodDivCompetitionsCodes.TRAINEESHIP_NO_DIV));

                            int ministerial = programSetOrgUnit.getMinisterialPlan() - programSetOrgUnit.getExclusivePlan() - programSetOrgUnit.getTargetAdmPlan(); // Общий конкурс
                            List<String> cells = new ArrayList<>(14);
                            {
                                cells.add("");

                                cells.add(programSetOrgUnit.getMinisterialPlan() > 0 ? String.valueOf(programSetOrgUnit.getMinisterialPlan()) : "—");
                                numbers.add(nullIfZero(programSetOrgUnit.getMinisterialPlan()));

                                cells.add(programSetOrgUnit.getExclusivePlan() > 0 ? String.valueOf(programSetOrgUnit.getExclusivePlan()) : "—");
                                numbers.add(nullIfZero(programSetOrgUnit.getExclusivePlan()));

                                cells.add(programSetOrgUnit.getTargetAdmPlan() > 0 ? String.valueOf(programSetOrgUnit.getTargetAdmPlan()) : "—");
                                numbers.add(nullIfZero(programSetOrgUnit.getTargetAdmPlan()));

                                cells.add(ministerial > 0 ? String.valueOf(ministerial) : "—");
                                numbers.add(nullIfZero(ministerial));

                                cells.add(programSetOrgUnit.getContractPlan() > 0 ? String.valueOf(programSetOrgUnit.getContractPlan()) : "—");
                                numbers.add(nullIfZero(programSetOrgUnit.getContractPlan()));
                            }

                            Map<String, Map<String, Integer>> requestMap = requestedCompetitionSummaryMap.get(programSetOrgUnitId);
                            {
                                // Всего
                                int value = 0;
                                {
                                    // Без ВИ
                                    value += sum(false, requestMap.get(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL));

                                    // Особые права
                                    value += sum(false ,requestMap.get(EnrCompetitionTypeCodes.EXCLUSIVE));

                                    // ЦП
                                    value += sum(false ,requestMap.get(EnrCompetitionTypeCodes.TARGET_ADMISSION));

                                    // Общий конкурс
                                    value += sum(false ,requestMap.get(EnrCompetitionTypeCodes.MINISTERIAL));
                                }

                                cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"));
                                numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value);

                                {
                                    Map<String, Integer> values = getValues(requestMap.get(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL), showDetails);
                                    cells.add(buildCellValue(values, ministerial == 0));
                                    numbers.add(values.get(""));
                                }
                                {
                                    Map<String, Integer> values = getValues(requestMap.get(EnrCompetitionTypeCodes.EXCLUSIVE), showDetails);
                                    cells.add(buildCellValue(values, programSetOrgUnit.getExclusivePlan() == 0));
                                    numbers.add(values.get(""));
                                }
                                {
                                    Map<String, Integer> values = getValues(requestMap.get(EnrCompetitionTypeCodes.TARGET_ADMISSION), showDetails);
                                    cells.add(buildCellValue(values, programSetOrgUnit.getTargetAdmPlan() == 0));
                                    numbers.add(values.get(""));
                                }
                                {
                                    Map<String, Integer> values = getValues(requestMap.get(EnrCompetitionTypeCodes.MINISTERIAL), showDetails);
                                    cells.add(buildCellValue(values, ministerial == 0));
                                    numbers.add(values.get(""));
                                }
                                {

                                    Map<String, Integer> values = getValues(requestMap.get(EnrCompetitionTypeCodes.CONTRACT), showDetails);
                                    Map<String, Integer> noExamValues = getValues(requestMap.get(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT), showDetails);

                                    for (Map.Entry<String, Integer> entry : noExamValues.entrySet()) {
                                        values.put(entry.getKey(), (safeInt(values.get(entry.getKey())) + entry.getValue()));
                                    }

                                    cells.add(buildCellValue(values, programSetOrgUnit.getContractPlan() == 0));
                                    numbers.add(values.get(""));
                                }
                            }

                            Map<String, Integer> takeDocsAwayMap = takeDocumentsAwayMap.get(programSetOrgUnitId);
                            {
                                // Бюджет
                                int value = safeInt(takeDocsAwayMap.get(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
                                cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"));
                                numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value);
                            }
                            {
                                // По договору
                                int value = safeInt(takeDocsAwayMap.get(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
                                cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getContractPlan() > 0 ? "0" : "—"));
                                numbers.add(value == 0 && programSetOrgUnit.getContractPlan() == 0 ? null : value);
                            }

                            rows.add(cells.toArray(new String[cells.size()]));
                            totalNumbers.add(numbers);
                        }
                    }

                    rows.add(new String[]{ "ВСЕГО " + programFormTitleMap.get(programFormCode) + " форма обучения, " + requestTypeTitleMap.get(requestTypeCode), "", "", "", "", "", "", "", "", "", "", "", "", "" });
                    List<String> cells = new ArrayList<>(14);
                    {
                        cells.add("");
                        for (int i = 0; i < 13; i++)
                        {
                            Integer value = null;
                            for (List<Integer> numbers : totalNumbers)
                            {
                                Integer currentValue = numbers.get(i);
                                if (currentValue != null)
                                    value = safeInt(value) + currentValue;
                            }

                            cells.add(value == null ? "—" : String.valueOf(value));
                        }
                    }

                    rows.add(cells.toArray(new String[cells.size()]));

                    RtfTableModifier modifier = new RtfTableModifier();
                    modifier.put("T1", rows.toArray(new String[rows.size()][]));
                    modifier.put("T1", intercepter);
                    modifier.modify(page);

                    pages.add(page);
                }
            }
        }

        for (RtfDocument page: pages)
        {
            if (pages.indexOf(page) != pages.size() -1)
            {
                page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                page.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }

            result.getElementList().addAll(page.getElementList());
        }

        return RtfUtil.toByteArray(result);
    }

    /** Наборы ОП на подразделении. */
    private Map<Long, EnrProgramSetOrgUnit> getProgramSetOUPlanMap(IDQLSelectableQuery programSetOUIds)
    {
        Map<Long, EnrProgramSetOrgUnit> result = new HashMap<>();
        DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(EnrProgramSetOrgUnit.class, "psou")
            .where(in(property("psou", EnrProgramSetOrgUnit.id()), programSetOUIds));

        for (EnrProgramSetOrgUnit programSetOrgUnit: this.<EnrProgramSetOrgUnit>getList(builder))
        {
            result.put(programSetOrgUnit.getId(), programSetOrgUnit);
        }

        return result;
    }

    /** Набор ОП на подразделении -> Вид приема -> Огр. на ур. образ. -> Кол-во заявлений */
    private Map<Long, Map<String, Map<String, Integer>>> getRequestedCompetitionSummaryMap(IDQLSelectableQuery requestedCompetitionIds)
    {
        Map<Long, Map<String, Map<String, Integer>>> result = SafeMap.get(key -> SafeMap.get(HashMap.class));

        DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "rc")
            /*0*/.column(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
            /*1*/.column(property("rc", EnrRequestedCompetition.competition().type().code()))
            /*2*/.column(property("rc", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
            /*3*/.column(DQLFunctions.count(property("rc", EnrRequestedCompetition.id())))
            .where(in(property("rc", EnrRequestedCompetition.id()), requestedCompetitionIds))
            .where(ne(property("rc", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)))
            .group(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
            .group(property("rc", EnrRequestedCompetition.competition().type().code()))
            .group(property("rc", EnrRequestedCompetition.competition().eduLevelRequirement().code()));

        for (Object[] row: this.<Object[]>getList(builder))
        {
            result.get((Long) row[0]).get((String) row[1]).put((String) row[2], ((Number) row[3]).intValue());
        }

        return result;
    }

    /** Набор ОП на подразделении -> Вид возмещения затрат -> Кол-во заявлений */
    private Map<Long, Map<String, Integer>> getTakeDocumentsAwayMap(IDQLSelectableQuery requestedCompetitionIds)
    {
        Map<Long, Map<String, Integer>> result = SafeMap.get(HashMap.class);
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "rc")
                /*0*/.column(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
                /*1*/.column(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()))
                /*2*/.column(DQLFunctions.count(property("rc", EnrRequestedCompetition.id())))
                .where(in(property("rc", EnrRequestedCompetition.id()), requestedCompetitionIds))
                .where(eq(property("rc", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)))
                .group(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().id()))
                .group(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()));

            for (Object[] row: this.<Object[]>getList(builder))
            {
                result.get((Long) row[0]).put((String) row[1], ((Number) row[2]).intValue());
            }
        }

        return result;
    }

    /** Преобразует null в ноль, остальные значения возвращает без изменений. */
    private static int safeInt(Integer value){ return value == null ? 0 : value; }

    /** Преобразует ноль в null, остальные значения возвращает без изменений. */
    private static Integer nullIfZero(int source){ return source == 0 ? null : source; }

    /** Значение для ячейки таблицы на основании заявлений по видам огр. на урочень образования и наличия мест. */
    private static Map<String, Integer> getValues(Map<String, Integer> eduLevReqValueMap, boolean composite)
    {
        Map<String, Integer> result = new HashMap<>();
        Integer value = eduLevReqValueMap.get(EnrEduLevelRequirementCodes.NO);
        value = value == null ? 0 : value;
            for (Map.Entry<String, Integer> entry : eduLevReqValueMap.entrySet()) {
                if (entry.getKey().equals(EnrEduLevelRequirementCodes.NO))
                    continue;
                value = safeInt(value) + safeInt(entry.getValue()) ;
                if (composite)
                    result.put(entry.getKey(), entry.getValue());
            }
        result.put("", value);

        return result;
    }

    /** Строка-значение ячейки таблицы. */
    private static String buildCellValue(Map<String, Integer> values, boolean useDash)
    {
        Integer value = values.get("");
        String result;
        if (value != null)
        {
            result = String.valueOf(value);
            if (values.size() > 1)
            {
                for (String code : EnrEduLevelRequirementCodes.CODES) {
                    if (code.equals(EnrEduLevelRequirementCodes.NO))
                        continue;
                    if (values.get(code) != null)
                        result += "#_#" + levelRequierement.get(code) + " - " + values.get(code);
                }
            }

        } else result = useDash ? "—" : "0";

        return result;

    }

    /** Сумма. */
    private static Integer sum(boolean nullable, Integer ... values)
    {
        Integer result = null;

        for (Integer value: values)
        {
            if (value != null)
                result = safeInt(result) + value;
        }

        return result == null ? (nullable ? null : 0) : result;
    }

    /** Сумма. */
    /*private static Integer sum(boolean nullable, List<Integer> values)
    {
        return sum(nullable, values.toArray(new Integer[values.size()]));
    }*/

    /** Сумма. */
    private static Integer sum(boolean nullable, Map<String, Integer> values)
    {
        if (values != null)
            return sum(nullable, values.values().toArray(new Integer[values.size()]));
        else return nullable ? null : 0;
    }
}