package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrAbstractExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_extract_abs_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("paragraph_id", DBType.LONG), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("createdate_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("state_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrAbstractExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_enr_extract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("entity_id", DBType.LONG).setNullable(false), 
				new DBColumn("grouptitle_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEnrollmentExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrAbstractOrder

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_abs_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)), 
				new DBColumn("createdate_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("state_id", DBType.LONG).setNullable(false), 
				new DBColumn("signeddate_p", DBType.DATE), 
				new DBColumn("commitdate_p", DBType.DATE), 
				new DBColumn("commitdatesystem_p", DBType.TIMESTAMP), 
				new DBColumn("textparagraph_p", DBType.TEXT), 
				new DBColumn("executor_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrAbstractOrder");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrder

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("type_id", DBType.LONG).setNullable(false), 
				new DBColumn("printformtype_id", DBType.LONG).setNullable(false), 
				new DBColumn("reason_id", DBType.LONG), 
				new DBColumn("basic_id", DBType.LONG), 
				new DBColumn("orderbasictext_p", DBType.createVarchar(2000)), 
				new DBColumn("ordertext_p", DBType.createVarchar(2000)), 
				new DBColumn("printform_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrder");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrAbstractParagraph

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_par_abs_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("order_id", DBType.LONG).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrAbstractParagraph");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentParagraph

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_enr_par_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("groupmanager_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEnrollmentParagraph");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrCampaignOrderVisaItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_visa_item_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("possiblevisa_id", DBType.LONG).setNullable(false), 
				new DBColumn("visatemplate_id", DBType.LONG).setNullable(false), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("groupsmembervising_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrCampaignOrderVisaItem");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrderBasic

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_order_basic_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("ordertype_id", DBType.LONG).setNullable(false), 
				new DBColumn("commentable_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrderBasic");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrderReason

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_order_reason_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("ordertype_id", DBType.LONG).setNullable(false), 
				new DBColumn("printtitle_p", DBType.createVarchar(255)), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrderReason");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrderReasonToBasicsRel

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_order_reason2basic_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("reason_id", DBType.LONG).setNullable(false), 
				new DBColumn("basic_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrderReasonToBasicsRel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrderType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_order_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)), 
				new DBColumn("componentprefix_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrderType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrVisasTemplate

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_visa_template_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrVisasTemplate");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrderPrintFormType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_order_form_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("ordertype_id", DBType.LONG).setNullable(false), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("used_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("basic_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("command_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("group_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("selectheadman_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("reasonandbasic_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrderPrintFormType");

		}


    }
}