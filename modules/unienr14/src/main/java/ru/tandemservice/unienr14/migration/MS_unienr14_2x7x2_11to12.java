/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 12.03.2015
 */
public class MS_unienr14_2x7x2_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.columnExists("enr14_request_t", "receiveedulevelfirst_p"))
        {
            // создать колонку
            tool.createColumn("enr14_request_t", new DBColumn("receiveedulevelfirst_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_request_t set receiveedulevelfirst_p=? where receiveedulevelfirst_p is null", true);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_request_t", "receiveedulevelfirst_p", false);
        }
    }
}