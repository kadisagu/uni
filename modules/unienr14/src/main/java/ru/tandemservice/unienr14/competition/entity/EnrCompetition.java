package ru.tandemservice.unienr14.competition.entity;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrPlanZeroFixer;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.competition.entity.gen.EnrCompetitionGen;

/**
 * Конкурс
 */
public class EnrCompetition extends EnrCompetitionGen implements ITitled, IEnrCompetitionDao.ICalculatedPlanOwner, EnrPlanZeroFixer.IPlanOwner
{
    public EnrCompetition()
    {
    }

    public EnrCompetition(final EnrProgramSetOrgUnit programSetOrgUnit, final EnrCompetitionType type, final EnrEduLevelRequirement requirement, final int plan, final EnrExamSetVariant variant, final EnrRequestType requestType)
    {
        this.setProgramSetOrgUnit(programSetOrgUnit);
        this.setType(type);
        this.setEduLevelRequirement(requirement);
        this.setPlan(plan);
        this.setExamSetVariant(variant);
        this.setRequestType(requestType);
    }

    public MultiKey buildKey() {
        return new MultiKey(
            getProgramSetOrgUnit(),
            getType(),
            getEduLevelRequirement()
        );
    }


    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getProgramSetOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        // [название набора ОП] ([форма обучения], в.п.: [вид приема], [СОО/ПО] | [филиал], [ФП])
        StringBuilder title = new StringBuilder();
        title.append(getProgramSetOrgUnit().getProgramSet().getTitle());
        title.append(" (").append(StringUtils.uncapitalize(getProgramSetOrgUnit().getProgramSet().getProgramForm().getTitle()));
        title.append(", в.п.: ").append(getType().getShortTitle());
        if (!EnrEduLevelRequirementCodes.NO.equals(getEduLevelRequirement().getCode())) {
            title.append(", ").append(getEduLevelRequirement().getShortTitle());
        }
        OrgUnit orgUnit = getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
        OrgUnit formingOrgUnit = getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
        if (!(orgUnit.isTop() && formingOrgUnit.isTop())) {
            title.append(" | ")
            .append(CommonBaseStringUtil.joinWithSeparator(", ",
                orgUnit.isTop() ? null : orgUnit.getShortTitle(),
                    orgUnit.equals(formingOrgUnit) ? null : formingOrgUnit.getShortTitle()));
        }
        title.append(")");
        return title.toString();
    }

    public boolean isTargetAdmission() {
        return EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(this.getType().getCode());
    }

    public boolean isExclusive() {
        return EnrCompetitionTypeCodes.EXCLUSIVE.equals(this.getType().getCode());
    }

    public boolean isContract()
    {
        return EnrCompetitionTypeCodes.CONTRACT.equals(this.getType().getCode()) || EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(this.getType().getCode());
    }

    public boolean isNoExams() {
        return EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(this.getType().getCode()) ||  EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(this.getType().getCode());
    }

    @Override
    public String getEduLevelReqCode()
    {
        return getEduLevelRequirement().getCode();
    }

    @Override
    public String getCompTypeCode()
    {
        return getType().getCode();
    }

    @Override
    public Object planBalanceKey()
    {
        return getType().getCode();
    }

    @Override
    public int getFixPlan()
    {
        return getCalculatedPlan();
    }

    @Override
    public void setFixPlan(int plan)
    {
        setCalculatedPlan(plan);
    }
}