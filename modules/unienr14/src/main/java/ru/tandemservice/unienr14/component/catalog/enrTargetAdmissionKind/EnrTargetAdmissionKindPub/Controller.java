/**
 *$Id: Controller.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrTargetAdmissionKind.EnrTargetAdmissionKindPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class Controller extends DefaultCatalogPubController<EnrTargetAdmissionKind, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<EnrTargetAdmissionKind> dataSource)
    {
        dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickUp"));
        dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickDown"));
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().doUp(component.<Long>getListenerParameter());
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().doDown(component.<Long>getListenerParameter());
    }

    @Override
    protected DynamicListDataSource<EnrTargetAdmissionKind> createListDataSource(IBusinessComponent context)
    {
        final DynamicListDataSource<EnrTargetAdmissionKind> dataSource = super.createListDataSource(context);

        for (AbstractColumn column : dataSource.getColumns())
            column.setOrderable(false);

        return dataSource;
    }
}
