/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.AddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType;

import java.io.IOException;
import java.util.Date;

/**
 * @author azhebko
 * @since 17.07.2014
 */
@Input(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "printFormType.id"))
public class EnrEnrollmentOrderParagraphPrintFormTypeAddEditUI extends UIPresenter
{
    private EnrEnrollmentOrderParagraphPrintFormType _printFormType = new EnrEnrollmentOrderParagraphPrintFormType();
    private DataWrapper _parallel;
    private IUploadFile _uploadFile;

    @Override
    public void onComponentRefresh()
    {
        if (isEditForm())
        {
            _printFormType = IUniBaseDao.instance.get().getNotNull(EnrEnrollmentOrderParagraphPrintFormType.class, getPrintFormType().getId());
            {
                Long parallelId;
                if (_printFormType.getParallel() == null) parallelId = 0L;
                else parallelId = _printFormType.getParallel() ? 1L : 2L;

                _parallel = this.getConfig().<EnrEnrollmentOrderParagraphPrintFormTypeAddEdit>getManager().parallelOptions().getItem(String.valueOf(parallelId));
            }

            getPrintFormType().setEditDate(new Date());
        }
    }

    public void onClickApply()
    {
        Boolean parallel = null;
        if (_parallel.getId().equals(EnrEnrollmentOrderParagraphPrintFormTypeAddEdit.ID_PARALLEL_PARAGRAPHS))
            parallel = Boolean.TRUE;
        else if (_parallel.getId().equals(EnrEnrollmentOrderParagraphPrintFormTypeAddEdit.ID_NOT_PARALLEL_PARAGRAPHS))
            parallel = Boolean.FALSE;

        getPrintFormType().setParallel(parallel);
        if (getUploadFile() != null)
        {
            byte[] templateContent;
            try
            {
                templateContent = IOUtils.toByteArray(getUploadFile().getStream());
            } catch (IOException e)
            {
                throw new RuntimeException();
            }

            getPrintFormType().setDocument(CommonBaseUtil.buildTemplateDocument(templateContent));
        }

        if (isAddForm())
            getPrintFormType().setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EnrEnrollmentOrderParagraphPrintFormType.class));

        IUniBaseDao.instance.get().saveOrUpdate(getPrintFormType());
        deactivate();
    }


    public boolean isAddForm(){ return getPrintFormType().getId() == null; }
    public boolean isEditForm(){ return !isAddForm(); }

    public EnrEnrollmentOrderParagraphPrintFormType getPrintFormType(){ return _printFormType; }

    public DataWrapper getParallel(){ return _parallel; }
    public void setParallel(DataWrapper parallel){ _parallel = parallel; }

    public IUploadFile getUploadFile(){ return _uploadFile; }
    public void setUploadFile(IUploadFile uploadFile){ _uploadFile = uploadFile; }
}