/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.List;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.DisciplineAndPassFormWrapper;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.EnrExamPassDisciplineWrapper;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.gen.IEnrExamSetElementValueGen;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/6/15
 */
public class EnrExamPassDisciplineListUI extends UIPresenter
{
    public static final String SETTING_ORG_UNIT = "orgUnit";
    public static final String SETTING_ENR_DISCIPLINE = "enrDiscipline";
    public static final String SETTING_RATE_DATE_FROM = "rateDateFrom";
    public static final String SETTING_RATE_DATE_TO = "rateDateTo";
    public static final String SETTING_ABSENCE_NOTE = "absenceNote";
    public static final String SETTING_EXAM_GROUP_SET = "examGroupSet";
	public static final String SETTING_EXAM_GROUP_SCHEDULE_EVENT = "examGroupScheduleEvent";
    public static final String SETTING_MARKED = "marked";

	public static final String PARAM_EXAM_GROUP = "examGroup";

    public Boolean _editMode;
    public List<EnrExamPassDisciplineWrapper> _wrappers = Lists.newArrayList();
    public EnrExamPassDisciplineWrapper _currentWrapper;
    public boolean _filledWrappers = false;
    public Date _defaultDate = new Date();

    private ISelectModel markedModel = TwinComboDataSourceHandler.getYesNoDefaultSelectModel();

    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(false)
                .configSettings(getSettingsKey());

        util.clearFilterItems();
        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfigBuilder().required(true).create())
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

        configWhereFilters();
        util.onComponentRefresh();
    }

    private void configWhereFilters()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
        {
            util.clearWhereFilter();
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(!EnrExamPassDisciplineList.ENR_CAMP_SELECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
            if(!EnrExamPassDisciplineList.ORG_UNIT_DS.equals(dataSource.getName()))
            {
                dataSource.put(SETTING_ORG_UNIT, getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
                dataSource.put(EnrExamPassDisciplineSearchDSHandler.PARAM_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
                dataSource.put(SETTING_EXAM_GROUP_SET, getSettings().get(SETTING_EXAM_GROUP_SET));
                if(EnrExamPassDisciplineList.EXAM_PASS_DISCIPLINE_SEARCH_DS.equals(dataSource.getName()))
                {
                    dataSource.put(SETTING_ENR_DISCIPLINE, getSettings().get(SETTING_ENR_DISCIPLINE));
                    dataSource.put(SETTING_MARKED, TwinComboDataSourceHandler.getSelectedValue(getSettings().get(SETTING_MARKED)));
                    dataSource.put(SETTING_RATE_DATE_FROM, getSettings().get(SETTING_RATE_DATE_FROM));
                    dataSource.put(SETTING_RATE_DATE_TO, getSettings().get(SETTING_RATE_DATE_TO));
                    dataSource.put(SETTING_ABSENCE_NOTE, getSettings().get(SETTING_ABSENCE_NOTE));
                    dataSource.put(PARAM_EXAM_GROUP, getExamGroupsFromSettings());
                }
            }
        }
		if (EnrExamPassDisciplineList.EXAM_GROUP_SCHEDULE_EVENT_SELECT_DS.equals(dataSource.getName()))
		{
			IIdentifiable enrDisciplineWrapper = getSettings().get(SETTING_ENR_DISCIPLINE);
			EnrExamPassDiscipline passDiscipline = enrDisciplineWrapper == null ? null : DataAccessServices.dao().get(EnrExamPassDiscipline.class, enrDisciplineWrapper.getId());
			IIdentifiable groupSetWrapper = getSettings().get(SETTING_EXAM_GROUP_SET);
			EnrExamGroupSet groupSet = groupSetWrapper == null ? null : DataAccessServices.dao().get(EnrExamGroupSet.class, groupSetWrapper.getId());
			dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_TERRITORIAL_ORG_UNIT, getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
			dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_EXAM_GROUP_SET, groupSet);
			dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_DISCIPLINE, passDiscipline == null ? null : passDiscipline.getDiscipline());
			dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_PASS_FORM, passDiscipline == null ? null : passDiscipline.getPassForm());
		}
    }


    @Override
    public void saveSettings()
    {
        super.saveSettings();
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.saveSettings();
        configWhereFilters();
        fillWrappers();
    }

    public void onClearSettings()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
        {
            util.getFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE.getSettingsName()).setValue(null);
            util.getFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM.getSettingsName()).setValue(null);
            util.getFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT.getSettingsName()).setValue(null);
            util.getFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET.getSettingsName()).setValue(null);
        }
        configWhereFilters();
        getSettings().remove(SETTING_MARKED);
        getSettings().remove(SETTING_RATE_DATE_FROM);
        getSettings().remove(SETTING_RATE_DATE_TO);
        getSettings().remove(SETTING_ABSENCE_NOTE);
        getSettings().remove(SETTING_EXAM_GROUP_SET);
        getSettings().remove(SETTING_EXAM_GROUP_SCHEDULE_EVENT);
        getSettings().save();

        fillWrappers();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getSettings().<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
    }

    public void onClickEnterAssessments()
    {
        fillWrappers();
        _editMode = true;
    }

    private void fillWrappers()
    {
        _wrappers = prepareWrappers();

        List<String> dataSourcesKeys = Lists.newArrayList(getConfig().getDataSources().keySet());
        for(String key : dataSourcesKeys)
        {
            if (key.endsWith("AbsenceNoteDS"))
                getConfig().getDataSources().remove(key);
        }

        for(EnrExamPassDisciplineWrapper wrapper : _wrappers)
        {
            SelectDataSource dataSource = new SelectDataSource(this, getCurrentAbsenceNoteDSName(wrapper.getId()));
            dataSource.setHandler(EnrExamPassDisciplineManager.instance().absenceNoteDSHandler());
            getConfig().addDataSource(dataSource);
        }
    }


    public void onClickApply()
    {
        validate();
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        saveWrappers();
        fillWrappers();
    }

    private void saveWrappers()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());

        if(util == null)
            throw new IllegalStateException();
        EnrEnrollmentCampaign enrollmentCampaign = _uiSettings.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        Object orgUnit =  _uiSettings.get(SETTING_ORG_UNIT);
        Object requestType = util.getFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE.getSettingsName()).getValue();
        Object disciplineWrapper = _uiSettings.get(SETTING_ENR_DISCIPLINE);
        if(enrollmentCampaign == null || orgUnit == null || requestType == null || disciplineWrapper == null)
            throw new ApplicationException("Не заполнены обязательные фильтры");

        EnrExamPassDisciplineManager.instance().dao().updateExamPassDisciplineData(enrollmentCampaign, _wrappers);
    }

    public void onClickSave()
    {
        validate();
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        saveWrappers();
        _wrappers.clear();
        _editMode = false;
    }

    private void validate()
    {
        for(EnrExamPassDisciplineWrapper wrapper : _wrappers)
        {
            if(wrapper.isMarked() && wrapper.getMarkDate() == null)
                _uiSupport.error("При выставлении балла необходимо указать дату сдачи.", getMarkDateFieldId(wrapper.getId()));
        }
    }

    public void onClickCancel()
    {
        _wrappers.clear();
        _editMode = false;
    }

    public void onClickFillDate()
    {
        for(EnrExamPassDisciplineWrapper wrapper : _wrappers)
        {
            if(wrapper.isMarked() && wrapper.getMarkDate() == null)
                wrapper.setMarkDate(_defaultDate);
        }
    }

    public void onChangeAbsenceNote()
    {
        Integer index = getListenerParameter();
        if(index != null && index <= _wrappers.size())
        {
            EnrExamPassDisciplineWrapper wrapper = _wrappers.get(index - 1);
            if(wrapper.getAbsenceNote() != null)
            {
                if (wrapper.getAbsenceNote().isValid()) {
                    wrapper.setMark(null);
                } else {
                    wrapper.setMark(0L);
                }
                wrapper.setMarkDisabled(true);
            }
            else
                wrapper.setMarkDisabled(false);
        }
    }

    // Getters & Setters

    public boolean isNothingSelected()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) == null || getOrgUnit() == null;
    }

    public boolean isShowFilterWarn()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
        {
            return (util.getFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE.getSettingsName()) != null
                    && util.getFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE.getSettingsName()).getValue() == null) || getSettings().get(SETTING_ENR_DISCIPLINE) == null;
        }
        else
            return true;
    }

    public EnrOrgUnit getOrgUnit(){ return getSettings().get(SETTING_ORG_UNIT); }

    public boolean isEditMode()
    {
        return _editMode != null && _editMode;
    }

    public EnrExamPassDisciplineWrapper getCurrentWrapper()
    {
        return _currentWrapper;
    }

    public void setCurrentWrapper(EnrExamPassDisciplineWrapper currentWrapper)
    {
        _currentWrapper = currentWrapper;
    }

    public Date getDefaultDate()
    {
        return _defaultDate;
    }

    public void setDefaultDate(Date defaultDate)
    {
        _defaultDate = defaultDate;
    }

    public List<EnrExamPassDisciplineWrapper> getWrappers()
    {
        if(!_filledWrappers)
        {
            fillWrappers();
            _filledWrappers = true;
        }
        return _wrappers;
    }

    public int getWrapperIndex()
    {
        return _wrappers.indexOf(_currentWrapper) + 1;
    }

    public boolean isEmptyWrappers()
    {
        return _wrappers.isEmpty();
    }

    public SelectDataSource getCurrentAbsenceNoteDS()
    {
        return getConfig().getDataSource(getCurrentAbsenceNoteDSName(_currentWrapper.getId()));
    }

    private String getCurrentAbsenceNoteDSName(Long id)
    {
        return id.toString() + "AbsenceNoteDS";
    }

    public String getCurrentAbsenceNoteFieldId()
    {
        return _currentWrapper.getId().toString() + "_absenceNote";
    }

    public String getCurrentMarkFieldId()
    {
        return _currentWrapper.getId().toString() + "_mark";
    }

    public String getCurrentMarkDateFieldId()
    {
        return getMarkDateFieldId(_currentWrapper.getId());
    }

    private String getMarkDateFieldId(Long id)
    {
        return id.toString() + "_markDate";
    }

    public ISelectModel getMarkedModel()
    {
        return markedModel;
    }

    // utils

	private List<EnrExamGroup> getExamGroupsFromSettings()
	{
		List<EnrExamGroupScheduleEvent> groupEvents = getSettings().get(SETTING_EXAM_GROUP_SCHEDULE_EVENT);
		if (CollectionUtils.isEmpty(groupEvents))
			return new ArrayList<>();
		return groupEvents.stream().map(EnrExamGroupScheduleEvent::getExamGroup).collect(Collectors.toList());
	}

    private List<EnrExamPassDisciplineWrapper> prepareWrappers()
    {
        EnrCompetitionFilterAddon addon = (EnrCompetitionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        IUISettings settings = getSettings();

        EnrEnrollmentCampaign enrollmentCampaign = settings.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        EnrOrgUnit enrOrgUnit = settings.get(EnrExamPassDisciplineListUI.SETTING_ORG_UNIT);

        if (enrollmentCampaign == null || enrOrgUnit == null)
            return Lists.newArrayList();

        DisciplineAndPassFormWrapper dds = settings.get(EnrExamPassDisciplineListUI.SETTING_ENR_DISCIPLINE);
        Boolean rated = TwinComboDataSourceHandler.getSelectedValue(settings.get(EnrExamPassDisciplineListUI.SETTING_MARKED));
        Date ratedDateFrom = settings.get(EnrExamPassDisciplineListUI.SETTING_RATE_DATE_FROM);
        Date ratedDateTo = settings.get(EnrExamPassDisciplineListUI.SETTING_RATE_DATE_TO);
        EnrAbsenceNote absenceNote = settings.get(EnrExamPassDisciplineListUI.SETTING_ABSENCE_NOTE);
        DataWrapper examGroupSet = settings.get(EnrExamPassDisciplineListUI.SETTING_EXAM_GROUP_SET);
        List<EnrExamGroup> examGroups = getExamGroupsFromSettings();

        //final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EnrExamPassDiscipline.class, "epd");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "epd");
        builder.joinEntity("epd", DQLJoinType.left, EnrExamPassAppeal.class, "epa", eq(property("epa", EnrExamPassAppeal.examPassDiscipline()), property("epd")));
        builder.column(property("epd"));
        builder.column(property("epa"));
        builder.where(eq(property("epd", EnrExamPassDiscipline.entrant().enrollmentCampaign()), commonValue(enrollmentCampaign)));
        builder.where(eq(property("epd", EnrExamPassDiscipline.territorialOrgUnit()), commonValue(enrOrgUnit.getInstitutionOrgUnit().getOrgUnit())));

        if (dds != null)
        {
            builder.where(eq(property("epd", EnrExamPassDiscipline.discipline()), value(dds.getCampaignDisciplineId())));
            builder.where(eq(property("epd", EnrExamPassDiscipline.passForm()), value(dds.getPassFormId())));
        }

        if (addon != null)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
            subBuilder.joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().examSetVariant().fromAlias("rc"), "esv");
            subBuilder.joinEntity("rc", DQLJoinType.inner, EnrExamVariant.class, "ev", eq(property("ev", EnrExamVariant.examSetVariant()), property("esv")));
            subBuilder.joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ev"), "v");
            subBuilder.joinEntity("rc", DQLJoinType.inner, IEnrExamSetElementValue.class, "el", eq(property("el"), property("v")));
            subBuilder.joinEntity("ev", DQLJoinType.inner, EnrExamVariantPassForm.class, "evpf", eq(property("evpf", EnrExamVariantPassForm.examVariant()), property("ev")));

            subBuilder.joinEntity("ev", DQLJoinType.left, EnrCampaignDiscipline.class, "cd", eq(property("cd", EnrCampaignDiscipline.id()), property("el", IEnrExamSetElementValueGen.id())));
            subBuilder.joinEntity("ev", DQLJoinType.left, EnrCampaignDisciplineGroup.class, "cdg", eq(property("cdg", EnrCampaignDisciplineGroup.id()), property("el", IEnrExamSetElementValueGen.id())));

            subBuilder.joinEntity("cdg", DQLJoinType.left, EnrCampaignDisciplineGroupElement.class, "cdge", eq(property("cdge", EnrCampaignDisciplineGroupElement.group()), property("cdg")));
            subBuilder.joinPath(DQLJoinType.left, EnrCampaignDisciplineGroupElement.discipline().fromAlias("cdge"), "gd");

            subBuilder.where(or(
                    eq(property("epd", EnrExamPassDiscipline.discipline()), property("cd")),
                    eq(property("epd", EnrExamPassDiscipline.discipline()), property("gd"))
            ));

            subBuilder.where(in(property("rc", EnrRequestedCompetition.competition()), addon.getEntityIdsFilteredBuilder("a").buildQuery()));
            subBuilder.where(eq(property("rc", EnrRequestedCompetition.request().entrant()), property("epd", EnrExamPassDiscipline.entrant())));

            builder.where(exists(subBuilder.buildQuery()));
        }

        if (rated != null)
        {
            if (rated)
                builder.where(or(
                        isNotNull(property("epd", EnrExamPassDiscipline.markAsLong())),
                        isNotNull(property("epd", EnrExamPassDiscipline.absenceNote()))
                ));
            else
                builder.where(and(
                        isNull(property("epd", EnrExamPassDiscipline.markAsLong())),
                        isNull(property("epd", EnrExamPassDiscipline.absenceNote()))
                ));
        }

        builder.where(betweenDays(EnrExamPassDiscipline.markDate().fromAlias("epd"), ratedDateFrom, ratedDateTo));

        if (absenceNote != null)
            builder.where(eq(property("epd", EnrExamPassDiscipline.absenceNote()), commonValue(absenceNote)));

        if (examGroupSet != null)
            builder.where(eq(property("epd", EnrExamPassDiscipline.examGroup().examGroupSet().id()), value(examGroupSet.getId())));

        if (CollectionUtils.isNotEmpty(examGroups))
            builder.where(in(property("epd", EnrExamPassDiscipline.examGroup()), examGroups));

        builder.order(property("epd", EnrExamPassDiscipline.entrant().person().identityCard().fullFio()));
        builder.order(property("epd", EnrExamPassDiscipline.discipline().discipline().title()));
        builder.order(property("epd", EnrExamPassDiscipline.id()));

        List<EnrExamPassDisciplineWrapper> wrappers = Lists.newArrayList();

        for (Object[] obj : DataAccessServices.dao().<Object[]>getList(builder))
        {
            wrappers.add(new EnrExamPassDisciplineWrapper((EnrExamPassDiscipline) obj[0], (EnrExamPassAppeal) obj[1]));
        }

        return wrappers;
    }
}
