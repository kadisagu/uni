/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubCustomStateTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.CustomStateAddEdit.EnrEntrantCustomStateAddEdit;

import java.util.Date;

/**
 * @author oleyba
 * @since 6/11/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantId")
})
public class EnrEntrantPubCustomStateTabUI extends UIPresenter
{
    private Long _entrantId;


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(_uiSettings.getAsMap(true, "customState", "activeDateFrom", "activeDateTo"));
        dataSource.put("entrantId", _entrantId);
    }

    public void onClickAddCustomState()
    {
        _uiActivation.asRegionDialog(EnrEntrantCustomStateAddEdit.class).
                parameter("entrantId", _entrantId).
                activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EnrEntrantCustomStateAddEdit.class).
                parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).
                parameter("entrantId", _entrantId).
                activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickApply()
    {
        Date activeDateFrom = _uiSettings.get("activeDateFrom");
        Date activeDateTo = _uiSettings.get("activeDateTo");
        if(null != activeDateFrom && null != activeDateTo)
        {
            if(activeDateFrom.after(activeDateTo))
            {
                _uiSupport.error("Дата активности статуса «с» должна быть не больше даты активности «по»", "activeDateFrom", "activeDateTo");
            }
        }
        if(!getUserContext().getErrorCollector().hasErrors())
        {
            saveSettings();
        }
    }

    // getters and setters

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }
}