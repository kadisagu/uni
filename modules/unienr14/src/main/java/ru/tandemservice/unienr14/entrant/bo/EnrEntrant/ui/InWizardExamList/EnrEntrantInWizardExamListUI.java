package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardExamList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.TopRegionActivation;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExams;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExamsUI;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab.EnrEntrantPubExamTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab.EnrEntrantPubExamTabUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizardUI;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Map;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_ENTRANT_ID, binding="entrantHolder.id", required=true)
})
@TopRegionActivation
public class EnrEntrantInWizardExamListUI extends UIPresenter {

    private final EntityHolder<EnrEntrant> entrantHolder = new EntityHolder<>();
    public EntityHolder<EnrEntrant> getEntrantHolder() { return this.entrantHolder; }
    public EnrEntrant getEntrant() { return this.getEntrantHolder().getValue(); }
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return this.getEntrant().getEnrollmentCampaign(); }

    public String getEntrantExamsTabComponentName() {
        return EnrEntrantPubExamTab.class.getSimpleName();
    }

    public Map<String, Object> getEntrantExamsTabComponentParameters() {
        return new ParametersMap()
        .add(UIPresenter.PUBLISHER_ID, getEntrantHolder().getId())
        .add(EnrEntrantPubExamTabUI.PARAM_INLINE, Boolean.TRUE)
        .add(EnrEntrantEditChosenExamsUI.PARAM_FORCE_REFRESH_EXAM_LIST, Boolean.TRUE);
    }

    @Override
    public void onComponentActivate() {
        try {
            _uiActivation.asRegion(EnrEntrantEditChosenExams.class).top()
            .parameter("entrantId", getEntrant().getId())
            .parameter(EnrEntrantEditChosenExamsUI.PARAM_FORCE_REFRESH_EXAM_LIST, Boolean.TRUE)
            .activate();
        } catch (Throwable t) {
            // do nothing
        }
    }

    @Override
    public void onComponentRefresh() {
    }

    public void onClickNext() {

        // здесь нужно реализовать проверки
        {

        }

        // явно обновляем экзам лист
        EnrExamPassDisciplineManager.instance().dao().doRefreshExamList(getEntrant());

        // просто переходим дальше
        deactivate(
            new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), getEntrantHolder().getId())
            .buildMap()
        );
    }

}
