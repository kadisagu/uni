package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.rating.entity.gen.EnrEntrantMarkSourceGen;

/**
 * Основание балла
 */
public abstract class EnrEntrantMarkSource extends EnrEntrantMarkSourceGen  implements IEntityDebugTitled
{
    /**
     * Описание источника балла: [балл] - [тип источника] ([описание источника])
     * Например: 99 - ЕГЭ (66-8908989080-55)
     * Подробнее см. в сабклассах
     * @return описание
     */
    public abstract String getInfoString();
    public abstract String getDocumentTitleForPrint();

    @EntityDSLSupport(parts = EnrEntrantMarkSource.P_MARK_AS_LONG)
    @Override
    public String getMarkAsString() {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
    }

    @EntityDSLSupport(parts = EnrEntrantMarkSource.P_MARK_AS_LONG)
    @Override
    public Double getMarkAsDouble() {
        return (double) getMarkAsLong() / (double) 1000;
    }

    @Override
    public String getEntityDebugTitle() {
        return getInfoString();
    }
}