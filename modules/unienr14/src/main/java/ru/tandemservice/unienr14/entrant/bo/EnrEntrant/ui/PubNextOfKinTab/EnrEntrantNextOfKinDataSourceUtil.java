/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubNextOfKinTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;

import java.util.List;

/**
 * @author oleyba
 * @since 6/12/14
 */
public class EnrEntrantNextOfKinDataSourceUtil
{
    public interface Owner extends IUIPresenter
    {
        Person getPerson();
        DynamicListDataSource<PersonNextOfKin> getDataSource();
        ISecureRoleContext getSecureRoleContext();
    }

    public static DynamicListDataSource<PersonNextOfKin> createDataSource(final Owner owner, boolean allowLinks)
    {
        DynamicListDataSource<PersonNextOfKin> dataSource = new DynamicListDataSource<>(owner.getConfig().getBusinessComponent(), component -> {
            MQBuilder builder = new MQBuilder(PersonNextOfKin.ENTITY_CLASS, "o");
            builder.addJoin("o", PersonNextOfKin.L_PERSON, "e");

            builder.add(MQExpression.eq("e", "id", owner.getPerson().getId()));

            OrderDescriptionRegistry orderSettings = new OrderDescriptionRegistry("o");
            orderSettings.setOrders(PersonNextOfKin.P_FULLFIO, new OrderDescription("o", PersonNextOfKin.P_LAST_NAME), new OrderDescription("o", PersonNextOfKin.P_FIRST_NAME), new OrderDescription("o", PersonNextOfKin.P_MIDDLE_NAME));
            orderSettings.applyOrder(builder, owner.getDataSource().getEntityOrder());

            List<PersonNextOfKin> entities = ISharedBaseDao.instance.get().getList(builder);
            int count = Math.max(entities.size() + 1, 2);
            owner.getDataSource().setTotalSize(count);
            owner.getDataSource().setCountRow(count);
            owner.getDataSource().createPage(entities);
        }, 5);

        if (allowLinks) {
            PublisherLinkColumn linkColumn = new PublisherLinkColumn("ФИО", PersonNextOfKin.P_FULLFIO);
            linkColumn.setResolver(new DefaultPublisherLinkResolver()
            {
                @Override
                public Object getParameters(IEntity entity)
                {
                    ParametersMap map = new ParametersMap();
                    map.add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                    map.add(ISecureRoleContext.SECURE_ROLE_CONTEXT, owner.getSecureRoleContext());
                    return map;
                }
            });
            dataSource.addColumn(linkColumn);
        } else {
            dataSource.addColumn(new SimpleColumn("ФИО", PersonNextOfKin.P_FULLFIO).setClickable(false));
        }
        dataSource.addColumn(new SimpleColumn("Степень родства", PersonNextOfKin.relationDegree().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", PersonNextOfKin.P_BIRTH_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контактный телефон", PersonNextOfKin.P_PHONES).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Место работы", PersonNextOfKin.P_EMPLOYMENT_PLACE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Должность", PersonNextOfKin.P_POST).setClickable(false));
        dataSource.addColumn(new BooleanColumn("На инвалидности", PersonNextOfKin.P_DISABLED_PERSON));
        return dataSource;
    }
}
