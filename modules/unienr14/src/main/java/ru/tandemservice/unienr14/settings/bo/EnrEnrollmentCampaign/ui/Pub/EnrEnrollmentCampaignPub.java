/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.SecTab.EnrEnrollmentCampaignSecTab;

/**
 * @author oleyba
 * @since 6/25/14
 */
@Configuration
public class EnrEnrollmentCampaignPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("enrollmentCampaignPubTabPanel")
                .addTab(htmlTab("infoTab", "EnrollmentCampaignInfoTab"))
                .addTab(componentTab("secTab", EnrEnrollmentCampaignSecTab.class))
                .create();
    }
}