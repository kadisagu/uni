/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.logic;

import jxl.CellView;
import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.EnrReportEnrollmentCampaignPassSummaryAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.*;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEnrollmentCampaignPassSummary;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 10.06.2014
 */
public class EnrReportEnrollmentCampaignPassSummaryDao extends UniBaseDao implements IEnrReportEnrollmentCampaignPassSummaryDao
{
    public static final String MASTER_CONST = "магистр";

    private static boolean hideEgeF4;
    private static boolean hideEgeS4;
    private static int egeS4Col = 0;
    private static int egeF4Col = 0;
    private static boolean deleteSheet = true;
    private static boolean budget = false;



    @Override
    public Long createReport(EnrReportEnrollmentCampaignPassSummaryAddUI model) {

        EnrReportEnrollmentCampaignPassSummary report = new EnrReportEnrollmentCampaignPassSummary();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setStage(model.getStage().getTitle());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEnrollmentCampaignPassSummary.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEnrollmentCampaignPassSummary.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEnrollmentCampaignPassSummary.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEnrollmentCampaignPassSummary.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEnrollmentCampaignPassSummary.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEnrollmentCampaignPassSummary.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEnrollmentCampaignPassSummary.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEnrollmentCampaignPassSummary.P_PROGRAM_SET, "title");
        if (model.isParallelActive())
            report.setParallel(model.getParallel().getTitle());
        report.setFirstPriorityOnly(model.isFirstPriorityOnly() ? "Да" : "Нет");
        if (model.isFilterByEnrollmentCommission())
            report.setEnrollmentCommission(UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; "));
        report.setSkipEmpty(model.isSkipEmpty() ? "Да" : "Нет");
        report.setRowFormingType(model.getFormingType().getTitle());
        DatabaseFile content = new DatabaseFile();
        budget = ((CompensationType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget();

        try {
            content.setContent(buildReport(model));
        }
        catch (JXLException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }
        catch (IOException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }
        content.setFilename("EnrReport.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEnrollmentCampaignPassSummaryAddUI model) throws JXLException, IOException
    {
        // документ
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);

        EnrEnrollmentCampaign campaign = model.getEnrollmentCampaign();

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(campaign);
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());


        EnrReportParallelSelector.applyParallelFilter(requestedCompDQL, requestedCompDQL.reqComp(), model.isParallelActive(), model.isParallelOnly());

       /* if (model.isParallelActive())
        {
            if (model.isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }*/

        if (model.isFilterByEnrollmentCommission())
        {
            EnrEnrollmentCommissionManager.instance().dao().filterRequestedCompetitionsByEnrCommission(requestedCompDQL, requestedCompDQL.reqComp(), model.getEnrollmentCommissionList());
        }

        if (model.isFirstPriorityOnly())
        {
            requestedCompDQL
                    .where(eq(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.priority()), value(1)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, requestedCompDQL.reqComp());


     /*   requestedCompDQL
                .where(le(property("reqComp", EnrRequestedCompetition.state().priority()), value(model.getStage().getId())));*/

        requestedCompDQL.column(property(requestedCompDQL.reqComp()));

        List<EnrRequestedCompetition> dataList = getList(requestedCompDQL);

        requestedCompDQL.resetColumns();
        requestedCompDQL
                .joinEntity(requestedCompDQL.reqComp(), DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property("vvi", EnrChosenEntranceExam.requestedCompetition()), property(requestedCompDQL.reqComp())))
                .column(property("vvi"));

        List<EnrChosenEntranceExam> vvi = getList(requestedCompDQL);

        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> vviMap = new HashMap<>();

        Map<EnrOrgUnit, Set<EnrRequestedCompetition>> orgUnitMap = new TreeMap<>(new Comparator<EnrOrgUnit>() {
            @Override
            public int compare(EnrOrgUnit o1, EnrOrgUnit o2) {
                return o1.getInstitutionOrgUnit().getOrgUnit().getPrintTitle().compareToIgnoreCase(o2.getInstitutionOrgUnit().getOrgUnit().getPrintTitle());
            }
        });




        // Карта, распределяющая выбранные конкурсы по подразделениям
        for (EnrRequestedCompetition competition  : dataList)
        {
            EnrOrgUnit orgUnit = competition.getCompetition().getProgramSetOrgUnit().getOrgUnit();
            if (!orgUnitMap.containsKey(orgUnit))
                orgUnitMap.put(orgUnit, new HashSet<EnrRequestedCompetition>());
            orgUnitMap.get(orgUnit).add(competition);
        }


        // Карта, связывающая наборы ВВИ с выбранными конкурсами
        for (EnrChosenEntranceExam exam : vvi)
        {
            if (!vviMap.containsKey(exam.getRequestedCompetition()))
                vviMap.put(exam.getRequestedCompetition(), new HashSet<EnrChosenEntranceExam>());
            vviMap.get(exam.getRequestedCompetition()).add(exam);
        }

        int indexOfSheet = 0;
        // Цикл с заполнением листов по филиалам
        for (Map.Entry<EnrOrgUnit, Set<EnrRequestedCompetition>> entry : orgUnitMap.entrySet())
        {

            OrgUnit orgUnit = entry.getKey().getInstitutionOrgUnit().getOrgUnit();
            StringBuilder sheetName = new StringBuilder().append(orgUnit.getPrintTitle());
            if (sheetName.length() > 30)
                sheetName.setLength(30);
            WritableSheet sheet = workbook.createSheet(sheetName.toString(), indexOfSheet++);
            deleteSheet = true;
            hideEgeF4 = true;
            hideEgeS4 = true;
            Set<EnrBenefitCategory> benefits = countBenefitCategories(entry.getValue());
            Set<EnrCampaignTargetAdmissionKind> targetAdms = countTargetAdmissions(entry.getValue());
            int index = printHeader(sheet, orgUnit, benefits, targetAdms, model);


            // В зависимости от способа формирования отчета происходит деление на стобцы и заполнение их данными
            if (model.formingByProgramSubject()) {
                Map<EduProgramSubject, Set<EnrRequestedCompetition>> anotherUgsMap = new HashMap<>();
                Map<EduProgramSubject, Set<EnrRequestedCompetition>> totalMap = new HashMap<>();

                for (EnrRequestedCompetition competition : entry.getValue()) {
                    EduProgramSubject subject = competition.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
                    if (subject instanceof EduProgramSubjectOkso || subject instanceof  EduProgramSubject2013) {
                        if (!totalMap.containsKey( subject))
                            totalMap.put(subject, new HashSet<EnrRequestedCompetition>());
                        totalMap.get( subject).add(competition);
                    }
                    else {
                        if (!anotherUgsMap.containsKey(subject))
                            anotherUgsMap.put(subject, new HashSet<EnrRequestedCompetition>());
                        anotherUgsMap.get(subject).add(competition);
                    }
                }
                printContentTable(sheet, totalMap, anotherUgsMap, index, benefits, targetAdms, vviMap, orgUnit, model);
            }


            //
            else {
                Map<EnrProgramSetBase, Set<EnrRequestedCompetition>> programSetMap = new HashMap<>();
                List<PairKey<String[], Set<EnrRequestedCompetition>>> printList = new ArrayList<>();

                for (EnrRequestedCompetition competition : entry.getValue())
                {
                    EnrProgramSetBase programSet = competition.getCompetition().getProgramSetOrgUnit().getProgramSet();
                    if (!programSetMap.containsKey(programSet))
                        programSetMap.put(programSet, new HashSet<EnrRequestedCompetition>());
                        programSetMap.get(programSet).add(competition);
                }


                for (Map.Entry<EnrProgramSetBase, Set<EnrRequestedCompetition>> pair : programSetMap.entrySet())
                {
                    printList.add(new PairKey<String[], Set<EnrRequestedCompetition>>(new String[]{"", pair.getKey().getTitle()}, pair.getValue()));
                }
                printContentTable(sheet, printList, index, benefits, targetAdms, vviMap, orgUnit, model);

            }

            if (hideEgeF4)
            {
                CellView view = sheet.getColumnView(egeF4Col);
                view.setHidden(true);
                sheet.setColumnView(egeF4Col, view);
            }
            if (hideEgeS4)
            {
                CellView view = sheet.getColumnView(egeS4Col);
                view.setHidden(true);
                sheet.setColumnView(egeS4Col, view);
            }
            if (deleteSheet && model.isSkipEmpty())
                workbook.removeSheet(--indexOfSheet);

        }


        // Сводный лист:

        WritableSheet total = workbook.createSheet("Всего", indexOfSheet);

        deleteSheet = true;
        hideEgeF4 = true;
        hideEgeS4 = true;

        Set<EnrBenefitCategory> benefits = countBenefitCategories(dataList);
        Set<EnrCampaignTargetAdmissionKind> targets = countTargetAdmissions(dataList);

        OrgUnit topOrgUnit = TopOrgUnit.getInstance();

        if (topOrgUnit != null) {
            int index = printHeader(total, null, benefits, targets, model);

            if (model.formingByProgramSubject()) {
                Map<EduProgramSubject, Set<EnrRequestedCompetition>> anotherUgsMap = new HashMap<>();
                Map<EduProgramSubject, Set<EnrRequestedCompetition>> totalMap = new HashMap<>();

                for (EnrRequestedCompetition competition : dataList) {
                    EduProgramSubject subject = competition.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
                    if (subject instanceof EduProgramSubjectOkso || subject instanceof EduProgramSubject2013) {
                        if (!totalMap.containsKey(subject))
                            totalMap.put(subject, new HashSet<EnrRequestedCompetition>());
                        totalMap.get(subject).add(competition);
                    } else {
                        if (!anotherUgsMap.containsKey(subject))
                            anotherUgsMap.put(subject, new HashSet<EnrRequestedCompetition>());
                        anotherUgsMap.get(subject).add(competition);
                    }
                }
                printContentTable(total, totalMap, anotherUgsMap, index, benefits, targets, vviMap, topOrgUnit, model);

            } else {
                Map<EnrProgramSetBase, Set<EnrRequestedCompetition>> programSetMap = new HashMap<>();
                List<PairKey<String[], Set<EnrRequestedCompetition>>> printList = new ArrayList<>();

                for (EnrRequestedCompetition competition : dataList) {
                    EnrProgramSetBase programSet = competition.getCompetition().getProgramSetOrgUnit().getProgramSet();
                    if (!programSetMap.containsKey(programSet))
                        programSetMap.put(programSet, new HashSet<EnrRequestedCompetition>());
                    programSetMap.get(programSet).add(competition);
                }
                for (Map.Entry<EnrProgramSetBase, Set<EnrRequestedCompetition>> pair : programSetMap.entrySet()) {
                    printList.add(new PairKey<String[], Set<EnrRequestedCompetition>>(new String[]{"", pair.getKey().getTitle()}, pair.getValue()));
                }
                printContentTable(total, printList, index, benefits, targets, vviMap, topOrgUnit, model);
            }

            if (hideEgeF4) {
                CellView view = total.getColumnView(egeF4Col);
                view.setHidden(true);
                total.setColumnView(egeF4Col, view);
            }
            if (hideEgeS4) {
                CellView view = total.getColumnView(egeS4Col);
                view.setHidden(true);
                total.setColumnView(egeS4Col, view);
            }
        }
        workbook.write();
        workbook.close();

        byte[] result = out.toByteArray();
        out.close();
        return result;
    }


    // Метод для вывода данных в документ при формировании по наборам обр. программ
    private void printContentTable(WritableSheet sheet,
                                          List<PairKey<String[], Set<EnrRequestedCompetition>>> printList,
                                          int rowIndex,
                                          Set<EnrBenefitCategory> benefits,
                                          Set<EnrCampaignTargetAdmissionKind> targets,
                                          Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> exams,
                                          OrgUnit orgUnit,
                                          EnrReportEnrollmentCampaignPassSummaryAddUI model
                                         ) throws JXLException
    {

        CellMergeMap mergeMap = new CellMergeMap();

        boolean foreign = false;
        if (orgUnit.getAddress() instanceof AddressInter)
            foreign = true;

        // оффсет считает разницу между стартовой строкой и текущей
        int offset = 0;
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        boolean firstIteration = true;
        String prevC0Label = "";
        int prevC0RowIndex = 0;

        // сортировка данных по алфавиту
        Collections.sort(printList, new Comparator<PairKey<String[], Set<EnrRequestedCompetition>>>() {
            @Override
            public int compare(PairKey<String[], Set<EnrRequestedCompetition>> o1, PairKey<String[], Set<EnrRequestedCompetition>> o2) {
                int result = o1.getFirst()[0].compareToIgnoreCase(o2.getFirst()[0]);
                if (result != 0) return result;
                else return o1.getFirst()[1].compareToIgnoreCase(o2.getFirst()[1]);
            }
        });

        for (PairKey<String[], Set<EnrRequestedCompetition>> pair : printList)
        {
            List<Integer> values = countValues(pair.getSecond(), benefits, targets, exams, orgUnit, foreign);
            List<String> row = new ArrayList<>();

            if (pair.getFirst()[0].equals("")) {
                mergeMap.addMapping(0, rowIndex + offset, 1, rowIndex + offset);
                row.add(pair.getFirst()[1]);
                row.add(pair.getFirst()[0]);
            }
            else
            {
                row.add(pair.getFirst()[0]);
                row.add(pair.getFirst()[1]);
                if (firstIteration)
                {
                    prevC0RowIndex = rowIndex + offset;
                    prevC0Label = pair.getFirst()[0];
                    firstIteration = false;
                }
                if (!prevC0Label.equals(pair.getFirst()[0]))
                {
                    mergeMap.addMapping(0, prevC0RowIndex, 0, rowIndex + offset - 1);
                    prevC0Label = pair.getFirst()[0];
                    prevC0RowIndex = rowIndex + offset;
                }
            }

            for (Integer i : values) {
                row.add(String.valueOf(i));

            }

            // Если чекбокс "пропускать пустые строки" активен, то пропускаем строки, в которых конкурс = 0
            if (!model.isSkipEmpty() || values.get(2)!=0) {
                ExcelReportUtils.printrow(sheet, row, rowIndex + offset++, contentFormat);
                deleteSheet = false; // если чекбокс не выделен или напечатана хоть одна строка, то лист остается
            }
        }

        mergeMap.makeMerges(sheet);
    }


    // Метод для вывода данных в документ при формировании по напр., спец., проф.
    private void printContentTable(WritableSheet sheet,
                                          Map<EduProgramSubject, Set<EnrRequestedCompetition>> content,
                                          Map<EduProgramSubject, Set<EnrRequestedCompetition>> anotherUgs,
                                          int rowIndex,
                                          Set<EnrBenefitCategory> benefits,
                                          Set<EnrCampaignTargetAdmissionKind> targets,
                                          Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> exams,
                                          OrgUnit orgUnit,
                                          EnrReportEnrollmentCampaignPassSummaryAddUI model
                                         ) throws JXLException
    {
        CellMergeMap mergeMap = new CellMergeMap();

        boolean foreign = false;
        if (orgUnit.getAddress() instanceof AddressInter)
            foreign = true;

        // оффсет считает разницу между стартовой строкой и текущей
        int offset = 0;
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        boolean firstIteration = true;
        String prevC0Label = "";
        int prevC0RowIndex = 0;

        List<PairKey<EduProgramSubject, Set<EnrRequestedCompetition>>> printList = new ArrayList<>();
        for(Map.Entry<EduProgramSubject, Set<EnrRequestedCompetition>> pair : content.entrySet())
            printList.add(new PairKey<EduProgramSubject, Set<EnrRequestedCompetition>>(pair.getKey(), pair.getValue()));

        // сортировка данных по алфавиту
        Collections.sort(printList, new Comparator<PairKey<EduProgramSubject, Set<EnrRequestedCompetition>>>() {
            @Override
            public int compare(PairKey<EduProgramSubject, Set<EnrRequestedCompetition>> o1, PairKey<EduProgramSubject, Set<EnrRequestedCompetition>> o2) {
                int result = o1.getFirst().getGroupTitle().compareToIgnoreCase(o2.getFirst().getGroupTitle());
                if (result != 0)
                    return o1.getFirst().getTitleWithCode().compareToIgnoreCase(o2.getFirst().getTitleWithCode())*1000;
                return o1.getFirst().getTitleWithCode().compareToIgnoreCase(o2.getFirst().getTitleWithCode());
            }
        });

        for (PairKey<EduProgramSubject, Set<EnrRequestedCompetition>> pair : printList)
        {
            List<Integer> values = countValues(pair.getSecond(), benefits, targets, exams, orgUnit, foreign);
            List<String> row = new ArrayList<>();
            row.add(pair.getFirst().getGroupTitle());
            row.add(pair.getFirst().getTitleWithCode());
            if (firstIteration)
            {
                prevC0RowIndex = rowIndex + offset;
                prevC0Label = pair.getFirst().getGroupTitle();
                firstIteration = false;
            }
            if (!prevC0Label.equals(pair.getFirst().getGroupTitle()))
            {
                mergeMap.addMapping(0, prevC0RowIndex, 0, rowIndex + offset - 1);
                prevC0Label = pair.getFirst().getGroupTitle();
                prevC0RowIndex = rowIndex + offset;
            }

            for (Integer i : values) {
                row.add(String.valueOf(i));
            }
            if (!model.isSkipEmpty() || values.get(2)!=0) {
                ExcelReportUtils.printrow(sheet, row, rowIndex + offset++, contentFormat);
                deleteSheet = false;
            }
        }
        prevC0RowIndex = rowIndex + offset;
        for (Map.Entry<EduProgramSubject, Set<EnrRequestedCompetition>> pair : anotherUgs.entrySet())
        {
            List<Integer> values = countValues(pair.getValue(), benefits, targets, exams, orgUnit, foreign);
            List<String> row = new ArrayList<>();
            row.add("Другие");
            row.add(pair.getKey().getTitleWithCode());
            for (Integer i : values)
                row.add(String.valueOf(i));
            if (!model.isSkipEmpty() || values.get(2)!=0)
                ExcelReportUtils.printrow(sheet, row, rowIndex+offset++, contentFormat);
        }

        if (prevC0RowIndex < (rowIndex + offset - 1))
            mergeMap.addMapping(0, prevC0RowIndex, 0, rowIndex + offset - 1);

        mergeMap.makeMerges(sheet);
    }








    // Метод, формирующий числовой контент таблицы
    private List<Integer> countValues(Collection<EnrRequestedCompetition> set,
                                             Set<EnrBenefitCategory> benefits,
                                             Set<EnrCampaignTargetAdmissionKind> targets,
                                             Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> exams,
                                             OrgUnit orgUnit,
                                             boolean foreign)
    {
        List<Integer> result = new ArrayList<>();
        boolean ouHasAddress = false;
        int year = CoreDateUtils.getYear(new Date());

        // Проверка подразделения на not null адрес.
        if (orgUnit.getAddress()!=null && orgUnit.getAddress().getSettlement() != null)
            ouHasAddress = true;
        AddressItem orgUnitRegion = null;
        // нахождение региона подразделения
        if (ouHasAddress)
        {
            orgUnitRegion = orgUnit.getAddress().getSettlement();
            if (!foreign) {
                while (true) {
                    if (orgUnitRegion.getParent() != null)
                        orgUnitRegion = orgUnitRegion.getParent();
                    else break;
                }
            }
        }

        // Сеты для подсчета количества абитуриентов, подходящих под выборку столбца
        Set<EnrProgramSetOrgUnit> programSetOrgUnits = new HashSet<>();
        Set<EnrEntrant> entrantsTotal = new HashSet<>();

        Set<EnrEntrant> originalDocs = new HashSet<>();
        Set<EnrEntrant> acceptedEnrollment = new HashSet<>();
        Set<EnrEntrant> finalAcceptance = new HashSet<>();



        Set<EnrEntrant> quotaTotal = new HashSet<>();
        Map<EnrBenefitCategory, Set<EnrEntrant>> quotaSpecial = new HashMap<>();

        Set<EnrEntrant> targetTotal = new HashSet<>();
        Map<EnrCampaignTargetAdmissionKind, Set<EnrEntrant>> targetSpecial = new HashMap<>();

        Set<EnrEntrant> olimpNoExam = new HashSet<>();
        Set<EnrEntrant> olimp100Points = new HashSet<>();


        Set<EnrEntrant> sooTotal = new HashSet<>();
        Set<EnrEntrant> sooDay = new HashSet<>();
        Set<EnrEntrant> sooEvn = new HashSet<>();
        Set<EnrEntrant> sooCorrect = new HashSet<>();

        Set<EnrEntrant> sooTotalThisYear = new HashSet<>();
        Set<EnrEntrant> sooDayThisYear = new HashSet<>();
        Set<EnrEntrant> sooEvnThisYear = new HashSet<>();
        Set<EnrEntrant> sooCorrectThisYear = new HashSet<>();

        Set<EnrEntrant> spoTotal = new HashSet<>();
        Set<EnrEntrant> spoNPO = new HashSet<>();
        Set<EnrEntrant> spoProf = new HashSet<>();

        Set<EnrEntrant> spoTotalThisYear = new HashSet<>();
        Set<EnrEntrant> spoNPOThisYear = new HashSet<>();
        Set<EnrEntrant> spoProfThisYear = new HashSet<>();

        Set<EnrEntrant> voTotal = new HashSet<>();
        Set<EnrEntrant> voBach = new HashSet<>();
        Set<EnrEntrant> voSpec = new HashSet<>();
        Set<EnrEntrant> voMag= new HashSet<>();

        Set<EnrEntrant> voTotalThisYear = new HashSet<>();
        Set<EnrEntrant> voBachThisYear = new HashSet<>();
        Set<EnrEntrant> voSpecThisYear = new HashSet<>();
        Set<EnrEntrant> voMagThisYear = new HashSet<>();

        Set<EnrEntrant> parallel = new HashSet<>();
        Set<Person> parallelWCurrent = new HashSet<>();

        Set<EnrEntrant> men = new HashSet<>();
        Set<EnrEntrant> menPassedArmy = new HashSet<>();

        Set<EnrEntrant> region = new HashSet<>();
        Set<EnrEntrant> city = new HashSet<>();
        Set<EnrEntrant> regionVillage = new HashSet<>();

        Set<EnrEntrant> anotherRegion = new HashSet<>();
        Set<EnrEntrant> anotherRegionVillage = new HashSet<>();

        Set<EnrEntrant> anotherCountry = new HashSet<>();
        Set<EnrEntrant> foreignCitizenship = new HashSet<>();

        Set<EnrEntrant> medal = new HashSet<>();
        Set<EnrEntrant> perfectDiploma = new HashSet<>();
        Set<EnrEntrant> exclusiveRights = new HashSet<>();

        Set<EnrEntrant> egeFTotal = new HashSet<>();
        Set<EnrEntrant> egeF1 = new HashSet<>();
        Set<EnrEntrant> egeF2 = new HashSet<>();
        Set<EnrEntrant> egeF3 = new HashSet<>();
        Set<EnrEntrant> egeF4 = new HashSet<>();

        Set<EnrEntrant> egeSTotal = new HashSet<>();
        Set<EnrEntrant> egeS1 = new HashSet<>();
        Set<EnrEntrant> egeS2 = new HashSet<>();
        Set<EnrEntrant> egeS3 = new HashSet<>();
        Set<EnrEntrant> egeS4 = new HashSet<>();

        for (EnrRequestedCompetition reqComp : set)
        {
            EnrEntrantRequest request = reqComp.getRequest();
            EnrEntrant entrant = request.getEntrant();
            EnrCompetition competition = reqComp.getCompetition();

            // все конкурсы, попавшие в строку
            programSetOrgUnits.add(competition.getProgramSetOrgUnit());

            // все абитуриенты
            entrantsTotal.add(entrant);


            // Сдан оригинал
            if (reqComp.isOriginalDocumentHandedIn())
                originalDocs.add(entrant);

            // Согласие на зачисление
            if (reqComp.isAccepted())
                acceptedEnrollment.add(entrant);

            // Итоговое согласие
            if (reqComp.isEnrollmentAvailable())
                finalAcceptance.add(entrant);


            // заполняем колонки "по квоте"
            if (competition.isExclusive()) {
                quotaTotal.add(entrant);
                if (!quotaSpecial.containsKey(((EnrRequestedCompetitionExclusive)reqComp).getBenefitCategory()))
                    quotaSpecial.put(((EnrRequestedCompetitionExclusive)reqComp).getBenefitCategory(), new HashSet<EnrEntrant>());
                quotaSpecial.get(((EnrRequestedCompetitionExclusive)reqComp).getBenefitCategory()).add(entrant);
            }

            // заполняем целевой прием
            if (competition.isTargetAdmission()){
                targetTotal.add(entrant);
                if (!targetSpecial.containsKey(((EnrRequestedCompetitionTA)reqComp).getTargetAdmissionKind()))
                    targetSpecial.put(((EnrRequestedCompetitionTA)reqComp).getTargetAdmissionKind(), new HashSet<EnrEntrant>());
                targetSpecial.get(((EnrRequestedCompetitionTA)reqComp).getTargetAdmissionKind()).add(entrant);
            }

            // Заполняем олимпиадников
            if (competition.isNoExams())
                olimpNoExam.add(entrant);
            if (exams.containsKey(reqComp))
            {
                for (EnrChosenEntranceExam exam : exams.get(reqComp))
                {
                    if (exam.getMarkSource() instanceof EnrEntrantMarkSourceBenefit)
                        olimp100Points.add(entrant);
                }
            }


            PersonEduDocument document = request.getEduDocument();

            if (document!=null && document.getEduLevel()!=null) {
                // СОО
                if (document.getEduLevel().getCode().equals(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE)) {
                    if (document.getYearEnd() != year) {
                        sooTotal.add(entrant);
                        if (request.isEveningSchool())
                            sooEvn.add(entrant);
                        if (request.isSpecialSchool())
                            sooCorrect.add(entrant);
                        else if (!request.isEveningSchool())
                            sooDay.add(entrant);
                    } else {
                        sooTotal.add(entrant);
                        sooTotalThisYear.add(entrant);
                        if (request.isEveningSchool()) {
                            sooEvn.add(entrant);
                            sooEvnThisYear.add(entrant);
                        }
                        if (request.isSpecialSchool()) {
                            sooCorrect.add(entrant);
                            sooCorrectThisYear.add(entrant);
                        } else if (!request.isEveningSchool()) {
                            sooDay.add(entrant);
                            sooDayThisYear.add(entrant);
                        }
                    }
                }

                // СПО
                if (document.getEduLevel().getCode().equals(EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE)) {
                    if (document.getYearEnd() == year) {
                        spoTotal.add(entrant);
                        spoTotalThisYear.add(entrant);
                        if (document.getEduDocumentKind().getCode().equals(EduDocumentKindCodes.DIPLOM_NPO)) {
                            spoNPO.add(entrant);
                            spoNPOThisYear.add(entrant);
                        }
                        if (reqComp.isProfileEducation()) {
                            spoProf.add(entrant);
                            spoProfThisYear.add(entrant);
                        }
                    } else {
                        spoTotal.add(entrant);
                        if (document.getEduDocumentKind().getCode().equals(EduDocumentKindCodes.DIPLOM_NPO))
                            spoNPO.add(entrant);
                        if (reqComp.isProfileEducation())
                            spoProf.add(entrant);
                    }
                }


                // ВО
                if (document.getEduLevel().getCode().equals(EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA)
                        || document.getEduLevel().getCode().equals(EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT)
                        || document.getEduLevel().getCode().equals(EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII)) {
                    if (document.getYearEnd() == year) {
                        voTotal.add(entrant);
                        voTotalThisYear.add(entrant);
                        if (document.getEduLevel().getCode().equals(EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT)) {
                            voBach.add(entrant);
                            voBachThisYear.add(entrant);
                        }
                        else if (MASTER_CONST.equals(document.getQualification())) {
                            voMag.add(entrant);
                            voMagThisYear.add(entrant);
                        } else {
                            voSpec.add(entrant);
                            voSpecThisYear.add(entrant);
                        }
                    } else {
                        voTotal.add(entrant);
                        if (document.getEduLevel().getCode().equals(EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT))
                            voBach.add(entrant);
                        else if (MASTER_CONST.equals(document.getQualification()))
                            voMag.add(entrant);
                        else
                            voSpec.add(entrant);
                    }
                }
            }
            // Параллельно поступающие
            if (reqComp.isParallel())
            {
                parallel.add(entrant);

            }

            // Мужчины, военнослужащие мужчины
            if (request.getIdentityCard().getSex().isMale())
            {
                men.add(entrant);
                if (request.getEntrant().isPassArmy())
                    menPassedArmy.add(entrant);
            }

            // Регион
            if (ouHasAddress) {

                if (foreign) {

                    if (request.getIdentityCard().getAddress() instanceof AddressInter) {

                        if (((AddressInter) request.getIdentityCard().getAddress()).getSettlement()!=null) {

                            if (((AddressInter) request.getIdentityCard().getAddress()).getSettlement().equals(orgUnit.getAddress().getSettlement()))
                                city.add(entrant);

                            if (((AddressInter) request.getIdentityCard().getAddress()).getDistrict().equals(((AddressInter) orgUnit.getAddress()).getDistrict()))
                                region.add(entrant);
                        }

                        // Другие страны
                        anotherCountry.add(entrant);
                    }
                } else {

                    if (request.getIdentityCard().getAddress() instanceof AddressRu) {

                        if (((AddressRu) request.getIdentityCard().getAddress()).getSettlement() != null) {

                            if (((AddressRu) request.getIdentityCard().getAddress()).getSettlement().equals(orgUnit.getAddress().getSettlement()))
                                city.add(entrant);

                            AddressItem entrantRegion = ((AddressRu) request.getIdentityCard().getAddress()).getSettlement();
                            while (true) {
                                if (entrantRegion.getParent() != null)
                                    entrantRegion = entrantRegion.getParent();
                                else break;
                            }

                            if (entrantRegion.equals(orgUnitRegion)) {
                                region.add(entrant);

                                if (((AddressRu) request.getIdentityCard().getAddress()).getSettlement().getAddressType().isCountryside())
                                    regionVillage.add(entrant);
                            } else {
                                anotherRegion.add(entrant);
                                if (((AddressRu) request.getIdentityCard().getAddress()).getSettlement().getAddressType().isCountryside())
                                    anotherRegionVillage.add(entrant);
                            }
                        }
                    }
                }

            }

            // Гражданство не РФ
            if (request.getIdentityCard().getCitizenship().getCode() != 0 && request.getIdentityCard().getCitizenship().getCode() != -1)
                foreignCitizenship.add(entrant);

            // Медалисты, краснодипломники
            if (document.getGraduationHonour() != null) {
                if (document.getGraduationHonour().getCode().equals(GraduationHonourCodes.GOLD_MEDAL) || document.getGraduationHonour().getCode().equals(GraduationHonourCodes.SILVER_MEDAL))
                    medal.add(entrant);

                if (document.getGraduationHonour().getCode().equals(GraduationHonourCodes.RED_DIPLOMA))
                    perfectDiploma.add(entrant);
            }

            // Преим. право
            if (request.getBenefitCategory() != null)
                exclusiveRights.add(entrant);

            // Егэ I, Егэ II
            if (exams.containsKey(reqComp))
            {
                int quantity = 0;
                boolean secondWave = false;
                for (EnrChosenEntranceExam exam : exams.get(reqComp))
                {
                    if(exam.getMarkSource() instanceof EnrEntrantMarkSourceStateExam) {
                        quantity++;
                        if (((EnrEntrantMarkSourceStateExam)exam.getMarkSource()).getStateExamResult().isSecondWave())
                            secondWave = true;
                    }
                }

                if (secondWave)
                {
                    egeSTotal.add(entrant);
                    if (quantity < 4)
                    switch (quantity){
                        case (1) : egeS1.add(entrant);
                        case (2) : egeS2.add(entrant);
                        case (3) : egeS3.add(entrant);
                    }
                    else {
                        egeS4.add(entrant);
                        hideEgeS4 = false;
                    }
                }
                else
                {
                    egeFTotal.add(entrant);
                    if (quantity < 4)
                    switch (quantity){
                        case (1) : egeF1.add(entrant);
                        case (2) : egeF2.add(entrant);
                        case (3) : egeF3.add(entrant);
                    }
                    else {
                        egeF4.add(entrant);
                        hideEgeF4 = false;
                    }
                }
            }


        }

        // Считаем количество мест по всем конкурсам в строке
        int places = 0;
        for(EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnits)
        {
            if (budget)
                places += programSetOrgUnit.getMinisterialPlan();
            else
                places += programSetOrgUnit.getContractPlan();
        }

        result.add((places));



        result.add(entrantsTotal.size());

        if (places == 0)
            result.add(0);
        else
            result.add(entrantsTotal.size()/places);

        result.add(originalDocs.size());

        result.add(acceptedEnrollment.size());
        result.add(finalAcceptance.size());

        result.add(quotaTotal.size());




        for (EnrBenefitCategory benefit : benefits)
        {
            if (quotaSpecial.containsKey(benefit)) {
                result.add(quotaSpecial.get(benefit).size());
            }
            else
                result.add(0);
        }

        result.add(targetTotal.size());

        for (EnrCampaignTargetAdmissionKind admissionKind : targets)
        {
            if (targetSpecial.containsKey(admissionKind))
                result.add(targetSpecial.get(admissionKind).size());
            else
                result.add(0);
        }

        Set<Person> parallelPersons = new HashSet<>();
        for (EnrEntrant entrant : parallel)
            parallelPersons.add(entrant.getPerson());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "student")
                .where(eq(property("student", Student.status().code()), value("1")))
                .where(in(property("student", Student.person()), parallelPersons))
                .column(property("student", Student.person()));

        List<Person> personList = getList(builder);
        parallelWCurrent.addAll(personList);

        result.add(olimpNoExam.size() + olimp100Points.size());

        result.add(olimpNoExam.size());

        result.add(olimp100Points.size());

        result.add(sooTotal.size());
        result.add(sooDay.size());
        result.add(sooEvn.size());
        result.add(sooCorrect.size());

        result.add(sooTotalThisYear.size());
        result.add(sooDayThisYear.size());
        result.add(sooEvnThisYear.size());
        result.add(sooCorrectThisYear.size());

        result.add(spoTotal.size());
        result.add(spoNPO.size());
        result.add(spoProf.size());

        result.add(spoTotalThisYear.size());
        result.add(spoNPOThisYear.size());
        result.add(spoProfThisYear.size());

        result.add(voTotal.size());
        result.add(voBach.size());
        result.add(voSpec.size());
        result.add(voMag.size());

        result.add(voTotalThisYear.size());
        result.add(voBachThisYear.size());
        result.add(voSpecThisYear.size());
        result.add(voMagThisYear.size());

        result.add(parallel.size());
        result.add(parallelWCurrent.size());

        result.add(men.size());
        result.add(menPassedArmy.size());


        result.add(region.size());
        result.add(city.size());
        result.add(regionVillage.size());

        result.add(anotherRegion.size());
        result.add(anotherRegionVillage.size());

        result.add(anotherCountry.size());

        result.add(foreignCitizenship.size());


        result.add(medal.size());
        result.add(perfectDiploma.size());
        result.add(exclusiveRights.size());

        result.add(egeFTotal.size());
        result.add(egeF1.size());
        result.add(egeF2.size());
        result.add(egeF3.size());
        result.add(egeF4.size());

        result.add(egeSTotal.size());
        result.add(egeS1.size());
        result.add(egeS2.size());
        result.add(egeS3.size());
        result.add(egeS4.size());


        return result;
    }

    // Возвращает виды квоты, которые есть среди всех переданных выбранных конкурсов
    private static Set<EnrBenefitCategory> countBenefitCategories(Collection<EnrRequestedCompetition> set)
    {
        Set<EnrBenefitCategory> result = new TreeSet<>(new Comparator<EnrBenefitCategory>() {
            @Override
            public int compare(EnrBenefitCategory o1, EnrBenefitCategory o2) {
                return o1.getShortTitle().compareToIgnoreCase(o2.getShortTitle());
            }
        });
        for (EnrRequestedCompetition competition : set)
        {
            if (competition.getCompetition().isExclusive())
                result.add(((EnrRequestedCompetitionExclusive)competition).getBenefitCategory());
        }
        return result;
    }

    // Возвращает виды целевого приема, которые есть среди всех переданных выбранных конкурсов
    private static Set<EnrCampaignTargetAdmissionKind> countTargetAdmissions(Collection<EnrRequestedCompetition> set)
    {
        Set<EnrCampaignTargetAdmissionKind> result = new TreeSet<>(new Comparator<EnrCampaignTargetAdmissionKind>() {
            @Override
            public int compare(EnrCampaignTargetAdmissionKind o1, EnrCampaignTargetAdmissionKind o2) {
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        });
        for (EnrRequestedCompetition competition : set)
        {
            if (competition.getCompetition().isTargetAdmission())
                result.add(((EnrRequestedCompetitionTA) competition).getTargetAdmissionKind());
        }
        return result;
    }

    // Формирует и вносит хэдер таблицы в xls лист.
    private int printHeader(WritableSheet sheet, OrgUnit orgUnit,
                                   Set<EnrBenefitCategory> benefits,
                                   Set<EnrCampaignTargetAdmissionKind> targets,
                                   EnrReportEnrollmentCampaignPassSummaryAddUI model) throws JXLException
    {
        int rowIndex = 0;
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);

        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat filterTitlesFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setBackground(Colour.GREY_25_PERCENT);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        filterTitlesFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        CellMergeMap mergeMap = new CellMergeMap();
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        List<String[]> hTable = new FilterParametersPrinter().getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        int orgUnitIndex = 0;
        for (int j = 0; j < hTable.size(); j++) {
            if (hTable.get(j)[0].equals("Филиал"))
                orgUnitIndex = j;
        }
        if (orgUnitIndex!=0)
            hTable.remove(orgUnitIndex);



        hTable.add(0, new String[]{"Приемная кампания", model.getEnrollmentCampaign().getTitle()});

        hTable.add(3, new String[]{"Стадия приемной кампании", model.getStage().getTitle()});

        if (model.isParallelActive())
            hTable.add(new String[]{"Поступающие параллельно", model.getParallel().getTitle()});

        if (model.isFilterByEnrollmentCommission())
            hTable.add(new String[]{"Отборочная комиссия", UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; ")});

        hTable.add(new String[]{"Только по выбранным конкурсам с первым приоритетом", model.isFirstPriorityOnly() ? "Да" : "Нет"});

        hTable.add(new String[]{"Формирование строк отчета", model.getFormingType().getTitle()});
        if (orgUnit == null)
            sheet.addCell(new Label(0, 0, "Сводная статистика по образовательной организации", labelFormat));
        else
            sheet.addCell(new Label(0, 0, orgUnit.getPrintTitle(), labelFormat));
        sheet.addCell(new Label(0, 1, "Сводка о ходе приемной кампании", labelFormat));
        sheet.addCell(new Label(1, 1, new SimpleDateFormat("dd.MM.yyyy").format(new Date()), labelFormat));


        for (int i = 0; i < hTable.size(); i++) {
            sheet.addCell(new Label(0, i+2, hTable.get(i)[0], filterTitlesFormat));
            sheet.addCell(new Label(1, i+2, hTable.get(i)[1], contentFormat));
            rowIndex = i+2;
        }

        rowIndex+=3;

        List<String> headerL1 = new ArrayList<>();
        List<String> headerL2 = new ArrayList<>();
        List<String> headerL3 = new ArrayList<>();


        int c = 0;

        headerL1.add(model.formingByProgramSubject() ? "Направление, профессия, специальность" : "Направления, профессии, специальности и профили, специализации");
        headerL1.add("");
        headerL2.add("");
        headerL2.add("");
        headerL3.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, ++c, rowIndex + 2);

        c++;

        headerL1.add("План");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Всего");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Конкурс");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Сдан оригинал");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);



        headerL1.add("Согласие на зачисление");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Итоговое согласие");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);


        headerL1.add("По квоте особых прав");
        int quotaMergeFromCell = c;
        headerL2.add("Всего");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        for (EnrBenefitCategory benefit : benefits)
        {
            headerL1.add("");
            headerL2.add(benefit.getShortTitle());
            headerL3.add("");
            mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        }
        mergeMap.addMapping(quotaMergeFromCell, rowIndex, c - 1, rowIndex);

        headerL1.add("Целевой прием");
        int targetMergeFromCell = c;
        headerL2.add("Всего");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        for (EnrCampaignTargetAdmissionKind targetAdm : targets)
        {
            headerL1.add("");
            headerL2.add(targetAdm.getTitle());
            headerL3.add("");
            mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        }
        mergeMap.addMapping(targetMergeFromCell, rowIndex, c - 1, rowIndex);

        headerL1.add("Уч. олимпиад");
        mergeMap.addMapping(c, rowIndex, c + 2, rowIndex);
        headerL2.add("всего");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("без экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("100 балл");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);


        headerL1.add("СОО");
        mergeMap.addMapping(c, rowIndex, c + 7, rowIndex);
        headerL2.add("всего");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("днев");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("веч");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("кор");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("выпуск тек. уч. года");
        mergeMap.addMapping(c, rowIndex + 1 , c + 3, rowIndex + 1);
        headerL3.add("всего");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("днев");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("веч");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("кор");
        c++;

        headerL1.add("СПО");
        mergeMap.addMapping(c, rowIndex, c + 5, rowIndex);
        headerL2.add("всего");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("НПО");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("проф.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("выпуск тек. уч. года");
        mergeMap.addMapping(c, rowIndex + 1 , c + 2, rowIndex + 1);
        headerL3.add("всего");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("НПО");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("проф.");
        c++;

        headerL1.add("ВО");

        mergeMap.addMapping(c, rowIndex, c + 7, rowIndex);
        headerL2.add("всего");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("бак");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("спец");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("маг");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("выпуск тек. уч. года");
        mergeMap.addMapping(c, rowIndex + 1 , c + 3, rowIndex + 1);
        headerL3.add("всего");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("бак");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("спец");
        c++;

        headerL1.add("");
        headerL2.add("");
        headerL3.add("маг");
        c++;


        headerL1.add("Паралл.");
        mergeMap.addMapping(c, rowIndex, c + 1, rowIndex);
        headerL2.add("Всего");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL3.add("");
        headerL1.add("");
        headerL2.add("тек. студ");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);



        headerL1.add("Мужчин");
        mergeMap.addMapping(c, rowIndex, c + 1, rowIndex);
        headerL2.add("всего");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL3.add("");
        headerL1.add("");
        headerL2.add("в/сл");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);


        headerL1.add("Регион");
        mergeMap.addMapping(c, rowIndex, c + 2, rowIndex);
        headerL2.add("всего");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL3.add("");

        headerL1.add("");
        headerL2.add("Город");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("");
        headerL2.add("село");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("др. р-ны России");
        mergeMap.addMapping(c, rowIndex, c + 1, rowIndex);
        headerL2.add("всего");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL3.add("");

        headerL1.add("");
        headerL2.add("село");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        headerL1.add("Др. гос-ва");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Иностр. граж-не");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Медаль");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Диплом с отл.");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Пр. право");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);



        headerL1.add("ЕГЭ-I");
        mergeMap.addMapping(c, rowIndex, c + 4, rowIndex);
        headerL2.add("всего");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL3.add("");
        headerL1.add("");
        headerL2.add("1 экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL1.add("");
        headerL2.add("2 экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL1.add("");
        headerL2.add("3 экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL1.add("");
        headerL2.add("4 экз.");
        headerL3.add("");
        egeF4Col = c;
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);


        headerL1.add("ЕГЭ-II");
        mergeMap.addMapping(c, rowIndex, c + 4, rowIndex);
        headerL2.add("всего");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL3.add("");
        headerL1.add("");
        headerL2.add("1 экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL1.add("");
        headerL2.add("2 экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL1.add("");
        headerL2.add("3 экз.");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);
        headerL1.add("");
        headerL2.add("4 экз.");
        headerL3.add("");
        egeS4Col = c;
        mergeMap.addMapping(c, rowIndex + 1, c++, rowIndex + 2);

        int[] widths = new int[c];
        widths[0] = 45;
        widths[1] = 25;
        for (int i = 2; i < widths.length ; i++) {
            widths[i] = 10;
        }

        setWidths(widths, sheet);


        ExcelReportUtils.printrow(sheet, headerL1, rowIndex++, headerFormat);
        ExcelReportUtils.printrow(sheet, headerL2, rowIndex++, headerFormat);
        ExcelReportUtils.printrow(sheet, headerL3, rowIndex++, headerFormat);

        mergeMap.makeMerges(sheet);

        return rowIndex;
    }



    // устанавливает ширину ячеек
    private static void setWidths(int[] widths, WritableSheet sheet)
    {
        for (int i = 0; i < widths.length; i++) {
            sheet.setColumnView(i, widths[i]);
        }
    }
}
