/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.EnrCancelParagraphManager;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author oleyba
 * @since 7/10/14
 */
@Input({
    @Bind(key = EnrCancelParagraphAddEditUI.PARAMETER_ORDER_ID, binding = EnrCancelParagraphAddEditUI.PARAMETER_ORDER_ID),
    @Bind(key = EnrCancelParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, binding = EnrCancelParagraphAddEditUI.PARAMETER_PARAGRAPH_ID)
})
public class EnrCancelParagraphAddEditUI extends UIPresenter
{
    public static final String PARAMETER_ORDER_ID = "order.id";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraph.id";

    private EnrOrder _order = new EnrOrder();
    private EnrCancelParagraph _paragraph = new EnrCancelParagraph();
    private boolean _editForm;

    // выбор приказа
    private EnrOrderType orderType;

    private Set<Long> _selectedExtractIds;

    @Override
    public void onComponentRefresh()
    {
        if (getParagraph().getId() == null)
        {
            // создание параграфа
            setEditForm(false);
            setOrder(DataAccessServices.dao().getNotNull(EnrOrder.class, getOrder().getId()));
            setParagraph(new EnrCancelParagraph());
        }
        else
        {
            // редактирование параграфа
            setEditForm(true);
            setParagraph(DataAccessServices.dao().getNotNull(EnrCancelParagraph.class, getParagraph().getId()));
            setOrder((EnrOrder) getParagraph().getOrder());

            EnrOrder cancelledOrder = (EnrOrder) getParagraph().getCancelledOrder();
            setOrderType(cancelledOrder.getType());
            setSelectedExtractIds(new HashSet<Long>());

            for (EnrCancelExtract extract : IUniBaseDao.instance.get().getList(EnrCancelExtract.class, EnrCancelExtract.paragraph(), getParagraph(), EnrCancelExtract.number().s())) {
                getSelectedExtractIds().add(extract.getEntity().getId());
            }
        }

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getOrder().getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + getOrder().getState().getTitle() +"», редактирование параграфов запрещено.");
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrCancelParagraphAddEdit.BIND_PARAGRAPH, getParagraph());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getOrder().getEnrollmentCampaign());
        dataSource.put(EnrCancelParagraphAddEdit.BIND_REQUEST_TYPE, getOrder().getRequestType());
        dataSource.put(EnrCancelParagraphAddEdit.BIND_COMPENSATION_TYPE, getOrder().getCompensationType());
        dataSource.put(EnrCancelParagraphAddEdit.BIND_CANCELLED_ORDER_TYPE, getOrderType());
        dataSource.put(EnrCancelParagraphAddEdit.BIND_CANCELLED_ORDER, getParagraph().getCancelledOrder());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (isEditForm() && EnrCancelParagraphAddEdit.DS_EXTRACT.equals(dataSource.getName()))
        {
            DynamicListDataSource ds = ((PageableSearchListDataSource) dataSource).getLegacyDataSource();

            List<IEntity> checkboxColumnSelected = new ArrayList<IEntity>();
            IEntity radioboxColumnSelected = null;

            for (IEntity record : (List<IEntity>) ds.getEntityList())
            {
                if (getSelectedExtractIds().contains(record.getId()))
                    checkboxColumnSelected.add(record);
            }

            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrCancelParagraphAddEdit.CHECKBOX_COLUMN));
            if (checkboxColumn != null)
                checkboxColumn.setSelectedObjects(checkboxColumnSelected);
        }
    }


    // Listeners

    public void onRefreshSelectionParams() {
        // ?
    }

    public void onClickApply()
    {
        try {

            DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource(EnrCancelParagraphAddEdit.DS_EXTRACT)).getLegacyDataSource();
            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrCancelParagraphAddEdit.CHECKBOX_COLUMN));

            List<EnrAbstractExtract> extracts = new ArrayList<EnrAbstractExtract>();
            if (checkboxColumn != null)
                for (IEntity entity : checkboxColumn.getSelectedObjects())
                    extracts.add((EnrAbstractExtract)((DataWrapper) entity).getWrapped());

            // сохраняем или обновляем параграф о зачислении
            EnrCancelParagraphManager.instance().dao().saveOrUpdateParagraph(getOrder(), getParagraph(), extracts);

            // закрываем форму
            deactivate();

        } catch (Throwable t) {
            Debug.exception(t);
            _uiSupport.setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // presenter

    public String getFormTitle() {
        String formTitle = (StringUtils.isNotEmpty(_order.getNumber()) ? "№" + _order.getNumber() + " " : "") + (_order.getCommitDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()) : "");
        if (isEditForm())
            formTitle = "Редактирование параграфа из приказа об отмене приказа " + formTitle;
        else
            formTitle = "Добавление параграфа приказа об отмене приказа " + formTitle;
        return formTitle;
    }

    public boolean isShowExcludeLowerPriorityCompetitions()
    {
        return getParagraph().getCancelledOrder() != null && EnrOrderTypeCodes.ENROLLMENT.equals(getParagraph().getCancelledOrder().getType().getCode());
    }


    // Getters & Setters

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public EnrCancelParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrCancelParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public Set<Long> getSelectedExtractIds()
    {
        return _selectedExtractIds;
    }

    public void setSelectedExtractIds(Set<Long> selectedExtractIds)
    {
        _selectedExtractIds = selectedExtractIds;
    }

    public EnrOrderType getOrderType()
    {
        return orderType;
    }

    public void setOrderType(EnrOrderType orderType)
    {
        this.orderType = orderType;
    }
}