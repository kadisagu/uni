package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEnrExamGroupDistributionDao extends INeedPersistenceSupport {


    /**
     * Распределяет ДДС абитуриента, невключенные в ЭГ по существующим ДДС. Записывает результат в базу (глобальная блокировка)
     * @param entrant абитуриент
     * @return { ДДС (все) -> ЭГ (распределение) }
     */
    Map<EnrExamPassDiscipline, EnrExamGroup> doDistributeEntrantExamPassDisciplines(EnrEntrant entrant);

}
