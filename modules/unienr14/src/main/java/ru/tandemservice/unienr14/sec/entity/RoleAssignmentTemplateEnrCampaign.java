package ru.tandemservice.unienr14.sec.entity;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import ru.tandemservice.unienr14.sec.entity.gen.*;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/** @see ru.tandemservice.unienr14.sec.entity.gen.RoleAssignmentTemplateEnrCampaignGen */
public class RoleAssignmentTemplateEnrCampaign extends RoleAssignmentTemplateEnrCampaignGen
{
    public RoleAssignmentTemplateEnrCampaign()
    {
    }

    public RoleAssignmentTemplateEnrCampaign(IPrincipalContext principalContext, RoleConfigTemplate role, EnrEnrollmentCampaign enrollmentCampaign)
    {
        setPrincipalContext(principalContext);
        setRoleConfig(role);
        setEnrollmentCampaign(enrollmentCampaign);
    }

    @Override
    public ILocalRoleContext getLocalRoleContext()
    {
        return getEnrollmentCampaign();
    }
}