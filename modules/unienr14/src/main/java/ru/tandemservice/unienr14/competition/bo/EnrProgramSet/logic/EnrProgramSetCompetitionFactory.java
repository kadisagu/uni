package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrCompetitionTypeGen;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrEduLevelRequirementGen;
import ru.tandemservice.unienr14.competition.entity.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes.*;

/**
 * @author vdanilov
 */
public abstract class EnrProgramSetCompetitionFactory<T extends EnrProgramSetBase> {

    // postfix before (<) 2016
    private static final String FIS_UID_POSTFIX_N = "n";
    private static final String FIS_UID_POSTFIX_S = "s";
    private static final String FIS_UID_POSTFIX_P = "p";
    private static final String FIS_UID_POSTFIX_V = "v";

    // postfix after (>=) 2016
    private static final String FIS_UID_POSTFIX_CMN = "cmn";
    private static final String FIS_UID_POSTFIX_Q   = "q";
    private static final String FIS_UID_POSTFIX_T   = "t";
    private static final String FIS_UID_POSTFIX_CTR = "ctr";

    /** { key(c) -> c = factory.buildCompetition(psOu) | factory.isAvailableFor(psOu) }*/
    public static Map<MultiKey, EnrCompetition> buildCompetitions(final EnrProgramSetOrgUnit psOu) {
        int eduYear = psOu.getProgramSet().getEnrollmentCampaign().getEducationYear().getIntValue();
        List<EnrProgramSetCompetitionFactory> factoryList = getFactoryList(eduYear);
        Map<MultiKey, EnrCompetition> result = new LinkedHashMap<>(factoryList.size());
        for (final EnrProgramSetCompetitionFactory factory: factoryList) {
            if (factory.isClassAndDivApply(psOu) && factory.isAvailableFor(psOu)) {
                EnrCompetition competition = factory.build(psOu);
                MultiKey key = competition.buildKey();
                if (null != result.put(key, competition)) {
                    throw new IllegalStateException();
                }
            }
        }
        return result;
    }

    // ||Вид заявления||Способ деления||Вид приема||На базе||Условия создания конкурса||Набор ВИ для конкурса||
    private static List<EnrProgramSetCompetitionFactory> getFactoryList(final int eduYear) {

        List<EnrProgramSetCompetitionFactory> _factories = new ArrayList<>();

        // |СПО|* без деления|общий конкурс|осн. общ|* На базе (baseLevel) ОП СПО из специальности (профессии) для приема — основное общее образование  * КЦП больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetSecondary>(eduYear, EnrProgramSetSecondary.class, SEC_NO_DIV,
            EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.OOO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) {
                String baseLevelCode = getProgramSet(psOu).getProgram().getBaseLevel().getCode();
                return EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE.equals(baseLevelCode) && psOu.isMinisterialOpen();
            }
        });
        // |СПО|* без деления|общий конкурс|СОО|* На базе (baseLevel) ОП СПО из специальности (профессии) для приема — среднее общее образование  * КЦП больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetSecondary>(eduYear, EnrProgramSetSecondary.class, SEC_NO_DIV,
            EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.SOO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) {
                String baseLevelCode = getProgramSet(psOu).getProgram().getBaseLevel().getCode();
                return EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE.equals(baseLevelCode) && psOu.isMinisterialOpen();
            }
        });
        // |СПО|* без деления|по договору|осн. общ|* На базе (baseLevel) ОП СПО из специальности (профессии) для приема — основное общее образование  * план приема на договор больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetSecondary>(eduYear, EnrProgramSetSecondary.class, SEC_NO_DIV,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.OOO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) {
                String baseLevelCode = getProgramSet(psOu).getProgram().getBaseLevel().getCode();
                return EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE.equals(baseLevelCode) && psOu.isContractOpen();
            }
        });
        // |СПО|* без деления|по договору|СОО|* На базе (baseLevel) ОП СПО из специальности (профессии) для приема — среднее общее образование  * план приема на договор больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetSecondary>(eduYear, EnrProgramSetSecondary.class, SEC_NO_DIV,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SOO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) {
                String baseLevelCode = getProgramSet(psOu).getProgram().getBaseLevel().getCode();
                return EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE.equals(baseLevelCode) && psOu.isContractOpen();
            }
        });
        // |бак и спец|* любой|без ВИ в рамках КЦП|СОО и СПО|* КЦП больше нуля|набор ВИ на базе СОО|
        for (String divMethod : BS_ALL)
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL, EnrEduLevelRequirementCodes.SOO_AND_SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isMinisterialOpen(); }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО * СОО, ПО * СОО и ВО, СПО|в рамках квоты лиц, имеющих особые права|СОО|* план приема по квоте особых прав больше нуля|набор ВИ на базе СОО|
        for (String divMethod : new String[] {BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_VO_APART_SPO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.EXCLUSIVE, EnrEduLevelRequirementCodes.SOO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getExclusivePlan() > 0; }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО * СОО, ПО * СОО и ВО, СПО|в рамках квоты лиц, имеющих особые права|СПО|* план приема по квоте особых прав больше нуля|набор ВИ на базе СПО|
        for (String divMethod : new String[] {BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_VO_APART_SPO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.EXCLUSIVE, EnrEduLevelRequirementCodes.SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getExclusivePlan() > 0; }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantSec();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_P); }
            });
        // |бак и спец|* без деления * СОО и СПО, ВО|в рамках квоты лиц, имеющих особые права|СОО и СПО|* план приема по квоте особых прав больше нуля|набор ВИ на базе СОО|
        for (String divMethod : new String[] {BS_NO_DIV, BS_SOO_AND_SPO_APART_VO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.EXCLUSIVE, EnrEduLevelRequirementCodes.SOO_AND_SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getExclusivePlan() > 0; }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО * СОО, ПО * СОО и ВО, СПО|целевой прием|СОО|* план приема по ЦП больше нуля|набор ВИ на базе СОО|
        for (String divMethod : new String[] {BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_VO_APART_SPO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.TARGET_ADMISSION, EnrEduLevelRequirementCodes.SOO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getTargetAdmPlan() > 0; }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО * СОО, ПО * СОО и ВО, СПО|целевой прием|СПО|* план приема по ЦП больше нуля|набор ВИ на базе СПО|
        for (String divMethod : new String[] {BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_VO_APART_SPO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.TARGET_ADMISSION, EnrEduLevelRequirementCodes.SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getTargetAdmPlan() > 0; }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantSec();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_P); }
            });
        // |бак и спец|* без деления * СОО и СПО, ВО|целевой прием|СОО и СПО|* план приема по ЦП больше нуля|набор ВИ на базе СОО|
        for (String divMethod : new String[] {BS_NO_DIV, BS_SOO_AND_SPO_APART_VO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.TARGET_ADMISSION, EnrEduLevelRequirementCodes.SOO_AND_SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getTargetAdmPlan() > 0; }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО * СОО, ПО * СОО и ВО, СПО|общий конкурс|СОО|* КЦП больше нуля|набор ВИ на базе СОО|
        for (String divMethod : new String[] {BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_VO_APART_SPO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.SOO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isMinisterialOpen(); }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО * СОО, ПО * СОО и ВО, СПО|общий конкурс|СПО|* КЦП больше нуля|набор ВИ на базе СПО|
        for (String divMethod : new String[] {BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_VO_APART_SPO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isMinisterialOpen(); }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantSec();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_P); }
            });
        // |бак и спец|* без деления * СОО и СПО, ВО|общий конкурс|СОО и СПО|* КЦП больше нуля|набор ВИ на базе СОО|
        for (String divMethod : new String[] {BS_NO_DIV, BS_SOO_AND_SPO_APART_VO})
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.SOO_AND_SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isMinisterialOpen(); }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* любой|без ВИ по договору|СОО и СПО|* план приема на договор больше нуля|набор ВИ на базе СОО|
        for (String divMethod : BS_ALL)
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, divMethod,
                EnrCompetitionTypeCodes.NO_EXAM_CONTRACT, EnrEduLevelRequirementCodes.SOO_AND_SPO)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
                @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                    return ps.getExamSetVariantBase();
                }
                @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
            });
        // |бак и спец|* СОО, СПО, ВО|по договору|СОО|* план приема на договор больше нуля|набор ВИ на базе СОО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_SPO_VO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SOO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantBase();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
        });
        // |бак и спец|* СОО, СПО, ВО|по договору|СПО|* план приема на договор больше нуля|набор ВИ на базе СПО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_SPO_VO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SPO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantSec();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_P); }
        });
        // |бак и спец|* СОО, СПО, ВО|по договору|ВО|* план приема на договор больше нуля|набор ВИ на базе ВО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_SPO_VO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.VO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantHigh();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_V); }
        });
        // |бак и спец|* СОО, ПО|по договору|СОО|* план приема на договор больше нуля|набор ВИ на базе СОО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_PO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SOO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantBase();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
        });
        // |бак и спец|* СОО, ПО|по договору|ПО|* план приема на договор больше нуля|набор ВИ на базе СПО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_PO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.PO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantSec();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_P); }
        });
        // |бак и спец|* без деления|по договору|СОО и ПО|* план приема на договор больше нуля|набор ВИ на базе СОО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_NO_DIV,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SOO_AND_PO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantSec();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
        });
        // |бак и спец|* СОО и СПО, ВО|по договору|СОО и СПО|* план приема на договор больше нуля|набор ВИ на базе СОО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_AND_SPO_APART_VO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SOO_AND_SPO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantBase();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
        });
        // |бак и спец|* СОО и СПО, ВО|по договору|ВО|* план приема на договор больше нуля|набор ВИ на базе ВО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_AND_SPO_APART_VO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.VO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantHigh();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_V); }
        });
        // |бак и спец|* СОО и ВО, СПО|по договору|СОО и ВО|* план приема на договор больше нуля|набор ВИ на базе СОО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_AND_VO_APART_SPO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SOO_AND_VO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantBase();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_S); }
        });
        // |бак и спец|* СОО и ВО, СПО|по договору|СПО|* план приема на договор больше нуля|набор ВИ на базе СПО|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetBS>(eduYear, EnrProgramSetBS.class, BS_SOO_AND_VO_APART_SPO,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.SPO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
            @Override protected EnrExamSetVariant chooseExamSetVariant(EnrProgramSetBS ps) {
                return ps.getExamSetVariantSec();
            }
            @Override protected String getFisUidPostfix(String defaultPostfix) { return super.getFisUidPostfix(FIS_UID_POSTFIX_P); }
        });
        // |магистратура|* без деления|целевой прием|ВО|* план приема по ЦП больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetMaster>(eduYear, EnrProgramSetMaster.class, MASTER_NO_DIV,
            EnrCompetitionTypeCodes.TARGET_ADMISSION, EnrEduLevelRequirementCodes.VO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getTargetAdmPlan() > 0; }
        });
        // |магистратура|* без деления|общий конкурс|ВО|* КЦП больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetMaster>(eduYear, EnrProgramSetMaster.class, MASTER_NO_DIV,
            EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.VO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isMinisterialOpen(); }
        });
        // |магистратура|* без деления|по договору|ВО|* план приема на договор больше нуля|набор ВИ|
        _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetMaster>(eduYear, EnrProgramSetMaster.class, MASTER_NO_DIV,
            EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.VO)
        {
            @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.isContractOpen(); }
        });

        for (String methodCode : EnrProgramSetHigher.AVAILABLE_DIV_METHOD_CODE)
        {
            // |высшая квалификация|* без деления|целевой прием|ВО (спец, маг)|* план приема по ЦП больше нуля|набор ВИ|
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetHigher>(eduYear, EnrProgramSetHigher.class, methodCode, EnrCompetitionTypeCodes.TARGET_ADMISSION, EnrEduLevelRequirementCodes.VO_SPEC_MAG)
            {
                @Override protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu) { return psOu.getTargetAdmPlan() > 0; }
            });
            // |высшая квалификация|* без деления|общий конкурс|ВО (спец, маг)|* КЦП больше нуля|набор ВИ|
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetHigher>(eduYear, EnrProgramSetHigher.class, methodCode, EnrCompetitionTypeCodes.MINISTERIAL, EnrEduLevelRequirementCodes.VO_SPEC_MAG)
            {
                @Override
                protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu)
                {
                    return psOu.isMinisterialOpen();
                }
            });
            // |высшая квалификация|* без деления|по договору|ВО (спец, маг)|* план приема на договор больше нуля|набор ВИ|
            _factories.add(new EnrProgramSetCompetitionFactory<EnrProgramSetHigher>(eduYear, EnrProgramSetHigher.class, methodCode, EnrCompetitionTypeCodes.CONTRACT, EnrEduLevelRequirementCodes.VO_SPEC_MAG)
            {
                @Override
                protected boolean isAvailableFor(EnrProgramSetOrgUnit psOu)
                {
                    return psOu.isContractOpen();
                }
            });
        }

        return _factories;
    }

    private static final String[] BS_ALL = new String[]{BS_NO_DIV, BS_SOO_PO, BS_SOO_AND_SPO_APART_VO, BS_SOO_AND_VO_APART_SPO, BS_SOO_SPO_VO};

    private final int eduYear;
    private final Class<T> psClass;
    private final String divMethod;
    private final String competitionTypeCode;
    private final String eduLevelRequirementCode;

    private EnrProgramSetCompetitionFactory(final int eduYear, final Class<T> psClass, final String divMethod, final String competitionTypeCode, final String eduLevelRequirementCode) {
        this.eduYear = eduYear;
        this.psClass = psClass;
        this.divMethod = divMethod;
        this.competitionTypeCode = competitionTypeCode;
        this.eduLevelRequirementCode = eduLevelRequirementCode;
    }

    private boolean isClassAndDivApply(EnrProgramSetOrgUnit psOu) {
        return psClass.isAssignableFrom(psOu.getProgramSet().getClass()) && divMethod.equals(psOu.getProgramSet().getMethodDivCompetitions().getCode());
    }

    /** @return true, если конкурс должен существовать */
    protected abstract boolean isAvailableFor(EnrProgramSetOrgUnit psOu);

    protected String getFisUidPostfix(String defaultPostfix)
    {
        if (eduYear >= 2016)
        {
            switch (competitionTypeCode)
            {
                case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL: return FIS_UID_POSTFIX_CMN;
                case EnrCompetitionTypeCodes.EXCLUSIVE: return FIS_UID_POSTFIX_Q;
                case EnrCompetitionTypeCodes.TARGET_ADMISSION: return FIS_UID_POSTFIX_T;
                case EnrCompetitionTypeCodes.MINISTERIAL: return FIS_UID_POSTFIX_CMN;
                case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT: return FIS_UID_POSTFIX_CTR;
                case EnrCompetitionTypeCodes.CONTRACT: return FIS_UID_POSTFIX_CTR;
                default: throw new IllegalStateException("Unknown competition type");
            }
        }
        else
        {
            if (null != defaultPostfix) return defaultPostfix;
            return FIS_UID_POSTFIX_N;
        }
    }
    protected EnrExamSetVariant chooseExamSetVariant(T ps) {
        throw new IllegalStateException();
    }

    @SuppressWarnings("unchecked")
    protected T getProgramSet(EnrProgramSetOrgUnit psOu) {
        return (T) psOu.getProgramSet();
    }

    protected EnrExamSetVariant getExamSetVariant(EnrProgramSetOrgUnit psOu) {
        T ps = getProgramSet(psOu);
        if (ps instanceof EnrProgramSetBase.ISingleExamSetOwner) {
            return ((EnrProgramSetBase.ISingleExamSetOwner)ps).getExamSetVariant();
        }
        return chooseExamSetVariant(ps);
    }

    protected EnrCompetition build(final EnrProgramSetOrgUnit psOu) {
        try {
            final ICommonDAO dao = DataAccessServices.dao();
            final EnrCompetition competition = new EnrCompetition();
            competition.setProgramSetOrgUnit(psOu);
            competition.setRequestType(psOu.getProgramSet().getRequestType());
            competition.setType(dao.<EnrCompetitionType>getByNaturalId(new EnrCompetitionTypeGen.NaturalId(this.competitionTypeCode)));
            competition.setEduLevelRequirement(dao.<EnrEduLevelRequirement>getByNaturalId(new EnrEduLevelRequirementGen.NaturalId(this.eduLevelRequirementCode)));
            competition.setExamSetVariant(getExamSetVariant(psOu));
            competition.setPlan(0);
            competition.setFisUidPostfix(getFisUidPostfix(null));
            return competition;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }


}
