package ru.tandemservice.unienr14.competition.entity;

import java.util.Comparator;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unienr14.competition.entity.gen.EnrExamVariantGen;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.EnrCampaignDisciplineManager;

/**
 * Настройка вступительного испытания
 */
public class EnrExamVariant extends EnrExamVariantGen implements ITitled
{
    public static enum Comparators implements Comparator<EnrExamVariant>
    {
        byElementNumber() {
            @Override public int compare(EnrExamVariant o1, EnrExamVariant o2) {
                return Integer.compare(o1.getExamSetElement().getNumber(), o2.getExamSetElement().getNumber());
            }
        },
        byPriority() {
            @Override public int compare(EnrExamVariant o1, EnrExamVariant o2) {
                return Integer.compare(o1.getPriority(), o2.getPriority());
            }
        }
        ;
    }

    public EnrExamVariant() {}

    public EnrExamVariant(EnrExamSetVariant examSetVariant, EnrExamSetElement examSetElement) {
        this();
        this.setExamSetVariant(examSetVariant);
        this.setExamSetElement(examSetElement);
        this.setPriority(examSetElement.getNumber());
    }

    public EnrExamVariant(EnrExamSetVariant examSetVariant, EnrExamSetElement examSetElement, Long passMarkAsLong) {
        this(examSetVariant, examSetElement);
        this.setPassMarkAsLong(null == passMarkAsLong ? 0L : passMarkAsLong.longValue());
    }

    @Override
    @EntityDSLSupport
    public String getPassMarkAsString() {
        return EnrCampaignDisciplineManager.DEFAULT_MARK_FORMATTER.format(getPassMarkAsLong());
    }

    public Double getPassMarkAsDouble() {
        return getPassMarkAsLong() / 1000.0;
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getExamSetElement() == null) {
            return this.getClass().getSimpleName();
        }
        return getExamSetElement().getTitle();
    }

    @Override
    @EntityDSLSupport
    public String getShortTitle() {
        return getExamSetElement().getShortTitle();
    }

}