/**
 *$Id: EnrEntrantOrgUnitList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List.EnrEntrantListUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.Arrays;

/**
 * @author Alexander Shaburov
 * @since 26.08.13
 */
@Configuration
public class EnrEntrantOrgUnitList extends BusinessComponentManager
{
    public static final String ENTRANT_DS = "entrantDS";
    public static final String ENTRANT_CUSTOM_STATE_DS = "entrantCustomStateDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS("entrantStateDS", entrantStateDSHandler()))
                .addDataSource(selectDS(ENTRANT_CUSTOM_STATE_DS, entrantCustomStateDSHandler()))
                .addDataSource(selectDS("archivalDS", archivalDSHandler()))
                .addDataSource(searchListDS(ENTRANT_DS, entrantDSColumns(), entrantDSHandler()))
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .create();
    }

    @Bean
    public ColumnListExtPoint entrantDSColumns()
    {
        return columnListExtPointBuilder(ENTRANT_DS)
                .addColumn(textColumn("personalNumber", EnrEntrant.personalNumber()).order())
                .addColumn(publisherColumn("requestNumber", "title")
                        .entityListProperty(EnrEntrantDSHandler.VIEW_PROP_REQUEST)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(publisherColumn("entrant", PersonRole.person().identityCard().fullFio()).required(Boolean.TRUE).parameters("mvel:['selectedTab':'requestTab']").order())
                .addColumn(textColumn("customState", EnrEntrantDSHandler.VIEW_PROP_CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
                .addColumn(textColumn("sex", PersonRole.person().identityCard().sex().shortTitle()).order())
                .addColumn(textColumn("passport", PersonRole.person().fullIdentityCardNumber()))
                .addColumn(textColumn("birthDate", PersonRole.person().identityCard().birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("state", EnrEntrant.state().stateDaemonSafe().s()))
                .addColumn(textColumn("registrationDate", EnrEntrant.registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("workPlace", EnrEntrant.person().workPlace()))
                .addColumn(toggleColumn("status", "status")
                        .toggleOffListener("onClickSwitchEntrantArchival").toggleOnListener("onClickSwitchEntrantArchival")
                        .toggleOffLabel("Абитуриент в архиве").toggleOnLabel("Абитуриент не в архиве")
                        .toggleOffListenerAlert(alert("Списать абитуриента «{0}» в архив?", EnrEntrant.P_FULL_FIO))
                        .toggleOnListenerAlert(alert("Восстановить абитуриента «{0}» из архива?", EnrEntrant.P_FULL_FIO))
                        .permissionKey("ui:secModel.orgUnit_archiveEnr14EntrantsTab"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEntrant").permissionKey("ui:secModel.orgUnit_deleteEnr14EntrantsTab").alert(alert("Удалить абитуриента «{0}»?", EnrEntrant.P_FULL_FIO)))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler entrantDSHandler()
    {
        return new EnrEntrantDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantState.class)
                .order(EnrEntrantState.title())
                .filter(EnrEntrantState.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantCustomStateType.class)
                .order(EnrEntrantCustomStateType.title())
                .filter(EnrEntrantCustomStateType.title());
    }

    @Bean IDefaultComboDataSourceHandler archivalDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        new IdentifiableWrapper(EnrEntrantListUI.SHOW_ARCHIVAL_CODE, "Показывать архивных"),
                        new IdentifiableWrapper(EnrEntrantListUI.SHOW_NON_ARCHIVAL_CODE, "Показывать не архивных")
                ))
                .filtered(true);
    }
}
