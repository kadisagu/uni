package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа о распределении по образовательным программам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrAllocationParagraphGen extends EnrAbstractParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph";
    public static final String ENTITY_NAME = "enrAllocationParagraph";
    public static final int VERSION_HASH = -1637856449;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_PROGRAM_SET_ORG_UNIT = "enrProgramSetOrgUnit";
    public static final String L_ENR_PROGRAM_SET_ITEM = "enrProgramSetItem";
    public static final String L_GROUP_MANAGER = "groupManager";

    private EnrProgramSetOrgUnit _enrProgramSetOrgUnit;     // Подразделение, ведущее прием по набору ОП
    private EnrProgramSetItem _enrProgramSetItem;     // Образовательная программа
    private EnrAllocationExtract _groupManager;     // Староста

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetOrgUnit getEnrProgramSetOrgUnit()
    {
        return _enrProgramSetOrgUnit;
    }

    /**
     * @param enrProgramSetOrgUnit Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     */
    public void setEnrProgramSetOrgUnit(EnrProgramSetOrgUnit enrProgramSetOrgUnit)
    {
        dirty(_enrProgramSetOrgUnit, enrProgramSetOrgUnit);
        _enrProgramSetOrgUnit = enrProgramSetOrgUnit;
    }

    /**
     * @return Образовательная программа. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetItem getEnrProgramSetItem()
    {
        return _enrProgramSetItem;
    }

    /**
     * @param enrProgramSetItem Образовательная программа. Свойство не может быть null.
     */
    public void setEnrProgramSetItem(EnrProgramSetItem enrProgramSetItem)
    {
        dirty(_enrProgramSetItem, enrProgramSetItem);
        _enrProgramSetItem = enrProgramSetItem;
    }

    /**
     * @return Староста.
     */
    public EnrAllocationExtract getGroupManager()
    {
        return _groupManager;
    }

    /**
     * @param groupManager Староста.
     */
    public void setGroupManager(EnrAllocationExtract groupManager)
    {
        dirty(_groupManager, groupManager);
        _groupManager = groupManager;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrAllocationParagraphGen)
        {
            setEnrProgramSetOrgUnit(((EnrAllocationParagraph)another).getEnrProgramSetOrgUnit());
            setEnrProgramSetItem(((EnrAllocationParagraph)another).getEnrProgramSetItem());
            setGroupManager(((EnrAllocationParagraph)another).getGroupManager());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrAllocationParagraphGen> extends EnrAbstractParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrAllocationParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EnrAllocationParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrProgramSetOrgUnit":
                    return obj.getEnrProgramSetOrgUnit();
                case "enrProgramSetItem":
                    return obj.getEnrProgramSetItem();
                case "groupManager":
                    return obj.getGroupManager();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrProgramSetOrgUnit":
                    obj.setEnrProgramSetOrgUnit((EnrProgramSetOrgUnit) value);
                    return;
                case "enrProgramSetItem":
                    obj.setEnrProgramSetItem((EnrProgramSetItem) value);
                    return;
                case "groupManager":
                    obj.setGroupManager((EnrAllocationExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrProgramSetOrgUnit":
                        return true;
                case "enrProgramSetItem":
                        return true;
                case "groupManager":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrProgramSetOrgUnit":
                    return true;
                case "enrProgramSetItem":
                    return true;
                case "groupManager":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrProgramSetOrgUnit":
                    return EnrProgramSetOrgUnit.class;
                case "enrProgramSetItem":
                    return EnrProgramSetItem.class;
                case "groupManager":
                    return EnrAllocationExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrAllocationParagraph> _dslPath = new Path<EnrAllocationParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrAllocationParagraph");
    }
            

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph#getEnrProgramSetOrgUnit()
     */
    public static EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> enrProgramSetOrgUnit()
    {
        return _dslPath.enrProgramSetOrgUnit();
    }

    /**
     * @return Образовательная программа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph#getEnrProgramSetItem()
     */
    public static EnrProgramSetItem.Path<EnrProgramSetItem> enrProgramSetItem()
    {
        return _dslPath.enrProgramSetItem();
    }

    /**
     * @return Староста.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph#getGroupManager()
     */
    public static EnrAllocationExtract.Path<EnrAllocationExtract> groupManager()
    {
        return _dslPath.groupManager();
    }

    public static class Path<E extends EnrAllocationParagraph> extends EnrAbstractParagraph.Path<E>
    {
        private EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> _enrProgramSetOrgUnit;
        private EnrProgramSetItem.Path<EnrProgramSetItem> _enrProgramSetItem;
        private EnrAllocationExtract.Path<EnrAllocationExtract> _groupManager;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph#getEnrProgramSetOrgUnit()
     */
        public EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> enrProgramSetOrgUnit()
        {
            if(_enrProgramSetOrgUnit == null )
                _enrProgramSetOrgUnit = new EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit>(L_ENR_PROGRAM_SET_ORG_UNIT, this);
            return _enrProgramSetOrgUnit;
        }

    /**
     * @return Образовательная программа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph#getEnrProgramSetItem()
     */
        public EnrProgramSetItem.Path<EnrProgramSetItem> enrProgramSetItem()
        {
            if(_enrProgramSetItem == null )
                _enrProgramSetItem = new EnrProgramSetItem.Path<EnrProgramSetItem>(L_ENR_PROGRAM_SET_ITEM, this);
            return _enrProgramSetItem;
        }

    /**
     * @return Староста.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph#getGroupManager()
     */
        public EnrAllocationExtract.Path<EnrAllocationExtract> groupManager()
        {
            if(_groupManager == null )
                _groupManager = new EnrAllocationExtract.Path<EnrAllocationExtract>(L_GROUP_MANAGER, this);
            return _groupManager;
        }

        public Class getEntityClass()
        {
            return EnrAllocationParagraph.class;
        }

        public String getEntityName()
        {
            return "enrAllocationParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
