/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEntrantCustomStateType.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;

/**
 * @author nvankov
 * @since 8/15/14
 */
@Configuration
public class EnrEntrantCustomStateTypeAddEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
                .create();
    }
}



    