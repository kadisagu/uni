/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 5/20/14
 */
public class EnrReportRequestCountAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date dateFrom;
    private Date dateTo;

    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;

    private boolean _replaceProgramSetBySingleProgramTitle;
    private boolean _doNotDisplaySingleProgramTitle;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "включать в отчет только выбранные параллельно условия поступления")));
    }

    // validate
    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public void onClickApply() {
        validate();
        Long reportId = EnrReportManager.instance().requestCountDao().createReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
            .parameter(PUBLISHER_ID, reportId)
            .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        setDateFrom(getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign().getDateTo());
    }

    // for report builder

    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }

    // utils

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
            .configDoubleWidthFilters(false)
            .configUseEnableCheckbox(true)
            .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
            .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, true, true, false, false, false))
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, new CommonFilterFormConfig(true, false, true, false, false, false))
            .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, new CommonFilterFormConfig(true, true, true, false, false, false))
            .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, new CommonFilterFormConfig(true, true, true, false, false, false))
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, new CommonFilterFormConfig(true, true, true, false, false, false))
            .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, new CommonFilterFormConfig(true, true, true, false, false, false))
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, new CommonFilterFormConfig(true, true, true, false, false, false));

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters


    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public IdentifiableWrapper getParallel()
    {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel)
    {
        this.parallel = parallel;
    }

    public boolean isParallelActive()
    {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        this.parallelActive = parallelActive;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        this.parallelList = parallelList;
    }

    public boolean isReplaceProgramSetBySingleProgramTitle(){ return _replaceProgramSetBySingleProgramTitle; }
    public void setReplaceProgramSetBySingleProgramTitle(boolean replaceProgramSetBySingleProgramTitle){ _replaceProgramSetBySingleProgramTitle = replaceProgramSetBySingleProgramTitle; }

    public boolean isDoNotDisplaySingleProgramTitle(){ return _doNotDisplaySingleProgramTitle; }
    public void setDoNotDisplaySingleProgramTitle(boolean doNotDisplaySingleProgramTitle){ _doNotDisplaySingleProgramTitle = doNotDisplaySingleProgramTitle; }
}