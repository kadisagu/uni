/* $Id:$ */
package ru.tandemservice.unienr14.student.bo.EnrStudent.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;

/**
 * @author oleyba
 * @since 8/14/14
 */
public class EnrStudentDao extends UniBaseDao implements IEnrStudentDao
{
    @Override
    public boolean isEntrantTabVisible(Long studentId)
    {
        if (null != DataAccessServices.dao().get(EnrAllocationExtract.class, EnrAllocationExtract.student().id(), studentId)) return true;
        if (null != DataAccessServices.dao().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.student().id(), studentId)) return true;
        return false;
    }
}