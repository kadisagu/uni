/* $Id: LocalRoleDSHandler.java 6508 2015-05-15 13:22:17Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.SecTab;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCampaign;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 07.11.2011
 */
public class LocalRoleDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String EC_ID = "orgUnitId";
    public static final String TITLE = "title";
    public static final String ROLE_STATE = "roleState";

    public LocalRoleDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long ecId = context.get(EC_ID);
        String title = context.get(TITLE);
        DataWrapper roleState = context.get(ROLE_STATE);

        DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(RoleConfigLocalEnrCampaign.class, "c")
            .joinPath(DQLJoinType.inner, RoleConfigLocalEnrCampaign.role().fromAlias("c"), "e")
            .column("e")
            .where(eq(property(RoleConfigLocalEnrCampaign.enrollmentCampaign().id().fromAlias("c")), value(ecId)))
            .order(property(RoleConfigLocalEnrCampaign.role().title().fromAlias("c")));

        CommonBaseFilterUtil.applySimpleLikeFilter(builder, "e", Role.title(), title);

        if (roleState != null)
            builder.where(eq(property(Role.active().fromAlias("e")), value(roleState.getId().equals(SecManager.ROLE_STATE_TURN_ON))));

        List<Role> list = builder.createStatement(context.getSession()).list();
        DSOutput output = ListOutputBuilder.get(input, list).pageable(false).build();
        output.setCountRecord(Math.max(5, list.size()));
        return output;
    }
}
