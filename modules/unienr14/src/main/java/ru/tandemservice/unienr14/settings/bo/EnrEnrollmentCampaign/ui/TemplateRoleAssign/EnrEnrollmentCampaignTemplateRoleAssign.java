/* $Id: SecTemplateRoleAssign.java 6520 2015-05-18 12:42:03Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.TemplateRoleAssign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.RoleAssign.SecRoleAssignUI;
import org.tandemframework.shared.organization.sec.entity.RoleConfig;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplateOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.codes.LocalRoleScopeCodes;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 09.11.2011
 */
@Configuration
public class EnrEnrollmentCampaignTemplateRoleAssign extends BusinessComponentManager
{
    public static final String LOCAL_ROLE_DS = "localRoleDS";
    public static final String DS_ENR_CAMPAIGN = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_ENR_CAMPAIGN, getName(), EnrEnrollmentCampaign.openSelectDSHandler(getName())))
            .addDataSource(selectDS(LOCAL_ROLE_DS, localRoleComboDSHandler()).addColumn(RoleConfigTemplateOrgUnit.role().title().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler localRoleComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), RoleConfigTemplate.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(notIn(property(RoleConfigTemplate.id().fromAlias(alias)), new DQLSelectBuilder()
                    .fromEntity(RoleAssignmentTemplateEnrCampaign.class, "r")
                    .column(property(RoleAssignmentTemplateEnrCampaign.roleConfig().id().fromAlias("r")))
                    .where(eq(property(RoleAssignmentTemplateEnrCampaign.principalContext().id().fromAlias("r")), value(context.<Long>get(SecRoleAssignUI.BIND_PRINCIPAL_CONTEXT_ID))))
                    .where(eq(property(RoleAssignmentTemplateEnrCampaign.enrollmentCampaign().fromAlias("r")), value(context.<IEntity>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                    .buildQuery()
                ));
            }
        }
            .where(RoleConfigTemplate.role().active(), Boolean.TRUE)
            .where(RoleConfigTemplate.localRoleScope().code(), (Object) LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN)
            .filter(RoleConfig.role().title())
            .order(RoleConfigTemplate.role().title());
    }
}
