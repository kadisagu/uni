package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrPersonEduDocumentRelGen;

/** @see ru.tandemservice.unienr14.entrant.entity.gen.EnrPersonEduDocumentRelGen */
public class EnrPersonEduDocumentRel extends EnrPersonEduDocumentRelGen
{
    public EnrPersonEduDocumentRel()
    {
    }

    public EnrPersonEduDocumentRel(EnrEntrant entrant, PersonEduDocument eduDocument)
    {
        setEntrant(entrant);
        setEduDocument(eduDocument);
    }

    @Override
    public PersonDocumentType getDocumentType()
    {
        return IUniBaseDao.instance.get().getCatalogItem(PersonDocumentType.class, PersonDocumentTypeCodes.EDU_INSTITUTION);
    }

    @Override
    public String getDisplayableTitle()
    {
        return getEduDocument().getDisplayableTitle();
    }

    @Override
    public String getFisUidTitle()
    {
        return getTitle() + ", " + getEntrant().getFullFio();
    }

    @Override
    public String getTitle()
    {
        if (getDocumentType() == null) {
            return this.getClass().getSimpleName();
        }
        return UniStringUtils.joinWithSeparator(": ", getDocumentType().getShortTitle(), UniStringUtils.joinWithSeparator(" ", getSeria(), getNumber()));
    }
}