package ru.tandemservice.unienr14.sec.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import org.tandemframework.shared.organization.sec.entity.RoleConfig;
import ru.tandemservice.unienr14.sec.entity.gen.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.LocalRoleAddEdit.EnrEnrollmentCommissionLocalRoleAddEdit;

/** @see ru.tandemservice.unienr14.sec.entity.gen.RoleConfigLocalEnrCommissionGen */
public class RoleConfigLocalEnrCommission extends RoleConfigLocalEnrCommissionGen
{
    @Override
    public ILocalRoleContext getRoleContext()
    {
        return getEnrollmentCommission();
    }

    @Override
    public String getTitle()
    {
        return "Локальная роль на ОК";
    }

    @Override
    public void doRoleTitleUniqueCheck()
    {
        if (new DQLSimple<>(RoleConfigLocalEnrCommission.class)
            .where(RoleConfigLocalEnrCommission.role().title(), getTitle())
            .where(RoleConfigLocalEnrCommission.enrollmentCommission(), getEnrollmentCommission())
            .whereNot(RoleConfig.role().id(), getRole().getId())
            .exists()) {
            throw new ApplicationException("Роль на ОК с таким названием уже существует.");
        }
    }

    @Override
    public String getFileExtension()
    {
        return "ec-pk";
    }

    @Override
    public Class<? extends BusinessComponentManager> getEditFormBc()
    {
        return EnrEnrollmentCommissionLocalRoleAddEdit.class;
    }
}