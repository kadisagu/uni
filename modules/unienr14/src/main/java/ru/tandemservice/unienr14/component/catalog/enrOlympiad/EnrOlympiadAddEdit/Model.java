/**
 *$Id: Model.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrOlympiad.EnrOlympiadAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;

/**
 * @author Alexander Shaburov
 * @since 14.05.13
 */
public class Model extends DefaultCatalogAddEditModel<EnrOlympiad>
{
    private ISingleSelectModel _eduYearModel;
    private ISingleSelectModel olympiadTypeModel;

    // Getters & Setters

    public ISingleSelectModel getEduYearModel()
    {
        return _eduYearModel;
    }

    public void setEduYearModel(ISingleSelectModel eduYearModel)
    {
        _eduYearModel = eduYearModel;
    }

    public ISingleSelectModel getOlympiadTypeModel()
    {
        return olympiadTypeModel;
    }

    public void setOlympiadTypeModel(ISingleSelectModel olympiadTypeModel)
    {
        this.olympiadTypeModel = olympiadTypeModel;
    }
}
