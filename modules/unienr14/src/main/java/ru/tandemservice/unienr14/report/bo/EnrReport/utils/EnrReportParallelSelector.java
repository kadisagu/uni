/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public class EnrReportParallelSelector {

    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;

    public EnrReportParallelSelector()
    {
        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "Не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "Включать в отчет только выбранные параллельно условия поступления")));
    }


    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }


    public boolean isParallelActive() {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive) {
        this.parallelActive = parallelActive;
    }

    public IdentifiableWrapper getParallel() {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel) {
        this.parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList() {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList) {
        this.parallelList = parallelList;
    }

    public static DQLSelectBuilder applyParallelFilter(DQLSelectBuilder dql, String alias, boolean parallelActive, boolean parallelOnly)
    {
        if (parallelActive)
        {
            if (parallelOnly)
                dql.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(alias)), value(Boolean.TRUE)));
            else
                dql.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(alias)), value(Boolean.FALSE)));
        }
        return dql;
    }
}
