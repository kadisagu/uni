/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author oleyba
 * @since 3/30/15
 */
@Configuration
public class EnrEnrollmentModelList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(searchListDS("modelDS", modelDSColumns(), modelDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint modelDSColumns()
    {
        return columnListExtPointBuilder("modelDS")
            .addColumn(textColumn("title", EnrEnrollmentModel.title()).order().clickable(true))
            .addColumn(textColumn("requestType", EnrEnrollmentModel.requestType().title()).order())
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete")
                .alert(new FormattedMessage("modelDS.delete.alert", EnrEnrollmentModel.title().s()))
                .permissionKey("enr14EnrollmentModelEverything"))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> modelDSHandler()
    {
        return new EnrEnrollmentModelDSHandler(getName());
    }
}