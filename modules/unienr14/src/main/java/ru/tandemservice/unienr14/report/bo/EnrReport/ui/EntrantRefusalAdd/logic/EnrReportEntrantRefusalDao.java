/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantRefusalReason;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.EnrReportEntrantRefusalAddUI;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantRefusal;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 28.05.2014
 */
public class EnrReportEntrantRefusalDao extends UniBaseDao implements IEnrReportEntrantRefusalDao
{

    private final static String RTF_LINE = "\\line";

    @Override
    public Long createReport(EnrReportEntrantRefusalAddUI model) {

        EnrReportEntrantRefusal report = new EnrReportEntrantRefusal();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        if (model.isOrgUnitRequired() && model.getOrgUnitList() != null) {
            report.setEnrOrgUnit(UniStringUtils.join(model.getOrgUnitList(), EnrOrgUnit.title().s(), "; "));
        }
        if (model.isReasonRequired() && model.getRefusalReasonList() != null) {
            report.setEnrRefusalReason(UniStringUtils.join(model.getRefusalReasonList(), EnrEntrantRefusalReason.title().s(), "; "));
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("EnrReportRefusal.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    public byte[] buildReport(EnrReportEntrantRefusalAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_REFUSAL);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Запрос данных из базы по входным параметрам
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantRefusal.class, "entrantRefusal")
                .where(eq(property("entrantRefusal", EnrEntrantRefusal.enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                .column(property("entrantRefusal"));
        if (model.isReasonRequired() && model.getRefusalReasonList() != null && model.getRefusalReasonList().size() > 0)
            builder.where(in(property("entrantRefusal", EnrEntrantRefusal.refusalReason()), model.getRefusalReasonList()));
        if (model.isOrgUnitRequired() && model.getOrgUnitList() != null && model.getOrgUnitList().size() > 0)
            builder.where(in(property("entrantRefusal", EnrEntrantRefusal.enrOrgUnit()), model.getOrgUnitList()));
        FilterUtils.applyBetweenFilter(builder, "entrantRefusal", EnrEntrantRefusal.registrationDate().s(), model.getDateFrom(), model.getDateTo());


        // формируем лист
        List<EnrEntrantRefusal> entrantRefusals = getList(builder);

        // листы таблицы с метками T и H соответсттвенно
        List<String[]> tTable = new ArrayList<>();
        List<String[]> hTable = new ArrayList<>();

        // Сортируем по именам абитуриентов
        Collections.sort(entrantRefusals, new Comparator<EnrEntrantRefusal>() {
            @Override
            public int compare(EnrEntrantRefusal o1, EnrEntrantRefusal o2) {
                return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFullName(), o2.getFullName());
            }
        });

        // Заполняем т-таблицу с абитуриентами
        int numb = 1;
        for (EnrEntrantRefusal entrantRefusal : entrantRefusals)
        {
            String[] row = new String[7];
            row[0] = String.valueOf(numb++);
            row[1] = entrantRefusal.getFullName();
            row[2] = DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRefusal.getRegistrationDate());
            row[3] = entrantRefusal.getRefusalReason().getTitle();
            row[4] = entrantRefusal.getEnrOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitleWithTopEmphasized();
            row[5] = entrantRefusal.getCompetitionData();
            row[6] = entrantRefusal.getComment();
            tTable.add(row);
        }

        // Заполняем Н-таблицу с параметрами выбора абитуриентов, которым отказали в приеме

        hTable.add(new String[]{"Приемная Кампания", model.getEnrollmentCampaign().getTitle()});
        hTable.add(new String[]{"Отказы добавлены с", new SimpleDateFormat("dd.MM.yyyy").format(model.getDateFrom())});
        hTable.add(new String[]{"Отказы добавлены по", new SimpleDateFormat("dd.MM.yyyy").format(model.getDateTo())});
        if (model.isOrgUnitRequired() && !CollectionUtils.isEmpty(model.getOrgUnitList())) {
            hTable.add(new String[] {"Филиал", UniStringUtils.join(model.getOrgUnitList(), EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s(), RTF_LINE)});
        }
        if (model.isReasonRequired() && !CollectionUtils.isEmpty(model.getRefusalReasonList())) {
            hTable.add(new String[] {"Причина отказа", UniStringUtils.join(model.getRefusalReasonList(), EnrEntrantRefusalReason.title().s(), RTF_LINE)});
        }

        // Записываем таблицы в rtf-файл
        RtfTableModifier tableModifier = new RtfTableModifier();

        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("T", tTable.toArray(new String[tTable.size()][]));
        tableModifier.put("H", new RtfRowIntercepterRawText());

        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }
}
