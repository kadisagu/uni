package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrEnrollmentCampaignTypeGen;

/** @see ru.tandemservice.unienr14.catalog.entity.gen.EnrEnrollmentCampaignTypeGen */
public class EnrEnrollmentCampaignType extends EnrEnrollmentCampaignTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrEnrollmentCampaignType.class)
                .titleProperty(EnrEnrollmentCampaignType.title().s())
                .filter(EnrEnrollmentCampaignType.title())
                .order(EnrEnrollmentCampaignType.priority());
    }
}