/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.IdentityCardStep;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.hibernate.NonUniqueObjectException;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.PersonIdentityCardEditInline;
import org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.IPersonSimilarAddValidator;
import org.tandemframework.shared.person.base.bo.Person.util.PersonRoleAddBase.PersonRoleAddBaseUI;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.SportType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.EnrEntrantRequestAddWizardManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.IdentityCardOption;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author nvankov
 * @since 5/30/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardIdentityCardStepUI extends PersonRoleAddBaseUI implements IWizardStep
{
    public static final String REGION_ADDRESS = "addressRegion";
    public static final String IDENTITY_CARD_REGION = "identityCardRegion";

    private EnrEntrantWizardState _state;
    private EnrEntrant _entrant;
    private DataWrapper _option;
    private EnrOnlineEntrant _onlineEntrant;
    private List<IdentityCardOption> _identityCardOptionsList = Lists.newArrayList();
    private IdentityCardOption _identityCardOption;
    private IdentityCard _selectedIdentityCard;
    private IdentityCard _lastCreatedCard;
    private boolean _sameFactAddress;
    private EnrEntrant _existEntrant;
    private IUploadFile _uploadFile;

    private ISelectModel _nationalityModel;

    @Override
    public void onComponentActivate()
    {
        super.onComponentActivate();
        setNationalityModel(new LazySimpleSelectModel<>(Nationality.class).setRecordLimit(50));
        setEntrant(new EnrEntrant());
        _option = EnrEntrantManager.instance().addEntrantWizardFirstStepOptionsExtPoint().getItem(EnrEntrantManager.OPTION_ENTRANT.toString());

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setFourFieldsWidth(true);
        addressConfig.setInline(true);
        addressConfig.setAreaVisible(true);
        addressConfig.setRefreshInParent(true);
        addressConfig.setFieldSetTitle("Адрес регистрации");
        addressConfig.setShowCurrentAddressString(false);
        _uiActivation.asRegion(AddressBaseEditInline.class, REGION_ADDRESS).
        parameter(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig).
        activate();
        _uiActivation.asRegion(PersonIdentityCardEditInline.class, IDENTITY_CARD_REGION).
                parameter(PersonIdentityCardEditInline.PERSON, getPerson()).
                activate();
    }

    @Override
    public void onComponentRefresh()
    {
        // заполнение полей
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EnrEntrantRequestAddWizardIdentityCardStep.ONLINE_ENTRANT_DS.equals(dataSource.getName()))
        {
            if(getEnrEnrollmentCampaign() != null)
                dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrEnrollmentCampaign().getId());
        }
    }

    // actions

    @Override
    public void onClickApplyFinalStep()
    {
    }

    public void onClickNext() {
        getEnrEnrollmentCampaign().checkOpen();
        saveStepData();
    }

    @Override
    public void saveStepData()
    {
        if (isShowSimilarPersons()) {
            onClickApplySimilarPersonStep();
            prepareFinalStep();
        } else if (isShowAfterCreateForm())
        {
            applyFinalStep();
            getState().setPersonId(getEntrant().getPerson().getId());
            getState().setEntrantId(getEntrant().getId());
            getState().setIdentityCardId(getSelectedIdentityCard().getId());

            getState().activateNextStep(EnrEntrantRequestAddWizardWizardUI.IDENTITY_CARD_STEP_CODE);

            EnrEntrantRequestAddWizardWizardUI wizardUI = EnrEntrantRequestAddWizardWizardUI.findWizard(_uiConfig.getBusinessComponent());
            assert wizardUI != null;
            EnrEntrantWizardStep nextStep = wizardUI.getNextStep();
            wizardUI.activateStep(nextStep.getStepName());
        } else {
            onClickApplySearchFormStep();
            getState().setEnrollmentCampaignId(getEnrEnrollmentCampaign().getId());
            if(getOnlineEntrant() != null)
                getState().setOnlineEntrantId(getOnlineEntrant().getId());
            if (isShowSimilarPersons()) {
                prepareSimilarPersonStep();
            }
            else if (isShowAfterCreateForm()) {
                prepareFinalStep();
            }
        }
    }

    public void onClickPrevious()
    {
        if (isShowSimilarPersons()) {
            onClickCancelSimilarPersonStep();
        } else if (isShowAfterCreateForm()) {
            onClickCancelFinalStep();
        }
    }

    public void onClickCancel()
    {
        EnrEntrantRequestAddWizardWizardUI wizardUI = EnrEntrantRequestAddWizardWizardUI.findWizard(_uiConfig.getBusinessComponent());
        assert wizardUI != null;
        wizardUI.deactivate();
    }

    public void onChangeOption()
    {
        _onlineEntrant = null;
        getPerson().setIdentityCard(new IdentityCard());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEnrollmentCampaign());
        getEntrant().setEnrollmentCampaign(getEnrEnrollmentCampaign());

    }

    public void onSelectOnlineEntrant()
    {
        getPerson().setIdentityCard(new IdentityCard());
        if(_onlineEntrant != null)
            getIdentityCard().update(_onlineEntrant.getIdentityCard());
    }

    public void onSelectIdentityCard()
    {
        _sameFactAddress = false;
        if(_identityCardOption != null)
        {
            if(_identityCardOption.getId() == 1L)
            {
                _selectedIdentityCard = _lastCreatedCard;
                if (_identityCardOption.isOnlineEntrant()) {
                    _selectedIdentityCard.update(_identityCardOption.getIdentityCard());

                    AddressString addressString = new AddressString();
                    addressString.setAddress(_onlineEntrant.getRegAddress());
                    _selectedIdentityCard.setAddress(addressString);
                }
                onChangeSelectedIdentityCardType();
            }
            else
            {
                if(_identityCardOption.isOnlineEntrant())
                {
                    _selectedIdentityCard = new IdentityCard();
                    _selectedIdentityCard.update(_identityCardOption.getIdentityCard());
                    _selectedIdentityCard.setFirstName(getIdentityCard().getFirstName());
                    _selectedIdentityCard.setLastName(getIdentityCard().getLastName());
                    _selectedIdentityCard.setMiddleName(getIdentityCard().getMiddleName());
                    _selectedIdentityCard.setCardType(getIdentityCard().getCardType());
                    _selectedIdentityCard.setSeria(getIdentityCard().getSeria());
                    _selectedIdentityCard.setCitizenship(getIdentityCard().getCitizenship());
                    _selectedIdentityCard.setNumber(getIdentityCard().getNumber());
                    AddressString addressString = new AddressString();
                    addressString.setAddress(_onlineEntrant.getRegAddress());
                    _selectedIdentityCard.setAddress(addressString);
                    if(getSelectedIdentityCard().getCitizenship() == null)
                        onChangeSelectedIdentityCardType();
                    _sameFactAddress = _onlineEntrant.isSameAddress();
                }
                else
                {
                    _selectedIdentityCard = _identityCardOption.getIdentityCard();
                }
            }
            AddressBaseEditInlineUI region = _uiSupport.getChildUI(REGION_ADDRESS);
            if(_selectedIdentityCard.getAddress() != null)
                region.getAddressEditUIConfig().setAddressBase(AddressBaseUtils.getSameAddress(_selectedIdentityCard.getAddress()));
            else
            {
                AddressRu addressRu = new AddressRu();
                addressRu.setCountry(DataAccessServices.dao().get(AddressCountry.class, AddressCountry.code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
                region.getAddressEditUIConfig().setAddressBase(addressRu);
            }
            region.refreshData();

            getPerson().setIdentityCard(_selectedIdentityCard);
            _uiConfig.getBusinessComponent().getChildRegion(IDENTITY_CARD_REGION).getActiveComponent().refresh();
        }
    }

    @Override
    public void onSelectPersonFromSimilarPersonDS()
    {
        List<DataWrapper> similarPersons = getSimilarPersonDS().getSelectedEntities();
        if(!similarPersons.isEmpty())
        {
            List roles = (List) similarPersons.get(0).getProperty(VIEW_PROP_ROLES);
            if(roles != null && !roles.isEmpty())
            {
                for (Object role : roles)
                {
                    if (role instanceof EnrEntrant && ((EnrEntrant) role).getEnrollmentCampaign().equals(getEnrEnrollmentCampaign()))
                    {
                        _existEntrant = (EnrEntrant) role;
                    }
                }
            }
        }
        else
            _existEntrant = null;
    }



    public void onChangeSelectedIdentityCardType()
    {
        _selectedIdentityCard.setCitizenship(_selectedIdentityCard.getCardType().getCitizenshipDefault());
    }

    public void onChangeSimilarStepOption()
    {
        _existEntrant = null;
    }

    // presenter
    public boolean isShowScanCopy()
    {
        return getSelectedIdentityCard() != null && getSelectedIdentityCard().getId() == null;
    }

    public boolean isNextButtonDisabled() {
        return ! (isShowSimilarPersons() || isShowAfterCreateForm() || isApplySearchFormStepButtonVisible());
    }

    public boolean isPreviousButtonVisible() {
        return isShowSimilarPersons() || isShowAfterCreateForm();
    }

    public String getCancelConfirmationMessage() {
        if (isPreviousButtonVisible()) return getConfig().getProperty("ui.cancelConfirmation");
        return getConfig().getProperty("ui.cancelConfirmation.firstForm");
    }

    public String getExistEntrantInfo() { return _existEntrant.getFullFio(); }

    @Override public String getNewPersonButtonName() { return "Добавить абитуриента, создав новую персону"; }

    @Override public String getOnBasisPersonButtonName() { return "Добавить абитуриента на основе существующей персоны"; }

    @Override public String getSearchFormStepPageTitle() { return "Поиск похожих персон и абитуриентов"; }

    @Override public String getSimilarPersonStepPageTitle() { return "Поиск подобных персон по введенным данным: " + getIdentityCard().getFullFio() + " (" + getIdentityCard().getTitle() + ")"; }

    @Override public String getAfterCreateFormStepPageTitle() { return null; } // вписано в template

    @Override public String getSimilarPersonStepInfoString() { return "<p style=\"margin: 2px 0 5px 10px;padding:0;\">В процессе добавления абитуриента найдены персоны с похожими личными данными.</p><p style=\"margin: 0 0 5px 10px;padding:0;\">Вы можете добавить абитуриента, создав новую персону, либо выбрав за основу одну из существующих персон.</p><p style=\"margin: 0 0 2px 10px;padding:0;\">Выбирайте существующую персону только тогда, когда вы полностью уверены в том, что абитуриент, для которого добавляется заявление, и выбранная персона – одно и тоже лицо.</p>"; }

    @Override public String getSearchFormPageName() { return getClass().getPackage().getName() + ".EnrEntrantAddWizardSearchForm"; }

    @Override public String getAfterCreateFormPageName() { return getClass().getPackage().getName() + ".EnrEntrantAddWizardIdentityCard"; }

    @Override public PersonRole getPersonRole() { return getEntrant(); }

    @Override protected IPersonSimilarAddValidator getBasisPersonValidator() { return null; }

    @Override public boolean isApplySearchFormStepButtonVisible() { return isEnrollmentCampaignSelected() && (!isOnlineEntrantOptionSelected() || _onlineEntrant != null); }

    public boolean isShowOnlineEntrantDataInfo()
    {
        return getOnlineEntrant() != null;
    }

    public boolean isShowOnlineEntrantScanCopyInfo()
    {
        return getOnlineEntrant() != null && getOnlineEntrant().getIdCardScanCopy() != null;
    }

    public String getOnlineEntrantData()
    {
        List<String> infoStrings = Lists.newArrayList();
        if(!StringUtils.isEmpty(getOnlineEntrant().getRegAddress()))
        {
            infoStrings.add("<div>Адрес регистрации, указанный в онлайн-регистрации: " + getOnlineEntrant().getRegAddress() + "</div>");
        }

        List<String> phones = Lists.newArrayList();

        if(!StringUtils.isEmpty(getOnlineEntrant().getContactPhone()))
        {
            phones.add("Контактный телефон: " +getOnlineEntrant().getContactPhone());
        }

        if(!StringUtils.isEmpty(getOnlineEntrant().getMobilePhone()))
        {
            phones.add("Мобильный телефон: " +getOnlineEntrant().getMobilePhone());
        }

        if(!StringUtils.isEmpty(getOnlineEntrant().getWorkPhone()))
        {
            phones.add("Рабочий телефон: " +getOnlineEntrant().getWorkPhone());
        }
        if(!phones.isEmpty() || !StringUtils.isEmpty(getOnlineEntrant().getFactAddress()))
        {
            if(!infoStrings.isEmpty())
            {
                infoStrings.add("<div>&nbsp;</div>");
            }

            infoStrings.add("<div>Для справки. Контактная информация, указанная в онлайн-регистрации.</div>");
        }

        if(!phones.isEmpty())
        {
            infoStrings.add("<div>" + StringUtils.join(phones, " ") + "</div>");
        }

        if(!StringUtils.isEmpty(getOnlineEntrant().getFactAddress()))
        {
            infoStrings.add("<div>Фактический адрес, указанный в онлайн-регистрации: " + getOnlineEntrant().getFactAddress() + "</div>");
        }

        return StringUtils.join(infoStrings, "");
    }

    public boolean isEnrollmentCampaignSelected() { return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) != null; }

    public boolean isShowForm() { return isApplySearchFormStepButtonVisible(); }

    public boolean isOnlineEntrantOptionSelected() { return EnrEntrantManager.OPTION_ONLINE_ENTRANT.equals(_option.getId()); }

    public boolean isShowOnlineEntrantSelect() { return isOnlineEntrantOptionSelected() && isEnrollmentCampaignSelected(); }

    public boolean isIdentityCardOptionGroupVisible() { return _identityCardOptionsList.size() > 1; }

    public boolean isSelectedIdCardTypeSelectDisabled() { return _selectedIdentityCard != null && _selectedIdentityCard.getId() != null; }

    public boolean isShowLinkToExistEntrant() { return _existEntrant != null; }

    @Override public boolean isShowSticker() { return true; }

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign() { return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN); }

    // utils

    protected void applyFinalStep()
    {
        AddressBaseEditInlineUI region = _uiSupport.getChildUI(REGION_ADDRESS);
        final AddressBase regAddress = region.getResult();
        final AddressBase factAddress = _sameFactAddress ? AddressBaseUtils.getSameAddress(regAddress) : null;

        int amount = -13;
        if (_selectedIdentityCard.getBirthDate() != null && _selectedIdentityCard.getBirthDate().after(CoreDateUtils.add(CoreDateUtils.getDayFirstTimeMoment(new Date()), Calendar.YEAR, amount)))
            _uiSupport.error("Абитуриент должен быть старше 13 лет.", "birthDate");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();

        if(getSelectedBasisPerson() == null)
            setEntrant(DataAccessServices.dao().doInTransaction(session -> {
                _state.setEntrantNew(true);
                getEntrant().setEnrollmentCampaign(getEnrEnrollmentCampaign());
                getPerson().setIdentityCard(getSelectedIdentityCard());
                if (_onlineEntrant != null)
                {
                    getPerson().setNeedDormitory(_onlineEntrant.isNeedDorm());
                    getPerson().setWorkPlace(_onlineEntrant.getCurrentWork());
                    getPerson().setWorkPlacePosition(_onlineEntrant.getCurrentPost());
                    PersonContactData contactData = new PersonContactData();
                    contactData.setPhoneDefault(_onlineEntrant.getContactPhone());
                    contactData.setPhoneMobile(_onlineEntrant.getMobilePhone());
                    contactData.setPhoneWork(_onlineEntrant.getWorkPhone());
                    contactData.setEmail(_onlineEntrant.getEmail());
                    getPerson().setContactData(contactData);
                    getEntrant().setRegistrationDate(_onlineEntrant.getRegDate());
                    if (_onlineEntrant.getArmyStartDate() != null || _onlineEntrant.getArmyEndDate() != null || _onlineEntrant.getArmyEndYear() != null)
                    {
                        getEntrant().setPassArmy(true);
                        getEntrant().setBeginArmy(_onlineEntrant.getArmyStartDate());
                        getEntrant().setEndArmy(_onlineEntrant.getArmyEndDate());
                        getEntrant().setEndArmyYear(_onlineEntrant.getArmyEndYear());
                        getEntrant().setRegistrationDate(_onlineEntrant.getRegDate());
                    }
                }
                else
                {
                    getEntrant().setRegistrationDate(new Date());
                }

                if (factAddress != null)
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(getPerson(), factAddress, Person.address().s(), null);
                else if (_onlineEntrant != null && !StringUtils.isEmpty(_onlineEntrant.getFactAddress()))
                {
                    AddressString factAddressStr = new AddressString();
                    factAddressStr.setAddress(_onlineEntrant.getFactAddress());
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(getPerson(), factAddressStr, Person.address().s(), null);
                }
                AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(getPerson().getIdentityCard(), regAddress, IdentityCard.address().s(), null);

                getEntrant().setPerson(getPerson());

                ErrorCollector errors = _uiConfig.getUserContext().getErrorCollector();

                PersonManager.instance().dao().checkIdentityCard(getPerson().getIdentityCard(), errors);

                if (getUserContext().getErrorCollector().hasErrors())
                    throw new ApplicationException();

                boolean newIdentityCard = getPerson().getIdentityCard().getId() == null;
                session.save(getPerson().getIdentityCard());

                if(newIdentityCard && getUploadFile() != null)
                {
                    try
                    {
                        PersonManager.instance().dao().addEditDocumentScan(true, null, _uploadFile, getPerson().getIdentityCard().getId(), "Удостоверение личности");
                    } catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
                else if (getOnlineEntrant() != null && getOnlineEntrant().getIdCardScanCopy() != null)
                {
                    saveOnlineEntrantIdCardScanCopy(getPerson().getIdentityCard());
                }

                session.save(getPerson().getContactData());
                session.save(getPerson());
                getPerson().getIdentityCard().setPerson(getPerson());
                session.save(getPerson().getIdentityCard());

                getEntrant().setPersonalNumber(EnrEntrantManager.instance().dao().getUniqueEntrantNumber(getEntrant()));
                getEntrant().setPrincipal((Principal) PersonManager.instance().dao().createPrincipalContext(getEntrant()).getPrincipal());

                PersonManager.instance().dao().savePersonRole(getPersonRole(), getPersonRole().getPerson());

                if (_onlineEntrant != null)
                {
                    _onlineEntrant.setEntrant(getEntrant());
                    session.update(_onlineEntrant);

                    if (_onlineEntrant.getForeignLanguage() != null)
                    {
                        PersonForeignLanguage language = new PersonForeignLanguage();
                        language.setPerson(getPerson());
                        language.setLanguage(_onlineEntrant.getForeignLanguage());
                        session.save(language);
                    }

                    List<EnrOnlineEntrantAccessCourse> courses = DataAccessServices.dao().getList(EnrOnlineEntrantAccessCourse.class, EnrOnlineEntrantAccessCourse.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantAccessCourse course : courses)
                    {
                        EnrEntrantAccessCourse entrantCourse = new EnrEntrantAccessCourse();
                        entrantCourse.setEntrant(getEntrant());
                        entrantCourse.setCourse(course.getCourse());
                        session.save(entrantCourse);
                    }

                    List<EnrOnlineEntrantAccessDepartment> departments = DataAccessServices.dao().getList(EnrOnlineEntrantAccessDepartment.class, EnrOnlineEntrantAccessDepartment.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantAccessDepartment department : departments)
                    {
                        EnrEntrantAccessDepartment entrantDepartment = new EnrEntrantAccessDepartment();
                        entrantDepartment.setEntrant(getEntrant());
                        entrantDepartment.setAccessDepartment(department.getDepartment());
                        session.save(entrantDepartment);
                    }

                    List<EnrOnlineEntrantSportAchievement> sportAchievements = DataAccessServices.dao().getList(EnrOnlineEntrantSportAchievement.class, EnrOnlineEntrantSportAchievement.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantSportAchievement sportAchievement : sportAchievements)
                    {
                        PersonSportAchievement personSportAchievement = new PersonSportAchievement();
                        personSportAchievement.setPerson(getPerson());
                        personSportAchievement.setSportKind(sportAchievement.getType());
                        personSportAchievement.setSportRank(sportAchievement.getRank());
                        session.save(personSportAchievement);
                    }

                    List<EnrOnlineEntrantSourceInfoAboutOU> sourceInfoAboutOUs = DataAccessServices.dao().getList(EnrOnlineEntrantSourceInfoAboutOU.class, EnrOnlineEntrantSourceInfoAboutOU.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantSourceInfoAboutOU sourceInfoAboutOU : sourceInfoAboutOUs)
                    {
                        EnrEntrantInfoAboutUniversity infoAboutUniversity = new EnrEntrantInfoAboutUniversity();
                        infoAboutUniversity.setEntrant(getEntrant());
                        infoAboutUniversity.setSourceInfo(sourceInfoAboutOU.getSourceInfo());
                        session.save(infoAboutUniversity);
                    }
                }
                return getEntrant();
            }));
        else
        {
            setEntrant(DataAccessServices.dao().doInTransaction(session -> {
                if (_onlineEntrant != null && _onlineEntrant.getEntrant() != null)
                {
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(_selectedIdentityCard, regAddress, IdentityCard.address().s(), null);
                    session.saveOrUpdate(_selectedIdentityCard);

                    if (_sameFactAddress)
                        AddressBaseManager.instance().dao().saveOrUpdateAddressWithEntityUpdate(getSelectedBasisPerson(), factAddress, Person.address().s(), null);

                    if(getUploadFile() != null)
                    {
                        try
                        {
                            PersonManager.instance().dao().addEditDocumentScan(true, null, _uploadFile, _selectedIdentityCard.getId(), "Удостоверение личности");
                        } catch (IOException e)
                        {
                            throw new RuntimeException(e);
                        }
                    }
                    else if(getOnlineEntrant().getIdCardScanCopy() != null)
                    {
                        saveOnlineEntrantIdCardScanCopy(_selectedIdentityCard);
                    }

                    return _onlineEntrant.getEntrant();
                }

                Person person = getSelectedBasisPerson();
                EnrEntrant enrEntrant = new EnrEntrant();
                enrEntrant.setEnrollmentCampaign(getEnrEnrollmentCampaign());

                List<PersonRole> roles = person.getRoles();
                for (PersonRole role : roles)
                {
                    if (role instanceof EnrEntrant && ((EnrEntrant) role).getEnrollmentCampaign().equals(getEnrEnrollmentCampaign()))
                    {
                        enrEntrant = (EnrEntrant) role;
                        break;
                    }
                }

                if (enrEntrant.getId() == null)
                {
                    enrEntrant.setPerson(person);
                    if(_onlineEntrant != null)
                        enrEntrant.setRegistrationDate(_onlineEntrant.getRegDate());
                    else
                        enrEntrant.setRegistrationDate(new Date());
                    enrEntrant.setPersonalNumber(EnrEntrantManager.instance().dao().getUniqueEntrantNumber(enrEntrant));
                    enrEntrant.setPrincipal((Principal) PersonManager.instance().dao().createPrincipalContext(enrEntrant).getPrincipal());
                    _state.setEntrantNew(true);
                }

                boolean newIdentityCard = _selectedIdentityCard.getId() == null;

                AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(_selectedIdentityCard, regAddress, IdentityCard.address().s(), null);
                try {
                    session.saveOrUpdate(_selectedIdentityCard);
                } catch (NonUniqueObjectException ex) {
                    _selectedIdentityCard = (IdentityCard) session.merge(_selectedIdentityCard);
                    session.saveOrUpdate(_selectedIdentityCard);
                }

                if(newIdentityCard && getUploadFile() != null)
                {
                    try
                    {
                        IdentityCardToDocumentScanCopy scanCopy = new IdentityCardToDocumentScanCopy();
                        scanCopy.setIdentityCard(_selectedIdentityCard);
                        scanCopy.setIsActiveScanCopy(true);
                        scanCopy.setScanCopyDescription("Удостоверение личности");

                        PersonManager.instance().dao().addEditDocumentScan(true, scanCopy, _uploadFile, _selectedIdentityCard.getId(), "Удостоверение личности");
                    } catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
                else if(getOnlineEntrant() != null && getOnlineEntrant().getIdCardScanCopy() != null)
                {
                   saveOnlineEntrantIdCardScanCopy(_selectedIdentityCard);
                }

                if (_sameFactAddress)
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(getPerson(), factAddress, Person.address().s(), null);


                if (_onlineEntrant != null)
                {
                    person.setNeedDormitory(_onlineEntrant.isNeedDorm());
                    if (StringUtils.isEmpty(person.getWorkPlace()))
                        person.setWorkPlace(_onlineEntrant.getCurrentWork());
                    if (StringUtils.isEmpty(person.getWorkPlacePosition()))
                        person.setWorkPlacePosition(_onlineEntrant.getCurrentPost());

                    PersonContactData contactData = person.getContactData();

                    if (contactData == null)
                        contactData = new PersonContactData();
                    if (StringUtils.isEmpty(contactData.getPhoneDefault()))
                        contactData.setPhoneDefault(_onlineEntrant.getContactPhone());
                    if (StringUtils.isEmpty(contactData.getPhoneMobile()))
                        contactData.setPhoneMobile(_onlineEntrant.getMobilePhone());
                    if (StringUtils.isEmpty(contactData.getPhoneWork()))
                        contactData.setPhoneWork(_onlineEntrant.getWorkPhone());
                    if (StringUtils.isEmpty(contactData.getEmail()))
                        contactData.setEmail(_onlineEntrant.getEmail());

                    session.saveOrUpdate(contactData);

                    person.setContactData(contactData);

                    if (!enrEntrant.isPassArmy() && (_onlineEntrant.getArmyStartDate() != null || _onlineEntrant.getArmyEndDate() != null || _onlineEntrant.getArmyEndYear() != null))
                    {
                        enrEntrant.setPassArmy(true);
                        enrEntrant.setBeginArmy(_onlineEntrant.getArmyStartDate());
                        enrEntrant.setEndArmy(_onlineEntrant.getArmyEndDate());
                        enrEntrant.setEndArmyYear(_onlineEntrant.getArmyEndYear());
                        enrEntrant.setRegistrationDate(_onlineEntrant.getRegDate());
                    }

                    session.saveOrUpdate(enrEntrant);
                    session.saveOrUpdate(person);
                    setEntrant(enrEntrant);

                    _onlineEntrant.setEntrant(getEntrant());
                    session.update(_onlineEntrant);

                    if (_onlineEntrant.getForeignLanguage() != null)
                    {
                        boolean notContainLanguage = true;
                        List<PersonForeignLanguage> languages = DataAccessServices.dao().getList(PersonForeignLanguage.class, PersonForeignLanguage.person().id(), person.getId());
                        for (PersonForeignLanguage language : languages)
                        {
                            if (language.getLanguage().equals(_onlineEntrant.getForeignLanguage()))
                                notContainLanguage = false;
                        }

                        if (notContainLanguage)
                        {
                            PersonForeignLanguage language = new PersonForeignLanguage();
                            language.setPerson(person);
                            language.setLanguage(_onlineEntrant.getForeignLanguage());
                            session.save(language);
                        }
                    }

                    List<EnrEntrantAccessCourse> entrantCourses = Lists.newArrayList();
                    if (enrEntrant.getId() != null)
                        entrantCourses.addAll(DataAccessServices.dao().getList(EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.entrant().id(), enrEntrant.getId()));
                    Set<EnrAccessCourse> accessCourseSet = CommonBaseEntityUtil.getPropertiesSet(entrantCourses, EnrEntrantAccessCourse.course());

                    List<EnrOnlineEntrantAccessCourse> courses = DataAccessServices.dao().getList(EnrOnlineEntrantAccessCourse.class, EnrOnlineEntrantAccessCourse.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantAccessCourse course : courses)
                    {
                        if (!accessCourseSet.contains(course.getCourse()))
                        {
                            EnrEntrantAccessCourse entrantCourse = new EnrEntrantAccessCourse();
                            entrantCourse.setEntrant(getEntrant());
                            entrantCourse.setCourse(course.getCourse());
                            session.save(entrantCourse);
                        }
                    }

                    List<EnrEntrantAccessDepartment> entrantDepartments = Lists.newArrayList();
                    if (enrEntrant.getId() != null)
                        entrantDepartments.addAll(DataAccessServices.dao().getList(EnrEntrantAccessDepartment.class, EnrEntrantAccessDepartment.entrant().id(), enrEntrant.getId()));
                    Set<EnrAccessDepartment> accessDepartmentSet = CommonBaseEntityUtil.getPropertiesSet(entrantDepartments, EnrEntrantAccessDepartment.accessDepartment());

                    List<EnrOnlineEntrantAccessDepartment> departments = DataAccessServices.dao().getList(EnrOnlineEntrantAccessDepartment.class, EnrOnlineEntrantAccessDepartment.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantAccessDepartment department : departments)
                    {
                        if (!accessDepartmentSet.contains(department.getDepartment()))
                        {
                            EnrEntrantAccessDepartment entrantDepartment = new EnrEntrantAccessDepartment();
                            entrantDepartment.setEntrant(getEntrant());
                            entrantDepartment.setAccessDepartment(department.getDepartment());
                            session.save(entrantDepartment);
                        }
                    }

                    List<PersonSportAchievement> personSportAchievements = DataAccessServices.dao().getList(PersonSportAchievement.class, PersonSportAchievement.person().id(), person.getId());
                    Set<SportType> sportTypeSet = CommonBaseEntityUtil.getPropertiesSet(personSportAchievements, PersonSportAchievement.sportKind());

                    List<EnrOnlineEntrantSportAchievement> sportAchievements = DataAccessServices.dao().getList(EnrOnlineEntrantSportAchievement.class, EnrOnlineEntrantSportAchievement.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantSportAchievement sportAchievement : sportAchievements)
                    {
                        if (!sportTypeSet.contains(sportAchievement.getType()))
                        {
                            PersonSportAchievement personSportAchievement = new PersonSportAchievement();
                            personSportAchievement.setPerson(person);
                            personSportAchievement.setSportKind(sportAchievement.getType());
                            personSportAchievement.setSportRank(sportAchievement.getRank());
                            session.save(personSportAchievement);
                        }
                    }

                    List<EnrEntrantInfoAboutUniversity> infoAboutUniversities = Lists.newArrayList();
                    if (enrEntrant.getId() != null)
                        infoAboutUniversities.addAll(DataAccessServices.dao().getList(EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.entrant().id(), enrEntrant.getId()));
                    Set<EnrSourceInfoAboutUniversity> infoAboutUniversitySet = CommonBaseEntityUtil.getPropertiesSet(infoAboutUniversities, EnrEntrantInfoAboutUniversity.sourceInfo());

                    List<EnrOnlineEntrantSourceInfoAboutOU> sourceInfoAboutOUs = DataAccessServices.dao().getList(EnrOnlineEntrantSourceInfoAboutOU.class, EnrOnlineEntrantSourceInfoAboutOU.onlineEntrant().id(), _onlineEntrant.getId());
                    for (EnrOnlineEntrantSourceInfoAboutOU sourceInfoAboutOU : sourceInfoAboutOUs)
                    {
                        if (!infoAboutUniversitySet.contains(sourceInfoAboutOU.getSourceInfo()))
                        {
                            EnrEntrantInfoAboutUniversity infoAboutUniversity = new EnrEntrantInfoAboutUniversity();
                            infoAboutUniversity.setEntrant(getEntrant());
                            infoAboutUniversity.setSourceInfo(sourceInfoAboutOU.getSourceInfo());
                            session.save(infoAboutUniversity);
                        }
                    }
                }

                session.saveOrUpdate(enrEntrant);
                try {
                    session.saveOrUpdate(person);
                } catch (NonUniqueObjectException ex) {
                    person = (Person) session.merge(person);
                    session.saveOrUpdate(_selectedIdentityCard);
                }

                _selectedIdentityCard.setPerson(person);
                session.save(_selectedIdentityCard);

                return enrEntrant;
            }));
        }
    }

    private void saveOnlineEntrantIdCardScanCopy(IdentityCard identityCard)
    {
        if (getUploadFile() == null && getOnlineEntrant() != null && getOnlineEntrant().getIdCardScanCopy() != null)
        {
            try
            {
                PersonManager.instance().dao().addEditDocumentScan(true, null, new IUploadFile()
                {
                    @Override
                    public String getFileName()
                    {
                        return getOnlineEntrant().getIdCardScanCopy().getFilename();
                    }

                    @Override
                    public String getFilePath()
                    {
                        return null;
                    }

                    @Override
                    public InputStream getStream()
                    {
                        return new ByteArrayInputStream(getOnlineEntrant().getIdCardScanCopy().getContent());
                    }

                    @Override
                    public String getContentType()
                    {
                        return getOnlineEntrant().getIdCardScanCopy().getContentType();
                    }

                    @Override
                    public void write(File file)
                    {
                    }

                    @Override
                    public boolean isInMemory()
                    {
                        return true;
                    }

                    @Override
                    public long getSize()
                    {
                        return getOnlineEntrant().getIdCardScanCopy().getContent().length;
                    }
                }, identityCard.getId(), "Удостоверение личности");
            } catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public void onClickDownloadIdCardFile()
    {
        if (isShowOnlineEntrantScanCopyInfo()) {
            DatabaseFile file = getOnlineEntrant().getIdCardScanCopy();
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(file.getContentType()).
                    fileName(file.getFilename()).document(file.getContent()), false);
        }
    }

    private void prepareIdentityCardOptions()
    {
        getIdentityCardOptionsList().clear();
        if(getSelectedBasisPerson() != null) {
            List<IdentityCard> identityCards = EnrEntrantRequestAddWizardManager.instance().dao().getIdentityCardsForPerson(getSelectedBasisPerson().getId());
            Collections.sort(identityCards, (o1, o2) -> {
                if (o2.isActive()) return 1;
                if (o1.getIssuanceDate() == null && o2.getIssuanceDate() == null) return - o1.getCreationDate().compareTo(o2.getCreationDate());
                if (o1.getIssuanceDate() == null) return 1;
                if (o2.getIssuanceDate() == null) return -1;
                return - o1.getIssuanceDate().compareTo(o2.getIssuanceDate());
            });

            for(IdentityCard identityCard : identityCards) {
                getIdentityCardOptionsList().add(new IdentityCardOption(identityCard.getId(), identityCard.getFio() + " - " + identityCard.getTitle(), identityCard, false));
            }
        }
        if(getOnlineEntrant() != null) {
            IdentityCard identityCard = getOnlineEntrant().getIdentityCard();
            getIdentityCardOptionsList().add(new IdentityCardOption(identityCard.getId(), identityCard.getFio() + " - " + identityCard.getTitle() + " (данные онлайн-регистрации)", identityCard, true));
        }
        else {
            getIdentityCardOptionsList().add(new IdentityCardOption(1L, "Новое удостоверение личности", null, false));
            _lastCreatedCard = getPerson().getIdentityCard();
        }
    }

    private void prepareSimilarPersonStep()
    {
    }

    private void prepareFinalStep()
    {
        prepareIdentityCardOptions();
        setIdentityCardOption(getIdentityCardOptionsList().get(0));
        onSelectIdentityCard();
    }

    // getters and setters

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    public DataWrapper getOption()
    {
        return _option;
    }

    public void setOption(DataWrapper option)
    {
        _option = option;
    }

    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public List<IdentityCardOption> getIdentityCardOptionsList()
    {
        return _identityCardOptionsList;
    }

    public void setIdentityCardOptionsList(List<IdentityCardOption> identityCardOptionsList)
    {
        _identityCardOptionsList = identityCardOptionsList;
    }

    public IdentityCardOption getIdentityCardOption()
    {
        return _identityCardOption;
    }

    public void setIdentityCardOption(IdentityCardOption identityCardOption)
    {
        _identityCardOption = identityCardOption;
    }

    public IdentityCard getSelectedIdentityCard()
    {
        return _selectedIdentityCard;
    }

    public void setSelectedIdentityCard(IdentityCard selectedIdentityCard)
    {
        _selectedIdentityCard = selectedIdentityCard;
    }

    public ISelectModel getNationalityModel()
    {
        return _nationalityModel;
    }

    public void setNationalityModel(ISelectModel nationalityModel)
    {
        _nationalityModel = nationalityModel;
    }

    public boolean isSameFactAddress()
    {
        return _sameFactAddress;
    }

    public void setSameFactAddress(boolean sameFactAddress)
    {
        _sameFactAddress = sameFactAddress;
    }

    public EnrEntrant getExistEntrant()
    {
        return _existEntrant;
    }

    public void setExistEntrant(EnrEntrant existEntrant)
    {
        _existEntrant = existEntrant;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }
}
