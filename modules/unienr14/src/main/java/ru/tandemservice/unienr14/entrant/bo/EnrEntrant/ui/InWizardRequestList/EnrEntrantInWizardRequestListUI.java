package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardRequestList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.TopRegionActivation;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizardUI;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.gen.EnrEntrantRequestGen;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_ENROLLMENT_CAMPAIGN_ID, binding="enrollmentCampaignHolder.id", required=true),
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_PERSON_ID, binding="personId", required=false)
})
@TopRegionActivation
public class EnrEntrantInWizardRequestListUI extends UIPresenter {

    private final EntityHolder<EnrEnrollmentCampaign> enrollmentCampaignHolder = new EntityHolder<>();
    public EntityHolder<EnrEnrollmentCampaign> getEnrollmentCampaignHolder() { return this.enrollmentCampaignHolder; }
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return this.getEnrollmentCampaignHolder().getValue(); }

    private Long personId;
    public Long getPersonId() { return this.personId; }
    public void setPersonId(final Long personId) { this.personId = personId; }

    private StaticListDataSource<EnrRequestedCompetition> requestDataSource;
    public StaticListDataSource<EnrRequestedCompetition> getRequestDataSource() { return this.requestDataSource; }
    public void setRequestDataSource(final StaticListDataSource<EnrRequestedCompetition> requestDataSource) { this.requestDataSource = requestDataSource; }

    public String getRequestListTitle() {
        return "Заявления ("+getEnrollmentCampaign().getTitle()+")";
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrEnrollmentCampaign ec = this.getEnrollmentCampaignHolder().refresh(EnrEnrollmentCampaign.class);

        if (null == getPersonId()) {
            // если нет персоны, то ничего не показываем
            this.onClickNext();
            return;
        }

        final List<EnrRequestedCompetition> requestList = new DQLSelectBuilder()
        .fromEntity(EnrRequestedCompetition.class, "rqd").column(property("rqd"))
        .where(eq(property(EnrRequestedCompetition.request().entrant().person().id().fromAlias("rqd")), value(this.getPersonId())))
        .where(eq(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().fromAlias("rqd")), value(ec)))
        .order(property(EnrRequestedCompetition.priority().fromAlias("rqd")))
        .createStatement(this.getSupport().getSession()).list();

        if (requestList.isEmpty()) {
            // если заявлений нет, то ничего не показываем
            this.onClickNext();
            return;
        }

        final StaticListDataSource<EnrRequestedCompetition> requestDataSource = new StaticListDataSource<>(requestList);

        //        final IMergeRowIdResolver entrantMergeResolver = new SimpleMergeIdResolver(EnrRequestedCompetition.requestedCompGroup().entrantRequest().entrant().id());
        //        requestDataSource.addColumn(
        //            new PublisherColumnBuilder("Абитуриент", PersonRoleGen.person().fullFio(), "requestTab").entity(EnrRequestedCompetition.requestedCompGroup().entrantRequest().entrant())
        //            .build()
        //            .setMergeRowIdResolver(entrantMergeResolver)
        //            .setOrderable(false)
        //            .setWidth(10)
        //        );

        final IMergeRowIdResolver requestMergeResolver = new SimpleMergeIdResolver(EnrRequestedCompetition.request().id());
        requestDataSource.addColumn(new PublisherLinkColumn("№ заявления", EnrEntrantRequestGen.stringNumber(), EnrRequestedCompetition.request()).setOrderable(false).setMergeRowIdResolver(requestMergeResolver).setWidth(10));

        requestDataSource.addColumn(new SimpleColumn("Вид приема", EnrRequestedCompetition.competition().type().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));

        requestDataSource.addColumn(new PublisherLinkColumn("Набор ОП для приема", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title().s())
            .setRequired(true).setClickable(true).setOrderable(false));
        requestDataSource.addColumn(new SimpleColumn("Подразделение", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));
        requestDataSource.addColumn(new SimpleColumn("Форма обучения", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programForm().title().s()).setClickable(false).setOrderable(false).setWidth(5));
        requestDataSource.addColumn(new SimpleColumn("Вид обр. программы", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));
        requestDataSource.addColumn(new SimpleColumn("Огр. на ур. образ.", EnrRequestedCompetition.competition().eduLevelRequirement().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));
        requestDataSource.addColumn(new SimpleColumn("Доп. условия", EnrRequestedCompetition.parametersTitle().s()).setClickable(false).setOrderable(false).setWidth(15));

        requestDataSource.addColumn(new SimpleColumn("Дата добавления", EnrRequestedCompetition.P_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false).setRequired(true).setWidth(7));

        requestDataSource.addColumn(new SimpleColumn("Состояние", EnrRequestedCompetition.L_STATE + "." + EnrEntrantState.P_TITLE).setClickable(false).setOrderable(false).setWidth(5));
        requestDataSource.addColumn(new SimpleColumn("Примечание", EnrRequestedCompetition.P_COMMENT).setClickable(false).setOrderable(false).setWidth(20));
        requestDataSource.addColumn(new SimpleColumn("Приоритет", EnrRequestedCompetition.priority().s()).setClickable(false).setOrderable(false).setWidth(5));

        this.setRequestDataSource(requestDataSource);
    }

    public void onClickNext()
    {
        // просто переходим дальше
        deactivate(
            new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), getEnrollmentCampaignHolder().getId())
            .buildMap()
        );
    }

}
