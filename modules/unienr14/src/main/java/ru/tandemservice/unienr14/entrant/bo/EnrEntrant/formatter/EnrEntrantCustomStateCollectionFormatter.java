/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IRawFormatter;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;

import java.util.Arrays;
import java.util.Collection;

/**
 * TODO убрать table, сделать INSTANCE
 * @author nvankov
 * @since 3/28/13
 */
public class EnrEntrantCustomStateCollectionFormatter implements IRawFormatter
{
    @SuppressWarnings("unchecked")
    @Override
    public String format(Object source)
    {
        if (source == null)
        {
            return "";
        }

        if (source instanceof Object[])
        {
            source = Arrays.asList((Object[]) source);
        }

        if (source instanceof Collection)
        {
            if(!((Collection) source).isEmpty())
            {
                StringBuilder tableBuilder = new StringBuilder("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");

//                List<String> entrantCustomStateStrList = Lists.newArrayList();
                for (EnrEntrantCustomState item : ((Collection<EnrEntrantCustomState>) source))
                {
                    StringBuilder entrantCustomStateStrBuilder = new StringBuilder(item.getCustomState().getShortTitle());
                    if (null != item.getBeginDate() || null != item.getEndDate())
                    {
                        entrantCustomStateStrBuilder.append(" (");
                        if (null != item.getBeginDate())
                        {
                            entrantCustomStateStrBuilder.append("c ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getBeginDate()));
                            if (null != item.getEndDate())
                                entrantCustomStateStrBuilder.append(" ");
                        }
                        if (null != item.getEndDate())
                            entrantCustomStateStrBuilder.append("по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getEndDate()));
                        entrantCustomStateStrBuilder.append(")");
                    }

                    tableBuilder.append("<tr><td style=\"white-space: nowrap;\"><div style=\"color:").append(item.getCustomState().getHtmlColor()).append("\">").append(entrantCustomStateStrBuilder).append("</div></td></tr>");
                }
                tableBuilder.append("</table>");
                return tableBuilder.toString();
            }
            else
                return "";
        }
        else
        {
            return "";
        }
    }
}

