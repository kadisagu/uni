/**
 *$Id: EnrExamGroupSearchDSHandler.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.List.EnrExamGroupList;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.List.EnrExamGroupListUI;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
public class EnrExamGroupSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String V_PROP_EXAM_PASS_DISC_SIZE = "examPassDiscSize";
    public static final String V_PROP_EVENT = "event";

    public EnrExamGroupSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrExamGroup.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final OrgUnit terrOrgUnit = context.get(EnrExamGroupList.BIND_ORG_UNIT);

        if (enrCamp == null || terrOrgUnit == null)
            return ListOutputBuilder.get(input, new ArrayList(0)).build();

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "b").column(property("b"))
                .where(eq(property(EnrExamGroup.examGroupSet().enrollmentCampaign().fromAlias("b")), value(enrCamp)))
                .where(eq(property(EnrExamGroup.territorialOrgUnit().fromAlias("b")), value(terrOrgUnit)));

        filters(dql, "b", input, context);
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();
        return wrap(output, input, context);
    }

    /**
     *
     * @param dql builder from EnrExamGroup
     */
    protected void filters(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final EnrExamGroupSet examGroupSet = context.get(EnrExamGroupListUI.SETTING_EXAM_GROUP_SET);
        final List<DataWrapper> examList = context.get(EnrExamGroupListUI.SETTING_DISCIPLINE_LIST);
        final List<DataWrapper> daysList = context.get(EnrExamGroupListUI.SETTING_DAYS_LIST);

        if (examGroupSet != null)
        {
            dql.where(eq(property(EnrExamGroup.examGroupSet().fromAlias(alias)), value(examGroupSet)));
        }
        if (CollectionUtils.isNotEmpty(examList))
        {
            final List<Object[]> inDataList = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "eg")
                    .column(property(EnrExamGroup.discipline().id().fromAlias("eg")))
                    .column(property(EnrExamGroup.passForm().id().fromAlias("eg")))
                    .where(in(property(EnrExamGroup.id().fromAlias("eg")), UniBaseDao.ids(examList)))
                    .createStatement(context.getSession()).list();

            dql
                    .where(in(property(EnrExamGroup.discipline().id().fromAlias(alias)), UniBaseUtils.getColumn(inDataList, 0)))
                    .where(in(property(EnrExamGroup.passForm().id().fromAlias(alias)), UniBaseUtils.getColumn(inDataList, 1)));
        }
        if (CollectionUtils.isNotEmpty(daysList))
        {
            final List<Integer> inDaysList = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "eg")
                    .column(property(EnrExamGroup.days().fromAlias("eg")))
                    .where(in(property(EnrExamGroup.id().fromAlias("eg")), UniBaseDao.ids(daysList)))
                    .createStatement(context.getSession()).list();

            dql.where(in(property(EnrExamGroup.days().fromAlias(alias)), inDaysList));
        }
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final Map<Long, MutableInt> group2PassDicsCountMap = SafeMap.get(MutableInt.class);
        final Map<Long, List<EnrExamScheduleEvent>> group2EventsMap = SafeMap.get(ArrayList.class);

        BatchUtils.execute(output.getRecordIds(), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                final List<Long> passDiscList = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "b")
                        .column(property(EnrExamPassDiscipline.examGroup().id().fromAlias("b")))
                        .where(in(property(EnrExamPassDiscipline.examGroup().id().fromAlias("b")), elements))
                        .createStatement(context.getSession()).list();

                final List<EnrExamGroupScheduleEvent> eventList = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "b").column(property("b"))
                        .where(in(property(EnrExamGroupScheduleEvent.examGroup().id().fromAlias("b")), elements))
                        .createStatement(context.getSession()).list();

                for (Long id : passDiscList)
                {
                    group2PassDicsCountMap.get(id).increment();
                }

                for (EnrExamGroupScheduleEvent groupEvent : eventList)
                {
                    group2EventsMap.get(groupEvent.getExamGroup().getId())
                            .add(groupEvent.getExamScheduleEvent());
                }
            }
        });

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            final EnrExamGroup examGroup = wrapper.getWrapped();

            wrapper.setProperty(V_PROP_EXAM_PASS_DISC_SIZE, getExmaPassDisc2SizeStr(examGroup, group2PassDicsCountMap.get(examGroup.getId()).intValue()));
            wrapper.setProperty(V_PROP_EVENT, getEventStrList(group2EventsMap.get(examGroup.getId())));
        }

        return output;
    }

    /**
     * Список событий по формату:
     * @return <дата ДД.ММ.ГГ>, <время начала ЧЧ:ММ>-<время окончания ЧЧ:ММ>, <название аудитории с указанием местоположения (вариант 2)>, комиссия: <комиссия для события>
     */
    protected String getEventStrList(List<EnrExamScheduleEvent> eventList)
    {
        final List<String> resultList = new ArrayList<>();

        for (EnrExamScheduleEvent event : eventList)
        {
            final StringBuilder string = new StringBuilder();
            string
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getScheduleEvent().getDurationBegin()))
                    .append(", ")
                    .append(DateFormatter.DATE_FORMATTER_TIME.format(event.getScheduleEvent().getDurationBegin()))
                    .append("-")
                    .append(DateFormatter.DATE_FORMATTER_TIME.format(event.getScheduleEvent().getDurationEnd()))
                    .append(", ")
                    .append(event.getExamRoom().getPlace().getTitleWithLocation());
            if (event.getCommission() != null)
                string.append(", комиссия: ").append(event.getCommission());

            resultList.add(string.toString());
        }

        return CoreStringUtils.join(resultList, "\n");
    }

    /**
     * @return <число ДДС в ЭГ>/<вместимость ЭГ>
     */
    protected String getExmaPassDisc2SizeStr(EnrExamGroup examGroup, int passDiscCount)
    {
        return passDiscCount + "/" + examGroup.getSize();
    }
}
