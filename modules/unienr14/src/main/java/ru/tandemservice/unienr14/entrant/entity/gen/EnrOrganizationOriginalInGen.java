package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Организация, в которую подан оригинал
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrganizationOriginalInGen extends EntityBase
 implements INaturalIdentifiable<EnrOrganizationOriginalInGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn";
    public static final String ENTITY_NAME = "enrOrganizationOriginalIn";
    public static final int VERSION_HASH = -276893557;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String P_TITLE = "title";

    private EnrEntrant _entrant;     // Абитуриент
    private PersonEduDocument _eduDocument;     // Документ о полученном образовании
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ о полученном образовании. Свойство не может быть null.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOrganizationOriginalInGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrOrganizationOriginalIn)another).getEntrant());
                setEduDocument(((EnrOrganizationOriginalIn)another).getEduDocument());
            }
            setTitle(((EnrOrganizationOriginalIn)another).getTitle());
        }
    }

    public INaturalId<EnrOrganizationOriginalInGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getEduDocument());
    }

    public static class NaturalId extends NaturalIdBase<EnrOrganizationOriginalInGen>
    {
        private static final String PROXY_NAME = "EnrOrganizationOriginalInNaturalProxy";

        private Long _entrant;
        private Long _eduDocument;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, PersonEduDocument eduDocument)
        {
            _entrant = ((IEntity) entrant).getId();
            _eduDocument = ((IEntity) eduDocument).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getEduDocument()
        {
            return _eduDocument;
        }

        public void setEduDocument(Long eduDocument)
        {
            _eduDocument = eduDocument;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrOrganizationOriginalInGen.NaturalId) ) return false;

            EnrOrganizationOriginalInGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getEduDocument(), that.getEduDocument()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getEduDocument());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getEduDocument());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrganizationOriginalInGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrganizationOriginalIn.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrganizationOriginalIn();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "eduDocument":
                    return obj.getEduDocument();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "eduDocument":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "eduDocument":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrganizationOriginalIn> _dslPath = new Path<EnrOrganizationOriginalIn>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrganizationOriginalIn");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrOrganizationOriginalIn> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrOrganizationOriginalInGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrOrganizationOriginalIn.class;
        }

        public String getEntityName()
        {
            return "enrOrganizationOriginalIn";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
