/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.logic;

import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

/**
 * @author nvankov
 * @since 2/18/16
 */
public class SettingsOptionGroup
{
    private String _title;
    private String _key;
    private int _priority = 9999;
    private String _propertyPath;
    private IDefaultComboDataSourceHandler _handler;
    private ItemListExtPoint<DataWrapper> _itemList;

    public SettingsOptionGroup(String title, String key, String propertyPath, IDefaultComboDataSourceHandler handler, ItemListExtPoint<DataWrapper> itemList, int priority)
    {
        this(title, key, propertyPath, handler, itemList);
        _priority = priority;
    }

    public SettingsOptionGroup(String title, String key, String propertyPath, IDefaultComboDataSourceHandler handler, ItemListExtPoint<DataWrapper> itemList)
    {
        _title = title;
        _key = key;
        _propertyPath = propertyPath;
        _handler = handler;
        _itemList = itemList;
    }

    public String getTitle()
    {
        return _title;
    }

    public String getKey()
    {
        return _key;
    }

    public int getPriority()
    {
        return _priority;
    }

    public String getPropertyPath()
    {
        return _propertyPath;
    }

    public IDefaultComboDataSourceHandler getHandler()
    {
        return _handler;
    }

    public ItemListExtPoint<DataWrapper> getItemList()
    {
        return _itemList;
    }
}
