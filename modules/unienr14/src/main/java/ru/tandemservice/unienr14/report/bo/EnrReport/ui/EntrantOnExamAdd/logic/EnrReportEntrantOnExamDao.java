/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd.logic;


import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd.EnrReportEntrantOnExamAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.RtfBackslashScreener;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantOnExam;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 23.05.2014
 */
public class EnrReportEntrantOnExamDao  extends UniBaseDao implements IEnrReportEntrantOnExamDao
{

    private final String RTF_LINE = "\\line ";

    @Override
    public Long createReport(EnrReportEntrantOnExamAddUI model) {

        EnrReportEntrantOnExam report = new EnrReportEntrantOnExam();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantOnExam.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantOnExam.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantOnExam.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantOnExam.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantOnExam.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantOnExam.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantOnExam.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantOnExam.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantOnExam.P_PROGRAM_SET, "title");

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantOnExam.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantOnExamAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_ON_EXAM);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL
                .column(property("entrant"))
                .column(property("request"))
                .column(property("psOu"))
                .column(property("rr"));

        requestedCompDQL
                .joinEntity("entrant", DQLJoinType.inner, EnrEntrantStateExamResult.class, "rr", eq(property("entrant", EnrEntrant.id()), property("rr", EnrEntrantStateExamResult.entrant().id())))
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property("reqComp", EnrRequestedCompetition.id()), property("vvi", EnrChosenEntranceExam.requestedCompetition().id())))
                .joinEntity("vvi", DQLJoinType.inner, EnrChosenEntranceExamForm.class, "vvif", eq(property("vvif", EnrChosenEntranceExamForm.chosenEntranceExam().id()), property("vvi", EnrChosenEntranceExam.id())))
                .where(eq(property("rr", EnrEntrantStateExamResult.secondWave()), value(Boolean.TRUE)))
                .where(eq(property("rr", EnrEntrantStateExamResult.year()), value(Calendar.getInstance().get(Calendar.YEAR))))
                .where(eq(property("vvi", EnrChosenEntranceExam.discipline().stateExamSubject().id()), property("rr", EnrEntrantStateExamResult.subject().id())))
                .where(eq(property("vvif", EnrChosenEntranceExamForm.actualExamPassForm().passForm().code()), value(EnrExamPassFormCodes.STATE_EXAM)));




        List<Object[]> properties = getList(requestedCompDQL);

        // перенос данных для  таблицы в удобную форму:  entrant - request - list <orgUnits> - list <results>

        // набор абитуриентов, упорядочен по ФИО
        TreeSet<EnrEntrant> entrantSet = new TreeSet<EnrEntrant>(new Comparator<EnrEntrant>(){
            @Override
            public int compare(EnrEntrant o1, EnrEntrant o2) {
                return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFullFio(), o2.getFullFio());
            }
        });
        Map<EnrEntrant, Set<EnrProgramSetOrgUnit>> programSetOUMap = SafeMap.get(HashSet.class);
        Map<EnrEntrant, Set<EnrEntrantStateExamResult>> resultListMap = SafeMap.get(HashSet.class);

        for (Object[] line : properties)
        {
            EnrEntrant entrant = (EnrEntrant)line[0];
            EnrEntrantStateExamResult result = (EnrEntrantStateExamResult) line[3];
            EnrProgramSetOrgUnit orgUnit = (EnrProgramSetOrgUnit) line[2];

            entrantSet.add(entrant);
            programSetOUMap.get(entrant).add(orgUnit);
            resultListMap.get(entrant).add(result);
        }

        // формирование таблицы для метки H, параметры выбора абитуриентов в отчет.

        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateFrom(), model.getDateTo());
        // формирование основной таблицы Т с абитуриентами, направленными на экзамен в доп.срок

        ArrayList<String[]> tTable = new ArrayList<>();
        int number = 1;
        for (EnrEntrant entrant : entrantSet)
        {
            String[] row = {"","","","","","","","","",""};
            row[0] = String.valueOf(number++);
            row[1] = "";
            row[2] = entrant.getFullFio();

            IdentityCard mainIdc = entrant.getPerson().getIdentityCard();
            if (mainIdc.getAddress() != null) {
                if (mainIdc.getAddress().getTitle().length() > 8)
                    row[3] = mainIdc.getAddress().getTitle().substring(8);
                else row[3] = "не указано";
            }

            StringBuilder idCardData = new StringBuilder();
            idCardData.append(mainIdc.getDisplayableTitle());
            if (mainIdc.getIssuancePlace() != null)
            {
                idCardData.append(" ");
                idCardData.append(mainIdc.getIssuancePlace());
            }
            row[4] = idCardData.toString();

            ArrayList<EnrEntrantStateExamResult> resultList = new ArrayList<>(resultListMap.get(entrant));
            // сортировка предметов по алфавиту
            Collections.sort(resultList, new Comparator<EnrEntrantStateExamResult>() {
                @Override
                public int compare(EnrEntrantStateExamResult o1, EnrEntrantStateExamResult o2) {
                    return o1.getSubject().getTitle().compareTo(o2.getSubject().getTitle());
                }
            });
            row[5] = new RtfBackslashScreener().screenBackslashes(UniStringUtils.join(resultList, EnrEntrantStateExamResult.subject().title().s(), RTF_LINE));
            row[6] = new RtfBackslashScreener().screenBackslashes(UniStringUtils.join(resultList, EnrEntrantStateExamResult.secondWaveExamPlace().title().s(), RTF_LINE));

            ArrayList<EnrProgramSetOrgUnit> orgUnitList = new ArrayList<>(programSetOUMap.get(entrant));

            // сортировка вывода образовательных организаций, за которыми закреплен студент. Сначала головная, затем по алфавиту.
            Collections.sort(orgUnitList, new Comparator<EnrProgramSetOrgUnit>() {
                @Override
                public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                    int i = Boolean.compare(null == o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getParent(), null == o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getParent());
                    if (0 != i) { return -i; }

                    return o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle().compareToIgnoreCase(o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle());
                }
            } );

            row[8] = new RtfBackslashScreener().screenBackslashes(UniStringUtils.join(orgUnitList, EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().shortTitle().s(), RTF_LINE));
            tTable.add(row);
        }

        RtfTableModifier tableModifier = new RtfTableModifier();

        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("T", tTable.toArray(new String[tTable.size()][]));
        tableModifier.put("T", new RtfRowIntercepterRawText());
        tableModifier.put("H", new RtfRowIntercepterRawText());

        tableModifier.modify(document);
        return RtfUtil.toByteArray(document);
    }


}
