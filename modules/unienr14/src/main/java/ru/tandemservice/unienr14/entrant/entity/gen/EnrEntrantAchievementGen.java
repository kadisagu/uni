package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальное достижение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantAchievementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement";
    public static final String ENTITY_NAME = "enrEntrantAchievement";
    public static final int VERSION_HASH = -1066573926;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_TYPE = "type";
    public static final String L_DOCUMENT = "document";
    public static final String L_REQUEST = "request";
    public static final String P_MARK_AS_LONG = "markAsLong";
    public static final String P_RATING_MARK_AS_LONG = "ratingMarkAsLong";
    public static final String P_MARK = "mark";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrEntrantAchievementType _type;     // Вид индивидуального достижения в ПК
    private IEnrEntrantAchievementProofDocument _document;     // Документ
    private EnrEntrantRequest _request;     // Заявление
    private long _markAsLong;     // Балл
    private long _ratingMarkAsLong;     // Балл для рейтинга

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Вид индивидуального достижения в ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantAchievementType getType()
    {
        return _type;
    }

    /**
     * @param type Вид индивидуального достижения в ПК. Свойство не может быть null.
     */
    public void setType(EnrEntrantAchievementType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Документ.
     */
    public IEnrEntrantAchievementProofDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ.
     */
    public void setDocument(IEnrEntrantAchievementProofDocument document)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && document!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrEntrantAchievementProofDocument.class);
            IEntityMeta actual =  document instanceof IEntity ? EntityRuntime.getMeta((IEntity) document) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Заявление.
     */
    public EnrEntrantRequest getRequest()
    {
        return _request;
    }

    /**
     * @param request Заявление.
     */
    public void setRequest(EnrEntrantRequest request)
    {
        dirty(_request, request);
        _request = request;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public long getMarkAsLong()
    {
        return _markAsLong;
    }

    /**
     * @param markAsLong Балл. Свойство не может быть null.
     */
    public void setMarkAsLong(long markAsLong)
    {
        dirty(_markAsLong, markAsLong);
        _markAsLong = markAsLong;
    }

    /**
     * Балл, который пойдет в рейтинг - обновляется демоном на основании балла и настройки ИД. Хранится со смещением в три знака для дробной части.
     *
     * @return Балл для рейтинга. Свойство не может быть null.
     */
    @NotNull
    public long getRatingMarkAsLong()
    {
        return _ratingMarkAsLong;
    }

    /**
     * @param ratingMarkAsLong Балл для рейтинга. Свойство не может быть null.
     */
    public void setRatingMarkAsLong(long ratingMarkAsLong)
    {
        dirty(_ratingMarkAsLong, ratingMarkAsLong);
        _ratingMarkAsLong = ratingMarkAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantAchievementGen)
        {
            setEntrant(((EnrEntrantAchievement)another).getEntrant());
            setType(((EnrEntrantAchievement)another).getType());
            setDocument(((EnrEntrantAchievement)another).getDocument());
            setRequest(((EnrEntrantAchievement)another).getRequest());
            setMarkAsLong(((EnrEntrantAchievement)another).getMarkAsLong());
            setRatingMarkAsLong(((EnrEntrantAchievement)another).getRatingMarkAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantAchievementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantAchievement.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantAchievement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "type":
                    return obj.getType();
                case "document":
                    return obj.getDocument();
                case "request":
                    return obj.getRequest();
                case "markAsLong":
                    return obj.getMarkAsLong();
                case "ratingMarkAsLong":
                    return obj.getRatingMarkAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "type":
                    obj.setType((EnrEntrantAchievementType) value);
                    return;
                case "document":
                    obj.setDocument((IEnrEntrantAchievementProofDocument) value);
                    return;
                case "request":
                    obj.setRequest((EnrEntrantRequest) value);
                    return;
                case "markAsLong":
                    obj.setMarkAsLong((Long) value);
                    return;
                case "ratingMarkAsLong":
                    obj.setRatingMarkAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "type":
                        return true;
                case "document":
                        return true;
                case "request":
                        return true;
                case "markAsLong":
                        return true;
                case "ratingMarkAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "type":
                    return true;
                case "document":
                    return true;
                case "request":
                    return true;
                case "markAsLong":
                    return true;
                case "ratingMarkAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "type":
                    return EnrEntrantAchievementType.class;
                case "document":
                    return IEnrEntrantAchievementProofDocument.class;
                case "request":
                    return EnrEntrantRequest.class;
                case "markAsLong":
                    return Long.class;
                case "ratingMarkAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantAchievement> _dslPath = new Path<EnrEntrantAchievement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantAchievement");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Вид индивидуального достижения в ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getType()
     */
    public static EnrEntrantAchievementType.Path<EnrEntrantAchievementType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Документ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getDocument()
     */
    public static IEnrEntrantAchievementProofDocumentGen.Path<IEnrEntrantAchievementProofDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Заявление.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getRequest()
     */
    public static EnrEntrantRequest.Path<EnrEntrantRequest> request()
    {
        return _dslPath.request();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getMarkAsLong()
     */
    public static PropertyPath<Long> markAsLong()
    {
        return _dslPath.markAsLong();
    }

    /**
     * Балл, который пойдет в рейтинг - обновляется демоном на основании балла и настройки ИД. Хранится со смещением в три знака для дробной части.
     *
     * @return Балл для рейтинга. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getRatingMarkAsLong()
     */
    public static PropertyPath<Long> ratingMarkAsLong()
    {
        return _dslPath.ratingMarkAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getMark()
     */
    public static SupportedPropertyPath<Double> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends EnrEntrantAchievement> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrEntrantAchievementType.Path<EnrEntrantAchievementType> _type;
        private IEnrEntrantAchievementProofDocumentGen.Path<IEnrEntrantAchievementProofDocument> _document;
        private EnrEntrantRequest.Path<EnrEntrantRequest> _request;
        private PropertyPath<Long> _markAsLong;
        private PropertyPath<Long> _ratingMarkAsLong;
        private SupportedPropertyPath<Double> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Вид индивидуального достижения в ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getType()
     */
        public EnrEntrantAchievementType.Path<EnrEntrantAchievementType> type()
        {
            if(_type == null )
                _type = new EnrEntrantAchievementType.Path<EnrEntrantAchievementType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Документ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getDocument()
     */
        public IEnrEntrantAchievementProofDocumentGen.Path<IEnrEntrantAchievementProofDocument> document()
        {
            if(_document == null )
                _document = new IEnrEntrantAchievementProofDocumentGen.Path<IEnrEntrantAchievementProofDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Заявление.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getRequest()
     */
        public EnrEntrantRequest.Path<EnrEntrantRequest> request()
        {
            if(_request == null )
                _request = new EnrEntrantRequest.Path<EnrEntrantRequest>(L_REQUEST, this);
            return _request;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getMarkAsLong()
     */
        public PropertyPath<Long> markAsLong()
        {
            if(_markAsLong == null )
                _markAsLong = new PropertyPath<Long>(EnrEntrantAchievementGen.P_MARK_AS_LONG, this);
            return _markAsLong;
        }

    /**
     * Балл, который пойдет в рейтинг - обновляется демоном на основании балла и настройки ИД. Хранится со смещением в три знака для дробной части.
     *
     * @return Балл для рейтинга. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getRatingMarkAsLong()
     */
        public PropertyPath<Long> ratingMarkAsLong()
        {
            if(_ratingMarkAsLong == null )
                _ratingMarkAsLong = new PropertyPath<Long>(EnrEntrantAchievementGen.P_RATING_MARK_AS_LONG, this);
            return _ratingMarkAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement#getMark()
     */
        public SupportedPropertyPath<Double> mark()
        {
            if(_mark == null )
                _mark = new SupportedPropertyPath<Double>(EnrEntrantAchievementGen.P_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return EnrEntrantAchievement.class;
        }

        public String getEntityName()
        {
            return "enrEntrantAchievement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getMark();
}
