package ru.tandemservice.unienr14.exams.entity;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.exams.entity.gen.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.*;
import java.util.function.Function;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Время проведения ВИ для ЭГ
 */
public class EnrExamGroupScheduleEvent extends EnrExamGroupScheduleEventGen
{
    public EnrExamGroupScheduleEvent()
    {
    }

    public EnrExamGroupScheduleEvent(EnrExamGroup examGroup, EnrExamScheduleEvent examScheduleEvent)
    {
        setExamGroup(examGroup);
        setExamScheduleEvent(examScheduleEvent);
    }

    @Override
    @EntityDSLSupport
    public String getScheduleTitle() {
        // считаем, что начинается и заканчивается событие в один день
        // формат: "20.07.2013, 10:00-15:00, ауд. 1А (ГУК, 1 этаж), комиссия: Иванов И.И., Петров П.П."
        StringBuilder title = new StringBuilder()
        .append(getTimeTitle())
        .append(", ").append(getExamScheduleEvent().getExamRoom().getPlace().getTitleWithLocation());
        if (getExamScheduleEvent().getCommission() != null)
            title.append(", комиссия: ").append(getExamScheduleEvent().getCommission());
        return title.toString();
    }

    public String getTimeTitle() {
        Date startTime = getExamScheduleEvent().getScheduleEvent().getDurationBegin();
        Date endTime = getExamScheduleEvent().getScheduleEvent().getDurationEnd();
		DateFormatter timeFormatter = DateFormatter.DATE_FORMATTER_TIME;
		return DateFormatter.DEFAULT_DATE_FORMATTER.format(startTime) + ", " + timeFormatter.format(startTime) + "-" + timeFormatter.format(endTime);
    }

	/**
	 * Событие расписания с названием экзаменационной группы.
	 * <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=34046931">http://wiki.tandemservice.ru/pages/viewpage.action?pageId=34046931</a><br/>
	 * Формат: <Название ЭГ> ([дней] getScheduleTitle())<br/>
	 * [дней]: <номер события>/<кол-во дней сдачи> дн.:<br/>
	 * Пример:<br/>490 (1/3 дн.: 18.07.2015, 10:00-13:00, 218б (Корпус "А", блок: Основной, 2 этаж))<br/>10 (18.07.2015, 10:00-13:00, 218б (Корпус "А", блок: Основной, 2 этаж))
	 */
	@Override
	@EntityDSLSupport
	public String getScheduleAndExamGroupTitle()
	{
		String days = "";
		if (getExamGroup().getDays() > 1)
		{
			final String groupEventAlias = "groupEvent";
			final Date startDate = getExamScheduleEvent().getScheduleEvent().getDurationBegin();
			int eventsBefore = DataAccessServices.dao().getCount(new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, groupEventAlias)
						.where(eq(property(groupEventAlias, EnrExamGroupScheduleEvent.examGroup()), value(getExamGroup())))
						.where(lt(property(groupEventAlias, EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin()), valueDate(startDate))));
			days = String.valueOf(eventsBefore + 1) + "/" + String.valueOf(getExamGroup().getDays()) + " дн.: ";
		}
		return getExamGroup().getTitle() + " (" + days + getScheduleTitle() + ")";
	}

	public static final String DS_PARAM_ENR_CAMPAIGN = EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN;
	public static final String DS_PARAM_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
	public static final String DS_PARAM_EXAM_GROUP_SET = "examGroupSet";
	public static final String DS_PARAM_DISCIPLINE = "discipline";
	public static final String DS_PARAM_PASS_FORM = "passForm";
	public static final String DS_PARAM_CLOSED = "closed";
	public static final String DS_PARAM_CURR_EXAM_GROUP = "currExamGroup";

	public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
	{
		return new EntityComboDataSourceHandler(name, EnrExamGroupScheduleEvent.class)
		{
			/**
			 * Вернуть новый список записей, в котором не будет записей с одинаковой экз. группой. Если в оригинальном списке было две или более записей с одной группой,
			 * то в результирующий список попадет только первая из них. Т.к. записи упорядочены по дате, времени и группе, то попадает самая ранняя запись.
			 */
			private List<EnrExamGroupScheduleEvent> skipDuplicateByGroups(List<EnrExamGroupScheduleEvent> records)
			{
				Set<Long> usedGroupIds = new HashSet<>();
				List<EnrExamGroupScheduleEvent> filteredRecords = Lists.newArrayList();
				records.forEach(row ->
								{
									Long groupId = row.getExamGroup().getId();
									if (!usedGroupIds.contains(groupId))
									{
										filteredRecords.add(row);
										usedGroupIds.add(groupId);
									}
								});
				return filteredRecords;
			}

			/**
			 * like-выражение, позволяющее осуществлять поиск по текстовому представлению даты - дата преобразуется в текстовое представление в формате ДД.ММ.ГГГГ.
			 * Используется для фильтрации по {@code EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin()} (дата начала имеет тип Date, не Time и не Timestamp).
			 */
            @Override
            protected IDQLExpression like(String alias, MetaDSLPath path, String filter)
            {
                if(EntityRuntime.getMeta(EnrExamGroupScheduleEvent.class.getSimpleName()).getPropertyByThroughPath(path.s()).getValueType().equals(Date.class))
	                return DQLExpressions.like(
			                DQLFunctions.upper(
					                DQLFunctions.concat(
											datePartExpr(alias, path, DQLFunctions::day),
											commonValue(".", PropertyType.STRING),
											datePartExpr(alias, path, DQLFunctions::month),
											commonValue(".", PropertyType.STRING),
											DQLFunctions.cast(DQLFunctions.year(property(alias, path)), PropertyType.STRING))),
			                value(CoreStringUtils.escapeLike(filter, true)));

                return super.like(alias, path, filter);
            }

			/**
			 * Выражение, содержащее текстовое представление числа, извлекаемого из {@code alias.path} методом {@code datePartExtractor}. Если число содержит только одну цифру,
			 * в начало добавляется лидирующий ноль. Метод используется для получения выражения, содержащего текстовое представление дня и месяца из даты в двухциферном формате.
			 * @param alias алиас сущности
			 * @param path путь к свойству
			 * @param datePartExtractor метод, извлекающий из свойства некоторое числовое значение
			 * @return DQL-аналог {@code (alias.path < 10) ? "0" + (alias.path).toString : (alias.path).toString}
			 */
            private IDQLExpression datePartExpr(String alias, MetaDSLPath path, Function<IDQLExpression, IDQLExpression> datePartExtractor)
            {
                IDQLExpression castPartToStringExpr = DQLFunctions.cast(datePartExtractor.apply(property(alias, path)), PropertyType.STRING);
                IDQLExpression exprFromOneDigit = DQLFunctions.concat(commonValue("0", PropertyType.STRING), castPartToStringExpr);
	            return caseExpr(lt(datePartExtractor.apply(property(alias, path)), value(10)), exprFromOneDigit, castPartToStringExpr);
            }

            @Override
			protected DSOutput execute(final DSInput input, final ExecutionContext context)
			{
				DSOutput output = super.execute(input, context);
				List<EnrExamGroupScheduleEvent> records = Lists.newArrayList(output.<EnrExamGroupScheduleEvent>getRecordList());
				records = skipDuplicateByGroups(records);
				output.setRecordList(Lists.newArrayList(records));
				output.setCountRecord(records.size());
				output.setTotalSize(records.size());
				return output;
			}
		}
				.titleProperty(EnrExamGroupScheduleEvent.scheduleAndExamGroupTitle().s())
				.where(EnrExamGroupScheduleEvent.examGroup().examGroupSet().enrollmentCampaign(), DS_PARAM_ENR_CAMPAIGN)
				.where(EnrExamGroupScheduleEvent.examGroup().territorialOrgUnit(), DS_PARAM_TERRITORIAL_ORG_UNIT)
				.where(EnrExamGroupScheduleEvent.examGroup().examGroupSet(), DS_PARAM_EXAM_GROUP_SET, true)
				.where(EnrExamGroupScheduleEvent.examGroup().discipline(), DS_PARAM_DISCIPLINE)
				.where(EnrExamGroupScheduleEvent.examGroup().passForm(), DS_PARAM_PASS_FORM)
				.filter(EnrExamGroupScheduleEvent.examGroup().title())
				.filter(EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin())
				.filter(EnrExamGroupScheduleEvent.examScheduleEvent().examRoom().place().floor().unit().building().title())
				.filter(EnrExamGroupScheduleEvent.examScheduleEvent().examRoom().place().floor().unit().title())
				.filter(EnrExamGroupScheduleEvent.examScheduleEvent().examRoom().place().title())
				.order(EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin())
				.order(EnrExamGroupScheduleEvent.examGroup().title())
				.customize((alias, dql, context, filter) ->
				{    // Фильтрация по открытости/закрытости группы. Если группа уже была выбрана, ее тоже показывать, несмотря на закрытость.
					Boolean closed = context.get(DS_PARAM_CLOSED);
					if (closed == null)
						return dql;
					final EnrExamGroup currentGroup = context.get(DS_PARAM_CURR_EXAM_GROUP);
					return dql.where(or(
							eq(property(alias, EnrExamGroupScheduleEvent.examGroup().closed()), value(closed)),
							eq(property(alias, EnrExamGroupScheduleEvent.examGroup()), value(currentGroup))));
                })
				;
	}
}