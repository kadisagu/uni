/* $Id: EcgpQuotaFreeDTO.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EnrPAQuotaFreeWrapper implements IEnrPAQuotaFreeWrapper
{
    private IEnrPAQuotaWrapper _quotaDTO;
    private IEnrPAQuotaUsedWrapper _quotaUsedDTO;
    private int _totalFree;
    private Map<Long, Integer> _freeMap;

    public EnrPAQuotaFreeWrapper(IEnrPAQuotaWrapper quotaDTO, IEnrPAQuotaUsedWrapper quotaUsedDTO, int totalFree, Map<Long, Integer> freeMap)
    {
        _quotaDTO = quotaDTO;
        _quotaUsedDTO = quotaUsedDTO;
        _totalFree = totalFree;
        _freeMap = freeMap;
    }

    // Getters

    public IEnrPAQuotaWrapper getQuota()
    {
        return _quotaDTO;
    }

    public IEnrPAQuotaUsedWrapper getQuotaUsed()
    {
        return _quotaUsedDTO;
    }

    public int getTotalFree()
    {
        return _totalFree;
    }

    public Map<Long, Integer> getFreeMap()
    {
        return _freeMap;
    }
}
