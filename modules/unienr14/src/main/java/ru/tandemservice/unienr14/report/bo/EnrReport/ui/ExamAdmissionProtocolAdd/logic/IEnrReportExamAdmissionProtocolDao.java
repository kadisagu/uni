/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamAdmissionProtocolAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamAdmissionProtocolAdd.EnrReportExamAdmissionProtocolAddUI;

/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public interface IEnrReportExamAdmissionProtocolDao extends INeedPersistenceSupport {
    long createReport(EnrReportExamAdmissionProtocolAddUI enrReportExamAdmissionProtocolAddUI);
}
