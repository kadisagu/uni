/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml;

import java.util.Map;

/**
 * @author oleyba
 * @since 4/2/15
 */
public interface IEnrXmlEnrollmentCheckDataOwner
{
    public EnrXmlEnrollmentCheckData getCheckData();
    public String getCompetitionPubBC();
    public Map getCompetitionPubParameters();
}
