/* $Id$ */
package ru.tandemservice.unienr14.order.daemon;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.dao.IUniBaseDao;

/**
 * @author azhebko
 * @since 29.08.2014
 */
public interface IEnrStudentOrderDataDaemonDao extends IUniBaseDao
{
    public static final SpringBeanCache<IEnrStudentOrderDataDaemonDao> instance = new SpringBeanCache<>(IEnrStudentOrderDataDaemonDao.class.getName());

    /**
     * Обновяляет данные о последних приказах студентов, включенных на данный момент в проведенные неотмененные приказы о зачислении или о распределении по ОП:
     * <ul>
     * <li>Приказ о зачислении - номер;</li>
     * <li>Приказ о зачислении - дата;</li>
     * <li>Приказ о зачислении - зачислен с;</li>
     * <li>Приказ о распределении по профилям/специализациям - дата;</li>
     * <li>Приказ о распределении по профилям/специализациям - номер.</li>
     * </ul>
     */
    void updateStudentOrderData();
}