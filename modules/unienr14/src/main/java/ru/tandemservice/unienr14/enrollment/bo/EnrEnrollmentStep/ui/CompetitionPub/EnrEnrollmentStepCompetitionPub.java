/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.resolver.DefaultLinkResolver;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

/**
 * @author oleyba
 * @since 12/17/13
 */
@Configuration
public class EnrEnrollmentStepCompetitionPub extends BusinessComponentManager
{
    public static final String ITEM_DS = "itemDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(ITEM_DS, competitionDSColumns(), competitionDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder(ITEM_DS)
            .addColumn(textColumn("position", EnrEnrollmentStepItemDSHandler.VIEW_PROP_RELATIVE_POSITION))
            .addColumn(actionColumn("up", new Icon("up"), "onClickUp").disabled("ui:currentItemUpDisabled")
                .permissionKey("enr14EnrollmentStepCompetitionPubPositionEdit"))
            .addColumn(actionColumn("down", new Icon("down"), "onClickDown").disabled("ui:currentItemDownDisabled")
                .permissionKey("enr14EnrollmentStepCompetitionPubPositionEdit"))
            .addColumn(textColumn("rank", EnrEnrollmentStepItemDSHandler.VIEW_PROP_RELATIVE_RANK))
            .addColumn(publisherColumn("fio", EnrEntrant.person().fullFio())
                .entityListProperty(EnrEnrollmentStepItem.entity().requestedCompetition().request().entrant().s())
                .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                    @Override public String getComponentName(IEntity entity) { return EnrEntrantPub.class.getSimpleName(); }
                    @Override public Object getParameters(IEntity entity) {
                        return new ParametersMap()
                            .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrEntrant) entity).getId())
                            .add("selectedTab", "ratingTab");
                    }
                }))
            .addColumn(textColumn(EnrEnrollmentStepItemDSHandler.VIEW_PROP_CUSTOM_STATES, EnrEnrollmentStepItemDSHandler.VIEW_PROP_CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
            .addColumn(headerColumn("competitionMarkHead")
                               .addSubColumn(textColumn("totalMark", "ui:totalMark").create())
                               .addSubColumn(textColumn("entranceExam", "ui:entranceExamSum").create())
                               .addSubColumn(textColumn("achievementMark", EnrEnrollmentStepItem.entity().achievementMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).create())
                               .visible("ui:bakSpecMag"))
            .addColumn(textColumn("eduInstitutionAvgMarkSpo", EnrEnrollmentStepItem.entity().requestedCompetition().request().eduDocument().avgMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("ui:spo"))
            .addColumn(textColumn("competitionMarkSum", EnrEnrollmentStepItem.entity().totalMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("mvel:!presenter.bakSpecMag"))
            .addColumn(textColumn("eduInstitutionAvgMarkTrInt", EnrEnrollmentStepItem.entity().requestedCompetition().request().eduDocument().avgMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("ui:trainInter"))
            .addColumn(textColumn("achievementMarkSum", EnrEnrollmentStepItem.entity().achievementMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE).visible("mvel:!presenter.bakSpecMag"))
            .addColumn(publisherColumn("otherOrders", "title")
                .entityListProperty(EnrEnrollmentStepItemDSHandler.VIEW_PROP_PREV_ORDERS)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                    @Override public String getComponentName(IEntity entity) { return entity.getId() < 0 ? null : EnrOrderPub.class.getSimpleName(); }
                }))
            .addColumn(publisherColumn("otherRecommendation", "title")
                .entityListProperty(EnrEnrollmentStepItemDSHandler.VIEW_PROP_OTHER_RECOMMENDATION)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override public Object getParameters(IEntity entity) { return ((EnrEnrollmentStepItemDSHandler.OtherRecommendationWrapper) entity).getPublisherParameters(); }
                    @Override public String getComponentName(IEntity entity) { return EnrEnrollmentStepCompetitionPub.class.getSimpleName(); }
                }))
            .addColumn(publisherColumn("otherEnrMark", "title")
                .entityListProperty(EnrEnrollmentStepItemDSHandler.VIEW_PROP_OTHER_ENR_SELECTION)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override public Object getParameters(IEntity entity) { return ((EnrEnrollmentStepItemDSHandler.OtherRecommendationWrapper) entity).getPublisherParameters(); }
                    @Override public String getComponentName(IEntity entity) { return EnrEnrollmentStepCompetitionPub.class.getSimpleName(); }
                }))
            .addColumn(textColumn("priority", EnrEnrollmentStepItem.entity().requestedCompetition().priority().s()))
            .addColumn(booleanColumn("parallel", EnrEnrollmentStepItem.entity().requestedCompetition().parallel().s()))
            // .addColumn(textColumn("ratingInfo", EnrEnrollmentStepItem.entity().)) // todo
            .addColumn(toggleColumn("includedEdit", EnrEnrollmentStepItem.included())
                .permissionKey("enr14EnrollmentStepCompetitionPubIncludeEdit")
                .toggleOnListener("onInclude").toggleOnLabel("Включить абитуриента в конкурсный список")
                .toggleOffListener("onExclude").toggleOffLabel("Исключить абитуриента из конкурсного списка")
                .visible("ui:includedEditColumnVisible")
                .create())
            .addColumn(textColumn("state", EnrEnrollmentStepItem.entity().requestedCompetition().state().stateDaemonSafe()))
            .addColumn(booleanColumn("included", EnrEnrollmentStepItem.included()).visible("ui:includedColumnVisible"))
            .addColumn(booleanColumn("originalIn", EnrEnrollmentStepItem.originalIn()))
            .addColumn(booleanColumn("accepted", EnrEnrollmentStepItem.accepted()))
            .addColumn(booleanColumn("enrollmentAvailable", EnrEnrollmentStepItem.enrollmentAvailable()))
//            .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEdit").disabled("ui:currentItemEditDisabled")
//                .permissionKey("denied_permission_key"))
            .addColumn(booleanColumn("recommended", EnrEnrollmentStepItem.recommended()).visible("ui:recommendVisible"))
            .addColumn(toggleColumn("recommendedEdit", EnrEnrollmentStepItem.recommended())
                .permissionKey("enr14EnrollmentStepCompetitionPubRecommendEdit")
                .toggleOnListener("onRecommend").toggleOnLabel("Рекомендовать абитуриента к зачислению")
                .toggleOffListener("onClearRecommend").toggleOffLabel("Снять рекомендацию абитуриента к зачислению")
                .visible("ui:recommendEditVisible")
                .create())
            .addColumn(booleanColumn("shouldBeEnrolled", EnrEnrollmentStepItem.shouldBeEnrolled()).visible("ui:shouldBeEnrolledVisible"))
            .addColumn(toggleColumn("shouldBeEnrolledEdit", EnrEnrollmentStepItem.shouldBeEnrolled())
                .permissionKey("enr14EnrollmentStepCompetitionPubShouldBeEnrolledEdit")
                .toggleOnListener("onSetShouldBeEnrolled").toggleOnLabel("Выбрать абитуриента к зачислению")
                .toggleOffListener("onClearShouldBeEnrolled").toggleOffLabel("Не выбирать абитуриента к зачислению")
                .visible("ui:shouldBeEnrolledEditVisible")
                .create())
            .addColumn(publisherColumn("thisOrder", "title")
                .entityListProperty(EnrEnrollmentStepItemDSHandler.VIEW_PROP_CURRENT_ORDER)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(DefaultLinkResolver.with()
                    .component(EnrOrderPub.class)
                    .primaryKeyAsParameter(PublisherActivator.PUBLISHER_ID_KEY)
                    .create()))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> competitionDSHandler()
    {
        return new EnrEnrollmentStepItemDSHandler(getName());
    }
}