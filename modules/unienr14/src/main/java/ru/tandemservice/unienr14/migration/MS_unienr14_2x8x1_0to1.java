package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Alexey Lopatin
 * @since 02.05.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		// Документы об образовании подтверждают ИД
        tool.executeUpdate("update enr14_camp_entrant_doc_t set achievement_p = ? where documenttype_id in (select id from enr14_c_entrant_doc_type_t where code_p = ?)", Boolean.TRUE, "4");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrPersonEduDocumentRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("enr14_person_edu_doc_rel_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrpersonedudocumentrel"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("entrant_id", DBType.LONG).setNullable(false),
				new DBColumn("edudocument_id", DBType.LONG).setNullable(false),
				new DBColumn("documenttype_id", DBType.LONG).setNullable(false),
				new DBColumn("registrationdate_p", DBType.TIMESTAMP).setNullable(false),
				new DBColumn("seria_p", DBType.createVarchar(255)),
				new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("issuancedate_p", DBType.DATE)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrPersonEduDocumentRel");
        }
    }
}