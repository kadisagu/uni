package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.competition.entity.gen.*;

/** @see ru.tandemservice.unienr14.competition.entity.gen.CustomerTargetContractTrainingGen */
public class CustomerTargetContractTraining extends CustomerTargetContractTrainingGen
{
    public CustomerTargetContractTraining(EnrProgramSetBase programSetBase, ExternalOrgUnit orgUnit)
    {
        setProgramSet(programSetBase);
        setExternalOrgUnit(orgUnit);
    }

    public CustomerTargetContractTraining(){}
}