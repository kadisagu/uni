package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Причина приказа по абитуриентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrderReasonGen extends EntityBase
 implements INaturalIdentifiable<EnrOrderReasonGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrOrderReason";
    public static final String ENTITY_NAME = "enrOrderReason";
    public static final int VERSION_HASH = -146968151;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_ORDER_TYPE = "orderType";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EnrOrderType _orderType;     // Тип приказа по абитуриентам
    private String _printTitle;     // Печатное название
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderType getOrderType()
    {
        return _orderType;
    }

    /**
     * @param orderType Тип приказа по абитуриентам. Свойство не может быть null.
     */
    public void setOrderType(EnrOrderType orderType)
    {
        dirty(_orderType, orderType);
        _orderType = orderType;
    }

    /**
     * @return Печатное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название. Свойство не может быть null.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOrderReasonGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrOrderReason)another).getCode());
            }
            setOrderType(((EnrOrderReason)another).getOrderType());
            setPrintTitle(((EnrOrderReason)another).getPrintTitle());
            setTitle(((EnrOrderReason)another).getTitle());
        }
    }

    public INaturalId<EnrOrderReasonGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrOrderReasonGen>
    {
        private static final String PROXY_NAME = "EnrOrderReasonNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrOrderReasonGen.NaturalId) ) return false;

            EnrOrderReasonGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrderReasonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrderReason.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrderReason();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "orderType":
                    return obj.getOrderType();
                case "printTitle":
                    return obj.getPrintTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "orderType":
                    obj.setOrderType((EnrOrderType) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "orderType":
                        return true;
                case "printTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "orderType":
                    return true;
                case "printTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "orderType":
                    return EnrOrderType.class;
                case "printTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrderReason> _dslPath = new Path<EnrOrderReason>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrderReason");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getOrderType()
     */
    public static EnrOrderType.Path<EnrOrderType> orderType()
    {
        return _dslPath.orderType();
    }

    /**
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrOrderReason> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EnrOrderType.Path<EnrOrderType> _orderType;
        private PropertyPath<String> _printTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrOrderReasonGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getOrderType()
     */
        public EnrOrderType.Path<EnrOrderType> orderType()
        {
            if(_orderType == null )
                _orderType = new EnrOrderType.Path<EnrOrderType>(L_ORDER_TYPE, this);
            return _orderType;
        }

    /**
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(EnrOrderReasonGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderReason#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrOrderReasonGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrOrderReason.class;
        }

        public String getEntityName()
        {
            return "enrOrderReason";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
