/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 5/5/13
 */
@Configuration
public class EnrEntrantPubExamTab extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .create();
    }
}