/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

import java.util.Calendar;
import java.util.List;

/**
 * @author oleyba
 * @since 5/9/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantRequest.id"),
    @Bind(key = EnrEntrantRequestEditUI.SEC_POSTFIX, binding = "secPostfix")
})
public class EnrEntrantRequestEditUI extends UIPresenter
{
    public static final String SEC_POSTFIX = "secPostfix";

    private EnrEntrantRequest entrantRequest = new EnrEntrantRequest();
    private PersonEduDocument oldEduDocument;
    private String secPostfix;

    private boolean enrollmentCommissionRequired = false;

    private boolean regDateDisabled = false;
    private boolean regDateVisible = true;  // todo надо ли это? в старом абитуриенте был вообще ключ в property-файле
    private int hack_for_fis_regDateMilliseconds;

    private List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs;

    @Override
    public void onComponentActivate()
    {
        setEntrantRequest(IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, getEntrantRequest().getId()));
        setOldEduDocument(getEntrantRequest().getEduDocument());
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCommissionRequired(!EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));
        hack_for_fis_regDateMilliseconds = CoreDateUtils.get(getEntrantRequest().getRegDate(), Calendar.MILLISECOND);
        setRegDateDisabled(!CoreServices.securityService().check(
            getEntrantRequest().getEntrant(), getUserContext().getPrincipalContext(),
            "editRequestRegDate_" + getSecPostfix()));

        setRequestBenefitProofDocs(EnrEntrantManager.instance().dao().getBenefitStatementProofDocs(getEntrantRequest()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_PERSON, getEntrantRequest().getEntrant().getPerson());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE, getEntrantRequest().getType());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST, getEntrantRequest().isReceiveEduLevelFirst());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEntrantRequest().getEntrant().getEnrollmentCampaign());
        dataSource.put("entrant", getEntrantRequest().getEntrant().getId());

        if (EnrEntrantRequestEdit.DS_BENEFIT_CATEGORY_PREFERENCE.equals(dataSource.getName()) || EnrEntrantRequestEdit.DS_BENEFIT_PROOF_PREFERENCE_DOC.equals(dataSource.getName()))
            dataSource.put(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE, EnrBenefitTypeCodes.PREFERENCE);

    }

    public void onClickApply()
    {
        if (!getEntrantRequest().isFiledByTrustee())
            getEntrantRequest().setTrusteeDetails(null);

        validate();

        if(ContextLocal.getErrorCollector().hasErrors())
            return;

        if (!isEduDocumentLevelSredneeObsthee())
        {
            getEntrantRequest().setSpecialSchool(false);
            getEntrantRequest().setEveningSchool(false);
        }

        // hack for fis - миллисекунды сохранятся в заявлении навсегда
        final Calendar calendar = CoreDateUtils.createCalendar(getEntrantRequest().getRegDate());
        calendar.set(Calendar.MILLISECOND, hack_for_fis_regDateMilliseconds);
        getEntrantRequest().setRegDate(calendar.getTime());

        EnrEntrantRequestManager.instance().dao().doUpdateEntrantRequest(getEntrantRequest(), getOldEduDocument(), getRequestBenefitProofDocs());
        deactivate();
    }

    private void validate()
    {
        if (getEntrantRequest().getBenefitCategory() != null && getRequestBenefitProofDocs().isEmpty())
        {
            _uiSupport.error("Выберите документы, подтверждающие особое право.", "requestBenefitProofDocs");
            return;
        }

        if(TwinComboDataSourceHandler.NO_ID.equals(getEntrantRequest().getEntrant().getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()) && !getOldEduDocument().equals(getEntrantRequest().getEduDocument()))
        {
            boolean hasErr = EnrEntrantRequestManager.instance().dao().checkOrigDocuments(getOldEduDocument(), getEntrantRequest().getEduDocument(), getEntrantRequest());

            if(hasErr)
            {
                _uiSupport.error("У приложенного или прикладываемого документа об образовании уже принят оригинал.");
                return;
            }
        }
    }

    public boolean isForeignIdentityCard()
    {
        return getEntrantRequest().getIdentityCard() != null && getEntrantRequest().getIdentityCard().getCitizenship().getCode() != 0;
    }

    public void onChangeEduDoc()
    {
        getEntrantRequest().setGeneralEduSchool(getEntrantRequest().getEduDocument() != null && getEntrantRequest().getEduDocument().getEduLevel().getHierarhyParent().getCode().equals(EduLevelCodes.OBTSHEE_OBRAZOVANIE));
    }

    public void onChangeEnrollmentCommission()
    {
        EnrEnrollmentCommissionManager.instance().dao().saveDefaultCommission(getEntrantRequest().getEnrollmentCommission());
    }

    // getters and setters

    public EnrEntrantRequest getEntrantRequest()
    {
        return entrantRequest;
    }

    public void setEntrantRequest(EnrEntrantRequest entrantRequest)
    {
        this.entrantRequest = entrantRequest;
    }

    public boolean isRegDateDisabled()
    {
        return regDateDisabled;
    }

    public void setRegDateDisabled(boolean regDateDisabled)
    {
        this.regDateDisabled = regDateDisabled;
    }

    public boolean isRegDateVisible()
    {
        return regDateVisible;
    }

    public void setRegDateVisible(boolean regDateVisible)
    {
        this.regDateVisible = regDateVisible;
    }

    public String getSecPostfix()
    {
        return secPostfix;
    }

    public void setSecPostfix(String secPostfix)
    {
        this.secPostfix = secPostfix;
    }

    public List<IEnrEntrantBenefitProofDocument> getRequestBenefitProofDocs(){ return requestBenefitProofDocs; }
    public void setRequestBenefitProofDocs(List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs){ this.requestBenefitProofDocs = requestBenefitProofDocs; }

    public boolean isRequestTypeBS(){ return getEntrantRequest().getType().getCode().equals(EnrRequestTypeCodes.BS); }

    public boolean isEduDocumentLevelSredneeObsthee(){ return getEntrantRequest().getEduDocument()!= null && getEntrantRequest().getEduDocument().getEduLevel() != null && getEntrantRequest().getEduDocument().getEduLevel().getCode().equals(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE); }

    public PersonEduDocument getOldEduDocument()
    {
        return oldEduDocument;
    }

    public void setOldEduDocument(PersonEduDocument oldEduDocument)
    {
        this.oldEduDocument = oldEduDocument;
    }

    public boolean isEnrollmentCommissionRequired()
    {
        return enrollmentCommissionRequired;
    }

    public void setEnrollmentCommissionRequired(boolean enrollmentCommissionRequired)
    {
        this.enrollmentCommissionRequired = enrollmentCommissionRequired;
    }
}