/* $Id$ */
package ru.tandemservice.unienr14.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.unienr14.catalog.entity.*;

/**
 * @author azhebko
 * @since 18.04.2014
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> dynamicCatalogsExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
                .add(StringUtils.uncapitalize(EnrRequestType.class.getSimpleName()), EnrRequestType.getUiDesc())
                .add(StringUtils.uncapitalize(EnrEntrantCustomStateType.class.getSimpleName()), EnrEntrantCustomStateType.getUiDesc())
                .add(StringUtils.uncapitalize(EnrOrderType.class.getSimpleName()), EnrOrderType.getUiDesc())
                .add(StringUtils.uncapitalize(EnrOrderBasic.class.getSimpleName()), EnrOrderBasic.getUiDesc())
                .add(StringUtils.uncapitalize(EnrOrderReason.class.getSimpleName()), EnrOrderReason.getUiDesc())
                .add(StringUtils.uncapitalize(EnrEntrantAchievementKind.class.getSimpleName()), EnrEntrantAchievementKind.getUiDesc())
                .add(StringUtils.uncapitalize(EnrEduLevelRequirement.class.getSimpleName()), EnrEduLevelRequirement.getUiDesc())
                .add(StringUtils.uncapitalize(EnrInternalExamReason.class.getSimpleName()), EnrInternalExamReason.getUiDesc())
                .add(StringUtils.uncapitalize(EnrOlympiadSubject.class.getSimpleName()), EnrOlympiadSubject.getUiDesc())
                .create();
    }
}