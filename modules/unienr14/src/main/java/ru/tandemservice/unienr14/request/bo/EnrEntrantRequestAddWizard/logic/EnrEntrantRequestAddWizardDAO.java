/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonSportAchievement;
import org.tandemframework.shared.person.catalog.entity.SportType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrPersonEduDocumentRelGen;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.EnrEntrantAchievementVO;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.EnrEntrantStateExamVO;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.PersonSportAchievementVO;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 6/9/14
 */
@Transactional
public class EnrEntrantRequestAddWizardDAO extends UniBaseDao implements IEnrEntrantRequestAddWizardDAO
{
    @Override
    public List<PersonEduDocument> getEduDocumentsFilteredByRequestType(Long personId, String requestTypeCode, boolean receiveEduLevelFirst)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonEduDocument.class, "d");
        builder.column(property("d"));
        builder.where(eq(property("d", PersonEduDocument.person().id()), value(personId)));

        DQLSelectBuilder levelDQLFilter = EnrEntrantBaseDocumentManager.getEduLevelDQLFilter(
                "el",
                property("d", PersonEduDocument.eduDocumentKind().code()),
                property("d", PersonEduDocument.eduDocumentKind().canCertifyQualificationSpec()),
                property("d", PersonEduDocument.certifyQualificationSpec()),
                requestTypeCode,
                receiveEduLevelFirst
        );
        levelDQLFilter.column(value(1));
        levelDQLFilter.where(eq(property("d", PersonEduDocument.eduLevel()), property("el")));
        builder.where(exists(levelDQLFilter.buildQuery()));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public List<EnrOnlineEntrantEduDocument> getOnlineEntrantEduDocumentsFilteredByRequestType(Long onlineEntrantId, String requestTypeCode, boolean receiveEduLevelFirst)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrOnlineEntrantEduDocument.class, "d");
        builder.where(eq(property("d", EnrOnlineEntrantEduDocument.onlineEntrant().id()), value(onlineEntrantId)));

        DQLSelectBuilder levelDQLFilter = EnrEntrantBaseDocumentManager.getEduLevelDQLFilter(
                "el",
                property("d", EnrOnlineEntrantEduDocument.docKind().code()),
                property("d", EnrOnlineEntrantEduDocument.docKind().canCertifyQualificationSpec()),
                null,
                requestTypeCode,
                receiveEduLevelFirst
        );
        levelDQLFilter.column(value(1));
        levelDQLFilter.where(eq(property("d", EnrOnlineEntrantEduDocument.eduLevel()), property("el")));
        builder.where(exists(levelDQLFilter.buildQuery()));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public List<IdentityCard> getIdentityCardsForPerson(Long personId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(IdentityCard.class, "ic");
        builder.where(eq(property("ic", IdentityCard.person().id()), value(personId)));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public boolean eduDocAppliedForRequest(Long eduDocumentId)
    {
        Object ret = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "r")
                .top(1).column("r.id")
                .where(eq(property("r", EnrEntrantRequest.eduDocument().id()), value(eduDocumentId)))
                .createStatement(getSession())
                .uniqueResult();

        return ret != null;
    }

    @Override
    public List<EnrEntrantStateExamVO> prepareStateExamList(EnrEntrantWizardState state)
    {
        List<EnrEntrantStateExamVO> stateExamValues = Lists.newArrayList();
        Session session = getSession();

        EnrEntrant enrEntrant = get(state.getEntrantId());

        // Существующие в наборах ви конкурсов выбранной приемной кампании
        DQLSelectBuilder stateExamSubjectsBuilder = new DQLSelectBuilder().fromEntity(EnrExamVariantPassForm.class, "pf");
        stateExamSubjectsBuilder.joinEntity("pf", DQLJoinType.inner, EnrExamVariant.class, "ev", eq(property("pf", EnrExamVariantPassForm.examVariant().id()), property("ev", EnrExamVariant.id())));
        stateExamSubjectsBuilder.joinEntity("ev", DQLJoinType.inner, EnrExamSetElement.class, "el", eq(property("ev", EnrExamVariant.examSetElement().id()), property("el", EnrExamSetElement.id())));

        stateExamSubjectsBuilder.joinEntity("el", DQLJoinType.left, EnrCampaignDiscipline.class, "d", eq(property("el", EnrExamSetElement.value().id()), property("d", EnrCampaignDiscipline.id())));
        stateExamSubjectsBuilder.joinEntity("d", DQLJoinType.left, EnrStateExamSubject.class, "dse", eq(property("d", EnrCampaignDiscipline.stateExamSubject().id()), property("dse", EnrStateExamSubject.id())));

        stateExamSubjectsBuilder.joinEntity("el", DQLJoinType.left, EnrCampaignDisciplineGroup.class, "g", eq(property("el", EnrExamSetElement.value().id()), property("g", EnrCampaignDisciplineGroup.id())));
        stateExamSubjectsBuilder.joinEntity("g", DQLJoinType.left, EnrCampaignDisciplineGroupElement.class, "ge", eq(property("g", EnrCampaignDisciplineGroup.id()), property("ge", EnrCampaignDisciplineGroupElement.group().id())));
        stateExamSubjectsBuilder.joinEntity("ge", DQLJoinType.left, EnrCampaignDiscipline.class, "ged", eq(property("ge", EnrCampaignDisciplineGroupElement.discipline().id()), property("ged", EnrCampaignDiscipline.id())));
        stateExamSubjectsBuilder.joinEntity("ged", DQLJoinType.left, EnrStateExamSubject.class, "dsg", eq(property("ged", EnrCampaignDiscipline.stateExamSubject().id()), property("dsg", EnrStateExamSubject.id())));

        stateExamSubjectsBuilder.column("dse");
        stateExamSubjectsBuilder.column("dsg");
        stateExamSubjectsBuilder.where(eq(property("pf", EnrExamVariantPassForm.examVariant().examSetVariant().examSet().enrollmentCampaign().id()), value(enrEntrant.getEnrollmentCampaign().getId())));
        stateExamSubjectsBuilder.where(eq(property("pf", EnrExamVariantPassForm.passForm().code()), value(EnrExamPassFormCodes.STATE_EXAM)));

        List<Object[]> result = stateExamSubjectsBuilder.createStatement(session).list();

        Set<EnrStateExamSubject> stateExamSubjects = Sets.newHashSet();
        for(Object[] res : result)
        {
            if(res[0] != null)
                stateExamSubjects.add((EnrStateExamSubject) res[0]);
            if(res[1] != null)
                stateExamSubjects.add((EnrStateExamSubject) res[1]);
        }

        // результаты абитуриента
        DQLSelectBuilder stateExamResultsBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantStateExamResult.class, "er");
        stateExamResultsBuilder.where(eq(property("er", EnrEntrantStateExamResult.entrant().id()), value(enrEntrant.getId())));

        List<EnrEntrantStateExamResult> stateExamResults = stateExamResultsBuilder.createStatement(session).list();
        Set<EnrStateExamSubject> existedSubjects = Sets.newHashSet();
        for(EnrEntrantStateExamResult stateExamResult : stateExamResults)
        {
            if(stateExamSubjects.contains(stateExamResult.getSubject())) {
                boolean createdInWizard = state.getCreatedObjectIds().contains(stateExamResult.getId());
                stateExamValues.add(new EnrEntrantStateExamVO(createdInWizard, stateExamResult.getSubject(), stateExamResult, createdInWizard));
                existedSubjects.add(stateExamResult.getSubject());
            }
        }

        // данные онлайн-регистрации
        if(state.getOnlineEntrantId() != null)
        {
            DQLSelectBuilder onlineEntrantStateExamSubjectsBuilder = new DQLSelectBuilder().fromEntity(EnrOnlineEntrantStateExam.class, "se");
            onlineEntrantStateExamSubjectsBuilder.column(property("se", EnrOnlineEntrantStateExam.stateExamSubject()));
            onlineEntrantStateExamSubjectsBuilder.where(eq(property("se", EnrOnlineEntrantStateExam.onlineEntrant().id()), value(state.getOnlineEntrantId())));

            List<EnrStateExamSubject> onlineEntrantStateExamSubjects = onlineEntrantStateExamSubjectsBuilder.createStatement(session).list();
            for(EnrStateExamSubject stateExamSubject : onlineEntrantStateExamSubjects)
            {
                if(!existedSubjects.contains(stateExamSubject) && stateExamSubjects.contains(stateExamSubject))
                {
                    EnrEntrantStateExamResult stateExamResult = new EnrEntrantStateExamResult();
                    stateExamResult.setSubject(stateExamSubject);
                    stateExamResult.setEntrant(enrEntrant);
                    stateExamResult.setYear(enrEntrant.getEnrollmentCampaign().getEducationYear().getIntValue());
                    stateExamValues.add(new EnrEntrantStateExamVO(true, stateExamSubject, stateExamResult, true));
                    existedSubjects.add(stateExamSubject);
                }
            }
        }

        // Существующие в наборах ви конкурсов выбранной приемной кампании
        for(EnrStateExamSubject stateExamSubject : stateExamSubjects)
        {
            if(!existedSubjects.contains(stateExamSubject))
            {
                EnrEntrantStateExamResult stateExamResult = new EnrEntrantStateExamResult();
                stateExamResult.setSubject(stateExamSubject);
                stateExamResult.setEntrant(enrEntrant);
                stateExamResult.setYear(enrEntrant.getEnrollmentCampaign().getEducationYear().getIntValue());
                stateExamValues.add(new EnrEntrantStateExamVO(false, stateExamSubject, stateExamResult, true));
            }
        }

        Collections.sort(stateExamValues, EnrEntrantStateExamVO.COMPARATOR);

        return stateExamValues;
    }

    @Override
    public void saveStateExamList(EnrEntrantWizardState state, List<EnrEntrantStateExamVO> exams)
    {
        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, state.getEnrollmentCampaignId());
        for (EnrEntrantStateExamVO stateExamVO : exams) {
            if (!stateExamVO.isCreatedInCurrentWizard()) continue;
            if (!stateExamVO.isSelected()) continue;
            stateExamVO.getResult().setAccepted(enrollmentCampaign.getSettings().isStateExamAccepted());
            stateExamVO.getResult().setVerifiedByUser(false);
            saveOrUpdate(stateExamVO.getResult());
            state.getCreatedObjectIds().add(stateExamVO.getResult().getId());
        }
    }

    @Override
    public List<EnrEntrantAchievementVO> prepareIndividualAchievementsList(EnrEntrantWizardState state)
    {
        List<EnrEntrantAchievementVO> individualAchievements = Lists.newArrayList();
        Session session = getSession();

        EnrEntrant enrEntrant = get(state.getEntrantId());

        // Существующие индивидуальные достижения
        DQLSelectBuilder individualAchievementsBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "a");
        individualAchievementsBuilder.where(eq(property("a", EnrEntrantAchievement.entrant().id()), value(enrEntrant.getId())));
        individualAchievementsBuilder.where(eq(property("a", EnrEntrantAchievement.type().achievementKind().requestType().id()), value(state.getRequestTypeId())));

        Set<Long> typeIds = Sets.newHashSet();

        for(EnrEntrantAchievement achievement : individualAchievementsBuilder.createStatement(session).<EnrEntrantAchievement>list())
        {
            typeIds.add(achievement.getType().getId());
            boolean createdInWizard = state.getCreatedObjectIds().contains(achievement.getId());
            EnrEntrantDocumentViewWrapper documentWrapper = achievement.getDocument() == null ? null : EnrEntrantDocumentViewWrapper.wrap(achievement.getDocument());
            individualAchievements.add(new EnrEntrantAchievementVO(achievement, documentWrapper, createdInWizard));
        }

        // достижения онлайн-абитуриента
        if(state.getOnlineEntrantId() != null)
        {
            DQLSelectBuilder onlineEntrantAchievementsBuilder = new DQLSelectBuilder().fromEntity(EnrOnlineEntrantAchievement.class, "oa");
            onlineEntrantAchievementsBuilder.where(eq(property("oa", EnrOnlineEntrantAchievement.onlineEntrant().id()), value(state.getOnlineEntrantId())));
            onlineEntrantAchievementsBuilder.where(eq(property("oa", EnrOnlineEntrantAchievement.achievementType().achievementKind().requestType().id()), value(state.getRequestTypeId())));

            for(EnrOnlineEntrantAchievement onlineEntrantAchievement : onlineEntrantAchievementsBuilder.createStatement(session).<EnrOnlineEntrantAchievement>list())
            {
                if(!typeIds.contains(onlineEntrantAchievement.getAchievementType().getId()))
                {
                    EnrEntrantAchievement achievement = new EnrEntrantAchievement();
                    achievement.setEntrant(enrEntrant);
                    achievement.setType(onlineEntrantAchievement.getAchievementType());
                    individualAchievements.add(new EnrEntrantAchievementVO(achievement, null, true));
                }
            }
        }

        Collections.sort(individualAchievements, EnrEntrantAchievementVO.COMPARATOR);
        return individualAchievements;
    }

    @Override
    public void saveIndividualAchievementsList(EnrEntrantWizardState state, List<EnrEntrantAchievementVO> achievements)
    {
        for (EnrEntrantAchievementVO achievementVO : achievements)
        {
            if (!achievementVO.isCreatedInCurrentWizard()) continue;

            EnrEntrantAchievement achievement = achievementVO.getAchievement();
            EnrEntrantDocumentViewWrapper documentWrapper = achievementVO.getDocumentWrapper();

            if(documentWrapper != null)
            {
                IEntity entity = documentWrapper.getEntity();
                if (entity instanceof PersonEduDocument)
                {
                    EnrEntrant entrant = achievement.getEntrant();
                    PersonEduDocument eduDocument = (PersonEduDocument) entity;
                    EnrPersonEduDocumentRel rel = getByNaturalId(new EnrPersonEduDocumentRelGen.NaturalId(entrant, eduDocument));
                    if (null == rel)
                        saveOrUpdate(rel = new EnrPersonEduDocumentRel(entrant, eduDocument));
                    achievement.setDocument(rel);
                } else
                    achievement.setDocument((IEnrEntrantAchievementProofDocument) entity);
            }
            else
            {
                achievement.setDocument(null);
            }

            saveOrUpdate(achievement);
            state.getCreatedObjectIds().add(achievement.getId());
        }
    }

    @Override
    public List<PersonSportAchievementVO> prepareSportAchievementsList(EnrEntrantWizardState state)
    {
        List<PersonSportAchievementVO> sportAchievements = Lists.newArrayList();
        Session session = getSession();

        // Существующие индивидуальные достижения
        DQLSelectBuilder sportAchievementsBuilder = new DQLSelectBuilder().fromEntity(PersonSportAchievement.class, "a");
        sportAchievementsBuilder.where(eq(property("a", PersonSportAchievement.person().id()), value(state.getPersonId())));

        for(PersonSportAchievement achievement : sportAchievementsBuilder.createStatement(session).<PersonSportAchievement>list())
        {
            boolean createdInWizard = state.getCreatedObjectIds().contains(achievement.getId());
            sportAchievements.add(new PersonSportAchievementVO(achievement, createdInWizard));
        }

        Collections.sort(sportAchievements, PersonSportAchievementVO.COMPARATOR);
        return sportAchievements;
    }

    @Override
    public void saveSportAchievementsList(EnrEntrantWizardState state, List<PersonSportAchievementVO> achievements)
    {
        for (PersonSportAchievementVO achievementVO : achievements) {
            saveOrUpdate(achievementVO.getSportAchievement());
            if (!achievementVO.isCreatedInCurrentWizard()) continue;
            state.getCreatedObjectIds().add(achievementVO.getSportAchievement().getId());
        }
    }

    @Override
    public List<SportType> listSportKinds(Long personId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SportType.class, "st");
        DQLSelectBuilder extisting = new DQLSelectBuilder().fromEntity(PersonSportAchievement.class, "psa");
        extisting.where(eq(property("psa", PersonSportAchievement.person().id()), value(personId)));
        extisting.column(property("psa", PersonSportAchievement.sportKind().id()));
        builder.where(notIn(property("st", SportType.id()), extisting.buildQuery()));
        builder.order(property("st", SportType.title()));

        return builder.createStatement(getSession()).list();
    }
}
