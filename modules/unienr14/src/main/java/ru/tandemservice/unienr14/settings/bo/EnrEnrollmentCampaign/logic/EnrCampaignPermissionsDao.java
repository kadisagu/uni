/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collections;
import java.util.List;

/**
 * @author nvankov
 * @since 3/18/16.
 */
public class EnrCampaignPermissionsDao extends UniBaseDao implements IEnrCampaignPermissionsDao
{
    @Override
    public List<Long> getEnrollmentCampaignIds(IPrincipalContext principalContext)
    {
        if(principalContext == null) return Collections.emptyList();
        
        return Lists.newArrayList(CollectionUtils.collect(getEnrollmentCampaignList(principalContext), EnrEnrollmentCampaign::getId));
    }

    @Override
    public List<EnrEnrollmentCampaign> getEnrollmentCampaignList(IPrincipalContext principalContext)
    {
        if(principalContext == null) return Collections.emptyList();

        return Lists.newArrayList(CollectionUtils.select(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getEnrollmentCampaignList(),
                enrEnrollmentCampaign -> hasPermissionForEnrCampaign(enrEnrollmentCampaign, principalContext)
        ));
    }

    @Override
    public ISelectModel getEnrollmentCampaignModel()
    {
        final LazySimpleSelectModel<EnrEnrollmentCampaign> model = new LazySimpleSelectModel<>(getEnrollmentCampaignList(UserContext.getInstance().getPrincipalContext()));
        model.setSearchFromStart(false);
        return model;
    }

    @Override
    public boolean hasPermissionForEnrCampaign(EnrEnrollmentCampaign enrollmentCampaign, IPrincipalContext principalContext)
    {
        return CoreServices.securityService().check(enrollmentCampaign, principalContext, "enr14EnrollmentCampaignSelect");

    }
}
