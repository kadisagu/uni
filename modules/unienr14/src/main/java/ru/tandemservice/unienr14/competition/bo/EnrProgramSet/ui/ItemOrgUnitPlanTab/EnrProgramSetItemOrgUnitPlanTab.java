/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab.logic.ProgramSetItemDSHandler;

/**
 * @author azhebko
 * @since 01.08.2014
 */
@Configuration
public class EnrProgramSetItemOrgUnitPlanTab extends BusinessComponentManager
{
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .create();
    }

    @Bean
    public ColumnListExtPoint programSetItemDSColumns()
    {
        return columnListExtPointBuilder("programSetItemDS")
            .addColumn(textColumn("title", "title"))
            .addColumn(textColumn("plan", "plan"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler programSetItemDSHandler()
    {
        return new ProgramSetItemDSHandler(this.getName());
    }

}