package ru.tandemservice.unienr14.exams.entity;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.util.ToString;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.entity.gen.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Набор вступительных испытаний (ВИ)
 */
public class EnrExamSet extends EnrExamSetGen
{
    public static Comparator<EnrExamSet> getComparator(final Map<EnrExamSet, List<EnrExamSetElement>> examSet2ElementsMap)
    {
        return new Comparator<EnrExamSet>()
        {
            @Override
            public int compare(EnrExamSet o1, EnrExamSet o2)
            {
                ToString<EnrExamSetElement> toString = new ToString<EnrExamSetElement>()
                {
                    @Override
                    public String toString(EnrExamSetElement value)
                    {
                        return value.getValue().getTitle();
                    }
                };

                String title1 = CoreStringUtils.join(examSet2ElementsMap.get(o1), toString, "");
                String title2 = CoreStringUtils.join(examSet2ElementsMap.get(o2), toString, "");

                return title1.compareTo(title2);
            }
        };
    }

    /**
     * Отображаем все элементы ВИ из набора друг под другом (в порядке номеров элементов ВИ) в формате:
     * @return [название дисциплины] [[сокр. название типа ВИ], П]
     */
    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=14356844")
    public String getElementsTitle() {
        return EnrExamSetManager.instance().examSetElementTitle(getElementList(), "\n");
    }


    public List<EnrExamSetElement> getElementList() {
        final List<EnrExamSetElement> list = EnrExamSetManager.instance().dao().examSet2ElementsMap(this).get(this);
        return (null != list ? list : new ArrayList<EnrExamSetElement>(0));
    }

    public boolean isEmptySet() {
        return getKey().isEmpty();
    }
}