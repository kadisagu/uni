/* $Id: Model.java 32254 2014-02-04 09:18:39Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Add;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.List;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
@Input(
        {
                @Bind(key = EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID, binding = EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID, required = true)
        }
)
@Output( {
    @Bind(key = "similarPersons"),
    @Bind(key = "personString"),
    @Bind(key="infoString"),
})
public class Model extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Model<EnrEntrant>
{
    private EnrEntrant entrant = new EnrEntrant();
    private Long enrollmentCampaignId;

    public IdentityCard getIdentityCard()
    {
        return getPerson().getIdentityCard();
    }

    public IdentityCardType getIdentityCardType()
    {
        return getIdentityCard().getCardType();
    }

    @Override
    protected EnrEntrant getNewInstance()
    {
        return getEntrant();
    }

    @Override
    public String getPersonString()
    {
        return "Добавление абитуриента: " + super.getPersonString();
    }

    @Override
    public String getInfoString()
    {
        return "В процессе добавления абитуриента найдены персоны с похожими личными данными. Вы можете добавить абитуриента на основе одной из существующих персон, или создать новую.<br/>Для того, чтобы добавить абитуриента на основе одной из существующих персон, выберите её в списке.";
    }

    @Override
    protected String getNewPersonButtonName()
    {
        return "Добавить абитуриента, создав новую персону";
    }

    @Override
    protected String getOnBasisPersonButtonName()
    {
        return "Добавить абитуриента на основе существующей персоны";
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public Long getEnrollmentCampaignId()
    {
        return enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        this.enrollmentCampaignId = enrollmentCampaignId;
    }
}