/* $Id$ */
package ru.tandemservice.unienr14.ws;

import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.interceptor.Fault;

/**
 * @author nvankov
 * @since 6/15/15
 */
public class ServerSoapFaultException extends SoapFault
{
    public static final String TANDEM_UNIVERSITY_ERROR_SYSTEM = "01";

    public ServerSoapFaultException(String errorCode, String message)
    {
        super("TU error code: "+ errorCode +".\nDetails:\n" + message, Fault.FAULT_CODE_SERVER);
    }
}
