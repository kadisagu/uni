package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Апелляция на результат ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamPassAppealGen extends EntityBase
 implements INaturalIdentifiable<EnrExamPassAppealGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal";
    public static final String ENTITY_NAME = "enrExamPassAppeal";
    public static final int VERSION_HASH = -1774014145;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_PASS_DISCIPLINE = "examPassDiscipline";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_MARK_AS_LONG = "markAsLong";
    public static final String P_APPEAL_DATE = "appealDate";
    public static final String P_DECISION_DATE = "decisionDate";
    public static final String P_DECISION_TEXT = "decisionText";
    public static final String P_MARK_AS_DOUBLE = "markAsDouble";
    public static final String P_MARK_AS_STRING = "markAsString";

    private EnrExamPassDiscipline _examPassDiscipline;     // ДДС
    private Date _modificationDate;     // Дата изменения
    private long _markAsLong;     // Балл
    private Date _appealDate;     // Дата подачи апелляции
    private Date _decisionDate;     // Дата решения апелляционной комиссии
    private String _decisionText;     // Решение апелляционной комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ДДС. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    /**
     * @param examPassDiscipline ДДС. Свойство не может быть null и должно быть уникальным.
     */
    public void setExamPassDiscipline(EnrExamPassDiscipline examPassDiscipline)
    {
        dirty(_examPassDiscipline, examPassDiscipline);
        _examPassDiscipline = examPassDiscipline;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public long getMarkAsLong()
    {
        return _markAsLong;
    }

    /**
     * @param markAsLong Балл. Свойство не может быть null.
     */
    public void setMarkAsLong(long markAsLong)
    {
        dirty(_markAsLong, markAsLong);
        _markAsLong = markAsLong;
    }

    /**
     * @return Дата подачи апелляции. Свойство не может быть null.
     */
    @NotNull
    public Date getAppealDate()
    {
        return _appealDate;
    }

    /**
     * @param appealDate Дата подачи апелляции. Свойство не может быть null.
     */
    public void setAppealDate(Date appealDate)
    {
        dirty(_appealDate, appealDate);
        _appealDate = appealDate;
    }

    /**
     * @return Дата решения апелляционной комиссии. Свойство не может быть null.
     */
    @NotNull
    public Date getDecisionDate()
    {
        return _decisionDate;
    }

    /**
     * @param decisionDate Дата решения апелляционной комиссии. Свойство не может быть null.
     */
    public void setDecisionDate(Date decisionDate)
    {
        dirty(_decisionDate, decisionDate);
        _decisionDate = decisionDate;
    }

    /**
     * @return Решение апелляционной комиссии.
     */
    public String getDecisionText()
    {
        initLazyForGet("decisionText");
        return _decisionText;
    }

    /**
     * @param decisionText Решение апелляционной комиссии.
     */
    public void setDecisionText(String decisionText)
    {
        initLazyForSet("decisionText");
        dirty(_decisionText, decisionText);
        _decisionText = decisionText;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamPassAppealGen)
        {
            if (withNaturalIdProperties)
            {
                setExamPassDiscipline(((EnrExamPassAppeal)another).getExamPassDiscipline());
            }
            setModificationDate(((EnrExamPassAppeal)another).getModificationDate());
            setMarkAsLong(((EnrExamPassAppeal)another).getMarkAsLong());
            setAppealDate(((EnrExamPassAppeal)another).getAppealDate());
            setDecisionDate(((EnrExamPassAppeal)another).getDecisionDate());
            setDecisionText(((EnrExamPassAppeal)another).getDecisionText());
        }
    }

    public INaturalId<EnrExamPassAppealGen> getNaturalId()
    {
        return new NaturalId(getExamPassDiscipline());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamPassAppealGen>
    {
        private static final String PROXY_NAME = "EnrExamPassAppealNaturalProxy";

        private Long _examPassDiscipline;

        public NaturalId()
        {}

        public NaturalId(EnrExamPassDiscipline examPassDiscipline)
        {
            _examPassDiscipline = ((IEntity) examPassDiscipline).getId();
        }

        public Long getExamPassDiscipline()
        {
            return _examPassDiscipline;
        }

        public void setExamPassDiscipline(Long examPassDiscipline)
        {
            _examPassDiscipline = examPassDiscipline;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamPassAppealGen.NaturalId) ) return false;

            EnrExamPassAppealGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamPassDiscipline(), that.getExamPassDiscipline()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamPassDiscipline());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamPassDiscipline());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamPassAppealGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamPassAppeal.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamPassAppeal();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examPassDiscipline":
                    return obj.getExamPassDiscipline();
                case "modificationDate":
                    return obj.getModificationDate();
                case "markAsLong":
                    return obj.getMarkAsLong();
                case "appealDate":
                    return obj.getAppealDate();
                case "decisionDate":
                    return obj.getDecisionDate();
                case "decisionText":
                    return obj.getDecisionText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examPassDiscipline":
                    obj.setExamPassDiscipline((EnrExamPassDiscipline) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "markAsLong":
                    obj.setMarkAsLong((Long) value);
                    return;
                case "appealDate":
                    obj.setAppealDate((Date) value);
                    return;
                case "decisionDate":
                    obj.setDecisionDate((Date) value);
                    return;
                case "decisionText":
                    obj.setDecisionText((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examPassDiscipline":
                        return true;
                case "modificationDate":
                        return true;
                case "markAsLong":
                        return true;
                case "appealDate":
                        return true;
                case "decisionDate":
                        return true;
                case "decisionText":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examPassDiscipline":
                    return true;
                case "modificationDate":
                    return true;
                case "markAsLong":
                    return true;
                case "appealDate":
                    return true;
                case "decisionDate":
                    return true;
                case "decisionText":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examPassDiscipline":
                    return EnrExamPassDiscipline.class;
                case "modificationDate":
                    return Date.class;
                case "markAsLong":
                    return Long.class;
                case "appealDate":
                    return Date.class;
                case "decisionDate":
                    return Date.class;
                case "decisionText":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamPassAppeal> _dslPath = new Path<EnrExamPassAppeal>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamPassAppeal");
    }
            

    /**
     * @return ДДС. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getExamPassDiscipline()
     */
    public static EnrExamPassDiscipline.Path<EnrExamPassDiscipline> examPassDiscipline()
    {
        return _dslPath.examPassDiscipline();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getMarkAsLong()
     */
    public static PropertyPath<Long> markAsLong()
    {
        return _dslPath.markAsLong();
    }

    /**
     * @return Дата подачи апелляции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getAppealDate()
     */
    public static PropertyPath<Date> appealDate()
    {
        return _dslPath.appealDate();
    }

    /**
     * @return Дата решения апелляционной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getDecisionDate()
     */
    public static PropertyPath<Date> decisionDate()
    {
        return _dslPath.decisionDate();
    }

    /**
     * @return Решение апелляционной комиссии.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getDecisionText()
     */
    public static PropertyPath<String> decisionText()
    {
        return _dslPath.decisionText();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getMarkAsDouble()
     */
    public static SupportedPropertyPath<Double> markAsDouble()
    {
        return _dslPath.markAsDouble();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getMarkAsString()
     */
    public static SupportedPropertyPath<String> markAsString()
    {
        return _dslPath.markAsString();
    }

    public static class Path<E extends EnrExamPassAppeal> extends EntityPath<E>
    {
        private EnrExamPassDiscipline.Path<EnrExamPassDiscipline> _examPassDiscipline;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Long> _markAsLong;
        private PropertyPath<Date> _appealDate;
        private PropertyPath<Date> _decisionDate;
        private PropertyPath<String> _decisionText;
        private SupportedPropertyPath<Double> _markAsDouble;
        private SupportedPropertyPath<String> _markAsString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ДДС. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getExamPassDiscipline()
     */
        public EnrExamPassDiscipline.Path<EnrExamPassDiscipline> examPassDiscipline()
        {
            if(_examPassDiscipline == null )
                _examPassDiscipline = new EnrExamPassDiscipline.Path<EnrExamPassDiscipline>(L_EXAM_PASS_DISCIPLINE, this);
            return _examPassDiscipline;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EnrExamPassAppealGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getMarkAsLong()
     */
        public PropertyPath<Long> markAsLong()
        {
            if(_markAsLong == null )
                _markAsLong = new PropertyPath<Long>(EnrExamPassAppealGen.P_MARK_AS_LONG, this);
            return _markAsLong;
        }

    /**
     * @return Дата подачи апелляции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getAppealDate()
     */
        public PropertyPath<Date> appealDate()
        {
            if(_appealDate == null )
                _appealDate = new PropertyPath<Date>(EnrExamPassAppealGen.P_APPEAL_DATE, this);
            return _appealDate;
        }

    /**
     * @return Дата решения апелляционной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getDecisionDate()
     */
        public PropertyPath<Date> decisionDate()
        {
            if(_decisionDate == null )
                _decisionDate = new PropertyPath<Date>(EnrExamPassAppealGen.P_DECISION_DATE, this);
            return _decisionDate;
        }

    /**
     * @return Решение апелляционной комиссии.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getDecisionText()
     */
        public PropertyPath<String> decisionText()
        {
            if(_decisionText == null )
                _decisionText = new PropertyPath<String>(EnrExamPassAppealGen.P_DECISION_TEXT, this);
            return _decisionText;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getMarkAsDouble()
     */
        public SupportedPropertyPath<Double> markAsDouble()
        {
            if(_markAsDouble == null )
                _markAsDouble = new SupportedPropertyPath<Double>(EnrExamPassAppealGen.P_MARK_AS_DOUBLE, this);
            return _markAsDouble;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal#getMarkAsString()
     */
        public SupportedPropertyPath<String> markAsString()
        {
            if(_markAsString == null )
                _markAsString = new SupportedPropertyPath<String>(EnrExamPassAppealGen.P_MARK_AS_STRING, this);
            return _markAsString;
        }

        public Class getEntityClass()
        {
            return EnrExamPassAppeal.class;
        }

        public String getEntityName()
        {
            return "enrExamPassAppeal";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getMarkAsDouble();

    public abstract String getMarkAsString();
}
