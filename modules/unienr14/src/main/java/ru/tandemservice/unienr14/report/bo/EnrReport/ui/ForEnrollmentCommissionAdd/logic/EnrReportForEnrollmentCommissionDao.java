/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.logic;

import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.EnrReportForEnrollmentCommissionAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.ExcelReportUtils;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportForEnrollmentCommission;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 06.06.2014
 */
public class EnrReportForEnrollmentCommissionDao extends UniBaseDao implements IEnrReportForEnrollmentCommissionDao
{
    public static String[] STATIC_HEADER_CONTENT = new String[]
            {
                    "№ п/п",
                    "ФИО",
                    "Серия и номер УЛ",
                    "Отборочная комиссия",
                    "Вид заявления",
                    "Вид возмещения затрат",
                    "Форма обучения",
                    "Вид приема",
                    "Филиал",
                    "Формирующее подр.",
                    "Направление, специальность, профессия",
                    "Набор ОП",
                    "Целевой прием",
                    "Вид целевого приема",
                    "Поступает на параллельное освоение",
                    "Особое право",
                    "Сдан оригинал",
                    "Согласие на зачисление",
                    "Итоговое согласие",
                    "Приоритет",
                    "Сумма баллов"
            };

    public Long createReport(EnrReportForEnrollmentCommissionAddUI model)
    {
        EnrReportForEnrollmentCommission report = new EnrReportForEnrollmentCommission();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setEnrollmentStage(model.getStageSelector().getStage().getTitle());
        if (model.isFilterByEnrCommission())
            report.setEnrollmentComission(UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; "));

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportForEnrollmentCommission.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportForEnrollmentCommission.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportForEnrollmentCommission.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportForEnrollmentCommission.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportForEnrollmentCommission.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportForEnrollmentCommission.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportForEnrollmentCommission.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportForEnrollmentCommission.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportForEnrollmentCommission.P_PROGRAM_SET, "title");
        if (model.isParallelActive())
            report.setParallel(model.getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        try {
            content.setContent(buildReport(model));
        } catch (JXLException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        } catch (IOException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }
        content.setFilename("EnrReportForEnrollmentCommission.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }

    // Для переопределения в проекте
    protected String getEnrollmentCommissionTitle(EnrRequestedCompetition requestedCompetition)
    {
        return requestedCompetition.getRequest().getEnrollmentCommission()!= null ? requestedCompetition.getRequest().getEnrollmentCommission().getTitle() : "";
    }

    protected EnrEntrantDQLSelectBuilder createReqCompBuilder(EnrReportForEnrollmentCommissionAddUI model)
    {
        EnrEnrollmentCampaign campaign = model.getEnrollmentCampaign();

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(campaign);
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isFilterByEnrCommission()) {
            EnrEnrollmentCommissionManager.instance().dao().filterRequestedCompetitionsByEnrCommission(requestedCompDQL, requestedCompDQL.reqComp(), model.getEnrollmentCommissionList());
        }

        if (model.isParallelActive())
        {
            if (model.isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, requestedCompDQL.reqComp());
        //requestedCompDQL.where(le(property("reqComp", EnrRequestedCompetition.state().priority()), value(model.getStageSelector().getStage().getId())));

        return requestedCompDQL;
    }

    private byte[] buildReport(EnrReportForEnrollmentCommissionAddUI model) throws JXLException, IOException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);
        WritableSheet sheet = workbook.createSheet("ReportSheet", 0);

        int indexOfHeader = printHeader(model, sheet);

        EnrEntrantDQLSelectBuilder requestedCompDQL = createReqCompBuilder(model);

        // Формируем лист с выбранными конкурсами
        requestedCompDQL.column(property(requestedCompDQL.reqComp()));

        List<EnrRequestedCompetition> reqCompList = getList(requestedCompDQL);

        // Карта, связывающая один выбранный конкурс с абитуриентом. Берется конкурс с высшим приоритетом.
        Map<Person, EnrRequestedCompetition> highPriorityAttachMap = new HashMap<>();

        for (EnrRequestedCompetition reqComp : reqCompList)
        {
            Person person = reqComp.getRequest().getIdentityCard().getPerson();
            if (highPriorityAttachMap.containsKey(person))
            {
                if (highPriorityAttachMap.get(person).getPriority() > reqComp.getPriority())
                    highPriorityAttachMap.put(person, reqComp);
            }
            else highPriorityAttachMap.put(person, reqComp);
        }

        // Компаратор выбранных конкурсов
        Set<EnrRequestedCompetition> dataSet = new HashSet<>();

        for (Map.Entry<Person, EnrRequestedCompetition> entry : highPriorityAttachMap.entrySet())
            dataSet.add(entry.getValue());


        // Присоединяем ВВИ
        requestedCompDQL
                .joinEntity(requestedCompDQL.reqComp(), DQLJoinType.inner, EnrChosenEntranceExam.class, "chosenExam", eq(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.id()), property("chosenExam", EnrChosenEntranceExam.requestedCompetition().id())) )
                .where(in(property("chosenExam", EnrChosenEntranceExam.requestedCompetition()), dataSet))
                .resetColumns();

        requestedCompDQL
                .column(property("chosenExam"));

        // Лист с ВВИ
        List<EnrChosenEntranceExam> chosenExams = getList(requestedCompDQL);

        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> vviMap = SafeMap.get(HashSet.class);
        Map<EnrCampaignDiscipline, Set<EnrChosenEntranceExam>> disciplineToVviMap = SafeMap.get(HashSet.class);

        Set<EnrCampaignDiscipline> disciplines = new TreeSet<>(new Comparator<EnrCampaignDiscipline>() {
            @Override
            public int compare(EnrCampaignDiscipline o1, EnrCampaignDiscipline o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });

        for (EnrChosenEntranceExam exam : chosenExams)
        {
            vviMap.get(exam.getRequestedCompetition()).add(exam);

            disciplines.add(exam.getDiscipline());

            disciplineToVviMap.get(exam.getDiscipline()).add(exam);
        }

        ICommonFilterItem requestTypeFilterItem = model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE.getSettingsName());

        EnrRequestType requestType =  requestTypeFilterItem != null && requestTypeFilterItem.getValue() != null ? (EnrRequestType) requestTypeFilterItem.getValue() : null;

        boolean printEduDocumentAvgMark = requestType != null && (EnrRequestTypeCodes.INTERNSHIP.equals(requestType.getCode())
                || EnrRequestTypeCodes.TRAINEESHIP.equals(requestType.getCode()) || EnrRequestTypeCodes.HIGHER.equals(requestType.getCode()));

        // Добавляем дисциплины в хедэр отчета
        printDisciplines(sheet, disciplines, indexOfHeader, printEduDocumentAvgMark);

        WritableFont contentFont = new WritableFont(WritableFont.ARIAL, 8);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        contentFormat.setWrap(true);


        // add rating item
        DQLSelectBuilder ratingItemBuilder = new DQLSelectBuilder()
                .fromEntity(EnrRatingItem.class, "ri")
                .where(in(property("ri", EnrRatingItem.requestedCompetition()), dataSet));

        Map<EnrRequestedCompetition, EnrRatingItem> ratingItemMap = new HashMap<>(dataSet.size());

        for (Object o : getList(ratingItemBuilder)) {
            EnrRatingItem item = (EnrRatingItem) o;
            ratingItemMap.put(item.getRequestedCompetition(), item);
        }

        int startRow = indexOfHeader + 2;
        int number = 1;

        List<EnrRequestedCompetition> competitionList = new ArrayList<>();
        competitionList.addAll(dataSet);

        final Comparator<EnrRequestedCompetition> competitionComparator = new Comparator<EnrRequestedCompetition>() {
            @Override
            public int compare(EnrRequestedCompetition o1, EnrRequestedCompetition o2) {
                int result;
                result = o1.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getCode().compareTo(o2.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getCode());
                if (result != 0) return result;
                result = o1.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getPrintTitle().compareTo(o2.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getPrintTitle());
                if (result != 0) return result;
                result =o1.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getCode().compareTo(o2.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getCode());
                if (result != 0) return result;
                result = o1.getCompetition().getType().getCompensationType().getCode().compareTo(o2.getCompetition().getType().getCompensationType().getCode());
                if (result != 0) return result;
                result = o1.getCompetition().getType().getCode().compareTo(o2.getCompetition().getType().getCode());
                if (result != 0) return result;
                result = o1.getPriority() - o2.getPriority();
                return result;
            }
        };
        Collections.sort(competitionList, competitionComparator);

        for (EnrRequestedCompetition reqComp : competitionList)
        {
            List<String> rowContent = new ArrayList<>();

            rowContent.add(String.valueOf(number++));

            rowContent.add(reqComp.getRequest().getIdentityCard().getPerson().getFullFio());

            rowContent.add(reqComp.getRequest().getIdentityCard().getFullNumber());

            rowContent.add(getEnrollmentCommissionTitle(reqComp));

            rowContent.add(reqComp.getRequest().getType().getTitle());

            rowContent.add(reqComp.getCompetition().getType().getCompensationType().getShortTitle());

            rowContent.add(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getTitle());

            rowContent.add(reqComp.getCompetition().getType().getTitle());

            rowContent.add(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle());

            rowContent.add(reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit().getShortTitle());

            rowContent.add(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitleWithCode());

            rowContent.add(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getPrintTitle());

            rowContent.add(reqComp.getCompetition().isTargetAdmission() ? "Да" : "Нет");

            rowContent.add(reqComp.getCompetition().isTargetAdmission() ? ((EnrRequestedCompetitionTA)reqComp).getTargetAdmissionKind().getTitle() : "");

            rowContent.add(reqComp.isParallel() ? "Да" : "Нет");

            StringBuilder benefitsString = new StringBuilder();

            if (reqComp instanceof EnrRequestedCompetitionExclusive)
                benefitsString.append(((EnrRequestedCompetitionExclusive) reqComp).getBenefitCategory().getBenefitType().getShortTitle()).append("\nкатегория: ").append(((EnrRequestedCompetitionExclusive) reqComp).getBenefitCategory().getShortTitle());
            if (reqComp instanceof EnrRequestedCompetitionNoExams)
                benefitsString.append(((EnrRequestedCompetitionNoExams) reqComp).getBenefitCategory().getBenefitType().getShortTitle()).append("\nкатегория: ").append(((EnrRequestedCompetitionNoExams) reqComp).getBenefitCategory().getShortTitle());


            if (vviMap.containsKey(reqComp) && vviMap.get(reqComp)!=null)
                for (EnrChosenEntranceExam exam : vviMap.get(reqComp))
                {
                    if (exam.getMarkSource() instanceof EnrEntrantMarkSourceBenefit) {
                        if (benefitsString.length() > 0)
                            benefitsString.append("\n");
                        benefitsString.append(((EnrEntrantMarkSourceBenefit) exam.getMarkSource()).getBenefitCategory().getBenefitType().getShortTitle()).append("\nкатегория: ").append(((EnrEntrantMarkSourceBenefit) exam.getMarkSource()).getBenefitCategory().getShortTitle());
                    }
                }
            if (reqComp.getRequest().getBenefitCategory() != null ) {
                if (benefitsString.length() > 0)
                    benefitsString.append("\n");
                benefitsString.append(reqComp.getRequest().getBenefitCategory().getBenefitType().getShortTitle()).append("\nкатегория: ").append(reqComp.getRequest().getBenefitCategory().getShortTitle());
            }



            rowContent.add(benefitsString.toString());

            rowContent.add(YesNoFormatter.INSTANCE.format(reqComp.isOriginalDocumentHandedIn()));
            rowContent.add(YesNoFormatter.INSTANCE.format(reqComp.isAccepted()));
            rowContent.add(YesNoFormatter.INSTANCE.format(reqComp.isEnrollmentAvailable()));

            rowContent.add(String.valueOf(reqComp.getPriority()));

            double sum = 0;

            for (EnrCampaignDiscipline disc : disciplines)
            {
                boolean contains = false;
                if (vviMap.containsKey(reqComp) && vviMap.get(reqComp)!=null)
                for (EnrChosenEntranceExam exam : vviMap.get(reqComp))
                {
                    if (disciplineToVviMap.get(disc).contains(exam))
                    {
                        contains = true;
                        if (exam.getMaxMarkForm() != null)
                            rowContent.add(exam.getMaxMarkForm().getPassForm().getTitle());
                        else rowContent.add("");
                        if (exam.getMarkAsDouble() != null) {
                            rowContent.add(String.valueOf(exam.getMarkAsDouble().intValue()));
                            sum += exam.getMarkAsDouble();
                        }
                        else rowContent.add("");
                    }
                }
                if (!contains)
                {
                    rowContent.add("x");
                    rowContent.add("x");
                }
            }

            rowContent.add(STATIC_HEADER_CONTENT.length - 1, String.valueOf(sum));

            if(printEduDocumentAvgMark)
            {
                Double avgMark = reqComp.getRequest().getEduDocument().getAvgMarkAsDouble();
                rowContent.add(avgMark == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(avgMark));
            }

            final EnrRatingItem ratingItem = ratingItemMap.get(reqComp);
            if (ratingItem != null) {
                Long ratingMark = ratingItem.getAchievementMarkAsLong();

                rowContent.add(String.valueOf(ratingMark / 1000.0));
            } else rowContent.add("");

            ExcelReportUtils.printrow(sheet, rowContent, startRow++, contentFormat);
        }
        workbook.write();
        workbook.close();

        byte[] bytes = out.toByteArray();
        out.close();
        return bytes;
    }

    private static int printHeader(EnrReportForEnrollmentCommissionAddUI model, WritableSheet sheet) throws JXLException, IOException
    {
        int nextFreeRow = 0;
        WritableFont labelFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        WritableFont contentFont = new WritableFont(WritableFont.ARIAL, 8);

        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        headerFormat.setBackground(Colour.GREY_25_PERCENT);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);
        WritableCellFormat titleFormat = new WritableCellFormat(headerFont);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        List<String[]> hTable = new FilterParametersPrinter().getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        hTable.add(0, new String[]{"Приемная кампания", model.getEnrollmentCampaign().getTitle()});
        hTable.add(3, new String[]{"Стадия приемной кампании", model.getStageSelector().getStage().getTitle()});
        if (model.isFilterByEnrCommission())
            hTable.add(new String[]{"Отборочная комиссия", UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; ")});
        if (model.isParallelActive())
            hTable.add(new String[]{"Поступающие параллельно", model.getParallel().getTitle()});



        sheet.addCell(new Label(1, 0, "Сводка для отборочных комиссий", labelFormat));
        sheet.addCell(new Label(2, 0, new SimpleDateFormat("dd.MM.yyyy").format(new Date()), labelFormat));

        for (int i = 0; i < hTable.size(); i++) {
            sheet.addCell(new Label(1, i+2, hTable.get(i)[0], titleFormat));
            sheet.addCell(new Label(2, i+2, hTable.get(i)[1], contentFormat));
            nextFreeRow = i+3;
        }


        for (int i = 0; i < STATIC_HEADER_CONTENT.length; i++)
        {
            sheet.mergeCells(i, nextFreeRow + 1, i, nextFreeRow + 2);
            sheet.addCell(new Label(i, nextFreeRow + 1, STATIC_HEADER_CONTENT[i], headerFormat));
        }

        int[] colSizes = {
                5,
                42,
                15,
                15,
                37,
                12,
                12,
                25,
                12,
                14,
                52,
                14,
                14,
                14,
                14,
                23,
                16,
                16,
                16,
                14,
                14
        };

        for (int i = 0; i < colSizes.length; i++) {
            sheet.setColumnView(i, colSizes[i]);
        }

        return nextFreeRow + 1;
    }

    private static void printDisciplines(WritableSheet sheet, Set<EnrCampaignDiscipline> disciplines, int indexOfHeader, boolean printEduDocumentAvgMark) throws  JXLException, IOException
    {
        WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        headerFormat.setBackground(Colour.GREY_25_PERCENT);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);


        int currentCol = STATIC_HEADER_CONTENT.length;
        for (EnrCampaignDiscipline disc : disciplines)
        {
            sheet.mergeCells(currentCol, indexOfHeader, currentCol+1, indexOfHeader);
            sheet.addCell(new Label(currentCol, indexOfHeader, disc.getTitle(), headerFormat));
            sheet.addCell(new Label(currentCol, indexOfHeader + 1, "Форма сдачи", headerFormat));

            sheet.addCell(new Label(++currentCol, indexOfHeader + 1, "Балл", headerFormat));

            currentCol++;
        }

        if(printEduDocumentAvgMark)
        {
            sheet.addCell(new Label(currentCol, indexOfHeader, "Средний балл документа об образовании", headerFormat));
            sheet.mergeCells(currentCol, indexOfHeader, currentCol, indexOfHeader + 1);
            sheet.setColumnView(currentCol, 14);

            currentCol++;
        }

        sheet.addCell(new Label(currentCol, indexOfHeader, "Сумма баллов за индивидуальные достижения", headerFormat));
        sheet.mergeCells(currentCol, indexOfHeader, currentCol, indexOfHeader + 1);
        sheet.setColumnView(currentCol, 14);
    }

}
