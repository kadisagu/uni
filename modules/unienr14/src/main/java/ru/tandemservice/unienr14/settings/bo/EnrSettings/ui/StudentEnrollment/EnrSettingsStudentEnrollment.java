/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.StudentEnrollment;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.gen.IEnrollmentStudentSettingsGen;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/15/14
 */
@Configuration
public class EnrSettingsStudentEnrollment extends BusinessComponentManager
{
    public static final String EXAM_SET_SEARCH_DS = "programSetSearchDS";

    public static final String BIND_REQUEST_TYPE = "requestType";
    public static final String BIND_PROGRAM_FORM = "programForm";
    public static final String BIND_ORG_UNITS = "orgUnits";
    public static final String BIND_SUBJECT_CODE = "subjectCode";
    public static final String BIND_PROGRAM_SUBJECTS = "programSubjects";
    public static final String BIND_EDU_PROGRAMS = "eduPrograms";
    public static final String BIND_PROGRAM_SET_TITLE = "programSetTitle";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
            .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
            .addDataSource(selectDS("orgUnitDS", orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(searchListDS(EXAM_SET_SEARCH_DS, programSetSearchDSColumns(), programSetSearchDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint programSetSearchDSColumns()
    {
        IColumnListExtPointBuilder ext = columnListExtPointBuilder(EXAM_SET_SEARCH_DS)
            .addColumn(publisherColumn("programSet", IEnrollmentStudentSettingsGen.programSet().title().s())
                .primaryKeyPath(IEnrollmentStudentSettingsGen.programSet().id().s())
                .order())
            .addColumn(textColumn("programSubject", IEnrollmentStudentSettingsGen.programSet().programSubject().titleWithCode().s()).order())
            .addColumn(textColumn("developForm", IEnrollmentStudentSettingsGen.programSet().programForm().title().s()).order())
            .addColumn(textColumn("orgUnit", IEnrollmentStudentSettingsGen.orgUnit().institutionOrgUnit().orgUnit().shortTitle().s()))
            .addColumn(textColumn("program", "programTitle"))
            .addColumn(publisherColumn("eduOu", IEnrollmentStudentSettingsGen.educationOrgUnit().educationLevelHighSchool().fullTitleExtended().s())
                .primaryKeyPath(IEnrollmentStudentSettingsGen.educationOrgUnit().id().s())
                .order())
            .addColumn(textColumn("formativeOrgUnit", IEnrollmentStudentSettingsGen.educationOrgUnit().formativeOrgUnit().shortTitle().s()).order())
            .addColumn(textColumn("territorialOrgUnit", IEnrollmentStudentSettingsGen.educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).order())
            .addColumn(textColumn("developCombination", IEnrollmentStudentSettingsGen.educationOrgUnit().developCombinationTitle().s()).order())
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("cancel"), "onClickDelete").disabled("disabled")
                               .alert(new FormattedMessage("programSetSearchDS.delete.alert", "title")));

        // todo подумай еще
//        if (Debug.isEnabled()) {
//            ext.addColumn(actionColumn("purge", new Icon("delete"), "onClickPurge").alert(new FormattedMessage("programSetSearchDS.purge.alert", "title")));
//        }

        ext.addColumn(actionColumn("purge", new Icon("delete"), "onClickPurge").disabled("disabled")
                              .alert(new FormattedMessage("programSetSearchDS.purge.alert", "title")));

        return ext.create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSetSearchDSHandler()
    {
        return new EnrStudentEnrollmentSettingsDsHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EnrRequestType requestType = context.get(BIND_REQUEST_TYPE);

                DQLSelectBuilder psOuDql = new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                    .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().enrollmentCampaign()), value(campaign)))
                    .where(eq(property("psou", EnrProgramSetOrgUnit.orgUnit()), property(alias)));
                if (requestType == null) {
                    psOuDql.where(nothing());
                }
                else {
                    psOuDql.joinEntity("psou", DQLJoinType.inner, requestType.getProgramSetClass(), "ps",
                        eq(property("ps.id"), property("psou", EnrProgramSetOrgUnit.programSet().id())));
                }

                dql.where(exists(psOuDql.buildQuery()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EnrRequestType requestType = context.get(BIND_REQUEST_TYPE);
                List<EnrOrgUnit> orgUnits = context.get(BIND_ORG_UNITS);

                DQLSelectBuilder psOuDql = new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                    .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().enrollmentCampaign()), value(campaign)))
                    .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().programSubject()), property(alias)));
                if (null != orgUnits && !orgUnits.isEmpty()) {
                    psOuDql.where(in(property("psou", EnrProgramSetOrgUnit.orgUnit()), orgUnits));
                }
                if (requestType == null) {
                    psOuDql.where(nothing());
                }
                else {
                    psOuDql.joinEntity("psou", DQLJoinType.inner, requestType.getProgramSetClass(), "ps",
                        eq(property("ps.id"), property("psou", EnrProgramSetOrgUnit.programSet().id())));
                }

                dql.where(exists(psOuDql.buildQuery()));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title());
    }
}