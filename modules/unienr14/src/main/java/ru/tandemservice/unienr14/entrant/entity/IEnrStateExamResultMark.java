package ru.tandemservice.unienr14.entrant.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;

/**
 * Балл по ЕГЭ (обезличенный, просто предмет и балл)
 * @author vdanilov
 */
public interface IEnrStateExamResultMark {
    EnrStateExamSubject getSubject();
    Long getMarkAsLong();
}
