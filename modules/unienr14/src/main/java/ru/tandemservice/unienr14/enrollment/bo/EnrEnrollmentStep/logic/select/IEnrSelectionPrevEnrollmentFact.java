/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 4/2/15
 */
public interface IEnrSelectionPrevEnrollmentFact extends IEntity
{
    EnrRequestedCompetition getRequestedCompetition();

    String getInfo();
}
