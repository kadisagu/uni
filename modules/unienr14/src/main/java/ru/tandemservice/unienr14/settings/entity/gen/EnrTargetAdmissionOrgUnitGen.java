package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Организация ЦП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrTargetAdmissionOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<EnrTargetAdmissionOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit";
    public static final String ENTITY_NAME = "enrTargetAdmissionOrgUnit";
    public static final int VERSION_HASH = 1978943269;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_CAMPAIGN_T_A_KIND = "enrCampaignTAKind";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";

    private EnrCampaignTargetAdmissionKind _enrCampaignTAKind;     // Вид ЦП, используемый в рамках ПК
    private ExternalOrgUnit _externalOrgUnit;     // Внешняя организация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignTargetAdmissionKind getEnrCampaignTAKind()
    {
        return _enrCampaignTAKind;
    }

    /**
     * @param enrCampaignTAKind Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     */
    public void setEnrCampaignTAKind(EnrCampaignTargetAdmissionKind enrCampaignTAKind)
    {
        dirty(_enrCampaignTAKind, enrCampaignTAKind);
        _enrCampaignTAKind = enrCampaignTAKind;
    }

    /**
     * @return Внешняя организация. Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Внешняя организация. Свойство не может быть null.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrTargetAdmissionOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrCampaignTAKind(((EnrTargetAdmissionOrgUnit)another).getEnrCampaignTAKind());
                setExternalOrgUnit(((EnrTargetAdmissionOrgUnit)another).getExternalOrgUnit());
            }
        }
    }

    public INaturalId<EnrTargetAdmissionOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getEnrCampaignTAKind(), getExternalOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EnrTargetAdmissionOrgUnitGen>
    {
        private static final String PROXY_NAME = "EnrTargetAdmissionOrgUnitNaturalProxy";

        private Long _enrCampaignTAKind;
        private Long _externalOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrCampaignTargetAdmissionKind enrCampaignTAKind, ExternalOrgUnit externalOrgUnit)
        {
            _enrCampaignTAKind = ((IEntity) enrCampaignTAKind).getId();
            _externalOrgUnit = ((IEntity) externalOrgUnit).getId();
        }

        public Long getEnrCampaignTAKind()
        {
            return _enrCampaignTAKind;
        }

        public void setEnrCampaignTAKind(Long enrCampaignTAKind)
        {
            _enrCampaignTAKind = enrCampaignTAKind;
        }

        public Long getExternalOrgUnit()
        {
            return _externalOrgUnit;
        }

        public void setExternalOrgUnit(Long externalOrgUnit)
        {
            _externalOrgUnit = externalOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrTargetAdmissionOrgUnitGen.NaturalId) ) return false;

            EnrTargetAdmissionOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrCampaignTAKind(), that.getEnrCampaignTAKind()) ) return false;
            if( !equals(getExternalOrgUnit(), that.getExternalOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrCampaignTAKind());
            result = hashCode(result, getExternalOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrCampaignTAKind());
            sb.append("/");
            sb.append(getExternalOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrTargetAdmissionOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrTargetAdmissionOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EnrTargetAdmissionOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrCampaignTAKind":
                    return obj.getEnrCampaignTAKind();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrCampaignTAKind":
                    obj.setEnrCampaignTAKind((EnrCampaignTargetAdmissionKind) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrCampaignTAKind":
                        return true;
                case "externalOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrCampaignTAKind":
                    return true;
                case "externalOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrCampaignTAKind":
                    return EnrCampaignTargetAdmissionKind.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrTargetAdmissionOrgUnit> _dslPath = new Path<EnrTargetAdmissionOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrTargetAdmissionOrgUnit");
    }
            

    /**
     * @return Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit#getEnrCampaignTAKind()
     */
    public static EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> enrCampaignTAKind()
    {
        return _dslPath.enrCampaignTAKind();
    }

    /**
     * @return Внешняя организация. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    public static class Path<E extends EnrTargetAdmissionOrgUnit> extends EntityPath<E>
    {
        private EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> _enrCampaignTAKind;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit#getEnrCampaignTAKind()
     */
        public EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> enrCampaignTAKind()
        {
            if(_enrCampaignTAKind == null )
                _enrCampaignTAKind = new EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind>(L_ENR_CAMPAIGN_T_A_KIND, this);
            return _enrCampaignTAKind;
        }

    /**
     * @return Внешняя организация. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

        public Class getEntityClass()
        {
            return EnrTargetAdmissionOrgUnit.class;
        }

        public String getEntityName()
        {
            return "enrTargetAdmissionOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
