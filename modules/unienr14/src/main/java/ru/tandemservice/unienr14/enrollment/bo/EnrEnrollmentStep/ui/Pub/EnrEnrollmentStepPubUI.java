/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.Pub;

import java.util.Map;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepKindCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.EnrEnrollmentStepDao;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.PlanEdit.EnrEnrollmentStepPlanEdit;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.PlanEdit.EnrEnrollmentStepPlanEditUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.EnrEnrollmentParagraphManager;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.EnrEntrantRefusalManager;
import ru.tandemservice.unienr14.request.daemon.EnrRequestExtParamsDaemonBean;

/**
 * @author oleyba
 * @since 12/17/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "step.id", required=true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrEnrollmentStepPubUI extends UIPresenter
{
    private EnrEnrollmentStep step = new EnrEnrollmentStep();
    private String selectedTab;

    @Override
    public void onComponentRefresh()
    {
        setStep(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentStep.class, getStep().getId()));
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
        {
            util
            .configDoubleWidthFilters(false)
            .configUseEnableCheckbox(false)
            .configSettings(getSettingsKey());

            util.clearFilterItems();
            util
            .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

            configWhereFilters();
        }
    }

    private CommonFilterAddon getFilterUtil(){return (CommonFilterAddon) this.getConfig().getAddon(CommonFilterAddon.class.getSimpleName());}

    private void configWhereFilters()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
        {
            util
            .clearWhereFilter()
            .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign(), getStep().getEnrollmentCampaign()))
            .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.requestType(), getStep().getRequestType()));
        }
    }

    public void onClickSearch()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickClear()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
            util.clearSettings();

        clearSettings();
        configWhereFilters();
        onClickSearch();
    }


    // actions

    public void onClickNextState() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentStepManager.instance().dao().doMoveToNextState(getStep());
    }

    public void onClickPrevState() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentStepManager.instance().dao().doMoveToPrevState(getStep());
    }

    public void onClickDelete() {
        EnrEnrollmentStepManager.instance().dao().doDelete(getStep().getId());
        deactivate();
    }

    public void onClickAutoRecommend() {
        _uiSupport.setRefreshScheduled(true);
        EnrEntrantDaemonBean.DAEMON.waitForComplete();
        EnrEnrollmentStepManager.instance().dao().doAutoRecommend(getStep());
    }

    public void onClickAllowManualRecommendation() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentStepManager.instance().dao().doAllowManualRecommendation(getStep());
    }

    public void onClickClearRecommendation() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentStepManager.instance().dao().doClearRecommendation(getStep());
    }

    public void onClickAutoMarkEnrolled() {
        _uiSupport.setRefreshScheduled(true);
        EnrRequestExtParamsDaemonBean.DAEMON.wakeUpAndWaitDaemon(3600);
        EnrEntrantDaemonBean.DAEMON.waitForComplete();
        EnrEnrollmentStepManager.instance().dao().doAutoMarkEnrolled(getStep());
    }

    public void onClickAllowManualMarkEnrolled() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentStepManager.instance().dao().doAllowManualMarkEnrolled(getStep());
    }

    public void onClickClearMarkEnrolled() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentStepManager.instance().dao().doClearMarkEnrolled(getStep());
    }

    public void onClickAutoCreateOrders() {
        _uiSupport.setRefreshScheduled(true);
        EnrEnrollmentParagraphManager.instance().dao().doCreateOrders(getStep());
    }

    public void onClickRefresh() {
        EnrRequestExtParamsDaemonBean.DAEMON.wakeUpAndWaitDaemon(3600);
        _uiSupport.setRefreshScheduled(true);

        EnrEnrollmentStepManager.instance().dao().doRefreshData(getStep());
    }

    public void onClickSelectEnrollmentHack5696() {
        _uiSupport.setRefreshScheduled(true);
        EnrEntrantDaemonBean.DAEMON.waitForComplete();
        EnrEnrollmentStepManager.instance().dao().doSelectEnrollmentHack5696(getStep());
    }

    public void onClickPlanEdit() {
        _uiActivation.asRegionDialog(EnrEnrollmentStepPlanEdit.class)
            .parameter(PUBLISHER_ID, getListenerParameterAsLong())
            .parameter(EnrEnrollmentStepPlanEditUI.BIND_STEP_ID, getStep().getId())
            .activate();
    }

    // presenter

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentStepCompetitionDSHandler.BIND_ENR_STEP, getStep());
        dataSource.put(EnrEnrollmentStepCompetitionDSHandler.BIND_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
    }

    public Map<String, Object> getCompPubParametersMap() {
        return new ParametersMap().add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, getStep().getId());
    }

    public boolean isDeleteDisabled() {
        return !EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode());
    }

    public boolean isShowRecommendationCheck() {
        return getStep().getKind().isUseRecommendation();
    }

    public boolean isShowEnrollmentCheck() {
        return ! (getStep().getKind().isUseRecommendation() && EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode()));
    }

    public boolean isHack5696Visible() {
        if (!EnrEnrollmentStepDao.isHachEnabled()) { return false; }
        if (!EnrEnrollmentStepStateCodes.RECOMMENDED.equals(step.getState().getCode())) { return false; }
        if (!EnrEnrollmentStepKindCodes.REC_AND_ADHERE.equals(step.getKind().getCode())) { return false; }
        return true;
    }

    public String getNextStateButtonPermissionKey() {
        switch (getStep().getState().getCode()) {
            case EnrEnrollmentStepStateCodes.START: return getStep().getKind().isUseRecommendation() ? "enr14EnrollmentStepFixRec" : "enr14EnrollmentStepFixEnr";
            case EnrEnrollmentStepStateCodes.RECOMMENDED: return "enr14EnrollmentStepFixEnr";
            case EnrEnrollmentStepStateCodes.FORMING_ORDERS: return "enr14EnrollmentStepClose";
        }
        throw new IllegalStateException();
    }

    public String getPrevStateButtonPermissionKey() {
        switch (getStep().getState().getCode()) {
            case EnrEnrollmentStepStateCodes.RECOMMENDED: return "enr14EnrollmentStepUnfixRec";
            case EnrEnrollmentStepStateCodes.FORMING_ORDERS: return "enr14EnrollmentStepUnfixEnr";
            case EnrEnrollmentStepStateCodes.CLOSED: return "enr14EnrollmentStepReopen";
        }
        throw new IllegalStateException();
    }

    public String getNextStateButtonTitle() {
        switch (getStep().getState().getCode()) {
            case EnrEnrollmentStepStateCodes.START: return getStep().getKind().isUseRecommendation() ? "Зафиксировать рекомендацию" : "Зафиксировать отметку к зачислению";
            case EnrEnrollmentStepStateCodes.RECOMMENDED: return "Зафиксировать отметку к зачислению";
            case EnrEnrollmentStepStateCodes.FORMING_ORDERS: return "Завершить зачисление";
        }
        throw new IllegalStateException();
    }

    public String getPrevStateButtonTitle() {
        switch (getStep().getState().getCode()) {
            case EnrEnrollmentStepStateCodes.RECOMMENDED: return "Вернуться к рекомендации";
            case EnrEnrollmentStepStateCodes.FORMING_ORDERS: return "Вернуться к зачислению";
            case EnrEnrollmentStepStateCodes.CLOSED: return "Вернуться к формированию приказов";
        }
        throw new IllegalStateException();
    }

    // getters and setters

    public EnrEnrollmentStep getStep()
    {
        return step;
    }

    public void setStep(EnrEnrollmentStep step)
    {
        this.step = step;
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }
}