/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.AddEdit.EnrEnrollmentOrderParagraphPrintFormTypeAddEdit;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
/**
 * @author azhebko
 * @since 17.07.2014
 */
@State({@Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode")})
public class EnrEnrollmentOrderParagraphPrintFormTypeListUI extends BaseCatalogPubUI
{
    private List<Long> _setDefaultDisabledIds;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentOrderParagraphPrintFormType.class, "e")
            .column(property("e", EnrEnrollmentOrderParagraphPrintFormType.id()))
            .where(isNull(property("e", EnrEnrollmentOrderParagraphPrintFormType.path())));

        _setDefaultDisabledIds = IUniBaseDao.instance.get().getList(builder);
    }

    protected void prepareItemDS()
    {
        final DynamicListDataSource<ICatalogItem> dataSource = new DynamicListDataSource<>(this, component -> {
            updateItemDS();
        });

        dataSource.addColumn(new SimpleColumn("Название", ICatalogItem.CATALOG_ITEM_TITLE).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Вид заявления", EnrEnrollmentOrderParagraphPrintFormType.requestType().shortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид приема", EnrEnrollmentOrderParagraphPrintFormType.competitionType().shortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Параллельное обучение", EnrEnrollmentOrderParagraphPrintFormType.parallel()).setFormatter(YesNoFormatter.STRICT).setOrderable(false).setClickable(false));

        if (Debug.isDisplay())
            dataSource.addColumn(new SimpleColumn("Системный код", ICatalogItem.CATALOG_ITEM_CODE).setOrderable(false).setWidth(1).setClickable(false));

        IEntityHandler deleteDisabledHandler = entity -> getDeleteDisabledIds().contains(entity.getId());
        IEntityHandler setDefaultDisabledHandler = entity -> _setDefaultDisabledIds.contains(entity.getId());

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickDownloadTemplate", "Скачать шаблон").defaultIndicator(new IndicatorColumn.Item("template_save", "Скачать шаблон")).setPermissionKey(getSec().getPermission("print")));
        dataSource.addColumn(new ActionColumn("Редактировать", "template_add", "onClickEditItem").setPermissionKey(getSec().getPermission("edit")));
        dataSource.addColumn(new ActionColumn("Вернуть шаблон по умолчанию", "template_revert", "onClickSetDefault", "Вернуть шаблон по умолчанию для «{0}»?", "title").setPermissionKey(getSec().getPermission("revert")).setDisableHandler(setDefaultDisabledHandler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE)
            .setPermissionKey(getSec().getPermission("delete")).setDisableHandler(deleteDisabledHandler));

        setItemDS(dataSource);
    }


    @Override
    public void onClickAddItem()
    {
        getActivationBuilder().asRegion(EnrEnrollmentOrderParagraphPrintFormTypeAddEdit.class).activate();
    }

    @Override
    public void onClickEditItem()
    {
        getActivationBuilder().asRegion(EnrEnrollmentOrderParagraphPrintFormTypeAddEdit.class).parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickSetDefault()
    {
        ITemplateDocument template = IUniBaseDao.instance.get().getNotNull(ITemplateDocument.class, getListenerParameterAsLong());
        if (template.getPath() == null)
            throw new RuntimeException();
        CatalogManager.instance().modifyDao().doRevertTemplateToDefault(getListenerParameterAsLong());
    }

    @Override
    public void onClickDownloadTemplate()
    {
        EnrEnrollmentOrderParagraphPrintFormType template = DataAccessServices.dao().get(getListenerParameterAsLong());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(template.getTitle() + ".rtf").document(CommonBaseUtil.getTemplateContent(template)), false);
    }
}