package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOriginalSubmissionAndReturnWay

        short entityCode;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_orig_subm_ret_way_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCode = tool.entityCodes().ensure("enrOriginalSubmissionAndReturnWay");
		}

        Long personallyId = EntityIDGenerator.generateNewId(entityCode);
        tool.executeUpdate("insert into enr14_c_orig_subm_ret_way_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)", personallyId, entityCode, "1", "Лично");

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantRequest

		// создано обязательное свойство originalSubmissionWay
		{
			// создать колонку
			tool.createColumn("enr14_request_t", new DBColumn("originalsubmissionway_id", DBType.LONG));

			// задать значение по умолчанию
			Long defaultOriginalSubmissionWay = personallyId;
			tool.executeUpdate("update enr14_request_t set originalsubmissionway_id=? where originalsubmissionway_id is null", defaultOriginalSubmissionWay);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_request_t", "originalsubmissionway_id", false);
		}

		// создано обязательное свойство originalReturnWay
		{
			// создать колонку
			tool.createColumn("enr14_request_t", new DBColumn("originalreturnway_id", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultOriginalReturnWay = personallyId;
			tool.executeUpdate("update enr14_request_t set originalreturnway_id=? where originalreturnway_id is null", defaultOriginalReturnWay);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_request_t", "originalreturnway_id", false);
		}
    }
}