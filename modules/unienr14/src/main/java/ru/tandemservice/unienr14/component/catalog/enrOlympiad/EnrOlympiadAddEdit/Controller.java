/**
 *$Id: Controller.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrOlympiad.EnrOlympiadAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;

/**
 * @author Alexander Shaburov
 * @since 14.05.13
 */
public class Controller extends DefaultCatalogAddEditController<EnrOlympiad, Model, IDAO>
{
}
