package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x9x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantOriginalDocumentStatus

		// создано свойство requestedCompetition
		{
			// создать колонку
			tool.createColumn("enr14_entrant_original_doc_t", new DBColumn("requestedcompetition_id", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetition

		// переименовано свойство acceptedContract в accepted
		{
			// удалить колонку
			tool.renameColumn("enr14_requested_comp_t", "acceptedcontract_p", "accepted_p");
		}

		// переименовано свойство contractEnrollmentAvailable в enrollmentAvailable
		{
			// удалить колонку
			tool.renameColumn("enr14_requested_comp_t", "contractenrollmentavailable_p", "enrollmentavailable_p");

		}
    }
}