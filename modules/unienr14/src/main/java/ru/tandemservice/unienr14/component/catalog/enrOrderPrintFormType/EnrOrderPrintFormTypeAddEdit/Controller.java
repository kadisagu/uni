/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypeAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditController;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;

/**
 * @author azhebko
 * @since 14.07.2014
 */
public class Controller extends DefaultScriptCatalogAddEditController<EnrOrderPrintFormType, Model, IDAO>
{
    public void onChangeOrderType(IBusinessComponent component)
    {
        Model model = getModel(component);
        EnrOrderPrintFormType orderPrintFormType = model.getCatalogItem();

        String templatePath = null;
        String scriptPath = null;

        if (orderPrintFormType.getOrderType() != null)
        {
            String printFormTypeCode = orderPrintFormType.getOrderType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT) ? EnrOrderPrintFormTypeCodes.BASE_ENROLLMENT : EnrOrderPrintFormTypeCodes.BASE_CANCEL_ENROLLMENT;
            EnrOrderPrintFormType baseOrderPrintFormType = IUniBaseDao.instance.get().getCatalogItem(EnrOrderPrintFormType.class, printFormTypeCode);

            templatePath = baseOrderPrintFormType.getTemplatePath();
            scriptPath = baseOrderPrintFormType.getScriptPath();

            if (!model.getUseUserScript())
                model.setUserScript(baseOrderPrintFormType.getScript());
        }

        orderPrintFormType.setTemplatePath(templatePath);
        orderPrintFormType.setScriptPath(scriptPath);
    }
}