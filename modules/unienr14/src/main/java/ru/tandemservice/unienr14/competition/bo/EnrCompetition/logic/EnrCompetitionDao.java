/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import com.google.common.collect.Iterables;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.hibernate.Session;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/3/14
 */
public class EnrCompetitionDao extends UniBaseDao implements IEnrCompetitionDao
{
    @Override
    public void doToggleAllowProgramPriorities(final Long competitionId)
    {
        // todo проверка, что еще не выбрали приоритеты + отдельная кнопка с отдельным правом
        final EnrCompetition competition = this.getNotNull(competitionId);
        competition.setAllowProgramPriorities(!competition.isAllowProgramPriorities());
        this.update(competition);
    }

    @Override
    public void doCalculateProgramSetPlans(EnrEnrollmentCampaign ec, PlanCalculationRule correctionRule, Collection<Long> programSetIds, boolean updatePlansSetManually)
    {
        final Session session = this.lock("doCorrectProgramSetBSPlans." + ec.getId());

        for (List<Long> elements : Iterables.partition(programSetIds, 100))
        {
            final List<EnrProgramSetOrgUnit> programSetOuList = new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetOrgUnit.class, "psOu").column(property("psOu"))
                    .where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("psOu")), value(ec)))
                    .where(in(property(EnrProgramSetOrgUnit.programSet().id().fromAlias("psOu")), elements))
                    .createStatement(session).list();

            for (final EnrProgramSetOrgUnit programSetOrgUnit: programSetOuList) {
                calculatePlans(new PlanCalculationContext(programSetOrgUnit, correctionRule, updatePlansSetManually));
            }

            session.flush();
            session.clear();
        }
    }


    // ----------------------------------------------------

    private void calculatePlans(final PlanCalculationContext context) {
        List<EnrCompetition> competitions = new DQLSelectBuilder()
            .fromEntity(EnrCompetition.class, "c").column(property("c"))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit()), value(context.programSetOrgUnit)))
            .createStatement(getSession()).list();

        Set<Long> updatePlanIds = new HashSet<>();
        Transformer<ICalculatedPlanOwner, Long> updatePlanIdsTransformer = c -> context.updatePlansSetManually || c.getCalculatedPlan() == c.getPlan() ? c.getId() : null;
        updatePlanIds.addAll(CollectionUtils.collect(competitions, updatePlanIdsTransformer));

        for (EnrCompetition c : competitions) {
            c.setFixPlan(EnrCompetitionPlanFormula.calculatePlan(c, context));
        }

        if (PlanCalculationRule.BY_PASSED.equals(context.correctionRule) || PlanCalculationRule.BY_PASSED_AND_FREE.equals(context.correctionRule)) {
            EnrPlanZeroFixer.fix(competitions, context, new EntityComparator<>(new EntityOrder(EnrCompetition.plan(), OrderDirection.desc)));
        }

        for (EnrCompetition c : competitions) {
            if (updatePlanIds.contains(c.getId())) c.setPlan(c.getCalculatedPlan());
        }

        // Для всех сущностей «План приема по виду ЦП» подразделения, ведущего прием, добавить нужные и удалить ненужные связанные сущности «Число мест по виду ЦП»
        // для каждого конкурса по ЦП подразделения должна быть своя сущность «Число мест по виду ЦП».
        // При добавлении сущности сохранять в поля мест 0.

        List<EnrTargetAdmissionCompetitionPlan> competitionTaPlans = new ArrayList<>();
        for (EnrTargetAdmissionPlan taPlan : getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), context.programSetOrgUnit)) {
            if (taPlan.getPlan() > 0) {
                for (EnrCompetition c : competitions) {
                    if (!c.isTargetAdmission()) continue;
                    competitionTaPlans.add(new EnrTargetAdmissionCompetitionPlan(taPlan, c, 0, 0));
                }
            }
        }

        new MergeAction.SessionMergeAction<INaturalId, EnrTargetAdmissionCompetitionPlan>() {
            @Override protected INaturalId key(final EnrTargetAdmissionCompetitionPlan source) {
                return source.getNaturalId();
            }

            @Override protected void fill(final EnrTargetAdmissionCompetitionPlan target, final EnrTargetAdmissionCompetitionPlan source) {
                // ничего не делаем
            }

            @Override protected EnrTargetAdmissionCompetitionPlan buildRow(final EnrTargetAdmissionCompetitionPlan source) {
                return source;
            }
        }.merge(
            getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition().programSetOrgUnit(), context.programSetOrgUnit),
            competitionTaPlans
        );

        getSession().flush();

        // обновляем планы по видам ЦП для конкурсов

        List<EnrTargetAdmissionCompetitionPlan> taCompPlans = getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition().programSetOrgUnit(), context.programSetOrgUnit);
        updatePlanIds.addAll(CollectionUtils.collect(taCompPlans, updatePlanIdsTransformer));

        for (EnrTargetAdmissionCompetitionPlan taPlan : taCompPlans) {
            taPlan.setFixPlan(EnrTargetAdmissionCompetitionPlanFormula.calculatePlan(taPlan, context));
        }

        if (PlanCalculationRule.BY_PASSED.equals(context.correctionRule)) {
            EnrPlanZeroFixer.fix(taCompPlans, context, new EntityComparator<>(new EntityOrder(EnrTargetAdmissionCompetitionPlan.plan(), OrderDirection.desc)));
        }

        for (EnrTargetAdmissionCompetitionPlan taPlan : taCompPlans) {
            if (updatePlanIds.contains(taPlan.getId())) taPlan.setPlan(taPlan.getCalculatedPlan());
        }
    }

    class PlanCalculationContext implements EnrCompetitionPlanFormula.IPlanCalculationContext {
        final PlanCalculationRule correctionRule;
        final boolean updatePlansSetManually;
        final EnrProgramSetOrgUnit programSetOrgUnit;

        Map<MultiKey, Integer> passedCountMap;
        Map<MultiKey, Integer> enrolledCountMap;

        public PlanCalculationContext(EnrProgramSetOrgUnit programSetOrgUnit, PlanCalculationRule correctionRule, boolean updatePlansSetManually)
        {
            this.programSetOrgUnit = programSetOrgUnit;
            this.correctionRule = correctionRule;
            this.updatePlansSetManually = updatePlansSetManually;
        }

        @Override
        public PlanCalculationRule correctionRule()
        {
            return correctionRule;
        }

        @Override
        public int s(String compType, String level) {
            if (passedCountMap == null) initPassed();
            Integer count = passedCountMap.get(new MultiKey(compType, level));
            return null == count ? 0 : count;
        }

        @Override
        public int e(String compType) {
            if (enrolledCountMap == null) initEnrolled();
            Integer count = enrolledCountMap.get(new MultiKey(compType, null));
            return null == count ? 0 : count;
        }

        @Override
        public int e(String compType, String level) {
            if (enrolledCountMap == null) initEnrolled();
            Integer count = enrolledCountMap.get(new MultiKey(compType, level));
            return null == count ? 0 : count;
        }

        @Override
        public int e(String taKind, String compType, String level) {
            if (enrolledCountMap == null) initEnrolled();
            Integer count = enrolledCountMap.get(new MultiKey(taKind, compType, level));
            return null == count ? 0 : count;
        }

        private void initEnrolled() {
            enrolledCountMap = new HashMap<>();
            // число абитуриентов, зачисленных по данному конкурсу:
            // состояние выбранного конкурса «зачислен»
            List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "r")
                .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                .where(eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit()), value(programSetOrgUnit)))
                .column(property("r", EnrRequestedCompetition.competition().type().code()))
                .column(property("r", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
                .group(property("r", EnrRequestedCompetition.competition().type().code()))
                .group(property("r", EnrRequestedCompetition.competition().eduLevelRequirement().code()))
                .column(DQLFunctions.count(DQLPredicateType.distinct, property("r", EnrRequestedCompetition.id())))
                .createStatement(getSession()).list();
            for (Object[] row : rows) {
                String type = (String) row[0];
                String level = (String) row[1];
                Integer count = ((Number) row[2]).intValue();
                enrolledCountMap.put(new MultiKey(type, level), count);
                MultiKey totalKey = new MultiKey(type, null);
                Integer total = enrolledCountMap.get(totalKey); if (null == total) total = 0;
                enrolledCountMap.put(totalKey, total + count);
            }

            // Число зачисленных по ЦП в рамках вида ЦП и базового уровня
            rows = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetitionTA.class, "r")
                    .where(eq(property("r", EnrRequestedCompetitionTA.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                    .where(eq(property("r", EnrRequestedCompetitionTA.competition().programSetOrgUnit()), value(programSetOrgUnit)))
                    .group(property("r", EnrRequestedCompetitionTA.competition().eduLevelRequirement().code()))
                    .group(property("r", EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().code()))
                    .column(property("r", EnrRequestedCompetitionTA.competition().eduLevelRequirement().code()))
                    .column(property("r", EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().code()))
                    .column(DQLFunctions.count(DQLPredicateType.distinct, property("r", EnrRequestedCompetitionTA.id())))
                    .createStatement(getSession()).list();
            for (Object[] row : rows) {
                String level = (String) row[0];
                String kind = (String) row[1];
                Integer count = ((Number) row[2]).intValue();
                MultiKey key = new MultiKey(kind, EnrCompetitionTypeCodes.TARGET_ADMISSION, level);
                Integer total = enrolledCountMap.get(key); if (null == total) total = 0;
                enrolledCountMap.put(key, total + count);
            }
        }

        private void initPassed() {
            passedCountMap = new HashMap<>();
            // число абитуриентов, сдавших все ВИ
            // 1. в рейтинге признак «По всем вступительным испытаниям балл не ниже порогового» = да
            // 2. в рейтинге признак «Выбранный конкурс абитуриента активный» = да
            List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrRatingItem.class, "r")
                .where(eq(property("r", EnrRatingItem.statusRatingPositive()), value(Boolean.TRUE)))
                .where(eq(property("r", EnrRatingItem.requestedCompetitionActive()), value(Boolean.TRUE)))
                .where(eq(property("r", EnrRatingItem.competition().programSetOrgUnit()), value(programSetOrgUnit)))
                .group(property("r", EnrRatingItem.requestedCompetition().competition().type().code()))
                .group(property("r", EnrRatingItem.competition().eduLevelRequirement().code()))
                .column(property("r", EnrRatingItem.requestedCompetition().competition().type().code()))
                .column(property("r", EnrRatingItem.competition().eduLevelRequirement().code()))
                .column(DQLFunctions.count(DQLPredicateType.distinct, property("r", EnrRequestedCompetition.id())))
                .createStatement(getSession()).list();
            for (Object[] row : rows) {
                String type = (String) row[0];
                String level = (String) row[1];
                Integer count = ((Number) row[2]).intValue();
                MultiKey key = new MultiKey(type, level);
                Integer total = passedCountMap.get(key); if (null == total) total = 0;
                passedCountMap.put(key, total + count);
            }
       }
    }
}