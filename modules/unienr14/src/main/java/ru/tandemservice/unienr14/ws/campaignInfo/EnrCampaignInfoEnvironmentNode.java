// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.unienr14.ws.campaignInfo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Информация о приемной кампании для вебсервиса EnrCampaignInfoService
 *
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
@XmlRootElement
public class EnrCampaignInfoEnvironmentNode
{
    /**
     * Название приемной кампании
     */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /**
     * Название года обучения приемной кампании
     */
    @XmlAttribute(required = true)
    public String educationYearTitle;


    /** Справочник "Виды и уровни образования" */
    public EduLevelNode eduLevel = new EduLevelNode();

    /** Справочник "Формы обучения" */
    public EduProgramFormNode eduProgramForm = new EduProgramFormNode();

    /** Справочник "Виды образовательных программ" */
    public EduProgramKindNode eduProgramKind = new EduProgramKindNode();

    /** Справочник "Направления подготовки, профессии и специальности профессионального образования " */
    public EduProgramSubjectNode eduProgramSubject = new EduProgramSubjectNode();

    /** Справочник "Виды заявлений" */
    public RequestTypeNode requestType = new RequestTypeNode();

    /** Справочник "Виды приема" */
    public CompetitionTypeNode competitionType = new CompetitionTypeNode();

    /** Справочник "Вид возмещения затрат" */
    public CompensationTypeNode compensationType = new CompensationTypeNode();

    /** Справочник "Требования к уровню образования поступающих" */
    public EduLevelRequirementNode eduLevelRequirement = new EduLevelRequirementNode();

    /** Справочник "Разрешенный уровень образования для поступающих" */
    public EduLevelReqEduLevelNode eduLevelReqEduLevel = new EduLevelReqEduLevelNode();

    /** Справочник "Формы сдачи" */
    public ExamPassFormNode examPassForm = new ExamPassFormNode();

    /** Справочник "Типы вступительных испытаний" */
    public ExamTypeNode examType = new ExamTypeNode();

    /** Справочник "Предметы ЕГЭ" */
    public StateExamSubjectNode stateExamSubject = new StateExamSubjectNode();

    /** Используемые виды целевого приема в указанной приемной кампании */
    public TargetAdmissionKindNode targetAdmissionKind = new TargetAdmissionKindNode();


    /** Наборы ВИ в указанной приемной кампании */
    public ExamSetVariantNode examSetVariant = new ExamSetVariantNode();

    /** Дисциплины вступительных испытаний в указанной приемной кампании */
    public DisciplineNode discipline = new DisciplineNode();

    /** Группы дисциплин вступительных испытаний в указанной приемной кампании */
    public DisciplineGroupNode disciplineGroup = new DisciplineGroupNode();

    /** Все типы всех подразделений, ведущих прием в указанной приемной кампании */
    public OrgUnitTypeNode orgUnitType = new OrgUnitTypeNode();

    /** Все подразделения, ведущие прием в указанной приемной кампании */
    public OrgUnitNode orgUnit = new OrgUnitNode();

    /** Все наборы образовательных программ для приема и специальности для приема СПО данной ПК */
    public ProgramSetNode programSet = new ProgramSetNode();

    /** Все конкурсы в указанной приемной кампании */
    public CompetitionNode competition = new CompetitionNode();

    /** Простая строка таблицы */
    public static class Row implements Comparable<Row>
    {
        public Row() { }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /** Справочник "Виды и уровни образования" */
    public static class EduLevelNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Формы обучения" */
    public static class EduProgramFormNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды образовательных программ" */
    public static class EduProgramKindNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Направления подготовки, профессии и специальности профессионального образования " */
    public static class EduProgramSubjectNode { public List<Row> row = new ArrayList<Row>(); }


    /** Справочник "Формы сдачи" */
    public static class ExamPassFormNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Типы вступительных испытаний" */
    public static class ExamTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Предметы ЕГЭ" */
    public static class StateExamSubjectNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Вид возмещения затрат" */
    public static class CompensationTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды приема" */
    public static class CompetitionTypeNode
    {
        public static class CompetitionTypeRow extends Row
        {
            public CompetitionTypeRow()
            {
            }

            public CompetitionTypeRow(String shortTitle, String title, String id, int priority, String publicTitle, String compensationType)
            {
                super(shortTitle, title, id);
                this.priority = priority;
                this.publicTitle = publicTitle;
                this.compensationType = compensationType;
            }

            /**
             * Публичное название
             */
            @XmlAttribute(required = true)
            public String publicTitle;

            /**
             * Вид возмещения затрат
             */
            @XmlAttribute(required = true)
            public String compensationType;

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;
        }

        public List<CompetitionTypeRow> row = new ArrayList<CompetitionTypeRow>();
    }



    /** Справочник "Требования к уровню образования поступающих" */
    public static class EduLevelRequirementNode
    {
        public static class EduLevelRequirementRow extends Row
        {
            public EduLevelRequirementRow()
            {
            }

            public EduLevelRequirementRow(String shortTitle, String title, String id, String genitiveTitle, int priority)
            {
                super(shortTitle, title, id);
                this.priority = priority;
                this.genitiveTitle = genitiveTitle;
            }

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;

            /**
             * Название (родительный падеж)
             */
            public String genitiveTitle;

        }

        public List<EduLevelRequirementRow> row = new ArrayList<EduLevelRequirementRow>();
    }

    /**
     * Справочник "Виды заявлений"
     */
    public static class RequestTypeNode
    {
        public static class RequestTypeRow extends Row
        {
            public RequestTypeRow()
            {
            }

            public RequestTypeRow(String shortTitle, String title, String id, int priority)
            {
                super(shortTitle, title, id);
                this.priority = priority;
            }

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;
        }

        public List<RequestTypeRow> row = new ArrayList<RequestTypeRow>();
    }


    /**
     * Используемые виды целевого приема в указанной приемной кампании
     */
    public static class TargetAdmissionKindNode
    {
        public static class TargetAdmissionKindRow extends Row
        {
            public TargetAdmissionKindRow()
            {
            }

            public TargetAdmissionKindRow(String shortTitle, String title, String id, int priority)
            {
                super(shortTitle, title, id);
                this.priority = priority;
            }

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;
        }

        public List<TargetAdmissionKindRow> row = new ArrayList<TargetAdmissionKindRow>();
    }

    /**
     * Разрешенный уровень образования для поступающих
     */
    public static class EduLevelReqEduLevelNode
    {
        public static class EduLevelReqEduLevelNodeRow
        {
            public EduLevelReqEduLevelNodeRow()
            {

            }

            public EduLevelReqEduLevelNodeRow(String eduLevelReq, String eduLevel)
            {
                this.eduLevelReq = eduLevelReq;
                this.eduLevel = eduLevel;
            }

            /**
             * Требования к уровню образования поступающих
             * @see ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode.EduLevelRequirementNode
             */
            @XmlAttribute(required = true)
            public String eduLevelReq;

            /**
             * Виды и уровни образования
             * @see ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode.EduLevelNode
             */
            @XmlAttribute(required = true)
            public String eduLevel;

        }

        public List<EduLevelReqEduLevelNodeRow> row = new ArrayList<EduLevelReqEduLevelNodeRow>();
    }

    /**
     * Наборы ВИ в указанной приемной кампании
     */
    public static class ExamSetVariantNode
    {
        /**
         * Набор ВИ
         */
        public static class ExamSetVariantRow
        {
            /**
             * Id набора ВИ
             */
            @XmlAttribute(required = true)
            public String id;

            /**
             * Вступительное испытание набора ВИ
             */
            public static class EntranceDisciplineRow
            {
                /**
                 * Вид вступительного испытания
                 */
                @XmlAttribute(required = true)
                public String examType;

                /**
                 * Перечень разрешенных форм сдачи
                 * @see ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode.ExamPassFormNode
                 */
                @XmlAttribute(required = true)
                public List<String> passForms;

                /**
                 * Дисциплина вступительного испытания или группа дисциплин вступительных испытаний
                 *
                 * @see ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode.DisciplineNode
                 * @see ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode.DisciplineGroupNode
                 */
                @XmlAttribute(required = true)
                public String discipline;

                /**
                 * Предметы ЕГЭ, которые можно зачесть для данного ВИ
                 */
                @XmlAttribute
                public List<String> stateExamSubjects;


                /**
                 * Зачетный балл. Передается в целом числе со сдвигом на три порядка для дробной части - т.е. умноженный на 1000
                 */
                public long passMarkMultipliedBy1000;

                /**
                 * Приоритет
                 */
                @XmlAttribute
                public Integer priority;
            }

            /**
             * Все вступительные испытания по данному набору
             */
            public static class EntranceDisciplineNode
            {
                public List<EntranceDisciplineRow> row = new ArrayList<EntranceDisciplineRow>();
            }

            /**
             * Все вступительные испытания по данному набору
             */
            public EntranceDisciplineNode entranceDiscipline = new EntranceDisciplineNode();
        }

        public List<ExamSetVariantRow> row = new ArrayList<>();
    }

    /**
     * Дисциплины вступительных испытаний в указанной приемной кампании
     */
    public static class DisciplineNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Группы дисциплин вступительных испытаний в указанной приемной кампании
     */
    public static class DisciplineGroupNode
    {
        public static class GroupRow extends Row
        {
            public GroupRow()
            {
            }

            public GroupRow(String shortTitle, String title, String id, List<String> disciplines)
            {
                super(shortTitle, title, id);
                this.disciplines = disciplines;
            }

            /**
             * Список дисциплин вступительных испытаний в указанной приемной кампании
             * В xml отображается как список идентификаторов через пробел
             *
             * @see ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode.DisciplineNode
             */
            @XmlList
            @XmlAttribute(required = true)
            public List<String> disciplines;
        }

        public List<GroupRow> row = new ArrayList<GroupRow>();
    }

    /**
     * Все типы всех подразделений, ведущих прием, в указанной приемной кампании
     */
    public static class OrgUnitTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Все подразделения, ведущие прием, в указанной приемной кампании
     */
    public static class OrgUnitNode
    {
        /**
         * Подразделение
         */
        public static class OrgUnitRow extends Row
        {
            public OrgUnitRow()
            {
            }

            public OrgUnitRow(String shortTitle, String title, String id, String orgUnitType, String nominativeCaseTitle, String settlementTitle, String territorialTitle, String territorialShortTitle, String territorialFullTitle)
            {
                super(shortTitle, title, id);
                this.orgUnitType = orgUnitType;
                this.nominativeCaseTitle = nominativeCaseTitle;
                this.settlementTitle = settlementTitle;
                this.territorialTitle = territorialTitle;
                this.territorialShortTitle = territorialShortTitle;
                this.territorialFullTitle = territorialFullTitle;
            }

            /**
             * Тип подразделения
             *
             * @see OrgUnitTypeNode
             */
            @XmlAttribute(required = true)
            public String orgUnitType;

            /**
             * Печатное название подразделения в именительном падеже
             */
            @XmlAttribute
            public String nominativeCaseTitle;

            /**
             * Название населенного пункта из фактического адреса подразделения
             */
            @XmlAttribute
            public String settlementTitle;

            /**
             * Название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialTitle;

            /**
             * Сокращенное название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialShortTitle;

            /**
             *  Полное название в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialFullTitle;

        }

        public List<OrgUnitRow> row = new ArrayList<OrgUnitRow>();
    }

    /**
     * Конкурсы (комбинации условий поступления) для приема
     */
    public static class CompetitionNode { public List<CompetitionRow> row = new ArrayList<CompetitionRow>(); }

    /**
     * Конкурс (комбинация условий поступления) для приема
     */
    public static class CompetitionRow
    {
        /**
         * Название
         */
        @XmlAttribute
        public String title;

        /**
         * План приема
         */
        @XmlAttribute
        public Integer plan;

        /**
         * Кол-во заявлений
         */
        @XmlAttribute
        public Integer requests;

        /**
         * Количество отозванных заявлений (забравших документы)
         */
        @XmlAttribute
        public Integer requestsWithdrawal;

        /**
         * Набор ОП для приема
         *
         * @see ProgramSetNode
         */
        @XmlAttribute(required = true)
        public String programSet;

        /**
         * Подразделение, ведущее прием
         *
         * @see OrgUnitNode
         */
        @XmlAttribute(required = true)
        public String enrOrgUnit;

        /**
         * Формирующее подразделение
         *
         * @see OrgUnitNode
         */
        @XmlAttribute
        public String formativeOrgUnit;

        /**
         * Вид приема
         *
         * @see CompetitionTypeNode
         */
        @XmlAttribute(required = true)
        public String competitionType;

        /**
         * Требования к уровню образования поступающих
         *
         * @see EduLevelRequirementNode
         */
        @XmlAttribute(required = true)
        public String eduLevelRequirement;

        /**
         * Идентификатор конкурса
         */
        @XmlAttribute(required = true)
        public String id;

        /**
         * Набор ВИ
         */
        @XmlAttribute
        public String examSetVariant;
    }

    /**
     * Наборы образовательных программ для приема
     */
    public static class ProgramSetNode { public List<ProgramSetRow> row = new ArrayList<ProgramSetRow>(); }

    /**
     * Набор образовательных программ для приема
     */
    public static class ProgramSetRow
    {
        public static class ProgramSetItemRow extends Row
        {
            public ProgramSetItemRow()
            {
            }

            public ProgramSetItemRow(String shortTitle, String title, String id)
            {
                super(shortTitle, title, id);
            }

            /**
             * Название специализации
             */

            @XmlAttribute
            public String specTitle;

            /**
             * Общая направленность
             */
            @XmlAttribute
            public boolean rootSpec;

            /**
             * Продолжительность
             */
            @XmlAttribute
            public String durationTitle;

            /**
             * Продолжительность в месяцах
             */
            @XmlAttribute
            public Integer durationInMonths;

            /**
             * Особенность
             */
            @XmlAttribute
            public String programTrait;

            /**
             * Квалификация для ОП бакалавриата
             */
            @XmlAttribute
            public String bachelorQualificationShortTitle;

            /**
             * Углубленное изучение для СПО
             */
            @XmlAttribute
            public String secondaryInDepthStudyString;

            /**
             * Лиценз. подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String institutionOrgUnit;

            /**
             * Выпускающее подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String ownerOrgUnit;
        }

        /**
         * Идентификатор
         */
        @XmlAttribute(required = true)
        public String id;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Печатное название набора ОП
         */

        @XmlAttribute(required = true)
        public String printTitle;

        /**
         * Вид заявления
         *
         * @see RequestTypeNode
         */
        @XmlAttribute(required = true)
        public String requestType;

        /**
         * Направление (профессия, специальность)
         *
         * @see EduProgramSubjectNode
         */
        @XmlAttribute(required = true)
        public String eduProgramSubject;

        /**
         * Тип направления для перечня
         */
        @XmlAttribute(required = true)
        public String eduProgramSubjectType;

        /**
         * Форма обучения
         *
         * @see EduProgramFormNode
         */
        @XmlAttribute(required = true)
        public String eduProgramForm;

        /**
         * Вид обр. программы
         *
         * @see EduProgramKindNode
         */
        @XmlAttribute(required = true)
        public String eduProgramKind;

        /**
         * Уровень образования
         *
         * @see EduLevelNode
         */
        @XmlAttribute(required = true)
        public String eduLevel;

        /**
         *  Прием лиц, постоянно проживающих в Крыму.
         */
        @XmlAttribute(required = true)
        public boolean inCrimea;

        /**
         * Образовательные программы данного набора
         */
        public List<ProgramSetItemRow> eduProgram = new ArrayList<ProgramSetItemRow>();
    }

}
