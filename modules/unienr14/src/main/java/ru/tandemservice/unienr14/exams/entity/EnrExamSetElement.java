package ru.tandemservice.unienr14.exams.entity;

import java.util.Comparator;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import ru.tandemservice.unienr14.exams.entity.gen.EnrExamSetElementGen;

/**
 * Элемент набора ВИ
 */
public class EnrExamSetElement extends EnrExamSetElementGen implements ITitled
{
    public static enum Comparators implements Comparator<EnrExamSetElement>
    {
        byNumber() {
            @Override public int compare(EnrExamSetElement o1, EnrExamSetElement o2) {
                return Integer.compare(o1.getNumber(), o2.getNumber());
            }
        };
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15010372")
    @Override
    @EntityDSLSupport
    public String getTitle()
    {
        if (getValue() == null) {
            return this.getClass().getSimpleName();
        }
        StringBuilder elementTitle = new StringBuilder();

        elementTitle.append(getValue().getTitle());

        // затем сокращенное название типа (если испытание обычное - пропускаем) и признак профильности через запятую
        String typeTitle = getType().isUsual() ? null : StringUtils.trimToNull(getType().getShortTitle());
        if (null != typeTitle) {
            elementTitle.append(" [").append(typeTitle).append("]");
        }

        return elementTitle.toString();
    }


    @Override
    @EntityDSLSupport
    public String getShortTitle()
    {
        return getValue().getShortTitle();
    }

}