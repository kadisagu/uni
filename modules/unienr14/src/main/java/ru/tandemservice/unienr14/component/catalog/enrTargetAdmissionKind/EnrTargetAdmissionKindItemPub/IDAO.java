/**
 *$Id: IDAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrTargetAdmissionKind.EnrTargetAdmissionKindItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

/**
 * @author Alexander Shaburov
 * @since 23.05.13
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<EnrTargetAdmissionKind, Model>
{
}
