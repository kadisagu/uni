package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа дисциплин, используемая в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignDisciplineGroupGen extends EntityBase
 implements IEnrExamSetElementValue{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup";
    public static final String ENTITY_NAME = "enrCampaignDisciplineGroup";
    public static final int VERSION_HASH = 94505246;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DEFAULT_PASS_MARK_AS_LONG = "defaultPassMarkAsLong";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private long _defaultPassMarkAsLong;     // Зачетный балл по умолчанию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * Обновляется при сохранении группы (и демоном IEnrCampDisciplineGroupDaemonBean), как максимальный зачетный бал по умолчанию из дисциплин группы.
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     */
    @NotNull
    public long getDefaultPassMarkAsLong()
    {
        return _defaultPassMarkAsLong;
    }

    /**
     * @param defaultPassMarkAsLong Зачетный балл по умолчанию. Свойство не может быть null.
     */
    public void setDefaultPassMarkAsLong(long defaultPassMarkAsLong)
    {
        dirty(_defaultPassMarkAsLong, defaultPassMarkAsLong);
        _defaultPassMarkAsLong = defaultPassMarkAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignDisciplineGroupGen)
        {
            setEnrollmentCampaign(((EnrCampaignDisciplineGroup)another).getEnrollmentCampaign());
            setTitle(((EnrCampaignDisciplineGroup)another).getTitle());
            setShortTitle(((EnrCampaignDisciplineGroup)another).getShortTitle());
            setDefaultPassMarkAsLong(((EnrCampaignDisciplineGroup)another).getDefaultPassMarkAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignDisciplineGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignDisciplineGroup.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignDisciplineGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "defaultPassMarkAsLong":
                    return obj.getDefaultPassMarkAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "defaultPassMarkAsLong":
                    obj.setDefaultPassMarkAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "defaultPassMarkAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "defaultPassMarkAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "defaultPassMarkAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignDisciplineGroup> _dslPath = new Path<EnrCampaignDisciplineGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignDisciplineGroup");
    }
            

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * Обновляется при сохранении группы (и демоном IEnrCampDisciplineGroupDaemonBean), как максимальный зачетный бал по умолчанию из дисциплин группы.
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getDefaultPassMarkAsLong()
     */
    public static PropertyPath<Long> defaultPassMarkAsLong()
    {
        return _dslPath.defaultPassMarkAsLong();
    }

    public static class Path<E extends EnrCampaignDisciplineGroup> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Long> _defaultPassMarkAsLong;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrCampaignDisciplineGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrCampaignDisciplineGroupGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * Обновляется при сохранении группы (и демоном IEnrCampDisciplineGroupDaemonBean), как максимальный зачетный бал по умолчанию из дисциплин группы.
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup#getDefaultPassMarkAsLong()
     */
        public PropertyPath<Long> defaultPassMarkAsLong()
        {
            if(_defaultPassMarkAsLong == null )
                _defaultPassMarkAsLong = new PropertyPath<Long>(EnrCampaignDisciplineGroupGen.P_DEFAULT_PASS_MARK_AS_LONG, this);
            return _defaultPassMarkAsLong;
        }

        public Class getEntityClass()
        {
            return EnrCampaignDisciplineGroup.class;
        }

        public String getEntityName()
        {
            return "enrCampaignDisciplineGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
