package ru.tandemservice.unienr14.competition.entity;

import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetMasterGen;

/**
 * Набор ОП для приема (магистратура)
 *
 * Определяет перечень ОП, на который формируются конкурсы, для приема в магистратуру.
 */
public class EnrProgramSetMaster extends EnrProgramSetMasterGen implements EnrProgramSetBase.ISingleExamSetOwner
{
    public EnrProgramSetMaster()
    {
    }

    @Override
    public boolean isContractDividedByEduLevel()
    {
        return false;
    }

    @Override
    public boolean isValidExamSetVariants()
    {
        return true;
    }

    @Override
    public boolean isMinisterialDividedByEduLevel()
    {
        return false;
    }
}