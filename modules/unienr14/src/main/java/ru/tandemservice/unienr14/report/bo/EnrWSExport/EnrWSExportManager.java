/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrWSExport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author rsizonenko
 * @since 27.06.2014
 */
@Configuration
public class EnrWSExportManager extends BusinessObjectManager {
}
