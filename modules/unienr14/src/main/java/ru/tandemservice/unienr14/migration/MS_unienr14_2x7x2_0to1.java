package ru.tandemservice.unienr14.migration;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_unienr14_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        final Map<String, Long> requestTypeCode2IdMap = MigrationUtils.getCatalogCode2IdMap(tool, "enr14_c_request_type_t");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantAchievementKind
        // создана новая сущность
        {
            // создать таблицу
            tool.createTable(new DBTable("enr14_achievement_kind_t",
                                         new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                         new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                         new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                         new DBColumn("title_p", DBType.createVarchar(1200)),
                                         new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
                                         new DBColumn("requesttype_id", DBType.LONG).setNullable(false),
                                         new DBColumn("description_p", DBType.TEXT)
            ));

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrEntrantAchievementKind");

            // По объектам «Вид индивидуального достижения в ПК» вычислить все уникальные комбинации (название, вид заявления).
            // Для каждой такой пары создать элемент справочника «Вид инд. достижения»,
            // поле «Описание» заполнить по первому подходящему (с таким же названием и видом заявления) объекту «Вид индивидуального достижения в ПК»,
            // сокр. название скопировать из названия.

            // Поле «Описание» заполнить по первому подходящему (с таким же названием и видом заявления) объекту «Вид индивидуального достижения в ПК»
            SQLSelectQuery descriptionSelect = new SQLSelectQuery()
                    .from(SQLFrom.table("enr14_achievement_type_t", "a"))
                    .top(1).column("a.description_p")
                    .where("a.requesttype_id=t.requesttype_id and a.title_p=t.title_p and a.description_p is not null")
                    .order("a.id", false);

            // подъем уникальных пар сразу с описанием
            List<Object[]> items = tool.executeQuery(
                    MigrationUtils.processor(Long.class, String.class, String.class),
                    "select t.requesttype_id, t.title_p, (" + tool.getDialect().getSQLTranslator().toSql(descriptionSelect) + ")"
                            + " from enr14_achievement_type_t t group by t.requesttype_id, t.title_p"
            );

            // Собираем уникальные пары и сразу присваиваем код будущего элемента справочника
            // В качестве ключа в мапе пара [Вид заявления]-[Название], в качетве значения [код будущего элемента справочника], [сокр. название], [описание]
            Map<Pair<Long, String>, String[]> table = new HashMap<>();
            int codeIdx = 1;
            for (Object[] item : items)
            {
                Long requestTypeId = (Long) item[0];
                String title = (String) item[1];
                String code = String.valueOf(codeIdx++);
                String description = (String) item[2];
                table.put(new Pair<>(requestTypeId, title), new String[]{code, /* сокр. название скопировать из названия */ title, description});
            }

            // Итоговое сочинение - единственный системный элемент. Его добавлем в мапу после остальных (чтобы перетер, если такое уже есть)
            table.put(new Pair<>(requestTypeCode2IdMap.get("1"), "Итоговое сочинение"), /* "1" - бакалавриат, специалитет */
                      new String[]{"bs_final_composition", "соч", "Итоговое сочинение в выпускных классах организаций, реализующих образовательные программы среднего общего образования"});

            // Теперь заполняем справочник
            BatchInsertBuilder ins = new BatchInsertBuilder(entityCode, "requesttype_id", "title_p", "code_p", "shorttitle_p", "description_p");
            for (Map.Entry<Pair<Long, String>, String[]> entry : table.entrySet())
            {
                Long requestTypeId = entry.getKey().getX();
                String title = entry.getKey().getY();
                String code = entry.getValue()[0];
                String shortTitle = entry.getValue()[1];
                String description = entry.getValue()[2];
                ins.addRow(requestTypeId, title, code, shortTitle, description);
            }
            ins.executeInsert(tool, "enr14_achievement_kind_t");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantAchievementSettings & сущность enrEnrollmentCampaignSettings
        {
            tool.createTable(new DBTable("enr14_achievement_sets_t",
                                         new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                         new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                         new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                         new DBColumn("requesttype_id", DBType.LONG).setNullable(false),
                                         new DBColumn("mxttlachvmntmrkaslng_p", DBType.LONG).setNullable(false)
            ));

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrEntrantAchievementSettings");

            // Для всех ПК создать объекты «Настройки индивидуальных достижений в ПК» для всех видов заявлений.
            // Заполнить их поля «Ограничение на сумму баллов индивидуальных достижений» по полям «Настройки приемной кампании»
            // id вида заявления -> название старой колонки в таблице настроек ПК
            Map<Long, String> map = ImmutableMap.of(
                    requestTypeCode2IdMap.get("1"), "mxttlachvmntmrkaslngbs_p", // Программы бакалавриата, специалитета
                    requestTypeCode2IdMap.get("2"), "mxttlachvmntmrkaslngmstr_p", // Программы магистратуры
                    requestTypeCode2IdMap.get("3"), "mxttlachvmntmrkaslnghghr_p", // Направления подготовки кадров высшей квалификации
                    requestTypeCode2IdMap.get("4"), "mxttlachvmntmrkaslngspo_p" // Среднее профессиональное образование
            );
            BatchInsertBuilder ins = new BatchInsertBuilder(entityCode, "enrollmentcampaign_id", "requesttype_id", "mxttlachvmntmrkaslng_p");
            for (Map.Entry<Long, String> entry : map.entrySet())
            {
                List<Object[]> items = tool.executeQuery(
                        MigrationUtils.processor(Long.class, Long.class),
                        "select c.id, s." + entry.getValue() + " from enr14_campaign_t c inner join enr14_campaign_settings_t s on c.settings_id=s.id"
                );
                for (Object[] item : items)
                {
                    Long enrCampaignId = (Long) item[0];
                    Long value = Math.max((Long) item[1], 0L); // Если какой-либо балл отрицательный, сохранять 0
                    ins.addRow(enrCampaignId, entry.getKey(), value);
                }
            }
            ins.executeInsert(tool, "enr14_achievement_sets_t");

            // После этого лишние поля «Настройки приемной кампании» можно удалить
            tool.dropColumn("enr14_campaign_settings_t", "lmtachvmntmrkwtheddcmrk_p");
            tool.dropColumn("enr14_campaign_settings_t", "ddavrgeddcmrkfrachvmntttlbs_p");
            tool.dropColumn("enr14_campaign_settings_t", "ddavrgeddcmrkfrachvmntttlspo_p");
            tool.dropColumn("enr14_campaign_settings_t", "ddavrgeddcmrkfrachvmntttlmst_p");
            tool.dropColumn("enr14_campaign_settings_t", "ddavrgeddcmrkfrachvmntttlhgh_p");
            tool.dropColumn("enr14_campaign_settings_t", "mxttlachvmntmrkaslngbs_p");
            tool.dropColumn("enr14_campaign_settings_t", "mxttlachvmntmrkaslngspo_p");
            tool.dropColumn("enr14_campaign_settings_t", "mxttlachvmntmrkaslngmstr_p");
            tool.dropColumn("enr14_campaign_settings_t", "mxttlachvmntmrkaslnghghr_p");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantAchievement. Создано обязательное свойство markAsLong. Установить всем объектам «Индивидуальное достижение» баллы в 0
        {
            tool.createColumn("enr14_entrant_achievement_t", new DBColumn("markaslong_p", DBType.LONG));
            tool.executeUpdate("update enr14_entrant_achievement_t set markaslong_p=? where markaslong_p is null", 0L);
            tool.setColumnNullable("enr14_entrant_achievement_t", "markaslong_p", false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantAchievementType (Вид индивидуального достижения в ПК)
        {
            // создано обязательное свойство achievementKind
            tool.createColumn("enr14_achievement_type_t", new DBColumn("achievementkind_id", DBType.LONG));

            // Для каждого объекта «Вид индивидуального достижения в ПК» установить ссылку на соответствующий элемент справочника (с таким же видом заявления и названием)
            SQLUpdateQuery upd = new SQLUpdateQuery("enr14_achievement_type_t", "t");
            upd.set("achievementkind_id", "k.id");
            upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("enr14_achievement_kind_t", "k"), "k.requesttype_id=t.requesttype_id and k.title_p=t.title_p");
            int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
            if (Debug.isEnabled())
                System.out.println(this.getClass().getSimpleName() + ": " + ret + " achievement types migrated to new catalog (achievement kind)");
            tool.setColumnNullable("enr14_achievement_type_t", "achievementkind_id", false);

            // создано обязательное свойство marked
            tool.createColumn("enr14_achievement_type_t", new DBColumn("marked_p", DBType.BOOLEAN));
            // в поле "Оценивается" сохранить false
            tool.executeUpdate("update enr14_achievement_type_t set marked_p=? where marked_p is null", false);
            tool.setColumnNullable("enr14_achievement_type_t", "marked_p", false);

            // если максимальный балл отрицательный, сохранить 0
            tool.executeUpdate("update enr14_achievement_type_t set achievementmarkaslong_p=? where achievementmarkaslong_p<0", 0L);

            // лишние поля сущности «Вид индивидуального достижения в ПК» можно удалить
            tool.dropColumn("enr14_achievement_type_t", "requesttype_id");
            tool.dropColumn("enr14_achievement_type_t", "title_p");
            tool.dropColumn("enr14_achievement_type_t", "description_p");
        }
    }
}