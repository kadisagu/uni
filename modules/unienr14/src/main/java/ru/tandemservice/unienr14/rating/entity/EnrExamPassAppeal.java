package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.rating.entity.gen.*;

/**
 * Апелляция на результат ВИ
 */
public class EnrExamPassAppeal extends EnrExamPassAppealGen
{

    @EntityDSLSupport(parts = EnrExamPassDiscipline.P_MARK_AS_LONG)
    @Override
    public String getMarkAsString() {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getMarkAsLong());
    }

    @EntityDSLSupport(parts = EnrExamPassDiscipline.P_MARK_AS_LONG)
    @Override
    public Double getMarkAsDouble() {
        return (double) getMarkAsLong() / (double) 1000;
    }
}