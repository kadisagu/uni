/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.ui.ReasonAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;

/**
 * @author oleyba
 * @since 7/7/14
 */
@Input({
    @Bind(key = "catalogItemId", binding = "holder.id"),
    @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EnrCatalogOrdersReasonAddEditUI extends BaseCatalogItemAddEditUI<EnrOrderReason>
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getProperties().remove(EnrOrderReason.P_PRINT_TITLE);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return "ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.ui.ReasonAddEdit.AdditProps";
    }
}