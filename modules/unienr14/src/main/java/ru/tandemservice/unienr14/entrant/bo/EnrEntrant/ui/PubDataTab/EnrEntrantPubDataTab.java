/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDataTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubContactDataTab.EnrEntrantPubContactDataTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubMainDataTab.EnrEntrantPubMainDataTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubMilitaryDataTab.EnrEntrantPubMilitaryDataTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubNextOfKinTab.EnrEntrantPubNextOfKinTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubScienceDataTab.EnrEntrantPubScienceDataTab;

/**
 * @author oleyba
 * @since 4/10/13
 */
@Configuration
public class EnrEntrantPubDataTab extends BusinessComponentManager
{
    public static final String TAB_PANEL = "entrantDataTabPanel";
    public static final String TAB_PANEL_REGION_NAME = EnrEntrantPub.TAB_PANEL_SUB_REGION_NAME;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

    @Bean
    public TabPanelExtPoint entrantDataTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
            .addTab(componentTab("mainDataTab", EnrEntrantPubMainDataTab.class).permissionKey("enr14EntrantPubMainDataTabView"))
            .addTab(componentTab("contactDataTab", EnrEntrantPubContactDataTab.class).permissionKey("enr14EntrantPubContactDataTabView"))
            .addTab(componentTab("nextOfKinTab", EnrEntrantPubNextOfKinTab.class).permissionKey("enr14EntrantPubNextOfKinTabView"))
            .addTab(componentTab("scienceTab", EnrEntrantPubScienceDataTab.class).permissionKey("enr14EntrantPubScienceTabView"))
            .addTab(componentTab("militaryTab", EnrEntrantPubMilitaryDataTab.class).permissionKey("enr14EntrantPubMilitaryTabView"))
            .create();
    }
}
