package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.rating.entity.gen.EnrEntrantMarkSourceStateExamGen;

/**
 * Основание балла (ЕГЭ) (2013)
 *
 * Создается и обновляется демоном на основе свидетельств ЕГЭ абитуриента IEnrRatingDaemonDao#doRefreshSource4StateExam (создание оснований по данным свидетельств ЕГЭ).
 */
public class EnrEntrantMarkSourceStateExam extends EnrEntrantMarkSourceStateExamGen
{
    public EnrEntrantMarkSourceStateExam() {}

    public EnrEntrantMarkSourceStateExam(EnrChosenEntranceExamForm chosenEntranceExamForm, EnrEntrantStateExamResult stateExamSubjectMark) {
        this.setChosenEntranceExamForm(chosenEntranceExamForm);
        this.setStateExamResult(stateExamSubjectMark);
    }

    public EnrEntrantMarkSourceStateExam(EnrChosenEntranceExamForm chosenEntranceExamForm, EnrEntrantStateExamResult stateExamSubjectMark, long markAsLong) {
        this(chosenEntranceExamForm, stateExamSubjectMark);
        this.setMarkAsLong(markAsLong);
    }


    @Override
    public String getInfoString() {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getChosenEntranceExamForm().getMarkAsDoubleNullSafe()) + " — ЕГЭ" + (getStateExamResult().getDocumentNumber() == null ? "" : "(" + getStateExamResult().getDocumentNumber() + ")");
    }

    @Override
    public String getDocumentTitleForPrint() {
        return getStateExamResult().getDocumentNumber();
    }
}