/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.Pub;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit.EnrEnrollmentCampaignAddEdit;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author oleyba
 * @since 6/25/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "enrollmentCampaign.id"),
    @Bind(key = "selectedPage", binding = "selectedPage")
})
public class EnrEnrollmentCampaignPubUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign = new EnrEnrollmentCampaign();
    private List<EnrOrgUnit> _enrOrgUnitList = Lists.newArrayList();
    private String _selectedPage;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(DataAccessServices.dao().getNotNull(EnrEnrollmentCampaign.class, getEnrollmentCampaign().getId()));
        setEnrOrgUnitList(DataAccessServices.dao().getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), getEnrollmentCampaign(), EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s()));
    }

    public void onClickEditEnrollmentCampaign()
    {
        _uiActivation.asRegion(EnrEnrollmentCampaignAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getEnrollmentCampaign().getId()).activate();
    }

    // getters and setters


    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public List<EnrOrgUnit> getEnrOrgUnitList()
    {
        return _enrOrgUnitList;
    }

    public void setEnrOrgUnitList(List<EnrOrgUnit> enrOrgUnitList)
    {
        _enrOrgUnitList = enrOrgUnitList;
    }

    public String getEnrOrgUnitTitles()
    {
        Set<String> enrOrgUnitTitles = new TreeSet<>();
        for (EnrOrgUnit ou : getEnrOrgUnitList())
        {
            String title = ou.getInstitutionOrgUnit().getOrgUnit().getFullTitle();
            enrOrgUnitTitles.add(title);
        }
        return RowCollectionFormatter.INSTANCE.format(enrOrgUnitTitles);
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }
}