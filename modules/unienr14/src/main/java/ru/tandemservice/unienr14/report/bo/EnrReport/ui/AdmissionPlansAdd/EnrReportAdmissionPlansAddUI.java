/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportParallelSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Calendar;
import java.util.Date;

/**
 * @author rsizonenko
 * @since 17.10.2014
 */
public class EnrReportAdmissionPlansAddUI extends UIPresenter {

    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrReportParallelSelector parallelSelector = new EnrReportParallelSelector();
    private EnrReportStageSelector stageSelector = new EnrReportStageSelector();
    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date _firstWaveDateFrom;
    private Date _firstWaveDateTo;

    private Date _secondWaveDateFrom;
    private Date _secondWaveDateTo;

    private Date _thirdWaveDateFrom;
    private Date _thirdWaveDateTo;

    public void onClickApply() {
        validate();
        Long reportId = (EnrReportManager.instance().admissionPlansDao().createReport(this));
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        dateSelector.refreshDates(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        Calendar c = Calendar.getInstance();
        c.setTime(getEnrollmentCampaign().getDateFrom());
        int year = c.get(Calendar.YEAR);


        if(_firstWaveDateFrom == null)
        {
            c.set(year, 6, 25, 0, 0, 0);
            _firstWaveDateFrom = c.getTime();

        }
        if(_firstWaveDateTo == null)
        {
            c.set(year, 7, 5, 0, 0, 0);
            _firstWaveDateTo = c.getTime();
        }

        if(_secondWaveDateFrom == null)
        {
            c.set(year, 7, 6, 0, 0, 0);
            _secondWaveDateFrom = c.getTime();
        }
        if(_secondWaveDateTo == null)
        {
            c.set(year, 7, 11, 0, 0, 0);
            _secondWaveDateTo = c.getTime();
        }

        if(_thirdWaveDateFrom == null)
        {
            c.set(year, 7, 12, 0, 0, 0);
            _thirdWaveDateFrom = c.getTime();
        }
        if(_thirdWaveDateTo == null)
        {
            c.set(year, 8, 1, 0, 0, 0);
            _thirdWaveDateTo = c.getTime();
        }

        configUtil(getCompetitionFilterAddon());
    }

    private void validate()
    {
        if(_firstWaveDateFrom.after(_firstWaveDateTo))
            _uiSupport.error("Дата начала этапа должна быть меньше даты окончания этапа." , "firstStageDateFrom", "firstStageDateTo");

        if(_secondWaveDateFrom.after(_secondWaveDateTo))
            _uiSupport.error("Дата начала этапа должна быть меньше даты окончания этапа." , "secondStageDateFrom", "secondStageDateTo");

        if(_thirdWaveDateFrom.after(_thirdWaveDateTo))
            _uiSupport.error("Дата начала этапа должна быть меньше даты окончания этапа." , "extStageDateFrom", "extStageDateTo");

        if(_firstWaveDateTo.after(_secondWaveDateFrom))
            _uiSupport.error("Дата окончания предыдущего этапа должна быть меньше даты начала последующего этапа." , "firstStageDateFrom", "firstStageDateTo", "secondStageDateFrom", "secondStageDateTo");

        if(_secondWaveDateTo.after(_thirdWaveDateFrom))
            _uiSupport.error("Дата окончания предыдущего этапа должна быть меньше даты начала последующего этапа." , "extStageDateFrom", "extStageDateTo", "secondStageDateFrom", "secondStageDateTo");

        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util
                .clearWhereFilter()
                .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrReportParallelSelector getParallelSelector() {
        return parallelSelector;
    }

    public Date getThirdWaveDateTo() {
        return _thirdWaveDateTo;
    }

    public void setThirdWaveDateTo(Date _thirdWaveDateTo) {
        this._thirdWaveDateTo = _thirdWaveDateTo;
    }

    public Date getThirdWaveDateFrom() {
        return _thirdWaveDateFrom;
    }

    public void setThirdWaveDateFrom(Date _thirdWaveDateFrom) {
        this._thirdWaveDateFrom = _thirdWaveDateFrom;
    }

    public Date getSecondWaveDateTo() {
        return _secondWaveDateTo;
    }

    public void setSecondWaveDateTo(Date _secondWaveDateTo) {
        this._secondWaveDateTo = _secondWaveDateTo;
    }

    public Date getSecondWaveDateFrom() {
        return _secondWaveDateFrom;
    }

    public void setSecondWaveDateFrom(Date _secondWaveDateFrom) {
        this._secondWaveDateFrom = _secondWaveDateFrom;
    }

    public Date getFirstWaveDateTo() {
        return _firstWaveDateTo;
    }

    public void setFirstWaveDateTo(Date _firstWaveDateTo) {
        this._firstWaveDateTo = _firstWaveDateTo;
    }

    public Date getFirstWaveDateFrom() {
        return _firstWaveDateFrom;
    }

    public void setFirstWaveDateFrom(Date _firstWaveDateFrom) {
        this._firstWaveDateFrom = _firstWaveDateFrom;
    }

    public EnrReportStageSelector getStageSelector() {
        return stageSelector;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}