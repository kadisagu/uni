package ru.tandemservice.unienr14.rating.daemon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;

import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;

/**
 * Базовый класс для механизма обновления оснований баллов на основе внешних данных.
 * Нужен для последовательного запуска фаз remove, insert, update с автоматической генерацией dql и обработкой результатов.
 * @author vdanilov
 * @param <T>
 */
abstract public class EnrRatingDaemonMarkSourceUpdate<T extends EnrEntrantMarkSource> {

    protected final Class<T> klass;
    protected final Session session;

    public EnrRatingDaemonMarkSourceUpdate(final Class<T> klass, final Session session) {
        this.klass = klass;
        this.session = session;
    }

    /**
     * Выставляет блокировку на указанный класс.
     * Последовательно выполняет фазы remove, insert, update.
     * @return количество измененных объектов в базе
     */
    public int execute() {
        NamedSyncInTransactionCheckLocker.register(session, klass.getName());
        int updates = 0;

        Debug.begin("source-updater("+klass.getSimpleName()+").remove");
        try {
            updates += executeRemove();
        } finally {
            Debug.end();
        }

        Debug.begin("source-updater("+klass.getSimpleName()+").insert");
        try {
            updates += executeInsert();
        } finally {
            Debug.end();
        }

        Debug.begin("source-updater("+klass.getSimpleName()+").update");
        try {
            updates += executeUpdate();
        } finally {
            Debug.end();
        }

        session.flush();
        return updates;
    }

    /**
     * генерирует и запускает запрос «delete from klass where not exists (query + join)».
     * @return количество удаленных объектов в базе
     */
    protected int executeRemove() {
        return EnrRatingDaemonDao.executeAndClear(
            new DQLDeleteBuilder(klass)
            .where(notExists(
                buildMainDataSource()
                .where(buildJoinExpression(null))
                .buildQuery()
            )),
            session
        );
    }

    /**
     * генерирует и запускает запрос «query where not exists (klass + join)»,
     * для каждой строки результата запускает insertSource.
     * @return количество добавленных значений а базе
     */
    protected int executeInsert() {
        int updates = 0;
        final List<Object[]> rows = buildMainDataSource()
        .where(notExists(
            new DQLSelectBuilder()
            .fromEntity(klass, "src")
            .where(buildJoinExpression("src"))
            .buildQuery()
        ))
        .createStatement(session).list();

        if (rows.size() > 0) {
            for (final Object[] row: rows) {
                if (insertSource(row)) {
                    updates ++;
                }
            }

            session.flush();
            session.clear();
        }
        return updates;
    }

    /**
     * По умолчанию не делает ничего (т.к. перечень объектов после фаз delete и insert соответсвует первоначальному запросу),
     * значения в основаниях автоматически посчитано быть не может - метод нужно переопределить и написать запрос на обновление,
     * используя buildMainDataSource и buildJoinExpression (либо информацию об их структуре).
     * @return количество объектов, которые изменились
     */
    protected int executeUpdate() {
        return 0;
    }

    /**
     * @return формирует основной запрос к внешним данным (в любой форме)
     */
    protected abstract DQLSelectBuilder buildMainDataSource();

    /**
     * формирует правила сопоставления запроса из buildMainDataSource и запроса к основаниям
     * @param srcAlias алиас основания
     * @return правила сопоставления запросов
     */
    protected abstract IDQLExpression buildJoinExpression(String srcAlias);

    /**
     * Вызывается на фазе insert для каждой строки основного запроса, для которой нет соответсвующего основания.
     * Метод должен добавить в базу объект, соответствующий данным строки.
     * @param row строка
     * @return true, если строка добавляется, false если добавление не произошло (последнее крайне нежелательно - лучше исправляйте buildMainDataSource)
     */
    protected abstract boolean insertSource(Object[] row);

}