package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;

/**
 * Факт использования особого права
 *
 * Интерфейс для фактов использования особого права - выбранного конкурса по квоте, без ВИ, преимущества в заявлении и зачтения балла по олимпиаде.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrEntrantBenefitStatementGen extends InterfaceStubBase
 implements IEnrEntrantBenefitStatement{
    public static final int VERSION_HASH = 1622219854;

    public static final String L_ENTRANT = "entrant";
    public static final String L_BENEFIT_CATEGORY = "benefitCategory";
    public static final String P_BENEFIT_STATEMENT_TITLE = "benefitStatementTitle";

    private EnrEntrant _entrant;
    private EnrBenefitCategory _benefitCategory;


    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }


    public EnrBenefitCategory getBenefitCategory()
    {
        return _benefitCategory;
    }

    public void setBenefitCategory(EnrBenefitCategory benefitCategory)
    {
        _benefitCategory = benefitCategory;
    }

    private static final Path<IEnrEntrantBenefitStatement> _dslPath = new Path<IEnrEntrantBenefitStatement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement");
    }
            

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Категория особого права.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement#getBenefitCategory()
     */
    public static EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
    {
        return _dslPath.benefitCategory();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement#getBenefitStatementTitle()
     */
    public static SupportedPropertyPath<String> benefitStatementTitle()
    {
        return _dslPath.benefitStatementTitle();
    }

    public static class Path<E extends IEnrEntrantBenefitStatement> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrBenefitCategory.Path<EnrBenefitCategory> _benefitCategory;
        private SupportedPropertyPath<String> _benefitStatementTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Категория особого права.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement#getBenefitCategory()
     */
        public EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
        {
            if(_benefitCategory == null )
                _benefitCategory = new EnrBenefitCategory.Path<EnrBenefitCategory>(L_BENEFIT_CATEGORY, this);
            return _benefitCategory;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement#getBenefitStatementTitle()
     */
        public SupportedPropertyPath<String> benefitStatementTitle()
        {
            if(_benefitStatementTitle == null )
                _benefitStatementTitle = new SupportedPropertyPath<String>(IEnrEntrantBenefitStatementGen.P_BENEFIT_STATEMENT_TITLE, this);
            return _benefitStatementTitle;
        }

        public Class getEntityClass()
        {
            return IEnrEntrantBenefitStatement.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
