package ru.tandemservice.unienr14.sec.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import org.tandemframework.shared.organization.sec.entity.IRoleAssignment;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Назначение шаблонной роли для ОК принципалу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RoleAssignmentTemplateEnrCommissionGen extends EntityBase
 implements IRoleAssignment, INaturalIdentifiable<RoleAssignmentTemplateEnrCommissionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission";
    public static final String ENTITY_NAME = "roleAssignmentTemplateEnrCommission";
    public static final int VERSION_HASH = -2043934029;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROLE_CONFIG = "roleConfig";
    public static final String L_PRINCIPAL_CONTEXT = "principalContext";
    public static final String L_ENROLLMENT_COMMISSION = "enrollmentCommission";

    private RoleConfigTemplate _roleConfig;     // Конфигурация шаблонной роли
    private IPrincipalContext _principalContext;     // Контекст принципала
    private EnrEnrollmentCommission _enrollmentCommission;     // Отборочная комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Конфигурация шаблонной роли. Свойство не может быть null.
     */
    @NotNull
    public RoleConfigTemplate getRoleConfig()
    {
        return _roleConfig;
    }

    /**
     * @param roleConfig Конфигурация шаблонной роли. Свойство не может быть null.
     */
    public void setRoleConfig(RoleConfigTemplate roleConfig)
    {
        dirty(_roleConfig, roleConfig);
        _roleConfig = roleConfig;
    }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     */
    @NotNull
    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    /**
     * @param principalContext Контекст принципала. Свойство не может быть null.
     */
    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && principalContext!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  principalContext instanceof IEntity ? EntityRuntime.getMeta((IEntity) principalContext) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_principalContext, principalContext);
        _principalContext = principalContext;
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    /**
     * @param enrollmentCommission Отборочная комиссия. Свойство не может быть null.
     */
    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        dirty(_enrollmentCommission, enrollmentCommission);
        _enrollmentCommission = enrollmentCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RoleAssignmentTemplateEnrCommissionGen)
        {
            if (withNaturalIdProperties)
            {
                setRoleConfig(((RoleAssignmentTemplateEnrCommission)another).getRoleConfig());
                setPrincipalContext(((RoleAssignmentTemplateEnrCommission)another).getPrincipalContext());
                setEnrollmentCommission(((RoleAssignmentTemplateEnrCommission)another).getEnrollmentCommission());
            }
        }
    }

    public INaturalId<RoleAssignmentTemplateEnrCommissionGen> getNaturalId()
    {
        return new NaturalId(getRoleConfig(), getPrincipalContext(), getEnrollmentCommission());
    }

    public static class NaturalId extends NaturalIdBase<RoleAssignmentTemplateEnrCommissionGen>
    {
        private static final String PROXY_NAME = "RoleAssignmentTemplateEnrCommissionNaturalProxy";

        private Long _roleConfig;
        private Long _principalContext;
        private Long _enrollmentCommission;

        public NaturalId()
        {}

        public NaturalId(RoleConfigTemplate roleConfig, IPrincipalContext principalContext, EnrEnrollmentCommission enrollmentCommission)
        {
            _roleConfig = ((IEntity) roleConfig).getId();
            _principalContext = ((IEntity) principalContext).getId();
            _enrollmentCommission = ((IEntity) enrollmentCommission).getId();
        }

        public Long getRoleConfig()
        {
            return _roleConfig;
        }

        public void setRoleConfig(Long roleConfig)
        {
            _roleConfig = roleConfig;
        }

        public Long getPrincipalContext()
        {
            return _principalContext;
        }

        public void setPrincipalContext(Long principalContext)
        {
            _principalContext = principalContext;
        }

        public Long getEnrollmentCommission()
        {
            return _enrollmentCommission;
        }

        public void setEnrollmentCommission(Long enrollmentCommission)
        {
            _enrollmentCommission = enrollmentCommission;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof RoleAssignmentTemplateEnrCommissionGen.NaturalId) ) return false;

            RoleAssignmentTemplateEnrCommissionGen.NaturalId that = (NaturalId) o;

            if( !equals(getRoleConfig(), that.getRoleConfig()) ) return false;
            if( !equals(getPrincipalContext(), that.getPrincipalContext()) ) return false;
            if( !equals(getEnrollmentCommission(), that.getEnrollmentCommission()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRoleConfig());
            result = hashCode(result, getPrincipalContext());
            result = hashCode(result, getEnrollmentCommission());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRoleConfig());
            sb.append("/");
            sb.append(getPrincipalContext());
            sb.append("/");
            sb.append(getEnrollmentCommission());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RoleAssignmentTemplateEnrCommissionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RoleAssignmentTemplateEnrCommission.class;
        }

        public T newInstance()
        {
            return (T) new RoleAssignmentTemplateEnrCommission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "roleConfig":
                    return obj.getRoleConfig();
                case "principalContext":
                    return obj.getPrincipalContext();
                case "enrollmentCommission":
                    return obj.getEnrollmentCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "roleConfig":
                    obj.setRoleConfig((RoleConfigTemplate) value);
                    return;
                case "principalContext":
                    obj.setPrincipalContext((IPrincipalContext) value);
                    return;
                case "enrollmentCommission":
                    obj.setEnrollmentCommission((EnrEnrollmentCommission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "roleConfig":
                        return true;
                case "principalContext":
                        return true;
                case "enrollmentCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "roleConfig":
                    return true;
                case "principalContext":
                    return true;
                case "enrollmentCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "roleConfig":
                    return RoleConfigTemplate.class;
                case "principalContext":
                    return IPrincipalContext.class;
                case "enrollmentCommission":
                    return EnrEnrollmentCommission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RoleAssignmentTemplateEnrCommission> _dslPath = new Path<RoleAssignmentTemplateEnrCommission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RoleAssignmentTemplateEnrCommission");
    }
            

    /**
     * @return Конфигурация шаблонной роли. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission#getRoleConfig()
     */
    public static RoleConfigTemplate.Path<RoleConfigTemplate> roleConfig()
    {
        return _dslPath.roleConfig();
    }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission#getPrincipalContext()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> principalContext()
    {
        return _dslPath.principalContext();
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission#getEnrollmentCommission()
     */
    public static EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
    {
        return _dslPath.enrollmentCommission();
    }

    public static class Path<E extends RoleAssignmentTemplateEnrCommission> extends EntityPath<E>
    {
        private RoleConfigTemplate.Path<RoleConfigTemplate> _roleConfig;
        private IPrincipalContextGen.Path<IPrincipalContext> _principalContext;
        private EnrEnrollmentCommission.Path<EnrEnrollmentCommission> _enrollmentCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Конфигурация шаблонной роли. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission#getRoleConfig()
     */
        public RoleConfigTemplate.Path<RoleConfigTemplate> roleConfig()
        {
            if(_roleConfig == null )
                _roleConfig = new RoleConfigTemplate.Path<RoleConfigTemplate>(L_ROLE_CONFIG, this);
            return _roleConfig;
        }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission#getPrincipalContext()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> principalContext()
        {
            if(_principalContext == null )
                _principalContext = new IPrincipalContextGen.Path<IPrincipalContext>(L_PRINCIPAL_CONTEXT, this);
            return _principalContext;
        }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission#getEnrollmentCommission()
     */
        public EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
        {
            if(_enrollmentCommission == null )
                _enrollmentCommission = new EnrEnrollmentCommission.Path<EnrEnrollmentCommission>(L_ENROLLMENT_COMMISSION, this);
            return _enrollmentCommission;
        }

        public Class getEntityClass()
        {
            return RoleAssignmentTemplateEnrCommission.class;
        }

        public String getEntityName()
        {
            return "roleAssignmentTemplateEnrCommission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
