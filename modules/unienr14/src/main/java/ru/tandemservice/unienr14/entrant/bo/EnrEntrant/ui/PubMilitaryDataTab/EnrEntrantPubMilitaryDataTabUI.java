/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubMilitaryDataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.MilitaryEdit.PersonMilitaryEdit;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditArmyData.EnrEntrantEditArmyData;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;

/**
 * @author oleyba
 * @since 4/22/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required = true),
    @Bind(key = EnrEntrantPubMilitaryDataTabUI.PARAM_INLINE, binding = "inline")
})
public class EnrEntrantPubMilitaryDataTabUI extends UIPresenter
{
    public static final String PARAM_INLINE = "inline";

    private EnrEntrant entrant = new EnrEntrant();
    private ISecureRoleContext secureRoleContext;
    private PersonMilitaryStatus _militaryStatus;

    private boolean inline;
    public boolean isInline() { return this.inline; }
    public void setInline(boolean inline) { this.inline = inline; }


    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setSecureRoleContext(EnrPersonLegacyUtils.getSecureRoleContext(getEntrant()));
        setMilitaryStatus(DataAccessServices.dao().get(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, getPerson()));
    }

    public void onClickEditArmyData()
    {
        _uiActivation.asRegionDialog(EnrEntrantEditArmyData.class)
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getEntrant().getId())
        .activate();
    }

    public void onClickEditPersonMilitary()
    {
        _uiActivation.asRegionDialog(PersonMilitaryEdit.class)
        .parameter("personId", getPerson().getId())
        .activate();
    }

    // presenter

    public Person getPerson() {
        return getEntrant().getPerson();
    }

    // utils

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return secureRoleContext;
    }

    public void setSecureRoleContext(ISecureRoleContext secureRoleContext)
    {
        this.secureRoleContext = secureRoleContext;
    }

    public PersonMilitaryStatus getMilitaryStatus()
    {
        return _militaryStatus;
    }

    public void setMilitaryStatus(PersonMilitaryStatus militaryStatus)
    {
        _militaryStatus = militaryStatus;
    }
}

