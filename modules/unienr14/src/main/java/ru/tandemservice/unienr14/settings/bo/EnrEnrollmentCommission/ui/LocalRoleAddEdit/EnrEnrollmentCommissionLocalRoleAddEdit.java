/* $Id: SecLocalRoleAddEdit.java 6467 2015-05-07 07:00:30Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.LocalRoleAddEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Vasily Zhukov
 * @since 07.11.2011
 */
@Configuration
public class EnrEnrollmentCommissionLocalRoleAddEdit extends BusinessComponentManager
{
}
