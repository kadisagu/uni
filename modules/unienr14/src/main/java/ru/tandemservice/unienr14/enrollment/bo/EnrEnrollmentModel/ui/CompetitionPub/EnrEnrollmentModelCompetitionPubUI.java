/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionWrapper;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;

/**
 * @author oleyba
 * @since 3/27/15
 */
@State({
    @Bind(key = EnrEnrollmentModelCompetitionPubUI.PARAM_MODEL, binding = "model.id", required = true),
    @Bind(key = EnrEnrollmentModelCompetitionPubUI.PARAM_COMPETITION, binding = "competitionHolder.id", required = true),
    @Bind(key = EnrEnrollmentModelCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, binding = "taKind.id")
})
public class EnrEnrollmentModelCompetitionPubUI extends UIPresenter
{
    public static final String PARAM_MODEL = "model";
    public static final String PARAM_COMPETITION = "competition";
    public static final String PARAM_TARGET_ADMISSION_KIND = "taKind";

    private EnrEnrollmentModel model = new EnrEnrollmentModel();
    private EntityHolder<EnrCompetition> competitionHolder = new EntityHolder<>();
    private EnrTargetAdmissionKind taKind = new EnrTargetAdmissionKind();

    private EnrModelCompetitionWrapper wrapper;

    private IUIDataSource itemDS;

    @Override
    public void onComponentRefresh()
    {
        setModel(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentModel.class, getModel().getId()));
        if (null != getTaKind() && null != getTaKind().getId()) {
            setTaKind(IUniBaseDao.instance.get().get(EnrTargetAdmissionKind.class, getTaKind().getId()));
        } else {
            setTaKind(null);
        }
        getCompetitionHolder().refresh();

        wrapper = EnrEnrollmentModelManager.instance().dao().calcStats(null, getModel(), getCompetition(), getTaKind());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrModelEntrantDSHandler.BIND_ENR_MODEL, getModel());
        dataSource.put(EnrModelEntrantDSHandler.BIND_COMPETITION, getCompetition());
        dataSource.put(EnrModelEntrantDSHandler.BIND_TA_KIND, getTaKind());
    }

    // presenter

    public IEntity getCurrentItem() {
        return getItemDS().getCurrent();
    }

    public IUIDataSource getItemDS()
    {
        if (itemDS == null)
            itemDS = getConfig().getDataSource("itemDS"); // todo сделать константу
        return itemDS;
    }

    public EnrCompetition getCompetition()
    {
        return getCompetitionHolder().getValue();
    }

    // getters and setters


    public EntityHolder<EnrCompetition> getCompetitionHolder()
    {
        return competitionHolder;
    }

    public EnrTargetAdmissionKind getTaKind()
    {
        return taKind;
    }

    public void setTaKind(EnrTargetAdmissionKind taKind)
    {
        this.taKind = taKind;
    }

    public EnrModelCompetitionWrapper getWrapper()
    {
        return wrapper;
    }

    public EnrEnrollmentModel getModel()
    {
        return model;
    }

    public void setModel(EnrEnrollmentModel model)
    {
        this.model = model;
    }
}