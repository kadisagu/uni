/**
 *$Id: EnrCampaignDisciplineGroupAddEdit.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.GroupAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
@Configuration
public class EnrCampaignDisciplineGroupAddEdit extends BusinessComponentManager
{
    public static final String ENR_CAMP_SEARCH_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String DISC_GROUP_ELEMENT_SELECT_DS = "campDisciplineDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(DISC_GROUP_ELEMENT_SELECT_DS, discGroupElementDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> discGroupElementDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignDiscipline.class)
                .where(EnrCampaignDiscipline.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .order(EnrCampaignDiscipline.discipline().title())
                .filter(EnrCampaignDiscipline.discipline().title());
    }
}
