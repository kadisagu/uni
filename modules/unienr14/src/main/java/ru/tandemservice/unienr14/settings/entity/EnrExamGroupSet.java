package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Набор экзаменационных групп
 */
public class EnrExamGroupSet extends EnrExamGroupSetGen
{
    /**
     * @return Название - в формате "15.01.2013 - 31.01.2013"
     */
    @EntityDSLSupport
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate());
    }
}