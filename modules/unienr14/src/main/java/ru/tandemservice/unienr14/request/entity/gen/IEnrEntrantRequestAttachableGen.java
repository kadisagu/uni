package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;

/**
 * Документ, прикладываемый к заявлению
 *
 * Интерфейс для документов, которые могут быть приложены к заявлению.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrEntrantRequestAttachableGen extends InterfaceStubBase
 implements IEnrEntrantRequestAttachable{
    public static final int VERSION_HASH = 216797371;

    public static final String L_ENTRANT = "entrant";

    private EnrEntrant _entrant;


    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    private static final Path<IEnrEntrantRequestAttachable> _dslPath = new Path<IEnrEntrantRequestAttachable>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable");
    }
            

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    public static class Path<E extends IEnrEntrantRequestAttachable> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

        public Class getEntityClass()
        {
            return IEnrEntrantRequestAttachable.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
