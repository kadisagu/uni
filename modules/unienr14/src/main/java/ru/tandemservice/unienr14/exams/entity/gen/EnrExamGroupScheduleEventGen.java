package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Время проведения ВИ для ЭГ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamGroupScheduleEventGen extends EntityBase
 implements INaturalIdentifiable<EnrExamGroupScheduleEventGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent";
    public static final String ENTITY_NAME = "enrExamGroupScheduleEvent";
    public static final int VERSION_HASH = 1426880854;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_GROUP = "examGroup";
    public static final String L_EXAM_SCHEDULE_EVENT = "examScheduleEvent";
    public static final String P_SCHEDULE_AND_EXAM_GROUP_TITLE = "scheduleAndExamGroupTitle";
    public static final String P_SCHEDULE_TITLE = "scheduleTitle";

    private EnrExamGroup _examGroup;     // ЭГ
    private EnrExamScheduleEvent _examScheduleEvent;     // Событие расписания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ЭГ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamGroup getExamGroup()
    {
        return _examGroup;
    }

    /**
     * @param examGroup ЭГ. Свойство не может быть null.
     */
    public void setExamGroup(EnrExamGroup examGroup)
    {
        dirty(_examGroup, examGroup);
        _examGroup = examGroup;
    }

    /**
     * @return Событие расписания. Свойство не может быть null.
     */
    @NotNull
    public EnrExamScheduleEvent getExamScheduleEvent()
    {
        return _examScheduleEvent;
    }

    /**
     * @param examScheduleEvent Событие расписания. Свойство не может быть null.
     */
    public void setExamScheduleEvent(EnrExamScheduleEvent examScheduleEvent)
    {
        dirty(_examScheduleEvent, examScheduleEvent);
        _examScheduleEvent = examScheduleEvent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamGroupScheduleEventGen)
        {
            if (withNaturalIdProperties)
            {
                setExamGroup(((EnrExamGroupScheduleEvent)another).getExamGroup());
                setExamScheduleEvent(((EnrExamGroupScheduleEvent)another).getExamScheduleEvent());
            }
        }
    }

    public INaturalId<EnrExamGroupScheduleEventGen> getNaturalId()
    {
        return new NaturalId(getExamGroup(), getExamScheduleEvent());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamGroupScheduleEventGen>
    {
        private static final String PROXY_NAME = "EnrExamGroupScheduleEventNaturalProxy";

        private Long _examGroup;
        private Long _examScheduleEvent;

        public NaturalId()
        {}

        public NaturalId(EnrExamGroup examGroup, EnrExamScheduleEvent examScheduleEvent)
        {
            _examGroup = ((IEntity) examGroup).getId();
            _examScheduleEvent = ((IEntity) examScheduleEvent).getId();
        }

        public Long getExamGroup()
        {
            return _examGroup;
        }

        public void setExamGroup(Long examGroup)
        {
            _examGroup = examGroup;
        }

        public Long getExamScheduleEvent()
        {
            return _examScheduleEvent;
        }

        public void setExamScheduleEvent(Long examScheduleEvent)
        {
            _examScheduleEvent = examScheduleEvent;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamGroupScheduleEventGen.NaturalId) ) return false;

            EnrExamGroupScheduleEventGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamGroup(), that.getExamGroup()) ) return false;
            if( !equals(getExamScheduleEvent(), that.getExamScheduleEvent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamGroup());
            result = hashCode(result, getExamScheduleEvent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamGroup());
            sb.append("/");
            sb.append(getExamScheduleEvent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamGroupScheduleEventGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamGroupScheduleEvent.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamGroupScheduleEvent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examGroup":
                    return obj.getExamGroup();
                case "examScheduleEvent":
                    return obj.getExamScheduleEvent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examGroup":
                    obj.setExamGroup((EnrExamGroup) value);
                    return;
                case "examScheduleEvent":
                    obj.setExamScheduleEvent((EnrExamScheduleEvent) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examGroup":
                        return true;
                case "examScheduleEvent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examGroup":
                    return true;
                case "examScheduleEvent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examGroup":
                    return EnrExamGroup.class;
                case "examScheduleEvent":
                    return EnrExamScheduleEvent.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamGroupScheduleEvent> _dslPath = new Path<EnrExamGroupScheduleEvent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamGroupScheduleEvent");
    }
            

    /**
     * @return ЭГ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getExamGroup()
     */
    public static EnrExamGroup.Path<EnrExamGroup> examGroup()
    {
        return _dslPath.examGroup();
    }

    /**
     * @return Событие расписания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getExamScheduleEvent()
     */
    public static EnrExamScheduleEvent.Path<EnrExamScheduleEvent> examScheduleEvent()
    {
        return _dslPath.examScheduleEvent();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getScheduleAndExamGroupTitle()
     */
    public static SupportedPropertyPath<String> scheduleAndExamGroupTitle()
    {
        return _dslPath.scheduleAndExamGroupTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getScheduleTitle()
     */
    public static SupportedPropertyPath<String> scheduleTitle()
    {
        return _dslPath.scheduleTitle();
    }

    public static class Path<E extends EnrExamGroupScheduleEvent> extends EntityPath<E>
    {
        private EnrExamGroup.Path<EnrExamGroup> _examGroup;
        private EnrExamScheduleEvent.Path<EnrExamScheduleEvent> _examScheduleEvent;
        private SupportedPropertyPath<String> _scheduleAndExamGroupTitle;
        private SupportedPropertyPath<String> _scheduleTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ЭГ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getExamGroup()
     */
        public EnrExamGroup.Path<EnrExamGroup> examGroup()
        {
            if(_examGroup == null )
                _examGroup = new EnrExamGroup.Path<EnrExamGroup>(L_EXAM_GROUP, this);
            return _examGroup;
        }

    /**
     * @return Событие расписания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getExamScheduleEvent()
     */
        public EnrExamScheduleEvent.Path<EnrExamScheduleEvent> examScheduleEvent()
        {
            if(_examScheduleEvent == null )
                _examScheduleEvent = new EnrExamScheduleEvent.Path<EnrExamScheduleEvent>(L_EXAM_SCHEDULE_EVENT, this);
            return _examScheduleEvent;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getScheduleAndExamGroupTitle()
     */
        public SupportedPropertyPath<String> scheduleAndExamGroupTitle()
        {
            if(_scheduleAndExamGroupTitle == null )
                _scheduleAndExamGroupTitle = new SupportedPropertyPath<String>(EnrExamGroupScheduleEventGen.P_SCHEDULE_AND_EXAM_GROUP_TITLE, this);
            return _scheduleAndExamGroupTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent#getScheduleTitle()
     */
        public SupportedPropertyPath<String> scheduleTitle()
        {
            if(_scheduleTitle == null )
                _scheduleTitle = new SupportedPropertyPath<String>(EnrExamGroupScheduleEventGen.P_SCHEDULE_TITLE, this);
            return _scheduleTitle;
        }

        public Class getEntityClass()
        {
            return EnrExamGroupScheduleEvent.class;
        }

        public String getEntityName()
        {
            return "enrExamGroupScheduleEvent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getScheduleAndExamGroupTitle();

    public abstract String getScheduleTitle();
}
