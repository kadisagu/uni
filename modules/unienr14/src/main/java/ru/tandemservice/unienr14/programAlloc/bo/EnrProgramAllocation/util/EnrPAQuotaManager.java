/* $Id: EcgpQuotaManager.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import org.apache.commons.lang.mutable.MutableInt;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;

import java.util.*;

/**
 * Алгоритм выбора профилей направлений из строки рейтинга в соответствии с планами
 *
 * @author Vasily Zhukov
 * @since 14.05.2012
 */
public class EnrPAQuotaManager
{
    private int _freeTotal;
    private Map<Long, MutableInt> _freeMap = new HashMap<Long, MutableInt>();

    public EnrPAQuotaManager(IEnrPAQuotaFreeWrapper quotaFreeDTO)
    {
        for (Map.Entry<Long, Integer> entry : quotaFreeDTO.getFreeMap().entrySet()) {
            _freeMap.put(entry.getKey(), new MutableInt(entry.getValue() == null ? Integer.MAX_VALUE : entry.getValue().intValue()));
        }
        _freeTotal = quotaFreeDTO.getTotalFree();
    }

    public EnrProgramSetItem doChoose(final List<EnrProgramSetItem> availableChoices, final EnrProgramSetItem preferredChoice)
    {

        EnrProgramSetItem chosen = null;
        List<EnrProgramSetItem> possible = new ArrayList<EnrProgramSetItem>();

        MutableInt chosenMutableQuota = null;

        for (EnrProgramSetItem program : availableChoices) {
            MutableInt mutableQuota = _freeMap.get(program.getId());
            if (mutableQuota != null && mutableQuota.intValue() > 0) {
                possible.add(program);
                if (preferredChoice != null && program.getId().equals(preferredChoice.getId())) {
                    chosen = program;
                    chosenMutableQuota = mutableQuota;
                }
            }
        }

        if (chosen == null) {
            if (!possible.isEmpty()) {
                chosen = possible.get(0);
                _freeMap.get(chosen.getId()).decrement();
            }
        } else {
            chosenMutableQuota.decrement();
        }

        return chosen;
    }

    public int getFreeTotal()
    {
        return _freeTotal;
    }
}
