/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.EnrReportEntrantsAgeDistributionAddUI;

/**
 * @author rsizonenko
 * @since 18.06.2014
 */
public interface IEnrReportEntrantsAgeDistributionDao extends INeedPersistenceSupport {
    public long createReport(EnrReportEntrantsAgeDistributionAddUI enrReportEntrantsAgeDistributionAddUI);
}
