package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * TODO: перенести в бранч
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentStepItem

        // создано обязательное свойство entrantArchived
        {
            // создать колонку
            tool.createColumn("enr14_enr_step_item_t", new DBColumn("entrantarchived_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultEntrantArchived = Boolean.FALSE;
            tool.executeUpdate("update enr14_enr_step_item_t set entrantarchived_p=? where entrantarchived_p is null", defaultEntrantArchived);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_enr_step_item_t", "entrantarchived_p", false);

        }

        // создано обязательное свойство entrantRequestTakeAwayDocuments
        {
            // создать колонку
            tool.createColumn("enr14_enr_step_item_t", new DBColumn("ntrntrqsttkawydcmnts_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultEntrantRequestTakeAwayDocuments = Boolean.FALSE;
            tool.executeUpdate("update enr14_enr_step_item_t set ntrntrqsttkawydcmnts_p=? where ntrntrqsttkawydcmnts_p is null", defaultEntrantRequestTakeAwayDocuments);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_enr_step_item_t", "ntrntrqsttkawydcmnts_p", false);

        }


    }
}