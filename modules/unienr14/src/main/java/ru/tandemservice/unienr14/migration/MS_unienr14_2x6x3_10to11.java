package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrCompetition

		// создано обязательное свойство calculatedPlan
        if(tool.tableExists("enr14_competition_t") && !tool.columnExists("enr14_competition_t", "calculatedplan_p"))
		{
			// создать колонку
			tool.createColumn("enr14_competition_t", new DBColumn("calculatedplan_p", DBType.INTEGER));

			tool.executeUpdate("update enr14_competition_t set calculatedplan_p=plan_p where calculatedplan_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_competition_t", "calculatedplan_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrTargetAdmissionPlan

		// создано обязательное свойство calculatedPlanSOO
        if(tool.tableExists("enr14_ta_plan_t") && !tool.columnExists("enr14_ta_plan_t", "calculatedplansoo_p"))
		{
			// создать колонку
			tool.createColumn("enr14_ta_plan_t", new DBColumn("calculatedplansoo_p", DBType.INTEGER));

			tool.executeUpdate("update enr14_ta_plan_t set calculatedplansoo_p=plansoo_p where calculatedplansoo_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_ta_plan_t", "calculatedplansoo_p", false);

		}

		// создано обязательное свойство calculatedPlanPO
        if(tool.tableExists("enr14_ta_plan_t") && !tool.columnExists("enr14_ta_plan_t", "calculatedplanpo_p"))
		{
			// создать колонку
			tool.createColumn("enr14_ta_plan_t", new DBColumn("calculatedplanpo_p", DBType.INTEGER));

			tool.executeUpdate("update enr14_ta_plan_t set calculatedplanpo_p=planpo_p where calculatedplanpo_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_ta_plan_t", "calculatedplanpo_p", false);

		}


    }
}