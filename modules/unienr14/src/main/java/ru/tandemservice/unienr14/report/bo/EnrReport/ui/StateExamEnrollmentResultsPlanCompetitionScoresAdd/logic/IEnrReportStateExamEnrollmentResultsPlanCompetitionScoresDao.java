package ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsPlanCompetitionScoresAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.EnrReportStateExamEnrollmentResultsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsPlanCompetitionScoresAdd.EnrReportStateExamEnrollmentResultsPlanCompetitionScoresAddUI;

/**
 * @author ilunin
 * @since 19.09.2014
 */
public interface IEnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao extends INeedPersistenceSupport {
    long createReport(EnrReportStateExamEnrollmentResultsPlanCompetitionScoresAddUI model);
}