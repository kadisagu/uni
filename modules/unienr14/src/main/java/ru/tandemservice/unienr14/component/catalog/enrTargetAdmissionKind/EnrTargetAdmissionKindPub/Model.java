/**
 *$Id: Model.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrTargetAdmissionKind.EnrTargetAdmissionKindPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

import java.util.Comparator;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class Model extends DefaultCatalogPubModel<EnrTargetAdmissionKind>
{
    public Comparator getPriorityComparator()
    {
        return new Comparator<EnrTargetAdmissionKind>()
        {
            @Override
            public int compare(EnrTargetAdmissionKind o1, EnrTargetAdmissionKind o2)
            {
                return Integer.compare(o1.getPriority(), o2.getPriority());
            }
        };
    }

}
