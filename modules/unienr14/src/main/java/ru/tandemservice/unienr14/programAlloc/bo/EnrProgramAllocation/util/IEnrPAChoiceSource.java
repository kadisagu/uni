/* $Id:$ */
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;

import java.util.List;
import java.util.Map;

/**
 * Источник данных для распределения по ОП
 * @author oleyba
 * @since 8/11/14
 */
public interface IEnrPAChoiceSource
{
    /**
     * @return выбранные конкурсы, по которым были зачислены абитуриенты, и которые еще не распределены по профилям.
     */
    List<EnrPAItemWrapper> getEntrantsForAllocation();
}
