/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.TargetAdmissionPlanEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrTargetAdmissionPlanWrapper;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 05.05.2014
 */
@Input(@Bind(key = "programSetOrgUnit", binding = "programSetOrgUnit.id", required = true))
public class EnrProgramSetTargetAdmissionPlanEditUI extends UIPresenter
{
    private EnrProgramSetOrgUnit _programSetOrgUnit = new EnrProgramSetOrgUnit();
    private Collection<EnrTargetAdmissionPlanWrapper> _rows;
    private EnrTargetAdmissionPlanWrapper _currentRow;

    private List<EnrCompetition> taCompetitionList;
    private EnrCompetition currentTaCompetition;

    @Override
    public void onComponentRefresh()
    {
        setProgramSetOrgUnit(IUniBaseDao.instance.get().get(EnrProgramSetOrgUnit.class, getProgramSetOrgUnit().getId()));
        setRows(EnrProgramSetManager.instance().dao().getProgramSetOrgUnitTAPlans(getProgramSetOrgUnit().getId(), true));
        setTaCompetitionList(DataAccessServices.dao().<EnrCompetition>getList(new DQLSelectBuilder()
            .fromEntity(EnrCompetition.class, "c")
            .where(eq(property("c", EnrCompetition.programSetOrgUnit()), value(getProgramSetOrgUnit())))
            .where(eq(property("c", EnrCompetition.type().code()), value(EnrCompetitionTypeCodes.TARGET_ADMISSION)))
            .order(property("c", EnrCompetition.eduLevelRequirement().priority()))));
    }

    public String getColumnCount()
    {
        return String.valueOf(2 + getTaCompetitionList().size() * 2 + 1);
    }

    public void onClickApply()
    {
        if (validatePlans()) {
            EnrProgramSetManager.instance().dao().updateProgramSetOrgUnitTargetAdmissionPlans(getProgramSetOrgUnit().getId(), getRows());
            deactivate();
        }
    }

    protected boolean validatePlans()
    {
        final ErrorCollector collector = UserContext.getInstance().getErrorCollector();
        for (EnrTargetAdmissionPlanWrapper row : getRows()) {

            if (row.getPlan() == null || row.getPlan() == 0) {
                boolean hasPlans = false;
                for (Integer plan : row.getCompetitionPlanMap().values()) {
                    if (plan != null && plan > 0) {
                        hasPlans = true;
                        break;
                    }
                }

                if (hasPlans) {
                    collector.add("Для вида целевого приема указано число мест по конкурсу, но не указан план приема.", generatePlanFieldId(row));
                }
            }
        }
        return !collector.hasErrors();
    }

    public void onClickCleanRow()
    {
        final Long rowId = getListenerParameterAsLong();
        for (EnrTargetAdmissionPlanWrapper row : getRows()) {

            if (row.getId().equals(rowId)) {
                row.setPlan(null);
                for (Long competitionPlanId : row.getCompetitionPlanMap().keySet()) {
                    row.getCompetitionPlanMap().put(competitionPlanId, null);
                }
                return;
            }
        }
        throw new IllegalStateException(); // wtf?
    }

    private String generateCompPlanFieldId(EnrTargetAdmissionPlanWrapper planWrapper, EnrCompetition competition)
    {
        return "pf." + planWrapper.getKind().getTargetAdmissionKind().getCode() + "." + competition.getEduLevelRequirement().getCode();
    }

    private String generatePlanFieldId(EnrTargetAdmissionPlanWrapper planWrapper)
    {
        return "pf." + planWrapper.getKind().getTargetAdmissionKind().getCode();
    }

    // presenter

    public String getCompPlanFieldId() {
        return generateCompPlanFieldId(getCurrentRow(), getCurrentTaCompetition());
    }

    public String getPlanFieldId() {
        return generatePlanFieldId(getCurrentRow());
    }

    public Integer getCalculatedPlan() {
        return getCurrentRow().getCalculatedPlan(getCurrentTaCompetition().getId());
    }

    public Integer getPlan() {
        return getCurrentRow().getPlan(getCurrentTaCompetition().getId());
    }

    public void setPlan(Integer plan) {
        getCurrentRow().getCompetitionPlanMap().put(getCurrentTaCompetition().getId(), plan);
    }

    // getters and setters

    public EnrProgramSetOrgUnit getProgramSetOrgUnit(){ return _programSetOrgUnit; }
    public void setProgramSetOrgUnit(EnrProgramSetOrgUnit programSetOrgUnit){ _programSetOrgUnit = programSetOrgUnit; }

    public Collection<EnrTargetAdmissionPlanWrapper> getRows(){ return _rows; }
    public void setRows(Collection<EnrTargetAdmissionPlanWrapper> rows){ _rows = rows; }

    public EnrTargetAdmissionPlanWrapper getCurrentRow(){ return _currentRow; }
    public void setCurrentRow(EnrTargetAdmissionPlanWrapper currentRow){ _currentRow = currentRow; }

    public EnrCompetition getCurrentTaCompetition()
    {
        return currentTaCompetition;
    }

    public void setCurrentTaCompetition(EnrCompetition currentTaCompetition)
    {
        this.currentTaCompetition = currentTaCompetition;
    }

    public List<EnrCompetition> getTaCompetitionList()
    {
        return taCompetitionList;
    }

    public void setTaCompetitionList(List<EnrCompetition> taCompetitionList)
    {
        this.taCompetitionList = taCompetitionList;
    }
}