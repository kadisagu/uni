/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.IDefaultScriptCatalogAddEditDAO;
import ru.tandemservice.unienr14.catalog.entity.IEnrScriptItem;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public interface IDAO<T extends ICatalogItem & IScriptItem & IEnrScriptItem, Model extends ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemAddEdit.Model<T>> extends IDefaultScriptCatalogAddEditDAO
{
}