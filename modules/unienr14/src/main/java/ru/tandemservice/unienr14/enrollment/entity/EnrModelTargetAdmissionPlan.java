package ru.tandemservice.unienr14.enrollment.entity;

import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrPlanZeroFixer;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrTargetAdmissionCompetitionPlanFormula;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.enrollment.entity.gen.*;

/** @see ru.tandemservice.unienr14.enrollment.entity.gen.EnrModelTargetAdmissionPlanGen */
public class EnrModelTargetAdmissionPlan extends EnrModelTargetAdmissionPlanGen implements EnrTargetAdmissionCompetitionPlanFormula.ITargetAdmissionPlan, EnrPlanZeroFixer.IPlanOwner
{
    public EnrModelTargetAdmissionPlan()
    {
    }

    public EnrModelTargetAdmissionPlan(EnrEnrollmentModel model, EnrTargetAdmissionCompetitionPlan taPlan)
    {
        setModel(model);
        setCompetition(taPlan.getCompetition());
        setTargetAdmissionPlan(taPlan.getTargetAdmissionPlan());
        setPlan(taPlan.getPlan());
    }

    public EnrModelTargetAdmissionPlan(EnrEnrollmentModel model, EnrCompetition competition, EnrTargetAdmissionPlan taPlan)
    {
        setModel(model);
        setCompetition(competition);
        setTargetAdmissionPlan(taPlan);
    }

    @Override
    public String getEduLevelReqCode()
    {
        return getCompetition().getEduLevelRequirement().getCode();
    }

    @Override
    public String getCompTypeCode()
    {
        return getCompetition().getType().getCode();
    }

    @Override
    public Object planBalanceKey()
    {
        return getTargetAdmissionPlan();
    }

    @Override
    public int getFixPlan()
    {
        return getPlan();
    }

    @Override
    public void setFixPlan(int plan)
    {
        setPlan(plan);
    }
}