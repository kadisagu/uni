/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.sec.runtime.SecurityRuntime;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

import java.util.Collections;
import java.util.List;

/**
 * @author nvankov
 * @since 21.03.2016
 */
public class EnrEnrollmentCommissionPermissionDao extends UniBaseDao implements IEnrEnrollmentCommissionPermissionDao
{
    @Override
    public List<EnrEnrollmentCommission> getEnrollmentCommissionList(IPrincipalContext principalContext)
    {
        if(principalContext == null) return Collections.emptyList();

        return Lists.newArrayList(CollectionUtils.select(EnrEnrollmentCommissionManager.instance().dao().getEnrollmentCommissionList(),
                enrEnrollmentCommission -> hasPermissionForEnrCommission(enrEnrollmentCommission, principalContext)
        ));
    }

    @Override
    public List<Long> getEnrollmentCommissionIds(IPrincipalContext principalContext)
    {
        if(principalContext == null) return Collections.emptyList();

        return Lists.newArrayList(CollectionUtils.collect(getEnrollmentCommissionList(principalContext), EnrEnrollmentCommission::getId));
    }

    @Override
    public boolean hasPermissionForEnrCommission(EnrEnrollmentCommission enrollmentCommission, IPrincipalContext principalContext)
    {
        return principalContext != null && CoreServices.securityService().check(enrollmentCommission, principalContext, "enr14EnrollmentCommissionSelect");
    }

    @Override
    public boolean hasGlobalPermissionForEnrCommission(IPrincipalContext principalContext)
    {
        return principalContext != null && CoreServices.securityService().check(SecurityRuntime.getInstance().getCommonSecurityObject(), principalContext, "enr14EnrollmentCommissionSelect");

    }
}
