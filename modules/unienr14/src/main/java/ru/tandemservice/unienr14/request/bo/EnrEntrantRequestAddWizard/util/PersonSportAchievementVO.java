/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util;

import org.tandemframework.shared.person.base.entity.PersonSportAchievement;

import java.util.Comparator;

/**
 * @author nvankov
 * @since 6/14/14
 */
public class PersonSportAchievementVO
{
    public static final Comparator<PersonSportAchievementVO> COMPARATOR = new Comparator<PersonSportAchievementVO>() {
        @Override public int compare(PersonSportAchievementVO o1, PersonSportAchievementVO o2) {
            if(o1.getSportAchievement().getSportKind() == null || o2.getSportAchievement().getSportKind() == null)
            {
                return o1.getSportAchievement().getSportKind() == null ? (o2.getSportAchievement().getSportKind()!= null ? 1 : 0) : -1;
            }
            else
            {
                return o1.getSportAchievement().getSportKind().getTitle().compareTo(o2.getSportAchievement().getSportKind().getTitle());
            }
        }
    };

    private PersonSportAchievement _sportAchievement;
    private boolean _createdInCurrentWizard;

    public PersonSportAchievementVO(PersonSportAchievement sportAchievement, boolean createdInCurrentWizard)
    {
        _sportAchievement = sportAchievement;
        _createdInCurrentWizard = createdInCurrentWizard;
    }

    public PersonSportAchievement getSportAchievement()
    {
        return _sportAchievement;
    }

    public boolean isCreatedInCurrentWizard()
    {
        return _createdInCurrentWizard;
    }

    public boolean isDisabled()
    {
        return !_createdInCurrentWizard;
    }
}
