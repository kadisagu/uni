package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ абитуриента, подтверждающий особое право
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantBenefitProofGen extends VersionedEntityBase
 implements INaturalIdentifiable<EnrEntrantBenefitProofGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof";
    public static final String ENTITY_NAME = "enrEntrantBenefitProof";
    public static final int VERSION_HASH = -620098101;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_BENEFIT_STATEMENT = "benefitStatement";
    public static final String L_DOCUMENT = "document";

    private int _version; 
    private IEnrEntrantBenefitStatement _benefitStatement;     // Факт использования особого права
    private IEnrEntrantBenefitProofDocument _document;     // Документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Факт использования особого права. Свойство не может быть null.
     */
    @NotNull
    public IEnrEntrantBenefitStatement getBenefitStatement()
    {
        return _benefitStatement;
    }

    /**
     * @param benefitStatement Факт использования особого права. Свойство не может быть null.
     */
    public void setBenefitStatement(IEnrEntrantBenefitStatement benefitStatement)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && benefitStatement!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrEntrantBenefitStatement.class);
            IEntityMeta actual =  benefitStatement instanceof IEntity ? EntityRuntime.getMeta((IEntity) benefitStatement) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_benefitStatement, benefitStatement);
        _benefitStatement = benefitStatement;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public IEnrEntrantBenefitProofDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(IEnrEntrantBenefitProofDocument document)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && document!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrEntrantBenefitProofDocument.class);
            IEntityMeta actual =  document instanceof IEntity ? EntityRuntime.getMeta((IEntity) document) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_document, document);
        _document = document;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantBenefitProofGen)
        {
            if (withNaturalIdProperties)
            {
                setBenefitStatement(((EnrEntrantBenefitProof)another).getBenefitStatement());
                setDocument(((EnrEntrantBenefitProof)another).getDocument());
            }
            setVersion(((EnrEntrantBenefitProof)another).getVersion());
        }
    }

    public INaturalId<EnrEntrantBenefitProofGen> getNaturalId()
    {
        return new NaturalId(getBenefitStatement(), getDocument());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantBenefitProofGen>
    {
        private static final String PROXY_NAME = "EnrEntrantBenefitProofNaturalProxy";

        private Long _benefitStatement;
        private Long _document;

        public NaturalId()
        {}

        public NaturalId(IEnrEntrantBenefitStatement benefitStatement, IEnrEntrantBenefitProofDocument document)
        {
            _benefitStatement = ((IEntity) benefitStatement).getId();
            _document = ((IEntity) document).getId();
        }

        public Long getBenefitStatement()
        {
            return _benefitStatement;
        }

        public void setBenefitStatement(Long benefitStatement)
        {
            _benefitStatement = benefitStatement;
        }

        public Long getDocument()
        {
            return _document;
        }

        public void setDocument(Long document)
        {
            _document = document;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantBenefitProofGen.NaturalId) ) return false;

            EnrEntrantBenefitProofGen.NaturalId that = (NaturalId) o;

            if( !equals(getBenefitStatement(), that.getBenefitStatement()) ) return false;
            if( !equals(getDocument(), that.getDocument()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBenefitStatement());
            result = hashCode(result, getDocument());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBenefitStatement());
            sb.append("/");
            sb.append(getDocument());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantBenefitProofGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantBenefitProof.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantBenefitProof();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "benefitStatement":
                    return obj.getBenefitStatement();
                case "document":
                    return obj.getDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "benefitStatement":
                    obj.setBenefitStatement((IEnrEntrantBenefitStatement) value);
                    return;
                case "document":
                    obj.setDocument((IEnrEntrantBenefitProofDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "benefitStatement":
                        return true;
                case "document":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "benefitStatement":
                    return true;
                case "document":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "benefitStatement":
                    return IEnrEntrantBenefitStatement.class;
                case "document":
                    return IEnrEntrantBenefitProofDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantBenefitProof> _dslPath = new Path<EnrEntrantBenefitProof>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantBenefitProof");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Факт использования особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof#getBenefitStatement()
     */
    public static IEnrEntrantBenefitStatementGen.Path<IEnrEntrantBenefitStatement> benefitStatement()
    {
        return _dslPath.benefitStatement();
    }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof#getDocument()
     */
    public static IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument> document()
    {
        return _dslPath.document();
    }

    public static class Path<E extends EnrEntrantBenefitProof> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private IEnrEntrantBenefitStatementGen.Path<IEnrEntrantBenefitStatement> _benefitStatement;
        private IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument> _document;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EnrEntrantBenefitProofGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Факт использования особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof#getBenefitStatement()
     */
        public IEnrEntrantBenefitStatementGen.Path<IEnrEntrantBenefitStatement> benefitStatement()
        {
            if(_benefitStatement == null )
                _benefitStatement = new IEnrEntrantBenefitStatementGen.Path<IEnrEntrantBenefitStatement>(L_BENEFIT_STATEMENT, this);
            return _benefitStatement;
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof#getDocument()
     */
        public IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument> document()
        {
            if(_document == null )
                _document = new IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument>(L_DOCUMENT, this);
            return _document;
        }

        public Class getEntityClass()
        {
            return EnrEntrantBenefitProof.class;
        }

        public String getEntityName()
        {
            return "enrEntrantBenefitProof";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
