/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.SearchListDSConfig;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanEdit.EnrProgramSetItemOrgUnitPlanEdit;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 01.08.2014
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "programSetHolder.id", required = true))
public class EnrProgramSetItemOrgUnitPlanTabUI extends UIPresenter
{
    private EntityHolder<EnrProgramSetBase> _programSetHolder = new EntityHolder<>();
    private Collection<ProgramSetOrgUnit> _orgUnitList;
    private ProgramSetOrgUnit _currentOrgUnit;
    private Map<String, Long> _dataSourceParamMap;

    @Override
    public void onComponentRefresh()
    {
        getProgramSetHolder().refresh();
        prepareOrgUnitList();

        _dataSourceParamMap = new HashMap<>();
        EnrProgramSetItemOrgUnitPlanTab manager = getConfig().getManager();
        for (ProgramSetOrgUnit orgUnit: _orgUnitList)
        {
            String dataSourceName = buildDataSourceName(orgUnit.getId());
            SearchListDSConfig dataSourceConfig = manager.searchListDS(dataSourceName).
                    columnListExtPoint(manager.programSetItemDSColumns()).
                    handler(manager.programSetItemDSHandler()).
                    create();

            this.getConfig().addDataSource(new PageableSearchListDataSource(this, dataSourceConfig));
            _dataSourceParamMap.put(dataSourceName, orgUnit.getId());
        }
    }

    private void prepareOrgUnitList()
    {
        final List<Long> visibleOrgUniIds = IUniBaseDao.instance.get().getList(
            new DQLSelectBuilder()
                .fromEntity(EnrProgramSetOrgUnit.class, "ou")
                .column(property("ou", EnrProgramSetOrgUnit.id()))
                .where(eq(property("ou", EnrProgramSetOrgUnit.programSet().id()), value(getProgramSet().getId())))
                .where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrCompetition.class, "c")
                        .where(eq(property("c", EnrCompetition.programSetOrgUnit().id()), property("ou", EnrProgramSetOrgUnit.id())))
                        .where(eq(property("c", EnrCompetition.allowProgramPriorities()), value(Boolean.TRUE)))
                        .buildQuery())));

        final List<Long> notSpecifiedOrgUnitIds = IUniBaseDao.instance.get().getList(
            new DQLSelectBuilder()
                .fromEntity(EnrProgramSetOrgUnit.class, "ou")
                .column(property("ou", EnrProgramSetOrgUnit.id()))
                .where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetItem.class, "i")
                        .where(notExists(
                            new DQLSelectBuilder()
                                .fromEntity(EnrProgramSetItemOrgUnitPlan.class, "p")
                                .where(eq(property("p", EnrProgramSetItemOrgUnitPlan.programSetItem().id()), property("i", EnrProgramSetItem.id())))
                                .where(eq(property("p", EnrProgramSetItemOrgUnitPlan.programSetOrgUnit().id()), property("ou", EnrProgramSetOrgUnit.id())))
                                .buildQuery()))
                        .where(eq(property("i", EnrProgramSetItem.programSet().id()), value(getProgramSet().getId())))
                        .buildQuery()                ))
                .where(eq(property("ou", EnrProgramSetOrgUnit.programSet().id()), value(getProgramSet().getId()))));

        List<EnrProgramSetOrgUnit> source = IUniBaseDao.instance.get().getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet().id(), getProgramSet().getId());

        Comparator<EnrProgramSetOrgUnit> comparator = new Comparator<EnrProgramSetOrgUnit>() {
            @Override public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                return EnrOrgUnit.ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR.compare(o1.getOrgUnit(), o2.getOrgUnit()); }};

        Collections.sort(source, comparator);
        _orgUnitList = new ArrayList<>(source.size());
        for (final EnrProgramSetOrgUnit orgUnit: source)
        {
            if (visibleOrgUniIds.contains(orgUnit.getId()))
                _orgUnitList.add(new ProgramSetOrgUnit()
                {
                    @Override public Long getId() { return orgUnit.getId(); }
                    @Override public boolean isShowWarningMessage() { return notSpecifiedOrgUnitIds.contains(getId()); }
                    @Override public String getCaption() { return buildOrgUnitCation(orgUnit.getOrgUnit().getDepartmentTitle(), orgUnit.getMinisterialPlan(), orgUnit.getContractPlan()); }
                });
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", _dataSourceParamMap.get(dataSource.getName()));
    }

    public void onClickEditPlan()
    {
        this.getActivationBuilder().asCurrent(EnrProgramSetItemOrgUnitPlanEdit.class).parameter("programSetOrgUnit", this.getListenerParameterAsLong()).activate();
    }


    // Getters && Setters
    public EntityHolder<EnrProgramSetBase> getProgramSetHolder() { return _programSetHolder; }
    public EnrProgramSetBase getProgramSet(){ return getProgramSetHolder().getValue(); }

    public Collection<ProgramSetOrgUnit> getOrgUnitList(){ return _orgUnitList; }

    public ProgramSetOrgUnit getCurrentOrgUnit() { return _currentOrgUnit; }
    public void setCurrentOrgUnit(ProgramSetOrgUnit currentOrgUnit) { _currentOrgUnit = currentOrgUnit; }


    // Util
    public IUIDataSource getCurrentDataSource()
    {
        return this.getConfig().getDataSource(buildDataSourceName(getCurrentOrgUnit().getId()));
    }

    private static String buildDataSourceName(Long programSetOrgUnitId)
    {
        return String.valueOf(programSetOrgUnitId);
    }

    private static String buildOrgUnitCation(String orgUnitTitle, int ministerial, int contract)
    {
        return "Планы приема по образовательным программам (" + orgUnitTitle + ", КЦП — " + String.valueOf(ministerial) + ", план приема по договору — " + String.valueOf(contract) + ")";
    }

    /** Обертка плана приема по образовательной программе. */
    public static class ProgramSetItemWrapper extends IdentifiableWrapper<EnrProgramSetItem>
    {
        private Integer _plan;

        public ProgramSetItemWrapper(Long id, String title, Integer plan)
        {
            super(id, title);
            _plan = plan;
        }

        public ProgramSetItemWrapper(EnrProgramSetItem programSetItem, Integer plan)
        {
            this(programSetItem.getId(), programSetItem.getTitle(), plan);
        }

        public Integer getPlan() { return _plan; }
        public void setPlan(Integer plan) { _plan = plan; }
    }

    /** Подразделение ведущее прием. */
    public static interface ProgramSetOrgUnit
    {
        /** Идентификатор подразделения, ведущего прием. */
        public Long getId();

        /** Показывать сообщение о незаданных планах приема. */
        public boolean isShowWarningMessage();

        /** Заголовок списка. */
        public String getCaption();
    }
}