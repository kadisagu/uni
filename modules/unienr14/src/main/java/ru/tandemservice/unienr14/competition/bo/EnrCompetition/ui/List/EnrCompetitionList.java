/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionDSHandler;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.Pub.EnrCompetitionPub;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author oleyba
 * @since 2/3/14
 */
@Configuration
public class EnrCompetitionList extends BusinessComponentManager
{
    public static final String COMPETITION_DS = "competitionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        .addDataSource(CommonManager.instance().yesNoDSConfig())
        .addDataSource(searchListDS(COMPETITION_DS, programSetSearchDSColumns(), programSetSearchDSHandler()))
        .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
        .create();
    }

    @Bean
    public ColumnListExtPoint programSetSearchDSColumns()
    {
        return columnListExtPointBuilder("competitionDS")
            .addColumn(textColumn("requestType", EnrCompetition.requestType().shortTitle().s()).order())
            .addColumn(textColumn("developForm", EnrCompetition.programSetOrgUnit().programSet().programForm().shortTitle().s()).order())
            .addColumn(publisherColumn("programSet", EnrCompetition.programSetOrgUnit().programSet().title())
                .order()
                .publisherLinkResolver(new SimplePublisherLinkResolver(EnrCompetition.id())
                    .setComponent(EnrCompetitionPub.class))
            )
            .addColumn(textColumn("competitionType", EnrCompetition.type().shortTitle().s()).order())
            .addColumn(textColumn("eduLevelRequirement", EnrCompetition.eduLevelRequirement().shortTitle().s()).order())
            .addColumn(textColumn("programKind", EnrCompetition.programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().shortTitle().s()).order())
            .addColumn(textColumn("programSubject", EnrCompetition.programSetOrgUnit().programSet().programSubject().titleWithCode().s()).order())
            .addColumn(textColumn("orgUnit", EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()).order())
            .addColumn(textColumn("formativeOrgUnit", EnrCompetition.programSetOrgUnit().formativeOrgUnit().shortTitleWithTopEmphasized().s()).order())
            .addColumn(textColumn("plan", EnrCompetition.plan().s()).order())
            .addColumn(textColumn("requestCount", EnrCompetitionDSHandler.VIEW_PROP_REQUEST_COUNT))
            .addColumn(textColumn("passed", EnrCompetitionDSHandler.VIEW_PROP_PASSED))
            .addColumn(textColumn("exams", EnrCompetitionDSHandler.VIEW_PROP_EXAMS).formatter(NewLineFormatter.NOBR_IN_LINES))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSetSearchDSHandler()
    {
        return new EnrCompetitionDSHandler(getName());
    }
}