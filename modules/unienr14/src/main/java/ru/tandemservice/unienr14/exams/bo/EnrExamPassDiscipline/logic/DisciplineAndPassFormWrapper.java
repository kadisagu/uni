/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

/**
 * @author Nikolay Fedorovskih
 * @since 25.06.2015
 */
public class DisciplineAndPassFormWrapper extends DataWrapper
{
    private final Long _campaignDisciplineId;
    private final Long _passFormId;

    public DisciplineAndPassFormWrapper(long id, String title, Long campaignDisciplineId, Long passFormId)
    {
        super(id, title);
        _campaignDisciplineId = campaignDisciplineId;
        _passFormId = passFormId;
    }

    public Long getCampaignDisciplineId()
    {
        return _campaignDisciplineId;
    }

    public Long getPassFormId()
    {
        return _passFormId;
    }
}