/* $Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Add;

import java.util.Calendar;
import java.util.Date;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;

import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class DAO extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.DAO<EnrEntrant, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        EnrEntrant entrant = model.getEntrant();
        entrant.setRegistrationDate(new Date());
        entrant.setEnrollmentCampaign(get(model.getEnrollmentCampaignId()));
    }

    @Override
    public void create(Model model)
    {
        Person person = model.getPerson();
        IdentityCard identityCard = person.getIdentityCard();

        PersonManager.instance().dao().checkPersonUnique(person);

        DatabaseFile photo = new DatabaseFile();
        getSession().save(photo);

        identityCard.setPhoto(photo);
        getSession().save(identityCard);

        getSession().save(person.getContactData());

        getSession().save(person);

        identityCard.setPerson(person);
        getSession().update(identityCard);

        createOnBasis(model.getEntrant(), person);
    }

    @Override
    public void createOnBasis(Model model)
    {
        createOnBasis(model.getEntrant(), getBasisPerson(model));
    }

    private void createOnBasis(EnrEntrant entrant, Person person)
    {
        for (EnrEntrant baseEntrant : getList(EnrEntrant.class, EnrEntrant.L_PERSON, person))
            if (baseEntrant.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Данная персона уже является абитуриентом в выбранной приемной кампании.");

        entrant.setPersonalNumber(EnrEntrantManager.instance().dao().getUniqueEntrantNumber(entrant));
        entrant.setPerson(person);
        getSession().save(entrant);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        model.getEntrant().getEnrollmentCampaign().checkOpen();

        IdentityCard identityCard = model.getPerson().getIdentityCard();
        int amount = -13;
        if (identityCard.getBirthDate() != null && identityCard.getBirthDate().after(CoreDateUtils.add(CoreDateUtils.getDayFirstTimeMoment(new Date()), Calendar.YEAR, amount)))
            errors.add("Абитуриент должен быть старше 13 лет.", "birthDate");

        PersonManager.instance().dao().checkIdentityCard(model.getPerson().getIdentityCard(), errors);

        if (model.getEntrant().getRegistrationDate().after(new Date()))
        {
            errors.add("Дата добавления должна быть не позже сегодняшней.", "registrationDate");
        }
    }
}
