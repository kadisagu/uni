/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

/**
 * @author rsizonenko
 * @since 19.06.2014
 */
public class RtfBackslashScreener {
    public String screenBackslashes(String input)
    {
        StringBuilder builder = new StringBuilder().append(input);
        for (int i = 0; i < builder.length(); i++) {
            if (builder.charAt(i) == '\\')
            {
                if (builder.length() > i + 4)
                    if (builder.charAt(i + 1) == 'l')
                        if (builder.charAt(i + 2) == 'i')
                            if (builder.charAt(i + 3) == 'n')
                                if (builder.charAt(i + 4) == 'e')
                                    continue;
                builder.insert(i, '\\');
                i++;
            }
        }

        return builder.toString();
    }
}
