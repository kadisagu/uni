package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

@State({
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrEnrollmentStepCheckTabUI extends UIPresenter
{
    private String selectedTab;

    @Override
    public void onComponentRefresh() {
    }

    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(String selectedTab) { this.selectedTab = selectedTab; }
}
