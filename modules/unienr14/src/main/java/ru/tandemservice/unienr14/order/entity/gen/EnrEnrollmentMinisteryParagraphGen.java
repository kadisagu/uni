package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа о зачислении по направлению Минобрнауки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentMinisteryParagraphGen extends EnrAbstractParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph";
    public static final String ENTITY_NAME = "enrEnrollmentMinisteryParagraph";
    public static final int VERSION_HASH = -1521079899;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String L_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_PROGRAM_FORM = "programForm";
    public static final String L_PROGRAM_SUBJECT = "programSubject";

    private EnrCompetitionType _competitionType;     // Вид приема
    private EnrOrgUnit _enrOrgUnit;     // Филиал
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private EduProgramForm _programForm;     // Форма обучения
    private EduProgramSubject _programSubject;     // Направление, профессия, специальность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetitionType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Вид приема. Свойство не может быть null.
     */
    public void setCompetitionType(EnrCompetitionType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Филиал. Свойство не может быть null.
     */
    @NotNull
    public EnrOrgUnit getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал. Свойство не может быть null.
     */
    public void setEnrOrgUnit(EnrOrgUnit enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(EduProgramForm programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Направление, профессия, специальность. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, профессия, специальность. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEnrollmentMinisteryParagraphGen)
        {
            setCompetitionType(((EnrEnrollmentMinisteryParagraph)another).getCompetitionType());
            setEnrOrgUnit(((EnrEnrollmentMinisteryParagraph)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrEnrollmentMinisteryParagraph)another).getFormativeOrgUnit());
            setProgramForm(((EnrEnrollmentMinisteryParagraph)another).getProgramForm());
            setProgramSubject(((EnrEnrollmentMinisteryParagraph)another).getProgramSubject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentMinisteryParagraphGen> extends EnrAbstractParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentMinisteryParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentMinisteryParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "competitionType":
                    return obj.getCompetitionType();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programForm":
                    return obj.getProgramForm();
                case "programSubject":
                    return obj.getProgramSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "competitionType":
                    obj.setCompetitionType((EnrCompetitionType) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((EnrOrgUnit) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "programForm":
                    obj.setProgramForm((EduProgramForm) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "competitionType":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programForm":
                        return true;
                case "programSubject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "competitionType":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programForm":
                    return true;
                case "programSubject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "competitionType":
                    return EnrCompetitionType.class;
                case "enrOrgUnit":
                    return EnrOrgUnit.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "programForm":
                    return EduProgramForm.class;
                case "programSubject":
                    return EduProgramSubject.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentMinisteryParagraph> _dslPath = new Path<EnrEnrollmentMinisteryParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentMinisteryParagraph");
    }
            

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getCompetitionType()
     */
    public static EnrCompetitionType.Path<EnrCompetitionType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getEnrOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Направление, профессия, специальность. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    public static class Path<E extends EnrEnrollmentMinisteryParagraph> extends EnrAbstractParagraph.Path<E>
    {
        private EnrCompetitionType.Path<EnrCompetitionType> _competitionType;
        private EnrOrgUnit.Path<EnrOrgUnit> _enrOrgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EduProgramForm.Path<EduProgramForm> _programForm;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getCompetitionType()
     */
        public EnrCompetitionType.Path<EnrCompetitionType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new EnrCompetitionType.Path<EnrCompetitionType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getEnrOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> programForm()
        {
            if(_programForm == null )
                _programForm = new EduProgramForm.Path<EduProgramForm>(L_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Направление, профессия, специальность. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentMinisteryParagraph.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentMinisteryParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
