/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.CAFServices;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author nvankov
 * @since 6/2/14
 */
public class EnrEntrantWizardStep
{
    // название шага (уникальнй ключ)
    private final String _stepName;

    // название компоента
    private final String _componentName;

    // заголовок шага
    private final String _title;

    // недоступен для перехода в мастере
    private boolean _disabled = false;

    // валидировать на переходе на следующий шаг
    private boolean validateOnClickNext = true;

    // показывать кнопки работы с шагами из самого визарда
    private boolean showWizardButtons = true;

    public EnrEntrantWizardStep(String stepName, String componentName, String title) {
        _stepName = stepName;
        _componentName = componentName;
        _title = StringUtils.trimToEmpty(null != title ? title : CAFServices.configurationService().getBusinessComponentMeta(componentName).getTitle());
    }

    public EnrEntrantWizardStep(String stepName, Class<? extends BusinessComponentManager> component, String title) {
        this(stepName, component.getSimpleName(), title);
    }

    public boolean isDisabledStep()
    {
        return isDisabled();
    }

    // getters and setters

    public String getStepName()
    {
        return _stepName;
    }

    public String getComponentName()
    {
        return _componentName;
    }

    public String getTitle()
    {
        return _title;
    }

    public boolean isDisabled() { return _disabled; }

    public void setDisabled(boolean disabled)
    {
        _disabled = disabled;
    }

    public boolean isValidateOnClickNext()
    {
        return validateOnClickNext;
    }

    public void setValidateOnClickNext(boolean validateOnClickNext)
    {
        this.validateOnClickNext = validateOnClickNext;
    }

    public boolean isShowWizardButtons()
    {
        return showWizardButtons;
    }

    public void setShowWizardButtons(boolean showWizardButtons)
    {
        this.showWizardButtons = showWizardButtons;
    }
}
