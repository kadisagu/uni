/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DocumentAttach;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;

/**
 * @author oleyba
 * @since 5/4/13
 */
@Configuration
public class EnrEntrantRequestDocumentAttach extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEntrantBaseDocumentManager.instance().eduDocumentDSConfig())
                .addDataSource(EnrEntrantBaseDocumentManager.instance().identityCardDSConfig())
                .addDataSource(EnrEntrantRequestManager.instance().originalSubmissionAndReturnWayDSConfig())
                .addDataSource(EnrRequestedCompetitionManager.instance().createEnrRequestedCompetitionDS())
                .create();
    }
}

