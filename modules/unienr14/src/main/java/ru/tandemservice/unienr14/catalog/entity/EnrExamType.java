package ru.tandemservice.unienr14.catalog.entity;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrExamTypeGen;

/**
 * Тип вступительного испытания
 */
public class EnrExamType extends EnrExamTypeGen
{
    /**
     * @return true, если это «Обычное» вступительное испытание
     */
    public boolean isUsual() {
        return EnrExamTypeCodes.USUAL.equals(getCode());
    }

}