package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.settings.entity.gen.EnrCampaignEntrantDocumentGen;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Тип документа абитуриента, используемый в рамках ПК
 */
public class EnrCampaignEntrantDocument extends EnrCampaignEntrantDocumentGen
{
    private static final List<String> DOCUMENT_ACCEPTED_COPY_ORIG = Arrays.asList(PersonDocumentTypeCodes.EDU_INSTITUTION, PersonDocumentTypeCodes.ORPHAN,
            PersonDocumentTypeCodes.COMBAT_VETERANS, PersonDocumentTypeCodes.BELONGING_NATION, PersonDocumentTypeCodes.SPORTS_DIPLOMA);
    public EnrCampaignEntrantDocument()
    {
    }

    public EnrCampaignEntrantDocument(EnrEnrollmentCampaign campaign, PersonDocumentType documentType, Collection<String> benefitTypeCodes)
    {
        setEnrollmentCampaign(campaign);
        setDocumentType(documentType);

        if(DOCUMENT_ACCEPTED_COPY_ORIG.contains(documentType.getCode()))
        {
            setOriginalCanBeAccepted(true);
            setCopyCanBeAccepted(true);
        }
        else
        {
            setCopyCanBeAccepted(true);
            setOriginalCanBeAccepted(false);
        }

        if (documentType.getCode().equals(PersonDocumentTypeCodes.EDU_INSTITUTION) || PersonDocumentTypeCodes.SPORTS_DIPLOMA.equals(documentType.getCode()))
        {
            setAchievement(true);
        }

        if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(documentType.getCode()) && (2016 <= campaign.getEducationYear().getIntValue()))
        {
            setAchievement(true);
        }

        for (String benefitTypeCode: benefitTypeCodes) {
            setBenefitValue(benefitTypeCode, true);
        }
    }

    public EnrCampaignEntrantDocument(EnrEnrollmentCampaign campaign, PersonDocumentType documentType)
    {
        setEnrollmentCampaign(campaign);
        setDocumentType(documentType);
        setCopyCanBeAccepted(true);
        setOriginalCanBeAccepted(false);
    }

    public static String getBenefitProperty(String benefitTypeCode) {
        switch(benefitTypeCode) {
            case EnrBenefitTypeCodes.PREFERENCE: return P_BENEFIT_PREFERENCE;
            case EnrBenefitTypeCodes.EXCLUSIVE: return P_BENEFIT_EXCLUSIVE;
            case EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS: return P_BENEFIT_NO_EXAMS;
            case EnrBenefitTypeCodes.MAX_EXAM_RESULT: return P_BENEFIT_MAX_MARK;
        }
        throw new IllegalArgumentException("Undefined benefitTypeCode=«"+benefitTypeCode+"»");
    }

    public boolean getBenefitValue(String benefitTypeCode) {
        return Boolean.TRUE.equals(getProperty(getBenefitProperty(benefitTypeCode)));
    }

    public void setBenefitValue(String benefitTypeCode, boolean value) {
        setProperty(getBenefitProperty(benefitTypeCode), Boolean.valueOf(value));
    }


}
