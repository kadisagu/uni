/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.CitizenshipModel;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.EnrEnrollmentParagraphManager;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements.EnrSettingsAdmissionRequirements;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author oleyba
 * @since 7/8/14
 */
@Input({
    @Bind(key = EnrEnrollmentParagraphAddEditUI.PARAMETER_ORDER_ID, binding = EnrEnrollmentParagraphAddEditUI.PARAMETER_ORDER_ID),
    @Bind(key = EnrEnrollmentParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, binding = EnrEnrollmentParagraphAddEditUI.PARAMETER_PARAGRAPH_ID)
})
public class EnrEnrollmentParagraphAddEditUI extends UIPresenter
{
    public static final String PARAMETER_ORDER_ID = "order.id";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraph.id";

    private EnrOrder _order = new EnrOrder();
    private EnrEnrollmentParagraph _paragraph = new EnrEnrollmentParagraph();
    private boolean _editForm;

    // выбор конкурсов
    private List<EnrProgramSetBase> programSetList;
    private List<EnrCompetition> competitionList;
    private List<EnrTargetAdmissionKind> targetAdmissionKindList;
    private boolean onlyEnrolledMarked;
    private List<EnrEntrantCustomStateType>  _statusList;

    private Set<Long> _selectedEntrantIds = new HashSet<>();
    private Long _groupManagerEntrantId;
    private String _groupTitle;

    private ISelectModel _citizenshipModel;
    private DataWrapper _citizenshipFilter;

    private boolean _originalHandIn = false;
    private boolean _enrollmentAccepted = false;
    private boolean _enrollmentAvailable = false;

    @Override
    public void onComponentRefresh()
    {
        if (getParagraph().getId() == null)
        {
            // создание параграфа
            setEditForm(false);
            setOrder(DataAccessServices.dao().getNotNull(EnrOrder.class, getOrder().getId()));
            getParagraph().setOrder(getOrder());
        }
        else
        {
            // редактирование параграфа
            setEditForm(true);
            setParagraph(DataAccessServices.dao().getNotNull(EnrEnrollmentParagraph.class, getParagraph().getId()));
            setOrder((EnrOrder) getParagraph().getOrder());

            for (EnrEnrollmentExtract extract : IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph(), getParagraph(), EnrEnrollmentExtract.number().s()))
            {
                getSelectedEntrantIds().add(extract.getEntity().getId());

                if (null != extract.getGroupTitle()) setGroupTitle(extract.getGroupTitle());

                //EduProgramForm extractForm = extract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm();
            }
            setGroupManagerEntrantId(getParagraph().getGroupManager() == null ? null : getParagraph().getGroupManager().getEntity().getId());
        }

        setCitizenshipModel(new CitizenshipModel());

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getOrder().getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + getOrder().getState().getTitle() +"», редактирование параграфов запрещено.");
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_PARAGRAPH, getParagraph());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_PARALLEL, getParagraph().isParallel());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getOrder().getEnrollmentCampaign());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_REQUEST_TYPE, getOrder().getRequestType());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_COMPENSATION_TYPE, getOrder().getCompensationType());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_FORM, getParagraph().getProgramForm());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_ORG_UNIT, getParagraph().getEnrOrgUnit());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_FORMATIVE_ORG_UNIT, getParagraph().getFormativeOrgUnit());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_SUBJECT, getParagraph().getProgramSubject());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_PROGRAM_SET_LIST, getProgramSetList());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_COMPETITION_TYPE, getParagraph().getCompetitionType());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_COMPETITION_LIST, getCompetitionList());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED, isOnlyEnrolledMarked());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_ORIGINAL_HAND_IN, isOriginalHandIn());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_ENROLLMENT_ACCEPTED, isEnrollmentAccepted());
        dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_ENROLLMENT_AVAILABLE, isEnrollmentAvailable());
        if (EnrEnrollmentParagraphAddEdit.DS_ENTRANT.equals(dataSource.getName()))
        {
            dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_TARGET_ADMISIION_KIND_LIST, getTargetAdmissionKindList());
            dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_CITIZENSHIP_ID, getCitizenshipFilter() != null ? getCitizenshipFilter().getId() : null);
            dataSource.put(EnrEnrollmentParagraphAddEdit.BIND_CUSTOM_STATE, getStatusList());

        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (isEditForm() && "entrantDS".equals(dataSource.getName()))
        {
            DynamicListDataSource ds = ((PageableSearchListDataSource) dataSource).getLegacyDataSource();

            List<IEntity> checkboxColumnSelected = new ArrayList<IEntity>();
            IEntity radioboxColumnSelected = null;

            for (IEntity record : (List<IEntity>) ds.getEntityList())
            {
                if (getSelectedEntrantIds().contains(record.getId()))
                    checkboxColumnSelected.add(record);

                if (record.getId().equals(getGroupManagerEntrantId()))
                    radioboxColumnSelected = record;
            }

            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrEnrollmentParagraphAddEdit.CHECKBOX_COLUMN));
            if (checkboxColumn != null)
                checkboxColumn.setSelectedObjects(checkboxColumnSelected);

            RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EnrEnrollmentParagraphAddEdit.RADIOBOX_COLUMN));
            if (radioboxColumn != null)
                radioboxColumn.setSelectedEntity(radioboxColumnSelected);
        }
    }


    // Listeners

    public void onRefreshSelectionParams() {
        // ?
        if (getTargetAdmissionKindList() != null && getParagraph().getCompetitionType() != null && !EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(getParagraph().getCompetitionType().getCode()))
        {
            getTargetAdmissionKindList().clear();
        }
    }

    public void onClickApply()
    {
        try {

            DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource("entrantDS")).getLegacyDataSource();
            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrEnrollmentParagraphAddEdit.CHECKBOX_COLUMN));
            RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EnrEnrollmentParagraphAddEdit.RADIOBOX_COLUMN));

            List<EnrRequestedCompetition> preStudents = new ArrayList<EnrRequestedCompetition>();
            if (checkboxColumn != null) {
                for (IEntity entity : checkboxColumn.getSelectedObjects()) {
                    preStudents.add((EnrRequestedCompetition) ((DataWrapper) entity).getWrapped());
                    getSelectedEntrantIds().add(entity.getId());
                }
            }

            EnrRequestedCompetition manager = null;
            if (getOrder().getPrintFormType().isSelectHeadman() && radioboxColumn != null && radioboxColumn.getSelectedEntity() != null)
                manager = ((DataWrapper) radioboxColumn.getSelectedEntity()).getWrapped();

            // сохраняем или обновляем параграф о зачислении
            EnrEnrollmentParagraphManager.instance().dao().saveOrUpdateParagraph(_order, _paragraph, preStudents, manager, getGroupTitle());

            // закрываем форму
            deactivate();

        } catch (Throwable t) {
            Debug.exception(t);
            _uiSupport.setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // presenter
    public String getFormTitle() {
        String formTitle = (StringUtils.isNotEmpty(_order.getNumber()) ? "№" + _order.getNumber() + " " : "") + (_order.getCommitDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()) : "");
        if (isEditForm())
            formTitle = "Редактирование параграфа из приказа о зачислении " + formTitle;
        else
            formTitle = "Добавление параграфа приказа о зачислении " + formTitle;
        return formTitle;
    }

    // Getters & Setters

    public boolean isOriginalHandInCheckboxDisabled()
    {
        return !getOrder().getCompensationType().isBudget() && EnrSettingsAdmissionRequirements.OPTION_ENROLL_RULES_CONTRACT_SECOND.equals(getOrder().getEnrollmentCampaign().getSettings().getEnrollmentRulesContract());
    }

    public boolean isEnrollmentAcceptedCheckboxDisabled()
    {
        EnrEnrollmentCampaignSettings settings = getOrder().getEnrollmentCampaign().getSettings();
        return (getOrder().getCompensationType().isBudget() && TwinComboDataSourceHandler.YES_ID.equals(settings.getEnrollmentRulesBudget()))
                    || (!getOrder().getCompensationType().isBudget() && EnrSettingsAdmissionRequirements.OPTION_ENROLL_RULES_CONTRACT_FIRST.equals(settings.getEnrollmentRulesContract()));
    }

    public boolean isTargetAdmissionKindVisible()
    {
        return getParagraph().getCompetitionType() != null && EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(getParagraph().getCompetitionType().getCode());
    }

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public EnrEnrollmentParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrEnrollmentParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public Set<Long> getSelectedEntrantIds()
    {
        return _selectedEntrantIds;
    }

    public void setSelectedEntrantIds(Set<Long> selectedEntrantIds)
    {
        _selectedEntrantIds = selectedEntrantIds;
    }

    public Long getGroupManagerEntrantId()
    {
        return _groupManagerEntrantId;
    }

    public void setGroupManagerEntrantId(Long groupManagerEntrantId)
    {
        _groupManagerEntrantId = groupManagerEntrantId;
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        _groupTitle = groupTitle;
    }

    public List<EnrCompetition> getCompetitionList()
    {
        return competitionList;
    }

    public void setCompetitionList(List<EnrCompetition> competitionList)
    {
        this.competitionList = competitionList;
    }

    public List<EnrProgramSetBase> getProgramSetList()
    {
        return programSetList;
    }

    public void setProgramSetList(List<EnrProgramSetBase> programSetList)
    {
        this.programSetList = programSetList;
    }

    public boolean isOnlyEnrolledMarked()
    {
        return onlyEnrolledMarked;
    }

    public void setOnlyEnrolledMarked(boolean onlyEnrolledMarked)
    {
        this.onlyEnrolledMarked = onlyEnrolledMarked;
    }

    public List<EnrTargetAdmissionKind> getTargetAdmissionKindList()
    {
        return targetAdmissionKindList;
    }

    public void setTargetAdmissionKindList(List<EnrTargetAdmissionKind> targetAdmissionKindList)
    {
        this.targetAdmissionKindList = targetAdmissionKindList;
    }

    public ISelectModel getCitizenshipModel()
    {
        return this._citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel)
    {
        this._citizenshipModel = citizenshipModel;
    }

    public DataWrapper getCitizenshipFilter()
    {
        return _citizenshipFilter;
    }

    public void setCitizenshipFilter(DataWrapper citizenshipFilter)
    {
        _citizenshipFilter = citizenshipFilter;
    }

    public boolean isOriginalHandIn()
    {
        return _originalHandIn;
    }

    public void setOriginalHandIn(boolean originalHandIn)
    {
        _originalHandIn = originalHandIn;
    }

    public boolean isEnrollmentAccepted()
    {
        return _enrollmentAccepted;
    }

    public void setEnrollmentAccepted(boolean enrollmentAccepted)
    {
        _enrollmentAccepted = enrollmentAccepted;
    }

    public boolean isEnrollmentAvailable()
    {
        return _enrollmentAvailable;
    }

    public void setEnrollmentAvailable(boolean enrollmentAvailable)
    {
        _enrollmentAvailable = enrollmentAvailable;
    }

    public List<EnrEntrantCustomStateType> getStatusList()
    {
        return _statusList;
    }

    public void setStatusList(List<EnrEntrantCustomStateType> statusList)
    {
        _statusList = statusList;
    }
}