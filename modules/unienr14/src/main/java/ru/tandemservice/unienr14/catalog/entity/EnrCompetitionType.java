package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrCompetitionTypeGen;

/**
 * Вид приема
 */
public class EnrCompetitionType extends EnrCompetitionTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    @EntityDSLSupport
    @Override
    public int getPriorityForProgramAlloc()
    {
        switch (getCode()) {
            case EnrCompetitionTypeCodes.TARGET_ADMISSION : return 1;
            case EnrCompetitionTypeCodes.EXCLUSIVE : return 2;
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL : return 3;
            case EnrCompetitionTypeCodes.MINISTERIAL : return 4;
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT : return 5;
            case EnrCompetitionTypeCodes.CONTRACT : return 6;
        }
        return 7;
    }
}