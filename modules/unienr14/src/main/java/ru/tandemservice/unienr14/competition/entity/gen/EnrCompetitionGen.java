package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Конкурс (комбинация условий поступления) для приема
 *
 * Конкурс - единая сущность, которая собирает в себя все условия приема универсальным образом, и именно ее абитуриент указывает в заявлении.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCompetitionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrCompetition";
    public static final String ENTITY_NAME = "enrCompetition";
    public static final int VERSION_HASH = 1853030168;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET_ORG_UNIT = "programSetOrgUnit";
    public static final String L_EDU_LEVEL_REQUIREMENT = "eduLevelRequirement";
    public static final String L_TYPE = "type";
    public static final String L_EXAM_SET_VARIANT = "examSetVariant";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String P_ALLOW_PROGRAM_PRIORITIES = "allowProgramPriorities";
    public static final String P_PLAN = "plan";
    public static final String P_CALCULATED_PLAN = "calculatedPlan";
    public static final String P_FIS_UID_POSTFIX = "fisUidPostfix";
    public static final String P_TITLE = "title";

    private EnrProgramSetOrgUnit _programSetOrgUnit;     // Набор ОП на подразделении
    private EnrEduLevelRequirement _eduLevelRequirement;     // Базовый уровень образования поступающих
    private EnrCompetitionType _type;     // Вид приема
    private EnrExamSetVariant _examSetVariant;     // Набор ВИ
    private EnrRequestType _requestType;     // Вид заявления
    private boolean _allowProgramPriorities;     // Разрешить выбор абитуриентом приоритетов ОП в рамках конкурса
    private int _plan;     // Число мест
    private int _calculatedPlan;     // Число мест (вычисляемый)
    private String _fisUidPostfix;     // Постфикс для uid КГ ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Определяет Акк.ОУ, форму обучения и перечень ОП, на которые объявляется конкурс.
     *
     * @return Набор ОП на подразделении. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetOrgUnit getProgramSetOrgUnit()
    {
        return _programSetOrgUnit;
    }

    /**
     * @param programSetOrgUnit Набор ОП на подразделении. Свойство не может быть null.
     */
    public void setProgramSetOrgUnit(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        dirty(_programSetOrgUnit, programSetOrgUnit);
        _programSetOrgUnit = programSetOrgUnit;
    }

    /**
     * @return Базовый уровень образования поступающих. Свойство не может быть null.
     */
    @NotNull
    public EnrEduLevelRequirement getEduLevelRequirement()
    {
        return _eduLevelRequirement;
    }

    /**
     * @param eduLevelRequirement Базовый уровень образования поступающих. Свойство не может быть null.
     */
    public void setEduLevelRequirement(EnrEduLevelRequirement eduLevelRequirement)
    {
        dirty(_eduLevelRequirement, eduLevelRequirement);
        _eduLevelRequirement = eduLevelRequirement;
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetitionType getType()
    {
        return _type;
    }

    /**
     * @param type Вид приема. Свойство не может быть null.
     */
    public void setType(EnrCompetitionType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetVariant getExamSetVariant()
    {
        return _examSetVariant;
    }

    /**
     * @param examSetVariant Набор ВИ. Свойство не может быть null.
     */
    public void setExamSetVariant(EnrExamSetVariant examSetVariant)
    {
        dirty(_examSetVariant, examSetVariant);
        _examSetVariant = examSetVariant;
    }

    /**
     * Кэш.
     *
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Разрешить выбор абитуриентом приоритетов ОП в рамках конкурса. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowProgramPriorities()
    {
        return _allowProgramPriorities;
    }

    /**
     * @param allowProgramPriorities Разрешить выбор абитуриентом приоритетов ОП в рамках конкурса. Свойство не может быть null.
     */
    public void setAllowProgramPriorities(boolean allowProgramPriorities)
    {
        dirty(_allowProgramPriorities, allowProgramPriorities);
        _allowProgramPriorities = allowProgramPriorities;
    }

    /**
     * Действующее число мест. Вводится пользователем и/или обновляется на основании calculatedPlan, подробности в актуальной постановке.
     *
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Число мест. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    /**
     * Рассчитывается системой, служит для обновления действующего числа мест.
     *
     * @return Число мест (вычисляемый). Свойство не может быть null.
     */
    @NotNull
    public int getCalculatedPlan()
    {
        return _calculatedPlan;
    }

    /**
     * @param calculatedPlan Число мест (вычисляемый). Свойство не может быть null.
     */
    public void setCalculatedPlan(int calculatedPlan)
    {
        dirty(_calculatedPlan, calculatedPlan);
        _calculatedPlan = calculatedPlan;
    }

    /**
     * @return Постфикс для uid КГ ФИС.
     */
    @Length(max=255)
    public String getFisUidPostfix()
    {
        return _fisUidPostfix;
    }

    /**
     * @param fisUidPostfix Постфикс для uid КГ ФИС.
     */
    public void setFisUidPostfix(String fisUidPostfix)
    {
        dirty(_fisUidPostfix, fisUidPostfix);
        _fisUidPostfix = fisUidPostfix;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCompetitionGen)
        {
            setProgramSetOrgUnit(((EnrCompetition)another).getProgramSetOrgUnit());
            setEduLevelRequirement(((EnrCompetition)another).getEduLevelRequirement());
            setType(((EnrCompetition)another).getType());
            setExamSetVariant(((EnrCompetition)another).getExamSetVariant());
            setRequestType(((EnrCompetition)another).getRequestType());
            setAllowProgramPriorities(((EnrCompetition)another).isAllowProgramPriorities());
            setPlan(((EnrCompetition)another).getPlan());
            setCalculatedPlan(((EnrCompetition)another).getCalculatedPlan());
            setFisUidPostfix(((EnrCompetition)another).getFisUidPostfix());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCompetitionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCompetition.class;
        }

        public T newInstance()
        {
            return (T) new EnrCompetition();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSetOrgUnit":
                    return obj.getProgramSetOrgUnit();
                case "eduLevelRequirement":
                    return obj.getEduLevelRequirement();
                case "type":
                    return obj.getType();
                case "examSetVariant":
                    return obj.getExamSetVariant();
                case "requestType":
                    return obj.getRequestType();
                case "allowProgramPriorities":
                    return obj.isAllowProgramPriorities();
                case "plan":
                    return obj.getPlan();
                case "calculatedPlan":
                    return obj.getCalculatedPlan();
                case "fisUidPostfix":
                    return obj.getFisUidPostfix();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSetOrgUnit":
                    obj.setProgramSetOrgUnit((EnrProgramSetOrgUnit) value);
                    return;
                case "eduLevelRequirement":
                    obj.setEduLevelRequirement((EnrEduLevelRequirement) value);
                    return;
                case "type":
                    obj.setType((EnrCompetitionType) value);
                    return;
                case "examSetVariant":
                    obj.setExamSetVariant((EnrExamSetVariant) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "allowProgramPriorities":
                    obj.setAllowProgramPriorities((Boolean) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
                case "calculatedPlan":
                    obj.setCalculatedPlan((Integer) value);
                    return;
                case "fisUidPostfix":
                    obj.setFisUidPostfix((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSetOrgUnit":
                        return true;
                case "eduLevelRequirement":
                        return true;
                case "type":
                        return true;
                case "examSetVariant":
                        return true;
                case "requestType":
                        return true;
                case "allowProgramPriorities":
                        return true;
                case "plan":
                        return true;
                case "calculatedPlan":
                        return true;
                case "fisUidPostfix":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSetOrgUnit":
                    return true;
                case "eduLevelRequirement":
                    return true;
                case "type":
                    return true;
                case "examSetVariant":
                    return true;
                case "requestType":
                    return true;
                case "allowProgramPriorities":
                    return true;
                case "plan":
                    return true;
                case "calculatedPlan":
                    return true;
                case "fisUidPostfix":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSetOrgUnit":
                    return EnrProgramSetOrgUnit.class;
                case "eduLevelRequirement":
                    return EnrEduLevelRequirement.class;
                case "type":
                    return EnrCompetitionType.class;
                case "examSetVariant":
                    return EnrExamSetVariant.class;
                case "requestType":
                    return EnrRequestType.class;
                case "allowProgramPriorities":
                    return Boolean.class;
                case "plan":
                    return Integer.class;
                case "calculatedPlan":
                    return Integer.class;
                case "fisUidPostfix":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCompetition> _dslPath = new Path<EnrCompetition>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCompetition");
    }
            

    /**
     * Определяет Акк.ОУ, форму обучения и перечень ОП, на которые объявляется конкурс.
     *
     * @return Набор ОП на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getProgramSetOrgUnit()
     */
    public static EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
    {
        return _dslPath.programSetOrgUnit();
    }

    /**
     * @return Базовый уровень образования поступающих. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getEduLevelRequirement()
     */
    public static EnrEduLevelRequirement.Path<EnrEduLevelRequirement> eduLevelRequirement()
    {
        return _dslPath.eduLevelRequirement();
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getType()
     */
    public static EnrCompetitionType.Path<EnrCompetitionType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getExamSetVariant()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
    {
        return _dslPath.examSetVariant();
    }

    /**
     * Кэш.
     *
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Разрешить выбор абитуриентом приоритетов ОП в рамках конкурса. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#isAllowProgramPriorities()
     */
    public static PropertyPath<Boolean> allowProgramPriorities()
    {
        return _dslPath.allowProgramPriorities();
    }

    /**
     * Действующее число мест. Вводится пользователем и/или обновляется на основании calculatedPlan, подробности в актуальной постановке.
     *
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    /**
     * Рассчитывается системой, служит для обновления действующего числа мест.
     *
     * @return Число мест (вычисляемый). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getCalculatedPlan()
     */
    public static PropertyPath<Integer> calculatedPlan()
    {
        return _dslPath.calculatedPlan();
    }

    /**
     * @return Постфикс для uid КГ ФИС.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getFisUidPostfix()
     */
    public static PropertyPath<String> fisUidPostfix()
    {
        return _dslPath.fisUidPostfix();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrCompetition> extends EntityPath<E>
    {
        private EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> _programSetOrgUnit;
        private EnrEduLevelRequirement.Path<EnrEduLevelRequirement> _eduLevelRequirement;
        private EnrCompetitionType.Path<EnrCompetitionType> _type;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariant;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private PropertyPath<Boolean> _allowProgramPriorities;
        private PropertyPath<Integer> _plan;
        private PropertyPath<Integer> _calculatedPlan;
        private PropertyPath<String> _fisUidPostfix;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Определяет Акк.ОУ, форму обучения и перечень ОП, на которые объявляется конкурс.
     *
     * @return Набор ОП на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getProgramSetOrgUnit()
     */
        public EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
        {
            if(_programSetOrgUnit == null )
                _programSetOrgUnit = new EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit>(L_PROGRAM_SET_ORG_UNIT, this);
            return _programSetOrgUnit;
        }

    /**
     * @return Базовый уровень образования поступающих. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getEduLevelRequirement()
     */
        public EnrEduLevelRequirement.Path<EnrEduLevelRequirement> eduLevelRequirement()
        {
            if(_eduLevelRequirement == null )
                _eduLevelRequirement = new EnrEduLevelRequirement.Path<EnrEduLevelRequirement>(L_EDU_LEVEL_REQUIREMENT, this);
            return _eduLevelRequirement;
        }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getType()
     */
        public EnrCompetitionType.Path<EnrCompetitionType> type()
        {
            if(_type == null )
                _type = new EnrCompetitionType.Path<EnrCompetitionType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getExamSetVariant()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
        {
            if(_examSetVariant == null )
                _examSetVariant = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT, this);
            return _examSetVariant;
        }

    /**
     * Кэш.
     *
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Разрешить выбор абитуриентом приоритетов ОП в рамках конкурса. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#isAllowProgramPriorities()
     */
        public PropertyPath<Boolean> allowProgramPriorities()
        {
            if(_allowProgramPriorities == null )
                _allowProgramPriorities = new PropertyPath<Boolean>(EnrCompetitionGen.P_ALLOW_PROGRAM_PRIORITIES, this);
            return _allowProgramPriorities;
        }

    /**
     * Действующее число мест. Вводится пользователем и/или обновляется на основании calculatedPlan, подробности в актуальной постановке.
     *
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrCompetitionGen.P_PLAN, this);
            return _plan;
        }

    /**
     * Рассчитывается системой, служит для обновления действующего числа мест.
     *
     * @return Число мест (вычисляемый). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getCalculatedPlan()
     */
        public PropertyPath<Integer> calculatedPlan()
        {
            if(_calculatedPlan == null )
                _calculatedPlan = new PropertyPath<Integer>(EnrCompetitionGen.P_CALCULATED_PLAN, this);
            return _calculatedPlan;
        }

    /**
     * @return Постфикс для uid КГ ФИС.
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getFisUidPostfix()
     */
        public PropertyPath<String> fisUidPostfix()
        {
            if(_fisUidPostfix == null )
                _fisUidPostfix = new PropertyPath<String>(EnrCompetitionGen.P_FIS_UID_POSTFIX, this);
            return _fisUidPostfix;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrCompetition#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrCompetitionGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrCompetition.class;
        }

        public String getEntityName()
        {
            return "enrCompetition";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
