/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 4/2/15
 */
public class EnrEnrollementStepPrevEnrollmentFact extends IdentifiableWrapper implements IEnrSelectionPrevEnrollmentFact
{
    private EnrEnrollmentExtract extract;

    public EnrEnrollementStepPrevEnrollmentFact(EnrEnrollmentExtract extract)
    {
        super(extract.getId(), extract.getTitle());
        this.extract = extract;
    }

    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return extract.getEntity();
    }

    @Override
    public String getInfo()
    {
        return extract.getOrderInfo();
    }
}
