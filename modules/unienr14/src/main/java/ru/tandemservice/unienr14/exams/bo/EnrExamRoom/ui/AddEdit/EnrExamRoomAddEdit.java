/**
 *$Id: EnrExamRoomAddEdit.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrExamRoomType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 21.05.13
 */
@Configuration
public class EnrExamRoomAddEdit extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String BUILDING_SELECT_DS = "buildingSelectDS";
    public static final String PLACE_SELECT_DS = "placeSelectDS";
    public static final String EXAM_ROOM_TYPE_SELECT_DS = "examRoomTypeSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(BUILDING_SELECT_DS, buildingSelectDSHandler()).addColumn(UniplacesBuilding.titleWithAddress().s()))
                .addDataSource(selectDS(PLACE_SELECT_DS, placeSelectDSHandler()).addColumn(UniplacesPlace.titleWithLocation().s()))
                .addDataSource(selectDS(EXAM_ROOM_TYPE_SELECT_DS, examRoomTypeSelectDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> examRoomTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamRoomType.class)
                .order(EnrExamRoomType.title())
                .filter(EnrExamRoomType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> placeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesPlace.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final UniplacesBuilding building = context.get(EnrExamRoomAddEditUI.PROP_BUILDING);

                dql.where(eq(property(UniplacesPlace.floor().unit().building().fromAlias(alias)), value(building)));
                dql.where(or(
                        eq(property(UniplacesPlace.purpose().code().fromAlias(alias)), value(UniplacesPlacePurposeCodes.AUDITORIYA_OBTSHAYA)),
                        eq(property(UniplacesPlace.purpose().code().fromAlias(alias)), value(UniplacesPlacePurposeCodes.AUDITORIYA_SPETSIALIZIROVANNAYA))
                ));
            }
        }
                .order(UniplacesPlace.title())
                .filter(UniplacesPlace.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> buildingSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesBuilding.class)
                .order(UniplacesBuilding.title())
                .filter(UniplacesBuilding.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql
                    .where(exists(new DQLSelectBuilder()
                        .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                        .where(eq(property("psou", EnrProgramSetOrgUnit.orgUnit().id()), property(alias, EnrOrgUnit.id())))
                        .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().enrollmentCampaign()), value(context.<IIdentifiable>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .buildQuery()));
            }
        };
    }
}
