/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import javax.validation.constraints.NotNull;

/**
 * @author azhebko
 * @since 31.07.2014
 */
public interface IEnrSubmittedEduDocumentDao extends INeedPersistenceSupport
{
    /** Меняет статус оригинала документа об образовании на противополжный (создает оригинал, если не было). */
    void updateEntrantOriginalDocumentStatus(Long personEduDocumentId, EnrEnrollmentCampaign campaign);

    /** Список всех уникальных названий организаций, в которую подан оригинал */
    SingleSelectTextModel getOrganizationOriginalInDS();

    /**
     * Сохраняет организацию, в которую подан оригинал
     *
     * @param entrant абитуриент
     * @param eduDocument документ о полученном образовании
     * @param title название
     */
    void saveOrUpdateOrganizationOriginalIn(@NotNull EnrEntrant entrant, @NotNull PersonEduDocument eduDocument, String title);
}