/**
 *$Id: EnrEntrantEditPreparationData.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditPreparationData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;

/**
 * @author Alexander Shaburov
 * @since 19.08.13
 */
@Configuration
public class EnrEntrantEditPreparationData extends BusinessComponentManager
{
    public static final String ACCESS_COURSE_SELECT_DS = "accessCourseSelectDS";
    public static final String ACCESS_DEPARTMENT_SELECT_DS = "accessDepartmentSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ACCESS_COURSE_SELECT_DS, accessCourseSelectDSHandler()))
                .addDataSource(selectDS(ACCESS_DEPARTMENT_SELECT_DS, accessDepartmentSelectDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> accessDepartmentSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAccessDepartment.class)
                .filter(EnrAccessDepartment.title())
                .order(EnrAccessDepartment.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> accessCourseSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAccessCourse.class)
                .filter(EnrAccessCourse.title())
                .order(EnrAccessCourse.title())
                .pageable(true);
    }
}
