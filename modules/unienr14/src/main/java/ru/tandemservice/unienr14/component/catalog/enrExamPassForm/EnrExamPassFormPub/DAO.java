/**
 *$Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrExamPassForm.EnrExamPassFormPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;

/**
 * @author Alexander Shaburov
 * @since 14.06.13
 */
public class DAO extends DefaultCatalogPubDAO<EnrExamPassForm, Model> implements IDAO
{
}
