/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/30/15
 */
public class EnrEnrollmentModelDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrEnrollmentModelDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentModel.class, "s")
            .column(property("s"))
            .where(eq(property(EnrEnrollmentStep.enrollmentCampaign().fromAlias("s")), value(context.<IIdentifiable>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))));

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();
    }
}
