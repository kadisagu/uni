/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author nvankov
 * @since 5/19/14
 */
@Configuration
public class EnrOnlineEntrantManager extends BusinessObjectManager
{
    public static EnrOnlineEntrantManager instance()
    {
        return instance(EnrOnlineEntrantManager.class);
    }
}



    