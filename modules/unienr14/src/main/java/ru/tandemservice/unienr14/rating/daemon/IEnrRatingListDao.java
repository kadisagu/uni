package ru.tandemservice.unienr14.rating.daemon;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;

/**
 * @author vdanilov
 */
public interface IEnrRatingListDao {

    final SpringBeanCache<IEnrRatingListDao> instance = new SpringBeanCache<IEnrRatingListDao>(IEnrRatingListDao.class.getName());

    /**
     * @return { entrantRatingCompetitionData(enrCompetition) } для всех используемых конкурсов
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона EnrRatingListDao")
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public List<EntrantRatingCompetitionData> getAllCompetitionIds();

    /**
     * @param competitionData entrantRatingCompetitionData(enrCompetition)
     * @return { entrantRatingData для всех активных выбранных конкурсов указанного конкурса }
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона EnrRatingListDao")
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public List<EntrantRatingData> getRatingItemDataList(EntrantRatingCompetitionData competitionData);

    /**
     * МЕТОД МОЖЕТ ВЫЗЫВАТЬСЯ ТОЛЬКО ВНУТРИ ДЕМОНА EnrRatingListDao
     * 
     * Обновляет ссылки на активные выбранные конкурсы,
     * сбрасывает признак активности выбранного конкурса,
     * добавляет недостающие записи для активных выбранных конкурсов
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона EnrRatingListDao")
    @Transactional(propagation=Propagation.REQUIRED)
    public boolean doPrepareRatingItems();

    /**
     * МЕТОД МОЖЕТ ВЫЗЫВАТЬСЯ ТОЛЬКО ВНУТРИ ДЕМОНА EnrRatingListDao
     * 
     * Обновляет данные enrRatingItem на основе dataList
     * 
     * @param updateList данные, подготовленные getRatingItemDataList и {@link EnrRatingListDao#setupValueHash(List)}
     * @return true, если что-то изменилось
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона EnrRatingListDao")
    @Transactional(propagation=Propagation.REQUIRED)
    public boolean doSaveRatingList(List<EntrantRatingData> updateList);



}
