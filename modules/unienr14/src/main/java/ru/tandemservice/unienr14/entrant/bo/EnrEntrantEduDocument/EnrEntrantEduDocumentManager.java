/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.datasource.select.ISelectDSConfigBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.logic.EnrEntrantEduDocumentDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.logic.IEnrEntrantEduDocumentDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements.EnrSettingsAdmissionRequirements;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author oleyba
 * @since 3/18/14
 */
@Configuration
public class EnrEntrantEduDocumentManager extends BusinessObjectManager
{
    public static EnrEntrantEduDocumentManager instance()
    {
        return instance(EnrEntrantEduDocumentManager.class);
    }

    @Bean
    public IEnrEntrantEduDocumentDao dao()
    {
        return new EnrEntrantEduDocumentDao();
    }
}
