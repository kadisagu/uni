/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;

/**
 * @author nvankov
 * @since 7/15/14
 */
public interface IEnrEntrantForeignRequestDAO extends INeedPersistenceSupport
{
    Long createOrUpdate(EnrEntrantForeignRequest request);

    boolean entrantTabVisible(Long entrantId);
}
