package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние абитуриента"
 * Имя сущности : enrEntrantState
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEntrantStateCodes
{
    /** Константа кода (code) элемента : Активный (title) */
    String ACTIVE = "1";
    /** Константа кода (code) элемента : Выбыл из конкурса (title) */
    String OUT_OF_COMPETITION = "2";
    /** Константа кода (code) элемента : Зачислен (title) */
    String ENROLLED = "3";
    /** Константа кода (code) элемента : Сданы ВИ (title) */
    String EXAMS_PASSED = "4";
    /** Константа кода (code) элемента : Рекомендован (title) */
    String RECOMMENDED = "5";
    /** Константа кода (code) элемента : В приказе (title) */
    String IN_ORDER = "6";
    /** Константа кода (code) элемента : Забрал документы (title) */
    String TAKE_DOCUMENTS_AWAY = "7";
    /** Константа кода (code) элемента : Отказ от зачисления (title) */
    String EXCLUDED_BY_REJECT = "8";
    /** Константа кода (code) элемента : Исключен (зачислен на другой конкурс) (title) */
    String EXCLUDED_BY_EXTRACT = "9";
    /** Константа кода (code) элемента : Выбран к зачислению (title) */
    String SELECT_SHOULD_BE_ENROLLED = "10";
    /** Константа кода (code) элемента : В архиве (title) */
    String ENTRANT_ARCHIVED = "11";

    Set<String> CODES = ImmutableSet.of(ACTIVE, OUT_OF_COMPETITION, ENROLLED, EXAMS_PASSED, RECOMMENDED, IN_ORDER, TAKE_DOCUMENTS_AWAY, EXCLUDED_BY_REJECT, EXCLUDED_BY_EXTRACT, SELECT_SHOULD_BE_ENROLLED, ENTRANT_ARCHIVED);
}
