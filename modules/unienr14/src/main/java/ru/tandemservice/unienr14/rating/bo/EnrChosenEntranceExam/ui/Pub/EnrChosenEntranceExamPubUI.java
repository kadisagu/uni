/* $Id:$ */
package ru.tandemservice.unienr14.rating.bo.EnrChosenEntranceExam.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;

/**
 * @author oleyba
 * @since 7/16/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EnrChosenEntranceExamPubUI extends UIPresenter
{
    private EntityHolder<EnrChosenEntranceExam> _holder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _uiActivation.asDesktopRoot(EnrEntrantPub.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getHolder().getValue().getRequestedCompetition().getRequest().getEntrant().getId())
            .parameter("selectedTab", "chosenExamTab")
            .activate();
    }

    // getters and setters

    public EntityHolder<EnrChosenEntranceExam> getHolder()
    {
        return _holder;
    }
}