package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentCampaign

		// создано обязательное свойство open
		{
			tool.createColumn("enr14_campaign_t", new DBColumn("open_p", DBType.BOOLEAN));
            tool.executeUpdate("update enr14_campaign_t set open_p=? where open_p is null", Boolean.TRUE);
            tool.executeUpdate("update enr14_campaign_t set open_p=? where exists(select y.id from educationYear_t y where enr14_campaign_t.educationYear_id=y.id and y.intvalue_p<?)", Boolean.FALSE, 2015);
			tool.setColumnNullable("enr14_campaign_t", "open_p", false);
		}
    }
}