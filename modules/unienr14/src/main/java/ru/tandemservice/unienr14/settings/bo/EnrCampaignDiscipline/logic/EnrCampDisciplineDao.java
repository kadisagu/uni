package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.gen.EnrCampaignDisciplineGen;

/**
 * User: amakarova
 * Date: 16.04.13
 */

public class EnrCampDisciplineDao extends UniBaseDao implements IEnrCampDisciplineDao {

    @Override
    public Long saveOrUpdateCampDiscipline(EnrDiscipline discipline, EnrEnrollmentCampaign campaign, EnrStateExamSubject examSubject)
    {
        EnrCampaignDiscipline campaignDiscipline = getByNaturalId(new EnrCampaignDisciplineGen.NaturalId(discipline, campaign));
        if (campaignDiscipline == null) {
           campaignDiscipline = createСampDiscipline(campaign, discipline);
        }
        if (examSubject != null)
            campaignDiscipline.setStateExamSubject(examSubject);
        else if (campaignDiscipline.getStateExamSubject() != null){
            campaignDiscipline.setStateExamSubject(null);
        }
        saveOrUpdate(campaignDiscipline);
        return campaignDiscipline.getId();
    }

    private EnrCampaignDiscipline createСampDiscipline(EnrEnrollmentCampaign campaign, EnrDiscipline discipline) {
        final EnrCampaignDiscipline campaignDiscipline = new EnrCampaignDiscipline();
        campaignDiscipline.setDiscipline(discipline);
        campaignDiscipline.setEnrollmentCampaign(campaign);
        return campaignDiscipline;
    }
}
