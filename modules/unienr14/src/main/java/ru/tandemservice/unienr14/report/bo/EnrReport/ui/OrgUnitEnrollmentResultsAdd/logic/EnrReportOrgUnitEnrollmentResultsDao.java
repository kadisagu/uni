/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitEnrollmentResultsAdd.logic;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitEnrollmentResultsAdd.EnrReportOrgUnitEnrollmentResultsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitEnrollmentResults;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 21.07.2014
 */
public class EnrReportOrgUnitEnrollmentResultsDao extends UniBaseDao implements IEnrReportOrgUnitEnrollmentResultsDao {
    @Override
    public long createReport(EnrReportOrgUnitEnrollmentResultsAddUI model) {
        
        EnrReportOrgUnitEnrollmentResults report = new EnrReportOrgUnitEnrollmentResults();
        
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportOrgUnitEnrollmentResults.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportOrgUnitEnrollmentResults.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportOrgUnitEnrollmentResults.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportOrgUnitEnrollmentResults.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportOrgUnitEnrollmentResults.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportOrgUnitEnrollmentResults.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportOrgUnitEnrollmentResults.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportOrgUnitEnrollmentResults.P_PROGRAM_SET, "title");

        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportOrgUnitEnrollmentResults.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }
    
    private byte[] buildReport(EnrReportOrgUnitEnrollmentResultsAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ORG_UNIT_ENROLLMENT_RESULTS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        requestedCompDQL
                .column(property("reqComp"));


        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");
        List<EnrRequestedCompetition> fromQueryDocAcceptanceStageList = getList(requestedCompDQL);

        EnrReportStageSelector.examPassResultsStageFilter().applyFilter(requestedCompDQL, "reqComp");
        List<EnrRequestedCompetition> fromQueryResultStageList = getList(requestedCompDQL);

        EnrReportStageSelector.enrollmentResultsStageFilter().applyFilter(requestedCompDQL, "reqComp");
        List<EnrRequestedCompetition> fromQueryEnrollmentStageList = getList(requestedCompDQL);

        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi")), property("reqComp")))
                .joinEntity("vvi", DQLJoinType.inner, EnrChosenEntranceExamForm.class, "vvif", eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("vvif")), property("vvi")))
                .resetColumns()
                .column(property("vvif"));
        List<EnrChosenEntranceExamForm> fromQueryExamFormList = getList(requestedCompDQL);

        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExamForm>> examFormMap = SafeMap.get(HashSet.class);


        Comparator<EnrProgramSetOrgUnit> programSetOrgUnitComparator = new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                OrgUnit orgUnit1 = o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                OrgUnit orgUnit2 = o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();

                int res = Boolean.compare(orgUnit1.getParent() == null, orgUnit2.getParent() == null);
                if (res != 0) return -res;

                res = orgUnit1.getPrintTitle().compareToIgnoreCase(orgUnit2.getPrintTitle());

                if (res != 0) return res;

                OrgUnit formative1 = o1.getEffectiveFormativeOrgUnit();
                OrgUnit formative2 = o2.getEffectiveFormativeOrgUnit();

                res = formative1.getPrintTitle().compareToIgnoreCase(formative2.getPrintTitle());

                return res;
            }
        };

        Set<EnrProgramSetBase> programSets = new HashSet<>();


        Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> docAcceptanceStageMap = new TreeMap<>(programSetOrgUnitComparator);
        Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> resultStageMap = new TreeMap<>(programSetOrgUnitComparator);
        Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> enrollmentStageMap = new TreeMap<>(programSetOrgUnitComparator);

        for (EnrChosenEntranceExamForm examForm : fromQueryExamFormList) {
            examFormMap.get(examForm.getChosenEntranceExam().getRequestedCompetition()).add(examForm);
        }
        String topOrgUnit = TopOrgUnit.getInstance().getPrintTitle();


        for (EnrRequestedCompetition reqComp : fromQueryDocAcceptanceStageList) {
            final EnrProgramSetOrgUnit programSetOrgUnit = reqComp.getCompetition().getProgramSetOrgUnit();
            if (!docAcceptanceStageMap.containsKey(programSetOrgUnit))
                docAcceptanceStageMap.put(programSetOrgUnit, new HashSet<EnrRequestedCompetition>());
            docAcceptanceStageMap.get(programSetOrgUnit).add(reqComp);
            programSets.add(programSetOrgUnit.getProgramSet());
        }

        for (EnrRequestedCompetition reqComp : fromQueryResultStageList) {
            final EnrProgramSetOrgUnit programSetOrgUnit = reqComp.getCompetition().getProgramSetOrgUnit();
            if (!resultStageMap.containsKey(programSetOrgUnit))
                resultStageMap.put(programSetOrgUnit, new HashSet<EnrRequestedCompetition>());
            resultStageMap.get(programSetOrgUnit).add(reqComp);
        }

        for (EnrRequestedCompetition reqComp : fromQueryEnrollmentStageList) {
            final EnrProgramSetOrgUnit programSetOrgUnit = reqComp.getCompetition().getProgramSetOrgUnit();
            if (!enrollmentStageMap.containsKey(programSetOrgUnit))
                enrollmentStageMap.put(programSetOrgUnit, new HashSet<EnrRequestedCompetition>());
            enrollmentStageMap.get(programSetOrgUnit).add(reqComp);
        }

        // Для формирования второй колонки нужно знать количество ОП в наборе. Формируем карту набор ОП - сет ОП, входящих в набор.
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, "programSetItem")
                .where(in(property(EnrProgramSetItem.programSet().fromAlias("programSetItem")), programSets))
                .column(property("programSetItem"));

        List<EnrProgramSetItem> fromQueryProgramSetItems = getList(builder);

        Map<EnrProgramSetBase, Set<EnrProgramSetItem>> programSetItemsMap = SafeMap.get(HashSet.class);

        for (EnrProgramSetItem programSetItem : fromQueryProgramSetItems) {
            programSetItemsMap.get(programSetItem.getProgramSet()).add(programSetItem);
        }

        boolean budget = ((CompensationType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget();


        new RtfInjectModifier().put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(new Date())).modify(document);

        RtfTable tTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfElement formativeTitle = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "formativeOrgUnitFilial");
        RtfElement programFormTitle = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "programForm");
        RtfElement compensationTypeTitle = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "compensationType");

        String programForm = ((ITitled)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue()).getTitle();
        String compensationType = ((ITitled)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).getTitle();


        document.getElementList().remove(tTableElement);
        document.getElementList().remove(formativeTitle);
        document.getElementList().remove(programFormTitle);
        document.getElementList().remove(compensationTypeTitle);


        boolean firstIteration = true;

        for (Map.Entry<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> entry : docAcceptanceStageMap.entrySet())
        {
            if (!firstIteration)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }

            StringBuilder orgUnitTitle = new StringBuilder().append(entry.getKey().getEffectiveFormativeOrgUnit().getPrintTitle());
            if (!entry.getKey().getEffectiveFormativeOrgUnit().getPrintTitle().equals(entry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getPrintTitle()))
                orgUnitTitle.append(" (").append(entry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getPrintTitle()).append(")");

            document.addElement(formativeTitle.getClone());
            new RtfInjectModifier().put("formativeOrgUnitFilial", orgUnitTitle.toString()).modify(document);
            document.addElement(programFormTitle.getClone());
            new RtfInjectModifier().put("programForm", programForm).modify(document);
            document.addElement(compensationTypeTitle.getClone());
            new RtfInjectModifier().put("compensationType", compensationType).modify(document);

            document.addElement(tTableElement.getClone());

            printContentTable(entry.getValue(), resultStageMap.get(entry.getKey()), enrollmentStageMap.get(entry.getKey()), examFormMap, programSetItemsMap, budget, document);



            firstIteration = false;

        }
        new RtfInjectModifier().put("academyTitle", topOrgUnit).modify(document);

        return RtfUtil.toByteArray(document);
    }


    // Формируем таблицу на подразделение
    private void printContentTable(Set<EnrRequestedCompetition> inputDocAcceptanceSet,
                                   Set<EnrRequestedCompetition> inputResultSet,
                                   Set<EnrRequestedCompetition> inputEnrollmentSet,
                                   Map<EnrRequestedCompetition, Set<EnrChosenEntranceExamForm>> examFormMap,
                                   Map<EnrProgramSetBase, Set<EnrProgramSetItem>> programSetItemsMap,
                                   boolean budget,
                                   RtfDocument document)
    {
        // компаратор для наборов ОП
        Comparator<EnrProgramSetOrgUnit> programSetOrgUnitComparator = new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                // сортировка по коду вида оп
                int res = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
                if (res != 0) return res;
                // затем по коду направления
                res = o1.getProgramSet().getProgramSubject().getCode().compareTo(o2.getProgramSet().getProgramSubject().getCode());
                if (res != 0) return res;
                // затем по названию набора
                return o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
            }
        };


        List<String[]> tTable = new ArrayList<>();

        List<EnrEntrantRequest> t2TotalRequestsDocAccStage = new ArrayList<>();
        List<EnrEntrantRequest> t2ManDocAccStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2NoExamDocAccStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2QuotaDocAccStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2TargetAdmDocAccStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2CommonDocAccStage = new ArrayList<>();
        List<EnrEntrantRequest> t2TotalRequestsResultStage = new ArrayList<>();
        List<EnrEntrantRequest> t2ManResultStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2NoExamResultStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2QuotaResultStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2TargetAdmResultStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2CommonResultStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2TotalReqCompsEnrollStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2ManEnrollStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2NoExamEnrollStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2QuotaEnrollStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2TargetAdmEnrollStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2CommonEnrollStage = new ArrayList<>();
        List<EnrRequestedCompetition> t2EgeOnlyEnrollStage = new ArrayList<>();

        int totalPlan = 0;
        NumberFormat formatter = new DecimalFormat("#0.00");


        Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> docAcceptanceStageMap = new TreeMap<>(programSetOrgUnitComparator);
        Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> resultStageMap = new TreeMap<>(programSetOrgUnitComparator);
        Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> enrollmentStageMap = new TreeMap<>(programSetOrgUnitComparator);

        for (EnrRequestedCompetition reqComp : inputDocAcceptanceSet) {
            if (!docAcceptanceStageMap.containsKey(reqComp.getCompetition().getProgramSetOrgUnit()))
                docAcceptanceStageMap.put(reqComp.getCompetition().getProgramSetOrgUnit(), new HashSet<EnrRequestedCompetition>());
            docAcceptanceStageMap.get(reqComp.getCompetition().getProgramSetOrgUnit()).add(reqComp);
        }

        if (inputResultSet != null)
        for (EnrRequestedCompetition reqComp : inputResultSet) {
            if (!resultStageMap.containsKey(reqComp.getCompetition().getProgramSetOrgUnit()))
                resultStageMap.put(reqComp.getCompetition().getProgramSetOrgUnit(), new HashSet<EnrRequestedCompetition>());
            resultStageMap.get(reqComp.getCompetition().getProgramSetOrgUnit()).add(reqComp);
        }

        if (inputEnrollmentSet != null)
        for (EnrRequestedCompetition reqComp : inputEnrollmentSet) {
            if (!enrollmentStageMap.containsKey(reqComp.getCompetition().getProgramSetOrgUnit()))
                enrollmentStageMap.put(reqComp.getCompetition().getProgramSetOrgUnit(), new HashSet<EnrRequestedCompetition>());
            enrollmentStageMap.get(reqComp.getCompetition().getProgramSetOrgUnit()).add(reqComp);
        }

        int num = 0;

        // заполняем данным Т-таблицу
        for (Map.Entry<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> entry : docAcceptanceStageMap.entrySet())
        {
            int plan = budget ? entry.getKey().getMinisterialPlan() : entry.getKey().getContractPlan();

            totalPlan += plan;

            Set<EnrEntrantRequest> totalRequestsDocAccStage = new HashSet<>();
            Set<EnrEntrantRequest> manDocAccStage = new HashSet<>();
            Set<EnrRequestedCompetition> noExamDocAccStage = new HashSet<>();
            Set<EnrRequestedCompetition> quotaDocAccStage = new HashSet<>();
            Set<EnrRequestedCompetition> targetAdmDocAccStage = new HashSet<>();
            Set<EnrRequestedCompetition> commonDocAccStage = new HashSet<>();

            Set<EnrEntrantRequest> totalRequestsResultStage = new HashSet<>();
            Set<EnrEntrantRequest> manResultStage = new HashSet<>();
            Set<EnrRequestedCompetition> noExamResultStage = new HashSet<>();
            Set<EnrRequestedCompetition> quotaResultStage = new HashSet<>();
            Set<EnrRequestedCompetition> targetAdmResultStage = new HashSet<>();
            Set<EnrRequestedCompetition> commonResultStage = new HashSet<>();

            Set<EnrRequestedCompetition> totalReqCompsEnrollStage = new HashSet<>();
            Set<EnrRequestedCompetition> manEnrollStage = new HashSet<>();
            Set<EnrRequestedCompetition> noExamEnrollStage = new HashSet<>();
            Set<EnrRequestedCompetition> quotaEnrollStage = new HashSet<>();
            Set<EnrRequestedCompetition> targetAdmEnrollStage = new HashSet<>();
            Set<EnrRequestedCompetition> commonEnrollStage = new HashSet<>();
            Set<EnrRequestedCompetition> egeOnlyEnrollStage = new HashSet<>();


            for (EnrRequestedCompetition reqComp : entry.getValue()) {
                EnrEntrantRequest request = reqComp.getRequest();

                totalRequestsDocAccStage.add(request);

                if (request.getIdentityCard().getSex().isMale())
                    manDocAccStage.add(request);

                if (reqComp instanceof EnrRequestedCompetitionNoExams)
                    noExamDocAccStage.add(reqComp);

                else if (reqComp instanceof EnrRequestedCompetitionExclusive)
                    quotaDocAccStage.add(reqComp);

                else if (reqComp instanceof EnrRequestedCompetitionTA)
                    targetAdmDocAccStage.add(reqComp);

                else commonDocAccStage.add(reqComp);
            }

            if (resultStageMap.containsKey(entry.getKey()))
                for (EnrRequestedCompetition reqComp : resultStageMap.get(entry.getKey())) {
                    EnrEntrantRequest request = reqComp.getRequest();

                    totalRequestsResultStage.add(request);

                    if (request.getIdentityCard().getSex().isMale())
                        manResultStage.add(request);

                    if (reqComp instanceof EnrRequestedCompetitionNoExams)
                        noExamResultStage.add(reqComp);

                    else if (reqComp instanceof EnrRequestedCompetitionExclusive)
                        quotaResultStage.add(reqComp);

                    else if (reqComp instanceof EnrRequestedCompetitionTA)
                        targetAdmResultStage.add(reqComp);

                    else commonResultStage.add(reqComp);
                }

            if (enrollmentStageMap.containsKey(entry.getKey()))
                for (EnrRequestedCompetition reqComp : enrollmentStageMap.get(entry.getKey())) {
                    totalReqCompsEnrollStage.add(reqComp);

                    if (reqComp.getRequest().getIdentityCard().getSex().isMale())
                        manEnrollStage.add(reqComp);

                    if (reqComp instanceof EnrRequestedCompetitionNoExams)
                        noExamEnrollStage.add(reqComp);

                    else if (reqComp instanceof EnrRequestedCompetitionExclusive)
                        quotaEnrollStage.add(reqComp);

                    else if (reqComp instanceof EnrRequestedCompetitionTA)
                        targetAdmEnrollStage.add(reqComp);

                    else commonEnrollStage.add(reqComp);

                    if (examFormMap.containsKey(reqComp))
                    {
                        boolean notEge = false;
                        for (EnrChosenEntranceExamForm examForm : examFormMap.get(reqComp)) {
                            if (!examForm.getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM))
                                notEge = true;
                        }
                        if (!notEge) {
                            egeOnlyEnrollStage.add(reqComp);
                            t2EgeOnlyEnrollStage.add(reqComp);
                        }
                    }

                }

            List<String> row = new ArrayList<>();

            row.add(String.valueOf(++num));

            if (programSetItemsMap.get(entry.getKey().getProgramSet()).size() != 1)
                row.add(entry.getKey().getProgramSet().getTitle());
            else for (EnrProgramSetItem enrProgramSetItem : programSetItemsMap.get(entry.getKey().getProgramSet()))
            {
                row.add((enrProgramSetItem.getProgram()).getTitleWithCodeAndConditionsShort());
            }


            row.add(String.valueOf(totalRequestsDocAccStage.size()));
            t2TotalRequestsDocAccStage.addAll(totalRequestsDocAccStage);
            row.add(String.valueOf(manDocAccStage.size()));
            t2ManDocAccStage.addAll(manDocAccStage);
            row.add(String.valueOf(noExamDocAccStage.size()));
            t2NoExamDocAccStage.addAll(noExamDocAccStage);
            row.add(String.valueOf(quotaDocAccStage.size()));
            t2QuotaDocAccStage.addAll(quotaDocAccStage);
            row.add(String.valueOf(targetAdmDocAccStage.size()));
            t2TargetAdmDocAccStage.addAll(targetAdmDocAccStage);
            row.add(String.valueOf(commonDocAccStage.size()));
            t2CommonDocAccStage.addAll(commonDocAccStage);
            float competitionValue = 0f;
            if (plan != 0) {
                competitionValue = (totalRequestsDocAccStage.size() * 1.0f) / plan;
                row.add(String.valueOf(formatter.format(competitionValue)));
            } else row.add("-");


            row.add(String.valueOf(totalRequestsResultStage.size()));
            t2TotalRequestsResultStage.addAll(totalRequestsResultStage);
            row.add(String.valueOf(manResultStage.size()));
            t2ManResultStage.addAll(manResultStage);
            row.add(String.valueOf(noExamResultStage.size()));
            t2NoExamResultStage.addAll(noExamResultStage);
            row.add(String.valueOf(quotaResultStage.size()));
            t2QuotaResultStage.addAll(quotaResultStage);
            row.add(String.valueOf(targetAdmResultStage.size()));
            t2TargetAdmResultStage.addAll(targetAdmResultStage);
            row.add(String.valueOf(commonResultStage.size()));
            t2CommonResultStage.addAll(commonResultStage);
            if (plan != 0) {
                competitionValue = (totalRequestsResultStage.size() * 1.0f) / plan;
                row.add(String.valueOf(formatter.format(competitionValue)));
            } else row.add("-");

            row.add(String.valueOf(totalReqCompsEnrollStage.size()));
            t2TotalReqCompsEnrollStage.addAll(totalReqCompsEnrollStage);
            row.add(String.valueOf(manEnrollStage.size()));
            t2ManEnrollStage.addAll(manEnrollStage);
            row.add(String.valueOf(noExamEnrollStage.size()));
            t2NoExamEnrollStage.addAll(noExamEnrollStage);
            row.add(String.valueOf(quotaEnrollStage.size()));
            t2QuotaEnrollStage.addAll(quotaEnrollStage);
            row.add(String.valueOf(targetAdmEnrollStage.size()));
            t2TargetAdmEnrollStage.addAll(targetAdmEnrollStage);
            row.add(String.valueOf(commonEnrollStage.size()));
            t2CommonEnrollStage.addAll(commonEnrollStage);
            row.add(String.valueOf(egeOnlyEnrollStage.size()));

            tTable.add(row.toArray(new String[row.size()]));
        }

        // Формируем т1 и т2 таблицы
        List<String> t1Row = new ArrayList<>();
        List<String> t2Row = new ArrayList<>();

        t1Row.add("Итого (По заявлениям)");
        t2Row.add("Итого (Человек)");


        t1Row.add(String.valueOf(t2TotalRequestsDocAccStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromRequests(t2TotalRequestsDocAccStage)));
        t1Row.add(String.valueOf(t2ManDocAccStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromRequests(t2ManDocAccStage)));
        t1Row.add(String.valueOf(t2NoExamDocAccStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2NoExamDocAccStage)));
        t1Row.add(String.valueOf(t2QuotaDocAccStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2QuotaDocAccStage)));
        t1Row.add(String.valueOf(t2TargetAdmDocAccStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2TargetAdmDocAccStage)));
        t1Row.add(String.valueOf(t2CommonDocAccStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2CommonDocAccStage)));
        t1Row.add(totalPlan == 0 ? "-" : String.valueOf(formatter.format(t2TotalRequestsDocAccStage.size() * 1.0f / totalPlan)));
        t2Row.add(totalPlan == 0 ? "-" : String.valueOf(formatter.format(countUniqueEntrantsFromRequests(t2TotalRequestsDocAccStage) * 1.0f / totalPlan)));

        t1Row.add(String.valueOf(t2TotalRequestsResultStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromRequests(t2TotalRequestsResultStage)));
        t1Row.add(String.valueOf(t2ManResultStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromRequests(t2ManResultStage)));
        t1Row.add(String.valueOf(t2NoExamResultStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2NoExamResultStage)));
        t1Row.add(String.valueOf(t2QuotaResultStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2QuotaResultStage)));
        t1Row.add(String.valueOf(t2TargetAdmResultStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2TargetAdmResultStage)));
        t1Row.add(String.valueOf(t2CommonResultStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2CommonResultStage)));
        t1Row.add(totalPlan == 0 ? "-" : String.valueOf(formatter.format(t2TotalRequestsResultStage.size() * 1.0f / totalPlan)));
        t2Row.add(totalPlan == 0 ? "-" : String.valueOf(formatter.format(countUniqueEntrantsFromRequests(t2TotalRequestsResultStage) * 1.0f / totalPlan)));

        t1Row.add(String.valueOf(t2TotalReqCompsEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2TotalReqCompsEnrollStage)));
        t1Row.add(String.valueOf(t2ManEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2ManEnrollStage)));
        t1Row.add(String.valueOf(t2NoExamEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2NoExamEnrollStage)));
        t1Row.add(String.valueOf(t2QuotaEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2QuotaEnrollStage)));
        t1Row.add(String.valueOf(t2TargetAdmEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2TargetAdmEnrollStage)));
        t1Row.add(String.valueOf(t2CommonEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2CommonEnrollStage)));

        t1Row.add(String.valueOf(t2EgeOnlyEnrollStage.size()));
        t2Row.add(String.valueOf(countUniqueEntrantsFromReqComps(t2EgeOnlyEnrollStage)));

        List<String[]> t1Table = new ArrayList<>();
        t1Table.add(t1Row.toArray(new String[t1Row.size()]));

        t1Table.add(t2Row.toArray(new String[t2Row.size()]));

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tTable.toArray(new String[tTable.size()][]));
        tableModifier.put("T1", t1Table.toArray(new String[t1Table.size()][]));
        tableModifier.modify(document);


    }

    // Методы для подсчета кол-ва уникальных абитуриентов в коллекции выбранных конкурсов или заявлений.
    private int countUniqueEntrantsFromReqComps(Collection<EnrRequestedCompetition> data)
    {
        Set<EnrEntrant> outputSet = new HashSet<>();
        for (EnrRequestedCompetition reqComp : data)
            outputSet.add(reqComp.getRequest().getEntrant());

        return outputSet.size();
    }

    private int countUniqueEntrantsFromRequests(Collection<EnrEntrantRequest> data)
    {
        Set<EnrEntrant> outputSet = new HashSet<>();
        for (EnrEntrantRequest request : data)
            outputSet.add(request.getEntrant());

        return outputSet.size();
    }
}
