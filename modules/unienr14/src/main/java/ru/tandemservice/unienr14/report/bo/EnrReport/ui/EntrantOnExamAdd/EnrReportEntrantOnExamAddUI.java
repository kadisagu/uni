/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author rsizonenko
 * @since 23.05.2014
 */
public class EnrReportEntrantOnExamAddUI extends UIPresenter {

    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date dateFrom;
    private Date dateTo;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());
    }

    // validate
    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public void onClickApply() {
        validate();
        Long reportId = EnrReportManager.instance().entrantOnExamDao().createReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        setDateFrom(getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign().getDateTo());
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);


        util.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE).setModel(new CommonFilterSelectModel() {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o) {
                List<String> values = Arrays.asList("02", "03", "04", "06");
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EnrCompetitionType.class, "rt")
                        .column(property("rt"))
                        .where(in(property("rt", EnrCompetitionType.code()), values));
                if (o != null)
                    builder.where(((o instanceof Set ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("rt", EnrCompetitionType.id(), o)));
                if (!StringUtils.isEmpty(filter))
                {
                    //* введена подстрока *//
                    List<IDQLExpression> concatExpressions = new ArrayList<>();
                    concatExpressions.add(property("rt", EnrCompetitionType.title()));

                    builder.where(likeUpper(DQLFunctions.concat(concatExpressions.toArray(new IDQLExpression[concatExpressions.size()])), value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.order(property("rt", EnrCompetitionType.title()), OrderDirection.asc);
                if (builder.createCountStatement(new DQLExecutionContext(getCompetitionFilterAddon().getSession())).<Long>uniqueResult() > 50)
                    builder.top(50);
                List<EnrCompetitionType> result = builder.createStatement(getCompetitionFilterAddon().getSession()).list();
                return new SimpleListResultBuilder<>(result);
            }
        });
        util.saveSettings();
        util.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).setValue(IUniBaseDao.instance.get().getCatalogItem(EnrRequestType.class, EnrRequestTypeCodes.BS));
        util.saveSettings();
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign() {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign) {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
}
