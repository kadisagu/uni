package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;

/**
 * @author vdanilov
 */
public interface IEnrSelectionDao extends INeedPersistenceSupport {

    /**
     * Для указанного шага зачисления создает контекст рекомендации, заполняет его данными из шага
     * @param step
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EnrEnrollmentStepRecommendationContext prepareRecommendationContext(EnrEnrollmentStep step, boolean debug);

    /**
     * Для указанного шага зачисления создает контекст зачисления, заполняет его данными из шага
     * @param step
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EnrEnrollmentStepEnrollmentContext prepareEnrollmentContext(EnrEnrollmentStep step, boolean debug);

    /**
     * Для указанного заполненного контекста запусает выбор абитуриентов, сохраняет результат в базу
     * @param context
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doExecuteSelection(EnrEnrollmentStepBaseContext context);

}
