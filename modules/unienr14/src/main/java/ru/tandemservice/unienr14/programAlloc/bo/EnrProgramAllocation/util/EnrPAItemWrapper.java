/* $Id:$ */
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.List;

/**
 * @author oleyba
 * @since 8/11/14
 */
public class EnrPAItemWrapper extends IdentifiableWrapper<EnrRatingItem>
{
    private EnrRatingItem ratingItem;
    private boolean selected;
    private EnrProgramSetItem program;
    private List<EnrProgramSetItem> programList;
    private ISelectModel programModel;
    private String priorities;

    public EnrPAItemWrapper(EnrRatingItem i) {
        super(i.getRequestedCompetition().getId(), i.getEntrant().getFullFio());
        this.ratingItem = i;
    }

    public EnrRequestedCompetition getEntrant() {
        return ratingItem.getRequestedCompetition();
    }

    public String getFinalMark() {
        return ratingItem.getTotalMarkAsString();
    }

    public String getEduDocMark() {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getEntrant().getRequest().getEduDocument().getAvgMarkAsLong());
    }

    public void setProgramSetItems(List<EnrProgramSetItem> programSetItems) {
        this.programList = programSetItems;
        this.programModel = new LazySimpleSelectModel<>(programSetItems);
        this.priorities = programSetItems == null ? null : UniStringUtils.join(programSetItems, EnrProgramSetItem.program().title().s(), ", ");
    }

    public boolean isSelected() { return selected; }
    public void setSelected(boolean selected) { this.selected = selected; }
    public EnrProgramSetItem getProgram() { return program; }
    public void setProgram(EnrProgramSetItem program) { this.program = program; }
    public List<EnrProgramSetItem> getProgramList() { return programList; }
    public ISelectModel getProgramModel() { return programModel; }
    public String getPriorities() { return priorities; }
}
