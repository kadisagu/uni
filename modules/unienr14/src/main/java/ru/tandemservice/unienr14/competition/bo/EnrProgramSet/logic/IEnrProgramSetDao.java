/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrProgramSetOrgUnitWrapper;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrTargetAdmissionPlanWrapper;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/3/14
 */
public interface IEnrProgramSetDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет (обновляет) набор ОП (специальность), сохраняет выбранные программы (набора ОП), план приема.
     *
     * @param programSet  набор ОП (специальность)
     * @param programList образовательные программы
     * @param rowList     план приема
     */
    void saveOrUpdateProgramSet(EnrProgramSetBase programSet, List<EduProgramHigherProf> programList, List<EnrProgramSetOrgUnitWrapper> rowList);

    /**
     * Удаляет выбранный набор ОП (специальность).
     *
     * @param programSetId набор ОП (специальность)
     */
    void deleteProgramSet(Long programSetId);

    /**
     * Создает конкурсы выбранного набора ОП (специальности).
     *
     * @param programSet набор ОП (специальность)
     */
    void doOpenCompetition(EnrProgramSetBase programSet);

    /**
     * Удаляет конкурсы выбранного набора ОП (специальности).
     *
     * @param programSet набор ОП (специальность)
     */
    void doCloseCompetition(EnrProgramSetBase programSet);

    /**
     * Создает недостающие конкурсы выбранного набора ОП (специальности).
     * Все лишние конкурсы, на которые нет заявлений - удаляются.
     *
     * @param programSet набор ОП (специальность)
     */
    void doRefreshCompetition(EnrProgramSetBase programSet);

    /**
     * Проверяет, что текущий набор конкурсов соответствует планам приема и настройкам ВИ
     *
     * @param programSet набор ОП
     * @return true, если набор конкурсов соответствует
     */
    boolean isCompetitionValid(EnrProgramSetBase programSet);

    /**
     * true, если у набора ОП есть хотя бы один конкурс
     * и множество уникальных установленных для набора ОП настроек наборов ВИ
     * отличаются от такового у конкурсов данного набора ОП
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    boolean isExamSetSettingsDiffer(EnrProgramSetBase programSet);

    /**
     * Строки плана приема по виду ЦП данного набора ОП на подразделении.
     *
     * @param programSetOrgUnitId набор ОП на подразделении
     * @return строки плана приема по виду ЦП
     */
    List<EnrTargetAdmissionPlanWrapper> getProgramSetOrgUnitTAPlans(Long programSetOrgUnitId, boolean allKinds);

    /**
     * Обновляет планы по виду ЦП данного набора ОП на подразделении.
     *
     * @param programSetOrgUnitId набор ОП на подразделении
     * @param rows                строки плана приема по виду ЦП
     */
    void updateProgramSetOrgUnitTargetAdmissionPlans(Long programSetOrgUnitId, Collection<EnrTargetAdmissionPlanWrapper> rows);

    /**
     * Проверяет, правильно ли распределен план приема по видам ЦП.
     *
     * @param programSetId набор ОП
     * @return коллекцию id тех подразделений данного набора ОП, чей план приема по ЦП не совпадает с суммой планов примема по видам ЦП
     */
    Collection<Long> getTAPlanDifferentProgramSetOrgUnits(Long programSetId);

    /**
     * Обновляет планы приема по образовательным программам на данном подразделении.
     */
    void updateProgramSetOrgUnitPlans(EnrProgramSetOrgUnit orgUnit, Map<Long, Integer> planMap);

    boolean isCompetitionTaPlanIncorrect(EnrProgramSetBase programSet);

    /**
     * Обновляет список организаций-заказчиков для целевой контрактной подготовки
     */
    void saveOrUpdateCustomerTargetContractTraining(EnrProgramSetBase programSet, boolean isTargetContract, List<ExternalOrgUnit> orgUnits);

    List<ExternalOrgUnit> getExternalOrgUnitsForProgramSet(EnrProgramSetBase programSetBase);


}