/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.NextOfKinStep;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEditUI;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubNextOfKinTab.EnrEntrantNextOfKinDataSourceUtil;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;

import java.util.List;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardNextOfKinStepUI extends PersonNextOfKinAddEditUI implements IWizardStep, EnrEntrantNextOfKinDataSourceUtil.Owner
{
    private EnrEntrantWizardState _state;

    private boolean displayOnlineEntrantInfo;

    private DynamicListDataSource<PersonNextOfKin> dataSource;

    private boolean editMode;
    private boolean initialized;

    @Override
    public void onComponentRefresh()
    {
        setPersonRoleName(EnrPersonLegacyUtils.getSecureRoleContext(getState().getEntrant()).getPersonRoleName());
        super.onComponentRefresh();
        if (isInitialized()) {
            setDisplayOnlineEntrantInfo(false);
        }
        if (getState().getOnlineEntrantId() != null && !isInitialized()) {
            List<EnrOnlineEntrantNextOfKin> nextOfKinList = IUniBaseDao.instance.get().getList(EnrOnlineEntrantNextOfKin.class, EnrOnlineEntrantNextOfKin.onlineEntrant().id(), getState().getOnlineEntrantId(), "id");
            EnrOnlineEntrantNextOfKin onlineNextOfKin = nextOfKinList.isEmpty() ? null : nextOfKinList.get(0);
            if (onlineNextOfKin != null && onlineNextOfKin.getFirstname() != null && onlineNextOfKin.getLastname() != null) {
                setDisplayOnlineEntrantInfo(true);
                onlineNextOfKin.fillTo(getNextOfKin());
                setInitialized(true);
            }
        }
        if (getDataSource() == null) {
            setDataSource(EnrEntrantNextOfKinDataSourceUtil.createDataSource(this, false));

            getDataSource().addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditNextOfKin").setDisableHandler(getState().getDisableHandler()));
            getDataSource().addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteNextOfKin", "Удалить ближайшего родственника: {0} — «{1} {2} {3}»?", PersonNextOfKin.L_RELATION_DEGREE + "." + ICatalogItem.CATALOG_ITEM_TITLE, PersonNextOfKin.P_LAST_NAME, PersonNextOfKin.P_FIRST_NAME, PersonNextOfKin.P_MIDDLE_NAME).setDisableHandler(getState().getDisableHandler()));
        }
    }

    // EnrEntrantNextOfKinDataSourceUtil.Owner

    @Override public Person getPerson() { return getState().getEntrant().getPerson(); }
    @Override public ISecureRoleContext getSecureRoleContext() { return null; }

    // actions

    public void onClickDeleteNextOfKin() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickEditNextOfKin() {
        setEditMode(true);
        setNextOfKin(IUniBaseDao.instance.get().getNotNull(PersonNextOfKin.class, getListenerParameterAsLong()));
    }

    public void onClickCancelEditNextOfKin() {
        setEditMode(false);
        setNextOfKin(new PersonNextOfKin());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickSaveNextOfKin() {
        if (saveNextOfKinData()) {
            getState().getCreatedObjectIds().add(getNextOfKin().getId());
            onClickCancelEditNextOfKin();
        }
    }

    @Override
    public void saveStepData()
    {
    }

    // presenter

    public boolean isShowEditModeButtons() {
        return isEditMode();
    }

    public boolean isShowSaveModeButtons() {
        return !isEditMode();
    }

    // getters and setters


    public boolean isDisplayOnlineEntrantInfo()
    {
        return displayOnlineEntrantInfo;
    }

    public void setDisplayOnlineEntrantInfo(boolean displayOnlineEntrantInfo)
    {
        this.displayOnlineEntrantInfo = displayOnlineEntrantInfo;
    }

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public void setDataSource(DynamicListDataSource<PersonNextOfKin> dataSource)
    {
        this.dataSource = dataSource;
    }

    @Override
    public DynamicListDataSource<PersonNextOfKin> getDataSource()
    {
        return dataSource;
    }

    public boolean isEditMode()
    {
        return editMode;
    }

    public void setEditMode(boolean editMode)
    {
        this.editMode = editMode;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    public void setInitialized(boolean initialized)
    {
        this.initialized = initialized;
    }
}
