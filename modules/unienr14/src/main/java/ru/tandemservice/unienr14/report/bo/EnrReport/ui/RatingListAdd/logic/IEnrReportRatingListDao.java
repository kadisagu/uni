/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.CompetitionListAdd.EnrReportCompetitionListAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.EnrReportRatingListAddUI;

/**
 * @author oleyba
 * @since 5/20/14
 */
public interface IEnrReportRatingListDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createReport(EnrReportRatingListAddUI enrReportEntrantListAddUI);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createCompetitionListReport(EnrReportCompetitionListAddUI enrReportEntrantListAddUI);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] buildReport(EnrReportRatingListAddUI model, String templateCatalogCode);
}