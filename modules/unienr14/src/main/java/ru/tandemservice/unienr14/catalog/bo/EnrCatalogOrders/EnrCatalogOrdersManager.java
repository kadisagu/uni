/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;

/**
 * @author oleyba
 * @since 7/7/14
 */
@Configuration
public class EnrCatalogOrdersManager extends BusinessObjectManager
{
    public static EnrCatalogOrdersManager instance()
    {
        return instance(EnrCatalogOrdersManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler orderTypeDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EnrOrderType.class)
            .order(EnrOrderType.code())
            .filter(EnrOrderType.title())
            ;
    }
}
