/**
 *$Id: EnrExamGroupSetAddEditUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;

/**
 * @author Alexander Shaburov
 * @since 17.05.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class EnrExamGroupSetAddEditUI extends UIPresenter
{
    private EntityHolder<EnrExamGroupSet> _entityHolder = new EntityHolder<>(new EnrExamGroupSet());

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();

        if (_entityHolder.getValue().getEnrollmentCampaign() == null)
            _entityHolder.getValue().setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickApply()
    {
        if (_entityHolder.getValue().getBeginDate().getTime() > _entityHolder.getValue().getEndDate().getTime())
        {
            ContextLocal.getErrorCollector().add(EnrExamGroupManager.instance().getProperty("examGroupSet.add.dateException"), "beginDate", "endDate");
            return;
        }

        EnrExamGroupManager.instance().setDao().saveOrUpdate(_entityHolder.getValue());
        deactivate();
    }

    // Getters & Setters

    public String getSticker()
    {
        if (_entityHolder.getId() == null)
            return getConfig().getProperty("ui.sticker.add");
        else
            return getConfig().getProperty("ui.sticker.edit");
    }

    /* Generated */

    public EntityHolder<EnrExamGroupSet> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrExamGroupSet> entityHolder)
    {
        _entityHolder = entityHolder;
    }
}
