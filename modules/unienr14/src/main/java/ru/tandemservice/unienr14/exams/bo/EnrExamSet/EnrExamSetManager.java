/**
 *$Id: EnrExamSetManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamTypeCodes;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.EnrExamSetDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Alexander Shaburov
 * @since 16.04.13
 */
@Configuration
public class EnrExamSetManager extends BusinessObjectManager
{
    public static final String CHOOSE_AND_SETUP_EXAM_SET_ADDON_NAME = "EnrExamSetChooseAndSetupAddon";

    public static EnrExamSetManager instance()
    {
        return instance(EnrExamSetManager.class);
    }

    @Bean
    public IEnrExamSetDao dao()
    {
        return new EnrExamSetDao();
    }

    /**
     * Отображает элементы разделенные delimiter.
     * @param elementList список элементов напора ВИ изходя из которого будет строиться строка
     * @param delimiter разделитель элементов
     * @return [название дисциплины] [[сокр. название типа ВИ], П]
     */
    public String examSetElementTitle(Collection<EnrExamSetElement> elementList, String delimiter)
    {
        if (elementList == null || elementList.isEmpty())
            return getProperty("examSet.emptyTitle");

        final StringBuilder resultBuilder = new StringBuilder();
        final Iterator<EnrExamSetElement> iterator = elementList.iterator();
        while (iterator.hasNext())
        {
            final EnrExamSetElement element = iterator.next();

            resultBuilder.append(element.getValue().getTitle());

            if (!element.getType().getCode().equals(EnrExamTypeCodes.USUAL)) {
                resultBuilder.append(" [").append(element.getType().getShortTitle()).append("]");
            }

            if (iterator.hasNext())
                resultBuilder.append(delimiter);
        }

        return resultBuilder.toString();
    }
}
