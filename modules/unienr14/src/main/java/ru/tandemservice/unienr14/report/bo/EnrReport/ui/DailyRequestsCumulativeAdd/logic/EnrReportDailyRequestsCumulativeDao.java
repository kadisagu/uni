/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.EnrReportDailyRequestsCumulativeAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative;
import ru.tandemservice.unienr14.report.entity.EnrReportRequestCount;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author rsizonenko
 * @since 04.06.2014
 */
public class EnrReportDailyRequestsCumulativeDao extends UniBaseDao implements IEnrReportDailyRequestsCumulativeDao
{

    private static final String[][] EMPTY_BUDGET_TABLE = {{new SimpleDateFormat("dd.MM").format(new Date()), "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"}};
    private static final String[][] EMPTY_COMMERCIAL_TABLE = {{new SimpleDateFormat("dd.MM").format(new Date()), "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"}};



    @Override
    public Long createReport(EnrReportDailyRequestsCumulativeAddUI model) {

        EnrReportDailyRequestsCumulative report = new EnrReportDailyRequestsCumulative();


        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setSplitted(model.isSplitByOrgUnits() ? "Да" : "Нет");

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRequestCount.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRequestCount.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRequestCount.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRequestCount.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRequestCount.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRequestCount.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRequestCount.P_PROGRAM_SET, "title");

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportDailyRequestsCumulative.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }


    private byte[] buildReport(EnrReportDailyRequestsCumulativeAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_DAILY_REQUESTS_CUMULATIVE);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL.column("reqComp");
        List<EnrRequestedCompetition> reqCompList = getList(requestedCompDQL);

        // Компаратор для подразделений
        Comparator<EnrOrgUnit> enrOrgUnitComparator = new Comparator<EnrOrgUnit>() {
            @Override
            public int compare(EnrOrgUnit o1, EnrOrgUnit o2) {
                int result = 0;
                if (o1.getInstitutionOrgUnit().getOrgUnit().getParent() == null) result-=10000;
                if (o2.getInstitutionOrgUnit().getOrgUnit().getParent() == null) result+=10000;

                return result += o1.getInstitutionOrgUnit().getOrgUnit().getPrintTitle().compareToIgnoreCase(o2.getInstitutionOrgUnit().getOrgUnit().getPrintTitle());
            }
        };

        // Карты под бюджетные выбранные конкурсы и под договоры
        Map<EnrOrgUnit, Set<EnrRequestedCompetition>> budgetMap = new HashMap<>();
        Map<EnrOrgUnit, Set<EnrRequestedCompetition>> commercialMap = new HashMap<>();
        Set<EnrOrgUnit> orgUnits = new TreeSet<>(enrOrgUnitComparator);

        // Вытаскиваем из документа метки
        RtfTable tTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfTable sTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "S");
        RtfTable hTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "H");
        RtfElement filialElement = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "filial");
        RtfElement titleElement = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "title");

        // Чистим документ
        document.getElementList().clear();

        // Заполняем карту Подразделение - список выбранных конкурсов
        for (EnrRequestedCompetition reqComp : reqCompList)
        {
            EnrOrgUnit orgUnit = reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit();
            if (reqComp.getCompetition().getType().getCompensationType().isBudget())
            {
                if (budgetMap.containsKey(orgUnit))
                    budgetMap.get(orgUnit).add(reqComp);
                else {
                    Set<EnrRequestedCompetition> reqCompSet = new HashSet<>();
                    Set<EnrRequestedCompetition> reqCompSet2 = new HashSet<>();
                    reqCompSet.add(reqComp);
                    budgetMap.put(orgUnit, reqCompSet);
                    commercialMap.put(orgUnit, reqCompSet2);
                    orgUnits.add(orgUnit);
                }
            }
            else
            {
                if (commercialMap.containsKey(orgUnit))
                    commercialMap.get(orgUnit).add(reqComp);
                else
                {
                    Set<EnrRequestedCompetition> reqCompSet = new HashSet<>();
                    Set<EnrRequestedCompetition> reqCompSet2 = new HashSet<>();
                    reqCompSet.add(reqComp);
                    commercialMap.put(orgUnit, reqCompSet);
                    budgetMap.put(orgUnit, reqCompSet2);
                    orgUnits.add(orgUnit);
                }
            }
        }


        List<Map<EnrOrgUnit, Set<EnrRequestedCompetition>>> dataList= new ArrayList<>();
        dataList.add(commercialMap);
        dataList.add(budgetMap);

        boolean isEmpty = true;
        boolean notFirstIteration = false;
        for (EnrOrgUnit orgUnit : orgUnits)
        {
            List<String[]> tTable = new ArrayList<>();
            List<String[]> sTable = new ArrayList<>();
            if (notFirstIteration)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            boolean hasCity = false;
            boolean hasArea = false;
            String city = "Не указано";
            String area = "Не указано";

            // Узнаем, есть ли указанный город и регион для подразделения
            if (orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress()!= null && orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement() != null) {
                AddressItem item = orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement();
                if (item.getTitle() != null){
                    city = item.getTitle();
                    hasCity = true;
                }
                if (item.getParent()!= null && item.getParent().getTitle()!=null) {
                    area = item.getParent().getTitle();
                    hasArea = true;
                }
            }


            for (int i = 0; i < dataList.size(); i++)
            {
                Map<EnrEntrantRequest, EnrRequestedCompetition> highPriorityMap = new HashMap<>();


                // Заполняем карту, которая связывает одно заявление с одним выбранным конкурсом, который имеет наивысший приоритет.
                for (EnrRequestedCompetition reqComp : dataList.get(i).get(orgUnit))
                {
                    if (highPriorityMap.containsKey(reqComp.getRequest()))
                    {
                        if (reqComp.getPriority() < highPriorityMap.get(reqComp.getRequest()).getPriority())
                            highPriorityMap.put(reqComp.getRequest(), reqComp);
                    }
                    else highPriorityMap.put(reqComp.getRequest(), reqComp);
                }

                // Формируем Т- и С- таблицы
                Map<EnrOrgUnit, Set<EnrRequestedCompetition>> dataMap = dataList.get(i);
                if (highPriorityMap.size() > 0)
                    if (i==1)
                        tTable = makeBudgetTable(orgUnit, model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo(), highPriorityMap, hasCity, hasArea);
                    else
                        sTable = makeCommercialTable(orgUnit, model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo(), highPriorityMap, hasCity, hasArea);
            }


            // Заполняем документ

            document.getElementList().add(titleElement.getClone());
            new RtfInjectModifier().put("title", "Ежедневная сводка по заявлениям на места, финансируемые из федерального бюджета").modify(document);

            if(model.isSplitByOrgUnits())
            {
                document.getElementList().add(filialElement.getClone());
                new RtfInjectModifier().put("filial", orgUnit.getInstitutionOrgUnit().getOrgUnit().getPrintTitle()).modify(document);
            }

            document.getElementList().add(hTableElement.getClone());
            new RtfInjectModifier().put("hTitle", "Параметры Отчета").modify(document);

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            RtfTableModifier modifier = new RtfTableModifier();
            document.getElementList().add(tTableElement.getClone());
            if (tTable.size() > 0)
                modifier.put("T", tTable.toArray(new String[tTable.size()][]));
            else
                modifier.put("T", EMPTY_BUDGET_TABLE);

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            document.getElementList().add(titleElement.getClone());
            new RtfInjectModifier().put("title", "Ежедневная сводка по заявлениям на места, предусматривающие оплату стоимости обучения").modify(document);

            if(model.isSplitByOrgUnits())
            {
                document.getElementList().add(filialElement.getClone());
                new RtfInjectModifier().put("filial", orgUnit.getInstitutionOrgUnit().getOrgUnit().getPrintTitle()).modify(document);
              }

            document.getElementList().add(hTableElement.getClone());
            new RtfInjectModifier().put("hTitle", "Параметры Отчета").modify(document);

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(sTableElement.getClone());
            if (sTable.size() > 0)
                modifier.put("S", sTable.toArray(new String[sTable.size()][]));
            else
                modifier.put("S", EMPTY_COMMERCIAL_TABLE);

            modifier.modify(document);

            new RtfInjectModifier().put("cityTitle", city).modify(document);
            new RtfInjectModifier().put("areaTitle", area).modify(document);
            isEmpty = false;
            notFirstIteration = true;

        }


        // Если не было вообще ни одного вошедшего в результат запроса выбранного конкурса, то заполняем отдельным способом
        if (isEmpty)
        {
            document.getElementList().add(titleElement.getClone());
            new RtfInjectModifier().put("title", "Ежедневная сводка по заявлениям на места, финансируемые из федерального бюджета").modify(document);

            document.getElementList().add(hTableElement.getClone());
            new RtfInjectModifier().put("hTitle", "Параметры Отчета").modify(document);
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            RtfTableModifier modifier = new RtfTableModifier();
            document.getElementList().add(tTableElement.getClone());
            modifier.put("T", EMPTY_BUDGET_TABLE);
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(titleElement.getClone());
            new RtfInjectModifier().put("title", "Ежедневная сводка по заявлениям на места, предусматривающие оплату стоимости обучения").modify(document);

            document.getElementList().add(hTableElement.getClone());
            new RtfInjectModifier().put("hTitle", "Параметры Отчета").modify(document);
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(sTableElement.getClone());
            modifier.put("S", EMPTY_COMMERCIAL_TABLE);
            modifier.modify(document);

            EnrOrgUnit orgUnit = (EnrOrgUnit) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT).getValue();
            String city = "Не указано";
            String area = "Не указано";
            if (orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress()!= null && orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement() != null) {

                AddressItem item = orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement();
                if (item.getTitle() != null)
                    city = item.getTitle();
                if (item.getParent()!= null && item.getParent().getTitle()!=null)
                    area = item.getParent().getTitle();
            }
            new RtfInjectModifier().put("cityTitle", city).modify(document);
            new RtfInjectModifier().put("areaTitle", area).modify(document);
        }

        // H - таблица

        List<String[]> hTable = new FilterParametersPrinter().getTable(filterAddon, model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
        modifier.put("H", new RtfRowIntercepterRawText());
        modifier.modify(document);

        return RtfUtil.toByteArray(document);

    }

    // Метод для заполнения таблицы бюджетников
    List<String[]> makeBudgetTable(EnrOrgUnit orgUnit, Date dateFrom, Date dateTo, Map<EnrEntrantRequest, EnrRequestedCompetition> inputMap, boolean hasCity, boolean hasArea)
    {
        List<String[]> tTable = new ArrayList<>();

        Date today = new Date();
        if (dateTo.getTime() > today.getTime())
            dateTo = today;
        Date currentDate = (Date)dateFrom.clone();
        int dayOfYearFrom = CoreDateUtils.getDateDifferenceInDays(dateFrom, CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(dateFrom)));
        int dayOfYearTo = CoreDateUtils.getDateDifferenceInDays(dateTo, CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(dateFrom)));

        for (int i = dayOfYearFrom; i <= dayOfYearTo; i++)
        {
            int total = 0;
            int target = 0;
            int medalist = 0;
            int golden = 0;
            int silver = 0;
            int perfect = 0;
            int bezVI = 0;
            int specialRights = 0;
            int commonVI = 0;
            int man = 0;
            int city = 0;
            int area = 0;
            for (Map.Entry<EnrEntrantRequest, EnrRequestedCompetition> entry : inputMap.entrySet())
            {
                EnrEntrantRequest request = entry.getKey();
                EnrRequestedCompetition reqComp = entry.getValue();

                if (CoreDateUtils.getDateDifferenceInDays(currentDate, request.getRegDate()) < 0)
                    continue;
                if (request.isTakeAwayDocument())
                {

                    if (CoreDateUtils.getDateDifferenceInDays(currentDate, request.getTakeAwayDocumentDate()) >= 0)
                        continue;
                }
                total++;
                if (reqComp.getCompetition().isTargetAdmission())
                    target++;

                if (request.getEduDocument().getGraduationHonour() != null)
                {
                    if (request.getEduDocument().getGraduationHonour().getCode().equals(GraduationHonourCodes.GOLD_MEDAL)) {
                        golden++;
                        medalist++;
                    }
                    if (request.getEduDocument().getGraduationHonour().getCode().equals(GraduationHonourCodes.SILVER_MEDAL)) {
                        silver++;
                        medalist++;
                        perfect++;
                    }
                    if (request.getEduDocument().getGraduationHonour().getCode().equals(GraduationHonourCodes.RED_DIPLOMA))
                        perfect++;
                }
                if (reqComp.getCompetition().isNoExams())
                    bezVI++;
                if (reqComp.getCompetition().isExclusive())
                    specialRights++;
                if (!reqComp.getCompetition().isNoExams() && !reqComp.getCompetition().isExclusive() && !reqComp.getCompetition().isTargetAdmission())
                    commonVI++;
                if (request.getIdentityCard().getSex().isMale())
                    man++;
                if (request.getIdentityCard().getAddress()!=null && hasCity) {
                    if (request.getIdentityCard().getAddress() instanceof AddressDetailed)
                    {
                        AddressDetailed entrantAddress = (AddressDetailed) request.getIdentityCard().getAddress();
                        if (entrantAddress.getSettlement()!=null) {
                            if (entrantAddress.getSettlement().equals(orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement()))
                                city++;
                            AddressItem region = entrantAddress.getSettlement();
                            while (true)
                            {
                                if (region.getParent() != null)
                                    region = region.getParent();
                                else break;
                            }
                            if (hasArea && entrantAddress.getSettlement().getParent() != null && region.equals(orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement().getParent()))
                                area++;
                        }
                    }
                }

            }
            String[] row = new String[]{
                    new SimpleDateFormat("dd.MM").format(currentDate).toString(),
                    String.valueOf(total),
                    String.valueOf(target),
                    String.valueOf(medalist),
                    String.valueOf(golden),
                    String.valueOf(silver),
                    String.valueOf(perfect),
                    String.valueOf(bezVI),
                    String.valueOf(specialRights),
                    String.valueOf(commonVI),
                    String.valueOf(man),
                    String.valueOf(total - man),
                    hasCity ? String.valueOf(city) : "пусто",
                    hasArea ? String.valueOf(area - city) : "пусто",
                    String.valueOf(total - area),
            };
            tTable.add(row);
            currentDate.setTime(currentDate.getTime() + 86400000);
        }
        return tTable;
    }


    // Метод для заполнения таблицы платников
    List<String[]> makeCommercialTable(EnrOrgUnit orgUnit, Date dateFrom, Date dateTo, Map<EnrEntrantRequest, EnrRequestedCompetition> inputMap, boolean hasCity, boolean hasArea)
    {
        List<String[]> sTable = new ArrayList<>();

        Date today = new Date();
        if (dateTo.getTime() > today.getTime())
            dateTo = today;
        Date currentDate = (Date)dateFrom.clone();
        int dayOfYearFrom = CoreDateUtils.getDateDifferenceInDays(dateFrom, CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(dateFrom)));
        int dayOfYearTo = CoreDateUtils.getDateDifferenceInDays(dateTo, CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(dateFrom)));



        for (int i = dayOfYearFrom; i <= dayOfYearTo; i++)
        {
            int total = 0;
            int medalist = 0;
            int golden = 0;
            int silver = 0;
            int perfect = 0;
            int bezVI = 0;
            int commonVI = 0;
            int man = 0;
            int city = 0;
            int area = 0;
            for (Map.Entry<EnrEntrantRequest, EnrRequestedCompetition> entry : inputMap.entrySet())
            {
                EnrEntrantRequest request = entry.getKey();
                EnrRequestedCompetition reqComp = entry.getValue();
                if (CoreDateUtils.getDateDifferenceInDays(currentDate, request.getRegDate()) < 0)
                    continue;
                if (request.isTakeAwayDocument())
                {

                    if (CoreDateUtils.getDateDifferenceInDays(currentDate, request.getTakeAwayDocumentDate()) >= 0)
                        continue;
                }
                total++;

                if (request.getEduDocument().getGraduationHonour() != null)
                {
                    if (request.getEduDocument().getGraduationHonour().getCode().equals(GraduationHonourCodes.GOLD_MEDAL)) {
                        golden++;
                        medalist++;
                    }
                    if (request.getEduDocument().getGraduationHonour().getCode().equals(GraduationHonourCodes.SILVER_MEDAL)) {
                        silver++;
                        medalist++;
                        perfect++;
                    }
                    if (request.getEduDocument().getGraduationHonour().getCode().equals(GraduationHonourCodes.RED_DIPLOMA))
                        perfect++;
                }
                if (reqComp.getCompetition().isNoExams())
                    bezVI++;

                if (!reqComp.getCompetition().isNoExams() && !reqComp.getCompetition().isExclusive() && !reqComp.getCompetition().isTargetAdmission())
                    commonVI++;
                if (request.getIdentityCard().getSex().isMale())
                    man++;
                if (request.getIdentityCard().getAddress()!=null && hasCity) {
                    if (request.getIdentityCard().getAddress() instanceof AddressDetailed)
                    {
                        AddressDetailed entrantAddress = (AddressDetailed) request.getIdentityCard().getAddress();
                        if (entrantAddress.getSettlement()!=null) {
                            if (entrantAddress.getSettlement().equals(orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement()))
                                city++;
                            AddressItem region = entrantAddress.getSettlement();
                            while (true)
                            {
                                if (region.getParent() != null)
                                    region = region.getParent();
                                else break;
                            }
                            if (hasArea && entrantAddress.getSettlement().getParent() != null && region.equals(orgUnit.getInstitutionOrgUnit().getOrgUnit().getAddress().getSettlement().getParent()))
                                area++;
                        }
                    }
                }

            }
            String[] row = new String[]{
                    new SimpleDateFormat("dd.MM").format(currentDate).toString(),
                    String.valueOf(total),
                    String.valueOf(medalist),
                    String.valueOf(golden),
                    String.valueOf(silver),
                    String.valueOf(perfect),
                    String.valueOf(bezVI),
                    String.valueOf(commonVI),
                    String.valueOf(man),
                    String.valueOf(total - man),
                    hasCity ? String.valueOf(city) : "пусто",
                    hasArea ? String.valueOf(area - city) : "пусто",
                    String.valueOf(total - area),
            };
            sTable.add(row);
            currentDate.setTime(currentDate.getTime() + 86400000);
        }
        return sTable;
    }

}
