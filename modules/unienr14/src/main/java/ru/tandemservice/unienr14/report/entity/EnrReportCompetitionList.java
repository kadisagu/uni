package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.CompetitionListAdd.EnrReportCompetitionListAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.*;

import java.util.Arrays;
import java.util.List;

/**
 * Списки поступающих (конкурсные списки)
 */
public class EnrReportCompetitionList extends EnrReportCompetitionListGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportCompetitionList";

    private static List<String> properties = Arrays.asList(P_REQUEST_TYPE,
        P_COMPENSATION_TYPE,
        P_PROGRAM_FORM,
        P_COMPETITION_TYPE,
        P_ENR_ORG_UNIT,
        P_FORMATIVE_ORG_UNIT,
        P_PROGRAM_SUBJECT,
        P_EDU_PROGRAM,
        P_PROGRAM_SET,
        P_PARALLEL,
        P_ORIG_EDU_DOCUMENT,
        P_CONSENT_ENROLLMENT,
        P_FINAL_AGREEMENT_ENROLLMENT
    );

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportCompetitionList.class; }
            @Override public List<String> getPropertyList() { return properties; }
            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportCompetitionListAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Списки поступающих (конкурсные списки)»"; }
            @Override public String getListTitle() { return "Список отчетов «Списки поступающих (конкурсные списки)»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}