/* $Id: IEcProfileDistributionDao.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.*;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public interface IEnrProgramAllocationDao extends INeedPersistenceSupport
{
    /**
     * Получает планы приема для указанного распределения
     *
     * @param programSetOrgUnitId конфигурация распределения
     * @return планы приема
     */
    IEnrPAQuotaWrapper getCurrentQuota(Long programSetOrgUnitId);

    /**
     * Получает кол-во занятых мест для указанного распределения
     *
     * @param allocationId распределение
     * @return кол-во занятых мест
     */
    IEnrPAQuotaUsedWrapper getUsedQuota(Long allocationId);

    /**
     * Получает кол-во свободных мест для указанного распределения
     *
     * @param allocationId распределение
     * @return кол-во свободных мест
     */
    IEnrPAQuotaFreeWrapper getFreeQuota(Long allocationId);

    /**
     * Получает описание свободных мест / планов в html формате
     *
     * @param wrapper информация о местах (можно получить с помощью getFreeQuotaDTO)
     * @return планы по профилям:
     *         id профиля -> план в формате html
     *         0 -> план по направлению приема
     */
    Map<Long, String> getQuotaHTMLDescription(IEnrPAQuotaFreeWrapper wrapper);

    /**
     * Массово создает распределения
     *
     * @param programSetOrgUnitIds id наборов ОП
     */
    void doBulkMakeAllocation(Set<Long> programSetOrgUnitIds);

    /**
     * Массово заполняет распределения
     *
     * @param programSetOrgUnitIds id наборов ОП
     */
    void doBulkFillAllocation(Set<Long> programSetOrgUnitIds);

    /**
     * Массово удаляет распределения
     *
     * @param programSetOrgUnitIds id наборов ОП
     */
    void doBulkDeleteAllocation(Set<Long> programSetOrgUnitIds);

    /**
     * Создает распределение
     *
     * @param programSetOrgUnitId id набора ОП
     * @return сохраненное распределение
     */
    EnrProgramAllocation doCreateAllocation(Long programSetOrgUnitId);

    /**
     * Удаляет распределение
     *
     * @param programSetOrgUnitId id набора ОП
     */
    void deleteAllocation(Long programSetOrgUnitId);

    /**
     * Удаляет элемент распределения
     *
     * @param allocationItemId id элемента распределения (Абитуриент в распределении по ОП, enrProgramAllocationItem)
     */
    void deleteAllocItem(Long allocationItemId);

    /**
     * Добавляет в распределение элементы
     *
     * @param allocation распределение
     * @param choiceResult выбранные ОП для выбранных конкурсов
     */
    void addItems(EnrProgramAllocation allocation, Map<EnrRequestedCompetition, EnrProgramSetItem> choiceResult);

    /**
     * Автоматически заполняет распределение (только если оно уже создано)
     *
     * @param programSetOrgUnitId id набора ОП
     */
    public void doFillAllocation(Long programSetOrgUnitId);

    /**
     * Абитуриенты для распределения по ОП - перечень нераспределенных выбранных конкурсов, по которым были зачислены абитуриенты, и выбранные ОП по каждому.
     * @param allocation распределение
     * @param competitionType вид приема
     * @return см. {@link EnrPAItemWrapper}, упорядочены по рейтингу
     */
    public List<EnrPAItemWrapper> getChoiceSource(EnrProgramAllocation allocation, EnrCompetitionType competitionType);

    /**
     * Массово создает приказы о распределении по образовательным программам
     *
     * @param programSetOrgUnitIds id наборов ОП
     */
    int doBulkCreateAllocationOrders(Set<Long> programSetOrgUnitIds);
}
