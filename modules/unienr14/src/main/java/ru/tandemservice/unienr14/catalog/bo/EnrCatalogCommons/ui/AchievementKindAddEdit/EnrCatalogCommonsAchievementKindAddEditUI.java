/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.ui.AchievementKindAddEdit;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;

/**
 * @author Nikolay Fedorovskih
 * @since 25.02.2015
 */
public class EnrCatalogCommonsAchievementKindAddEditUI extends BaseCatalogItemAddEditUI<EnrEntrantAchievementKind>
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getProperties().remove(EnrEntrantAchievementKind.P_DESCRIPTION);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return EnrCatalogCommonsAchievementKindAddEditUI.class.getPackage().getName() + ".AdditProps";
    }

    public boolean isDescriptionDisabled()
    {
        return getDisabledProperties().contains(EnrEntrantAchievementKind.P_DESCRIPTION);
    }
}