/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.mq.UniDQLExpressions;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/8/14
 */
public class EnrAllocationParagraphDao extends UniBaseDao implements IEnrAllocationParagraphDao
{
    @Override
    public void saveOrUpdateParagraph(EnrOrder order, EnrAllocationParagraph paragraph, List<EnrRequestedCompetition> preStudents, EnrRequestedCompetition manager, String groupTitle)
    {
        // В один и тот же приказ нельзя одновременно добавлять параграф
        NamedSyncInTransactionCheckLocker.register(getSession(), "EnrOrder" + order.getId());

        // V A L I D A T E

        if (preStudents == null || preStudents.isEmpty())
            throw new ApplicationException("Параграф не может быть пустым.");

        final Session session = getSession();
        session.refresh(order);

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + order.getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        List<EnrRequestedCompetition> list = new ArrayList<>(preStudents);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        if (manager != null && !list.contains(manager))
            throw new ApplicationException("Старостой может быть только один из предварительно зачисленных абитуриентов.");

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (EnrRequestedCompetition reqComp : list)
        {
            final EnrEntrant entrant = reqComp.getRequest().getEntrant();

            // абитуриент не должен быть в другом приказе
            EnrAllocationExtract allocationExtract = get(EnrAllocationExtract.class, IAbstractExtract.L_ENTITY, reqComp);
            if (allocationExtract != null && !(paragraph != null && paragraph.equals(allocationExtract.getParagraph())))
            {
                String orderNumber = allocationExtract.getParagraph().getOrder().getNumber();
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» уже в приказе" + (StringUtils.isEmpty(orderNumber) ? "" : " №" + orderNumber) + ".");
            }

            // данные приказа должны совпадать с данными абитуриента
            if (!order.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Приемная кампания у абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!order.getRequestType().equals(reqComp.getRequest().getType()))
                throw new ApplicationException("Вид заявления у выбранного конкурса «" + reqComp.getTitle() + "» отличается от указанного в приказе.");
            if (!order.getCompensationType().equals(reqComp.getCompetition().getType().getCompensationType()))
                throw new ApplicationException("Вид возмещения затрат у выбранного конкурса «" + reqComp.getTitle() + "» отличается от указанного в приказе.");

            // правильное состояние
            if (!EnrEntrantStateCodes.ENROLLED.equals(reqComp.getState().getCode()))
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» не может быть включен в приказ, так как выбранный конкурс в состоянии «" + reqComp.getState().getTitle() + "».");

            // EnrOrgUnit orgUnit, EduProgramForm form, EduProgramSubject subject, EnrCompetitionType competitionType

            // признаки должны соответствовать тому, что выбрано на форме
            if (!paragraph.getEnrProgramSetOrgUnit().getOrgUnit().equals(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit()))
                throw new ApplicationException("Филиал у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
            if (!paragraph.getEnrProgramSetItem().getProgramSet().getProgramForm().equals(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm()))
                throw new ApplicationException("Форма оубчения у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!paragraph.getEnrProgramSetItem().getProgramSet().getProgramSubject().equals(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject()))
                throw new ApplicationException("Направление (профессия, специальность) у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
        }

        if (paragraph.getId() != null)
        {
            // E D I T   P A R A G R A P H
            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (IAbstractExtract extract : paragraph.getExtractList()) {
                EnrAllocationExtract allocationExtract = (EnrAllocationExtract) extract;
                if (!list.remove(allocationExtract.getEntity())) {
                    if (paragraph.getGroupManager() != null && paragraph.getGroupManager().equals(allocationExtract)) {
                        paragraph.setGroupManager(null);
                        session.update(paragraph);
                    }
                    session.delete(allocationExtract);
                }
            }
        }
        else
        {
            // C R E A T E   P A R A G R A P H
            paragraph.setOrder(order);
            paragraph.setNumber(order.getParagraphCount() + 1);  // номер параграфа с единицы
            session.save(paragraph);
        }

        int counter = -1;
        for (EnrRequestedCompetition preStudent : list)
        {
            // создаем выписку
            EnrAllocationExtract extract = new EnrAllocationExtract();

            // в preStudent уже есть все данные для проведения выписки
            extract.setEntity(preStudent);
            extract.setParagraph(paragraph);
            extract.setCreateDate(order.getCreateDate());
            extract.setNumber(counter--); // нет номера. перенумеруем в конце метода update
            extract.setState(getByCode(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            extract.setGroupTitle(groupTitle);

            // устанавливается состояние «В приказе» для выбранного направления приема
//            extract.getEntity().getRequestedDirection().setState(getByCode(EnrEntrantState.class, EnrEntrantStateCodes.IN_ORDER));
//            session.update(extract.getEntity().getRequestedDirection());

            // сохраняем выписку
            session.save(extract);
        }

        // перенумеруем все выписки, так чтобы сортировка была по ФИО
        session.flush(); // flush перед запросом, чтобы исзлечь ВСЕ выписки
        List<EnrAllocationExtract> extractList = getList(EnrAllocationExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        extractList.sort(CommonCollator.comparing(e -> e.getEntity().getRequest().getEntrant().getFullFio(), true));

        // присваиваем уникальные номера выпискам
        counter = -1 - extractList.size(); /* заведомо неиспользованные номера */
        for (EnrAllocationExtract allocationExtract : extractList)
        {
            allocationExtract.setNumber(counter--);
            session.update(allocationExtract);
        }
        session.flush();

        // нумеруем выписки с единицы, расставляем имена групп зачисления, вычисляем выписку старосты
        counter = 1;
        EnrAllocationExtract groupManager = null;
        for (EnrAllocationExtract allocationExtract : extractList)
        {
            allocationExtract.setNumber(counter++);
            allocationExtract.setGroupTitle(groupTitle);

            if (allocationExtract.getEntity().equals(manager)) groupManager = allocationExtract;

            session.update(allocationExtract);
        }

        paragraph.setGroupManager(groupManager);
        session.update(paragraph);
    }

    @Override
    public int doCreateOrders(Set<Long> programSetOrgUnitIds)
    {
        final Session session = getSession();

        final Map<MultiKey, Map<EnrProgramSetOrgUnit, Map<EnrProgramSetItem, List<EnrRequestedCompetition>>>> competitionsMap = Maps.newHashMap();

        BatchUtils.execute(programSetOrgUnitIds, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            for(EnrProgramAllocationItem allocationItem : new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "ai")
                    .where(in(property("ai", EnrProgramAllocationItem.entrant().competition().programSetOrgUnit().id()), elements))
                    .where(notExists(new DQLSelectBuilder().fromEntity(EnrAllocationExtract.class, "ae")
                                             .where(eq(property("ai", EnrProgramAllocationItem.entrant().id()), property("ae", EnrAllocationExtract.entity().id())))
                                             .buildQuery()))
                    .createStatement(session).<EnrProgramAllocationItem>list())
            {
                EnrRequestedCompetition competition = allocationItem.getEntrant();
                MultiKey key = new MultiKey(competition.getRequest().getType().getId(),
                        competition.getCompetition().getType().getCompensationType().getCode());
                SafeMap.safeGet(
                        SafeMap.safeGet(
                                SafeMap.safeGet(competitionsMap, key, HashMap.class),
                                competition.getCompetition().getProgramSetOrgUnit(), HashMap.class),
                        allocationItem.getProgramSetItem(), ArrayList.class).add(competition);
            }
        });

        if(competitionsMap.isEmpty()) return 0;

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Set<String> takenOrderNumbers = new HashSet<>(new DQLSelectBuilder()
                .fromEntity(EnrOrder.class, "e").column(property("e", EnrOrder.number()))
                .where(UniDQLExpressions.eqYear("e", EnrOrder.createDate().s(), c.get(Calendar.YEAR)))
                .createStatement(getSession()).<String>list());

        EnrOrderPrintFormType printFormType = getCatalogItem(EnrOrderPrintFormType.class, EnrOrderPrintFormTypeCodes.BASE_ALLOCATION);
        EnrOrderType orderType = getCatalogItem(EnrOrderType.class, EnrOrderTypeCodes.ALLOCATION);
        String orderBasicText = EnrOrderManager.instance().dao().getDefaultOrderBasicText(EnrOrderTypeCodes.ALLOCATION);

        int count = 0;

        long orderNumberNext = 1;

        for(Map.Entry<MultiKey, Map<EnrProgramSetOrgUnit, Map<EnrProgramSetItem, List<EnrRequestedCompetition>>>> entry : competitionsMap.entrySet())
        {
            count++;

            EnrRequestedCompetition sample = entry.getValue().values().iterator().next().values().iterator().next().get(0);
            EnrRequestType requestType = sample.getRequest().getType();
            CompensationType compensationType = sample.getCompetition().getType().getCompensationType();

            EnrOrder order = new EnrOrder();
            orderNumberNext = findNumber(orderNumberNext, takenOrderNumbers);
            order.setNumber(String.valueOf(orderNumberNext));
            takenOrderNumbers.add(order.getNumber());
            order.setCommitDate(c.getTime());
            order.setEnrollmentCampaign(sample.getRequest().getEntrant().getEnrollmentCampaign());
            order.setRequestType(requestType);
            order.setCompensationType(compensationType);
            order.setType(orderType);
            order.setPrintFormType(getPrintFormType(printFormType, sample));
            order.setOrderBasicText(orderBasicText);

            EnrOrderManager.instance().dao().saveOrUpdateOrder(order, new Date(), true, null);
            getSession().flush();

            List<EnrProgramSetOrgUnit> ouKeys = Lists.newArrayList(entry.getValue().keySet());
            Collections.sort(ouKeys, (o1, o2) -> {
                if(!o1.equals(o2))
                {
                    if(o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit() instanceof TopOrgUnit) return -1;
                    else if(o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit() instanceof TopOrgUnit) return 1;
                    else return o1.getTitle().compareTo(o2.getTitle());
                }
                else
                    return 0;
            });
            for(EnrProgramSetOrgUnit programSetOrgUnit : ouKeys)
            {
                List<EnrProgramSetItem> itemKeys = Lists.newArrayList(entry.getValue().get(programSetOrgUnit).keySet());
                Collections.sort(itemKeys, CommonCollator.TITLED_WITH_ID_COMPARATOR);

                for(EnrProgramSetItem item : itemKeys)
                {
                    EnrAllocationParagraph paragraph = new EnrAllocationParagraph();
                    paragraph.setOrder(order);
                    paragraph.setEnrProgramSetOrgUnit(programSetOrgUnit);
                    paragraph.setEnrProgramSetItem(item);
                    saveOrUpdateParagraph(order, paragraph, entry.getValue().get(programSetOrgUnit).get(item), null, null);
                }
            }
        }
        return count;
    }

    private long findNumber(long orderNumberNext, Set<String> takenOrderNumbers)
    {
        while (takenOrderNumbers.contains(String.valueOf(orderNumberNext))) {
            orderNumberNext++;
        }
        return orderNumberNext;
    }

    protected EnrOrderPrintFormType getPrintFormType(EnrOrderPrintFormType defaultType, EnrRequestedCompetition sample) {
        return defaultType;
    }

    @Override
    public Collection<EnrAbstractExtract> getOtherExtracts(Collection<EnrEntrant> entrants, EnrAbstractParagraph paragraph)
    {
        return new DQLSelectBuilder()
            .fromEntity(EnrAbstractExtract.class, "e")
            .column(property("e"))
            .where(paragraph == null ? null : ne(property("e", EnrAbstractExtract.paragraph()), value(paragraph)))
            .where(or(
                    exists(new DQLSelectBuilder()
                            .fromEntity(EnrCancelExtract.class, "ce")
                            .where(eq(property("ce", EnrCancelExtract.id()), property("e", EnrAbstractExtract.id())))
                            .where(paragraph == null ? null : ne(property("ce", EnrCancelExtract.entity().paragraph().id()), value(paragraph.getId())))
                            .where(in(property("ce", EnrCancelExtract.requestedCompetition().request().entrant()), entrants))
                            .buildQuery()),
                    exists(new DQLSelectBuilder()
                            .fromEntity(EnrAllocationExtract.class, "ae")
                            .where(eq(property("ae", EnrAllocationExtract.id()), property("e", EnrAbstractExtract.id())))
                            .where(in(property("ae", EnrAllocationExtract.entity().request().entrant()), entrants))
                            .buildQuery())
            ))
                .order(property("e", EnrAbstractExtract.createDate()))
                .createStatement(this.getSession()).list();
    }
}