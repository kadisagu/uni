/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.cast;

/**
 * enr_competition_ext_view
 *
 * @author oleyba
 * @since 8/23/13
 */
public class EnrExtViewProvider4Competition extends SimpleDQLExternalViewConfig
{

    @Override
    protected DQLSelectBuilder buildDqlQuery() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrCompetition.class, "comp")

            .joinPath(DQLJoinType.left, EnrCompetition.programSetOrgUnit().fromAlias("comp"), "ps_ou")
            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.programSet().fromAlias("ps_ou"), "ps")
            .joinPath(DQLJoinType.left, EnrProgramSetBase.programSubject().fromAlias("ps"), "subject")
            .joinPath(DQLJoinType.left, EduProgramSubject.subjectIndex().fromAlias("subject"), "s_index")
            .joinPath(DQLJoinType.left, EduProgramSubjectIndex.programKind().fromAlias("s_index"), "program_kind")

            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.formativeOrgUnit().fromAlias("ps_ou"), "form_ou")
            .joinPath(DQLJoinType.left, EnrProgramSetOrgUnit.orgUnit().fromAlias("ps_ou"), "enr_ou")
            .joinPath(DQLJoinType.left, EnrOrgUnit.institutionOrgUnit().fromAlias("enr_ou"), "inst_ou")
            .joinPath(DQLJoinType.left, EduInstitutionOrgUnit.orgUnit().fromAlias("inst_ou"), "enr_ou_ou")
        ;

        column(dql, property("comp", EnrCompetition.id()), "id").comment("uid конкурса");
        column(dql, property("ps", EnrProgramSetBase.enrollmentCampaign().title()), "enrollmentCampaign").comment("приемная кампания");
        column(dql, property("ps", EnrProgramSetBase.enrollmentCampaign().educationYear().intValue()), "enrollmentCampaignYear").comment("год приема (календарный)");
        column(dql, property("ps", EnrProgramSetBase.enrollmentCampaign().educationYear().title()), "enrollmentCampaignEduYear").comment("год приема (учебный)");

        column(dql, property("comp", EnrCompetition.plan()), "admissionPlan").comment("план приема");

        column(dql, property("ps", EnrProgramSetBase.id()), "programSetId").comment("uid набора ОП");
        column(dql, property("ps", EnrProgramSetBase.title()), "programSetTitle").comment("название набора ОП");

        column(dql, property("ps_ou", EnrProgramSetOrgUnit.id()), "programSetOuId").comment("uid подразделения, ведущего прием по набору ОП");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.ministerialPlan()), "programSetOuMinisterialPlan").comment("КЦП по набору ОП для подразделения, ведущего прием");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.targetAdmPlan()), "programSetOuTargetAdmPlan").comment("план приема по ЦП по набору ОП для подразделения, ведущего прием");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.exclusivePlan()), "programSetOuExclusivePlan").comment("план приема по квоте по набору ОП для подразделения, ведущего прием");
        column(dql, property("ps_ou", EnrProgramSetOrgUnit.contractPlan()), "programSetOuContractPlan").comment("план приема по договору по набору ОП для подразделения, ведущего прием");

        column(dql, property("enr_ou", EnrOrgUnit.id()), "enrOrgUnitId").comment("uid филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.fullTitle()), "enrOrgUnitFullTitle").comment("полное название филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.shortTitle()), "enrOrgUnitShortTitle").comment("сокращенное название филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.territorialFullTitle()), "enrOrgUnitTerrFullTitle").comment("полное терр. название филиала в рамках ПК");
        column(dql, property("enr_ou_ou", OrgUnit.territorialShortTitle()), "enrOrgUnitTerrShortTitle").comment("сокращенное терр. название филиала в рамках ПК");

        column(dql, property("form_ou", OrgUnit.id()), "formativeOrgUnitId").comment("uid формирующего подразделения");
        column(dql, property("form_ou", OrgUnit.fullTitle()), "formativeOrgUnitFullTitle").comment("полное название формирующего подразделения");
        column(dql, property("form_ou", OrgUnit.shortTitle()), "formativeOrgUnitShortTitle").comment("сокращенное название формирующего подразделения");

        column(dql, property("comp", EnrCompetition.type().title()), "competitionType").comment("вид приема");
        column(dql, property("comp", EnrCompetition.type().code()), "competitionTypeCode").comment("код вида приема");

        column(dql, property("comp", EnrCompetition.requestType().title()), "requestType").comment("вид заявления");
        column(dql, property("comp", EnrCompetition.requestType().code()), "requestTypeCode").comment("код вида заявления");

        column(dql, property("comp", EnrCompetition.type().compensationType().title()), "compensationType").comment("вид возм. затрат");
        column(dql, property("comp", EnrCompetition.type().compensationType().code()), "compensationTypeCode").comment("код вида возм. затрат");

        column(dql, property("comp", EnrCompetition.eduLevelRequirement().title()), "eduLevelRequirement").comment("ограничение по уровню образования поступающих");
        column(dql, property("comp", EnrCompetition.eduLevelRequirement().code()), "eduLevelRequirementCode").comment("код ограничения по уровню образования поступающих");

        column(dql, property("ps", EnrProgramSetBase.programForm().title()), "programForm").comment("форма обучения");
        column(dql, property("ps", EnrProgramSetBase.programForm().code()), "programFormCode").comment("код формы обучения");

        column(dql, property("ps", EnrProgramSetBase.programSubject().title()), "programSubject").comment("направление (профессия, специальность)");
        column(dql, property("ps", EnrProgramSetBase.programSubject().subjectCode()), "programSubjectCode").comment("код направления (профессии, специальности)");

        column(dql, property("program_kind", EduProgramKind.title()), "programKind").comment("вид обр. программы");
        column(dql, property("program_kind", EduProgramKind.code()), "programKindCode").comment("код вида обр. программы");
        column(dql, property("program_kind", EduProgramKind.shortTitle()), "programKindShortTitle").comment("сокр. название вида обр. программы");

        column(dql, property("program_kind", EduProgramKind.eduLevel().title()), "levelType").comment("уровень образования");
        column(dql, property("program_kind", EduProgramKind.eduLevel().code()), "levelTypeCode").comment("код уровня образования");

        booleanIntColumn(dql, EduProgramKind.programBachelorDegree().fromAlias("program_kind"), "bachelor").comment("ОП по программе бакалавриата (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programHigherProf().fromAlias("program_kind"), "high").comment("ОП по программе высшего образования (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programMasterDegree().fromAlias("program_kind"), "master").comment("ОП по программе магистратуры (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programSecondaryProf().fromAlias("program_kind"), "middle").comment("ОП по программе СПО (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programSpecialistDegree().fromAlias("program_kind"), "specialty").comment("ОП по программе специалитета (1 - да, 0 - нет)");

        column(dql,
            new DQLCaseExpressionBuilder(property("program_kind", EduProgramKind.code()))
            .when(value(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA), cast(value("бакалавр"), PropertyType.STRING))
            .when(value(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV), cast(value("специалист"), PropertyType.STRING))
            .when(value(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY), cast(value("магистр"), PropertyType.STRING))
            .otherwise(cast(value("-"), PropertyType.STRING))
            .build(),
            "qualification"
        ).comment("квалификация (не для всех видов программ, разделение бакалавров на академ. и прикл. пока не поддерживается)");

        return dql;
    }
}
