/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.MarkEdit;

import java.util.Date;
import java.util.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

/**
 * @author oleyba
 * @since 5/9/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "examPassDiscipline.id")
})
public class EnrExamPassDisciplineMarkEditUI extends UIPresenter
{
    private EnrExamPassDiscipline examPassDiscipline = new EnrExamPassDiscipline();

    @Override
    public void onComponentRefresh()
    {
        setExamPassDiscipline(IUniBaseDao.instance.get().getNotNull(EnrExamPassDiscipline.class, getExamPassDiscipline().getId()));
        Date examDate = getExamDate();
        if (examDate != null && !examDate.equals(getExamPassDiscipline().getMarkDate())) {
            getExamPassDiscipline().setMarkDate(examDate);
        }
    }

    public void onClickApply()
    {
        EnrExamPassDisciplineManager.instance().dao().updateExamPassDiscipline(getExamPassDiscipline());
        deactivate();
    }

    public void onSetAbsenceNote() {
        getExamPassDiscipline().syncAbsenceNoteAndMarkState();
    }

    public boolean isDateDisabled() {
        return getExamPassDiscipline().getExamGroup() != null;
    }

    private Date getExamDate() {
        if (getExamPassDiscipline().getExamGroup() == null)
            return null;
        List<EnrExamGroupScheduleEvent> events = IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class,
            EnrExamGroupScheduleEvent.examGroup(), getExamPassDiscipline().getExamGroup(),
            EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());
        if (events.isEmpty())
            throw new ApplicationException("Нельзя выставить оценку по дисциплине для сдачи, включенной в ЭГ, если ЭГ не помещена в ячейку расписания.");
        return events.get(events.size() - 1).getExamScheduleEvent().getScheduleEvent().getDurationBegin();
    }

    // getters and setters

    public EnrExamPassDiscipline getExamPassDiscipline()
    {
        return examPassDiscipline;
    }

    public void setExamPassDiscipline(EnrExamPassDiscipline examPassDiscipline)
    {
        this.examPassDiscipline = examPassDiscipline;
    }
}