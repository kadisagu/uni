/**
 *$Id: EnrEntrantDocumentTypeEnrCampaign.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.ui.TypeEnrCampaign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic.EnrEntrantDocumentTypeEnrCampaignSearchDSHandler;

/**
 * @author Alexander Shaburov
 * @since 30.04.13
 */
@Configuration
public class EnrEntrantDocumentTypeEnrCampaign extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String ENTRANT_DOCUMENT_TYPE_SEARCH_DS = "entrantDocumentTypeSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        .addDataSource(searchListDS(ENTRANT_DOCUMENT_TYPE_SEARCH_DS, entrantDocumentTypeSearchDSColumns(), entrantDocumentTypeSearchDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint entrantDocumentTypeSearchDSColumns()
    {
        return columnListExtPointBuilder(ENTRANT_DOCUMENT_TYPE_SEARCH_DS)
        .addColumn(textColumn("title", PersonDocumentType.title()))
//        .addColumn(
//            toggleColumn("use", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_USE_IN_ENR_CAMP)
//            .disabled("ui:systemItem")
//            .toggleOnListener("onToggleEntrantDocumentTypeUse").toggleOnLabel("entrantDocumentType.use")
//            .toggleOffListener("onToggleEntrantDocumentTypeNotUse").toggleOffLabel("entrantDocumentType.notUse")
//        )
        .addColumn(
            headerColumn("benefit")
            .addSubColumn(
                toggleColumn("benefitNoExams", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_BENEFIT_NO_EXAMS)
                .disabled("ui:systemItem")
                .toggleOnListener("onToggleEntrantDocumentTypeBenefitNoExams").toggleOnLabel("entrantDocumentType.benefitNoExams")
                .toggleOffListener("onToggleEntrantDocumentTypeNotBenefitNoExams").toggleOffLabel("entrantDocumentType.notBenefitNoExams")
                .create()
            )
            .addSubColumn(
                toggleColumn("benefitExclusive", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_BENEFIT_EXCLUSIVE)
                .disabled("ui:systemItem")
                .toggleOnListener("onToggleEntrantDocumentTypeBenefitExclusive").toggleOnLabel("entrantDocumentType.benefitExclusive")
                .toggleOffListener("onToggleEntrantDocumentTypeNotBenefitExclusive").toggleOffLabel("entrantDocumentType.notBenefitExclusive")
                .create()
            )
            .addSubColumn(
                toggleColumn("benefitPreference", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_BENEFIT_PREFERENCE)
                .disabled("ui:systemItem")
                .toggleOnListener("onToggleEntrantDocumentTypeBenefitPreference").toggleOnLabel("entrantDocumentType.benefitPreference")
                .toggleOffListener("onToggleEntrantDocumentTypeNotBenefitPreference").toggleOffLabel("entrantDocumentType.notBenefitPreference")
                .create()
            )
            .addSubColumn(
                toggleColumn("benefitMaxMark", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_BENEFIT_MAX_MARK)
                .disabled("ui:systemItem")
                .toggleOnListener("onToggleEntrantDocumentTypeBenefitMaxMark").toggleOnLabel("entrantDocumentType.benefitMaxMark")
                .toggleOffListener("onToggleEntrantDocumentTypeNotBenefitMaxMark").toggleOffLabel("entrantDocumentType.notBenefitMaxMark")
                .create()
            )
        )

        .addColumn(
            toggleColumn("copy", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_COPY)
            .disabled("ui:eduDocOrIdentityCard")
            .toggleOnListener("onToggleEntrantDocumentTypeCopyAccepted").toggleOnLabel("entrantDocumentType.copyAccepted")
            .toggleOffListener("onToggleEntrantDocumentTypeCopyNotAccepted").toggleOffLabel("entrantDocumentType.copyNotAccepted")
        )
                .addColumn(
                        toggleColumn("original", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_ORIGINAL)
                                .disabled("ui:eduDocOrIdentityCard")
                                .toggleOnListener("onToggleEntrantDocumentTypeOriginalAccepted").toggleOnLabel("entrantDocumentType.originalAccepted")
                                .toggleOffListener("onToggleEntrantDocumentTypeOriginalNotAccepted").toggleOffLabel("entrantDocumentType.originalNotAccepted")
                )
        .addColumn(
            toggleColumn("achievement", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_ACHIEVEMENT)
            .disabled("ui:systemItemExcludeOlympiad")
            .toggleOnListener("onToggleEntrantDocumentTypeConfirmAchievement").toggleOnLabel("entrantDocumentType.confirmAchievement")
            .toggleOffListener("onToggleEntrantDocumentTypeNotConfirmAchievement").toggleOffLabel("entrantDocumentType.notConfirmAchievement")
        )
                .addColumn(
                        toggleColumn("sendFIS", EnrEntrantDocumentTypeEnrCampaignSearchDSHandler.PROP_SEND_FIS)
                                .disabled("ui:systemItem")
                                .toggleOnListener("onToggleEntrantDocumentTypeSendFIS").toggleOnLabel("entrantDocumentType.sendFIS")
                                .toggleOffListener("onToggleEntrantDocumentTypeNotSendFIS").toggleOffLabel("entrantDocumentType.notSendFIS")
                )

        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entrantDocumentTypeSearchDSHandler()
    {
        return new EnrEntrantDocumentTypeEnrCampaignSearchDSHandler(getName());
    }
}
