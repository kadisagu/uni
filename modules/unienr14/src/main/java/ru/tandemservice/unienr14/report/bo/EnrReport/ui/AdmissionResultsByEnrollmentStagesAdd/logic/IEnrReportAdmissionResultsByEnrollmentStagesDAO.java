/* $Id$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd.EnrReportAdmissionResultsByEnrollmentStagesAddUI;

/**
 * @author nvankov
 * @since 8/19/14
 */
public interface IEnrReportAdmissionResultsByEnrollmentStagesDAO extends INeedPersistenceSupport
{
    Long createReport(EnrReportAdmissionResultsByEnrollmentStagesAddUI enrReportAdmissionResultsByEnrollmentStagesAddUI);
}
