/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubStateExamResultTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantStateExamResultDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

/**
 * @author oleyba
 * @since 4/12/14
 */
@Configuration
public class EnrEntrantPubStateExamResultTab extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS("stateExamResultDS", stateExamResultDSColumns(), stateExamResultDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint stateExamResultDSColumns()
    {
        return columnListExtPointBuilder("stateExamResultDS")
            .addColumn(textColumn("year", EnrEntrantStateExamResult.year().s()))
            .addColumn(textColumn("subject", EnrEntrantStateExamResult.subject().title().s()))
            .addColumn(textColumn("mark", EnrEntrantStateExamResult.mark().s()))
            .addColumn(booleanColumn("abovePassingMark", EnrEntrantStateExamResult.abovePassingMark().s()))
            .addColumn(textColumn("certificate", EnrEntrantStateExamResult.documentNumber().s()))
            .addColumn(textColumn("issuanceDate", EnrEntrantStateExamResult.documentIssuanceDate().s()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
            .addColumn(textColumn("checkStatus", EnrEntrantStateExamResult.stateCheckStatus().s()))
            .addColumn(booleanColumn("secondWave", EnrEntrantStateExamResult.secondWave().s()))
            .addColumn(textColumn("secondWaveExamPlace", EnrEntrantStateExamResult.secondWaveExamPlace().title().s()))
            .addColumn(toggleColumn("accepted", EnrEntrantStateExamResult.accepted())
                .permissionKey("enr14EntrantPubStateExamResultTabAccept")
                .toggleOnListener("onToggleAccepted")
                .toggleOffListener("onToggleAccepted"))
            .addColumn(toggleColumn("verified", EnrEntrantStateExamResult.verifiedByUser())
                .permissionKey("enr14EntrantPubStateExamResultTabVerifyByUser")
                .toggleOnListener("onToggleVerified")
                .toggleOffListener("onToggleVerified"))
            .addColumn(textColumn("verifiedComment", EnrEntrantStateExamResult.verifiedByUserComment().s()))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit").permissionKey("enr14EntrantPubStateExamResultTabEdit").disabled(EnrEntrantStateExamResult.P_EDIT_DISABLED).alert("ui:editAlert"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete").permissionKey("enr14EntrantPubStateExamResultTabDelete").disabled(EnrEntrantStateExamResult.P_EDIT_DISABLED).alert(new FormattedMessage("stateExamResultDS.delete.alert", EnrEntrantStateExamResult.title().s())))


//        dataSource.addColumn(new ToggleColumn("Зачтено", EnrStateExamCertificate.P_ACCEPTED).setListener("onChangeCertificateChecked").setPermissionKey("enr14EntrantPubStateExamCertificateTabAcceptCert").setDisableHandler(verifiedByUserHandler));
//        dataSource.addColumn(new ToggleColumn("Проверено вручную", EnrStateExamCertificate.P_VERIFIED_BY_USER).setListener("onChangeCertificateVerified").setPermissionKey("enr14EntrantPubStateExamCertificateTabVerifyByUserCert").setDisableHandler(entityHandler));
//        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStateExamCertificate", null).setPermissionKey("enr14EntrantPubStateExamCertificateTabEditCert").setDisableHandler(verifiedByUserHandler));
//        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить свидетельство о результатах ЕГЭ № {0}?", EnrStateExamCertificate.formattedNumber().s()).setPermissionKey("enr14EntrantPubStateExamCertificateTabDeleteCert").setDisableHandler(verifiedByUserHandler));
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> stateExamResultDSHandler()
    {
        return new EnrEntrantStateExamResultDSHandler(getName()).setPageable(false);
    }
}