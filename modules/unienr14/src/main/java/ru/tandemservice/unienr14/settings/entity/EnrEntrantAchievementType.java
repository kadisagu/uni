package ru.tandemservice.unienr14.settings.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantAchievementKindCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;
import ru.tandemservice.unienr14.settings.entity.gen.*;

import java.math.BigDecimal;

/**
 * Вид индивидуального достижения в ПК
 */
public class EnrEntrantAchievementType extends EnrEntrantAchievementTypeGen implements IFisUidByIdOwner, ITitled
{
    @EntityDSLSupport
    @Override
    public Double getAchievementMark()
    {
        return 0.001d * getAchievementMarkAsLong();
    }

    public BigDecimal getAchievementMarkAsBD() {
        return BigDecimal.valueOf(getAchievementMarkAsLong()).divide(BigDecimal.valueOf(1000));
    }

    public String getAchievementMarkAsString()
    {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getAchievementMarkAsLong());
    }

    public void setAchievementMark(Double mark)
    {
        setAchievementMarkAsLong(mark == null ? (long) 0 : (long) (1000.0d * mark));
    }

    public String getAchievementMarkFieldTitle()
    {
        return isMarked() ? "Максимальный балл за достижение" : "Балл за достижение";
    }

    @Override
    public String getFisUidTitle()
    {
        return "Вид ИД в ПК: " + getAchievementKind().getTitle() + ", " + getEnrollmentCampaign().getTitle();
    }

    @Override
    public String getTitle()
    {
        if (getAchievementKind() == null) {
            return this.getClass().getSimpleName();
        }
        return getAchievementKind().getTitle();
    }
}
