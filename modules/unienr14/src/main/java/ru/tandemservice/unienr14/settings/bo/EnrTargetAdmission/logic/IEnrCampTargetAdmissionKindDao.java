/**
 *$Id: IEnrCampTargetAdmissionKindDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public interface IEnrCampTargetAdmissionKindDao extends INeedPersistenceSupport
{
    /**
     * Вид ЦП помечается как используемый в ПК:
     * создается объект EnrCampaignTargetAdmissionKind для данного Вида ЦП и ПК.
     * @param kind вид целевого приема
     * @param enrollmentCampaign приемная компания
     * @return id созданного объекта или того объекта, который уже существовал
     */
    Long doToogleTargetAdmissionKindUse(EnrTargetAdmissionKind kind, EnrEnrollmentCampaign enrollmentCampaign);

    /**
     * Вид ЦП помечается как не используемый в ПК:
     * удаляется объект EnrCampaignTargetAdmissionKind для данного Вида ЦП и ПК.
     * @param kind вид целевого приема
     * @param enrollmentCampaign приемная компания
     * @return true, если объект был успешно удален, либо false, если объекта не существовало
     */
    boolean doToogleTargetAdmissionKindNotUse(EnrTargetAdmissionKind kind, EnrEnrollmentCampaign enrollmentCampaign);

    /**
     * Используется для задания цифр по планам приема и получения их же
     * @param campaign ПК
     * @return Виды ЦП, используемые в ПК, которые являются "листьями" - т.е. не имеют используемых в ПК детей
     */
    Set<EnrTargetAdmissionKind> getUsedTaKinds(EnrEnrollmentCampaign campaign);

    List<EnrTargetAdmissionKind> getSortedTaKindList(EnrEnrollmentCampaign campaign);

    /**
     * Используется только один вид ЦП в рамках данной ПК.
     * @param campaign ПК
     * @return true, если используется только один вид ЦП, иначе false
     */
    boolean isUsedOnceTA(EnrEnrollmentCampaign campaign);

    /**
     * Обновляет связанные с видом ЦП в ПК внешние организации.
     * @param taKindId вид ЦП
     * @param campaign преимная кампания
     * @param externalOrgUnits внешние организации
     */
    void updateTAKindExtOrgUnits(Long taKindId, EnrEnrollmentCampaign campaign, List<ExternalOrgUnit> externalOrgUnits);

    /**
     * Выставляется значение Прием в интересах государства для вида ЦП в ПК:
     * @param kind вид целевого приема
     * @param enrollmentCampaign приемная компания
     * @param stateInterest прием в интересах государства
     * @return true, если объект был успешно удален, либо false, если объекта не существовало
     */

    void doToogleTargetAdmissionStateInterest(EnrTargetAdmissionKind kind, EnrEnrollmentCampaign enrollmentCampaign, boolean stateInterest);
}
