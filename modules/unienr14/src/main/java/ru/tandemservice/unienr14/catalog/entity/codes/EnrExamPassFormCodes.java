package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма сдачи"
 * Имя сущности : enrExamPassForm
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrExamPassFormCodes
{
    /** Константа кода (code) элемента : ЕГЭ (title) */
    String STATE_EXAM = "1";
    /** Константа кода (code) элемента : Тест. (title) */
    String TEST = "2";
    /** Константа кода (code) элемента : Экзамен (title) */
    String EXAM = "3";
    /** Константа кода (code) элемента : Собесед. (title) */
    String INTERVIEW = "4";
    /** Константа кода (code) элемента : Олимп. (title) */
    String OLYMPIAD = "6";

    Set<String> CODES = ImmutableSet.of(STATE_EXAM, TEST, EXAM, INTERVIEW, OLYMPIAD);
}
