/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitTab.EnrEntrantOrgUnitTab;

@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String TAB_ENR_ORG_UNIT = "enr14OrgUnitTab";

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return this.tabPanelExtensionBuilder(this._orgUnitView.orgUnitTabPanelExtPoint())
        .addTab(
            componentTab(TAB_ENR_ORG_UNIT, EnrEntrantOrgUnitTab.class)
            .permissionKey("ui:secModel.orgUnit_viewEnr14Tab")
            .parameters("mvel:['publisherId':presenter.orgUnit.id]")
            .visible("ognl:@"+OrgUnitViewExt.class.getName()+"@showEnrOrgUnitTab(presenter.orgUnit)")
        )
        .create();
    }

    public static boolean showEnrOrgUnitTab(final OrgUnit orgUnit) {
        return EnrEntrantManager.instance().dao().isOrgUnitFormingOrEnrOrgUnit(orgUnit);
    }

}
