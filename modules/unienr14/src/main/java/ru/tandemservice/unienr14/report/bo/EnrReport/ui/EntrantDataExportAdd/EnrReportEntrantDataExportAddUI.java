/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantDataExportAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantDataExport;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/7/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entity.id")
})
public class EnrReportEntrantDataExportAddUI extends UIPresenter {


    // Fields
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrEnrollmentCampaign enrollmentCampaign;

    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;

    private EnrReportStageSelector stageSelector = new EnrReportStageSelector();

    // From UI
    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "Не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "Включать в отчет только выбранные параллельно условия поступления")));

        configUtil(getCompetitionFilterAddon());
    }

    // validate
    private void validate()
    {
        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    // Listeners

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        configUtilWhere(getCompetitionFilterAddon());
        dateSelector.refreshDates(getEnrollmentCampaign());
    }

    public void onClickApply() {
        validate();
        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EnrReportEntrantDataExport report = new EnrReportEntrantDataExport();

            report.setFormingDate(new Date());

            report.setEnrollmentCampaign(getEnrollmentCampaign());
            report.setDateFrom(getDateSelector().getDateFrom());
            report.setDateTo(getDateSelector().getDateTo());
            report.setEnrollmentStage(getStage().getTitle());

            EnrCompetitionFilterAddon filterAddon = getCompetitionFilterAddon();
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantDataExport.P_REQUEST_TYPE, "title");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantDataExport.P_COMPENSATION_TYPE, "shortTitle");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantDataExport.P_PROGRAM_FORM, "title");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantDataExport.P_COMPETITION_TYPE, "title");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantDataExport.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantDataExport.P_FORMATIVE_ORG_UNIT, "fullTitle");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantDataExport.P_PROGRAM_SUBJECT, "title");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantDataExport.P_EDU_PROGRAM, "title");
            EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantDataExport.P_PROGRAM_SET, "title");
            if (isParallelActive()) {
                report.setParallel(getParallel().getTitle());
            }

            List<String> parameters = new ArrayList<>();
            for (String property : EnrReportEntrantDataExport.getDescription().getPropertyList()) {
                Object propertyValue = report.getProperty(property);
                if (propertyValue instanceof String) {
                    parameters.add(report.getEntityMeta().getProperty(property).getTitle() + ": " + String.valueOf(propertyValue));
                } else if (propertyValue instanceof ITitled) {
                    parameters.add(report.getEntityMeta().getProperty(property).getTitle() + ": " + ((ITitled)propertyValue).getTitle());
                }
            }

            EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(getDateSelector().getDateFrom(), getDateSelector().getDateTo())
                .filter(getEnrollmentCampaign())
                ;

            getStageSelector().applyFilter(requestedCompDQL, "reqComp");

            filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

            if (isParallelActive()) {
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(1L==getParallel().getId())));
            }

            requestedCompDQL.order(property(EnrEntrant.person().identityCard().fullFio().fromAlias(requestedCompDQL.entrant())));

            requestedCompDQL.column(requestedCompDQL.reqComp() + ".id");
            List<Long> requestedCompetitions = requestedCompDQL.createStatement(session).list();

            DatabaseFile dbFile = new DatabaseFile();

            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_DATA_EXPORT),
                    "template", new byte[]{}, // Шаблон не используем, делаем заглушку
                    "parameters", parameters,
                    "requestedCompetitionIds", requestedCompetitions
                    );

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }

            dbFile.setContent(content);
            dbFile.setFilename(filename);
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_RTF));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });

        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    // Util

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    // Getters-setters

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public void setDateSelector(EnrReportDateSelector dateSelector) {
        this.dateSelector = dateSelector;
    }

    public boolean isParallelActive() {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive) {
        this.parallelActive = parallelActive;
    }

    public IdentifiableWrapper getParallel() {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel) {
        this.parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList() {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList) {
        this.parallelList = parallelList;
    }

    public IdentifiableWrapper getStage() {
        return getStageSelector().getStage();
    }

    public void setStage(IdentifiableWrapper stage) {
        getStageSelector().setStage(stage);
    }

    public List<IdentifiableWrapper> getStageList() {
        return getStageSelector().getStageList();
    }

    public EnrReportStageSelector getStageSelector() {
        return stageSelector;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}