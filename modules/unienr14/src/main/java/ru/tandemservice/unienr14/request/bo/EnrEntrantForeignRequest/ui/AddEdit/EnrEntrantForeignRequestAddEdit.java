/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 7/15/14
 */
@Configuration
public class EnrEntrantForeignRequestAddEdit extends BusinessComponentManager
{
    public static final String ENTRANT_DS = "entrantDS";
    public static final String COMPETITION_DS = "competitionDS";
    public static final String IDENTITY_CARD_DS = "identityCardDS";
    public static final String EDU_DOC_DS = "eduDocDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENTRANT_DS, entrantDSHandler()).addColumn("fio", EnrEntrant.person().identityCard().s(), new IFormatter<IdentityCard>()
                {
                    @Override
                    public String format(IdentityCard source)
                    {
                        return source.getFullFio() + " (" + source.getDisplayableTitle() +")";
                    }
                }))
                .addDataSource(selectDS(COMPETITION_DS, competitionDSHandler()).addColumn(EnrCompetition.title().s()))
                .addDataSource(selectDS(IDENTITY_CARD_DS, identityCardDSHandler()).addColumn(IdentityCard.displayableTitle().s()))
                .addDataSource(selectDS(EDU_DOC_DS, eduDocDSHandler()).addColumn(PersonEduDocument.displayableTitle().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrant.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long enrCampaignId = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                dql.where(eq(
                        property(alias, EnrEntrant.enrollmentCampaign().id()),
                        value(enrCampaignId)));
                dql.where(notExists(new DQLSelectBuilder().fromEntity(IdentityCard.class, "ic")
                        .where(eq(property("ic", IdentityCard.id()), property(alias, EnrEntrant.person().identityCard().id())))
                        .where(eq(property("ic", IdentityCard.cardType().code()), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)))
                        .buildQuery()));
            }
        }
                .filter(EnrEntrant.person().identityCard().fullFio())
                .order(EnrEntrant.person().identityCard().fullFio())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetition.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long enrCampaignId = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                dql.where(eq(
                        property(alias, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().id()),
                        value(enrCampaignId)));

                DQLSelectBuilder  priorityBuilder = new DQLSelectBuilder()
                        .fromEntity(EnrCompetition.class, "c")
                        .column(property("c.id"))
                        .order(property("c", EnrCompetition.type().code()), OrderDirection.asc)
                        .order(property("c", EnrCompetition.eduLevelRequirement().priority()), OrderDirection.asc)
                        .where(or(eq(property("c", EnrCompetition.type().code()), value(EnrCompetitionTypeCodes.MINISTERIAL)),
                                eq(property("c", EnrCompetition.type().code()), value(EnrCompetitionTypeCodes.CONTRACT))))
                        .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().id()), property(alias, EnrCompetition.programSetOrgUnit().programSet().id())))
                        .top(1);

                dql.where(eq(property(alias, EnrCompetition.id()), priorityBuilder.buildQuery()));
            }
        }
                .filter(EnrCompetition.programSetOrgUnit().programSet().title())
                .order(EnrCompetition.programSetOrgUnit().programSet().title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler identityCardDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), IdentityCard.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                Long entrantId = context.get("entrantId");
                if(entrantId != null)
                {
                    dql.where(exists(new DQLSelectBuilder().fromEntity(EnrEntrant.class, "entr")
                            .where(eq(
                                    property("entr", EnrEntrant.person().id()),
                                    property(alias, IdentityCard.person().id())))
                            .where(eq(property("entr", EnrEntrant.id()), value(entrantId)))
                            .buildQuery()));
                }
                else
                {
                    dql.where(eq(value(1), value(0)));
                }
            }
        }
                .filter(IdentityCard.seria())
                .filter(IdentityCard.number())
                .filter(IdentityCard.fullFio())
                .order(IdentityCard.fullFio())
                .order(IdentityCard.seria())
                .order(IdentityCard.number())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduDocDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PersonEduDocument.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long entrantId = context.get("entrantId");
                if (entrantId != null)
                {
                    dql.where(exists(new DQLSelectBuilder().fromEntity(EnrEntrant.class, "entr")
                            .where(eq(
                                    property("entr", EnrEntrant.person().id()),
                                    property(alias, PersonEduDocument.person().id())))
                            .where(eq(property("entr", EnrEntrant.id()), value(entrantId)))
                            .buildQuery()));
                }
                else
                {
                    dql.where(eq(value(1), value(0)));
                }
            }
        }
                .order(PersonEduDocument.seria())
                .order(PersonEduDocument.number())
                .pageable(true);
    }
}



    