/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements;

import com.google.common.collect.Maps;
import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.UIDataSource;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unienr14.catalog.entity.EnrTieBreakRatingRule;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.EnrSettingsManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.SettingsOptionGroup;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;

import java.util.*;

/**
 * @author nvankov
 * @since 2/18/16
 */
public class EnrSettingsAdmissionRequirementsUI extends UIPresenter
{
    public static final String PARAM_ENR_CAMPAIGN_DS = "enrCampaignDS";

    private DataWrapper _accountingRulesOrigEduDoc;
    private DataWrapper _enrollmentRulesBudget;
    private DataWrapper _accountingRulesAgreementBudget;
    private DataWrapper _enrollmentRulesContract;
    private DataWrapper _accountingRulesAgreementContract;
    private DataWrapper _acceptedContractDefault;
    private EnrTieBreakRatingRule _tieBreakRatingRule;
    private DataWrapper _reenrollEnabled;
    private DataWrapper _excludeEnrByLowPriority;

    private List<SettingsOptionGroup> _optionsGroups = Lists.newArrayList();
    private SettingsOptionGroup _currentOptionsGroup;

    private Map<String, DataWrapper> _valueMap = Maps.newHashMap();

    @Override
    public void onComponentRefresh()
    {
        EnrEnrollmentCampaign defaultCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        getSettings().set(PARAM_ENR_CAMPAIGN_DS, defaultCampaign);
        if(defaultCampaign != null)
            registerDataSourcesAndFillValueMap();
    }

    private void registerDataSourcesAndFillValueMap()
    {
        final EnrEnrollmentCampaignSettings settings = getSettings().<EnrEnrollmentCampaign>get(PARAM_ENR_CAMPAIGN_DS).getSettings();

        EnrSettingsAdmissionRequirements config = getConfig().getManager();

        setAccountingRulesOrigEduDoc(settings.getAccountingRulesOrigEduDoc() == 0L ? TwinComboDataSourceHandler.getYesOption() : TwinComboDataSourceHandler.getNoOption());
        setEnrollmentRulesBudget(settings.getEnrollmentRulesBudget() == 0L ? TwinComboDataSourceHandler.getYesOption() : TwinComboDataSourceHandler.getNoOption());
        setAccountingRulesAgreementBudget(settings.getAccountingRulesAgreementBudget() == 0L ? TwinComboDataSourceHandler.getYesOption() : TwinComboDataSourceHandler.getNoOption());
        setEnrollmentRulesContract(config.enrollmentRulesContractExtPoint().getItem(String.valueOf(settings.getEnrollmentRulesContract())));
        setAccountingRulesAgreementContract(settings.getAccountingRulesAgreementContract() == 0L ? TwinComboDataSourceHandler.getYesOption() : TwinComboDataSourceHandler.getNoOption());
        setAcceptedContractDefault(settings.isAcceptedContractDefault() ? TwinComboDataSourceHandler.getYesOption() : TwinComboDataSourceHandler.getNoOption());
        setTieBreakRatingRule(settings.getTieBreakRatingRule());
        setReenrollEnabled(settings.isReenrollByPriorityEnabled() ? TwinComboDataSourceHandler.getYesOption(): TwinComboDataSourceHandler.getNoOption());
        setExcludeEnrByLowPriority(settings.isExcludeEnrolledByLowPriority() ? TwinComboDataSourceHandler.getYesOption(): TwinComboDataSourceHandler.getNoOption());

        getOptionsGroups().clear();
        getValueMap().clear();
        Map<String, SettingsOptionGroup> optionsGroupMap = EnrSettingsManager.instance().settingsOptionsGroupExtPoint().getItems();
        if(optionsGroupMap != null && !optionsGroupMap.isEmpty())
        {
            List<SettingsOptionGroup> groups = Lists.newArrayList(optionsGroupMap.values());
            Collections.sort(groups, (o1, o2) ->
                    {
                        if (Objects.equals(o1.getPriority(), o2.getPriority()))
                        {
                            return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getTitle(), o2.getTitle());
                        }
                        return o1.getPriority() - o2.getPriority();
                    }
            );
            for (SettingsOptionGroup group : groups)
            {
                getOptionsGroups().add(group);
                SelectDataSource dataSource = new SelectDataSource(this, group.getKey());
                dataSource.setHandler(group.getHandler());
                _uiConfig.addDataSource(dataSource);
                Long value = (Long) settings.getProperty(group.getPropertyPath());
                if (value != null)
                {
                    getValueMap().put(group.getKey(), group.getItemList().getItem(value.toString()));
                }
            }
        }
    }

    public boolean isShowAccountingRulesAgreementBudget()
    {
        return !TwinComboDataSourceHandler.getSelectedValue(getEnrollmentRulesBudget());
    }

    public boolean isShowAccountingRulesAgreementContract()
    {
        return !EnrSettingsAdmissionRequirements.OPTION_ENROLL_RULES_CONTRACT_FIRST.equals(getEnrollmentRulesContract().getId());
    }

    public boolean isShowAcceptedContractDefault()
    {
        return isShowAccountingRulesAgreementContract() && getAccountingRulesAgreementContract() != null && !TwinComboDataSourceHandler.getSelectedValue(getAccountingRulesAgreementContract());
    }

    public DataWrapper getValue()
    {
        return getValueMap().get(getCurrentOptionsGroup().getKey());
    }

    public void setValue(DataWrapper value)
    {
        getValueMap().put(getCurrentOptionsGroup().getKey(), value);
    }

    public UIDataSource getCurrentOptionGroupDS()
    {
        return _uiConfig.getDataSource(getCurrentOptionsGroup().getKey());
    }

    public EnrEnrollmentCampaign getCampaign()
    {
        return EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getSettings().<EnrEnrollmentCampaign>get(PARAM_ENR_CAMPAIGN_DS));
        getSupport().setRefreshScheduled(true);
    }

    public boolean isNothingSelected()
    {
        return getSettings().get(PARAM_ENR_CAMPAIGN_DS) == null;
    }

    public void onClickApply()
    {
        validate();
        if(getUserContext().getErrorCollector().hasErrors())
            return;

        final EnrEnrollmentCampaignSettings settings = getSettings().<EnrEnrollmentCampaign>get(PARAM_ENR_CAMPAIGN_DS).getSettings();

        DataAccessServices.dao().doInTransaction(session -> {
            settings.setAccountingRulesOrigEduDoc(getAccountingRulesOrigEduDoc().getId());
            settings.setEnrollmentRulesBudget(getEnrollmentRulesBudget().getId());
            settings.setTieBreakRatingRule(getTieBreakRatingRule());
            settings.setReenrollByPriorityEnabled(TwinComboDataSourceHandler.getSelectedValueNotNull(getReenrollEnabled()));
            settings.setExcludeEnrolledByLowPriority(TwinComboDataSourceHandler.getSelectedValueNotNull(getExcludeEnrByLowPriority()));

            if(isShowAccountingRulesAgreementBudget())
                settings.setAccountingRulesAgreementBudget(getAccountingRulesAgreementBudget().getId());
            else
                settings.setAccountingRulesAgreementBudget(TwinComboDataSourceHandler.getYesOption().getId());

            settings.setEnrollmentRulesContract(getEnrollmentRulesContract().getId());

            if(isShowAccountingRulesAgreementContract())
                settings.setAccountingRulesAgreementContract(getAccountingRulesAgreementContract().getId());
            else
                settings.setAccountingRulesAgreementContract(TwinComboDataSourceHandler.getNoOption().getId());

            if(isShowAcceptedContractDefault())
                settings.setAcceptedContractDefault(TwinComboDataSourceHandler.getSelectedValueNotNull(getAcceptedContractDefault()));
            else
                settings.setAcceptedContractDefault(false);

            for(Map.Entry<String, DataWrapper> entry : getValueMap().entrySet())
            {
                SettingsOptionGroup optionsGroup = EnrSettingsManager.instance().settingsOptionsGroupExtPoint().getItems().get(entry.getKey());
                if(optionsGroup != null)
                {
                    settings.setProperty(optionsGroup.getPropertyPath(), entry.getValue().getId());
                }
            }
            session.saveOrUpdate(settings);
            return null;
        });
    }

    private void validate()
    {
        if(TwinComboDataSourceHandler.getSelectedValueNotNull(getAccountingRulesAgreementBudget()))
        {
            boolean accRuleBudgetError = EnrSettingsManager.instance().dao().checkAccountingRulesAgreementBudget(getCampaign());
            if(accRuleBudgetError)
                _uiSupport.error("Нельзя изменить настройку, т.к. в выбранной приемной кампании есть абитуриент у которого уже дано согласие на зачисление по нескольким выбранным конкурсам на места в рамках КЦП.");
        }

        if(TwinComboDataSourceHandler.getSelectedValueNotNull(getAccountingRulesAgreementContract()))
        {
            boolean accRuleContractError = EnrSettingsManager.instance().dao().checkAccountingRulesAgreementContract(getCampaign());
            if(accRuleContractError)
                _uiSupport.error("Нельзя изменить настройку, т.к. в выбранной приемной кампании есть абитуриент у которого уже дано согласие на зачисление по нескольким выбранным конкурсам на места по договору.");
        }
    }

    // getters and setters
    public Map<String, DataWrapper> getValueMap()
    {
        return _valueMap;
    }

    public List<SettingsOptionGroup> getOptionsGroups()
    {
        return _optionsGroups;
    }

    public void setOptionsGroups(List<SettingsOptionGroup> optionsGroups)
    {
        _optionsGroups = optionsGroups;
    }

    public SettingsOptionGroup getCurrentOptionsGroup()
    {
        return _currentOptionsGroup;
    }

    public void setCurrentOptionsGroup(SettingsOptionGroup currentOptionsGroup)
    {
        _currentOptionsGroup = currentOptionsGroup;
    }

    public DataWrapper getAccountingRulesOrigEduDoc()
    {
        return _accountingRulesOrigEduDoc;
    }

    public void setAccountingRulesOrigEduDoc(DataWrapper accountingRulesOrigEduDoc)
    {
        _accountingRulesOrigEduDoc = accountingRulesOrigEduDoc;
    }

    public DataWrapper getEnrollmentRulesBudget()
    {
        return _enrollmentRulesBudget;
    }

    public void setEnrollmentRulesBudget(DataWrapper enrollmentRulesBudget)
    {
        _enrollmentRulesBudget = enrollmentRulesBudget;
    }

    public DataWrapper getAccountingRulesAgreementBudget()
    {
        return _accountingRulesAgreementBudget;
    }

    public void setAccountingRulesAgreementBudget(DataWrapper accountingRulesAgreementBudget)
    {
        _accountingRulesAgreementBudget = accountingRulesAgreementBudget;
    }

    public DataWrapper getEnrollmentRulesContract()
    {
        return _enrollmentRulesContract;
    }

    public void setEnrollmentRulesContract(DataWrapper enrollmentRulesContract)
    {
        _enrollmentRulesContract = enrollmentRulesContract;
    }

    public DataWrapper getAccountingRulesAgreementContract()
    {
        return _accountingRulesAgreementContract;
    }

    public void setAccountingRulesAgreementContract(DataWrapper accountingRulesAgreementContract)
    {
        _accountingRulesAgreementContract = accountingRulesAgreementContract;
    }

    public DataWrapper getAcceptedContractDefault()
    {
        return _acceptedContractDefault;
    }

    public void setAcceptedContractDefault(DataWrapper acceptedContractDefault)
    {
        _acceptedContractDefault = acceptedContractDefault;
    }

    public EnrTieBreakRatingRule getTieBreakRatingRule()
    {
        return _tieBreakRatingRule;
    }

    public void setTieBreakRatingRule(EnrTieBreakRatingRule tieBreakRatingRule)
    {
        _tieBreakRatingRule = tieBreakRatingRule;
    }

    public DataWrapper getReenrollEnabled()
    {
        return _reenrollEnabled;
    }

    public void setReenrollEnabled(DataWrapper reenrollEnabled)
    {
        _reenrollEnabled = reenrollEnabled;
    }

    public DataWrapper getExcludeEnrByLowPriority()
    {
        return _excludeEnrByLowPriority;
    }

    public void setExcludeEnrByLowPriority(DataWrapper excludeEnrByLowPriority)
    {
        _excludeEnrByLowPriority = excludeEnrByLowPriority;
    }
}
