/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.SecTab;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.AccessMatrixEdit.SecAccessMatrixEditUI;
import org.tandemframework.shared.organization.sec.bo.Sec.util.BasePermissionGroupListUI;
import ru.tandemservice.unienr14.catalog.entity.codes.LocalRoleScopeCodes;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AccessMatrixEdit.EnrEnrollmentCampaignAccessMatrixEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AccessMatrixEdit.EnrEnrollmentCampaignAccessMatrixEditUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.LocalRoleAddEdit.EnrEnrollmentCampaignLocalRoleAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.LocalRoleAddEdit.EnrEnrollmentCampaignLocalRoleAddEditUI;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Map;

/**
 * @author oleyba
 * @since 5/19/15
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "enrollmentCampaign.id"),
    @Bind(key = OrgUnitViewUI.SELECTED_SUB_PAGE, binding = "selectedSubPage")
})
public class EnrEnrollmentCampaignSecTabUI extends BasePermissionGroupListUI
{
    public static final String TITLE = "title";
    public static final String ROLE_STATE = "roleState";

    private EnrEnrollmentCampaign _enrollmentCampaign = new EnrEnrollmentCampaign();
    private String selectedSubPage;

    @Override
    protected String getLocalRoleScopeCode()
    {
        return LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN;
    }

    @Override
    public String getAccessMatrixBC()
    {
        return EnrEnrollmentCampaignAccessMatrixEdit.class.getSimpleName();
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(DataAccessServices.dao().getNotNull(EnrEnrollmentCampaign.class, getEnrollmentCampaign().getId()));
        super.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(LocalRoleDSHandler.EC_ID, getEnrollmentCampaign().getId());
        dataSource.put(LocalRoleDSHandler.TITLE, _uiSettings.get(TITLE));
        dataSource.put(LocalRoleDSHandler.ROLE_STATE, _uiSettings.get(ROLE_STATE));
    }

    // Util

    @Override
    public Map<String, Object> getGroupLinkParams()
    {
        return ParametersMap
            .createWith(EnrEnrollmentCampaignAccessMatrixEditUI.BIND_ENROLLMENT_CAMPAIGN_ID, getEnrollmentCampaign().getId())
            .add(SecAccessMatrixEditUI.BIND_GROUP_NAME, getCurrentGroup().getName());
    }

    // Listeners

    public void onClickAddLocalRole()
    {
        _uiActivation.asRegionDialog(EnrEnrollmentCampaignLocalRoleAddEdit.class)
            .parameter(EnrEnrollmentCampaignLocalRoleAddEditUI.ENROLLMENT_CAMPAIGN_ID, getEnrollmentCampaign().getId())
            .activate();
    }

    // Getters & Setters


    public String getAddLocalRolePermissionKey()
    {
        return "menuEnr14Campaigns";
    }

    public String getEditPermissionKey()
    {
        return "menuEnr14Campaigns";
    }

    public String getDeletePermissionKey()
    {
        return "menuEnr14Campaigns";
    }

    public String getSelectedSubPage()
    {
        return selectedSubPage;
    }

    public void setSelectedSubPage(String selectedSubPage)
    {
        this.selectedSubPage = selectedSubPage;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}
