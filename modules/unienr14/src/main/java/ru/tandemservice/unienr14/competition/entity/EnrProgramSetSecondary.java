package ru.tandemservice.unienr14.competition.entity;

import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetSecondaryGen;

/**
 * Специальность (профессия) для приема на СПО
 *
 * Определяет ОП для приема СПО.
 */
public class EnrProgramSetSecondary extends EnrProgramSetSecondaryGen implements EnrProgramSetBase.ISingleExamSetOwner
{
    public EnrProgramSetSecondary()
    {
    }

    @Override
    public boolean isContractDividedByEduLevel()
    {
        return false;
    }

    @Override
    public boolean isValidExamSetVariants()
    {
        return true;
    }

    @Override
    public boolean isMinisterialDividedByEduLevel()
    {
        return false;
    }
}