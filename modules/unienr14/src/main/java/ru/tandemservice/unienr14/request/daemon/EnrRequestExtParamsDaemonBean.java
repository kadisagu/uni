package ru.tandemservice.unienr14.request.daemon;

import java.util.Collection;
import java.util.Date;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.IPropertyUpdateRule;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;

/**
 * TODO: перенести в базовый механизм вместе с IPropertyUpdateRule
 * @author vdanilov
 */
public class EnrRequestExtParamsDaemonBean extends UniBaseDao implements IEnrRequestExtParamsDaemonBean {

    public static final SyncDaemon DAEMON = new SyncDaemon(EnrRequestExtParamsDaemonBean.class.getName(), 120, EnrEntrantDaemonBean.DAEMON.getName()) {

        @Override
        protected void initDaemon()
        {
            final IDSetEventListener afterCompleteWakeUpListener = this.getAfterCompleteWakeUpListener();

            // признаки оригиналов
//            registerWakeUp(EnrEntrant.class);
//            registerWakeUp(EnrEntrantRequest.class);
//            registerWakeUp(EnrRequestedCompetition.class);

            registerWakeUp(EnrEnrollmentCampaignSettings.class,
                    EnrEnrollmentCampaignSettings.P_ENR_ENTRANT_CONTRACT_EXIST_CHECK_MODE,
                    EnrEnrollmentCampaignSettings.P_ACCOUNTING_RULES_ORIG_EDU_DOC,
                    EnrEnrollmentCampaignSettings.P_ENROLLMENT_RULES_BUDGET,
                    EnrEnrollmentCampaignSettings.P_ACCOUNTING_RULES_AGREEMENT_BUDGET,
                    EnrEnrollmentCampaignSettings.P_ENROLLMENT_RULES_CONTRACT,
                    EnrEnrollmentCampaignSettings.P_ACCOUNTING_RULES_AGREEMENT_CONTRACT);

            final Collection<IPropertyUpdateRule<Boolean>> rules = EnrRequestedCompetitionManager.instance().enrollmentAvailableRules().getItems().values();
            for (final IPropertyUpdateRule<Boolean> rule: rules) { rule.registerWakeUp(afterCompleteWakeUpListener); }
        }

        @Override
        protected void main()
        {
            final IEnrRequestExtParamsDaemonBean dao = IEnrRequestExtParamsDaemonBean.instance.get();
            try {
                dao.doUpdateEnrReqCompEnrollAvailableAndEduDoc();
            }
            catch (final Throwable t) { Debug.exception(t); }
        }
    };




    @Override
    public boolean doUpdateEnrReqCompEnrollAvailableAndEduDoc()
    {
        Debug.begin("doUpdateEnrReqCompEnrollAvailableAndEduDoc");
        try {
            int updates = 0;

            updates += EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDoc();

            return (updates > 0);
        } finally {
            Debug.end();
        }
    }

    private static void registerWakeUp(final Class<? extends IEntity> klass, String... affectedProperties) {
        EnrRequestExtParamsDaemonBean.DAEMON.registerWakeUpListener4Class(klass, affectedProperties);
    }
}
