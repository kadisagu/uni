/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.CopyForm;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.List.EnrEnrollmentCampaignListUI;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Calendar;
import java.util.Date;

/**
 * @author rsizonenko
 * @since 31.01.2015
 */
public class EnrEnrollmentCampaignCopyFormUI extends UIPresenter
{
    private EnrEnrollmentCampaign _srcEnrollmentCampaign;
    private EnrEnrollmentCampaign _newEnrollmentCampaign;

    @Override
    public void onComponentRefresh()
    {
        _newEnrollmentCampaign = new EnrEnrollmentCampaign();
        setSrcEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        onChangeSrcEnrollmentCampaign();
    }

    private Date setYear(Date date, int year)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    // Listeners

    public void onChangeSrcEnrollmentCampaign()
    {
        if (getSrcEnrollmentCampaign() != null) {
            getNewEnrollmentCampaign().setEducationYear(IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.intValue(), getSrcEnrollmentCampaign().getEducationYear().getIntValue() + 1));
        } else {
            getNewEnrollmentCampaign().setEducationYear(null);
        }
        onChangeEduYear();
    }

    public void onChangeEduYear()
    {
        final EducationYear educationYear = getNewEnrollmentCampaign().getEducationYear();
        if (educationYear != null) {
            getNewEnrollmentCampaign().setTitle(educationYear.getTitle());

            if (getSrcEnrollmentCampaign() != null) {
                getNewEnrollmentCampaign().setDateFrom(setYear(getSrcEnrollmentCampaign().getDateFrom(), educationYear.getIntValue()));
                getNewEnrollmentCampaign().setDateTo(setYear(getSrcEnrollmentCampaign().getDateTo(), educationYear.getIntValue()));
            }
        }
    }

    public void onClickApply()
    {
        final EnrEnrollmentCampaign newCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().copyEnrollmentCampaign(getSrcEnrollmentCampaign(), getNewEnrollmentCampaign());
        if (newCampaign == null) {
            return;
        }

        StringBuilder str = new StringBuilder();
        int voCountSrc = IUniBaseDao.instance.get().getCount(EnrProgramSetBS.class, EnrProgramSetBS.L_ENROLLMENT_CAMPAIGN, getSrcEnrollmentCampaign())
                + IUniBaseDao.instance.get().getCount(EnrProgramSetHigher.class, EnrProgramSetHigher.L_ENROLLMENT_CAMPAIGN, getSrcEnrollmentCampaign())
                + IUniBaseDao.instance.get().getCount(EnrProgramSetMaster.class, EnrProgramSetMaster.L_ENROLLMENT_CAMPAIGN, getSrcEnrollmentCampaign());
        int voCountNew = IUniBaseDao.instance.get().getCount(EnrProgramSetBS.class, EnrProgramSetBS.L_ENROLLMENT_CAMPAIGN, newCampaign)
                + IUniBaseDao.instance.get().getCount(EnrProgramSetHigher.class, EnrProgramSetHigher.L_ENROLLMENT_CAMPAIGN, newCampaign)
                + IUniBaseDao.instance.get().getCount(EnrProgramSetMaster.class, EnrProgramSetMaster.L_ENROLLMENT_CAMPAIGN, newCampaign);
        str.append("<div>скопировано ")
                .append(CommonBaseStringUtil.numberWithPostfixCase(voCountNew, "набор", "набора", "наборов"))
                .append(" образовательных программ ВО для приема, пропущено — ")
                .append(CommonBaseStringUtil.numberWithPostfixCase(voCountSrc - voCountNew, "набор", "набора", "наборов"))
                .append(";</div>");

        int secCountSrc = IUniBaseDao.instance.get().getCount(EnrProgramSetSecondary.class, EnrProgramSetSecondary.L_ENROLLMENT_CAMPAIGN, getSrcEnrollmentCampaign());
        int secCountNew = IUniBaseDao.instance.get().getCount(EnrProgramSetSecondary.class, EnrProgramSetSecondary.L_ENROLLMENT_CAMPAIGN, newCampaign);
        str.append("<div>скопировано ")
                .append(CommonBaseStringUtil.numberWithPostfixCase(secCountNew, "специальность  (профессия)", "специальности  (профессии)", "специальностей  (профессий)"))
                .append(" СПО для приема, пропущено — ")
                .append(CommonBaseStringUtil.numberWithPostfixCase(secCountSrc - secCountNew, "специальность  (профессия)", "специальности  (профессии)", "специальностей  (профессий)"))
                .append(".</div>");

        deactivate(ParametersMap.createWith(EnrEnrollmentCampaignListUI.BIND_AFTER_COPY_INFO, str.toString()));
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getNewEnrollmentCampaign()
    {
        return _newEnrollmentCampaign;
    }

    public EnrEnrollmentCampaign getSrcEnrollmentCampaign()
    {
        return _srcEnrollmentCampaign;
    }

    public void setSrcEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _srcEnrollmentCampaign = enrollmentCampaign;
    }
}