/**
 *$Id: EnrEnrollmentOrderBasicDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import org.tandemframework.hibsupport.dao.MergeAction;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel;
import ru.tandemservice.unienr14.settings.entity.gen.EnrOrderReasonToBasicsRelGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
public class EnrEnrollmentOrderBasicDao extends UniBaseDao implements IEnrEnrollmentOrderBasicDao
{
    @Override
    public void updateOrderReasonToBasicsRel(long orderReasonId, Collection<Long> orderBasicIds)
    {
        final EnrOrderReason reason = getNotNull(EnrOrderReason.class, orderReasonId);
        final List<EnrOrderBasic> basicList = getList(EnrOrderBasic.class, EnrOrderBasic.id(), orderBasicIds);

        List<EnrOrderReasonToBasicsRel> targetRelList = new ArrayList<>();
        for (EnrOrderBasic basic : basicList)
            targetRelList.add(new EnrOrderReasonToBasicsRel(reason, basic));

        new MergeAction.SessionMergeAction<EnrOrderReasonToBasicsRel.NaturalId, EnrOrderReasonToBasicsRel>()
        {

            @Override
            protected EnrOrderReasonToBasicsRelGen.NaturalId key(EnrOrderReasonToBasicsRel source)
            {
                return (EnrOrderReasonToBasicsRelGen.NaturalId) source.getNaturalId();
            }

            @Override
            protected EnrOrderReasonToBasicsRel buildRow(EnrOrderReasonToBasicsRel source)
            {
                return new EnrOrderReasonToBasicsRel(source.getReason(), source.getBasic());
            }

            @Override
            protected void fill(EnrOrderReasonToBasicsRel target, EnrOrderReasonToBasicsRel source)
            {

            }
        }
                .merge(getList(EnrOrderReasonToBasicsRel.class, EnrOrderReasonToBasicsRel.reason(), reason), targetRelList);
    }
}
