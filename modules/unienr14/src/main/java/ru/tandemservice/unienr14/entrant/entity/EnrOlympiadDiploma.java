package ru.tandemservice.unienr14.entrant.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadType;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrOlympiadDiplomaGen;

/**
 * Диплом участника олимпиады
 */
public class EnrOlympiadDiploma extends EnrOlympiadDiplomaGen
{
    @Override
    @EntityDSLSupport(parts = {EnrOlympiadDiploma.P_SERIA, EnrOlympiadDiploma.P_NUMBER})
    public String getTitle()
    {
        if (getDocumentType() == null) {
            return this.getClass().getSimpleName();
        }
        return UniStringUtils.joinWithSeparator(": ", getDocumentType().getShortTitle(), UniStringUtils.joinWithSeparator(" ", getSeria(), getNumber()));
    }

    @Override
    @EntityDSLSupport(parts = {EnrOlympiadDiploma.P_SERIA, EnrOlympiadDiploma.P_NUMBER})
    public String getExtendedTitle() {
        return UniStringUtils.joinWithSeparator(", ", getTitle(), getInfoString());
    }

    @Override
    @Deprecated
    @EntityDSLSupport(parts = {EnrOlympiadDiploma.P_SERIA, EnrOlympiadDiploma.P_NUMBER})
    public String getFullTitle() {
        return getExtendedTitle();
    }

    @Override
    @EntityDSLSupport(parts = {EnrOlympiadDiploma.P_SERIA, EnrOlympiadDiploma.P_NUMBER})
    public String getSeriaAndNumber() {
        return UniStringUtils.joinWithSeparator(" ", getSeria(), getNumber());
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    @EntityDSLSupport
    public String getDisplayableTitle()
    {
        StringBuilder result = new StringBuilder()
        .append("Диплом олимпиады - ");

        if (this.getSeria() != null)
            result.append(this.getSeria()).append(" ");

        result
        .append(this.getNumber()).append(" (")
        .append(this.getOlympiad().getOlympiadType().getShortTitle()).append(", ")
        .append(this.getSubject().getTitle());
        if (this.getIssuanceDate() != null) {
            result.append(", ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getIssuanceDate()));
        }
        result.append(")");

        return result.toString();
    }

//    @Override
//    public String getFisUidTitle()
//    {
//        return getTitle() + ", " + getEntrant().getFullFio();
//    }

    /**
     * DEV-2878
     * @return информация о документе
     */
    public String getInfoString()
    {
        // <степень отличия>, «<название олимпиады>» (<предмет олимпиады>)
        StringBuilder builder = new StringBuilder();
        builder.append(StringUtils.uncapitalize(getHonour().getTitle())).append(", «");
        builder.append(getOlympiad().getTitle());
        builder.append("» (").append(getSubject().getTitle()).append(")");
        return builder.toString();
    }

    public EnrOlympiadType getOlympiadType() {
        return getOlympiad() == null ? null : getOlympiad().getOlympiadType();
    }
}