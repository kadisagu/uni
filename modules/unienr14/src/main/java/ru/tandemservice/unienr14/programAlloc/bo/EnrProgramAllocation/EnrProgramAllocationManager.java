/* $Id: EcProfileDistributionManager.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.EnrProgramAllocationDao;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.IEnrProgramAllocationDao;

/**
 * @author Vasily Zhukov
 * @since 05.05.2012
 */
@Configuration
public class EnrProgramAllocationManager extends BusinessObjectManager
{
    public static EnrProgramAllocationManager instance()
    {
        return instance(EnrProgramAllocationManager.class);
    }

    @Bean
    public IEnrProgramAllocationDao dao()
    {
        return new EnrProgramAllocationDao();
    }

    public static final DataWrapper DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS = new DataWrapper(0L, "Студент/Слушатель");
    public static final DataWrapper DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION = new DataWrapper(1L, "Второе высшее");
}
