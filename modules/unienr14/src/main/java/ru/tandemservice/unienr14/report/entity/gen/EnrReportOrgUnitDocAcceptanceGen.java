package ru.tandemservice.unienr14.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ход приема документов по подразделениям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrReportOrgUnitDocAcceptanceGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance";
    public static final String ENTITY_NAME = "enrReportOrgUnitDocAcceptance";
    public static final int VERSION_HASH = 374298415;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления добавлены с
    private Date _dateTo;     // Заявления добавлены по
    private String _requestType;     // Вид заявления
    private String _compensationType;     // Вид возмещения затрат
    private String _programForm;     // Форма обучения
    private String _enrOrgUnit;     // Филиал
    private String _formativeOrgUnit;     // Формирующее подр.

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления добавлены с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления добавлены по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид заявления.
     */
    @Length(max=255)
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Филиал.
     */
    public String getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал.
     */
    public void setEnrOrgUnit(String enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrReportOrgUnitDocAcceptanceGen)
        {
            setEnrollmentCampaign(((EnrReportOrgUnitDocAcceptance)another).getEnrollmentCampaign());
            setDateFrom(((EnrReportOrgUnitDocAcceptance)another).getDateFrom());
            setDateTo(((EnrReportOrgUnitDocAcceptance)another).getDateTo());
            setRequestType(((EnrReportOrgUnitDocAcceptance)another).getRequestType());
            setCompensationType(((EnrReportOrgUnitDocAcceptance)another).getCompensationType());
            setProgramForm(((EnrReportOrgUnitDocAcceptance)another).getProgramForm());
            setEnrOrgUnit(((EnrReportOrgUnitDocAcceptance)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrReportOrgUnitDocAcceptance)another).getFormativeOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrReportOrgUnitDocAcceptanceGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrReportOrgUnitDocAcceptance.class;
        }

        public T newInstance()
        {
            return (T) new EnrReportOrgUnitDocAcceptance();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "requestType":
                    return obj.getRequestType();
                case "compensationType":
                    return obj.getCompensationType();
                case "programForm":
                    return obj.getProgramForm();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "requestType":
                        return true;
                case "compensationType":
                        return true;
                case "programForm":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "requestType":
                    return true;
                case "compensationType":
                    return true;
                case "programForm":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "requestType":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "programForm":
                    return String.class;
                case "enrOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrReportOrgUnitDocAcceptance> _dslPath = new Path<EnrReportOrgUnitDocAcceptance>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrReportOrgUnitDocAcceptance");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getEnrOrgUnit()
     */
    public static PropertyPath<String> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    public static class Path<E extends EnrReportOrgUnitDocAcceptance> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _enrOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrReportOrgUnitDocAcceptanceGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrReportOrgUnitDocAcceptanceGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(EnrReportOrgUnitDocAcceptanceGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(EnrReportOrgUnitDocAcceptanceGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EnrReportOrgUnitDocAcceptanceGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getEnrOrgUnit()
     */
        public PropertyPath<String> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new PropertyPath<String>(EnrReportOrgUnitDocAcceptanceGen.P_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportOrgUnitDocAcceptance#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EnrReportOrgUnitDocAcceptanceGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

        public Class getEntityClass()
        {
            return EnrReportOrgUnitDocAcceptance.class;
        }

        public String getEntityName()
        {
            return "enrReportOrgUnitDocAcceptance";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
