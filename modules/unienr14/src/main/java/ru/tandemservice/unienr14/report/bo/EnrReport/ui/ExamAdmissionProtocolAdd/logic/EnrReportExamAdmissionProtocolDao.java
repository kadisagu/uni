/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamAdmissionProtocolAdd.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.node.RtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrExamTypeGen;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamAdmissionProtocolAdd.EnrReportExamAdmissionProtocolAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportExamAdmissionProtocol;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public class EnrReportExamAdmissionProtocolDao extends UniBaseDao implements IEnrReportExamAdmissionProtocolDao
{
    @Override
    public long createReport(EnrReportExamAdmissionProtocolAddUI model) {

        EnrReportExamAdmissionProtocol report = new EnrReportExamAdmissionProtocol();
        
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportExamAdmissionProtocol.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportExamAdmissionProtocol.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportExamAdmissionProtocol.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportExamAdmissionProtocol.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportExamAdmissionProtocol.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportExamAdmissionProtocol.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportExamAdmissionProtocol.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportExamAdmissionProtocol.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportExamAdmissionProtocol.P_PROGRAM_SET, "title");

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportExamAdmissionProtocol.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }
    
    private byte[] buildReport(EnrReportExamAdmissionProtocolAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_EXAM_ADMISSION_PROTOCOL);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        // Убираем отозванные заявления
        requestedCompDQL
                .where(eq(property("request", EnrEntrantRequest.takeAwayDocument()), value(Boolean.FALSE)));

        //Убираем выбывших из конкурса
        requestedCompDQL.where(ne(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));

        requestedCompDQL
            .fromEntity(EnrChosenEntranceExamForm.class, "vvi")
            .where(eq(property("vvi", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()), property("reqComp", EnrRequestedCompetition.id())))
            .where(eq(property("vvi", EnrChosenEntranceExamForm.passForm().internal()), value(Boolean.TRUE)))
            .where(isNotNull(property("vvi", EnrChosenEntranceExamForm.chosenEntranceExam().actualExam())));

        RtfTable tTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfElement disciplineElement = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "discipline");

        requestedCompDQL.column(property("vvi"));

        document.getElementList().remove(tTableElement);
        document.getElementList().remove(disciplineElement);

        // Компараторы для сортировки
        final Comparator<EnrChosenEntranceExamForm> examComparator = CommonCollator.comparing(o -> o.getChosenEntranceExam().getRequestedCompetition().getRequest().getIdentityCard().getFullFio(), true);
        final Comparator<EnrExamType> examTypeComparator = CommonCollator.comparing(EnrExamTypeGen::getShortTitle);

        List<EnrChosenEntranceExamForm> vviList = getList(requestedCompDQL);

        // Заполняем структуру данных
        Map<EnrCampaignDiscipline, Map<EnrExamPassForm, Map<EnrExamType, Set<EnrChosenEntranceExamForm>>>> dataMap = new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR);


        for (EnrChosenEntranceExamForm examForm : vviList)
        {
            EnrChosenEntranceExam exam = examForm.getChosenEntranceExam();
            EnrCampaignDiscipline discipline = exam.getDiscipline();
            EnrExamPassForm passForm = examForm.getPassForm();
            EnrExamType examType = exam.getActualExam().getExamSetElement().getType();

            Map<EnrExamPassForm, Map<EnrExamType, Set<EnrChosenEntranceExamForm>>> examPassFormMap = SafeMap.safeGet(dataMap, discipline, enrCampaignDiscipline -> new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR));
            Map<EnrExamType, Set<EnrChosenEntranceExamForm>> examTypeMap = SafeMap.safeGet(examPassFormMap, passForm, enrExamPassForm -> new TreeMap<>(examTypeComparator));
            SafeMap.safeGet(examTypeMap, examType, enrExamType -> new TreeSet<>(examComparator)).add(examForm);
        }

        // Переносим в документ
        for (Map.Entry<EnrCampaignDiscipline, Map<EnrExamPassForm, Map<EnrExamType, Set<EnrChosenEntranceExamForm>>>> dataEntry : dataMap.entrySet())
        {
            for (Map.Entry<EnrExamPassForm, Map<EnrExamType, Set<EnrChosenEntranceExamForm>>> examEntry : dataEntry.getValue().entrySet())
            {
                for (Map.Entry<EnrExamType, Set<EnrChosenEntranceExamForm>> typeEntry : examEntry.getValue().entrySet()) {
                    document.getElementList().add(disciplineElement.getClone());
                    StringBuilder builder = new StringBuilder();
                    builder.append("По вступительному испытанию: ").append(dataEntry.getKey().getTitle()).append(" (").append(examEntry.getKey().getTitle());
                    if (!typeEntry.getKey().getCode().equals(EnrExamTypeCodes.USUAL))
                        builder.append(", ").append(typeEntry.getKey().getTitle().toLowerCase()).append(")");
                    else builder.append(")");
                    new RtfInjectModifier().put("discipline", builder.toString()).modify(document);
                    Set<CoreCollectionUtils.Pair<EnrEntrant, String>> tableSet = new LinkedHashSet<>();
                    for (EnrChosenEntranceExamForm examForm : typeEntry.getValue()) {

                        EnrChosenEntranceExam exam = examForm.getChosenEntranceExam();
                        EnrEntrantRequest request = exam.getRequestedCompetition().getRequest();

                        String reason = "";
                        if (!exam.getActualExam().getExamSetElement().getType().isUsual())
                            reason = "ВИ " + exam.getActualExam().getExamSetElement().getType().getTitle().toLowerCase();
                        else if (!EnrRequestTypeCodes.BS.equals(request.getType().getCode()))
                            switch (request.getType().getCode()) {
                                case EnrRequestTypeCodes.MASTER:
                                    reason = "лицо, поступающее в магистратуру"; break;
                                case EnrRequestTypeCodes.HIGHER:
                                case EnrRequestTypeCodes.POSTGRADUATE:
                                    reason = "лицо, поступающее в аспирантуру"; break;
                                case EnrRequestTypeCodes.TRAINEESHIP:
                                    reason = "лицо, поступающее в ординатуру"; break;
                                case EnrRequestTypeCodes.INTERNSHIP:
                                    reason = "лицо, поступающее в интернатуру"; break;
                                case EnrRequestTypeCodes.SPO:
                                    reason = "лицо, поступающее для получения СПО"; break;
                            }
                        else if (request.getEduDocument().getEduLevel().getCode().contains(EduLevelCodes.PROFESSIONALNOE_OBRAZOVANIE))
                            reason = "лицо, поступающее на базе профессионального образования";
                        else if (request.getInternalExamReason() != null)
                            reason = request.getInternalExamReason().getTitle();
                        else reason = "\\cf1не указано основание";
                        tableSet.add(new CoreCollectionUtils.Pair<>(request.getEntrant(), reason));
                    }

                    List<String[]> tTable = new ArrayList<>();
                    int number = 1;
                    for (CoreCollectionUtils.Pair<EnrEntrant, String> item : tableSet)
                    {
                        String[] row = new String[3];
                        row[0] = String.valueOf(number++);
                        row[1] = item.getX().getPerson().getFullFio();
                        row[2]=item.getY();
                        tTable.add(row);
                    }


                    // Модифицируем документ
                    document.getElementList().add(tTableElement.getClone());
                    RtfTableModifier modifier = new RtfTableModifier();
                    modifier.put("T", tTable.toArray(new String[tTable.size()][]));
                    modifier.put("T", new RtfRowIntercepterRawText());
                    modifier.modify(document);
                }
            }
        }
        // Для красного цвета текста нужно добавлять свой колор-тэйбл, потому что родной почему-то трется из хедэра документа.
        RtfText text = new RtfText();
        text.setString("{\\colortbl;\\red255\\green0\\blue0;}");
        text.setRaw(true);
        document.getElementList().add(0, text);

        return RtfUtil.toByteArray(document);
    }
}
