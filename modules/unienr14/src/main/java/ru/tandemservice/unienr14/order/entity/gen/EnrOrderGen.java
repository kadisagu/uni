package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ на абитуриентов (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrderGen extends EnrAbstractOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrOrder";
    public static final String ENTITY_NAME = "enrOrder";
    public static final int VERSION_HASH = 1910590130;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_TYPE = "type";
    public static final String L_PRINT_FORM_TYPE = "printFormType";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_REASON = "reason";
    public static final String L_BASIC = "basic";
    public static final String P_ORDER_BASIC_TEXT = "orderBasicText";
    public static final String P_ORDER_TEXT = "orderText";
    public static final String L_PRINT_FORM = "printForm";
    public static final String P_DATE_AND_NUMBER = "dateAndNumber";
    public static final String P_INFO = "info";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_ORDER = "titleWithOrder";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EnrOrderType _type;     // Тип приказа
    private EnrOrderPrintFormType _printFormType;     // Вид печатной формы
    private EnrRequestType _requestType;     // Вид заявления
    private CompensationType _compensationType;     // Вид возмещения затрат
    private EnrOrderReason _reason;     // Причина приказа
    private EnrOrderBasic _basic;     // Основание приказа
    private String _orderBasicText;     // Текст основания приказа
    private String _orderText;     // Формулировка приказа
    private DatabaseFile _printForm;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Свойство не может быть null.
     */
    public void setType(EnrOrderType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Вид печатной формы. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderPrintFormType getPrintFormType()
    {
        return _printFormType;
    }

    /**
     * @param printFormType Вид печатной формы. Свойство не может быть null.
     */
    public void setPrintFormType(EnrOrderPrintFormType printFormType)
    {
        dirty(_printFormType, printFormType);
        _printFormType = printFormType;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Причина приказа.
     */
    public EnrOrderReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина приказа.
     */
    public void setReason(EnrOrderReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Основание приказа.
     */
    public EnrOrderBasic getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание приказа.
     */
    public void setBasic(EnrOrderBasic basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Текст основания приказа.
     */
    @Length(max=2000)
    public String getOrderBasicText()
    {
        return _orderBasicText;
    }

    /**
     * @param orderBasicText Текст основания приказа.
     */
    public void setOrderBasicText(String orderBasicText)
    {
        dirty(_orderBasicText, orderBasicText);
        _orderBasicText = orderBasicText;
    }

    /**
     * @return Формулировка приказа.
     */
    @Length(max=2000)
    public String getOrderText()
    {
        return _orderText;
    }

    /**
     * @param orderText Формулировка приказа.
     */
    public void setOrderText(String orderText)
    {
        dirty(_orderText, orderText);
        _orderText = orderText;
    }

    /**
     * @return Сохраненная печатная форма.
     */
    public DatabaseFile getPrintForm()
    {
        return _printForm;
    }

    /**
     * @param printForm Сохраненная печатная форма.
     */
    public void setPrintForm(DatabaseFile printForm)
    {
        dirty(_printForm, printForm);
        _printForm = printForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrOrderGen)
        {
            setEnrollmentCampaign(((EnrOrder)another).getEnrollmentCampaign());
            setType(((EnrOrder)another).getType());
            setPrintFormType(((EnrOrder)another).getPrintFormType());
            setRequestType(((EnrOrder)another).getRequestType());
            setCompensationType(((EnrOrder)another).getCompensationType());
            setReason(((EnrOrder)another).getReason());
            setBasic(((EnrOrder)another).getBasic());
            setOrderBasicText(((EnrOrder)another).getOrderBasicText());
            setOrderText(((EnrOrder)another).getOrderText());
            setPrintForm(((EnrOrder)another).getPrintForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrderGen> extends EnrAbstractOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrder.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "type":
                    return obj.getType();
                case "printFormType":
                    return obj.getPrintFormType();
                case "requestType":
                    return obj.getRequestType();
                case "compensationType":
                    return obj.getCompensationType();
                case "reason":
                    return obj.getReason();
                case "basic":
                    return obj.getBasic();
                case "orderBasicText":
                    return obj.getOrderBasicText();
                case "orderText":
                    return obj.getOrderText();
                case "printForm":
                    return obj.getPrintForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "type":
                    obj.setType((EnrOrderType) value);
                    return;
                case "printFormType":
                    obj.setPrintFormType((EnrOrderPrintFormType) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "reason":
                    obj.setReason((EnrOrderReason) value);
                    return;
                case "basic":
                    obj.setBasic((EnrOrderBasic) value);
                    return;
                case "orderBasicText":
                    obj.setOrderBasicText((String) value);
                    return;
                case "orderText":
                    obj.setOrderText((String) value);
                    return;
                case "printForm":
                    obj.setPrintForm((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "type":
                        return true;
                case "printFormType":
                        return true;
                case "requestType":
                        return true;
                case "compensationType":
                        return true;
                case "reason":
                        return true;
                case "basic":
                        return true;
                case "orderBasicText":
                        return true;
                case "orderText":
                        return true;
                case "printForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "type":
                    return true;
                case "printFormType":
                    return true;
                case "requestType":
                    return true;
                case "compensationType":
                    return true;
                case "reason":
                    return true;
                case "basic":
                    return true;
                case "orderBasicText":
                    return true;
                case "orderText":
                    return true;
                case "printForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "type":
                    return EnrOrderType.class;
                case "printFormType":
                    return EnrOrderPrintFormType.class;
                case "requestType":
                    return EnrRequestType.class;
                case "compensationType":
                    return CompensationType.class;
                case "reason":
                    return EnrOrderReason.class;
                case "basic":
                    return EnrOrderBasic.class;
                case "orderBasicText":
                    return String.class;
                case "orderText":
                    return String.class;
                case "printForm":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrder> _dslPath = new Path<EnrOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrder");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getType()
     */
    public static EnrOrderType.Path<EnrOrderType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Вид печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getPrintFormType()
     */
    public static EnrOrderPrintFormType.Path<EnrOrderPrintFormType> printFormType()
    {
        return _dslPath.printFormType();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Причина приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getReason()
     */
    public static EnrOrderReason.Path<EnrOrderReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Основание приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getBasic()
     */
    public static EnrOrderBasic.Path<EnrOrderBasic> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Текст основания приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getOrderBasicText()
     */
    public static PropertyPath<String> orderBasicText()
    {
        return _dslPath.orderBasicText();
    }

    /**
     * @return Формулировка приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getOrderText()
     */
    public static PropertyPath<String> orderText()
    {
        return _dslPath.orderText();
    }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getPrintForm()
     */
    public static DatabaseFile.Path<DatabaseFile> printForm()
    {
        return _dslPath.printForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getDateAndNumber()
     */
    public static SupportedPropertyPath<String> dateAndNumber()
    {
        return _dslPath.dateAndNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getInfo()
     */
    public static SupportedPropertyPath<String> info()
    {
        return _dslPath.info();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getTitleWithOrder()
     */
    public static SupportedPropertyPath<String> titleWithOrder()
    {
        return _dslPath.titleWithOrder();
    }

    public static class Path<E extends EnrOrder> extends EnrAbstractOrder.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EnrOrderType.Path<EnrOrderType> _type;
        private EnrOrderPrintFormType.Path<EnrOrderPrintFormType> _printFormType;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private CompensationType.Path<CompensationType> _compensationType;
        private EnrOrderReason.Path<EnrOrderReason> _reason;
        private EnrOrderBasic.Path<EnrOrderBasic> _basic;
        private PropertyPath<String> _orderBasicText;
        private PropertyPath<String> _orderText;
        private DatabaseFile.Path<DatabaseFile> _printForm;
        private SupportedPropertyPath<String> _dateAndNumber;
        private SupportedPropertyPath<String> _info;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithOrder;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getType()
     */
        public EnrOrderType.Path<EnrOrderType> type()
        {
            if(_type == null )
                _type = new EnrOrderType.Path<EnrOrderType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Вид печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getPrintFormType()
     */
        public EnrOrderPrintFormType.Path<EnrOrderPrintFormType> printFormType()
        {
            if(_printFormType == null )
                _printFormType = new EnrOrderPrintFormType.Path<EnrOrderPrintFormType>(L_PRINT_FORM_TYPE, this);
            return _printFormType;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Причина приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getReason()
     */
        public EnrOrderReason.Path<EnrOrderReason> reason()
        {
            if(_reason == null )
                _reason = new EnrOrderReason.Path<EnrOrderReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Основание приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getBasic()
     */
        public EnrOrderBasic.Path<EnrOrderBasic> basic()
        {
            if(_basic == null )
                _basic = new EnrOrderBasic.Path<EnrOrderBasic>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Текст основания приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getOrderBasicText()
     */
        public PropertyPath<String> orderBasicText()
        {
            if(_orderBasicText == null )
                _orderBasicText = new PropertyPath<String>(EnrOrderGen.P_ORDER_BASIC_TEXT, this);
            return _orderBasicText;
        }

    /**
     * @return Формулировка приказа.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getOrderText()
     */
        public PropertyPath<String> orderText()
        {
            if(_orderText == null )
                _orderText = new PropertyPath<String>(EnrOrderGen.P_ORDER_TEXT, this);
            return _orderText;
        }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getPrintForm()
     */
        public DatabaseFile.Path<DatabaseFile> printForm()
        {
            if(_printForm == null )
                _printForm = new DatabaseFile.Path<DatabaseFile>(L_PRINT_FORM, this);
            return _printForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getDateAndNumber()
     */
        public SupportedPropertyPath<String> dateAndNumber()
        {
            if(_dateAndNumber == null )
                _dateAndNumber = new SupportedPropertyPath<String>(EnrOrderGen.P_DATE_AND_NUMBER, this);
            return _dateAndNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getInfo()
     */
        public SupportedPropertyPath<String> info()
        {
            if(_info == null )
                _info = new SupportedPropertyPath<String>(EnrOrderGen.P_INFO, this);
            return _info;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrOrderGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrOrder#getTitleWithOrder()
     */
        public SupportedPropertyPath<String> titleWithOrder()
        {
            if(_titleWithOrder == null )
                _titleWithOrder = new SupportedPropertyPath<String>(EnrOrderGen.P_TITLE_WITH_ORDER, this);
            return _titleWithOrder;
        }

        public Class getEntityClass()
        {
            return EnrOrder.class;
        }

        public String getEntityName()
        {
            return "enrOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDateAndNumber();

    public abstract String getInfo();

    public abstract String getTitle();

    public abstract String getTitleWithOrder();
}
