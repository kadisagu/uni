/* $Id$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfigBuilder;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportParallelSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Calendar;
import java.util.Date;

/**
 * @author nvankov
 * @since 8/19/14
 */
public class EnrReportAdmissionResultsByEnrollmentStagesAddUI extends UIPresenter
{
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrReportParallelSelector parallelSelector = new EnrReportParallelSelector();
    private EnrEnrollmentCampaign enrollmentCampaign;

    private long reportId;

    private Date _firstStageDateFrom;
    private Date _firstStageDateTo;

    private Date _secondStageDateFrom;
    private Date _secondStageDateTo;

    private Date _extStageDateFrom;
    private Date _extStageDateTo;

    @Override
    public void onComponentPrepareRender()
    {
        if (reportId != 0)
        {
            activatePublisher();
            reportId = 0L;
        }
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        Calendar c = Calendar.getInstance();
        c.setTime(getEnrollmentCampaign().getDateFrom());
        int year = c.get(Calendar.YEAR);


        if(_firstStageDateFrom == null)
        {
            c.set(year, 6, 25, 0, 0, 0);
            _firstStageDateFrom = c.getTime();

        }
        if(_firstStageDateTo == null)
        {
            c.set(year, 7, 5, 0, 0, 0);
            _firstStageDateTo = c.getTime();
        }

        if(_secondStageDateFrom == null)
        {
            c.set(year, 7, 6, 0, 0, 0);
            _secondStageDateFrom = c.getTime();
        }
        if(_secondStageDateTo == null)
        {
            c.set(year, 7, 11, 0, 0, 0);
            _secondStageDateTo = c.getTime();
        }

        if(_extStageDateFrom == null)
        {
            c.set(year, 7, 12, 0, 0, 0);
            _extStageDateFrom = c.getTime();
        }
        if(_extStageDateTo == null)
        {
            c.set(year, 8, 1, 0, 0, 0);
            _extStageDateTo = c.getTime();
        }

        configUtil(getCompetitionFilterAddon());
    }

    public void onClickApply() {
        validate();

        setReportId(EnrReportManager.instance().admissionResultsByEnrollmentStagesDao().createReport(EnrReportAdmissionResultsByEnrollmentStagesAddUI.this));
//        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase()
//        {
//            @Override
//            public ProcessResult run(final ProcessState state)
//            {
//                setReportId(EnrReportManager.instance().admissionResultsByEnrollmentStagesDao().createReport(EnrReportAdmissionResultsByEnrollmentStagesAddUI.this));
//                return null;
//            }
//        }, ProcessDisplayMode.unknown));
    }

    private void validate()
    {
        if(_firstStageDateFrom.after(_firstStageDateTo))
            _uiSupport.error("Дата начала этапа должна быть меньше даты окончания этапа." , "firstStageDateFrom", "firstStageDateTo");

        if(_secondStageDateFrom.after(_secondStageDateTo))
            _uiSupport.error("Дата начала этапа должна быть меньше даты окончания этапа." , "secondStageDateFrom", "secondStageDateTo");

        if(_extStageDateFrom.after(_extStageDateTo))
            _uiSupport.error("Дата начала этапа должна быть меньше даты окончания этапа." , "extStageDateFrom", "extStageDateTo");

        if(_firstStageDateTo.after(_secondStageDateFrom))
            _uiSupport.error("Дата окончания предыдущего этапа должна быть меньше даты начала последующего этапа." , "firstStageDateFrom", "firstStageDateTo", "secondStageDateFrom", "secondStageDateTo");

        if(_secondStageDateTo.after(_extStageDateFrom))
            _uiSupport.error("Дата окончания предыдущего этапа должна быть меньше даты начала последующего этапа." , "extStageDateFrom", "extStageDateTo", "secondStageDateFrom", "secondStageDateTo");

        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public void activatePublisher()
    {
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
        deactivate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        dateSelector.refreshDates(getEnrollmentCampaign());
    }

    // utils

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfigBuilder().required(true).enableCheckboxChecked(true).enableCheckboxDisabled(true).create())
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, new CommonFilterFormConfigBuilder().required(true).enableCheckboxChecked(true).enableCheckboxDisabled(true).create())
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, new CommonFilterFormConfigBuilder().required(true).enableCheckboxChecked(true).enableCheckboxDisabled(true).create())
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters


    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrReportParallelSelector getParallelSelector() {
        return parallelSelector;
    }

    public Date getFirstStageDateFrom()
    {
        return _firstStageDateFrom;
    }

    public void setFirstStageDateFrom(Date firstStageDateFrom)
    {
        _firstStageDateFrom = firstStageDateFrom;
    }

    public Date getFirstStageDateTo()
    {
        return _firstStageDateTo;
    }

    public void setFirstStageDateTo(Date firstStageDateTo)
    {
        _firstStageDateTo = firstStageDateTo;
    }

    public Date getSecondStageDateFrom()
    {
        return _secondStageDateFrom;
    }

    public void setSecondStageDateFrom(Date secondStageDateFrom)
    {
        _secondStageDateFrom = secondStageDateFrom;
    }

    public Date getSecondStageDateTo()
    {
        return _secondStageDateTo;
    }

    public void setSecondStageDateTo(Date secondStageDateTo)
    {
        _secondStageDateTo = secondStageDateTo;
    }

    public Date getExtStageDateFrom()
    {
        return _extStageDateFrom;
    }

    public void setExtStageDateFrom(Date extStageDateFrom)
    {
        _extStageDateFrom = extStageDateFrom;
    }

    public Date getExtStageDateTo()
    {
        return _extStageDateTo;
    }

    public void setExtStageDateTo(Date extStageDateTo)
    {
        _extStageDateTo = extStageDateTo;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}
