package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x4_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("enr14_program_alloc_t")) return;

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramAllocation

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_program_alloc_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("programsetorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("formingdate_p", DBType.TIMESTAMP).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrProgramAllocation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramAllocationItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_program_alloc_item_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("allocation_id", DBType.LONG).setNullable(false), 
				new DBColumn("entrant_id", DBType.LONG).setNullable(false), 
				new DBColumn("programsetitem_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrProgramAllocationItem");

		}


    }
}