/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.EnrEnrollmentStepDao;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.IEnrEnrollmentStepDao;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionDao;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrSelectionDao;

/**
 * @author oleyba
 * @since 12/17/13
 */
@Configuration
public class EnrEnrollmentStepManager extends BusinessObjectManager
{
    public static EnrEnrollmentStepManager instance() {
        return instance(EnrEnrollmentStepManager.class);
    }

    @Bean
    public IEnrEnrollmentStepDao dao() {
        return new EnrEnrollmentStepDao();
    }

    @Bean
    public IEnrSelectionDao selectionDao() {
        return new EnrSelectionDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentStepKindDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EnrEnrollmentStepKind.class)
            .where(EnrEnrollmentStepKind.allowed(), Boolean.TRUE)
            .order(EnrEnrollmentStepKind.code())
            .filter(EnrEnrollmentStepKind.title())
            ;
    }

}
