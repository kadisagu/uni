package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard;

import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings;
import ru.tandemservice.unienr14.settings.entity.gen.EnrEnrollmentCampaignRequestWizardSettingsGen;

import java.util.Map;

/**
 * @author vdanilov
 */
public class EnrEntrantRequestWizardUI extends SimpleWizardUIPresenter {

    public static final String BC_PARAM_ENROLLMENT_CAMPAIGN_ID = "enrollmentCampaignId";
    public static final String BC_PARAM_ENROLLMENT_CAMPAIGN_WIZARD_SETTINGS = "enrollmentCampaignWizardSettings";
    public static final String BC_PARAM_PERSON_ID = "personId";
    public static final String BC_PARAM_ENTRANT_ID = "entrantId";
    public static final String BC_PARAM_ENTRANT_REQUEST_ID = "entrantRequestId";

    @Override
    public void onComponentActivate()
    {
        if (null == this.getId()) {
            throw new IllegalStateException();
        }

        final EnrEnrollmentCampaign ec = DataAccessServices.dao().get(EnrEnrollmentCampaign.class, this.getId());
        final IComponentRegion region = this.getConfig().getBusinessComponent().getParentRegion();
        region.getBusinessData().put(BC_PARAM_ENROLLMENT_CAMPAIGN_ID, ec.getId());

        final EnrEnrollmentCampaignRequestWizardSettings wizardSettings = getWizardSettings(ec);
        region.getBusinessData().put(BC_PARAM_ENROLLMENT_CAMPAIGN_WIZARD_SETTINGS, wizardSettings);

        // грузим все шаги-компоненты из точек расширения
        super.onComponentActivate();

        // TODO: кастомизируем шаги на основе settings

        //        if (Debug.isDisplay()) {
        //            this.register(new SimpleWizardStep("step.debug", EnrEntrantRequestInWizardDebug.class));
        //        }
    }

    private EnrEnrollmentCampaignRequestWizardSettings getWizardSettings(final EnrEnrollmentCampaign ec) {
        final EnrEnrollmentCampaignRequestWizardSettings settings = IUniBaseDao.instance.get().getByNaturalId(new EnrEnrollmentCampaignRequestWizardSettingsGen.NaturalId(ec));
        if (null != settings) { return settings; }
        return new EnrEnrollmentCampaignRequestWizardSettings(ec);
    }

    @Override
    protected void onWizardFinish() {
        final Map<String, Object> data = getBusinessData();
        final Object entrantId = data.get(BC_PARAM_ENTRANT_ID);

        this.deactivate(
            new ParametersMap()
            .add(BC_PARAM_ENTRANT_ID, entrantId)
            .add(BC_PARAM_ENTRANT_REQUEST_ID, data.get(BC_PARAM_ENTRANT_REQUEST_ID))
        );

        if (entrantId instanceof Long) {
            // переходим на карточку абитуриента
            getActivationBuilder().asDesktopRoot(EnrEntrantPub.class).parameter(PUBLISHER_ID, entrantId).parameter("selectedTab", "requestTab").activate();
        }
    }

    @Override
    protected Map<String, Object> getBusinessData() {
        return this.getConfig().getBusinessComponent().getParentRegion().getBusinessData();
    }
}
