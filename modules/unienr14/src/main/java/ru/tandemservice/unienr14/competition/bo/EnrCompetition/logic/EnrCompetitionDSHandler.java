/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/3/14
 */
public class EnrCompetitionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_EMPTY_EXAM_SET_ID = "emptyExamSetId";
    public static final String PARAM_EMPTY_EXAM_SET_EQ = "emptyExamSetEq";
    public static final String PARAM_ENR_COMPETITION_UTIL = EnrCompetitionFilterAddon.class.getSimpleName();

    public static final String PARAM_EXAM_SET = "examSet";
    public static final String PARAM_PROGRAM_SET = "programSet";
    public static final String PARAM_COMP_TYPE = "compType";

    public static final String VIEW_PROP_EXAMS = "exams";
    public static final String VIEW_PROP_REQUEST_COUNT = "requestCount";
    public static final String VIEW_PROP_PASSED = "passed";

    public EnrCompetitionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrCompetition.class, "d");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("d"));

        filter(dql, context, input, "d");

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(isPageable())
            .build();

        if (!isPageable()) {
            output.setCountRecord(Math.max(1, output.getRecordList().size()));

            output.ordering(new EntityComparator(
                new EntityOrder(EnrCompetition.programSetOrgUnit().programSet().title().s(), OrderDirection.asc),
                new EntityOrder(EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().fullTitle().s(), OrderDirection.asc),
                new EntityOrder(EnrCompetition.programSetOrgUnit().programSet().programForm().code().s(), OrderDirection.asc),
                new EntityOrder(EnrCompetition.type().code().s(), OrderDirection.asc),
                new EntityOrder(EnrCompetition.eduLevelRequirement().priority(), OrderDirection.asc)
            ));
        }

        return wrap(input, output, context);
    }

    /**
     * Фильтрует запрос.
     * @param dql builder from EnrDirectionCompetition
     */
    protected void filter(DQLSelectBuilder dql, ExecutionContext context, DSInput input, String alias)
    {
        final EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final EnrProgramSetBase programSet = context.get(PARAM_PROGRAM_SET);
        final EnrExamSet examSet = context.get(PARAM_EXAM_SET);
        final EnrCompetitionType compType = context.get(PARAM_COMP_TYPE);
        EnrCompetitionFilterAddon util = context.get(PARAM_ENR_COMPETITION_UTIL);
        Boolean emptyExamSetEq = context.get(PARAM_EMPTY_EXAM_SET_EQ);
        if (emptyExamSetEq != null) {
            if (emptyExamSetEq) {
                dql.where(eq(property(alias, EnrCompetition.examSetVariant().examSet().id()), commonValue(context.get(PARAM_EMPTY_EXAM_SET_ID))));
            }
            else {
                dql.where(ne(property(alias, EnrCompetition.examSetVariant().examSet().id()), commonValue(context.get(PARAM_EMPTY_EXAM_SET_ID))));
            }
        }

        if (util != null) {
            util.applyFilters(dql, alias);
        }

        FilterUtils.applySelectFilter(dql, alias, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign(), campaign);

        if (programSet != null) {
            dql.where(eq(property(EnrCompetition.programSetOrgUnit().programSet().fromAlias("d")), value(programSet)));
        }
        if (examSet != null) {
            dql.where(eq(property(EnrCompetition.examSetVariant().examSet().fromAlias(alias)), value(examSet)));
            dql.where(ne(property(EnrCompetition.type().code().fromAlias(alias)), value(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)));
            dql.where(ne(property(EnrCompetition.type().code().fromAlias(alias)), value(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL)));
        }

        if (compType != null) {
            dql.where(eq(property(EnrCompetition.type().fromAlias(alias)), value(compType)));
        }

    }

    protected DSOutput wrap(final DSInput input, DSOutput output, ExecutionContext context)
    {
        Set<Long> variants = new HashSet<>();
        Map<Long, EnrCompetition> competitionMap = new HashMap<>();
        Map<Long, Integer> requestCount = new HashMap<>();
        Map<Long, Integer> passedCount = new HashMap<>();
        for (EnrCompetition competition : output.<EnrCompetition>getRecordList()) {
            variants.add(competition.getExamSetVariant().getId());
            competitionMap.put(competition.getId(), competition);
            requestCount.put(competition.getId(), 0);
            passedCount.put(competition.getId(), 0);
        }
        Map<Long, IEnrExamSetDao.IExamSetSettings> contents = EnrExamSetManager.instance().dao().getExamSetContent(variants);
        for (EnrRatingItem ri : new DQLSimple<>(EnrRatingItem.class)
            .where(EnrRatingItem.competition().id(), requestCount.keySet())
            .where(EnrRatingItem.requestedCompetitionActive(), true)
            .list()) {
            Long comp = ri.getCompetition().getId();
            requestCount.put(comp, requestCount.get(comp) + 1);
            if (ri.isStatusRatingPositiveSafe()) {
                passedCount.put(comp, passedCount.get(comp) + 1);
            }
        }

        for (DataWrapper wrapper : DataWrapper.wrap(output)) {
            EnrCompetition competition = competitionMap.get(wrapper.getId());
            wrapper.setProperty(VIEW_PROP_EXAMS, competition == null || competition.isNoExams() ? null : contents.get(((EnrExamSetVariant) wrapper.getProperty(EnrCompetition.L_EXAM_SET_VARIANT)).getId()).getTitle());
            wrapper.setProperty(VIEW_PROP_REQUEST_COUNT, requestCount.get(wrapper.getId()));
            wrapper.setProperty(VIEW_PROP_PASSED, passedCount.get(wrapper.getId()));
        }

        return output;
    }
}

