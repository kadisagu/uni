/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DirectionListAddon.EnrEntrantRequestDirectionListAddon;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 5/3/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "request.id", required = true)
})
public class EnrEntrantRequestPubUI extends UIPresenter implements EnrEntrantRequestActionsAddon.IOwner, EnrEntrantRequestDirectionListAddon.IOwner
{
    public static final String VIEW_PROPERTY_ORIGINAL = "original";

    private EnrEntrantRequest request = new EnrEntrantRequest();

    private StaticListDataSource<EnrRequestedCompetition> directionDataSource;
    private StaticListDataSource<ViewWrapper> documentDataSource;

    private Date originalReceivedDate;

    @Override
    public void onComponentRefresh()
    {
        setRequest(IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, getRequest().getId()));

        List<EnrRequestedCompetition> reqCompetitionList = IUniBaseDao.instance.get().getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request(), getRequest(), EnrRequestedCompetition.priority().s());
        prepareDirectionDataSource(reqCompetitionList);
        prepareDocumentDataSource();

        if (getRequest().isEduInstDocOriginalHandedIn()) {
            setOriginalReceivedDate(getRequest().getEduInstDocOriginalRef().getRegistrationDate());
        } else {
            setOriginalReceivedDate(null);
        }
    }

    public void onClickDeleteEntrantRequest() {
        getActionsAddon().onClickDeleteEntrantRequest();
        deactivate();
    }

    // interfaces for addons usage

    @Override
    public EnrEntrantRequest getRequestForActions()
    {
        return getRequest();
    }

    @Override
    public String getSecPostfix()
    {
        return "enrEntrantRequestPub";
    }

    @Override
    public boolean isAccessible()
    {
        return getEntrant().isAccessible();
    }

    // presenter

    public EnrEntrant getEntrant() {
        return getRequest().getEntrant();
    }

    // utils

    public boolean isForeignIdentityCard()
    {
        if (getRequest().getIdentityCard() != null && getRequest().getIdentityCard().getCitizenship().getCode() != 0)
            return true;
        return false;
    }

    private EnrEntrantRequestDirectionListAddon getDirectionListAddon() {
        return (EnrEntrantRequestDirectionListAddon) getConfig().getAddon(EnrEntrantRequestDirectionListAddon.NAME);
    }

    private EnrEntrantRequestActionsAddon getActionsAddon() {
        return (EnrEntrantRequestActionsAddon) getConfig().getAddon(EnrEntrantRequestActionsAddon.NAME);
    }

    private void prepareDirectionDataSource(List<EnrRequestedCompetition> reqCompetitionList)
    {
        setDirectionDataSource(getDirectionListAddon().prepareDataSource(getRequest()));
        getDirectionDataSource().setupRows(reqCompetitionList);
    }

    private void prepareDocumentDataSource()
    {
        if (getDocumentDataSource() == null) {
            setDocumentDataSource(new StaticListDataSource<ViewWrapper>());
            PublisherLinkColumn linkColumn = new PublisherLinkColumn("Тип документа", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE);
            linkColumn.setResolver(EnrPersonLegacyUtils.personRelatedStuffLinkResolver(getEntrant()));
            getDocumentDataSource().addColumn(linkColumn.setOrderable(false));

            getDocumentDataSource().addColumn(new SimpleColumn("Серия и номер", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Дата выдачи", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Информация о документе", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Дата добавления", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new BooleanColumn("Оригинал", VIEW_PROPERTY_ORIGINAL).setClickable(false).setOrderable(false));
        }

        ArrayList<ViewWrapper> wrappers = new ArrayList<>();

        EnrEntrantDocumentViewWrapper idcWrapper = new EnrEntrantDocumentViewWrapper(getRequest().getIdentityCard());
        idcWrapper.setViewProperty(VIEW_PROPERTY_ORIGINAL, false);
        wrappers.add(idcWrapper);

        EnrEntrantDocumentViewWrapper eduInstWrapper = new EnrEntrantDocumentViewWrapper(getRequest().getEduDocument());
        eduInstWrapper.setViewProperty(VIEW_PROPERTY_ORIGINAL, getRequest().isEduInstDocOriginalHandedIn());
        wrappers.add(eduInstWrapper);

        List<EnrEntrantRequestAttachment> attachments = IUniBaseDao.instance.get().getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest(), getRequest());
        Collections.sort(attachments, IEnrEntrantRequestAttachable.ATTACHMENT_COMPARATOR);
        for (EnrEntrantRequestAttachment attachment : attachments) {
            EnrEntrantDocumentViewWrapper wrapper = EnrEntrantDocumentViewWrapper.wrap(attachment.getDocument());
            wrapper.setViewProperty(VIEW_PROPERTY_ORIGINAL, attachment.isOriginalHandedIn());
            wrappers.add(wrapper);
        }

        getDocumentDataSource().setupRows(wrappers);
    }

    // getters and setters

    public EnrEntrantRequest getRequest()
    {
        return request;
    }

    public void setRequest(EnrEntrantRequest request)
    {
        this.request = request;
    }

    public StaticListDataSource<EnrRequestedCompetition> getDirectionDataSource()
    {
        return directionDataSource;
    }

    public void setDirectionDataSource(StaticListDataSource<EnrRequestedCompetition> directionDataSource)
    {
        this.directionDataSource = directionDataSource;
    }

    public StaticListDataSource<ViewWrapper> getDocumentDataSource()
    {
        return documentDataSource;
    }

    public void setDocumentDataSource(StaticListDataSource<ViewWrapper> documentDataSource)
    {
        this.documentDataSource = documentDataSource;
    }

    public Date getOriginalReceivedDate()
    {
        return originalReceivedDate;
    }

    public void setOriginalReceivedDate(Date originalReceivedDate)
    {
        this.originalReceivedDate = originalReceivedDate;
    }
}
