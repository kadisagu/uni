/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.RequestTypeAndEduDocStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Configuration
public class EnrEntrantRequestAddWizardRequestTypeAndEduDocStep extends BusinessComponentManager
{
    public static final String DS_COUNTRY = "countryDS";
    public static final String DS_SETTLEMENT = "settlementDS";
    public static final String OPTIONS_DS = "optionsDS";
    public static final String DS_DOCUMENT_KIND = "documentKindDS";
    public static final String DS_EDU_LEVEL = "eduLevelDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(DS_COUNTRY, AddressBaseManager.instance().countryComboDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SETTLEMENT, getName(), AddressItem.settlementComboDSHandler(getName())))
                .addDataSource(EnrRequestTypeManager.instance().requestTypeEnrCampaignDSConfig())
                .addDataSource(selectDS(DS_EDU_LEVEL, eduLevelDSHandler()))
                .addDataSource(selectDS(DS_DOCUMENT_KIND, documentKindDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLevelDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduLevel.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long requestTypeId = context.get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE);
                Long documentKindId = context.get(EnrEntrantBaseDocumentManager.BIND_DOCUMENT_KIND);
                Boolean receiveEduLevelFirst = context.get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST);

                if(requestTypeId != null && documentKindId != null )
                {
                    EduDocumentKind documentKind = DataAccessServices.dao().get(documentKindId);
                    if(documentKind.getFixedEducationLevel() == null)
                    {
                        EnrRequestType requestType = DataAccessServices.dao().get(requestTypeId);
                        DQLSelectBuilder eduLevelDQLFilter = EnrEntrantBaseDocumentManager.getEduLevelDQLFilter(
                                "el",
                                value(documentKind.getCode()),
                                value(documentKind.isCanCertifyQualificationSpec()),
                                null,
                                requestType.getCode(),
                                receiveEduLevelFirst
                        );
                        eduLevelDQLFilter.column(value(1));
                        eduLevelDQLFilter.where(eq(property(alias), property("el")));
                        dql.where(exists(eduLevelDQLFilter.buildQuery()));
                    }
                }
            }
        }
                .where(EduLevel.level(), Boolean.TRUE)
                .order(EduLevel.code())
                .filter(EduLevel.title())
                .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler documentKindDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduDocumentKind.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long requestTypeId = context.get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE);
                Boolean receiveEduLevelFirst = context.get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST);
                Boolean otherEduDocPermission = context.getBoolean("otherEduDocPermission", false);

                if(requestTypeId != null)
                {
                    EnrRequestType requestType = DataAccessServices.dao().get(requestTypeId);
                    DQLSelectBuilder eduLevelDQLFilter = EnrEntrantBaseDocumentManager.getEduLevelDQLFilter(
                            "el",
                            property(alias, EduDocumentKind.code()),
                            property(alias, EduDocumentKind.canCertifyQualificationSpec()),
                            null,
                            requestType.getCode(),
                            receiveEduLevelFirst
                    );
                    eduLevelDQLFilter.column(value(1));
                    eduLevelDQLFilter.where(eq(property(alias, EduDocumentKind.fixedEducationLevel()), property("el")));

                    if (otherEduDocPermission)
                    {
                        dql.where(or(
                                eq(property(alias, EduDocumentKind.code()), value(EduDocumentKindCodes.OTHER)),
                                exists(eduLevelDQLFilter.buildQuery())
                        ));
                    }
                    else
                    {
                        dql.where(exists(eduLevelDQLFilter.buildQuery()));
                    }
                }
                else
                {
                    dql.where(isNull(alias + ".id"));
                }
            }
        }
                .order(EduDocumentKind.title())
                .filter(EduDocumentKind.title());
    }
}



    