package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadHonour;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadProfileDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Диплом участника олимпиады
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOlympiadDiplomaGen extends PersonDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma";
    public static final String ENTITY_NAME = "enrOlympiadDiploma";
    public static final int VERSION_HASH = 1249885552;
    private static IEntityMeta ENTITY_META;

    public static final String L_SETTLEMENT = "settlement";
    public static final String L_OLYMPIAD = "olympiad";
    public static final String L_SUBJECT = "subject";
    public static final String L_HONOUR = "honour";
    public static final String P_TRAINING_CLASS = "trainingClass";
    public static final String P_COMBINED_TEAM = "combinedTeam";
    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String L_OLYMPIAD_PROFILE_DISCIPLINE = "olympiadProfileDiscipline";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_EXTENDED_TITLE = "extendedTitle";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_SERIA_AND_NUMBER = "seriaAndNumber";
    public static final String P_TITLE = "title";

    private AddressItem _settlement;     // Место проведения
    private EnrOlympiad _olympiad;     // Олимпиада
    private EnrOlympiadSubject _subject;     // Профиль
    private EnrOlympiadHonour _honour;     // Степень отличия
    private Integer _trainingClass;     // Класс обучения
    private Boolean _combinedTeam;     // Сборная команда
    private EnrStateExamSubject _stateExamSubject;     // Общеобразовательный предмет
    private EnrOlympiadProfileDiscipline _olympiadProfileDiscipline;     // Профильная дисциплина

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Место проведения.
     */
    public AddressItem getSettlement()
    {
        return _settlement;
    }

    /**
     * @param settlement Место проведения.
     */
    public void setSettlement(AddressItem settlement)
    {
        dirty(_settlement, settlement);
        _settlement = settlement;
    }

    /**
     * @return Олимпиада. Свойство не может быть null.
     */
    @NotNull
    public EnrOlympiad getOlympiad()
    {
        return _olympiad;
    }

    /**
     * @param olympiad Олимпиада. Свойство не может быть null.
     */
    public void setOlympiad(EnrOlympiad olympiad)
    {
        dirty(_olympiad, olympiad);
        _olympiad = olympiad;
    }

    /**
     * @return Профиль. Свойство не может быть null.
     */
    @NotNull
    public EnrOlympiadSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Профиль. Свойство не может быть null.
     */
    public void setSubject(EnrOlympiadSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Степень отличия. Свойство не может быть null.
     */
    @NotNull
    public EnrOlympiadHonour getHonour()
    {
        return _honour;
    }

    /**
     * @param honour Степень отличия. Свойство не может быть null.
     */
    public void setHonour(EnrOlympiadHonour honour)
    {
        dirty(_honour, honour);
        _honour = honour;
    }

    /**
     * @return Класс обучения.
     */
    public Integer getTrainingClass()
    {
        return _trainingClass;
    }

    /**
     * @param trainingClass Класс обучения.
     */
    public void setTrainingClass(Integer trainingClass)
    {
        dirty(_trainingClass, trainingClass);
        _trainingClass = trainingClass;
    }

    /**
     * null — не задано, false - Сборная команда Украины, true - Сборная команда РФ
     *
     * @return Сборная команда.
     */
    public Boolean getCombinedTeam()
    {
        return _combinedTeam;
    }

    /**
     * @param combinedTeam Сборная команда.
     */
    public void setCombinedTeam(Boolean combinedTeam)
    {
        dirty(_combinedTeam, combinedTeam);
        _combinedTeam = combinedTeam;
    }

    /**
     * @return Общеобразовательный предмет.
     */
    public EnrStateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject Общеобразовательный предмет.
     */
    public void setStateExamSubject(EnrStateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    /**
     * @return Профильная дисциплина.
     */
    public EnrOlympiadProfileDiscipline getOlympiadProfileDiscipline()
    {
        return _olympiadProfileDiscipline;
    }

    /**
     * @param olympiadProfileDiscipline Профильная дисциплина.
     */
    public void setOlympiadProfileDiscipline(EnrOlympiadProfileDiscipline olympiadProfileDiscipline)
    {
        dirty(_olympiadProfileDiscipline, olympiadProfileDiscipline);
        _olympiadProfileDiscipline = olympiadProfileDiscipline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrOlympiadDiplomaGen)
        {
            setSettlement(((EnrOlympiadDiploma)another).getSettlement());
            setOlympiad(((EnrOlympiadDiploma)another).getOlympiad());
            setSubject(((EnrOlympiadDiploma)another).getSubject());
            setHonour(((EnrOlympiadDiploma)another).getHonour());
            setTrainingClass(((EnrOlympiadDiploma)another).getTrainingClass());
            setCombinedTeam(((EnrOlympiadDiploma)another).getCombinedTeam());
            setStateExamSubject(((EnrOlympiadDiploma)another).getStateExamSubject());
            setOlympiadProfileDiscipline(((EnrOlympiadDiploma)another).getOlympiadProfileDiscipline());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOlympiadDiplomaGen> extends PersonDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOlympiadDiploma.class;
        }

        public T newInstance()
        {
            return (T) new EnrOlympiadDiploma();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "settlement":
                    return obj.getSettlement();
                case "olympiad":
                    return obj.getOlympiad();
                case "subject":
                    return obj.getSubject();
                case "honour":
                    return obj.getHonour();
                case "trainingClass":
                    return obj.getTrainingClass();
                case "combinedTeam":
                    return obj.getCombinedTeam();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
                case "olympiadProfileDiscipline":
                    return obj.getOlympiadProfileDiscipline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "settlement":
                    obj.setSettlement((AddressItem) value);
                    return;
                case "olympiad":
                    obj.setOlympiad((EnrOlympiad) value);
                    return;
                case "subject":
                    obj.setSubject((EnrOlympiadSubject) value);
                    return;
                case "honour":
                    obj.setHonour((EnrOlympiadHonour) value);
                    return;
                case "trainingClass":
                    obj.setTrainingClass((Integer) value);
                    return;
                case "combinedTeam":
                    obj.setCombinedTeam((Boolean) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((EnrStateExamSubject) value);
                    return;
                case "olympiadProfileDiscipline":
                    obj.setOlympiadProfileDiscipline((EnrOlympiadProfileDiscipline) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "settlement":
                        return true;
                case "olympiad":
                        return true;
                case "subject":
                        return true;
                case "honour":
                        return true;
                case "trainingClass":
                        return true;
                case "combinedTeam":
                        return true;
                case "stateExamSubject":
                        return true;
                case "olympiadProfileDiscipline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "settlement":
                    return true;
                case "olympiad":
                    return true;
                case "subject":
                    return true;
                case "honour":
                    return true;
                case "trainingClass":
                    return true;
                case "combinedTeam":
                    return true;
                case "stateExamSubject":
                    return true;
                case "olympiadProfileDiscipline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "settlement":
                    return AddressItem.class;
                case "olympiad":
                    return EnrOlympiad.class;
                case "subject":
                    return EnrOlympiadSubject.class;
                case "honour":
                    return EnrOlympiadHonour.class;
                case "trainingClass":
                    return Integer.class;
                case "combinedTeam":
                    return Boolean.class;
                case "stateExamSubject":
                    return EnrStateExamSubject.class;
                case "olympiadProfileDiscipline":
                    return EnrOlympiadProfileDiscipline.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOlympiadDiploma> _dslPath = new Path<EnrOlympiadDiploma>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOlympiadDiploma");
    }
            

    /**
     * @return Место проведения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getSettlement()
     */
    public static AddressItem.Path<AddressItem> settlement()
    {
        return _dslPath.settlement();
    }

    /**
     * @return Олимпиада. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getOlympiad()
     */
    public static EnrOlympiad.Path<EnrOlympiad> olympiad()
    {
        return _dslPath.olympiad();
    }

    /**
     * @return Профиль. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getSubject()
     */
    public static EnrOlympiadSubject.Path<EnrOlympiadSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Степень отличия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getHonour()
     */
    public static EnrOlympiadHonour.Path<EnrOlympiadHonour> honour()
    {
        return _dslPath.honour();
    }

    /**
     * @return Класс обучения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getTrainingClass()
     */
    public static PropertyPath<Integer> trainingClass()
    {
        return _dslPath.trainingClass();
    }

    /**
     * null — не задано, false - Сборная команда Украины, true - Сборная команда РФ
     *
     * @return Сборная команда.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getCombinedTeam()
     */
    public static PropertyPath<Boolean> combinedTeam()
    {
        return _dslPath.combinedTeam();
    }

    /**
     * @return Общеобразовательный предмет.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getStateExamSubject()
     */
    public static EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    /**
     * @return Профильная дисциплина.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getOlympiadProfileDiscipline()
     */
    public static EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline> olympiadProfileDiscipline()
    {
        return _dslPath.olympiadProfileDiscipline();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getExtendedTitle()
     */
    public static SupportedPropertyPath<String> extendedTitle()
    {
        return _dslPath.extendedTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getSeriaAndNumber()
     */
    public static SupportedPropertyPath<String> seriaAndNumber()
    {
        return _dslPath.seriaAndNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrOlympiadDiploma> extends PersonDocument.Path<E>
    {
        private AddressItem.Path<AddressItem> _settlement;
        private EnrOlympiad.Path<EnrOlympiad> _olympiad;
        private EnrOlympiadSubject.Path<EnrOlympiadSubject> _subject;
        private EnrOlympiadHonour.Path<EnrOlympiadHonour> _honour;
        private PropertyPath<Integer> _trainingClass;
        private PropertyPath<Boolean> _combinedTeam;
        private EnrStateExamSubject.Path<EnrStateExamSubject> _stateExamSubject;
        private EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline> _olympiadProfileDiscipline;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<String> _extendedTitle;
        private SupportedPropertyPath<String> _fullTitle;
        private SupportedPropertyPath<String> _seriaAndNumber;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Место проведения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getSettlement()
     */
        public AddressItem.Path<AddressItem> settlement()
        {
            if(_settlement == null )
                _settlement = new AddressItem.Path<AddressItem>(L_SETTLEMENT, this);
            return _settlement;
        }

    /**
     * @return Олимпиада. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getOlympiad()
     */
        public EnrOlympiad.Path<EnrOlympiad> olympiad()
        {
            if(_olympiad == null )
                _olympiad = new EnrOlympiad.Path<EnrOlympiad>(L_OLYMPIAD, this);
            return _olympiad;
        }

    /**
     * @return Профиль. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getSubject()
     */
        public EnrOlympiadSubject.Path<EnrOlympiadSubject> subject()
        {
            if(_subject == null )
                _subject = new EnrOlympiadSubject.Path<EnrOlympiadSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Степень отличия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getHonour()
     */
        public EnrOlympiadHonour.Path<EnrOlympiadHonour> honour()
        {
            if(_honour == null )
                _honour = new EnrOlympiadHonour.Path<EnrOlympiadHonour>(L_HONOUR, this);
            return _honour;
        }

    /**
     * @return Класс обучения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getTrainingClass()
     */
        public PropertyPath<Integer> trainingClass()
        {
            if(_trainingClass == null )
                _trainingClass = new PropertyPath<Integer>(EnrOlympiadDiplomaGen.P_TRAINING_CLASS, this);
            return _trainingClass;
        }

    /**
     * null — не задано, false - Сборная команда Украины, true - Сборная команда РФ
     *
     * @return Сборная команда.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getCombinedTeam()
     */
        public PropertyPath<Boolean> combinedTeam()
        {
            if(_combinedTeam == null )
                _combinedTeam = new PropertyPath<Boolean>(EnrOlympiadDiplomaGen.P_COMBINED_TEAM, this);
            return _combinedTeam;
        }

    /**
     * @return Общеобразовательный предмет.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getStateExamSubject()
     */
        public EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new EnrStateExamSubject.Path<EnrStateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

    /**
     * @return Профильная дисциплина.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getOlympiadProfileDiscipline()
     */
        public EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline> olympiadProfileDiscipline()
        {
            if(_olympiadProfileDiscipline == null )
                _olympiadProfileDiscipline = new EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline>(L_OLYMPIAD_PROFILE_DISCIPLINE, this);
            return _olympiadProfileDiscipline;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EnrOlympiadDiplomaGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getExtendedTitle()
     */
        public SupportedPropertyPath<String> extendedTitle()
        {
            if(_extendedTitle == null )
                _extendedTitle = new SupportedPropertyPath<String>(EnrOlympiadDiplomaGen.P_EXTENDED_TITLE, this);
            return _extendedTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EnrOlympiadDiplomaGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getSeriaAndNumber()
     */
        public SupportedPropertyPath<String> seriaAndNumber()
        {
            if(_seriaAndNumber == null )
                _seriaAndNumber = new SupportedPropertyPath<String>(EnrOlympiadDiplomaGen.P_SERIA_AND_NUMBER, this);
            return _seriaAndNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrOlympiadDiplomaGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrOlympiadDiploma.class;
        }

        public String getEntityName()
        {
            return "enrOlympiadDiploma";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();

    public abstract String getExtendedTitle();

    public abstract String getFullTitle();

    public abstract String getSeriaAndNumber();

    public abstract String getTitle();
}
