package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniedu.catalog.bo.EduProgramDuration.ui.AddEdit.EduProgramDurationAddEdit;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.ui.BasicAddEdit.EnrCatalogOrdersBasicAddEdit;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/**
 * Основание приказа по абитуриентам
 */
public class EnrOrderBasic extends EnrOrderBasicGen implements IDynamicCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() { return EnrCatalogOrdersBasicAddEdit.class.getSimpleName(); }
        };
    }
}