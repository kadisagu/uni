package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.EnrReportOrgUnitDocAcceptanceAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportOrgUnitDocAcceptanceGen;

import java.util.Arrays;
import java.util.List;

/**
 * Ход приема документов по подразделениям
 */
public class EnrReportOrgUnitDocAcceptance extends EnrReportOrgUnitDocAcceptanceGen implements IEnrReport
{

    public static final String REPORT_KEY = "enr14ReportOrgUnitDocAcceptance";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_REQUEST_TYPE,
        P_COMPENSATION_TYPE,
        P_PROGRAM_FORM,
        P_ENR_ORG_UNIT,
        P_FORMATIVE_ORG_UNIT);


    public static IEnrStorableReportDesc getDescription() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportOrgUnitDocAcceptance.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportOrgUnitDocAcceptanceAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Ход приема документов по подразделениям»"; }
            @Override public String getListTitle() { return "Список отчетов «Ход приема документов по подразделениям»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}