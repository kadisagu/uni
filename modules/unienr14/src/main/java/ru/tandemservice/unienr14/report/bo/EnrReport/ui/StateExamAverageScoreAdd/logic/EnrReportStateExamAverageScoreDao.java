/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.logic;

import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.EnrReportStateExamAverageScoreAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.CellMergeMap;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.ExcelReportUtils;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportStateExamAverageScore;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 30.06.2014
 */
public class EnrReportStateExamAverageScoreDao extends UniBaseDao implements IEnrReportStateExamAverageScoreDao {

    @Override
    public long createReport(EnrReportStateExamAverageScoreAddUI model) {

        EnrReportStateExamAverageScore report = new EnrReportStateExamAverageScore();
        
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportStateExamAverageScore.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportStateExamAverageScore.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportStateExamAverageScore.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportStateExamAverageScore.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportStateExamAverageScore.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportStateExamAverageScore.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportStateExamAverageScore.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportStateExamAverageScore.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportStateExamAverageScore.P_PROGRAM_SET, "title");

        report.setStage(model.getStageSelector().getStage().getTitle());

        DatabaseFile content = new DatabaseFile();

        report.setReportForm(model.getReportFormType().getTitle());

        try {
            content.setContent(buildReport(model));
        } catch (IOException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        } catch (JXLException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        content.setFilename("EnrReportStateExamAverageScore.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportStateExamAverageScoreAddUI model) throws IOException, JXLException
    {

        // документ
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL
                .where(eq(property("request", EnrEntrantRequest.takeAwayDocument()), value(java.lang.Boolean.FALSE)));
        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property("vvi", EnrChosenEntranceExam.requestedCompetition()), property("reqComp")))
                .column(property("vvi"));


        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");

        // Запрос
        List<EnrChosenEntranceExam> vviList = getList(requestedCompDQL);

        // Сет, в который выбранный конкурс попадает, если у него хоть 1 ВВИ не проходит под условием
        Set<EnrRequestedCompetition> bannedReqCompetitions = new HashSet<>();
        for (EnrChosenEntranceExam exam : vviList)
        {
            if (exam.getMaxMarkForm() == null ||
                exam.getMaxMarkForm().getMarkSource()==null ||
                !exam.getMaxMarkForm().getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM))
            {
                bannedReqCompetitions.add(exam.getRequestedCompetition());
            }
        }

        // Сортируем данные
        Map<EduProgramKind, Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>>>> dataMap = new TreeMap<>(ITitled.TITLED_COMPARATOR);

        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };

        Comparator<CompensationType> compensationTypeComparator = new Comparator<CompensationType>() {
            @Override
            public int compare(CompensationType o1, CompensationType o2) {
                return -Boolean.compare(o1.isBudget(), o2.isBudget());
            }
        };

        Comparator<EnrProgramSetBase> programSetBaseComparator = new Comparator<EnrProgramSetBase>() {
            @Override
            public int compare(EnrProgramSetBase o1, EnrProgramSetBase o2) {
                if (o1 == o2) return 0;
                if (o1 == null) return 1;
                if (o2 == null) return -1;
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };



        for (EnrChosenEntranceExam exam : vviList)
        {
            EnrProgramSetBase programSet = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet();
            EduProgramKind programKind = programSet.getProgramKind();
            EduProgramSubject programSubject = programSet.getProgramSubject();
            EduProgramForm programForm = programSet.getProgramForm();
            CompensationType compensationType = exam.getRequestedCompetition().getCompetition().getType().getCompensationType();

            // Если формируется отчет не по ОП, то просто заменяем все ОП на нулл-значение. Соотв-но не группируется.
            if (!model.isReportFormTypeByProgramSets())
                programSet = null;


            if (bannedReqCompetitions.contains(exam.getRequestedCompetition()))
                continue;

            Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>>> eduProgramSubjectMap = SafeMap.safeGet(dataMap, programKind, TreeMap.class, subjectComparator);
            Map<EnrProgramSetBase, Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>> enrProgramSetBaseMap = SafeMap.safeGet(eduProgramSubjectMap, programSubject, TreeMap.class, programSetBaseComparator);
            Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>> eduProgramFormMap = SafeMap.safeGet(enrProgramSetBaseMap, programSet, TreeMap.class, ITitled.TITLED_COMPARATOR);
            Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>> compensationTypeMap = SafeMap.safeGet(eduProgramFormMap, programForm, TreeMap.class, compensationTypeComparator);
            Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> enrRequestedCompetitionMap = SafeMap.safeGet(compensationTypeMap, compensationType, HashMap.class);
            Set<EnrChosenEntranceExam> exams = SafeMap.safeGet(enrRequestedCompetitionMap, exam.getRequestedCompetition(), HashSet.class);
            exams.add(exam);
        }

        // Заполняем отчет.
        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        contentFormat.setWrap(true);

        WritableCellFormat numberFormat = new WritableCellFormat(contentFont, new jxl.write.NumberFormat("#0"));
        numberFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat fractionalFormat = new WritableCellFormat(contentFont, new jxl.write.NumberFormat("#0.00"));
        fractionalFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        int index = printHeader(model, sheet);

        for (Map.Entry<EduProgramKind, Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>>>> kindEntry : dataMap.entrySet())
        {
            for (Map.Entry<EduProgramSubject, Map<EnrProgramSetBase, Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>>> subjectEntry : kindEntry.getValue().entrySet())
            {
                for (Map.Entry<EnrProgramSetBase, Map<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>> programSetEntry : subjectEntry.getValue().entrySet())
                {
                    for (Map.Entry<EduProgramForm, Map<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>> programFormEntry : programSetEntry.getValue().entrySet())
                    {
                        for (Map.Entry<CompensationType, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>> compensationTypeEntry : programFormEntry.getValue().entrySet())
                        {

                            List<String> row = new ArrayList<>();

                            if (model.isReportFormTypeByProgramSets())
                                row.add(programSetEntry.getKey().getTitle());
                            else row.add(subjectEntry.getKey().getTitleWithCode());

                            row.add(programFormEntry.getKey().getTitle());

                            row.add(compensationTypeEntry.getKey().getShortTitle());

                            row.add(kindEntry.getKey().getShortTitle());



                            sheet.setRowView(index, (row.get(0).length()/45) * 300 + 300);
                            printRowWithDigits(sheet, makeRow(compensationTypeEntry.getValue()), row, index++, contentFormat, numberFormat, fractionalFormat);

                        }
                    }
                }
            }
        }
        workbook.write();
        workbook.close();
        byte[] bytes = out.toByteArray();
        out.close();
        return bytes;
    }

    // считаем мин, макс и среднее
    private List<Double> makeRow(Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> map)
    {
        NumberFormat formatter = new DecimalFormat("#0.00");


        List<Double> marks = new ArrayList<>();
        Double average = new Double(0);

        Map<EnrEntrant, Set<Double>> uniqueMarksMap = SafeMap.get(HashSet.class);

        for (Map.Entry<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> entry : map.entrySet())
        {
            Double marksSum = new Double(0);
            for (EnrChosenEntranceExam exam : entry.getValue())
                marksSum += exam.getMarkAsDouble();

            uniqueMarksMap.get(entry.getKey().getRequest().getEntrant()).add(marksSum);
        }

        for (Map.Entry<EnrEntrant, Set<Double>> entry : uniqueMarksMap.entrySet())
        {
            for (Double marksSum : entry.getValue()) {
                marks.add(marksSum);
                average += marksSum;
            }
        }

        Integer min = Collections.min(marks).intValue();
        Integer max = Collections.max(marks).intValue();
        average = average/marks.size();

        return Arrays.asList(new Double[]{
                Collections.min(marks),
                Collections.max(marks),
                (average)
        });
    }

    // Печатаем хедэр таблицы и таблицу с параметрами отчета
    private int printHeader(EnrReportStateExamAverageScoreAddUI model, WritableSheet sheet) throws JXLException
    {

        int rowIndex = 0;
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);

        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat filterTitlesFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setBackground(Colour.GREY_25_PERCENT);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        filterTitlesFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        headerFormat.setWrap(true);

        CellMergeMap mergeMap = new CellMergeMap();
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setWrap(true);

        sheet.addCell(new Label( 0, 0, "Сводка о средних баллах абитуриентов, поступающих по ЕГЭ", labelFormat));

        FilterParametersPrinter filterParametersPrinter = new FilterParametersPrinter();
        filterParametersPrinter.setStageSelector(model.getStageSelector());
        List<String[]> hTable = filterParametersPrinter.getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        hTable.add(new String[]{
                "Форма отчета",
                model.getReportFormType().getTitle()
        });

        for (int i = 0; i < hTable.size(); i++) {
            sheet.addCell(new Label(0, i + 2, hTable.get(i)[0], filterTitlesFormat));
            sheet.addCell(new Label(1, i + 2, hTable.get(i)[1], contentFormat));
            mergeMap.addMapping(1, i + 2, 10, i + 2);
            sheet.setRowView(i+2, (hTable.get(i)[1].length()/120) * 300 + 300);
            rowIndex = i+2;
        }

        rowIndex+=2;


        int c = 0;

        List<String> headerL1 = new ArrayList<>();
        List<String> headerL2 = new ArrayList<>();
        List<String> headerL3 = new ArrayList<>();

        if (model.isReportFormTypeByProgramSets())
            headerL1.add("Наименование набора ОП");
        else headerL1.add("Направление подготовки, специальности");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Форма обучения");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Вид возмещения затрат");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Вид обр. программы");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Min");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Max");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Средний балл");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Место в рейтинге в субъекте");
        headerL2.add("");
        headerL3.add("");
        mergeMap.addMapping(c, rowIndex, c++, rowIndex + 2);

        headerL1.add("Вузы");
        mergeMap.addMapping(c, rowIndex, c+2, rowIndex);
        headerL2.add("Первый вуз в рейтинге в субъекте");
        headerL3.add("Средний балл");

        c++;

        headerL1.add("");
        headerL2.add("Основной конкурент");
        mergeMap.addMapping(c, rowIndex + 1, c + 1, rowIndex + 1);
        headerL3.add("Место в рейтинге в субъекте");

        headerL1.add("");
        headerL2.add("");
        headerL3.add("Средний балл");

        ExcelReportUtils.setWidths(new int[]{
                40,
                15,
                15,
                15,
                10,
                10,
                10,
                12,
                12,
                12,
                12,
        }, sheet);


        ExcelReportUtils.printrow(sheet, headerL1, rowIndex++, headerFormat);
        ExcelReportUtils.printrow(sheet, headerL2, rowIndex++, headerFormat);
        ExcelReportUtils.printrow(sheet, headerL3, rowIndex++, headerFormat);


        mergeMap.makeMerges(sheet);

        return rowIndex;
    }


    public void printRowWithDigits(WritableSheet sheet, List<Double> digitValues, List<String> rowContent, int rowIndex, WritableCellFormat contentFormat, WritableCellFormat numberFormat, WritableCellFormat fractionalFormat) throws JXLException
    {
        int cIndex = 0;
        int firstBlockSize = rowContent.size();
        for (cIndex = 0; cIndex < firstBlockSize; cIndex++) {
            sheet.addCell(new Label(cIndex, rowIndex, rowContent.get(cIndex), contentFormat));
        }
        for (; cIndex - firstBlockSize < digitValues.size(); cIndex++)
        {
            if (cIndex - firstBlockSize < 2)
                sheet.addCell(new Number(cIndex, rowIndex, digitValues.get(cIndex - firstBlockSize), numberFormat));
            else
                sheet.addCell(new Number(cIndex, rowIndex, digitValues.get(cIndex - firstBlockSize), fractionalFormat));
        }
        for (int i = 0; i < 4; i++) {
            sheet.addCell(new Label(cIndex++, rowIndex, "", numberFormat));
        }

    }
}
