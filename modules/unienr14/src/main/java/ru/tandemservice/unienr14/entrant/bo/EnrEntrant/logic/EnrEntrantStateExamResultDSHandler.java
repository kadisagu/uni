/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/12/14
 */
public class EnrEntrantStateExamResultDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_ENTRANT = "entrant";

    public EnrEntrantStateExamResultDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrEntrantStateExamResult.class, "m");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("m"));
        dql.where(eq(property(EnrEntrantStateExamResult.entrant().fromAlias("m")), commonValue(context.get(PARAM_ENTRANT))));

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(isPageable())
            .build();

        if (!isPageable()) {
            output.setCountRecord(Math.max(1, output.getRecordList().size()));

            output.ordering(new EntityComparator(
                new EntityOrder(EnrEntrantStateExamResult.subject().title().s(), OrderDirection.asc),
                new EntityOrder(EnrEntrantStateExamResult.markAsLong().s(), OrderDirection.desc),
                new EntityOrder(EnrEntrantStateExamResult.year().s(), OrderDirection.desc)
            ));
        }

        return output;
    }

}


