package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид дополнительного статуса абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantCustomStateTypeGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantCustomStateTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType";
    public static final String ENTITY_NAME = "enrEntrantCustomStateType";
    public static final int VERSION_HASH = 194750027;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DESCRIPTION = "description";
    public static final String P_HTML_COLOR = "htmlColor";
    public static final String L_STUDENT_CUSTOM_STATE = "studentCustomState";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _description;     // Описание
    private String _htmlColor;     // Цвет в виде hex-строки (например, AABBCC)
    private StudentCustomStateCI _studentCustomState;     // Дополнительный статус студента
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Цвет в виде hex-строки (например, AABBCC).
     */
    @Length(max=255)
    public String getHtmlColor()
    {
        return _htmlColor;
    }

    /**
     * @param htmlColor Цвет в виде hex-строки (например, AABBCC).
     */
    public void setHtmlColor(String htmlColor)
    {
        dirty(_htmlColor, htmlColor);
        _htmlColor = htmlColor;
    }

    /**
     * @return Дополнительный статус студента.
     */
    public StudentCustomStateCI getStudentCustomState()
    {
        return _studentCustomState;
    }

    /**
     * @param studentCustomState Дополнительный статус студента.
     */
    public void setStudentCustomState(StudentCustomStateCI studentCustomState)
    {
        dirty(_studentCustomState, studentCustomState);
        _studentCustomState = studentCustomState;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantCustomStateTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrEntrantCustomStateType)another).getCode());
            }
            setShortTitle(((EnrEntrantCustomStateType)another).getShortTitle());
            setDescription(((EnrEntrantCustomStateType)another).getDescription());
            setHtmlColor(((EnrEntrantCustomStateType)another).getHtmlColor());
            setStudentCustomState(((EnrEntrantCustomStateType)another).getStudentCustomState());
            setTitle(((EnrEntrantCustomStateType)another).getTitle());
        }
    }

    public INaturalId<EnrEntrantCustomStateTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantCustomStateTypeGen>
    {
        private static final String PROXY_NAME = "EnrEntrantCustomStateTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantCustomStateTypeGen.NaturalId) ) return false;

            EnrEntrantCustomStateTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantCustomStateTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantCustomStateType.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantCustomStateType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "description":
                    return obj.getDescription();
                case "htmlColor":
                    return obj.getHtmlColor();
                case "studentCustomState":
                    return obj.getStudentCustomState();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "htmlColor":
                    obj.setHtmlColor((String) value);
                    return;
                case "studentCustomState":
                    obj.setStudentCustomState((StudentCustomStateCI) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "description":
                        return true;
                case "htmlColor":
                        return true;
                case "studentCustomState":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "description":
                    return true;
                case "htmlColor":
                    return true;
                case "studentCustomState":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "description":
                    return String.class;
                case "htmlColor":
                    return String.class;
                case "studentCustomState":
                    return StudentCustomStateCI.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantCustomStateType> _dslPath = new Path<EnrEntrantCustomStateType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantCustomStateType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Цвет в виде hex-строки (например, AABBCC).
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getHtmlColor()
     */
    public static PropertyPath<String> htmlColor()
    {
        return _dslPath.htmlColor();
    }

    /**
     * @return Дополнительный статус студента.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getStudentCustomState()
     */
    public static StudentCustomStateCI.Path<StudentCustomStateCI> studentCustomState()
    {
        return _dslPath.studentCustomState();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrEntrantCustomStateType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _description;
        private PropertyPath<String> _htmlColor;
        private StudentCustomStateCI.Path<StudentCustomStateCI> _studentCustomState;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrEntrantCustomStateTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrEntrantCustomStateTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EnrEntrantCustomStateTypeGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Цвет в виде hex-строки (например, AABBCC).
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getHtmlColor()
     */
        public PropertyPath<String> htmlColor()
        {
            if(_htmlColor == null )
                _htmlColor = new PropertyPath<String>(EnrEntrantCustomStateTypeGen.P_HTML_COLOR, this);
            return _htmlColor;
        }

    /**
     * @return Дополнительный статус студента.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getStudentCustomState()
     */
        public StudentCustomStateCI.Path<StudentCustomStateCI> studentCustomState()
        {
            if(_studentCustomState == null )
                _studentCustomState = new StudentCustomStateCI.Path<StudentCustomStateCI>(L_STUDENT_CUSTOM_STATE, this);
            return _studentCustomState;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrEntrantCustomStateTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrEntrantCustomStateType.class;
        }

        public String getEntityName()
        {
            return "enrEntrantCustomStateType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
