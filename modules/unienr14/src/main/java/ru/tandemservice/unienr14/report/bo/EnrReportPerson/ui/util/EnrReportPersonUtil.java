/* $Id: EnrReportPersonUtil.java 33259 2014-03-27 11:59:44Z azhebko $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.util;

import com.google.common.collect.Sets;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrPartnerEduInstitution;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 02.09.2011
 */
public class EnrReportPersonUtil
{

    public static List<EnrExamGroupScheduleEvent> getEntrantExamGroups(IReportPrintInfo printInfo, EnrRequestedCompetition competition)
    {
        if (null == competition) return Collections.emptyList();

        Map<Long, List<EnrExamGroupScheduleEvent>> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENTRANT_EXAMGROUP_MAP);
        if (null == map) return Collections.emptyList();

        return map.get(competition.getId());
    }


    public static void prefetchEntrantExamGroups(IReportPrintInfo printInfo, List<Object[]> rows, int directionIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENTRANT_EXAMGROUP_MAP) != null) return;

        final Map<Long, List<EnrExamGroupScheduleEvent>> result = new HashMap<>();

        final List<Long> competitionsIds = getEntityIds(rows, directionIndex, 3, EnrRequestedCompetition.class);

        BatchUtils.execute(competitionsIds, 1000, new BatchUtils.Action<Long>()
            {
            @Override
            public void execute(Collection<Long> competitionsIds)
            {
                List<Object[]> rows = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().fromAlias("rc"), "entrant")
                        .joinEntity("rc", DQLJoinType.inner, EnrExamPassDiscipline.class, "d", eq(property(EnrEntrant.id().fromAlias("entrant")), property("d", EnrExamPassDiscipline.entrant().id())))
                        .joinPath(DQLJoinType.inner, EnrExamPassDiscipline.examGroup().fromAlias("d"), "group")
                        .joinEntity("d", DQLJoinType.inner, EnrExamGroupScheduleEvent.class, "se", eq(property("se", EnrExamGroupScheduleEvent.examGroup().id()), property("group", EnrExamGroup.id())))
                        .column(property(EnrRequestedCompetition.id().fromAlias("rc")))
                        .column("se")
                        .where(eq(property("d", EnrExamPassDiscipline.passForm().internal()), value(true)))
                        .where(isNotNull(property(EnrExamPassDiscipline.examGroup().fromAlias("d"))))
                        .where(in(property(EnrRequestedCompetition.id().fromAlias("rc")), competitionsIds)));


                for (Object[] row: rows) {
                    SafeMap.safeGet(result, (Long)row[0], ArrayList.class).add((EnrExamGroupScheduleEvent)row[1]);
                }
            }
            });

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_ENTRANT_EXAMGROUP_MAP, result);
    }

    public static List<Long> getPartnerEduInstitutionList(IReportPrintInfo printInfo)
    {
        List<Long> list = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_PARTNER_EDU_INSTITUTION);
        if (null == list) return Collections.emptyList();

        return list;
    }

    public static void prefetchPassPartnerEduInstitution(IReportPrintInfo printInfo)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_PARTNER_EDU_INSTITUTION) != null) return;

        final List<Long> result = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(EnrPartnerEduInstitution.class, "e").column(property(EnrPartnerEduInstitution.eduInstitution().id().fromAlias("e"))));

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_PARTNER_EDU_INSTITUTION, result);
    }



    public static List<EnrOlympiadDiploma> getEntrantOlympiad(IReportPrintInfo printInfo, EnrEntrant entrant)
    {
        if (null == entrant) return Collections.emptyList();

        Map<Long, List<EnrOlympiadDiploma>> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_DIRECTION_OLYMPIAD_MAP);
        if (null == map) return Collections.emptyList();

        return map.get(entrant.getId());
    }

    public static void prefetchEntrantOlympiad(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_DIRECTION_OLYMPIAD_MAP) != null) return;

        final Map<Long, List<EnrOlympiadDiploma>> result = new HashMap<>();

        final List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> entrantIds) {

                List<Object[]> list = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(EnrEntrantBaseDocument.class, "e")
                        .joinEntity("e", DQLJoinType.inner, EnrOlympiadDiploma.class, "od", eq(property("od.id"), property("e", EnrEntrantBaseDocument.docRelation().document().id())))
                        .column(property("e"))
                        .column(property("od"))
                    .where(in(property(EnrEntrantBaseDocument.entrant().id().fromAlias("e")), entrantIds)));

                for (Object[] obj: list)
                {
                    EnrEntrantBaseDocument baseDocument = (EnrEntrantBaseDocument) obj[0];
                    EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) obj[1];
                    SafeMap.safeGet(result, baseDocument.getEntrant().getId(), ArrayList.class).add(diploma);
                }
            }
        });

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_DIRECTION_OLYMPIAD_MAP, result);
    }

    public static Map<Integer, Map<Long, Object[]>> getStateExamSubjectMarkMap(IReportPrintInfo printInfo, EnrEntrant entrant)
    {
        Map<Long, Map<Integer, Map<Long, Object[]>>> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP);

        return map.get(entrant.getId());
    }

    public static void prefetchStateExamSubjectMarkMap(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex, int stateExamIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP) != null) return;

        // entrantId -> year -> stateExamSubjectId -> [mark (int), certNumber (String)]
        Map<Long, Map<Integer, Map<Long, Object[]>>> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        List<Long> stateExamIds = getEntityIds(rows, stateExamIndex, 1, EnrEntrantStateExamResult.class);
        final List<IDQLExpression> seInExpressions = new ArrayList<>();
        {
            BatchUtils.execute(stateExamIds, 512, new BatchUtils.Action<Long>()
                {
                @Override public void execute(Collection<Long> elements){ seInExpressions.add(in(property("e", EnrEntrantStateExamResult.id()), elements)); }
                });
        }

        if (!entrantIds.isEmpty() && !seInExpressions.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(EnrEntrantStateExamResult.class, "e")
            .column(property("e", EnrEntrantStateExamResult.entrant().id()))
            .column(property("e", EnrEntrantStateExamResult.year()))
            .column(property("e", EnrEntrantStateExamResult.subject().id()))
            .column(property("e", EnrEntrantStateExamResult.markAsLong()))
            .column(property("e", EnrEntrantStateExamResult.documentNumber()))

            .order(property("e", EnrEntrantStateExamResult.entrant().id()))
            .order(property("e", EnrEntrantStateExamResult.year()), OrderDirection.desc)
            .order(property("e", EnrEntrantStateExamResult.subject().id()))

            .where(isNotNull(property("e", EnrEntrantStateExamResult.markAsLong())))
            .where(or(seInExpressions.toArray(new IDQLExpression[seInExpressions.size()])));

            if (entrantIds.size() < 1000)
            {
                builder.where(in(property(EnrEntrantStateExamResult.entrant().id().fromAlias("e")), entrantIds));
            } else
            {
                EnrEnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);
                if (enrollmentCampaign != null)
                    builder.where(eq(property(EnrEntrantStateExamResult.entrant().enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)));
            }

            List<Object[]> rowList = ISharedBaseDao.instance.get().getList(builder);
            for (Object[] row : rowList)
            {
                Long entrantId = (Long) row[0];
                Integer year = (Integer) row[1];
                Long subjectId = (Long) row[2];
                int mark = (int)((Long) row[3]/ 1000L);
                String number = (String) row[4];

                Map<Integer, Map<Long, Object[]>> yearMap = SafeMap.safeGet(result, entrantId, LinkedHashMap.class);
                Map<Long, Object[]> subjectMap = SafeMap.safeGet(yearMap, year, HashMap.class);

                subjectMap.put(subjectId, new Object[]{ mark, number });
            }
        }

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP, result);
    }

    public static EnrEnrollmentExtract getEnrollmentExtract(IReportPrintInfo printInfo, EnrRequestedCompetition direction)
    {
        Map<Long, EnrEnrollmentExtract> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_EXTRACT_MAP);

        return map.get(direction.getId());
    }

    public static void prefetchEnrollmentExtractMap(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_EXTRACT_MAP) != null) return;

        Map<Long, EnrEnrollmentExtract> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        if (!entrantIds.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e");

            if (entrantIds.size() < 1000)
            {
                builder.where(in(property(EnrEnrollmentExtract.entity().request().entrant().id().fromAlias("e")), entrantIds));
            } else
            {
                EnrEnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);
                if (enrollmentCampaign != null)
                    builder.where(eq(property(EnrEnrollmentExtract.entity().request().entrant().enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)));
            }

            List<EnrEnrollmentExtract> rowList = ISharedBaseDao.instance.get().getList(builder);

            for (EnrEnrollmentExtract extract : rowList)
                result.put(extract.getEntity().getId(), extract);
        }

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_EXTRACT_MAP, result);
    }

    public static void prefetchRatingData(IReportPrintInfo printInfo, List<Object[]> rows, int directionIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENR_RATING_DATA) != null) return;

        final List<Long> reqCompIds = getEntityIds(rows, directionIndex, 3, EnrRequestedCompetition.class);

        final  Map<Long, EnrRatingItem> ratingItemMap = new HashMap<>();
        for (EnrRatingItem item : IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.requestedCompetition().id(), reqCompIds)) {
            ratingItemMap.put(item.getRequestedCompetition().getId(), item);
        }

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_ENR_RATING_DATA, ratingItemMap);
    }

    public static void prefetchAchievementData(IReportPrintInfo printInfo, List<Object[]> rows, int requestIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ACHIEVEMENTS) != null) return;

        List<Long> requestIds = getEntityIds(rows, requestIndex, 2, EnrEntrantRequest.class);

        List<EnrEntrantRequest> requests = IUniBaseDao.instance.get().getList(EnrEntrantRequest.class, EnrEntrantRequest.id(), requestIds);

        final Map<Long, List<EnrEntrantAchievement>> achievementMap = new HashMap<>();

        final Map<Long, List<EnrEntrantAchievement>> tempMap = new HashMap<>();

        final Set<Long> entrantIds = CommonBaseEntityUtil.getPropertiesSet(requests, EnrEntrantRequest.entrant().id());

        BatchUtils.execute(entrantIds, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            DQLSelectBuilder achievementBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "ea")
                    .where(in(property("ea", EnrEntrantAchievement.entrant().id()), elements));

            for(EnrEntrantAchievement achievement : IUniBaseDao.instance.get().<EnrEntrantAchievement>getList(achievementBuilder))
            {
                SafeMap.safeGet(tempMap, achievement.getEntrant().getId(), ArrayList.class).add(achievement);
            }

        });

        for (EnrEntrantRequest request : requests) {
            final List<EnrEntrantAchievement> enrEntrantAchievements = tempMap.get(request.getEntrant().getId());
            if (enrEntrantAchievements == null) continue;

            for (EnrEntrantAchievement achievement : enrEntrantAchievements)
            {
                if (
                        (Boolean.FALSE.equals(achievement.getType().isForRequest())
                                && request.getType().equals(achievement.getType().getAchievementKind().getRequestType())
                        )
                                || (Boolean.TRUE.equals(achievement.getType().isForRequest()) && request.equals(achievement.getRequest())))
                {
                    SafeMap.safeGet(achievementMap, request.getId(), ArrayList.class).add(achievement);
                }
            }
        }

        if(requestIds == null || requestIds.isEmpty())
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_ACHIEVEMENTS, Collections.emptyMap());
        else
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_ACHIEVEMENTS, achievementMap);
    }

    public static List<EnrEntrantAchievement> getEntrantAchievements(IReportPrintInfo printInfo, Long requestId)
    {
        Map<Long, List<EnrEntrantAchievement>> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ACHIEVEMENTS);
        return map.get(requestId);
    }

    public static EnrRatingItem getRatingItem(IReportPrintInfo printInfo, Long competitionId)
    {
        Map<Long, EnrRatingItem> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENR_RATING_DATA);
        return map.get(competitionId);
    }

    public static void prefetchMarkData(IReportPrintInfo printInfo, List<Object[]> rows, int directionIndex) {

        final Map<Long, Map<EnrCampaignDiscipline, EnrChosenEntranceExam>> result = SafeMap.get(HashMap.class);

        final List<Long> reqCompIds = getEntityIds(rows, directionIndex, 3, EnrRequestedCompetition.class);

        BatchUtils.execute(reqCompIds, 256, new BatchUtils.Action<Long>()
            {
            @Override
            public void execute(Collection<Long> elements)
            {
                DQLSelectBuilder chosenDQL = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExam.class, "exam")
                .order(property("exam.id")) // чтобы в мапе хотя бы были одни и те же ВВИ при дублировании ссылок на акт. ВИ
                .column(property("exam"))
                .where(in(property(EnrChosenEntranceExam.requestedCompetition().id().fromAlias("exam")), elements))
                .fetchPath(DQLJoinType.inner, EnrChosenEntranceExam.actualExam().fromAlias("exam"), "act_exam")
                .fetchPath(DQLJoinType.inner, EnrExamVariant.examSetElement().fromAlias("act_exam"), "exam_set_el")
                .fetchPath(DQLJoinType.left, EnrChosenEntranceExam.maxMarkForm().fromAlias("exam"), "max_form")
                .fetchPath(DQLJoinType.left, EnrChosenEntranceExamForm.markSource().fromAlias("max_form"), "mark_src")
                ;

                for (EnrChosenEntranceExam chosenEntranceExam : IUniBaseDao.instance.get().<EnrChosenEntranceExam>getList(chosenDQL)) {
                    result.get(chosenEntranceExam.getRequestedCompetition().getId()).put(chosenEntranceExam.getDiscipline(), chosenEntranceExam);
                }
            }
            });


        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_CHOSEN_EXAM_MAP, result);
    }

    public static Map<EnrCampaignDiscipline, EnrChosenEntranceExam> getChosenExamMap(IReportPrintInfo printInfo, Long competitionId)
    {
        Map<Long, Map<EnrCampaignDiscipline, EnrChosenEntranceExam>> map = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_CHOSEN_EXAM_MAP);
        return map.get(competitionId);
    }

    public static void prefetchStateExamResults(IReportPrintInfo printInfo, List<Object[]> rows, int stateExamResultIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_STATE_EXAM_RESULTS) != null) return;
        Map<Long, List<EnrEntrantStateExamResult>> result = SafeMap.get(ArrayList.class);
        DQLSelectBuilder builder = new DQLSelectBuilder()
        .fromEntity(EnrEntrantStateExamResult.class, "ser")
        .fetchPath(DQLJoinType.inner, EnrEntrantStateExamResult.subject().fromAlias("ser"))
        .order(property("ser", EnrEntrantStateExamResult.year()), OrderDirection.desc)
        .order(property("ser", EnrEntrantStateExamResult.subject().code()));

        final List<IDQLExpression> inExpressions = new ArrayList<>();
        BatchUtils.execute(getEntityIds(rows, stateExamResultIndex, 1, EnrEntrantStateExamResult.class), 512, new BatchUtils.Action<Long>()
            {
            @Override
            public void execute(Collection<Long> elements)
            {
                inExpressions.add(in(property("ser", EnrEntrantStateExamResult.id()), elements));
            }
            });

        builder.where(inExpressions.isEmpty() ? nothing() : or(inExpressions.toArray(new IDQLExpression[inExpressions.size()])));

        for (EnrEntrantStateExamResult stateExamResult: IUniBaseDao.instance.get().<EnrEntrantStateExamResult>getList(builder))
            result.get(stateExamResult.getEntrant().getId()).add(stateExamResult);

        printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_STATE_EXAM_RESULTS, result);
    }

    public static void prefetchCountEntrantsTotalData(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_ENTRANTS) != null) return;
        List<Long> entrantsIds = getEntityIds(rows, entrantIndex, 1, EnrEntrant.class);
        if(entrantsIds == null || entrantsIds.isEmpty())
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_ENTRANTS, 0);
        else
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_ENTRANTS, Sets.newHashSet(entrantsIds).size());
    }

    public static void prefetchCountRequestsTotalData(IReportPrintInfo printInfo, List<Object[]> rows, int requestIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_REQUESTS) != null) return;

        List<Long> requestsIds = getEntityIds(rows, requestIndex, 2, EnrEntrantRequest.class);
        if(requestsIds == null || requestsIds.isEmpty())
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_REQUESTS, 0);
        else
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_REQUESTS, Sets.newHashSet(requestsIds).size());
    }


    public static void prefetchCountCompetitionsTotalData(IReportPrintInfo printInfo, List<Object[]> rows, int competitionIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_COMPETITIONS) != null) return;
        List<Long> competitionsIds = getEntityIds(rows, competitionIndex, 3, EnrRequestedCompetition.class);
        if(competitionsIds == null || competitionsIds.isEmpty())
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_COMPETITIONS, 0);
        else
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_COMPETITIONS, Sets.newHashSet(competitionsIds).size());
    }

    public static List<EnrEntrantStateExamResult> getStateExamResults(IReportPrintInfo printInfo, EnrEntrant entrant)
    {
        return printInfo.<Map<Long, List<EnrEntrantStateExamResult>>>getSharedObject(EnrReportPersonAdd.PREFETCH_STATE_EXAM_RESULTS).get(entrant.getId());
    }

    private static void _fillIdsFromMap(Collection<Long> ids, Object o, int levels, Class<?> check)
    {
        // пустые значения пропускаем
        if (null == o) { return; }

        // если мы должны взять значение (ключ)
        if (0 == levels) {

            // предварительная обработка для Map.Entry
            if (o instanceof Map.Entry) {
                o = ((Map.Entry)o).getKey(); // значение берется из ключа
                if (null == o) { return; } // что, впринципе, бред... но чего только не случается
            }

            if (null != check) {
                if (!check.isInstance(o)) {
                    throw new ClassCastException(String.valueOf(ClassUtils.getUserClass(o)) + " is not instance of " + check);
                }
            }

            // обработка ключей
            if (o instanceof Long) { ids.add((Long)o); }
            else if (o instanceof IEntity) { ids.add(((IEntity) o).getId()); }
            else { throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o))); }

            // значение получили - сохранили его в ids, модем выходить
            return;
        }

        // в противном случае - мы долдны получить список для дальнейшей обработки

        // предварительная обработка для Map.Entry
        if (o instanceof Map.Entry) {
            o = ((Map.Entry)o).getValue(); // коллекция берется из значения
            if (null == o) { return; } // что, впринципе, бред... но чего только не случается
        }

        // обработка значений
        if (o instanceof Collection) {
            for (Object item : (Collection)o) {
                if (null == item) { continue; }
                _fillIdsFromMap(ids, item, levels-1, check);
            }
        } else if (o instanceof Map) {
            for (Object entry : ((Map)o).entrySet()) {
                if (null == entry) { continue; }
                _fillIdsFromMap(ids, entry, levels-1, check);
            }
        } else {
            throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o)));
        }
    }

    private static List<Long> getEntityIds(List<Object[]> rows, int entityListIndex, int maplevel, Class<?> check) {
        List<Long> ids = new ArrayList<>();
        for (Object[] row : rows) {
            _fillIdsFromMap(ids, row[entityListIndex], maplevel, check);
        }
        return ids;
    }


    private static List<Long> getEntrantIds(List<Object[]> rows, int entrantListIndex)
    {
        return getEntityIds(rows, entrantListIndex, 1, EnrEntrant.class);
    }

}
