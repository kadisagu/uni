/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.tandemframework.core.debug.Debug;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.ArrayList;
import java.util.List;

/**
* @author oleyba
* @since 4/2/15
*/ // информация о строке шага зачисления
public class EnrSelectionItem
{
    private final EnrSelectionGroup group;
    private final IEnrEnrollmentStepItem stepItem;
    private EnrSelectionSkipReason skipReason;
    private boolean selected;

    // те конкурсы, с которых будет осуществлено перезачисление (перевыбор) на данный - в них места будут освобождаться
    private final List<IEnrSelectionItemPrevEnrollmentInfo> reenrollFromList = new ArrayList<>(2);
    // те конкурсы, для которых будет осуществлено повторное зачисление (повторный выбор) - в них места не освобождаются
    private final List<IEnrSelectionItemPrevEnrollmentInfo> addEnrollList = new ArrayList<>(2);

    public EnrSelectionItem(final EnrSelectionGroup group, final IEnrEnrollmentStepItem stepItem) {
        this.group = group;
        this.stepItem = stepItem;
    }

    public EnrSelectionGroup getGroup() { return this.group; }

    public IEnrEnrollmentStepItem getStepItem() { return this.stepItem; }
    public EnrRatingItem getRatingItem() { return this.getStepItem().getEntity(); }
    public EnrEntrant getEntrant() { return this.getRatingItem().getEntrant(); }
    public EnrCompetition getCompetition() { return this.getRatingItem().getCompetition(); }

    public EnrRequestedCompetition getRequestedCompetition() { return this.getRatingItem().getRequestedCompetition(); }

    public String getEntrantFio() { return this.getEntrant().getFio(); }
    public String getEntrantTitle() { return this.getEntrantFio() + ", prio=" + this.getRequestedCompetition().getPriority(); }
    public String getCompetitionTitle() { return this.getRequestedCompetition().getTitle() + ", prio=" + this.getRequestedCompetition().getPriority();  }

    public EnrSelectionSkipReason getSkipReason() { return this.skipReason; }
    protected void setSkipReason(final EnrSelectionSkipReason skipReason) { this.skipReason = skipReason; }
    public boolean isSkipped() { return (null != this.getSkipReason()); }

    public boolean isSelected() { return this.selected; }
    public void setSelected(final boolean selected) { this.selected = selected; }

    // перезачислить с этих конкурсов на данных
    public List<IEnrSelectionItemPrevEnrollmentInfo> getReenrollFromList() { return this.reenrollFromList; }
    // зачислить повторно
    public List<IEnrSelectionItemPrevEnrollmentInfo> getAddEnrollList() { return this.addEnrollList; }

    public String getSelectedInfo() {
        EnrSelectionSkipReason r = getSkipReason();
        if (null == r) { return null; }
        return r.getInfo();
    }

    // действие: пропустить (сбрасывает признак «выбран»)
    public void skip(final EnrSelectionSkipReason reason)
    {
        if (group.getContext().isDebug()) {
            Debug.message("## " + this.getEntrantFio() + ", " + this.getCompetitionTitle() + ": " + reason.getInfo());
            if (this.isSelected()) {
                Debug.message("-- " + this.getEntrantFio() + ", " + this.getCompetitionTitle() + ": " + reason.getInfo());
            }
        }

        this.setSelected(false);
        this.setSkipReason(reason.then(this.getSkipReason()));
        group.getContext().change(); // что-то поменялось
    }

    // действие: выбрать
    public void select()
    {
        if (this.isSkipped()) { throw new IllegalStateException("alredy-skipped"); }
        if (this.isSelected()) { throw new IllegalStateException("alredy-selected"); }

        if (group.getContext().isDebug()) {
            Debug.message("++ " + this.getEntrantFio() + ", " + this.getCompetitionTitle());
        }

        this.setSelected(true);
        group.getContext().change(); // что-то поменялось
    }
}
