/* $Id: EnrollmentOrderPrintScript.groovy 34426 2014-05-26 09:55:08Z oleyba $ */
// Copyright 2006-2012 Tandem Service Software
package unienr14.scripts.order

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrEnrollmentOrderPrintUtil
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EnrollmentOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 *
 * @author Vasily Zhukov
 * @since 29.12.2011
 */
class EnrollmentOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    List<IAbstractParagraph> paragraphList

    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        def academy = TopOrgUnit.instance
        paragraphList = order.paragraphList

        // заполнение меток
        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.actionDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)

        im.put('reasonText', order.orderBasicText)
        im.put('orderDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))
        im.put('executorFio', order.executor)

        if (paragraphList.size() > 0 && paragraphList[0] instanceof EnrEnrollmentParagraph)
        {
            EnrEnrollmentOrderPrintUtil.initOrgUnit(im,
                    ((EnrEnrollmentParagraph) paragraphList[0]).formativeOrgUnit, 'formativeOrgUnit', '')
        }

        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        def document = new RtfReader().read(template)
        fillParagraphs(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    void fillParagraphs(RtfHeader header)
    {
        List<IRtfElement> paragraphList = []
        for (IAbstractParagraph abstractParagraph : order.paragraphList)
        {
            if (!(abstractParagraph instanceof EnrEnrollmentParagraph))
                throw new ApplicationException("Параграф №${abstractParagraph.number} в приказе «" + abstractParagraph.order.title + "» не может быть напечатан, так как не является параграфом о зачислении.")

            def paragraph = (EnrEnrollmentParagraph) abstractParagraph
            def enrollmentExtractList = (List<EnrEnrollmentExtract>) paragraph.extractList
            if (enrollmentExtractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «" + paragraph.order.title + "» не может быть напечатан.")

            def firstExtract = enrollmentExtractList.get(0)
            def durations = IUniBaseDao.instance.get().<String> getList(new DQLSelectBuilder().fromEntity(EnrProgramSetItem.class, "i")
                    .predicate(DQLPredicateType.distinct)
                    .column(property("d", EduProgramDuration.P_TITLE))
                    .joinPath(DQLJoinType.inner, EnrProgramSetItem.program().duration().fromAlias("i"), "d")
                    .where(eqValue(property("i", EnrProgramSetItem.L_PROGRAM_SET), ((EnrRequestedCompetition) firstExtract.entity).competition.programSetOrgUnit.programSet))
                    .order(property("d", EduProgramDuration.P_TITLE))
            )

            // заполняем метки в шаблоне параграфа
            def injectModifier = new RtfInjectModifier()
                    .put('parNumber', String.valueOf(paragraph.number))
                    .put('developForm', paragraph.programForm.shortTitle)
                    .put('developPeriod', UniStringUtils.joinNotEmpty(durations, ", "))
                    .put('educationOrgUnit', paragraph.programSubject.titleWithCode)
                    .put('bachelorType', getParagraphBachelorTypeStr(paragraph))

            def programKindCode = paragraph.programSubject.eduProgramKind.code
            def types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ? UniRtfUtil.SPECIALITY_CASES : (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ? UniRtfUtil.EDU_PROFESSION_CASES : UniRtfUtil.EDU_DIRECTION_CASES)
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                injectModifier.put('educationType' + UniRtfUtil.CASE_POSTFIX.get(i), types[i])
            }

            byte[] paragraphTemplate = EnrEnrollmentOrderPrintUtil.getEnrollmentOrderParagraphTemplate(paragraph.getId())
            def paragraphDocument = new RtfReader().read(paragraphTemplate)
            def tableModifier = new RtfTableModifier()

            if (paragraph.competitionType.code.equals(EnrCompetitionTypeCodes.TARGET_ADMISSION))
            {
                def studentList = new ArrayList<String[]>(enrollmentExtractList.size())
                int counter = 1
                for (def extract : enrollmentExtractList)
                {
                    def requestedEnrollmentDirection = (EnrRequestedCompetitionTA) extract.entity
                    def person = requestedEnrollmentDirection.request.entrant.person
                    EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
                    def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble()

                    studentList.add([
                            counter++,
                            person.fullFio,
                            sumMark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "",
                            requestedEnrollmentDirection.targetAdmissionKind.targetAdmissionKind.title + "(" + requestedEnrollmentDirection.targetAdmissionOrgUnit.title + ")",
                            requestedEnrollmentDirection.contractNumber
                    ] as String[])
                }

                tableModifier.put('STUDENT_LIST', studentList as String[][])

            }
            else
            {
                // получаем rtf-строку студентов из параграфа
                def rtf = new RtfString().par()
                int counter = 1
                for (def extract : enrollmentExtractList)
                {
                    def requestedEnrollmentDirection = extract.entity
                    def person = requestedEnrollmentDirection.request.entrant.person
                    rtf.append((counter++).toString()).append('.  ').append(person.fullFio)

                    def levelTitle = requestedEnrollmentDirection.request.eduDocument.eduLevel?.shortTitle
                    if (levelTitle)
                        rtf.append(' (').append(levelTitle).append(')')

                    EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
                    def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble()
                    if (sumMark)
                        rtf.append(' ').append(IRtfData.ENDASH).append(' ').append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark))
                    rtf.par()
                }

                injectModifier.put('STUDENT_LIST', rtf)
            }

            // подготавливаем клон шаблона параграфа для вставки в печатную форму приказа
            def paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            injectModifier.modify(paragraphPart)
            tableModifier.modify(paragraphPart)

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }

        im.put('PARAGRAPHS', paragraphList)
    }

    String getParagraphBachelorTypeStr(EnrEnrollmentParagraph paragraph) {

        def orientationsTitles = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, 'e')
                .predicate(DQLPredicateType.distinct)
                .column(property('i', EnrProgramSetItem.program().programOrientation().title()))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().competition().programSetOrgUnit().programSet().fromAlias('e'), 's')
                .joinEntity('s', DQLJoinType.inner, EnrProgramSetItem.class, 'i', eq(property('s'), property('i', EnrProgramSetItem.L_PROGRAM_SET)))
                .where(eq(property('e', EnrEnrollmentExtract.paragraph()), value(paragraph)))
                .where(eq(property('i', EnrProgramSetItem.program().programOrientation().subjectIndex().code()), value(EduProgramSubjectIndexCodes.TITLE_2013_03)))
                .createStatement(session).<String>list()

        return orientationsTitles.size() == 1 ? orientationsTitles.get(0) : ''
    }
}
