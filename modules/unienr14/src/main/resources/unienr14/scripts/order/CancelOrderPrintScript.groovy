/* $Id: EnrollmentRevertOrderPrintScript.groovy 33756 2014-04-21 10:15:49Z nfedorovskih $ */
package unienr14.scripts

import org.hibernate.Session
import org.tandemframework.core.common.ITitled
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract
import ru.tandemservice.unienr14.order.entity.EnrCancelParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractOrderGen
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EnrollmentRevertOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 08.08.2013
 */

class EnrollmentRevertOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def rtfFactory = RtfBean.elementFactory
    def qcElement = rtfFactory.createRtfControl(IRtfData.QC) // выравнивание по центру
    def qjElement = rtfFactory.createRtfControl(IRtfData.QJ) // выравнивание по ширине
    def parElement = rtfFactory.createRtfControl(IRtfData.PAR) // перевод строки (каретка)

    def print()
    {
        def academy = TopOrgUnit.instance
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()
        RtfDocument doc = new RtfReader().read(template)

        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.actionDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)
        im.put('PARAGRAPHS', getParagraphs(doc.header))

        // заполнение виз
        def visaGroupList = IUniBaseDao.instance.get().getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }

        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        im.modify(doc)
        tm.modify(doc)
        return [document: RtfUtil.toByteArray(doc), fileName: 'EnrollmentRevertOrder.rtf']
    }

    def List<IRtfElement> getSubParagraphs(IAbstractParagraph paragraph)
    {
        Map<EnrCompetition, List<EnrCancelExtract>> map = [:]
        List<EnrCancelExtract> extractList = new DQLSelectBuilder().fromEntity(EnrCancelExtract.class, "e")
                .where(eq(property("e", EnrCancelExtract.paragraph()), value(paragraph)))
                .order(property("e", EnrCancelExtract.requestedCompetition().request().entrant().person().identityCard().fullFio()))
                .createStatement(session).list()

        extractList.each {
            List<EnrCancelExtract> list = map.get(it.requestedCompetition.competition)
            if (list == null)
                map.put(it.requestedCompetition.competition, list = [])
            list.add(it)
        }

        List<IRtfElement> elements = []

        List<EnrCompetition> competitions = new ArrayList<>(map.keySet())
        Collections.sort(competitions, ITitled.TITLED_COMPARATOR);

        for (EnrCompetition competition : competitions)
        {
            elements.add(qcElement)

            List<EnrProgramSetItem> programs = IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), competition.getProgramSetOrgUnit().getProgramSet());

            def rtfString = new RtfString()
                    .boldBegin()
                    .append('по ')
                    .append(getLevelTypeStr_G(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind()))
                    .append(' «').append(competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitleWithCode()).append('»')
                    .boldEnd()
                    .par()
                    .append('(')
                    .append(competition.getProgramSetOrgUnit().getProgramSet().getProgramForm().getTitle().toLowerCase()).append(' форма обучения')
                    .append(', ')
                    .append(UniStringUtils.joinUniqueSorted(programs, EnrProgramSetItem.program().duration().title().s(), ", "))
                    .append(')')

            elements.addAll(rtfString.toList())
            elements.addAll([parElement, parElement, qjElement])

            rtfString = new RtfString()
            int idx = 1
            for (EnrCancelExtract revertExtract : map.get(competition))
            {
                rtfString.append(String.valueOf(idx++))
                rtfString.append('. ')
                rtfString.append(revertExtract.requestedCompetition.request.entrant.person.identityCard.fullFio)
                rtfString.par()
            }
            elements.addAll(rtfString.toList())
            elements.add(parElement)
        }

        return elements
    }

    def List<IRtfElement> getParagraphs(RtfHeader header)
    {
        byte[] tpl =IUniBaseDao.instance.get().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.ORDER_PAR_CANCEL).getCurrentTemplate();
        RtfDocument paragraphDocument = new RtfReader().read(tpl)

        List<IRtfElement> paragraphList = []
        order.paragraphList.each {
            RtfDocument paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            EnrAbstractOrder canceledOrder = (it as EnrCancelParagraph).cancelledOrder
            new RtfInjectModifier()
                    .put('parNumber', it.number.toString())
                    .put('canceledOrderDate', canceledOrder.commitDate?.format('dd.MM.yyyy'))
                    .put('canceledOrderNumber', canceledOrder.number)
                    .put('STUDENT_LIST', getSubParagraphs(it))
                    .modify(paragraphPart)

            def group = rtfFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }
        return paragraphList
    }

    static String getLevelTypeStr_G(EduProgramKind levelType)
    {
        String programKindCode = levelType.getCode();

        if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode)) {
            return "специальности";
        }
        if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode)) {
            return "профессии";
        }
        return "направлению";
    }

}