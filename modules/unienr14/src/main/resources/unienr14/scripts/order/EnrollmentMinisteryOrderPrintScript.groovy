/* $Id: EnrollmentOrderPrintScript.groovy 34426 2014-05-26 09:55:08Z oleyba $ */
// Copyright 2006-2012 Tandem Service Software
package unienr14.scripts.order

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest
import ru.tandemservice.unimove.IAbstractExtract
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new EnrollmentMinisteryOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказа о зачислении студентов по направлению от министерства
 */
class EnrollmentMinisteryOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        def academy = TopOrgUnit.instance

        // заполнение меток
        im.put('highSchoolTitle', academy.title)
        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('reasonText', order.orderBasicText)
        im.put('orderDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)
        im.put('executorFio', order.executor)

        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        im.put('PARAGRAPHS', getParagraphsAsRtfElements())

        // загрузить шаблон приказа
        def document = new RtfReader().read(template)

        // применить значения меток
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    // получить параграфы как готовые rtf элементы
    List<IRtfElement> getParagraphsAsRtfElements(){
        // загрузить шаблон параграфа
        byte[] tpl = IUniBaseDao.instance.get().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.ORDER_PAR_ENROLL_MIN).getCurrentTemplate();
        RtfDocument paragraphTemplate = new RtfReader().read(tpl)
        // преобразовать коллекцию параграфов в rtf формат
        order.paragraphList.collect {
            // если неверный тип
            if (!(it instanceof EnrEnrollmentMinisteryParagraph))
                throw new ApplicationException("Параграф №${it.number} в приказе «${it.order.title}» не может быть напечатан, так как не является параграфом о зачислении по направлению от Минобрнауки.")

            def paragraph = (EnrEnrollmentMinisteryParagraph) it
            // если нет выписок
            if (paragraph.extractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «${paragraph.order.title}» не может быть напечатан.")
            // задать значения меток
            RtfInjectModifier injectModifier = new RtfInjectModifier()
                    .put('parNumber', paragraph.number.toString())
                    .put('educationOrgUnit', paragraph.programSubject.titleWithCode)
                    .put('developForm', paragraph.programForm.shortTitle)
                    .put('developPeriod', UniStringUtils.joinNotEmpty(getDurations(paragraph), ', '))
                    .put('STUDENT_LIST', studentsListAsRTF(paragraph))
            putEducationTypes(injectModifier, paragraph)
            // получить копию шаблона параграфа, чтобы применять изменения к ней
            def paragraphTemplateCopy = paragraphTemplate.clone
            // применить значения меток
            injectModifier.modify(paragraphTemplateCopy)
            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = paragraphTemplateCopy.elementList
            group
        }
    }

    // Получить список студентов как готовый rtf элемент
    RtfString studentsListAsRTF(EnrEnrollmentMinisteryParagraph paragraph){
        // аггрегировать инф-цию о студентах в rtf строку
        paragraph.extractList.inject( new RtfString().par(), { RtfString rtf, IAbstractExtract extract ->
            // получить "Направление на обучение иностранного гражданина"
            def foreignRequest = (EnrEntrantForeignRequest) extract.entity
            // получить студента
            def person = foreignRequest.entrant.person
            // <порядковый номер абитуриента в параграфе>. <ФИО абитуриента полностью> (<название гражданства>) — <номер министерского направления>
            rtf.append("${extract.number}.  ${person.fullFio} (${person.identityCard.citizenship.title}) — ${foreignRequest.regNumber}").par()
        })
    }

    // установить educationType во всех падежах
    void putEducationTypes(RtfInjectModifier injectModifier, EnrEnrollmentMinisteryParagraph paragraph) {
        def programKindCode = paragraph.programSubject.eduProgramKind.code
        def types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ?
                UniRtfUtil.SPECIALITY_CASES :
                (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ?
                        UniRtfUtil.EDU_PROFESSION_CASES :
                        UniRtfUtil.EDU_DIRECTION_CASES)

        (0 ..< UniRtfUtil.CASE_POSTFIX.size()).each { i ->
            injectModifier.put('educationType' + UniRtfUtil.CASE_POSTFIX.get(i), types[i])
        }
    }

    // получить список сроков обучения
    Collection<String> getDurations(EnrEnrollmentMinisteryParagraph paragraph) {
        def firstExtract = paragraph.extractList.get(0)
        // все наборы ОП(специальности профессии) из выписок этого параграфа
        def programSetIds = paragraph.extractList.collect { extr ->
            ((EnrEntrantForeignRequest) extr.entity).competition.programSetOrgUnit.programSet.id
        }

        if (((EnrOrder)paragraph.order).requestType.code.equals(EnrRequestTypeCodes.SPO)) {
            IUniBaseDao.instance.get().getList(
                    new DQLSelectBuilder()
                            .fromEntity(EduProgramSecondaryProf.class, "spo")
                            .column(property("spo", EduProgramSecondaryProf.duration().title()))
                            .distinct()
                            .where(exists(new DQLSelectBuilder()
                                .fromEntity(EnrProgramSetSecondary.class, "s")
                                .where(DQLExpressions.in(property("s", EnrProgramSetSecondary.id()), programSetIds))
                                .where(eq(property("spo", EduProgramSecondaryProf.id()), property("s", EnrProgramSetSecondary.program().id())))
                                .buildQuery()))
                            .where(eq(property("spo", EduProgramSecondaryProf.programSubject()), value(((EnrEntrantForeignRequest) firstExtract.entity).competition.programSetOrgUnit.programSet.programSubject)))
                            .order(property("spo", EduProgramSecondaryProf.duration().title()))
            )
        } else {
            IUniBaseDao.instance.get().<String> getList(new DQLSelectBuilder().fromEntity(EnrProgramSetItem.class, "i")
                    .predicate(DQLPredicateType.distinct)
                    .column(property("d", EduProgramDuration.P_TITLE))
                    .joinPath(DQLJoinType.inner, EnrProgramSetItem.program().duration().fromAlias("i"), "d")
                    .where(eqValue(property("i", EnrProgramSetItem.L_PROGRAM_SET), ((EnrEntrantForeignRequest) firstExtract.entity).competition.programSetOrgUnit.programSet))
                    .order(property("d", EduProgramDuration.P_TITLE))
            )
        }
    }
}
