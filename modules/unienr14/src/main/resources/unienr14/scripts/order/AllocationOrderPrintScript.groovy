/* $Id: EnrollmentOrderPrintScript.groovy 34426 2014-05-26 09:55:08Z oleyba $ */
// Copyright 2006-2012 Tandem Service Software
package unienr14.scripts.order

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrEnrollmentOrderPrintUtil
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
import static ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes.*

return new AllocationOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 *
 * @author Vasily Zhukov
 * @since 29.12.2011
 */
class AllocationOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    List<IAbstractParagraph> paragraphList

    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        def academy = TopOrgUnit.instance
        paragraphList = order.paragraphList

        // заполнение меток
        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.actionDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)

        im.put('reasonText', StringUtils.isEmpty(order.orderBasicText) ? "" : order.orderBasicText + " ")
        im.put('orderDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))
        im.put('executorFio', order.executor)

        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        def document = new RtfReader().read(template)
        fillParagraphs(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'AllocationOrder.rtf']
    }

    void fillParagraphs(RtfHeader header)
    {
        List<IRtfElement> paragraphList = []
        for (IAbstractParagraph abstractParagraph : order.paragraphList)
        {
            if (!(abstractParagraph instanceof EnrAllocationParagraph))
                throw new ApplicationException("Параграф №${abstractParagraph.number} в приказе «" + abstractParagraph.order.title + "» не может быть напечатан, так как не является параграфом о распределении по образовательным программам.")

            def paragraph = (EnrAllocationParagraph) abstractParagraph
            def allocationExtractList = (List<EnrAllocationExtract>) paragraph.extractList
            if (allocationExtractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «" + paragraph.order.title + "» не может быть напечатан.")

            def firstExtract = allocationExtractList.get(0)

            def reqCompetitions = CommonBaseEntityUtil.getPropertiesList(allocationExtractList, EnrAllocationExtract.entity());

            def requestedPrograms = IUniBaseDao.instance.get().<String> getList(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "rp")
                    .where(DQLExpressions.in(property("rp", EnrRequestedProgram.requestedCompetition()), reqCompetitions))
            )

            Map<Long, List<EnrRequestedProgram>> requestedProgramsMap = Maps.newHashMap();

            for(EnrRequestedProgram program : requestedPrograms)
            {
                SafeMap.safeGet(requestedProgramsMap, program.requestedCompetition.id, ArrayList.class).add(program)
            }

            // заполняем метки в шаблоне параграфа
            def injectModifier = new RtfInjectModifier()
                    .put('parNumber', paragraph.number.toString())
                    .put('developForm', paragraph.enrProgramSetItem.programSet.programForm.shortTitle)
                    .put('developPeriod', paragraph.enrProgramSetItem.program.duration.title)
                    .put('eduProgram', paragraph.enrProgramSetItem.title)
                    .put('group', firstExtract.groupTitle)
                    .put('formativeOrgUnit', paragraph.enrProgramSetOrgUnit.formativeOrgUnit.printTitle)



            def enrollmentOrders = IUniBaseDao.instance.get().<String> getList(new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ee")
                    .distinct()
                    .column(property("ee", EnrEnrollmentExtract.paragraph().order()))
                    .where(DQLExpressions.in(property("ee", EnrEnrollmentExtract.entity()), reqCompetitions))
            )

            if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(((EnrOrder)paragraph.order).compensationType.code))
            {
                injectModifier.put('compensationType', ", финансируемые из федерального бюджета")
            }
            else
            {
                injectModifier.put('compensationType', " с оплатой стоимости обучения")
            }

            if(enrollmentOrders.size() > 1)
            {
                injectModifier.put('oe', "ами")
            }
            else
            {
                injectModifier.put('oe', "ом")
            }

            List<String> enrollmentOrdersInfo = Lists.newArrayList()

            for(EnrAbstractOrder order : enrollmentOrders)
            {
                enrollmentOrdersInfo.add("№" + order.number + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.commitDate))
            }
            injectModifier.put('orders', StringUtils.join(enrollmentOrdersInfo, ", "))


            byte[] tpl =IUniBaseDao.instance.get().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.ORDER_PAR_ALLOC).getCurrentTemplate();
            RtfDocument paragraphDocument = new RtfReader().read(tpl)

            def tableModifier = new RtfTableModifier()

            def studentList = new ArrayList<String[]>(allocationExtractList.size())
            int counter = 1
            def ratingItems = IUniBaseDao.instance.get().<String> getList(new DQLSelectBuilder().fromEntity(EnrRatingItem.class, "ri")
                    .where(DQLExpressions.in(property("ri", EnrRatingItem.requestedCompetition()), reqCompetitions))
                    .order(property("ri", EnrRatingItem.totalMarkAsLong()), OrderDirection.desc)
            )

            Map<Long, EnrRatingItem> ratingItemsMap = Maps.newHashMap();

            for(EnrRatingItem item : ratingItems)
            {
                ratingItemsMap.put(item.requestedCompetition.id, item)
            }

            Collections.sort(allocationExtractList, new Comparator<EnrAllocationExtract>() {
                @Override
                int compare(EnrAllocationExtract o1, EnrAllocationExtract o2) {

                    int entrantCompare = o1.requestedCompetition.request.entrant.fullFio.compareTo(o2.requestedCompetition.request.entrant.fullFio)
                    if(entrantCompare == 0)
                        entrantCompare = o1.requestedCompetition.request.entrant.id.compareTo(o2.requestedCompetition.request.entrant.id)

                    def ratingItem1 = ratingItemsMap.get(o1.requestedCompetition.id);
                    def ratingItem2 = ratingItemsMap.get(o2.requestedCompetition.id);
                    if(ratingItem1 == null && ratingItem2 == null) return entrantCompare;
                    else if(ratingItem1 != null && ratingItem2 == null) return -1;
                    else if(ratingItem1 == null && ratingItem2 != null) return 1;
                    else
                    {
                        if(ratingItem1.getTotalMarkAsLong() == ratingItem2.getTotalMarkAsLong()) return entrantCompare;
                        else return ratingItem2.getTotalMarkAsLong() - ratingItem1.getTotalMarkAsLong()
                    }
                }
            })

            for (def extract : allocationExtractList) {
                def person = extract.requestedCompetition.request.entrant.person
                EnrRatingItem ratingItem = ratingItemsMap.get(extract.requestedCompetition.id)
                def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble()
                def reqPrograms = requestedProgramsMap.get(extract.requestedCompetition.id)

                if(reqPrograms != null && !reqPrograms.isEmpty()) {
                    Collections.sort(reqPrograms, new Comparator<EnrRequestedProgram>() {
                        @Override
                        int compare(EnrRequestedProgram o1, EnrRequestedProgram o2) {
                            return o1.priority - o2.priority
                        }
                    })
                }

                studentList.add([
                        counter++,
                        person.fullFio,
                        sumMark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "",
                        reqPrograms != null
                                ?
                                StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reqPrograms, EnrRequestedProgram.programSetItem().program().shortTitle()), ", ")
                                :
                                ""
                ] as String[])
            }
            tableModifier.put('T', studentList as String[][])

            // подготавливаем клон шаблона параграфа для вставки в печатную форму приказа
            def paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            injectModifier.modify(paragraphPart)
            tableModifier.modify(paragraphPart)

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }

        im.put('PARAGRAPHS', paragraphList)
    }


}
