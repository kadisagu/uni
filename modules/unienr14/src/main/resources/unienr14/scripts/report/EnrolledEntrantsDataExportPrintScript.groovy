package unienr14.scripts.report

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.Label
import org.apache.commons.lang.math.NumberUtils
import org.hibernate.Session
import org.tandemframework.shared.fias.base.entity.AddressBase
import org.tandemframework.shared.fias.base.entity.AddressDetailed
import org.tandemframework.shared.fias.base.entity.AddressInter
import org.tandemframework.shared.fias.base.entity.AddressItem
import org.tandemframework.shared.fias.base.entity.AddressRu
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA

return new EnrolledEntrantsDataExportPrint(
                session: session,
                extractIds: extractIds,
                ratingIds: ratingIds
).print()

/**
 * @author rsizonenko
 * @since 13.08.2014
 */



class EnrolledEntrantsDataExportPrint
{
    Session session
    List<Long> extractIds
    List<Long> ratingIds
    List<EnrEnrollmentExtract> extracts
    List<EnrRatingItem> ratings
    Map<EnrRequestedCompetition, EnrRatingItem> ratingMap = new HashMap<>()
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    def print()
    {

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);


        ratings = IUniBaseDao.instance.get().getList(EnrRatingItem.class, "id", ratingIds)
        for (EnrRatingItem item : ratings)
        {
            ratingMap.put(item.getRequestedCompetition(), item)
        }

        // создаем лист
        def sheet = workbook.createSheet("Данные зачисленных абитуриентов", 0);

        extracts = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, "id", extractIds)

        printTable(sheet)

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(),
                fileName: "entrant_data_export_" + new Date().format("dd.MM.yyyy") + ".xls"]

    }

    def printTable(def sheet)
    {
        int colN = 0;

        ["personalNumber", "lastName", "firstName", "middleName", "cardType", "seria", "number", "issuanceDate", "issuancePlace", "sex", "birthDate", "birthPlace",
         "citizenship", "country", "postcode", "region", "districtRegion", "settlement", "district", "street", "houseNumber", "houseUnitNumber", "flatNumber", "phoneDefault", "phoneMoblie",
         "regNumber", "programForm", "compensationType", "type", "institutionOrgUnit", "formativeOrgUnit", "programSubject", "programSet", "enrRating", "contractNumberTA",
         "OrgUnitTA", "commitDateOrder", "numberOrder"].each {
            sheet.addCell(new Label(colN++, 0, it))
        }

        int rowNumber = 1;
        for (EnrEnrollmentExtract extract : extracts)
        {
            EnrRequestedCompetition reqComp = extract.requestedCompetition;
            EnrRequestedCompetitionTA reqCompTA = reqComp instanceof EnrRequestedCompetitionTA ? reqComp : null;
            AddressBase address = reqComp.request.entrant.person.identityCard.address
            AddressDetailed addressDetailed = address instanceof AddressDetailed ? address : null
            AddressInter addressInter = addressDetailed instanceof AddressInter ? addressDetailed : null
            AddressRu addressRu = addressDetailed instanceof  AddressRu ? addressDetailed : null
            def districtCodes = [434, 202, 205, 101, 204, 206, 201, 312, 203, 526];


            AddressItem region = addressDetailed?.getSettlement()
            AddressItem districtRegion
            if (districtCodes.contains(region?.getAddressType()?.code)) districtRegion = region

            while (region?.parent != null) {
                region = region.parent
                if (districtRegion == null && districtCodes.contains(region.getAddressType()?.code)) districtRegion = region;
            };

            boolean doNotShowSettlement = (region?.getAddressType()?.code == 301 || region?.getAddressType()?.code == 103) && region.equals(addressDetailed?.getSettlement());

            if (districtRegion.equals(region)) districtRegion = null


            sheet.addCell(!NumberUtils.isNumber(reqComp.request.entrant.personalNumber) ? new Label(0, rowNumber, reqComp.request.entrant.personalNumber) : new jxl.write.Number(0, rowNumber,  Double.valueOf(reqComp.request.entrant.personalNumber)))
            sheet.addCell(new Label(1, rowNumber, reqComp.request.entrant.person.identityCard.lastName))
            sheet.addCell(new Label(2, rowNumber, reqComp.request.entrant.person.identityCard.firstName))
            sheet.addCell(new Label(3, rowNumber, reqComp.request.entrant.person.identityCard.middleName))
            sheet.addCell(new Label(4, rowNumber, reqComp.request.entrant.person.identityCard.cardType.title))
            sheet.addCell(new Label(5, rowNumber, reqComp.request.entrant.person.identityCard.seria))
            sheet.addCell(new Label(6, rowNumber, reqComp.request.entrant.person.identityCard.number))
            sheet.addCell(new Label(7, rowNumber, reqComp.request.entrant.person.identityCard.issuanceDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(8, rowNumber, reqComp.request.entrant.person.identityCard.issuancePlace))
            sheet.addCell(new Label(9, rowNumber, reqComp.request.entrant.person.identityCard.sex.code.equals(SexCodes.MALE) ? "муж" : "жен"))
            sheet.addCell(new Label(10, rowNumber, reqComp.request.entrant.person.identityCard.birthDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(11, rowNumber, reqComp.request.entrant.person.identityCard.birthPlace))
            sheet.addCell(new Label(12, rowNumber, reqComp.request.entrant.person.identityCard.citizenship.shortTitle))
            sheet.addCell(new Label(13, rowNumber, addressDetailed?.country?.title))
            sheet.addCell(new Label(14, rowNumber, addressDetailed?.getSettlement()?.getInheritedPostCode()))
            sheet.addCell(new Label(15, rowNumber, region?.title))
            sheet.addCell(new Label(16, rowNumber, districtRegion?.title))
            sheet.addCell(new Label(17, rowNumber, doNotShowSettlement ? "" : addressDetailed?.getSettlement()?.getTitleWithType()))
            sheet.addCell(new Label(18, rowNumber, addressRu != null ? addressRu.district?.title : addressInter?.district))
            sheet.addCell(new Label(19, rowNumber, addressRu != null ? addressRu.getStreet()?.title : addressInter?.getAddressLocation()))
            sheet.addCell(new Label(20, rowNumber, addressRu?.houseNumber))
            sheet.addCell(new Label(21, rowNumber, addressRu?.houseUnitNumber))
            sheet.addCell(new Label(22, rowNumber, addressRu?.flatNumber))


            sheet.addCell(new Label(23, rowNumber, reqComp.getRequest().getIdentityCard().getPerson().getContactData().getPhoneDefault()))
            sheet.addCell(new Label(24, rowNumber, reqComp.getRequest().getIdentityCard().getPerson().getContactData().getPhoneMobile()))
            sheet.addCell(new Label(25, rowNumber, reqComp.getRequest().getRegNumber()))
            sheet.addCell(new Label(26, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().title))
            sheet.addCell(new Label(27, rowNumber, reqComp.getCompetition().getType().getCompensationType().shortTitle))
            sheet.addCell(new Label(28, rowNumber, reqComp.getCompetition().getType().shortTitle))
            sheet.addCell(new Label(29, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().code))
            sheet.addCell(new Label(30, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit()?.getShortTitle()))
            sheet.addCell(new Label(31, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().titleWithCode))
            sheet.addCell(new Label(32, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getTitle()))
            sheet.addCell(new Label(33, rowNumber, ratingMap.get(reqComp)?.totalMarkAsString))
            sheet.addCell(new Label(34, rowNumber, reqCompTA?.contractNumber))
            sheet.addCell(new Label(35, rowNumber, reqCompTA?.targetAdmissionOrgUnit?.title))
            sheet.addCell(new Label(36, rowNumber, extract.order.commitDate.format("dd.MM.yyyy")))
            sheet.addCell(new Label(37, rowNumber++, extract.order.number))


        }
    }
}