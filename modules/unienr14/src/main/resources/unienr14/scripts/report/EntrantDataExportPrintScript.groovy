/** 
 *$Id:$
 */
// Copyright 2006-2012 Tandem Service Software
package unienr14.scripts.report

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.Label
import jxl.write.Number
import org.apache.commons.lang.math.NumberUtils
import org.hibernate.Session
import org.tandemframework.core.util.BatchUtils
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.shared.fias.base.entity.AddressBase
import org.tandemframework.shared.fias.base.entity.AddressDetailed
import org.tandemframework.shared.fias.base.entity.AddressRu
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition

import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new EntrantDataExportPrint(                           // входные параметры скрипта
        session: session,                                    // сессия
        parameters: parameters,                              // параметры для вывода в отчетную форму (если нужно)
        requestedCompetitionIds: requestedCompetitionIds        // выбранные конкурсы, подходящие под параметры отчета
).print()

/**
 * Алгоритм печати отчета Экспорт данных абитуриентов
 *
 * @author Alexander Shaburov
 * @since 02.08.12
 */
class EntrantDataExportPrint
{
  // входные параметры
  Session session
  List<String> parameters
  List<Long> requestedCompetitionIds

  // данные для печати, заполняются в методе prepareData
  ByteArrayOutputStream out = new ByteArrayOutputStream();
  List<EnrRequestedCompetition> requestedCompetitions
  def fatherMap // мапа Персоны на Места работы отца
  def matherMap // мапа Персоны на Места работы матери
  def foreignLanguageMap // мапа Персоны на список Иностранных языков персоны

    /**
   * Основной метод печати.
   */
  def print()
  {
    // создаем печатную форму
    WorkbookSettings ws = new WorkbookSettings();
    ws.setMergedCellChecking(false);
    ws.setRationalization(false);
    ws.setGCDisabled(true);
    def workbook = Workbook.createWorkbook(out, ws);

    // создаем лист
    def sheet = workbook.createSheet("Данные абитуриентов", 0);

    prepareData()

    printTable(sheet)

    workbook.write();
    workbook.close();

    // стандартные выходные параметры скрипта
    return [document: out.toByteArray(),
            fileName: "entrant_data_export_" + new Date().format("dd.MM.yyyy") + ".xls"]
  }

  /**
   * Подготавливает данные для печати отчета.
   */
  def prepareData()
  {
      fatherMap = new HashMap();
      matherMap = new HashMap();
      foreignLanguageMap = new HashMap<Long, List>()

      requestedCompetitions = IUniBaseDao.instance.get().getList(EnrRequestedCompetition.class, "id", requestedCompetitionIds)
      Set<Long> personIds = requestedCompetitions.collect {e -> e.request.entrant.person.id}

      BatchUtils.execute(personIds, 256, new BatchUtils.Action<Long>() {
          @Override
          void execute(Collection<Long> elements)
          {
              def personNextOfKinList = new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, "n")
                  .where(DQLExpressions.in(property("n", PersonNextOfKin.person().id()), elements))
                  .column(property(PersonNextOfKin.person().id().fromAlias("n")))
                  .column(property(PersonNextOfKin.relationDegree().code().fromAlias("n")))
                  .column(property(PersonNextOfKin.employmentPlace().fromAlias("n")))
                  .createStatement(session).<Object[]>list()
              for (def personNextOfKin : personNextOfKinList)
              {
                if (personNextOfKin[1].equals(RelationDegreeCodes.FATHER))
                  fatherMap.put(personNextOfKin[0], personNextOfKin[2])
                if (personNextOfKin[1].equals(RelationDegreeCodes.MOTHER))
                  matherMap.put(personNextOfKin[0], personNextOfKin[2])
              }
          
              def personForeignLanguageList = new DQLSelectBuilder().fromEntity(PersonForeignLanguage.class, "l")
                  .where(DQLExpressions.in(property("l", PersonForeignLanguage.person().id()), elements))
                  .column(property(PersonForeignLanguage.person().id().fromAlias("l")))
                  .column(property(PersonForeignLanguage.language().title().fromAlias("l")))
                  .createStatement(session).<Object[]>list();
              for (def language : personForeignLanguageList)
              {
                List languageList = foreignLanguageMap.get(language[0])
                if (languageList == null)
                  languageList = new ArrayList()
                languageList.add(language[1])
                foreignLanguageMap.put(language[0], languageList)
              }
          }
      })
  }

  /**
   * Рисует таблицу
   * @param sheet лист
   */
  def printTable(def sheet)
  {
    int colN = 0;
    ["id", "facul", "fam", "ima", "otc", "UCHD", "Pas_s", "Pas_n", "Pas_d", "Pas_v", "Pol", "Fath_wor", "Moth_wor", "Godok", "Uchzav", "Nom_dip", "Medal", "Krdipl", "Inyaz", "Obcheg", "Mesroj", "Grajd", "Mesgit", "Ind", "Resp", "Gorod", "Ul", "Tel", "Sot_tel", "Mail", "Prim"].each
            {
              sheet.addCell(new Label(colN++, 0, it))
            }



    int rowNumber = 1;
    for (EnrRequestedCompetition reqComp : requestedCompetitions)
    {
        AddressBase address = reqComp.request.entrant.person.identityCard.address
        AddressDetailed addressDetailed = address instanceof AddressDetailed ? address : null;
        AddressRu addressRu = address instanceof AddressRu ? address : null;

      sheet.addCell(!NumberUtils.isNumber(reqComp.request.entrant.personalNumber) ? new Label(0, rowNumber, reqComp.request.entrant.personalNumber) : new Number(0, rowNumber,  Double.valueOf(reqComp.request.entrant.personalNumber)))
      sheet.addCell(new Label(1, rowNumber, reqComp.competition.title))
      sheet.addCell(new Label(2, rowNumber, reqComp.request.entrant.person.identityCard.lastName))
      sheet.addCell(new Label(3, rowNumber, reqComp.request.entrant.person.identityCard.firstName))
      sheet.addCell(new Label(4, rowNumber, reqComp.request.entrant.person.identityCard.middleName))
      sheet.addCell(!NumberUtils.isNumber(reqComp.request.regNumber) ? new Label(5, rowNumber, reqComp.request.regNumber) : new Number(5, rowNumber, Double.valueOf(reqComp.request.regNumber)))
      sheet.addCell(new Label(6, rowNumber, reqComp.request.entrant.person.identityCard.seria))
      sheet.addCell(new Label(7, rowNumber, reqComp.request.entrant.person.identityCard.number))
      sheet.addCell(new Label(8, rowNumber, reqComp.request.entrant.person.identityCard.issuanceDate?.format("dd.MM.yyyy")))
      sheet.addCell(new Label(9, rowNumber, reqComp.request.entrant.person.identityCard.issuancePlace))
      sheet.addCell(new Label(10, rowNumber, reqComp.request.entrant.person.identityCard.sex.code.equals(SexCodes.MALE) ? "муж" : "жен"))
      sheet.addCell(new Label(11, rowNumber, getFatherWorkPlace(reqComp.request.entrant.person)))
      sheet.addCell(new Label(12, rowNumber, getMotherWorkPlace(reqComp.request.entrant.person)))
      sheet.addCell( reqComp.request.eduDocument.yearEnd == null ? new Label(13, rowNumber, "") : new Number(13, rowNumber, Double.valueOf(reqComp.request.eduDocument.yearEnd)))
      sheet.addCell(new Label(14, rowNumber, reqComp.request.eduDocument.eduOrganization))
      sheet.addCell(new Label(15, rowNumber, reqComp.request.eduDocument.fullNumber))
      sheet.addCell(new Label(16, rowNumber, Arrays.asList(GraduationHonourCodes.GOLD_MEDAL, GraduationHonourCodes.SILVER_MEDAL).contains(reqComp.request.eduDocument.graduationHonour?.code) ? "да" : "нет"))
      sheet.addCell(new Label(17, rowNumber, GraduationHonourCodes.RED_DIPLOMA.equals(reqComp.request.eduDocument.graduationHonour?.code) ? "да" : "нет"))
      sheet.addCell(new Label(18, rowNumber, getForeignLanguage(reqComp.request.entrant.person)))
      sheet.addCell(new Label(19, rowNumber, reqComp.request.entrant.person.needDormitory ? "да" : "нет"))
      sheet.addCell(new Label(20, rowNumber, reqComp.request.entrant.person.identityCard.birthPlace))
      sheet.addCell(new Label(21, rowNumber, reqComp.request.entrant.person.identityCard.citizenship.shortTitle))
      sheet.addCell(new Label(22, rowNumber, getSettlement(reqComp.request.entrant.person.identityCard.address)))
      sheet.addCell(((addressRu == null) || (addressRu.inheritedPostCode == null)) ? new Label(23, rowNumber, "") : new Number(23, rowNumber, Double.parseDouble(addressRu.inheritedPostCode)))
      sheet.addCell(new Label(24, rowNumber, addressRu == null ? "" : getRegion(reqComp.request.entrant.person.identityCard.address)))
      sheet.addCell(new Label(25, rowNumber, addressDetailed?.settlement?.title))
      sheet.addCell(new Label(26, rowNumber, addressRu?.shortTitleWithFlat))
      sheet.addCell(new Label(27, rowNumber, reqComp.request.entrant.person.contactData.phoneFact))
      sheet.addCell(new Label(28, rowNumber, reqComp.request.entrant.person.contactData.phoneMobile))
      sheet.addCell(new Label(29, rowNumber, reqComp.request.entrant.person.contactData.email))
      sheet.addCell(new Label(30, rowNumber++, reqComp.request.entrant.additionalInfo))
    }
  }

  /**
   * @param person персона
   * @return место работы отца, либо пусто
   */
  String getFatherWorkPlace(def person)
  {
    def get = fatherMap.get(person.id)
    return get == null ? "" : get
  }

  /**
   * @param person персона
   * @return место работы матери, либо пусто
   */
  String getMotherWorkPlace(def person)
  {
    def get = matherMap.get(person.id)
    return get == null ? "" : get
  }

  /**
   * @param person персона
   * @return ин. языки, через пробел, либо пусто
   */
  String getForeignLanguage(def person)
  {
    def result = "";
    def get = foreignLanguageMap.get(person.id)
    if (get == null)
      return "";
    get.each
            {
              result += it + " "
            }
    return result.trim();
  }

  /**
   * @param address рег. адресс персоны
   * @return насел. пункт, либо пусто
   */
  static String getSettlement(def address)
  {
    if (address?.settlement?.inheritedRegionCode == "77")
      return "Москва"
    if (address?.settlement?.inheritedRegionCode == "50" && !address?.settlement?.addressType?.countryside)
      return "М.О. (город)"
    if (address?.settlement?.inheritedRegionCode == "50" && address?.settlement?.addressType?.countryside)
      return "М.О. (село)"
    if (address?.settlement?.inheritedRegionCode != "50" && address?.settlement?.inheritedRegionCode != "77" && !address?.settlement?.addressType?.countryside)
      return "Др. город"
    if (address?.settlement?.inheritedRegionCode != "50" && address?.settlement?.inheritedRegionCode != "77" && address?.settlement?.addressType?.countryside)
      return "Др. село"

    return ""
  }

  /**
   * @param address рег. адресс персоны
   * @return регион
   */
  static String getRegion(def address)
  {
    def result = ""
    def parent = address?.settlement
    while (parent != null)
    {
      result = parent.title
      parent = parent.parent
    }

    return result;
  }
}
