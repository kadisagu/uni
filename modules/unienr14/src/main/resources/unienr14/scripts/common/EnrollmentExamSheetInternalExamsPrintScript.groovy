package unienr14.scripts.common

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author oleyba
 * @since 5/13/13
 */
return new EnrollmentExamInternalExamsSheetPrint(                              // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrant: session.get(EnrEntrant.class, object) // объект печати
).print()

/**
 * Алгоритм печати экзаменационного листа абитуриента
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class EnrollmentExamInternalExamsSheetPrint
{
    Session session
    byte[] template
    EnrEntrant entrant
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def card = entrant.person.identityCard

        im.put('highSchoolTitle', TopOrgUnit.instance.title);
        im.put('lastName', card.lastName);
        im.put('firstName', card.firstName);
        im.put('middleName', card.middleName);

        im.put('number', entrant.getPersonalNumber());
        im.put('date', new Date().format('dd.MM.yyyy'));

        fillEnrollmentResults();

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Экзаменационный лист абитуриента ${entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillEnrollmentResults()
    {
        // вычисляем дисциплины для сдачи
        List<EnrExamPassDiscipline> examPassDisciplines = new DQLSelectBuilder()
            .fromEntity(EnrExamPassDiscipline.class, "e").column("e")
            .where(eq(property("e", EnrExamPassDiscipline.entrant().id()), value(entrant.id)))
            .where(eq(property("e", EnrExamPassDiscipline.retake()), value(Boolean.FALSE)))
            .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
            .order(property("e", EnrExamPassDiscipline.passForm().code()))
            .createStatement(session).list();

        // список строк
        final List<String[]> rows = new ArrayList<String[]>();

        Map<EnrCampaignDiscipline, Integer> discCountMap = new HashMap<>();
        for (EnrExamPassDiscipline exam : examPassDisciplines) {
            discCountMap.put(exam.getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline())) + 1);
        }
        
        Map<String, String> places = new LinkedHashMap<>();

        int i = 1;
        // для каждой дисциплины для сдачи
        for (def exam : examPassDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline()));
            boolean printForm = discCount > 1;
            Long mark = exam.getMarkAsDouble() == null ? null : Math.round(exam.getMarkAsDouble());
            List<EnrExamGroupScheduleEvent> events = exam.getExamGroup() == null ? Collections.emptyList() :
                IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class,
                EnrExamGroupScheduleEvent.examGroup(), exam.getExamGroup(),
                EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());
            
            for (EnrExamGroupScheduleEvent event : events)
                places.put(event.examScheduleEvent.examRoom.place.displayableTitle, event.examScheduleEvent.examRoom.place.fullLocationInfo)
            
            // сформировать строку
            String[] row = new String[8];
            row[0] = i++;
            row[1] = exam.discipline.title + (printForm ? " (" + exam.passForm.title + ")" : "");
            row[2] = exam.examGroup?.title
            row[3] = events.collect { it.timeTitle + ", " + it.examScheduleEvent.examRoom.place.displayableTitle }.grep().join("\n");

            // итоговый балл цифрами и прописью
            row[4] = mark == null ? "" : mark;
            row[5] = mark == null ? "" : NumberSpellingUtil.spellNumberMasculineGender(mark)

            row[6] = "" // events.collect { it.examScheduleEvent.commission }.grep().join("\n"); - можно подставить, если хочется вывести экзаменаторов

            rows.add(row);
        }

        tm.put('T', rows as String[][]);

        def placeList = new ArrayList<String>()
        for (def place: places.entrySet())
            placeList.add(place.key + " - " + place.value)

        def resultPlace = new RtfString()
        for (def place: placeList)
        {
            if (placeList.indexOf(place) > 0)
                resultPlace.par()
            resultPlace.append(place)
        }

        im.put("place", resultPlace)
    }
}



