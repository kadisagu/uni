package unienr14.scripts.common

import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.text.StrBuilder
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.core.view.formatter.YesNoFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.SharedRtfUtil
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.IRtfModifier
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao
import org.tandemframework.shared.person.base.entity.*
import org.tandemframework.shared.person.base.entity.gen.IdentityCardDeclinabilityGen
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.entrant.entity.*
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.core.view.formatter.DateFormatter.DEFAULT_DATE_FORMATTER
import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintHigher(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author oleyba
 * @since 5/13/13
 */
@SuppressWarnings("UnnecessaryQualifiedReference")
class EntrantRequestPrintHigher
{
    def Session session
    def byte[] template
    def EnrEntrantRequest entrantRequest
    def RtfDocument mainDocument
    def List<EnrRequestedCompetition> requestedCompetitions // lazy initialized
    def Map<Long, List<EduProgramSpecialization>> specializationMap
    final im = new RtfInjectModifier()
    final tm = new RtfTableModifier()
    final List<String> labelListForDeleteWithParagraph = [] // Список меток, которые надо удалить вместе с параграфом
    final List<String> labelListForDeleteWithTableRow = [] // Список меток, которые надо удалить вместе со строкой таблицы

    def print()
    {
        mainDocument = new RtfReader().read(template)

        // Заполняем отдельные блоки данных (таблицы). Либо удаляем их, если нет данных.

        fillCompetitions() // Выбранные направления (T1, T2)
        fillAchievements() // Индивидуальные достижения
        fillInternalExams() // Внутренние вступительные испытания
        fillNextOfKin() // Ближайшие родственники
        fillAcceptedContract() // Выбранные конкурсы по договору (в т.ч. без ви)
        fillRequestedPrograms() // Приоритеты выбранных ОП

        // Заполняем общие данные и прочие метки

        final entrant = entrantRequest.entrant
        final person = entrant.person
        final card = entrantRequest.identityCard

        im.put('highSchoolTitleShort', TopOrgUnit.instance.shortTitle) // Сокр. назв. головного подразделения (вуза)
        im.put('rector_D', getRectorIof(GrammaCase.DATIVE)) // [И.О. Фамилия] ректора в дательном падеже

        im.put('regNumber', entrantRequest.stringNumber) // Номер заявления в правильном формате
        im.put('FIO_G', PersonManager.instance().declinationDao().getCalculatedFIODeclination(card, InflectorVariantCodes.RU_GENITIVE)) // ФИО абитуриента в родительном падеже
        im.put('birthDate', DEFAULT_DATE_FORMATTER.format(card.birthDate)) // дата рождения абитуриента
        im.put('birthPlace', card.birthPlace) // место рождения
        im.put('sex', card.sex.title) // пол
        im.put('citizenship', card.citizenship.fullTitle) // гражданство
        im.put('identityCardTitle', card.shortTitle) // сокр. название типа удостоверения личности
        im.put('identityCardPlaceAndDate', [card.issuancePlace, DEFAULT_DATE_FORMATTER.format(card.issuanceDate)].grep().join(', ')) // место и дата выдачи удостоверения личности
        im.put('addressTitleWithFlat', person.address?.titleWithFlat) // домашний адрес
        im.put('addressPhonesTitle', CommonBaseStringUtil.joinArrayUnique(', ', person.contactData.phoneDefault, person.contactData.phoneWork, person.contactData.phoneMobile)) // телефоны
        im.put('email', person.contactData.email) // email
        im.put('age', card.age as String) // возраст
        im.put('foreignLanguage', getEntrantForeignLanguageTitle()) // Основной иностранный язык
        im.put('serviceLength', person.serviceLength) // стаж работы (лет, месяцев, дней)
        im.put('needHotel', person.needDormitory ? 'Нуждаюсь' : 'Не нуждаюсь') // нуждается ли в общежитии
        im.put('additionalInfo', entrant.additionalInfo) // дополнительные сведения

        im.put('education', entrantRequest.eduDocument.eduLevel?.title ?: entrantRequest.eduDocument.documentEducationLevel) // Уровень образования документа об образовании
        im.put('eduOrganization', entrantRequest.eduDocument.eduOrganization) // Название образовательной организации из документа об образовании
        im.put('yearEnd', entrantRequest.eduDocument.yearEnd as String) // Год окончания из документа об образовании
        im.put('certificate', entrantRequest.eduDocument.title) // Документ об образовании (с серией и номером)
        im.put('wayOfProviding', entrantRequest.originalSubmissionWay.title) // Способ подачи оригинала документа об образовании
        im.put('howToReturn', entrantRequest.originalReturnWay.title) // способ возврата оригиналов документов
        im.put('eduLevelFirstTime', entrantRequest.receiveEduLevelFirst ? 'впервые' : 'повторно') // Получает образование впервые или повторно

        // Дата добавления заявления
        im.put('regDay', RussianDateFormatUtils.getDayString(entrantRequest.regDate, true)) // день
        im.put('regMonthStr', RussianDateFormatUtils.getMonthName(entrantRequest.regDate, false)) // месяц строкой
        im.put('regYear', RussianDateFormatUtils.getYearString(entrantRequest.regDate, false)) // год

        if (!labelListForDeleteWithParagraph.empty) {
            // Удаляем метки вместе с параграфами, если такие есть
            SharedRtfUtil.removeParagraphsWithTagsRecursive(mainDocument, labelListForDeleteWithParagraph, false, false)
        }

        if (!labelListForDeleteWithTableRow.empty) {
            // Удаляем метки вместе со строками таблиц, если такие есть
            UniRtfUtil.deleteRowsWithLabels(mainDocument, labelListForDeleteWithTableRow)
        }

        im.modify(mainDocument)

        //noinspection GrDeprecatedAPIUsage
        fillOldDeprecatedLabels() // старые метки. для совместимости

        im.modify(mainDocument) // Необходим второй вызов модифаера - чтобы во вставиленных шаблонах заменились метки

        tm.modify(mainDocument)

        return [document: RtfUtil.toByteArray(mainDocument), fileName: "Заявление абитуриента ${card.fullFio}.rtf"]
    }

    /**
     * Метод для обратной совместимости со старыми шаблонами - заполнение меток, которые были в старом варианте заявления.
     * В новых шаблонах данные метки не используются.
     *
     * @deprecated не использовать, если у вас нет в щаблоне данных меток
     */
    void fillOldDeprecatedLabels()
    {
        if (!SharedRtfUtil.hasFields(this.mainDocument.elementList,
                ['FIO', 'rector_G', 'adressTitleWithFlat', 'adressPhonesTitle', 'foreignLanguages',
                 'surName', 'name', 'secondName', 'addressFact', 'idPlaceCode', 'idTitle', 'idSeria', 'idNumber', 'numberOf5', 'numberOf4', 'numberOf3', 'avgMark',
                 'militaryCardNum', 'beginArmy', 'endArmy'].toSet()))
        {
            return // В серверной и так шумно
        }

        final entrant = entrantRequest.entrant
        final person = entrant.person
        final card = entrantRequest.identityCard

        im.put('FIO', card.fullFio) // ФИО абитуриента в именительном падеже
        im.put('rector_G', getRectorIof(GrammaCase.GENITIVE)) // [И.О. Фамилия] ректора в родительном падеже
        im.put('adressTitleWithFlat', person.address?.titleWithFlat) // Адрес. С ошибкой в слове address
        im.put('adressPhonesTitle', person.contactData.mainPhones) // Телефон. С ошибкой в слове address
        im.put("foreignLanguages", getEntrantForeignLanguageTitle()) // Основной иностранный язык

        im.put('surName', card.lastName) // Фамилия
        im.put('name', card.firstName) // Имя
        im.put('secondName', card.middleName) // Отчество
        im.put('addressFact', person.address?.titleWithFlat) // Адрес фактический

        im.put('idPlaceCode', card.issuanceCode) // Код подразделения удостоверения личности
        im.put('idTitle', card.cardType.title) // Название типа удостоверения личности
        im.put('idSeria', card.seria) // Серия удостоверения личности
        im.put('idNumber', card.number) // Номер удостоверения личности

        final eduDocument = entrantRequest.eduDocument
        im.put('numberOf5', eduDocument.mark5 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark5))
        im.put('numberOf4', eduDocument.mark4 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark4))
        im.put('numberOf3', eduDocument.mark3 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark3))
        im.put('avgMark', DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(eduDocument.avgMarkAsDouble))

        final militaryStatusList = IUniBaseDao.instance.get().getList(PersonMilitaryStatus.class, PersonMilitaryStatus.person(), person)
        final status = !militaryStatusList.isEmpty() ? militaryStatusList.iterator().next() : null

        im.put('militaryCardNum', status?.militaryNumber)
        im.put('beginArmy', DEFAULT_DATE_FORMATTER.format(entrant.beginArmy))
        im.put('endArmy', DEFAULT_DATE_FORMATTER.format(entrant.endArmy))
    }

    /** Таблицы с выбранныеми конкурсами (обычное и параллельное обучение) */
    void fillCompetitions() {
        final simpleRows = new ArrayList<String[]>()
        final parallelRows = new ArrayList<String[]>()
        for (requestedCompetition in getRequestedCompetitions())
        {
            final row = [requestedCompetition.priority as String,
                         getProgramSubjectStr(requestedCompetition),
                         requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                         getPlaces(requestedCompetition),
                         entrantRequest.type.shortTitle
            ]
            if (requestedCompetition.parallel){
                parallelRows.add(row as String[])
            } else {
                simpleRows.add(row as String[])
            }
        }

        // Таблица с конкурсами
        fillOrRemoveTable(this.mainDocument, 'T1', !parallelRows.empty, true, simpleRows.empty ? null : new RtfTableModifier()
                .put('T1', simpleRows as String[][])
        )

        // Таблица с параллельными конкурсами удаляется, если таких нет
        fillOrRemoveTable(this.mainDocument, 'T2', true, false, parallelRows.empty ? null : new RtfTableModifier()
                .put('T2', parallelRows as String[][])
        )
    }

    /** Индвидиуальные достижения */
    void fillAchievements()
    {
        final item = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACHIEVEMENTS) // Метка заменяется на документ из отдельного шаблона
        final achievementsDoc = new RtfReader().read(item.getCurrentTemplate())
        final achievementList = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'e')
                .column(property('e'))
                .where(eq(property('e', EnrEntrantAchievement.entrant()), value(this.entrantRequest.entrant)))
                .where(or(
                        and(eq(property('e', EnrEntrantAchievement.type().forRequest()), value(false)),
                            eq(property('e',EnrEntrantAchievement.type().achievementKind().requestType()), value(this.entrantRequest.type))),
                        and(eq(property('e', EnrEntrantAchievement.type().forRequest()), value(true)),
                            eq(property('e', EnrEntrantAchievement.request()), value(this.entrantRequest)))
                 ))
                .createStatement(this.session).<EnrEntrantAchievement>list()

        // Удаляем таблицу с ИД, если ИД нет. Иначе заполняем её индивидуальными достижениями
        fillOrRemoveTable(achievementsDoc, 'TA1', true, false, achievementList.empty ? null : new RtfTableModifier()
                .put('TA1', achievementList.collect { e -> [e.type.achievementKind.shortTitle, getEntrantDocumentStr(e.document)]} as String[][]) // [сокр.название вида инд. достижения], [документ, подтверждающий достижение]
        )

        // Оставляем таблицу "Индивидуальных достижений не имею.", метку "noAchievements" удаляем, если нет ИД. Иначе - удаляем таблицу.
        fillOrRemoveTable(achievementsDoc, 'noAchievements', true, false, !achievementList.empty ? null : new RtfInjectModifier()
                .put('noAchievements', '')
        )

        // Вставляем заполненный шаблон индивидуальных достижений в основной документ
        RtfUtil.modifySourceList(this.mainDocument.header, achievementsDoc.header, achievementsDoc.elementList)
        achievementsDoc.elementList.remove(achievementsDoc.elementList.size() - 1) // Удаляем последний пустой параграф, чтобы не было лишнего перевода строки
        this.im.put('achievements', achievementsDoc.elementList)
    }

    /** Внутренние вступительные испытания */
    void fillInternalExams()
    {
        final internalExams =  new DQLSelectBuilder().fromEntity(EnrChosenEntranceExamForm.class, 'e')
                .predicate(DQLPredicateType.distinct)
                .column(property('e', EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .where(DQLExpressions.in(property('e', EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition()), getRequestedCompetitions()))
                .where(eq(property('e', EnrChosenEntranceExamForm.passForm().internal()), value(true)))
                .order(property('e', EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .createStatement(this.session).<String>list()

        fillOrRemoveTable(this.mainDocument, 'internalExams', true, false, internalExams.empty ? null : new RtfInjectModifier()
                .put('internalExams', internalExams.join(', ')) // Список, сдаваемых внутренних вступит. испытаний
        )

        if (!internalExams.empty) {
            if (this.entrantRequest.entrant.needSpecialExamConditions) {
                //Нуждается в спец. условиях для прохождения ВИ.
                if(StringUtils.isEmpty(entrantRequest.entrant.getSpecialExamConditionsDetails())){
                    im.put('needSpecialConditions', '');
                    labelListForDeleteWithParagraph.add('needSpecialConditionsWithDetails');
                }else{
                    im.put('needSpecialConditionsWithDetails', entrantRequest.entrant.getSpecialExamConditionsDetails());
                    labelListForDeleteWithParagraph.add('needSpecialConditions');
                }
            } else {
                // Не нуждается в спец. условиях - удаляем абзац
                labelListForDeleteWithParagraph.add('needSpecialConditions')
                labelListForDeleteWithParagraph.add('needSpecialConditionsWithDetails');
            }
        }
    }

    /** Ближайшие родственники */
    void fillNextOfKin()
    {
        final Map<String, String> map = [:]
        new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, 'e')
                .column(property('e'))
                .where(eq(property('e', PersonNextOfKin.person()), value(this.entrantRequest.entrant.person)))
                .where(DQLExpressions.in(property('e', PersonNextOfKin.relationDegree().code()), [RelationDegreeCodes.FATHER, RelationDegreeCodes.MOTHER, RelationDegreeCodes.TUTOR]))
                .createStatement(this.session).<PersonNextOfKin>list().each {
            map.put(it.relationDegree.code, [it.fullFio, [it.employmentPlace, it.post != null ? "(${it.post})" : null].grep().join(' '), it.phones].grep().join(', '))
        }

        final mother = map[RelationDegreeCodes.MOTHER] // мать
        final father = map[RelationDegreeCodes.FATHER] // отец
        final tutor = map[RelationDegreeCodes.TUTOR] // опекун

        final RtfInjectModifier modifier
        if (map.isEmpty()) {
            modifier = null
        } else {
            modifier = new RtfInjectModifier()
            if (mother) {
                modifier.put('mother', mother)
            } else {
                this.labelListForDeleteWithTableRow.add('mother')
            }
            if (father) {
                modifier.put('father', father)
            } else {
                this.labelListForDeleteWithTableRow.add('father')
            }
            if (tutor) {
                modifier.put('tutor', tutor)
            } else {
                this.labelListForDeleteWithTableRow.add('tutor')
            }
        }
        fillOrRemoveTable(this.mainDocument, 'mother', false, false, modifier)
    }

    /** Выбранные конкурсы по договору (в т.ч. без ви). Если таких нет, то страница не выводится. */
    void fillAcceptedContract()
    {
        this.labelListForDeleteWithParagraph.add('acceptedContract') // Саму метку надо удалить
        if (!SharedRtfUtil.hasFields(mainDocument.elementList, ['acceptedContract'].toSet()))
            return;

        final acceptedContractCompetitions = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, 'rc')
                .column(property('rc'))
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().type().fromAlias('rc'), 'ct')
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().eduLevelRequirement().fromAlias('rc'), 'elr')
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().fromAlias('rc'), 'ps')
                .where(eq(property('rc', EnrRequestedCompetition.request()), value(this.entrantRequest)))
                .where(eq(property('rc', EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
                .order(property('rc', EnrRequestedCompetition.priority()))
                .createStatement(session).<EnrRequestedCompetition>list()

        if (!acceptedContractCompetitions.empty) {

            final item = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT) // Метка заменяется на документ из отдельного шаблона
            final injectedDoc = new RtfReader().read(item.getCurrentTemplate())
            final rows = new ArrayList<>()
            for (requestedCompetition in acceptedContractCompetitions) {
                final eduLevelReq = requestedCompetition.competition.eduLevelRequirement
                rows.add([
                        requestedCompetition.getPriority(),
                        getProgramSubjectStr(requestedCompetition),
                        requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                        getPlaces(requestedCompetition),
                        EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? 'СОО и ПО' : eduLevelReq.shortTitle,
                        YesNoFormatter.INSTANCE.format(requestedCompetition.accepted)
                ] as String[])
            }
            this.tm.put('TAC1', rows as String[][])

            // Вставляем в основной документ вместо метки "acceptedContract"
            RtfUtil.modifySourceList(this.mainDocument.header, injectedDoc.header, injectedDoc.elementList)
            this.mainDocument.elementList.addAll(injectedDoc.elementList)
        }
    }

    /** Приоритеты выбранных образовательных программ. Если не заданы, то страница не выводится. */
    void fillRequestedPrograms()
    {
        this.labelListForDeleteWithParagraph.add('reqPrograms') // Саму метку надо удалить
        if (!SharedRtfUtil.hasFields(mainDocument.elementList, ['reqPrograms'].toSet()))
            return;

        final requestedPrograms = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, 'rc')
                .column(property('rc'))
                .fetchPath(DQLJoinType.inner, EnrRequestedProgram.requestedCompetition().competition().fromAlias('rc'), 'c')
                .fetchPath(DQLJoinType.inner, EnrCompetition.type().compensationType().fromAlias('c'), 'ct')
                .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias('c'), 'pf')
                .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias('c'), 'ps')
                .where(eq(property('rc', EnrRequestedProgram.requestedCompetition().request()), value(entrantRequest)))
                .order(property('rc', EnrRequestedProgram.requestedCompetition().priority()))
                .order(property('rc', EnrRequestedProgram.priority()))
                .createStatement(session).<EnrRequestedProgram>list()

        if (!requestedPrograms.empty) {

            final item = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_REQUESTED_PROGRAMS) // Метка заменяется на документ из отдельного шаблона
            final injectedDoc = new RtfReader().read(item.getCurrentTemplate())
            final rows = new ArrayList<>()
            for (final requestedProgram: requestedPrograms) {

                def competition = requestedProgram.getRequestedCompetition().getCompetition()
                def program = requestedProgram.getProgramSetItem().getProgram()
                rows.add([
                        requestedProgram.getRequestedCompetition().getPriority(),
                        competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitle(),
                        competition.getType().getPrintTitle(),
                        competition.getProgramSetOrgUnit().getProgramSet().getProgramForm().getShortTitle(),
                        competition.getType().getCompensationType().getTitle(),
                        program.getProgramSpecialization().isRootSpecialization() ? '' : (program.getProgramSpecialization()).getTitle(),
                        program.getEduProgramTrait() == null ? '' : program.getEduProgramTrait().getShortTitle(),
                        requestedProgram.getPriority()
                ] as String[])
            }
            this.tm.put('TRP1', rows as String[][])

            // Вставляем в основной документ вместо метки "reqPrograms"
            RtfUtil.modifySourceList(this.mainDocument.header, injectedDoc.header, injectedDoc.elementList)
            this.mainDocument.elementList.addAll(injectedDoc.elementList)
        }
    }

    /** @return список выбранных конкурсов по зявлению, сортировка по приоритету */
    def List<EnrRequestedCompetition> getRequestedCompetitions() {

        // lazy!
        if (this.requestedCompetitions == null) {
            this.requestedCompetitions = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, 'e')
                    .column(property('e'))
                    .where(eq(property('e', EnrRequestedCompetition.request()), value(this.entrantRequest)))
                    .order(property('e', EnrRequestedCompetition.priority()), OrderDirection.asc)
                    .createStatement(this.session).<EnrRequestedCompetition>list()
        }
        return requestedCompetitions
    }

    /** @return Направленности образовательных программ для выбранных конкурсов */
    def Map<Long, List<EduProgramSpecialization>> getRequestedProgramMap() {

        // lazy!
        if (this.specializationMap == null) {
            this.specializationMap = SafeMap.get(ArrayList.class)
            new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, 'p')
                    .column(property('p'))
                    .where(DQLExpressions.in(property('p', EnrRequestedProgram.requestedCompetition()), getRequestedCompetitions()))
                    .createStatement(this.session).<EnrRequestedProgram> list().each { this.specializationMap.get(it.requestedCompetition.id).add(it.programSetItem.program.programSpecialization) }

            for (e in this.specializationMap.entrySet()) {
                if (e.value.size() == 1 && e.value[0].rootSpecialization) {
                    e.value.clear()
                }
            }
        }
        return this.specializationMap
    }

    private String cachedForeignLang // кэшируем для старой метки
    /** Название основного иностранного языка абитуриента */
    def String getEntrantForeignLanguageTitle()
    {
        return cachedForeignLang ?: (cachedForeignLang = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, 'e')
                .column(property('e', PersonForeignLanguage.language().title()))
                .where(eq(property('e', PersonForeignLanguage.person()), value(entrantRequest.entrant.person)))
                .where(eq(property('e', PersonForeignLanguage.main()), value(Boolean.TRUE)))
                .createStatement(session).uniqueResult())
    }

    /** @return Название направления подготовки из выбранного конкурса с кодом и списком направленностей в скобках, если направленности есть */
    def String getProgramSubjectStr(EnrRequestedCompetition requestedCompetition)
    {
        final programSet = requestedCompetition.competition.programSetOrgUnit.programSet
        final specs = getRequestedProgramMap().get(requestedCompetition.id)
        return programSet.printTitle + ((specs != null && !specs.isEmpty()) ? ' (' + StringUtils.join(specs.collect {e -> e.title}, ', ') + ')' : '')
    }

    /** @return "на места ..." */
    static String getPlaces(EnrRequestedCompetition requestedCompetition) {
        def String str
        switch (requestedCompetition.competition.type.code)
        {
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
            case EnrCompetitionTypeCodes.CONTRACT:
                str = 'по договорам об оказании платных образовательных услуг'
                break
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
            case EnrCompetitionTypeCodes.MINISTERIAL:
                str = 'финансируемые из федерального бюджета'
                break
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                str = 'в пределах квоты целевого приема'
                break
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                str = 'в пределах квоты приема лиц, имеющих особое право'
                break
            default:
                str = ''
        }
        if (requestedCompetition instanceof EnrRequestedCompetitionTA) {
            // Если конкурс по ЦП, дописываем название вида ЦП в скобках
            str += ' (' + (requestedCompetition as EnrRequestedCompetitionTA).targetAdmissionKind.targetAdmissionKind.title + ')'
        }
        return str
    }

    /** {@link #fillAndCloneOrRemoveTable} */
    static boolean fillOrRemoveTable(RtfDocument document, String labelInTable, boolean removeNextElement, boolean removePrevElement, IRtfModifier modifier)
    {
        fillAndCloneOrRemoveTable(document, labelInTable, removeNextElement, removePrevElement, modifier ? [modifier] : null)
    }

    /**
     * Клонирование таблицы и заполнение меток.
     * В метод передается метка из таблицы, чтобы тублицу найти, и набор модифаеров, содержащих заполненные метки для данной таблицы.
     * Если модифаеров ноль, то таблица удаляется.
     * Если модифаер один, то оставляется, как есть.
     * Если модифаеров больше одного, то таблица клонируется по количеству модифаеров.
     *
     * @param labelInTable одна из меток в таблице, по которой таблицу можно найти
     * @param modifierList набор модифаеров для каждой таблицы. Это может быть как RtfInjectModifier, так и RtfTableModifier
     * @param removeNextElement удалять элемент (параграф), следующий после таблицы, если таблица удаляется
     * @param removePrevElement удалять элемент (параграф), предшествующий таблице, если таблица удаляется
     * @return true, если список модифаеров не пуст
     */
    static boolean fillAndCloneOrRemoveTable(RtfDocument document, String labelInTable, boolean removeNextElement, boolean removePrevElement, List<IRtfModifier> modifierList)
    {
        final index = SharedRtfUtil.findRtfTableIndex(document.elementList, labelInTable)
        if (index == null) {
            return false // Метка не найдена, наверное таблицу просто убрали из шаблона
        }

        if (modifierList == null || modifierList.empty) {
            // Удаляем таблицу, если список модифаеров пуст
            document.elementList.remove(index)
            if (removeNextElement) {
                document.elementList.remove(index) // Удаляем абзац после таблицы
            }
            if (removePrevElement) {
                document.elementList.remove(index - 1) // Удаляем абзац перед таблицей
            }
            return false
        }

        def rtfTable = (RtfTable) document.elementList[index]
        if (modifierList.size() == 1) {
            // Модифаер всего один - просто применяем его к таблице
            modifierList[0].modify([rtfTable])
        } else {
            // Модифаеров много, надо растиражировать таблицу и к каждой копии применить модифаер
            final tableTemplate = rtfTable.clone
            final iterator = modifierList.iterator()
            final List<IRtfElement> tableElementList = []
            while (iterator.hasNext())
            {
                final modifier = iterator.next()
                modifier.modify([rtfTable])
                tableElementList.add(rtfTable)
                if (iterator.hasNext()) {
                    rtfTable = tableTemplate.clone
                }
            }
            // Вставляем набор таблиц
            document.elementList.remove(index)
            document.elementList.addAll(index, tableElementList)
        }
        return true
    }

    /**
     * Вывод документа абитуриента в нужном формате.
     *
     * @param doc документ, подтверждающий особое право или индивидуальное достижение
     * @return строка в нужном формате
     */
    static String getEntrantDocumentStr(def doc)
    {
        if (doc == null)
            return '';
        final str = new StrBuilder()
        if (doc instanceof EnrPersonEduDocumentRel) {
            // документ об образовании — <сокр. наз. вида документа об образовании> (<название степени отличия>) - <серия> <номер> (<сокр. наз. вида/уровня образования>, <год окончания> г., <населенный пункт>)
            doc = doc.eduDocument

            str.append(doc.eduDocumentKind.shortTitle)
            if (doc.graduationHonour) { str.append(' (').append(doc.graduationHonour.title).append(')') }
            str.append(' -')
            if (doc.seria) { str.append(' ').append(doc.seria) }
            if (doc.number) { str.append(' ').append(doc.number) }
            str.append(' (')
            if (doc.eduLevel) { str.append(doc.eduLevel.shortTitle).append(', ') }
            str.append(doc.yearEnd).append(' г., ')
            str.append(doc.eduOrganizationAddressItem.titleWithType).append(')')

        } else if (doc instanceof EnrEntrantBaseDocument) {

            def personDoc = doc.getDocRelation().document

            if (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code)) {
                def diploma = (EnrOlympiadDiploma) personDoc

                // диплом участника олимпиады — <сокр. название типа документа> <серия> <номер> (<название олимпиады>) по предмету <предмет>, <степень отличия>
                str.append(diploma.documentType.shortTitle)
                if (doc.seria) { str.append(' ').append(diploma.seria) }
                if (doc.number) { str.append(' ').append(diploma.number) }
                str.append(' (').append(diploma.olympiad.title)
                str.append(') по предмету ').append(diploma.subject.title)
                str.append(', ').append(StringUtils.uncapitalize(diploma.honour.title))

            } else if (PersonDocumentTypeCodes.DISABLED_CERT.equals(doc.documentType.code)) {

                // справка об инвалидности — <сокр. название типа документа> (<сокр. название категории инвалидности>) <серия> <номер>, выдан: <кем выдан> <когда выдан>, действителен до: <действителен до>
                str.append(personDoc.documentType.shortTitle)
                str.append(' (').append(personDoc.documentCategory.shortTitle).append(')')
                if (personDoc.seria) { str.append(' ').append(personDoc.seria) }
                if (personDoc.number) { str.append(' ').append(personDoc.number) }
                if (personDoc.issuanceDate || personDoc.issuancePlace) {
                    str.append(', выдан:')
                    if (personDoc.issuancePlace) { str.append(' ').append(personDoc.issuancePlace) }
                    if (personDoc.issuanceDate) { str.append(' ').append(DEFAULT_DATE_FORMATTER.format(personDoc.issuanceDate)) }
                }
                if (personDoc.expirationDate) {
                    str.append(', действителен до: ').append(DEFAULT_DATE_FORMATTER.format(personDoc.expirationDate))
                }

            }
            else {
                // другие документы — <сокр. название типа документа> <серия> <номер>, выдан: <кем выдан> <когда выдан>, действителен до: <действителен до>
                str.append(personDoc.documentType.shortTitle)
                if (personDoc.seria) {
                    str.append(' ').append(personDoc.seria)
                }
                if (personDoc.number) {
                    str.append(' ').append(personDoc.number)
                }
                if (personDoc.issuanceDate || personDoc.issuancePlace) {
                    str.append(', выдан:')
                    if (personDoc.issuancePlace) {
                        str.append(' ').append(personDoc.issuancePlace)
                    }
                    if (personDoc.issuanceDate) {
                        str.append(' ').append(DEFAULT_DATE_FORMATTER.format(personDoc.issuanceDate))
                    }
                }
                if (personDoc.expirationDate) {
                    str.append(', действителен до: ').append(DEFAULT_DATE_FORMATTER.format(personDoc.expirationDate))
                }
            }
        } else if (doc instanceof IEnrEntrantBenefitProofDocument || doc instanceof IEnrEntrantRequestAttachable) {
            // Что-то доселе неизвестное
            return doc.displayableTitle
        } else {
            throw new IllegalArgumentException()
        }

        return str.toString()
    }

    private IdentityCard cachedHeadCard // Кэшируем для старой метки в другом падеже
    /**
     * @param grammaCase падеж
     * @return "И.О. Фамилия" ректора в нужном падеже
     */
    def String getRectorIof(GrammaCase grammaCase)
    {
        final IdentityCard card = this.cachedHeadCard ?: (TopOrgUnit.instance.head?.employee?.person?.identityCard as IdentityCard) ?: getHeaderIdentityCard(TopOrgUnit.instance)
        if (card != null) {

            this.cachedHeadCard = card
            final headIof = new StrBuilder()
            headIof.append(card.firstName[0].toUpperCase()).append('.')
            if (card.middleName) {
                headIof.append(card.middleName[0].toUpperCase()).append('.')
            }
            headIof.append(' ')

            final customCase = DataAccessServices.dao().<IdentityCardDeclinability>getByNaturalId(new IdentityCardDeclinabilityGen.NaturalId(card))
            if (customCase != null) {
                if (customCase.isLastNameNonDeclinable()) {
                    return headIof.append(card.lastName).toString() // Фамилия не склоняется. Вставляем как есть
                } else {
                    // Фамилия склоняется, но, возможно, в базе правильное склонение указано пользователем
                    final variantMap = DeclinableManager.instance().dao().getPropertyValuesMap(customCase, IdentityCard.P_LAST_NAME)
                    final dativeLastName = variantMap?.get(DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase))
                    if (dativeLastName != null) {
                        return headIof.append(dativeLastName).toString()
                    }
                }
            }

            // Используем автосклонятор
            headIof.append(PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, grammaCase, card.sex.male))
            return headIof.toString()
        }
        return ''
    }

    /** @return Получаение удостоверения личности одного из руководителей подразделения */
    def IdentityCard getHeaderIdentityCard(final OrgUnit orgUnit)
    {
        return new DQLSelectBuilder().fromEntity(EmployeePost.class, 'p')
                .top(1)
                .column(property('p', EmployeePost.employee().person().identityCard()))
                .where(eq(property('p', EmployeePost.orgUnit()), value(orgUnit)))
                .where(eq(property('p', EmployeePost.postRelation().headerPost()), value(true)))
                .where(eq(property('p', EmployeePost.employee().archival()), value(false)))
                .where(eq(property('p', EmployeePost.postStatus().active()), value(true)))
                .createStatement(this.session).uniqueResult()
    }
}
