package unienr14.scripts.common

import com.google.common.collect.Lists
import com.google.common.collect.Sets
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author oleyba
 * @since 5/13/13
 */
return new EnrollmentExamAllExamsSheetPrint(                              // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrant: session.get(EnrEntrant.class, object) // объект печати
).print()

/**
 * Алгоритм печати экзаменационного листа абитуриента
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class EnrollmentExamAllExamsSheetPrint
{
    Session session
    byte[] template
    EnrEntrant entrant
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def card = entrant.person.identityCard

        im.put('highSchoolTitle', TopOrgUnit.instance.title);
        im.put('lastName', card.lastName);
        im.put('firstName', card.firstName);
        im.put('middleName', card.middleName);

        im.put('number', entrant.getPersonalNumber());
        im.put('date', new Date().format('dd.MM.yyyy'));

        fillEnrollmentResults();

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Экзаменационный лист абитуриента ${entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillEnrollmentResults()
    {
        // вычисляем внешние выбранные ВИ
        List<EnrChosenEntranceExamForm> chosenExamDisciplines = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExamForm.class, "e").column("e")
                .where(eq(property("e", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().id()), value(entrant.id)))
                .where(eq(property("e", EnrChosenEntranceExamForm.passForm().internal()), value(false)))
                .order(property("e", EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .order(property("e", EnrChosenEntranceExamForm.passForm().code()))
                .createStatement(session).list();


        // вычисляем внутренние дисциплины для сдачи
        List<EnrExamPassDiscipline> internalExamPassDisciplines = new DQLSelectBuilder()
            .fromEntity(EnrExamPassDiscipline.class, "e").column("e")
            .where(eq(property("e", EnrExamPassDiscipline.entrant().id()), value(entrant.id)))
            .where(eq(property("e", EnrExamPassDiscipline.retake()), value(Boolean.FALSE)))
            .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
            .order(property("e", EnrExamPassDiscipline.passForm().code()))
            .createStatement(session).list();

        // список строк
        final List<String[]> rows = new ArrayList<String[]>();

        Map<EnrCampaignDiscipline, Integer> discCountMap = new HashMap<>();
        for (EnrExamPassDiscipline exam : internalExamPassDisciplines) {
            discCountMap.put(exam.getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline())) + 1);
        }
        for (EnrChosenEntranceExamForm exam : chosenExamDisciplines) {
            discCountMap.put(exam.getChosenEntranceExam().getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getChosenEntranceExam().getDiscipline())) + 1);
        }


        Map<String, String> places = new LinkedHashMap<>();

        int i = 1;
        Set<Row> tableRowsSet = Sets.newHashSet();

        // для каждой дисциплины для сдачи
        for (def exam : internalExamPassDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline()));
            boolean printForm = discCount > 1;
            Long mark = exam.getMarkAsDouble() == null ? null : Math.round(exam.getMarkAsDouble());
            List<EnrExamGroupScheduleEvent> events = exam.getExamGroup() == null ? Collections.emptyList() :
                IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class,
                EnrExamGroupScheduleEvent.examGroup(), exam.getExamGroup(),
                EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());
            
            for (EnrExamGroupScheduleEvent event : events)
                places.put(event.examScheduleEvent.examRoom.place.displayableTitle, event.examScheduleEvent.examRoom.place.fullLocationInfo)
            
            // сформировать строку
            String[] row = new String[8];
            row[0] = i++;
            row[1] = exam.discipline.title + (printForm ? " (" + exam.passForm.title + ")" : "");
            row[2] = exam.examGroup?.title
            row[3] = events.collect { it.timeTitle + ", " + it.examScheduleEvent.examRoom.place.displayableTitle }.grep().join("\n");

            // итоговый балл цифрами и прописью
            row[4] = mark == null ? "" : mark;
            row[5] = mark == null ? "" : NumberSpellingUtil.spellNumberMasculineGender(mark)

            row[6] = "" // events.collect { it.examScheduleEvent.commission }.grep().join("\n"); - можно подставить, если хочется вывести экзаменаторов

            tableRowsSet.add(new Row(row, exam.discipline, exam.passForm));
        }

        // для каждой дисциплины для сдачи
        for (def exam : chosenExamDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getChosenEntranceExam().getDiscipline()));
            boolean printForm = discCount > 1;
            Long mark = exam.getMarkAsLong() == 0 ? null : Math.round(exam.getMarkAsDoubleNullSafe());

            // сформировать строку
            String[] row = new String[8];
            row[0] = i++;
            row[1] = exam.chosenEntranceExam.discipline.title + (printForm ? " (" + exam.passForm.title + ")" : "");
            row[2] = ""
            row[3] = "";

            // итоговый балл цифрами и прописью
            row[4] = mark == null ? "" : mark;
            row[5] = mark == null ? "" : NumberSpellingUtil.spellNumberMasculineGender(mark)

            row[6] = ""

            tableRowsSet.add(new Row(row, exam.chosenEntranceExam.discipline, exam.passForm));
        }

        List<Row> tableRows = Lists.newArrayList(tableRowsSet);

        Collections.sort(tableRows, new Comparator<Row>() {
            @Override
            int compare(Row o1, Row o2) {
                if(o1.discipline.equals(o2.discipline))
                {
                    return o1.passForm.title.compareTo(o2.passForm.title);
                }
                else
                {
                    return o1.discipline.title.compareTo(o2.discipline.title);
                }
            }
        })

        for(Row row : tableRows)
        {
            rows.add(row.getRow())
        }

        tm.put('T', rows as String[][]);

        def placeList = new ArrayList<String>()
        for (def place: places.entrySet())
            placeList.add(place.key + " - " + place.value)

        def resultPlace = new RtfString()
        for (def place: placeList)
        {
            if (placeList.indexOf(place) > 0)
                resultPlace.par()
            resultPlace.append(place)
        }

        im.put("place", resultPlace)
    }
}

class Row
{
    String[] row;
    EnrCampaignDiscipline discipline;
    EnrExamPassForm passForm;

    Row(String[] row, EnrCampaignDiscipline discipline, EnrExamPassForm passForm) {
        this.row = row
        this.discipline = discipline
        this.passForm = passForm
    }

    String[] getRow() {
        return row
    }

    EnrCampaignDiscipline getDiscipline() {
        return discipline
    }

    EnrExamPassForm getPassForm() {
        return passForm
    }

    @Override
    boolean equals(Object obj) {
        if(obj == null) return false;
        if(!(obj instanceof Row)) return false;

        return discipline.equals(obj.discipline) && passForm.equals(obj.passForm)
    }

    @Override
    int hashCode() {
        return discipline.hashCode() + passForm.hashCode();
    }
}


