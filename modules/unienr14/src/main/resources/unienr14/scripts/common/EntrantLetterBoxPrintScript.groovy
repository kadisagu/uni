package unienr14.scripts.common

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType

import static org.tandemframework.hibsupport.dql.DQLExpressions.and
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq
import static org.tandemframework.hibsupport.dql.DQLExpressions.or
import static org.tandemframework.hibsupport.dql.DQLExpressions.property
import static org.tandemframework.hibsupport.dql.DQLExpressions.value

return new EntrantLetterBoxPrint(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Denis Perminov
 * @since 27.06.2014
 */
class EntrantLetterBoxPrint {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print() {

        // получаем список выбранных направлений приема
        def directions;
        if (isIndividualAchievementsForRequest())
        {
            directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()}=${entrantRequest.id}
                order by ${EnrRequestedCompetition.request().regDate()}, ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()
        }
        else
            directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().entrant().id()}=${entrantRequest.entrant.id}
                order by ${EnrRequestedCompetition.request().regDate()}, ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillRequestedDirectionList(directions)
        fillInjectParameters()

        RtfDocument document = new RtfReader().read(template);

        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Титульный лист ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillRequestedDirectionList(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def rows = new ArrayList<String[]>()
        short i = 1

        //Поднимем данные рейтинга
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRatingItem.class, "r")
                                    .where(eq(property("r", EnrRatingItem.entrant()), value(entrantRequest.entrant)))
                                    .where(DQLExpressions.in(property("r", EnrRatingItem.requestedCompetition()), enrRequestedCompetitions));

        List<EnrRatingItem> ratingItemList = builder.createStatement(session).list();
        def ratingMap = ratingItemList.<EnrRequestedCompetition, EnrRatingItem> collectEntries { [it.requestedCompetition, it] }

        for (EnrRequestedCompetition requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из восьми столбцов
            String[] row = new String[8]

            row[0] = i++
            def programSet = requestedCompetition.competition.programSetOrgUnit.programSet
            def programSubject = programSet.programSubject
            def programForm = programSet.programForm
            def competitionType = requestedCompetition.competition.type
            def compensationType = competitionType.compensationType

            row[1] = programSubject.title + "-" + programForm.shortTitle + "-" + compensationType.shortTitle.substring(0,3)
            row[2] = requestedCompetition.request.stringNumber
            row[3] = DateFormatter.DEFAULT_DATE_FORMATTER.format(requestedCompetition.request.regDate)

            def choosenEntranceExam = DQL.createStatement(session, /
                    from ${EnrChosenEntranceExam.class.simpleName}
                    where ${EnrChosenEntranceExam.requestedCompetition().id()}=${requestedCompetition.id}
                    order by ${EnrChosenEntranceExam.discipline().id()}
                    /).<EnrChosenEntranceExam> list()

            def discipResult = new StringBuilder()
            def first = true
            def olymp = ""
            for (EnrChosenEntranceExam exam : choosenEntranceExam)
            {
                if (!first)
                    discipResult.append("\n")
                discipResult.append(exam.discipline.discipline.title)
                if (null != exam.maxMarkForm && exam.maxMarkForm.markAsLong > 0)
                {
                    discipResult.append("\u2013" + exam.maxMarkForm.markAsString)

                    if (exam.markSource instanceof EnrEntrantMarkSourceBenefit)
                    {
                        olymp = ((EnrEntrantMarkSourceBenefit) exam.markSource).benefitCategory.shortTitle
                    }
                }
                first = false
            }

            List<String> achievements = fillIndividualAchievements();
            for(String item : achievements)
            {
                if (!first)
                    discipResult.append("\n")
                discipResult.append(item)
                first=false
            }

            EnrRatingItem rating = ratingMap.get(requestedCompetition);
            row[4] = (rating != null && rating.statusRatingComplete && rating.totalMarkAsLong > 0) ? String.valueOf(rating.totalMarkAsLong / 1000) : ""
            row[5] = discipResult.toString()

            def coExcl = ""
            def competitionsExcl = requestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }.collect { e -> (EnrRequestedCompetitionExclusive) e }
            if (!competitionsExcl.isEmpty()) {
                coExcl = ((EnrRequestedCompetitionExclusive)competitionsExcl.get(0)).benefitCategory.shortTitle
            }
            def coNoEx = ""
            def competitionsNoExam = requestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }.collect { e -> (EnrRequestedCompetitionNoExams) e }
            if (!competitionsNoExam.isEmpty()) {
                coNoEx = ((EnrRequestedCompetitionNoExams)competitionsNoExam.get(0)).benefitCategory.shortTitle
            }
            row[6] = (!olymp.empty || !coExcl.empty || !coNoEx.empty) ? "Преим. " + [olymp, coExcl, coNoEx].toSet().toList().join(", ") : ""

            row[7] = (EnrCompetitionTypeCodes.MINISTERIAL.equals(competitionType.code) ? "" : competitionType.printTitle)
            rows.add(row)
        }
        tm.put("T", rows as String[][])
        // внедряем параграфы в 6 столбце
        tm.put("T", new RtfRowIntercepterBase() {
            @Override
            List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 5)
                {
                    String[] valStr = value.split("\n")
                    def first = true
                    RtfString newStr = new RtfString()
                    for (String item : valStr)
                    {
                        if (!first)
                            newStr.par()
                        newStr.append(item)
                        first = false
                    }
                    return newStr.toList()
                }
            }
        })
    }

    //формируем результаты обработки инд. достижений
    List<String> fillIndividualAchievements()
    {
        final achievementList = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'e')
                .column(property('e'))
                .where(eq(property('e', EnrEntrantAchievement.entrant()), value(this.entrantRequest.entrant)))
                .where(or(
                    and(eq(property('e', EnrEntrantAchievement.type().forRequest()), value(false)),
                            eq(property('e',EnrEntrantAchievement.type().achievementKind().requestType()), value(this.entrantRequest.type))),
                    and(eq(property('e', EnrEntrantAchievement.type().forRequest()), value(true)),
                            eq(property('e', EnrEntrantAchievement.request()), value(this.entrantRequest)))))
                .createStatement(this.session).<EnrEntrantAchievement>list()

        List<String> resultList = new ArrayList<>()
        achievementList.sort({a,b ->
            int result = (b.type.marked? b.markAsLong / 1000 : b.type.achievementMark).compareTo(a.type.marked? a.markAsLong / 1000 : a.type.achievementMark)
            if (result == 0)
                result = b.type.achievementKind.title.compareTo(a.type.achievementKind.title)
            return result})

        for (EnrEntrantAchievement achievement : achievementList)
        {
            long mark = achievement.type.marked? achievement.markAsLong / 1000 : achievement.type.achievementMark
            StringBuilder row = new StringBuilder(achievement.type.title)
            if (mark != null && mark > 0)
                row.append("\u2013").append(String.valueOf(mark))
          resultList.add(row.toString())
        }
        return resultList
    }

    def fillInjectParameters() {
        def entrant = entrantRequest.entrant
        def card = entrantRequest.identityCard

        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("lastName", card.lastName)
        im.put("entrantNumber", entrant.personalNumber)
        im.put("passport", card.title)
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("edEndYear", entrantRequest.eduDocument.yearEnd as String)
    }

    boolean isIndividualAchievementsForRequest()
    {
        return ISharedBaseDao.instance.get().existsEntity(new DQLSelectBuilder()
                .fromEntity(EnrEntrantAchievementType.class, "at")
                .where(eq(property("at", EnrEntrantAchievementType.forRequest()), value(true)))
                .where(eq(property("at", EnrEntrantAchievementType.achievementKind().requestType()), value(entrantRequest.type)))
                .where(eq(property("at", EnrEntrantAchievementType.enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
                .buildQuery())
    }
}