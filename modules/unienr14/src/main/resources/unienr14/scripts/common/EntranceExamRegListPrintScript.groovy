/* $Id$ */
package unienr14.scripts.common

import org.hibernate.Session
import org.tandemframework.caf.logic.wrapper.DataWrapper
import org.tandemframework.core.entity.IEntity
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.testng.collections.Maps
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic.EnrScheduleByRoomEventSearchDSHandler
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists
import static org.tandemframework.hibsupport.dql.DQLExpressions.in as dqlIn
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new EntranceExamRegListPrint(            // стандартные входные параметры скрипта
        session: session,
        template: template,
        enrExamScheduleEventList: enrExamScheduleEventList
).print()
/**
 * @author Denis Katkov
 * @since 31.05.2016
 */
class EntranceExamRegListPrint {
    Session session
    byte[] template
    Collection<IEntity> enrExamScheduleEventList

    def print() {
        RtfDocument document = new RtfReader().read(template);

        injectModifier().modify(document);
        injectTableModifier().modify(document);

        return [document: document, fileName: 'Журнал регистрации абитуриентов на ВИ.rtf']
    }

    def RtfInjectModifier injectModifier()
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        Map<String, String> markerMap = Maps.newHashMap();
        markerMap.put("subject", enrExamScheduleEventList.sort {((DataWrapper)it).getProperty(EnrScheduleByRoomEventSearchDSHandler.V_PROP_DISCIPLINE)}.collect{((DataWrapper)it).getProperty(EnrScheduleByRoomEventSearchDSHandler.V_PROP_DISCIPLINE)}.join(", "));
        markerMap.put("room", enrExamScheduleEventList.sort{ ((EnrExamScheduleEvent)((DataWrapper)it).getWrapped()).examRoom.place.displayableTitle }.collect{((EnrExamScheduleEvent)((DataWrapper)it).getWrapped()).examRoom.place.displayableTitle}.join(", "));
        markerMap.put("passDate", enrExamScheduleEventList.sort{ ((EnrExamScheduleEvent)((DataWrapper)it).getWrapped()).scheduleEvent.durationBegin }.collect{DateFormatter.DATE_FORMATTER_WITH_TIME.format(
                ((EnrExamScheduleEvent)((DataWrapper)it).getWrapped()).scheduleEvent.durationBegin)}.join(", "));

        modifier.put("highSchoolTitle", ((EnrExamScheduleEvent)((DataWrapper)enrExamScheduleEventList.stream().findAny().get()).getWrapped()).getExamRoom().getResponsibleOrgUnit().getTitle());
        markerMap.each {modifier.put(it.getKey(), it.getValue().toString())};
        return modifier;
    }

    def RtfTableModifier injectTableModifier()
    {
        final RtfTableModifier tableModifier = new RtfTableModifier();

        def innerDql = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "event")
                .where(dqlIn(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("event")), enrExamScheduleEventList))
                .column(property(EnrExamGroupScheduleEvent.examGroup().id().fromAlias("event")));

        def dql = new DQLSelectBuilder().fromEntity(EnrEntrant, "ent")
                .where(dqlIn(property(EnrEntrant.id().fromAlias("ent")),
                        new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "disc")
                                .where(exists(EnrExamGroup.class,
                                EnrExamGroup.id().s(), property(EnrExamPassDiscipline.examGroup().id().fromAlias("disc")),
                                EnrExamGroup.id().s(), innerDql.createStatement(session).list()))
                                .group(property(EnrExamPassDiscipline.entrant().id().fromAlias("disc")))
                                .column(property(EnrExamPassDiscipline.entrant().id().fromAlias("disc"))).buildQuery()))
                .order(property(EnrEntrant.person().identityCard().fullFio().fromAlias("ent")))

        def enrExamPassDisciplineList  = dql.createStatement(session).<EnrEntrant> list();

        int flag;
        List<String[]> table = new ArrayList<>();
        enrExamPassDisciplineList.each {
            String[] line = new String[4];
            line[0] = String.valueOf(++flag);
            line[1] = it.personalNumber;
            line[2] = it.fullFio;
            line[3] = it.person.identityCard.fullNumber;
            table.add(line);
        }

        tableModifier.put("T", table.toArray(new String[table.size()][]));

        return tableModifier;
    }
}