package unienr14.scripts.common

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantDocumentType
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument

return new DocumentListAndReceiptPrint(                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author oleyba
 * @since 5/13/13
 */
class DocumentListAndReceiptPrint
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def person = entrantRequest.entrant.person

        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('inventoryNumber', entrantRequest.stringNumber)
        im.put('FIO', person.fullFio)
        im.put('receiptNumber', entrantRequest.stringNumber)

        def fio
        def identityCardDeclinability = IUniBaseDao.instance.get().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), person.identityCard.id)
        if (identityCardDeclinability != null)
        {
            def lastName = person.identityCard.lastName
            if (identityCardDeclinability.lastNameDeclinable)
                lastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastName, GrammaCase.GENITIVE, person.isMale())

            def firstName = person.identityCard.firstName
            if (identityCardDeclinability.firstNameDeclinable)
                firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstName, GrammaCase.GENITIVE, person.isMale())

            def middleName = person.identityCard.middleName
            if (identityCardDeclinability.middleNameDeclinable)
                middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, person.isMale())

            def result = new ArrayList<String>()
            if (!StringUtils.isEmpty(lastName)) result.add(lastName)
            if (!StringUtils.isEmpty(firstName)) result.add(firstName)
            if (!StringUtils.isEmpty(middleName)) result.add(middleName)

            fio = StringUtils.join(result, ' ')

        } else
        {
            fio = PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.GENITIVE)
        }

        im.put('fromFIO', fio)

        // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок'
        def titles = getTitles()

        int i = 1
        tm.put('T1', titles.collect {[i++, it]} as String[][])
        i = 1
        tm.put('T2', titles.collect {[(i++) + '.', it]} as String[][])

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Опись и расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles()
    {
        def entrant = entrantRequest.entrant    // абитуриент
        def idc = entrantRequest.identityCard   // выбранное в заявлении УЛ
        def edu = entrantRequest.eduDocument // выбранный док. об образовании

        // получаем список документов из заявления в порядке приоритета
        def attachments = DQL.createStatement(session, /
                from ${EnrEntrantRequestAttachment.class.simpleName}
                where ${EnrEntrantRequestAttachment.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EnrEntrantRequestAttachment.id()}
        /).<EnrEntrantRequestAttachment> list()

        attachments.sort() // в объекте реализован корректный compareTo() - сначала по типу по настройке порядка документов, затем по дате создания документа

        def titles = new ArrayList<String>()
        
        titles.add(idc.getDisplayableTitle() + " (копия)")
        titles.add(edu.getDisplayableTitle() + (entrantRequest.eduInstDocOriginalHandedIn ? " (оригинал)" : " (копия)"))
        
        for (def attachment: attachments) {
            titles.add(attachment.document.displayableTitle + (attachment.originalHandedIn ? " (оригинал)" : " (копия)"))
        }
        return titles
    }
}

