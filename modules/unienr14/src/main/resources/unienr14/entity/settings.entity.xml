<?xml version="1.0" encoding="UTF-8"?>

<entity-config name="unienr14-settings" package-name="ru.tandemservice.unienr14.settings.entity" xmlns="http://www.tandemframework.org/meta/entity" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.tandemframework.org/meta/entity http://www.tandemframework.org/schema/meta/meta-entity.xsd">

    <entity name="enrEnrollmentCampaign" title="Приемная кампания" table-name="enr14_campaign_t">
        <comment>
            Проводится в рамках аккредитованного подразделения.
        </comment>
        <many-to-one name="educationYear" entity-ref="educationYear" title="Учебный год" required="true"/>
        <property name="title" type="trimmedstring" title="Название" required="true" unique="true"/>
        <many-to-one name="settings" entity-ref="enrEnrollmentCampaignSettings" title="Настройки ПК" required="true" unique="true" forward-cascade="delete"/>

        <property name="dateFrom" type="date" title="Дата начала приема" required="true"/>
        <property name="dateTo" type="date" title="Дата окончания приема" required="true"/>

        <property name="open" type="boolean" title="Открыта" required="true" default-value="true"/>

        <implement interface-ref="org.tandemframework.shared.organization.sec.entity.ILocalRoleContext"/>
        <property name="localRoleContextTitle" type="trimmedstring" formula="title"/>

        <check-constraint name="enrCampaign_achievementSettingCount" insert-phase="commit" update-phase="commit">
            <comment>Для всех видов зявлений должна быть настройка индивидуальных достижений ПК.</comment>
            <![CDATA[  (select count(*) from enrRequestType) = (select count(*) from enrEntrantAchievementSettings s where s.enrollmentCampaign.id = id) ]]>
        </check-constraint>
    </entity>

    <entity name="enrEnrollmentCampaignSettings" title="Настройки приемной кампании" table-name="enr14_campaign_settings_t">
        <comment>
            Хранит все настройки приемной кампании.
        </comment>

        <property name="reenrollByPriorityEnabled" type="boolean" required="true" default-value="true" title="Перезачислять на лучший приоритет">
            <comment>
                http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686876
                Влияет на разрешение конфликтов при формировании шага зачисления:
                false - не разрешать перезачислять абитуриентов;
                true - разрешать перезачислять абитуриентов
            </comment>
        </property>

        <property name="stateExamRestriction" type="boolean" required="false" title="Ограничение выбора конкурсов в заявлении по данным свидетельств ЕГЭ (покрытие набора ВИ баллами по предметам ЕГЭ)">
        	<comment>
                DEV-2891 DEV-2856
        		Влияет на поведение формы добавления заявления (добавление НП/КГ) абитуриента: отображение и значение по умолчанию для поля «Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении»:
        		null - Не используется (поле не отображается);
        		false - Используется, по умолчанию выбор не ограничен (поле отображается, по умолчанию выключено);
        		true - Используется, по умолчанию выбор ограничен (поле отображается, по умолчанию включено)
        	</comment>
        </property>

        <property name="numberOnRequestManual" type="boolean" required="true" title="Присваивать регистрационный номер заявления вручную">
            <comment>
                DEV-2840
           		Влияет на поведение формы добавления заявления абитуриента: отображение поля «№ заявления»:
           		false - поле не отображается, номер присваивается инкрементом от максимального в базе в рамках ПК при сохранении заявления;
           		true - поле не отображается, заполняется инкрементом от максимального в базе в рамках ПК на момент открытия формы, при сохранении проверяется на уникальность в рамках ПК
           	</comment>
        </property>

        <property name="stateExamAutoCheck" type="boolean" required="true" title="Регистрировать свидетельства ЕГЭ проверенными">
            <comment>
                DEV-2920
                Влияет на поведение формы создания свидетельства ЕГЭ:
                false - свидетельство создается не проверенным (признак проверено = true);
                true - свидетельство создается проверенным (признак проверено = true)
            </comment>
        </property>

        <property name="examGroupTitleFormingManual" type="boolean" required="true" title="Формировать названия экзам. групп вручную">
            <comment>
                Влияет на поведение формы создания ЭГ:
                true - название ЭГ задается вручную
                false - названия ЭГ формируются системой автоматически: представляют из себя натуральные числа, начиная с 1, при добавлении ЭГ в качестве ее названия берется наибольшее число из названий уже созданных ЭГ в ПК +1
            </comment>
        </property>

        <property name="stateExamAccepted" type="boolean" required="true" title="Добавлять результаты ЕГЭ по умолчанию зачтенными">
            <comment>
                Влияет на поведение формы создания результатов ЕГЭ:
                true – результаты ЕГЭ по умолчанию добавляются зачтенными;
                false – результаты ЕГЭ по умолчанию добавляются не зачтенными.
            </comment>
        </property>

        <property name="acceptedContractDefault" type="boolean" required="true" default-value="false" title="При выборе конкурса на договор абитуриент по умолчанию согласен на зачисление"/>

        <property name="targetAdmissionCompetition" type="boolean" required="true" title="Конкурс по целевому приему">
            <comment>
                false - общий для всех видов целевого приема
                true - отдельный для каждого вида целевого приема
            </comment>
        </property>

        <property name="examIntervalAuto" type="integer" required="true" title="Промежуток между ВИ при автомат. выборе ЭГ (дн.)" default-value="1"/>
        <property name="examIntervalManual" type="integer" required="true" title="Промежуток между ВИ при ручном выборе ЭГ (дн.)" default-value="1"/>
        <property name="appealDeadlineDays" type="integer" required="true" title="Срок подачи апелляции" default-value="1"/>
        <property name="appealHearingDays" type="integer" required="true" title="Срок рассмотрения апелляции" default-value="1"/>
        
        <property name="enrEntrantContractExistCheckMode" type="long" required="true" title="Вариант проверки наличия договора при определении возможности зачисления по договору" default-value="0">
        	<comment>
        		Используется, только если модуль unienr14_ctr подключен, соответствует коду проверки в EnrEntrantContractExistsRule (unienr14_ctr).
        		0 - означает, что проверка не используется (наличие договора не требуется).
        	</comment>
        </property>

        <property name="accountingRulesOrigEduDoc" type="long" required="true" title="Правила учета оригинала документа об образовании абитуриента" default-value="0">
            <comment>
                TODO:
            </comment>
        </property>

        <property name="enrollmentRulesBudget" type="long" required="true" title="Условия зачисления при поступлении на места в рамках КЦП" default-value="1">
            <comment>
                TODO:
            </comment>
        </property>

        <property name="accountingRulesAgreementBudget" type="long" required="true" title="Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП" default-value="0">
            <comment>
                TODO:
            </comment>
        </property>

        <property name="enrollmentRulesContract" type="long" required="true" title="Условия зачисления при поступлении на места по договору" default-value="1">
            <comment>
                TODO:
            </comment>
        </property>

        <property name="accountingRulesAgreementContract" type="long" required="true" title="Правила учета согласия на зачисление абитуриента при поступлении на места по договору" default-value="1">
            <comment>
                TODO:
            </comment>
        </property>

        <property name="enrEntrantExamListPrintType" type="long" required="true" title="Тип печати экзаменационного листа">
            <comment>
                DEV-5450:
                1 - Выводить все вступительные испытания;
                2 - Выводить только внутренние вступительные испытания.
            </comment>
        </property>

        <property name="excludeEnrolledByLowPriority" type="boolean" required="true" title="Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом"/>

        <many-to-one name="tieBreakRatingRule" entity-ref="enrTieBreakRatingRule" required="true"/>
        <many-to-one name="levelBudgetFinancing" entity-ref="enrLevelBudgetFinancing" required="true"/>
        <property name="acceptPeopleResidingInCrimea" type="boolean" required="true" default-value="false" title="Прием лиц, постоянно проживающих в Крыму"/>
        <many-to-one name="enrollmentCampaignType" entity-ref="enrEnrollmentCampaignType" title="Тип приемной кампании"/>
    </entity>
    
    <entity name="enrEnrollmentCampaignRequestWizardSettings" title="Настройки мастера добавления заявления приемной кампании" table-name="enr14_campaign_rwsettings_t">
        <comment>
            Хранит настройки полей, шагов и проверок для мастера добавления заявления в приемной кампании.
        </comment>
        <natural-id>
        	<many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>
        </natural-id>
        
        <property name="mainDataStepVisible" type="boolean" required="true" title="Отображать шаг «Основные данные»" default-value="true"/>
        <property name="contactsStepVisible" type="boolean" required="true" title="Отображать шаг «Контактная информация»" default-value="true"/>
        <property name="nextOfKinStepVisible" type="boolean" required="true" title="Отображать шаг «Ближайшие родственники»" default-value="true"/>
        <property name="militaryStepVisible" type="boolean" required="true" title="Отображать шаг «Служба в армии»" default-value="true"/>
        
    </entity>



    <entity name="enrOrgUnit" title="Филиал в рамках ПК" table-name="enr14_orgunit_t">
        <comment>
            Подразделение, аккредитованное осуществлять прием.
            Существует в рамках приемной кампании учебного года, т.е. создается, если есть аккредитация на прием в этом учебном году.
        </comment>
        <natural-id>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" required="true" title="Приемная кампания" backward-cascade="delete"/>
            <many-to-one name="institutionOrgUnit" entity-ref="eduInstitutionOrgUnit" required="true" title="Филиал"/>
        </natural-id>

        <property name="fisDataSendingIndependent" type="boolean" required="true" title="Передает данные в ФИС самостоятельно">
           <comment>Для связи с ФИС.</comment>
        </property>
        <property name="login" type="trimmedstring" required="false" title="Логин">
           <comment>Для связи с ФИС.</comment>
        </property>
        <property name="password" type="trimmedstring" required="false" title="Пароль">
           <comment>Для связи с ФИС.</comment>
       </property>

        <property name="fisUid" type="integer" title="Идентификатор в ФИС"/>

        <check-constraint name="enrOrgUnit_fisUid" message="Идентификатор в ФИС не может быть отрицательным.">
            <![CDATA[ fisUid is null or fisUid >= 0]]>
        </check-constraint>
    </entity>

    <entity name="enrEntrantAchievementSettings" title="Настройки индивидуальных достижений в ПК" table-name="enr14_achievement_sets_t">
        <natural-id>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" required="true" title="Приемная кампания" backward-cascade="delete"/>
            <many-to-one name="requestType" entity-ref="enrRequestType" required="true" title="Вид заявления"/>
        </natural-id>

        <property name="maxTotalAchievementMarkAsLong" type="long" required="true" title="Ограничение на сумму баллов индивидуальных достижений">
            <comment>
                Хранится со смещением в три знака для дробной части.
                Задает максимальное значение суммы баллов всех достижений абитуриента, за исключением итогового сочинения.
            </comment>
        </property>
    </entity>

    <entity name="enrEntrantAchievementType" title="Вид индивидуального достижения в ПК" table-name="enr14_achievement_type_t">
        <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" required="true" title="Приемная кампания" backward-cascade="delete"/>
        <many-to-one name="achievementKind" entity-ref="enrEntrantAchievementKind" required="true" title="Вид индивидуального достижения" backward-cascade="delete"/>
        <property name="marked" type="boolean" required="true" title="Оценивается"/>
        <property name="forRequest" type="boolean" required="true" default-value="false" title="Учитывается в рамках заявления"/>

        <property name="achievementMarkAsLong" type="long" required="true" title="Балл (макс. балл) за достижение">
            <comment>
                Хранится со смещением в три знака для дробной части.
                Если достижение оценивается, то поле используется в качестве ограничения максимального балла за достижение, если не оценивается — в качестве балла за достижение.
            </comment>
        </property>

        <unique-constraint name="achievementKind" message="Указанный вид индивидуального достижения уже добавлен в данную приемную кампанию">
            <property-link name="enrollmentCampaign"/>
            <property-link name="achievementKind"/>
        </unique-constraint>

        <check-constraint name="achievementMark" message="Балл за достижение не может быть меньше нуля.">
            achievementMarkAsLong >= 0
        </check-constraint>
    </entity>

    <entity name="enrCampaignTargetAdmissionKind" title="Вид ЦП, используемый в рамках ПК" table-name="enr14_camp_target_adm_kind_t">
        <natural-id>
            <many-to-one name="targetAdmissionKind" entity-ref="enrTargetAdmissionKind" title="Вид ЦП" required="true"/>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>
        </natural-id>
        <property name="stateInterest" type="boolean" required="true" default-value="false" title="Прием в интересах государства"/>
    </entity>

    <entity name="enrTargetAdmissionOrgUnit" title="Организация ЦП">
        <natural-id>
            <many-to-one name="enrCampaignTAKind" entity-ref="enrCampaignTargetAdmissionKind" required="true" title="Вид ЦП, используемый в рамках ПК" backward-cascade="delete"/>
            <many-to-one name="externalOrgUnit" entity-ref="externalOrgUnit" required="true" title="Внешняя организация"/>
        </natural-id>
    </entity>

    <entity name="enrCampaignDiscipline" title="Дисциплина, используемая в рамках ПК" table-name="enr14_camp_discipline_t">
        <natural-id>
            <many-to-one name="discipline" entity-ref="enrDiscipline" title="Дисциплина" required="true"/>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>
        </natural-id>
        <many-to-one name="stateExamSubject" entity-ref="enrStateExamSubject" required="false"/>
        
        <property name="defaultPassMarkAsLong" type="long" required="true" title="Зачетный балл по умолчанию" formula="discipline.defaultPassMarkAsLong" lazy="true">
            <comment>Хранится со смещением в три знака для дробной части.</comment>
        </property>
        
        <implement interface-ref="ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue"/>
    </entity>

    <entity name="enrCampaignDisciplineGroup" title="Группа дисциплин, используемая в рамках ПК" table-name="enr14_camp_disc_group_t">
        <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete" unique="uk_enr_camp_disc_group_title,uk_enr_camp_disc_group_sh_title"/>
        <property name="title" type="trimmedstring" title="Название" required="true" unique="uk_enr_camp_disc_group_title"/>
        <property name="shortTitle" type="trimmedstring" title="Сокращенное название" required="true" unique="uk_enr_camp_disc_group_sh_title"/>

        <property name="defaultPassMarkAsLong" type="long" required="true" title="Зачетный балл по умолчанию">
            <comment>
            	Обновляется при сохранении группы (и демоном IEnrCampDisciplineGroupDaemonBean), как максимальный зачетный бал по умолчанию из дисциплин группы.
            	Хранится со смещением в три знака для дробной части.
            </comment>
        </property>

        <implement interface-ref="ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue"/>
    </entity>

    <entity name="enrCampaignDisciplineGroupElement" title="Дисциплина в группе в рамках ПК" table-name="enr14_camp_disc_group_el_t">
        <natural-id>
            <many-to-one name="discipline" entity-ref="enrCampaignDiscipline" title="Дисциплина" required="true"/>
            <many-to-one name="group" entity-ref="enrCampaignDisciplineGroup" title="Группа" required="true" backward-cascade="delete"/>
        </natural-id>

        <check-constraint name="enrCampaignDisciplineGroupElement_enrollmentCampaign" message="В группу дисциплин можно включать только дисциплины той же ПК.">
            <![CDATA[ discipline.enrollmentCampaign=group.enrollmentCampaign ]]>
        </check-constraint>
        
    </entity>

    <!--
    <entity name="enrEduYearBenefitCategory" title="Категория граждан, обладающих особым правом при поступлении в учебном году" table-name="enr14_edu_year_benefit_cat_t">
        <natural-id>
            <many-to-one name="educationYear" entity-ref="educationYear" title="Учебный год" required="true"/>
            <many-to-one name="benefitCategory" entity-ref="enrBenefitCategory" title="Категория граждан" required="true"/>
        </natural-id>

        <many-to-one name="benefitType" entity-ref="enrBenefitType" title="Вид льготы, предоставляемый данной категории" required="true"/>
    </entity>
    -->

    <entity name="enrCampaignEntrantDocument" title="Тип документа абитуриента, используемый в рамках ПК" table-name="enr14_camp_entrant_doc_t">
        <natural-id>
            <many-to-one name="documentType" entity-ref="personDocumentType" title="Тип документа" required="true"/>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>
        </natural-id>

        <property name="benefitNoExams"    type="boolean" required="true" title="Подтверждает особое право - без ВИ"/>
        <property name="benefitExclusive"  type="boolean" required="true" title="Подтверждает особое право - квота"/>
        <property name="benefitPreference" type="boolean" required="true" title="Подтверждает особое право - преимущество"/>
        <property name="benefitMaxMark"    type="boolean" required="true" title="Подтверждает особое право - 100 баллов"/>
        
        <property name="achievement" type="boolean" required="true" title="Подтверждает индивидуальное достижение"/>
        <property name="copyCanBeAccepted" type="boolean" required="true" title="Разрешено принимать копии"/>
        <property name="originalCanBeAccepted" type="boolean" required="true" title="Разрешено принимать оригинал"/>
        <property name="sendFIS" type="boolean" required="true" default-value="true" title="Передавать в ФИС ГИА и приема"/>
    </entity>

    <entity name="enrExamGroupSet" title="Набор экзаменационных групп" table-name="enr14_exam_grp_set_t">
        <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>

        <property name="beginDate" type="date" required="true" title="Дата с"/>
        <property name="endDate" type="date" required="true" title="Дата по"/>
        <property name="size" type="integer" required="true" title="Число мест в ЭГ (по умолчанию)"/>

        <property name="open" type="boolean" required="true" title="Открыт"/>

        <unique-constraint name="uk_enr_exam_grp_set_open" message="Нельзя добавить набор ЭГ, т.к. уже есть открытый набор. Необходимо закрыть существующий набор ЭГ перед добавлением нового.">
            <property-link name="enrollmentCampaign"/>
            <condition>open=true</condition>
        </unique-constraint>

    </entity>

    <entity name="enrCampaignExamRoom" title="Аудитория для проведения ВИ" table-name="enr14_camp_exam_room_t">
        <natural-id>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>
            <many-to-one name="place" entity-ref="uniplacesPlace" title="Помещение" required="true"/>
        </natural-id>

        <many-to-one name="responsibleOrgUnit" entity-ref="orgUnit" title="Ответственное терр. подразделение" required="true"/>
        <many-to-one name="examRoomType" entity-ref="enrExamRoomType" title="Тип аудитории" required="true"/>
        <property name="preparedForDisabledPeople" type="boolean" required="true" title="Подготовлена для проведения ВИ лиц с огр. возм."/>

        <property name="size" type="integer" required="true" title="Число мест"/>

        <property name="comment" type="trimmedstring" title="Комментарий" length="2000"/>
    </entity>

    <entity name="enrPartnerEduInstitution" title="Партнерские образовательные учреждения">
        <many-to-one name="eduInstitution" entity-ref="eduInstitution" title="Образовательное учреждение" required="true"/>

        <unique-constraint name="uk_enr_partner_edu_inst">
            <property-link name="eduInstitution"/>
        </unique-constraint>
    </entity>

    <entity name="enrCampaignEnrollmentStage" title="Стадия зачисления в рамках ПК" table-name="enr14_camp_enr_stage_t">
        <natural-id mutable="true">
            <many-to-one name="requestType" entity-ref="enrRequestType" title="Вид заявления" required="true"/>
            <many-to-one name="competitionType" entity-ref="enrCompetitionType" title="Вид приема" required="true"/>
            <many-to-one name="programForm" entity-ref="eduProgramForm" required="true" title="Форма обучения"/>
            <many-to-one name="enrollmentCampaign" entity-ref="enrEnrollmentCampaign" title="ПК" required="true" backward-cascade="delete"/>
            <property name="date" type="date" required="true" title="Дата зачисления"/>
            <property name="acceptPeopleResidingInCrimea" type="boolean" required="true" default-value="false" title="Прием лиц, постоянно проживающих в Крыму"/>
        </natural-id>
    </entity>

    <!--
    <entity name="enrOrderTypeToGroup" title="Связь типа приказа о зачислении с группой" table-name="enr14_order_type2grp_t">
        <natural-id>
            <many-to-one name="type" entity-ref="enrOrderType" required="true"/>
            <many-to-one name="group" entity-ref="studentExtractGroup" required="true"/>
        </natural-id>
    </entity>
    -->

    <entity name="enrOrderReasonToBasicsRel" title="Связь причин с основаниями для приказов по абитуриентам" table-name="enr14_order_reason2basic_t">
        <natural-id>
            <many-to-one name="reason" entity-ref="enrOrderReason" required="true" backward-cascade="delete"/>
            <many-to-one name="basic" entity-ref="enrOrderBasic" required="true" backward-cascade="delete"/>
        </natural-id>
    </entity>

    <entity name="enrVisasTemplate" title="Шаблон визирования" table-name="enr14_visa_template_t">
        <property name="title" type="trimmedstring" required="true" title="Название"/>
    </entity>

    <entity name="enrCampaignOrderVisaItem" title="Виза для приказа о зачислении" table-name="enr14_visa_item_t">
        <many-to-one name="possibleVisa" entity-ref="ru.tandemservice.unimv.IPossibleVisa" required="true" backward-cascade="delete"/>
        <many-to-one name="visaTemplate" entity-ref="enrVisasTemplate" required="true" backward-cascade="delete"/>
        <property name="priority" type="integer" required="true" title="Приоритет"/>
        <many-to-one entity-ref="groupsMemberVising" title="Группа участников визирования" required="true"/>

        <unique-constraint name="uk_enr_camp_order_visa_template">
            <property-link name="visaTemplate"/>
            <property-link name="groupsMemberVising"/>
            <property-link name="possibleVisa"/>
        </unique-constraint>

        <unique-constraint name="uk_enr_camp_order_visa_priority">
            <property-link name="visaTemplate"/>
            <property-link name="groupsMemberVising"/>
            <property-link name="priority"/>
        </unique-constraint>
    </entity>


</entity-config>
