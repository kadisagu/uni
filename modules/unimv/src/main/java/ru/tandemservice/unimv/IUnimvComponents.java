/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv;

/**
 * @author vip_delete
 */
public interface IUnimvComponents
{
    String VISA_TASK_TAB = ru.tandemservice.unimv.component.visatask.VisaTaskList.Controller.class.getPackage().getName();
    String VISA_ADD = "ru.tandemservice.unimv.component.visa.VisaAdd";
    String VISA_HISTORY_LIST = "ru.tandemservice.unimv.component.visahistory.VisaHistoryList";
    String VISA_LIST = "ru.tandemservice.unimv.component.visa.VisaList";
    String VISA_TASK_ACCEPT = "ru.tandemservice.unimv.component.visatask.VisaTaskAccept";
    String VISA_TASK_REJECT = "ru.tandemservice.unimv.component.visatask.VisaTaskReject";
}
