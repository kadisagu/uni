package ru.tandemservice.unimv.base.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа участников визирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupsMemberVisingGen extends EntityBase
 implements INaturalIdentifiable<GroupsMemberVisingGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising";
    public static final String ENTITY_NAME = "groupsMemberVising";
    public static final int VERSION_HASH = 978298347;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String P_DEFAULT_USE = "defaultUse";
    public static final String P_PRINT_LABEL = "printLabel";
    public static final String P_INDEX = "index";
    public static final String P_PRINT_LABEL_TITLE = "printLabelTitle";

    private String _code;     // Системный код
    private String _title;     // Название группы
    private String _printTitle;     // Печатное название
    private boolean _defaultUse;     // Используется по умолчанию
    private String _printLabel;     // Метка в печатном шаблоне
    private int _index;     // Номер
    private String _printLabelTitle;     // Метка в шаблоне с печатным названием группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название группы. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название группы. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Печатное название.
     */
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Используется по умолчанию. Свойство не может быть null.
     */
    @NotNull
    public boolean isDefaultUse()
    {
        return _defaultUse;
    }

    /**
     * @param defaultUse Используется по умолчанию. Свойство не может быть null.
     */
    public void setDefaultUse(boolean defaultUse)
    {
        dirty(_defaultUse, defaultUse);
        _defaultUse = defaultUse;
    }

    /**
     * @return Метка в печатном шаблоне. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getPrintLabel()
    {
        return _printLabel;
    }

    /**
     * @param printLabel Метка в печатном шаблоне. Свойство не может быть null и должно быть уникальным.
     */
    public void setPrintLabel(String printLabel)
    {
        dirty(_printLabel, printLabel);
        _printLabel = printLabel;
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index Номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Метка в шаблоне с печатным названием группы. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getPrintLabelTitle()
    {
        return _printLabelTitle;
    }

    /**
     * @param printLabelTitle Метка в шаблоне с печатным названием группы. Свойство не может быть null и должно быть уникальным.
     */
    public void setPrintLabelTitle(String printLabelTitle)
    {
        dirty(_printLabelTitle, printLabelTitle);
        _printLabelTitle = printLabelTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupsMemberVisingGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((GroupsMemberVising)another).getCode());
            }
            setTitle(((GroupsMemberVising)another).getTitle());
            setPrintTitle(((GroupsMemberVising)another).getPrintTitle());
            setDefaultUse(((GroupsMemberVising)another).isDefaultUse());
            setPrintLabel(((GroupsMemberVising)another).getPrintLabel());
            setIndex(((GroupsMemberVising)another).getIndex());
            setPrintLabelTitle(((GroupsMemberVising)another).getPrintLabelTitle());
        }
    }

    public INaturalId<GroupsMemberVisingGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<GroupsMemberVisingGen>
    {
        private static final String PROXY_NAME = "GroupsMemberVisingNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof GroupsMemberVisingGen.NaturalId) ) return false;

            GroupsMemberVisingGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupsMemberVisingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupsMemberVising.class;
        }

        public T newInstance()
        {
            return (T) new GroupsMemberVising();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "printTitle":
                    return obj.getPrintTitle();
                case "defaultUse":
                    return obj.isDefaultUse();
                case "printLabel":
                    return obj.getPrintLabel();
                case "index":
                    return obj.getIndex();
                case "printLabelTitle":
                    return obj.getPrintLabelTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "defaultUse":
                    obj.setDefaultUse((Boolean) value);
                    return;
                case "printLabel":
                    obj.setPrintLabel((String) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "printLabelTitle":
                    obj.setPrintLabelTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "printTitle":
                        return true;
                case "defaultUse":
                        return true;
                case "printLabel":
                        return true;
                case "index":
                        return true;
                case "printLabelTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "printTitle":
                    return true;
                case "defaultUse":
                    return true;
                case "printLabel":
                    return true;
                case "index":
                    return true;
                case "printLabelTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "defaultUse":
                    return Boolean.class;
                case "printLabel":
                    return String.class;
                case "index":
                    return Integer.class;
                case "printLabelTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupsMemberVising> _dslPath = new Path<GroupsMemberVising>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupsMemberVising");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название группы. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Печатное название.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Используется по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#isDefaultUse()
     */
    public static PropertyPath<Boolean> defaultUse()
    {
        return _dslPath.defaultUse();
    }

    /**
     * @return Метка в печатном шаблоне. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getPrintLabel()
     */
    public static PropertyPath<String> printLabel()
    {
        return _dslPath.printLabel();
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Метка в шаблоне с печатным названием группы. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getPrintLabelTitle()
     */
    public static PropertyPath<String> printLabelTitle()
    {
        return _dslPath.printLabelTitle();
    }

    public static class Path<E extends GroupsMemberVising> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _printTitle;
        private PropertyPath<Boolean> _defaultUse;
        private PropertyPath<String> _printLabel;
        private PropertyPath<Integer> _index;
        private PropertyPath<String> _printLabelTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(GroupsMemberVisingGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название группы. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(GroupsMemberVisingGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Печатное название.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(GroupsMemberVisingGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Используется по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#isDefaultUse()
     */
        public PropertyPath<Boolean> defaultUse()
        {
            if(_defaultUse == null )
                _defaultUse = new PropertyPath<Boolean>(GroupsMemberVisingGen.P_DEFAULT_USE, this);
            return _defaultUse;
        }

    /**
     * @return Метка в печатном шаблоне. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getPrintLabel()
     */
        public PropertyPath<String> printLabel()
        {
            if(_printLabel == null )
                _printLabel = new PropertyPath<String>(GroupsMemberVisingGen.P_PRINT_LABEL, this);
            return _printLabel;
        }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(GroupsMemberVisingGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Метка в шаблоне с печатным названием группы. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising#getPrintLabelTitle()
     */
        public PropertyPath<String> printLabelTitle()
        {
            if(_printLabelTitle == null )
                _printLabelTitle = new PropertyPath<String>(GroupsMemberVisingGen.P_PRINT_LABEL_TITLE, this);
            return _printLabelTitle;
        }

        public Class getEntityClass()
        {
            return GroupsMemberVising.class;
        }

        public String getEntityName()
        {
            return "groupsMemberVising";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
