/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.settings.ToggleVising;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author vip_delete
 */
public class Model
{
    public static final String STATUS_COLUMN_NAME = "status";

    private DynamicListDataSource<IdentifiableWrapper> _dataSource;
    private List<IdentifiableWrapper> _statusList;

    public DynamicListDataSource<IdentifiableWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<IdentifiableWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public List<IdentifiableWrapper> getStatusList()
    {
        return _statusList;
    }

    public void setStatusList(List<IdentifiableWrapper> statusList)
    {
        _statusList = statusList;
    }
}
