package ru.tandemservice.unimv.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Denis Perminov
 * @since 29.05.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimv_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // DEV-5087
        tool.createColumn("groupsmembervising_t", new DBColumn("printlabeltitle_p", DBType.createVarchar(255)));

        Statement selStmt = tool.getConnection().createStatement();
        selStmt.execute("select INDEX_P, PRINTLABEL_P from GROUPSMEMBERVISING_T");
        ResultSet selRslt = selStmt.getResultSet();
        PreparedStatement updStmt = tool.prepareStatement("update GROUPSMEMBERVISING_T set PRINTLABELTITLE_P=? where INDEX_P=?");

        while (selRslt.next())
        {
            updStmt.setString(1, selRslt.getString(2) + "_TITLED");
            updStmt.setInt(2, selRslt.getInt(1));
            updStmt.addBatch();
        }
        updStmt.executeBatch();

        tool.setColumnNullable("groupsmembervising_t", "printlabeltitle_p", false);
    }
}