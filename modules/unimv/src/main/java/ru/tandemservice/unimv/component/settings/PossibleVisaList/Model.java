/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.component.settings.PossibleVisaList;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unimv.IPossibleVisa;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
public class Model
{
    public static final String POST_BOUNDED_WITH_QGANDQL_TITLE = "postBoundedWithQGandQLTitle";
    public static final String ORG_UNIT_TITLE = "orgUnitTitle";

    private IDataSettings _settings;
    private DynamicListDataSource<IPossibleVisa> _dataSource;
    private List<IdentifiableWrapper> _categoryList;

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<IPossibleVisa> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<IPossibleVisa> dataSource)
    {
        _dataSource = dataSource;
    }

    public List<IdentifiableWrapper> getCategoryList()
    {
        return _categoryList;
    }

    public void setCategoryList(List<IdentifiableWrapper> categoryList)
    {
        _categoryList = categoryList;
    }
}
