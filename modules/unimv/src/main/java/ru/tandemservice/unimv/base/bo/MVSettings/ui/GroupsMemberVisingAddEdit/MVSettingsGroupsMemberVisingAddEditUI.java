/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings.ui.GroupsMemberVisingAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

/**
 * Create by ashaburov
 * Date 21.10.11
 */
@Input({
        @Bind(key = MVSettingsGroupsMemberVisingAddEditUI.GROUPS_MEMBER_VISING_ID, binding = MVSettingsGroupsMemberVisingAddEditUI.GROUPS_MEMBER_VISING_ID)
})
public class MVSettingsGroupsMemberVisingAddEditUI extends UIPresenter
{
    public static final String GROUPS_MEMBER_VISING_ID = "groupsVisingId";

    private Long _groupsVisingId;
    private GroupsMemberVising _groupsVising;

    private boolean _addForm;

    //Listeners

    @Override
    public void onComponentRefresh()
    {
        _addForm = _groupsVisingId == null;

        if (!_addForm)
            _groupsVising = DataAccessServices.dao().getNotNull(_groupsVisingId);
        else
        {
            _groupsVising = new GroupsMemberVising();
            _groupsVising.setDefaultUse(false);
        }
    }

    public void onClickApply()
    {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();

        if (_addForm)
        {
            int count = dao.getCount(GroupsMemberVising.class);

            while (dao.getCatalogItem(GroupsMemberVising.class, Integer.toString(count)) != null)
            {
                count++;
            }

            if (dao.getCatalogItem(GroupsMemberVising.class, String.valueOf(count)) != null)
                throw new ApplicationException("Элемент с таким кодом уже существует.");

            _groupsVising.setCode(String.valueOf(count));
            _groupsVising.setIndex(dao.getCount(GroupsMemberVising.class));
        }

        dao.saveOrUpdate(_groupsVising);
        deactivate();
    }

    //Getters & Setters

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public Long getGroupsVisingId()
    {
        return _groupsVisingId;
    }

    public void setGroupsVisingId(Long groupsVisingId)
    {
        _groupsVisingId = groupsVisingId;
    }

    public GroupsMemberVising getGroupsVising()
    {
        return _groupsVising;
    }

    public void setGroupsVising(GroupsMemberVising groupsVising)
    {
        _groupsVising = groupsVising;
    }
}
