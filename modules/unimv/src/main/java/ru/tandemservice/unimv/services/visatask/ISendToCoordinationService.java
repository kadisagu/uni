/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.services.visatask;

import org.tandemframework.common.base.entity.IPersistentPersonable;

import ru.tandemservice.uni.services.IUniService;
import ru.tandemservice.unimv.IAbstractDocument;

/**
 * Инициализирует процедуру согласования объекта
 *
 * @author vip_delete
 */
public interface ISendToCoordinationService extends IUniService
{
    String SEND_TO_COORDINATION_SERVICE = "SendToCoordinationService";

    /**
     * Инициализация сервиса отправки на согласование.
     * для класса объекта должны быть созданы 3 сервиса интерфейса ITouchVisaTaskHandler
     *
     *
     * @param document документ, который надо отправить на согласование
     * @param initiator сотрудник который инициировал процедуру согласования
     * @see ITouchVisaTaskHandler
     */
    void init(IAbstractDocument document, IPersistentPersonable initiator);
}
