/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.services.visatask;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.services.UniService;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unimv.entity.visa.VisaTask;

import java.util.Date;
import java.util.List;

/**
 * Инициализирует процедуру согласования объекта
 * 1. запустить сервис инициализации для entity (тут обычно только изменяется состояние объекта)
 * 2. взять первую визу
 * 3. если виз нет, то запустить сервис "положительного согласования"
 * 4. если визы есть, то первой визе создать задачу
 *
 * @author vip_delete
 */
public class SendToCoordinationService extends UniService implements ISendToCoordinationService
{
    private IAbstractDocument _document;
    private IPersistentPersonable _initiator;

    public IAbstractDocument getDocument()
    {
        return _document;
    }

    public void setDocument(IAbstractDocument document)
    {
        _document = document;
    }

    public IPersistentPersonable getInitiator()
    {
        return _initiator;
    }

    public void setInitiator(IPersistentPersonable initiator)
    {
        _initiator = initiator;
    }

    @Override
    public void init(IAbstractDocument document, IPersistentPersonable initiator)
    {
        setDocument(document);
        setInitiator(initiator);
    }

    @Override
    protected void doValidate()
    {
        if (_document == null)
            throw new RuntimeException("Cannot initialize coordination procedure: visaOwnerId is null!");
    }

    @Override
    protected void doExecute() throws Exception
    {
        //запускаем service инициализации процедуры согласования
        ITouchVisaTaskHandler serviceToInitialize = UnimvUtil.findTouchService(_document, UnimvDefines.VISA_INIT_SERVICE_MAP);
        serviceToInitialize.init(_document);
        serviceToInitialize.execute();

        Long visingStatus = UnimvDaoFacade.getVisaDao().getVisingStatus(_document);
        if (visingStatus == null || visingStatus != UnimvDefines.VISING_RUN_FULL)
        {
            // визирование отключено, либо нет полного цикла согласования
            accept();
            return;
        }

        //1. получаем первую визу, на которую хоть кто-то назначен
        int i = 0;
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(_document);

        if (i == visaList.size())
        {
            // некому визировать
            accept();
            return;
        }

        Visa visa = visaList.get(i);

        //на одну и ту же визу нельзя создать несколько задач
        if (ISharedBaseDao.instance.get().existsEntity(VisaTask.class, VisaTask.L_VISA, visa))
            throw new ApplicationException("На визу уже назначена задача.");

        //создаем задачу согласования на визу
        VisaTask task = new VisaTask();
        task.setCreationDate(new Date());
        task.setVisa(visa);
        task.setInitiator(_initiator);

        getCoreDao().save(task);
    }

    private void accept()
    {
        ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(_document, UnimvDefines.VISA_ACCEPT_SERVICE_MAP);
        touchService.init(_document);
        touchService.execute();
    }
}
