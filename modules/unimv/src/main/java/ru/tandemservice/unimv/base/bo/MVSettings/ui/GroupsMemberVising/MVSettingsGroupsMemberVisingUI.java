/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings.ui.GroupsMemberVising;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unimv.base.bo.MVSettings.MVSettingsManager;
import ru.tandemservice.unimv.base.bo.MVSettings.ui.GroupsMemberVisingAddEdit.MVSettingsGroupsMemberVisingAddEdit;
import ru.tandemservice.unimv.base.bo.MVSettings.ui.GroupsMemberVisingAddEdit.MVSettingsGroupsMemberVisingAddEditUI;

/**
 * Create by ashaburov
 * Date 21.10.11
 */
public class MVSettingsGroupsMemberVisingUI extends UIPresenter
{
    //Listeners

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(MVSettingsGroupsMemberVisingAddEdit.class).parameter(MVSettingsGroupsMemberVisingAddEditUI.GROUPS_MEMBER_VISING_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        MVSettingsManager.instance().dao().deleteGroupsMemberVising(getListenerParameterAsLong());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegion(MVSettingsGroupsMemberVisingAddEdit.class).parameter(MVSettingsGroupsMemberVisingAddEditUI.GROUPS_MEMBER_VISING_ID, null).activate();
    }

    public void onUpGroup()
    {
        MVSettingsManager.instance().dao().doUpGroup(getListenerParameterAsLong());
    }

    public void onDownGroup()
    {
        MVSettingsManager.instance().dao().doDownGroup(getListenerParameterAsLong());
    }
}
