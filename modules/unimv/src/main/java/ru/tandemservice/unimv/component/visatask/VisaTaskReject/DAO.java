/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unimv.component.visatask.VisaTaskReject;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void update(Model model)
    {
        ITouchVisaTaskService service = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        service.init(model.getVisaTaskId(), model.getComment());
        service.execute();
    }
}
