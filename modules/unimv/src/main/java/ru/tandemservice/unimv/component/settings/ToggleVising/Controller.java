/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.settings.ToggleVising;

import java.util.Map;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimv.UnimvDefines;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<IdentifiableWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название документа", "title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.STATUS_COLUMN_NAME, ""));

        model.setDataSource(dataSource);
    }

    @SuppressWarnings({"unchecked"})
    public synchronized void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        IDataSettings settings = DataSettingsFacade.getSettings(UnimvDefines.VISA_MODULE_OWNER, UnimvDefines.IS_NEED_VISING_DOCUMENT);

        Map<Long, IdentifiableWrapper> valueMap = ((BlockColumn) model.getDataSource().getColumn(Model.STATUS_COLUMN_NAME)).getValueMap();
        for (Map.Entry<Long, IdentifiableWrapper> entry : valueMap.entrySet())
        {
            IEntityMeta meta = EntityRuntime.getMeta(entry.getKey().shortValue());
            long value = entry.getValue().getId();
            if (value == 0L)
                settings.remove(meta.getName());
            else
                settings.set(meta.getName(), value);
        }

        DataSettingsFacade.saveSettings(settings);
    }
}
