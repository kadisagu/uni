package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.base.entity.gen.IPersistentPersonableGen;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Задача согласования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VisaTaskGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimv.entity.visa.VisaTask";
    public static final String ENTITY_NAME = "visaTask";
    public static final int VERSION_HASH = 1318591366;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATION_DATE = "creationDate";
    public static final String L_VISA = "visa";
    public static final String L_INITIATOR = "initiator";
    public static final String P_DOCUMENT = "document";

    private Date _creationDate;     // Дата создания
    private Visa _visa;     // Виза
    private IPersistentPersonable _initiator;     // Инициатор процедуры согласования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Виза.
     */
    public Visa getVisa()
    {
        return _visa;
    }

    /**
     * @param visa Виза.
     */
    public void setVisa(Visa visa)
    {
        dirty(_visa, visa);
        _visa = visa;
    }

    /**
     * @return Инициатор процедуры согласования.
     */
    public IPersistentPersonable getInitiator()
    {
        return _initiator;
    }

    /**
     * @param initiator Инициатор процедуры согласования.
     */
    public void setInitiator(IPersistentPersonable initiator)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && initiator!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPersistentPersonable.class);
            IEntityMeta actual =  initiator instanceof IEntity ? EntityRuntime.getMeta((IEntity) initiator) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_initiator, initiator);
        _initiator = initiator;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VisaTaskGen)
        {
            setCreationDate(((VisaTask)another).getCreationDate());
            setVisa(((VisaTask)another).getVisa());
            setInitiator(((VisaTask)another).getInitiator());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VisaTaskGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VisaTask.class;
        }

        public T newInstance()
        {
            return (T) new VisaTask();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "creationDate":
                    return obj.getCreationDate();
                case "visa":
                    return obj.getVisa();
                case "initiator":
                    return obj.getInitiator();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "visa":
                    obj.setVisa((Visa) value);
                    return;
                case "initiator":
                    obj.setInitiator((IPersistentPersonable) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "creationDate":
                        return true;
                case "visa":
                        return true;
                case "initiator":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "creationDate":
                    return true;
                case "visa":
                    return true;
                case "initiator":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "creationDate":
                    return Date.class;
                case "visa":
                    return Visa.class;
                case "initiator":
                    return IPersistentPersonable.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VisaTask> _dslPath = new Path<VisaTask>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VisaTask");
    }
            

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Виза.
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getVisa()
     */
    public static Visa.Path<Visa> visa()
    {
        return _dslPath.visa();
    }

    /**
     * @return Инициатор процедуры согласования.
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getInitiator()
     */
    public static IPersistentPersonableGen.Path<IPersistentPersonable> initiator()
    {
        return _dslPath.initiator();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getDocument()
     */
    public static SupportedPropertyPath<IEntity> document()
    {
        return _dslPath.document();
    }

    public static class Path<E extends VisaTask> extends EntityPath<E>
    {
        private PropertyPath<Date> _creationDate;
        private Visa.Path<Visa> _visa;
        private IPersistentPersonableGen.Path<IPersistentPersonable> _initiator;
        private SupportedPropertyPath<IEntity> _document;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(VisaTaskGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Виза.
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getVisa()
     */
        public Visa.Path<Visa> visa()
        {
            if(_visa == null )
                _visa = new Visa.Path<Visa>(L_VISA, this);
            return _visa;
        }

    /**
     * @return Инициатор процедуры согласования.
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getInitiator()
     */
        public IPersistentPersonableGen.Path<IPersistentPersonable> initiator()
        {
            if(_initiator == null )
                _initiator = new IPersistentPersonableGen.Path<IPersistentPersonable>(L_INITIATOR, this);
            return _initiator;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimv.entity.visa.VisaTask#getDocument()
     */
        public SupportedPropertyPath<IEntity> document()
        {
            if(_document == null )
                _document = new SupportedPropertyPath<IEntity>(VisaTaskGen.P_DOCUMENT, this);
            return _document;
        }

        public Class getEntityClass()
        {
            return VisaTask.class;
        }

        public String getEntityName()
        {
            return "visaTask";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract IEntity getDocument();
}
