/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.ui;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 07.10.2010
 */
public class PossibleVisaListModel extends FullCheckSelectModel
{
    private Session _session;

    public PossibleVisaListModel(Session session)
    {
        _session = session;
    }

    private final String[] columns = new String[]{
            "ФИО",
            "Название должности (звание) в документе",
            "Категория визы",
            "Подразделение"
    };

    @Override
    public int getColumnCount()
    {
        return columns.length;
    }

    @Override
    public String[] getColumnTitles()
    {
        return columns;
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        IPossibleVisa possibleVisa = (IPossibleVisa) value;

        switch (columnIndex)
        {
            case 0:
                return possibleVisa.getEntity().getPerson().getIdentityCard().getFullFio();
            case 1:
                return possibleVisa.getTitle();
            case 2:
                return possibleVisa.getPersonOwnerTitle();
            case 3:
                // не у всех возможных виз есть подразделение, и тут делается исключение для визи сотрудника
                if (possibleVisa instanceof EmployeePostPossibleVisa)
                    return ((EmployeePostPossibleVisa) possibleVisa).getEntity().getOrgUnit().getExtendedShortTitle2();
                else if (possibleVisa instanceof StudentPossibleVisa)
                    return ((StudentPossibleVisa) possibleVisa).getEntity().getEducationOrgUnit().getFormativeOrgUnit().getExtendedShortTitle2() + ", " + ((StudentPossibleVisa) possibleVisa).getEntity().getCourse().getTitle() + " курс";
                else
                    return "";
            default:
                return "";
        }
    }

    @Override
    public ListResult findValues(String filter)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(IPossibleVisa.class, "p");
        builder.column("p");

        // фильтруем по фио
        if (StringUtils.isNotEmpty(filter))
            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("p", IPossibleVisaGen.entity().person().identityCard().fullFio().s())), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));

        // только активные роли
        builder.where(DQLExpressions.eq(DQLExpressions.property("p", IPossibleVisaGen.P_ACTIVE), DQLExpressions.value(Boolean.TRUE)));

        // сортируем по фио
        builder.order(DQLExpressions.property("p", IPossibleVisaGen.entity().person().identityCard().fullFio().s()), OrderDirection.asc);

        List<IPossibleVisa> list = builder.createStatement(new DQLExecutionContext(_session)).list();

        return new ListResult<IPossibleVisa>(list);
    }
}
