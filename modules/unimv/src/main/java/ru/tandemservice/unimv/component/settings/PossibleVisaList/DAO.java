/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.component.settings.PossibleVisaList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        String title = (String) model.getSettings().get("title");
        String fio = (String) model.getSettings().get("fio");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(IPossibleVisa.class, "p");
        builder.column(DQLExpressions.property("p"));

        if (StringUtils.isNotEmpty(fio))
            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("p", IPossibleVisaGen.entity().person().identityCard().fullFio().s())), DQLExpressions.value(CoreStringUtils.escapeLike(fio).replace(" ", "%"))));

        if (StringUtils.isNotEmpty(title))
            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("p", IPossibleVisaGen.title().s())), DQLExpressions.value(CoreStringUtils.escapeLike(title).replace(" ", "%"))));

        builder.order(DQLExpressions.property("p", model.getDataSource().getEntityOrder().getKeyString()), OrderDirection.valueOf(model.getDataSource().getEntityOrder().getDirection().name()));

        List<IPossibleVisa> list = builder.createStatement(new DQLExecutionContext(getSession())).list();

        UniBaseUtils.createPage(model.getDataSource(), list);

        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            IEntity possibleVisa = viewWrapper.getEntity();

            if (possibleVisa instanceof EmployeePostPossibleVisa)
                viewWrapper.setViewProperty(Model.ORG_UNIT_TITLE, ((EmployeePostPossibleVisa) possibleVisa).getEntity().getOrgUnit().getExtendedShortTitle2());
            else if (possibleVisa instanceof StudentPossibleVisa)
                viewWrapper.setViewProperty(Model.ORG_UNIT_TITLE, ((StudentPossibleVisa) possibleVisa).getEntity().getEducationOrgUnit().getFormativeOrgUnit().getExtendedShortTitle2() + ", " + ((StudentPossibleVisa) possibleVisa).getEntity().getCourse().getTitle() + " курс");
            else
                throw new RuntimeException("Unknown possibleVisa class '" + possibleVisa + "'");
        }
    }

    @Override
    public void doDeletePossibleVisa(Long possibleVisaId)
    {
        UnimvDaoFacade.getVisaDao().doDeletePossibleVisa((IPossibleVisa) getNotNull(possibleVisaId));
    }
}
