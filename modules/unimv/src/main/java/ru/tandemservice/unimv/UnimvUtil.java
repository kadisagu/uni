/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv;

import java.util.Map;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;

import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;

/**
 * @author vip_delete
 */
public final class UnimvUtil
{
    private UnimvUtil()
    {
    }

    @SuppressWarnings("unchecked")
    public static ITouchVisaTaskHandler findTouchService(IAbstractDocument document, String touchServiceGroupName)
    {
        Map<String, ITouchVisaTaskHandler> map = (Map<String, ITouchVisaTaskHandler>) ApplicationRuntime.getBean(touchServiceGroupName);
        String entityName = EntityRuntime.getMeta(document).getName();
        ITouchVisaTaskHandler handler = map.get(entityName);
        if (handler == null)
            throw new RuntimeException("No service in group '" + touchServiceGroupName + "' found for entity '" + entityName + "'.");
        return handler;
    }
}
