/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unimv.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.unimv.IUnimvComponents;

/**
 * @author Vasily Zhukov
 * @since 29.03.2012
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unimv" + EmployeePostViewExtUI.class.getSimpleName();

    @Autowired
    public EmployeePostView _employeePostView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        // <html-tab name="visaTaskTab" html-template-name="ru.tandemservice.unimv.component.visatask.VisaTaskList.VisaTaskList" label="Задачи" permission-key="fast:modelMap[{extension}].secModel.viewTabVisaTask" visible="fast:modelMap[{extension}].hasVisaTasks"/>
        return tabPanelExtensionBuilder(_employeePostView.employeePostPubTabPanelExtPoint())
                .addTab(componentTab("visaTaskTab", IUnimvComponents.VISA_TASK_TAB).permissionKey("ui:secModel.viewTabVisaTask").visible("addon:" + ADDON_NAME + ".hasVisaTasks").parameters("mvel:['" + ISecureRoleContext.SECURE_ROLE_CONTEXT + "':presenter.secureRoleContext]"))
                .create();
    }
}
