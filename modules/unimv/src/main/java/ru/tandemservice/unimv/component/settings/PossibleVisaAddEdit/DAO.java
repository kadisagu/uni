/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.component.settings.PossibleVisaAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;

import java.util.Arrays;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setCategoryList(Arrays.asList(
                new IdentifiableWrapper(Model.EMPLOYEE_POST_ID, EntityRuntime.getMeta(EmployeePost.class).getTitle()),
                new IdentifiableWrapper(Model.STUDENT_ID, EntityRuntime.getMeta(Student.class).getTitle())
        ));

        model.setAddForm(model.getPossibleVisaId() == null);
        model.setEditForm(!model.isAddForm());

        if (!model.isAddForm())
        {
            IPossibleVisa possibleVisa = get(model.getPossibleVisaId());

            model.setTitle(possibleVisa.getTitle());

            if (possibleVisa instanceof EmployeePostPossibleVisa)
            {
                model.setCategory(model.getCategoryList().get(Model.EMPLOYEE_POST_ID.intValue()));
                model.setEmployeePost(((EmployeePostPossibleVisa) possibleVisa).getEntity());
            } else if (possibleVisa instanceof StudentPossibleVisa)
            {
                model.setCategory(model.getCategoryList().get(Model.STUDENT_ID.intValue()));
                model.setStudent((Student) possibleVisa.getEntity());
            } else
                throw new RuntimeException("Unknown possibleVisa class '" + possibleVisa + "'");
        }

        model.setEmployeePostListModel(new BaseSingleSelectModel(EmployeePost.P_FULL_TITLE)
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if (primaryKey == null) return null;
                EmployeePost employeePost = get(EmployeePost.class, (Long) primaryKey);
                return employeePost == null ? null : employeePost;
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (!model.isCategoryEmployeePost()) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e");
                builder.column("e");

                // фильтр по фио + должность + подр. + тип подр.
                if (StringUtils.isNotEmpty(filter))
                {
                    IDQLExpression expression = DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()),
                            DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()),
                                    DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.orgUnit().shortTitle().s()),
                                            DQLExpressions.property("e", EmployeePost.orgUnit().orgUnitType().title().s())
                                    )));

                    builder.where(DQLExpressions.like(DQLFunctions.upper(expression), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
                }

                // только активные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.postStatus().active().s()), DQLExpressions.value(Boolean.TRUE)));

                // не архивные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival().s()), DQLExpressions.value(Boolean.FALSE)));

                // сортируем по фио
                builder.order(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()));

                // показываем не больше 50
                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
                statement.setMaxResults(50);

                return new ListResult<Student>(statement.<Student>list(), count == null ? 0 : count.intValue());
            }
        });

        model.setStudentListModel(new BaseSingleSelectModel()
        {
            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                Student student = (Student) value;
                return student.getPerson().getIdentityCard().getFullFio() + " (" + (student.getGroup() == null ? "" : "гр. " + student.getGroup().getTitle() + ", ") + student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle() + ")";
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                if (primaryKey == null) return null;
                Student student = get(Student.class, (Long) primaryKey);
                return student == null ? null : student;
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (!model.isCategoryStudent()) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s");
                builder.column("s");
                builder.joinPath(DQLJoinType.left, "s." + Student.L_GROUP, "g");

                // фильтр по фио + группа
                if (StringUtils.isNotEmpty(filter))
                {
                    IDQLExpression expression = DQLFunctions.concat(DQLExpressions.property("s", Student.person().identityCard().fullFio().s()),
                            // когда соединяется null, то не работает, надо использовать case when then
                            DQLExpressions.caseExpr(
                                    new IDQLExpression[]{DQLExpressions.isNull(DQLExpressions.property("s", Student.group().s()))}, new IDQLExpression[]{DQLExpressions.value("")},
                                    DQLExpressions.property("s", Student.group().title().s())
                            )
                    );

                    builder.where(DQLExpressions.like(DQLFunctions.upper(expression), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
                }

                // только активные студенты
                builder.where(DQLExpressions.eq(DQLExpressions.property("s", Student.status().active().s()), DQLExpressions.value(Boolean.TRUE)));

                // не архивные студенты
                builder.where(DQLExpressions.eq(DQLExpressions.property("s", Student.archival().s()), DQLExpressions.value(Boolean.FALSE)));

                // сортируем по фио
                builder.order(DQLExpressions.property("s", Student.person().identityCard().fullFio().s()));

                // показываем не больше 50
                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
                statement.setMaxResults(50);

                return new ListResult<Student>(statement.<Student>list(), count == null ? 0 : count.intValue());
            }
        });
    }

    @Override
    public void update(Model model)
    {
        if (model.getCategory() == null)
            throw new RuntimeException("PossibleVisa category cannot be null!");

        if (model.isAddForm())
        {
            // форма создания
            if (model.isCategoryEmployeePost())
                saveEmployeePostPossibleVisa(model);
            else if (model.isCategoryStudent())
                saveStudentPossibleVisa(model);
            else
                throw new RuntimeException("Unknown possibleVisa category: " + model.getCategory().getTitle());
        } else
        {
            // форма редактирования
            IPossibleVisa possibleVisa = get(model.getPossibleVisaId());

            // можно редактировать только название должности (звание) в документе
            possibleVisa.setProperty(IPossibleVisaGen.title().s(), model.getTitle());

            update(possibleVisa);
        }
    }

    private void saveEmployeePostPossibleVisa(Model model)
    {
        if (!model.isCategoryEmployeePost()) throw new RuntimeException("Can't save employeePostPossibleVisa");

        MQBuilder builder = new MQBuilder(EmployeePostPossibleVisa.ENTITY_NAME, "p");
        builder.add(MQExpression.eq("p", EmployeePostPossibleVisa.P_TITLE, model.getTitle()));
        builder.add(MQExpression.eq("p", EmployeePostPossibleVisa.L_ENTITY, model.getEmployeePost()));
        EmployeePostPossibleVisa employeePostPossibleVisa = (EmployeePostPossibleVisa) builder.uniqueResult(getSession());

        if (employeePostPossibleVisa != null)
            throw new ApplicationException("Возможная виза на сотрудника с такими параметрами уже существует.");

        EmployeePostPossibleVisa possibleVisa = new EmployeePostPossibleVisa();
        possibleVisa.setTitle(model.getTitle());
        possibleVisa.setEntity(model.getEmployeePost());
        save(possibleVisa);
    }

    private void saveStudentPossibleVisa(Model model)
    {
        if (!model.isCategoryStudent()) throw new RuntimeException("Can't save studentPossibleVisa");

        MQBuilder builder = new MQBuilder(StudentPossibleVisa.ENTITY_NAME, "p");
        builder.add(MQExpression.eq("p", StudentPossibleVisa.P_TITLE, model.getTitle()));
        builder.add(MQExpression.eq("p", StudentPossibleVisa.L_ENTITY, model.getStudent()));
        StudentPossibleVisa studentPossibleVisa = (StudentPossibleVisa) builder.uniqueResult(getSession());

        if (studentPossibleVisa != null)
            throw new ApplicationException("Возможная виза на студента с такими параметрами уже существует.");

        StudentPossibleVisa possibleVisa = new StudentPossibleVisa();
        possibleVisa.setTitle(model.getTitle());
        possibleVisa.setEntity(model.getStudent());

        save(possibleVisa);
    }
}
