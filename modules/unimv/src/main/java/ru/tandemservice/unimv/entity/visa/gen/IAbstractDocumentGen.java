package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unimv.IAbstractDocument;

/**
 * Абстрактный документ
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IAbstractDocumentGen extends InterfaceStubBase
 implements IAbstractDocument{
    public static final int VERSION_HASH = 2135439791;

    public static final String P_CREATE_DATE = "createDate";

    private Date _createDate;


    public Date getCreateDate()
    {
        return _createDate;
    }

    public void setCreateDate(Date createDate)
    {
        _createDate = createDate;
    }

    private static final Path<IAbstractDocument> _dslPath = new Path<IAbstractDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unimv.IAbstractDocument");
    }
            

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.unimv.IAbstractDocument#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    public static class Path<E extends IAbstractDocument> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.unimv.IAbstractDocument#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(IAbstractDocumentGen.P_CREATE_DATE, this);
            return _createDate;
        }

        public Class getEntityClass()
        {
            return IAbstractDocument.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unimv.IAbstractDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
