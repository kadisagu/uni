/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unimv.component.visa.VisaList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.IUnimvComponents;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.entity.visa.Visa;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));

        prepareVisaDataSource(context);
    }

    private void prepareVisaDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getVisaDataSource() != null) return;

        DynamicListDataSource<Visa> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("№", "number").setOrderable(false).setClickable(false));


        IMergeRowIdResolver mergeRowIdResolver = entity -> ((Visa) ((ViewWrapper) entity).getEntity()).getGroupMemberVising().getId().toString();
        dataSource.addColumn(new SimpleColumn("Группа участников визирования", Visa.groupMemberVising().title()).setOrderable(false).setClickable(false).setMergeRowIdResolver(mergeRowIdResolver));
        // небольшой анахронизм связанный с автодобавлением виз: визы, добавленные автоматически, нельзя удалять из документа.
        // сейчас нет автоматически добавляемых виз (никто не знает как их делать универсально)
        if (Boolean.parseBoolean(ApplicationRuntime.getProperty(UnimvDefines.HAS_AUTO_VISA_LIST_GENERATION)))
            dataSource.addColumn(new SimpleColumn("Обязательность", Visa.P_MANDATORY, YesNoFormatter.INSTANCE).setOrderable(false).setClickable(false));

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("ФИО", Visa.possibleVisa().entity().person().identityCard().fullFio());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return entity.getProperty(Visa.possibleVisa().entity().id().s());
            }
        });
        dataSource.addColumn(linkColumn.setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Название должности (звание) в документе", Visa.possibleVisa().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Категория визы", Visa.L_POSSIBLE_VISA + "." + IPossibleVisa.PERSON_OWNER_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", Model.ORG_UNIT_TITLE).setOrderable(false).setClickable(false));

        dataSource.addColumn(new BooleanColumn("Прогресс согласования", Visa.CURRENT_COORDINATOR));

        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickVisaUp").setPermissionKey(model.getSecModel().getPermission("editVisaIndex")));
            dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickVisaDown").setPermissionKey(model.getSecModel().getPermission("editVisaIndex")));
            dataSource.addColumn(new ActionColumn("Удалить визу", ActionColumn.DELETE, "onClickDeleteVisa", "Удалить визу?").setPermissionKey(model.getSecModel().getPermission("delVisa")));
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditGroupVisa").setPermissionKey(model.getSecModel().getPermission("editGroupVisa")).setMergeRowIdResolver(mergeRowIdResolver));
            dataSource.addColumn(new ActionColumn("Удалить группу", ActionColumn.DELETE, "onClickDeleteGroupVisa", "Удалить группу?").setPermissionKey(model.getSecModel().getPermission("delGroupVisa")).setMergeRowIdResolver(mergeRowIdResolver));
        }

        model.setVisaDataSource(dataSource);
    }

    public void onClickVisaUp(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            getDao().doVisaUp(model, (Long) component.getListenerParameter());
            model.getVisaDataSource().refresh();
        }
    }

    public void onClickVisaDown(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            getDao().doVisaDown(model, (Long) component.getListenerParameter());
            model.getVisaDataSource().refresh();
        }
    }

    public void onClickDeleteVisa(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            getDao().doDeleteVisa(model, (Long) component.getListenerParameter());
            model.getVisaDataSource().refresh();
        }
    }

    public void onClickAddVisa(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            component.createDefaultChildRegion(new ComponentActivator(IUnimvComponents.VISA_ADD, new ParametersMap()
                    .add("documentId", getModel(component).getVisaOwnerModel().getDocument().getId())
                    .add("groupVisaId", null)
            ));
        }
    }

    public void onClickEditGroupVisa(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            component.createDefaultChildRegion(new ComponentActivator(IUnimvComponents.VISA_ADD, new ParametersMap()
                    .add("documentId", getModel(component).getVisaOwnerModel().getDocument().getId())
                    .add("groupVisaId", getDao().get(Visa.class, component.<Long>getListenerParameter()).getGroupMemberVising().getId())
            ));
        }
    }

    public void onClickDeleteGroupVisa(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getVisaOwnerModel().isVisaListModificable())
        {
            getDao().doDeleteGroupVisa(model, (Long) component.getListenerParameter());
            model.getVisaDataSource().refresh();
        }
    }
}
