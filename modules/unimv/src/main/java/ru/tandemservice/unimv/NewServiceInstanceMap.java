/* $Id:$ */
package ru.tandemservice.unimv;

import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.services.IUniService;

import java.util.HashMap;

/**
 * Мапа, которая хранит инстансы или классы IUniService и при get()
 * всегда создает новый инстанс сервиса, а не отдает тот, который хранит.
 * Ёто, по сути, костыль. Васины сервиси спроектированы криво и немногопоточно,
 * из-за чего возникают проблемы.
 * ----
 * [For using only in spring.xml]
 *
 * @author Nikolay Fedorovskih
 * @since 25.07.2015
 */
@Zlo
@DoNotUseMe
public class NewServiceInstanceMap extends HashMap {
    @Override
    public Object get(Object key) {
        final Object value = super.get(key);
        if (value == null) {
            return null;
        }
        Class clazz;
        if (value instanceof Class) {
            clazz = (Class) value;
        } else {
            clazz = value.getClass();
        }
        if (!IUniService.class.isAssignableFrom(clazz)) {
            throw new IllegalStateException();
        }
        try {
            return value.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}