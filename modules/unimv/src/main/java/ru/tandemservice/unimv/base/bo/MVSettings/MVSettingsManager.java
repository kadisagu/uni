/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimv.base.bo.MVSettings.logic.IMVSettingsDAO;
import ru.tandemservice.unimv.base.bo.MVSettings.logic.MVSettingsDAO;

/**
 * Create by ashaburov
 * Date 21.10.11
 */
@Configuration
public class MVSettingsManager extends BusinessObjectManager
{
    public static MVSettingsManager instance()
    {
        return instance(MVSettingsManager.class);
    }

    @Bean
    public IMVSettingsDAO dao()
    {
        return new MVSettingsDAO();
    }
}
