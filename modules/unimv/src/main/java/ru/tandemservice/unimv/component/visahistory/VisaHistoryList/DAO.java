/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visahistory.VisaHistoryList;

import java.util.List;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        // права данной вкладки имеют вид <permissionKey>_<secPostfix>
        // где permissionKey - префикс права
        // entityName - имя entity для visaOwnerId
        // регистрация этих прав должна происходить в модуле, который используется этот бизнес компонент
        // ПРАВА : addVisa_secPostfix, editVisaIndex_secPostfix, delVisa_secPostfix
        // + не забудьте добавить права на просмотр вкладок "Визы" и "История согласования"
        model.setSecModel(new CommonPostfixPermissionModel(model.getVisaOwnerModel().getSecPostfix()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        List<VisaHistoryItem> historyItemList = UnimvDaoFacade.getVisaDao().getVisaHistoryList(model.getVisaOwnerModel().getDocument());

        UniBaseUtils.createPage(model.getVisaHistoryDataSource(), historyItemList);
    }
}