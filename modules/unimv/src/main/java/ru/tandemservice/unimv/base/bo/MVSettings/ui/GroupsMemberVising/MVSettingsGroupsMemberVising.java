/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings.ui.GroupsMemberVising;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unimv.base.bo.MVSettings.logic.MVGroupsMemberVisingDSHandler;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

/**
 * Create by ashaburov
 * Date 21.10.11
 */
@Configuration
public class MVSettingsGroupsMemberVising extends BusinessComponentManager
{
    public static final String MV_GROUPS_MEMBER_VISING_SETTINGS_DS = "mvGroupsMemberVisingSettingsDS";

    @Bean
    public ColumnListExtPoint mvGroupsMemberVisingSettingsDS()
    {
        return columnListExtPointBuilder(MV_GROUPS_MEMBER_VISING_SETTINGS_DS)
                .addColumn(textColumn("title", GroupsMemberVising.title()))
                .addColumn(textColumn("printTitle", GroupsMemberVising.printTitle()))
                .addColumn(booleanColumn("default", GroupsMemberVising.defaultUse()))
                .addColumn(textColumn("printLabel", GroupsMemberVising.printLabel()))
                // DEV-5087
                .addColumn(textColumn("printLabelTitle", GroupsMemberVising.printLabelTitle()))
                .addColumn(actionColumn("upGroup", CommonDefines.ICON_UP, "onUpGroup"))
                .addColumn(actionColumn("downGroup", CommonDefines.ICON_DOWN, "onDownGroup"))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("Удалить группу {0}?", GroupsMemberVising.title())).disabled("disabled_view"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(MV_GROUPS_MEMBER_VISING_SETTINGS_DS, mvGroupsMemberVisingSettingsDS(), mvGroupsMemberVisingSettingsDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> mvGroupsMemberVisingSettingsDSHandler()
    {
        return new MVGroupsMemberVisingDSHandler(getName());
    }
}
