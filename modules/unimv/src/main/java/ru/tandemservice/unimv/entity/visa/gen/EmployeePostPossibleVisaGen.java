package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Возможная виза сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeePostPossibleVisaGen extends EntityBase
 implements IPossibleVisa{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa";
    public static final String ENTITY_NAME = "employeePostPossibleVisa";
    public static final int VERSION_HASH = 932863793;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_ENTITY = "entity";
    public static final String P_ACTIVE = "active";

    private String _title;     // Название должности (звание) в документе
    private EmployeePost _entity;     // Сотрудник
    private boolean _active; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название должности (звание) в документе. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Сотрудник. Свойство не может быть null.
     */
    public void setEntity(EmployeePost entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entity.postStatus.active".
     */
    // @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active  Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeePostPossibleVisaGen)
        {
            setTitle(((EmployeePostPossibleVisa)another).getTitle());
            setEntity(((EmployeePostPossibleVisa)another).getEntity());
            setActive(((EmployeePostPossibleVisa)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeePostPossibleVisaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeePostPossibleVisa.class;
        }

        public T newInstance()
        {
            return (T) new EmployeePostPossibleVisa();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "entity":
                    return obj.getEntity();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "entity":
                    obj.setEntity((EmployeePost) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "entity":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "entity":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "entity":
                    return EmployeePost.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeePostPossibleVisa> _dslPath = new Path<EmployeePostPossibleVisa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeePostPossibleVisa");
    }
            

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa#getEntity()
     */
    public static EmployeePost.Path<EmployeePost> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entity.postStatus.active".
     * @see ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends EmployeePostPossibleVisa> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private EmployeePost.Path<EmployeePost> _entity;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeePostPossibleVisaGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa#getEntity()
     */
        public EmployeePost.Path<EmployeePost> entity()
        {
            if(_entity == null )
                _entity = new EmployeePost.Path<EmployeePost>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entity.postStatus.active".
     * @see ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(EmployeePostPossibleVisaGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return EmployeePostPossibleVisa.class;
        }

        public String getEntityName()
        {
            return "employeePostPossibleVisa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
