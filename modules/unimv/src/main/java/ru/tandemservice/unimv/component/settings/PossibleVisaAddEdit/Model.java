/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.component.settings.PossibleVisaAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
@Input(keys = "possibleVisaId", bindings = "possibleVisaId")
public class Model
{
    public static final Long EMPLOYEE_POST_ID = 0L;
    public static final Long STUDENT_ID = 1L;

    private Long _possibleVisaId;
    private String _title;
    private boolean _addForm;
    private boolean _editForm;

    private List<IdentifiableWrapper> _categoryList;
    private ISelectModel _employeePostListModel;
    private ISelectModel _studentListModel;

    private IdentifiableWrapper _category;
    private EmployeePost _employeePost;
    private Student _student;

    public boolean isCategoryEmployeePost()
    {
        return _category != null && _category.getId().equals(EMPLOYEE_POST_ID);
    }

    public boolean isCategoryStudent()
    {
        return _category != null && _category.getId().equals(STUDENT_ID);
    }

    // Getters & Setters

    public Long getPossibleVisaId()
    {
        return _possibleVisaId;
    }

    public void setPossibleVisaId(Long possibleVisaId)
    {
        _possibleVisaId = possibleVisaId;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public List<IdentifiableWrapper> getCategoryList()
    {
        return _categoryList;
    }

    public void setCategoryList(List<IdentifiableWrapper> categoryList)
    {
        _categoryList = categoryList;
    }

    public ISelectModel getEmployeePostListModel()
    {
        return _employeePostListModel;
    }

    public void setEmployeePostListModel(ISelectModel employeePostListModel)
    {
        _employeePostListModel = employeePostListModel;
    }

    public ISelectModel getStudentListModel()
    {
        return _studentListModel;
    }

    public void setStudentListModel(ISelectModel studentListModel)
    {
        _studentListModel = studentListModel;
    }

    public IdentifiableWrapper getCategory()
    {
        return _category;
    }

    public void setCategory(IdentifiableWrapper category)
    {
        _category = category;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}
