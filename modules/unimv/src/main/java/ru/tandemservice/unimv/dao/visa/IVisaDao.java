/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.dao.visa;

import java.util.List;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * Dao to work with Visa
 *
 * @author vip_delete
 */
public interface IVisaDao
{
    final String VISA_DAO_BEAN_NAME = "visaDao";
    final SpringBeanCache<IVisaDao> instance = new SpringBeanCache<IVisaDao>(VISA_DAO_BEAN_NAME);
    
    /**
     * Получает визу по объекту в котором она находится и возможной визе
     * В объекте не может быть двух визы на одну и туже возможную визу (архитектурно)
     *
     * @param document    приказ/документ/выписка, в котором ищется виза
     * @param possibleVisa возможная виза по которой ищется виза
     * @return виза
     */
    Visa getVisa(IAbstractDocument document, IPossibleVisa possibleVisa);

    /**
     * Получает список виз по порядку для данного объекта
     *
     * @param document приказ/документ/выписка, в котором ищется виза
     * @return список виз отсортированных по индексу по возрастанию
     */
    List<Visa> getVisaList(IAbstractDocument document);

    /**
     * Получает текущую задачу согласования для объекта, который проходит процедуру согласования
     * Для объекта не может быть двух текущих заданий (архитектурно)
     *
     * @param document приказ/документ/выписка, в котором ищется виза
     * @return задание согласование, либо null если для объекта нет заданий
     */
    VisaTask getCurrentVisaTask(IAbstractDocument document);

    /**
     * Получает список истории согласования
     *
     * @param document приказ/документ/выписка, в котором ищется виза
     * @return список историй согласований в порядке создания
     */
    List<VisaHistoryItem> getVisaHistoryList(IAbstractDocument document);

    /**
     * Получает статус визирования. см в UnimvDefines статусы визирования
     *
     * @param document приказ/документ/выписка, в котором ищется виза
     * @return null - визирование отключено
     *         1 - визирование включено, но задания не создаются (т.е. сразу согласуется)
     *         2 - визирование включено и задания создаются (полный цикл визирования)
     */
    Long getVisingStatus(IAbstractDocument document);

    /**
     * Удаляем возможную визу.
     * Проверяет, что нет ни одного объекта ссылающегося на этот объект
     *
     * @param possibleVisa возможная виза которую надо удалить
     */
    void doDeletePossibleVisa(IPossibleVisa possibleVisa);
}
