/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visa.VisaList;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;

/**
 * @author vip_delete
 */
@State(keys = {"visaOwnerModel"}, bindings = {"visaOwnerModel"})
public class Model
{
    public static final String ORG_UNIT_TITLE = "orgUnitTitle";

    private IVisaOwnerModel _visaOwnerModel;
    private DynamicListDataSource _visaDataSource;
    private CommonPostfixPermissionModel _secModel;

    public IVisaOwnerModel getVisaOwnerModel()
    {
        return _visaOwnerModel;
    }

    public void setVisaOwnerModel(IVisaOwnerModel visaOwnerModel)
    {
        _visaOwnerModel = visaOwnerModel;
    }

    public DynamicListDataSource getVisaDataSource()
    {
        return _visaDataSource;
    }

    public void setVisaDataSource(DynamicListDataSource visaDataSource)
    {
        _visaDataSource = visaDataSource;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
