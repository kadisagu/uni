/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.entity.IEntity;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
public interface IPossibleVisa extends IEntity
{
    String PERSON_OWNER_TITLE = "personOwnerTitle";
    String POSSIBLE_VISA_FULL_TITLE = "possibleVisaFullTitle";

    String getTitle();    // название в печатном документе

    String getPersonOwnerTitle(); // название роли персоны

    IPersistentPersonable getEntity();  // объект, к которому будут привязываться задания для согласования

    boolean isActive();   // true, если у entity активное состояние

    String getPossibleVisaFullTitle();
}
