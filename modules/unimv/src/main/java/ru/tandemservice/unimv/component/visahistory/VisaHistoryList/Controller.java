/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visahistory.VisaHistoryList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        prepareVisaDataSource(context);
    }

    private void prepareVisaDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getVisaHistoryDataSource() != null) return;

        DynamicListDataSource<VisaHistoryItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Дата", VisaHistoryItem.creationDate(), DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Автор", VisaHistoryItem.author().person().identityCard().fullFio()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Комментарий", VisaHistoryItem.comment(), NewLineFormatter.SIMPLE).setOrderable(false).setClickable(false));

        model.setVisaHistoryDataSource(dataSource);
    }
}
