/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visatask.VisaTaskList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimv.IUnimvComponents;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * Бизнесс компонент для работы со списком заданий для сотрудника
 *
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        prepareVisaTaskListIncomingDataSource(component);
        prepareVisaTaskListIncomingCompletedDataSource(component);
        prepareVisaTaskListOutgoingDataSource(component);
        prepareVisaTaskListOutgoingCompletedDataSource(component);
    }

    private void prepareVisaTaskListIncomingDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getIncomingDataSource() != null) return;

        DynamicListDataSource<VisaTask> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareVisaTaskListIncoming(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Дата создания", VisaTask.P_CREATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Документ", new String[]{"title"}, VisaTask.visa().document()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Инициатор", (VisaTask.initiator() == null) ? "" : SimpleFormatter.INSTANCE.format(VisaTask.initiator().person().identityCard().fullFio())).setOrderable(false).setClickable(false));

        dataSource.addColumn(new ActionColumn("Согласовать", "accept", "onClickAcceptVisaTask").setPermissionKey(model.getSecModel().getPermission("passTask")));
        dataSource.addColumn(new ActionColumn("Отклонить", "reject", "onClickRejectVisaTask").setPermissionKey(model.getSecModel().getPermission("passTask")));

        model.setIncomingDataSource(dataSource);
    }

    private void prepareVisaTaskListIncomingCompletedDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getIncomingCompletedDataSource() != null) return;

        DynamicListDataSource<VisaTask> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareVisaTaskListIncomingCompleted(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Дата создания", VisaTask.P_CREATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Документ", new String[]{"title"}, VisaTask.P_DOCUMENT).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Инициатор", (VisaTask.initiator() == null)? "" : SimpleFormatter.INSTANCE.format(VisaTask.initiator().person().identityCard().fullFio())).setOrderable(false).setClickable(false));

        model.setIncomingCompletedDataSource(dataSource);
    }

    private void prepareVisaTaskListOutgoingDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getOutgoingDataSource() != null) return;

        DynamicListDataSource<VisaTask> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareVisaTaskListOutgoing(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Дата создания", VisaTask.P_CREATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Документ", new String[]{"title"}, VisaTask.P_DOCUMENT).setOrderable(false));

        model.setOutgoingDataSource(dataSource);
    }

    private void prepareVisaTaskListOutgoingCompletedDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getOutgoingCompletedDataSource() != null) return;

        DynamicListDataSource<VisaTask> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareVisaTaskListOutgoingCompleted(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Дата создания", VisaTask.P_CREATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Документ", new String[]{"title"}, VisaTask.P_DOCUMENT).setOrderable(false));

        model.setOutgoingCompletedDataSource(dataSource);
    }

    public void onClickAcceptVisaTask(IBusinessComponent component)
    {
        component.createChildRegion("visaTaskScope", new ComponentActivator(IUnimvComponents.VISA_TASK_ACCEPT, new ParametersMap()
                .add("visaTaskId", component.getListenerParameter())
        ));
    }

    public void onClickRejectVisaTask(IBusinessComponent component)
    {
        component.createChildRegion("visaTaskScope", new ComponentActivator(IUnimvComponents.VISA_TASK_REJECT, new ParametersMap()
                .add("visaTaskId", component.getListenerParameter())
        ));
    }
}
