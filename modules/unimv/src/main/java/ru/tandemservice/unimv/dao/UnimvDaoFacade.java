/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

import ru.tandemservice.unimv.dao.visa.IVisaDao;
import ru.tandemservice.unimv.dao.visatask.IVisaTaskDao;

/**
 * Фабрика всех дао из модуля UNIMV
 *
 * @author vip_delete
 */
public class UnimvDaoFacade
{
    private static IVisaDao _visaDao;

    private static IVisaTaskDao _visaTaskDao;

    public static IVisaDao getVisaDao()
    {
        if (_visaDao == null)
            _visaDao = (IVisaDao) ApplicationRuntime.getBean(IVisaDao.VISA_DAO_BEAN_NAME);
        return _visaDao;
    }

    public static IVisaTaskDao getVisaTaskDao()
    {
        if (_visaTaskDao == null)
            _visaTaskDao = (IVisaTaskDao) ApplicationRuntime.getBean(IVisaTaskDao.VISA_TASK_DAO_BEAN_NAME);
        return _visaTaskDao;
    }
}
