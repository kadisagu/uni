package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Возможная виза студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPossibleVisaGen extends EntityBase
 implements IPossibleVisa, INaturalIdentifiable<StudentPossibleVisaGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimv.entity.visa.StudentPossibleVisa";
    public static final String ENTITY_NAME = "studentPossibleVisa";
    public static final int VERSION_HASH = 102724277;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_ENTITY = "entity";
    public static final String P_ACTIVE = "active";

    private String _title;     // Название должности (звание) в документе
    private Student _entity;     // Студент
    private boolean _active; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название должности (звание) в документе. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Студент. Свойство не может быть null.
     */
    public void setEntity(Student entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entity.status.active".
     */
    // @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active  Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPossibleVisaGen)
        {
            if (withNaturalIdProperties)
            {
                setTitle(((StudentPossibleVisa)another).getTitle());
                setEntity(((StudentPossibleVisa)another).getEntity());
            }
            setActive(((StudentPossibleVisa)another).isActive());
        }
    }

    public INaturalId<StudentPossibleVisaGen> getNaturalId()
    {
        return new NaturalId(getTitle(), getEntity());
    }

    public static class NaturalId extends NaturalIdBase<StudentPossibleVisaGen>
    {
        private static final String PROXY_NAME = "StudentPossibleVisaNaturalProxy";

        private String _title;
        private Long _entity;

        public NaturalId()
        {}

        public NaturalId(String title, Student entity)
        {
            _title = title;
            _entity = ((IEntity) entity).getId();
        }

        public String getTitle()
        {
            return _title;
        }

        public void setTitle(String title)
        {
            _title = title;
        }

        public Long getEntity()
        {
            return _entity;
        }

        public void setEntity(Long entity)
        {
            _entity = entity;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentPossibleVisaGen.NaturalId) ) return false;

            StudentPossibleVisaGen.NaturalId that = (NaturalId) o;

            if( !equals(getTitle(), that.getTitle()) ) return false;
            if( !equals(getEntity(), that.getEntity()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTitle());
            result = hashCode(result, getEntity());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTitle());
            sb.append("/");
            sb.append(getEntity());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPossibleVisaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPossibleVisa.class;
        }

        public T newInstance()
        {
            return (T) new StudentPossibleVisa();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "entity":
                    return obj.getEntity();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "entity":
                    obj.setEntity((Student) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "entity":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "entity":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "entity":
                    return Student.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPossibleVisa> _dslPath = new Path<StudentPossibleVisa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPossibleVisa");
    }
            

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.StudentPossibleVisa#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.StudentPossibleVisa#getEntity()
     */
    public static Student.Path<Student> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entity.status.active".
     * @see ru.tandemservice.unimv.entity.visa.StudentPossibleVisa#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends StudentPossibleVisa> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private Student.Path<Student> _entity;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.StudentPossibleVisa#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentPossibleVisaGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.StudentPossibleVisa#getEntity()
     */
        public Student.Path<Student> entity()
        {
            if(_entity == null )
                _entity = new Student.Path<Student>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entity.status.active".
     * @see ru.tandemservice.unimv.entity.visa.StudentPossibleVisa#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(StudentPossibleVisaGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return StudentPossibleVisa.class;
        }

        public String getEntityName()
        {
            return "studentPossibleVisa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
