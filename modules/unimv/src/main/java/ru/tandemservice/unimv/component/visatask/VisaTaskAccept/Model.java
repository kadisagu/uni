/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visatask.VisaTaskAccept;

import org.tandemframework.core.component.State;

/**
 * @author vip_delete
 */
@State(keys = {"visaTaskId"}, bindings = {"visaTaskId"})
public class Model
{
    private Long _visaTaskId;      //id задачи которую принимаем
    private String _comment;       //комментарий пользователя

    public Long getVisaTaskId()
    {
        return _visaTaskId;
    }

    public void setVisaTaskId(Long visaTaskId)
    {
        _visaTaskId = visaTaskId;
    }

    public String getComment()
    {
        return _comment;
    }

    public void setComment(String comment)
    {
        _comment = comment;
    }
}
