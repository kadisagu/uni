package ru.tandemservice.unimv.entity.visa;

import org.tandemframework.core.runtime.EntityRuntime;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimv.entity.visa.gen.StudentPossibleVisaGen;

/**
 * Возможная виза студента
 */
public class StudentPossibleVisa extends StudentPossibleVisaGen
{
    @Override
    public String getPersonOwnerTitle()
    {
        return EntityRuntime.getMeta(Student.class).getTitle();
    }

    @Override
    public String getPossibleVisaFullTitle()
    {
        return getEntity().getPerson().getFullFio() + ", " + getTitle();
    }
}