/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.services.visatask;

import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.List;

/**
 * Согласовываем задачу
 *
 * @author vip_delete
 */
public class AcceptVisaTaskService extends TouchVisaTaskService implements ITouchVisaTaskService
{
    @Override
    protected void doExecute() throws Exception
    {
        super.doExecute();

        IAbstractDocument document = getTouchedVisaTask().getVisa().getDocument();

        //1. получаем следующую визу, на которую хоть кто-то назначен
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(document);
        int i = visaList.indexOf(getTouchedVisaTask().getVisa()) + 1;

        if (i == visaList.size())
        {
            // некому дальше визировать
            accept(document);
            getTouchedVisaTask().setVisa(null);
            getCoreDao().update(getTouchedVisaTask());
            return;
        }

        // подставляем следующую визу в задачу согласования
        getTouchedVisaTask().setVisa(visaList.get(i));

        getCoreDao().update(getTouchedVisaTask());
    }

    private void accept(IAbstractDocument document)
    {
        ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(document, UnimvDefines.VISA_ACCEPT_SERVICE_MAP);
        touchService.init(document);
        touchService.execute();
    }
}
