package ru.tandemservice.unimv.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimv_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность visaTask

		//  свойство initiator стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("visatask_t", "initiator_id", true);

		}


    }
}