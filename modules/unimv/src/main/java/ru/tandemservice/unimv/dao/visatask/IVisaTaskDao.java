/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.dao.visatask;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * Dao to work with VisaTasks
 *
 * @author vip_delete
 */
public interface IVisaTaskDao
{
    String VISA_TASK_DAO_BEAN_NAME = "visaTaskDao";

    /**
     * Показывает, есть ли задачи на роли персоны.
     * @param entity роль персоны
     * @return true - если есть задача, false - иначе
     */
    boolean isHasVisaTasks(IPersistentPersonable entity);

    /**
     * Возвращает список входящих задач данной роли персоны.
     * @param entity роль персоны
     * @param firstResult номер первой задачи
     * @param maxResults число задач
     * @return список входящих задач
     */
    ListResult<VisaTask> getIncomingList(IPersistentPersonable entity, int firstResult, int maxResults);

    /**
     * Возвращает список завершенных входящих задач данной роли персоны.
     * @param entity роль персоны
     * @param firstResult номер первой задачи
     * @param maxResults число задач
     * @return список завершенных входящих задач
     */
    ListResult<VisaTask> getIncomingCompletedList(IPersistentPersonable entity, int firstResult, int maxResults);

    /**
     * Возвращает список исходящих задач данной роли персоны.
     * @param entity роль персоны
     * @param firstResult номер первой задачи
     * @param maxResults число задач
     * @return список исходящих задач
     */
    ListResult<VisaTask> getOutgoingList(IPersistentPersonable entity, int firstResult, int maxResults);

    /**
     * Возвращает список завершенных исходящих задач данной роли персоны.
     * @param entity роль персоны
     * @param firstResult номер первой задачи
     * @param maxResults число задач
     * @return список завершенных исходящих задач
     */
    ListResult<VisaTask> getOutgoingCompletedList(IPersistentPersonable entity, int firstResult, int maxResults);

    /**
     * Возвращает количество входящих задач данной роли персоны.
     * @param entity роль персоны
     * @return количество входящих задач
     */
    int getCountIncoming(IPersistentPersonable entity);

    /**
     * Возвращает количество завершенных входящих задач данной роли персоны.
     * @param entity роль персоны
     * @return количество завершенных входящих задач
     */
    int getCountIncomingCompleted(IPersistentPersonable entity);

    /**
     * Возвращает количество исходящих задач данной роли персоны.
     * @param entity роль персоны
     * @return количество исходящих задач
     */
    int getCountOutgoing(IPersistentPersonable entity);

    /**
     * Возвращает количество завершенных исходящих задач данной роли персоны.
     * @param entity роль персоны
     * @return количество завершенных исходящих задач
     */
    int getCountOutgoingCompleted(IPersistentPersonable entity);

    /**
     * Возвращает документ, по которому создана задача.
     * @param visaTask задача
     * @return документ
     */
    IEntity getDocument(VisaTask visaTask);
}
