/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.services.visatask;

import java.util.Date;

import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.services.UniService;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * Согласовываем или нет задачу сотрудника
 *
 * @author vip_delete
 */
public class TouchVisaTaskService extends UniService implements ITouchVisaTaskService
{
    //input
    private Long _visaTaskId;
    private String _comment;

    //output
    private VisaTask _visaTask;

    @Override
    public void init(Long visaTaskId, String comment)
    {
        _visaTaskId = visaTaskId;
        _comment = comment;
    }

    public Long getVisaTaskId()
    {
        return _visaTaskId;
    }

    public void setVisaTaskId(Long visaTaskId)
    {
        _visaTaskId = visaTaskId;
    }

    public String getComment()
    {
        return _comment;
    }

    public void setComment(String comment)
    {
        _comment = comment;
    }

    @Override
    protected void doValidate()
    {
        _visaTask = getCoreDao().getNotNull(VisaTask.class, _visaTaskId);
        Visa visa = _visaTask.getVisa();
        if (visa == null)
            throw new ApplicationException("Задача уже завершена.");
    }

    @Override
    protected void doExecute() throws Exception
    {
        //надо добавить комментарий в историю согласования
        VisaHistoryItem visaHistoryItem = new VisaHistoryItem();
        visaHistoryItem.setDocument(_visaTask.getVisa().getDocument());
        visaHistoryItem.setAuthor(_visaTask.getVisa().getPossibleVisa().getEntity());
        visaHistoryItem.setCreationDate(new Date());
        visaHistoryItem.setComment(_comment);
        visaHistoryItem.setVisaTask(_visaTask);
        getCoreDao().save(visaHistoryItem);
    }

    @Override
    public VisaTask getTouchedVisaTask()
    {
        return _visaTask;
    }
}