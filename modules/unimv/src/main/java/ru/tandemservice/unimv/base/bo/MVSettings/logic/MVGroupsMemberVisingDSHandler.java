/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by ashaburov
 * Date 21.10.11
 */
public class MVGroupsMemberVisingDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ALIAS = "b";

    public MVGroupsMemberVisingDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(GroupsMemberVising.class, ALIAS)
                .column(ALIAS);

        builder.order(DQLExpressions.property(ALIAS, GroupsMemberVising.index()),
                OrderDirection.asc);

        List<DataWrapper> recordList = new ArrayList<DataWrapper>();

        for (GroupsMemberVising item : builder.createStatement(context.getSession()).<GroupsMemberVising>list())
        {
            DataWrapper record = new DataWrapper(item.getId(), item.getTitle(), item);

            boolean synchronizeUser = EntityDataRuntime.getEntityPolicy(GroupsMemberVising.ENTITY_NAME).getItemPolicyById(item.getCode()).getSynchronize().equals(SynchronizeMeta.user);

            record.setProperty("disabled_view", synchronizeUser);

            recordList.add(record);
        }

        return ListOutputBuilder.get(dsInput, recordList).pageable(true).build();
    }
}
