/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visahistory.VisaHistoryList;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

/**
 * @author vip_delete
 */
@State(keys = {"visaOwnerModel"}, bindings = {"visaOwnerModel"})
public class Model
{
    private IVisaOwnerModel _visaOwnerModel;
    private DynamicListDataSource _visaHistoryDataSource;
    private CommonPostfixPermissionModel _secModel;

    public IVisaOwnerModel getVisaOwnerModel()
    {
        return _visaOwnerModel;
    }

    public void setVisaOwnerModel(IVisaOwnerModel visaOwnerModel)
    {
        _visaOwnerModel = visaOwnerModel;
    }

    public DynamicListDataSource getVisaHistoryDataSource()
    {
        return _visaHistoryDataSource;
    }

    public void setVisaHistoryDataSource(DynamicListDataSource visaHistoryDataSource)
    {
        _visaHistoryDataSource = visaHistoryDataSource;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this._secModel = secModel;
    }
}