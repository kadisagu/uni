/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visa.VisaAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.List;

/**
 * @author vip_delete
 */
@Input({
        @Bind( key = "documentId", binding = "documentId"),
        @Bind( key = "groupVisaId", binding = "groupVisaId")
})
public class Model
{
    private Long _documentId;
    private Long _groupVisaId;

    private List<Visa> _toDeleteVisaList;

    private ISelectModel _possibleVisaListModel;

    private List<IPossibleVisa> _possibleVisaList;

    private ISingleSelectModel _groupVisingModel;
    private GroupsMemberVising _groupVising;

    // Getters & Setters

    public List<Visa> getToDeleteVisaList()
    {
        return _toDeleteVisaList;
    }

    public void setToDeleteVisaList(List<Visa> toDeleteVisaList)
    {
        _toDeleteVisaList = toDeleteVisaList;
    }

    public Long getGroupVisaId()
    {
        return _groupVisaId;
    }

    public void setGroupVisaId(Long groupVisaId)
    {
        _groupVisaId = groupVisaId;
    }

    public ISingleSelectModel getGroupVisingModel()
    {
        return _groupVisingModel;
    }

    public void setGroupVisingModel(ISingleSelectModel groupVisingModel)
    {
        _groupVisingModel = groupVisingModel;
    }

    public GroupsMemberVising getGroupVising()
    {
        return _groupVising;
    }

    public void setGroupVising(GroupsMemberVising groupVising)
    {
        _groupVising = groupVising;
    }

    public Long getDocumentId()
    {
        return _documentId;
    }

    public void setDocumentId(Long documentId)
    {
        _documentId = documentId;
    }

    public ISelectModel getPossibleVisaListModel()
    {
        return _possibleVisaListModel;
    }

    public void setPossibleVisaListModel(ISelectModel possibleVisaListModel)
    {
        _possibleVisaListModel = possibleVisaListModel;
    }

    public List<IPossibleVisa> getPossibleVisaList()
    {
        return _possibleVisaList;
    }

    public void setPossibleVisaList(List<IPossibleVisa> possibleVisaList)
    {
        _possibleVisaList = possibleVisaList;
    }
}
