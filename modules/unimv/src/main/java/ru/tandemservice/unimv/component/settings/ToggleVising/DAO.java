/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.settings.ToggleVising;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.BlockColumn;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.UnimvDefines;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStatusList(Arrays.asList(
                new IdentifiableWrapper(0L, "выкл"),
                new IdentifiableWrapper(UnimvDefines.VISING_RUN_NO_TASK, "вкл"),
                new IdentifiableWrapper(UnimvDefines.VISING_RUN_FULL, "акт")
        ));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        //1. получаем настройку визирования
        IDataSettings settings = DataSettingsFacade.getSettings(UnimvDefines.VISA_MODULE_OWNER, UnimvDefines.IS_NEED_VISING_DOCUMENT);

        //2. получаем список документов который можно завизировать
        List<IdentifiableWrapper> result = new ArrayList<>();
        Map<Long, IdentifiableWrapper> valueMap = new HashMap<>();
        for (String entryName : (Set<String>) ApplicationRuntime.getBean(UnimvDefines.VISING_ENTITY_SET))
        {
            IEntityMeta meta = EntityRuntime.getMeta(StringUtils.capitalize(entryName));
            Long status = (Long) settings.getEntry(entryName).getValue();
            valueMap.put(meta.getEntityCode().longValue(), new IdentifiableWrapper(status == null ? 0L : status, ""));
            result.add(new IdentifiableWrapper(meta.getEntityCode().longValue(), meta.getTitle()));
        }

        //3. проставляем значения по умолчанию
        BlockColumn<IdentifiableWrapper> column = (BlockColumn<IdentifiableWrapper>) model.getDataSource().getColumn(Model.STATUS_COLUMN_NAME);
        column.setValueMap(valueMap);

        //4. сортируем
        Collections.sort(result, ITitled.TITLED_COMPARATOR);

        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);
    }
}
