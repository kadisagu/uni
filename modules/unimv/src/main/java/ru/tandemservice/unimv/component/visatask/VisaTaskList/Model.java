/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visatask.VisaTaskList;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * @author vip_delete
 */
@State({
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = "personRoleModel", required = true),
        @Bind(key = "visaTaskSelectedTab", binding = "visaTaskSelectedTab")
})
public class Model
{
    private CommonPostfixPermissionModel _secModel;
    private ISecureRoleContext _personRoleModel;
    private String _visaTaskSelectedTab;
    private Long _visaTaskId;  // id задачи которую entity будет либо принимать, либо отклонять

    private DynamicListDataSource<VisaTask> _incomingDataSource;
    private DynamicListDataSource<VisaTask> _incomingCompletedDataSource;
    private DynamicListDataSource<VisaTask> _outgoingDataSource;
    private DynamicListDataSource<VisaTask> _outgoingCompletedDataSource;

    // Util

    public IPersistentPersonable getEntity()
    {
        return (IPersistentPersonable) _personRoleModel.getSecuredObject();
    }

    // Getters & Setters

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public ISecureRoleContext getPersonRoleModel()
    {
        return _personRoleModel;
    }

    public void setPersonRoleModel(ISecureRoleContext personRoleModel)
    {
        _personRoleModel = personRoleModel;
    }

    public String getVisaTaskSelectedTab()
    {
        return _visaTaskSelectedTab;
    }

    public void setVisaTaskSelectedTab(String visaTaskSelectedTab)
    {
        _visaTaskSelectedTab = visaTaskSelectedTab;
    }

    public Long getVisaTaskId()
    {
        return _visaTaskId;
    }

    public void setVisaTaskId(Long visaTaskId)
    {
        _visaTaskId = visaTaskId;
    }

    public DynamicListDataSource<VisaTask> getIncomingDataSource()
    {
        return _incomingDataSource;
    }

    public void setIncomingDataSource(DynamicListDataSource<VisaTask> incomingDataSource)
    {
        _incomingDataSource = incomingDataSource;
    }

    public DynamicListDataSource<VisaTask> getIncomingCompletedDataSource()
    {
        return _incomingCompletedDataSource;
    }

    public void setIncomingCompletedDataSource(DynamicListDataSource<VisaTask> incomingCompletedDataSource)
    {
        _incomingCompletedDataSource = incomingCompletedDataSource;
    }

    public DynamicListDataSource<VisaTask> getOutgoingDataSource()
    {
        return _outgoingDataSource;
    }

    public void setOutgoingDataSource(DynamicListDataSource<VisaTask> outgoingDataSource)
    {
        _outgoingDataSource = outgoingDataSource;
    }

    public DynamicListDataSource<VisaTask> getOutgoingCompletedDataSource()
    {
        return _outgoingCompletedDataSource;
    }

    public void setOutgoingCompletedDataSource(DynamicListDataSource<VisaTask> outgoingCompletedDataSource)
    {
        _outgoingCompletedDataSource = outgoingCompletedDataSource;
    }
}
