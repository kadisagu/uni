/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.dao.visatask;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;
import ru.tandemservice.unimv.entity.visa.VisaTask;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Dao Implementation to work with VisaTasks
 *
 * @author vip_delete
 */
public class VisaTaskDao extends HibernateDaoSupport implements IVisaTaskDao
{
    @Override
    public boolean isHasVisaTasks(IPersistentPersonable entity)
    {
        IDQLExpression plus = DQLExpressions.plus(getIncomingBuilder(entity).buildCountQuery(),
                DQLExpressions.plus(getIncomingCompletedBuilder(entity).buildCountQuery(),
                        DQLExpressions.plus(getOutgoingBuilder(entity).buildCountQuery(), getOutgoingCompletedBuilder(entity).buildCountQuery()))
        );

        DQLSelectBuilder b = new DQLSelectBuilder().column(plus);
        Number number = b.createStatement(new DQLExecutionContext(getSession())).uniqueResult();
        int count = number == null ? 0 : number.intValue();
        return count > 0;
    }

    @Override
    public ListResult<VisaTask> getIncomingList(IPersistentPersonable entity, int firstResult, int maxResults)
    {
        return getListResult(getIncomingBuilder(entity), firstResult, maxResults);
    }

    @Override
    public ListResult<VisaTask> getIncomingCompletedList(IPersistentPersonable entity, int firstResult, int maxResults)
    {
        return getListResult(getIncomingCompletedBuilder(entity), firstResult, maxResults);
    }

    @Override
    public ListResult<VisaTask> getOutgoingList(IPersistentPersonable entity, int firstResult, int maxResults)
    {
        return getListResult(getOutgoingBuilder(entity), firstResult, maxResults);
    }

    @Override
    public ListResult<VisaTask> getOutgoingCompletedList(IPersistentPersonable entity, int firstResult, int maxResults)
    {
        return getListResult(getOutgoingCompletedBuilder(entity), firstResult, maxResults);
    }

    @Override
    public int getCountIncoming(IPersistentPersonable entity)
    {
        return getResultCount(getIncomingBuilder(entity));
    }

    @Override
    public int getCountIncomingCompleted(IPersistentPersonable entity)
    {
        return getResultCount(getIncomingCompletedBuilder(entity));
    }

    @Override
    public int getCountOutgoing(IPersistentPersonable entity)
    {
        return getResultCount(getOutgoingBuilder(entity));
    }

    @Override
    public int getCountOutgoingCompleted(IPersistentPersonable entity)
    {
        return getResultCount(getOutgoingCompletedBuilder(entity));
    }

    @Override
    public IEntity getDocument(VisaTask visaTask)
    {
        if (visaTask.getVisa() != null)
        {
            //задача еще не завершена
            return visaTask.getVisa().getDocument();
        } else
        {
            //задача завершена. надо смотреть историю
            Criteria c = getSession().createCriteria(VisaHistoryItem.ENTITY_CLASS);
            c.add(Restrictions.eq(VisaHistoryItem.L_VISA_TASK, visaTask));
            c.setProjection(Projections.property(VisaHistoryItem.L_DOCUMENT));
            return (IEntity) c.uniqueResult();
        }
    }

    private static DQLSelectBuilder getIncomingBuilder(IPersistentPersonable entity)
    {
        long id = entity.getPerson().getId();
        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(VisaTask.class, "t").column("t");
        b.where(eq(property("t", VisaTask.visa().possibleVisa().entity().person().id()), value(id)));
        return b;
    }

    private static DQLSelectBuilder getIncomingCompletedBuilder(IPersistentPersonable entity)
    {
        long id = entity.getPerson().getId();
        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(VisaTask.class, "t").column("t");
        b.where(isNull(property("t", VisaTask.visa())));
        b.where(exists(VisaHistoryItem.class,
                       VisaHistoryItem.author().person().id().s(), id,
                       VisaHistoryItem.visaTask().s(), property("t")));
        return b;
    }

    private static DQLSelectBuilder getOutgoingBuilder(IPersistentPersonable entity)
    {
        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(VisaTask.class, "t").column("t");
        b.where(eq(property("t", VisaTask.initiator().person()), value(entity.getPerson())));
        b.where(isNotNull(property("t", VisaTask.visa())));
        return b;
    }

    private static DQLSelectBuilder getOutgoingCompletedBuilder(IPersistentPersonable entity)
    {
        long id = entity.getPerson().getId();
        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(VisaTask.class, "t").column("t");
        b.where(eq(property("t", VisaTask.initiator().person()), value(entity.getPerson())));
        b.where(isNull(property("t", VisaTask.visa())));
        return b;
    }

    private int getResultCount(DQLSelectBuilder builder)
    {
        DQLExecutionContext content = new DQLExecutionContext(getSession());
        Number number = builder.createCountStatement(content).uniqueResult();
        return number == null ? 0 : number.intValue();
    }

    private ListResult<VisaTask> getListResult(DQLSelectBuilder builder, int firstResult, int maxResults)
    {
        Number number = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        int count = number == null ? 0 : number.intValue();
        IDQLStatement s = builder.createStatement(new DQLExecutionContext(getSession()));
        s.setFirstResult(firstResult);
        s.setMaxResults(maxResults);
        return new ListResult<VisaTask>(s.<VisaTask>list(), count);
    }
}
