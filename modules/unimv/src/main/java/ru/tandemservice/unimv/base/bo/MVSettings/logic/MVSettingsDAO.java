/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

/**
 * Create by ashaburov
 * Date 24.10.11
 */
public class MVSettingsDAO extends CommonDAO implements IMVSettingsDAO
{
    @Override
    public void deleteGroupsMemberVising(Long id)
    {
        GroupsMemberVising groupsMemberVising = get(GroupsMemberVising.class, id);
        boolean synchronizeUser = EntityDataRuntime.getEntityPolicy(GroupsMemberVising.ENTITY_NAME).getItemPolicyById(groupsMemberVising.getCode()).getSynchronize().equals(SynchronizeMeta.user);
        if (synchronizeUser)
            throw new ApplicationException("Группу «" + groupsMemberVising.getTitle() + "» нельзя удалить.");
        delete(groupsMemberVising);
    }

    @Override
    public void doDownGroup(Long id)
    {
        GroupsMemberVising prevGroup = get(GroupsMemberVising.class, id);
        if (prevGroup.getIndex() == getCount(GroupsMemberVising.class) - 1)
            return;

        GroupsMemberVising nextGroup = get(GroupsMemberVising.class, GroupsMemberVising.index(), prevGroup.getIndex() + 1);
        int prevIndex = prevGroup.getIndex();
        int nextIndex = nextGroup.getIndex();

        nextGroup.setIndex(-1);
        update(nextGroup);
        getSession().flush();

        prevGroup.setIndex(nextIndex);
        update(prevGroup);
        getSession().flush();

        nextGroup.setIndex(prevIndex);

        update(nextGroup);
        update(prevGroup);
    }

    @Override
    public void doUpGroup(Long id)
    {
        GroupsMemberVising prevGroup = get(GroupsMemberVising.class, id);
        if (prevGroup.getIndex() == 0)
            return;

        GroupsMemberVising nextGroup = get(GroupsMemberVising.class, GroupsMemberVising.index(), prevGroup.getIndex() - 1);
        int prevIndex = prevGroup.getIndex();
        int nextIndex = nextGroup.getIndex();

        nextGroup.setIndex(-1);
        update(nextGroup);
        getSession().flush();

        prevGroup.setIndex(nextIndex);
        update(prevGroup);
        getSession().flush();

        nextGroup.setIndex(prevIndex);

        update(nextGroup);
        update(prevGroup);
    }
}
