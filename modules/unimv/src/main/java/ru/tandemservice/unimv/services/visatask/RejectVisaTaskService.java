/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.services.visatask;

import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;

/**
 * Согласовываем задачу
 *
 * @author vip_delete
 */
public class RejectVisaTaskService extends TouchVisaTaskService implements ITouchVisaTaskService
{
    @Override
    protected void doExecute() throws Exception
    {
        super.doExecute();

        //задача согласования завершена
        IAbstractDocument document = getTouchedVisaTask().getVisa().getDocument();

        getTouchedVisaTask().setVisa(null);

        getCoreDao().update(getTouchedVisaTask());

        //запускаем процедуру отрицательного завершения цикла согласования

        ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(document, UnimvDefines.VISA_REJECT_SERVICE_MAP);
        touchService.init(document);
        touchService.execute();
    }
}