package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.Visa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виза
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VisaGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimv.entity.visa.Visa";
    public static final String ENTITY_NAME = "visa";
    public static final int VERSION_HASH = -1580020125;
    private static IEntityMeta ENTITY_META;

    public static final String L_POSSIBLE_VISA = "possibleVisa";
    public static final String L_DOCUMENT = "document";
    public static final String P_MANDATORY = "mandatory";
    public static final String P_INDEX = "index";
    public static final String L_GROUP_MEMBER_VISING = "groupMemberVising";

    private IPossibleVisa _possibleVisa;     // Возможная виза
    private IAbstractDocument _document;     // Документ
    private boolean _mandatory;     // Обязательна
    private int _index;     // Номер
    private GroupsMemberVising _groupMemberVising;     // Группа участников визирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Возможная виза. Свойство не может быть null.
     */
    @NotNull
    public IPossibleVisa getPossibleVisa()
    {
        return _possibleVisa;
    }

    /**
     * @param possibleVisa Возможная виза. Свойство не может быть null.
     */
    public void setPossibleVisa(IPossibleVisa possibleVisa)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && possibleVisa!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPossibleVisa.class);
            IEntityMeta actual =  possibleVisa instanceof IEntity ? EntityRuntime.getMeta((IEntity) possibleVisa) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_possibleVisa, possibleVisa);
        _possibleVisa = possibleVisa;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public IAbstractDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(IAbstractDocument document)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && document!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  document instanceof IEntity ? EntityRuntime.getMeta((IEntity) document) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Обязательна. Свойство не может быть null.
     */
    @NotNull
    public boolean isMandatory()
    {
        return _mandatory;
    }

    /**
     * @param mandatory Обязательна. Свойство не может быть null.
     */
    public void setMandatory(boolean mandatory)
    {
        dirty(_mandatory, mandatory);
        _mandatory = mandatory;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index Номер. Свойство не может быть null.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     */
    @NotNull
    public GroupsMemberVising getGroupMemberVising()
    {
        return _groupMemberVising;
    }

    /**
     * @param groupMemberVising Группа участников визирования. Свойство не может быть null.
     */
    public void setGroupMemberVising(GroupsMemberVising groupMemberVising)
    {
        dirty(_groupMemberVising, groupMemberVising);
        _groupMemberVising = groupMemberVising;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VisaGen)
        {
            setPossibleVisa(((Visa)another).getPossibleVisa());
            setDocument(((Visa)another).getDocument());
            setMandatory(((Visa)another).isMandatory());
            setIndex(((Visa)another).getIndex());
            setGroupMemberVising(((Visa)another).getGroupMemberVising());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VisaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Visa.class;
        }

        public T newInstance()
        {
            return (T) new Visa();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "possibleVisa":
                    return obj.getPossibleVisa();
                case "document":
                    return obj.getDocument();
                case "mandatory":
                    return obj.isMandatory();
                case "index":
                    return obj.getIndex();
                case "groupMemberVising":
                    return obj.getGroupMemberVising();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "possibleVisa":
                    obj.setPossibleVisa((IPossibleVisa) value);
                    return;
                case "document":
                    obj.setDocument((IAbstractDocument) value);
                    return;
                case "mandatory":
                    obj.setMandatory((Boolean) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "groupMemberVising":
                    obj.setGroupMemberVising((GroupsMemberVising) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "possibleVisa":
                        return true;
                case "document":
                        return true;
                case "mandatory":
                        return true;
                case "index":
                        return true;
                case "groupMemberVising":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "possibleVisa":
                    return true;
                case "document":
                    return true;
                case "mandatory":
                    return true;
                case "index":
                    return true;
                case "groupMemberVising":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "possibleVisa":
                    return IPossibleVisa.class;
                case "document":
                    return IAbstractDocument.class;
                case "mandatory":
                    return Boolean.class;
                case "index":
                    return Integer.class;
                case "groupMemberVising":
                    return GroupsMemberVising.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Visa> _dslPath = new Path<Visa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Visa");
    }
            

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getPossibleVisa()
     */
    public static IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
    {
        return _dslPath.possibleVisa();
    }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getDocument()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Обязательна. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#isMandatory()
     */
    public static PropertyPath<Boolean> mandatory()
    {
        return _dslPath.mandatory();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getGroupMemberVising()
     */
    public static GroupsMemberVising.Path<GroupsMemberVising> groupMemberVising()
    {
        return _dslPath.groupMemberVising();
    }

    public static class Path<E extends Visa> extends EntityPath<E>
    {
        private IPossibleVisaGen.Path<IPossibleVisa> _possibleVisa;
        private IAbstractDocumentGen.Path<IAbstractDocument> _document;
        private PropertyPath<Boolean> _mandatory;
        private PropertyPath<Integer> _index;
        private GroupsMemberVising.Path<GroupsMemberVising> _groupMemberVising;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getPossibleVisa()
     */
        public IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
        {
            if(_possibleVisa == null )
                _possibleVisa = new IPossibleVisaGen.Path<IPossibleVisa>(L_POSSIBLE_VISA, this);
            return _possibleVisa;
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getDocument()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> document()
        {
            if(_document == null )
                _document = new IAbstractDocumentGen.Path<IAbstractDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Обязательна. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#isMandatory()
     */
        public PropertyPath<Boolean> mandatory()
        {
            if(_mandatory == null )
                _mandatory = new PropertyPath<Boolean>(VisaGen.P_MANDATORY, this);
            return _mandatory;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(VisaGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.Visa#getGroupMemberVising()
     */
        public GroupsMemberVising.Path<GroupsMemberVising> groupMemberVising()
        {
            if(_groupMemberVising == null )
                _groupMemberVising = new GroupsMemberVising.Path<GroupsMemberVising>(L_GROUP_MEMBER_VISING, this);
            return _groupMemberVising;
        }

        public Class getEntityClass()
        {
            return Visa.class;
        }

        public String getEntityName()
        {
            return "visa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
