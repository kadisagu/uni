/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visatask.VisaTaskList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.Person.util.PersonRoleSecModel;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setSecModel(PersonRoleSecModel.instance(getNotNull(Person.class, model.getPersonRoleModel().getPersonId()), model.getPersonRoleModel().getSecuredObject()));
    }

    @Override
    public void prepareVisaTaskListIncoming(Model model)
    {
        DynamicListDataSource<VisaTask> dataSource = model.getIncomingDataSource();

        dataSource.setTotalSize(UnimvDaoFacade.getVisaTaskDao().getCountIncoming(model.getEntity()));

        int firstResult = (int) dataSource.getStartRow();
        int maxResults = (int) dataSource.getCountRow();

        ListResult<VisaTask> visaTaskListPage = UnimvDaoFacade.getVisaTaskDao().getIncomingList(model.getEntity(), firstResult, maxResults);

        dataSource.createPage(visaTaskListPage.getObjects());
    }

    @Override
    public void prepareVisaTaskListIncomingCompleted(Model model)
    {
        DynamicListDataSource<VisaTask> dataSource = model.getIncomingCompletedDataSource();
        dataSource.setTotalSize(UnimvDaoFacade.getVisaTaskDao().getCountIncomingCompleted(model.getEntity()));

        int firstResult = (int) dataSource.getStartRow();
        int maxResults = (int) dataSource.getCountRow();

        ListResult<VisaTask> visaTaskListPage = UnimvDaoFacade.getVisaTaskDao().getIncomingCompletedList(model.getEntity(), firstResult, maxResults);

        dataSource.createPage(visaTaskListPage.getObjects());
    }

    @Override
    public void prepareVisaTaskListOutgoing(Model model)
    {
        DynamicListDataSource<VisaTask> dataSource = model.getOutgoingDataSource();
        dataSource.setTotalSize(UnimvDaoFacade.getVisaTaskDao().getCountOutgoing(model.getEntity()));

        int firstResult = (int) dataSource.getStartRow();
        int maxResults = (int) dataSource.getCountRow();

        ListResult<VisaTask> visaTaskListPage = UnimvDaoFacade.getVisaTaskDao().getOutgoingList(model.getEntity(), firstResult, maxResults);

        dataSource.createPage(visaTaskListPage.getObjects());
    }

    @Override
    public void prepareVisaTaskListOutgoingCompleted(Model model)
    {
        DynamicListDataSource<VisaTask> dataSource = model.getOutgoingCompletedDataSource();
        dataSource.setTotalSize(UnimvDaoFacade.getVisaTaskDao().getCountOutgoingCompleted(model.getEntity()));

        int firstResult = (int) dataSource.getStartRow();
        int maxResults = (int) dataSource.getCountRow();

        ListResult<VisaTask> visaTaskListPage = UnimvDaoFacade.getVisaTaskDao().getOutgoingCompletedList(model.getEntity(), firstResult, maxResults);

        dataSource.createPage(visaTaskListPage.getObjects());
    }
}
