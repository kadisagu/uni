/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.component.settings.PossibleVisaList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
public interface IDAO extends IUniDao<Model>
{
    void doDeletePossibleVisa(Long possibleVisaId);
}
