/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unimv.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;

/**
 * @author Vasily Zhukov
 * @since 29.03.2012
 */
public class EmployeePostViewExtUI extends UIAddon
{
    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
    
    public boolean isHasVisaTasks()
    {
        EmployeePostViewUI presenter = getPresenter();
        
        return UnimvDaoFacade.getVisaTaskDao().isHasVisaTasks(presenter.getEmployeePost());
    }
}
