/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv.component.settings.PossibleVisaList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;

/**
 * @author Vasily Zhukov
 * @since 06.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<IPossibleVisa> dataSource = UniBaseUtils.createDataSource(component, getDao());

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("ФИО", IPossibleVisaGen.entity().person().identityCard().fullFio());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return ((IPossibleVisa) ((ViewWrapper) entity).getEntity()).getEntity().getId();
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Название должности (звание) в документе", IPossibleVisaGen.title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Категория визы", IPossibleVisa.PERSON_OWNER_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", Model.ORG_UNIT_TITLE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить возможную визу «{0}»?", IPossibleVisaGen.title()));

        getModel(component).setDataSource(dataSource);
    }

    public void onClickAddItem(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.unimv.component.settings.PossibleVisaAddEdit.Controller.class.getPackage().getName()));
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.unimv.component.settings.PossibleVisaAddEdit.Controller.class.getPackage().getName(), new ParametersMap()
                .add("possibleVisaId", component.<Long>getListenerParameter())
        ));
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        getDao().doDeletePossibleVisa(component.<Long>getListenerParameter());
    }

    public void onClickSearch(IBusinessComponent component)
    {
        final Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }
}