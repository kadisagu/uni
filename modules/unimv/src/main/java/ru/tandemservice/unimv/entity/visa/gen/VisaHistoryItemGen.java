package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.base.entity.gen.IPersistentPersonableGen;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент истории согласования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VisaHistoryItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimv.entity.visa.VisaHistoryItem";
    public static final String ENTITY_NAME = "visaHistoryItem";
    public static final int VERSION_HASH = 1837366676;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_COMMENT = "comment";
    public static final String L_AUTHOR = "author";
    public static final String L_DOCUMENT = "document";
    public static final String L_VISA_TASK = "visaTask";

    private Date _creationDate;     // Дата создания
    private String _comment;     // Комментарий к согласованию/отклонению визы
    private IPersistentPersonable _author;     // Автор
    private IAbstractDocument _document;     // Документ
    private VisaTask _visaTask;     // Задача согласования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Комментарий к согласованию/отклонению визы.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий к согласованию/отклонению визы.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Автор. Свойство не может быть null.
     */
    @NotNull
    public IPersistentPersonable getAuthor()
    {
        return _author;
    }

    /**
     * @param author Автор. Свойство не может быть null.
     */
    public void setAuthor(IPersistentPersonable author)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && author!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPersistentPersonable.class);
            IEntityMeta actual =  author instanceof IEntity ? EntityRuntime.getMeta((IEntity) author) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_author, author);
        _author = author;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public IAbstractDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(IAbstractDocument document)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && document!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  document instanceof IEntity ? EntityRuntime.getMeta((IEntity) document) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Задача согласования. Свойство не может быть null.
     */
    @NotNull
    public VisaTask getVisaTask()
    {
        return _visaTask;
    }

    /**
     * @param visaTask Задача согласования. Свойство не может быть null.
     */
    public void setVisaTask(VisaTask visaTask)
    {
        dirty(_visaTask, visaTask);
        _visaTask = visaTask;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VisaHistoryItemGen)
        {
            setCreationDate(((VisaHistoryItem)another).getCreationDate());
            setComment(((VisaHistoryItem)another).getComment());
            setAuthor(((VisaHistoryItem)another).getAuthor());
            setDocument(((VisaHistoryItem)another).getDocument());
            setVisaTask(((VisaHistoryItem)another).getVisaTask());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VisaHistoryItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VisaHistoryItem.class;
        }

        public T newInstance()
        {
            return (T) new VisaHistoryItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "creationDate":
                    return obj.getCreationDate();
                case "comment":
                    return obj.getComment();
                case "author":
                    return obj.getAuthor();
                case "document":
                    return obj.getDocument();
                case "visaTask":
                    return obj.getVisaTask();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "author":
                    obj.setAuthor((IPersistentPersonable) value);
                    return;
                case "document":
                    obj.setDocument((IAbstractDocument) value);
                    return;
                case "visaTask":
                    obj.setVisaTask((VisaTask) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "creationDate":
                        return true;
                case "comment":
                        return true;
                case "author":
                        return true;
                case "document":
                        return true;
                case "visaTask":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "creationDate":
                    return true;
                case "comment":
                    return true;
                case "author":
                    return true;
                case "document":
                    return true;
                case "visaTask":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "creationDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "author":
                    return IPersistentPersonable.class;
                case "document":
                    return IAbstractDocument.class;
                case "visaTask":
                    return VisaTask.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VisaHistoryItem> _dslPath = new Path<VisaHistoryItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VisaHistoryItem");
    }
            

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Комментарий к согласованию/отклонению визы.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Автор. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getAuthor()
     */
    public static IPersistentPersonableGen.Path<IPersistentPersonable> author()
    {
        return _dslPath.author();
    }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getDocument()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Задача согласования. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getVisaTask()
     */
    public static VisaTask.Path<VisaTask> visaTask()
    {
        return _dslPath.visaTask();
    }

    public static class Path<E extends VisaHistoryItem> extends EntityPath<E>
    {
        private PropertyPath<Date> _creationDate;
        private PropertyPath<String> _comment;
        private IPersistentPersonableGen.Path<IPersistentPersonable> _author;
        private IAbstractDocumentGen.Path<IAbstractDocument> _document;
        private VisaTask.Path<VisaTask> _visaTask;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(VisaHistoryItemGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Комментарий к согласованию/отклонению визы.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(VisaHistoryItemGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Автор. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getAuthor()
     */
        public IPersistentPersonableGen.Path<IPersistentPersonable> author()
        {
            if(_author == null )
                _author = new IPersistentPersonableGen.Path<IPersistentPersonable>(L_AUTHOR, this);
            return _author;
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getDocument()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> document()
        {
            if(_document == null )
                _document = new IAbstractDocumentGen.Path<IAbstractDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Задача согласования. Свойство не может быть null.
     * @see ru.tandemservice.unimv.entity.visa.VisaHistoryItem#getVisaTask()
     */
        public VisaTask.Path<VisaTask> visaTask()
        {
            if(_visaTask == null )
                _visaTask = new VisaTask.Path<VisaTask>(L_VISA_TASK, this);
            return _visaTask;
        }

        public Class getEntityClass()
        {
            return VisaHistoryItem.class;
        }

        public String getEntityName()
        {
            return "visaHistoryItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
