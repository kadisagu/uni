/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.dao.visa;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;
import ru.tandemservice.unimv.entity.visa.VisaTask;

import java.util.List;

/**
 * Dao Implementation to work with Visa
 *
 * @author vip_delete
 */
public class VisaDao extends SharedBaseDao implements IVisaDao
{
    @Override
    public Visa getVisa(IAbstractDocument document, IPossibleVisa possibleVisa)
    {
        Criteria c = getSession().createCriteria(Visa.ENTITY_CLASS, "visa");
        c.add(Restrictions.eq("visa." + Visa.L_DOCUMENT, document));
        c.add(Restrictions.eq("visa." + Visa.L_POSSIBLE_VISA, possibleVisa));
        return (Visa) c.uniqueResult();
    }

    @Override
    public List<Visa> getVisaList(IAbstractDocument document)
    {
        return getList(Visa.class, Visa.L_DOCUMENT, document, Visa.groupMemberVising().index().s(), Visa.P_INDEX);
    }

    @Override
    public VisaTask getCurrentVisaTask(IAbstractDocument document)
    {
        Criteria c = getSession().createCriteria(VisaTask.ENTITY_CLASS, "visaTask");
        c.createAlias("visaTask." + VisaTask.L_VISA, "visa");
        c.add(Restrictions.eq("visa." + Visa.L_DOCUMENT, document));
        return (VisaTask) c.uniqueResult();
    }

    @Override
    public List<VisaHistoryItem> getVisaHistoryList(IAbstractDocument document)
    {
        return getList(VisaHistoryItem.class, VisaHistoryItem.L_DOCUMENT, document, VisaHistoryItem.P_CREATION_DATE);
    }

    @Override
    public Long getVisingStatus(IAbstractDocument document)
    {
        String entityName = EntityRuntime.getMeta(document).getName();
        IDataSettings settings = DataSettingsFacade.getSettings(UnimvDefines.VISA_MODULE_OWNER, UnimvDefines.IS_NEED_VISING_DOCUMENT);
        return (Long) settings.getEntry(entityName).getValue();
    }

    @Override
    public void doDeletePossibleVisa(IPossibleVisa possibleVisa)
    {
        IEntityMeta possibleVisaMeta = EntityRuntime.getMeta(IPossibleVisa.class);

        for (IRelationMeta rel : possibleVisaMeta.getIncomingRelations())
        {
            String className = rel.getForeignEntity().getClassName();
            String propertyName = rel.getForeignPropertyName();

            MQBuilder builder = new MQBuilder(className, "r");
            builder.add(MQExpression.eq("r", propertyName, possibleVisa));
            if (builder.getResultCount(getSession()) > 0)
                throw new ApplicationException("Нельзя удалить объект, т.к. на него ссылается объект типа «" + rel.getForeignEntity().getTitle() + "».");
        }

        getSession().delete(possibleVisa);
    }
}
