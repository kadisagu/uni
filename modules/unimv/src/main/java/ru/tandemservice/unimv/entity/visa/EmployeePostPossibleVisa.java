package ru.tandemservice.unimv.entity.visa;

import org.tandemframework.core.runtime.EntityRuntime;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unimv.entity.visa.gen.EmployeePostPossibleVisaGen;

/**
 * Возможная виза сотрудника
 */
public class EmployeePostPossibleVisa extends EmployeePostPossibleVisaGen
{
    @Override
    public String getPersonOwnerTitle()
    {
        return EntityRuntime.getMeta(EmployeePost.class).getTitle();
    }

    @Override
    public String getPossibleVisaFullTitle()
    {
        return getEntity().getPerson().getFullFio() + ", " + getTitle();
    }
}