/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.services.visatask;

import ru.tandemservice.uni.services.IUniService;
import ru.tandemservice.unimv.IAbstractDocument;

/**
 * Интерфейс сервиса, который либо запускается, например,
 * в момент "инициализации процедуры согласования",
 * либо при условии "положительного согласования",
 * либо в случае "не согласования"
 *
 * @author vip_delete
 */
public interface ITouchVisaTaskHandler extends IUniService
{
    void init(IAbstractDocument document);
}
