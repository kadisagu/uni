/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv.component.visa.VisaList;

import org.hibernate.Session;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.List;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        // права данной вкладки имеют вид <permissionKey>_<secPostfix>
        // где permissionKey - префикс права
        // entityName - имя entity для visaOwnerId
        // регистрация этих прав должна происходить в модуле, который использует этот бизнес компонент
        // ПРАВА : addVisa_secPostfix, editVisaIndex_secPostfix, delVisa_secPostfix
        // + не забудьте добавить права на просмотр вкладок "Визы" и "История согласования"
        model.setSecModel(new CommonPostfixPermissionModel(model.getVisaOwnerModel().getSecPostfix()));
    }

    @Override
    public void doVisaUp(Model model, Long visaId)
    {
        Session session = getSession();
        IAbstractDocument document = model.getVisaOwnerModel().getDocument();
        Visa visa = getNotNull(Visa.class, visaId);

        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(document);

        int i = 0;
        while (i < visaList.size() && !visa.equals(visaList.get(i))) i++;
        if (i == visaList.size())
            throw new ApplicationException("Такой визы уже нет в списке виз.");

        visaList.removeIf(visaItem -> !visaItem.getGroupMemberVising().equals(visa.getGroupMemberVising()));

        int index = visaList.indexOf(visa);

        if (index > 0)
        {
            // если есть предыдущая виза, то меняемся с ней индексами
            // надо учесть что существует UniqueConstraint("visaUniqueIndexes", "visaOwnerId_p", "index_p")
            // поэтому используется трюк с "-1"

            Visa prevVisa = visaList.get(index - 1);
            Visa nextVisa = visaList.get(index);

            Integer prevIndex = prevVisa.getIndex();
            prevVisa.setIndex(-1);
            session.update(prevVisa);
            session.flush();

            Integer nextIndex = nextVisa.getIndex();
            nextVisa.setIndex(prevIndex);
            session.update(nextVisa);
            session.flush();

            prevVisa.setIndex(nextIndex);
            session.update(prevVisa);
        }
    }

    @Override
    public void doVisaDown(Model model, Long visaId)
    {
        Session session = getSession();
        IAbstractDocument document = model.getVisaOwnerModel().getDocument();
        Visa visa = getNotNull(Visa.class, visaId);

        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(document);

        int i = 0;
        while (i < visaList.size() && !visa.equals(visaList.get(i))) i++;
        if (i == visaList.size())
            throw new ApplicationException("Такой визы уже нет в списке виз.");

        visaList.removeIf(visaItem -> !visaItem.getGroupMemberVising().equals(visa.getGroupMemberVising()));

        int index = visaList.indexOf(visa);

        if (index < visaList.size() - 1)
        {
            // если есть следующая виза, то меняемся с ней индексами
            // надо учесть что существует UniqueConstraint("visaUniqueIndexes", "visaOwnerId_p", "index_p")
            // поэтому используется трюк с "-1"

            Visa prevVisa = visaList.get(index);
            Visa nextVisa = visaList.get(index + 1);

            Integer prevIndex = prevVisa.getIndex();
            prevVisa.setIndex(-1);
            session.update(prevVisa);
            session.flush();

            Integer nextIndex = nextVisa.getIndex();
            nextVisa.setIndex(prevIndex);
            session.update(nextVisa);
            session.flush();

            prevVisa.setIndex(nextIndex);
            session.update(prevVisa);
        }
    }

    @Override
    public void doDeleteVisa(Model model, Long visaId)
    {
        Session session = getSession();
        IAbstractDocument document = model.getVisaOwnerModel().getDocument();
        Visa visa = getNotNull(Visa.class, visaId);

        session.delete(visa);

        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(document);

        int i = 999;
        for (Visa visaItem : visaList)
            visaItem.setIndex(i++);

        session.flush();

        i = 0;
        for (Visa visaItem : visaList)
            visaItem.setIndex(i++);
    }

    @Override
    public void doDeleteGroupVisa(Model model, Long visaId)
    {
        Session session = getSession();
        IAbstractDocument document = model.getVisaOwnerModel().getDocument();
        Visa visa = getNotNull(Visa.class, visaId);
        GroupsMemberVising groupVising = visa.getGroupMemberVising();

        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(document);

        for (Visa visaItem : visaList)
            if (visaItem.getGroupMemberVising().equals(groupVising))
                session.delete(visaItem);

        int i = 999;
        for (Visa visaItem : visaList)
            visaItem.setIndex(i++);

        session.flush();

        i = 0;
        for (Visa visaItem : visaList)
            visaItem.setIndex(i++);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(model.getVisaOwnerModel().getDocument());

        model.getVisaDataSource().setCountRow(visaList.size());

        UniBaseUtils.createPage(model.getVisaDataSource(), visaList);

        int i = 1;
        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getVisaDataSource()))
        {
            viewWrapper.setViewProperty("number", i++);

            IPossibleVisa possibleVisa = ((Visa) viewWrapper.getEntity()).getPossibleVisa();

            if (possibleVisa instanceof EmployeePostPossibleVisa)
                viewWrapper.setViewProperty(Model.ORG_UNIT_TITLE, ((EmployeePost) possibleVisa.getEntity()).getOrgUnit().getExtendedShortTitle2());
            else if (possibleVisa instanceof StudentPossibleVisa)
                viewWrapper.setViewProperty(Model.ORG_UNIT_TITLE, ((StudentPossibleVisa) possibleVisa).getEntity().getEducationOrgUnit().getFormativeOrgUnit().getExtendedShortTitle2() + ", " + ((StudentPossibleVisa) possibleVisa).getEntity().getCourse().getTitle() + " курс");
            else
                throw new RuntimeException("Unknown possibleVisa class '" + possibleVisa + "'");
        }
    }
}
