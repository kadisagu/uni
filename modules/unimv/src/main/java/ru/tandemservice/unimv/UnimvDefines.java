/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimv;

/**
 * @author vip_delete
 */
public interface UnimvDefines
{
    // это имя spring-bean типа tandem-uni:map. key равен имени entity, value - имени класса
    // типа ITouchVisaTaskHandler
    // при инициализации процерусы согласования для entityId берется entityName изапускается метод execute
    // в указанном классе. Для остальных Map механизм работает так же.
    //INFO: обычно здесь объект переводят в состояние "на согласовании"
    String VISA_INIT_SERVICE_MAP = "visaInitCoordinationMap";

    // для этого spring-bean'а сервис завершает процедуру положительного согласования
    //INFO: обычно здесь объект переводят в состояние "согласован"
    String VISA_ACCEPT_SERVICE_MAP = "visaAcceptMap";

    // для этого spring-bean'а сервис завершает процедуру отрицательного согласования
    //INFO: обычно здесь объект переводят в состояние "отклонен"
    String VISA_REJECT_SERVICE_MAP = "visaRejectMap";

    //владелец настроек модуля unimv
    String VISA_MODULE_OWNER = "visaModuleOwner";

    //имя настройки в которой хранится "надо ли визировать определенный тип документа"
    String IS_NEED_VISING_DOCUMENT = "isNeedVisingDocument";

    // по этому spring-bean'у строится страница настройки визирования
    String VISING_ENTITY_SET = "visingEntitySet";

    //имя boolean свойства: если ли в системе механизм автоматического добавления виз в документ
    String HAS_AUTO_VISA_LIST_GENERATION = "unimv.hasAutoVisaListGeneration";

    //статусы визирования (по умолчанию null - визирования нет)
    long VISING_RUN_NO_TASK = 1L; // визирование включено, но задания не создаются
    long VISING_RUN_FULL = 2L;    // визирование включено и задания создаются (полный цикл визирования)
}
