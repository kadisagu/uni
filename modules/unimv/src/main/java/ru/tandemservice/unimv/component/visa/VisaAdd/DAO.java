/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unimv.component.visa.VisaAdd;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unimv.ui.PossibleVisaListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (model.getGroupVisaId() != null)
        {
            model.setGroupVising(get(GroupsMemberVising.class, model.getGroupVisaId()));

            List<Visa> toDeleteVisaList = new ArrayList<>();
            List<IPossibleVisa> possibleVisaList = new ArrayList<>();
            for (Visa visa : UnimvDaoFacade.getVisaDao().getVisaList(get(model.getDocumentId())))
                if (visa.getGroupMemberVising().equals(model.getGroupVising()))
                {
                    possibleVisaList.add(visa.getPossibleVisa());
                    toDeleteVisaList.add(visa);
                }

            model.setPossibleVisaList(possibleVisaList);
            model.setToDeleteVisaList(toDeleteVisaList  );
        }

        model.setPossibleVisaListModel(new PossibleVisaListModel(getSession()));

        model.setGroupVisingModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(GroupsMemberVising.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", GroupsMemberVising.P_TITLE, CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", GroupsMemberVising.P_ID, o));

                return new MQListResultBuilder(builder);
            }
        });
    }

    @Override
    public void update(Model model)
    {
        IAbstractDocument document = get(model.getDocumentId());

        if (model.getGroupVisaId() != null)
        {
            for (Visa visa : model.getToDeleteVisaList())
                delete(visa);

            getSession().flush();
        }

        MQBuilder builder = new MQBuilder(Visa.ENTITY_CLASS, "b", new String[]{Visa.P_INDEX});
        builder.add(MQExpression.eq("b", Visa.document().s(), document));
        List<Integer> indexList = new ArrayList<>(builder.<Integer>getResultList(getSession()));


        for (IPossibleVisa possibleVisa : model.getPossibleVisaList())
        {
//            if (UnimvDaoFacade.getVisaDao().getVisa(document, possibleVisa) == null)
//            {
                int i = 0;
                while (indexList.contains(i))
                    i++;

                indexList.add(i);

                Visa visa = new Visa();
                visa.setMandatory(false);
                visa.setPossibleVisa(possibleVisa);
                visa.setDocument(document);
                visa.setIndex(i);
                visa.setGroupMemberVising(model.getGroupVising());
                save(visa);
//            }
        }
    }
}
