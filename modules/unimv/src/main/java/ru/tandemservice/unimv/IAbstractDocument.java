/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.unimv;

import java.util.Date;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author Vasily Zhukov
 * @since 12.10.2010
 */
public interface IAbstractDocument extends IEntity, ITitled
{
    String P_TITLE = "title";            // название документа
    String P_CREATE_DATE = "createDate"; // дата создания
    String L_STATE = "state";            // состояние
    String L_TYPE = "type";              // тип

    Date getCreateDate();                // персистеное свойство

    void setCreateDate(Date createDate);

    ICatalogItem getState();

    void setState(ICatalogItem state);

    ICatalogItem getType();
}
