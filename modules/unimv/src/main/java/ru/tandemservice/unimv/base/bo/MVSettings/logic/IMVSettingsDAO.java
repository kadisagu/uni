/**
 *$Id$
 */
package ru.tandemservice.unimv.base.bo.MVSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * Create by ashaburov
 * Date 24.10.11
 */
public interface IMVSettingsDAO extends INeedPersistenceSupport
{
    /**
     * Удаляет Группу участников визирования.<p>
     * Если элемент является системным (код 01, 02), то его нельзя удалить.
     * @param id идентификатор группы.
     */
    void deleteGroupsMemberVising(Long id);

    /**
     * Поднимает группу в списке на одну строку вверх.
     * @param id идентификатор группы.
     */
    void doUpGroup(Long id);

    /**
     * Опускает группу в списке на одну строку вниз.
     * @param id идентификатор группы.
     */
    void doDownGroup(Long id);
}
