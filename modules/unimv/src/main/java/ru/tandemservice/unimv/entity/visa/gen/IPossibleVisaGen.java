package ru.tandemservice.unimv.entity.visa.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.base.entity.gen.IPersistentPersonableGen;
import ru.tandemservice.unimv.IPossibleVisa;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unimv.IPossibleVisa;

/**
 * Возможная виза
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IPossibleVisaGen extends InterfaceStubBase
 implements IPossibleVisa{
    public static final int VERSION_HASH = -37117018;

    public static final String P_TITLE = "title";
    public static final String L_ENTITY = "entity";
    public static final String P_ACTIVE = "active";

    private String _title;
    private IPersistentPersonable _entity;
    private boolean _active;

    @NotNull
    @Length(max=255)

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    @NotNull

    public IPersistentPersonable getEntity()
    {
        return _entity;
    }

    public void setEntity(IPersistentPersonable entity)
    {
        _entity = entity;
    }

    @NotNull

    public boolean isActive()
    {
        return _active;
    }

    public void setActive(boolean active)
    {
        _active = active;
    }

    private static final Path<IPossibleVisa> _dslPath = new Path<IPossibleVisa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unimv.IPossibleVisa");
    }
            

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     * @see ru.tandemservice.unimv.IPossibleVisa#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Объект, к которому будут привязываться задания для согласования. Свойство не может быть null.
     * @see ru.tandemservice.unimv.IPossibleVisa#getEntity()
     */
    public static IPersistentPersonableGen.Path<IPersistentPersonable> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unimv.IPossibleVisa#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends IPossibleVisa> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private IPersistentPersonableGen.Path<IPersistentPersonable> _entity;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название должности (звание) в документе. Свойство не может быть null.
     * @see ru.tandemservice.unimv.IPossibleVisa#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(IPossibleVisaGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Объект, к которому будут привязываться задания для согласования. Свойство не может быть null.
     * @see ru.tandemservice.unimv.IPossibleVisa#getEntity()
     */
        public IPersistentPersonableGen.Path<IPersistentPersonable> entity()
        {
            if(_entity == null )
                _entity = new IPersistentPersonableGen.Path<IPersistentPersonable>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unimv.IPossibleVisa#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(IPossibleVisaGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return IPossibleVisa.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unimv.IPossibleVisa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
