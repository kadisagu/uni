/* $Id$ */
package ru.tandemservice.unibase.util;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.math.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;

/**
 * @author azhebko
 * @since 16.07.2014
 */
public class SimpleCollectionSelectorTest
{
    @Test
    public void testSelect()
    {
        // Integers
        for (TestCase<Integer> testCase: Arrays.asList(buildEvenIntegerTestCase(), buildOddIntegerTestCase(), buildRandomIntegerTestCase()))
        {
            SimpleCollectionSelector<Integer> evenIntegerCollectionSelector = new SimpleCollectionSelector<>(testCase.getSource(), testCase.getStopCriterion(), testCase.getPredicates());
            Assert.assertEquals(evenIntegerCollectionSelector.select(), testCase.getExpected());
        }

        // Objects
        for (TestCase<Object> testCase: Arrays.asList(buildObjectTestCase()))
        {
            SimpleCollectionSelector<Object> objectSimpleCollectionSelector = new SimpleCollectionSelector<>(testCase.getSource(), testCase.getStopCriterion(), testCase.getPredicates());
            Assert.assertEquals(objectSimpleCollectionSelector.select(), testCase.getExpected());
        }
    }

    @Test
    public void testSelectAdvanced()
    {
        // Тестируем объекты посложнее (см. также EnrEnrollmentOrderPrintUtil#getEnrollmentOrderParagraphTemplate())
        class Item
        {
            private final boolean a;
            private final boolean b;
            private final boolean c;

            public Item(boolean a, boolean b, boolean c)
            {
                this.a = a;
                this.b = b;
                this.c = c;
            }
        }

        Collection<Item> items = Arrays.asList(
//            new Item(false, false, false),
            new Item(false, false, true),
            new Item(false, true, false),
            new Item(false, true, true),
            new Item(true, false, false),
            new Item(true, false, true),
            new Item(true, true, false),
            new Item(true, true, true)
        );

        {
            Predicate<Item> aPredicate = aItem -> aItem.a;
            Predicate<Item> bPredicate = bItem -> bItem.b;
            Predicate<Item> cPredicate = cItem -> cItem.c;

            List<Predicate<? super Item>> predicates = Arrays.<Predicate<? super Item>>asList(aPredicate, bPredicate, cPredicate);
            SimpleCollectionSelector.StopCriterion<Item> stopCriterion = source -> source.size() == 1;

            SimpleCollectionSelector<Item> selector = new SimpleCollectionSelector<>(items, stopCriterion, predicates);

            Assert.assertEquals(selector.select().size(), 1);

            Item result = selector.select().iterator().next();
            Assert.assertTrue(result.a);
            Assert.assertTrue(result.b);
            Assert.assertTrue(result.c);
        }
        {
            Predicate<Item> aPredicate = aItem -> !aItem.a;
            Predicate<Item> bPredicate = bItem -> !bItem.b;
            Predicate<Item> cPredicate = cItem -> !cItem.c;

            List<Predicate<? super Item>> predicates = Arrays.<Predicate<? super Item>>asList(aPredicate, bPredicate, cPredicate);
            SimpleCollectionSelector.StopCriterion<Item> stopCriterion = source -> source.size() == 1;

            SimpleCollectionSelector<Item> selector = new SimpleCollectionSelector<>(items, stopCriterion, predicates);

            Assert.assertEquals(selector.select().size(), 1);

            Item result = selector.select().iterator().next();
            Assert.assertFalse(result.a);
            Assert.assertFalse(result.b);
            Assert.assertTrue(result.c);
        }
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullSource()
    {
        new SimpleCollectionSelector<>(null, source -> true, Collections.<Predicate<? super Object>>emptyList());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullStopCriterion()
    {
        new SimpleCollectionSelector<>(Collections.emptyList(), null, Collections.<Predicate<? super Object>>emptyList());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullPredicates()
    {
        new SimpleCollectionSelector<>(Collections.emptyList(), source -> true, null);
    }

    @Test
    public void testRepeatSelectCall()
    {
        Set<Collection<Integer>> results = new HashSet<>();
        Collection<Integer> source = new ArrayList<>(20);
        for (int i = 0; i < 20; i++)
            source.add(RandomUtils.nextInt(100));

        SimpleCollectionSelector<Integer> selector = new SimpleCollectionSelector<>(source, evenStopCriterion, Collections.singletonList(evenPredicate));

        for (int i = 0; i < 100; i++)
            results.add(selector.select());

        Assert.assertEquals(results.size(), 1);
    }


    private static final Predicate<? super Integer> oddPredicate = i -> i % 2 == 1;
    private static final Predicate<? super Integer> evenPredicate = i -> i % 2 == 0;
    private static final SimpleCollectionSelector.StopCriterion<Integer> evenStopCriterion = source -> {
        for (Integer i: source)
            if (i % 2 == 1) return false;
        return true;
    };


    private static TestCase<Integer> buildEvenIntegerTestCase()
    {
        return new TestCase<Integer>()
        {
            @Override public Collection<Integer> getSource(){ return Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9); }
            @Override public SimpleCollectionSelector.StopCriterion<Integer> getStopCriterion(){ return evenStopCriterion; }
            @Override public Collection<Predicate<? super Integer>> getPredicates(){ return Collections.singletonList((Integer i) -> i % 2 == 0); }
            @Override public Collection<Integer> getExpected(){ return Arrays.asList(0, 2, 4, 6, 8); }
        };
    }

    private static TestCase<Integer> buildOddIntegerTestCase()
    {
        final SimpleCollectionSelector.StopCriterion<Integer> stopCriterion = source -> {
            for (Integer i : source)
                if (i % 2 == 0) return false;
            return true;
        };

        return new TestCase<Integer>()
        {
            @Override public Collection<Integer> getSource(){ return Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9); }
            @Override public Collection<Predicate<? super Integer>> getPredicates(){ return Collections.singletonList((Integer i) -> i % 2 == 1); }
            @Override public Collection<Integer> getExpected(){ return Arrays.asList(1, 3, 5, 7, 9); }
            @Override public SimpleCollectionSelector.StopCriterion<Integer> getStopCriterion(){ return stopCriterion; }
        };
    }

    private static TestCase<Integer> buildRandomIntegerTestCase()
    {
        final Collection<Integer> initialSource = new ArrayList<>(1000);
        for (int i = 0; i < 1000; i++)
            initialSource.add((RandomUtils.nextInt(100) + 1) * 4 + i);
        final SimpleCollectionSelector.StopCriterion<Integer> stopCriterion = source -> source.size() < initialSource.size() / 3;

        return new TestCase<Integer>()
        {
            @Override public Collection<Integer> getSource(){ return initialSource; }
            @Override public SimpleCollectionSelector.StopCriterion<Integer> getStopCriterion(){ return stopCriterion; }
            @Override public Collection<Predicate<? super Integer>> getPredicates(){ return Arrays.<Predicate<? super Integer>>asList(oddPredicate, evenPredicate); }
            @Override public Collection<Integer> getExpected(){ return Collections.emptyList(); }
        };
    }

    private static TestCase<Object> buildObjectTestCase()
    {
        final SimpleCollectionSelector.StopCriterion<Object> stopCriterion = source -> source.size() == 1;

        return new TestCase<Object>()
        {
            @Override public Collection<Object> getSource(){ return Arrays.asList(null, "0", Boolean.FALSE); }
            @Override public SimpleCollectionSelector.StopCriterion<Object> getStopCriterion(){ return stopCriterion; }
            @Override public Collection<Predicate<? super Object>> getPredicates(){ return Collections.singletonList(i -> i instanceof String); }
            @Override public Collection<Object> getExpected(){ return Collections.singletonList("0"); }
        };
    }

    /** Сценарий тестирования. */
    private interface TestCase<T>
    {
        Collection<T> getSource();
        SimpleCollectionSelector.StopCriterion<T> getStopCriterion();
        Collection<Predicate<? super T>> getPredicates();
        Collection<T> getExpected();
    }
}