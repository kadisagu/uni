/**
 *$Id$
 */
package ru.tandemservice.unibase.base.ext.Archive.ui.List;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;

/**
 * @author Alexander Zhebko
 * @since 19.02.2014
 */
@Configuration
public class ArchiveListExtManager extends BusinessComponentExtensionManager
{
}