package ru.tandemservice.unibase.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип архивного объекта"
 * Имя сущности : archiveType
 * Файл data.xml : unibase.catalog.data.xml
 */
public interface ArchiveTypeCodes
{
    /** Константа кода (code) элемента : Учебный план (title) */
    String EDU_PLAN = "eduPlan";
    /** Константа кода (code) элемента : Рабочий план (title) */
    String WORK_PLAN = "workPlan";

    Set<String> CODES = ImmutableSet.of(EDU_PLAN, WORK_PLAN);
}
