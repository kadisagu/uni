/**
 *$Id$
 */
package ru.tandemservice.unibase.base.ext.Archive;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Zhebko
 * @since 19.02.2014
 */
@Configuration
public class ArchiveExtManager extends BusinessObjectExtensionManager
{
}