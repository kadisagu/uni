package ru.tandemservice.unibase.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип атрибута архивного объекта"
 * Имя сущности : archiveAttribute
 * Файл data.xml : unibase.catalog.data.xml
 */
public interface ArchiveAttributeCodes
{
    /** Константа кода (code) элемента : Номер версии УП (title) */
    String EDU_PLAN_NUMBER = "eduPlan_number";
    /** Константа кода (code) элемента : Направление подготовки (специальность) (title) */
    String EDU_PLAN_EDU_LEVEL_HIGH_SCHOOL = "eduPlan_eduLevelHighSchool";
    /** Константа кода (code) элемента : Форма освоения (title) */
    String EDU_PLAN_DEVELOP_FORM = "eduPlan_developForm";
    /** Константа кода (code) элемента : Номер рабочего плана (title) */
    String WORK_PLAN_NUMBER = "workPlan_number";
    /** Константа кода (code) элемента : Направление подготовки (специальность) (title) */
    String WORK_PLAN_EDU_LEVEL_HIGH_SCHOOL = "workPlan_eduLevelHighSchool";
    /** Константа кода (code) элемента : Учебный год (title) */
    String WORK_PLAN_EDU_YEAR = "workPlan_eduYear";
    /** Константа кода (code) элемента : Семестр (title) */
    String WORK_PLAN_TERM = "workPlan_term";

    Set<String> CODES = ImmutableSet.of(EDU_PLAN_NUMBER, EDU_PLAN_EDU_LEVEL_HIGH_SCHOOL, EDU_PLAN_DEVELOP_FORM, WORK_PLAN_NUMBER, WORK_PLAN_EDU_LEVEL_HIGH_SCHOOL, WORK_PLAN_EDU_YEAR, WORK_PLAN_TERM);
}
