/**
 *$Id$
 */
package ru.tandemservice.unibase.remoteDocument.ext.RemoteDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.archive.base.entity.ArchiveItem;
import org.tandemframework.shared.commonbase.remoteDocument.bo.RemoteDocument.RemoteDocumentManager;
import ru.tandemservice.unibase.catalog.entity.codes.ArchiveTypeCodes;
import ru.tandemservice.unibase.remoteDocument.ext.RemoteDocument.util.RemoteDocumentFlexAttributes;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.03.2014
 */
@Configuration
public class RemoteDocumentExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private RemoteDocumentManager _remoteDocumentManager;

    @Bean
    public ItemListExtension<List<String>> flexAttributesOrderExtension()
    {
        String archiveItemName = EntityRuntime.getMeta(ArchiveItem.class).getName();
        return itemListExtension(_remoteDocumentManager.flexAttributesOrderExtPoint())
                .add(archiveItemName + "." + ArchiveTypeCodes.EDU_PLAN, RemoteDocumentFlexAttributes.EDU_PLAN_ATTRIBUTES_ORDER_TEMPLATE)
                .add(archiveItemName + "." + ArchiveTypeCodes.WORK_PLAN, RemoteDocumentFlexAttributes.WORK_PLAN_ATTRIBUTES_ORDER_TEMPLATE)
                .create();
    }
}