/* $Id$ */
package ru.tandemservice.unibase.util;


import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * Селектор подмножества элементов коллекции.
 * Для заданной коллекции перебирает все комбинации применяемых предикатов, пока не будет выполнен критерий остановки.
 * Если критерий не выполнен, возвращается пустая коллекция.
 *
 * @author azhebko
 * @since 16.07.2014
 */
public class SimpleCollectionSelector<T>
{
    private final Collection<T> _source;
    private final List<Predicate<? super T>> _predicates;
    private final StopCriterion<T> _stopCriterion;

    private Collection<T> _result;


    /**
     * Создает селектор для заданной коллекции, предикатов и критерия остановки.
     * @param source коллекция, из которой требуется выбрать элементы
     * @param stopCriterion критерий остановки
     * @param predicates предикаты, применяемые к элементам коллекции
     * @throws java.lang.NullPointerException если коллекция, критерий остановки или предикаты null
     */
    public SimpleCollectionSelector(Collection<T> source, StopCriterion<T> stopCriterion, Collection<Predicate<? super T>> predicates)
    {
        if (source == null || stopCriterion == null || predicates == null)
            throw new NullPointerException();

        _source = Collections.unmodifiableCollection(source);
        _predicates = new ArrayList<>(predicates);
        _stopCriterion = stopCriterion;
    }

    /**
     * Выбирает подмножество элементов исходной коллекции, полученное при помощи применения некоторой комбинации из заданных предикатов, удовлетворяющее критерию остановки.
     * Если критерий не выполняется ни для одной комбинации, возвращает пустую коллекцию.
     * @return подмножество элементов исходной коллекции
     */
    public Collection<T> select()
    {
        if (_result == null)
            _result = select(0, _source);

        return _result;
    }

    private Collection<T> select(int predicateInd, Collection<T> source)
    {
        Collection<T> result = source;
        if (_stopCriterion.isSatisfied(result) || result.isEmpty())
            return result;

        if (predicateInd == _predicates.size())
            return Collections.emptyList();

        result = select(predicateInd + 1, CollectionUtils.select(source, _predicates.get(predicateInd)));

        if (!result.isEmpty())
            return result;

        result = select(predicateInd + 1, source);

        if (!result.isEmpty())
            return result;

        return Collections.emptyList();
    }

    public interface StopCriterion<T>
    {
        boolean isSatisfied(Collection<T> source);
    }
}