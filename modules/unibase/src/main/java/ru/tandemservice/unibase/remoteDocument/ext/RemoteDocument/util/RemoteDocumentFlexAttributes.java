/**
 *$Id$
 */
package ru.tandemservice.unibase.remoteDocument.ext.RemoteDocument.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.03.2014
 */
public interface RemoteDocumentFlexAttributes
{
    // "гибкие" атрибуты УП - уникальные ключи
    public static final String EDU_PLAN_VERSION_NUMBER = "eduPlanVersionNumber";
    public static final String EDU_PLAN_EDU_LEVEL = "eduPlanLevel";
    public static final String EDU_PLAN_QUALIFICATION = "eduPlanQualification";
    public static final String EDU_PLAN_STANDARD = "eduPlanStandard";
    public static final String EDU_PLAN_DEVELOP_FORM = "eduPlanDevelopForm";
    public static final String EDU_PLAN_DEVELOP_CONDITION = "eduPlanDevelopCondition";
    public static final String EDU_PLAN_DEVELOP_TECH = "eduPlanDevelopTech";
    public static final String EDU_PLAN_DEVELOP_PERIOD = "eduPlanDevelopPeriod";
    public static final String EDU_PLAN_STATE = "eduPlanState";
    public static final String EDU_PLAN_COMMENT = "eduPlanComment";

    // "гибкие" атрибуты РП - уникальные ключи
    public static final String WORK_PLAN_NUMBER = "workPlanNumber";
    public static final String WORK_PLAN_EDU_LEVEL = "workPlanEduLevel";
    public static final String WORK_PLAN_EDU_YEAR = "workPlanEduYear";
    public static final String WORK_PLAN_TERM_NUMBER = "workPlanTermNumber";
    public static final String WORK_PLAN_DEVELOP_FORM = "workPlanDevelopForm";
    public static final String WORK_PLAN_DEVELOP_CONDITION = "workPlanDevelopCondition";
    public static final String WORK_PLAN_DEVELOP_TECH = "workPlanDevelopTech";
    public static final String WORK_PLAN_DEVELOP_PERIOD = "workPlanDevelopPeriod";
    public static final String WORK_PLAN_STATE = "workPlanState";
    public static final String WORK_PLAN_EDU_PLAN_VERSION = "workPlanEduPlanVersion";
    public static final String WORK_PLAN_COMMENT = "workPlanComment";

    /** Шаблон сортировки "гибких" атрибутов УП */
    public static final List<String> EDU_PLAN_ATTRIBUTES_ORDER_TEMPLATE =
            Arrays.asList(EDU_PLAN_VERSION_NUMBER, EDU_PLAN_EDU_LEVEL, EDU_PLAN_QUALIFICATION, EDU_PLAN_STANDARD,
                    EDU_PLAN_DEVELOP_FORM, EDU_PLAN_DEVELOP_CONDITION, EDU_PLAN_DEVELOP_TECH, EDU_PLAN_DEVELOP_PERIOD,
                    EDU_PLAN_STATE, EDU_PLAN_COMMENT);

    /** Шаблон сортировки "гибких" атрибутов РП */
    public static final List<String> WORK_PLAN_ATTRIBUTES_ORDER_TEMPLATE =
            Arrays.asList(WORK_PLAN_NUMBER, WORK_PLAN_EDU_LEVEL, WORK_PLAN_EDU_YEAR, WORK_PLAN_TERM_NUMBER,
                    WORK_PLAN_DEVELOP_FORM, WORK_PLAN_DEVELOP_CONDITION, WORK_PLAN_DEVELOP_TECH, WORK_PLAN_DEVELOP_PERIOD,
                    WORK_PLAN_STATE, WORK_PLAN_EDU_PLAN_VERSION, WORK_PLAN_COMMENT);
}