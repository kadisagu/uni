/**
 *$Id$
 */
package ru.tandemservice.unibase.remoteDocument.ext.RemoteDocument.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.shared.commonbase.remoteDocument.bo.RemoteDocument.ui.Pub.RemoteDocumentPub;

/**
 * @author Alexander Zhebko
 * @since 07.03.2014
 */
@Configuration
public class RemoteDocumentExtPub extends BusinessComponentExtensionManager
{
    @Autowired
    RemoteDocumentPub _remoteDocumentPub;
}