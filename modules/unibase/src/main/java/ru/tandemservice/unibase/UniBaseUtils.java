/* $Id: UniBaseUtils.java 29079 2013-08-01 08:16:23Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unibase;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.validator.BaseValidator;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.dbsupport.ddl.IDataSource;
import org.tandemframework.dbsupport.ddl.connect.DBConnect;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.*;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author lefay
 */
public class UniBaseUtils extends CommonBaseUtil
{
    public static boolean eq(final double d1, final double d2)
    {
        return (Math.abs(d1 - d2) < 0.0001d);
    }

    public static int nullToZero(Integer value)
    {
        return (null == value) ? 0 : value;
    }

    public static double nullToZero(Double value)
    {
        return (null == value) ? 0 : value;
    }


    public static Integer safeSum(Integer i1, Integer i2)
    {
        return (i1 == null) ? i2 : (i2 == null) ? i1 : Integer.valueOf(i1 + i2);
    }

    /**
     * Use PersonManager.instance().dao().checkPersonUnique(person);
     *
     * @param session session
     * @param person  person
     * @deprecated Use PersonManager.instance().dao().checkPersonUnique(person);
     */
    @Deprecated
    public static void checkPersonUnique(Session session, Person person)
    {
        PersonManager.instance().dao().checkPersonUnique(person);
    }

    public static <T extends IEntity, MODEL> DynamicListDataSource<T> createDataSource(IBusinessComponent component, final IListDataSourceDao<MODEL> dao)
    {
        return new DynamicListDataSource<>(component, component1 -> {
            dao.prepareListDataSource(component1.<MODEL>getModel());
        });
    }

    public static boolean checkUnique(Session session, String entityClass, String propertyName, IEntity entity)
    {
        final DQLSelectBuilder existsDql = new DQLSelectBuilder()
                .fromEntity(entityClass, "entity")
                .top(1).column("entity.id")
                .where(DQLExpressions.eq(property("entity", propertyName), commonValue(entity.getProperty(propertyName))));

        if (entity.getId() != null)
        {
            existsDql.where(DQLExpressions.ne(property("entity.id"), value(entity.getId())));
        }

        return existsDql.createStatement(session).uniqueResult() == null;
    }

    public static String getOrgUnitClassName(OrgUnitType orgUnitType)
    {
        return "ru.tandemservice.uni.entity.orgstruct." + StringUtils.capitalize(orgUnitType.getCode());
    }

    public static String generateNextStringNumber(Session session, String clazz, String property)
    {
        return Integer.toString(generateNextNumber(session, clazz, property, true));
    }

    public static Integer generateNextIntegerNumber(Session session, String clazz, String property)
    {
        return generateNextNumber(session, clazz, property, false);
    }

    private static Integer generateNextNumber(Session session, String clazz, String property, boolean string)
    {
        Number countNumber = (Number) session.createCriteria(clazz).setProjection(Projections.count("id")).uniqueResult();
        int count = countNumber == null ? 0 : countNumber.intValue();

        while (true)
        {
            count++;
            countNumber = (Number) session.createCriteria(clazz).setProjection(Projections.count("id")).add(Restrictions.eq(property, (string) ? Integer.toString(count) : count)).uniqueResult();
            if (countNumber == null || countNumber.intValue() == 0)
                return count;
        }
    }

    public static void createFullPage(DynamicListDataSource<? extends IEntity> dataSource, MQBuilder builder, Session session)
    {
        CommonBaseSearchListUtil.createFullPage(dataSource, builder, session);
    }

    public static <T extends IEntity> void createFullPage(DynamicListDataSource<T> dataSource, List<T> entities)
    {
        CommonBaseSearchListUtil.createFullPage(dataSource, entities);
    }

   public static void createPage(DynamicListDataSource<? extends IEntity> dataSource, MQBuilder builder, Session session)
    {
        CommonBaseSearchListUtil.createPage(dataSource, builder, session);
    }

    public static void createPage(DynamicListDataSource<? extends IEntity> dataSource, DQLSelectBuilder dql, Session session)
    {
        CommonBaseSearchListUtil.createPage(dataSource, dql, session);
    }


    public static <T extends IEntity> void createPage(DynamicListDataSource<T> dataSource, List<T> entities)
    {
        CommonBaseSearchListUtil.createPage(dataSource, entities);
    }

    /**
     * @deprecated don't use hql
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    public static void createPage(DynamicListDataSource<? extends IEntity> dataSource, String hql, String order, Map<String, Object> paramsMap, Session session)
    {
        CommonBaseSearchListUtil.createPage(dataSource, hql, order, paramsMap);
    }


    public static <T extends IEntity> void createPageOrderedByNumberAsString(final Class<T> entityClass, DynamicListDataSource<T> dataSource, DQLSelectBuilder dql, String alias, final MetaDSLPath<String> numberPropertyPath)
    {
        // добавляются колонка с id и колонка со значениями, грузим все такие пары из базы
        dql.column(property(alias, "id"));
        dql.column(property(numberPropertyPath.fromAlias(alias)));


        // формируем компаратор
        final boolean direct = OrderDirection.asc.equals(dataSource.getEntityOrder().getDirection());
        final Comparator<Object[]> comparator = direct ? UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_DIRECT : UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_INDIRECT;

        // грузим список id, отсортированных по компаратору
        final IUniBaseDao dao = IUniBaseDao.instance.get();
        final List<Long> ids = dao.getPageIdsOrderedByComparator(dql, comparator);
        final int count = ids.size();

        // выставляем в searchList число элементов в списке (без этого нельзя получить номер страницы)
        dataSource.setTotalSize(count);
        int startRow = (int)(dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow());
        int countRow = (int)(dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow());

        // берем подмножество
        final List<Long> subIds = ids.subList(startRow, Math.min(startRow + countRow, count));
        dataSource.createPage(CommonDAO.sort(dao.getList(entityClass, "id", subIds), subIds));
    }

    /**
     * use PersonManager.instance().dao().checkIdentityCard
     */
    @Deprecated
    public static void check(IdentityCard identityCard, ErrorCollector errors) throws ApplicationException
    {
        checkSeria(identityCard, errors);
        checkNumber(identityCard, errors);
        checkIssuanceDate(identityCard, errors);

        identityCard.setLastName(StringUtils.capitalize(identityCard.getLastName()));
        identityCard.setFirstName(StringUtils.capitalize(identityCard.getFirstName()));
        identityCard.setMiddleName(StringUtils.capitalize(identityCard.getMiddleName()));
    }

    private static void checkSeria(IdentityCard identityCard, ErrorCollector errors) throws ApplicationException
    {
        IdentityCardType cardType = identityCard.getCardType();
        if (identityCard.getSeria() != null)
        {
            int seriaLength = identityCard.getSeria().length();
            if (seriaLength < cardType.getMinSeriaLength() || seriaLength > cardType.getMaxSeriaLength())
            {
                errors.add("Серия должна быть от " + cardType.getMinSeriaLength() + " до " + cardType.getMaxSeriaLength() + " символов.", "icSeria0", "icSeria1", "icSeria2", "icSeria3");
            }
        }
    }

    private static void checkNumber(IdentityCard identityCard, ErrorCollector errors) throws ApplicationException
    {
        if (identityCard.getNumber() != null)
        {
            IdentityCardType cardType = identityCard.getCardType();
            int numberLength = identityCard.getNumber().length();
            if (numberLength < cardType.getMinNumberLength() || numberLength > cardType.getMaxNumberLength())
            {
                errors.add("Номер должен быть от " + cardType.getMinNumberLength() + " до " + cardType.getMaxNumberLength() + " символов.", "icNumber0", "icNumber1");
            }
        }
    }

    private static void checkIssuanceDate(IdentityCard identityCard, ErrorCollector errors) throws ApplicationException
    {
        if (identityCard.getIssuanceDate() != null && identityCard.getIssuanceDate().after(new Date()))
        {
            errors.add("Дата выдачи удостоверения личности должна быть не позже сегодняшней.", "issuanceDate");
        }
    }

    public static int getCurrentYear()
    {
        return CommonBaseUtil.getCurrentYear();
    }

    public static IDataSettings getDataSettings(String settingsKey)
    {
        return DataSettingsFacade.getSettings(UserContext.getInstance().getPrincipal().getId().toString(), settingsKey);
    }

    public static IDataSettings getDataSettings(IBusinessComponent component, String settingsKey)
    {
        return DataSettingsFacade.getSettings(component.getUserContext().getPrincipal().getId().toString(), settingsKey);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public static DBConnect createDDLTool(Session session) throws Exception
    {
        IDataSource ds = (IDataSource) ApplicationRuntime.getBean("mainDataSource");
        return new DBConnect(ds, session.connection());
    }

    /**
     * @param number число от ноля до тысячи
     * @return запись этого числа словами
	 * @deprecated Используйте {@link NumberSpellingUtil#spellNumberMasculineGender(long)} и {@link NumberSpellingUtil#spellNumberFeminineGender(long)}.
	 * Эти методы работают корректно и поддерживают весь диапазон значений типа Long.
     */
	@Deprecated
    public static String frame(int number)
    {
		return NumberSpellingUtil.spellNumberMasculineGender(number);
    }

    /**
     * @param entities     коллекция entity'ей
     * @param propertyPath путь до property
     * @return список из соответствующих property'ей этих entity'ей
     * @deprecated use {@link org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil#getPropertiesList(java.util.Collection, Object)} }
     */
    public static <U> List<U> getPropertiesList(Collection<? extends IEntity> entities, Object propertyPath)
    {
        return CommonBaseEntityUtil.getPropertiesList(entities, propertyPath);
    }

    /**
     * @param entities     коллекция entity'ей
     * @param propertyPath путь до property
     * @return сет из соответствующих property'ей этих entity'ей
     */
    public static <U> Set<U> getPropertiesSet(Collection<? extends IEntity> entities, Object propertyPath)
    {
        return CommonBaseEntityUtil.getPropertiesSet(entities, propertyPath);
    }

    /**
     * @param entities коллекция entity'ей
     * @return список id этих entity'ей
     */
    public static List<Long> getIdList(Collection<? extends IEntity> entities)
    {
        return CommonBaseEntityUtil.getIdList(entities);
    }

    /**
     * @param list список
     * @return последний элемент списка, если тот не пуст, иначе null
     */
    public static <T> T getLast(List<T> list)
    {
        return (!list.isEmpty()) ? list.get(list.size() - 1) : null;
    }

    public static String toString(Object object)
    {
        return (object != null) ? object.toString() : null;
    }


    @SuppressWarnings("unchecked")
    public static void changePriority(Class<?> clazz, Long entityId, String priorityProperty, boolean up, Session session)
    {
        Criteria criteria = session.createCriteria(clazz);
        criteria.addOrder(Order.asc(priorityProperty));
        changePriority(entityId, criteria.list(), priorityProperty, up, session);
    }

    /**
     * Use CommonManager.instance().commonPriorityDao()
     */
    @Deprecated
    public static void changePriority(Long entityId, List<IEntity> entities, String priorityProperty, boolean up, Session session)
    {
        int entityIndex = 0;

        while (entityIndex < entities.size() && !(entities.get(entityIndex).getId().equals(entityId)))
        {
            entityIndex++;
        }

        if (entityIndex == entities.size())
        {
            throw new RuntimeException("Unknown entity with id: " + entityId);
        }

        int pairIndex = entityIndex + ((up) ? -1 : 1); // индекс пары, с которой надо обменяться приоритетами
        if (pairIndex >= 0 && pairIndex < entities.size())
        {
            IEntity one = entities.get(pairIndex);
            IEntity two = entities.get(entityIndex);

            int onePriority = (Integer) one.getProperty(priorityProperty);
            int twoPriority = (Integer) two.getProperty(priorityProperty);

            one.setProperty(priorityProperty, Integer.MAX_VALUE);
            session.update(one);
            session.flush();

            two.setProperty(priorityProperty, onePriority);
            session.update(two);
            session.flush();

            one.setProperty(priorityProperty, twoPriority);
            session.update(one);
        }
    }

    /**
     * @param o1              объект, который сравниваем
     * @param o2              объект, с которым сравниваем
     * @param nullValuesFirst true если null должны идти в начале, false в противном случае
     * @return результат сравнения
     */
    public static <T extends Comparable<T>> int compare(T o1, T o2, boolean nullValuesFirst)
    {
        if (o1 == null)
        {
            return (o2 == null) ? 0 : (nullValuesFirst) ? -1 : 1;
        } else if (o2 == null)
        {
            return (nullValuesFirst) ? 1 : -1;
        } else
        {
            return o1.compareTo(o2);
        }
    }

    /**
     * Проверяет, имеют ли отрезки общую точку (границы, учитываются)
     *
     * @param left1  левая граница первого отрезка
     * @param right1 правая граница первого отрезка
     * @param left2  левая граница второго отрезка
     * @param right2 правая граница второго отрезка
     * @return true, если отрезки имеют общую точку, false в противном случае
     */
    public static <T> boolean isIntersectedSegments(Comparable<T> left1, T right1, Comparable<T> left2, T right2)
    {
        if (left1.compareTo(right1) > 0 || left2.compareTo(right2) > 0)
        {
            throw new IllegalArgumentException();
        }

        return (left2.compareTo(right1) <= 0 && left1.compareTo(right2) <= 0);
    }

    /**
     * @param identityCardType тип удостоверения личности
     * @return список валидаторов серии удостоверения
     * @deprecated use IdentityCardType.getSeriaValidators()
     */
    @Deprecated
    public static List<BaseValidator> getIdentityCardSeriaValidators(IdentityCardType identityCardType) {
        return identityCardType.getSeriaValidators();
    }

    /**
     * @param identityCardType тип удостоверения личности
     * @return список валидаторов номера удостоверения
     * @deprecated use IdentityCardType.getNumberValidators()
     */
    @Deprecated
    public static List<BaseValidator> getIdentityCardNumberValidators(IdentityCardType identityCardType) {
        return identityCardType.getNumberValidators();
    }

    /**
     * @param entity сущность (может быть null)
     * @return hash-code
     */
    public static int hashCode(IEntity entity)
    {
        return ObjectUtils.hashCode(entity);
    }

    /**
     * @param e1 сущность (может быть null)
     * @param e2 сущность (может быть null)
     * @return equals с учетом равенства null
     */
    public static boolean equals(IEntity e1, IEntity e2)
    {
        return ObjectUtils.equals(e1, e2);
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] get(T[] array, int[] indices)
    {
        T[] result = (T[]) new Object[indices.length];
        for (int i = 0, j = 0; i < indices.length; i++, j++)
        {
            result[j] = array[indices[i]];
        }
        return result;
    }

    public static abstract class KeyBase<T>
    {
        public abstract T key();

        @Override
        public int hashCode()
        {
            return key().hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj == this)
            {
                return true;
            }
            if (obj instanceof KeyBase)
            {
                return this.key().equals(((KeyBase) obj).key());
            }
            return false;
        }
    }

    public static void validateDatesPeriod(IDataSettings settings, String dateFromFieldId, String dateToFieldId)
    {
        Date dateFrom = (Date) settings.get(dateFromFieldId);
        Date dateTo = (Date) settings.get(dateToFieldId);

        if (null != dateFrom && null != dateTo && dateFrom.after(dateTo))
            UserContext.getInstance().getErrorCollector().add("Дата начала периода должна быть раньше даты его окончания.", dateFromFieldId, dateToFieldId);
    }

    @SuppressWarnings("unchecked")
    public static ISelectModel getTitleSelectModel(final SingleValueCache<List<String>> titles) {

        return new FullCheckSelectModel("", "") {
            @Override public Object getPrimaryKey(final Object value) { return value; }
            @Override public String getFullLabel(final Object value) { return String.valueOf(value); }
            @Override public String getLabelFor(final Object value, final int columnIndex) { return this.getFullLabel(value); }

            @Override public ListResult findValues(final String filter) {
                final List selected = new ArrayList(this.list(StringUtils.trimToNull(filter)));
                return new ListResult(selected, selected.size());
            }

            private Set list(final String filter) {
                final Set set = new LinkedHashSet(CollectionUtils.collect(titles.get(), input -> {
                    return StringUtils.trimToEmpty((String)input);
                }));
                if (null != filter) {
                    final String f = StringUtils.trimToEmpty(filter);
                    CollectionUtils.filter(set, object -> StringUtils.startsWithIgnoreCase((String)object, f));
                }
                return set;
            }
        };
    }
}
