/* $Id: CoreDao.java 27784 2013-05-30 09:25:08Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Базовый DAO
 *
 * @author vip_delete
 */
public class UniBaseDao extends SharedBaseDao implements IUniBaseDao
{

    // TODO: FIXME: перенести в CommonDao
    // XXX: увы, это не перенести (catalogItem не в том же модуле, что и CommonDAO)
    // XXX: еще одно доказательство того, что в платформе с модулями полный бардак

    // TODO: FIXME: перенести в CommonDao


    /**
     * обновляем все нафиг (позволяет обновить модель и все данные в ней при очистке сессии )
     * * DANGEROUS очень опасно, но иногда необходимо
     * * DANGEROUS не дописано
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    protected void reloadModelEntities(final Object model)
    {
        try
        {
            final Session s = this.getSession();
            Class<?> klass = model.getClass();
            while (null != klass)
            {
                final Field[] fields = klass.getDeclaredFields();
                for (final Field f : fields)
                {
                    try
                    {
                        if (EntityBase.class.isAssignableFrom(f.getType()))
                        {
                            f.setAccessible(true);
                            Object value = f.get(model);
                            value = this.reassociateEntity(s, (EntityBase) value);
                            f.set(model, value);

                        } else if (Collection.class.isAssignableFrom(f.getType()))
                        {
                            f.setAccessible(true);
                            final Object value = f.get(model);
                            if (value instanceof List)
                            {
                                this.reassociateListEntities(s, (List) value);
                            } else
                            {
                                // надо думать
                            }

                        }

                    } catch (final Throwable t)
                    {
                        // do nothing (i just try to repair model)
                        this.logger.warn(t);
                    }
                }
                klass = klass.getSuperclass();
            }
        } catch (final Throwable t)
        {
            // do nothing
            this.logger.warn(t);
        }
    }

    @SuppressWarnings("unchecked")
    @Deprecated
    protected void reassociateListEntities(final Session session, final List value)
    {
        final ListIterator iterator = value.listIterator();
        while (iterator.hasNext())
        {
            final Object v = iterator.next();
            if (v instanceof EntityBase)
            {
                iterator.set(this.reassociateEntity(session, (EntityBase) v));
            }
        }
    }

    /**
     * обновляет состояние объекта, если сессия упала
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    protected <T extends EntityBase> T reassociateEntity(final Session session, final T entity)
    {
        if (null == entity)
        {
            return null;
        }
        if (session.contains(entity))
        {
            return entity;
        }

        final Class realClass = ClassUtils.getUserClass(entity);
        if (null != entity.getId())
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entity.getId());
            if ((null != meta) && realClass.isAssignableFrom(meta.getEntityClass()))
            {
                if (Hibernate.isInitialized(entity))
                {
                    return (T) session.get(meta.getEntityClass(), entity.getId());
                }

                return (T) session.load(meta.getEntityClass(), entity.getId());
            }
            // ? WTF ? //
            return entity;
        }

        // ну и теперь, кто выжил подвергается пыткам по изменению его свойств
        this.reassociateEntityProperties(session, entity);
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Deprecated
    protected <T extends EntityBase> void reassociateEntityProperties(final Session session, final T entity)
    {
        final Class realClass = ClassUtils.getUserClass(entity);
        final IEntityMeta meta = EntityRuntime.getMeta(realClass);
        if (null != meta)
        {
            for (final String name : meta.getPropertyNames())
            {
                final Object value = entity.getProperty(name);
                if (value instanceof EntityBase)
                {
                    entity.setProperty(name, this.reassociateEntity(session, (EntityBase) value));
                }
            }
        }
    }


    @Override
    @SuppressWarnings("deprecation")
    public TopOrgUnit getAcademy()
    {
        return TopOrgUnit.getInstance();
    }


    @Deprecated
    public void clear() { this.flushClearAndRefresh(); }


    /** обновляет сессию, требует обновления вызывающего его бизнес-компонента */
    public void flushClearAndRefresh()
    {
        Session session = this.getSession();
        session.flush(); // сначала сохраняем все, что есть в сессии (иначе данные потеряются)
        session.clear(); // затем обнуляем сессию
        try { UserContext.getInstance().getDesktop().setRefreshScheduled(true); }
        catch (final NullPointerException e) { /* do nothing */ }
    }

    protected void infoActivity(final char c)
    {
        if (this.logger.isInfoEnabled())
        {
            System.out.print(c);
        }
    }


    ///////////////////////////////////////////////////////
    // utils (перетащить в SANDBOX)
    ///////////////////////////////////////////////////////


    // если есть errorCollector
    // TODO: перетащить в sandbox
    protected void applicationException(String errorMessage)
    {

        if (TransactionSynchronizationManager.isActualTransactionActive()) {
            if (ContextLocal.isRun()) {
                try {
                    ContextLocal.getErrorCollector().add(errorMessage);
                    return;
                } catch (Throwable t) {
                    logger.warn(t.getMessage(), t);
                }
            }
        }

        throw new ApplicationException(errorMessage);

    }

}
