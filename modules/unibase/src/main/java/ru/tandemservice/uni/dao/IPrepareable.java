package ru.tandemservice.uni.dao;

/**
 * @author vdanilov
 */
public interface IPrepareable<Model> {
	void prepare(Model model);
}
