package ru.tandemservice.uni.dao;

import org.tandemframework.core.component.IBusinessComponent;


public interface IListDataSourceDao<Model> extends IPrepareable<Model> {

    public abstract void prepareListDataSource(Model model);

    void deleteRow(IBusinessComponent component);
}
