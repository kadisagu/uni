/* $Id: ICoreDao.java 24699 2012-10-29 08:05:28Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

/**
 * Базовый DAO для абстрагирования Services от hibernate Session
 *
 * @author vip_delete
 */
public interface IUniBaseDao extends ISharedBaseDao
{
    final SpringBeanCache<IUniBaseDao> instance = new SpringBeanCache<IUniBaseDao>("coreDao");

    /**
     * @return корневое подразделение
     * @deprecated используйте TopOrgUnit.getInstance();
     */
    @Deprecated
    TopOrgUnit getAcademy();
}
