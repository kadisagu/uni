package ru.tandemservice.uni.dao;

import org.tandemframework.core.info.ErrorCollector;

/**
 * @author vdanilov
 */
public interface IUpdateable<Model> extends IPrepareable<Model> {

    void update(Model model);

    void validate(Model model, ErrorCollector errors);

}
