// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleContractMassAdd;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.DormitoryBenefit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractType;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 15.11.2010
 */
public class Model
{
    private Long startNumber;
    private Date regDate;
    private Date checkinDate;
    private Date checkoutDate;
    private UnisettleContractType type;

    private ISelectModel сontractTypeList;
    private ISelectModel sexList;
    private ISelectModel countryList;
    private ISelectModel formativeOrgUnitList;
    private ISelectModel courseListModel;
    private ISelectModel developPeriodListModel;
    private ISelectModel benefitList;
    private ISelectModel buildingList;
    private ISelectModel unitList;
    private ISelectModel floorList;
    private ISelectModel placeList;

    private Sex sex;
    private AddressCountry citizenship;
    private OrgUnit formativeOrgUnit;
    private List<Course> courseList;
    private List<DevelopPeriod> developPeriodList;
    private DormitoryBenefit benefit;
    private UniplacesBuilding building;
    private UniplacesUnit unit;
    private UniplacesFloor floor;
    private UniplacesHabitablePlace place;

    private DynamicListDataSource<LineWrapper> dataSource;

    protected static class LineWrapper extends IdentifiableWrapper<Student> implements Comparable
    {
        private static final long serialVersionUID = 1L;
        private UnisettleResident resident;
        private List<Student> students = new ArrayList<>();
        private UniplacesHabitablePlace place;
        private Date requestDate;

        private static final UnisettleResident.Path _residentPath = new UnisettleResident.Path("resident");
        public static UnisettleResident.Path resident() { return _residentPath; }
        private static final UniplacesHabitablePlace.Path _placePath = new UniplacesHabitablePlace.Path("place");
        public static UniplacesHabitablePlace.Path place() { return _placePath; }

        public LineWrapper(UnisettleResident resident)
        {
            super(resident.getId(), "");
            this.resident = resident;
        }

        public void addStudent(Student student)
        {
            if (!students.contains(student))
                students.add(student);
        }

        public List<Course> getCourses()
        {
            return CommonBaseUtil.<Course>getPropertiesList(students, Student.course());
        }

        public List<Group> getGroups()
        {
            return CommonBaseUtil.<Group>getPropertiesList(students, Student.group());
        }

        public String getBookNumbers()
        {
            return UniStringUtils.joinUnique(students, Student.P_BOOK_NUMBER, ",");
        }

        public boolean isDisabled()
        {
            return null == place;  
        }

        public UnisettleResident getResident()
        {
            return resident;
        }

        public List<Student> getStudents()
        {
            return students;
        }

        public UniplacesHabitablePlace getPlace()
        {
            return place;
        }

        public void setPlace(UniplacesHabitablePlace place)
        {
            this.place = place;
        }

        public Date getRequestDate()
        {
            return requestDate;
        }

        public void setRequestDate(Date requestDate)
        {
            this.requestDate = requestDate;
        }

        public void processRequest(UnisettleRequest request)
        {
            if (requestDate == null || requestDate.after(request.getRegDate()))
                requestDate = request.getRegDate();
        }

        @Override
        public int compareTo(Object o)
        {
            LineWrapper other = (LineWrapper) o;
            if (null != getRequestDate() && null != other.getRequestDate())
                return getRequestDate().compareTo(other.getRequestDate());
            if (null != getRequestDate())
                return -1;
            if (null != other.getRequestDate())
                return 1;
            return Person.FULL_FIO_AND_ID_COMPARATOR.compare(getResident().getPerson(), other.getResident().getPerson());
        }
    }

    public void clear()
    {
        setCitizenship(null);
        setFormativeOrgUnit(null);
        setCourseList(null);
        setDevelopPeriodList(null);
        setBenefit(null);
        setBuilding(null);
        setUnit(null);
        setFloor(null);
        setPlace(null);
    }

    public Long getStartNumber()
    {
        return startNumber;
    }

    public void setStartNumber(Long startNumber)
    {
        this.startNumber = startNumber;
    }

    public Date getRegDate()
    {
        return regDate;
    }

    public void setRegDate(Date regDate)
    {
        this.regDate = regDate;
    }

    public Date getCheckinDate()
    {
        return checkinDate;
    }

    public void setCheckinDate(Date checkinDate)
    {
        this.checkinDate = checkinDate;
    }

    public Date getCheckoutDate()
    {
        return checkoutDate;
    }

    public void setCheckoutDate(Date checkoutDate)
    {
        this.checkoutDate = checkoutDate;
    }

    public UnisettleContractType getType()
    {
        return type;
    }

    public void setType(UnisettleContractType type)
    {
        this.type = type;
    }

    public ISelectModel getСontractTypeList()
    {
        return сontractTypeList;
    }

    public void setСontractTypeList(ISelectModel сontractTypeList)
    {
        this.сontractTypeList = сontractTypeList;
    }

    public ISelectModel getSexList()
    {
        return sexList;
    }

    public void setSexList(ISelectModel sexList)
    {
        this.sexList = sexList;
    }

    public ISelectModel getCountryList()
    {
        return countryList;
    }

    public void setCountryList(ISelectModel countryList)
    {
        this.countryList = countryList;
    }

    public ISelectModel getFormativeOrgUnitList()
    {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(ISelectModel formativeOrgUnitList)
    {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public ISelectModel getCourseListModel()
    {
        return courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel)
    {
        this.courseListModel = courseListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        this.developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getBenefitList()
    {
        return benefitList;
    }

    public void setBenefitList(ISelectModel benefitList)
    {
        this.benefitList = benefitList;
    }

    public ISelectModel getBuildingList()
    {
        return buildingList;
    }

    public void setBuildingList(ISelectModel buildingList)
    {
        this.buildingList = buildingList;
    }

    public ISelectModel getUnitList()
    {
        return unitList;
    }

    public void setUnitList(ISelectModel unitList)
    {
        this.unitList = unitList;
    }

    public ISelectModel getFloorList()
    {
        return floorList;
    }

    public void setFloorList(ISelectModel floorList)
    {
        this.floorList = floorList;
    }

    public ISelectModel getPlaceList()
    {
        return placeList;
    }

    public void setPlaceList(ISelectModel placeList)
    {
        this.placeList = placeList;
    }

    public Sex getSex()
    {
        return sex;
    }

    public void setSex(Sex sex)
    {
        this.sex = sex;
    }

    public AddressCountry getCitizenship()
    {
        return citizenship;
    }

    public void setCitizenship(AddressCountry citizenship)
    {
        this.citizenship = citizenship;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public List<Course> getCourseList()
    {
        return courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        this.courseList = courseList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        this.developPeriodList = developPeriodList;
    }

    public DormitoryBenefit getBenefit()
    {
        return benefit;
    }

    public void setBenefit(DormitoryBenefit benefit)
    {
        this.benefit = benefit;
    }

    public UniplacesBuilding getBuilding()
    {
        return building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        this.building = building;
    }

    public UniplacesUnit getUnit()
    {
        return unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        this.unit = unit;
    }

    public UniplacesFloor getFloor()
    {
        return floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        this.floor = floor;
    }

    public UniplacesHabitablePlace getPlace()
    {
        return place;
    }

    public void setPlace(UniplacesHabitablePlace place)
    {
        this.place = place;
    }

    public DynamicListDataSource<LineWrapper> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<LineWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }
}
