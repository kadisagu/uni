// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractResidentAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;

/**
 * @author oleyba
 * @since 10.11.2010
 */
@Input({@Bind(key = "contractId", binding = "contractId"),
        @Bind(key = "residentId", binding = "resident.id"),
        @Bind(key = "baseResidentId", binding = "baseResidentId")})

public class Model
{
    private Long contractId;
    private Long baseResidentId;
    private UnisettleContract contract;
    private UnisettleContractResident resident = new UnisettleContractResident();

    private ISelectModel residentList;
    private ISelectModel relationDegreeList;

    private boolean editForm;
    private boolean showAll;
    private boolean showArea;
    private boolean showBedNumber;
    private boolean showRelationDegree;
    private boolean residentDisabled;

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }

    public Long getBaseResidentId()
    {
        return baseResidentId;
    }

    public void setBaseResidentId(Long baseResidentId)
    {
        this.baseResidentId = baseResidentId;
    }

    public UnisettleContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleContract contract)
    {
        this.contract = contract;
    }

    public UnisettleContractResident getResident()
    {
        return resident;
    }

    public void setResident(UnisettleContractResident resident)
    {
        this.resident = resident;
    }

    public ISelectModel getResidentList()
    {
        return residentList;
    }

    public void setResidentList(ISelectModel residentList)
    {
        this.residentList = residentList;
    }

    public ISelectModel getRelationDegreeList()
    {
        return relationDegreeList;
    }

    public void setRelationDegreeList(ISelectModel relationDegreeList)
    {
        this.relationDegreeList = relationDegreeList;
    }

    public boolean isEditForm()
    {
        return editForm;
    }

    public void setEditForm(boolean editForm)
    {
        this.editForm = editForm;
    }

    public boolean isShowAll()
    {
        return showAll;
    }

    public void setShowAll(boolean showAll)
    {
        this.showAll = showAll;
    }

    public boolean isShowArea()
    {
        return showArea;
    }

    public void setShowArea(boolean showArea)
    {
        this.showArea = showArea;
    }

    public boolean isShowBedNumber()
    {
        return showBedNumber;
    }

    public void setShowBedNumber(boolean showBedNumber)
    {
        this.showBedNumber = showBedNumber;
    }

    public boolean isShowRelationDegree()
    {
        return showRelationDegree;
    }

    public void setShowRelationDegree(boolean showRelationDegree)
    {
        this.showRelationDegree = showRelationDegree;
    }

    public boolean isResidentDisabled()
    {
        return residentDisabled;
    }

    public void setResidentDisabled(boolean residentDisabled)
    {
        this.residentDisabled = residentDisabled;
    }
}
