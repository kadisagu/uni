/* $Id:$ */
package ru.tandemservice.unisettle.base.ext.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unisettle", () -> Arrays.asList(
                "Число поселяемых: " + IUniBaseDao.instance.get().getCount(UnisettleResident.class),
                "Число поселяемых, являющихся активным студентом: " + IUniBaseDao.instance.get().getCount(new DQLSelectBuilder().fromEntity(UnisettleResident.class, "a")
                                .joinEntity("a", DQLJoinType.inner, Student.class, "s", eq(property("s", Student.person().id()), property("a", UnisettleResident.person().id())))
                                .where(eq(property("s", Student.status().active()), value(true)))
                                .column(property("a.id"))
                                .distinct()
                ),
                "Число договоров на поселение: " + IUniBaseDao.instance.get().getCount(UnisettleContract.class)
            ))
            .create();
    }
}
