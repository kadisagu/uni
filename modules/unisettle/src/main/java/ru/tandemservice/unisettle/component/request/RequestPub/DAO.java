// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.RequestPub;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;

import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.catalog.UnisettleRequestState;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setRequest(get(UnisettleRequest.class, model.getRequest().getId()));   
    }

    @Override
    public void doCancelRequest(Model model)
    {
        Session session = getSession();
        final UnisettleRequest request = model.getRequest();
        Person person = model.getRequest().getResident().getPerson();

        NamedSyncInTransactionCheckLocker.register(session, person.getId() + ".unisettleRequest");

        if (!request.getState().getCode().equals(UnisettleDefines.REQUEST_STATE_REGISTERED))
            throw new ApplicationException("Невозможно отменить заявление, так как оно находится в состоянии «"+request.getState().getTitle()+"».");

        request.setState(get(UnisettleRequestState.class, "code", UnisettleDefines.REQUEST_STATE_ARCHIVE));
    }
}
