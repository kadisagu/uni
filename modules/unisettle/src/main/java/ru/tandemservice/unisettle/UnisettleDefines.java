// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle;

import java.util.Arrays;
import java.util.List;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public interface UnisettleDefines
{
    String MODULE_SETTINGS_KEY = "unisettle.";
    String SETTINGS_PERSON_ROLE_SETTLEMENT = "personRoleSettlement";    

    String REQUEST_STATE_REGISTERED = "1";
    String REQUEST_STATE_ACCEPTED = "2";
    String REQUEST_STATE_DECLINED = "3";
    String REQUEST_STATE_ARCHIVE = "4";

    String CONTRACT_STATE_ARCHIVE = "1"; // Архив
    String CONTRACT_STATE_ACCEPTABLE = "2"; // На согласовании
    String CONTRACT_STATE_REJECTED = "3"; // Отклонено
    String CONTRACT_STATE_ACCEPTED = "4"; // Согласовано
    String CONTRACT_STATE_FORMING = "5"; // Формируется

    List<String> CONTRACT_STATES_ACTIVE = Arrays.asList(CONTRACT_STATE_FORMING, CONTRACT_STATE_ACCEPTABLE, CONTRACT_STATE_ACCEPTED);
    List<String> CONTRACT_STATES_NOT_ACTIVE = Arrays.asList(CONTRACT_STATE_ARCHIVE, CONTRACT_STATE_REJECTED);

    String CONTRACT_TYPE_REGISTRATION_AND_SETTLE = "1"; // Поселение и прописка
    String CONTRACT_TYPE_REGISTRATION_ONLY = "2"; // Прописка
}
