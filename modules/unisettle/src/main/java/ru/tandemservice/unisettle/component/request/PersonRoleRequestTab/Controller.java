// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.PersonRoleRequestTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UnisettleRequest> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Номер", UnisettleRequest.number()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", UnisettleRequest.state().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата подачи", UnisettleRequest.regDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата рассмотрения", UnisettleRequest.reviewDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRequest").setPermissionKey(model.getSec().getPermission("editUnisettleRequest")));

        model.setDataSource(dataSource);
    }

    public void onClickAddRequest(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.REQUEST_ADD_EDIT, new ParametersMap()
                .add("decline", Boolean.FALSE)
                .add("personId", getModel(component).getPersonRoleModel().getPersonId())
                .add(PublisherActivator.PUBLISHER_ID_KEY, null)));
    }

    public void onClickEditRequest(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.REQUEST_ADD_EDIT, new ParametersMap()
                .add("decline", Boolean.FALSE)
                .add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }
}