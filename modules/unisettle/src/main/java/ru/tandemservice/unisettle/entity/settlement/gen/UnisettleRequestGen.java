package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisettle.entity.catalog.UnisettleRequestState;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявление на поселение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleRequestGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleRequest";
    public static final String ENTITY_NAME = "unisettleRequest";
    public static final int VERSION_HASH = -1696634854;
    private static IEntityMeta ENTITY_META;

    public static final String L_RESIDENT = "resident";
    public static final String P_NUMBER = "number";
    public static final String P_REG_DATE = "regDate";
    public static final String P_REVIEW_DATE = "reviewDate";
    public static final String L_STATE = "state";
    public static final String P_COMMENT = "comment";

    private UnisettleResident _resident;     // Поселяемый
    private long _number;     // Номер
    private Date _regDate;     // Дата подачи заявления
    private Date _reviewDate;     // Дата рассмотрения заявления
    private UnisettleRequestState _state;     // Состояния заявления о поселении
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Поселяемый. Свойство не может быть null.
     */
    @NotNull
    public UnisettleResident getResident()
    {
        return _resident;
    }

    /**
     * @param resident Поселяемый. Свойство не может быть null.
     */
    public void setResident(UnisettleResident resident)
    {
        dirty(_resident, resident);
        _resident = resident;
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setNumber(long number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата подачи заявления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Дата рассмотрения заявления.
     */
    public Date getReviewDate()
    {
        return _reviewDate;
    }

    /**
     * @param reviewDate Дата рассмотрения заявления.
     */
    public void setReviewDate(Date reviewDate)
    {
        dirty(_reviewDate, reviewDate);
        _reviewDate = reviewDate;
    }

    /**
     * @return Состояния заявления о поселении. Свойство не может быть null.
     */
    @NotNull
    public UnisettleRequestState getState()
    {
        return _state;
    }

    /**
     * @param state Состояния заявления о поселении. Свойство не может быть null.
     */
    public void setState(UnisettleRequestState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=4000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisettleRequestGen)
        {
            setResident(((UnisettleRequest)another).getResident());
            setNumber(((UnisettleRequest)another).getNumber());
            setRegDate(((UnisettleRequest)another).getRegDate());
            setReviewDate(((UnisettleRequest)another).getReviewDate());
            setState(((UnisettleRequest)another).getState());
            setComment(((UnisettleRequest)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleRequest.class;
        }

        public T newInstance()
        {
            return (T) new UnisettleRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "resident":
                    return obj.getResident();
                case "number":
                    return obj.getNumber();
                case "regDate":
                    return obj.getRegDate();
                case "reviewDate":
                    return obj.getReviewDate();
                case "state":
                    return obj.getState();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "resident":
                    obj.setResident((UnisettleResident) value);
                    return;
                case "number":
                    obj.setNumber((Long) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "reviewDate":
                    obj.setReviewDate((Date) value);
                    return;
                case "state":
                    obj.setState((UnisettleRequestState) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "resident":
                        return true;
                case "number":
                        return true;
                case "regDate":
                        return true;
                case "reviewDate":
                        return true;
                case "state":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "resident":
                    return true;
                case "number":
                    return true;
                case "regDate":
                    return true;
                case "reviewDate":
                    return true;
                case "state":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "resident":
                    return UnisettleResident.class;
                case "number":
                    return Long.class;
                case "regDate":
                    return Date.class;
                case "reviewDate":
                    return Date.class;
                case "state":
                    return UnisettleRequestState.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisettleRequest> _dslPath = new Path<UnisettleRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleRequest");
    }
            

    /**
     * @return Поселяемый. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getResident()
     */
    public static UnisettleResident.Path<UnisettleResident> resident()
    {
        return _dslPath.resident();
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getNumber()
     */
    public static PropertyPath<Long> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Дата рассмотрения заявления.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getReviewDate()
     */
    public static PropertyPath<Date> reviewDate()
    {
        return _dslPath.reviewDate();
    }

    /**
     * @return Состояния заявления о поселении. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getState()
     */
    public static UnisettleRequestState.Path<UnisettleRequestState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends UnisettleRequest> extends EntityPath<E>
    {
        private UnisettleResident.Path<UnisettleResident> _resident;
        private PropertyPath<Long> _number;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Date> _reviewDate;
        private UnisettleRequestState.Path<UnisettleRequestState> _state;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Поселяемый. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getResident()
     */
        public UnisettleResident.Path<UnisettleResident> resident()
        {
            if(_resident == null )
                _resident = new UnisettleResident.Path<UnisettleResident>(L_RESIDENT, this);
            return _resident;
        }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getNumber()
     */
        public PropertyPath<Long> number()
        {
            if(_number == null )
                _number = new PropertyPath<Long>(UnisettleRequestGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата подачи заявления. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(UnisettleRequestGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Дата рассмотрения заявления.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getReviewDate()
     */
        public PropertyPath<Date> reviewDate()
        {
            if(_reviewDate == null )
                _reviewDate = new PropertyPath<Date>(UnisettleRequestGen.P_REVIEW_DATE, this);
            return _reviewDate;
        }

    /**
     * @return Состояния заявления о поселении. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getState()
     */
        public UnisettleRequestState.Path<UnisettleRequestState> state()
        {
            if(_state == null )
                _state = new UnisettleRequestState.Path<UnisettleRequestState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleRequest#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UnisettleRequestGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return UnisettleRequest.class;
        }

        public String getEntityName()
        {
            return "unisettleRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
