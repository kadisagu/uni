// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.list.BaseResidentList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.UnisettleComponents;

/**
 * @author oleyba
 * @since 21.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<Model.UnisettleResidentListWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new PublisherColumnBuilder("ФИО", DAO.fioKey, "residentTab").subTab("residentContractTab").build());
        dataSource.addColumn(new SimpleColumn("Пол", DAO.sexKey).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Гражданство", Model.UnisettleResidentListWrapper.resident().person().identityCard().citizenship().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Поселение", Model.UnisettleResidentListWrapper.P_SETTLE_STATUS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Место поселения", UniplacesHabitablePlace.P_ADDRESS_TITLE, Model.UnisettleResidentListWrapper.P_ACCOMODATION_LIST).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата поселения", DAO.checkinDateKey, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выселения", DAO.checkoutDateKey, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Категории", PersonRole.P_CONTEXT_TITLE_FROM_PRINCIPAL_CONTEXT, Model.UnisettleResidentListWrapper.P_CATEGORY_LIST).setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickAddResident(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.RESIDENT_ADD));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        DynamicListDataSource<Model.UnisettleResidentListWrapper> dataSource = getModel(component).getDataSource();
        dataSource.showFirstPage();
        dataSource.refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.clearSettings();
        onClickSearch(component);
    }

    public void onClickMassAddContracts(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_MASS_ADD));
    }
}