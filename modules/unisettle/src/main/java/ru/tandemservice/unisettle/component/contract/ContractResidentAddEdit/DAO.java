// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractResidentAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uni.dao.UniDao;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

/**
 * @author oleyba
 * @since 10.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEditForm(model.getResident().getId() != null);

        if (model.isEditForm())
        {
            UnisettleContractResident resident = get(UnisettleContractResident.class, model.getResident().getId());
            model.setResident(resident);
            model.setContract(resident.getContract());
        }
        else
        {
            UnisettleContract contract = get(model.getContractId());
            model.setContract(contract);
            model.getResident().setContract(contract);
        }

        UnisettleContract contract = model.getContract();

        model.setShowArea(contract instanceof UnisettleSumAreaContract);
        model.setShowBedNumber(contract instanceof UnisettleSumAreaContract);
        model.setShowBedNumber(contract instanceof UnisettleSumAreaContract);
        model.setShowRelationDegree(contract instanceof UnisettleSumAreaContract);

        if (!model.isEditForm() && model.getBaseResidentId() != null)
        {
            model.getResident().setResident(get(UnisettleResident.class, model.getBaseResidentId()));
            model.setResidentDisabled(true);
        }

        if (model.getResident().getContract().getResident().equals(model.getResident().getResident()))
            model.setShowRelationDegree(false);

        model.setResidentList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                if (model.isEditForm())
                {
                    MQBuilder builder = new MQBuilder(UnisettleResident.ENTITY_CLASS, alias);
                    builder.add(MQExpression.eq(alias, "id", model.getResident().getResident().getId()));
                    return builder;
                }
                MQBuilder builder = new MQBuilder(UnisettleResident.ENTITY_CLASS, alias);
                MQBuilder self = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "rel");
                self.add(MQExpression.eq("rel", UnisettleContractResident.contract(), model.getContract()));
                self.add(MQExpression.eqProperty("rel", UnisettleContractResident.resident().id().s(), alias, "id"));
                builder.add(MQExpression.notExists(self));
                if (!model.isShowAll())
                {
                    MQBuilder active = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "rel");
                    active.add(MQExpression.eq("rel", UnisettleContractResident.contract().state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
                    active.add(MQExpression.eqProperty("rel", UnisettleContractResident.resident().id().s(), alias, "id"));
                    builder.add(MQExpression.notExists(active));
                }
                builder.addJoinFetch(alias, UnisettleResident.person().identityCard(), "idc");
                builder.addOrder("idc", IdentityCardGen.P_LAST_NAME);
                builder.addOrder("idc", IdentityCardGen.P_FIRST_NAME);
                builder.addOrder("idc", IdentityCardGen.P_MIDDLE_NAME);
                if (StringUtils.isNotBlank(filter))
                    builder.add(UniMQExpression.searchByFio("idc", filter));
                return builder;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((UnisettleResident) value).getPerson().getFio();
            }
        }.setPageSize(100));

        model.setRelationDegreeList(new LazySimpleSelectModel<>(getCatalogItemListOrderByUserCode(RelationDegree.class)));
    }

    @Override
    public void update(Model model)
    {
        getSession().refresh(model.getContract());
        if (!UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");
        getSession().saveOrUpdate(model.getResident());
    }
}
