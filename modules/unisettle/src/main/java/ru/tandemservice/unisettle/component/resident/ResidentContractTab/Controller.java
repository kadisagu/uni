// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.ResidentContractTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

/**
 * @author oleyba
 * @since 22.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UnisettleContract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Номер", UnisettleContract.number()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата заключения", UnisettleContract.regDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата заселения", UnisettleContract.checkinDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выселения", UnisettleContract.checkoutDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата фактического выселения", UnisettleContract.actualCheckoutDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", UnisettleContract.state().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид", UnisettleContract.type().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Помещение", UniplacesHabitablePlace.P_ADDRESS_TITLE, "accomodation").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditContract").setPermissionKey("editContractResidentContractTab"));
        model.setDataSource(dataSource);
    }

    public void onClickAddSingleContract(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.SINGLE_BED_CONTRACT_ADD_EDIT, new ParametersMap()
                .add("residentId", getModel(component).getResident().getId())
                .add("contractId", null)));
    }

    public void onClickAddAreaContract(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.AREA_SUM_CONTRACT_ADD_EDIT, new ParametersMap()
                .add("residentId", getModel(component).getResident().getId())
                .add("contractId", null)));
    }

    public void onClickEditContract(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();
        UnisettleContract contract = (UnisettleContract) getDao().get(id);
        if (contract instanceof UnisettleSingleBedContract)
            component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.SINGLE_BED_CONTRACT_ADD_EDIT, new ParametersMap()
                    .add("contractId", id)));
        if (contract instanceof UnisettleSumAreaContract)
            component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.AREA_SUM_CONTRACT_ADD_EDIT, new ParametersMap()
                    .add("contractId", id)));
    }

    public void onClickDeleteContract(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
    }

}