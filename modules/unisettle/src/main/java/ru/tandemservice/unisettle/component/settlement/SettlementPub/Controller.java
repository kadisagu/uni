/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author agolubenko
 * @since Sep 8, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareResidentDataSource(component);
        prepareContractDataSource(component);
    }

    private void prepareResidentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getContractDataSource() != null)
            return;

        DynamicListDataSource<UnisettleContractResident> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareResidentDataSource(model);
        });
        dataSource.addColumn(new PublisherLinkColumn("ФИО", UnisettleResident.person().fio().s(), UnisettleContractResident.resident().s()).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Номер договора поселения", UnisettleContract.title(), UnisettleContractResident.contract().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата заселения", UnisettleContractResident.contract().checkinDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выселения", UnisettleContractResident.contract().checkoutDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Число койко-мест", UnisettleContractResident.bedNumber()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Занимаемая площадь, кв.м", UnisettleContractResident.fractionalArea(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид договора поселения", UnisettleContractResident.contract().type().title()).setOrderable(false).setClickable(false));
        model.setResidentDataSource(dataSource);
    }

    private void prepareContractDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getContractDataSource() != null)
            return;

        DynamicListDataSource<UnisettleContract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareContractDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Номер договора поселения", UnisettleContract.title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата заселения", UnisettleContract.checkinDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выселения", UnisettleContract.checkoutDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата фактического выселения", UnisettleContract.actualCheckoutDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид договора поселения", UnisettleContract.type().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Cостояние договора", UnisettleContract.state().title()).setClickable(false));
        model.setContractDataSource(dataSource);
    }
    
    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.SETTLEMENT_EDIT, new ParametersMap().add("settlementId", getModel(component).getSettlement().getId())));
    }
}
