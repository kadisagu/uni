/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;

/**
 * @author agolubenko
 * @since Sep 8, 2010
 */
@State(@Bind(key = "publisherId", binding = "settlement.id"))
public class Model
{
    private UniplacesHabitablePlace _settlement = new UniplacesHabitablePlace();
    private DynamicListDataSource<UnisettleContractResident> residentDataSource;
    private DynamicListDataSource<UnisettleContract> contractDataSource;
    private String _selectedTabId;

    private long residentCount;
    
    public UniplacesPlace getPlace()
    {
        return getSettlement().getPlace();
    }
    
    public String getWindowNumber()
    {
        Integer result = getPlace().getWindowNumber();
        return (result != null && result > 0) ? result.toString() : "Без окон";
    }

    public UniplacesHabitablePlace getSettlement()
    {
        return _settlement;
    }

    public void setSettlement(UniplacesHabitablePlace settlement)
    {
        _settlement = settlement;
    }

    public DynamicListDataSource<UnisettleContractResident> getResidentDataSource()
    {
        return residentDataSource;
    }

    public void setResidentDataSource(DynamicListDataSource<UnisettleContractResident> residentDataSource)
    {
        this.residentDataSource = residentDataSource;
    }

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        _selectedTabId = selectedTabId;
    }

    public DynamicListDataSource<UnisettleContract> getContractDataSource()
    {
        return contractDataSource;
    }

    public void setContractDataSource(DynamicListDataSource<UnisettleContract> contractDataSource)
    {
        this.contractDataSource = contractDataSource;
    }

    public long getResidentCount()
    {
        return residentCount;
    }

    public void setResidentCount(long residentCount)
    {
        this.residentCount = residentCount;
    }
}