// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SumAreaContractPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;

/**
 * @author oleyba
 * @since 11.11.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        prepareResidentDataSource(component);
        prepareAccomodationDataSource(component);
    }

    private void prepareResidentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getResidentDataSource() != null)
            return;

        DynamicListDataSource<UnisettleContractResident> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareResidentDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Число койко-мест", UnisettleContractResident.bedNumber()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Площадь", UnisettleContractResident.fractionalArea(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ФИО", UnisettleContractResident.resident().person().identityCard().fio()));
        dataSource.addColumn(new SimpleColumn("Степень родства с нанимателем", UnisettleContractResident.relationDegree().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditResident").setPermissionKey("editAreaContractResidents").setDisabledProperty("editDisabled"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteResident").setPermissionKey("editAreaContractResidents").setDisabledProperty("editDisabled"));
        model.setResidentDataSource(dataSource);
    }

    private void prepareAccomodationDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getAccomodationDataSource() != null)
            return;

        DynamicListDataSource<UnisettleContractAccomodation> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareAccomodationDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Площадь", UnisettleContractAccomodation.fractionalArea(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Помещение", "number", UnisettleContractAccomodation.place().place().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Адрес", UnisettleContractAccomodation.place().addressTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditAccomodation").setPermissionKey("editAreaContractAccomodations").setDisabledProperty("editDisabled"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteAccomodation").setPermissionKey("editAreaContractAccomodations").setDisabledProperty("editDisabled"));
        model.setAccomodationDataSource(dataSource);
    }

    public void onClickEditContract(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.AREA_SUM_CONTRACT_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId())));
    }


    public void onClickChangeState(IBusinessComponent component)
    {
        UnisettleContractStateChange stateChange = (UnisettleContractStateChange) component.getListenerParameter();
        Model model = getModel(component);
        if (stateChange.isStartComponent())
            component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_ARCHIVE,
                                                             new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getContract().getId()).add("stateChange", stateChange)));
        else
            getDao().doChangeState(model, stateChange);
    }

    public void onClickAddResident(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_RESIDENT_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId())));
    }

    public void onClickAddResidentFromContract(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_RESIDENT_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId()).add("baseResidentId", model.getContract().getResident().getId())));
    }

    public void onClickAddResidentFromRelatives(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.RELATIVE_AS_RESIDENT_ADD,
                                                         new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getContract().getResident().getId())));
    }

    public void onClickEditResident(IBusinessComponent component)
    {
        getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_RESIDENT_ADD_EDIT,
                                                         new ParametersMap().add("residentId", component.getListenerParameter())));
    }

    public void onClickDeleteResident(IBusinessComponent component)
    {
        getDao().deleteResident(getModel(component), (Long) component.getListenerParameter());
        onRefreshComponent(component);
    }

    public void onClickAddAccomodation(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_ACCOMODATION_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId())));
    }

    public void onClickEditAccomodation(IBusinessComponent component)
    {
        getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_ACCOMODATION_ADD_EDIT,
                                                         new ParametersMap().add("accomodationId", component.getListenerParameter())));
    }

    public void onClickDeleteAccomodation(IBusinessComponent component)
    {
        getDao().deleteAccomodation(getModel(component), (Long) component.getListenerParameter());
    }
}