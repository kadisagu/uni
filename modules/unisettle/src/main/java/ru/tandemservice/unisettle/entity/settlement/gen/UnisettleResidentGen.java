package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Поселяемый
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleResidentGen extends PersonRole
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleResident";
    public static final String ENTITY_NAME = "unisettleResident";
    public static final int VERSION_HASH = 533930924;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisettleResidentGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleResidentGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleResident.class;
        }

        public T newInstance()
        {
            return (T) new UnisettleResident();
        }
    }
    private static final Path<UnisettleResident> _dslPath = new Path<UnisettleResident>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleResident");
    }
            

    public static class Path<E extends UnisettleResident> extends PersonRole.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UnisettleResident.class;
        }

        public String getEntityName()
        {
            return "unisettleResident";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
