// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.PersonRoleRequestTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
@State({
        @Bind(key = "personRoleModel", binding = "personRoleModel")
})
public class Model
{
    private ISecureRoleContext _personRoleModel;

    private CommonPostfixPermissionModel sec;

    private boolean addingDisabled;
    private DynamicListDataSource<UnisettleRequest> dataSource;

    public String getSettingsKey()
    {
        return getPersonRoleModel().getPersonRoleName() + "UnisettleRequestTab.";
    }

    public boolean isAddingDisabled()
    {
        return addingDisabled;
    }

    public void setAddingDisabled(boolean addingDisabled)
    {
        this.addingDisabled = addingDisabled;
    }

    public ISecureRoleContext getPersonRoleModel()
    {
        return _personRoleModel;
    }

    public void setPersonRoleModel(ISecureRoleContext personRoleModel)
    {
        _personRoleModel = personRoleModel;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }

    public DynamicListDataSource<UnisettleRequest> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<UnisettleRequest> dataSource)
    {
        this.dataSource = dataSource;
    }
}
