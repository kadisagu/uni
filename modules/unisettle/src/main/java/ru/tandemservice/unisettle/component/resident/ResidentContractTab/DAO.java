// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.ResidentContractTab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 22.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Session session = getSession();

        model.setResident(getNotNull(UnisettleResident.class, model.getPersonRoleModel().getSecuredObject().getId()));

        MQBuilder contractBuilder = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "resident");
        contractBuilder.addJoin("resident", UnisettleContractResident.contract(), "contract");
        contractBuilder.add(MQExpression.eq("resident", UnisettleContractResident.resident().person(), model.getResident().getPerson()));
        contractBuilder.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        UnisettleContractResident resident = (UnisettleContractResident) contractBuilder.uniqueResult(session);
        if (null == resident)
        {
            model.setContract(null);
            model.setSettled(false);
            model.getAccomodationList().clear();
            return;
        }

        model.setContract(resident.getContract());
        model.setSettled(true);

        model.getAccomodationList().clear();
        MQBuilder accomodationBuilder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accomodationBuilder.addJoinFetch("acc", UnisettleContractAccomodation.place(), "place");
        accomodationBuilder.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accomodationBuilder.add(MQExpression.eq("contract", UnisettleContract.id(), model.getContract().getId()));
        for (UnisettleContractAccomodation acc : accomodationBuilder.<UnisettleContractAccomodation>getResultList(session))
            model.getAccomodationList().add(acc.getPlace());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Session session = getSession();
        DynamicListDataSource<UnisettleContract> dataSource = model.getDataSource();

        MQBuilder contractBuilder = new MQBuilder(UnisettleContract.ENTITY_CLASS, "contract");
        MQBuilder sub = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "rel");
        sub.add(MQExpression.eqProperty("rel", UnisettleContractResident.contract().id().s(), "contract", "id"));
        sub.add(MQExpression.eq("rel", UnisettleContractResident.resident(), model.getResident()));
        contractBuilder.add(MQExpression.or(
                MQExpression.eq("contract", UnisettleContract.resident(), model.getResident()),
                MQExpression.exists(sub)));
        contractBuilder.addOrder("contract", UnisettleContract.regDate());
        List<UnisettleContract> contractList = contractBuilder.getResultList(session);
        UniBaseUtils.createPage(dataSource, contractList);

        Map<UnisettleContract, List<UniplacesHabitablePlace>> accomodationMap = new HashMap<UnisettleContract, List<UniplacesHabitablePlace>>();
        for (UnisettleContract contract : contractList)
            accomodationMap.put(contract, new ArrayList<UniplacesHabitablePlace>());

        MQBuilder accomodationBuilder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accomodationBuilder.addJoinFetch("acc", UnisettleContractAccomodation.place(), "place");
        accomodationBuilder.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accomodationBuilder.addJoinFetch("contract", UnisettleContract.resident(), "resident");
        sub = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "rel");
        sub.add(MQExpression.eqProperty("rel", UnisettleContractResident.contract().id().s(), "contract", "id"));
        sub.add(MQExpression.eq("rel", UnisettleContractResident.resident(), model.getResident()));
        accomodationBuilder.add(MQExpression.or(
                MQExpression.eq("contract", UnisettleContract.resident(), model.getResident()),
                MQExpression.exists(sub)));
        for (UnisettleContractAccomodation acc : accomodationBuilder.<UnisettleContractAccomodation>getResultList(getSession()))
            accomodationMap.get(acc.getContract()).add(acc.getPlace());

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
            wrapper.setViewProperty("accomodation", accomodationMap.get((UnisettleContract) wrapper.getEntity()));
    }
}
