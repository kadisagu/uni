/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementList;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Sep 6, 2010
 */
@State(@Bind(key = "selectedTabId", binding = "selectedTabId"))
public class Model
{
    private IDataSettings _settings;
    private String _selectedTabId;

    private ISelectModel _buildingList;
    private ISelectModel _unitList;
    private ISelectModel _floorList;
    private ISelectModel _placeList;
    private List<HSelectOption> _purposeList;

    // TODO vacantPlace;
    // TODO overcrowding;

    private DynamicListDataSource<UniplacesHabitablePlace> _dataSource;
    private DynamicListDataSource<UniplacesHabitablePlace> conflictDataSource;

    public UniplacesBuilding getBuilding()
    {
        return (UniplacesBuilding) getSettings().get("building");
    }

    public UniplacesUnit getUnit()
    {
        return (UniplacesUnit) getSettings().get("unit");
    }

    public UniplacesFloor getFloor()
    {
        return (UniplacesFloor) getSettings().get("floor");
    }

    public UniplacesPlace getPlace()
    {
        return (UniplacesPlace) getSettings().get("place");
    }

    public UniplacesPlacePurpose getPurpose()
    {
        return (UniplacesPlacePurpose) getSettings().get("purpose");
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(ISelectModel buildingList)
    {
        _buildingList = buildingList;
    }

    public ISelectModel getUnitList()
    {
        return _unitList;
    }

    public void setUnitList(ISelectModel unitList)
    {
        _unitList = unitList;
    }

    public ISelectModel getFloorList()
    {
        return _floorList;
    }

    public void setFloorList(ISelectModel floorList)
    {
        _floorList = floorList;
    }

    public ISelectModel getPlaceList()
    {
        return _placeList;
    }

    public void setPlaceList(ISelectModel placeList)
    {
        _placeList = placeList;
    }

    public List<HSelectOption> getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(List<HSelectOption> purposeList)
    {
        _purposeList = purposeList;
    }

    public DynamicListDataSource<UniplacesHabitablePlace> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesHabitablePlace> dataSource)
    {
        _dataSource = dataSource;
    }

    public DynamicListDataSource<UniplacesHabitablePlace> getConflictDataSource()
    {
        return conflictDataSource;
    }

    public void setConflictDataSource(DynamicListDataSource<UniplacesHabitablePlace> conflictDataSource)
    {
        this.conflictDataSource = conflictDataSource;
    }

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        _selectedTabId = selectedTabId;
    }
}
