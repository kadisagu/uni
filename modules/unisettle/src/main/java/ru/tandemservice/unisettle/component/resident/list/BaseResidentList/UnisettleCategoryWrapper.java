// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.list.BaseResidentList;

import java.io.Serializable;

import org.tandemframework.core.common.ITitled;

/**
 * @author oleyba
 * @since 22.11.2010
 */
public class UnisettleCategoryWrapper implements Serializable, ITitled
{
    private static final long serialVersionUID = 1L;
    private String title;
    private short entityCode;

    public UnisettleCategoryWrapper(String title, short entityCode)
    {
        this.title = title;
        this.entityCode = entityCode;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    public short getEntityCode()
    {
        return entityCode;
    }
}
