// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SumAreaContractAddEdit;

import java.util.Date;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractState;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractType;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

/**
 * @author oleyba
 * @since 11.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getContract().getId() != null)
        {
            model.setContract(getNotNull(UnisettleSumAreaContract.class, model.getContract().getId()));
            model.setResident(model.getContract().getResident());
        }
        else
        {
            UnisettleSumAreaContract contract = model.getContract();
            model.setResident(getNotNull(UnisettleResident.class, model.getResident().getId()));
            contract.setResident(model.getResident());
            contract.setRegDate(new Date());
            contract.setCheckinDate(new Date());
            contract.setState(get(UnisettleContractState.class, UnisettleContractState.code().s(), UnisettleDefines.CONTRACT_STATE_FORMING));
            contract.setType(get(UnisettleContractType.class, UnisettleContractType.code().s(), UnisettleDefines.CONTRACT_TYPE_REGISTRATION_AND_SETTLE));
        }
        model.setСontractTypeList(new LazySimpleSelectModel<UnisettleContractType>(UnisettleContractType.class));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();
        UnisettleSumAreaContract contract = model.getContract();

        boolean addForm = contract.getId() == null;

        if (!addForm && !UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");

        session.saveOrUpdate(contract);

        if (addForm)
        {
            UnisettleContractResident resident = new UnisettleContractResident();
            resident.setContract(contract);
            resident.setResident(model.getResident());
            resident.setArea(0);
            resident.setBedNumber(0);
            session.save(resident);
        }
    }

}
