/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unisettle.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EmployeePostView _employeePostView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeePostView.employeePostPubTabPanelExtPoint())
                .addTab(componentTab("unsettleRequestTab", "ru.tandemservice.unisettle.component.request.PersonRoleRequestTab").permissionKey("viewUnisettleRequestTab_employeePost").parameters("mvel:['" + ISecureRoleContext.SECURE_ROLE_CONTEXT + "':presenter.secureRoleContext]"))
                .create();
    }
}
