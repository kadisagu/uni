// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractArchive;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;

/**
 * @author oleyba
 * @since 10.11.2010
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "contractId"),
        @Bind(key = "stateChange", binding = "stateChange")})
public class Model
{
    private Long contractId;
    private UnisettleContract contract;
    private UnisettleContractStateChange stateChange;

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }

    public UnisettleContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleContract contract)
    {
        this.contract = contract;
    }

    public UnisettleContractStateChange getStateChange()
    {
        return stateChange;
    }

    public void setStateChange(UnisettleContractStateChange stateChange)
    {
        this.stateChange = stateChange;
    }
}
