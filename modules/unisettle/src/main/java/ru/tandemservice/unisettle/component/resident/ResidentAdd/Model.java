// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.ResidentAdd;

import java.util.List;

import org.apache.tapestry.form.validator.BaseValidator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 21.10.2010
 */
@Output( { @Bind(key = "similarPersons"), @Bind(key = "personString"), @Bind(key="infoString") })
public class Model extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Model<UnisettleResident>
{
    private UnisettleResident _resident = new UnisettleResident();

    @Override
    protected UnisettleResident getNewInstance()
    {
        return getResident();
    }

    @Override
    public String getPersonString()
    {
        return "Добавление поселяемого: " + super.getPersonString();
    }

    @Override
    public String getInfoString()
    {
        return "В процессе регистрации поселяемого найдены персоны с похожими личными данными. Вы можете зарегистрировать поселяемого на основе одной из существующих персон, или создать новую.<br/>Для того, чтобы зарегистрировать поселяемого на основе одной из существующих персон, выберите её в списке.";
    }

    @Override
    protected String getNewPersonButtonName()
    {
        return "Добавить поселяемого, создав новую персону";
    }

    @Override
    protected String getOnBasisPersonButtonName()
    {
        return "Добавить поселяемого на основе существующей персоны";
    }

    public IdentityCard getIdentityCard()
    {
        return getPerson().getIdentityCard();
    }

    public IdentityCardType getIdentityCardType()
    {
        return getIdentityCard().getCardType();
    }

    public UnisettleResident getResident()
    {
        return _resident;
    }

    public void setResident(UnisettleResident resident)
    {
        _resident = resident;
    }
}
