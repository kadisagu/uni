// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementMassEdit;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author oleyba
 * @since 15.09.2010
 */
public class Model
{
    private ISelectModel _buildingList;
    private ISelectModel _unitList;
    private ISelectModel _floorList;
    private List<HSelectOption> _purposeList;
    private ISelectModel _conditionList;
    private ISelectModel _orgUnitList;

    private UniplacesBuilding _building;
    private UniplacesUnit _unit;
    private UniplacesFloor _floor;
    private UniplacesPlacePurpose _purpose;
    private UniplacesPlaceCondition _condition;
    private OrgUnit _orgUnit;

    private DynamicListDataSource<UniplacesHabitablePlace> _dataSource;

    private UniplacesPlacePurpose _newPurpose;
    private UniplacesPlaceCondition _newCondition;
    private Integer _newBedNumber;

    public boolean isShowBedNumber()
    {
        return getNewPurpose() != null && getNewPurpose().isCountBedNumber();
    }

    public void clear()
    {
        setBuilding(null);
        setUnit(null);
        setFloor(null);
        setPurpose(null);
        setCondition(null);
        setOrgUnit(null);
    }

    public ISelectModel getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(ISelectModel buildingList)
    {
        _buildingList = buildingList;
    }

    public ISelectModel getUnitList()
    {
        return _unitList;
    }

    public void setUnitList(ISelectModel unitList)
    {
        _unitList = unitList;
    }

    public ISelectModel getFloorList()
    {
        return _floorList;
    }

    public void setFloorList(ISelectModel floorList)
    {
        _floorList = floorList;
    }

    public List<HSelectOption> getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(List<HSelectOption> purposeList)
    {
        _purposeList = purposeList;
    }

    public ISelectModel getConditionList()
    {
        return _conditionList;
    }

    public void setConditionList(ISelectModel conditionList)
    {
        _conditionList = conditionList;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        _floor = floor;
    }

    public UniplacesPlacePurpose getPurpose()
    {
        return _purpose;
    }

    public void setPurpose(UniplacesPlacePurpose purpose)
    {
        _purpose = purpose;
    }

    public UniplacesPlaceCondition getCondition()
    {
        return _condition;
    }

    public void setCondition(UniplacesPlaceCondition condition)
    {
        _condition = condition;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public DynamicListDataSource<UniplacesHabitablePlace> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniplacesHabitablePlace> dataSource)
    {
        _dataSource = dataSource;
    }

    public UniplacesPlacePurpose getNewPurpose()
    {
        return _newPurpose;
    }

    public void setNewPurpose(UniplacesPlacePurpose newPurpose)
    {
        _newPurpose = newPurpose;
    }

    public UniplacesPlaceCondition getNewCondition()
    {
        return _newCondition;
    }

    public void setNewCondition(UniplacesPlaceCondition newCondition)
    {
        _newCondition = newCondition;
    }

    public Integer getNewBedNumber()
    {
        return _newBedNumber;
    }

    public void setNewBedNumber(Integer newBedNumber)
    {
        _newBedNumber = newBedNumber;
    }

}
