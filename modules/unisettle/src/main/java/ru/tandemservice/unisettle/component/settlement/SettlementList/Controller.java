/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.dao.IUnisettleSettlementDAO;

/**
 * @author agolubenko
 * @since Sep 6, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
        prepareConflictDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UniplacesHabitablePlace> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Помещение", UniplacesHabitablePlace.place().number().s()));
        dataSource.addColumn(new SimpleColumn("Площадь, кв.м", UniplacesHabitablePlace.place().s() + "." + UniplacesPlace.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Занятая площадь, кв.м", IUnisettleSettlementDAO.P_TAKEN_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Число койко-мест", UniplacesHabitablePlace.bedNumber().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Число занятых койко-мест", IUnisettleSettlementDAO.P_TAKEN_BEDS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Число проживающих", IUnisettleSettlementDAO.P_RESIDENT_COUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Этаж", UniplacesHabitablePlace.place().floor().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Блок", UniplacesHabitablePlace.place().floor().unit().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Здание", UniplacesHabitablePlace.place().floor().unit().building().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Назначение", UniplacesHabitablePlace.place().purpose().title().s()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditSettlement").setPermissionKey("editSettlement_list"));

        model.setDataSource(dataSource);
    }

    private void prepareConflictDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getConflictDataSource() != null)
            return;

        DynamicListDataSource<UniplacesHabitablePlace> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareConflictDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Помещение", UniplacesHabitablePlace.place().number().s()));
        dataSource.addColumn(new SimpleColumn("Площадь, кв.м", UniplacesHabitablePlace.place().s() + "." + UniplacesPlace.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Число койко-мест", UniplacesHabitablePlace.bedNumber().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Этаж", UniplacesHabitablePlace.place().floor().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Блок", UniplacesHabitablePlace.place().floor().unit().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Здание", UniplacesHabitablePlace.place().floor().unit().building().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Назначение", UniplacesHabitablePlace.place().purpose().title().s()).setClickable(false));
        model.setConflictDataSource(dataSource);
    }

    public void onClickMassEditSettlement(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.SETTLEMENT_MASS_EDIT));
    }

    public void onClickEditSettlement(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.SETTLEMENT_EDIT, new ParametersMap().add("settlementId", component.getListenerParameter())));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        DynamicListDataSource<UniplacesHabitablePlace> dataSource = getModel(component).getDataSource();
        dataSource.showFirstPage();
        dataSource.refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.clearSettings();
        onClickSearch(component);
    }

//    public void onClickPrint(IBusinessComponent component)
//    {
//        activateInRoot(component,
//                new ComponentActivator(
//                "ru.tandemservice.unisettle.component.settlement.SettlementList",
//                new ParametersMap().add("selectedTabId", "mainTab"))
//                .setPrintDisplayMode(true)
//                );
//    }
}


