package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractState;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractType;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор на поселение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleContractGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleContract";
    public static final String ENTITY_NAME = "unisettleContract";
    public static final int VERSION_HASH = 1902626836;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_REG_DATE = "regDate";
    public static final String P_CHECKIN_DATE = "checkinDate";
    public static final String P_CHECKOUT_DATE = "checkoutDate";
    public static final String P_ACTUAL_CHECKOUT_DATE = "actualCheckoutDate";
    public static final String L_STATE = "state";
    public static final String L_TYPE = "type";
    public static final String L_RESIDENT = "resident";
    public static final String P_TITLE = "title";

    private String _number;     // Номер
    private Date _regDate;     // Дата заключения
    private Date _checkinDate;     // Дата заселения
    private Date _checkoutDate;     // Дата выселения
    private Date _actualCheckoutDate;     // Дата фактического выселения
    private UnisettleContractState _state;     // Состояния договора поселения
    private UnisettleContractType _type;     // Виды договора поселения
    private UnisettleResident _resident;     // Наниматель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата заключения. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата заключения. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Дата заселения. Свойство не может быть null.
     */
    @NotNull
    public Date getCheckinDate()
    {
        return _checkinDate;
    }

    /**
     * @param checkinDate Дата заселения. Свойство не может быть null.
     */
    public void setCheckinDate(Date checkinDate)
    {
        dirty(_checkinDate, checkinDate);
        _checkinDate = checkinDate;
    }

    /**
     * @return Дата выселения.
     */
    public Date getCheckoutDate()
    {
        return _checkoutDate;
    }

    /**
     * @param checkoutDate Дата выселения.
     */
    public void setCheckoutDate(Date checkoutDate)
    {
        dirty(_checkoutDate, checkoutDate);
        _checkoutDate = checkoutDate;
    }

    /**
     * @return Дата фактического выселения.
     */
    public Date getActualCheckoutDate()
    {
        return _actualCheckoutDate;
    }

    /**
     * @param actualCheckoutDate Дата фактического выселения.
     */
    public void setActualCheckoutDate(Date actualCheckoutDate)
    {
        dirty(_actualCheckoutDate, actualCheckoutDate);
        _actualCheckoutDate = actualCheckoutDate;
    }

    /**
     * @return Состояния договора поселения. Свойство не может быть null.
     */
    @NotNull
    public UnisettleContractState getState()
    {
        return _state;
    }

    /**
     * @param state Состояния договора поселения. Свойство не может быть null.
     */
    public void setState(UnisettleContractState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Виды договора поселения. Свойство не может быть null.
     */
    @NotNull
    public UnisettleContractType getType()
    {
        return _type;
    }

    /**
     * @param type Виды договора поселения. Свойство не может быть null.
     */
    public void setType(UnisettleContractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Наниматель. Свойство не может быть null.
     */
    @NotNull
    public UnisettleResident getResident()
    {
        return _resident;
    }

    /**
     * @param resident Наниматель. Свойство не может быть null.
     */
    public void setResident(UnisettleResident resident)
    {
        dirty(_resident, resident);
        _resident = resident;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisettleContractGen)
        {
            setNumber(((UnisettleContract)another).getNumber());
            setRegDate(((UnisettleContract)another).getRegDate());
            setCheckinDate(((UnisettleContract)another).getCheckinDate());
            setCheckoutDate(((UnisettleContract)another).getCheckoutDate());
            setActualCheckoutDate(((UnisettleContract)another).getActualCheckoutDate());
            setState(((UnisettleContract)another).getState());
            setType(((UnisettleContract)another).getType());
            setResident(((UnisettleContract)another).getResident());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleContract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("UnisettleContract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "regDate":
                    return obj.getRegDate();
                case "checkinDate":
                    return obj.getCheckinDate();
                case "checkoutDate":
                    return obj.getCheckoutDate();
                case "actualCheckoutDate":
                    return obj.getActualCheckoutDate();
                case "state":
                    return obj.getState();
                case "type":
                    return obj.getType();
                case "resident":
                    return obj.getResident();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "checkinDate":
                    obj.setCheckinDate((Date) value);
                    return;
                case "checkoutDate":
                    obj.setCheckoutDate((Date) value);
                    return;
                case "actualCheckoutDate":
                    obj.setActualCheckoutDate((Date) value);
                    return;
                case "state":
                    obj.setState((UnisettleContractState) value);
                    return;
                case "type":
                    obj.setType((UnisettleContractType) value);
                    return;
                case "resident":
                    obj.setResident((UnisettleResident) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "regDate":
                        return true;
                case "checkinDate":
                        return true;
                case "checkoutDate":
                        return true;
                case "actualCheckoutDate":
                        return true;
                case "state":
                        return true;
                case "type":
                        return true;
                case "resident":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "regDate":
                    return true;
                case "checkinDate":
                    return true;
                case "checkoutDate":
                    return true;
                case "actualCheckoutDate":
                    return true;
                case "state":
                    return true;
                case "type":
                    return true;
                case "resident":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "regDate":
                    return Date.class;
                case "checkinDate":
                    return Date.class;
                case "checkoutDate":
                    return Date.class;
                case "actualCheckoutDate":
                    return Date.class;
                case "state":
                    return UnisettleContractState.class;
                case "type":
                    return UnisettleContractType.class;
                case "resident":
                    return UnisettleResident.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisettleContract> _dslPath = new Path<UnisettleContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleContract");
    }
            

    /**
     * @return Номер.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата заключения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Дата заселения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getCheckinDate()
     */
    public static PropertyPath<Date> checkinDate()
    {
        return _dslPath.checkinDate();
    }

    /**
     * @return Дата выселения.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getCheckoutDate()
     */
    public static PropertyPath<Date> checkoutDate()
    {
        return _dslPath.checkoutDate();
    }

    /**
     * @return Дата фактического выселения.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getActualCheckoutDate()
     */
    public static PropertyPath<Date> actualCheckoutDate()
    {
        return _dslPath.actualCheckoutDate();
    }

    /**
     * @return Состояния договора поселения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getState()
     */
    public static UnisettleContractState.Path<UnisettleContractState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Виды договора поселения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getType()
     */
    public static UnisettleContractType.Path<UnisettleContractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Наниматель. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getResident()
     */
    public static UnisettleResident.Path<UnisettleResident> resident()
    {
        return _dslPath.resident();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UnisettleContract> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Date> _checkinDate;
        private PropertyPath<Date> _checkoutDate;
        private PropertyPath<Date> _actualCheckoutDate;
        private UnisettleContractState.Path<UnisettleContractState> _state;
        private UnisettleContractType.Path<UnisettleContractType> _type;
        private UnisettleResident.Path<UnisettleResident> _resident;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UnisettleContractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата заключения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(UnisettleContractGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Дата заселения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getCheckinDate()
     */
        public PropertyPath<Date> checkinDate()
        {
            if(_checkinDate == null )
                _checkinDate = new PropertyPath<Date>(UnisettleContractGen.P_CHECKIN_DATE, this);
            return _checkinDate;
        }

    /**
     * @return Дата выселения.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getCheckoutDate()
     */
        public PropertyPath<Date> checkoutDate()
        {
            if(_checkoutDate == null )
                _checkoutDate = new PropertyPath<Date>(UnisettleContractGen.P_CHECKOUT_DATE, this);
            return _checkoutDate;
        }

    /**
     * @return Дата фактического выселения.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getActualCheckoutDate()
     */
        public PropertyPath<Date> actualCheckoutDate()
        {
            if(_actualCheckoutDate == null )
                _actualCheckoutDate = new PropertyPath<Date>(UnisettleContractGen.P_ACTUAL_CHECKOUT_DATE, this);
            return _actualCheckoutDate;
        }

    /**
     * @return Состояния договора поселения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getState()
     */
        public UnisettleContractState.Path<UnisettleContractState> state()
        {
            if(_state == null )
                _state = new UnisettleContractState.Path<UnisettleContractState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Виды договора поселения. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getType()
     */
        public UnisettleContractType.Path<UnisettleContractType> type()
        {
            if(_type == null )
                _type = new UnisettleContractType.Path<UnisettleContractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Наниматель. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getResident()
     */
        public UnisettleResident.Path<UnisettleResident> resident()
        {
            if(_resident == null )
                _resident = new UnisettleResident.Path<UnisettleResident>(L_RESIDENT, this);
            return _resident;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContract#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(UnisettleContractGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UnisettleContract.class;
        }

        public String getEntityName()
        {
            return "unisettleContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
