// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleBedContractPub;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.dao.IUnisettleContractDAO;
import ru.tandemservice.unisettle.entity.catalog.UnisettleTemplateDocument;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;

import java.text.SimpleDateFormat;

/**
 * @author oleyba
 * @since 09.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setContract(get(UnisettleSingleBedContract.class, model.getContract().getId()));
        model.setContractResident(get(UnisettleContractResident.class, UnisettleContractResident.contract().s(), model.getContract()));
        model.setAccomodation(get(UnisettleContractAccomodation.class, UnisettleContractAccomodation.contract().s(), model.getContract()));
    }

    @Override
    public void doChangeState(Model model, UnisettleContractStateChange stateChange)
    {
        UnisettleSingleBedContract contract = model.getContract();

        IUnisettleContractDAO.instance.get().doChangeContractState(contract, stateChange);
    }

    @Override
    public void deleteResident(Model model)
    {
        getSession().refresh(model.getContract());
        if (!UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");
        
        UnisettleContractResident resident = model.getContractResident();
        if (null != resident)
            delete(resident.getId());
    }

    @Override
    public void deleteAccomodation(Model model)
    {
        getSession().refresh(model.getContract());
        if (!UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");

        UnisettleContractAccomodation accomodation = model.getAccomodation();
        if (null != accomodation)
            delete(accomodation.getId());
    }

    @Override
    public byte[] printContract(Model model)
    {
        UnisettleTemplateDocument template = getCatalogItem(UnisettleTemplateDocument.class, "singleContract");
        RtfDocument document = new RtfReader().read(template.getContent());

        UnisettleSingleBedContract contract = model.getContract();
        UnisettleContractAccomodation accomodation = model.getAccomodation();
        IdentityCard card = contract.getResident().getPerson().getIdentityCard();

        TopOrgUnit academy = TopOrgUnit.getInstance();
        EmployeePost rector = EmployeeManager.instance().dao().getHead(academy);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("number", contract.getNumber());
        modifier.put("city", academy.getAddress() == null ? "" : academy.getAddress().getSettlement().getTitleWithType());
        modifier.put("regDate", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(contract.getRegDate()) + "г.");
        modifier.put("residentFio", card.getFullFio());
        modifier.put("residentBirthday", DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()));
        modifier.put("placeNumber", accomodation == null ? "-" : accomodation.getPlace().getPlace().getNumber());
        modifier.put("buildingNumber", accomodation == null ? "-" : accomodation.getPlace().getPlace().getFloor().getUnit().getBuilding().getIdentifier());
        modifier.put("address", accomodation == null ? "-" : accomodation.getPlace().getPlace().getFloor().getUnit().getBuilding().getAddress().getTitleWithFlat());
        modifier.put("academy", academy.getPrintTitle());
        modifier.put("academyAddress", academy.getAddress() == null ? "-" : academy.getAddress().getTitleWithFlat());
        modifier.put("rectorFio", rector == null ? "-" : rector.getEmployee().getPerson().getFullFio());
        modifier.put("residentPassport", card.getTitle());

        modifier.modify(document);

        return RtfUtil.toByteArray(document);
    }
}
