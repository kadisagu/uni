// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.RequestPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unisettle.UnisettleComponents;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickDecline(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.REQUEST_ADD_EDIT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getRequest().getId()).add("decline", Boolean.TRUE)));
    }

    public void onClickCancel(IBusinessComponent component)
    {
        getDao().doCancelRequest(getModel(component));
    }
}