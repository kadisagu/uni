// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementMassEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unisettle.dao.IUnisettleSettlementDAO;

/**
 * @author oleyba
 * @since 15.09.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UniplacesHabitablePlace> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn().setSelectCaption("Выбор").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер", UniplacesHabitablePlace.place().number().s()));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", UniplacesHabitablePlace.place().s() + "." + UniplacesPlace.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Число койко-мест", UniplacesHabitablePlace.bedNumber().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Число проживающих", IUnisettleSettlementDAO.P_RESIDENT_COUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Этаж", UniplacesHabitablePlace.place().floor().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Блок", UniplacesHabitablePlace.place().floor().unit().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Здание", UniplacesHabitablePlace.place().floor().unit().building().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Назначение", UniplacesHabitablePlace.place().purpose().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ответственное подразделение", UniplacesHabitablePlace.place().responsibleOrgUnit().fullTitle().s()).setClickable(false));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }

    public void onClickShow(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).clear();
        onClickShow(component);
    }
}