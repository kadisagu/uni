// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.list.BaseResidentList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 21.10.2010
 */
public class Model
{
    private IDataSettings _settings;

    private ISelectModel sexModel;
    private List<UnisettleCategoryWrapper> categoryModel;
    private DynamicListDataSource<UnisettleResidentListWrapper> _dataSource;

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getSexModel()
    {
        return sexModel;
    }

    public void setSexModel(ISelectModel sexModel)
    {
        this.sexModel = sexModel;
    }

    public List<UnisettleCategoryWrapper> getCategoryModel()
    {
        return categoryModel;
    }

    public void setCategoryModel(List<UnisettleCategoryWrapper> categoryModel)
    {
        this.categoryModel = categoryModel;
    }

    public DynamicListDataSource<UnisettleResidentListWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UnisettleResidentListWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public static class UnisettleResidentListWrapper extends IdentifiableWrapper<UnisettleResident>
    {
        private static final long serialVersionUID = 1L;
        private UnisettleResident resident;
        private UnisettleContract contract;
        private String settleStatus = "нет";
        private List<UniplacesHabitablePlace> accomodationList = new ArrayList<UniplacesHabitablePlace>();
        private List<IPrincipalContext> categoryList;

        private static final UnisettleResident.Path _residentPath = new UnisettleResident.Path("resident");
        public static UnisettleResident.Path resident() { return _residentPath; }

        private static final UnisettleContract.Path _contractPath = new UnisettleContract.Path("contract");
        public static UnisettleContract.Path contract() { return _contractPath; }

        public static final String P_SETTLE_STATUS = "settleStatus";
        public static final String P_ACCOMODATION_LIST = "accomodationList";
        public static final String P_CATEGORY_LIST = "categoryList";

        public UnisettleResidentListWrapper(UnisettleResident resident) throws ClassCastException
        {
            super(resident);
            this.resident = resident;
        }

        public UnisettleResident getResident()
        {
            return resident;
        }

        public UnisettleContract getContract()
        {
            return contract;
        }

        public void setContract(UnisettleContract contract)
        {
            this.contract = contract;
        }

        public String getSettleStatus()
        {
            return settleStatus;
        }

        public void setSettleStatus(String settleStatus)
        {
            this.settleStatus = settleStatus;
        }

        public List<UniplacesHabitablePlace> getAccomodationList()
        {
            return accomodationList;
        }

        public void setAccomodationList(List<UniplacesHabitablePlace> accomodationList)
        {
            this.accomodationList = accomodationList;
        }

        public List<IPrincipalContext> getCategoryList()
        {
            return categoryList;
        }

        public void setCategoryList(List<IPrincipalContext> categoryList)
        {
            this.categoryList = categoryList;
        }
    }
}
