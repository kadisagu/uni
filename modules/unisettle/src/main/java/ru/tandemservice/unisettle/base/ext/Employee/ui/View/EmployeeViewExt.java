/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unisettle.base.ext.Employee.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeView;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class EmployeeViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public EmployeeView _employeeView;
    
    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeeView.employeePubTabPanelExtPoint())
                .addTab(componentTab("unsettleRequestTab", "ru.tandemservice.unisettle.component.request.PersonRoleRequestTab").permissionKey("viewUnisettleRequestTab_employee").parameters("mvel:['" + ISecureRoleContext.SECURE_ROLE_CONTEXT + "':presenter.secureRoleContext]"))
                .create();
    }
}
