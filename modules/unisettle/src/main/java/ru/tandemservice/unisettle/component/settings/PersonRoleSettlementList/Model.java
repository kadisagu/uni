// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settings.PersonRoleSettlementList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author oleyba
 * @since 22.11.2010
 */
public class Model
{
    private DynamicListDataSource<PersonRoleWrapper> dataSource;
    private IDataSettings _settings;
    private UnisettlePersonRoleSettings _personRoleSettings;

    protected static class PersonRoleWrapper extends IdentifiableWrapper
    {
        private static final long serialVersionUID = 1L;
        private Short entityCode;
        private boolean selected;

        public PersonRoleWrapper(Long id, String title, Short entityCode)
        {
            super(id, title);
            this.entityCode = entityCode;
        }

        public Short getEntityCode()
        {
            return entityCode;
        }

        public boolean isSelected()
        {
            return selected;
        }

        public void setSelected(boolean selected)
        {
            this.selected = selected;
        }
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<PersonRoleWrapper> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<PersonRoleWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

    public UnisettlePersonRoleSettings getPersonRoleSettings()
    {
        return _personRoleSettings;
    }

    public void setPersonRoleSettings(UnisettlePersonRoleSettings personRoleSettings)
    {
        _personRoleSettings = personRoleSettings;
    }
}
