package ru.tandemservice.unisettle.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.unisettle.entity.catalog.gen.UnisettleSettlementKindGen;

/**
 * Вид поселения
 */
public class UnisettleSettlementKind extends UnisettleSettlementKindGen implements IHierarchyItem
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }
}