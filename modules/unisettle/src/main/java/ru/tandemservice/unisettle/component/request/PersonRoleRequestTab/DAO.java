// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.PersonRoleRequestTab;

import org.hibernate.Session;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.base.bo.Person.util.PersonRoleSecModel;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisettle.dao.IUnisettleRequestDAO;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Person person = get(Person.class, model.getPersonRoleModel().getPersonId());
        model.setSec(PersonRoleSecModel.instance(person, model.getPersonRoleModel().getSecuredObject()));
        model.setAddingDisabled(!IUnisettleRequestDAO.instance.get().isAddingRequestAllowed(model.getPersonRoleModel().getPersonId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Session session = getSession();
        DynamicListDataSource<UnisettleRequest> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(UnisettleRequest.ENTITY_CLASS, "request");
        builder.add(MQExpression.eq("request", UnisettleRequest.resident().person().id(), model.getPersonRoleModel().getPersonId()));
        builder.addOrder("request", UnisettleRequest.number());
        UniBaseUtils.createPage(dataSource, builder, session);
    }
}
