/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;

/**
 * @author agolubenko
 * @since Sep 8, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setSettlement(getNotNull(UniplacesHabitablePlace.class, model.getSettlement().getId()));
        model.setResidentCount(getResidentBuilder(model).getResultCount(getSession()));
    }

    @Override
    public void prepareResidentDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getResidentDataSource(), getResidentBuilder(model), getSession());
    }

    @Override
    public void prepareContractDataSource(Model model)
    {
        DynamicListDataSource<UnisettleContract> dataSource = model.getContractDataSource();

        MQBuilder contractBuilder = new MQBuilder(UnisettleContract.ENTITY_CLASS, "contract");
        MQBuilder sub = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.contract(), "contract", UnisettleContract.id()));
        sub.add(MQExpression.eq("acc", UnisettleContractAccomodation.place(), model.getSettlement()));
        contractBuilder.add(MQExpression.exists(sub));
        contractBuilder.applyOrder(dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, contractBuilder, getSession());
    }

   private MQBuilder getResidentBuilder(Model model)
    {
        MQBuilder residentBuilder = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "resident");
        residentBuilder.addJoin("resident", UnisettleContractResident.contract(), "contract");
        MQBuilder sub = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.contract(), "contract", UnisettleContract.id()));
        sub.add(MQExpression.eq("acc", UnisettleContractAccomodation.place(), model.getSettlement()));
        sub.add(MQExpression.eq("acc", UnisettleContractAccomodation.contract().state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        residentBuilder.add(MQExpression.exists(sub));
        residentBuilder.addJoinFetch("resident", UnisettleContractResident.resident().person().identityCard(), "idc");
        residentBuilder.addOrder("idc", IdentityCardGen.P_LAST_NAME);
        residentBuilder.addOrder("idc", IdentityCardGen.P_FIRST_NAME);
        residentBuilder.addOrder("idc", IdentityCardGen.P_MIDDLE_NAME);
        return residentBuilder;
    }
}
