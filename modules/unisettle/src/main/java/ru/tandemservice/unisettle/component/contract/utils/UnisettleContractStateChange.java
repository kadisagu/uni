// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.utils;

import java.util.Arrays;
import java.util.List;

import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.UnisettleDefines;

/**
 * @author oleyba
 * @since 09.11.2010
 */
public class UnisettleContractStateChange
{
    private String buttonName;
    private String permissionKey;
    private String targetStateCode;
    private List<String> allowedStatesCodeList;
    private String componentName;
    boolean startComponent;

    public UnisettleContractStateChange(String buttonName, String permissionKey, String targetStateCode, List<String> allowedStatesCodeList, String componentName)
    {
        this.buttonName = buttonName;
        this.permissionKey = permissionKey;
        this.targetStateCode = targetStateCode;
        this.componentName = componentName;
        this.allowedStatesCodeList = allowedStatesCodeList;
        startComponent = true;
    }

    public UnisettleContractStateChange(String buttonName, String permissionKey, String targetStateCode, List<String> allowedStatesCodeList)
    {
        this.buttonName = buttonName;
        this.permissionKey = permissionKey;
        this.targetStateCode = targetStateCode;
        this.allowedStatesCodeList = allowedStatesCodeList;
        startComponent = false;
    }

    public static List<UnisettleContractStateChange> getChangeList(String permissionKeySuffix)
    {

        String permission = "change" + permissionKeySuffix + "ContractState";
        String unarchivePermission = "unarchive" + permissionKeySuffix + "Contract";
        return Arrays.asList(
            new UnisettleContractStateChange("Согласовать", permission, UnisettleDefines.CONTRACT_STATE_ACCEPTED, Arrays.asList(UnisettleDefines.CONTRACT_STATE_ACCEPTABLE)),
            new UnisettleContractStateChange("Отправить на согласование", permission, UnisettleDefines.CONTRACT_STATE_ACCEPTABLE, Arrays.asList(UnisettleDefines.CONTRACT_STATE_FORMING)),
            new UnisettleContractStateChange("Отклонить договор", permission, UnisettleDefines.CONTRACT_STATE_REJECTED, Arrays.asList(UnisettleDefines.CONTRACT_STATE_FORMING, UnisettleDefines.CONTRACT_STATE_ACCEPTABLE)),
            new UnisettleContractStateChange("Отправить в архив (выселить)", permission, UnisettleDefines.CONTRACT_STATE_ARCHIVE, Arrays.asList(UnisettleDefines.CONTRACT_STATE_ACCEPTED), UnisettleComponents.CONTRACT_ARCHIVE),
            new UnisettleContractStateChange("Восстановить", unarchivePermission, UnisettleDefines.CONTRACT_STATE_FORMING, Arrays.asList(UnisettleDefines.CONTRACT_STATE_ARCHIVE, UnisettleDefines.CONTRACT_STATE_REJECTED))
            );
    }

    public String getButtonName()
    {
        return buttonName;
    }

    public String getPermissionKey()
    {
        return permissionKey;
    }

    public String getTargetStateCode()
    {
        return targetStateCode;
    }

    public List<String> getAllowedStatesCodeList()
    {
        return allowedStatesCodeList;
    }

    public String getComponentName()
    {
        return componentName;
    }

    public boolean isStartComponent()
    {
        return startComponent;
    }
}
