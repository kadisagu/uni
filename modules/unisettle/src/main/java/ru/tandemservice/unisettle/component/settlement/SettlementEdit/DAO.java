/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;

/**
 * @author agolubenko
 * @since Sep 8, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setSettlement(getNotNull(UniplacesHabitablePlace.class, model.getSettlement().getId()));

        model.setPurposeList(IUniplacesDAO.instance.get().getHabitablePlacePurposeOptionList());
        model.setConditionList(getCatalogItemListOrderByCode(UniplacesPlaceCondition.class));
    }

    @Override
    public void update(Model model)
    {
        if (!model.getSettlement().getPlace().getPurpose().isCountBedNumber())
            model.getSettlement().setBedNumber(0);
        update(model.getSettlement().getPlace());
        update(model.getSettlement());
    }
}
