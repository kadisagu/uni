// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementMassEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.place.*;
import ru.tandemservice.unisettle.dao.IUnisettleSettlementDAO;

import java.util.List;

/**
 * @author oleyba
 * @since 15.09.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("settlement");
    static
    {
        _orderSettings.setOrders(UniplacesHabitablePlace.place().s() + "." + UniplacesPlace.P_FRACTIONAL_AREA, new OrderDescription("place", UniplacesPlace.P_AREA));
    }


    @Override
    public void prepare(final Model model)
    {
        final Session session = getSession();
        model.setBuildingList(new LazySimpleSelectModel<UniplacesBuilding>(UniplacesBuilding.class));
        model.setUnitList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesUnit.ENTITY_CLASS, alias);
                if (null == model.getBuilding())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesUnit.building().s(), model.getBuilding()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesUnit.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setFloorList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesFloor.ENTITY_CLASS, alias);
                if (null == model.getUnit())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesFloor.unit().s(), model.getUnit()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesFloor.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesFloor.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setPurposeList(IUniplacesDAO.instance.get().getHabitablePlacePurposeOptionList());
        model.setConditionList(new LazySimpleSelectModel<UniplacesPlaceCondition>(UniplacesPlaceCondition.class));
        List<OrgUnit> orgUnits = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou").add(MQExpression.notEq("ou", OrgUnit.orgUnitType().code().s(), OrgUnitTypeCodes.TOP)).<OrgUnit>getResultList(session);
        model.setOrgUnitList(new LazySimpleSelectModel<OrgUnit>(orgUnits, "fullTitle").setSearchFromStart(false).setSearchProperty("fullTitle"));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(UniplacesHabitablePlace.ENTITY_CLASS, "settlement");
        builder.addJoinFetch("settlement", UniplacesHabitablePlace.place().s(), "place");

        if (model.getFloor() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.floor().s(), model.getFloor()));
        else if (model.getUnit() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.floor().unit().s(), model.getUnit()));
        else if (model.getBuilding() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.floor().unit().building().s(), model.getBuilding()));
        if (model.getPurpose() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.purpose().s(), model.getPurpose()));
        if (model.getCondition() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.condition().s(), model.getCondition()));
        if (model.getOrgUnit() != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.responsibleOrgUnit().s(), model.getOrgUnit()));

        builder.add(MQExpression.in("place", UniplacesPlace.purpose().s(), IUniplacesDAO.instance.get().getHabitablePlacePurposeList()));

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        long count = builder.getResultCount(getSession());
        model.getDataSource().setCountRow(count);
        model.getDataSource().setTotalSize(count);
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());

        IUnisettleSettlementDAO.instance.get().wrapDataSource(model.getDataSource());
    }

    @Override
    public void update(Model model)
    {
        for (IEntity checked : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            UniplacesHabitablePlace place = (UniplacesHabitablePlace) ((ViewWrapper) checked).getEntity();
            if (model.getNewCondition() != null)
                place.getPlace().setCondition(model.getNewCondition());
            if (model.getNewPurpose() != null)
                place.getPlace().setPurpose(model.getNewPurpose());
            update(place.getPlace());
            if (model.getNewPurpose() != null && !model.getNewPurpose().isCountBedNumber())
                place.setBedNumber(0);
            else if (model.getNewBedNumber() != null)
                place.setBedNumber(model.getNewBedNumber());
            update(place);
        }
        ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).reset();
    }
}
