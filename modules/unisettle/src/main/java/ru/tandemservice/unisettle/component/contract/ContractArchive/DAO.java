// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractArchive;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisettle.dao.IUnisettleContractDAO;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;

import java.util.Date;

/**
 * @author oleyba
 * @since 10.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        UnisettleContract contract = (UnisettleContract) get(model.getContractId());
        model.setContract(contract);
        if (null == contract.getActualCheckoutDate())
            contract.setActualCheckoutDate(new Date());

    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (CommonBaseDateUtil.isBefore(model.getContract().getActualCheckoutDate(), model.getContract().getCheckinDate()))
            errors.add("Дата фактического выселения должна быть не раньше даты заселения.", "actualCheckoutDate");
    }

    @Override
    public void update(Model model)
    {
        IUnisettleContractDAO.instance.get().doChangeContractState(model.getContract(), model.getStateChange());
    }
}
