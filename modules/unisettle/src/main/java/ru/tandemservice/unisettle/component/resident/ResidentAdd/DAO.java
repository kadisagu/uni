// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.ResidentAdd;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 21.10.2010
 */
public class DAO extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.DAO<UnisettleResident, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }

    @Override
    public void create(Model model)
    {
        Session session = getSession();
        Person person = model.getPerson();
        IdentityCard identityCard = person.getIdentityCard();

        PersonManager.instance().dao().checkPersonUnique(person);

        DatabaseFile photo = new DatabaseFile();
        session.save(photo);

        identityCard.setPhoto(photo);
        session.save(identityCard);

        session.save(person.getContactData());

        session.save(person);

        identityCard.setPerson(person);
        session.update(identityCard);

        createOnBasis(model.getResident(), person);
    }

    @Override
    public void createOnBasis(Model model)
    {
        createOnBasis(model.getResident(), getBasisPerson(model));
    }

    private void createOnBasis(UnisettleResident resident, Person person)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), person.getId() + ".unisettleResident");

        if (!getList(UnisettleResident.class, UnisettleResident.L_PERSON, person).isEmpty())
            throw new ApplicationException("Для данной персоны уже зарегистрирован поселяемый.");

        resident.setPerson(person);
        getSession().save(resident);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        PersonManager.instance().dao().checkIdentityCard(model.getPerson().getIdentityCard(), errors);
    }
}
