package ru.tandemservice.unisettle.entity.settlement;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unisettle.entity.settlement.gen.UnisettleResidentGen;

/**
 * Поселяемый
 */
public class UnisettleResident extends UnisettleResidentGen implements ITitled
{
    @Override
    public String getTitle()
    {
        return "Поселяемый";
    }

    @Override
    public String getFullTitle()
    {
        return "Поселяемый";
    }

    @Override
    public String getContextTitle()
    {
        return getFullTitle();
    }
}