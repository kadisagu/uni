package ru.tandemservice.unisettle.entity.settlement;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.unisettle.entity.settlement.gen.UnisettleContractGen;

/**
 * Договор на поселение
 */
public abstract class UnisettleContract extends UnisettleContractGen
{
    public static final String P_TITLE = "title";

    @Override
    @EntityDSLSupport
    public String getTitle()
    {
        return getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getRegDate());
    }

    @EntityDSLSupport
    public void setTitle(String title)
    {
    }
}