// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractAccomodationAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;

/**
 * @author oleyba
 * @since 10.11.2010
 */
@Input({@Bind(key = "contractId", binding = "contractId"),
        @Bind(key = "accomodationId", binding = "accomodation.id")})
public class Model
{
    private Long contractId;
    private UnisettleContract contract;
    private UnisettleContractAccomodation accomodation = new UnisettleContractAccomodation();

    private ISelectModel buildingList;
    private ISelectModel unitList;
    private ISelectModel floorList;
    private ISelectModel placeList;

    private UniplacesBuilding building;
    private UniplacesUnit unit;
    private UniplacesFloor floor;
    private IdentifiableWrapper<UniplacesHabitablePlace> place;

    private boolean showArea;
    private boolean editForm;
    private boolean showAll;
    private boolean countRegistration;
    private boolean countCheckout;

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }

    public UnisettleContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleContract contract)
    {
        this.contract = contract;
    }

    public UnisettleContractAccomodation getAccomodation()
    {
        return accomodation;
    }

    public void setAccomodation(UnisettleContractAccomodation accomodation)
    {
        this.accomodation = accomodation;
    }

    public ISelectModel getBuildingList()
    {
        return buildingList;
    }

    public void setBuildingList(ISelectModel buildingList)
    {
        this.buildingList = buildingList;
    }

    public ISelectModel getUnitList()
    {
        return unitList;
    }

    public void setUnitList(ISelectModel unitList)
    {
        this.unitList = unitList;
    }

    public ISelectModel getFloorList()
    {
        return floorList;
    }

    public void setFloorList(ISelectModel floorList)
    {
        this.floorList = floorList;
    }

    public ISelectModel getPlaceList()
    {
        return placeList;
    }

    public void setPlaceList(ISelectModel placeList)
    {
        this.placeList = placeList;
    }

    public UniplacesBuilding getBuilding()
    {
        return building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        this.building = building;
    }

    public UniplacesUnit getUnit()
    {
        return unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        this.unit = unit;
    }

    public UniplacesFloor getFloor()
    {
        return floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        this.floor = floor;
    }

    public boolean isShowArea()
    {
        return showArea;
    }

    public void setShowArea(boolean showArea)
    {
        this.showArea = showArea;
    }

    public boolean isEditForm()
    {
        return editForm;
    }

    public void setEditForm(boolean editForm)
    {
        this.editForm = editForm;
    }

    public boolean isShowAll()
    {
        return showAll;
    }

    public void setShowAll(boolean showAll)
    {
        this.showAll = showAll;
    }

    public boolean isCountRegistration()
    {
        return countRegistration;
    }

    public void setCountRegistration(boolean countRegistration)
    {
        this.countRegistration = countRegistration;
    }

    public boolean isCountCheckout()
    {
        return countCheckout;
    }

    public void setCountCheckout(boolean countCheckout)
    {
        this.countCheckout = countCheckout;
    }

    public IdentifiableWrapper<UniplacesHabitablePlace> getPlace()
    {
        return place;
    }

    public void setPlace(IdentifiableWrapper<UniplacesHabitablePlace> place)
    {
        this.place = place;
    }
}
