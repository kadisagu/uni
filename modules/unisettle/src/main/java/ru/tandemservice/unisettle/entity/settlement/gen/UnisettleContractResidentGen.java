package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проживающий по договору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleContractResidentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident";
    public static final String ENTITY_NAME = "unisettleContractResident";
    public static final int VERSION_HASH = 214034941;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTRACT = "contract";
    public static final String L_RESIDENT = "resident";
    public static final String P_AREA = "area";
    public static final String P_BED_NUMBER = "bedNumber";
    public static final String L_RELATION_DEGREE = "relationDegree";
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    private UnisettleContract _contract;     // Договор на поселение
    private UnisettleResident _resident;     // Поселяемый
    private long _area;     // Площадь, кв. м
    private int _bedNumber;     // Число койко-мест
    private RelationDegree _relationDegree;     // Степень родства с нанимателем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор на поселение. Свойство не может быть null.
     */
    @NotNull
    public UnisettleContract getContract()
    {
        return _contract;
    }

    /**
     * @param contract Договор на поселение. Свойство не может быть null.
     */
    public void setContract(UnisettleContract contract)
    {
        dirty(_contract, contract);
        _contract = contract;
    }

    /**
     * @return Поселяемый. Свойство не может быть null.
     */
    @NotNull
    public UnisettleResident getResident()
    {
        return _resident;
    }

    /**
     * @param resident Поселяемый. Свойство не может быть null.
     */
    public void setResident(UnisettleResident resident)
    {
        dirty(_resident, resident);
        _resident = resident;
    }

    /**
     * @return Площадь, кв. м. Свойство не может быть null.
     */
    @NotNull
    public long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь, кв. м. Свойство не может быть null.
     */
    public void setArea(long area)
    {
        dirty(_area, area);
        _area = area;
    }

    /**
     * @return Число койко-мест. Свойство не может быть null.
     */
    @NotNull
    public int getBedNumber()
    {
        return _bedNumber;
    }

    /**
     * @param bedNumber Число койко-мест. Свойство не может быть null.
     */
    public void setBedNumber(int bedNumber)
    {
        dirty(_bedNumber, bedNumber);
        _bedNumber = bedNumber;
    }

    /**
     * @return Степень родства с нанимателем.
     */
    public RelationDegree getRelationDegree()
    {
        return _relationDegree;
    }

    /**
     * @param relationDegree Степень родства с нанимателем.
     */
    public void setRelationDegree(RelationDegree relationDegree)
    {
        dirty(_relationDegree, relationDegree);
        _relationDegree = relationDegree;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisettleContractResidentGen)
        {
            setContract(((UnisettleContractResident)another).getContract());
            setResident(((UnisettleContractResident)another).getResident());
            setArea(((UnisettleContractResident)another).getArea());
            setBedNumber(((UnisettleContractResident)another).getBedNumber());
            setRelationDegree(((UnisettleContractResident)another).getRelationDegree());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleContractResidentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleContractResident.class;
        }

        public T newInstance()
        {
            return (T) new UnisettleContractResident();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contract":
                    return obj.getContract();
                case "resident":
                    return obj.getResident();
                case "area":
                    return obj.getArea();
                case "bedNumber":
                    return obj.getBedNumber();
                case "relationDegree":
                    return obj.getRelationDegree();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contract":
                    obj.setContract((UnisettleContract) value);
                    return;
                case "resident":
                    obj.setResident((UnisettleResident) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
                case "bedNumber":
                    obj.setBedNumber((Integer) value);
                    return;
                case "relationDegree":
                    obj.setRelationDegree((RelationDegree) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contract":
                        return true;
                case "resident":
                        return true;
                case "area":
                        return true;
                case "bedNumber":
                        return true;
                case "relationDegree":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contract":
                    return true;
                case "resident":
                    return true;
                case "area":
                    return true;
                case "bedNumber":
                    return true;
                case "relationDegree":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contract":
                    return UnisettleContract.class;
                case "resident":
                    return UnisettleResident.class;
                case "area":
                    return Long.class;
                case "bedNumber":
                    return Integer.class;
                case "relationDegree":
                    return RelationDegree.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisettleContractResident> _dslPath = new Path<UnisettleContractResident>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleContractResident");
    }
            

    /**
     * @return Договор на поселение. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getContract()
     */
    public static UnisettleContract.Path<UnisettleContract> contract()
    {
        return _dslPath.contract();
    }

    /**
     * @return Поселяемый. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getResident()
     */
    public static UnisettleResident.Path<UnisettleResident> resident()
    {
        return _dslPath.resident();
    }

    /**
     * @return Площадь, кв. м. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @return Число койко-мест. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getBedNumber()
     */
    public static PropertyPath<Integer> bedNumber()
    {
        return _dslPath.bedNumber();
    }

    /**
     * @return Степень родства с нанимателем.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getRelationDegree()
     */
    public static RelationDegree.Path<RelationDegree> relationDegree()
    {
        return _dslPath.relationDegree();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getFractionalArea()
     */
    public static SupportedPropertyPath<Double> fractionalArea()
    {
        return _dslPath.fractionalArea();
    }

    public static class Path<E extends UnisettleContractResident> extends EntityPath<E>
    {
        private UnisettleContract.Path<UnisettleContract> _contract;
        private UnisettleResident.Path<UnisettleResident> _resident;
        private PropertyPath<Long> _area;
        private PropertyPath<Integer> _bedNumber;
        private RelationDegree.Path<RelationDegree> _relationDegree;
        private SupportedPropertyPath<Double> _fractionalArea;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор на поселение. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getContract()
     */
        public UnisettleContract.Path<UnisettleContract> contract()
        {
            if(_contract == null )
                _contract = new UnisettleContract.Path<UnisettleContract>(L_CONTRACT, this);
            return _contract;
        }

    /**
     * @return Поселяемый. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getResident()
     */
        public UnisettleResident.Path<UnisettleResident> resident()
        {
            if(_resident == null )
                _resident = new UnisettleResident.Path<UnisettleResident>(L_RESIDENT, this);
            return _resident;
        }

    /**
     * @return Площадь, кв. м. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(UnisettleContractResidentGen.P_AREA, this);
            return _area;
        }

    /**
     * @return Число койко-мест. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getBedNumber()
     */
        public PropertyPath<Integer> bedNumber()
        {
            if(_bedNumber == null )
                _bedNumber = new PropertyPath<Integer>(UnisettleContractResidentGen.P_BED_NUMBER, this);
            return _bedNumber;
        }

    /**
     * @return Степень родства с нанимателем.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getRelationDegree()
     */
        public RelationDegree.Path<RelationDegree> relationDegree()
        {
            if(_relationDegree == null )
                _relationDegree = new RelationDegree.Path<RelationDegree>(L_RELATION_DEGREE, this);
            return _relationDegree;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident#getFractionalArea()
     */
        public SupportedPropertyPath<Double> fractionalArea()
        {
            if(_fractionalArea == null )
                _fractionalArea = new SupportedPropertyPath<Double>(UnisettleContractResidentGen.P_FRACTIONAL_AREA, this);
            return _fractionalArea;
        }

        public Class getEntityClass()
        {
            return UnisettleContractResident.class;
        }

        public String getEntityName()
        {
            return "unisettleContractResident";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getFractionalArea();
}
