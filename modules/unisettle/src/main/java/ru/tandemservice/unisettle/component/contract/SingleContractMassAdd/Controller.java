// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleContractMassAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;

/**
 * @author oleyba
 * @since 15.11.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<Model.LineWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn().setSelectCaption("Выбор").setClickable(false).setOrderable(false).setDisabledProperty("disabled"));
        dataSource.addColumn(new SimpleColumn("ФИО", Model.LineWrapper.resident().person().fio().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Model.LineWrapper.resident().person().identityCard().sex().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер зач. книжки", "bookNumbers").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Гражданство", Model.LineWrapper.resident().person().identityCard().citizenship().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Группа", "groups").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Курс", "courses").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Место поселения", UniplacesHabitablePlace.addressTitle(), Model.LineWrapper.place()).setOrderable(false));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }

    public void onClickShow(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).clear();
        onClickShow(component);
    }
}