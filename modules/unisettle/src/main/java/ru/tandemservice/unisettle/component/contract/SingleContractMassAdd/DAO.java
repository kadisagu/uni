// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleContractMassAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonDormitoryBenefit;
import org.tandemframework.shared.person.catalog.entity.DormitoryBenefit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractState;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractType;
import ru.tandemservice.unisettle.entity.settlement.*;

import java.util.*;

/**
 * @author oleyba
 * @since 15.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setСontractTypeList(new LazySimpleSelectModel<>(UnisettleContractType.class));
        model.setSexList(new LazySimpleSelectModel<>(getCatalogItemList(Sex.class)));
        model.setCountryList(new CountryAutocompleteModel());
        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setFormativeOrgUnitList(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setDevelopPeriodListModel(EducationCatalogsManager.getDevelopPeriodSelectModel());
        model.setBenefitList(new LazySimpleSelectModel<>(getCatalogItemList(DormitoryBenefit.class)));
        model.setBuildingList(new LazySimpleSelectModel<>(UniplacesBuilding.class));
        model.setUnitList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesUnit.ENTITY_CLASS, alias);
                if (null == model.getBuilding())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesUnit.building().s(), model.getBuilding()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesUnit.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setFloorList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesFloor.ENTITY_CLASS, alias);
                if (null == model.getUnit())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesFloor.unit().s(), model.getUnit()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesFloor.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesFloor.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setPlaceList(new UniQueryFullCheckSelectModel(UniplacesHabitablePlace.place().number().s())
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesHabitablePlace.ENTITY_CLASS, alias);
                // todo
                if (null != model.getFloor())
                    builder.add(MQExpression.eq(alias, UniplacesHabitablePlace.place().floor().s(), model.getFloor()));
                if (null == model.getSex())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesHabitablePlace.place().purpose().sex(), model.getSex()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesHabitablePlace.place().title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesHabitablePlace.place().number().s());
                return builder;
            }
        }.setPageSize(100));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        if (null == model.getSex())
        {
            UniBaseUtils.createPage(model.getDataSource(), Collections.<Model.LineWrapper>emptyList());
            return;
        }

        MQBuilder residentBuilder = new MQBuilder(UnisettleResident.ENTITY_CLASS, "resident");

        MQBuilder contract = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "contract_resident");
        contract.add(MQExpression.eqProperty("contract_resident", UnisettleContractResident.resident().person(), "resident", UnisettleResident.person()));
        contract.add(MQExpression.in("contract_resident", UnisettleContractResident.contract().state().code(), UnisettleDefines.CONTRACT_STATES_ACTIVE));
        residentBuilder.add(MQExpression.notExists(contract));

        residentBuilder.addJoin("resident", UnisettleResident.person().identityCard(), "idc");
        residentBuilder.add(MQExpression.eq("idc", IdentityCard.sex(), model.getSex()));
        if (null != model.getCitizenship())
            residentBuilder.add(MQExpression.eq("idc", IdentityCard.citizenship(), model.getCitizenship()));
        if (null != model.getBenefit())
        {
            MQBuilder benefit = new MQBuilder(PersonDormitoryBenefit.ENTITY_CLASS, "benefit");
            benefit.add(MQExpression.eqProperty("benefit", PersonDormitoryBenefit.person(), "resident", UnisettleResident.person()));
            benefit.add(MQExpression.eq("benefit", PersonDormitoryBenefit.benefit(), model.getBenefit()));
            residentBuilder.add(MQExpression.exists(benefit));
        }

        residentBuilder.addDomain("student", Student.ENTITY_CLASS);
        residentBuilder.addSelect("student");
        residentBuilder.add(MQExpression.eqProperty("student", Student.person(), "resident", UnisettleResident.person()));
        if (null != model.getFormativeOrgUnit())
            residentBuilder.add(MQExpression.eq("student", Student.educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnit()));
        if (null != model.getCourseList() && !model.getCourseList().isEmpty())
            residentBuilder.add(MQExpression.in("student", Student.course(), model.getCourseList()));
        if (null != model.getDevelopPeriodList() && !model.getDevelopPeriodList().isEmpty())
            residentBuilder.add(MQExpression.in("student", Student.educationOrgUnit().developPeriod(), model.getDevelopPeriodList()));
        residentBuilder.addOrder("idc", IdentityCard.lastName());
        residentBuilder.addOrder("idc", IdentityCard.firstName());
        residentBuilder.addOrder("idc", IdentityCard.middleName());
        residentBuilder.addOrder("student", Student.id());

        Map<UnisettleResident, Model.LineWrapper> wrapperMap = new HashMap<>();

        for (Object[] row : residentBuilder.<Object[]>getResultList(getSession()))
        {
            UnisettleResident resident = (UnisettleResident) row[0];
            Student student = (Student) row[1];
            Model.LineWrapper wrapper = wrapperMap.get(resident);
            if (null == wrapper)
                wrapperMap.put(resident, wrapper = new Model.LineWrapper(resident));
            wrapper.addStudent(student);
        }

        MQBuilder requests = new MQBuilder(UnisettleRequest.ENTITY_CLASS, "request");
        requests.add(MQExpression.in("request", UnisettleRequest.resident(), wrapperMap.keySet()));
        requests.add(MQExpression.notEq("request", UnisettleRequest.state().code(), UnisettleDefines.REQUEST_STATE_DECLINED));
        requests.add(MQExpression.notEq("request", UnisettleRequest.state().code(), UnisettleDefines.REQUEST_STATE_ARCHIVE));
        for (UnisettleRequest request : requests.<UnisettleRequest>getResultList(getSession()))
            wrapperMap.get(request.getResident()).processRequest(request);

        MQBuilder places = new MQBuilder(UniplacesHabitablePlace.ENTITY_CLASS, "place");
        if (model.getPlace() != null)
            places.add(MQExpression.eq("place", UniplacesHabitablePlace.id(), model.getPlace().getId()));
        else if (model.getFloor() != null)
            places.add(MQExpression.eq("place", UniplacesHabitablePlace.place().floor(), model.getFloor()));
        else if (model.getUnit() != null)
            places.add(MQExpression.eq("place", UniplacesHabitablePlace.place().floor().unit(), model.getUnit()));
        else if (model.getBuilding() != null)
            places.add(MQExpression.eq("place", UniplacesHabitablePlace.place().floor().unit().building(), model.getBuilding()));
        places.add(MQExpression.eq("place", UniplacesHabitablePlace.place().purpose().sex(), model.getSex()));
        places.add(MQExpression.eq("place", UniplacesHabitablePlace.place().purpose().countBedNumber(), Boolean.TRUE));
        places.add(MQExpression.isNotNull("place", UniplacesHabitablePlace.bedNumber()));

        Map<UniplacesHabitablePlace, Integer> placesMap = new HashMap<>();
        for (UniplacesHabitablePlace place : places.<UniplacesHabitablePlace>getResultList(getSession()))
            placesMap.put(place, place.getBedNumber());

        MQBuilder accomodations = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accomodations.add(MQExpression.in("acc", UnisettleContractAccomodation.place(), places));
        accomodations.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accomodations.add(MQExpression.in("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATES_ACTIVE));
        for (UnisettleContractAccomodation acc : accomodations.<UnisettleContractAccomodation>getResultList(getSession()))
            placesMap.put(acc.getPlace(), placesMap.get(acc.getPlace()) - 1);

        places.addOrder("place", UniplacesHabitablePlace.place().number());
        int rowNum = 0;
        List<Model.LineWrapper> lineList = new ArrayList<>(wrapperMap.values());
        Collections.sort(lineList);
        for (UniplacesHabitablePlace place : places.<UniplacesHabitablePlace>getResultList(getSession()))
        {
            Integer freeSpace = placesMap.get(place);
            if (freeSpace <= 0) continue;
            if (rowNum >= lineList.size()) continue;
            lineList.get(rowNum++).setPlace(place);
            placesMap.put(place, freeSpace - 1);
        }
        UniBaseUtils.createPage(model.getDataSource(), lineList);
    }

    @Override
    public void update(Model model)
    {
        long number = model.getStartNumber();
        UnisettleContractState state = get(UnisettleContractState.class, UnisettleContractState.code().s(), UnisettleDefines.CONTRACT_STATE_FORMING);
        for (IEntity checked : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            Model.LineWrapper wrapper = (Model.LineWrapper) checked;
            UnisettleSingleBedContract contract = new UnisettleSingleBedContract();
            contract.setNumber(String.valueOf(number++));
            contract.setRegDate(model.getRegDate());
            contract.setCheckinDate(model.getCheckinDate());
            contract.setCheckoutDate(model.getCheckoutDate());
            contract.setType(model.getType());
            contract.setState(state);
            contract.setResident(wrapper.getResident());
            save(contract);
            UnisettleContractResident resident = new UnisettleContractResident();
            resident.setContract(contract);
            resident.setResident(wrapper.getResident());
            resident.setArea(0);
            resident.setBedNumber(1);
            save(resident);
            UnisettleContractAccomodation acc = new UnisettleContractAccomodation();
            acc.setContract(contract);
            acc.setPlace(wrapper.getPlace());
            acc.setArea(0);
            save(acc);
        }
        ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).reset();
    }
}
