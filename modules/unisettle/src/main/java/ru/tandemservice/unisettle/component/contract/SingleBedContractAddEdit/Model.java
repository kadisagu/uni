// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleBedContractAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;

/**
 * @author oleyba
 * @since 26.10.2010
 */
@Input({@Bind(key = "contractId", binding = "contract.id"),
        @Bind(key = "residentId", binding = "resident.id")})
public class Model
{
    private UnisettleSingleBedContract contract = new UnisettleSingleBedContract();
    private UnisettleResident resident = new UnisettleResident();

    private ISelectModel сontractTypeList;

    public boolean isEditForm()
    {
        return getContract().getId() != null;
    }

    public UnisettleSingleBedContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleSingleBedContract contract)
    {
        this.contract = contract;
    }

    public UnisettleResident getResident()
    {
        return resident;
    }

    public void setResident(UnisettleResident resident)
    {
        this.resident = resident;
    }

    public ISelectModel getСontractTypeList()
    {
        return сontractTypeList;
    }

    public void setСontractTypeList(ISelectModel сontractTypeList)
    {
        this.сontractTypeList = сontractTypeList;
    }
}
