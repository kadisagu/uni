// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.RequestAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "request.id"),
        @Bind(key = "personId", binding = "person.id"),
        @Bind(key = "decline", binding = "decline")
})
public class Model
{
    private UnisettleRequest request = new UnisettleRequest();
    private boolean decline;
    private Person person = new Person();

    public String getFormTitle()
    {
        if (decline)
            return "Отказать в предоставлении места в общежитии";
        else if (getRequest().getId() == null)
            return "Добавить заявление";
        else
            return "Редактировать заявление";
    }

    public UnisettleRequest getRequest()
    {
        return request;
    }

    public void setRequest(UnisettleRequest request)
    {
        this.request = request;
    }

    public boolean isDecline()
    {
        return decline;
    }

    public void setDecline(boolean decline)
    {
        this.decline = decline;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }
}
