// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleBedContractPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 09.11.2010
 */
@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "contract.id"))
public class Model
{
    private UnisettleSingleBedContract contract = new UnisettleSingleBedContract();
    private UnisettleContractResident contractResident;
    private UnisettleContractAccomodation accomodation;

    private List<UnisettleContractStateChange> stateChangeList = UnisettleContractStateChange.getChangeList("Single");
    private UnisettleContractStateChange currentStateChange;

    public Map<String, Object> getResidentLinkParams()
    {
        if (null == getContract().getResident())
            return null;
        return new ParametersMap()
            .add("selectedTab", "residentTab")
            .add("selectedDataTab", "residentContractTab")
            .add(PublisherActivator.PUBLISHER_ID_KEY, getContract().getResident().getId());
    }

    public Map<String, Object> getContractResidentLinkParams()
    {
        if (null == getResident())
            return null;
        return new ParametersMap()
            .add("selectedTab", "residentTab")
            .add("selectedDataTab", "residentContractTab")
            .add(PublisherActivator.PUBLISHER_ID_KEY, getResident().getId());
    }

    public boolean isStateChangeAllowed()
    {
        return getCurrentStateChange() != null && getCurrentStateChange().getAllowedStatesCodeList().contains(getContract().getState().getCode());
    }

    public boolean isResidentExists()
    {
        return getResident() != null;
    }

    public boolean isAccomodationExists()
    {
        return accomodation != null;
    }

    public UniplacesPlace getPlace()
    {
        return (accomodation == null ? null : accomodation.getPlace().getPlace());
    }

    public boolean isEditDisabled()
    {
        return !UnisettleDefines.CONTRACT_STATE_FORMING.equals(getContract().getState().getCode());
    }

    // accessors

    public UnisettleSingleBedContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleSingleBedContract contract)
    {
        this.contract = contract;
    }

    public UnisettleResident getResident()
    {
        return getContractResident() == null ? null : getContractResident().getResident();
    }

    public UnisettleContractAccomodation getAccomodation()
    {
        return accomodation;
    }

    public void setAccomodation(UnisettleContractAccomodation accomodation)
    {
        this.accomodation = accomodation;
    }

    public List<UnisettleContractStateChange> getStateChangeList()
    {
        return stateChangeList;
    }

    public void setStateChangeList(List<UnisettleContractStateChange> stateChangeList)
    {
        this.stateChangeList = stateChangeList;
    }

    public UnisettleContractStateChange getCurrentStateChange()
    {
        return currentStateChange;
    }

    public void setCurrentStateChange(UnisettleContractStateChange currentStateChange)
    {
        this.currentStateChange = currentStateChange;
    }

    public UnisettleContractResident getContractResident()
    {
        return contractResident;
    }

    public void setContractResident(UnisettleContractResident contractResident)
    {
        this.contractResident = contractResident;
    }
}
