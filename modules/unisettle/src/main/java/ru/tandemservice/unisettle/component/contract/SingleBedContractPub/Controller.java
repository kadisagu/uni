// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SingleBedContractPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unisettle.UnisettleComponents;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;

/**
 * @author oleyba
 * @since 09.11.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickEditContract(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.SINGLE_BED_CONTRACT_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId())));
    }

    public void onClickChangeState(IBusinessComponent component)
    {
        UnisettleContractStateChange stateChange = (UnisettleContractStateChange) component.getListenerParameter();
        Model model = getModel(component);
        if (stateChange.isStartComponent())
            component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_ARCHIVE,
                                                             new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getContract().getId()).add("stateChange", stateChange)));
        else
            getDao().doChangeState(model, stateChange);
    }

    public void onClickAddResident(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_RESIDENT_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId())));
    }

    public void onClickDeleteResident(IBusinessComponent component)
    {
        getDao().deleteResident(getModel(component));
        onRefreshComponent(component);
    }

    public void onClickAddAccomodation(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(UnisettleComponents.CONTRACT_ACCOMODATION_ADD_EDIT,
                                                         new ParametersMap().add("contractId", model.getContract().getId())));
    }

    public void onClickDeleteAccomodation(IBusinessComponent component)
    {
        getDao().deleteAccomodation(getModel(component));
        onRefreshComponent(component);
    }

    @SuppressWarnings("unchecked")
    public void onClickPrintContract(IBusinessComponent component)
    {
        Model model = getModel(component);
        byte[] content = getDao().printContract(model);
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Договор.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", false).add("extension", "rtf")));
    }

}