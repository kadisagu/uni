// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.ResidentContractTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 22.10.2010
 */
@State({
        @Bind(key = "personRoleModel", binding = "personRoleModel")
})
public class Model
{
    private ISecureRoleContext _personRoleModel;

    private UnisettleResident resident;
    private UnisettleContract contract;
    private List<UniplacesHabitablePlace> accomodationList = new ArrayList<UniplacesHabitablePlace>();

    private DynamicListDataSource<UnisettleContract> dataSource;

    private boolean settled = false;

    public String getContractPubComponentName()
    {
        if (getContract() == null)
            return "";
        if (getContract() instanceof UnisettleSingleBedContract)
            return "ru.tandemservice.unisettle.component.contract.SingleBedContractPub";
        if (getContract() instanceof UnisettleSumAreaContract)
            return "ru.tandemservice.unisettle.component.contract.SumAreaContractPub";
        return "";        
    }

    public Map<String, Object> getResidentLinkParams()
    {
        if (null == getContract())
            return null;
        return new ParametersMap()
            .add("selectedTab", "residentTab")
            .add("selectedDataTab", "residentContractTab")
            .add(PublisherActivator.PUBLISHER_ID_KEY, getContract().getResident().getId());
    }

    public ISecureRoleContext getPersonRoleModel()
    {
        return _personRoleModel;
    }

    public void setPersonRoleModel(ISecureRoleContext personRoleModel)
    {
        _personRoleModel = personRoleModel;
    }

    public UnisettleResident getResident()
    {
        return resident;
    }

    public void setResident(UnisettleResident resident)
    {
        this.resident = resident;
    }

    public UnisettleContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleContract contract)
    {
        this.contract = contract;
    }

    public List<UniplacesHabitablePlace> getAccomodationList()
    {
        return accomodationList;
    }

    public void setAccomodationList(List<UniplacesHabitablePlace> accomodationList)
    {
        this.accomodationList = accomodationList;
    }

    public DynamicListDataSource<UnisettleContract> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<UnisettleContract> dataSource)
    {
        this.dataSource = dataSource;
    }

    public boolean isSettled()
    {
        return settled;
    }

    public void setSettled(boolean settled)
    {
        this.settled = settled;
    }
}
