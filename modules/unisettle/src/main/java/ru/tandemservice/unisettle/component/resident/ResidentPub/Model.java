// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.ResidentPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import ru.tandemservice.uni.component.person.util.ISecureRoleContextOwner;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 22.10.2010
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "resident.id"),
        @Bind(key = "selectedTab"),
        @Bind(key = "selectedDataTab")
})
@Output({
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = ISecureRoleContextOwner.SECURE_ROLE_CONTEXT),
        @Bind(key = "residentId", binding = "resident.id")
})
public class Model implements ISecureRoleContextOwner
{
    private UnisettleResident resident = new UnisettleResident();

    private String _selectedTab;
    private String _selectedDataTab;

    @Override
    public ISecureRoleContext getSecureRoleContext()
    {
        return SecureRoleContext.instance(resident);
    }

    public UnisettleResident getResident()
    {
        return resident;
    }

    public void setResident(UnisettleResident resident)
    {
        this.resident = resident;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return _selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        _selectedDataTab = selectedDataTab;
    }
}
