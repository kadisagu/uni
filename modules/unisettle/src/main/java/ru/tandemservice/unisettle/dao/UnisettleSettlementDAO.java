// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.dao;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;

import java.util.*;

/**
 * @author oleyba
 * @since 12.11.2010
 */
public class UnisettleSettlementDAO extends UniBaseDao implements IUnisettleSettlementDAO
{
    @Override
    public List<IdentifiableWrapper<UniplacesHabitablePlace>> getWrappedPlacesList(UniplacesFloor floor, UnisettleContract contract, boolean countBedNumber, boolean excludeNonFree, boolean countCheckout, boolean countRegistration)
    {
        if (null == floor)
            return Collections.emptyList();

        MQBuilder placesBuilder = new MQBuilder(UniplacesHabitablePlace.ENTITY_CLASS, "hplace");
        placesBuilder.addJoinFetch("hplace", UniplacesHabitablePlace.place(), "place");
        placesBuilder.add(MQExpression.eq("place", UniplacesPlace.purpose().countBedNumber(), countBedNumber));
        placesBuilder.add(MQExpression.eq("place", UniplacesPlace.floor(), floor));
        MQBuilder sub = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        sub.add(MQExpression.eq("acc", UnisettleContractAccomodation.contract(), contract));
        sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.place().id().s(), "hplace", "id"));
        placesBuilder.add(MQExpression.notExists(sub));
        placesBuilder.addOrder("place", UniplacesPlace.number());
        List<UniplacesHabitablePlace> places = placesBuilder.getResultList(getSession());

        if (places.isEmpty())
            return Collections.emptyList();

        return processPlaces(contract, countBedNumber, excludeNonFree, countCheckout, countRegistration, places);
    }

    @Override
    public IdentifiableWrapper<UniplacesHabitablePlace> getWrappedPlace(UnisettleContractAccomodation accomodation)
    {
        return processPlaces(accomodation.getContract(), accomodation.getPlace().getPlace().getPurpose().isCountBedNumber(), false, false, false, Arrays.asList(accomodation.getPlace())).get(0);
    }

    private List<IdentifiableWrapper<UniplacesHabitablePlace>> processPlaces(UnisettleContract contract, boolean countBedNumber, boolean excludeNonFree, boolean countCheckout, boolean countRegistration, List<UniplacesHabitablePlace> places)
    {
        MQBuilder accBuilder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accBuilder.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accBuilder.addJoinFetch("contract", UnisettleContract.type(), "type");
        accBuilder.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        accBuilder.add(MQExpression.in("acc", UnisettleContractAccomodation.place(), places));
        accBuilder.add(UniMQExpression.lessOrEq("contract", UnisettleContract.checkinDate().s(), contract.getCheckinDate()));
        List<UnisettleContractAccomodation> accList = accBuilder.getResultList(getSession());

        Map<UniplacesHabitablePlace, PlaceStatistics> statisticsMap = new HashMap<>();
        for (UniplacesHabitablePlace place : places)
            statisticsMap.put(place, new PlaceStatistics());
        for (UnisettleContractAccomodation acc : accList)
            addToStatistics(
                    statisticsMap.get(acc.getPlace()),
                    acc.getContract().getCheckoutDate(),
                    UnisettleDefines.CONTRACT_TYPE_REGISTRATION_ONLY.equals(acc.getContract().getType().getCode()),
                    acc.getContract().getCheckoutDate() != null && !acc.getContract().getCheckoutDate().before(contract.getCheckinDate()),
                    countBedNumber ? 1 : acc.getArea());
        List<IdentifiableWrapper<UniplacesHabitablePlace>> wrappers = new ArrayList<>();

        for (UniplacesHabitablePlace habitablePlace : places)
        {
            UniplacesPlace place = habitablePlace.getPlace();
            PlaceStatistics statistics = statisticsMap.get(habitablePlace);
            String area = place.getFractionalArea() == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(place.getFractionalArea());
            int beds = habitablePlace.getBedNumber();

            if (!statistics.inited)
            {
                String title = countBedNumber ? place.getNumber() + " -/-/" + beds +", " + area : place.getNumber() + " -/" + area;
                wrappers.add(new IdentifiableWrapper<UniplacesHabitablePlace>(habitablePlace.getId(),title));
                continue;
            }

            if (excludeNonFree)
            {
                long freeSpace = countBedNumber ? beds : (place.getArea() == null ? 0 : place.getArea() * 100);

                if (countCheckout)
                    freeSpace -= statistics.takenWithCheckout;
                else
                    freeSpace -= statistics.taken;
                if (countRegistration)
                    freeSpace += countCheckout ? statistics.takenForRegWithCheckout : statistics.takenForReg;

                if (freeSpace <= 0) continue;
            }

            String checkout = statistics.checkout == null ? "" : ", " + DateFormatter.DEFAULT_DATE_FORMATTER.format(statistics.checkout);

            if (countBedNumber)
            {
                wrappers.add(new IdentifiableWrapper<UniplacesHabitablePlace>(
                    habitablePlace.getId(),
                    place.getNumber() + " " + statistics.takenForReg + "/" + statistics.taken + "/" + beds + ", " + area + checkout));
            }
            else
            {
                String taken = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(0.0001d * statistics.taken);

                wrappers.add(new IdentifiableWrapper<UniplacesHabitablePlace>(
                    habitablePlace.getId(),
                    place.getNumber() + " " + taken + "/" + area + checkout));
            }
        }
        return wrappers;
    }

    @Override
    public void wrapDataSource(DynamicListDataSource<UniplacesHabitablePlace> dataSource)
    {
        /*

    * Число проживающих – "residentCount"
    *   вычисляется на основании всех договоров в состоянии «Согласовано» для данного помещения,
    *   может быть больше числа койкомест.

         */
        List<UniplacesHabitablePlace> places = dataSource.getEntityList();

        MQBuilder accBuilder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accBuilder.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accBuilder.addJoinFetch("contract", UnisettleContract.type(), "type");
        accBuilder.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        accBuilder.add(MQExpression.in("acc", UnisettleContractAccomodation.place(), places));
        List<UnisettleContractAccomodation> accList = accBuilder.getResultList(getSession());

        Map<UniplacesHabitablePlace, PlaceShortStatistics> statisticsMap = new HashMap<UniplacesHabitablePlace, PlaceShortStatistics>();
        Map<UnisettleContract, Set<Long>> residentMap = new HashMap<UnisettleContract, Set<Long>>();
        for (UniplacesHabitablePlace place : places)
            statisticsMap.put(place, new PlaceShortStatistics());
        for (UnisettleContractAccomodation acc : accList)
        {
            addToStatistics(statisticsMap.get(acc.getPlace()), acc);
            residentMap.put(acc.getContract(), new HashSet<Long>());
        }

        MQBuilder residentBuilder = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "res");
        residentBuilder.addJoinFetch("res", UnisettleContractAccomodation.contract(), "contract");
        residentBuilder.addJoinFetch("contract", UnisettleContract.type(), "type");
        residentBuilder.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        residentBuilder.add(MQExpression.in("res", UnisettleContractAccomodation.contract(), residentMap.keySet()));
        List<UnisettleContractResident> residentList = residentBuilder.getResultList(getSession());
        for (UnisettleContractResident resident : residentList)
            residentMap.get(resident.getContract()).add(resident.getId());

        for (UnisettleContractAccomodation acc : accList)
            statisticsMap.get(acc.getPlace()).residents.addAll(residentMap.get(acc.getContract()));

        for (ViewWrapper<UniplacesHabitablePlace> wrapper: ViewWrapper.<UniplacesHabitablePlace>getPatchedList(dataSource))
        {
            PlaceShortStatistics statistics = statisticsMap.get(wrapper.getEntity());
            wrapper.setViewProperty(P_TAKEN_BEDS, statistics.taken);
            wrapper.setViewProperty(P_TAKEN_AREA, statistics.takenArea * 0.0001d);
            wrapper.setViewProperty(P_RESIDENT_COUNT, statistics.residents.size());
        }
    }

    @Override
    public boolean checkEnoughSpaceOnContractAcceptance(UnisettleContract contract)
    {
        List<UnisettleContractAccomodation> contractAccList = getList(UnisettleContractAccomodation.class, UnisettleContractAccomodation.contract().s(), contract);
        Set<UniplacesHabitablePlace> places = new HashSet<>(CommonBaseUtil.<UniplacesHabitablePlace>getPropertiesList(contractAccList, UnisettleContractAccomodation.place().s()));

        MQBuilder accBuilder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accBuilder.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accBuilder.addJoinFetch("contract", UnisettleContract.type(), "type");
        accBuilder.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        accBuilder.add(MQExpression.notEq("acc", UnisettleContractAccomodation.contract(), contract));
        accBuilder.add(MQExpression.in("acc", UnisettleContractAccomodation.place(), places));
        List<UnisettleContractAccomodation> accList = accBuilder.getResultList(getSession());

        Map<UniplacesHabitablePlace, PlaceShortStatistics> statisticsMap = new HashMap<UniplacesHabitablePlace, PlaceShortStatistics>();
        for (UniplacesHabitablePlace place : places)
            statisticsMap.put(place, new PlaceShortStatistics());
        for (UnisettleContractAccomodation acc : accList)
            addToStatistics(statisticsMap.get(acc.getPlace()), acc);

        boolean countBeds = contract instanceof UnisettleSingleBedContract;
        for (UnisettleContractAccomodation acc : contractAccList)
        {
            UniplacesHabitablePlace place = acc.getPlace();
            PlaceShortStatistics statistics = statisticsMap.get(place);
            long taken = statistics == null ? 0 : (countBeds ? statistics.taken : statistics.takenArea);
            if (countBeds)
                taken++;
            else
                taken += acc.getArea();
            long capacity = countBeds ? acc.getPlace().getBedNumber() : (acc.getPlace().getPlace().getArea() * 100);
            if (taken > capacity)
                return false;
        }

        return true;
    }

    private void addToStatistics(PlaceStatistics statistics, Date accCheckout, boolean regOnly, boolean withCheckout, long taken)
    {
        statistics.inited = true;
        statistics.taken += taken;
        if (withCheckout)
        {
            statistics.takenWithCheckout += taken;
        }
        if (regOnly)
            statistics.takenForReg += taken;
        if (withCheckout && regOnly)
            statistics.takenForRegWithCheckout += taken;
        if (accCheckout != null && (statistics.checkout == null || statistics.checkout.after(accCheckout)))
            statistics.checkout = accCheckout;
    }

    private static final class PlaceStatistics
    {
        boolean inited;
        long taken;
        long takenForReg;
        long takenWithCheckout;
        long takenForRegWithCheckout;
        Date checkout;
    }

    private void addToStatistics(PlaceShortStatistics statistics, UnisettleContractAccomodation acc)
    {
        if (acc.getContract() instanceof UnisettleSingleBedContract)
            statistics.taken++;
        else
            statistics.takenArea += acc.getArea();
    }

    private static final class PlaceShortStatistics
    {
        long taken;
        long takenArea;
        Set<Long> residents = new HashSet<Long>();
    }
}
