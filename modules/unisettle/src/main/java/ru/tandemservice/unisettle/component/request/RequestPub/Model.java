// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.RequestPub;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "request.id")
public class Model
{
    private UnisettleRequest request = new UnisettleRequest();

    public boolean isDeclineEnabled()
    {
        return getRequest().getState().getCode().equals(UnisettleDefines.REQUEST_STATE_REGISTERED);
    }

    public UnisettleRequest getRequest()
    {
        return request;
    }

    public void setRequest(UnisettleRequest request)
    {
        this.request = request;
    }
}
