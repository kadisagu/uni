package ru.tandemservice.unisettle.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisettle.entity.catalog.gen.UnisettleTemplateDocumentGen;

/**
 * Печатный шаблон
 */
public class UnisettleTemplateDocument extends UnisettleTemplateDocumentGen implements ITemplateDocument
{
	@Override
	public byte[] getContent()
	{
		return CommonBaseUtil.getTemplateContent(this);
	}
}