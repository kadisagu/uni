// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SumAreaContractPub;

import java.util.List;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.dao.IUnisettleContractDAO;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

/**
 * @author oleyba
 * @since 11.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _accomodationOrderSettings = new OrderDescriptionRegistry("acc");
    private static final OrderDescriptionRegistry _residentOrderSettings = new OrderDescriptionRegistry("res");

    static
    {
        _accomodationOrderSettings.setOrders("number", new OrderDescription("acc", UnisettleContractAccomodation.place().place().number().s()));
        //_residentOrderSettings.setOrders("number", new OrderDescription("acc", UnisettleContractAccomodation.place().place().number().s()));
    }


    @Override
    public void prepare(Model model)
    {
        model.setContract(get(UnisettleSumAreaContract.class, model.getContract().getId()));
        List<UnisettleContractResident> contractResidents = getList(UnisettleContractResident.class, UnisettleContractResident.contract().s(), model.getContract());
        model.setResidentFromContractAdded(CommonBaseUtil.getPropertiesList(contractResidents, UnisettleContractResident.resident().s()).contains(model.getContract().getResident()));
    }

    @Override
    public void doChangeState(Model model, UnisettleContractStateChange stateChange)
    {
        UnisettleSumAreaContract contract = model.getContract();
        IUnisettleContractDAO.instance.get().doChangeContractState(contract, stateChange);
    }

    @Override
    public void prepareAccomodationDataSource(Model model)
    {
        DynamicListDataSource<UnisettleContractAccomodation> dataSource = model.getAccomodationDataSource();
        MQBuilder builder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        builder.add(MQExpression.eq("acc", UnisettleContractAccomodation.contract(), model.getContract()));
        _accomodationOrderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createFullPage(dataSource, builder, getSession());
    }

    @Override
    public void prepareResidentDataSource(Model model)
    {
        DynamicListDataSource<UnisettleContractResident> dataSource = model.getResidentDataSource();
        MQBuilder builder = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "res");
        builder.add(MQExpression.eq("res", UnisettleContractResident.contract(), model.getContract()));
        _residentOrderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createFullPage(dataSource, builder, getSession());        
    }

    @Override
    public void deleteAccomodation(Model model, Long id)
    {
        getSession().refresh(model.getContract());
        if (!UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");

        delete(id);
    }

    @Override
    public void deleteResident(Model model, Long id)
    {
        getSession().refresh(model.getContract());
        if (!UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");

        delete(id);
    }
}
