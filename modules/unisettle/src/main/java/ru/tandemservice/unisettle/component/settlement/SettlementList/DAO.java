/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.*;
import ru.tandemservice.unisettle.dao.IUnisettleSettlementDAO;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author agolubenko
 * @since Sep 6, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("settlement");
    @Override
    public void prepare(final Model model)
    {
        getSession();

        model.setBuildingList(new LazySimpleSelectModel<UniplacesBuilding>(UniplacesBuilding.class));
        model.setUnitList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesUnit.ENTITY_CLASS, alias);
                if (null == model.getBuilding())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesUnit.building().s(), model.getBuilding()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesUnit.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setFloorList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesFloor.ENTITY_CLASS, alias);
                if (null == model.getUnit())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesFloor.unit().s(), model.getUnit()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesFloor.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesFloor.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setPlaceList(new UniQueryFullCheckSelectModel("number")
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesPlace.ENTITY_CLASS, alias);
                if (model.getFloor() != null)
                    builder.add(MQExpression.eq(alias, UniplacesPlace.floor().s(), model.getFloor()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesPlace.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesPlace.number());
                return builder;
            }
        }.setPageSize(100));
        model.setPurposeList(IUniplacesDAO.instance.get().getHabitablePlacePurposeOptionList());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<UniplacesHabitablePlace> dataSource = model.getDataSource();
        UniplacesBuilding building = model.getBuilding();
        UniplacesUnit unit = model.getUnit();
        UniplacesFloor floor = model.getFloor();
        UniplacesPlace place = model.getPlace();

        MQBuilder builder = new MQBuilder(UniplacesHabitablePlace.ENTITY_CLASS, "settlement");
        builder.addJoinFetch("settlement", UniplacesHabitablePlace.place().s(), "place");
        builder.addJoinFetch("place", UniplacesPlace.floor().s(), "floor");
        builder.addJoinFetch("floor", UniplacesFloor.unit().s(), "unit");
        builder.addJoinFetch("unit", UniplacesUnit.building().s(), "building");

        if (place != null)
            builder.add(MQExpression.eq("settlement", UniplacesHabitablePlace.place().s(), place));
        else if (floor != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.floor().s(), floor));
        else if (unit != null)
            builder.add(MQExpression.eq("floor", UniplacesFloor.unit().s(), unit));
        else if (building != null)
            builder.add(MQExpression.eq("unit", UniplacesUnit.building().s(), building));

        UniplacesPlacePurpose purpose = model.getPurpose();
        if (purpose != null)
            builder.add(MQExpression.eq("place", UniplacesPlace.purpose().s(), purpose));

        builder.add(MQExpression.in("place", UniplacesPlace.purpose().s(), IUniplacesDAO.instance.get().getHabitablePlacePurposeList()));
        sortCurrentTab(dataSource, builder);
        IUnisettleSettlementDAO.instance.get().wrapDataSource(dataSource);
    }

    @Override
    public void prepareConflictDataSource(Model model)
    {
        DynamicListDataSource<UniplacesHabitablePlace> dataSource = model.getConflictDataSource();
        Set<Long> ids = new HashSet<Long>();

        {
            MQBuilder builder = getBaseSettlementBuilder();
            MQBuilder sub = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
            sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.place().id().s(), "settlement", "id"));
            sub.addDomain("res", UnisettleContractResident.ENTITY_CLASS);
            sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.contract(), "res", UnisettleContractResident.contract()));
            sub.add(MQExpression.notEqProperty("res", UnisettleContractResident.resident().person().identityCard().sex(), "place", UniplacesPlace.purpose().sex()));
            builder.add(MQExpression.exists(sub));
            builder.add(MQExpression.isNotNull("place", UniplacesPlace.purpose().sex()));
            builder.getSelectAliasList().clear();
            builder.addSelect("settlement", new Object[]{"id"});
            ids.addAll(builder.<Long>getResultList(getSession()));
        }

        {
            MQBuilder builder = getBaseSettlementBuilder();
            MQBuilder sub = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
            sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.place().id().s(), "settlement", "id"));
            sub.addDomain("contract", UnisettleSingleBedContract.ENTITY_CLASS);
            sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.contract().id().s(), "contract", "id"));
            builder.add(MQExpression.exists(sub));
            builder.add(MQExpression.eq("place", UniplacesPlace.purpose().countBedNumber(), Boolean.FALSE));
            builder.getSelectAliasList().clear();
            builder.addSelect("settlement", new Object[]{"id"});
            ids.addAll(builder.<Long>getResultList(getSession()));
        }

        {
            MQBuilder builder = getBaseSettlementBuilder();
            MQBuilder sub = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
            sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.place().id().s(), "settlement", "id"));
            sub.addDomain("contract", UnisettleSumAreaContract.ENTITY_CLASS);
            sub.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.contract().id().s(), "contract", "id"));
            builder.add(MQExpression.exists(sub));
            builder.add(MQExpression.eq("place", UniplacesPlace.purpose().countBedNumber(), Boolean.TRUE));
            builder.getSelectAliasList().clear();
            builder.addSelect("settlement", new Object[]{"id"});
            ids.addAll(builder.<Long>getResultList(getSession()));
        }

        MQBuilder builder = getBaseSettlementBuilder();
        builder.add(MQExpression.in("settlement", "id", ids)); // если выйдут за 2к, им будет проще вайпнуть базу, чем разгрести это
        sortCurrentTab(dataSource, builder);
    }

    private MQBuilder getBaseSettlementBuilder()
    {
        MQBuilder builder = new MQBuilder(UniplacesHabitablePlace.ENTITY_CLASS, "settlement");
        builder.addJoinFetch("settlement", UniplacesHabitablePlace.place(), "place");
        builder.add(MQExpression.in("place", UniplacesPlace.purpose().s(), IUniplacesDAO.instance.get().getHabitablePlacePurposeList()));
        return builder;
    }

    private void sortCurrentTab(DynamicListDataSource<UniplacesHabitablePlace> dataSource, MQBuilder builder) {
        EntityOrder entityOrder = dataSource.getEntityOrder();
        if (entityOrder.getKey().equals(UniplacesHabitablePlace.place().number().s())
                || (entityOrder.getKey().equals(UniplacesHabitablePlace.place().floor().title().s())))
        {
            List<UniplacesHabitablePlace> result = builder.getResultList(getSession());
            Collections.sort(result, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, (String) entityOrder.getKey()));
            if (entityOrder.getDirection() == OrderDirection.desc)
            {
                Collections.reverse(result);
            }
            UniBaseUtils.createPage(dataSource, result);
        } else {
            _orderSettings.applyOrder(builder, entityOrder);
            UniBaseUtils.createPage(dataSource, builder, getSession());
        }
    }
}
