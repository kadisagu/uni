package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор найма на время обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleSingleBedContractGen extends UnisettleContract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract";
    public static final String ENTITY_NAME = "unisettleSingleBedContract";
    public static final int VERSION_HASH = 1627748987;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisettleSingleBedContractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleSingleBedContractGen> extends UnisettleContract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleSingleBedContract.class;
        }

        public T newInstance()
        {
            return (T) new UnisettleSingleBedContract();
        }
    }
    private static final Path<UnisettleSingleBedContract> _dslPath = new Path<UnisettleSingleBedContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleSingleBedContract");
    }
            

    public static class Path<E extends UnisettleSingleBedContract> extends UnisettleContract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UnisettleSingleBedContract.class;
        }

        public String getEntityName()
        {
            return "unisettleSingleBedContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
