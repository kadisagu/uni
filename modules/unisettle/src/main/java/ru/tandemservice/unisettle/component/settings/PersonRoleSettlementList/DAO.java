// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settings.PersonRoleSettlementList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.CheckboxColumn;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisettle.UnisettleDefines;

/**
 * @author oleyba
 * @since 22.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        IDataSettings settings = DataSettingsFacade.getSettings("", UnisettleDefines.MODULE_SETTINGS_KEY);
        if (!settings.containsKey(UnisettleDefines.SETTINGS_PERSON_ROLE_SETTLEMENT))
        {
            UnisettlePersonRoleSettings personRoleSettings = new UnisettlePersonRoleSettings();
            IEntityMeta student = EntityRuntime.getMeta(Student.class);
            if (student != null)
                personRoleSettings.getUsageByEntityCode().put(student.getEntityCode(), Boolean.TRUE);
            IEntityMeta employee = EntityRuntime.getMeta(Employee.class);
            if (employee != null)
                personRoleSettings.getUsageByEntityCode().put(employee.getEntityCode(), Boolean.TRUE);
            settings.set(UnisettleDefines.SETTINGS_PERSON_ROLE_SETTLEMENT, personRoleSettings);
            DataSettingsFacade.saveSettings(settings);
        }
        model.setPersonRoleSettings((UnisettlePersonRoleSettings) settings.get(UnisettleDefines.SETTINGS_PERSON_ROLE_SETTLEMENT));
        model.setSettings(settings);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Set<IEntityMeta> children = new HashSet<IEntityMeta>();
        children.addAll(EntityRuntime.getMeta(PersonRole.class).getChildren());

        Set<IEntityMeta> level = new HashSet<IEntityMeta>();
        level.addAll(EntityRuntime.getMeta(PersonRole.class).getChildren());
        while (!level.isEmpty())
        {
            Set<IEntityMeta> newLevel = new HashSet<IEntityMeta>();
            for (IEntityMeta current : level)
            {
                children.addAll(current.getChildren());
                newLevel.addAll(current.getChildren());
            }
            level.clear();
            level.addAll(newLevel);
        }

        List<IEntityMeta> childrenList = new ArrayList<IEntityMeta>(children);
        //Collections.sort(childrenList, ITitled.TITLED_COMPARATOR);
        List<Model.PersonRoleWrapper> wrappers = new ArrayList<Model.PersonRoleWrapper>();
        Collection<IEntity> selected = new ArrayList<IEntity>();
        long id = 0;

        for (IEntityMeta child : childrenList)
        {
            Short entityCode = child.getEntityCode();
            Model.PersonRoleWrapper wrapper = new Model.PersonRoleWrapper(id++, child.getTitle(), entityCode);
            Boolean use = model.getPersonRoleSettings().getUsageByEntityCode().get(entityCode);
            if (use == null) use = Boolean.FALSE;
            wrapper.setSelected(use);
            if (use) selected.add(wrapper);
            wrappers.add(wrapper);
        }

        UniBaseUtils.createFullPage(model.getDataSource(), wrappers);
        ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).setSelectedObjects(selected);
    }

    @Override
    public void update(Model model)
    {
        Collection<IEntity> checked = ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects();
        for (Model.PersonRoleWrapper wrapper : model.getDataSource().getEntityList())
            model.getPersonRoleSettings().getUsageByEntityCode().put(wrapper.getEntityCode(), checked.contains(wrapper));
        model.getSettings().set(UnisettleDefines.SETTINGS_PERSON_ROLE_SETTLEMENT, model.getPersonRoleSettings());
        DataSettingsFacade.saveSettings(model.getSettings());
    }
}

