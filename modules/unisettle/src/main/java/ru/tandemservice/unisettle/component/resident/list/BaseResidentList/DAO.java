// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.resident.list.BaseResidentList;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.component.settings.PersonRoleSettlementList.UnisettlePersonRoleSettings;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractState;
import ru.tandemservice.unisettle.entity.catalog.UnisettleRequestState;
import ru.tandemservice.unisettle.entity.settlement.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 21.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setSexModel(new LazySimpleSelectModel<>(Sex.class));
        List<UnisettleCategoryWrapper> categoryWrappers = new ArrayList<>();
        IDataSettings settings = DataSettingsFacade.getSettings("", UnisettleDefines.MODULE_SETTINGS_KEY);
        if (settings.containsKey(UnisettleDefines.SETTINGS_PERSON_ROLE_SETTLEMENT))
        {
            UnisettlePersonRoleSettings personRoleSettings = settings.get(UnisettleDefines.SETTINGS_PERSON_ROLE_SETTLEMENT);
            for (Map.Entry<Short, Boolean> entry : personRoleSettings.getUsageByEntityCode().entrySet())
            {
                if (!entry.getValue()) continue;
                Short key = entry.getKey();
                IEntityMeta meta = EntityRuntime.getMeta(key);
                if (null == meta) continue;
                categoryWrappers.add(new UnisettleCategoryWrapper(meta.getTitle(), key));
            }
        }
        Collections.sort(categoryWrappers, ITitled.TITLED_COMPARATOR);
        model.setCategoryModel(categoryWrappers);
    }

    public static final String fioKey = Model.UnisettleResidentListWrapper.resident().person().fullFio().s();
    public static final String sexKey = Model.UnisettleResidentListWrapper.resident().person().identityCard().sex().title().s();
    public static final String checkinDateKey = Model.UnisettleResidentListWrapper.contract().checkinDate().s();
    public static final String checkoutDateKey = Model.UnisettleResidentListWrapper.contract().checkoutDate().s();

    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("b");
    {
        DAO._orderSettings.setOrders(fioKey, new OrderDescription("idc", IdentityCard.lastName()), new OrderDescription("idc", IdentityCard.firstName()), new OrderDescription("idc", IdentityCard.middleName()));
        DAO._orderSettings.setOrders(sexKey, new OrderDescription("sex", Sex.title().s()));
        DAO._orderSettings.setOrders(checkinDateKey, new OrderDescription("c", UnisettleContract.checkinDate().s()));
        DAO._orderSettings.setOrders(checkoutDateKey, new OrderDescription("c", UnisettleContract.checkoutDate().s()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Session session = getSession();

        Map<Long, Model.UnisettleResidentListWrapper> wrapperMap = new HashMap<>();

        IDataSettings settings = model.getSettings();
        String lastName = settings.get("lastName");
        String firstName = settings.get("firstName");
        String middleName = settings.get("middleName");
        IEntity sex = settings.get("sex");
        UnisettleCategoryWrapper category = settings.get("category");

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UnisettleResident.class, "resident").column(property("resident"));
        DQLSelectBuilder contractBuilder = new DQLSelectBuilder().fromEntity(UnisettleContractResident.class, "contractResident")
        .joinEntity("contractResident", DQLJoinType.left, UnisettleContract.class, "contract",
                    eq(property(UnisettleContractResident.contract().id().fromAlias("contractResident")), property("contract.id")))
        .column(property("contractResident.contract"), "settle_contract")
        .column(property("contract.checkinDate"), "settle_checkinDate")
        .column(property("contract.checkoutDate"), "settle_checkoutDate")
        .column(property("contractResident"), "settle_resident");
        contractBuilder.where(eq(property(UnisettleContractResident.contract().state().code().fromAlias("contractResident")), DQLExpressions.value(UnisettleDefines.CONTRACT_STATE_ACCEPTED)));
        dql.joinDataSource("resident", DQLJoinType.left, contractBuilder.buildQuery(), "settle", eq(property("settle.settle_resident.resident.id"), property("resident", "id")));
        dql.joinEntity("resident", DQLJoinType.inner, Person.class, "person", eq(property(UnisettleResident.person().id().fromAlias("resident")), property("person.id")));
        dql.joinEntity("person", DQLJoinType.inner, IdentityCard.class, "idc", eq(property(Person.identityCard().id().fromAlias("person")), property("idc.id")));
        FilterUtils.applySimpleLikeFilter(dql, "idc", IdentityCard.lastName(), lastName);
        FilterUtils.applySimpleLikeFilter(dql, "idc", IdentityCard.firstName(), firstName);
        FilterUtils.applySimpleLikeFilter(dql, "idc", IdentityCard.middleName(), middleName);
        if (null != sex)
            dql.where(eq(property(IdentityCard.sex().fromAlias("idc")), DQLExpressions.value(sex)));
        if (null != category)
        {
            IEntityMeta meta = EntityRuntime.getMeta(category.getEntityCode());
            if (null != meta)
            {
                DQLSelectBuilder categoryBuilder = new DQLSelectBuilder().fromEntity(meta.getEntityClass(), "role")
                .column(property("role.id"))
                .where(eq(property("role.person.id"), property("person.id")));
                dql.where(exists(categoryBuilder.buildQuery()));
            }
        }

        EntityOrder order = model.getDataSource().getEntityOrder();
        OrderDirection dqlOrderDirection = order.getDirection().equals(OrderDirection.asc) ? OrderDirection.asc : OrderDirection.desc;

        if (order.getKey().equals(fioKey))
        {
            dql.order(property(IdentityCard.lastName().fromAlias("idc")), dqlOrderDirection);
            dql.order(property(IdentityCard.firstName().fromAlias("idc")), dqlOrderDirection);
            dql.order(property(IdentityCard.middleName().fromAlias("idc")), dqlOrderDirection);
        }
        if (order.getKey().equals(sexKey))
            dql.order(property(IdentityCard.sex().code().fromAlias("idc")), dqlOrderDirection);
        if (order.getKey().equals(checkinDateKey))
            dql.order(property("settle.settle_checkinDate"), dqlOrderDirection);
        if (order.getKey().equals(checkoutDateKey))
            dql.order(property("settle.settle_checkoutDate"), dqlOrderDirection);

        DQLExecutionContext qdlContext = new DQLExecutionContext(session);
        Number count = dql.createCountStatement(qdlContext).uniqueResult();
        model.getDataSource().setTotalSize(count.longValue());

        List<UnisettleResident> residentList = dql.createStatement(getSession()).
        setFirstResult((int) model.getDataSource().getStartRow())
        .setMaxResults((int) model.getDataSource().getCountRow())
        .list();

        List<Model.UnisettleResidentListWrapper> result = new ArrayList<>();
        Map<Person, List<IPrincipalContext>> personRoleMap = PersonManager.instance().dao().getPrincipalContextMap(CommonBaseUtil.<Person>getPropertiesList(residentList, UnisettleResident.person().s()));

        for (final UnisettleResident resident : residentList)
        {
            Model.UnisettleResidentListWrapper wrapper = new Model.UnisettleResidentListWrapper(resident);
            Person person = resident.getPerson();
            List<IPrincipalContext> list = personRoleMap.get(person);
            if (list == null)
                list = Collections.emptyList();
            wrapper.setCategoryList(list);
            wrapperMap.put(resident.getId(), wrapper);
            result.add(wrapper);
        }

        model.getDataSource().createPage(result);

        initSettleState(wrapperMap);
        initContractAndAccomodation(wrapperMap);

        super.prepareListDataSource(model);
    }

    private void initContractAndAccomodation(Map<Long, Model.UnisettleResidentListWrapper> wrapperMap)
    {
        MQBuilder accomodationBuilder = new MQBuilder(UnisettleContractAccomodation.ENTITY_CLASS, "acc");
        accomodationBuilder.addJoinFetch("acc", UnisettleContractAccomodation.place(), "place");
        accomodationBuilder.addJoinFetch("acc", UnisettleContractAccomodation.contract(), "contract");
        accomodationBuilder.addJoinFetch("contract", UnisettleContract.resident(), "resident");
        accomodationBuilder.addDomain("rel", UnisettleContractResident.ENTITY_CLASS);
        accomodationBuilder.add(MQExpression.eqProperty("acc", UnisettleContractAccomodation.contract(), "rel", UnisettleContractResident.contract()));
        accomodationBuilder.add(MQExpression.in("rel", UnisettleContractResident.resident().id(), wrapperMap.keySet()));
        accomodationBuilder.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        for (UnisettleContractAccomodation acc : accomodationBuilder.<UnisettleContractAccomodation>getResultList(getSession()))
        {
            Model.UnisettleResidentListWrapper wrapper = wrapperMap.get(acc.getContract().getResident().getId());
            wrapper.getAccomodationList().add(acc.getPlace());
        }

        MQBuilder contractBuilder = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "rel");
        contractBuilder.addJoinFetch("rel", UnisettleContractResident.contract(), "contract");
        contractBuilder.addJoinFetch("rel", UnisettleContractResident.resident(), "resident");
        contractBuilder.addJoinFetch("contract", UnisettleContract.state(), "state");
        contractBuilder.add(MQExpression.in("rel", UnisettleContractResident.resident().id(), wrapperMap.keySet()));
        contractBuilder.add(MQExpression.eq("state", UnisettleContractState.code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        for (UnisettleContractResident rel : contractBuilder.<UnisettleContractResident>getResultList(getSession()))
        {
            Model.UnisettleResidentListWrapper wrapper = wrapperMap.get(rel.getResident().getId());
            wrapper.setContract(rel.getContract());
        }
    }

    private void initSettleState(Map<Long, Model.UnisettleResidentListWrapper> wrapperMap)
    {
        final Session session = getSession();

        MQBuilder requests = new MQBuilder(UnisettleRequest.ENTITY_CLASS, "request", new String[] {UnisettleRequest.resident().id().s()});
        requests.addJoin("request", UnisettleRequest.state(), "state");
        requests.add(MQExpression.notEq("state", UnisettleRequestState.code(), UnisettleDefines.REQUEST_STATE_DECLINED));
        requests.add(MQExpression.notEq("state", UnisettleRequestState.code(), UnisettleDefines.REQUEST_STATE_ARCHIVE));
        requests.add(MQExpression.in("request", UnisettleRequest.resident().id(), wrapperMap.keySet()));
        for (Long residentId : requests.<Long>getResultList(session))
            wrapperMap.get(residentId).setSettleStatus("заявление");

        MQBuilder contracts = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "resident", new String[]{UnisettleContractResident.resident().id().s()});
        contracts.addJoin("resident", UnisettleContractResident.contract(), "contract");
        contracts.add(MQExpression.in("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATES_ACTIVE));
        contracts.add(MQExpression.in("resident", UnisettleContractResident.resident().id(), wrapperMap.keySet()));
        for (Long residentId : contracts.<Long>getResultList(session))
            wrapperMap.get(residentId).setSettleStatus("договор");

        MQBuilder contractResidence = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "resident", new String[]{UnisettleContractResident.resident().id().s()});
        contractResidence.addJoin("resident", UnisettleContractResident.contract(), "contract");
        contractResidence.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        contractResidence.add(MQExpression.in("resident", UnisettleContractResident.resident().id(), wrapperMap.keySet()));
        for (Long residentId : contractResidence.<Long>getResultList(session))
            wrapperMap.get(residentId).setSettleStatus("да");
    }
}
