
package ru.tandemservice.unisettle.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.sec.entity.Principal;

import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.util.IPersonMergeListener;
import ru.tandemservice.uni.dao.UniBaseDao;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.catalog.UnisettleRequestState;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 29.11.2010
 */
public class PersonMergeListener extends UniBaseDao implements IPersonMergeListener
{
    @Override
    public List<PersonRole> doMerge(Person template, List<Person> duplicates)
    {
        Session session = getSession();

        Person2PrincipalRelation relation = get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, template);
        if (relation == null)
            relation = PersonManager.instance().dao().createPersonPrincipalRelation(template);
        Principal principal = relation.getPrincipal();

        UnisettleResident baseResident = null;
        List<UnisettleResident> baseResidentList = getList(UnisettleResident.class, UnisettleResident.L_PERSON, template);
        if (!baseResidentList.isEmpty())
        {
            baseResident = baseResidentList.get(0);
            baseResidentList.remove(baseResident);
        }

        MQBuilder residentBuilder = new MQBuilder(UnisettleResident.ENTITY_CLASS, "res");
        residentBuilder.add(MQExpression.in("res", UnisettleResident.person(), duplicates));
        List<UnisettleResident> duplicateResidents = residentBuilder.getResultList(session);
        duplicateResidents.addAll(baseResidentList);

        if (duplicateResidents.isEmpty())
            return Collections.emptyList();

        if (null == baseResident)
        {
            baseResident = duplicateResidents.get(0);
            baseResident.setPerson(template);
            baseResident.setPrincipal(principal);
            duplicateResidents.remove(0);
            session.update(baseResident);
        }

        if (duplicateResidents.isEmpty())
        {
            session.flush();
            return Arrays.asList((PersonRole) baseResident);
        }

        List<Person> affectedPersons = new ArrayList<Person>();
        affectedPersons.addAll(duplicates);
        affectedPersons.add(template);

        List<UnisettleRequest> openedRequests = new ArrayList<UnisettleRequest>();
        UnisettleRequest earliestRequest = null;

        MQBuilder requestsBuilder = new MQBuilder(UnisettleRequest.ENTITY_CLASS, "req");
        requestsBuilder.add(MQExpression.in("req", UnisettleRequest.resident().person(), affectedPersons));
        List<UnisettleRequest> requests = requestsBuilder.getResultList(getSession());

        for (UnisettleRequest request : requests)
        {
            if (UnisettleDefines.REQUEST_STATE_REGISTERED.equals(request.getState().getCode()))
            {
                openedRequests.add(request);
                if (null == earliestRequest)
                    earliestRequest = request;
                else if (earliestRequest.getRegDate().after(request.getRegDate()))
                    earliestRequest = request;
            }
            request.setResident(baseResident);
            session.update(request);
        }

        if (!openedRequests.isEmpty())
        {
            openedRequests.remove(earliestRequest);
            UnisettleRequestState archive = get(UnisettleRequestState.class, UnisettleRequestState.code().s(), UnisettleDefines.REQUEST_STATE_ARCHIVE);
            for (UnisettleRequest request : openedRequests)
            {
                request.setState(archive);
                session.update(request);
            }
        }

        MQBuilder activeSettlement = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "res");
        activeSettlement.addJoin("res", UnisettleContractResident.contract(), "contract");
        activeSettlement.add(MQExpression.in("contract", UnisettleContract.resident().person(), affectedPersons));
        activeSettlement.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
        if (activeSettlement.getResultCount(getSession()) > 1)
            throw new ApplicationException("Невозможно объединить персон, так как более одной из выбранных персон зарегистрировано, как проживающие по действующим договорам поселения.");

        MQBuilder settlementBuilder = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "res");
        settlementBuilder.addJoin("res", UnisettleContractResident.contract(), "contract");
        settlementBuilder.add(MQExpression.in("contract", UnisettleContract.resident().person(), affectedPersons));
        List<UnisettleContractResident> settlementList = settlementBuilder.getResultList(getSession());
        for (UnisettleContractResident settlement : settlementList)
        {
            settlement.setResident(baseResident);
            session.update(settlement);
        }

        MQBuilder contractBuilder = new MQBuilder(UnisettleContract.ENTITY_CLASS, "contract");
        contractBuilder.add(MQExpression.in("contract", UnisettleContract.resident().person(), affectedPersons));
        List<UnisettleContract> contractList = contractBuilder.getResultList(getSession());
        for (UnisettleContract contract : contractList)
        {
            contract.setResident(baseResident);
            session.update(contract);
        }

        session.flush();

        for (UnisettleResident duplicate : duplicateResidents)
            session.delete(duplicate);

        ArrayList<PersonRole> processed = new ArrayList<PersonRole>(duplicateResidents);
        processed.add(baseResident);
        return processed;
    }
}
