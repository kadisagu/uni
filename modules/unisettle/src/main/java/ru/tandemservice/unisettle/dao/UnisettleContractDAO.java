// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.dao;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.entity.catalog.UnisettleContractState;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;

/**
 * @author oleyba
 * @since 10.11.2010
 */
public class UnisettleContractDAO extends UniBaseDao implements IUnisettleContractDAO
{
    @Override
    public void doChangeContractState(UnisettleContract contract, UnisettleContractStateChange stateChange)
    {
        Session session = getSession();
        Person person = contract.getResident().getPerson();

        NamedSyncInTransactionCheckLocker.register(session, person.getId() + ".unisettleContract");

        if (UnisettleDefines.CONTRACT_STATE_ACCEPTED.equals(stateChange.getTargetStateCode()))
        {
            MQBuilder activeResidence = new MQBuilder(UnisettleContractResident.ENTITY_CLASS, "rel");
            activeResidence.addJoin("rel", UnisettleContractResident.contract(), "contract");
            activeResidence.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
            activeResidence.add(MQExpression.notEq("contract", UnisettleContract.id(), contract.getId()));
            activeResidence.addDomain("rel1", UnisettleContractResident.ENTITY_CLASS);
            activeResidence.add(MQExpression.eqProperty("rel", UnisettleContractResident.resident().person(), "rel1", UnisettleContractResident.resident().person()));
            activeResidence.add(MQExpression.eq("rel1", UnisettleContractResident.contract(), contract));
            if (activeResidence.getResultCount(session) > 0)
               throw new ApplicationException("Невозможно согласовать договор, так как некоторые из проживающих по данному договору уже зарегистрированы в таком качестве по другому согласованному договору.");

            MQBuilder numberCheck = new MQBuilder(UnisettleContract.ENTITY_CLASS, "contract");
            numberCheck.add(MQExpression.eq("contract", UnisettleContract.number(), StringUtils.trim(contract.getNumber())));
            numberCheck.add(MQExpression.eq("contract", UnisettleContract.state().code(), UnisettleDefines.CONTRACT_STATE_ACCEPTED));
            numberCheck.add(MQExpression.notEq("contract", UnisettleContract.id(), contract.getId()));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(contract.getRegDate());
            numberCheck.add(UniMQExpression.eqYear("contract", UnisettleContract.regDate().s(), calendar.get(Calendar.YEAR)));
            if (numberCheck.getResultCount(session) > 0)
               throw new ApplicationException("Невозможно согласовать договор, так как номер согласованного договора должен быть уникален в рамках года.");

            boolean checkSpace = IUnisettleSettlementDAO.instance.get().checkEnoughSpaceOnContractAcceptance(contract);
            if (!checkSpace)
                throw new ApplicationException("Невозможно согласовать договор, так как в прикрепленных местах поселения недостаточно свободного места.");
        }

        if (UnisettleDefines.CONTRACT_STATE_FORMING.equals(stateChange.getTargetStateCode()))
            contract.setActualCheckoutDate(null);

        contract.setState(get(UnisettleContractState.class, UnisettleContractState.code().s(), stateChange.getTargetStateCode()));
        update(contract);
    }
}
