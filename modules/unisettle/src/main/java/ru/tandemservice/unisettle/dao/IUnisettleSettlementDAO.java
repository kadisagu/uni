// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.dao;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;

/**
 * @author oleyba
 * @since 12.11.2010
 */
public interface IUnisettleSettlementDAO
{
    static final SpringBeanCache<IUnisettleSettlementDAO> instance = new SpringBeanCache<IUnisettleSettlementDAO>(IUnisettleSettlementDAO.class.getName());

    public static final String P_TAKEN_AREA = "takenArea";
    public static final String P_TAKEN_BEDS = "takenBeds";
    public static final String P_RESIDENT_COUNT = "residentCount";

    public List<IdentifiableWrapper<UniplacesHabitablePlace>> getWrappedPlacesList(UniplacesFloor floor, UnisettleContract contract, boolean countBedNumber, boolean excludeNonFree, boolean countCheckout, boolean countRegistration);

    public IdentifiableWrapper<UniplacesHabitablePlace> getWrappedPlace(UnisettleContractAccomodation accomodation);

    public void wrapDataSource(DynamicListDataSource<UniplacesHabitablePlace> dataSource);

    public boolean checkEnoughSpaceOnContractAcceptance(UnisettleContract contract);
}
