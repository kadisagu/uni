// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.request.RequestAddEdit;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;

import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.dao.IUnisettleRequestDAO;
import ru.tandemservice.unisettle.entity.catalog.UnisettleRequestState;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getRequest().getId() != null)
        {
            model.setRequest(get(UnisettleRequest.class, model.getRequest().getId()));
            model.setPerson(model.getRequest().getResident().getPerson());
        }
        else if (model.getPerson().getId() != null)
            model.setPerson(get(Person.class, model.getPerson().getId()));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();
        final UnisettleRequest request = model.getRequest();
        Person person = request.getId() == null ? model.getPerson() : model.getRequest().getResident().getPerson();

        NamedSyncInTransactionCheckLocker.register(session, person.getId() + ".unisettleRequest");

        if (request.getId() == null)
        {
            if (!IUnisettleRequestDAO.instance.get().isAddingRequestAllowed(person.getId()))
                throw new ApplicationException("Невозможно зарегистрировать заявление, так как уже существует заявление на поселение в состоянии «Активно» или «Удовлетворено» для данной персоны.");
            UnisettleResident resident = null;
            List<UnisettleResident> residents = getList(UnisettleResident.class, UnisettleResident.person().s(), person, "id");
            if (!residents.isEmpty()) resident = residents.get(residents.size() - 1);
            if (null == resident)
            {
                NamedSyncInTransactionCheckLocker.register(session, person.getId() + ".unisettleResident");
                resident = new UnisettleResident();
                resident.setPerson(person);
                getSession().save(resident);
            }
            request.setResident(resident);
            request.setState(get(UnisettleRequestState.class, "code", UnisettleDefines.REQUEST_STATE_REGISTERED));
            UniDaoFacade.getNumberDao().execute("unisettleRequest", new INumberQueueDAO.Action()
            {
                @Override
                public int execute(int number)
                {
                    request.setNumber(number + 1);
                    return number + 1;
                }
            });
        }
        
        if (model.isDecline())
        {
            if (!request.getState().getCode().equals(UnisettleDefines.REQUEST_STATE_REGISTERED))
                throw new ApplicationException("Невозможно отклонить заявление, так как оно находится в состоянии «"+request.getState().getTitle()+"».");    
            request.setState(get(UnisettleRequestState.class, "code", UnisettleDefines.REQUEST_STATE_DECLINED));
        }

        session.saveOrUpdate(request);
    }
}
