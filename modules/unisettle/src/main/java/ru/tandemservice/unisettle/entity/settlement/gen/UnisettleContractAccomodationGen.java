package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Место заселения по договору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleContractAccomodationGen extends EntityBase
 implements INaturalIdentifiable<UnisettleContractAccomodationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation";
    public static final String ENTITY_NAME = "unisettleContractAccomodation";
    public static final int VERSION_HASH = 1690589226;
    private static IEntityMeta ENTITY_META;

    public static final String L_PLACE = "place";
    public static final String L_CONTRACT = "contract";
    public static final String P_AREA = "area";
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    private UniplacesHabitablePlace _place;     // Жилое помещение
    private UnisettleContract _contract;     // Договор на поселение
    private long _area;     // Площадь, кв. м

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Жилое помещение. Свойство не может быть null.
     */
    @NotNull
    public UniplacesHabitablePlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Жилое помещение. Свойство не может быть null.
     */
    public void setPlace(UniplacesHabitablePlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Договор на поселение. Свойство не может быть null.
     */
    @NotNull
    public UnisettleContract getContract()
    {
        return _contract;
    }

    /**
     * @param contract Договор на поселение. Свойство не может быть null.
     */
    public void setContract(UnisettleContract contract)
    {
        dirty(_contract, contract);
        _contract = contract;
    }

    /**
     * @return Площадь, кв. м. Свойство не может быть null.
     */
    @NotNull
    public long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь, кв. м. Свойство не может быть null.
     */
    public void setArea(long area)
    {
        dirty(_area, area);
        _area = area;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisettleContractAccomodationGen)
        {
            if (withNaturalIdProperties)
            {
                setPlace(((UnisettleContractAccomodation)another).getPlace());
                setContract(((UnisettleContractAccomodation)another).getContract());
            }
            setArea(((UnisettleContractAccomodation)another).getArea());
        }
    }

    public INaturalId<UnisettleContractAccomodationGen> getNaturalId()
    {
        return new NaturalId(getPlace(), getContract());
    }

    public static class NaturalId extends NaturalIdBase<UnisettleContractAccomodationGen>
    {
        private static final String PROXY_NAME = "UnisettleContractAccomodationNaturalProxy";

        private Long _place;
        private Long _contract;

        public NaturalId()
        {}

        public NaturalId(UniplacesHabitablePlace place, UnisettleContract contract)
        {
            _place = ((IEntity) place).getId();
            _contract = ((IEntity) contract).getId();
        }

        public Long getPlace()
        {
            return _place;
        }

        public void setPlace(Long place)
        {
            _place = place;
        }

        public Long getContract()
        {
            return _contract;
        }

        public void setContract(Long contract)
        {
            _contract = contract;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UnisettleContractAccomodationGen.NaturalId) ) return false;

            UnisettleContractAccomodationGen.NaturalId that = (NaturalId) o;

            if( !equals(getPlace(), that.getPlace()) ) return false;
            if( !equals(getContract(), that.getContract()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPlace());
            result = hashCode(result, getContract());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPlace());
            sb.append("/");
            sb.append(getContract());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleContractAccomodationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleContractAccomodation.class;
        }

        public T newInstance()
        {
            return (T) new UnisettleContractAccomodation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "place":
                    return obj.getPlace();
                case "contract":
                    return obj.getContract();
                case "area":
                    return obj.getArea();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesHabitablePlace) value);
                    return;
                case "contract":
                    obj.setContract((UnisettleContract) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "place":
                        return true;
                case "contract":
                        return true;
                case "area":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "place":
                    return true;
                case "contract":
                    return true;
                case "area":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "place":
                    return UniplacesHabitablePlace.class;
                case "contract":
                    return UnisettleContract.class;
                case "area":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisettleContractAccomodation> _dslPath = new Path<UnisettleContractAccomodation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleContractAccomodation");
    }
            

    /**
     * @return Жилое помещение. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getPlace()
     */
    public static UniplacesHabitablePlace.Path<UniplacesHabitablePlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Договор на поселение. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getContract()
     */
    public static UnisettleContract.Path<UnisettleContract> contract()
    {
        return _dslPath.contract();
    }

    /**
     * @return Площадь, кв. м. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getFractionalArea()
     */
    public static SupportedPropertyPath<Double> fractionalArea()
    {
        return _dslPath.fractionalArea();
    }

    public static class Path<E extends UnisettleContractAccomodation> extends EntityPath<E>
    {
        private UniplacesHabitablePlace.Path<UniplacesHabitablePlace> _place;
        private UnisettleContract.Path<UnisettleContract> _contract;
        private PropertyPath<Long> _area;
        private SupportedPropertyPath<Double> _fractionalArea;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Жилое помещение. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getPlace()
     */
        public UniplacesHabitablePlace.Path<UniplacesHabitablePlace> place()
        {
            if(_place == null )
                _place = new UniplacesHabitablePlace.Path<UniplacesHabitablePlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Договор на поселение. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getContract()
     */
        public UnisettleContract.Path<UnisettleContract> contract()
        {
            if(_contract == null )
                _contract = new UnisettleContract.Path<UnisettleContract>(L_CONTRACT, this);
            return _contract;
        }

    /**
     * @return Площадь, кв. м. Свойство не может быть null.
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(UnisettleContractAccomodationGen.P_AREA, this);
            return _area;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation#getFractionalArea()
     */
        public SupportedPropertyPath<Double> fractionalArea()
        {
            if(_fractionalArea == null )
                _fractionalArea = new SupportedPropertyPath<Double>(UnisettleContractAccomodationGen.P_FRACTIONAL_AREA, this);
            return _fractionalArea;
        }

        public Class getEntityClass()
        {
            return UnisettleContractAccomodation.class;
        }

        public String getEntityName()
        {
            return "unisettleContractAccomodation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getFractionalArea();
}
