package ru.tandemservice.unisettle.entity.settlement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор найма на время работы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisettleSumAreaContractGen extends UnisettleContract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract";
    public static final String ENTITY_NAME = "unisettleSumAreaContract";
    public static final int VERSION_HASH = -1329964777;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisettleSumAreaContractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisettleSumAreaContractGen> extends UnisettleContract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisettleSumAreaContract.class;
        }

        public T newInstance()
        {
            return (T) new UnisettleSumAreaContract();
        }
    }
    private static final Path<UnisettleSumAreaContract> _dslPath = new Path<UnisettleSumAreaContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisettleSumAreaContract");
    }
            

    public static class Path<E extends UnisettleSumAreaContract> extends UnisettleContract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UnisettleSumAreaContract.class;
        }

        public String getEntityName()
        {
            return "unisettleSumAreaContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
