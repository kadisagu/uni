// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractAccomodationAddEdit;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.dao.IUnisettleSettlementDAO;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;

/**
 * @author oleyba
 * @since 10.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEditForm(model.getAccomodation().getId() != null);

        if (model.isEditForm())
        {
            UnisettleContractAccomodation accomodation = get(UnisettleContractAccomodation.class, model.getAccomodation().getId());
            model.setAccomodation(accomodation);
            model.setPlace(IUnisettleSettlementDAO.instance.get().getWrappedPlace(model.getAccomodation()));
            model.setContract(accomodation.getContract());
            model.setShowArea(!(accomodation.getContract() instanceof UnisettleSingleBedContract));
            return;
        }

        UnisettleContract contract = (UnisettleContract) get(model.getContractId());
        model.setContract(contract);
        model.setShowArea(!(contract instanceof UnisettleSingleBedContract));

        if (model.getAccomodation().getContract() == null)
        {
            model.getAccomodation().setContract(contract);
            model.getAccomodation().setArea(0);
        }

        initFilterModels(model);

    }

    @Override
    public void update(Model model)
    {
        getSession().refresh(model.getContract());
        if (!UnisettleDefines.CONTRACT_STATE_FORMING.equals(model.getContract().getState().getCode()))
            throw new ApplicationException("Изменять договор можно только в состоянии «Формируется».");

        if (!model.isEditForm())
            model.getAccomodation().setPlace(get(UniplacesHabitablePlace.class, model.getPlace().getId()));
        getSession().saveOrUpdate(model.getAccomodation());
    }

    private void initFilterModels(final Model model)
    {
        model.setBuildingList(new LazySimpleSelectModel<UniplacesBuilding>(UniplacesBuilding.class));
        model.setUnitList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesUnit.ENTITY_CLASS, alias);
                if (null == model.getBuilding())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesUnit.building().s(), model.getBuilding()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesUnit.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setFloorList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(UniplacesFloor.ENTITY_CLASS, alias);
                if (null == model.getUnit())
                    builder.add(MQExpression.isNull(alias, "id"));
                else
                    builder.add(MQExpression.eq(alias, UniplacesFloor.unit().s(), model.getUnit()));
                if (StringUtils.isNotBlank(filter))
                    builder.add(MQExpression.like(alias, UniplacesFloor.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder(alias, UniplacesFloor.title().s());
                return builder;
            }
        }.setPageSize(100));
        model.setPlaceList(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<IdentifiableWrapper<UniplacesHabitablePlace>> places = IUnisettleSettlementDAO.instance.get().getWrappedPlacesList(model.getFloor(), model.getContract(), !model.isShowArea(), !model.isShowAll(), model.isCountCheckout(), model.isCountRegistration());

                if (places.isEmpty())
                    return ListResult.getEmpty();
                return new ListResult<IdentifiableWrapper<UniplacesHabitablePlace>>(places);
            }
        });
    }

}
