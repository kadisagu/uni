/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle;

/**
 * @author agolubenko
 * @since Sep 8, 2010
 */
public interface UnisettleComponents
{
    String REQUEST_ADD_EDIT = "ru.tandemservice.unisettle.component.request.RequestAddEdit";

    String SETTLEMENT_EDIT = "ru.tandemservice.unisettle.component.settlement.SettlementEdit";
    String SETTLEMENT_MASS_EDIT = "ru.tandemservice.unisettle.component.settlement.SettlementMassEdit";

    String RESIDENT_ADD = "ru.tandemservice.unisettle.component.resident.ResidentAdd";

    String SINGLE_BED_CONTRACT_ADD_EDIT = "ru.tandemservice.unisettle.component.contract.SingleBedContractAddEdit";
    String AREA_SUM_CONTRACT_ADD_EDIT = "ru.tandemservice.unisettle.component.contract.SumAreaContractAddEdit";
    String CONTRACT_ARCHIVE = "ru.tandemservice.unisettle.component.contract.ContractArchive";
    String CONTRACT_ACCOMODATION_ADD_EDIT = "ru.tandemservice.unisettle.component.contract.ContractAccomodationAddEdit";
    String CONTRACT_RESIDENT_ADD_EDIT = "ru.tandemservice.unisettle.component.contract.ContractResidentAddEdit";
    String CONTRACT_MASS_ADD = "ru.tandemservice.unisettle.component.contract.SingleContractMassAdd";
    String RELATIVE_AS_RESIDENT_ADD = "ru.tandemservice.unisettle.component.contract.RelativeAsResidentAdd";
}

