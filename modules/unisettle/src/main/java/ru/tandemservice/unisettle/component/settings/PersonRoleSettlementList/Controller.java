// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settings.PersonRoleSettlementList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 22.11.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
	@Override
	public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<Model.PersonRoleWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new org.tandemframework.core.view.list.column.CheckboxColumn().setSelectCaption("Выбор").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Название", "title").setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
}