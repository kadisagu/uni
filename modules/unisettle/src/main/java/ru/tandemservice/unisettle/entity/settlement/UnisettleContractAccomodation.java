package ru.tandemservice.unisettle.entity.settlement;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.gen.UnisettleContractAccomodationGen;

/**
 * Место заселения по договору
 */
public class UnisettleContractAccomodation extends UnisettleContractAccomodationGen
{
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    @Override
    @EntityDSLSupport(parts = UnisettleContractResident.P_AREA)
    public Double getFractionalArea()
    {
        Long area = getArea();
        return (area != null) ? 0.0001d * area : null;
    }

    @EntityDSLSupport(parts = UnisettleContractResident.P_AREA)
    public void setFractionalArea(Double area)
    {
        setArea((area != null) ? Math.round(area * 10000.0d) : null);
    }

    public boolean isEditDisabled()
    {
        return !UnisettleDefines.CONTRACT_STATE_FORMING.equals(getContract().getState().getCode());
    }
}