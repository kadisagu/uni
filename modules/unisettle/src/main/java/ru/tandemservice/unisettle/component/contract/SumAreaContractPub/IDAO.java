// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SumAreaContractPub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;

/**
 * @author oleyba
 * @since 11.11.2010
 */
public interface IDAO extends IUniDao<Model>
{
    void doChangeState(Model model, UnisettleContractStateChange stateChange);

    void prepareAccomodationDataSource(Model model);

    void deleteAccomodation(Model model, Long id);

    void deleteResident(Model model, Long id);

    void prepareResidentDataSource(Model model);
}
