// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.dao;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.entity.settlement.UnisettleRequest;

/**
 * @author oleyba
 * @since 14.10.2010
 */
public class UnisettleRequestDAO extends UniBaseDao implements IUnisettleRequestDAO
{
    @Override
    public boolean isAddingRequestAllowed(Long personId)
    {
        MQBuilder builder = new MQBuilder(UnisettleRequest.ENTITY_CLASS, "request");
        builder.add(MQExpression.eq("request", UnisettleRequest.resident().person().id(), personId));
        builder.add(MQExpression.or(
                MQExpression.eq("request", UnisettleRequest.state().code(), UnisettleDefines.REQUEST_STATE_REGISTERED),
                MQExpression.eq("request", UnisettleRequest.state().code(), UnisettleDefines.REQUEST_STATE_ACCEPTED)));
        return builder.getResultCount(getSession()) == 0;
    }
}
