// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.ContractResidentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.unisettle.entity.settlement.UnisettleContract;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

/**
 * @author oleyba
 * @since 10.11.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;
        getDao().update(model);
        deactivate(component);
    }

    public void onChangeResident(IBusinessComponent component)
    {
        Model model = getModel(component);
        UnisettleContract contract = model.getResident().getContract();
        model.setShowRelationDegree(contract instanceof UnisettleSumAreaContract);
        if (contract.getResident().getId().equals(model.getResident().getId()))
            model.setShowRelationDegree(false);
    }
}