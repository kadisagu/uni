/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.settlement.SettlementEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesHabitablePlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author agolubenko
 * @since Sep 8, 2010
 */
@Input(@Bind(key = "settlementId", binding = "settlement.id"))
public class Model
{
    private UniplacesHabitablePlace _settlement = new UniplacesHabitablePlace();

    private List<HSelectOption> _purposeList;
    private List<UniplacesPlaceCondition> _conditionList;

    public String getFieldSetTitle()
    {
        UniplacesPlace place = getSettlement().getPlace();
        UniplacesFloor floor = place.getFloor();
        UniplacesUnit unit = floor.getUnit();
        UniplacesBuilding building = unit.getBuilding();

        return String.format("Данные места поселения Здание №%s Блок %s Этаж %s Помещение %s", building.getFullNumber(), unit.getTitle(), floor.getTitle(), place.getNumber());
    }

    public UniplacesPlace getPlace()
    {
        return getSettlement().getPlace();
    }

    public boolean isShowBedNumber()
    {
        return getSettlement().getPlace().getPurpose() != null && getSettlement().getPlace().getPurpose().isCountBedNumber();
    }

    public UniplacesHabitablePlace getSettlement()
    {
        return _settlement;
    }

    public void setSettlement(UniplacesHabitablePlace settlement)
    {
        _settlement = settlement;
    }

    public List<HSelectOption> getPurposeList()
    {
        return _purposeList;
    }

    public void setPurposeList(List<HSelectOption> purposeList)
    {
        _purposeList = purposeList;
    }

    public List<UniplacesPlaceCondition> getConditionList()
    {
        return _conditionList;
    }

    public void setConditionList(List<UniplacesPlaceCondition> conditionList)
    {
        _conditionList = conditionList;
    }
}