// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.SumAreaContractPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisettle.UnisettleDefines;
import ru.tandemservice.unisettle.component.contract.utils.UnisettleContractStateChange;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractResident;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSumAreaContract;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 11.11.2010
 */
@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "contract.id"))
public class Model
{
    private UnisettleSumAreaContract contract = new UnisettleSumAreaContract();
    private DynamicListDataSource<UnisettleContractResident> residentDataSource;
    private DynamicListDataSource<UnisettleContractAccomodation> accomodationDataSource;

    private List<UnisettleContractStateChange> stateChangeList = UnisettleContractStateChange.getChangeList("Area");
    private UnisettleContractStateChange currentStateChange;

    private boolean residentFromContractAdded;

    public Map<String, Object> getResidentLinkParams()
    {
        if (null == getContract().getResident())
            return null;
        return new ParametersMap()
            .add("selectedTab", "residentTab")
            .add("selectedDataTab", "residentContractTab")
            .add(PublisherActivator.PUBLISHER_ID_KEY, getContract().getResident().getId());
    }

    public boolean isStateChangeAllowed()
    {
        return getCurrentStateChange() != null && getCurrentStateChange().getAllowedStatesCodeList().contains(getContract().getState().getCode());
    }

    public boolean isEditDisabled()
    {
        return !UnisettleDefines.CONTRACT_STATE_FORMING.equals(getContract().getState().getCode());
    }

    public boolean isAddResidentFromContractDisabled()
    {
        return isEditDisabled() || residentFromContractAdded;
    }

    public UnisettleSumAreaContract getContract()
    {
        return contract;
    }

    public void setContract(UnisettleSumAreaContract contract)
    {
        this.contract = contract;
    }

    public void setResidentFromContractAdded(boolean residentFromContractAdded)
    {
        this.residentFromContractAdded = residentFromContractAdded;
    }

    public DynamicListDataSource<UnisettleContractResident> getResidentDataSource()
    {
        return residentDataSource;
    }

    public void setResidentDataSource(DynamicListDataSource<UnisettleContractResident> residentDataSource)
    {
        this.residentDataSource = residentDataSource;
    }

    public DynamicListDataSource<UnisettleContractAccomodation> getAccomodationDataSource()
    {
        return accomodationDataSource;
    }

    public void setAccomodationDataSource(DynamicListDataSource<UnisettleContractAccomodation> accomodationDataSource)
    {
        this.accomodationDataSource = accomodationDataSource;
    }

    public List<UnisettleContractStateChange> getStateChangeList()
    {
        return stateChangeList;
    }

    public void setStateChangeList(List<UnisettleContractStateChange> stateChangeList)
    {
        this.stateChangeList = stateChangeList;
    }

    public UnisettleContractStateChange getCurrentStateChange()
    {
        return currentStateChange;
    }

    public void setCurrentStateChange(UnisettleContractStateChange currentStateChange)
    {
        this.currentStateChange = currentStateChange;
    }
}
