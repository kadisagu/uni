// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisettle.component.contract.RelativeAsResidentAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.unisettle.entity.settlement.UnisettleResident;

/**
 * @author oleyba
 * @since 19.11.2010
 */
public class Controller extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Controller<IDAO, Model, UnisettleResident>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<PersonNextOfKin> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(getModel(component1), component1.getSettings());
        });

        RadioButtonColumn radioButtonColumn = new RadioButtonColumn("radio");
        radioButtonColumn.setClearable(false);
        radioButtonColumn.setWidth(1);
        dataSource.addColumn(radioButtonColumn);
        dataSource.addColumn(new SimpleColumn("ФИО", PersonNextOfKin.P_FULLFIO).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Степень родства", PersonNextOfKin.relationDegree().title().s()).setClickable(false).setOrderable(false));

        model.setDataSource(dataSource);
    }

    @Override
    protected void startNewInstancePublisher(IBusinessComponent component, UnisettleResident newInstance)
    {

    }

    public void onClickFill(IBusinessComponent component)
    {
        getDao().prepareIdentityCardFields(getModel(component));
    }
}