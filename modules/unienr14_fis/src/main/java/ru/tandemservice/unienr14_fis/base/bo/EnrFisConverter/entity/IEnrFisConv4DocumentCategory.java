/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public interface IEnrFisConv4DocumentCategory
{
    /** Категория для документов **/
    PersonDocumentCategory getDocCategory();

    /** Значение (элемент справочника ФИС) **/
    EnrFisCatalogItem getValue();
}
