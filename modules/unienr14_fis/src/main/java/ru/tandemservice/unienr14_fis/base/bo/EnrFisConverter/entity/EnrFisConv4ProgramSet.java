package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/**
 * ФИС: Сопоставление: Набор ОП для приема
 */
public class EnrFisConv4ProgramSet extends EnrFisConv4ProgramSetGen
{
    public EnrFisConv4ProgramSet()
    {
    }

    public EnrFisConv4ProgramSet(EnrProgramSetBase programSet, EnrFisCatalogItem value)
    {
        setProgramSet(programSet);
        setValue(value);
    }
}