package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x4_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.4")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrFisSyncSession

        // создано обязательное свойство skippedRecItemCount
        if(tool.tableExists("enr14fis_s_period_key_t"))
        {
            SQLSelectQuery keyQuery = new SQLSelectQuery()
                    .from(SQLFrom.table("enr14fis_s_period_key_t", "k")
                                    .innerJoin(SQLFrom.table("enr14fis_catalog_item_t", "ci"), "ci.id=k.educationsource_id"))
                    .column("k.id", "id")
                    .where("ci.fiscatalogcode_p='15'")
                    .where("ci.fisitemcode_p='20'");

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();
            SQLSelectQuery itemQuery = new SQLSelectQuery()
                    .from(
                            SQLFrom.table("enr14fis_s_period_item_t", "i").innerJoin(SQLFrom.table("enr14fis_s_period_key_t", "k"), "i.key_id=k.id")
                                    .innerJoin(SQLFrom.table("enr14fis_catalog_item_t", "ci"), "ci.id=k.educationsource_id")
                    )
                    .column("i.id", "id")
                    .where("ci.fiscatalogcode_p='15'")
                    .where("ci.fisitemcode_p='20'");

            tool.executeUpdate("delete from enr14fis_s_period_item_t where id in (" + translator.toSql(itemQuery)  + ")");
            tool.executeUpdate("delete from enr14fis_s_period_key_t where id in (" + translator.toSql(keyQuery)  + ")");
        }
    }
}