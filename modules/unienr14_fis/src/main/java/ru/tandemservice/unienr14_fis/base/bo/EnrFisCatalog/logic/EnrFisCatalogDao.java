package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.log.UpdateEntityEvent;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.EnrFisCatalogManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.gen.EnrFisCatalogItemGen;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;
import ru.tandemservice.unienr14_fis.util.IEnrFisAuthentication;
import ru.tandemservice.unienr14_fis.util.IEnrFisService;
import ru.tandemservice.unienr14_fis.util.IllegalConnectionParametersException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrFisCatalogDao extends UniBaseDao implements IEnrFisCatalogDao
{
    public static final String LOG_FILE_NAME = "enr14fis-catalogSync";

    private static final boolean debug = Debug.isEnabled();

    protected static Logger initFileLogger(final Class<?> klass, final Level threshold)
    {
        final Logger logger = Logger.getLogger(klass);
        try {
            final FileAppender appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} [%t] %p - %m%n"), getLogFilePath());
            appender.setName("file-" + LOG_FILE_NAME + "-appender");
            appender.setThreshold(threshold);
            logger.addAppender(appender);
            logger.setLevel(threshold);
            logger.setAdditivity(false);
            return logger;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static String getLogFilePath()
    {
        return Paths.get(System.getProperty("catalina.base"), "logs", LOG_FILE_NAME + ".log").toString();
    }

    protected final Logger logger = initFileLogger(this.getClass(), Level.INFO);

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // получение данных об элементах справочника

    protected <T, I> Map<Long, String> getFisCatalogItems(final IEnrFisAuthentication auth, final Entry<Long, String> entry, final EnrFisCatalogDataHandler<T, I> handler)
    {
        this.logger.info("getFisCatalogItems["+entry.getKey()+"]: begin");
        final Map<Long, String> data = new LinkedHashMap<>();
        try {
            final IEnrFisService service = IEnrFisService.instance.get();

            // готовим запрос
            final enr14fis.schema.catalog.items.req.Root root = new enr14fis.schema.catalog.items.req.Root();
            root.setGetDictionaryContent(new enr14fis.schema.catalog.items.req.GetDictionaryContent());
            root.setAuthData(new enr14fis.schema.catalog.items.req.AuthData());
            root.getGetDictionaryContent().setDictionaryCode(entry.getKey());
            root.getAuthData().setLogin(auth.getLogin());
            root.getAuthData().setPass(auth.getPassword());

            // посылаем запрос, получаем ответ
            final T dictionaryData;
            {
                final IEnrFisService.EnrFisConnection connection = service.connect("dictionarydetails");
                try {
                    dictionaryData = connection.call(handler.getResponseKlass(), root);
                } catch (final IllegalConnectionParametersException e) {
                    UserContext.getInstance().getErrorCollector().add(e.getMessage());
                    throw e;
                } catch (final Throwable t) {
                    this.logger.error(connection.getRequest());
                    this.logger.error(connection.getResponse());
                    throw t;
                }

                if (debug) {
                    this.logger.info(connection.getRequest());
                    this.logger.info(connection.getResponse());
                }
            }

            // проверяем, что справочник именно тот, который запрашивали (были случаи, когда не совпадало)
            final Long catalogCode = handler.getCatalogCode(dictionaryData);
            if (!entry.getKey().equals(catalogCode)) {
                throw new IllegalStateException("getFisCatalogItems["+entry.getKey()+"]: illegal catalog code «"+catalogCode+"» (req=«"+entry.getKey()+"»)");
            }

            final String catalogName = handler.getCatalogName(dictionaryData);
            if (!entry.getValue().equals(catalogName)) {
                throw new IllegalStateException("getFisCatalogItems["+entry.getKey()+"]: illegal catalog name «"+catalogName+"» (req=«"+entry.getValue()+"»)");
            }

            // предварительный показ справочника
            for (final I item: handler.getItems(dictionaryData)) {
                final Long id = handler.getItemID(item);
                final String name = StringUtils.trimToEmpty(handler.getItemName(item));
                this.logger.info("getFisCatalogItems["+entry.getKey()+"]: ["+id+"]=«"+name+"»");
            }

            // обработка результата
            final Set<Long> ducplicateIdSet = new HashSet<>();
            final Set<String> nameSet = new HashSet<>();
            for (final I item: handler.getItems(dictionaryData)) {
                final Long id = handler.getItemID(item);
                final String name = StringUtils.trimToEmpty(handler.getItemName(item));

                if (!nameSet.add(name)) {
                    // если у нас есть элементы с разными идентификаторами, но одинаковыми названиями
                    // то к названию прицепляем идентификатор ФИС (чтобы как-то различать в интерфейсе)
                    ducplicateIdSet.add(id);
                    this.logger.warn("getFisCatalogItems["+entry.getKey()+"]: duplicate item name «"+name+"»");
                }

                final String old = data.put(id, name);
                if (null != old) {
                    final String message = "getFisCatalogItems["+entry.getKey()+"]: duplicate item id «"+id+"»";
                    if (old.equals(name)) {
                        // то же значение (видимо, просто дубль)
                        this.logger.warn(message);
                    } else {
                        // значение иное - это сразу смерть (нужно делать обработчик для этого справочника)
                        throw new IllegalStateException(message);
                    }
                }
            }

            // если для элемента есть дубль - то добавляем в него id (чтобы как-то отличать их в интерфейсе)
            // http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163108&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163108
            for (final Long id: ducplicateIdSet) {
                data.put(id, data.get(id) + ", id="+id);
            }

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            this.logger.info("getFisCatalogItems[" + entry.getKey() + "]: end\n");
        }
        return data;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // получение данных об элементах справочника

    protected <T, I> Map<Long, String> getFisCatalogItemsFromFile(final Entry<Long, String> entry, final EnrFisCatalogDataHandler<T, I> handler, IUploadFile file)
    {
        this.logger.info("getFisCatalogItems["+entry.getKey()+"]: begin");
        final Map<Long, String> data = new LinkedHashMap<>();
        T dictionaryData = null;
        try {

            byte[] buffer = new byte[1024];

            ZipInputStream zis = new ZipInputStream(file.getStream());
            ZipEntry entryZip;

            while ((entryZip = zis.getNextEntry()) != null)
            {
                if(entry.getKey().equals(Long.parseLong(entryZip.getName().split("\\.")[0])))
                {
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, len);
                    }
                    dictionaryData = EnrFisServiceImpl.fromXml(handler.getResponseKlass(), outputStream.toByteArray());
                    break;
                }
            }

            zis.closeEntry();
            zis.close();

            if(dictionaryData == null) return data;

            // проверяем, что справочник именно тот, который запрашивали (были случаи, когда не совпадало)
            final Long catalogCode = handler.getCatalogCode(dictionaryData);
            if (!entry.getKey().equals(catalogCode)) {
                throw new IllegalStateException("getFisCatalogItems["+entry.getKey()+"]: illegal catalog code «"+catalogCode+"» (req=«"+entry.getKey()+"»)");
            }

            final String catalogName = handler.getCatalogName(dictionaryData);
            if (!entry.getValue().equals(catalogName)) {
                throw new IllegalStateException("getFisCatalogItems["+entry.getKey()+"]: illegal catalog name «"+catalogName+"» (req=«"+entry.getValue()+"»)");
            }

            // предварительный показ справочника
            for (final I item: handler.getItems(dictionaryData)) {
                final Long id = handler.getItemID(item);
                final String name = StringUtils.trimToEmpty(handler.getItemName(item));
                this.logger.info("getFisCatalogItems["+entry.getKey()+"]: ["+id+"]=«"+name+"»");
            }

            // обработка результата
            final Set<Long> ducplicateIdSet = new HashSet<>();
            final Set<String> nameSet = new HashSet<>();
            for (final I item: handler.getItems(dictionaryData)) {
                final Long id = handler.getItemID(item);
                final String name = StringUtils.trimToEmpty(handler.getItemName(item));

                if (!nameSet.add(name)) {
                    // если у нас есть элементы с разными идентификаторами, но одинаковыми названиями
                    // то к названию прицепляем идентификатор ФИС (чтобы как-то различать в интерфейсе)
                    ducplicateIdSet.add(id);
                    this.logger.warn("getFisCatalogItems["+entry.getKey()+"]: duplicate item name «"+name+"»");
                }

                final String old = data.put(id, name);
                if (null != old) {
                    final String message = "getFisCatalogItems["+entry.getKey()+"]: duplicate item id «"+id+"»";
                    if (old.equals(name)) {
                        // то же значение (видимо, просто дубль)
                        this.logger.warn(message);
                    } else {
                        // значение иное - это сразу смерть (нужно делать обработчик для этого справочника)
                        throw new IllegalStateException(message);
                    }
                }
            }

            // если для элемента есть дубль - то добавляем в него id (чтобы как-то отличать их в интерфейсе)
            // http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163108&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163108
            for (final Long id: ducplicateIdSet) {
                data.put(id, data.get(id) + ", id="+id);
            }

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            this.logger.info("getFisCatalogItems["+entry.getKey()+"]: end\n");
        }
        return data;
    }

    protected void sync(final Long catalogCode, final Map<Long, String> items, final Date now)
    {
        final String catalogCodeAsString = String.valueOf(catalogCode);

        final List<EnrFisCatalogItem> databaseList = new DQLSelectBuilder()
        .fromEntity(EnrFisCatalogItem.class, "i").column(property("i"))
        .where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("i")), value(catalogCodeAsString)))
        .createStatement(this.getSession()).list();

        final List<EnrFisCatalogItem> targetList = new ArrayList<>();
        for (final Entry<Long, String> e: items.entrySet()) {
            final EnrFisCatalogItem item = new EnrFisCatalogItem();
            item.setFisCatalogCode(String.valueOf(catalogCodeAsString));
            item.setFisItemCode(String.valueOf(e.getKey()));
            item.setTitle(e.getValue());
            targetList.add(item);
        }

        new MergeAction.SessionMergeAction<EnrFisCatalogItemGen.NaturalId, EnrFisCatalogItem>() {
            @Override protected EnrFisCatalogItemGen.NaturalId key(final EnrFisCatalogItem source) {
                return (EnrFisCatalogItemGen.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisCatalogItem buildRow(final EnrFisCatalogItem source) {
                return source; // не надо клонировать - здесь всегда новый объект
            }
            @Override protected void fill(final EnrFisCatalogItem target, final EnrFisCatalogItem source) {
                if (!target.getTitle().equals(source.getTitle())) {
                    EnrFisCatalogDao.this.logger.warn("sync["+source.getFisCatalogCode()+", "+source.getFisItemCode()+"]: «"+target.getTitle()+"» -> «"+source.getTitle()+"»");
                }
                target.update(source);
                target.setRemovalDate(null);
            }
            @Override protected void doDeleteRecord(final EnrFisCatalogItem databaseRecord) {
                if (null == databaseRecord.getRemovalDate()) {
                    EnrFisCatalogDao.this.logger.warn("sync["+databaseRecord.getFisCatalogCode()+", "+databaseRecord.getFisItemCode()+"]: delete");
                    databaseRecord.setRemovalDate(now);
                    EnrFisCatalogDao.this.saveOrUpdate(databaseRecord);
                }
            }
        }.merge(databaseList, targetList);
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // получение данных о справочниках

    // проверка того, что справочники действительно те, на которые мы заложились
    // (ФИСовцы любят менять перечень справочников, их коды и названия - если упадет, то мы об этом тут же узнаем)
    private static final Map<Long, Object[]> REQUIRED_CATALOGS;
    static {
        final Map<Long, Object[]> map = new LinkedHashMap<>();
        map.put(1L, new Object[] { "Общеобразовательные предметы",                                       EnrFisCatalogDataHandler.CATALOG_1_DISCIPLINE /* ! */ });
        map.put(2L, new Object[] { "Уровень образования",                                                EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(3L, new Object[] { "Уровень олимпиады",                                                  EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(4L, new Object[] { "Статус заявления",                                                   EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(5L, new Object[] { "Пол",                                                                EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(6L, new Object[] { "Основание для оценки",                                               EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(7L, new Object[] { "Страна",                                                             EnrFisCatalogDataHandler.CATALOG_7_COUNTRIES /* ! */ });
        map.put(8L, new Object[] { "Регион",                                                             EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(9L, new Object[] { "Коды направлений подготовки",                                        EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(10L, new Object[] { "Направления подготовки",                                            EnrFisCatalogDataHandler.CATALOG_10_SPEC     /* ! */ });
        map.put(11L, new Object[] { "Тип вступительных испытаний",                                       EnrFisCatalogDataHandler.CATALOG_11_DISCIPLINE /* ! */ });
        map.put(12L, new Object[] { "Статус проверки заявлений",                                         EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(13L, new Object[] { "Статус проверки документа",                                         EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(14L, new Object[] { "Форма обучения",                                                    EnrFisCatalogDataHandler.CATALOG_14_EDUFORM  /* ! */ });
        map.put(15L, new Object[] { "Источник финансирования",                                           EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(17L, new Object[] { "Сообщения об ошибках",                                              EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(18L, new Object[] { "Тип диплома",                                                       EnrFisCatalogDataHandler.CATALOG_18_OLYMP_TYPE /* ! */ });
        map.put(19L, new Object[] { "Олимпиады",                                                         EnrFisCatalogDataHandler.CATALOG_19_OLYMP    /* ! */ });
        map.put(21L, new Object[] { "Гражданство",                                                       EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(22L, new Object[] { "Тип документа, удостоверяющего личность",                           EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(23L, new Object[] { "Группа инвалидности",                                               EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(24L, new Object[] { "Коды профессий",                                                    EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(25L, new Object[] { "Профессия",                                                         EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(26L, new Object[] { "Коды квалификаций",                                                 EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(27L, new Object[] { "Квалификация",                                                      EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(28L, new Object[] { "Коды специальностей",                                               EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(29L, new Object[] { "Специальности",                                                     EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(30L, new Object[] { "Вид льготы",                                                        EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(31L, new Object[] { "Тип документа",                                                     EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(32L, new Object[] { "Иностранные языки",                                                 EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(33L, new Object[] { "Тип документа для вступительного испытания ОУ",                     EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(34L, new Object[] { "Статус приемной кампании",                                          EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(35L, new Object[] { "Уровень бюджета",                                                   EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(36L, new Object[] { "Категории индивидуальных достижений",                               EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(37L, new Object[] { "Статус апелляции, перепроверки",                                    EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(38L, new Object[] { "Тип приемной кампании",                                             EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(39L, new Object[] { "Профили олимпиад",                                                  EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(40L, new Object[] { "Классы олимпиад",                                                   EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(41L, new Object[] { "Тип населенного пункта",                                            EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(42L, new Object[] { "Тип документа, подтверждающего сиротство",                          EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(43L, new Object[] { "Тип диплома в области спорта",                                      EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        map.put(44L, new Object[] { "Тип документа, подтверждающего принадлежность к соотечественникам", EnrFisCatalogDataHandler.CATALOG_DEFAULT });
        REQUIRED_CATALOGS = Collections.unmodifiableMap(map);
    }

    protected String getFisCatalogName(final Long code) {
        final Object[] row = REQUIRED_CATALOGS.get(code);
        if (null == row) { return null; }
        return (String)row[0];
    }

    protected EnrFisCatalogDataHandler getFisCatalogHandler(final Long code) {
        final Object[] row = REQUIRED_CATALOGS.get(code);
        if (null == row) { return EnrFisCatalogDataHandler.CATALOG_DEFAULT; }
        return (EnrFisCatalogDataHandler)row[1];
    }

    // список справочников
    @SuppressWarnings("deprecation")
    protected Map<Long, String> getFisCatalogCodes(final IEnrFisAuthentication auth)
    {
        this.logger.info("getFisCatalogCodes: begin");
        final Map<Long, String> data = new LinkedHashMap<>();
        try {
            final IEnrFisService service = IEnrFisService.instance.get();

            // готовим запрос
            final enr14fis.schema.catalog.list.req.Root root = new enr14fis.schema.catalog.list.req.Root();
            root.setAuthData(new enr14fis.schema.catalog.list.req.AuthData());
            root.getAuthData().setLogin(auth.getLogin());
            root.getAuthData().setPass(auth.getPassword());

            // посылаем запрос, получаем ответ
            final enr14fis.schema.catalog.list.resp.Dictionaries dictionaries;
            {
                final IEnrFisService.EnrFisConnection connection = service.connect("dictionary");
                try {
                    dictionaries = connection.call(enr14fis.schema.catalog.list.resp.Dictionaries.class, root);
                } catch (final IllegalConnectionParametersException e) {
                    UserContext.getInstance().getErrorCollector().add(e.getMessage());
                    throw e;
                } catch (final Throwable t) {
                    this.logger.error(connection.getRequest());
                    this.logger.error(connection.getResponse());
                    throw t;
                }

                if (debug) {
                    this.logger.info(connection.getRequest());
                    this.logger.info(connection.getResponse());
                }
            }

            // предварительный показ справочника
            for (final enr14fis.schema.catalog.list.resp.Dictionaries.Dictionary dict: dictionaries.getDictionary()) {
                final String reqName = this.getFisCatalogName(dict.getCode());
                final String name = StringUtils.trimToEmpty(dict.getName());
                this.logger.info("getFisCatalogCodes: ["+dict.getCode()+"]=«"+name+"» (req=«"+reqName+"»)");
            }

            // обработка результата
            for (final enr14fis.schema.catalog.list.resp.Dictionaries.Dictionary dict: dictionaries.getDictionary()) {
                final String reqName = this.getFisCatalogName(dict.getCode());
                final String name = StringUtils.trimToEmpty(dict.getName());
                if (null == reqName) {
                    // выводим сообщение, что у нас появился левый справочник - плохой знак, но не фатально
                    this.logger.warn("getFisCatalogCodes: undefined catalog with code «"+dict.getCode()+"»");
                } else if (!reqName.equals(name)) {
                    // название справочника изменилось - они что-то изменили и не сказали нам, при этом мы ничего не исправляли
                    // это фатальный баг, надо править код, даже если все кажется работающим (есть шанс выгрузить значение не того справочника, который они ждали)
                    throw new IllegalStateException("getFisCatalogCodes: illegal catalog name for «"+dict.getCode()+"»=«"+name+"» (req=«"+reqName+"»)");
                }

                final String old = data.put(dict.getCode(), name);
                if (null != old) {
                    final String message = "getFisCatalogCodes: duplicate catalog code «"+dict.getCode()+"»";
                    if (old.equals(name)) {
                        this.logger.warn(message);
                    } else {
                        throw new IllegalStateException(message);
                    }
                }
            }
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            this.logger.info("getFisCatalogCodes: end\n");
        }
        return data;
    }

    // список справочников
    @SuppressWarnings("deprecation")
    protected Map<Long, String> getFisCatalogCodes()
    {
        return new org.tandemframework.shared.commonbase.base.util.UniMap.Parametrized<Long, String>()
                .add(1L, "Общеобразовательные предметы")
                .add(2L, "Уровень образования")
                .add(3L, "Уровень олимпиады")
                .add(4L, "Статус заявления")
                .add(5L, "Пол")
                .add(6L, "Основание для оценки")
                .add(7L, "Страна")
                .add(8L, "Регион")
                .add(9L, "Коды направлений подготовки")
                .add(10L, "Направления подготовки")
                .add(11L, "Тип вступительных испытаний")
                .add(12L, "Статус проверки заявлений")
                .add(13L, "Статус проверки документа")
                .add(14L, "Форма обучения")
                .add(15L, "Источник финансирования")
                .add(17L, "Сообщения об ошибках")
                .add(18L, "Тип диплома")
                .add(19L, "Олимпиады")
                .add(21L, "Гражданство")
                .add(22L, "Тип документа, удостоверяющего личность")
                .add(23L, "Группа инвалидности")
                .add(24L, "Коды профессий")
                .add(25L, "Профессия")
                .add(26L, "Коды квалификаций")
                .add(27L, "Квалификация")
                .add(28L, "Коды специальностей")
                .add(29L, "Специальности")
                .add(30L, "Вид льготы")
                .add(31L, "Тип документа")
                .add(32L, "Иностранные языки")
                .add(33L, "Тип документа для вступительного испытания ОУ")
                .add(34L, "Статус приемной кампании")
                .add(35L, "Уровень бюджета")
                .add(36L, "Категории индивидуальных достижений")
                .add(37L, "Статус апелляции, перепроверки")
                .add(38L, "Тип приемной кампании")
                .add(39L, "Профили олимпиад")
                .add(40L, "Классы олимпиад")
                .add(41L, "Тип населенного пункта")
                .add(42L, "Тип документа, подтверждающего сиротство")
                .add(43L, "Тип диплома в области спорта")
                .add(44L, "Тип документа, подтверждающего принадлежность к соотечественникам");
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doSyncCatalogs(final IEnrFisAuthentication auth)
    {
        if (StringUtils.isBlank(ApplicationRuntime.getProperty("uni.enr.fis.connection.url"))) {
            throw new ApplicationException(EnrFisCatalogManager.instance().getProperty("error.auth-error.not-fis-connection-url"));
        }

        if (StringUtils.isBlank(auth.getLogin())) {
            throw new ApplicationException(EnrFisCatalogManager.instance().getProperty("error.auth-error.no-login"));
        }
        if (StringUtils.isBlank(auth.getPassword())) {
            throw new ApplicationException(EnrFisCatalogManager.instance().getProperty("error.auth-error.no-password"));
        }

        clearLogFile();
        this.logger.info("\n--------------------------------------------------------------------------------\n");
        this.logger.info("doSyncCatalogs.start");
        try
        {
            final Date now = new Date();

            // получаем список справочников, затем синхранизируем каждый по отдельности
            final Map<Long, String> codes = this.getFisCatalogCodes(auth);
            for (final Entry<Long, String> e: codes.entrySet()) {
                final EnrFisCatalogDataHandler handler = this.getFisCatalogHandler(e.getKey());
                final Map<Long, String> items = this.getFisCatalogItems(auth, e, handler);
                this.sync(e.getKey(), items, now);
            }

            this.logger.info("doSyncCatalogs.complete\n");
        }
        catch (final Exception t)
        {
            this.logger.error("doSyncCatalogs.error: " + t.getMessage(), t);
            Debug.exception(t.getMessage(), t);
            throw new ApplicationException(EnrFisCatalogManager.instance().getProperty("error.sync-error"), t);
        }
        CoreServices.eventService().fireEvent(new UpdateEntityEvent("Справочники ФИС обновлены", TopOrgUnit.getInstance()));
    }

    private void clearLogFile()
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(new File(getLogFilePath()));
            fos.write(new byte[0]);
            fos.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Can't clear log file: " + LOG_FILE_NAME, e);
        }
    }

    @Override
    public void doImportCatalogs(IUploadFile file)
    {
        this.logger.info("\n--------------------------------------------------------------------------------\n");
        this.logger.info("doImportCatalogs.start");
        try
        {
            final Date now = new Date();

            // получаем список справочников, затем синхранизируем каждый по отдельности
            final Map<Long, String> codes = this.getFisCatalogCodes();
            for (final Entry<Long, String> e: codes.entrySet()) {
                final EnrFisCatalogDataHandler handler = this.getFisCatalogHandler(e.getKey());
                final Map<Long, String> items = this.getFisCatalogItemsFromFile(e, handler, file);
                this.sync(e.getKey(), items, now);
            }

            this.logger.info("doImportCatalogs.complete\n");
        }
        catch (final Exception t)
        {
            this.logger.error("doImportCatalogs.error: " + t.getMessage(), t);
            Debug.exception(t.getMessage(), t);
            throw new ApplicationException("Во время обработки файла произошла ошибка. Проверьте корректность файла или обратитесь к администратору системы.", t);
        }
    }
}
