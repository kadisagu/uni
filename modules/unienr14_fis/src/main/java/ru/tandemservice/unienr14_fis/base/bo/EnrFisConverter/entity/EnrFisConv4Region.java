package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4RegionGen */
public class EnrFisConv4Region extends EnrFisConv4RegionGen
{
    public EnrFisConv4Region() {}
    public EnrFisConv4Region(AddressItem addressItem, EnrFisCatalogItem value) {
        setAddressItem(addressItem);
        setValue(value);
    }
}