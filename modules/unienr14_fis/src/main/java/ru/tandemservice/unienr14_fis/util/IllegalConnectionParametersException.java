/**
 *$Id: NotSpecifiedFisConnectionUrlException.java 26461 2013-03-11 12:21:59Z ashaburov $
 */
package ru.tandemservice.unienr14_fis.util;

/**
 * Исключение сигнализирует о том, что реквизиты сервера ФИС заданы некорректно.
 *
 * @author Alexander Shaburov
 * @since 11.03.13
 */
@SuppressWarnings("serial")
public class IllegalConnectionParametersException extends IllegalStateException
{
    public IllegalConnectionParametersException() {
    }

    public IllegalConnectionParametersException(String s) {
        super(s);
    }
}
