package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu;


import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;


/**
 * @author vdanilov
 */
public interface IEnrFisConverterDao4ProgramSet extends IEnrFisConverterDao<EnrProgramSetBase> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAutoSync();

    /** @return уровень (fisCatalogCode=2) образования для набора ОП */
    @Wiki(url="todo")
    EnrFisCatalogItem getEnrFisItem4EducationLevel(EnrProgramSetBase programSet);
}
