package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p1;

import enr14fis.schema.pkg.send.req.PackageData;
import enr14fis.schema.pkg.send.req.Root;
import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCampaignType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4CampaignInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.PkgDaoBase;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

import java.util.*;

/**
 * @author vdanilov
 */
public class PkgDao4CampaignInfo extends PkgDaoBase implements IPkgDao4CampaignInfo
{

    @Override
    public EnrFisSyncPackage4CampaignInfo doCreatePackage(final EnrFisSyncSession fisSession)
    {
        if (fisSession.isSkipEnrCampaignInfo()) { return null; }

        final EnrFisSyncPackage4CampaignInfo dbo = new EnrFisSyncPackage4CampaignInfo();
        dbo.setCreationDate(new Date());
        dbo.setSession(fisSession);
        dbo.setXmlPackage(EnrFisServiceImpl.toXml(this.buildRoot(dbo)));
        this.saveOrUpdate(dbo);
        flush(fisSession);
        return dbo;
    }

    protected Root buildRoot(final EnrFisSyncPackage4CampaignInfo dbo)
    {
        final String PREFIX = EnrFisSyncSessionManager.instance().getProperty("title.campaign");
        final EnrFisSyncSession fisSession = dbo.getSession();

        // состояние
        status(fisSession, PREFIX, 0);

        final EnrOrgUnit enrOrgUnit = fisSession.getEnrOrgUnit();
        final EnrEnrollmentCampaign enrollmentCampaign = enrOrgUnit.getEnrollmentCampaign();

        final IEnrFisConverter<EduProgramForm> educationFormConverter = EnrFisConverterManager.instance().educationForm().getConverter();
        final IEnrFisConverterDao4ProgramSet eduOrgUnitDao = EnrFisConverterManager.instance().programSet();
        final IEnrFisConverter<EnrEnrollmentCampaignType> enrollmentCampaignTypeConverter = EnrFisConverterManager.instance().enrollmentCampaignType().getConverter();

        // campaign
        final PackageData.CampaignInfo.Campaigns.Campaign campaign = new PackageData.CampaignInfo.Campaigns.Campaign();
        campaign.setUID(uid(enrollmentCampaign));
        campaign.setName(enrollmentCampaign.getTitle());
        campaign.setStatusID(Long.valueOf(fisSession.getCampaignInfoStatus().getFisItemCode()));

        // periods
        campaign.setYearStart(CoreDateUtils.get(enrollmentCampaign.getDateFrom(), Calendar.YEAR));
        campaign.setYearEnd(CoreDateUtils.get(enrollmentCampaign.getDateTo(), Calendar.YEAR));

        // enrollment directions
        final Collection<EnrCompetition> competitions = EnrFisSettingsManager.instance().dao().getCompetitions(enrOrgUnit);
        if (competitions.isEmpty()) {
            fisSession.error(
                PREFIX,
                EnrFisSyncSessionManager.instance().getProperty("error.campaign.no-directions")
            );
        }

        // education forms (develop forms)
        {
            final Set<Long> educationFromIdSet = new HashSet<Long>();
            for (final EnrCompetition competition: competitions)
            {
                EduProgramForm programForm = competition.getProgramSetOrgUnit().getProgramSet().getProgramForm();
                final EnrFisCatalogItem fisEduForm = educationFormConverter.getCatalogItem(programForm, false);
                if (null == fisEduForm) {
                    fisSession.error(
                        PREFIX,
                        EnrFisSyncSessionManager.instance().getProperty("error.campaign.missing-develop-form", programForm.getTitle())
                    );
                    continue;
                }
                educationFromIdSet.add(fisEduForm.getFisID());
            }
            campaign.setEducationForms(new PackageData.CampaignInfo.Campaigns.Campaign.EducationForms());
            campaign.getEducationForms().getEducationFormID().addAll(educationFromIdSet);
        }

        // education levels
        {
            final Set<Long> lvlIdSet = new HashSet<Long>();
            for (final EnrCompetition competition: competitions)
            {
                final EnrFisCatalogItem fisEduLvl = eduOrgUnitDao.getEnrFisItem4EducationLevel(competition.getProgramSetOrgUnit().getProgramSet());
                if (null == fisEduLvl) {
                    fisSession.error(
                        PREFIX,
                        EnrFisSyncSessionManager.instance().getProperty("error.campaign.missing-education-level", competition.getTitle())
                    );
                    continue;
                }
                lvlIdSet.add(fisEduLvl.getFisID());
            }

            campaign.setEducationLevels(new PackageData.CampaignInfo.Campaigns.Campaign.EducationLevels());
            campaign.getEducationLevels().getEducationLevelID().addAll(lvlIdSet);
        }

        // campaign type
        {
            final EnrEnrollmentCampaignType campaignType = enrollmentCampaign.getSettings().getEnrollmentCampaignType();
            if (null == campaignType)
                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.campaign.missing-campaign-type", enrollmentCampaign.getTitle()));
            else
            {
                final EnrFisCatalogItem fisCampaignType = enrollmentCampaignTypeConverter.getCatalogItem(campaignType, false);
                if (null == fisCampaignType)
                    fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.campaign.no-fis-campaign-type", campaignType.getTitle()));
                else
                    campaign.setCampaignTypeID(fisCampaignType.getFisID());
            }
        }

        // состояние
        status(fisSession, PREFIX, 100);

        // заполняем root
        final Root root = this.newRoot(enrOrgUnit);
        root.getPackageData().setCampaignInfo(new PackageData.CampaignInfo());
        root.getPackageData().getCampaignInfo().setCampaigns(new PackageData.CampaignInfo.Campaigns());
        root.getPackageData().getCampaignInfo().getCampaigns().getCampaign().add(campaign);
        return root;
    }
}
