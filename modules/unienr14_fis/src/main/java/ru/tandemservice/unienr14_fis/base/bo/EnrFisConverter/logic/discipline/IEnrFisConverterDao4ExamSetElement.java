/**
 *$Id: IEnrFisConverterDao4ExamSetElement.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.discipline;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Alexander Shaburov
 * @since 18.07.13
 */
public interface IEnrFisConverterDao4ExamSetElement extends IEnrFisConverterDao<IEnrExamSetElementValue>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync(EnrEnrollmentCampaign ec);
}
