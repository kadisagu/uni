package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4AOrdersInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет 4: Приказы по абитуриентам
 *
 * Пакет 4: Приказы по абитуриентам
 * Root / PackageData / OrdersOfAdmission
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSyncPackage4AOrdersInfoGen extends EnrFisSyncPackage
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4AOrdersInfo";
    public static final String ENTITY_NAME = "enrFisSyncPackage4AOrdersInfo";
    public static final int VERSION_HASH = -75856425;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrFisSyncPackage4AOrdersInfoGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSyncPackage4AOrdersInfoGen> extends EnrFisSyncPackage.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSyncPackage4AOrdersInfo.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSyncPackage4AOrdersInfo();
        }
    }
    private static final Path<EnrFisSyncPackage4AOrdersInfo> _dslPath = new Path<EnrFisSyncPackage4AOrdersInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSyncPackage4AOrdersInfo");
    }
            

    public static class Path<E extends EnrFisSyncPackage4AOrdersInfo> extends EnrFisSyncPackage.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EnrFisSyncPackage4AOrdersInfo.class;
        }

        public String getEntityName()
        {
            return "enrFisSyncPackage4AOrdersInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
