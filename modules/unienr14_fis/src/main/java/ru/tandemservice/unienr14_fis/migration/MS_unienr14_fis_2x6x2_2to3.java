package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSettingsStateExamMinMark

		// создано обязательное свойство enrollmentCampaign
        if(tool.tableExists("enr14fis_s_st_exam_min_mark_t"))
		{
            tool.executeUpdate("delete from enr14fis_s_st_exam_min_mark_t");

            if(!tool.columnExists("enr14fis_s_st_exam_min_mark_t", "enrollmentcampaign_id"))
            {
                // создать колонку
                tool.createColumn("enr14fis_s_st_exam_min_mark_t", new DBColumn("enrollmentcampaign_id", DBType.LONG));

                // сделать колонку NOT NULL
                tool.setColumnNullable("enr14fis_s_st_exam_min_mark_t", "enrollmentcampaign_id", false);
            }
		}
    }
}