package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

/**
 * @author vdanilov
 */
public interface IEnrFisSyncSessionFillStatus {

    /** @return progress [0..100] */
    int getProgress();

    /** @return название этапа */
    String getStageName();

}
