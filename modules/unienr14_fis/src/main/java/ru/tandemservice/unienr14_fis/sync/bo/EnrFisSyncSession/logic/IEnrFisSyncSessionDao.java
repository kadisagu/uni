package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.function.Function;

/**
 * @author vdanilov
 */
public interface IEnrFisSyncSessionDao extends INeedPersistenceSupport {

    /**
     * Сохраняет сессию
     * @param fisSession сессия ФИС
     */
    void doSave(EnrFisSyncSession fisSession);

    /**
     * Заполняет пакетами сессию
     * @param fisSession сессия ФИС
     */
    void doFill(EnrFisSyncSession fisSession);

    /**
     * Оправляет сессию в очередь на передачу в ФИС
     * @param fisSession сессия ФИС
     */
    void doQueue(EnrFisSyncSession fisSession);

    void doArchive(EnrFisSyncSession fisSession);

    void doDelete(EnrFisSyncSession fisSession);

    /**
     * Функция преобразования таймстампа в xml-формат.
     * Для ПК до 2016 года формат с миллисекундами и часовым поясом.
     * Для ПК с 2016 года формат без миллисекунд и часового пояса.
     *
     * @param campaign приемная кампания. Если не задана, то считается, что год >= 2016.
     * @param dataTypeFactory dataTypeFactory
     * @return фнкция
     */
    @NotNull Function<Date, XMLGregorianCalendar> getDateTimeXmlConverter(@Nullable EnrEnrollmentCampaign campaign, @NotNull DatatypeFactory dataTypeFactory);
}
