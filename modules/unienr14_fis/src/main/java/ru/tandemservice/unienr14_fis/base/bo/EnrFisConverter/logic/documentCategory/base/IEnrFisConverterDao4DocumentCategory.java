package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public interface IEnrFisConverterDao4DocumentCategory extends IEnrFisConverterDao<PersonDocumentCategory>
{
    void doAutoSync();
}
