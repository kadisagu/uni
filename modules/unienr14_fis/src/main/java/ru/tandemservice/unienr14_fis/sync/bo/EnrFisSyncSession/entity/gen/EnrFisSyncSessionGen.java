package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сессия синхронизации ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSyncSessionGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession";
    public static final String ENTITY_NAME = "enrFisSyncSession";
    public static final int VERSION_HASH = -2129873248;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_REQUEST_STATE = "requestState";
    public static final String P_FILL_DATE = "fillDate";
    public static final String P_QUEUE_DATE = "queueDate";
    public static final String P_ARCHIVE_DATE = "archiveDate";
    public static final String P_ZIP_LOG = "zipLog";
    public static final String L_CAMPAIGN_INFO_STATUS = "campaignInfoStatus";
    public static final String P_SKIP_ADMISSION_INFO = "skipAdmissionInfo";
    public static final String P_EDU_PROGRAM_UNIQUE_TITLE_FORMING = "eduProgramUniqueTitleForming";
    public static final String P_FORCE_ALL_ENTRANTS = "forceAllEntrants";
    public static final String P_ACCEPTED_INFO = "acceptedInfo";
    public static final String P_SEND_REQUEST_IN_ORDER_INFO = "sendRequestInOrderInfo";
    public static final String P_SKIP_ENR_CAMPAIGN_INFO = "skipEnrCampaignInfo";
    public static final String P_SKIP_APPLICATIONS_INFO = "skipApplicationsInfo";
    public static final String P_SKIP_ORDERS_OF_ADMISSION = "skipOrdersOfAdmission";
    public static final String P_SKIP_ILLEGAL_DIRECTIONS = "skipIllegalDirections";
    public static final String P_SKIP_ERROR_ENTRANTS = "skipErrorEntrants";
    public static final String P_SKIPPED_ENTRANT_COUNT = "skippedEntrantCount";
    public static final String P_SKIPPED_EXTRACT_COUNT = "skippedExtractCount";
    public static final String P_SKIPPED_REC_ITEM_COUNT = "skippedRecItemCount";
    public static final String P_ALIVE = "alive";
    public static final String P_ARCHIVE_DATE_AS_STRING = "archiveDateAsString";
    public static final String P_ARCHIVE_DISABLED = "archiveDisabled";
    public static final String P_ARCHIVED = "archived";
    public static final String P_CONTENT_STATUS = "contentStatus";
    public static final String P_CREATION_DATE_AS_STRING = "creationDateAsString";
    public static final String P_DATE_LOG = "dateLog";
    public static final String P_DELETE_DISABLED = "deleteDisabled";
    public static final String P_QUEUE_DISABLED = "queueDisabled";
    public static final String P_QUEUED = "queued";
    public static final String P_STATE_TITLE = "stateTitle";
    public static final String P_TITLE = "title";

    private EnrOrgUnit _enrOrgUnit;     // Подразделение, ведущее прием
    private Date _creationDate;     // Дата создания сессии
    private int _requestState = 4;     // Состояние неотозванных заявлений
    private Date _fillDate;     // Дата формирования сессии
    private Date _queueDate;     // Дата постановки сессии в очередь
    private Date _archiveDate;     // Дата помещения сессии в архив
    private byte[] _zipLog;     // ZIP: лог
    private EnrFisCatalogItem _campaignInfoStatus;     // Статус ПК ФИС
    private boolean _skipAdmissionInfo = false;     // Не передавать сведения об объеме и структуре приема
    private boolean _eduProgramUniqueTitleForming = false;     // Сформировать уникальные названия образовательных программ
    private boolean _forceAllEntrants = false;     // Выгружать заявления всех абитуриентов
    private boolean _acceptedInfo = true;     // Передавать данные о согласиях на зачисление
    private boolean _sendRequestInOrderInfo = true;     // Передавать данные о заявлениях в приказах
    private boolean _skipEnrCampaignInfo = false;     // Не формировать пакет с данными приемной кампании
    private boolean _skipApplicationsInfo = false;     // Не формировать пакеты с заявлениями абитуриентов
    private boolean _skipOrdersOfAdmission = false;     // Не формировать пакет с приказами
    private boolean _skipIllegalDirections = false;     // Пропускать направления, не имеющие аналогов в ФИС
    private boolean _skipErrorEntrants = false;     // Пропускать абитуриентов с ошибками
    private int _skippedEntrantCount;     // Число пропущенных при формировании 3 пакета абитуриентов
    private int _skippedExtractCount;     // Число пропущенных при формировании 4 пакета выписок
    private int _skippedRecItemCount;     // Число пропущенных при формировании 4 пакета рекомендованных

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Подразделение (в рамках ПК), в рамках которого осуществляется формирование и передача пакетов ФИС
     *
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     */
    @NotNull
    public EnrOrgUnit getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Подразделение, ведущее прием. Свойство не может быть null.
     */
    public void setEnrOrgUnit(EnrOrgUnit enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * Дата, когда сессия была создана в системе
     *
     * @return Дата создания сессии. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания сессии. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * Состояние неотозванных заявлений
     *
     * @return Состояние неотозванных заявлений. Свойство не может быть null.
     */
    @NotNull
    public int getRequestState()
    {
        return _requestState;
    }

    /**
     * @param requestState Состояние неотозванных заявлений. Свойство не может быть null.
     */
    public void setRequestState(int requestState)
    {
        dirty(_requestState, requestState);
        _requestState = requestState;
    }

    /**
     * Дата, когда сессия была заполнена пакетами
     *
     * @return Дата формирования сессии.
     */
    public Date getFillDate()
    {
        return _fillDate;
    }

    /**
     * @param fillDate Дата формирования сессии.
     */
    public void setFillDate(Date fillDate)
    {
        dirty(_fillDate, fillDate);
        _fillDate = fillDate;
    }

    /**
     * Показывает, когда сессия была добавлена в очередь
     *
     * @return Дата постановки сессии в очередь.
     */
    public Date getQueueDate()
    {
        return _queueDate;
    }

    /**
     * @param queueDate Дата постановки сессии в очередь.
     */
    public void setQueueDate(Date queueDate)
    {
        dirty(_queueDate, queueDate);
        _queueDate = queueDate;
    }

    /**
     * Показывает, когда сессия была перемещен в архив (остановлены все операции)
     *
     * @return Дата помещения сессии в архив.
     */
    public Date getArchiveDate()
    {
        return _archiveDate;
    }

    /**
     * @param archiveDate Дата помещения сессии в архив.
     */
    public void setArchiveDate(Date archiveDate)
    {
        dirty(_archiveDate, archiveDate);
        _archiveDate = archiveDate;
    }

    /**
     * Текстовый лог, содержащий всю информацию о формировании сессии и обработке ее пакетов
     *
     * @return ZIP: лог.
     */
    public byte[] getZipLog()
    {
        initLazyForGet("zipLog");
        return _zipLog;
    }

    /**
     * @param zipLog ZIP: лог.
     */
    public void setZipLog(byte[] zipLog)
    {
        initLazyForSet("zipLog");
        dirty(_zipLog, zipLog);
        _zipLog = zipLog;
    }

    /**
     * Статус ПК в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус ПК ФИС. Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getCampaignInfoStatus()
    {
        return _campaignInfoStatus;
    }

    /**
     * @param campaignInfoStatus Статус ПК ФИС. Свойство не может быть null.
     */
    public void setCampaignInfoStatus(EnrFisCatalogItem campaignInfoStatus)
    {
        dirty(_campaignInfoStatus, campaignInfoStatus);
        _campaignInfoStatus = campaignInfoStatus;
    }

    /**
     * Пропустить генерацию и передачу 2ого пакета (например, при ежедневной выгрузке).
     *
     * @return Не передавать сведения об объеме и структуре приема. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipAdmissionInfo()
    {
        return _skipAdmissionInfo;
    }

    /**
     * @param skipAdmissionInfo Не передавать сведения об объеме и структуре приема. Свойство не может быть null.
     */
    public void setSkipAdmissionInfo(boolean skipAdmissionInfo)
    {
        dirty(_skipAdmissionInfo, skipAdmissionInfo);
        _skipAdmissionInfo = skipAdmissionInfo;
    }

    /**
     * Формирует уникальные названия для образовательных программ.
     *
     * @return Сформировать уникальные названия образовательных программ. Свойство не может быть null.
     */
    @NotNull
    public boolean isEduProgramUniqueTitleForming()
    {
        return _eduProgramUniqueTitleForming;
    }

    /**
     * @param eduProgramUniqueTitleForming Сформировать уникальные названия образовательных программ. Свойство не может быть null.
     */
    public void setEduProgramUniqueTitleForming(boolean eduProgramUniqueTitleForming)
    {
        dirty(_eduProgramUniqueTitleForming, eduProgramUniqueTitleForming);
        _eduProgramUniqueTitleForming = eduProgramUniqueTitleForming;
    }

    /**
     * Выгружать заявления всех абитуриентов (например, когда требуется полностью обновить или восстановить состояние в ФИс по данным системы).
     *
     * @return Выгружать заявления всех абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isForceAllEntrants()
    {
        return _forceAllEntrants;
    }

    /**
     * @param forceAllEntrants Выгружать заявления всех абитуриентов. Свойство не может быть null.
     */
    public void setForceAllEntrants(boolean forceAllEntrants)
    {
        dirty(_forceAllEntrants, forceAllEntrants);
        _forceAllEntrants = forceAllEntrants;
    }

    /**
     * Передавать даты согласий на зачисление.
     *
     * @return Передавать данные о согласиях на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcceptedInfo()
    {
        return _acceptedInfo;
    }

    /**
     * @param acceptedInfo Передавать данные о согласиях на зачисление. Свойство не может быть null.
     */
    public void setAcceptedInfo(boolean acceptedInfo)
    {
        dirty(_acceptedInfo, acceptedInfo);
        _acceptedInfo = acceptedInfo;
    }

    /**
     * Если выключена, то пропускать заявления ФИС, в которых есть хотя бы один выбранный конкурс в состоянии «Зачислен».
     *
     * @return Передавать данные о заявлениях в приказах. Свойство не может быть null.
     */
    @NotNull
    public boolean isSendRequestInOrderInfo()
    {
        return _sendRequestInOrderInfo;
    }

    /**
     * @param sendRequestInOrderInfo Передавать данные о заявлениях в приказах. Свойство не может быть null.
     */
    public void setSendRequestInOrderInfo(boolean sendRequestInOrderInfo)
    {
        dirty(_sendRequestInOrderInfo, sendRequestInOrderInfo);
        _sendRequestInOrderInfo = sendRequestInOrderInfo;
    }

    /**
     * Не формировать пакет с данными приемной кампании.
     *
     * @return Не формировать пакет с данными приемной кампании. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipEnrCampaignInfo()
    {
        return _skipEnrCampaignInfo;
    }

    /**
     * @param skipEnrCampaignInfo Не формировать пакет с данными приемной кампании. Свойство не может быть null.
     */
    public void setSkipEnrCampaignInfo(boolean skipEnrCampaignInfo)
    {
        dirty(_skipEnrCampaignInfo, skipEnrCampaignInfo);
        _skipEnrCampaignInfo = skipEnrCampaignInfo;
    }

    /**
     * Не формировать пакеты с заявлениями вообще (для обновления данных о структуре приема).
     *
     * @return Не формировать пакеты с заявлениями абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipApplicationsInfo()
    {
        return _skipApplicationsInfo;
    }

    /**
     * @param skipApplicationsInfo Не формировать пакеты с заявлениями абитуриентов. Свойство не может быть null.
     */
    public void setSkipApplicationsInfo(boolean skipApplicationsInfo)
    {
        dirty(_skipApplicationsInfo, skipApplicationsInfo);
        _skipApplicationsInfo = skipApplicationsInfo;
    }

    /**
     * Не формировать пакет с приказами.
     *
     * @return Не формировать пакет с приказами. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipOrdersOfAdmission()
    {
        return _skipOrdersOfAdmission;
    }

    /**
     * @param skipOrdersOfAdmission Не формировать пакет с приказами. Свойство не может быть null.
     */
    public void setSkipOrdersOfAdmission(boolean skipOrdersOfAdmission)
    {
        dirty(_skipOrdersOfAdmission, skipOrdersOfAdmission);
        _skipOrdersOfAdmission = skipOrdersOfAdmission;
    }

    /**
     * Не включать в пакеты планы приема и ВНП абитуриентв по направлениям, которые не поддерживаются ФИС.
     * см. ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.IEnrFisSettingsDao#getEnrDirectionSet (например, экстернат).
     *
     * @return Пропускать направления, не имеющие аналогов в ФИС. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipIllegalDirections()
    {
        return _skipIllegalDirections;
    }

    /**
     * @param skipIllegalDirections Пропускать направления, не имеющие аналогов в ФИС. Свойство не может быть null.
     */
    public void setSkipIllegalDirections(boolean skipIllegalDirections)
    {
        dirty(_skipIllegalDirections, skipIllegalDirections);
        _skipIllegalDirections = skipIllegalDirections;
    }

    /**
     * Не включать в пакеты абитуриентв, в заявлениях которых возникли обишки при генерации.
     *
     * @return Пропускать абитуриентов с ошибками. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipErrorEntrants()
    {
        return _skipErrorEntrants;
    }

    /**
     * @param skipErrorEntrants Пропускать абитуриентов с ошибками. Свойство не может быть null.
     */
    public void setSkipErrorEntrants(boolean skipErrorEntrants)
    {
        dirty(_skipErrorEntrants, skipErrorEntrants);
        _skipErrorEntrants = skipErrorEntrants;
    }

    /**
     * @return Число пропущенных при формировании 3 пакета абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public int getSkippedEntrantCount()
    {
        return _skippedEntrantCount;
    }

    /**
     * @param skippedEntrantCount Число пропущенных при формировании 3 пакета абитуриентов. Свойство не может быть null.
     */
    public void setSkippedEntrantCount(int skippedEntrantCount)
    {
        dirty(_skippedEntrantCount, skippedEntrantCount);
        _skippedEntrantCount = skippedEntrantCount;
    }

    /**
     * @return Число пропущенных при формировании 4 пакета выписок. Свойство не может быть null.
     */
    @NotNull
    public int getSkippedExtractCount()
    {
        return _skippedExtractCount;
    }

    /**
     * @param skippedExtractCount Число пропущенных при формировании 4 пакета выписок. Свойство не может быть null.
     */
    public void setSkippedExtractCount(int skippedExtractCount)
    {
        dirty(_skippedExtractCount, skippedExtractCount);
        _skippedExtractCount = skippedExtractCount;
    }

    /**
     * @return Число пропущенных при формировании 4 пакета рекомендованных. Свойство не может быть null.
     */
    @NotNull
    public int getSkippedRecItemCount()
    {
        return _skippedRecItemCount;
    }

    /**
     * @param skippedRecItemCount Число пропущенных при формировании 4 пакета рекомендованных. Свойство не может быть null.
     */
    public void setSkippedRecItemCount(int skippedRecItemCount)
    {
        dirty(_skippedRecItemCount, skippedRecItemCount);
        _skippedRecItemCount = skippedRecItemCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisSyncSessionGen)
        {
            setEnrOrgUnit(((EnrFisSyncSession)another).getEnrOrgUnit());
            setCreationDate(((EnrFisSyncSession)another).getCreationDate());
            setRequestState(((EnrFisSyncSession)another).getRequestState());
            setFillDate(((EnrFisSyncSession)another).getFillDate());
            setQueueDate(((EnrFisSyncSession)another).getQueueDate());
            setArchiveDate(((EnrFisSyncSession)another).getArchiveDate());
            setZipLog(((EnrFisSyncSession)another).getZipLog());
            setCampaignInfoStatus(((EnrFisSyncSession)another).getCampaignInfoStatus());
            setSkipAdmissionInfo(((EnrFisSyncSession)another).isSkipAdmissionInfo());
            setEduProgramUniqueTitleForming(((EnrFisSyncSession)another).isEduProgramUniqueTitleForming());
            setForceAllEntrants(((EnrFisSyncSession)another).isForceAllEntrants());
            setAcceptedInfo(((EnrFisSyncSession)another).isAcceptedInfo());
            setSendRequestInOrderInfo(((EnrFisSyncSession)another).isSendRequestInOrderInfo());
            setSkipEnrCampaignInfo(((EnrFisSyncSession)another).isSkipEnrCampaignInfo());
            setSkipApplicationsInfo(((EnrFisSyncSession)another).isSkipApplicationsInfo());
            setSkipOrdersOfAdmission(((EnrFisSyncSession)another).isSkipOrdersOfAdmission());
            setSkipIllegalDirections(((EnrFisSyncSession)another).isSkipIllegalDirections());
            setSkipErrorEntrants(((EnrFisSyncSession)another).isSkipErrorEntrants());
            setSkippedEntrantCount(((EnrFisSyncSession)another).getSkippedEntrantCount());
            setSkippedExtractCount(((EnrFisSyncSession)another).getSkippedExtractCount());
            setSkippedRecItemCount(((EnrFisSyncSession)another).getSkippedRecItemCount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSyncSessionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSyncSession.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSyncSession();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "creationDate":
                    return obj.getCreationDate();
                case "requestState":
                    return obj.getRequestState();
                case "fillDate":
                    return obj.getFillDate();
                case "queueDate":
                    return obj.getQueueDate();
                case "archiveDate":
                    return obj.getArchiveDate();
                case "zipLog":
                    return obj.getZipLog();
                case "campaignInfoStatus":
                    return obj.getCampaignInfoStatus();
                case "skipAdmissionInfo":
                    return obj.isSkipAdmissionInfo();
                case "eduProgramUniqueTitleForming":
                    return obj.isEduProgramUniqueTitleForming();
                case "forceAllEntrants":
                    return obj.isForceAllEntrants();
                case "acceptedInfo":
                    return obj.isAcceptedInfo();
                case "sendRequestInOrderInfo":
                    return obj.isSendRequestInOrderInfo();
                case "skipEnrCampaignInfo":
                    return obj.isSkipEnrCampaignInfo();
                case "skipApplicationsInfo":
                    return obj.isSkipApplicationsInfo();
                case "skipOrdersOfAdmission":
                    return obj.isSkipOrdersOfAdmission();
                case "skipIllegalDirections":
                    return obj.isSkipIllegalDirections();
                case "skipErrorEntrants":
                    return obj.isSkipErrorEntrants();
                case "skippedEntrantCount":
                    return obj.getSkippedEntrantCount();
                case "skippedExtractCount":
                    return obj.getSkippedExtractCount();
                case "skippedRecItemCount":
                    return obj.getSkippedRecItemCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((EnrOrgUnit) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "requestState":
                    obj.setRequestState((Integer) value);
                    return;
                case "fillDate":
                    obj.setFillDate((Date) value);
                    return;
                case "queueDate":
                    obj.setQueueDate((Date) value);
                    return;
                case "archiveDate":
                    obj.setArchiveDate((Date) value);
                    return;
                case "zipLog":
                    obj.setZipLog((byte[]) value);
                    return;
                case "campaignInfoStatus":
                    obj.setCampaignInfoStatus((EnrFisCatalogItem) value);
                    return;
                case "skipAdmissionInfo":
                    obj.setSkipAdmissionInfo((Boolean) value);
                    return;
                case "eduProgramUniqueTitleForming":
                    obj.setEduProgramUniqueTitleForming((Boolean) value);
                    return;
                case "forceAllEntrants":
                    obj.setForceAllEntrants((Boolean) value);
                    return;
                case "acceptedInfo":
                    obj.setAcceptedInfo((Boolean) value);
                    return;
                case "sendRequestInOrderInfo":
                    obj.setSendRequestInOrderInfo((Boolean) value);
                    return;
                case "skipEnrCampaignInfo":
                    obj.setSkipEnrCampaignInfo((Boolean) value);
                    return;
                case "skipApplicationsInfo":
                    obj.setSkipApplicationsInfo((Boolean) value);
                    return;
                case "skipOrdersOfAdmission":
                    obj.setSkipOrdersOfAdmission((Boolean) value);
                    return;
                case "skipIllegalDirections":
                    obj.setSkipIllegalDirections((Boolean) value);
                    return;
                case "skipErrorEntrants":
                    obj.setSkipErrorEntrants((Boolean) value);
                    return;
                case "skippedEntrantCount":
                    obj.setSkippedEntrantCount((Integer) value);
                    return;
                case "skippedExtractCount":
                    obj.setSkippedExtractCount((Integer) value);
                    return;
                case "skippedRecItemCount":
                    obj.setSkippedRecItemCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "creationDate":
                        return true;
                case "requestState":
                        return true;
                case "fillDate":
                        return true;
                case "queueDate":
                        return true;
                case "archiveDate":
                        return true;
                case "zipLog":
                        return true;
                case "campaignInfoStatus":
                        return true;
                case "skipAdmissionInfo":
                        return true;
                case "eduProgramUniqueTitleForming":
                        return true;
                case "forceAllEntrants":
                        return true;
                case "acceptedInfo":
                        return true;
                case "sendRequestInOrderInfo":
                        return true;
                case "skipEnrCampaignInfo":
                        return true;
                case "skipApplicationsInfo":
                        return true;
                case "skipOrdersOfAdmission":
                        return true;
                case "skipIllegalDirections":
                        return true;
                case "skipErrorEntrants":
                        return true;
                case "skippedEntrantCount":
                        return true;
                case "skippedExtractCount":
                        return true;
                case "skippedRecItemCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "creationDate":
                    return true;
                case "requestState":
                    return true;
                case "fillDate":
                    return true;
                case "queueDate":
                    return true;
                case "archiveDate":
                    return true;
                case "zipLog":
                    return true;
                case "campaignInfoStatus":
                    return true;
                case "skipAdmissionInfo":
                    return true;
                case "eduProgramUniqueTitleForming":
                    return true;
                case "forceAllEntrants":
                    return true;
                case "acceptedInfo":
                    return true;
                case "sendRequestInOrderInfo":
                    return true;
                case "skipEnrCampaignInfo":
                    return true;
                case "skipApplicationsInfo":
                    return true;
                case "skipOrdersOfAdmission":
                    return true;
                case "skipIllegalDirections":
                    return true;
                case "skipErrorEntrants":
                    return true;
                case "skippedEntrantCount":
                    return true;
                case "skippedExtractCount":
                    return true;
                case "skippedRecItemCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrOrgUnit":
                    return EnrOrgUnit.class;
                case "creationDate":
                    return Date.class;
                case "requestState":
                    return Integer.class;
                case "fillDate":
                    return Date.class;
                case "queueDate":
                    return Date.class;
                case "archiveDate":
                    return Date.class;
                case "zipLog":
                    return byte[].class;
                case "campaignInfoStatus":
                    return EnrFisCatalogItem.class;
                case "skipAdmissionInfo":
                    return Boolean.class;
                case "eduProgramUniqueTitleForming":
                    return Boolean.class;
                case "forceAllEntrants":
                    return Boolean.class;
                case "acceptedInfo":
                    return Boolean.class;
                case "sendRequestInOrderInfo":
                    return Boolean.class;
                case "skipEnrCampaignInfo":
                    return Boolean.class;
                case "skipApplicationsInfo":
                    return Boolean.class;
                case "skipOrdersOfAdmission":
                    return Boolean.class;
                case "skipIllegalDirections":
                    return Boolean.class;
                case "skipErrorEntrants":
                    return Boolean.class;
                case "skippedEntrantCount":
                    return Integer.class;
                case "skippedExtractCount":
                    return Integer.class;
                case "skippedRecItemCount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSyncSession> _dslPath = new Path<EnrFisSyncSession>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSyncSession");
    }
            

    /**
     * Подразделение (в рамках ПК), в рамках которого осуществляется формирование и передача пакетов ФИС
     *
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getEnrOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * Дата, когда сессия была создана в системе
     *
     * @return Дата создания сессии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * Состояние неотозванных заявлений
     *
     * @return Состояние неотозванных заявлений. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getRequestState()
     */
    public static PropertyPath<Integer> requestState()
    {
        return _dslPath.requestState();
    }

    /**
     * Дата, когда сессия была заполнена пакетами
     *
     * @return Дата формирования сессии.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getFillDate()
     */
    public static PropertyPath<Date> fillDate()
    {
        return _dslPath.fillDate();
    }

    /**
     * Показывает, когда сессия была добавлена в очередь
     *
     * @return Дата постановки сессии в очередь.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getQueueDate()
     */
    public static PropertyPath<Date> queueDate()
    {
        return _dslPath.queueDate();
    }

    /**
     * Показывает, когда сессия была перемещен в архив (остановлены все операции)
     *
     * @return Дата помещения сессии в архив.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getArchiveDate()
     */
    public static PropertyPath<Date> archiveDate()
    {
        return _dslPath.archiveDate();
    }

    /**
     * Текстовый лог, содержащий всю информацию о формировании сессии и обработке ее пакетов
     *
     * @return ZIP: лог.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getZipLog()
     */
    public static PropertyPath<byte[]> zipLog()
    {
        return _dslPath.zipLog();
    }

    /**
     * Статус ПК в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус ПК ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getCampaignInfoStatus()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> campaignInfoStatus()
    {
        return _dslPath.campaignInfoStatus();
    }

    /**
     * Пропустить генерацию и передачу 2ого пакета (например, при ежедневной выгрузке).
     *
     * @return Не передавать сведения об объеме и структуре приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipAdmissionInfo()
     */
    public static PropertyPath<Boolean> skipAdmissionInfo()
    {
        return _dslPath.skipAdmissionInfo();
    }

    /**
     * Формирует уникальные названия для образовательных программ.
     *
     * @return Сформировать уникальные названия образовательных программ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isEduProgramUniqueTitleForming()
     */
    public static PropertyPath<Boolean> eduProgramUniqueTitleForming()
    {
        return _dslPath.eduProgramUniqueTitleForming();
    }

    /**
     * Выгружать заявления всех абитуриентов (например, когда требуется полностью обновить или восстановить состояние в ФИс по данным системы).
     *
     * @return Выгружать заявления всех абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isForceAllEntrants()
     */
    public static PropertyPath<Boolean> forceAllEntrants()
    {
        return _dslPath.forceAllEntrants();
    }

    /**
     * Передавать даты согласий на зачисление.
     *
     * @return Передавать данные о согласиях на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isAcceptedInfo()
     */
    public static PropertyPath<Boolean> acceptedInfo()
    {
        return _dslPath.acceptedInfo();
    }

    /**
     * Если выключена, то пропускать заявления ФИС, в которых есть хотя бы один выбранный конкурс в состоянии «Зачислен».
     *
     * @return Передавать данные о заявлениях в приказах. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSendRequestInOrderInfo()
     */
    public static PropertyPath<Boolean> sendRequestInOrderInfo()
    {
        return _dslPath.sendRequestInOrderInfo();
    }

    /**
     * Не формировать пакет с данными приемной кампании.
     *
     * @return Не формировать пакет с данными приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipEnrCampaignInfo()
     */
    public static PropertyPath<Boolean> skipEnrCampaignInfo()
    {
        return _dslPath.skipEnrCampaignInfo();
    }

    /**
     * Не формировать пакеты с заявлениями вообще (для обновления данных о структуре приема).
     *
     * @return Не формировать пакеты с заявлениями абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipApplicationsInfo()
     */
    public static PropertyPath<Boolean> skipApplicationsInfo()
    {
        return _dslPath.skipApplicationsInfo();
    }

    /**
     * Не формировать пакет с приказами.
     *
     * @return Не формировать пакет с приказами. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipOrdersOfAdmission()
     */
    public static PropertyPath<Boolean> skipOrdersOfAdmission()
    {
        return _dslPath.skipOrdersOfAdmission();
    }

    /**
     * Не включать в пакеты планы приема и ВНП абитуриентв по направлениям, которые не поддерживаются ФИС.
     * см. ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.IEnrFisSettingsDao#getEnrDirectionSet (например, экстернат).
     *
     * @return Пропускать направления, не имеющие аналогов в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipIllegalDirections()
     */
    public static PropertyPath<Boolean> skipIllegalDirections()
    {
        return _dslPath.skipIllegalDirections();
    }

    /**
     * Не включать в пакеты абитуриентв, в заявлениях которых возникли обишки при генерации.
     *
     * @return Пропускать абитуриентов с ошибками. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipErrorEntrants()
     */
    public static PropertyPath<Boolean> skipErrorEntrants()
    {
        return _dslPath.skipErrorEntrants();
    }

    /**
     * @return Число пропущенных при формировании 3 пакета абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getSkippedEntrantCount()
     */
    public static PropertyPath<Integer> skippedEntrantCount()
    {
        return _dslPath.skippedEntrantCount();
    }

    /**
     * @return Число пропущенных при формировании 4 пакета выписок. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getSkippedExtractCount()
     */
    public static PropertyPath<Integer> skippedExtractCount()
    {
        return _dslPath.skippedExtractCount();
    }

    /**
     * @return Число пропущенных при формировании 4 пакета рекомендованных. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getSkippedRecItemCount()
     */
    public static PropertyPath<Integer> skippedRecItemCount()
    {
        return _dslPath.skippedRecItemCount();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isAlive()
     */
    public static SupportedPropertyPath<Boolean> alive()
    {
        return _dslPath.alive();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getArchiveDateAsString()
     */
    public static SupportedPropertyPath<String> archiveDateAsString()
    {
        return _dslPath.archiveDateAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isArchiveDisabled()
     */
    public static SupportedPropertyPath<Boolean> archiveDisabled()
    {
        return _dslPath.archiveDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isArchived()
     */
    public static SupportedPropertyPath<Boolean> archived()
    {
        return _dslPath.archived();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getContentStatus()
     */
    public static SupportedPropertyPath<String> contentStatus()
    {
        return _dslPath.contentStatus();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getCreationDateAsString()
     */
    public static SupportedPropertyPath<String> creationDateAsString()
    {
        return _dslPath.creationDateAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getDateLog()
     */
    public static SupportedPropertyPath<String> dateLog()
    {
        return _dslPath.dateLog();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isDeleteDisabled()
     */
    public static SupportedPropertyPath<Boolean> deleteDisabled()
    {
        return _dslPath.deleteDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isQueueDisabled()
     */
    public static SupportedPropertyPath<Boolean> queueDisabled()
    {
        return _dslPath.queueDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isQueued()
     */
    public static SupportedPropertyPath<Boolean> queued()
    {
        return _dslPath.queued();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getStateTitle()
     */
    public static SupportedPropertyPath<String> stateTitle()
    {
        return _dslPath.stateTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrFisSyncSession> extends EntityPath<E>
    {
        private EnrOrgUnit.Path<EnrOrgUnit> _enrOrgUnit;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Integer> _requestState;
        private PropertyPath<Date> _fillDate;
        private PropertyPath<Date> _queueDate;
        private PropertyPath<Date> _archiveDate;
        private PropertyPath<byte[]> _zipLog;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _campaignInfoStatus;
        private PropertyPath<Boolean> _skipAdmissionInfo;
        private PropertyPath<Boolean> _eduProgramUniqueTitleForming;
        private PropertyPath<Boolean> _forceAllEntrants;
        private PropertyPath<Boolean> _acceptedInfo;
        private PropertyPath<Boolean> _sendRequestInOrderInfo;
        private PropertyPath<Boolean> _skipEnrCampaignInfo;
        private PropertyPath<Boolean> _skipApplicationsInfo;
        private PropertyPath<Boolean> _skipOrdersOfAdmission;
        private PropertyPath<Boolean> _skipIllegalDirections;
        private PropertyPath<Boolean> _skipErrorEntrants;
        private PropertyPath<Integer> _skippedEntrantCount;
        private PropertyPath<Integer> _skippedExtractCount;
        private PropertyPath<Integer> _skippedRecItemCount;
        private SupportedPropertyPath<Boolean> _alive;
        private SupportedPropertyPath<String> _archiveDateAsString;
        private SupportedPropertyPath<Boolean> _archiveDisabled;
        private SupportedPropertyPath<Boolean> _archived;
        private SupportedPropertyPath<String> _contentStatus;
        private SupportedPropertyPath<String> _creationDateAsString;
        private SupportedPropertyPath<String> _dateLog;
        private SupportedPropertyPath<Boolean> _deleteDisabled;
        private SupportedPropertyPath<Boolean> _queueDisabled;
        private SupportedPropertyPath<Boolean> _queued;
        private SupportedPropertyPath<String> _stateTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Подразделение (в рамках ПК), в рамках которого осуществляется формирование и передача пакетов ФИС
     *
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getEnrOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * Дата, когда сессия была создана в системе
     *
     * @return Дата создания сессии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EnrFisSyncSessionGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * Состояние неотозванных заявлений
     *
     * @return Состояние неотозванных заявлений. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getRequestState()
     */
        public PropertyPath<Integer> requestState()
        {
            if(_requestState == null )
                _requestState = new PropertyPath<Integer>(EnrFisSyncSessionGen.P_REQUEST_STATE, this);
            return _requestState;
        }

    /**
     * Дата, когда сессия была заполнена пакетами
     *
     * @return Дата формирования сессии.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getFillDate()
     */
        public PropertyPath<Date> fillDate()
        {
            if(_fillDate == null )
                _fillDate = new PropertyPath<Date>(EnrFisSyncSessionGen.P_FILL_DATE, this);
            return _fillDate;
        }

    /**
     * Показывает, когда сессия была добавлена в очередь
     *
     * @return Дата постановки сессии в очередь.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getQueueDate()
     */
        public PropertyPath<Date> queueDate()
        {
            if(_queueDate == null )
                _queueDate = new PropertyPath<Date>(EnrFisSyncSessionGen.P_QUEUE_DATE, this);
            return _queueDate;
        }

    /**
     * Показывает, когда сессия была перемещен в архив (остановлены все операции)
     *
     * @return Дата помещения сессии в архив.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getArchiveDate()
     */
        public PropertyPath<Date> archiveDate()
        {
            if(_archiveDate == null )
                _archiveDate = new PropertyPath<Date>(EnrFisSyncSessionGen.P_ARCHIVE_DATE, this);
            return _archiveDate;
        }

    /**
     * Текстовый лог, содержащий всю информацию о формировании сессии и обработке ее пакетов
     *
     * @return ZIP: лог.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getZipLog()
     */
        public PropertyPath<byte[]> zipLog()
        {
            if(_zipLog == null )
                _zipLog = new PropertyPath<byte[]>(EnrFisSyncSessionGen.P_ZIP_LOG, this);
            return _zipLog;
        }

    /**
     * Статус ПК в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус ПК ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getCampaignInfoStatus()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> campaignInfoStatus()
        {
            if(_campaignInfoStatus == null )
                _campaignInfoStatus = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_CAMPAIGN_INFO_STATUS, this);
            return _campaignInfoStatus;
        }

    /**
     * Пропустить генерацию и передачу 2ого пакета (например, при ежедневной выгрузке).
     *
     * @return Не передавать сведения об объеме и структуре приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipAdmissionInfo()
     */
        public PropertyPath<Boolean> skipAdmissionInfo()
        {
            if(_skipAdmissionInfo == null )
                _skipAdmissionInfo = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SKIP_ADMISSION_INFO, this);
            return _skipAdmissionInfo;
        }

    /**
     * Формирует уникальные названия для образовательных программ.
     *
     * @return Сформировать уникальные названия образовательных программ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isEduProgramUniqueTitleForming()
     */
        public PropertyPath<Boolean> eduProgramUniqueTitleForming()
        {
            if(_eduProgramUniqueTitleForming == null )
                _eduProgramUniqueTitleForming = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_EDU_PROGRAM_UNIQUE_TITLE_FORMING, this);
            return _eduProgramUniqueTitleForming;
        }

    /**
     * Выгружать заявления всех абитуриентов (например, когда требуется полностью обновить или восстановить состояние в ФИс по данным системы).
     *
     * @return Выгружать заявления всех абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isForceAllEntrants()
     */
        public PropertyPath<Boolean> forceAllEntrants()
        {
            if(_forceAllEntrants == null )
                _forceAllEntrants = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_FORCE_ALL_ENTRANTS, this);
            return _forceAllEntrants;
        }

    /**
     * Передавать даты согласий на зачисление.
     *
     * @return Передавать данные о согласиях на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isAcceptedInfo()
     */
        public PropertyPath<Boolean> acceptedInfo()
        {
            if(_acceptedInfo == null )
                _acceptedInfo = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_ACCEPTED_INFO, this);
            return _acceptedInfo;
        }

    /**
     * Если выключена, то пропускать заявления ФИС, в которых есть хотя бы один выбранный конкурс в состоянии «Зачислен».
     *
     * @return Передавать данные о заявлениях в приказах. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSendRequestInOrderInfo()
     */
        public PropertyPath<Boolean> sendRequestInOrderInfo()
        {
            if(_sendRequestInOrderInfo == null )
                _sendRequestInOrderInfo = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SEND_REQUEST_IN_ORDER_INFO, this);
            return _sendRequestInOrderInfo;
        }

    /**
     * Не формировать пакет с данными приемной кампании.
     *
     * @return Не формировать пакет с данными приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipEnrCampaignInfo()
     */
        public PropertyPath<Boolean> skipEnrCampaignInfo()
        {
            if(_skipEnrCampaignInfo == null )
                _skipEnrCampaignInfo = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SKIP_ENR_CAMPAIGN_INFO, this);
            return _skipEnrCampaignInfo;
        }

    /**
     * Не формировать пакеты с заявлениями вообще (для обновления данных о структуре приема).
     *
     * @return Не формировать пакеты с заявлениями абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipApplicationsInfo()
     */
        public PropertyPath<Boolean> skipApplicationsInfo()
        {
            if(_skipApplicationsInfo == null )
                _skipApplicationsInfo = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SKIP_APPLICATIONS_INFO, this);
            return _skipApplicationsInfo;
        }

    /**
     * Не формировать пакет с приказами.
     *
     * @return Не формировать пакет с приказами. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipOrdersOfAdmission()
     */
        public PropertyPath<Boolean> skipOrdersOfAdmission()
        {
            if(_skipOrdersOfAdmission == null )
                _skipOrdersOfAdmission = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SKIP_ORDERS_OF_ADMISSION, this);
            return _skipOrdersOfAdmission;
        }

    /**
     * Не включать в пакеты планы приема и ВНП абитуриентв по направлениям, которые не поддерживаются ФИС.
     * см. ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.IEnrFisSettingsDao#getEnrDirectionSet (например, экстернат).
     *
     * @return Пропускать направления, не имеющие аналогов в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipIllegalDirections()
     */
        public PropertyPath<Boolean> skipIllegalDirections()
        {
            if(_skipIllegalDirections == null )
                _skipIllegalDirections = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SKIP_ILLEGAL_DIRECTIONS, this);
            return _skipIllegalDirections;
        }

    /**
     * Не включать в пакеты абитуриентв, в заявлениях которых возникли обишки при генерации.
     *
     * @return Пропускать абитуриентов с ошибками. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isSkipErrorEntrants()
     */
        public PropertyPath<Boolean> skipErrorEntrants()
        {
            if(_skipErrorEntrants == null )
                _skipErrorEntrants = new PropertyPath<Boolean>(EnrFisSyncSessionGen.P_SKIP_ERROR_ENTRANTS, this);
            return _skipErrorEntrants;
        }

    /**
     * @return Число пропущенных при формировании 3 пакета абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getSkippedEntrantCount()
     */
        public PropertyPath<Integer> skippedEntrantCount()
        {
            if(_skippedEntrantCount == null )
                _skippedEntrantCount = new PropertyPath<Integer>(EnrFisSyncSessionGen.P_SKIPPED_ENTRANT_COUNT, this);
            return _skippedEntrantCount;
        }

    /**
     * @return Число пропущенных при формировании 4 пакета выписок. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getSkippedExtractCount()
     */
        public PropertyPath<Integer> skippedExtractCount()
        {
            if(_skippedExtractCount == null )
                _skippedExtractCount = new PropertyPath<Integer>(EnrFisSyncSessionGen.P_SKIPPED_EXTRACT_COUNT, this);
            return _skippedExtractCount;
        }

    /**
     * @return Число пропущенных при формировании 4 пакета рекомендованных. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getSkippedRecItemCount()
     */
        public PropertyPath<Integer> skippedRecItemCount()
        {
            if(_skippedRecItemCount == null )
                _skippedRecItemCount = new PropertyPath<Integer>(EnrFisSyncSessionGen.P_SKIPPED_REC_ITEM_COUNT, this);
            return _skippedRecItemCount;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isAlive()
     */
        public SupportedPropertyPath<Boolean> alive()
        {
            if(_alive == null )
                _alive = new SupportedPropertyPath<Boolean>(EnrFisSyncSessionGen.P_ALIVE, this);
            return _alive;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getArchiveDateAsString()
     */
        public SupportedPropertyPath<String> archiveDateAsString()
        {
            if(_archiveDateAsString == null )
                _archiveDateAsString = new SupportedPropertyPath<String>(EnrFisSyncSessionGen.P_ARCHIVE_DATE_AS_STRING, this);
            return _archiveDateAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isArchiveDisabled()
     */
        public SupportedPropertyPath<Boolean> archiveDisabled()
        {
            if(_archiveDisabled == null )
                _archiveDisabled = new SupportedPropertyPath<Boolean>(EnrFisSyncSessionGen.P_ARCHIVE_DISABLED, this);
            return _archiveDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isArchived()
     */
        public SupportedPropertyPath<Boolean> archived()
        {
            if(_archived == null )
                _archived = new SupportedPropertyPath<Boolean>(EnrFisSyncSessionGen.P_ARCHIVED, this);
            return _archived;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getContentStatus()
     */
        public SupportedPropertyPath<String> contentStatus()
        {
            if(_contentStatus == null )
                _contentStatus = new SupportedPropertyPath<String>(EnrFisSyncSessionGen.P_CONTENT_STATUS, this);
            return _contentStatus;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getCreationDateAsString()
     */
        public SupportedPropertyPath<String> creationDateAsString()
        {
            if(_creationDateAsString == null )
                _creationDateAsString = new SupportedPropertyPath<String>(EnrFisSyncSessionGen.P_CREATION_DATE_AS_STRING, this);
            return _creationDateAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getDateLog()
     */
        public SupportedPropertyPath<String> dateLog()
        {
            if(_dateLog == null )
                _dateLog = new SupportedPropertyPath<String>(EnrFisSyncSessionGen.P_DATE_LOG, this);
            return _dateLog;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isDeleteDisabled()
     */
        public SupportedPropertyPath<Boolean> deleteDisabled()
        {
            if(_deleteDisabled == null )
                _deleteDisabled = new SupportedPropertyPath<Boolean>(EnrFisSyncSessionGen.P_DELETE_DISABLED, this);
            return _deleteDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isQueueDisabled()
     */
        public SupportedPropertyPath<Boolean> queueDisabled()
        {
            if(_queueDisabled == null )
                _queueDisabled = new SupportedPropertyPath<Boolean>(EnrFisSyncSessionGen.P_QUEUE_DISABLED, this);
            return _queueDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#isQueued()
     */
        public SupportedPropertyPath<Boolean> queued()
        {
            if(_queued == null )
                _queued = new SupportedPropertyPath<Boolean>(EnrFisSyncSessionGen.P_QUEUED, this);
            return _queued;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getStateTitle()
     */
        public SupportedPropertyPath<String> stateTitle()
        {
            if(_stateTitle == null )
                _stateTitle = new SupportedPropertyPath<String>(EnrFisSyncSessionGen.P_STATE_TITLE, this);
            return _stateTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrFisSyncSessionGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrFisSyncSession.class;
        }

        public String getEntityName()
        {
            return "enrFisSyncSession";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isAlive();

    public abstract String getArchiveDateAsString();

    public abstract boolean isArchiveDisabled();

    public abstract boolean isArchived();

    public abstract String getContentStatus();

    public abstract String getCreationDateAsString();

    public abstract String getDateLog();

    public abstract boolean isDeleteDisabled();

    public abstract boolean isQueueDisabled();

    public abstract boolean isQueued();

    public abstract String getStateTitle();

    public abstract String getTitle();
}
