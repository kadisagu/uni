package ru.tandemservice.unienr14_fis.base.ext.Depersonalization.objects;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.application.IModuleMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.DepersonalizationUtils;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.nul;

/**
 * @author avedernikov
 * @since 30.10.2015
 */

public class Unienr14fisClearBlobsDO implements IDepersonalizationObject
{
	@Override
	public String getTitle()
	{
		return "Очистка «ZIP» и «ZIP-XML» (Интеграция с ФИС)";
	}

	@Override
	public Collection<Class<? extends IEntity>> getEntityClasses()
	{
		return ImmutableList.of(
				EnrFisSyncSession.class,
				EnrFisSyncPackageIOLog.class
		);
	}

	@Override
	public String getSettingsName()
	{
		return "unienr14fisClearBlobs";
	}

	@Override
	public Collection<String> getDescription()
	{
		return ImmutableList.of(
				"Очищаются «ZIP: лог» из «Сессия синхронизации ФИС», " +
				"«ZIP-XML: отправленные данные в ФИС», «ZIP-XML: ответ из ФИС» из «Лог взаимодействия с сервером ФИС»."
		);
	}

	@Override
	public IModuleMeta getModule()
	{
		return DepersonalizationUtils.getModuleMeta(EnrFisEntrantSyncData.class);
	}

	@Override
	public void apply(Session session)
	{
		clearBlobs(EnrFisSyncSession.class, EnrFisSyncSession.zipLog().s(), session);
		ImmutableList<String> ioLogBlobs = ImmutableList.of(EnrFisSyncPackageIOLog.zipXmlRequest().s(), EnrFisSyncPackageIOLog.zipXmlResponse().s());
		clearBlobs(EnrFisSyncPackageIOLog.class, ioLogBlobs, session);
	}

	@Override
	public boolean isOptional()
	{
		return true;
	}

	private void clearBlobs(Class<? extends IEntity> target, String blobProp, Session session)
	{
		CommonDAO.executeAndClear(new DQLUpdateBuilder(target).set(blobProp, nul(PropertyType.BLOB)), session);
	}

	private void clearBlobs(Class<? extends IEntity> target, Collection<String> blobProps, Session session)
	{
		DQLUpdateBuilder dql = new DQLUpdateBuilder(target);
		for (String prop: blobProps)
			dql.set(prop, nul(PropertyType.BLOB));
		CommonDAO.executeAndClear(dql, session);
	}
}
