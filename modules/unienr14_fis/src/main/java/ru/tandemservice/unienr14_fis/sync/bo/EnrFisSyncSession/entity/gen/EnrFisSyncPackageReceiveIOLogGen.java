package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог взаимодействия с сервером ФИС (получение результата)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSyncPackageReceiveIOLogGen extends EnrFisSyncPackageIOLog
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog";
    public static final String ENTITY_NAME = "enrFisSyncPackageReceiveIOLog";
    public static final int VERSION_HASH = -1913502191;
    private static IEntityMeta ENTITY_META;

    public static final String P_FAILURE_COUNT = "failureCount";
    public static final String P_CONFLICT_COUNT = "conflictCount";
    public static final String P_SUCCESSFUL_COUNT = "successfulCount";

    private int _failureCount;     // Число тегов Failure
    private int _conflictCount;     // Число тегов Conflict
    private int _successfulCount;     // Число тегов Successful

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Число тегов Failure. Свойство не может быть null.
     */
    @NotNull
    public int getFailureCount()
    {
        return _failureCount;
    }

    /**
     * @param failureCount Число тегов Failure. Свойство не может быть null.
     */
    public void setFailureCount(int failureCount)
    {
        dirty(_failureCount, failureCount);
        _failureCount = failureCount;
    }

    /**
     * @return Число тегов Conflict. Свойство не может быть null.
     */
    @NotNull
    public int getConflictCount()
    {
        return _conflictCount;
    }

    /**
     * @param conflictCount Число тегов Conflict. Свойство не может быть null.
     */
    public void setConflictCount(int conflictCount)
    {
        dirty(_conflictCount, conflictCount);
        _conflictCount = conflictCount;
    }

    /**
     * @return Число тегов Successful. Свойство не может быть null.
     */
    @NotNull
    public int getSuccessfulCount()
    {
        return _successfulCount;
    }

    /**
     * @param successfulCount Число тегов Successful. Свойство не может быть null.
     */
    public void setSuccessfulCount(int successfulCount)
    {
        dirty(_successfulCount, successfulCount);
        _successfulCount = successfulCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrFisSyncPackageReceiveIOLogGen)
        {
            setFailureCount(((EnrFisSyncPackageReceiveIOLog)another).getFailureCount());
            setConflictCount(((EnrFisSyncPackageReceiveIOLog)another).getConflictCount());
            setSuccessfulCount(((EnrFisSyncPackageReceiveIOLog)another).getSuccessfulCount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSyncPackageReceiveIOLogGen> extends EnrFisSyncPackageIOLog.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSyncPackageReceiveIOLog.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSyncPackageReceiveIOLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "failureCount":
                    return obj.getFailureCount();
                case "conflictCount":
                    return obj.getConflictCount();
                case "successfulCount":
                    return obj.getSuccessfulCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "failureCount":
                    obj.setFailureCount((Integer) value);
                    return;
                case "conflictCount":
                    obj.setConflictCount((Integer) value);
                    return;
                case "successfulCount":
                    obj.setSuccessfulCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "failureCount":
                        return true;
                case "conflictCount":
                        return true;
                case "successfulCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "failureCount":
                    return true;
                case "conflictCount":
                    return true;
                case "successfulCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "failureCount":
                    return Integer.class;
                case "conflictCount":
                    return Integer.class;
                case "successfulCount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSyncPackageReceiveIOLog> _dslPath = new Path<EnrFisSyncPackageReceiveIOLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSyncPackageReceiveIOLog");
    }
            

    /**
     * @return Число тегов Failure. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog#getFailureCount()
     */
    public static PropertyPath<Integer> failureCount()
    {
        return _dslPath.failureCount();
    }

    /**
     * @return Число тегов Conflict. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog#getConflictCount()
     */
    public static PropertyPath<Integer> conflictCount()
    {
        return _dslPath.conflictCount();
    }

    /**
     * @return Число тегов Successful. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog#getSuccessfulCount()
     */
    public static PropertyPath<Integer> successfulCount()
    {
        return _dslPath.successfulCount();
    }

    public static class Path<E extends EnrFisSyncPackageReceiveIOLog> extends EnrFisSyncPackageIOLog.Path<E>
    {
        private PropertyPath<Integer> _failureCount;
        private PropertyPath<Integer> _conflictCount;
        private PropertyPath<Integer> _successfulCount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Число тегов Failure. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog#getFailureCount()
     */
        public PropertyPath<Integer> failureCount()
        {
            if(_failureCount == null )
                _failureCount = new PropertyPath<Integer>(EnrFisSyncPackageReceiveIOLogGen.P_FAILURE_COUNT, this);
            return _failureCount;
        }

    /**
     * @return Число тегов Conflict. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog#getConflictCount()
     */
        public PropertyPath<Integer> conflictCount()
        {
            if(_conflictCount == null )
                _conflictCount = new PropertyPath<Integer>(EnrFisSyncPackageReceiveIOLogGen.P_CONFLICT_COUNT, this);
            return _conflictCount;
        }

    /**
     * @return Число тегов Successful. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog#getSuccessfulCount()
     */
        public PropertyPath<Integer> successfulCount()
        {
            if(_successfulCount == null )
                _successfulCount = new PropertyPath<Integer>(EnrFisSyncPackageReceiveIOLogGen.P_SUCCESSFUL_COUNT, this);
            return _successfulCount;
        }

        public Class getEntityClass()
        {
            return EnrFisSyncPackageReceiveIOLog.class;
        }

        public String getEntityName()
        {
            return "enrFisSyncPackageReceiveIOLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
