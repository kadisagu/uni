package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_fis_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4OlympiadProfileDiscipline

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14fis_conv_olym_prof_disc_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_f447f113"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("olympiadprofilediscipline_id", DBType.LONG).setNullable(false), 
				new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrFisConv4OlympiadProfileDiscipline");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4StateExamSubject

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14fis_conv_st_exam_subj_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrfisconv4stateexamsubject"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("stateexamsubject_id", DBType.LONG).setNullable(false), 
				new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrFisConv4StateExamSubject");

		}


    }
}