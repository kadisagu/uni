package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_fis_2x10x5_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncSession

		// создано обязательное свойство sendRequestInOrderInfo
		{
			// создать колонку
			tool.createColumn("enr14fis_session_t", new DBColumn("sendrequestinorderinfo_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14fis_session_t set sendrequestinorderinfo_p=? where sendrequestinorderinfo_p is null", true);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14fis_session_t", "sendrequestinorderinfo_p", false);
		}
    }
}