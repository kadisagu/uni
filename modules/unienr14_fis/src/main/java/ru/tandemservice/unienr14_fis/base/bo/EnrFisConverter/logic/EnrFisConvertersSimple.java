/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic;

import ru.tandemservice.unienr14.catalog.entity.EnrLevelBudgetFinancing;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrLevelBudgetFinancingCodes;

/**
 * @author oleyba
 * @since 6/30/15
 */
public class EnrFisConvertersSimple
{
    public static long getLevelBudgetFinancing(EnrLevelBudgetFinancing levelBudgetFinancing)
    {
        switch (levelBudgetFinancing.getCode()) {
            case EnrLevelBudgetFinancingCodes.FEDERAL_BUDGET: return 1;
            case EnrLevelBudgetFinancingCodes.BUDGET_SUBJECT_R_F: return 2;
            case EnrLevelBudgetFinancingCodes.LOCAL_BUDGET: return 3;
        }
        throw new IllegalArgumentException();
    }
}
