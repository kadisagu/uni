/**
 *$Id: EnrFisConverterConfig4Olympiad.java 32223 2014-02-03 10:55:07Z vdanilov $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4Olympiad;

import com.google.common.collect.Collections2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOlympiadTypeCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiad.IEnrFisConverterDao4Olympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
@Configuration
public class EnrFisConverterConfig4Olympiad extends EnrFisConverterConfig
{
    public static final String EDU_YEAR_SEARCH_DS = EducationCatalogsManager.DS_EDU_YEAR;

    public static final String BIND_EDUCATION_YEAR = "bindEducationYear";

    @Override
    protected IEnrFisConverterDao4Olympiad getConverterDao()
    {
        return EnrFisConverterManager.instance().olympiad();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig()));
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
        .addColumn(textColumn("fullTitle", EnrOlympiad.title()).create())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4OlympiadEdit").hasBlockHeader(true).create())
        .create();
    }


    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        return super.fisConverterRecordDSHandler();
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EducationYear eduYear = context.get(BIND_EDUCATION_YEAR);
        if (null == eduYear) { return new ArrayList<>(0); }

        // фильтруем по селектору Учебный год
        return new ArrayList<>(Collections2.filter(
            entityList,
            input -> {
                if (!(input instanceof EnrOlympiad)) { return false; }
                final EnrOlympiad olympiad = (EnrOlympiad)input;
                return olympiad.getEduYear().equals(eduYear);
            }
        ));
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return super.fisConverterValueDSHandler();
    }
}
