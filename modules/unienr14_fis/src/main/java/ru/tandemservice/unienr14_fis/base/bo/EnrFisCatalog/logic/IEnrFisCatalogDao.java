package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic;

import org.apache.tapestry.request.IUploadFile;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14_fis.util.IEnrFisAuthentication;

/**
 * @author vdanilov
 */
public interface IEnrFisCatalogDao extends INeedPersistenceSupport {

    /** обновляет данные справочников ФИС */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void doSyncCatalogs(IEnrFisAuthentication auth);

    /** обновляет данные справочников ФИС */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void doImportCatalogs(IUploadFile file);
}
