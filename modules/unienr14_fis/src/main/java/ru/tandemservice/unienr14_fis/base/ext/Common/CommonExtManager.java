/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.ext.Common;

import org.hibernate.ScrollableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.*;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.day;
import static org.tandemframework.hibsupport.dql.DQLFunctions.min;

/**
 * @author rsizonenko
 * @since 26.02.2015
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unienr14_fis", () -> {

                final IUniBaseDao dao = IUniBaseDao.instance.get();
                final String alias = "a";

                List<String> result = new ArrayList<>();

                List<EducationYear> years = dao.getList(new DQLSelectBuilder().fromEntity(EducationYear.class, "e")
                    .joinEntity("e", DQLJoinType.inner, EnrEnrollmentCampaign.class, "c", eq(property("e"), property("c", EnrEnrollmentCampaign.educationYear())))
                    .column(property("e"))
                    .order(property("e", EducationYear.intValue()))
                    .distinct()
                );
                for (EducationYear year : years) {
                    List<EnrOrgUnit> enrOrgUnits = dao.getList(new DQLSelectBuilder().fromEntity(EnrOrgUnit.class, "e")
                        .where(eq(property("e", EnrOrgUnit.enrollmentCampaign().educationYear()), value(year)))
                        .order(property(EnrOrgUnit.institutionOrgUnit().orgUnit().title().fromAlias("e")))
                    );

                    result.add("");
                    result.add(year.getTitle());

                    for (final EnrOrgUnit enrOrgUnit : enrOrgUnits) {

                        result.add("");
                        result.add(enrOrgUnit.getTitle());
                        result.add("");
                        result.add("Число пакетов: ");
                        result.add("Пакет 1: Приемная кампания - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4CampaignInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4CampaignInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                        ));
                        result.add("Пакет 2: Сведения об объеме и структуре приема - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4AdmissionInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4AdmissionInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                        ));
                        result.add("Пакет 3: Заявления абитуриентов - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4ApplicationsInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4ApplicationsInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                        ));
                        result.add("Пакет 4: Приказы по абитуриентам - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4AOrdersInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4AOrdersInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                        ));
                        result.add("");

                        result.add("Число успешно отправленных пакетов:");
                        result.add("Пакет 1: Приемная кампания - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4CampaignInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4CampaignInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgSendSuccess())))
                        ));
                        result.add("Пакет 2: Сведения об объеме и структуре приема - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4AdmissionInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4AdmissionInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgSendSuccess())))
                        ));
                        result.add("Пакет 3: Заявления абитуриентов - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4ApplicationsInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4ApplicationsInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgSendSuccess())))
                        ));
                        result.add("Пакет 4: Приказы по абитуриентам - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4AOrdersInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4AOrdersInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgSendSuccess())))
                        ));
                        result.add("");

                        result.add("Число успешно обработанных пакетов:");
                        result.add("Пакет 1: Приемная кампания - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4CampaignInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4CampaignInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgRecvSuccess())))
                        ));
                        result.add("Пакет 2: Сведения об объеме и структуре приема - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4AdmissionInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4AdmissionInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgRecvSuccess())))
                        ));
                        result.add("Пакет 3: Заявления абитуриентов - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4ApplicationsInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4ApplicationsInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgRecvSuccess())))
                        ));
                        result.add("Пакет 4: Приказы по абитуриентам - " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncPackage4AOrdersInfo.class, alias)
                            .where(eq(property(alias, EnrFisSyncPackage4AOrdersInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                            .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgRecvSuccess())))
                        ));
                        result.add("");

                        result.add("Число сессий: " + dao.getCount(EnrFisSyncSession.class, EnrFisSyncSession.L_ENR_ORG_UNIT, enrOrgUnit));
                        result.add("Число сессий с успешно обработанными пакетами: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncSession.class, alias)
                            .joinEntity(alias, DQLJoinType.inner, EnrFisSyncPackage.class, "d", eq(property("d", EnrFisSyncPackage.session().id()), property(alias, EnrFisSyncSession.id())))
                            .where(isNotNull(property("d", EnrFisSyncPackage.pkgRecvSuccess())))
                            .column(property(alias, EnrFisSyncSession.id()))
                            .distinct()
                        ));
                        result.add("Число сессий с безуспешно обработанными пакетами: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrFisSyncSession.class, alias)
                            .joinEntity(alias, DQLJoinType.inner, EnrFisSyncPackage.class, "d", eq(property("d", EnrFisSyncPackage.session().id()), property(alias, EnrFisSyncSession.id())))
                            .where(isNull(property("d", EnrFisSyncPackage.pkgRecvSuccess())))
                            .column(property(alias, EnrFisSyncSession.id()))
                            .distinct()
                        ));
                        result.add("");

                        result.add("Число дней отправки: " + dao.getCount(new DQLSelectBuilder()
                            .fromEntity(EnrFisSyncSession.class, alias)
                            .joinEntity(alias, DQLJoinType.inner, EnrFisSyncPackage.class, "d", eq(property("d", EnrFisSyncPackage.session().id()), property(alias, EnrFisSyncSession.id())))
                            .where(isNull(property("d", EnrFisSyncPackage.pkgRecvSuccess())))
                            .column(day(property(alias, EnrFisSyncSession.queueDate())))
                            .distinct()
                        ));
                        result.add("");

                        result.add("Первая успешная отправка:");
                        result.add("Пакет 1: Приемная кампания - " + dao.getCalculatedValue(session -> {
                            ScrollableResults scroll = new DQLSelectBuilder()
                                .fromEntity(EnrFisSyncPackage4CampaignInfo.class, alias)
                                .where(eq(property(alias, EnrFisSyncPackage4CampaignInfo.session().enrOrgUnit()), value(enrOrgUnit)))
                                .where(isNotNull(property(alias, EnrFisSyncPackage4CampaignInfo.pkgRecvSuccess())))
                                .order(min(day(property(alias, EnrFisSyncPackage4CampaignInfo.session().queueDate()))))
                                .column((property(alias, EnrFisSyncPackage4CampaignInfo.session().queueDate())))
                                .group(property(alias, EnrFisSyncPackage4CampaignInfo.session().queueDate()))
                                .createStatement(session).scroll();
                            if (scroll.next()) {
                                return DateFormatter.DEFAULT_DATE_FORMATTER.format(scroll.getDate(0));
                            }
                            return "-";
                        }));
                        result.add("");

                        // todo число дней отправки  и всё остальное
                        result.add("");
                        result.add("");
                        result.add("");
                        result.add("");
                    }
                }

                return result;
            }
            ).create();
    }
}