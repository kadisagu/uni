/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.enrollmentCampaignType;

import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCampaignType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Alexey Lopatin
 * @since 23.05.2016
 */
public interface IEnrFisConverterDao4EnrollmentCampaignType extends IEnrFisConverterDao<EnrEnrollmentCampaignType>
{
}
