/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategory;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public abstract class EnrFisConverterConfig4DocumentCategory extends EnrFisConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrollmentCampaign = context.get(BIND_ENROLLMENT_CAMPAIGN);
        if (null == enrollmentCampaign) return Lists.newArrayList();

        return entityList;
    }
}
