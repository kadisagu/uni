/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author nvankov
 * @since 01.09.2016
 */
public interface IEnr14FisAppDeletePackageGenerator extends INeedPersistenceSupport
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    String getPackage(EnrEnrollmentCampaign campaign, OrgUnit orgUnit, boolean fromOrders);
}
