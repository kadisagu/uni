package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.finSource;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEnrFisFinSourceDao extends INeedPersistenceSupport {

    Map<EnrCompetition, EnrFisCatalogItem> getSourceMap(Collection<EnrCompetition> directions);

    EnrFisCatalogItem getSource(EnrCompetition competition);

    EnrFisCatalogItem getSource(EnrCompetitionType competitionType);
}
