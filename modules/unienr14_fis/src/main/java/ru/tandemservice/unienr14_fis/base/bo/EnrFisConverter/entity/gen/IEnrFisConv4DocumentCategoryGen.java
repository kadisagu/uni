package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;

/**
 * Конвертер категорий документов
 *
 * Интерфейс для конвертеров по категории документов.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrFisConv4DocumentCategoryGen extends InterfaceStubBase
 implements IEnrFisConv4DocumentCategory{
    public static final int VERSION_HASH = 75489519;

    public static final String L_DOC_CATEGORY = "docCategory";
    public static final String L_VALUE = "value";

    private PersonDocumentCategory _docCategory;
    private EnrFisCatalogItem _value;


    public PersonDocumentCategory getDocCategory()
    {
        return _docCategory;
    }

    public void setDocCategory(PersonDocumentCategory docCategory)
    {
        _docCategory = docCategory;
    }

    @NotNull

    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    public void setValue(EnrFisCatalogItem value)
    {
        _value = value;
    }

    private static final Path<IEnrFisConv4DocumentCategory> _dslPath = new Path<IEnrFisConv4DocumentCategory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory");
    }
            

    /**
     * @return Категория для документов.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory#getDocCategory()
     */
    public static PersonDocumentCategory.Path<PersonDocumentCategory> docCategory()
    {
        return _dslPath.docCategory();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends IEnrFisConv4DocumentCategory> extends EntityPath<E>
    {
        private PersonDocumentCategory.Path<PersonDocumentCategory> _docCategory;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория для документов.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory#getDocCategory()
     */
        public PersonDocumentCategory.Path<PersonDocumentCategory> docCategory()
        {
            if(_docCategory == null )
                _docCategory = new PersonDocumentCategory.Path<PersonDocumentCategory>(L_DOC_CATEGORY, this);
            return _docCategory;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return IEnrFisConv4DocumentCategory.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
