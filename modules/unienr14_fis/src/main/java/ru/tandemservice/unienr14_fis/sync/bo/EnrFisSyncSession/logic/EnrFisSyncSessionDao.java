package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import com.sun.istack.NotNull;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.DebugBlock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

import javax.annotation.Nullable;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.Function;

/**
 * @author vdanilov
 */
public class EnrFisSyncSessionDao extends UniBaseDao implements IEnrFisSyncSessionDao {

    @Override
    public void doSave(final EnrFisSyncSession fisSession)
    {
        // блокируем всю ПК
        final Session session = lock("enr-fis-sync-create-"+fisSession.getEnrOrgUnit().getEnrollmentCampaign().getId());

        fisSession.setCreationDate(new Date());
        session.save(fisSession);
    }

    @Override
    public void doFill(final EnrFisSyncSession fisSession)
    {
        // блокируем всю ПК
        final Session session = lock("enr-fis-sync-fill-"+fisSession.getEnrOrgUnit().getEnrollmentCampaign().getId());

        PkgDaoBase.doInPkgDao(fisSession.getId(), () -> {

            // на время генерации flush будет вызываться только руками
            FlushMode flushMode = session.getFlushMode();
            try {
                session.setFlushMode(FlushMode.MANUAL); // при генерации делаем flush только руками
                session.flush(); // не забываем сделать flush перед геренацией

                lock(fisSession.getEnrOrgUnit().getEnrollmentCampaign());
                if (null != fisSession.getFillDate()) { throw new IllegalStateException(); }
                if (null != fisSession.getQueueDate()) { throw new IllegalStateException(); }
                if (null != fisSession.getArchiveDate()) { throw new IllegalStateException(); }

                final EnrFisSyncSessionManager instance = EnrFisSyncSessionManager.instance();
                try (DebugBlock b = Debug.block("EnrFisSyncSessionDao.dao4CampaignInfo.doCreatePackage")) {
                    instance.dao4CampaignInfo().doCreatePackage(fisSession);
                }
                try (DebugBlock b = Debug.block("EnrFisSyncSessionDao.dao4AdmissionInfo.doCreatePackage")) {
                    instance.dao4AdmissionInfo().doCreatePackage(fisSession);
                }
                try (DebugBlock b = Debug.block("EnrFisSyncSessionDao.dao4ApplicationInfo.doCreatePackage")) {
                    instance.dao4ApplicationInfo().doCreatePackages(fisSession);
                }
                try (DebugBlock b = Debug.block("EnrFisSyncSessionDao.dao4OrdersOfAdmission.doCreatePackage")) {
                    instance.dao4OrdersOfAdmission().doCreatePackage(fisSession);
                }

                fisSession.info(
                    EnrFisSyncSessionManager.instance().getProperty("title.pkggen"),
                    EnrFisSyncSessionManager.instance().getProperty("info.pkggen-complete")
                );

                fisSession.setFillDate(new Date());
                session.saveOrUpdate(fisSession);
                session.flush();
                session.clear();

            } finally {
                session.setFlushMode(flushMode);
            }

        });
    }

    @Override
    public void doQueue(EnrFisSyncSession fisSession)
    {
        // блокируем всю ПК
        final Session session = lock("enr-fis-sync-queue-"+fisSession.getEnrOrgUnit().getEnrollmentCampaign().getId());
        session.refresh(fisSession);

        if (fisSession.isArchived()) { throw new ApplicationException(""); }
        if (fisSession.isQueueDisabled()) { throw new ApplicationException(""); }

        if (null != fisSession.getQueueDate()) { return; }
        fisSession.setQueueDate(new Date());
        session.saveOrUpdate(fisSession);
    }

    @Override
    public void doArchive(EnrFisSyncSession fisSession)
    {
        // блокируем всю ПК
        final Session session = lock("enr-fis-sync-queue-"+fisSession.getEnrOrgUnit().getEnrollmentCampaign().getId());
        session.refresh(fisSession);

        // todo нужны проверки, что можно архивировать сессию, и сброс состояний абитуриентов видимо тоже нужен

        if (null != fisSession.getArchiveDate()) { return; }
        fisSession.setArchiveDate(new Date());
        session.saveOrUpdate(fisSession);
    }

    @Override
    public void doDelete(EnrFisSyncSession fisSession)
    {
        final Session session = lock("enr-fis-sync-queue-"+fisSession.getEnrOrgUnit().getEnrollmentCampaign().getId());
        session.refresh(fisSession);

        // todo нужны проверки, что можно удалять

        delete(fisSession);
    }

    @Override
    public Function<Date, XMLGregorianCalendar> getDateTimeXmlConverter(@Nullable final EnrEnrollmentCampaign campaign, @NotNull final DatatypeFactory dataTypeFactory)
    {
        return (Date date) -> {
            if (date == null) {
                return null;
            }
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());

            final XMLGregorianCalendar xmlGC = dataTypeFactory.newXMLGregorianCalendar(gc);

            if (campaign == null || campaign.getEducationYear().getIntValue() > 2015 || "true".equals(ApplicationRuntime.getProperty("unienr14_fis.trimDate"))) {
                // Для новых ПК (> 2015) формируем таймстамп без миллисекунд и часового пояса, т.к. огребли с этим.
                xmlGC.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                xmlGC.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
            }

            return xmlGC;
        };
    }
}
