package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageSendIOLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог взаимодействия с сервером ФИС (отправка пакета)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSyncPackageSendIOLogGen extends EnrFisSyncPackageIOLog
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageSendIOLog";
    public static final String ENTITY_NAME = "enrFisSyncPackageSendIOLog";
    public static final int VERSION_HASH = 1293545423;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATA_NODE_COUNT = "dataNodeCount";

    private int _dataNodeCount;     // Число отправленных значимых тегов (даты ПК, КГ, абитуриенты, приказы)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Число отправленных значимых тегов (даты ПК, КГ, абитуриенты, приказы). Свойство не может быть null.
     */
    @NotNull
    public int getDataNodeCount()
    {
        return _dataNodeCount;
    }

    /**
     * @param dataNodeCount Число отправленных значимых тегов (даты ПК, КГ, абитуриенты, приказы). Свойство не может быть null.
     */
    public void setDataNodeCount(int dataNodeCount)
    {
        dirty(_dataNodeCount, dataNodeCount);
        _dataNodeCount = dataNodeCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrFisSyncPackageSendIOLogGen)
        {
            setDataNodeCount(((EnrFisSyncPackageSendIOLog)another).getDataNodeCount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSyncPackageSendIOLogGen> extends EnrFisSyncPackageIOLog.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSyncPackageSendIOLog.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSyncPackageSendIOLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dataNodeCount":
                    return obj.getDataNodeCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dataNodeCount":
                    obj.setDataNodeCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dataNodeCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dataNodeCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dataNodeCount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSyncPackageSendIOLog> _dslPath = new Path<EnrFisSyncPackageSendIOLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSyncPackageSendIOLog");
    }
            

    /**
     * @return Число отправленных значимых тегов (даты ПК, КГ, абитуриенты, приказы). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageSendIOLog#getDataNodeCount()
     */
    public static PropertyPath<Integer> dataNodeCount()
    {
        return _dslPath.dataNodeCount();
    }

    public static class Path<E extends EnrFisSyncPackageSendIOLog> extends EnrFisSyncPackageIOLog.Path<E>
    {
        private PropertyPath<Integer> _dataNodeCount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Число отправленных значимых тегов (даты ПК, КГ, абитуриенты, приказы). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageSendIOLog#getDataNodeCount()
     */
        public PropertyPath<Integer> dataNodeCount()
        {
            if(_dataNodeCount == null )
                _dataNodeCount = new PropertyPath<Integer>(EnrFisSyncPackageSendIOLogGen.P_DATA_NODE_COUNT, this);
            return _dataNodeCount;
        }

        public Class getEntityClass()
        {
            return EnrFisSyncPackageSendIOLog.class;
        }

        public String getEntityName()
        {
            return "enrFisSyncPackageSendIOLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
