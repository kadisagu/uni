package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.exams.entity.gen.IEnrExamSetElementValueGen;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Элемент набора вступительных испытаний
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4ExamSetElementGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4ExamSetElementGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement";
    public static final String ENTITY_NAME = "enrFisConv4ExamSetElement";
    public static final int VERSION_HASH = -116792689;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET_ELEMENT = "examSetElement";
    public static final String L_VALUE = "value";

    private IEnrExamSetElementValue _examSetElement;     // Элемент набора вступительных испытаний
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public IEnrExamSetElementValue getExamSetElement()
    {
        return _examSetElement;
    }

    /**
     * @param examSetElement Элемент набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     */
    public void setExamSetElement(IEnrExamSetElementValue examSetElement)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && examSetElement!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrExamSetElementValue.class);
            IEntityMeta actual =  examSetElement instanceof IEntity ? EntityRuntime.getMeta((IEntity) examSetElement) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_examSetElement, examSetElement);
        _examSetElement = examSetElement;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4ExamSetElementGen)
        {
            if (withNaturalIdProperties)
            {
                setExamSetElement(((EnrFisConv4ExamSetElement)another).getExamSetElement());
            }
            setValue(((EnrFisConv4ExamSetElement)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4ExamSetElementGen> getNaturalId()
    {
        return new NaturalId(getExamSetElement());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4ExamSetElementGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4ExamSetElementNaturalProxy";

        private Long _examSetElement;

        public NaturalId()
        {}

        public NaturalId(IEnrExamSetElementValue examSetElement)
        {
            _examSetElement = ((IEntity) examSetElement).getId();
        }

        public Long getExamSetElement()
        {
            return _examSetElement;
        }

        public void setExamSetElement(Long examSetElement)
        {
            _examSetElement = examSetElement;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4ExamSetElementGen.NaturalId) ) return false;

            EnrFisConv4ExamSetElementGen.NaturalId that = (NaturalId) o;

            if( !equals(getExamSetElement(), that.getExamSetElement()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExamSetElement());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExamSetElement());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4ExamSetElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4ExamSetElement.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4ExamSetElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examSetElement":
                    return obj.getExamSetElement();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examSetElement":
                    obj.setExamSetElement((IEnrExamSetElementValue) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examSetElement":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examSetElement":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examSetElement":
                    return IEnrExamSetElementValue.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4ExamSetElement> _dslPath = new Path<EnrFisConv4ExamSetElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4ExamSetElement");
    }
            

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement#getExamSetElement()
     */
    public static IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue> examSetElement()
    {
        return _dslPath.examSetElement();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4ExamSetElement> extends EntityPath<E>
    {
        private IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue> _examSetElement;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement#getExamSetElement()
     */
        public IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue> examSetElement()
        {
            if(_examSetElement == null )
                _examSetElement = new IEnrExamSetElementValueGen.Path<IEnrExamSetElementValue>(L_EXAM_SET_ELEMENT, this);
            return _examSetElement;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4ExamSetElement.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4ExamSetElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
