package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4StateExamSubjectGen */
public class EnrFisConv4StateExamSubject extends EnrFisConv4StateExamSubjectGen
{
    public EnrFisConv4StateExamSubject()
    {
    }

    public EnrFisConv4StateExamSubject(EnrStateExamSubject subject, EnrFisCatalogItem fisCatalogItem)
    {
        setStateExamSubject(subject);
        setValue(fisCatalogItem);
    }
}