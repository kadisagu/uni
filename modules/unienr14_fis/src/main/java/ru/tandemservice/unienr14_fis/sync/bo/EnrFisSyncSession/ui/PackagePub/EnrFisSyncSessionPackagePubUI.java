package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.PackagePub;

import enr14fis.schema.pkg.result.resp.ImportResultPackage;
import enr14fis.schema.pkg.result.resp.TErrorInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.entity.UidRegistryItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageReceiveIOLog;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

import java.util.*;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrFisSyncSessionPackagePubUI extends UIPresenter {

    private final EntityHolder<EnrFisSyncPackage> holder = new EntityHolder<EnrFisSyncPackage>();
    private OrgUnitSecModel _secModel;
    private String selectedTab;

    private List<EnrFisSyncPackageIOLog> ioLogList;
    private EnrFisSyncPackageIOLog ioLog;

    private Map<EnrFisSyncPackageIOLog, List<FailureWrapper>> failureMap = new HashMap<>();
    private FailureWrapper failure;

    @Override
    public void onComponentRefresh() {
        getHolder().refresh();
        _secModel = new OrgUnitSecModel(getOrgUnit());

        setIoLogList(
            DataAccessServices.dao().<EnrFisSyncPackageIOLog>getList(
                new DQLSelectBuilder()
                .fromEntity(EnrFisSyncPackageIOLog.class, "x")
                .column(property("x"))
                .where(eq(property(EnrFisSyncPackageIOLog.owner().fromAlias("x")), value(getFisPackage())))
                .order(property(EnrFisSyncPackageIOLog.operationDate().fromAlias("x")), OrderDirection.desc)
            )
        );

        prepareFailureData();
    }

    // presenter

    public OrgUnit getOrgUnit() {
        return getFisPackage().getSession().getEnrOrgUnit().getInstitutionOrgUnit().getOrgUnit();
    }

    public List<FailureWrapper> getFailureList() {
        return getFailureMap().get(getIoLog());
    }

    public boolean isShowFailureList() {
        return !CollectionUtils.isEmpty(getFailureList());
    }

    public boolean isShowLogError() {
        return getIoLog() != null && getIoLog().isErrorByZipLog() && StringUtils.isEmpty(getIoLog().getErrorString());
    }

    // utils

    private void prepareFailureData()
    {
        setFailureMap(new HashMap<EnrFisSyncPackageIOLog, List<FailureWrapper>>());
        for (EnrFisSyncPackageIOLog log : getIoLogList()) {
            if (!(log instanceof EnrFisSyncPackageReceiveIOLog)) continue;
            if (log.isErrorByZipLog()) continue;

            byte[] xmlResponse = log.getXmlResponse();
            if (null == xmlResponse) { continue; }

            ImportResultPackage resultPackage;
            try {
                resultPackage = EnrFisServiceImpl.fromXml(ImportResultPackage.class, xmlResponse);
            } catch (Exception e) {
                Debug.exception(e);
                continue;
            }
            if (null == resultPackage) continue;
            List<FailureWrapper> wrappers = new ArrayList<>();
            getFailureMap().put(log, wrappers);
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getAdmissionVolumes())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getDistributedAdmissionVolumes())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getInstitutionAchievements())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getCompetitiveGroups())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getCompetitiveGroupItems())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getTargetOrganizations())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getTargetOrganizationDirections())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getEntranceTestItems())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getCommonBenefit())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getEntranceTestBenefits())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getApplications())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getOlympicDocuments())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getOlympicTotalDocuments())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getDisabilityDocuments())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getMedicalDocuments())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getAllowEducationDocuments())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getCampaigns())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getCustomDocuments())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getEntranceTestResults())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getApplicationCommonBenefits())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getApplicationsInOrders())); } catch (NullPointerException ignored) {}
            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getOrdersOfAdmissions())); } catch (NullPointerException ignored) {}
//            try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getCampaignDates())); } catch (NullPointerException ignored) {}
            // try { wrappers.addAll(FailureWrapper.initFrom(resultPackage.getLog().getFailed().getRecommendedLists())); } catch (NullPointerException ignored) {}
        }
    }

    // getters and setters

    public EntityHolder<EnrFisSyncPackage> getHolder() { return this.holder; }
    public EnrFisSyncPackage getFisPackage() { return getHolder().getValue(); }
    public OrgUnitSecModel getSecModel() { return _secModel; }
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(String selectedTab) { this.selectedTab = selectedTab; }
    public List<EnrFisSyncPackageIOLog> getIoLogList() { return this.ioLogList; }
    public void setIoLogList(List<EnrFisSyncPackageIOLog> ioLogList) { this.ioLogList = ioLogList; }
    public EnrFisSyncPackageIOLog getIoLog() { return this.ioLog; }
    public void setIoLog(EnrFisSyncPackageIOLog ioLog) { this.ioLog = ioLog; }

    public FailureWrapper getFailure()
    {
        return failure;
    }

    public void setFailure(FailureWrapper failure)
    {
        this.failure = failure;
    }

    public Map<EnrFisSyncPackageIOLog, List<FailureWrapper>> getFailureMap()
    {
        return failureMap;
    }

    public void setFailureMap(Map<EnrFisSyncPackageIOLog, List<FailureWrapper>> failureMap)
    {
        this.failureMap = failureMap;
    }

    // inner classes

    public static class FailureWrapper {
        private String uid;
        private Long entityId;
        private String entityTitle;
        private String text;
        private String envInfo;

        public Long getEntityId() { return entityId; }
        public void setEntityId(Long entityId) { this.entityId = entityId; }
        public String getEntityTitle() { return entityTitle; }
        public void setEntityTitle(String entityTitle) { this.entityTitle = entityTitle; }
        public String getEnvInfo() { return envInfo; }
        public void setEnvInfo(String envInfo) { this.envInfo = envInfo; }
        public String getText() { return text; }
        public void setText(String text) { this.text = text; }
        public String getUid() { return uid; }
        public void setUid(String uid) { this.uid = uid; }

        private static final Pattern UID_CANDIDATE_PATTERN = Pattern.compile("[a-zA-Z0-9]*");
        private static final Pattern ID_CANDIDATE_PATTERN = Pattern.compile("UID_[a-zA-Z0-9]*_[a-zA-Z]*");

        private void findEntity()
        {
            findEntityByUid();
            findEntityById();
        }

        private void findEntityById()
        {
            Set<String> candidates = new HashSet<String>(Arrays.asList(StringUtils.split(getText(), " ()[]<>,.-=:{}")));
            for (Iterator<String> it = candidates.iterator(); it.hasNext(); ) {
                String candidate = it.next();
                if (!ID_CANDIDATE_PATTERN.matcher(candidate).matches()) {
                    it.remove();
                }
            }

            if (candidates.isEmpty()) { return; }

            for (String candidate: candidates) {
                String[] split = candidate.split("_");
                if (split.length < 3) continue;
                String hex = split[1];
                try {
                    Long id = Long.parseLong(hex, 16);
                    IEntity entity = IUniBaseDao.instance.get().get(id);
                    if (entity instanceof IFisUidByIdOwner) {
                        setUid(candidate);
                        setEntityId(id);
                        setEntityTitle(((IFisUidByIdOwner)entity).getFisUidTitle());
                        break;
                    }
                } catch (NumberFormatException e) {
                    // ну и не нашли значит
                }
            }
        }

        private void findEntityByUid()
        {
            Set<String> candidates = new HashSet<String>(Arrays.asList(StringUtils.split(getText(), " ()[]<>,.-_=:{}")));
            for (Iterator<String> it = candidates.iterator(); it.hasNext(); ) {
                String candidate = it.next();
                if (!UID_CANDIDATE_PATTERN.matcher(candidate).matches()) {
                    it.remove();
                }
            }

            if (candidates.isEmpty()) { return; }

            final String uidRegistryCode = EnrFisSettingsManager.instance().uidRegistry().getCode();
            final List<Object[]> rows = DataAccessServices.dao().<Object[]>getList(
                new DQLSelectBuilder()
                .fromEntity(UidRegistryItem.class, "i")
                .column(property(UidRegistryItem.uid().fromAlias("i")))
                .column(property(UidRegistryItem.relatedEntityId().fromAlias("i")))
                .column(property(UidRegistryItem.relatedEntityTitle().fromAlias("i")))
                .column(property(UidRegistryItem.relatedEntityComment().fromAlias("i")))
                .where(eq(property(UidRegistryItem.registry().code().fromAlias("i")), value(uidRegistryCode)))
                .where(in(property(UidRegistryItem.uid().fromAlias("i")), candidates))
                .order(property(UidRegistryItem.id().fromAlias("i")), OrderDirection.desc),
                0, 1
            );

            if (rows.isEmpty()) { return; }
            final Object[] row = rows.get(0);
            setUid((String)row[0]);
            setEntityId((Long)row[1]);
            setEntityTitle((String)row[2] + (StringUtils.isEmpty((String)row[3]) ? "" : " (" + (String)row[3] + ")"));
        }

        private static FailureWrapper wrapper(TErrorInfo errorInfo, String envInfo)
        {
            FailureWrapper wrapper = new FailureWrapper();
            wrapper.setText(errorInfo.getMessage());
            wrapper.setEnvInfo(envInfo);
            wrapper.findEntity();
            return wrapper;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.AdmissionVolumes node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.AdmissionVolumes.AdmissionVolume element : node.getAdmissionVolume()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Направление ФИС: " + element.getDirectionName() + "</div>Уровень образования ФИС: " + element.getEducationLevelName()
                ));
            }
            return list;
        }


        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.DistributedAdmissionVolumes node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.DistributedAdmissionVolumes.DistributedAdmissionVolume element : node.getDistributedAdmissionVolume()) {
                list.add(wrapper(element.getErrorInfo(),
                    "Уровень бюджета: " + element.getLevelBudget()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.InstitutionAchievements node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.InstitutionAchievements.InstitutionAchievement element : node.getInstitutionAchievement()) {
                list.add(wrapper(element.getErrorInfo(),
                    "ИД: " + element.getName()
                ));
            }
            return list;
        }


//        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.RecommendedLists node)
//        {
//            List<FailureWrapper> list = new ArrayList<>();
//            for (ImportResultPackage.Log.Failed.RecommendedLists.RecommendedList element : node.getRecommendedList()) {
//                list.add(wrapper(element.getErrorInfo(),
//                    "<div>Этап зачисления: " + element.getStage()
//                ));
//            }
//            return list;
//        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.AllowEducationDocuments node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.AllowEducationDocuments.AllowEducationDocument element : node.getAllowEducationDocument()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Номер документа ФИС: " + element.getDocumentNumber()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.ApplicationCommonBenefits node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.ApplicationCommonBenefits.ApplicationCommonBenefit element : node.getApplicationCommonBenefit()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Льгота ФИС: " + element.getBenefitKindName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.ApplicationsInOrders node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.ApplicationsInOrders.ApplicationsInOrder element : node.getApplicationsInOrder()) {
                list.add(wrapper(element.getErrorInfo(),
                    "ID заявления ФИС: " + element.getUID()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.Applications node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.Applications.Application element : node.getApplication()) {
                list.add(wrapper(element.getErrorInfo(),
                    "Заявление ФИС: " + element.getApplicationNumber()
                ));
            }
            return list;
        }

//        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.CampaignDates node)
//        {
//            List<FailureWrapper> list = new ArrayList<>();
//            for (ImportResultPackage.Log.Failed.CampaignDates.CampaignDate element : node.getCampaignDate()) {
//                list.add(wrapper(element.getErrorInfo(),
//                    "UID ФИС: " + element.getUID()
//                ));
//            }
//            return list;
//        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.Campaigns node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.Campaigns.Campaign element : node.getCampaign()) {
                list.add(wrapper(element.getErrorInfo(),
                    "ПК ФИС: " + element.getName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.CommonBenefit node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.CommonBenefit.CommonBenefitItem element : node.getCommonBenefitItem()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>КГ ФИС: " + element.getCompetitiveGroupName() + "</div>Льгота ФИС: " + element.getBenefitKindName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.CompetitiveGroupItems node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.CompetitiveGroupItems.CompetitiveGroupItem element : node.getCompetitiveGroupItem()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>КГ ФИС: " + element.getCompetitiveGroupName() + "</div>Направление ФИС: " + element.getDirectionCode() + " " + element.getDirectionName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.CompetitiveGroups node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.CompetitiveGroups.CompetitiveGroup element : node.getCompetitiveGroup()) {
                list.add(wrapper(element.getErrorInfo(),
                    "КГ ФИС: " + element.getCompetitiveGroupName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.CustomDocuments node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.CustomDocuments.CustomDocument element : node.getCustomDocument()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Документ ФИС: " + element.getDocumentNumber()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.DisabilityDocuments node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.DisabilityDocuments.DisabilityDocument element : node.getDisabilityDocument()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Документ ФИС: " + element.getDocumentNumber()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.EntranceTestBenefits node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.EntranceTestBenefits.EntranceTestBenefitItem element : node.getEntranceTestBenefitItem()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>КГ ФИС: " + element.getCompetitiveGroupName() + "</div><div>Льгота ФИС: " + element.getBenefitKindName() + "</div><div>Предмет ФИС: " + element.getSubjectName() + "</div>Вид ВИ ФИС: " + element.getEntranceTestType()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.EntranceTestItems node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.EntranceTestItems.EntranceTestItem element : node.getEntranceTestItem()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>КГ ФИС: " + element.getCompetitiveGroupName() + "</div><div>Предмет ФИС: " + element.getSubjectName() + "</div>Вид ВИ ФИС: " + element.getEntranceTestType()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.EntranceTestResults node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.EntranceTestResults.EntranceTestResult element : node.getEntranceTestResult()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Предмет ФИС: " + element.getSubjectName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.MedicalDocuments node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.MedicalDocuments.MedicalDocument element : node.getMedicalDocument()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Документ ФИС: " + element.getDocumentNumber()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.OlympicDocuments node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.OlympicDocuments.OlympicDocument element : node.getOlympicDocument()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Документ ФИС: " + element.getDocumentNumber()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.OlympicTotalDocuments node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.OlympicTotalDocuments.OlympicTotalDocument element : node.getOlympicTotalDocument()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>Заявление ФИС: " + element.getApplicationNumber() + "</div>Документ ФИС: " + element.getDocumentNumber()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.OrdersOfAdmissions node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.OrdersOfAdmissions.OrdersOfAdmission element : node.getOrdersOfAdmission()) {
                list.add(wrapper(element.getErrorInfo(),
                    "Приказ ФИС: " + element.getOrderName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.TargetOrganizationDirections node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.TargetOrganizationDirections.TargetOrganizationDirection element : node.getTargetOrganizationDirection()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>КГ ФИС: " + element.getCompetitiveGroupName() + "</div><div>Направление ФИС: " + element.getDirectionName() + "</div><div>Уровень образования ФИС: " + element.getEducationLevelName() + "</div>Организация ЦП ФИС: " + element.getTargetOrganizationName()
                ));
            }
            return list;
        }

        public static Collection<? extends FailureWrapper> initFrom(ImportResultPackage.Log.Failed.TargetOrganizations node)
        {
            List<FailureWrapper> list = new ArrayList<>();
            for (ImportResultPackage.Log.Failed.TargetOrganizations.TargetOrganization element : node.getTargetOrganization()) {
                list.add(wrapper(element.getErrorInfo(),
                    "<div>КГ ФИС: " + element.getCompetitiveGroupName() + "</div>Организация ЦП ФИС: " + element.getTargetOrganizationName()
                ));
            }
            return list;
        }

    }

}

