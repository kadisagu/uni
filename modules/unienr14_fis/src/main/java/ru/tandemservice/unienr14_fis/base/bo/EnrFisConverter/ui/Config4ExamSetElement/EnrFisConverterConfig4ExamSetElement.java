/**
 *$Id: EnrFisConverterConfig4ExamSetElement.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4ExamSetElement;

import com.google.common.collect.Collections2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.discipline.IEnrFisConverterDao4ExamSetElement;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 18.07.13
 */
@Configuration
public class EnrFisConverterConfig4ExamSetElement extends EnrFisConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    protected IEnrFisConverterDao4ExamSetElement getConverterDao() {
        return EnrFisConverterManager.instance().examSetElement();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
        .addColumn(textColumn("shortTitle", IEnrExamSetElementValue.P_SHORT_TITLE).create())
        .addColumn(textColumn("fullTitle", IEnrExamSetElementValue.P_TITLE).create())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4ExamSetElementEdit").hasBlockHeader(true).create())
        .create();
    }


    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        final AbstractSearchDataSourceHandler handler = (AbstractSearchDataSourceHandler) super.fisConverterRecordDSHandler();
        return handler.setPageable(false);
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrollmentCampaign = context.get(BIND_ENROLLMENT_CAMPAIGN);
        if (null == enrollmentCampaign) { return new ArrayList<>(0); }

        // фильтруем по селектору ПК
        return new ArrayList<>(Collections2.filter(
            entityList,
            input -> {
                if (!(input instanceof IEnrExamSetElementValue)) { return false; }
                final IEnrExamSetElementValue discipline = (IEnrExamSetElementValue)input;
                return enrollmentCampaign.equals((EnrEnrollmentCampaign)discipline.getProperty("enrollmentCampaign"));
            }
        ));
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return super.fisConverterValueDSHandler();
    }
}
