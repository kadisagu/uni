/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4EntrantAchievementType;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author Alexey Lopatin
 * @since 09.06.2015
 */
public class EnrFisConverterConfig4EntrantAchievementTypeUI extends EnrFisConverterConfigUI
{
    private EnrEnrollmentCampaign _enrCampaign;
    public boolean _withoutFisCatalogs;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisConverterConfig4EntrantAchievementType.BIND_ENROLLMENT_CAMPAIGN, _enrCampaign);
        dataSource.put(EnrFisConverterConfig4EntrantAchievementType.BIND_ITEM_TITLE, getSettings().get("itemTitle"));
    }

    public void onRefreshSelectors()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    // Getters & Setters

    public boolean isEmptySelectors()
    {
        return _enrCampaign == null;
    }

    // Accessors

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }

    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }
}
