/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.region;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author nvankov
 * @since 04.07.2016
 */
public interface IEnrFisConverterDao4Region extends IEnrFisConverterDao<AddressItem>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync();
}
