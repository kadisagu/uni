package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.*;

/**
 * Пакет 1: Приемная кампания
 *
 * Пакет 1: Приемная кампания
 * Root / PackageData / CampaignInfo / Campaigns / Campaign
 */
public class EnrFisSyncPackage4CampaignInfo extends EnrFisSyncPackage4CampaignInfoGen
{
    @Override public String getTypeTitle() {
        return "Пакет 1: Приемная кампания";
    }
    @Override public String getTypeNumber() {
        return "1";
    }
}