package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import com.google.common.base.Strings;
import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.zip.GZipCompressionInterface;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisEntrantSyncDataGen;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

/**
 * Данные абитуриента в ФИС
 *
 * Хранит данные заявления абитуриента (последнее состояние, на основе которого был сформирован какой-либо пакет).
 */
public class EnrFisEntrantSyncData extends EnrFisEntrantSyncDataGen implements IEntityDebugTitled
{
    public static final GZipCompressionInterface zip = EnrFisSyncSession.zip;

    public EnrFisEntrantSyncData() {}
    public EnrFisEntrantSyncData(EnrEntrant entrant, String uid) {
        this.setEntrant(entrant);
        this.setUid(uid);
    }


    public byte[] getXmlPackage() {
        return zip.decompress(this.getZipXmlPackage());
    }
    public void setXmlPackage(byte[] xmlPackage) {
        this.setZipXmlPackage(zip.compress(xmlPackage));
    }

    public String getXmlPackageString() {
        final byte[] xmlPackage = getXmlPackage();
        return (null == xmlPackage ? null : new String(xmlPackage));
    }
    public void setXmlPackageString(String xmlPackage) {
        this.setXmlPackage(xmlPackage.getBytes());
    }

    public String getFormattedXmlPackage() {
        String xmlPackage = getXmlPackageString();
        return Strings.isNullOrEmpty(xmlPackage) ? null : EnrFisServiceImpl.prettyFormat(xmlPackage);
    }

    public boolean isSuccessfulImportNullSafe() {
        return Boolean.TRUE.equals(getSuccessfulImport());
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getEntrant().getFio() + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getSyncCompleteDate());
    }
}