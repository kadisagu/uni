package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSettingsStateExamMinMark

		// создана новая сущность
		{
            if(!tool.tableExists("enr14fis_s_st_exam_min_mark_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("enr14fis_s_st_exam_min_mark_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("minmark_p", DBType.INTEGER).setNullable(false),
                        new DBColumn("eduprogramsubject_id", DBType.LONG).setNullable(false),
                        new DBColumn("discipline_id", DBType.LONG),
                        new DBColumn("stateexamsubject_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrFisSettingsStateExamMinMark");

		}


    }
}