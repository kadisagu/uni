/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkNewAddEdit;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 11.07.2016
 */
@Configuration
public class EnrFisSettingsStateExamMinMarkNewAddEdit extends BusinessComponentManager
{
    public static final String EDU_PROGRAM_KIND_DS = "eduProgramKindDS";
    public static final String EDU_PROGRAM_SUBJECT_DS = "eduProgramSubjectDS";
    public static final String EDU_PROGRAM_FORM_DS = "eduProgramFormDS";
    public static final String PROGRAM_SET_DS = "programSetDS";
    public static final String BENEFIT_TYPE_DS = "benefitTypeDS";
    public static final String STATE_EXAM_SUBJECT_DS = "stateExamSubjectDS";

    public static final String BIND_ENR_CAMPAIGN_ID = EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID;
    public static final String BIND_EDU_PROGRAM_KIND_ID = "eduProgramKindId";
    public static final String BIND_EDU_PROGRAM_SUBJECT_ID = "eduProgramSubjectId";
    public static final String BIND_EDU_PROGRAM_FORM_ID = "eduProgramFormId";
    public static final String BIND_PROGRAM_SET_ID = "programSetId";
    public static final String BIND_SELECTED_SUBJECT_ID = "selectedSubjectId";


    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PROGRAM_KIND_DS, eduProgramKindDSHandler()).addColumn(EduProgramKind.title().s()))
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_DS, eduProgramSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCodeIndexAndGen().s()))
                .addDataSource(selectDS(EDU_PROGRAM_FORM_DS, eduProgramFormDSHandler()))
                .addDataSource(selectDS(PROGRAM_SET_DS, programSetDSHandler()))
                .addDataSource(selectDS(BENEFIT_TYPE_DS, benefitTypeDSHandler()))
                .addDataSource(selectDS(STATE_EXAM_SUBJECT_DS, stateExamSubjectDSHandler()).addColumn(EnrFisCatalogItem.titleWithRemovalDate().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramKindDSHandler()
    {
        return EduProgramKind.defaultSelectDSHandler(getName())
                .customize(((alias, dql, context, filter) ->
                {
                    Long enrCampaignId = context.getNotNull(BIND_ENR_CAMPAIGN_ID);
                    Long eduProgramSubjectId = context.get(BIND_EDU_PROGRAM_SUBJECT_ID);
                    Long eduProgramFormId = context.get(BIND_EDU_PROGRAM_FORM_ID);
                    Long programSetId = context.get(BIND_PROGRAM_SET_ID);

                    DQLSelectBuilder existBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps")
                            .where(eq(property("ps", EnrProgramSetBase.enrollmentCampaign().id()), value(enrCampaignId)))
                            .where(eq(property("ps", EnrProgramSetBase.programSubject().subjectIndex().programKind()), property(alias)));

                    if(eduProgramSubjectId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programSubject().id()), value(eduProgramSubjectId)));
                    }

                    if(eduProgramFormId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programForm().id()), value(eduProgramFormId)));
                    }

                    if(programSetId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.id()), value(programSetId)));
                    }


                    return dql.where(exists(existBuilder.buildQuery()));
                }))
                ;
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectDSHandler()
    {
        return EduProgramSubject.defaultSelectDSHandler(getName())
                .customize(((alias, dql, context, filter) ->
                {
                    Long enrCampaignId = context.getNotNull(BIND_ENR_CAMPAIGN_ID);
                    Long eduProgramKindId = context.get(BIND_EDU_PROGRAM_KIND_ID);
                    Long eduProgramFormId = context.get(BIND_EDU_PROGRAM_FORM_ID);
                    Long programSetId = context.get(BIND_PROGRAM_SET_ID);

                    DQLSelectBuilder existBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps")
                            .where(eq(property("ps", EnrProgramSetBase.enrollmentCampaign().id()), value(enrCampaignId)))
                            .where(eq(property("ps", EnrProgramSetBase.programSubject()), property(alias)));

                    if(eduProgramKindId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programSubject().subjectIndex().programKind().id()), value(eduProgramKindId)));
                    }

                    if(eduProgramFormId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programForm().id()), value(eduProgramFormId)));
                    }

                    if(programSetId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.id()), value(programSetId)));
                    }


                    return dql.where(exists(existBuilder.buildQuery()));
                }));
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return EduProgramForm.defaultSelectDSHandler(getName())
                .customize(((alias, dql, context, filter) ->
                {
                    Long enrCampaignId = context.getNotNull(BIND_ENR_CAMPAIGN_ID);
                    Long eduProgramKindId = context.get(BIND_EDU_PROGRAM_KIND_ID);
                    Long eduProgramSubjectId = context.get(BIND_EDU_PROGRAM_SUBJECT_ID);
                    Long programSetId = context.get(BIND_PROGRAM_SET_ID);

                    DQLSelectBuilder existBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps")
                            .where(eq(property("ps", EnrProgramSetBase.enrollmentCampaign().id()), value(enrCampaignId)))
                            .where(eq(property("ps", EnrProgramSetBase.programForm()), property(alias)));

                    if(eduProgramKindId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programSubject().subjectIndex().programKind().id()), value(eduProgramKindId)));
                    }

                    if(eduProgramSubjectId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programSubject().id()), value(eduProgramSubjectId)));
                    }

                    if(programSetId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.id()), value(programSetId)));
                    }


                    return dql.where(exists(existBuilder.buildQuery()));
                }));
    }

    @Bean
    public IDefaultComboDataSourceHandler programSetDSHandler()
    {
        return EnrProgramSetBase.defaultSelectDSHandler(getName())
                .customize(((alias, dql, context, filter) ->
                {
                    Long enrCampaignId = context.getNotNull(BIND_ENR_CAMPAIGN_ID);
                    Long eduProgramKindId = context.get(BIND_EDU_PROGRAM_KIND_ID);
                    Long eduProgramSubjectId = context.get(BIND_EDU_PROGRAM_SUBJECT_ID);
                    Long eduProgramFormId = context.get(BIND_EDU_PROGRAM_FORM_ID);

                    DQLSelectBuilder existBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps")
                            .where(eq(property("ps", EnrProgramSetBase.enrollmentCampaign().id()), value(enrCampaignId)))
                            .where(eq(property("ps"), property(alias)));

                    if(eduProgramKindId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programSubject().subjectIndex().programKind().id()), value(eduProgramKindId)));
                    }

                    if(eduProgramSubjectId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programSubject().id()), value(eduProgramSubjectId)));
                    }

                    if(eduProgramFormId != null)
                    {
                        existBuilder.where(eq(property("ps", EnrProgramSetBase.programForm().id()), value(eduProgramFormId)));
                    }


                    return dql.where(exists(existBuilder.buildQuery()));
                }));
    }

    @Bean
    public IDefaultComboDataSourceHandler benefitTypeDSHandler()
    {
        return EnrBenefitType.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) ->
                        dql.where(in(property(alias, EnrBenefitType.code()),
                                Lists.newArrayList(EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS, EnrBenefitTypeCodes.MAX_EXAM_RESULT))));
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamSubjectDSHandler()
    {
        return EnrFisCatalogItem.defaultSelectDSHandler(getName()).customize((alias, dql, context, filter) ->
        {
            Long selectedSubjectId = context.get(BIND_SELECTED_SUBJECT_ID);
            dql.where(eq(property(alias, EnrFisCatalogItem.fisCatalogCode()), value("1")));
            if(selectedSubjectId != null)
            {
                dql.where(or(
                        isNull(property(alias, EnrFisCatalogItem.removalDate())),
                        eq(property(alias, EnrFisCatalogItem.id()), value(selectedSubjectId))
                ));
            }
            else
            {
                dql.where(isNull(property(alias, EnrFisCatalogItem.removalDate())));
            }

            return dql;
        });
    }
}
