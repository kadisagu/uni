package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.country;


import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;


/**
 * @author vdanilov
 */
public interface IEnrFisConverterDao4Country extends IEnrFisConverterDao<AddressCountry> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAutoSync();

}
