/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.OrgUnitCredentialsEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * @author oleyba
 * @since 6/2/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnit.id")
})
public class EnrFisSettingsOrgUnitCredentialsEditUI extends UIPresenter
{
    private EnrOrgUnit orgUnit = new EnrOrgUnit();

    @Override
    public void onComponentRefresh() {
        setOrgUnit(IUniBaseDao.instance.get().getNotNull(EnrOrgUnit.class, getOrgUnit().getId()));
    }

    public void onClickApply() {
        IUniBaseDao.instance.get().update(getOrgUnit());
        deactivate();
    }

    // getters and setters


    public EnrOrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }
}