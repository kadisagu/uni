/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4OlympProfileDiscipline;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;

import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadProfileDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympProfileDiscipline.IEnrFisConverterDao4OlympProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 16.08.2016
 */
@Configuration
public class EnrFisConverterConfig4OlympProfileDiscipline extends EnrFisConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    protected IEnrFisConverterDao4OlympProfileDiscipline getConverterDao() {
        return EnrFisConverterManager.instance().profileDiscipline();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
                .addColumn(textColumn("title", EnrOlympiadProfileDiscipline.P_TITLE).create())
                .addColumn(blockColumn("value", "valueBlock").width("460px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4OlympiadProfDiscEdit").hasBlockHeader(true).create())
                .create();
    }


    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        final AbstractSearchDataSourceHandler handler = (AbstractSearchDataSourceHandler) super.fisConverterRecordDSHandler();
        return handler.setPageable(false);
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        return entityList;
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return super.fisConverterValueDSHandler();
    }
}