/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.StateExamMinMarksDSHandler;
import ru.tandemservice.unienr14_fis.base.ext.OrgUnit.ui.View.OrgUnitViewExt;

/**
 * @author nvankov
 * @since 7/9/14
 */
@Configuration
public class EnrFisSettingsStateExamMinMarkList extends BusinessComponentManager
{
    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrCampaign";
    public static final String STATE_EXAM_MIN_MARKS_DS = "stateExamMinMarksDS";
    @Bean
    public ColumnListExtPoint stateExamMinMarksCL() {
        return columnListExtPointBuilder(STATE_EXAM_MIN_MARKS_DS)
                .addColumn(textColumn("subject", EnrFisSettingsStateExamMinMark.eduProgramSubject().titleWithCode()))
                .addColumn(textColumn("exclusive", "exclusive"))
                .addColumn(textColumn("discipline", EnrFisSettingsStateExamMinMark.discipline().title()))
                .addColumn(textColumn("stateExam", EnrFisSettingsStateExamMinMark.stateExamSubject().title()))
                .addColumn(textColumn("minMark", EnrFisSettingsStateExamMinMark.minMark()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(true).permissionKey("stateExamMinMarkListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(alert("stateExamMinMarksDS.delete.alert")).disabled(true)
                        .permissionKey("stateExamMinMarkListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(STATE_EXAM_MIN_MARKS_DS, stateExamMinMarksCL(), stateExamMinMarksDS()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler stateExamMinMarksDS()
    {
        return new StateExamMinMarksDSHandler(getName());
    }
}



    