/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkNewList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.StateExamMinMarksDSHandler;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.StateExamMinMarksNewDSHandler;

/**
 * @author nvankov
 * @since 7/9/14
 */
@Configuration
public class EnrFisSettingsStateExamMinMarkNewList extends BusinessComponentManager
{
    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrCampaign";
    public static final String STATE_EXAM_MIN_MARKS_DS = "stateExamMinMarksDS";
    @Bean
    public ColumnListExtPoint stateExamMinMarksCL() {
        return columnListExtPointBuilder(STATE_EXAM_MIN_MARKS_DS)
                .addColumn(textColumn("programSet", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase().title()))
                .addColumn(textColumn("programSubject", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase().programSubject().titleWithCode()))
                .addColumn(textColumn("programForm", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase().programForm().title()))
                .addColumn(textColumn("programKind", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase().programSubject().subjectIndex().programKind().title()))
                .addColumn(textColumn("benefitType", EnrFisSettingsStateExamMinMarkNew.benefitType().shortTitle()))
                .addColumn(textColumn("subject", EnrFisSettingsStateExamMinMarkNew.subject().titleWithRemovalDate()))
                .addColumn(textColumn("minMark", EnrFisSettingsStateExamMinMarkNew.minMark()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("stateExamMinMarkListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(alert("stateExamMinMarksDS.delete.alert"))
                        .permissionKey("stateExamMinMarkListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(STATE_EXAM_MIN_MARKS_DS, stateExamMinMarksCL(), stateExamMinMarksDS()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler stateExamMinMarksDS()
    {
        return new StateExamMinMarksNewDSHandler(getName());
    }
}



    