package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.*;

/**
 * Пакет 2: Сведения об объеме и структуре приема
 *
 * Пакет 2: Сведения об объеме и структуре приема
 * Root / PackageData / AdmissionInfo
 */
public class EnrFisSyncPackage4AdmissionInfo extends EnrFisSyncPackage4AdmissionInfoGen
{
    @Override public String getTypeTitle() {
        return "Пакет 2: Сведения об объеме и структуре приема";
    }
    @Override public String getTypeNumber() {
        return "2";
    }
}