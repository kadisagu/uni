package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;


/**
 * @author vdanilov
 */
public class EnrFisOrgUnitSyncSessionDSHandler extends AbstractReadAggregateHandler<DSInput, DSOutput> {

    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrCampaign";
    public static final String PARAM_ORG_UNIT = "orgUnit";

    public EnrFisOrgUnitSyncSessionDSHandler(final String ownerId) {
        super(ownerId);
    }


    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrFisSyncSession.class, "e").column(property("e"))
            .order(property(EnrFisSyncSession.creationDate().fromAlias("e")), OrderDirection.desc);

        List<EnrOrgUnit> enrOrgUnits = IUniBaseDao.instance.get().<EnrOrgUnit>getList(new DQLSelectBuilder()
            .fromEntity(EnrOrgUnit.class, "o").column("o")
            .where(eq(property("o", EnrOrgUnit.enrollmentCampaign()), commonValue(context.get(PARAM_ENROLLMENT_CAMPAIGN))))
            .where(eq(property("o", EnrOrgUnit.institutionOrgUnit().orgUnit()), commonValue(context.get(PARAM_ORG_UNIT))))
        );
        if (enrOrgUnits.isEmpty()) {
            dql.where(isNull("e.id"));
        }
        dql.where(in(property("e", EnrFisSyncSession.enrOrgUnit()), enrOrgUnits));

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();

        final List<DataWrapper> wrappers = DataWrapper.wrap(output);

        final List<EnrFisSyncPackageIOLog> rows = new DQLSelectBuilder()
            .fromEntity(EnrFisSyncPackageIOLog.class, "x").column("x")
            .fetchPath(DQLJoinType.inner, EnrFisSyncPackageIOLog.owner().fromAlias("x"), "o", true)
            .where(in(property(EnrFisSyncPackageIOLog.owner().session().id().fromAlias("x")), CommonDAO.ids(wrappers)))
            .order(property(EnrFisSyncPackageIOLog.operationDate().fromAlias("x")), OrderDirection.desc)
            .order(property(EnrFisSyncPackageIOLog.id().fromAlias("x")), OrderDirection.desc)
            .createStatement(context.getSession()).list();

        final Map<Long, DataWrapper> map = CommonDAO.map(wrappers);
        for (final EnrFisSyncPackageIOLog row: rows) {
            final DataWrapper wrapper = map.get(row.getOwner().getSession().getId());
            if (null == wrapper) { continue; }

            List<String> log = (List<String>)wrapper.getProperty("iolog");
            if (null == log) { wrapper.setProperty("iolog", log = new ArrayList<String>()); }
            log.add(row.getTitleWithPackage());
        }

        for (final DataWrapper wrapper: wrappers) {
            final List<String> log = (List<String>)wrapper.getProperty("iolog");
            if (null == log) { continue; }

            wrapper.setProperty("iologSize", log.size());

            if (log.size() > 4) {
                wrapper.setProperty("iolog", StringUtils.join(log.subList(0, 4), "\n")+"\n... еще записей " + (log.size()-4));
            } else {
                wrapper.setProperty("iolog", StringUtils.join(log, "\n"));
            }
        }

        return output;
    }

}
