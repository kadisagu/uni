package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Минимальный балл ЕГЭ для предоставления особых прав
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSettingsStateExamMinMarkNewGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew";
    public static final String ENTITY_NAME = "enrFisSettingsStateExamMinMarkNew";
    public static final int VERSION_HASH = 252751420;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_PROGRAM_SET_BASE = "enrProgramSetBase";
    public static final String L_SUBJECT = "subject";
    public static final String L_BENEFIT_TYPE = "benefitType";
    public static final String P_MIN_MARK = "minMark";

    private EnrProgramSetBase _enrProgramSetBase;     // Набор ОП
    private EnrFisCatalogItem _subject;     // Общеобразовательный предмет ФИС
    private EnrBenefitType _benefitType;     // Вид особого права
    private int _minMark;     // Минимальный балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetBase getEnrProgramSetBase()
    {
        return _enrProgramSetBase;
    }

    /**
     * @param enrProgramSetBase Набор ОП. Свойство не может быть null.
     */
    public void setEnrProgramSetBase(EnrProgramSetBase enrProgramSetBase)
    {
        dirty(_enrProgramSetBase, enrProgramSetBase);
        _enrProgramSetBase = enrProgramSetBase;
    }

    /**
     * @return Общеобразовательный предмет ФИС. Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Общеобразовательный предмет ФИС. Свойство не может быть null.
     */
    public void setSubject(EnrFisCatalogItem subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Вид особого права. Свойство не может быть null.
     */
    @NotNull
    public EnrBenefitType getBenefitType()
    {
        return _benefitType;
    }

    /**
     * @param benefitType Вид особого права. Свойство не может быть null.
     */
    public void setBenefitType(EnrBenefitType benefitType)
    {
        dirty(_benefitType, benefitType);
        _benefitType = benefitType;
    }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     */
    @NotNull
    public int getMinMark()
    {
        return _minMark;
    }

    /**
     * @param minMark Минимальный балл. Свойство не может быть null.
     */
    public void setMinMark(int minMark)
    {
        dirty(_minMark, minMark);
        _minMark = minMark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisSettingsStateExamMinMarkNewGen)
        {
            setEnrProgramSetBase(((EnrFisSettingsStateExamMinMarkNew)another).getEnrProgramSetBase());
            setSubject(((EnrFisSettingsStateExamMinMarkNew)another).getSubject());
            setBenefitType(((EnrFisSettingsStateExamMinMarkNew)another).getBenefitType());
            setMinMark(((EnrFisSettingsStateExamMinMarkNew)another).getMinMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSettingsStateExamMinMarkNewGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSettingsStateExamMinMarkNew.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSettingsStateExamMinMarkNew();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrProgramSetBase":
                    return obj.getEnrProgramSetBase();
                case "subject":
                    return obj.getSubject();
                case "benefitType":
                    return obj.getBenefitType();
                case "minMark":
                    return obj.getMinMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrProgramSetBase":
                    obj.setEnrProgramSetBase((EnrProgramSetBase) value);
                    return;
                case "subject":
                    obj.setSubject((EnrFisCatalogItem) value);
                    return;
                case "benefitType":
                    obj.setBenefitType((EnrBenefitType) value);
                    return;
                case "minMark":
                    obj.setMinMark((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrProgramSetBase":
                        return true;
                case "subject":
                        return true;
                case "benefitType":
                        return true;
                case "minMark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrProgramSetBase":
                    return true;
                case "subject":
                    return true;
                case "benefitType":
                    return true;
                case "minMark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrProgramSetBase":
                    return EnrProgramSetBase.class;
                case "subject":
                    return EnrFisCatalogItem.class;
                case "benefitType":
                    return EnrBenefitType.class;
                case "minMark":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSettingsStateExamMinMarkNew> _dslPath = new Path<EnrFisSettingsStateExamMinMarkNew>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSettingsStateExamMinMarkNew");
    }
            

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getEnrProgramSetBase()
     */
    public static EnrProgramSetBase.Path<EnrProgramSetBase> enrProgramSetBase()
    {
        return _dslPath.enrProgramSetBase();
    }

    /**
     * @return Общеобразовательный предмет ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getSubject()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Вид особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getBenefitType()
     */
    public static EnrBenefitType.Path<EnrBenefitType> benefitType()
    {
        return _dslPath.benefitType();
    }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getMinMark()
     */
    public static PropertyPath<Integer> minMark()
    {
        return _dslPath.minMark();
    }

    public static class Path<E extends EnrFisSettingsStateExamMinMarkNew> extends EntityPath<E>
    {
        private EnrProgramSetBase.Path<EnrProgramSetBase> _enrProgramSetBase;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _subject;
        private EnrBenefitType.Path<EnrBenefitType> _benefitType;
        private PropertyPath<Integer> _minMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getEnrProgramSetBase()
     */
        public EnrProgramSetBase.Path<EnrProgramSetBase> enrProgramSetBase()
        {
            if(_enrProgramSetBase == null )
                _enrProgramSetBase = new EnrProgramSetBase.Path<EnrProgramSetBase>(L_ENR_PROGRAM_SET_BASE, this);
            return _enrProgramSetBase;
        }

    /**
     * @return Общеобразовательный предмет ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getSubject()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> subject()
        {
            if(_subject == null )
                _subject = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Вид особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getBenefitType()
     */
        public EnrBenefitType.Path<EnrBenefitType> benefitType()
        {
            if(_benefitType == null )
                _benefitType = new EnrBenefitType.Path<EnrBenefitType>(L_BENEFIT_TYPE, this);
            return _benefitType;
        }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew#getMinMark()
     */
        public PropertyPath<Integer> minMark()
        {
            if(_minMark == null )
                _minMark = new PropertyPath<Integer>(EnrFisSettingsStateExamMinMarkNewGen.P_MIN_MARK, this);
            return _minMark;
        }

        public Class getEntityClass()
        {
            return EnrFisSettingsStateExamMinMarkNew.class;
        }

        public String getEntityName()
        {
            return "enrFisSettingsStateExamMinMarkNew";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
