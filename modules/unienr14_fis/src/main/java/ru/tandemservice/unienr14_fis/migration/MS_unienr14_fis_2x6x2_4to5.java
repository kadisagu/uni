package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackage4ApplicationsInfo

		// удалено свойство skippedEntrantCount
		{
			tool.dropColumn("enr14fis_package_3apps_t", "skippedentrantcount_p");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncSession

		// создано обязательное свойство skippedEntrantCount
		{
			tool.createColumn("enr14fis_session_t", new DBColumn("skippedentrantcount_p", DBType.INTEGER));
            tool.executeUpdate("update enr14fis_session_t set skippedentrantcount_p=? where skippedentrantcount_p is null", (Integer) 0);
			tool.setColumnNullable("enr14fis_session_t", "skippedentrantcount_p", false);
		}
    }
}