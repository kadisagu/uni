/**
 *$Id: EnrFisConverterDao4Olympiad.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiad;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
public class EnrFisConverterDao4Olympiad extends EnrFisConverterBaseDao<EnrOlympiad, Long> implements IEnrFisConverterDao4Olympiad
{
    @Override
    public String getCatalogCode()
    {
        return "19";
    }

    @Override
    protected List<EnrOlympiad> getEntityList()
    {
        return new DQLSelectBuilder().fromEntity(EnrOlympiad.class, "x").column(property("x"))
                .order(property(EnrOlympiad.eduYear().intValue().fromAlias("x")))
                .order(property(EnrOlympiad.title().fromAlias("x")))
                .createStatement(getSession()).list();
    }

    @Override
    protected Long getItemMapKey(EnrOlympiad entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4Olympiad.class, "x")
                        .column(property(EnrFisConv4Olympiad.olympiad().id().fromAlias("x")), "oly_id")
                        .column(property(EnrFisConv4Olympiad.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())));
    }

    @Override
    public void update(final Map<EnrOlympiad, EnrFisCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EnrFisConv4Olympiad> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<EnrOlympiad, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4Olympiad(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4Olympiad.NaturalId, EnrFisConv4Olympiad>() {
            @Override protected EnrFisConv4Olympiad.NaturalId key(final EnrFisConv4Olympiad source) {
                return (EnrFisConv4Olympiad.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4Olympiad buildRow(final EnrFisConv4Olympiad source) {
                return new EnrFisConv4Olympiad(source.getOlympiad(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4Olympiad target, final EnrFisConv4Olympiad source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4Olympiad databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4Olympiad.class), targetRecords);

    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4Olympiad.class, "b").column(property(EnrFisConv4Olympiad.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4Olympiad.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync(EducationYear eduYear)
    {
        // поднимаем Олимпиады, для которых нет сопоставления с Элементом справочника ФИС
        final DQLSelectBuilder disciplineDQL = new DQLSelectBuilder().fromEntity(EnrOlympiad.class, "o").column(property("o"))
                .where(notIn(property(EnrOlympiad.id().fromAlias("o")),
                        new DQLSelectBuilder().fromEntity(EnrFisConv4Olympiad.class, "c").column(property(EnrFisConv4Olympiad.olympiad().id().fromAlias("c")))
                                .buildQuery()));
        if (eduYear != null)
            disciplineDQL.where(eq(property(EnrOlympiad.eduYear().fromAlias("o")), value(eduYear)));

        // поднимаем Олимпиады ФИС
        final DQLSelectBuilder fisCatalogDQL = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "f").column(property("f"))
                .where(isNull(property("f", EnrFisCatalogItem.removalDate())))
                .where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("f")), value(getCatalogCode())));

        // строим мап Названий олимпиад ФИС - Олимпиада ФИС
        // если навзнание не уникальное в Справочниках ФИС, убираем такие Элементы ФИС(Олимпиады) из обработки
        final List<String> doubleTitleList = new ArrayList<>();
        final Map<String, EnrFisCatalogItem> fisCatalogItemtTitleMap = new HashMap<>();
        for (EnrFisCatalogItem item : fisCatalogDQL.createStatement(getSession()).<EnrFisCatalogItem>list())
        {
            // в названии префиксом являются Номер и Уровень олимпиады, поэтому отсекаем их и берем только Название(пример: "[4] № 2 Байкальская олимпиада школьников" -> "Байкальская олимпиада школьников")
            String title = item.getTitle().trim();
            String[] split = title.split(" ");

            String year = split[0];
            if (eduYear != null && !year.contains(String.valueOf(eduYear.getEducationYear().getIntValue())))
                continue;

            title = title.replace(split[0], "").replace(split[1], "").replace(split[2], "").trim().toUpperCase();

            if (doubleTitleList.contains(title))
                continue;

            if (fisCatalogItemtTitleMap.put(title, item) != null)
            {
                fisCatalogItemtTitleMap.remove(title);
                doubleTitleList.add(title);
            }
        }

        final Map<EnrOlympiad, EnrFisCatalogItem> values = new HashMap<>();
        for (EnrOlympiad olympiad : disciplineDQL.createStatement(getSession()).<EnrOlympiad>list())
        {
            EnrFisCatalogItem item = fisCatalogItemtTitleMap.get(olympiad.getTitle().toUpperCase());
            if (null == item) { continue; }

            values.put(olympiad, item);
        }

        // для тех для которых уже выбрали - не меняем
        for (EnrFisConv4Olympiad discipline : getList(EnrFisConv4Olympiad.class))
            values.put(discipline.getOlympiad(), discipline.getValue());

        this.update(values, false);
    }
}
