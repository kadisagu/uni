/* $Id$ */
package ru.tandemservice.unienr14_fis.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import ru.tandemservice.unienr14_fis.base.ext.Depersonalization.objects.Unienr14fisClearBlobsDO;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;

import static org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.DepersonalizationUtils.ReplaceFileType.gz;

/**
 * @author Nikolay Fedorovskih
 * @since 14.11.2014
 */
@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(_depersonalizationManager.updateBuilder(EnrFisEntrantSyncData.class).replaceWithTemplateDifferentInstances(EnrFisEntrantSyncData.P_ZIP_XML_PACKAGE, gz))
                .add(_depersonalizationManager.updateBuilder(EnrFisSyncPackage.class).replaceWithTemplateDifferentInstances(EnrFisSyncPackage.P_ZIP_XML_PACKAGE, gz))
                .add(Unienr14fisClearBlobsDO.class)
                .build();
    }
}