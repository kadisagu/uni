package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author vdanilov
 */
public interface IEnrFisOrgUnitDao extends INeedPersistenceSupport {

    /**
     * @param orgUnit
     * @return
     */
    boolean isEnrFisOrgUnit(OrgUnit orgUnit);

}
