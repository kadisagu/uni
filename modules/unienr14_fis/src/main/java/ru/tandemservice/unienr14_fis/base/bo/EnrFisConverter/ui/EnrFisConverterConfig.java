package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic.ItemListDSHandler;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public abstract class EnrFisConverterConfig extends BusinessComponentManager {

    public static final String PARAM_CONVERTER_NAME = "converterName";
    public static final String PROPERTY_FIS_VALUE = "value";

    public static final String FIS_CONVERTER_RECORD_DS = "fisConverterRecordDS";
    public static final String FIS_CONVERTER_VALUE_DS = "fisConverterValueDS";

    protected abstract IEnrFisConverterDao getConverterDao();

    public PresenterExtPoint presenterExtPoint(IPresenterExtPointBuilder builder) {
        return builder
        .addDataSource(this.searchListDS(FIS_CONVERTER_RECORD_DS, this.fisConverterRecordDS()).handler(this.fisConverterRecordDSHandler()))
        .addDataSource(this.selectDS(FIS_CONVERTER_VALUE_DS, this.fisConverterValueDSHandler()))
        .create();
    }

    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
        .addColumn(textColumn("title", "title").create())
        .addColumn(textColumn("value", PROPERTY_FIS_VALUE +".title").create())
        .create();
    }

    protected List<IEntity> filterConverterRecordDS(final List<IEntity> entityList, final ExecutionContext context) {
        return entityList; // ФИЛЬТРЫ ПИШЕМ ТУТ
    }

    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(this.getName()) {
            @Override protected DSOutput execute(final DSInput input, final ExecutionContext context) {
                final IEnrFisConverterDao<IEntity> dao = EnrFisConverterConfig.this.getConverterDao();
                final IEnrFisConverter<IEntity> converter = dao.getConverter();

                final List<IEntity> entityList = EnrFisConverterConfig.this.filterConverterRecordDS(converter.getEntityList(), context);
                final DSOutput output = ListOutputBuilder.get(input, entityList).pageable(isPageable()).build();
                final List<DataWrapper> wrappers = DataWrapper.wrap(output);
                for (final DataWrapper w: wrappers) {
                    final EnrFisCatalogItem fisCatalogItem = converter.getCatalogItem(w.<IEntity>getWrapped(), false);
                    w.setProperty(PROPERTY_FIS_VALUE, fisCatalogItem);
                }

                return output;
            }
        }.setPageable(true);
    }

    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return new ItemListDSHandler(this.getName()) {
            @Override protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                final IEnrFisConverterDao<IEntity> dao = EnrFisConverterConfig.this.getConverterDao();
                dql.where(isNull(property(alias, EnrFisCatalogItem.removalDate())));
                dql.where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias(alias)), value(dao.getCatalogCode())));
                dql.order(property(EnrFisCatalogItem.fisItemCode().fromAlias(alias)));
            }
        }.order(EnrFisCatalogItem.fisCatalogCode()).pageable(true);
    }

}
