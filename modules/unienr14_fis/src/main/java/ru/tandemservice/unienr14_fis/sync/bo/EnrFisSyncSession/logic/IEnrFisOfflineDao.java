/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 25.07.15
 * Time: 1:49
 */
public interface IEnrFisOfflineDao extends INeedPersistenceSupport
{
    void getSessionsXml(EnrEnrollmentCampaign enrCampaign, OrgUnit orgUnit);

    void doImportSessionsXmlResults(IUploadFile file, Long enrCampaignId, Long orgUnitId);
}
