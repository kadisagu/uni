/**
 *$Id: EnrFisConverterDao4DiplomaType.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.diplomaType;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadHonour;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterStaticDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 23.07.13
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15664965")
public class EnrFisConverterDao4DiplomaType extends EnrFisConverterStaticDao<EnrOlympiadHonour, String> implements IEnrFisConverterDao4DiplomaType
{
    /* { uni.code, uni.title, fis.code, fis.title } */
    private static final String[][] DATA = {
        { "1", "победитель", "1", "победитель"},
        { "2", "призер",     "2", "призер"}
    };

    /* проверка того, что в ФИС ничего не поменялось { fis.code -> fis.title } */
    private static final Map<String, String> STATIC_TITLE_CHECK_MAP = new HashMap<String, String>();

    /* сопоставление элементов { uni.code -> fis.code }*/
    private static final Map<String, String> STATIC_ITEM_CODE_MAP = new HashMap<String, String>();

    static
    {
        for (final String[] row: DATA)
        {
            if (null != row[2])
            {
                if (null != STATIC_ITEM_CODE_MAP.put(row[0], row[2]))
                    throw new IllegalStateException("STATIC_ITEM_CODE_MAP: Duplicate value for key=«"+row[0]+"»");

                final String prev = STATIC_TITLE_CHECK_MAP.put(row[2], row[3]);
                if (null != prev && !prev.equals(row[3]))
                    throw new IllegalStateException("COUNTRY_CHECK_MAP: Duplicate value for key=«"+row[2]+"»");
            }
        }
    }

    @Override
    protected Map<String, String> getStaticTitleCheckMap()
    {
        return STATIC_TITLE_CHECK_MAP;
    }

    @Override
    protected Map<String, String> getStaticItemCodeMap()
    {
        return STATIC_ITEM_CODE_MAP;
    }

    @Override
    public String getCatalogCode()
    {
        return "18";
    }

    @Override
    protected List<EnrOlympiadHonour> getEntityList()
    {
        return getCatalogItemList(EnrOlympiadHonour.class);
    }

    @Override
    protected String getItemMapKey(EnrOlympiadHonour entity)
    {
        return entity.getCode();
    }
}
