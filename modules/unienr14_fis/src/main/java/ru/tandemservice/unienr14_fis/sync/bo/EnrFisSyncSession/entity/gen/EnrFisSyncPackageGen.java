package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет синхронизации ФИС
 *
 * Пакет взаимодействия с сервисом экспорта данных в ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSyncPackageGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage";
    public static final String ENTITY_NAME = "enrFisSyncPackage";
    public static final int VERSION_HASH = -383573813;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION = "session";
    public static final String P_COMMENT = "comment";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_NEXT_OPERATION_THRESHOLD_DATE = "nextOperationThresholdDate";
    public static final String P_ZIP_XML_PACKAGE = "zipXmlPackage";
    public static final String P_FIS_PACKAGE_I_D = "fisPackageID";
    public static final String L_PKG_SEND_SUCCESS = "pkgSendSuccess";
    public static final String L_PKG_RECV_SUCCESS = "pkgRecvSuccess";
    public static final String P_FILE_NAME = "fileName";
    public static final String P_NEXT_OPERATION_THRESHOLD_DATE_AS_STRING = "nextOperationThresholdDateAsString";
    public static final String P_PACKAGE_RECV_DATE = "packageRecvDate";
    public static final String P_PACKAGE_SEND_DATE = "packageSendDate";
    public static final String P_STATE_TITLE = "stateTitle";
    public static final String P_TITLE = "title";
    public static final String P_TYPE_NUMBER = "typeNumber";
    public static final String P_TYPE_TITLE = "typeTitle";

    private EnrFisSyncSession _session;     // Сессия синхронизации
    private String _comment;     // Комментарий
    private Date _creationDate;     // Дата формирования пакета
    private Date _nextOperationThresholdDate;     // Ограничение на время проведения следующей операции
    private byte[] _zipXmlPackage;     // ZIP-XML: Пакет
    private Long _fisPackageID;     // Идентификатор пакета в ФИС
    private EnrFisSyncPackageIOLog _pkgSendSuccess;     // Успешная операция передачи данных пакета в ФИС
    private EnrFisSyncPackageIOLog _pkgRecvSuccess;     // Успешная операция получения результатов обработки из ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Сессия синхронизации пакета
     *
     * @return Сессия синхронизации. Свойство не может быть null.
     */
    @NotNull
    public EnrFisSyncSession getSession()
    {
        return _session;
    }

    /**
     * @param session Сессия синхронизации. Свойство не может быть null.
     */
    public void setSession(EnrFisSyncSession session)
    {
        dirty(_session, session);
        _session = session;
    }

    /**
     * Описание пакета для отображения в интерфейсе (генерируется при создании)
     *
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Дата, когда пакет был сформирован в системе
     *
     * @return Дата формирования пакета. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата формирования пакета. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * Если поле заполнено, то следующея операция по пакету пройдет не раньше указанной в поле отметки времени
     *
     * @return Ограничение на время проведения следующей операции.
     */
    public Date getNextOperationThresholdDate()
    {
        return _nextOperationThresholdDate;
    }

    /**
     * @param nextOperationThresholdDate Ограничение на время проведения следующей операции.
     */
    public void setNextOperationThresholdDate(Date nextOperationThresholdDate)
    {
        dirty(_nextOperationThresholdDate, nextOperationThresholdDate);
        _nextOperationThresholdDate = nextOperationThresholdDate;
    }

    /**
     * Содержимое пакета
     *
     * @return ZIP-XML: Пакет. Свойство не может быть null.
     */
    @NotNull
    public byte[] getZipXmlPackage()
    {
        initLazyForGet("zipXmlPackage");
        return _zipXmlPackage;
    }

    /**
     * @param zipXmlPackage ZIP-XML: Пакет. Свойство не может быть null.
     */
    public void setZipXmlPackage(byte[] zipXmlPackage)
    {
        initLazyForSet("zipXmlPackage");
        dirty(_zipXmlPackage, zipXmlPackage);
        _zipXmlPackage = zipXmlPackage;
    }

    /**
     * Идентификатор, который выдается ФИС для загруженного пакета
     * (заполняется на основе данных в pkgImportSuccess)
     *
     * @return Идентификатор пакета в ФИС.
     */
    public Long getFisPackageID()
    {
        return _fisPackageID;
    }

    /**
     * @param fisPackageID Идентификатор пакета в ФИС.
     */
    public void setFisPackageID(Long fisPackageID)
    {
        dirty(_fisPackageID, fisPackageID);
        _fisPackageID = fisPackageID;
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличине этого признака указывает на факт передачи данных в ФИС)
     *
     * @return Успешная операция передачи данных пакета в ФИС.
     */
    public EnrFisSyncPackageIOLog getPkgSendSuccess()
    {
        return _pkgSendSuccess;
    }

    /**
     * @param pkgSendSuccess Успешная операция передачи данных пакета в ФИС.
     */
    public void setPkgSendSuccess(EnrFisSyncPackageIOLog pkgSendSuccess)
    {
        dirty(_pkgSendSuccess, pkgSendSuccess);
        _pkgSendSuccess = pkgSendSuccess;
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличие этого признака указывает на факт получения резулльтатов обработки пакета в ФИС)
     *
     * @return Успешная операция получения результатов обработки из ФИС.
     */
    public EnrFisSyncPackageIOLog getPkgRecvSuccess()
    {
        return _pkgRecvSuccess;
    }

    /**
     * @param pkgRecvSuccess Успешная операция получения результатов обработки из ФИС.
     */
    public void setPkgRecvSuccess(EnrFisSyncPackageIOLog pkgRecvSuccess)
    {
        dirty(_pkgRecvSuccess, pkgRecvSuccess);
        _pkgRecvSuccess = pkgRecvSuccess;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisSyncPackageGen)
        {
            setSession(((EnrFisSyncPackage)another).getSession());
            setComment(((EnrFisSyncPackage)another).getComment());
            setCreationDate(((EnrFisSyncPackage)another).getCreationDate());
            setNextOperationThresholdDate(((EnrFisSyncPackage)another).getNextOperationThresholdDate());
            setZipXmlPackage(((EnrFisSyncPackage)another).getZipXmlPackage());
            setFisPackageID(((EnrFisSyncPackage)another).getFisPackageID());
            setPkgSendSuccess(((EnrFisSyncPackage)another).getPkgSendSuccess());
            setPkgRecvSuccess(((EnrFisSyncPackage)another).getPkgRecvSuccess());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSyncPackageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSyncPackage.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrFisSyncPackage is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "session":
                    return obj.getSession();
                case "comment":
                    return obj.getComment();
                case "creationDate":
                    return obj.getCreationDate();
                case "nextOperationThresholdDate":
                    return obj.getNextOperationThresholdDate();
                case "zipXmlPackage":
                    return obj.getZipXmlPackage();
                case "fisPackageID":
                    return obj.getFisPackageID();
                case "pkgSendSuccess":
                    return obj.getPkgSendSuccess();
                case "pkgRecvSuccess":
                    return obj.getPkgRecvSuccess();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "session":
                    obj.setSession((EnrFisSyncSession) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "nextOperationThresholdDate":
                    obj.setNextOperationThresholdDate((Date) value);
                    return;
                case "zipXmlPackage":
                    obj.setZipXmlPackage((byte[]) value);
                    return;
                case "fisPackageID":
                    obj.setFisPackageID((Long) value);
                    return;
                case "pkgSendSuccess":
                    obj.setPkgSendSuccess((EnrFisSyncPackageIOLog) value);
                    return;
                case "pkgRecvSuccess":
                    obj.setPkgRecvSuccess((EnrFisSyncPackageIOLog) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "session":
                        return true;
                case "comment":
                        return true;
                case "creationDate":
                        return true;
                case "nextOperationThresholdDate":
                        return true;
                case "zipXmlPackage":
                        return true;
                case "fisPackageID":
                        return true;
                case "pkgSendSuccess":
                        return true;
                case "pkgRecvSuccess":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "session":
                    return true;
                case "comment":
                    return true;
                case "creationDate":
                    return true;
                case "nextOperationThresholdDate":
                    return true;
                case "zipXmlPackage":
                    return true;
                case "fisPackageID":
                    return true;
                case "pkgSendSuccess":
                    return true;
                case "pkgRecvSuccess":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "session":
                    return EnrFisSyncSession.class;
                case "comment":
                    return String.class;
                case "creationDate":
                    return Date.class;
                case "nextOperationThresholdDate":
                    return Date.class;
                case "zipXmlPackage":
                    return byte[].class;
                case "fisPackageID":
                    return Long.class;
                case "pkgSendSuccess":
                    return EnrFisSyncPackageIOLog.class;
                case "pkgRecvSuccess":
                    return EnrFisSyncPackageIOLog.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSyncPackage> _dslPath = new Path<EnrFisSyncPackage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSyncPackage");
    }
            

    /**
     * Сессия синхронизации пакета
     *
     * @return Сессия синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getSession()
     */
    public static EnrFisSyncSession.Path<EnrFisSyncSession> session()
    {
        return _dslPath.session();
    }

    /**
     * Описание пакета для отображения в интерфейсе (генерируется при создании)
     *
     * @return Комментарий.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Дата, когда пакет был сформирован в системе
     *
     * @return Дата формирования пакета. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * Если поле заполнено, то следующея операция по пакету пройдет не раньше указанной в поле отметки времени
     *
     * @return Ограничение на время проведения следующей операции.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getNextOperationThresholdDate()
     */
    public static PropertyPath<Date> nextOperationThresholdDate()
    {
        return _dslPath.nextOperationThresholdDate();
    }

    /**
     * Содержимое пакета
     *
     * @return ZIP-XML: Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getZipXmlPackage()
     */
    public static PropertyPath<byte[]> zipXmlPackage()
    {
        return _dslPath.zipXmlPackage();
    }

    /**
     * Идентификатор, который выдается ФИС для загруженного пакета
     * (заполняется на основе данных в pkgImportSuccess)
     *
     * @return Идентификатор пакета в ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getFisPackageID()
     */
    public static PropertyPath<Long> fisPackageID()
    {
        return _dslPath.fisPackageID();
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличине этого признака указывает на факт передачи данных в ФИС)
     *
     * @return Успешная операция передачи данных пакета в ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPkgSendSuccess()
     */
    public static EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog> pkgSendSuccess()
    {
        return _dslPath.pkgSendSuccess();
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличие этого признака указывает на факт получения резулльтатов обработки пакета в ФИС)
     *
     * @return Успешная операция получения результатов обработки из ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPkgRecvSuccess()
     */
    public static EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog> pkgRecvSuccess()
    {
        return _dslPath.pkgRecvSuccess();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getFileName()
     */
    public static SupportedPropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getNextOperationThresholdDateAsString()
     */
    public static SupportedPropertyPath<String> nextOperationThresholdDateAsString()
    {
        return _dslPath.nextOperationThresholdDateAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPackageRecvDate()
     */
    public static SupportedPropertyPath<Date> packageRecvDate()
    {
        return _dslPath.packageRecvDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPackageSendDate()
     */
    public static SupportedPropertyPath<Date> packageSendDate()
    {
        return _dslPath.packageSendDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getStateTitle()
     */
    public static SupportedPropertyPath<String> stateTitle()
    {
        return _dslPath.stateTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getTypeNumber()
     */
    public static SupportedPropertyPath<String> typeNumber()
    {
        return _dslPath.typeNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getTypeTitle()
     */
    public static SupportedPropertyPath<String> typeTitle()
    {
        return _dslPath.typeTitle();
    }

    public static class Path<E extends EnrFisSyncPackage> extends EntityPath<E>
    {
        private EnrFisSyncSession.Path<EnrFisSyncSession> _session;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Date> _nextOperationThresholdDate;
        private PropertyPath<byte[]> _zipXmlPackage;
        private PropertyPath<Long> _fisPackageID;
        private EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog> _pkgSendSuccess;
        private EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog> _pkgRecvSuccess;
        private SupportedPropertyPath<String> _fileName;
        private SupportedPropertyPath<String> _nextOperationThresholdDateAsString;
        private SupportedPropertyPath<Date> _packageRecvDate;
        private SupportedPropertyPath<Date> _packageSendDate;
        private SupportedPropertyPath<String> _stateTitle;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _typeNumber;
        private SupportedPropertyPath<String> _typeTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Сессия синхронизации пакета
     *
     * @return Сессия синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getSession()
     */
        public EnrFisSyncSession.Path<EnrFisSyncSession> session()
        {
            if(_session == null )
                _session = new EnrFisSyncSession.Path<EnrFisSyncSession>(L_SESSION, this);
            return _session;
        }

    /**
     * Описание пакета для отображения в интерфейсе (генерируется при создании)
     *
     * @return Комментарий.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EnrFisSyncPackageGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Дата, когда пакет был сформирован в системе
     *
     * @return Дата формирования пакета. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EnrFisSyncPackageGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * Если поле заполнено, то следующея операция по пакету пройдет не раньше указанной в поле отметки времени
     *
     * @return Ограничение на время проведения следующей операции.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getNextOperationThresholdDate()
     */
        public PropertyPath<Date> nextOperationThresholdDate()
        {
            if(_nextOperationThresholdDate == null )
                _nextOperationThresholdDate = new PropertyPath<Date>(EnrFisSyncPackageGen.P_NEXT_OPERATION_THRESHOLD_DATE, this);
            return _nextOperationThresholdDate;
        }

    /**
     * Содержимое пакета
     *
     * @return ZIP-XML: Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getZipXmlPackage()
     */
        public PropertyPath<byte[]> zipXmlPackage()
        {
            if(_zipXmlPackage == null )
                _zipXmlPackage = new PropertyPath<byte[]>(EnrFisSyncPackageGen.P_ZIP_XML_PACKAGE, this);
            return _zipXmlPackage;
        }

    /**
     * Идентификатор, который выдается ФИС для загруженного пакета
     * (заполняется на основе данных в pkgImportSuccess)
     *
     * @return Идентификатор пакета в ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getFisPackageID()
     */
        public PropertyPath<Long> fisPackageID()
        {
            if(_fisPackageID == null )
                _fisPackageID = new PropertyPath<Long>(EnrFisSyncPackageGen.P_FIS_PACKAGE_I_D, this);
            return _fisPackageID;
        }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличине этого признака указывает на факт передачи данных в ФИС)
     *
     * @return Успешная операция передачи данных пакета в ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPkgSendSuccess()
     */
        public EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog> pkgSendSuccess()
        {
            if(_pkgSendSuccess == null )
                _pkgSendSuccess = new EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog>(L_PKG_SEND_SUCCESS, this);
            return _pkgSendSuccess;
        }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличие этого признака указывает на факт получения резулльтатов обработки пакета в ФИС)
     *
     * @return Успешная операция получения результатов обработки из ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPkgRecvSuccess()
     */
        public EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog> pkgRecvSuccess()
        {
            if(_pkgRecvSuccess == null )
                _pkgRecvSuccess = new EnrFisSyncPackageIOLog.Path<EnrFisSyncPackageIOLog>(L_PKG_RECV_SUCCESS, this);
            return _pkgRecvSuccess;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getFileName()
     */
        public SupportedPropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new SupportedPropertyPath<String>(EnrFisSyncPackageGen.P_FILE_NAME, this);
            return _fileName;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getNextOperationThresholdDateAsString()
     */
        public SupportedPropertyPath<String> nextOperationThresholdDateAsString()
        {
            if(_nextOperationThresholdDateAsString == null )
                _nextOperationThresholdDateAsString = new SupportedPropertyPath<String>(EnrFisSyncPackageGen.P_NEXT_OPERATION_THRESHOLD_DATE_AS_STRING, this);
            return _nextOperationThresholdDateAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPackageRecvDate()
     */
        public SupportedPropertyPath<Date> packageRecvDate()
        {
            if(_packageRecvDate == null )
                _packageRecvDate = new SupportedPropertyPath<Date>(EnrFisSyncPackageGen.P_PACKAGE_RECV_DATE, this);
            return _packageRecvDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getPackageSendDate()
     */
        public SupportedPropertyPath<Date> packageSendDate()
        {
            if(_packageSendDate == null )
                _packageSendDate = new SupportedPropertyPath<Date>(EnrFisSyncPackageGen.P_PACKAGE_SEND_DATE, this);
            return _packageSendDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getStateTitle()
     */
        public SupportedPropertyPath<String> stateTitle()
        {
            if(_stateTitle == null )
                _stateTitle = new SupportedPropertyPath<String>(EnrFisSyncPackageGen.P_STATE_TITLE, this);
            return _stateTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrFisSyncPackageGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getTypeNumber()
     */
        public SupportedPropertyPath<String> typeNumber()
        {
            if(_typeNumber == null )
                _typeNumber = new SupportedPropertyPath<String>(EnrFisSyncPackageGen.P_TYPE_NUMBER, this);
            return _typeNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage#getTypeTitle()
     */
        public SupportedPropertyPath<String> typeTitle()
        {
            if(_typeTitle == null )
                _typeTitle = new SupportedPropertyPath<String>(EnrFisSyncPackageGen.P_TYPE_TITLE, this);
            return _typeTitle;
        }

        public Class getEntityClass()
        {
            return EnrFisSyncPackage.class;
        }

        public String getEntityName()
        {
            return "enrFisSyncPackage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFileName();

    public abstract String getNextOperationThresholdDateAsString();

    public abstract Date getPackageRecvDate();

    public abstract Date getPackageSendDate();

    public abstract String getStateTitle();

    public abstract String getTitle();

    public abstract String getTypeNumber();

    public abstract String getTypeTitle();
}
