package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public abstract class EnrFisConverterStaticDao<T extends IEntity, KEY> extends EnrFisConverterBaseDao<T, KEY> implements IEnrFisConverterStaticDao<T> {

    /** @return !static! { fis-item.code -> fis-item.title} */
    protected abstract Map<String, String> getStaticTitleCheckMap();

    /** @return !static! { key(entity) -> getItemCode(fis-item) } */
    protected abstract Map<KEY, String> getStaticItemCodeMap();

    public String getItemCode(EnrFisCatalogItem item) {
        return item.getFisItemCode();
    }

    @Override
    public String checkTitle(String code, String title)
    {
        Map<String, String> staticTitleCheckMap = getStaticTitleCheckMap();

        title = StringUtils.trimToEmpty(title);
        code = StringUtils.trimToEmpty(code);

        final String expectedTitle = StringUtils.trimToNull(staticTitleCheckMap.get(code));
        if (null != expectedTitle) {
            // сопоставление есть, мы на него завязались (требуем, чтобы название сохранилось)
            if (!expectedTitle.equals(title)) {
                throw new IllegalStateException("fisCatalogItem(catalog="+this.getCatalogCode()+", code="+code+"): title missmatch: title=«"+title+"», expectedTitle=«"+expectedTitle+"»");
            }
        }
        return title;
    }

    @Override
    protected Map<KEY, Long> getItemMap()
    {
        final List<EnrFisCatalogItem> items = new DQLSelectBuilder()
        .fromEntity(EnrFisCatalogItem.class, "x")
        .column(property("x"))
        .where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("x")), value(this.getCatalogCode())))
        .createStatement(this.getSession()).list();

        final Map<String, Long> code2idMap = new HashMap<>();
        for (final EnrFisCatalogItem item: items) {
            final Long id = item.getId();
            final String code = getItemCode(item);

            if (null != code2idMap.put(code, id)) {
                throw new IllegalStateException("code2idMap: duplicate value for key=«"+code+"»");
            }
        }
        if (items.isEmpty())
            throw new NoSuchElementException("Catalog «" + this.getCatalogCode() + "» is empty");

        String errorPostfix = " in catalog with code=«" + this.getCatalogCode() + "»";
        final Map<KEY, String> staticItemCodeMap = getStaticItemCodeMap();
        return SafeMap.get(key -> {

            final String code = staticItemCodeMap.get(key);
            if (null == code) {
                throw new NoSuchElementException("Missing fisCode for uni.code=«" + key + "»" + errorPostfix);
            }

            final Long id = code2idMap.get(code);
            if (null == id) {
                throw new NoSuchElementException("Missing enrFisCatalogItem with fisItemCode=«" + code + "» (uni.code=«" + key + "»)" + errorPostfix);
            }

            return id;
        });
    }

}
