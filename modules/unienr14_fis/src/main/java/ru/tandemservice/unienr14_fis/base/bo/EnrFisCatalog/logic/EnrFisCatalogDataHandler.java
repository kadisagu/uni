package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic;

import enr14fis.schema.catalog.items.resp.spec.DictionaryData;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * @author vdanilov
 */
public interface EnrFisCatalogDataHandler<T, I>
{
    Class<T> getResponseKlass();

    Long getCatalogCode(T response);
    String getCatalogName(T response);

    Collection<I> getItems(T response);

    Long getItemID(I item);
    String getItemName(I item);


    class Default implements EnrFisCatalogDataHandler<enr14fis.schema.catalog.items.resp.DictionaryData, enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem> {

        @Override public
        Class<enr14fis.schema.catalog.items.resp.DictionaryData> getResponseKlass() {
            return enr14fis.schema.catalog.items.resp.DictionaryData.class;
        }

        @Override public Long
        getCatalogCode(final enr14fis.schema.catalog.items.resp.DictionaryData response) {
            return response.getCode(); // чертов ФИС, достали, пусть уже разбирутся с типами
        }

        @Override public String
        getCatalogName(final enr14fis.schema.catalog.items.resp.DictionaryData response) {
            return response.getName();
        }

        @Override public Collection<enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final enr14fis.schema.catalog.items.resp.DictionaryData response) {
            if (response.getDictionaryItems() == null || response.getDictionaryItems().getDictionaryItem() == null)
                return Collections.emptyList();
            return response.getDictionaryItems().getDictionaryItem();
        }

        @Override public Long
        getItemID(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getID();
        }

        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
        }
    }


    // стандартная обработка справочника
    EnrFisCatalogDataHandler CATALOG_DEFAULT = new Default();

    // Общеобразовательные предметы (с проверкой на использование в пользовательских справочниках и проверкой названий)
    EnrFisCatalogDataHandler CATALOG_1_DISCIPLINE = new Default() {
        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
            //return EcfConverterManager.instance().stateExamSubject().checkTitle(
            //    String.valueOf(getItemID(item)),
            //    item.getName()
            //);
        }

        @Override public Collection<enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final enr14fis.schema.catalog.items.resp.DictionaryData response)
        {
            //Map<Long, String> itemMap = new HashMap<>();
            //for (enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item : super.getItems(response))
            //{
            //    final Long id = getItemID(item);
            //    final String name = StringUtils.trimToEmpty(getItemName(item));
            //    itemMap.put(id, name);
            //}
            // EcfConverterManager.instance().setDiscipline().validateUsedInUserConverter(itemMap);

            return super.getItems(response);
        }
    };

    // обработка справочника страны (с проверкой)
    EnrFisCatalogDataHandler CATALOG_7_COUNTRIES = new Default() {
        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
            //return EcfConverterManager.instance().addressCountry().checkTitle(
            //    String.valueOf(getItemID(item)),
            //    item.getName()
            //);
        }
    };

    // обработка справочника форма обучения (с проверкой)
    EnrFisCatalogDataHandler CATALOG_14_EDUFORM = new Default() {
        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
            //return EcfConverterManager.instance().educationForm().checkTitle(
            //    String.valueOf(getItemID(item)),
            //    item.getName()
            //);
        }
    };

    // обработка справочника Тип вступительных испытаний (с проверкой)
    EnrFisCatalogDataHandler CATALOG_11_DISCIPLINE = new Default() {
        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
            //return EcfConverterManager.instance().educationForm().checkTitle(
            //    String.valueOf(getItemID(item)),
            //    item.getName()
            //);
        }
    };

    // обработка справочника Тип вступительных испытаний (с проверкой)
    EnrFisCatalogDataHandler CATALOG_18_OLYMP_TYPE = new Default() {
        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
            //return EcfConverterManager.instance().educationForm().checkTitle(
            //    String.valueOf(getItemID(item)),
            //    item.getName()
            //);
        }
    };


    // направления подготовки (10) (с проверкой на использование в пользовательских справочниках)
    EnrFisCatalogDataHandler CATALOG_10_SPEC = new EnrFisCatalogDataHandler<enr14fis.schema.catalog.items.resp.spec.DictionaryData, enr14fis.schema.catalog.items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem>() {

        @Override public Class<enr14fis.schema.catalog.items.resp.spec.DictionaryData>
        getResponseKlass() {
            return enr14fis.schema.catalog.items.resp.spec.DictionaryData.class;
        }

        @Override public Long
        getCatalogCode(final enr14fis.schema.catalog.items.resp.spec.DictionaryData response) {
            return response.getCode(); // чертов ФИС, достали, пусть уже разбирутся с типами
        }

        @Override public String
        getCatalogName(final enr14fis.schema.catalog.items.resp.spec.DictionaryData response) {
            return response.getName();
        }

        @Override public Collection<enr14fis.schema.catalog.items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final enr14fis.schema.catalog.items.resp.spec.DictionaryData response) {
            return CollectionUtils.select(response.getDictionaryItems().getDictionaryItem(), dictionaryItem -> StringUtils.isNotEmpty(dictionaryItem.getNewCode()));
        }

        @Override public Long
        getItemID(final enr14fis.schema.catalog.items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getID();
        }

        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem item) {
            final String base = item.getNewCode() + " " + item.getName();
            String spoLevel = getSpoLevel(item);
            if (null != spoLevel ) {
                return base + " (" + spoLevel + ")";
            } else {
                return base;
            }
        }

        private String getSpoLevel(DictionaryData.DictionaryItems.DictionaryItem item) {
            String spoLevel = null;
            if ("51".equals(item.getQualificationCode())) spoLevel = "базов.";
            if ("52".equals(item.getQualificationCode())) spoLevel = "повыш.";
            return spoLevel;
        }
    };

    // олимпиады (19) (с проверкой на использование в пользовательских справочниках)
    EnrFisCatalogDataHandler CATALOG_19_OLYMP = new EnrFisCatalogDataHandler<enr14fis.schema.catalog.items.resp.olymp.DictionaryData, enr14fis.schema.catalog.items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem>() {

        @Override public Class<enr14fis.schema.catalog.items.resp.olymp.DictionaryData>
        getResponseKlass() {
            return enr14fis.schema.catalog.items.resp.olymp.DictionaryData.class;
        }

        @Override public Long
        getCatalogCode(final enr14fis.schema.catalog.items.resp.olymp.DictionaryData response) {
            return response.getCode(); // чертов ФИС, достали, пусть уже разбирутся с типами
        }

        @Override public String
        getCatalogName(final enr14fis.schema.catalog.items.resp.olymp.DictionaryData response) {
            return response.getName();
        }

        @Override public Collection<enr14fis.schema.catalog.items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final enr14fis.schema.catalog.items.resp.olymp.DictionaryData response) {
            //Map<Long, String> itemMap = new HashMap<>();
            //for (enr14fis.schema.catalog.items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem item : response.getDictionaryItems().getDictionaryItem())
            //{
            //    final Long id = getItemID(item);
            //    final String name = StringUtils.trimToEmpty(getItemName(item));
            //    itemMap.put(id, name);
            //}
            //EcfConverterManager.instance().olympiad().validateUsedInUserConverter(itemMap);

            return response.getDictionaryItems().getDictionaryItem();
        }

        @Override public Long
        getItemID(final enr14fis.schema.catalog.items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getOlympicID();
        }

        @Override public String
        getItemName(final enr14fis.schema.catalog.items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem item) {
            // «(<Year>) №<OlympicNumber> <OlympicName>»

            return "("+item.getYear() +") № "+item.getOlympicNumber()+" "+item.getOlympicName();
        }
    };


}