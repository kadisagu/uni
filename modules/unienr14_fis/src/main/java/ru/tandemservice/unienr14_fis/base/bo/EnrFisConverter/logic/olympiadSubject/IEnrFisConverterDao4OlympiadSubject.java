/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiadSubject;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author oleyba
 * @since 7/17/14
 */
public interface IEnrFisConverterDao4OlympiadSubject extends IEnrFisConverterDao<EnrOlympiadSubject>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync();
}

