/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympProfileDiscipline;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Ekaterina Zvereva
 * @since 16.08.2016
 */
public interface IEnrFisConverterDao4OlympProfileDiscipline extends IEnrFisConverterDao<EnrOlympiadProfileDiscipline>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync();
}
