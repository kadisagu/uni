package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Профили олимпиад
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4OlympiadSubjectGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4OlympiadSubjectGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject";
    public static final String ENTITY_NAME = "enrFisConv4OlympiadSubject";
    public static final int VERSION_HASH = 1199718941;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLYMPIAD_SUBJECT = "olympiadSubject";
    public static final String L_VALUE = "value";

    private EnrOlympiadSubject _olympiadSubject;     // Профильная дисциплина олимпиады
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrOlympiadSubject getOlympiadSubject()
    {
        return _olympiadSubject;
    }

    /**
     * @param olympiadSubject Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    public void setOlympiadSubject(EnrOlympiadSubject olympiadSubject)
    {
        dirty(_olympiadSubject, olympiadSubject);
        _olympiadSubject = olympiadSubject;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4OlympiadSubjectGen)
        {
            if (withNaturalIdProperties)
            {
                setOlympiadSubject(((EnrFisConv4OlympiadSubject)another).getOlympiadSubject());
            }
            setValue(((EnrFisConv4OlympiadSubject)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4OlympiadSubjectGen> getNaturalId()
    {
        return new NaturalId(getOlympiadSubject());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4OlympiadSubjectGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4OlympiadSubjectNaturalProxy";

        private Long _olympiadSubject;

        public NaturalId()
        {}

        public NaturalId(EnrOlympiadSubject olympiadSubject)
        {
            _olympiadSubject = ((IEntity) olympiadSubject).getId();
        }

        public Long getOlympiadSubject()
        {
            return _olympiadSubject;
        }

        public void setOlympiadSubject(Long olympiadSubject)
        {
            _olympiadSubject = olympiadSubject;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4OlympiadSubjectGen.NaturalId) ) return false;

            EnrFisConv4OlympiadSubjectGen.NaturalId that = (NaturalId) o;

            if( !equals(getOlympiadSubject(), that.getOlympiadSubject()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOlympiadSubject());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOlympiadSubject());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4OlympiadSubjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4OlympiadSubject.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4OlympiadSubject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "olympiadSubject":
                    return obj.getOlympiadSubject();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "olympiadSubject":
                    obj.setOlympiadSubject((EnrOlympiadSubject) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "olympiadSubject":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "olympiadSubject":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "olympiadSubject":
                    return EnrOlympiadSubject.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4OlympiadSubject> _dslPath = new Path<EnrFisConv4OlympiadSubject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4OlympiadSubject");
    }
            

    /**
     * @return Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject#getOlympiadSubject()
     */
    public static EnrOlympiadSubject.Path<EnrOlympiadSubject> olympiadSubject()
    {
        return _dslPath.olympiadSubject();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4OlympiadSubject> extends EntityPath<E>
    {
        private EnrOlympiadSubject.Path<EnrOlympiadSubject> _olympiadSubject;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject#getOlympiadSubject()
     */
        public EnrOlympiadSubject.Path<EnrOlympiadSubject> olympiadSubject()
        {
            if(_olympiadSubject == null )
                _olympiadSubject = new EnrOlympiadSubject.Path<EnrOlympiadSubject>(L_OLYMPIAD_SUBJECT, this);
            return _olympiadSubject;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4OlympiadSubject.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4OlympiadSubject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
