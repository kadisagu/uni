/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkNewAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;

/**
 * @author nvankov
 * @since 11.07.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "element.id"),
        @Bind(key = "enrCampaign", binding = "enrCampaignId", required = true)
})
public class EnrFisSettingsStateExamMinMarkNewAddEditUI extends UIPresenter
{
    private EnrFisSettingsStateExamMinMarkNew _element = new EnrFisSettingsStateExamMinMarkNew();
    private Long _enrCampaignId;
    private EnrEnrollmentCampaign _enrEnrollmentCampaign;
    private EduProgramKind _eduProgramKind;
    private EduProgramSubject _eduProgramSubject;
    private EduProgramForm _eduProgramForm;

    public boolean isAddForm()
    {
        return _element.getId() == null;
    }

    public boolean isEditForm()
    {
        return !isAddForm();
    }

    @Override
    public void onComponentRefresh() {
        setEnrEnrollmentCampaign(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentCampaign.class, _enrCampaignId));

        if(isEditForm())
        {
            setElement(IUniBaseDao.instance.get().getNotNull(_element.getId()));
            setEduProgramKind(getElement().getEnrProgramSetBase().getProgramKind());
            setEduProgramSubject(getElement().getEnrProgramSetBase().getProgramSubject());
            setEduProgramForm(getElement().getEnrProgramSetBase().getProgramForm());
        }
        else
        {
            getElement().setMinMark(75);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisSettingsStateExamMinMarkNewAddEdit.BIND_ENR_CAMPAIGN_ID, _enrCampaignId);
        dataSource.put(EnrFisSettingsStateExamMinMarkNewAddEdit.BIND_EDU_PROGRAM_KIND_ID, getEduProgramKind() != null ? getEduProgramKind().getId() : null);
        dataSource.put(EnrFisSettingsStateExamMinMarkNewAddEdit.BIND_EDU_PROGRAM_SUBJECT_ID, getEduProgramSubject() != null ? getEduProgramSubject().getId() : null);
        dataSource.put(EnrFisSettingsStateExamMinMarkNewAddEdit.BIND_EDU_PROGRAM_FORM_ID, getEduProgramForm() != null ? getEduProgramForm().getId() : null);
        dataSource.put(EnrFisSettingsStateExamMinMarkNewAddEdit.BIND_PROGRAM_SET_ID, getElement().getEnrProgramSetBase() != null ? getElement().getEnrProgramSetBase().getId() : null);
        if(EnrFisSettingsStateExamMinMarkNewAddEdit.STATE_EXAM_SUBJECT_DS.equals(dataSource.getName()) && isEditForm())
        {
            dataSource.put(EnrFisSettingsStateExamMinMarkNewAddEdit.BIND_SELECTED_SUBJECT_ID, getElement().getSubject().getId());
        }
    }

    public void onClickApply() {
        validate();
        if(getUserContext().getErrorCollector().hasErrors())
        {
            return;
        }
        IUniBaseDao.instance.get().doInTransaction(session ->
        {
            IUniBaseDao.instance.get().saveOrUpdate(getElement());
            return null;
        });
        deactivate();
    }

    private void validate()
    {
        if(getElement().getEnrProgramSetBase() == null)
        {
            _uiSupport.error("Поле «Набор ОП» должно быть заполнено", "programSet");
        }
    }

    // getters and setters

    public Long getEnrCampaignId()
    {
        return _enrCampaignId;
    }

    public void setEnrCampaignId(Long enrCampaignId)
    {
        _enrCampaignId = enrCampaignId;
    }

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return _enrEnrollmentCampaign;
    }

    public void setEnrEnrollmentCampaign(EnrEnrollmentCampaign enrEnrollmentCampaign)
    {
        _enrEnrollmentCampaign = enrEnrollmentCampaign;
    }

    public EnrFisSettingsStateExamMinMarkNew getElement()
    {
        return _element;
    }

    public void setElement(EnrFisSettingsStateExamMinMarkNew element)
    {
        _element = element;
    }

    public EduProgramKind getEduProgramKind()
    {
        return _eduProgramKind;
    }

    public void setEduProgramKind(EduProgramKind eduProgramKind)
    {
        _eduProgramKind = eduProgramKind;
    }

    public EduProgramSubject getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    public void setEduProgramSubject(EduProgramSubject eduProgramSubject)
    {
        _eduProgramSubject = eduProgramSubject;
    }

    public EduProgramForm getEduProgramForm()
    {
        return _eduProgramForm;
    }

    public void setEduProgramForm(EduProgramForm eduProgramForm)
    {
        _eduProgramForm = eduProgramForm;
    }
}
