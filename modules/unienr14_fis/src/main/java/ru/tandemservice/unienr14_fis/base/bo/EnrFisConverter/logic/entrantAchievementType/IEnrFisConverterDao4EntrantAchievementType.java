/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.entrantAchievementType;

import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Alexey Lopatin
 * @since 09.06.2015
 */
public interface IEnrFisConverterDao4EntrantAchievementType extends IEnrFisConverterDao<EnrEntrantAchievementType>
{
}
