/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p4;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4AOrdersInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

/**
 * @author oleyba
 * @since 7/2/14
 */
public interface IPkgDao4OrdersOfAdmission extends INeedPersistenceSupport
{

    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    EnrFisSyncPackage4AOrdersInfo doCreatePackage(EnrFisSyncSession fisSession);
}
