/* $Id: CommonCatalogList.java 6 2012-04-04 12:30:37Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.ui.ItemList;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.log.UpdateEntityEvent;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.zip.ZipCompressionInterface;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.EnrFisCatalogManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic.EnrFisCatalogDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic.ItemListDSHandler;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.ui.Import.EnrFisCatalogImport;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.EnrFisOfflineDao;
import ru.tandemservice.unienr14_fis.util.IEnrFisAuthentication;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EnrFisCatalogItemListUI extends UIPresenter
{
    public static final String PARAM_CATALOG_CODE = "catalogCode";
    public static final String PARAM_CATALOG_ITEM_TITLE = "catalogItemTitle";

    private boolean offlineMode =  false;

    @Override
    public void onComponentRefresh()
    {
        offlineMode = EnrFisOfflineDao.isFisOfflineMode();
    }

    public void onClickImport() {
        _uiActivation.asRegionDialog(EnrFisCatalogImport.class).activate();
    }

    public void onClickSync() {

        final EnrOrgUnit enrOrgUnit = new DQLSelectBuilder()
        .fromEntity(EnrOrgUnit.class, "ou")
        .where(eq(property(EnrOrgUnit.institutionOrgUnit().orgUnit().fromAlias("ou")), value(TopOrgUnit.getInstance())))
        .where(eq(property(EnrOrgUnit.enrollmentCampaign().fromAlias("ou")), value(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign())))
        .order(property(EnrOrgUnit.id().fromAlias("ou")), OrderDirection.desc)
        .createStatement(getSupport().getSession()).setMaxResults(1).uniqueResult();

        if (null == enrOrgUnit) {
            throw new ApplicationException("Не найдено подразделение для реквизитов доступа к ФИС.");
        }

        EnrFisCatalogManager.instance().dao().doSyncCatalogs(
            new IEnrFisAuthentication() {
                @Override public String getLogin() {
                    return enrOrgUnit.getLoginSafe();
                }
                @Override public String getPassword() {
                    return enrOrgUnit.getPasswordSafe();
                }
            });

    }

    public void onClickCleanup()
    {
        final MutableInt count = new MutableInt(0);
        IUniBaseDao.instance.get().doInTransaction(session -> {
            count.add(
                    new DQLDeleteBuilder(EnrFisCatalogItem.class)
                            .where(new DQLCanDeleteExpressionBuilder(EnrFisCatalogItem.class, "id").getExpression())
                            .createStatement(session).execute()
            );
            return null;
        });

        ContextLocal.getInfoCollector().add("Удалено элементов справочников ФИС: " + count.intValue() + ".");
        CoreServices.eventService().fireEvent(new UpdateEntityEvent("Справочники ФИС очищены", TopOrgUnit.getInstance()));
    }

    public void onGetZipLog()
    {
        byte[] content = getContent();
        if (isContentEmpty(content))
            throw new ApplicationException("Не найден журнал обновления.");

        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .zip()
                        .fileName("Журнал обновления.zip")
                        .document(ZipCompressionInterface.INSTANCE.compress(content, EnrFisCatalogDao.LOG_FILE_NAME + ".log")),
                true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(ItemListDSHandler.PARAM_CATALOG_CODE, getSettings().get(PARAM_CATALOG_CODE));
        dataSource.put(ItemListDSHandler.PARAM_CATALOG_ITEM_TITLE, getSettings().get(PARAM_CATALOG_ITEM_TITLE));
    }

    // getters

    public boolean isContentEmpty()
    {
        return isContentEmpty(getContent());
    }

    private boolean isContentEmpty(byte[] content)
    {
        return ArrayUtils.isEmpty(content);
    }

    public byte[] getContent()
    {
        try { return Files.readAllBytes(Paths.get(EnrFisCatalogDao.getLogFilePath())); }
        catch (IOException e) { return new byte[0]; }
    }


    public boolean isOfflineMode()
    {
        return offlineMode;
    }
}
