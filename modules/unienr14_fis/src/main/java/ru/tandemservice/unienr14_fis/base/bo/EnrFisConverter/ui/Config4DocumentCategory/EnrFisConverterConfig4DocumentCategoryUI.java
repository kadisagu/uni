/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategory;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategoryOrphan.EnrFisConverterConfig4DocumentCategoryOrphan;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public abstract class EnrFisConverterConfig4DocumentCategoryUI extends EnrFisConverterConfigUI
{
    public static final String MENU_PERMISSION_KEY = "menuEnr14FisConverterConfig4DocumentCategory";
    public static final String POSTFIX_PERMISSION_KEY = "enr14FisConverterConfig4DocumentCategory";

    private EnrEnrollmentCampaign _enrCampaign;
    public boolean _withoutFisCatalogs;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisConverterConfig4DocumentCategoryOrphan.BIND_ENROLLMENT_CAMPAIGN, _enrCampaign);
    }

    public void onRefreshSelectors()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    public String getMenuPermissionKey()
    {
        return MENU_PERMISSION_KEY + getDocumentCategoryTypeCode();
    }

    public String getEditPermissionKey()
    {
        return POSTFIX_PERMISSION_KEY + getDocumentCategoryTypeCode() + "Edit";
    }

    public String getAutoSyncPermissionKey()
    {
        return POSTFIX_PERMISSION_KEY + getDocumentCategoryTypeCode() + "AutoSync";
    }

    protected String getDocumentCategoryTypeCode()
    {
        throw new UnsupportedOperationException();
    }

    // Getters & Setters

    public boolean isEmptySelectors()
    {
        return _enrCampaign == null;
    }

    // Accessors

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }

    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }
}
