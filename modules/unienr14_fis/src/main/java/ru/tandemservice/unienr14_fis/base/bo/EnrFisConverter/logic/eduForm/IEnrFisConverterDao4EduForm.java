package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduForm;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterStaticDao;

/**
 * @author vdanilov
 */
public interface IEnrFisConverterDao4EduForm extends IEnrFisConverterStaticDao<EduProgramForm> {

    /**
     * @return O, Z, OZ для очной, заочной и очно-заочной формы соответственно
     */
    String getEduFormPostfix(EduProgramForm developForm);

}
