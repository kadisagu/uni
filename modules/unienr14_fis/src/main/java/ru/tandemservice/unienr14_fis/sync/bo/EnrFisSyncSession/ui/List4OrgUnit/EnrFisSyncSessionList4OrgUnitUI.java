package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.List4OrgUnit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.process.*;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.daemon.EnrFisSyncSessionSendDaemonBean;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.EnrFisOfflineDao;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.EnrFisOrgUnitSyncSessionDSHandler;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.Add.EnrFisSyncSessionAdd;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.Import.EnrFisSyncSessionImport;

import java.util.Arrays;
import java.util.Date;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true)
})
public class EnrFisSyncSessionList4OrgUnitUI extends UIPresenter {

    public static final String PARAM_ENR_CAMPAIGN_DS = "enrCampaign";

    private final OrgUnitHolder holder = new OrgUnitHolder();
    private IUIDataSource _enrFisOrgUnitSyncSessionDS;

    private EnrEnrollmentCampaign _enrollmentCampaign;
    private boolean offlineMode =  false;

    private byte[] deletePackageContent = null;

    @Override public String getSettingsKey() {
        return super.getSettingsKey() + "." + getHolder().getId();
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (deletePackageContent != null) {            ;
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                    .xml().fileName("package-" + Arrays.hashCode(deletePackageContent) + ".xml")
                    .document(deletePackageContent), true);
            deletePackageContent = null;
        }
    }

    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        _enrFisOrgUnitSyncSessionDS = getConfig().getDataSource(EnrFisSyncSessionList4OrgUnit.ENRFIS_ORG_UNIT_SYNC_SESSION_DS);
        offlineMode = EnrFisOfflineDao.isFisOfflineMode();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(EnrFisOrgUnitSyncSessionDSHandler.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(EnrFisOrgUnitSyncSessionDSHandler.PARAM_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
    }



    // actions
    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickSearch() {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        getSettings().save();
    }

    public void onGenAppDeletePackage() throws Exception {
        byte[] bytes = EnrFisSyncSessionManager.instance().deletePackageGenerator().getPackage(getEnrollmentCampaign(), getOrgUnit(), false).getBytes();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .xml().fileName("package-" +bytes.hashCode() + ".xml")
            .document(bytes), true);
    }

    public void onGenOrderDeletePackage() throws Exception {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                try
                {
                    deletePackageContent =  EnrFisSyncSessionManager.instance().deletePackageGenerator().getPackage(getEnrollmentCampaign(), getOrgUnit(), true).getBytes();
                } catch(Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
                return null;
            }
        };

        new BackgroundProcessHolder().start("Формирование пакета на исключение заявлений из приказов", process, ProcessDisplayMode.unknown);
    }

//    public void onGenRecommendedPackage() throws Exception {
//        // неправильно работает - надо, как в Enr14FisAppDeletePackageGenerator, с головным передавать еще данные тех, кто не передает самостоятельно
//        List<EnrOrgUnit> orgUnits = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
//            .fromEntity(EnrOrgUnit.class, "e").column("e")
//            .where(eq(property("e", EnrOrgUnit.institutionOrgUnit().orgUnit()), value(getOrgUnit())))
//            .where(eq(property("e", EnrOrgUnit.enrollmentCampaign()), value(getEnrollmentCampaign()))));
//        if (orgUnits.isEmpty()) throw new ApplicationException("В рамках выбранной ПК не найдено подразделение, ведущее прием, соответствующее текущему.");
//
//        EnrOrgUnit enrOrgUnit = orgUnits.get(0);
//
//        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
//                .xml().fileName("package.xml")
//                .document(EnrFisSyncSessionManager.instance().dao4OrdersOfAdmission().getRecommendedPackageXml(enrOrgUnit)), true);
//    }

    public void onClickAddSession() {
        getActivationBuilder().asRegionDialog(EnrFisSyncSessionAdd.class)
        .parameter("enrollmentCampaignId", getEnrollmentCampaign().getId())
        .parameter(UIPresenter.PUBLISHER_ID, getHolder().getId())
        .activate();
    }

    public void onClickArchive() {
        EnrFisSyncSession fisSession = DataAccessServices.dao().get(EnrFisSyncSession.class, getSupport().getListenerParameterAsLong());
        EnrFisSyncSessionManager.instance().dao().doArchive(fisSession);
    }

    public void onClickDelete() {
        EnrFisSyncSession fisSession = DataAccessServices.dao().get(EnrFisSyncSession.class, getSupport().getListenerParameterAsLong());
        EnrFisSyncSessionManager.instance().dao().doDelete(fisSession);
    }

    public void onClickWakeUpSendDaemon() {
        EnrFisSyncSessionSendDaemonBean.activateAndWakeUp();
    }

    public void onClickGetSessionsXml()
    {
        EnrFisSyncSessionManager.instance().offlineDao().getSessionsXml(getEnrollmentCampaign(), getOrgUnit());
    }

    public void onClickImportSessionsXmlResults()
    {
        _uiActivation.asRegionDialog(EnrFisSyncSessionImport.class).parameter("enrCampaignId", getEnrollmentCampaign().getId()).parameter("orgUnitId", getOrgUnit().getId()).activate();
    }

    // presenter

    public boolean isSendDaemonActive() {
        return EnrFisSyncSessionSendDaemonBean.isActive();
    }

    public String getSubmitId(String action, Long id) {
        return "submit_"+action+"_"+id;
    }

    public boolean isOnlineMode()
    {
        return !isOfflineMode();
    }

    public boolean isNothingSelected(){ return null == getEnrollmentCampaign(); }

    // getters and setters

    public OrgUnitHolder getHolder() { return this.holder; }
    public OrgUnit getOrgUnit() { return this.getHolder().getValue(); }

    public CommonPostfixPermissionModelBase getSecModel() { return this.getHolder().getSecModel(); }

    public IUIDataSource getEnrFisOrgUnitSyncSessionDS() { return _enrFisOrgUnitSyncSessionDS; }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isOfflineMode()
    {
        return offlineMode;
    }

}
