package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Олимпиада
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4OlympiadGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4OlympiadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad";
    public static final String ENTITY_NAME = "enrFisConv4Olympiad";
    public static final int VERSION_HASH = -1535533864;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLYMPIAD = "olympiad";
    public static final String L_VALUE = "value";

    private EnrOlympiad _olympiad;     // Олимпиада
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Олимпиада. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrOlympiad getOlympiad()
    {
        return _olympiad;
    }

    /**
     * @param olympiad Олимпиада. Свойство не может быть null и должно быть уникальным.
     */
    public void setOlympiad(EnrOlympiad olympiad)
    {
        dirty(_olympiad, olympiad);
        _olympiad = olympiad;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4OlympiadGen)
        {
            if (withNaturalIdProperties)
            {
                setOlympiad(((EnrFisConv4Olympiad)another).getOlympiad());
            }
            setValue(((EnrFisConv4Olympiad)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4OlympiadGen> getNaturalId()
    {
        return new NaturalId(getOlympiad());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4OlympiadGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4OlympiadNaturalProxy";

        private Long _olympiad;

        public NaturalId()
        {}

        public NaturalId(EnrOlympiad olympiad)
        {
            _olympiad = ((IEntity) olympiad).getId();
        }

        public Long getOlympiad()
        {
            return _olympiad;
        }

        public void setOlympiad(Long olympiad)
        {
            _olympiad = olympiad;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4OlympiadGen.NaturalId) ) return false;

            EnrFisConv4OlympiadGen.NaturalId that = (NaturalId) o;

            if( !equals(getOlympiad(), that.getOlympiad()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOlympiad());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOlympiad());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4OlympiadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4Olympiad.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4Olympiad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "olympiad":
                    return obj.getOlympiad();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "olympiad":
                    obj.setOlympiad((EnrOlympiad) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "olympiad":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "olympiad":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "olympiad":
                    return EnrOlympiad.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4Olympiad> _dslPath = new Path<EnrFisConv4Olympiad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4Olympiad");
    }
            

    /**
     * @return Олимпиада. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad#getOlympiad()
     */
    public static EnrOlympiad.Path<EnrOlympiad> olympiad()
    {
        return _dslPath.olympiad();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4Olympiad> extends EntityPath<E>
    {
        private EnrOlympiad.Path<EnrOlympiad> _olympiad;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Олимпиада. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad#getOlympiad()
     */
        public EnrOlympiad.Path<EnrOlympiad> olympiad()
        {
            if(_olympiad == null )
                _olympiad = new EnrOlympiad.Path<EnrOlympiad>(L_OLYMPIAD, this);
            return _olympiad;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Olympiad#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4Olympiad.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4Olympiad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
