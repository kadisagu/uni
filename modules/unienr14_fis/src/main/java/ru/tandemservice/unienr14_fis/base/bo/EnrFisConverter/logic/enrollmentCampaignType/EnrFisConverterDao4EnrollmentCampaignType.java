/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.enrollmentCampaignType;

import com.google.common.collect.Maps;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCampaignType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterStaticDao;

import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 23.05.2016
 */
public class EnrFisConverterDao4EnrollmentCampaignType extends EnrFisConverterStaticDao<EnrEnrollmentCampaignType, String> implements IEnrFisConverterDao4EnrollmentCampaignType
{
    /* { fis.code -> fis.title } */
    private static final Map<String, String> STATIC_TITLE_CHECK_MAP = Maps.newHashMap();
    /* { uni.code -> fis.code } */
    private static final Map<String, String> STATIC_ITEM_CODE_MAP = Maps.newHashMap();

    /* { uni.code, uni.title, fis.code, fis.title } */
    private static final String[][] DATA = {
            { "1", "Прием на обучение по программам бакалавриата/специалитета",             "1", "Прием на обучение на бакалавриат/специалитет" },
            { "2", "Прием на обучение по программам магистратуры",                          "2", "Прием на обучение в магистратуру" },
            { "3", "Прием на обучение по программам СПО",                                   "3", "Прием на обучение на СПО" },
            { "4", "Прием на обучение по программам подготовки кадров высшей квалификации", "4", "Прием на подготовку кадров высшей квалификации" },
            { "5", "Прием иностранных абитуриентов по направлениям Минобрнауки РФ",         "5", "Прием иностранцев по направлениям Минобрнауки" }
    };

    static
    {
        for (final String[] row : DATA)
        {
            if (null != row[2])
            {
                if (null != STATIC_ITEM_CODE_MAP.put(row[0], row[2]))
                    throw new IllegalStateException("STATIC_ITEM_CODE_MAP: Duplicate value for key=«" + row[0] + "»");

                final String prev = STATIC_TITLE_CHECK_MAP.put(row[2], row[3]);
                if (null != prev && !prev.equals(row[3]))
                    throw new IllegalStateException("COUNTRY_CHECK_MAP: Duplicate value for key=«" + row[2] + "»");
            }
        }
    }

    @Override
    public String getCatalogCode()
    {
        return "38";
    }

    @Override
    protected List<EnrEnrollmentCampaignType> getEntityList()
    {
        return getList(EnrEnrollmentCampaignType.class, EnrEnrollmentCampaignType.P_CODE);
    }

    @Override
    protected String getItemMapKey(EnrEnrollmentCampaignType entity)
    {
        return entity.getCode();
    }

    @Override
    protected Map<String, String> getStaticTitleCheckMap()
    {
        return STATIC_TITLE_CHECK_MAP;
    }

    @Override
    protected Map<String, String> getStaticItemCodeMap()
    {
        return STATIC_ITEM_CODE_MAP;
    }
}
