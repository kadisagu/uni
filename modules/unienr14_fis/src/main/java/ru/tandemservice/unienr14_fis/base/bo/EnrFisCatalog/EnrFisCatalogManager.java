package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic.EnrFisCatalogDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic.IEnrFisCatalogDao;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisCatalogManager extends BusinessObjectManager {

    public static EnrFisCatalogManager instance() {
        return instance(EnrFisCatalogManager.class);
    }

    @Bean
    public IEnrFisCatalogDao dao() {
        return new EnrFisCatalogDao();
    }
}
