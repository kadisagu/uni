/**
 *$Id: IEnrFisConverterDao4ExamType.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examType;

import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterStaticDao;

/**
 * @author Alexander Shaburov
 * @since 22.07.13
 */
public interface IEnrFisConverterDao4ExamType extends IEnrFisConverterStaticDao<EnrExamType>
{
}
