/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiadSubject;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/17/14
 */
public class EnrFisConverterDao4OlympiadSubject extends EnrFisConverterBaseDao<EnrOlympiadSubject, Long> implements IEnrFisConverterDao4OlympiadSubject
{
    @Override
    public String getCatalogCode()
    {
        return "39";
    }

    @Override
    protected List<EnrOlympiadSubject> getEntityList()
    {
        return new DQLSelectBuilder().fromEntity(EnrOlympiadSubject.class, "x").column(property("x"))
                .order(property(EnrOlympiadSubject.title().fromAlias("x")))
                .createStatement(getSession()).list();
    }

    @Override
    protected Long getItemMapKey(EnrOlympiadSubject entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4OlympiadSubject.class, "x")
                        .column(property(EnrFisConv4OlympiadSubject.olympiadSubject().id().fromAlias("x")), "oly_id")
                        .column(property(EnrFisConv4OlympiadSubject.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())));
    }

    @Override
    public void update(final Map<EnrOlympiadSubject, EnrFisCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EnrFisConv4OlympiadSubject> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<EnrOlympiadSubject, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4OlympiadSubject(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4OlympiadSubject.NaturalId, EnrFisConv4OlympiadSubject>() {
            @Override protected EnrFisConv4OlympiadSubject.NaturalId key(final EnrFisConv4OlympiadSubject source) {
                return (EnrFisConv4OlympiadSubject.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4OlympiadSubject buildRow(final EnrFisConv4OlympiadSubject source) {
                return new EnrFisConv4OlympiadSubject(source.getOlympiadSubject(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4OlympiadSubject target, final EnrFisConv4OlympiadSubject source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4OlympiadSubject databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4OlympiadSubject.class), targetRecords);

    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4OlympiadSubject.class, "b").column(property(EnrFisConv4OlympiadSubject.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4OlympiadSubject.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync()
    {
        // поднимаем Олимпиады, для которых нет сопоставления с Элементом справочника ФИС
        final DQLSelectBuilder subjectDQL = new DQLSelectBuilder().fromEntity(EnrOlympiadSubject.class, "o").column(property("o"))
                .where(notIn(property(EnrOlympiadSubject.id().fromAlias("o")),
                        new DQLSelectBuilder().fromEntity(EnrFisConv4OlympiadSubject.class, "c").column(property(EnrFisConv4OlympiadSubject.olympiadSubject().id().fromAlias("c")))
                                .buildQuery()));

        final DQLSelectBuilder fisCatalogDQL = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "f").column(property("f"))
                .where(isNull(property("f", EnrFisCatalogItem.removalDate())))
                .where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("f")), value(getCatalogCode())));

        final Map<String, EnrFisCatalogItem> fisCatalogItemTitleMap = new HashMap<>();
        for (EnrFisCatalogItem item : fisCatalogDQL.createStatement(getSession()).<EnrFisCatalogItem>list())
        {
            fisCatalogItemTitleMap.put(item.getTitle().trim().toLowerCase(), item);
        }

        final Map<EnrOlympiadSubject, EnrFisCatalogItem> values = new HashMap<>();
        for (EnrOlympiadSubject subject : subjectDQL.createStatement(getSession()).<EnrOlympiadSubject>list())
        {
            EnrFisCatalogItem item = fisCatalogItemTitleMap.get(subject.getTitle().toLowerCase());
            if (null == item) { continue; }

            values.put(subject, item);
        }

        // для тех для которых уже выбрали - не меняем
        for (EnrFisConv4OlympiadSubject discipline : getList(EnrFisConv4OlympiadSubject.class))
            values.put(discipline.getOlympiadSubject(), discipline.getValue());

        this.update(values, false);
    }
}

