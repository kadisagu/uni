package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4DocumentCategoryCombatVeteransGen */
public class EnrFisConv4DocumentCategoryCombatVeterans extends EnrFisConv4DocumentCategoryCombatVeteransGen
{
    public EnrFisConv4DocumentCategoryCombatVeterans()
    {
    }

    public EnrFisConv4DocumentCategoryCombatVeterans(PersonDocumentCategory docCategory, EnrFisCatalogItem value)
    {
        setDocCategory(docCategory);
        setValue(value);
    }
}