package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p2;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4AdmissionInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

/**
 * @author vdanilov
 */
@DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм формирования пакета в рамках сессии")
public interface IPkgDao4AdmissionInfo extends INeedPersistenceSupport {

    /**
     * Создает пакет (пакет 2: Сведения об объеме и структуре приема)
     * @param fisSession
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EnrFisSyncPackage4AdmissionInfo doCreatePackage(EnrFisSyncSession fisSession);

}
