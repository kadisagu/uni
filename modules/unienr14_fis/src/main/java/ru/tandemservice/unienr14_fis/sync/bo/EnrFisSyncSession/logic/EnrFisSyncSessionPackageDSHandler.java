package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public class EnrFisSyncSessionPackageDSHandler extends AbstractReadAggregateHandler<DSInput, DSOutput> {

    public static final String PARAM_FIS_SESSION = "fisSession";

    public EnrFisSyncSessionPackageDSHandler(final String ownerId) {
        super(ownerId);
    }


    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {

        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrFisSyncPackage.class, "e").column(property("e"))
        .order(property(EnrFisSyncPackage.creationDate().fromAlias("e")), OrderDirection.asc)
        .order(property(EnrFisSyncPackage.id().fromAlias("e")), OrderDirection.asc)
        .where(eq(property(EnrFisSyncPackage.session().fromAlias("e")), commonValue(context.get(PARAM_FIS_SESSION))));

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build();

        final List<DataWrapper> wrappers = DataWrapper.wrap(output);

        final List<EnrFisSyncPackageIOLog> rows = new DQLSelectBuilder()
        .fromEntity(EnrFisSyncPackageIOLog.class, "x").column("x")
        .order(property(EnrFisSyncPackageIOLog.operationDate().fromAlias("x")), OrderDirection.desc)
        .order(property(EnrFisSyncPackageIOLog.id().fromAlias("x")), OrderDirection.desc)
        .where(in(property(EnrFisSyncPackageIOLog.owner().id().fromAlias("x")), CommonDAO.ids(wrappers)))
        .createStatement(context.getSession()).list();

        final Map<Long, DataWrapper> map = CommonDAO.map(wrappers);
        for (final EnrFisSyncPackageIOLog row: rows) {
            final DataWrapper wrapper = map.get(row.getOwner().getId());
            if (null == wrapper) { continue; }

            List<String> log = (List<String>)wrapper.getProperty("iolog");
            if (null == log) { wrapper.setProperty("iolog", log = new ArrayList<>()); }
            log.add(row.getTitle());
        }

        for (final DataWrapper wrapper: wrappers) {
            final List<String> log = (List<String>)wrapper.getProperty("iolog");
            if (null == log) { continue; }

            wrapper.setProperty("iologSize", log.size());

            if (log.size() > 4) {
                wrapper.setProperty("iolog", StringUtils.join(log.subList(0, 4), "\n")+"\n... еще записей " + (log.size()-4));
            } else {
                wrapper.setProperty("iolog", StringUtils.join(log, "\n"));
            }
        }

        return output;
    }

}
