package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4DocumentCategorySportsDiplomaGen */
public class EnrFisConv4DocumentCategorySportsDiploma extends EnrFisConv4DocumentCategorySportsDiplomaGen
{
    public EnrFisConv4DocumentCategorySportsDiploma()
    {
    }

    public EnrFisConv4DocumentCategorySportsDiploma(PersonDocumentCategory docCategory, EnrFisCatalogItem value)
    {
        setDocCategory(docCategory);
        setValue(value);
    }
}