package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_fis_2x10x4_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrFisSettingsStateExamMinMarkNew

        // создана новая сущность
        {
            if (!tool.tableExists("enr14fis_s_st_xm_mn_mrk_nw_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("enr14fis_s_st_xm_mn_mrk_nw_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_576a947"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("enrprogramsetbase_id", DBType.LONG).setNullable(false),
                        new DBColumn("subject_id", DBType.LONG).setNullable(false),
                        new DBColumn("benefittype_id", DBType.LONG).setNullable(false),
                        new DBColumn("minmark_p", DBType.INTEGER).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrFisSettingsStateExamMinMarkNew");

        }


    }
}