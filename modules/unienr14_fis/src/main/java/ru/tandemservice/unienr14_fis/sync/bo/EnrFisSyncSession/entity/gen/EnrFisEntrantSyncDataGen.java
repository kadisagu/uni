package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные абитуриента в ФИС
 *
 * Хранит данные заявления абитуриента (последнее состояние, на основе которого был сформирован какой-либо пакет).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisEntrantSyncDataGen extends EntityBase
 implements INaturalIdentifiable<EnrFisEntrantSyncDataGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData";
    public static final String ENTITY_NAME = "enrFisEntrantSyncData";
    public static final int VERSION_HASH = -1388952959;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String P_UID = "uid";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_SUCCESSFUL_IMPORT = "successfulImport";
    public static final String P_SKIP_REASON = "skipReason";
    public static final String P_ZIP_XML_PACKAGE_HASH = "zipXmlPackageHash";
    public static final String P_ZIP_XML_PACKAGE = "zipXmlPackage";
    public static final String L_SYNC_PACKAGE = "syncPackage";
    public static final String P_SYNC_COMPLETE_DATE = "syncCompleteDate";

    private EnrEntrant _entrant;     // Абитуриент
    private String _uid;     // Идентификатор абитуриента в ФИС
    private Date _modificationDate;     // Дата модификации пакета
    private Boolean _successfulImport;     // Успешный результат импорта
    private String _skipReason;     // Причина пропуска при формировании пакета
    private int _zipXmlPackageHash;     // ZIP-XML: Пакет (хэш содержимого zipXmlPackage)
    private byte[] _zipXmlPackage;     // ZIP-XML: Пакет
    private EnrFisSyncPackage _syncPackage;     // Последний сформированный пакет ФИС, в который помещены данные абитуриента
    private Date _syncCompleteDate;     // Дата, когда данные абитуриента были отправлены в ФИС и прошли обработку

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Идентификатор абитуриента в ФИС. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getUid()
    {
        return _uid;
    }

    /**
     * @param uid Идентификатор абитуриента в ФИС. Свойство не может быть null и должно быть уникальным.
     */
    public void setUid(String uid)
    {
        dirty(_uid, uid);
        _uid = uid;
    }

    /**
     * Дата, когда пакет был изменен в системе
     *
     * @return Дата модификации пакета. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата модификации пакета. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Успешный результат импорта.
     */
    public Boolean getSuccessfulImport()
    {
        return _successfulImport;
    }

    /**
     * @param successfulImport Успешный результат импорта.
     */
    public void setSuccessfulImport(Boolean successfulImport)
    {
        dirty(_successfulImport, successfulImport);
        _successfulImport = successfulImport;
    }

    /**
     * @return Причина пропуска при формировании пакета.
     */
    @Length(max=2000)
    public String getSkipReason()
    {
        return _skipReason;
    }

    /**
     * @param skipReason Причина пропуска при формировании пакета.
     */
    public void setSkipReason(String skipReason)
    {
        dirty(_skipReason, skipReason);
        _skipReason = skipReason;
    }

    /**
     * Хэш пакета, чтобы быстро находить те, которые более не актуальны
     *
     * @return ZIP-XML: Пакет (хэш содержимого zipXmlPackage). Свойство не может быть null.
     */
    @NotNull
    public int getZipXmlPackageHash()
    {
        return _zipXmlPackageHash;
    }

    /**
     * @param zipXmlPackageHash ZIP-XML: Пакет (хэш содержимого zipXmlPackage). Свойство не может быть null.
     */
    public void setZipXmlPackageHash(int zipXmlPackageHash)
    {
        dirty(_zipXmlPackageHash, zipXmlPackageHash);
        _zipXmlPackageHash = zipXmlPackageHash;
    }

    /**
     * Данные абитуриента
     *
     * @return ZIP-XML: Пакет. Свойство не может быть null.
     */
    @NotNull
    public byte[] getZipXmlPackage()
    {
        initLazyForGet("zipXmlPackage");
        return _zipXmlPackage;
    }

    /**
     * @param zipXmlPackage ZIP-XML: Пакет. Свойство не может быть null.
     */
    public void setZipXmlPackage(byte[] zipXmlPackage)
    {
        initLazyForSet("zipXmlPackage");
        dirty(_zipXmlPackage, zipXmlPackage);
        _zipXmlPackage = zipXmlPackage;
    }

    /**
     * Ссылка на последний сформированный пакет, в который помещены данные абитуриента.
     * Используется для отслеживания статуса передачи данных в ФИС по абитуриенту.
     *
     * @return Последний сформированный пакет ФИС, в который помещены данные абитуриента.
     */
    public EnrFisSyncPackage getSyncPackage()
    {
        return _syncPackage;
    }

    /**
     * @param syncPackage Последний сформированный пакет ФИС, в который помещены данные абитуриента.
     */
    public void setSyncPackage(EnrFisSyncPackage syncPackage)
    {
        dirty(_syncPackage, syncPackage);
        _syncPackage = syncPackage;
    }

    /**
     * Дата, когда пакет syncPackage, был отправлен в ФИС, по пакету был получен ответ и данные абитуриента полностью проимпортировались.
     * Обновляется демоном при получении ответа по пакету.
     *
     * @return Дата, когда данные абитуриента были отправлены в ФИС и прошли обработку.
     */
    public Date getSyncCompleteDate()
    {
        return _syncCompleteDate;
    }

    /**
     * @param syncCompleteDate Дата, когда данные абитуриента были отправлены в ФИС и прошли обработку.
     */
    public void setSyncCompleteDate(Date syncCompleteDate)
    {
        dirty(_syncCompleteDate, syncCompleteDate);
        _syncCompleteDate = syncCompleteDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisEntrantSyncDataGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrFisEntrantSyncData)another).getEntrant());
            }
            setUid(((EnrFisEntrantSyncData)another).getUid());
            setModificationDate(((EnrFisEntrantSyncData)another).getModificationDate());
            setSuccessfulImport(((EnrFisEntrantSyncData)another).getSuccessfulImport());
            setSkipReason(((EnrFisEntrantSyncData)another).getSkipReason());
            setZipXmlPackageHash(((EnrFisEntrantSyncData)another).getZipXmlPackageHash());
            setZipXmlPackage(((EnrFisEntrantSyncData)another).getZipXmlPackage());
            setSyncPackage(((EnrFisEntrantSyncData)another).getSyncPackage());
            setSyncCompleteDate(((EnrFisEntrantSyncData)another).getSyncCompleteDate());
        }
    }

    public INaturalId<EnrFisEntrantSyncDataGen> getNaturalId()
    {
        return new NaturalId(getEntrant());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisEntrantSyncDataGen>
    {
        private static final String PROXY_NAME = "EnrFisEntrantSyncDataNaturalProxy";

        private Long _entrant;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant)
        {
            _entrant = ((IEntity) entrant).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisEntrantSyncDataGen.NaturalId) ) return false;

            EnrFisEntrantSyncDataGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisEntrantSyncDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisEntrantSyncData.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisEntrantSyncData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "uid":
                    return obj.getUid();
                case "modificationDate":
                    return obj.getModificationDate();
                case "successfulImport":
                    return obj.getSuccessfulImport();
                case "skipReason":
                    return obj.getSkipReason();
                case "zipXmlPackageHash":
                    return obj.getZipXmlPackageHash();
                case "zipXmlPackage":
                    return obj.getZipXmlPackage();
                case "syncPackage":
                    return obj.getSyncPackage();
                case "syncCompleteDate":
                    return obj.getSyncCompleteDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "uid":
                    obj.setUid((String) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "successfulImport":
                    obj.setSuccessfulImport((Boolean) value);
                    return;
                case "skipReason":
                    obj.setSkipReason((String) value);
                    return;
                case "zipXmlPackageHash":
                    obj.setZipXmlPackageHash((Integer) value);
                    return;
                case "zipXmlPackage":
                    obj.setZipXmlPackage((byte[]) value);
                    return;
                case "syncPackage":
                    obj.setSyncPackage((EnrFisSyncPackage) value);
                    return;
                case "syncCompleteDate":
                    obj.setSyncCompleteDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "uid":
                        return true;
                case "modificationDate":
                        return true;
                case "successfulImport":
                        return true;
                case "skipReason":
                        return true;
                case "zipXmlPackageHash":
                        return true;
                case "zipXmlPackage":
                        return true;
                case "syncPackage":
                        return true;
                case "syncCompleteDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "uid":
                    return true;
                case "modificationDate":
                    return true;
                case "successfulImport":
                    return true;
                case "skipReason":
                    return true;
                case "zipXmlPackageHash":
                    return true;
                case "zipXmlPackage":
                    return true;
                case "syncPackage":
                    return true;
                case "syncCompleteDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "uid":
                    return String.class;
                case "modificationDate":
                    return Date.class;
                case "successfulImport":
                    return Boolean.class;
                case "skipReason":
                    return String.class;
                case "zipXmlPackageHash":
                    return Integer.class;
                case "zipXmlPackage":
                    return byte[].class;
                case "syncPackage":
                    return EnrFisSyncPackage.class;
                case "syncCompleteDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisEntrantSyncData> _dslPath = new Path<EnrFisEntrantSyncData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisEntrantSyncData");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Идентификатор абитуриента в ФИС. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getUid()
     */
    public static PropertyPath<String> uid()
    {
        return _dslPath.uid();
    }

    /**
     * Дата, когда пакет был изменен в системе
     *
     * @return Дата модификации пакета. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Успешный результат импорта.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSuccessfulImport()
     */
    public static PropertyPath<Boolean> successfulImport()
    {
        return _dslPath.successfulImport();
    }

    /**
     * @return Причина пропуска при формировании пакета.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSkipReason()
     */
    public static PropertyPath<String> skipReason()
    {
        return _dslPath.skipReason();
    }

    /**
     * Хэш пакета, чтобы быстро находить те, которые более не актуальны
     *
     * @return ZIP-XML: Пакет (хэш содержимого zipXmlPackage). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getZipXmlPackageHash()
     */
    public static PropertyPath<Integer> zipXmlPackageHash()
    {
        return _dslPath.zipXmlPackageHash();
    }

    /**
     * Данные абитуриента
     *
     * @return ZIP-XML: Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getZipXmlPackage()
     */
    public static PropertyPath<byte[]> zipXmlPackage()
    {
        return _dslPath.zipXmlPackage();
    }

    /**
     * Ссылка на последний сформированный пакет, в который помещены данные абитуриента.
     * Используется для отслеживания статуса передачи данных в ФИС по абитуриенту.
     *
     * @return Последний сформированный пакет ФИС, в который помещены данные абитуриента.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSyncPackage()
     */
    public static EnrFisSyncPackage.Path<EnrFisSyncPackage> syncPackage()
    {
        return _dslPath.syncPackage();
    }

    /**
     * Дата, когда пакет syncPackage, был отправлен в ФИС, по пакету был получен ответ и данные абитуриента полностью проимпортировались.
     * Обновляется демоном при получении ответа по пакету.
     *
     * @return Дата, когда данные абитуриента были отправлены в ФИС и прошли обработку.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSyncCompleteDate()
     */
    public static PropertyPath<Date> syncCompleteDate()
    {
        return _dslPath.syncCompleteDate();
    }

    public static class Path<E extends EnrFisEntrantSyncData> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PropertyPath<String> _uid;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Boolean> _successfulImport;
        private PropertyPath<String> _skipReason;
        private PropertyPath<Integer> _zipXmlPackageHash;
        private PropertyPath<byte[]> _zipXmlPackage;
        private EnrFisSyncPackage.Path<EnrFisSyncPackage> _syncPackage;
        private PropertyPath<Date> _syncCompleteDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Идентификатор абитуриента в ФИС. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getUid()
     */
        public PropertyPath<String> uid()
        {
            if(_uid == null )
                _uid = new PropertyPath<String>(EnrFisEntrantSyncDataGen.P_UID, this);
            return _uid;
        }

    /**
     * Дата, когда пакет был изменен в системе
     *
     * @return Дата модификации пакета. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EnrFisEntrantSyncDataGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Успешный результат импорта.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSuccessfulImport()
     */
        public PropertyPath<Boolean> successfulImport()
        {
            if(_successfulImport == null )
                _successfulImport = new PropertyPath<Boolean>(EnrFisEntrantSyncDataGen.P_SUCCESSFUL_IMPORT, this);
            return _successfulImport;
        }

    /**
     * @return Причина пропуска при формировании пакета.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSkipReason()
     */
        public PropertyPath<String> skipReason()
        {
            if(_skipReason == null )
                _skipReason = new PropertyPath<String>(EnrFisEntrantSyncDataGen.P_SKIP_REASON, this);
            return _skipReason;
        }

    /**
     * Хэш пакета, чтобы быстро находить те, которые более не актуальны
     *
     * @return ZIP-XML: Пакет (хэш содержимого zipXmlPackage). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getZipXmlPackageHash()
     */
        public PropertyPath<Integer> zipXmlPackageHash()
        {
            if(_zipXmlPackageHash == null )
                _zipXmlPackageHash = new PropertyPath<Integer>(EnrFisEntrantSyncDataGen.P_ZIP_XML_PACKAGE_HASH, this);
            return _zipXmlPackageHash;
        }

    /**
     * Данные абитуриента
     *
     * @return ZIP-XML: Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getZipXmlPackage()
     */
        public PropertyPath<byte[]> zipXmlPackage()
        {
            if(_zipXmlPackage == null )
                _zipXmlPackage = new PropertyPath<byte[]>(EnrFisEntrantSyncDataGen.P_ZIP_XML_PACKAGE, this);
            return _zipXmlPackage;
        }

    /**
     * Ссылка на последний сформированный пакет, в который помещены данные абитуриента.
     * Используется для отслеживания статуса передачи данных в ФИС по абитуриенту.
     *
     * @return Последний сформированный пакет ФИС, в который помещены данные абитуриента.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSyncPackage()
     */
        public EnrFisSyncPackage.Path<EnrFisSyncPackage> syncPackage()
        {
            if(_syncPackage == null )
                _syncPackage = new EnrFisSyncPackage.Path<EnrFisSyncPackage>(L_SYNC_PACKAGE, this);
            return _syncPackage;
        }

    /**
     * Дата, когда пакет syncPackage, был отправлен в ФИС, по пакету был получен ответ и данные абитуриента полностью проимпортировались.
     * Обновляется демоном при получении ответа по пакету.
     *
     * @return Дата, когда данные абитуриента были отправлены в ФИС и прошли обработку.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData#getSyncCompleteDate()
     */
        public PropertyPath<Date> syncCompleteDate()
        {
            if(_syncCompleteDate == null )
                _syncCompleteDate = new PropertyPath<Date>(EnrFisEntrantSyncDataGen.P_SYNC_COMPLETE_DATE, this);
            return _syncCompleteDate;
        }

        public Class getEntityClass()
        {
            return EnrFisEntrantSyncData.class;
        }

        public String getEntityName()
        {
            return "enrFisEntrantSyncData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
