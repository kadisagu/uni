/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4ExamSubject;

import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author Ekaterina Zvereva
 * @since 16.08.2016
 */
public class EnrFisConverterConfig4ExamSubjectUI extends EnrFisConverterConfigUI
{
    public boolean _withoutFisCatalogs;

    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
    }

    public void onClickAutoSync()
    {
        EnrFisConverterManager.instance().stateExamSubject().doAutoSync();
    }

}