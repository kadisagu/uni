/* $Id: CommonCatalogList.java 6 2012-04-04 12:30:37Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.ui.ItemList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic.ItemListDSHandler;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

@Configuration
public class EnrFisCatalogItemList extends BusinessComponentManager
{
    public static final String ENR_FIS_CATALOG_ITEM_DS = "enrFisCatalogItemDS";
    public static final String ENR_FIS_CATALOG_ITEM_CODE_DS = "enrFisCatalogItemCodeDS";

    @Bean
    public ColumnListExtPoint enrFisCatalogItemDS()
    {
        return columnListExtPointBuilder(ENR_FIS_CATALOG_ITEM_DS)
                .addColumn(textColumn(EnrFisCatalogItem.P_FIS_CATALOG_CODE, EnrFisCatalogItem.fisCatalogCode()).width("1px"))
                .addColumn(textColumn(EnrFisCatalogItem.P_FIS_ITEM_CODE, EnrFisCatalogItem.fisItemCode()).width("1px"))
                .addColumn(textColumn(EnrFisCatalogItem.P_TITLE, EnrFisCatalogItem.title()))
                .addColumn(dateColumn(EnrFisCatalogItem.P_REMOVAL_DATE, EnrFisCatalogItem.removalDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).width("1px"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(selectDS(ENR_FIS_CATALOG_ITEM_CODE_DS, enrFisCatalogItemCodeDSHandler()))
        .addDataSource(this.searchListDS(ENR_FIS_CATALOG_ITEM_DS, this.enrFisCatalogItemDS()).handler(this.enrFisCatalogItemDSHandler()))
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrFisCatalogItemDSHandler() {
        return new ItemListDSHandler(this.getName()).order(EnrFisCatalogItem.fisCatalogCode());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrFisCatalogItemCodeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected void initFromList(Map<Long, Object> resultMap, Object listObj)
            {
                final List<String> codeList = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "c")
                        .where(isNull(property("c", EnrFisCatalogItem.removalDate())))
                        .column(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("c")))
                        .predicate(DQLPredicateType.distinct)
                        .createStatement(getSession()).list();

                Collections.sort(codeList, NumberAsStringComparator.INSTANCE);
                super.initFromList(resultMap, codeList);
            }
        }
        .filtered(true);
    }
}
