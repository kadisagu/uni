/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base;

import com.google.common.collect.Maps;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.IEnrFisConv4DocumentCategoryGen;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public abstract class EnrFisConverterDao4DocumentCategory extends EnrFisConverterBaseDao<PersonDocumentCategory, Long> implements IEnrFisConverterDao4DocumentCategory
{
    @Override
    protected List<PersonDocumentCategory> getEntityList()
    {
        return getEnrDocumentCategory(null);
    }

    @Override
    protected Long getItemMapKey(PersonDocumentCategory entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(getEnrFisConv4DocumentCategoryClass(), "x")
                        .column(property(IEnrFisConv4DocumentCategoryGen.docCategory().id().fromAlias("x")), "at_id")
                        .column(property(IEnrFisConv4DocumentCategoryGen.value().id().fromAlias("x")), "value_id")
                        .createStatement(getSession())));
    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(getEnrFisConv4DocumentCategoryClass(), "x")
                .column(property(IEnrFisConv4DocumentCategoryGen.value().fromAlias("x")))
                .where(isNotNull(property(IEnrFisConv4DocumentCategoryGen.value().fromAlias("x"))))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync()
    {
        List<EnrFisCatalogItem> itemList = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "i")
                .where(isNull(property("i", EnrFisCatalogItem.removalDate())))
                .where(eq(property("i", EnrFisCatalogItem.fisCatalogCode()), value(getCatalogCode())))
                .createStatement(getSession()).list();

        List<PersonDocumentCategory> docCategories = getEntityList();
        Map<PersonDocumentCategory, EnrFisCatalogItem> values = Maps.newHashMap();

        for (final PersonDocumentCategory category : docCategories)
        {
            itemList.stream()
                    .filter(fisItem -> category.getTitle().toUpperCase().equals(fisItem.getTitle().toUpperCase()))
                    .forEach(fisItem -> values.put(category, fisItem));
        }
        this.update(values, false);
    }

    protected List<PersonDocumentCategory> getEnrDocumentCategory(String docTypeCode)
    {
        if (null == docTypeCode)
            throw new UnsupportedOperationException();

        return new DQLSelectBuilder().fromEntity(PersonDocumentCategory.class, "c")
                .where(eq(property("c", PersonDocumentCategory.documentType().code()), value(docTypeCode)))
                .order(property("c", PersonDocumentCategory.title()))
                .createStatement(getSession()).list();
    }

    protected Class<? extends IEnrFisConv4DocumentCategory> getEnrFisConv4DocumentCategoryClass()
    {
        throw new UnsupportedOperationException();
    }
}
