package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true)
})
@State({
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrFisOrgUnitTabUI extends UIPresenter {

    private final OrgUnitHolder holder = new OrgUnitHolder();
    public OrgUnitHolder getHolder() { return this.holder; }
    public OrgUnit getOrgUnit() { return this.getHolder().getValue(); }

    public CommonPostfixPermissionModelBase getSecModel() { return this.getHolder().getSecModel(); }

    private String _selectedTab;
    public String getSelectedTab() { return this._selectedTab; }
    public void setSelectedTab(final String selectedTab) { this._selectedTab = selectedTab; }

    @Override
    public void onComponentRefresh() {
    }

}
