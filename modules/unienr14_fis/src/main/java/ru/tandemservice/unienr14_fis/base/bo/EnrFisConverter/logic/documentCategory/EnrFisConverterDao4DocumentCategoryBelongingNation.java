/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4DocumentCategoryBelongingNation;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base.EnrFisConverterDao4DocumentCategory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public class EnrFisConverterDao4DocumentCategoryBelongingNation extends EnrFisConverterDao4DocumentCategory
{
    @Override
    public String getCatalogCode()
    {
        return "44";
    }

    protected List<PersonDocumentCategory> getEnrDocumentCategory(String entrantDocTypeCode)
    {
        return super.getEnrDocumentCategory(PersonDocumentTypeCodes.BELONGING_NATION);
    }

    protected Class<? extends IEnrFisConv4DocumentCategory> getEnrFisConv4DocumentCategoryClass()
    {
        return EnrFisConv4DocumentCategoryBelongingNation.class;
    }

    @Override
    public void update(final Map<PersonDocumentCategory, EnrFisCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        List<EnrFisConv4DocumentCategoryBelongingNation> targetRecords = values.entrySet().stream()
                .filter(e -> null != e.getValue())
                .map(e -> new EnrFisConv4DocumentCategoryBelongingNation(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4DocumentCategoryBelongingNation.NaturalId, EnrFisConv4DocumentCategoryBelongingNation>()
        {
            @Override protected EnrFisConv4DocumentCategoryBelongingNation.NaturalId key(final EnrFisConv4DocumentCategoryBelongingNation source) { return (EnrFisConv4DocumentCategoryBelongingNation.NaturalId) source.getNaturalId(); }
            @Override protected EnrFisConv4DocumentCategoryBelongingNation buildRow(final EnrFisConv4DocumentCategoryBelongingNation source) { return new EnrFisConv4DocumentCategoryBelongingNation(source.getDocCategory(), source.getValue()); }
            @Override protected void fill(final EnrFisConv4DocumentCategoryBelongingNation target, final EnrFisConv4DocumentCategoryBelongingNation source) { target.update(source, false); }
            @Override protected void doDeleteRecord(final EnrFisConv4DocumentCategoryBelongingNation databaseRecord) { if (clearNulls) { super.doDeleteRecord(databaseRecord); } }
        }.merge(this.getList(EnrFisConv4DocumentCategoryBelongingNation.class), targetRecords);
    }
}