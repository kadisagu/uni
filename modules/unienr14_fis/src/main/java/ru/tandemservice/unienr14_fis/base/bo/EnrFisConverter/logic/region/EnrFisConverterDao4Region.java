/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.region;

import com.google.common.collect.Maps;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.codes.AddressLevelCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.isNotNull;

/**
 * @author nvankov
 * @since 04.07.2016
 */
public class EnrFisConverterDao4Region extends EnrFisConverterBaseDao<AddressItem, Long> implements IEnrFisConverterDao4Region
{

    @Override
    public String getCatalogCode() {
        return "8";
    }

    @Override
    public List<AddressItem> getEntityList() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressItem.class, "ai")
                .where(eq(property("ai", AddressItem.country().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)))
                .where(eq(property("ai", AddressItem.addressType().addressLevel().code()), value(AddressLevelCodes.REGION)))
                .order(property("ai", AddressItem.title()))
                ;
        return getList(builder);
    }

    @Override
    protected Long getItemMapKey(final AddressItem entity) {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap() {
        return EnrFisConverterBaseDao.<Long, Long>map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4Region.class, "x")
                        .column(property(EnrFisConv4Region.addressItem().id().fromAlias("x")), "ac_id")
                        .column(property(EnrFisConv4Region.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<AddressItem, EnrFisCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EnrFisConv4Region> targetRecords =  new ArrayList<EnrFisConv4Region>(values.size());
        for (final Map.Entry<AddressItem, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4Region(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4Region.NaturalId, EnrFisConv4Region>() {
            @Override protected EnrFisConv4Region.NaturalId key(final EnrFisConv4Region source) {
                return (EnrFisConv4Region.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4Region buildRow(final EnrFisConv4Region source) {
                return new EnrFisConv4Region(source.getAddressItem(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4Region target, final EnrFisConv4Region source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4Region databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4Region.class), targetRecords);
    }

    @Override
    public void doAutoSync()
    {
        final List<EnrFisCatalogItem> itemList = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "i")
                .where(isNull(property("i", EnrFisCatalogItem.removalDate())))
                .where(eq(property("i", EnrFisCatalogItem.fisCatalogCode()), value(getCatalogCode())))
                .createStatement(getSession()).list();

        final Map<String, EnrFisCatalogItem> itemMap = Maps.newHashMap();
        for(EnrFisCatalogItem item : itemList)
        {
            itemMap.put(item.getTitle().toUpperCase(), item);
        }

        final List<AddressItem> regionList = this.getEntityList();
        final Map<AddressItem, EnrFisCatalogItem> values = new HashMap<>(regionList.size());

        for (final AddressItem ps : regionList)
        {
            int i = 0;
            EnrFisCatalogItem it = null;
            for (EnrFisCatalogItem item : itemList)
            {
                String[] splited = item.getTitle().split(" ");
                boolean find = false;
                for(String part : splited)
                {
                    if (part.toUpperCase().equals(ps.getTitle().toUpperCase()))
                    {
                        it = item;
                        find = true;
                    }
                }
                if(find)
                {
                    i++;
                }
            }
            if (it != null && i == 1)
            {
                values.put(ps, it);
            }
        }

        this.update(values, false);
    }


    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4Region.class, "b")
                .column(property(EnrFisConv4Region.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4Region.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }
}
