/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.entrantAchievementType;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNotNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexey Lopatin
 * @since 09.06.2015
 */
public class EnrFisConverterDao4EntrantAchievementType extends EnrFisConverterBaseDao<EnrEntrantAchievementType, Long> implements IEnrFisConverterDao4EntrantAchievementType
{
    @Override
    public String getCatalogCode()
    {
        return "36";
    }

    @Override
    protected List<EnrEntrantAchievementType> getEntityList()
    {
        return new DQLSelectBuilder().fromEntity(EnrEntrantAchievementType.class, "at")
                .order(property("at", EnrEntrantAchievementType.achievementKind().requestType().code()))
                .order(property("at", EnrEntrantAchievementType.achievementKind().title()))
                .createStatement(getSession()).list();
    }

    @Override
    protected Long getItemMapKey(EnrEntrantAchievementType entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4EntrantAchievementType.class, "x")
                        .column(property(EnrFisConv4EntrantAchievementType.achievementType().id().fromAlias("x")), "at_id")
                        .column(property(EnrFisConv4EntrantAchievementType.value().id().fromAlias("x")), "value_id")
                        .createStatement(getSession())));
    }

    @Override
    public void update(final Map<EnrEntrantAchievementType, EnrFisCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EnrFisConv4EntrantAchievementType> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<EnrEntrantAchievementType, EnrFisCatalogItem> e : values.entrySet())
        {
            if (null != e.getValue())
            {
                targetRecords.add(new EnrFisConv4EntrantAchievementType(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4EntrantAchievementType.NaturalId, EnrFisConv4EntrantAchievementType>()
        {
            @Override
            protected EnrFisConv4EntrantAchievementType.NaturalId key(final EnrFisConv4EntrantAchievementType source)
            {
                return (EnrFisConv4EntrantAchievementType.NaturalId) source.getNaturalId();
            }

            @Override
            protected EnrFisConv4EntrantAchievementType buildRow(final EnrFisConv4EntrantAchievementType source)
            {
                return new EnrFisConv4EntrantAchievementType(source.getAchievementType(), source.getValue());
            }

            @Override
            protected void fill(final EnrFisConv4EntrantAchievementType target, final EnrFisConv4EntrantAchievementType source)
            {
                target.update(source, false);
            }

            @Override
            protected void doDeleteRecord(final EnrFisConv4EntrantAchievementType databaseRecord)
            {
                if (clearNulls)
                {
                    super.doDeleteRecord(databaseRecord);
                }
            }
        }.merge(this.getList(EnrFisConv4EntrantAchievementType.class), targetRecords);
    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4EntrantAchievementType.class, "b")
                .column(property(EnrFisConv4EntrantAchievementType.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4EntrantAchievementType.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }
}
