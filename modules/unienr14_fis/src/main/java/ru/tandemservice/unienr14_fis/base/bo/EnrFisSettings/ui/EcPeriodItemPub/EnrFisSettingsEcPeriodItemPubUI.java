/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.EcPeriodItemPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.EcPeriodList.EnrFisSettingsEcPeriodList;

/**
 * @author oleyba
 * @since 6/25/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entityId")
})
public class EnrFisSettingsEcPeriodItemPubUI extends UIPresenter
{
    private Long entityId;

    @Override
    public void onComponentRefresh()
    {
        _uiActivation.asDesktopRoot(EnrFisSettingsEcPeriodList.class).activate();
    }

    // getters and setters

    public Long getEntityId()
    {
        return entityId;
    }

    public void setEntityId(Long entityId)
    {
        this.entityId = entityId;
    }
}