/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.ui.EntrantList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List.EnrEntrantListUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic.EnrFisOrgUnitEntrantListDSHandler;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.PackagePub.EnrFisSyncSessionPackagePub;

import java.util.Arrays;

/**
 * @author oleyba
 * @since 7/15/14
 */
@Configuration
public class EnrFisOrgUnitEntrantList extends BusinessComponentManager
{
    public static final String ENTRANT_DS = "entrantDS";
    public static final String ENTRANT_CUSTOM_STATE_DS = "entrantCustomStateDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS("entrantStateDS", entrantStateDSHandler()))
                .addDataSource(selectDS(ENTRANT_CUSTOM_STATE_DS, entrantCustomStateDSHandler()))
                .addDataSource(selectDS("archivalDS", archivalDSHandler()))
                .addDataSource(searchListDS(ENTRANT_DS, entrantDSColumns(), entrantDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint entrantDSColumns()
    {
        return columnListExtPointBuilder(ENTRANT_DS)
            .addColumn(textColumn("personalNumber", EnrEntrant.personalNumber()).order())
            .addColumn(publisherColumn("requestNumber", "title")
                .entityListProperty(EnrEntrantDSHandler.VIEW_PROP_REQUEST)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(publisherColumn("entrant", PersonRole.person().identityCard().fullFio()).required(Boolean.TRUE).parameters("mvel:['selectedTab':'requestTab']").order())
            .addColumn(textColumn("passport", PersonRole.person().fullIdentityCardNumber()))
            .addColumn(textColumn("registrationDate", EnrEntrant.registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(textColumn("customState", EnrEntrantDSHandler.VIEW_PROP_CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
            .addColumn(textColumn("state", EnrEntrant.state().stateDaemonSafe().s()))
            .addColumn(textColumn("fisDate", "fisDate").formatter(DateFormatter.DATE_FORMATTER_WITH_TIME))
            .addColumn(publisherColumn("fisStatus", "title")
                .entityListProperty("fisStatus")
                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return entity.getId() < 0 ? null : EnrFisSyncSessionPackagePub.class.getSimpleName();
                    }
                }))
            .addColumn(textColumn("fisSkipReason", "fisSkipReason"))
        .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler entrantDSHandler()
    {
        return new EnrFisOrgUnitEntrantListDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantState.class)
                .order(EnrEntrantState.title())
                .filter(EnrEntrantState.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantCustomStateType.class)
                .order(EnrEntrantCustomStateType.title())
                .filter(EnrEntrantCustomStateType.title());
    }

    @Bean IDefaultComboDataSourceHandler archivalDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
            .addAll(Arrays.asList(
                new IdentifiableWrapper(EnrEntrantListUI.SHOW_ARCHIVAL_CODE, "Показывать архивных"),
                new IdentifiableWrapper(EnrEntrantListUI.SHOW_NON_ARCHIVAL_CODE, "Показывать не архивных")
            ))
            .filtered(true);
    }
}