/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 7/9/14
 */
public class StateExamMinMarksDSHandler extends DefaultSearchDataSourceHandler
{
    public StateExamMinMarksDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrCampaignId = context.get(EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID);

        if(enrCampaignId == null)
            return ListOutputBuilder.get(input, Collections.emptyList()).build();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsStateExamMinMark.class, "mm");
        builder.where(eq(property("mm", EnrFisSettingsStateExamMinMark.enrollmentCampaign().id()), value(enrCampaignId)));
        builder.order(property("mm", EnrFisSettingsStateExamMinMark.eduProgramSubject().title()));
        builder.order(property("mm", EnrFisSettingsStateExamMinMark.eduProgramSubject().subjectCode()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build().transform((EnrFisSettingsStateExamMinMark minMark) -> {
            ViewWrapper<EnrFisSettingsStateExamMinMark> wrapper = new ViewWrapper<>(minMark);
            wrapper.setViewProperty("exclusive", (minMark != null && minMark.getDiscipline() == null) ? "без ВИ" : "100 баллов");
            return wrapper;
        });
    }
}
