/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4OlympiadSubject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadSubject;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiadSubject.IEnrFisConverterDao4OlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.List;

/**
 * @author oleyba
 * @since 7/17/14
 */
@Configuration
public class EnrFisConverterConfig4OlympiadSubject extends EnrFisConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    protected IEnrFisConverterDao4OlympiadSubject getConverterDao() {
        return EnrFisConverterManager.instance().olympiadSubject();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
        .addColumn(textColumn("title", EnrOlympiadSubject.P_TITLE).create())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4OlympiadSubjectEdit").hasBlockHeader(true).create())
        .create();
    }


    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        final AbstractSearchDataSourceHandler handler = (AbstractSearchDataSourceHandler) super.fisConverterRecordDSHandler();
        return handler.setPageable(false);
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        return entityList;
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return super.fisConverterValueDSHandler();
    }
}
