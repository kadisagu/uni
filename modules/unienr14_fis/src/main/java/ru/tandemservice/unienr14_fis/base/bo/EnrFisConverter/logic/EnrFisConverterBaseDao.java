package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.transaction.DaoCache;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;

/**
 * @author vdanilov
 */
public abstract class EnrFisConverterBaseDao<T extends IEntity, KEY> extends UniBaseDao implements IEnrFisConverterDao<T> {

    /**
     * @return код справочника ФИС, с которым происходит сопоставление
     */
    @Override
    public abstract String getCatalogCode();

    /**
     * @return список сущностей, которые необходимо сопоставить
     */
    protected abstract List<T> getEntityList();

    /**
     * @param entity
     * @return код (для сущности uni), на базе которого осуществляется сопоставление
     */
    protected abstract KEY getItemMapKey(T entity);

    /**
     * @return { код (для сущности uni) -> EnrFisCatalogItem.id }
     */
    protected abstract Map<KEY, Long> getItemMap();


    /**
     * 
     * @param rows [ id(key), id(value) ]
     * @return { id(key) -> id(value) }
     */
    @SuppressWarnings("unchecked")
    protected static <K, V> Map<K, V> map(Iterable<Object[]> rows) {
        final Map<K, V> map = new LinkedHashMap<K, V>();
        for (Object[] row: rows) {
            if (null != map.put((K)row[0], (V)row[1])) {
                throw new IllegalStateException("duplicate value for key=«"+row[0]+"»");
            }
        }
        return map;
    }

    protected IEnrFisConverter<T> buildNewConverter() {
        return new IEnrFisConverter<T>() {
            final List<T> entityList = EnrFisConverterBaseDao.this.getEntityList();
            final Map<KEY, Long> itemIdsMap = EnrFisConverterBaseDao.this.getItemMap();
            final Map<Long, EnrFisCatalogItem> cacheMap = EnrFisConverterBaseDao.this.getLoadCacheMap(EnrFisCatalogItem.class);

            @Override public String getEcfCatalogCode() {  return EnrFisConverterBaseDao.this.getCatalogCode(); }
            @Override public List<T> getEntityList() { return this.entityList; }
            @Override public EnrFisCatalogItem getCatalogItem(final T entity, boolean required) {
                final KEY id = EnrFisConverterBaseDao.this.getItemMapKey(entity);
                final Long itemId = this.itemIdsMap.get(id);
                if (null == itemId) {
                    if (required) {
                        throw new ApplicationException(
                            EnrFisConverterManager.instance().getProperty("error.no-catalog-converter-item", getEcfCatalogCode(), entity.getProperty("title")),
                            new NoSuchElementException(String.valueOf(id))
                        );
                    }
                    return null;
                }
                return this.cacheMap.get(itemId);
            }
        };
    }

    @Override
    public IEnrFisConverter<T> getConverter() {
        final String key = "converter."+this.getClass().getName();
        IEnrFisConverter<T> converter = DaoCache.get(key);
        if (null == converter) {
            DaoCache.put(key, converter = this.buildNewConverter());
        }
        return converter;
    }


    @Override
    public void update(T key, EnrFisCatalogItem value) {
        final IEnrFisConverter<T> converter = this.getConverter();
        final List<T> entityList = this.getEntityList();
        final Map<T, EnrFisCatalogItem> values = new HashMap<T, EnrFisCatalogItem>(entityList.size());
        for (final T entity: entityList) {
            final EnrFisCatalogItem current = converter.getCatalogItem(entity, false);
            if (null != current) {
                values.put(entity, current);
            }
        }
        values.put(key, value);
        update(values, true);
    }

    @Override
    public void update(Map<T, EnrFisCatalogItem> values, boolean clearNulls) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void validateUsedInUserConverter(Map<Long, String> itemMap)
    {
        for (EnrFisCatalogItem item : getUsedFisItemList())
        {
            // используемый элемент не пришел в ответе
            if (!itemMap.containsKey(item.getFisID())) {
                UserContext.getInstance().getErrorCollector().add(
                    EnrFisConverterManager.instance().getProperty("error.update.catalog-item-is-in-use")
                );
                throw new IllegalStateException("fisCatalogItem(catalog="+getCatalogCode()+", code="+item.getFisItemCode()+"): used fis catalog item has been deleted");
            }

            // у используемого элемента в ответе поменялось название
            final String title = itemMap.get(item.getFisID());
            final String expectedTitle = item.getTitle();
            if (!expectedTitle.equals(title)) {
                UserContext.getInstance().getErrorCollector().add(
                    EnrFisConverterManager.instance().getProperty("error.update.catalog-item-is-in-use")
                );
                throw new IllegalStateException("fisCatalogItem(catalog="+getCatalogCode()+", code="+item.getFisItemCode()+"): used fis catalog item title mismatch: title=«"+title+"», expectedTitle=«"+expectedTitle+"»");
            }
        }
    }

    /**
     * @return спикок элементов справочника ФИС, которые поиспользованы в пользовательских конвертерах
     */
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        throw new UnsupportedOperationException("Need to be implemented. Don't used for not user converters.");
    }
}
