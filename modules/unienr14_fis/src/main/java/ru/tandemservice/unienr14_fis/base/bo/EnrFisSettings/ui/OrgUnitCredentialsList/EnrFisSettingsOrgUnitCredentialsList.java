/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.OrgUnitCredentialsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.ext.OrgUnit.ui.View.OrgUnitViewExt;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/2/14
 */
@Configuration
public class EnrFisSettingsOrgUnitCredentialsList extends BusinessComponentManager {

    public static final String ENR_ORG_UNIT_DS = "enrOrgUnitDS";
    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrCampaign";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(this.searchListDS(ENR_ORG_UNIT_DS, this.EnrOrgUnitListDS()).handler(this.EnrOrgUnitDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint EnrOrgUnitListDS() {
        final IPublisherLinkResolver publisherLinkResolver = new SimplePublisherLinkResolver(EnrOrgUnit.institutionOrgUnit().orgUnit().id())
            .param(OrgUnitViewUI.SELECTED_PAGE, OrgUnitViewExt.TAB_ENR_FIS_ORG_UNIT);
        return this.columnListExtPointBuilder(ENR_ORG_UNIT_DS)
        .addColumn(bcLinkColumn("title", EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle()).linkResolver(publisherLinkResolver).order().create())
        .addColumn(textColumn("login", EnrOrgUnit.login()).create())
        .addColumn(textColumn(EnrOrgUnit.P_FIS_UID, EnrOrgUnit.fisUid()).create())
        .addColumn(booleanColumn("fisDataSendingIndependent", EnrOrgUnit.fisDataSendingIndependent()).create())
        .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEdit").create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> EnrOrgUnitDSHandler() {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(this.getName()) {
            private final DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(EnrOrgUnit.class, "e");
            @Override protected DSOutput execute(final DSInput input, final ExecutionContext context) {
                final DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EnrOrgUnit.class, "e").column("e")
                    .where(eq(property(EnrOrgUnit.enrollmentCampaign().fromAlias("e")), commonValue(context.get(BIND_ENROLLMENT_CAMPAIGN))))
                    ;
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(this.orderRegistry).build();
            }
        };
    }
}
