package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.PackagePub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisSyncSessionPackagePub extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }
}
