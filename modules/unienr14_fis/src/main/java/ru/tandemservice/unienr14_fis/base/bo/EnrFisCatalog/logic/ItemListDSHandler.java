package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.logic;

import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public class ItemListDSHandler extends EntityComboDataSourceHandler {

    public static final String PARAM_CATALOG_CODE = "catalogCode";
    public static final String PARAM_CATALOG_ITEM_TITLE = "catalogItemTitle";


    public ItemListDSHandler(final String ownerId) {
        super(ownerId, EnrFisCatalogItem.class);
        this.filter(EnrFisCatalogItem.title());
    }

    @Override
    protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);

        final DataWrapper code = context.get(PARAM_CATALOG_CODE);
        final String title = context.get(PARAM_CATALOG_ITEM_TITLE);

        if (code != null) {
            dql.where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias(alias)), value(code.getTitle())));
        }

        if (title != null) {
            dql.where(like(alias, EnrFisCatalogItem.title(), title));
        }
    }

    @Override
    protected void execute(final DQLSelectBuilder builder, final String alias, final DSOutput output, ExecutionContext context)
    {
        // находим все id
        final DQLExecutionContext qdlContext = new DQLExecutionContext(context.getSession());
        final Iterable<Object[]> rows = CommonDAO.scrollRows(
            builder
            .column(property(alias, "id"))
            .column(property(alias, EnrFisCatalogItem.fisCatalogCode()))
            .column(property(alias, EnrFisCatalogItem.fisItemCode()))
            .createStatement(qdlContext)
        );

        // сортируем id
        final List<Long> ids = UniBaseDao.getSortedIds(rows, NumberAsStringComparator.INSTANCE, NumberAsStringComparator.INSTANCE);
        output.setTotalSize(ids.size());

        // выбираем область
        final List<Long> subIds = ids.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), ids.size()));
        output.addRecords(CommonDAO.sort(IUniBaseDao.instance.get().getList(EnrFisCatalogItem.class, "id", subIds), subIds));
    }

}
