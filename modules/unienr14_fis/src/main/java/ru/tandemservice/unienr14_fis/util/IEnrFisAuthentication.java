package ru.tandemservice.unienr14_fis.util;

/**
 * @author vdanilov
 */
public interface IEnrFisAuthentication {

    String getLogin();

    String getPassword();

}
