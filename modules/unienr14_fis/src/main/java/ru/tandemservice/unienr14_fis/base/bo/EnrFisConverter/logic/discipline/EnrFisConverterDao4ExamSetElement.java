/**
 *$Id: EnrFisConverterDao4ExamSetElement.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.discipline;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.unienr14.exams.entity.gen.IEnrExamSetElementValueGen;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.gen.EnrFisCatalogItemGen;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ExamSetElement;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 18.07.13
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15664939")
public class EnrFisConverterDao4ExamSetElement extends EnrFisConverterBaseDao<IEnrExamSetElementValue, Long> implements IEnrFisConverterDao4ExamSetElement
{
    @Override
    public String getCatalogCode()
    {
        return "1";
    }

    @Override
    protected List<IEnrExamSetElementValue> getEntityList()
    {
        return new DQLSelectBuilder().fromEntity(IEnrExamSetElementValue.class, "x").column(property("x"))
                .order(property(IEnrExamSetElementValueGen.enrollmentCampaign().educationYear().intValue().fromAlias("x")))
                .order(property(IEnrExamSetElementValueGen.enrollmentCampaign().title().fromAlias("x")))
                .createStatement(getSession()).list();
    }

    @Override
    protected Long getItemMapKey(IEnrExamSetElementValue entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4ExamSetElement.class, "x")
                        .column(property(EnrFisConv4ExamSetElement.examSetElement().id().fromAlias("x")), "dsc_id")
                        .column(property(EnrFisConv4ExamSetElement.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())));
    }

    @Override
    public void update(final Map<IEnrExamSetElementValue, EnrFisCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EnrFisConv4ExamSetElement> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<IEnrExamSetElementValue, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4ExamSetElement(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4ExamSetElement.NaturalId, EnrFisConv4ExamSetElement>() {
            @Override protected EnrFisConv4ExamSetElement.NaturalId key(final EnrFisConv4ExamSetElement source) {
                return (EnrFisConv4ExamSetElement.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4ExamSetElement buildRow(final EnrFisConv4ExamSetElement source) {
                return new EnrFisConv4ExamSetElement(source.getExamSetElement(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4ExamSetElement target, final EnrFisConv4ExamSetElement source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4ExamSetElement databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4ExamSetElement.class), targetRecords);

    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4ExamSetElement.class, "b").column(property(EnrFisConv4ExamSetElement.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4ExamSetElement.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync(EnrEnrollmentCampaign ec)
    {
        // поднимаем Дисциплины ПК, для которых нет сопоставления с Элементом справочника ФИС
        final DQLSelectBuilder disciplineDQL = new DQLSelectBuilder().fromEntity(EnrCampaignDiscipline.class, "d").column(property("d"))
                .where(notIn(property(EnrCampaignDiscipline.id().fromAlias("d")),
                        new DQLSelectBuilder().fromEntity(EnrFisConv4ExamSetElement.class, "c").column(property(EnrFisConv4ExamSetElement.examSetElement().id().fromAlias("c")))
                                .buildQuery()));
        if (ec != null)
            disciplineDQL.where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("d")), value(ec)));

        final Map<IEnrExamSetElementValue, EnrFisCatalogItem> values = new HashMap<>();
        for (EnrCampaignDiscipline discipline: disciplineDQL.createStatement(getSession()).<EnrCampaignDiscipline>list())
        {
            if (discipline.getStateExamSubject() == null) { continue; }
            String fisItemCode = StringUtils.trimToNull(discipline.getStateExamSubject().getSubjectFISCode());
            if (null == fisItemCode) { continue; }
            EnrFisCatalogItem item = getByNaturalId(new EnrFisCatalogItemGen.NaturalId(getCatalogCode(), fisItemCode));
            if (null == item || null != item.getRemovalDate()) { continue; }

            values.put(discipline, item);
        }

        // для тех для которых уже выбрали - не меняем
        for (EnrFisConv4ExamSetElement discipline : getList(EnrFisConv4ExamSetElement.class))
        {
            if (discipline.getValue() != null)
                values.put(discipline.getExamSetElement(), discipline.getValue());
        }

        this.update(values, false);
    }
}
