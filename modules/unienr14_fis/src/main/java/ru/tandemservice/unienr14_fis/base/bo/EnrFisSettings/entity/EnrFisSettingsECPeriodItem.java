package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.gen.*;

/**
 * Даты приемной кампании ФИС (период)
 */
public class EnrFisSettingsECPeriodItem extends EnrFisSettingsECPeriodItemGen implements Comparable<EnrFisSettingsECPeriodItem>, ITitled
{

    public EnrFisSettingsECPeriodItem() {}

    public EnrFisSettingsECPeriodItem(EnrFisSettingsECPeriodItem source) {
        this.update(source, true);
    }

    public EnrFisSettingsECPeriodItem(EnrFisSettingsECPeriodKey key, int stage) {
        this.setKey(key);
        this.setStage(stage);
    }

    @Override
    public int compareTo(EnrFisSettingsECPeriodItem o)
    {
        // если это одно и то же, то 0
        if (this.equals(o)) {
            return 0;
        }

        int result;

        // по ключу
        if (0 != (result = this.getKey().compareTo(o.getKey()))) {
            return result;
        }

        // по номеру этапа
        return (this.getStage() - o.getStage());
    }


    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getKey() == null) {
            return this.getClass().getSimpleName();
        }
        if (getKey().getSize() == 1) { return ""; }
        return getStage() + " этап";
    }

    @Override
    @EntityDSLSupport
    public String getDateStartTitle() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateStart());
    }

    @Override
    @EntityDSLSupport
    public String getDateEndTitle() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateEnd());
    }

    @Override
    @EntityDSLSupport
    public String getDateOrderTitle() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateOrder());
    }


}