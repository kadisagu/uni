package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.List4OrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.button.IButtonListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.EnrFisOrgUnitSyncSessionDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisSyncSessionList4OrgUnit extends BusinessComponentManager {

    public static final String ENRFIS_ORG_UNIT_SYNC_SESSION_DS = "enrFisOrgUnitSyncSessionDS";
    public static final String ENRFIS_ORG_UNIT_SYNC_SESSION_BL = "enrFisOrgUnitSyncSessionActions";

    public static final String DS_ENR_CAMPAIGN = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_ENR_CAMPAIGN, getName(), EnrEnrollmentCampaign.permissionSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    OrgUnit orgUnit = context.get(EnrFisOrgUnitSyncSessionDSHandler.PARAM_ORG_UNIT);
                    return dql.where(exists(new DQLSelectBuilder()
                            .fromEntity(EnrOrgUnit.class, "ou")
                            .where(eq(property("ou", EnrOrgUnit.enrollmentCampaign()), property(alias)))
                            .where(eq(property("ou", EnrOrgUnit.institutionOrgUnit().orgUnit()), value(orgUnit)))
                            .where(or(
                                    eq(property("ou", EnrOrgUnit.fisDataSendingIndependent()), value(Boolean.TRUE)),
                                    isNull(property("ou", EnrOrgUnit.institutionOrgUnit().orgUnit().parent()))
                            ))
                            .buildQuery()));
                })))
        .addDataSource(this.searchListDS(ENRFIS_ORG_UNIT_SYNC_SESSION_DS, this.enrFisOrgUnitSyncSessionDS()).handler(enrFisOrgUnitSyncSessionHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint enrFisOrgUnitSyncSessionDS() {
        return this.columnListExtPointBuilder(ENRFIS_ORG_UNIT_SYNC_SESSION_DS)
        .addColumn(textColumn("title", EnrFisSyncSession.title()).clickable(true).create())
        .addColumn(textColumn("contentStatus", EnrFisSyncSession.contentStatus()).formatter(NewLineFormatter.NOBR_IN_LINES).create())
        .addColumn(textColumn("stateTitle", EnrFisSyncSession.stateTitle()).create())
        .addColumn(textColumn("datelog", EnrFisSyncSession.dateLog()).formatter(NewLineFormatter.NOBR_IN_LINES).create())
        .addColumn(textColumn("iolog", "iolog").formatter(NewLineFormatter.NOBR_IN_LINES).create())
        .addColumn(blockColumn("actions", "actionsColumnBlock").hasBlockHeader(Boolean.TRUE).width("1px").permissionKey("ui:holder.secModel.orgUnit_editEnrFisSync"))
        .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> enrFisOrgUnitSyncSessionHandler() {
        return new EnrFisOrgUnitSyncSessionDSHandler(getName());
    }

    @Bean
    public ButtonListExtPoint enrFisOrgUnitSyncSessionActions() {
        IButtonListExtPointBuilder builder = buttonListExtPointBuilder(ENRFIS_ORG_UNIT_SYNC_SESSION_BL);
        builder.addButton(submitButton("addSession", "onClickAddSession").permissionKey("ui:holder.secModel.orgUnit_addEnr14FisSync"));
        builder.addButton(submitButton("getSessionsXml", "onClickGetSessionsXml").visible("ui:offlineMode").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        builder.addButton(submitButton("importSessionsXmlResults", "onClickImportSessionsXmlResults").visible("ui:offlineMode").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        //        builder.addButton(submitButton("addPkg4Admission", "onClickAddPkg4Admission").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        //        builder.addButton(submitButton("addPkg4Applications", "onClickAddPkg4Applications").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        //        builder.addButton(submitButton("addPkg4AdmissionOrders", "onClickAddPkg4AdmissionOrders").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        return builder.create();
    }




}
