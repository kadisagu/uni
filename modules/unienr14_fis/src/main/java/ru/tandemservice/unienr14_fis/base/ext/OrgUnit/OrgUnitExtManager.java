/* $Id: OrgUnitExtManager.java 22487 2012-04-04 13:16:00Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14_fis.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
