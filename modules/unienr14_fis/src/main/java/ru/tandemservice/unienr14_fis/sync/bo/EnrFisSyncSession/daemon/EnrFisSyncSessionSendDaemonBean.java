package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.daemon;

import enr14fis.schema.pkg.result.resp.ImportResultPackage;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.*;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.EnrFisOfflineDao;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;
import ru.tandemservice.unienr14_fis.util.IEnrFisService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author vdanilov
 */
public class EnrFisSyncSessionSendDaemonBean extends UniBaseDao implements IEnrFisSyncSessionSendDaemonBean
{
    public static final int ITERATIONS_GLOBAL = 4;  // количество итераций демона (циплов чтение-запись) за один такст - чем их больше, тем больше кэш сессии
    public static final int ITERATIONS_SEND   = 4;  // число сессий, передаваемых при одном цикле демона
    public static final int ITERATIONS_RECV   = 16; // число сессий, принимаемых при одном цикле демона

    // при старте приложения демон активируется только, если debug-режим выключен (только на боевых серверах)
    private static volatile boolean active = !Debug.isEnabled(); // здесь должно быть именно условие на дебаг (кнопка включения демона, она же кнопка отправки пакетов, доступна только в дебаг-режиме)

    // активизирует демона (если он был выключен)
    public static void activateAndWakeUp() {
        active = true;
        DAEMON.wakeUpDaemon(); // сдесь будет сброшен интервал до исходного
    }

    public static boolean isActive() {
        return active;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(EnrFisSyncSessionSendDaemonBean.class.getName(), 2 /* две минуты, затем интервал будет увеличиваться при простое */) {
        @Override protected void main()
        {
            // проверяем активность демона и режим ФИС
            if (!active || EnrFisOfflineDao.isFisOfflineMode()) {
                this.setupCurrentTimeout(ONE_MINUTE*60*24); // если он не активен или ФИС в режиме оффлайн, зачем запускать демон часто
                return;
            }

            // проверяем, что передавать вообще есть куда
            if (!IEnrFisService.instance.get().isEnabled()) {
                active = false; // если нет - то выключаем
                return;
            }

            // поехали
            final IEnrFisSyncSessionSendDaemonBean dao = IEnrFisSyncSessionSendDaemonBean.instance.get();

            int global_iterations = ITERATIONS_GLOBAL;
            boolean activity_global_send = false;
            boolean activity_global_recv = false;
            while (global_iterations --> 0) {
                boolean activity_recv = false;
                boolean activity_send = false;

                // сначала пробуем передать данные в ФИС
                Debug.begin("send-pkg");
                try {
                    int iterations = ITERATIONS_SEND;
                    final List<EnrFisSyncSession> sessionList = dao.getQueuedSessions4Send(); // фиксируем список на момент запуска
                    while ((sessionList.size() > 0) && (iterations --> 0)) {
                        for (final Iterator<EnrFisSyncSession> it = sessionList.iterator(); it.hasNext(); ) {
                            if (!this.send(it.next())) { it.remove(); }
                            else { activity_send = activity_global_send = true; }
                        }
                    }
                } catch (final Exception t) {
                    Debug.exception(t.getMessage(), t);
                } finally {
                    Debug.end();
                }

                // затем пробуем получить ответ
                Debug.begin("recv-pkg");
                try {
                    int iterations = ITERATIONS_RECV;
                    final List<EnrFisSyncSession> sessionList = dao.getQueuedSessions4Recv(); // фиксируем список на момент запуска
                    while ((sessionList.size() > 0) && (iterations --> 0)) {
                        for (final Iterator<EnrFisSyncSession> it = sessionList.iterator(); it.hasNext(); ) {
                            if (!this.recv(it.next())) { it.remove(); }
                            else { activity_recv = activity_global_recv = true; }
                        }
                    }
                } catch (final Exception t) {
                    Debug.exception(t.getMessage(), t);
                } finally {
                    Debug.end();
                }

                // сбрасываем пакеты, которые уже прошли обработку
                Debug.begin("do-archive");
                try {
                    dao.doArchiveSessions();
                } catch (final Exception t) {
                    Debug.exception(t.getMessage(), t);
                } finally {
                    Debug.end();
                }

                // если никаких действий в данном цикле не совершалось - выходим
                if (!activity_send && !activity_recv) { break; }

                // если что-то делалось - то пинаем повторяем операции (ждем 1 секунду и начинаем все с начала)
                if (global_iterations > 0) {
                    try { Thread.sleep(ONE_SECOND); }
                    catch (InterruptedException t) { return; }
                }
            }

            // корректируем времени следующего запуска демона
            if (global_iterations <= 0)
            {
                // если демон выбрал все итерации, то следующий раз запускаем демон как можно раньше
                this.setupCurrentTimeout(ONE_MINUTE);
            }
            else if (activity_global_send)
            {
                // если мы что-то отправили, то следующий wake-up должен случиться раньше (через минуту - это меньше, чем стандартный интервал)
                this.setupCurrentTimeout(ONE_MINUTE);
            }
            else if (activity_global_recv)
            {
                // если мы только получали данные (но не отправляем) - сбрасываем интервал до стандартного (он больше одной минуты)
                this.timeoutReset();
            }
            else
            {
                // если никаких действий не совершалось - увеличиваем интервал
                this.timeoutIncrease();
                final int threshold = ONE_MINUTE*10;
                if (this.getTimeoutCurrent() > threshold) {
                    this.setupCurrentTimeout(threshold);
                }
            }
        }

        // пробует послать один пакет из сессии
        private boolean send(final EnrFisSyncSession fisSession)
        {
            final IEnrFisSyncSessionSendDaemonBean dao = IEnrFisSyncSessionSendDaemonBean.instance.get();
            try
            {
                final Long nextPackageId = dao.getNextSendQueuePackageId(fisSession);
                if (null == nextPackageId) { return false; } // нечего отправлять

                try {
                    if (!dao.tryToSendPackage(fisSession, nextPackageId)) {
                        return false; // ошибка
                    }
                } catch (final Exception t) {
                    final String message = t.getMessage()+": package-id="+nextPackageId;
                    this.logger.error(message, t);
                    Debug.exception(message, t);
                    dao.error(
                        fisSession.getId(), false /* произошла какая-то ошибка, что делать - хз (но блокировать сразу - нельзя) */,
                        EnrFisSyncSessionManager.instance().getProperty("error.sync.send", Long.toString(nextPackageId, 32)),
                        t
                    );
                    return false; // ошибка
                }

                return true; // все, что хотели передать - передали
            }
            catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
                return false; // ошибка
            }
        }

        // пробует получить ответ на один пакет из сессии
        private boolean recv(final EnrFisSyncSession fisSession)
        {
            final IEnrFisSyncSessionSendDaemonBean dao = IEnrFisSyncSessionSendDaemonBean.instance.get();
            try
            {
                final Long nextPackageId = dao.getNextRecvQueuePackageId(fisSession);
                if (null == nextPackageId) { return false; } // нечего отправлять

                try {
                    if (!dao.tryToRecvPackage(fisSession, nextPackageId)) { return false;}
                } catch (final Exception t) {
                    final String message = t.getMessage()+": package-id="+nextPackageId;
                    this.logger.error(message, t);
                    Debug.exception(message, t);
                    dao.error(
                        fisSession.getId(), false /* произошла какая-то ошибка, что делать - хз (но блокировать сразу - нельзя) */,
                        EnrFisSyncSessionManager.instance().getProperty("error.sync.recv", Long.toString(nextPackageId, 32)),
                        t
                    );
                    return false; // ошибка
                }

                return true; // все, что хотели получить - передали
            }
            catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
                return false; // ошибка
            }
        }
    };

    @Override
    protected void initDao() {
        DAEMON.registerWakeUpListener4Class(EnrFisSyncSession.class);
        DAEMON.registerWakeUpListener4Class(EnrFisSyncPackage.class);
    }


    ///////////////////////////////////////////////////////


    @Override
    public void error(final Long fisSessionId, final boolean fatal, final String message, final Throwable t) {
        EnrFisSyncSession fisSession = get(EnrFisSyncSession.class, fisSessionId);
        fisSession = refresh(fisSession);
        fisSession.error(EnrFisSyncSessionManager.instance().getProperty("title.sync"), message, t);
        // если ошибка фатальная - исключаем из обработки сессию
        if (fatal)
        {
            fisSession.error(EnrFisSyncSessionManager.instance().getProperty("title.sync"), EnrFisSyncSessionManager.instance().getProperty("error.sync.fatal-and-archive"),
                // сохраняем в лог не только информацию о ошибке, но и traceback - но не для ошибок ФИС, чтобы не создавать у пользователей впечатления, что это наши ошибки
                t instanceof IEnrFisService.EnrFisError ? null : t);
            fisSession.setArchiveDate(new Date());
        }
        saveOrUpdate(fisSession);
        getSession().flush();
    }


    protected interface CallResult<T> {
        Long getLogEntryId();
        T getResult();
    }

    /* фис-о-вызыватель (по завершению операции открывает транзакцию и добавляет все в базу) */
    protected <T> CallResult<T> call(final EnrFisSyncSession fisSession, final EnrFisSyncPackageIOLog io, final Class<T> responseClass, final String urlPostfix, final Object root, final Long packageId)
    {
        Debug.begin("EnrFisSyncSessionDaemonBean.call");
        try {
            final T result;
            io.setOperationDate(new Date());

            final IEnrFisService.EnrFisConnection connection = IEnrFisService.instance.get().connect(urlPostfix);
            try
            {
                // формируем данные для отправки
                final byte[] xmlRequest = EnrFisServiceImpl.toXml(root);
                io.setXmlRequest(xmlRequest);

                // сохранем результат вызова
                final byte[] xmlResponse = connection.call(xmlRequest);
                io.setXmlResponse(xmlResponse);

                // расшифровываем результат вызова
                result = EnrFisServiceImpl.fromXml(responseClass, xmlResponse);
            }
            catch (final IEnrFisService.EnrFisError e)
            {
                // ошибка ФИС
                io.setFisErrorText(e.getMessage());
                io.registerError(e);

                // регистрируем ошибку
                IEnrFisSyncSessionSendDaemonBean.instance.get().error(
                    fisSession.getId(),
                    e.isFatal() /* если ошибка фатальная, то дальнейшую передачу пакетов проводить смысла нет */,
                    EnrFisSyncSessionManager.instance().getProperty("error.sync.fisError", Long.toString(packageId, 32), e.getMessage()),
                    e
                );
                return null;
            }
            catch (final Exception t)
            {
                // иная ошибка - придется сохранять stackTrace
                io.registerError(t);

                // (если info включено, формируем более подробное сообщение - в debug-целях)
                if (Debug.isEnabled() && this.logger.isInfoEnabled())
                {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("\nec=").append(fisSession.getEnrOrgUnit().getEnrollmentCampaign().getTitle()).append(", ecouid=").append(fisSession.getEnrOrgUnit().getId());
                    sb.append("\nurl=").append(urlPostfix);
                    sb.append("\nrequest=").append(connection.getRequest());
                    sb.append("\nresponse=").append(connection.getResponse());
                    sb.append("\nerror-class=").append(t.getClass());
                    sb.append("\nerror-message=").append(t.getMessage());
                    final String message = sb.toString();
                    Debug.exception(message, t);
                    this.logger.error(message, t);

                    IEnrFisSyncSessionSendDaemonBean.instance.get().error(
                        fisSession.getId(), isFatalError(t) /* фатальность определяем по типу ошибки */,
                        EnrFisSyncSessionManager.instance().getProperty("error.sync.io", Long.toString(packageId, 32), message),
                        t
                    );
                    return null;
                }

                // иначе регистрируем просто ошибку с простым текстом
                Debug.exception(t.getMessage(), t);
                IEnrFisSyncSessionSendDaemonBean.instance.get().error(
                    fisSession.getId(), isFatalError(t) /* фатальность определяем по типу ошибки */,
                    EnrFisSyncSessionManager.instance().getProperty("error.sync.io", Long.toString(packageId, 32), t.getMessage()),
                    t
                );
                return null;
            }
            finally
            {
                // после того, как произошло обращение к ФИС, сохраняем в базу и возвращаем объект
                // в отдельной транзакции (сохраняем операцию в базу), ВСЕГДА
                io.setOperationUrl(connection.getUrl().toString());
                IUniBaseDao.instance.get().doInTransaction(session -> {
                    io.setOwner((EnrFisSyncPackage)session.load(EnrFisSyncPackage.class, packageId));
                    session.save(io);

                    if (io.isErrorByZipLog())
                    {
                        // если это ошибка
                        // извлекаем число операций того же типа, по их количеству определяем задержку исполнения следующего пакета
                        final Date nextOperationThresholdDate = getNextOperationThresholdDate(
                            new DQLSelectBuilder()
                            .fromEntity(ClassUtils.getUserClass(io), "io")
                            .column(count("io.id"))
                            .where(eq(property(EnrFisSyncPackageIOLog.owner().id().fromAlias("io")), value(packageId)))
                            .createStatement(getSession())
                            .<Number>uniqueResult()
                        );

                        // обновляем объект в базе (там стоит optimistic-lock=false, так что ок)
                        executeAndClear(
                            new DQLUpdateBuilder(EnrFisSyncPackage.class)
                            .set(EnrFisSyncPackage.P_NEXT_OPERATION_THRESHOLD_DATE, valueTimestamp(nextOperationThresholdDate))
                            .where(or(
                                    isNull(property(EnrFisSyncPackage.P_NEXT_OPERATION_THRESHOLD_DATE)),
                                    lt(property(EnrFisSyncPackage.P_NEXT_OPERATION_THRESHOLD_DATE), valueTimestamp(nextOperationThresholdDate))
                            ))
                            .where(eq(property(EnrFisSyncPackage.P_ID), value(packageId)))
                        );
                    }
                    else
                    {
                        // сбрасываем в базе метку времени следующего выполнения (как только, так сразу)
                        // обновляем объект в базе (там стоит optimistic-lock=false, так что ок)
                        executeAndClear(
                            new DQLUpdateBuilder(EnrFisSyncPackage.class)
                            .set(EnrFisSyncPackage.P_NEXT_OPERATION_THRESHOLD_DATE, nul(PropertyType.TIMESTAMP))
                            .where(isNotNull(property(EnrFisSyncPackage.P_NEXT_OPERATION_THRESHOLD_DATE)))
                            .where(eq(property(EnrFisSyncPackage.P_ID), value(packageId)))
                        );
                    }

                    return null;
                });
            }

            final Long id = io.getId();
            return new CallResult<T>() {
                @Override public Long getLogEntryId() { return id; }
                @Override public T getResult() { return result; }
            };

        } finally {
            Debug.end();
        }
    }

    /* @return true, если дальнейшие действия с пакетам бессмысленны */
    private boolean isFatalError(Throwable t) {
        while (null != t) {
            if (t instanceof java.io.IOException) { return false; /* если это передача данных, то надо попробовать еще раз */ }
            t = t.getCause();
        }

        // иначе - все плохо (если xml не парсится сейчас, то он не будет парсится и дальше)
        return true;
    }

    // задержки в минутах в зависимости от количества операций того же типа в пакете (номер текущей попытки отправить сообщение - 1)
    private static final int[] NEXT_OPERATION_THRESHOLD_MINUTES = { 2, 5, 10, 15, 30 };
    private Date getNextOperationThresholdDate(Number prevIoLogCount) {
        int index = Math.min((null == prevIoLogCount ? 0 : prevIoLogCount.intValue()), NEXT_OPERATION_THRESHOLD_MINUTES.length-1);
        int threshold = NEXT_OPERATION_THRESHOLD_MINUTES[index];
        return DateUtils.addMinutes(new Date(), threshold);
    }

    // количество ошибок одного типа по пакету, до того как вся сессия будет отправлена в архив
    //public static final int ERROR_THRESHOLD = 300; /* 1+NEXT_OPERATION_THRESHOLD_MINUTES.length */;
    // TEMP FIX
    public static final int ERROR_THRESHOLD = 300;

    ///////////////////////////////////////////////////////


    @Override
    public List<EnrFisSyncSession> getQueuedSessions4Send()
    {
        final Date now = new Date();

        final List<EnrFisSyncSession> list = new DQLSelectBuilder()
        .fromEntity(EnrFisSyncSession.class, "s")
        .column(property("s"))
        .order(property(EnrFisSyncSession.creationDate().fromAlias("s")))
        .order(property(EnrFisSyncSession.id().fromAlias("s")))
        .where(isNull(property(EnrFisSyncSession.archiveDate().fromAlias("s"))))
        .where(isNotNull(property(EnrFisSyncSession.queueDate().fromAlias("s"))))

        // есть пакеты, которые надо отправлять И нет пакета, который надо отправлять,с ограничением больше текущего времени
        .where(and(
                exists(
                        new DQLSelectBuilder()
                                .fromEntity(EnrFisSyncPackage.class, "p")
                                .column(property("p.id"))
                                .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), property("s")))
                                .where(isNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p"))))
                                .buildQuery()
                )
                ,
                notExists(
                        new DQLSelectBuilder()
                                .fromEntity(EnrFisSyncPackage.class, "p")
                                .column(property("p.id"))
                                .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), property("s")))
                                .where(isNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p"))))
                                .where(gt(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p")), valueTimestamp(now)))
                                .buildQuery()
                )
        ))
        .createStatement(getSession()).list();

        // проверяем, что в рамках ПК только одна сессия
        final Map<EnrOrgUnit, EnrFisSyncSession> testMap = new HashMap<>();
        for (final EnrFisSyncSession s: list) {
            if (null != testMap.put(s.getEnrOrgUnit(), s)) {
                throw new IllegalStateException("send-queue-is-not-unique: "+s.getEnrOrgUnit().getTitle());
            }
        }

        return list;
    }

    @Override
    public Long getNextSendQueuePackageId(final EnrFisSyncSession fisSession)
    {
        final Session session = getSession();

        // нужно получить перечень отправленных пакетов, по которым еще не получены ответы
        final Set<Long> unrecvEntityCodes = new HashSet<>(new DQLSelectBuilder()
        .fromEntity(EnrFisSyncPackage.class, "p")
        .column(property("p", EnrFisSyncPackage.clazz()))
        .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), value(fisSession)))
        .where(isNotNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p")))) /* передан в ФИС */
        .where(isNull(property(EnrFisSyncPackage.pkgRecvSuccess().fromAlias("p")))) /* ответ от ФИС еще не получен */
        .predicate(DQLPredicateType.distinct)
        .createStatement(session)
        .<Long>list());

        if (unrecvEntityCodes.size() > 1) {
            // это значит, что у нас есть больше одно типа переданных пакетов (передавать еще пакеты в такой ситуации нельзя)
            return null;
        }

        // нужно получить первый пакет, который можно передать
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrFisSyncPackage.class, "p")
        .column(property("p.id"))
        .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), value(fisSession)))
        .where(isNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p")))) /* пакет еще не передан в ФИС */
        .order(property(EnrFisSyncPackage.creationDate().fromAlias("p")) /* сортируем по дате формирования (считаем, что они сформированы правильно) */)
        .order(property(EnrFisSyncPackage.id().fromAlias("p")) /* сортируем по дате формирования (считаем, что они сформированы правильно) */);

        if (!unrecvEntityCodes.isEmpty()) {
            // концепция в следующем:
            // * если есть пакеты, которые сейчас обрабатываются ФИС (переданы, но ответа еще нет), то посылать можем пакеты того же типа
            // * если таких пакетов нет, то посылать можем что угодно (первый не переданный пакет)
            dql.where(in(property("p", EnrFisSyncPackage.clazz()), unrecvEntityCodes));
        }


        // не должно быть пакетов, отправленных меньше 1 минуты назад
        dql.where(notExists(
            new DQLSelectBuilder()
            .fromEntity(EnrFisSyncPackageIOLog.class, "l").column(property("l.id"))
            .where(eq(property("p"), property(EnrFisSyncPackageIOLog.owner().fromAlias("l"))))
            .where(gt(property(EnrFisSyncPackageIOLog.operationDate().fromAlias("l")), valueTimestamp(DateUtils.addMinutes(new Date(), -1))))
            .buildQuery()
        ));

        return dql.createStatement(session).setMaxResults(1).<Long>uniqueResult();
    }

    @Override
    public boolean tryToSendPackage(final EnrFisSyncSession fisSession, final Long packageId)
    {
        Debug.begin("EnrFisSyncSessionDaemonBean.tryToSendPackage");
        try {
            final EnrFisSyncPackage pkg = this.get(EnrFisSyncPackage.class, packageId);
            if (null != pkg.getPkgSendSuccess()) { return false; } // oops (пакет уже удачно передан)

            // запрос в ФИС
            final CallResult<enr14fis.schema.pkg.send.resp.ImportPackageInfo> result;
            try {
                // формируем запрос в ФИС (обновляем реквизиты доступа)
                final enr14fis.schema.pkg.send.req.Root root = EnrFisServiceImpl.fromXml(enr14fis.schema.pkg.send.req.Root.class, pkg.getXmlPackage());
                root.getAuthData().setLogin(fisSession.getEnrOrgUnit().getLoginSafe());
                root.getAuthData().setPass(fisSession.getEnrOrgUnit().getPasswordSafe());
                root.getAuthData().setInstitutionID(fisSession.getEnrOrgUnit().getFisUid());

                // отправляем пакет в ФИС
                final EnrFisSyncPackageSendIOLog io = new EnrFisSyncPackageSendIOLog();
                io.countStatistics(root);
                result = this.call(fisSession, io, enr14fis.schema.pkg.send.resp.ImportPackageInfo.class, "import", root, packageId);
                if (null == result) { return false; /* ошибка (ее уже зарегистировали) */ }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            // если мы смогли прочитать id-пакета, сохраняем его (если в результате нет id-пакета, то загрузка не считается успешной)
            // (транзакцию открывать надо именно здесь)
            final Long fisId = result.getResult().getPackageID();
            if (null == fisId) {
                IEnrFisSyncSessionSendDaemonBean.instance.get().error(
                    fisSession.getId(), true /* в пакете отсутствует нужный нам тег - дальнейшая передача пакетов будет бессмысленной */,
                    EnrFisSyncSessionManager.instance().getProperty("error.sync.no-fis-package-id", Long.toString(packageId, 32)),
                    null
                );
                return false;
            }

            // если все ок, пробуем сохранить
            return IUniBaseDao.instance.get().doInTransaction(session -> {
                final EnrFisSyncPackage pkg1 = (EnrFisSyncPackage)session.get(EnrFisSyncPackage.class, packageId);
                pkg1.setFisPackageID(fisId);
                pkg1.setPkgSendSuccess((EnrFisSyncPackageIOLog) session.load(EnrFisSyncPackageIOLog.class, result.getLogEntryId()));
                session.saveOrUpdate(pkg1);
                return true;
            });
        } finally {
            Debug.end();
        }
    }


    ///////////////////////////////////////////////////////


    @Override
    public List<EnrFisSyncSession> getQueuedSessions4Recv()
    {
        final Date now = new Date();

        final List<EnrFisSyncSession> list = new DQLSelectBuilder()
        .fromEntity(EnrFisSyncSession.class, "s")
        .column(property("s"))
        .order(property(EnrFisSyncSession.creationDate().fromAlias("s")))
        .order(property(EnrFisSyncSession.id().fromAlias("s")))
        .where(isNull(property(EnrFisSyncSession.archiveDate().fromAlias("s"))))
        .where(isNotNull(property(EnrFisSyncSession.queueDate().fromAlias("s"))))

        // есть отправленный пакет, по которому еще нет результата
        // при этом в пакете либо нет ограничения на передачу пакетов, либо ограничение уже прошло
        .where(exists(
            new DQLSelectBuilder()
            .fromEntity(EnrFisSyncPackage.class, "p")
            .column(property("p.id"))
            .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), property("s")))
            .where(isNotNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p"))))
            .where(isNull(property(EnrFisSyncPackage.pkgRecvSuccess().fromAlias("p"))))
            .where(or(
                isNull(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p"))),
                le(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p")), valueTimestamp(now))
            ))
            .buildQuery()
        ))
        .createStatement(getSession()).list();

        // проверяем, что в рамках ПК только одна сессия
        final Map<EnrOrgUnit, EnrFisSyncSession> testMap = new HashMap<>();
        for (final EnrFisSyncSession s: list) {
            if (null != testMap.put(s.getEnrOrgUnit(), s)) {
                throw new IllegalStateException("send-queue-is-not-unique: "+s.getEnrOrgUnit().getTitle());
            }
        }

        return list;
    }

    @Override
    public Long getNextRecvQueuePackageId(final EnrFisSyncSession fisSession)
    {
        final Date now = new Date();

        // в отличии от передачи данны, получение обработанных пакетов можно делать в любом порядке
        return new DQLSelectBuilder()
        .fromEntity(EnrFisSyncPackage.class, "p")
        .column(property("p.id"))
        .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), value(fisSession)))
        .where(isNotNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p")))) /* пакет передан в ФИС */
        .where(isNull(property(EnrFisSyncPackage.pkgRecvSuccess().fromAlias("p")))) /* пакет еще не обработан в ФИС */
        .order(property(EnrFisSyncPackage.creationDate().fromAlias("p")) /* сортируем по дате формирования (считаем, что они сформированы правильно) */)
        .order(property(EnrFisSyncPackage.id().fromAlias("p")) /* сортируем по дате формирования (считаем, что они сформированы правильно) */)

        // либо нет ограничения на передачу пакетов, либо ограничение уже прошло
        .where(or(
            isNull(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p"))),
            le(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p")), valueTimestamp(now))
        ))

        // не должно быть пакетов, отправленных меньше 1 минуты назад (это безусловная проверка)
        .where(notExists(
            new DQLSelectBuilder()
            .fromEntity(EnrFisSyncPackageIOLog.class, "l").column(property("l.id"))
            .where(eq(property("p"), property(EnrFisSyncPackageIOLog.owner().fromAlias("l"))))
            .where(gt(property(EnrFisSyncPackageIOLog.operationDate().fromAlias("l")), valueTimestamp(DateUtils.addMinutes(now, -1))))
            .buildQuery()
        ))

        .createStatement(getSession()).setMaxResults(1).<Long>uniqueResult();
    }

    @Override
    public boolean tryToRecvPackage(final EnrFisSyncSession fisSession, final Long packageId)
    {
        Debug.begin("EnrFisSyncSessionDaemonBean.tryToSendPackage");
        try {
            final EnrFisSyncPackage pkg = this.get(EnrFisSyncPackage.class, packageId);
            if (null != pkg.getPkgRecvSuccess()) { return false; } // oops (уже что-то получили)
            if (null == pkg.getFisPackageID()) { return false; } // oops (нет номера пакета)

            // запрос в ФИС
            final CallResult<enr14fis.schema.pkg.result.resp.ImportResultPackage> result;
            try {
                // формируем запрос в ФИС
                final enr14fis.schema.pkg.result.req.Root root = new enr14fis.schema.pkg.result.req.Root();
                root.setAuthData(new enr14fis.schema.pkg.result.req.AuthData());
                root.getAuthData().setLogin(fisSession.getEnrOrgUnit().getLoginSafe());
                root.getAuthData().setPass(fisSession.getEnrOrgUnit().getPasswordSafe());
                root.getAuthData().setInstitutionID(fisSession.getEnrOrgUnit().getFisUid());

                root.setGetResultImportApplication(new enr14fis.schema.pkg.result.req.GetResultImportApplication());
                root.getGetResultImportApplication().setPackageID(pkg.getFisPackageID());

                // отправляем запрос на результат в ФИС
                final EnrFisSyncPackageReceiveIOLog io = new EnrFisSyncPackageReceiveIOLog();
                result = this.call(fisSession, io, enr14fis.schema.pkg.result.resp.ImportResultPackage.class, "import/result", root, packageId);
                if (null == result) { return false; /* ошибка (ее уже зарегистрировали) */ }

                // если ок, то считаем статистику и сохраняем пакет в базе
                io.countStatistic(result.getResult());
                IUniBaseDao.instance.get().doInTransaction(session -> {
                    saveOrUpdate(io); return null;
                });

                if (null != result.getResult()) {
                    doAnalyzePackageResult(io, result.getResult());
                }

            } catch (final Exception t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            // если смогли получить данные (и данные те самые), то сохраняем все в базу
            // (транзакцию открывать надо именно здесь)
            if (!pkg.getFisPackageID().equals(result.getResult().getPackageID())) {
                IEnrFisSyncSessionSendDaemonBean.instance.get().error(
                    fisSession.getId(), true /* далее нет смысла что-либо делать, пришла какая-то лажа и распутать ее руками мы не сможем */,
                    EnrFisSyncSessionManager.instance().getProperty("error.sync.wrong-fis-package-id", Long.toString(packageId, 32)),
                    null
                );
                return false;
            }

            // пробуем сохранить в базу
            return IUniBaseDao.instance.get().doInTransaction(session -> {
                final EnrFisSyncPackage pkg1 = (EnrFisSyncPackage)session.get(EnrFisSyncPackage.class, packageId);
                pkg1.setPkgRecvSuccess((EnrFisSyncPackageIOLog) session.load(EnrFisSyncPackageIOLog.class, result.getLogEntryId()));
                session.saveOrUpdate(pkg1);
                return true;
            });

        } finally {
            Debug.end();
        }
    }

    protected void doAnalyzePackageResult(EnrFisSyncPackageReceiveIOLog io, ImportResultPackage result)
    {
        Session session = getSession();
        List<EnrFisEntrantSyncData> syncDataList = new DQLSelectBuilder().fromEntity(EnrFisEntrantSyncData.class, "sd")
        .where(eq(property("sd", EnrFisEntrantSyncData.syncPackage().id()), value(io.getOwner().getId())))
        .createStatement(session).list();

        if(syncDataList.isEmpty()) return;

        List<Long> failedEntrantIds = EnrFisSyncPackageReceiveIOLog.getFailedEntrantIds(result);

        Date syncCompleteDate = new Date();
        for(EnrFisEntrantSyncData syncData : syncDataList) {
            syncData.setSyncCompleteDate(syncCompleteDate);
            syncData.setSuccessfulImport(!failedEntrantIds.contains(syncData.getEntrant().getId()));
            session.update(syncData);
        }
    }

    ///////////////////////////////////////////////////////


    @Override
    public void doArchiveSessions()
    {
        // если в пакете все хорошо - отправляем в архив
        this.executeAndClear(
            new DQLUpdateBuilder(EnrFisSyncSession.class)
            .set(EnrFisSyncSession.P_ARCHIVE_DATE, value(new Date(), PropertyType.TIMESTAMP))
            .where(isNull(property(EnrFisSyncSession.P_ARCHIVE_DATE)))
            .where(isNotNull(property(EnrFisSyncSession.P_QUEUE_DATE)))
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(EnrFisSyncPackage.class, "p").column(property("p.id"))
                .where(eq(property(EnrFisSyncPackage.session().id().fromAlias("p")), property("id")))
                .where(or(
                    isNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p"))),
                    isNull(property(EnrFisSyncPackage.pkgRecvSuccess().fromAlias("p")))
                ))
                .buildQuery()
            ))
        );

        // если в каком-то пакете слишком много ошибок - отправляем всю сессию в архив
        // для каждого пакета руками
        {
            final Session session = getSession();
            final List<Long> ids = new DQLSelectBuilder()
            .fromEntity(EnrFisSyncSession.class, "s").column(property("s.id"))
            .where(isNull(property("s", EnrFisSyncSession.P_ARCHIVE_DATE)))
            .where(isNotNull(property("s", EnrFisSyncSession.P_QUEUE_DATE)))
            .where(exists(
                new DQLSelectBuilder()
                .fromEntity(EnrFisSyncPackageIOLog.class, "l")
                .column(property(EnrFisSyncPackageIOLog.owner().id().fromAlias("l")))
                .column(property(EnrFisSyncPackageIOLog.operationUrl().fromAlias("l")))
                .where(eq(property(EnrFisSyncPackageIOLog.owner().session().id().fromAlias("l")), property("s.id")))
                .where(isNotNull(property(EnrFisSyncPackageIOLog.zipErrorLog().fromAlias("l"))))
                .group(property(EnrFisSyncPackageIOLog.owner().id().fromAlias("l")))
                .group(property(EnrFisSyncPackageIOLog.operationUrl().fromAlias("l")))
                .having(ge(count(property("l.id")), value(ERROR_THRESHOLD)))
                .buildQuery()
            ))
            .predicate(DQLPredicateType.distinct)
            .createStatement(session)
            .list();

            // для каждого пакета делаем сразу flush + clear (zipLong может быть большим, держать его в памяти - зло)
            for (final Long id: ids) {
                final EnrFisSyncSession fisSession = (EnrFisSyncSession)session.get(EnrFisSyncSession.class, id);
                fisSession.error(EnrFisSyncSessionManager.instance().getProperty("title.sync"), EnrFisSyncSessionManager.instance().getProperty("error.sync.too-many-io-errors-and-archive"));
                fisSession.setArchiveDate(new Date());
                session.saveOrUpdate(fisSession);
                session.flush();
                session.clear();
            }
        }

        // если в каком-то пакете слишком много ошибок - отправляем всю сессию в архив
        // если вдруг не получилось или что-то пропустили
        this.executeAndClear(
            new DQLUpdateBuilder(EnrFisSyncSession.class)
            .set(EnrFisSyncSession.P_ARCHIVE_DATE, value(new Date(), PropertyType.TIMESTAMP))
            .where(isNull(property(EnrFisSyncSession.P_ARCHIVE_DATE)))
            .where(isNotNull(property(EnrFisSyncSession.P_QUEUE_DATE)))
            .where(exists(
                new DQLSelectBuilder()
                .fromEntity(EnrFisSyncPackageIOLog.class, "l")
                .column(property(EnrFisSyncPackageIOLog.owner().id().fromAlias("l")))
                .column(property(EnrFisSyncPackageIOLog.operationUrl().fromAlias("l")))
                .where(eq(property(EnrFisSyncPackageIOLog.owner().session().id().fromAlias("l")), property("id")))
                .where(isNotNull(property(EnrFisSyncPackageIOLog.zipErrorLog().fromAlias("l"))))
                .group(property(EnrFisSyncPackageIOLog.owner().id().fromAlias("l")))
                .group(property(EnrFisSyncPackageIOLog.operationUrl().fromAlias("l")))
                .having(ge(count(property("l.id")), value(ERROR_THRESHOLD)))
                .buildQuery()
            ))
        );


    }


}
