package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.renameColumn("enr14fis_session_t", "skipallentrants_p", "skipapplicationsinfo_p");

		// создано обязательное свойство skipOrdersOfAdmission
		{
			tool.createColumn("enr14fis_session_t", new DBColumn("skipordersofadmission_p", DBType.BOOLEAN));
            tool.executeUpdate("update enr14fis_session_t set skipordersofadmission_p=? where skipordersofadmission_p is null", (Boolean) true);
			tool.setColumnNullable("enr14fis_session_t", "skipordersofadmission_p", false);
		}
    }
}