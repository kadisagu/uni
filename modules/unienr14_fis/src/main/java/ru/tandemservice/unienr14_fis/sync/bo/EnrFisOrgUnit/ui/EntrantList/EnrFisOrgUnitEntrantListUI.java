/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.ui.EntrantList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic.EnrFisOrgUnitEntrantListDSHandler;

/**
 * @author oleyba
 * @since 7/15/14
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true)
})
public class EnrFisOrgUnitEntrantListUI extends UIPresenter
{
    private final OrgUnitHolder holder = new OrgUnitHolder();

    private ISelectModel fisStatusList;

    private EnrEnrollmentCampaign enrCampaign;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setEnrCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setFisStatusList(EnrFisOrgUnitEntrantListDSHandler.FIS_STATUS_LIST);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(EnrFisOrgUnitEntrantListDSHandler.PARAM_ORG_UNIT, getHolder().getId());
        dataSource.put(EnrFisOrgUnitEntrantListDSHandler.PARAM_ENROLLMENT_CAMPAIGN, getEnrCampaign());
        dataSource.put("settings", getSettings());
    }

    public void onClickSearch() {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrCampaign());
        getSettings().save();
    }

    // getters and setters

    public ISelectModel getFisStatusList()
    {
        return fisStatusList;
    }

    public void setFisStatusList(ISelectModel fisStatusList)
    {
        this.fisStatusList = fisStatusList;
    }

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        this.enrCampaign = enrCampaign;
    }

    public OrgUnitHolder getHolder()
    {
        return holder;
    }
}