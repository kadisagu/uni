/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.UidRegistryItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.BufferedWriter;
import java.io.CharArrayWriter;
import java.io.Writer;
import java.util.*;
import java.util.function.Function;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/16/14
 */
public class Enr14FisAppDeletePackageGenerator extends PkgDaoBase implements IEnr14FisAppDeletePackageGenerator
{
    private static final String HOME = Enr14FisAppDeletePackageGenerator.class.getPackage().getName().replace(".", "/");

    public static class App {
        public String number;
        public String date;

        public String appUID;
        public String orderUID;

        private MultiKey _key;

        public App(String number, String date)
        {
            this.date = date;
            this.number = number;
            this._key = new MultiKey(number, date);
        }

        public App(String number, String date, String appUID, String orderUID)
        {
            this.date = date;
            this.number = number;
            this.appUID = appUID;
            this.orderUID = orderUID;
            this._key = new MultiKey(number, date, orderUID);
        }

        public String getDate() { return date; }
        public String getNumber() { return number; }

        public String getAppUID() { return appUID; }
        public String getOrderUID() { return orderUID; }

        @Override
        public boolean equals(Object other)
        {
            if (other == this) return true;

            if (other instanceof App)
                return _key.equals(((App) other)._key);
            return false;
        }

        @Override
        public int hashCode()
        {
            return _key.hashCode();
        }
    }

    public String getPackage(EnrEnrollmentCampaign campaign, OrgUnit orgUnit, boolean fromOrders)
    {
        try {
            final Collection<App> apps = fromOrders? getExtracts(campaign, orgUnit) : getApps(campaign, orgUnit);

            final Writer charArrayWriter = new CharArrayWriter();
            final Writer bufferedWriter = new BufferedWriter(charArrayWriter);

            final VelocityContext context = new VelocityContext();
            context.put("apps", apps);

            try {
                final Template TEMPLATE_SCHEMA =  Velocity.getTemplate(HOME + (fromOrders ? "/clearOrders.vm" : "/deleteApplications.vm"));
                TEMPLATE_SCHEMA.merge(context, bufferedWriter);
            } catch (final Throwable e) {
                throw new RuntimeException(e);
            }

            bufferedWriter.flush();
            bufferedWriter.close();
            charArrayWriter.flush();
            charArrayWriter.close();

            return charArrayWriter.toString();
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private Collection<App> getExtracts(EnrEnrollmentCampaign campaign, OrgUnit orgUnit)
    {
        Set<App> apps = new LinkedHashSet<>();
        List<EnrOrgUnit> enrOrgUnits = getList(new DQLSelectBuilder()
                .fromEntity(EnrOrgUnit.class, "e")
                .where(and(eq(property("e", EnrOrgUnit.institutionOrgUnit().orgUnit().id()), value(orgUnit.getId()))
                , eq(property("e", EnrOrgUnit.enrollmentCampaign().id()), value(campaign.getId())))
                ));
        if (enrOrgUnits.size() < 1) { //Не нашли филиал
            return apps;
        }

        final Set<EnrCompetition> competitionSet = new HashSet<>(EnrFisSettingsManager.instance().dao().getCompetitions(enrOrgUnits.get(0)));
        final List<EnrAbstractExtract> allExtracts = getAllExtracts(competitionSet);

        if (allExtracts.size() > 0)
        {
            final Function<Date, XMLGregorianCalendar> dateTimeFunc = EnrFisSyncSessionManager.instance().dao().getDateTimeXmlConverter(campaign, dataTypeFactory);
            for (EnrAbstractExtract extract : allExtracts)
            {
                EnrOrder order = (EnrOrder) extract.getParagraph().getOrder();
                EnrEntrantRequest request = extract.getRequestedCompetition().getRequest();
                final XMLGregorianCalendar xmlGC = dateTimeFunc.apply(request.getRegDate());
                assert xmlGC != null;

                apps.add(new App(
                        uid(request.getEntrant()) + "/" + request.getRegNumber(),
                        xmlGC.toXMLFormat(),
                        uid(extract.getRequestedCompetition().getRequest()),
                        uid(order)
                ));
            }
        }
        return apps;
    }

    private List<EnrAbstractExtract> getAllExtracts(Set<EnrCompetition> competitionSet)
    {
        final List<EnrAbstractExtract> allExtracts = new ArrayList<>();

        BatchUtils.execute(competitionSet, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            List<EnrEnrollmentExtract> extracts = getList(new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                    .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().fromAlias("e"), "r")
                    .where(in(property("r", EnrRequestedCompetition.competition()), elements))
                    .where(or(
                            eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.ACCEPTED)),
                            eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED))
                    )));

            allExtracts.addAll(extracts);
        });

        BatchUtils.execute(competitionSet, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            List<Object[]> extracts = getList(new DQLSelectBuilder()
                    .fromEntity(EnrCancelExtract.class, "e")
                    .joinPath(DQLJoinType.inner, EnrCancelExtract.entity().fromAlias("e"), "r")
                    .joinPath(DQLJoinType.inner, EnrCancelExtract.requestedCompetition().fromAlias("e"), "rc")
                    .joinEntity("e", DQLJoinType.inner, EnrEnrollmentExtract.class, "enr", eq(property("enr"), property("r")))

                    .where(in(property("rc", EnrRequestedCompetition.competition()), elements))
                    .where(eq(property("rc"), property("enr", EnrEnrollmentExtract.entity())))
                    .where(eq(property("e", EnrCancelExtract.cancelled()), value(false)))
                    .where(eq(property("e", EnrCancelExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                    .column(property("e"))
                    .column(property("r")));

            for (Object[] ext : extracts)
            {
                allExtracts.add((EnrCancelExtract) ext[0]);
            }
        });
        return allExtracts;
    }

    protected static final DatatypeFactory dataTypeFactory;
    static {
        try {
            dataTypeFactory = DatatypeFactory.newInstance();
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private Collection<App> getApps(EnrEnrollmentCampaign campaign, OrgUnit orgUnit) throws Exception
    {
        List<EnrOrgUnit> orgUnits = getList(new DQLSelectBuilder()
            .fromEntity(EnrOrgUnit.class, "e").column("e")
            .where(eq(property("e", EnrOrgUnit.institutionOrgUnit().orgUnit()), value(orgUnit)))
            .where(eq(property("e", EnrOrgUnit.enrollmentCampaign()), value(campaign))));
        if (orgUnits.isEmpty()) return Collections.emptyList();

        EnrOrgUnit enrOrgUnit = orgUnits.get(0);

        DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, "r")
            .where(eq(property("r", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(campaign)))
            .column(property("r", EnrRequestedCompetition.request().entrant().id()))
            .column(property("r", EnrRequestedCompetition.request().regNumber()))
            .column(property("r", EnrRequestedCompetition.request().regDate()))
            .order(property("r", EnrRequestedCompetition.request().entrant().id()))
            .order(property("r", EnrRequestedCompetition.request().regNumber()))
            .order(property("r", EnrRequestedCompetition.request().regDate()))
            .predicate(DQLPredicateType.distinct);
        if (enrOrgUnit.isTop()) {
            builder.where(or(
                eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit()), value(enrOrgUnit)),
                eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().fisDataSendingIndependent()), value(Boolean.FALSE))
            ));
        } else {
            builder.where(eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit()), value(enrOrgUnit)));
        }

        final Function<Date, XMLGregorianCalendar> dateTimeFunc = EnrFisSyncSessionManager.instance().dao().getDateTimeXmlConverter(campaign, dataTypeFactory);
        Set<App> apps = new LinkedHashSet<>();
        for (Object[] row : this.<Object[]>getList(builder)) {

            Long entrantId = (Long) row[0];
            String regNumber = (String) row[1];
            Date regDate = (Date) row[2];

            String uidBase = Long.toString(entrantId, 32);
            List<String> uids = getList(new DQLSelectBuilder()
                .fromEntity(UidRegistryItem.class, "i")
                .column(property(UidRegistryItem.uid().fromAlias("i")))
                .where(eq(property(UidRegistryItem.registry().code().fromAlias("i")), value("enrFisUniRegistry")))
                .where(eq(property(UidRegistryItem.key().fromAlias("i")), value(uidBase))));

            if (uids.isEmpty()) {
                continue;
            }

            final String number = uids.get(0)+"/"+ regNumber;
            final XMLGregorianCalendar xmlGC = dateTimeFunc.apply(regDate);
            assert xmlGC != null;

            apps.add(new App(number, xmlGC.toXMLFormat()));
        }
        return apps;
    }
}
