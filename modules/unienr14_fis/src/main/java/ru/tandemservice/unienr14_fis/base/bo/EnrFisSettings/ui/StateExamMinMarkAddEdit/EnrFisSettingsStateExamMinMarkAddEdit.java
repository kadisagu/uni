/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;

/**
 * @author nvankov
 * @since 7/9/14
 */
@Configuration
public class EnrFisSettingsStateExamMinMarkAddEdit extends BusinessComponentManager
{
    public static final String EXCLUSIVE_OPTIONS = "exclusiveOptionsDS";
    public static final String EDU_PROGRAM_SUBJECT_DS = "eduProgramSubjectDS";
    public static final String DISCIPLINE_DS = "disciplineDS";
    public static final String STATE_EXAM_SUBJECT_DS = "stateExamSubjectDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EXCLUSIVE_OPTIONS, EnrFisSettingsManager.instance().exclusiveOptionsDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_DS, eduProgramSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(DISCIPLINE_DS, disciplineDSHandler()))
                .addDataSource(selectDS(STATE_EXAM_SUBJECT_DS, stateExamSubjectDSHandler()))
                .create();
    }



    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                Long enrCampainId = context.getNotNull("enrCampaignId");

                dql.where(DQLExpressions.exists(new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "sh")
                        .where(DQLExpressions.eq(DQLExpressions.property("sh", EnrProgramSetBase.enrollmentCampaign().id()), DQLExpressions.value(enrCampainId)))
                        .where(DQLExpressions.eq(DQLExpressions.property("sh", EnrProgramSetBase.programSubject().id()), DQLExpressions.property(alias, EduProgramSubject.id())))
                        .buildQuery()));
                dql.where(
                        DQLExpressions.or(
                                DQLExpressions.eq(DQLExpressions.property(alias, EduProgramSubject.subjectIndex().programKind().programBachelorDegree()), DQLExpressions.value(true)),
                                DQLExpressions.eq(DQLExpressions.property(alias, EduProgramSubject.subjectIndex().programKind().programSpecialistDegree()), DQLExpressions.value(true)))
                );
            }
        }
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler disciplineDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(),  EnrCampaignDiscipline.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long enrCampainId = context.getNotNull("enrCampaignId");
                dql.where(DQLExpressions.eq(DQLExpressions.property(alias, EnrCampaignDiscipline.enrollmentCampaign().id()), DQLExpressions.value(enrCampainId)));
            }
        }
                .filter(EnrCampaignDiscipline.discipline().title())
                .order(EnrCampaignDiscipline.discipline().title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrStateExamSubject.class)
                .filter(EnrStateExamSubject.title())
                .order(EnrStateExamSubject.title())
                .pageable(true);
    }
}



    