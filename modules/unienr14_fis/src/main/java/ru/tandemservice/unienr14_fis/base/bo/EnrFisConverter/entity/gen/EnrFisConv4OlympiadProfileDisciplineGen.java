package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Профильные дисциплины олимпиад
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4OlympiadProfileDisciplineGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4OlympiadProfileDisciplineGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline";
    public static final String ENTITY_NAME = "enrFisConv4OlympiadProfileDiscipline";
    public static final int VERSION_HASH = -1830796291;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLYMPIAD_PROFILE_DISCIPLINE = "olympiadProfileDiscipline";
    public static final String L_VALUE = "value";

    private EnrOlympiadProfileDiscipline _olympiadProfileDiscipline;     // Профильная дисциплина олимпиады
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrOlympiadProfileDiscipline getOlympiadProfileDiscipline()
    {
        return _olympiadProfileDiscipline;
    }

    /**
     * @param olympiadProfileDiscipline Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    public void setOlympiadProfileDiscipline(EnrOlympiadProfileDiscipline olympiadProfileDiscipline)
    {
        dirty(_olympiadProfileDiscipline, olympiadProfileDiscipline);
        _olympiadProfileDiscipline = olympiadProfileDiscipline;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4OlympiadProfileDisciplineGen)
        {
            if (withNaturalIdProperties)
            {
                setOlympiadProfileDiscipline(((EnrFisConv4OlympiadProfileDiscipline)another).getOlympiadProfileDiscipline());
            }
            setValue(((EnrFisConv4OlympiadProfileDiscipline)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4OlympiadProfileDisciplineGen> getNaturalId()
    {
        return new NaturalId(getOlympiadProfileDiscipline());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4OlympiadProfileDisciplineGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4OlympiadProfileDisciplineNaturalProxy";

        private Long _olympiadProfileDiscipline;

        public NaturalId()
        {}

        public NaturalId(EnrOlympiadProfileDiscipline olympiadProfileDiscipline)
        {
            _olympiadProfileDiscipline = ((IEntity) olympiadProfileDiscipline).getId();
        }

        public Long getOlympiadProfileDiscipline()
        {
            return _olympiadProfileDiscipline;
        }

        public void setOlympiadProfileDiscipline(Long olympiadProfileDiscipline)
        {
            _olympiadProfileDiscipline = olympiadProfileDiscipline;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4OlympiadProfileDisciplineGen.NaturalId) ) return false;

            EnrFisConv4OlympiadProfileDisciplineGen.NaturalId that = (NaturalId) o;

            if( !equals(getOlympiadProfileDiscipline(), that.getOlympiadProfileDiscipline()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOlympiadProfileDiscipline());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOlympiadProfileDiscipline());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4OlympiadProfileDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4OlympiadProfileDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4OlympiadProfileDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "olympiadProfileDiscipline":
                    return obj.getOlympiadProfileDiscipline();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "olympiadProfileDiscipline":
                    obj.setOlympiadProfileDiscipline((EnrOlympiadProfileDiscipline) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "olympiadProfileDiscipline":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "olympiadProfileDiscipline":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "olympiadProfileDiscipline":
                    return EnrOlympiadProfileDiscipline.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4OlympiadProfileDiscipline> _dslPath = new Path<EnrFisConv4OlympiadProfileDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4OlympiadProfileDiscipline");
    }
            

    /**
     * @return Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline#getOlympiadProfileDiscipline()
     */
    public static EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline> olympiadProfileDiscipline()
    {
        return _dslPath.olympiadProfileDiscipline();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4OlympiadProfileDiscipline> extends EntityPath<E>
    {
        private EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline> _olympiadProfileDiscipline;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Профильная дисциплина олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline#getOlympiadProfileDiscipline()
     */
        public EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline> olympiadProfileDiscipline()
        {
            if(_olympiadProfileDiscipline == null )
                _olympiadProfileDiscipline = new EnrOlympiadProfileDiscipline.Path<EnrOlympiadProfileDiscipline>(L_OLYMPIAD_PROFILE_DISCIPLINE, this);
            return _olympiadProfileDiscipline;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4OlympiadProfileDiscipline.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4OlympiadProfileDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
