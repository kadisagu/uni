/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examSubject;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 16.08.2016
 */
public class EnrFisConverterDao4StateExamSubject extends EnrFisConverterBaseDao<EnrStateExamSubject, Long> implements IEnrFisConverterDao4StateExamSubject
{
    @Override
    public String getCatalogCode()
    {
        return "1";
    }

    @Override
    protected List<EnrStateExamSubject> getEntityList()
    {
        return new DQLSelectBuilder().fromEntity(EnrStateExamSubject.class, "x").column(property("x"))
                .order(property(EnrStateExamSubject.title().fromAlias("x")))
                .createStatement(getSession()).list();
    }

    @Override
    protected Long getItemMapKey(EnrStateExamSubject entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4StateExamSubject.class, "x")
                        .column(property(EnrFisConv4StateExamSubject.stateExamSubject().id().fromAlias("x")), "exam_id")
                        .column(property(EnrFisConv4StateExamSubject.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())));
    }

    @Override
    public void update(Map<EnrStateExamSubject, EnrFisCatalogItem> values, boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EnrFisConv4StateExamSubject> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<EnrStateExamSubject, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4StateExamSubject(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4StateExamSubject.NaturalId, EnrFisConv4StateExamSubject>() {
            @Override protected EnrFisConv4StateExamSubject.NaturalId key(final EnrFisConv4StateExamSubject source) {
                return (EnrFisConv4StateExamSubject.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4StateExamSubject buildRow(final EnrFisConv4StateExamSubject source) {
                return new EnrFisConv4StateExamSubject(source.getStateExamSubject(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4StateExamSubject target, final EnrFisConv4StateExamSubject source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4StateExamSubject databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4StateExamSubject.class), targetRecords);
    }

    @Override
    public void doAutoSync()
    {
        // поднимаем Предметы ЕГЭ, для которых нет сопоставления с Элементом справочника ФИС
        final DQLSelectBuilder subjectDQL = new DQLSelectBuilder().fromEntity(EnrStateExamSubject.class, "o").column(property("o"))
                .where(notIn(property(EnrStateExamSubject.id().fromAlias("o")),
                             new DQLSelectBuilder().fromEntity(EnrFisConv4StateExamSubject.class, "c").column(property(EnrFisConv4StateExamSubject.stateExamSubject().id().fromAlias("c")))
                                     .buildQuery()));

        final DQLSelectBuilder fisCatalogDQL = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "f").column(property("f"))
                .where(isNull(property("f", EnrFisCatalogItem.removalDate())))
                .where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("f")), value(getCatalogCode())));

        final Map<String, EnrFisCatalogItem> fisCatalogItemTitleMap = new HashMap<>();
        for (EnrFisCatalogItem item : fisCatalogDQL.createStatement(getSession()).<EnrFisCatalogItem>list())
        {
            fisCatalogItemTitleMap.put(item.getTitle().trim().toLowerCase(), item);
        }

        final Map<EnrStateExamSubject, EnrFisCatalogItem> values = new HashMap<>();
        for (EnrStateExamSubject subject : subjectDQL.createStatement(getSession()).<EnrStateExamSubject>list())
        {
            EnrFisCatalogItem item = fisCatalogItemTitleMap.get(subject.getTitle().toLowerCase());
            if (null == item) { continue; }

            values.put(subject, item);
        }

        // для тех для которых уже выбрали - не меняем
        for (EnrFisConv4StateExamSubject discipline : getList(EnrFisConv4StateExamSubject.class))
            values.put(discipline.getStateExamSubject(), discipline.getValue());

        this.update(values, false);
    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4StateExamSubject.class, "b").column(property(EnrFisConv4StateExamSubject.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4StateExamSubject.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }
}