/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;

/**
 * @author nvankov
 * @since 7/9/14
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "element.id"),
        @Bind(key = "enrCampaign", binding = "enrCampaignId", required = true)
})
public class EnrFisSettingsStateExamMinMarkAddEditUI extends UIPresenter
{
    private EnrFisSettingsStateExamMinMark _element = new EnrFisSettingsStateExamMinMark();
    private DataWrapper _exclusiveOption;
    private Long _enrCampaignId;
    private EnrEnrollmentCampaign _enrEnrollmentCampaign;

    public boolean isAddForm() {return _element.getId() == null;}

    @Override
    public void onComponentRefresh() {
        _enrEnrollmentCampaign = IUniBaseDao.instance.get().getNotNull(EnrEnrollmentCampaign.class, _enrCampaignId);
        _element.setEnrollmentCampaign(_enrEnrollmentCampaign);
        if(_element.getId() != null)
        {
            setElement(IUniBaseDao.instance.get().getNotNull(EnrFisSettingsStateExamMinMark.class, getElement().getId()));
            if(getElement().getDiscipline() != null)
                setExclusiveOption(EnrFisSettingsManager.instance().exclusiveOptionsItemList().getItem(EnrFisSettingsManager.EXCLUSIVE_OPTION_ONE_HUNDRED_POINTS.toString()));
            else
                setExclusiveOption(EnrFisSettingsManager.instance().exclusiveOptionsItemList().getItem(EnrFisSettingsManager.EXCLUSIVE_OPTION_WITHOUT_EXAMS.toString()));
            _enrEnrollmentCampaign = getElement().getEnrollmentCampaign();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("enrCampaignId", _enrCampaignId);
    }

    public boolean isShowDiscipline()
    {
        return _exclusiveOption != null && EnrFisSettingsManager.EXCLUSIVE_OPTION_ONE_HUNDRED_POINTS.equals(_exclusiveOption.getId());
    }

    public void onChangeDiscipline()
    {
        if(_element.getDiscipline() != null && _element.getDiscipline().getStateExamSubject() != null)
            _element.setStateExamSubject(_element.getDiscipline().getStateExamSubject());
    }

    public void onClickApply() {
        if(EnrFisSettingsManager.EXCLUSIVE_OPTION_WITHOUT_EXAMS.equals(_exclusiveOption.getId()))
            getElement().setDiscipline(null);
        IUniBaseDao.instance.get().saveOrUpdate(getElement());
        deactivate();
    }

    // getters and setters


    public EnrFisSettingsStateExamMinMark getElement()
    {
        return _element;
    }

    public void setElement(EnrFisSettingsStateExamMinMark element)
    {
        _element = element;
    }

    public DataWrapper getExclusiveOption()
    {
        return _exclusiveOption;
    }

    public void setExclusiveOption(DataWrapper exclusiveOption)
    {
        _exclusiveOption = exclusiveOption;
    }

    public Long getEnrCampaignId()
    {
        return _enrCampaignId;
    }

    public void setEnrCampaignId(Long enrCampaignId)
    {
        _enrCampaignId = enrCampaignId;
    }

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return _enrEnrollmentCampaign;
    }

    public void setEnrEnrollmentCampaign(EnrEnrollmentCampaign enrEnrollmentCampaign)
    {
        _enrEnrollmentCampaign = enrEnrollmentCampaign;
    }
}
