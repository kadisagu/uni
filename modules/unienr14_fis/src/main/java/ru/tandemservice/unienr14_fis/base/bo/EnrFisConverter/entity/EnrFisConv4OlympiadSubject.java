package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/**
 * ФИС: Сопоставление: Предмет олимпиады
 */
public class EnrFisConv4OlympiadSubject extends EnrFisConv4OlympiadSubjectGen
{
    public EnrFisConv4OlympiadSubject()
    {
    }

    public EnrFisConv4OlympiadSubject(EnrOlympiadSubject subject, EnrFisCatalogItem fisCatalogItem)
    {
        setOlympiadSubject(subject);
        setValue(fisCatalogItem);
    }
}