package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Минимальный балл ЕГЭ для подтверждения результатов олимпиад (старый)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSettingsStateExamMinMarkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark";
    public static final String ENTITY_NAME = "enrFisSettingsStateExamMinMark";
    public static final int VERSION_HASH = -1269509136;
    private static IEntityMeta ENTITY_META;

    public static final String P_MIN_MARK = "minMark";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_EDU_PROGRAM_SUBJECT = "eduProgramSubject";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";

    private int _minMark;     // Минимальный балл
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private EduProgramSubject _eduProgramSubject;     // Направление (специальность)
    private EnrCampaignDiscipline _discipline;     // Дисциплина
    private EnrStateExamSubject _stateExamSubject;     // Предмет ЕГЭ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     */
    @NotNull
    public int getMinMark()
    {
        return _minMark;
    }

    /**
     * @param minMark Минимальный балл. Свойство не может быть null.
     */
    public void setMinMark(int minMark)
    {
        dirty(_minMark, minMark);
        _minMark = minMark;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Направление (специальность). Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    /**
     * @param eduProgramSubject Направление (специальность). Свойство не может быть null.
     */
    public void setEduProgramSubject(EduProgramSubject eduProgramSubject)
    {
        dirty(_eduProgramSubject, eduProgramSubject);
        _eduProgramSubject = eduProgramSubject;
    }

    /**
     * @return Дисциплина.
     */
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public EnrStateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject Предмет ЕГЭ. Свойство не может быть null.
     */
    public void setStateExamSubject(EnrStateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisSettingsStateExamMinMarkGen)
        {
            setMinMark(((EnrFisSettingsStateExamMinMark)another).getMinMark());
            setEnrollmentCampaign(((EnrFisSettingsStateExamMinMark)another).getEnrollmentCampaign());
            setEduProgramSubject(((EnrFisSettingsStateExamMinMark)another).getEduProgramSubject());
            setDiscipline(((EnrFisSettingsStateExamMinMark)another).getDiscipline());
            setStateExamSubject(((EnrFisSettingsStateExamMinMark)another).getStateExamSubject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSettingsStateExamMinMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSettingsStateExamMinMark.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSettingsStateExamMinMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "minMark":
                    return obj.getMinMark();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "eduProgramSubject":
                    return obj.getEduProgramSubject();
                case "discipline":
                    return obj.getDiscipline();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "minMark":
                    obj.setMinMark((Integer) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "eduProgramSubject":
                    obj.setEduProgramSubject((EduProgramSubject) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((EnrStateExamSubject) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "minMark":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "eduProgramSubject":
                        return true;
                case "discipline":
                        return true;
                case "stateExamSubject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "minMark":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "eduProgramSubject":
                    return true;
                case "discipline":
                    return true;
                case "stateExamSubject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "minMark":
                    return Integer.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "eduProgramSubject":
                    return EduProgramSubject.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "stateExamSubject":
                    return EnrStateExamSubject.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSettingsStateExamMinMark> _dslPath = new Path<EnrFisSettingsStateExamMinMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSettingsStateExamMinMark");
    }
            

    /**
     * @return Минимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getMinMark()
     */
    public static PropertyPath<Integer> minMark()
    {
        return _dslPath.minMark();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Направление (специальность). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getEduProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> eduProgramSubject()
    {
        return _dslPath.eduProgramSubject();
    }

    /**
     * @return Дисциплина.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getStateExamSubject()
     */
    public static EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    public static class Path<E extends EnrFisSettingsStateExamMinMark> extends EntityPath<E>
    {
        private PropertyPath<Integer> _minMark;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EduProgramSubject.Path<EduProgramSubject> _eduProgramSubject;
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrStateExamSubject.Path<EnrStateExamSubject> _stateExamSubject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getMinMark()
     */
        public PropertyPath<Integer> minMark()
        {
            if(_minMark == null )
                _minMark = new PropertyPath<Integer>(EnrFisSettingsStateExamMinMarkGen.P_MIN_MARK, this);
            return _minMark;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Направление (специальность). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getEduProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> eduProgramSubject()
        {
            if(_eduProgramSubject == null )
                _eduProgramSubject = new EduProgramSubject.Path<EduProgramSubject>(L_EDU_PROGRAM_SUBJECT, this);
            return _eduProgramSubject;
        }

    /**
     * @return Дисциплина.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark#getStateExamSubject()
     */
        public EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new EnrStateExamSubject.Path<EnrStateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

        public Class getEntityClass()
        {
            return EnrFisSettingsStateExamMinMark.class;
        }

        public String getEntityName()
        {
            return "enrFisSettingsStateExamMinMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
