package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.Pub;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisSyncPackageGen;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrFisSyncSessionPubUI extends UIPresenter {

    private final EntityHolder<EnrFisSyncSession> holder = new EntityHolder<EnrFisSyncSession>();
    public EntityHolder<EnrFisSyncSession> getHolder() { return this.holder; }
    public EnrFisSyncSession getFisSession() { return getHolder().getValue(); }

    public OrgUnit getOrgUnit() { return getFisSession().getEnrOrgUnit().getInstitutionOrgUnit().getOrgUnit(); }

    private OrgUnitSecModel _secModel;
    public  OrgUnitSecModel getSecModel() { return _secModel; }

    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(String selectedTab) { this.selectedTab = selectedTab; }

    private boolean hasPacks;

    @Override
    public void onComponentRefresh() {
        getHolder().refresh();
        _secModel = new OrgUnitSecModel(getOrgUnit());
    }

    @Override
    public void onComponentPrepareRender()
    {
        this.hasPacks = IUniBaseDao.instance.get().existsEntity(EnrFisSyncPackage.class, EnrFisSyncPackage.session().s(), getFisSession());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put("fisSession", getFisSession());
    }

    // actions

    public void onClickQueueSession() {
        try {
            EnrFisSyncSessionManager.instance().dao().doQueue(getFisSession());
        } finally {
            getSupport().setRefreshScheduled(true);
        }
    }

    public void onClickDownloadPackage()
    {
        EnrFisSyncPackage pkg = DataAccessServices.dao().get(EnrFisSyncPackage.class, getSupport().getListenerParameterAsLong());
        if (pkg.getFormattedXmlPackage() == null)
            throw new ApplicationException("«ZIP-XML: Пакет» пуст");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(getGlobalFileNamePrefix() + " - " + pkg.getFileName()).document(pkg.getFormattedXmlPackage().getBytes()).xml(), false);
    }

    public void onClickExportZipArchive()
    {
        String filename = getGlobalFileNamePrefix()+".zip";
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(output);

            List<EnrFisSyncPackage> pkgList = DataAccessServices.dao().getList(EnrFisSyncPackage.class, EnrFisSyncPackageGen.session(), getFisSession(), EnrFisSyncPackageGen.P_CREATION_DATE);
            for (EnrFisSyncPackage pkg: pkgList) {
                zip.putNextEntry(new ZipEntry(pkg.getFileName() /* здесь нельзя использовать русские буквы - винда не пойдем */ ));
                IOUtils.write(pkg.getFormattedXmlPackage(), zip);
            }

            zip.close();
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(output.toByteArray()).zip(), false);
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // presenter

    public boolean isExportZipArchiveDisabled() {
        return null == getFisSession().getFillDate();
    }

    public boolean isQueueSessionVisible() {
        EnrFisSyncSession fisSession = getFisSession();
        return this.hasPacks && !fisSession.isQueueDisabled() && null == fisSession.getArchiveDate() && null == fisSession.getQueueDate();
    }

    public boolean isExportButtonVisible() {
        return this.hasPacks;
    }

    public String getRequestState()
    {
        return getFisSession().getRequestState() == 2 ? "Новое" : "Принято";
    }

    // utils

    private String getGlobalFileNamePrefix() {
        EnrFisSyncSession fisSession = getFisSession();
        return fisSession.getEnrOrgUnit().getEnrollmentCampaign().getTitle()+" - "+EnrFisSyncSession.DATE_FORMATTER_FILE.format(fisSession.getFillDate());
    }


}
