package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4DocumentCategoryOrphanGen;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4DocumentCategoryOrphanGen */
public class EnrFisConv4DocumentCategoryOrphan extends EnrFisConv4DocumentCategoryOrphanGen
{
    public EnrFisConv4DocumentCategoryOrphan()
    {
    }

    public EnrFisConv4DocumentCategoryOrphan(PersonDocumentCategory docCategory, EnrFisCatalogItem value)
    {
        setDocCategory(docCategory);
        setValue(value);
    }
}