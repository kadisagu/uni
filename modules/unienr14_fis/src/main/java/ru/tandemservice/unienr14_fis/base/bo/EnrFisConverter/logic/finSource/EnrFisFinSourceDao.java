package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.finSource;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import java.util.*;

/**
 * @author vdanilov
 */
public class EnrFisFinSourceDao extends UniBaseDao implements IEnrFisFinSourceDao {

    @Override
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15664459", section="Правило преобразования для НП")
    public Map<EnrCompetition, EnrFisCatalogItem> getSourceMap(final Collection<EnrCompetition> directions)
    {
        if (directions.isEmpty()) {
            return Collections.emptyMap();
        }

        final EnrEnrollmentCampaign ec;
        {
            final Iterator<EnrCompetition> it = directions.iterator();
            ec = it.next().getProgramSetOrgUnit().getProgramSet().getEnrollmentCampaign();
            while (it.hasNext()) {
                if (!ec.equals(it.next().getProgramSetOrgUnit().getProgramSet().getEnrollmentCampaign())) {
                    throw new IllegalStateException("∃ d ∊ directions | d.enrollmentCampaign ≠ directions[0].enrollmentCampaign");
                }
            }
        }

        Map<EnrCompetition, EnrFisCatalogItem> map = new HashMap<>();

        for (EnrCompetition competition: directions) {
            map.put(competition, getSource(competition));
        }

        return map;
    }

    @Override
    public EnrFisCatalogItem getSource(EnrCompetition competition)
    {
        return getSource(competition.getType());
    }

    @Override
    public EnrFisCatalogItem getSource(EnrCompetitionType competitionType)
    {
        if(EnrCompetitionTypeCodes.EXCLUSIVE.equals(competitionType.getCode()))
        {
            return getItem4Exclusive();
        }
        if(EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competitionType.getCode()))
        {
            return getItem4TargetAdmission();
        }
        if(competitionType.getCompensationType().isBudget())
        {
            return getItem4Budget();
        }
        else
        {
            return getItem4Contract();
        }
    }

    /** @return ЦП */
    protected EnrFisCatalogItem getItem4TargetAdmission() {
        return this.<EnrFisCatalogItem>getByNaturalId(new EnrFisCatalogItem.NaturalId("15", "16"));
    }

    /** @return контракт */
    protected EnrFisCatalogItem getItem4Contract() {
        return this.<EnrFisCatalogItem>getByNaturalId(new EnrFisCatalogItem.NaturalId("15", "15"));
    }

    /** @return бюджет, исключая ЦП и квоту */
    protected EnrFisCatalogItem getItem4Budget() {
        return this.<EnrFisCatalogItem>getByNaturalId(new EnrFisCatalogItem.NaturalId("15", "14"));
    }

    /** @return квота */
    protected EnrFisCatalogItem getItem4Exclusive() {
        return this.<EnrFisCatalogItem>getByNaturalId(new EnrFisCatalogItem.NaturalId("15", "20"));
    }


}
