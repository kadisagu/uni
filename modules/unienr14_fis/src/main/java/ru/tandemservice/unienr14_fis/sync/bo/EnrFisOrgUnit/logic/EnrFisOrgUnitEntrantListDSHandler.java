/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/15/14
 */
public class EnrFisOrgUnitEntrantListDSHandler extends EnrEntrantDSHandler
{
    private static final IdentifiableWrapper SUCCESS = new IdentifiableWrapper(0L, "Принят");
    private static final IdentifiableWrapper NOT_SEND = new IdentifiableWrapper(1L, "Еще не был отправлен");
    private static final IdentifiableWrapper PROCESSING = new IdentifiableWrapper(2L, "В очереди");
    private static final IdentifiableWrapper ERROR_ON_SEND = new IdentifiableWrapper(3L, "Невозможно сформировать данные");
    private static final IdentifiableWrapper ERROR_FIS = new IdentifiableWrapper(4L, "Не принят");

    public static final ISelectModel FIS_STATUS_LIST = new LazySimpleSelectModel<>(ImmutableList.of(
            SUCCESS,
            NOT_SEND,
            PROCESSING,
            ERROR_ON_SEND,
            ERROR_FIS
    ));

    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrCampaign";

    public EnrFisOrgUnitEntrantListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = getEntrantDQL(context);

        Long orgUnitId = context.get(PARAM_ORG_UNIT);
        EnrEnrollmentCampaign campaign = context.get(PARAM_ENROLLMENT_CAMPAIGN);
        EnrOrgUnit enrOrgUnit = new DQLSelectBuilder()
            .fromEntity(EnrOrgUnit.class, "e").column("e")
            .where(eq(property("e", EnrOrgUnit.institutionOrgUnit().orgUnit().id()), value(orgUnitId)))
            .where(eq(property("e", EnrOrgUnit.enrollmentCampaign()), value(campaign)))
            .createStatement(context.getSession()).uniqueResult();
        if (null == enrOrgUnit || (!enrOrgUnit.isTop() && !enrOrgUnit.isFisDataSendingIndependent())) {
            builder.where(isNull(ENTRANT_ALIAS + ".id"));
        } else {
            if (enrOrgUnit.isTop()) {
                builder.where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r")
                    .where(eq(property("r", EnrRequestedCompetition.request().entrant()), property(ENTRANT_ALIAS)))
                    .where(or(
                        eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit()), value(enrOrgUnit)),
                        eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().fisDataSendingIndependent()), value(Boolean.FALSE))
                        ))
                    .buildQuery()));
            } else {
                builder.where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r")
                    .where(eq(property("r", EnrRequestedCompetition.request().entrant()), property(ENTRANT_ALIAS)))
                    .where(eq(property("r", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit()), value(enrOrgUnit)))
                    .buildQuery()));
            }
        }

        IDataSettings settings = context.get("settings");
        IdentifiableWrapper fisStatus = settings.get("fisStatusFilter");
        if (fisStatus != null) {
            if (SUCCESS.getId().equals(fisStatus.getId())) {
                builder.joinEntity(ENTRANT_ALIAS, DQLJoinType.inner, EnrFisEntrantSyncData.class, "s", eq(property("s", EnrFisEntrantSyncData.entrant()), property(ENTRANT_ALIAS)));
                builder.where(eq(property("s", EnrFisEntrantSyncData.successfulImport()), value(Boolean.TRUE)));
                builder.where(isNull(property("s", EnrFisEntrantSyncData.skipReason())));
            }
            else if (NOT_SEND.getId().equals(fisStatus.getId())) {
                builder.joinEntity(ENTRANT_ALIAS, DQLJoinType.left, EnrFisEntrantSyncData.class, "s", eq(property("s", EnrFisEntrantSyncData.entrant()), property(ENTRANT_ALIAS)));
                builder.where(isNull(property("s.id")));
            }
            else if (PROCESSING.getId().equals(fisStatus.getId())) {
                builder.joinEntity(ENTRANT_ALIAS, DQLJoinType.inner, EnrFisEntrantSyncData.class, "s", eq(property("s", EnrFisEntrantSyncData.entrant()), property(ENTRANT_ALIAS)));
                builder.where(isNull(property("s", EnrFisEntrantSyncData.successfulImport())));
                builder.where(isNull(property("s", EnrFisEntrantSyncData.skipReason())));
            }
            else if (ERROR_ON_SEND.getId().equals(fisStatus.getId())) {
                builder.joinEntity(ENTRANT_ALIAS, DQLJoinType.inner, EnrFisEntrantSyncData.class, "s", eq(property("s", EnrFisEntrantSyncData.entrant()), property(ENTRANT_ALIAS)));
                builder.where(isNotNull(property("s", EnrFisEntrantSyncData.skipReason())));
            }
            else if (ERROR_FIS.getId().equals(fisStatus.getId())) {
                builder.joinEntity(ENTRANT_ALIAS, DQLJoinType.inner, EnrFisEntrantSyncData.class, "s", eq(property("s", EnrFisEntrantSyncData.entrant()), property(ENTRANT_ALIAS)));
                builder.where(eq(property("s", EnrFisEntrantSyncData.successfulImport()), value(Boolean.FALSE)));
                builder.where(isNull(property("s", EnrFisEntrantSyncData.skipReason())));
            }
        }

        DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrEntrant.class, ENTRANT_ALIAS);
        orderDescriptionRegistry
                .setOrders("personalNumber", new OrderDescription(EnrEntrant.personalNumber()))
                .setOrders("entrant", new OrderDescription(PersonRole.person().identityCard().fullFio()))
                .setOrders("sex", new OrderDescription(PersonRole.person().identityCard().sex().shortTitle()))
                .setOrders("birthDate", new OrderDescription(PersonRole.person().identityCard().birthDate()))
                .setOrders("registrationDate", new OrderDescription(EnrEntrant.registrationDate()));

        orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
        final List<Long> entrantIds = output.getRecordIds();

        final Multimap<Long, DataWrapper> entrant2RequestsMap = ArrayListMultimap.create(entrantIds.size(), 2);
        final Multimap<Long, EnrEntrantCustomState> entrantActiveCustomStateMap = ArrayListMultimap.create(entrantIds.size(), 2);
        fillRequestAndCustomStateMap(entrantIds, entrant2RequestsMap, entrantActiveCustomStateMap, context);

        final Map<Long, EnrFisEntrantSyncData> fisSyncDataMap = getFisSyncDataMap(entrantIds);

        return output.transform((EnrEntrant entrant) -> {
            ViewWrapper<EnrEntrant> wrapper = new ViewWrapper<>(entrant);
            wrapper.setViewProperty(VIEW_PROP_REQUEST, entrant2RequestsMap.get(wrapper.getEntity().getId()));
            wrapper.setViewProperty(VIEW_PROP_CUSTOM_STATES, entrantActiveCustomStateMap.get(wrapper.getEntity().getId()));
            EnrFisEntrantSyncData entrantSyncData = fisSyncDataMap.get(wrapper.getEntity().getId());
            wrapper.setViewProperty("fisStatus", getFisStatus(entrantSyncData));
            wrapper.setViewProperty("fisDate", entrantSyncData == null ? null : entrantSyncData.getModificationDate());
            wrapper.setViewProperty("fisSkipReason", entrantSyncData == null ? null : entrantSyncData.getSkipReason());
            return wrapper;
        });
    }

    protected IdentifiableWrapper getFisStatus(EnrFisEntrantSyncData entrantSyncData)
    {
        if (null != entrantSyncData && entrantSyncData.getSkipReason() != null) {
            return new IdentifiableWrapper(-1L, ERROR_ON_SEND.getTitle());
        }
        if (null == entrantSyncData || null == entrantSyncData.getSyncPackage()) {
            return new IdentifiableWrapper(-1L, NOT_SEND.getTitle());
        }
        if (null == entrantSyncData.getSuccessfulImport()) {
            return new IdentifiableWrapper(entrantSyncData.getSyncPackage().getId(), PROCESSING.getTitle());
        }
        return entrantSyncData.isSuccessfulImportNullSafe() ? new IdentifiableWrapper(entrantSyncData.getSyncPackage().getId(), SUCCESS.getTitle()) : new IdentifiableWrapper(entrantSyncData.getSyncPackage().getId(), ERROR_FIS.getTitle());
    }

    protected Map<Long, EnrFisEntrantSyncData> getFisSyncDataMap(List<Long> entrantIds)
    {
        final Map<Long, EnrFisEntrantSyncData> map = Maps.newHashMapWithExpectedSize(entrantIds.size());
        for (EnrFisEntrantSyncData data : IUniBaseDao.instance.get().getList(EnrFisEntrantSyncData.class, EnrFisEntrantSyncData.entrant().id(), entrantIds)) {
            map.put(data.getEntrant().getId(), data);
        }
        return map;
    }
}
