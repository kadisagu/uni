package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.shared.commonbase.base.util.zip.GZipCompressionInterface;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisSyncPackageIOLogGen;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;
import ru.tandemservice.unienr14_fis.util.IEnrFisService;

/**
 * Лог взаимодействия с сервером ФИС
 */
public abstract class EnrFisSyncPackageIOLog extends EnrFisSyncPackageIOLogGen implements ITitled
{
    public static final Logger log = Logger.getLogger(EnrFisSyncPackageIOLog.class);
    public static final GZipCompressionInterface zip = EnrFisSyncSession.zip;

    @Override
    @EntityDSLSupport
    public String getTitle() {
        return getOperationDateAsString() +" " + getDescription();
    }

    public String getTitleWithPackage() {
        return getTitle() + " (пакет " + getOwner().getTypeNumber() + (StringUtils.isEmpty(getOwner().getComment()) ? "" : ", " + getOwner().getComment()) + ")";
    }

    @Override
    @EntityDSLSupport
    public String getOperationDateAsString() {
        return EnrFisSyncSession.DATE_FORMATTER.format(super.getOperationDate());
    }


    public byte[] getXmlRequest() {
        return zip.decompress(this.getZipXmlRequest());
    }
    public void setXmlRequest(byte[] xmlRequest) {
        this.setZipXmlRequest(zip.compress(xmlRequest));
    }

    public String getXmlRequestString() {
        final byte[] xmlRequest = getXmlRequest();
        return (null == xmlRequest ? null : new String(xmlRequest));
    }
    public void setXmlRequestString(String xmlRequest) {
        this.setXmlRequest(xmlRequest.getBytes());
    }

    public String getFormattedXmlRequest() {
        String xmlRequestString = getXmlRequestString();
        if (StringUtils.isEmpty(xmlRequestString)) { return null; }
        return EnrFisServiceImpl.prettyFormat(xmlRequestString);
    }


    public byte[] getXmlResponse() {
        return zip.decompress(this.getZipXmlResponse());
    }
    public void setXmlResponse(byte[] xmlResponse) {
        this.setZipXmlResponse(zip.compress(xmlResponse));
    }

    public String getXmlResponseString() {
        final byte[] xmlResponse = getXmlResponse();
        return (null == xmlResponse ? null : new String(xmlResponse));
    }
    public void setXmlResponseString(String xmlResponse) {
        this.setXmlResponse(xmlResponse.getBytes());
    }

    public String getFormattedXmlResponse() {
        String xmlResponseString = getXmlResponseString();
        if (StringUtils.isEmpty(xmlResponseString)) { return null; }
        return EnrFisServiceImpl.prettyFormat(xmlResponseString);
    }

    private static final StringLimitFormatter FIS_ERROR_TEXT_FORMATTER = new StringLimitFormatter(1000);

    public void registerError(Throwable t) {
        if (t instanceof IEnrFisService.EnrFisError) {
            this.setFisErrorText(t.getMessage());
        }

        if (null != getFisErrorText()) {
            setFisErrorText(FIS_ERROR_TEXT_FORMATTER.format(getFisErrorText()));
        }

        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        PrintStream print = new PrintStream(buff);
        while (null != t) {
            t.printStackTrace(print);
            t = t.getCause();
        }
        print.flush();
        this.setZipErrorLog(zip.compress(buff.toByteArray()));
    }


    public String getErrorString() {
        final byte[] bytes = zip.decompress(getZipErrorLog());
        return (null == bytes ? null : new String(bytes));
    }

    public boolean isErrorByZipLog()
    {
        return getZipErrorLog() != null; // TODO: разобраться с формулой isError
    }

    @Override
    @EntityDSLSupport
    public abstract String getDescription();

    public abstract String getStatisticCaption();

    public abstract String getStatistic();

    public abstract boolean isShowStatistic();
}