package ru.tandemservice.unienr14_fis.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.mvel2.MVEL;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;

/**
 * @author vdanilov
 */
public class EnrFisServiceImpl implements IEnrFisService {

    private static final LoadingCache<Package, JAXBContext> _contextCache = CacheBuilder.newBuilder().build(new CacheLoader<Package, JAXBContext>() {
        @Override public JAXBContext load(Package contextPath) {
            try { return JAXBContext.newInstance(contextPath.getName()); }
            catch (final Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
        }
    });

    private static JAXBContext get(final Package contextPath) {
        try { return _contextCache.get(contextPath); }
        catch (final Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public static byte[] toXml(final Object root) {
        try {
            final JAXBContext jc = get(root.getClass().getPackage());
            final Marshaller m = jc.createMarshaller();

            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            ba.flush();

            return ba.toByteArray();
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static <T> T fromXml(final Class<T> clazz, final byte[] xml) {
        try {
            final JAXBContext jc = get(clazz.getPackage());
            final Unmarshaller u = jc.createUnmarshaller();
            final Object object = u.unmarshal(new ByteArrayInputStream(xml));

            // если это нужный класс, то его и возвращаем
            if (clazz.isInstance(object)) {
                return clazz.cast(object);
            }

            // если корневой элемент <error>
            // либо не декларирован в схеме (как это было в старом протоколе)
            // либо является ссылкой на тип TError (как сделано сейчас)
            if (object instanceof JAXBElement) {
                final JAXBElement element = ((JAXBElement)object);
                if ("error".equalsIgnoreCase(element.getName().getLocalPart())) {
                    Object value = element.getValue();

                    // пробуем распарсить terror внутри error
                    if ("TError".equalsIgnoreCase(value.getClass().getSimpleName())) {
                        throw new EnrFisError(
                            value,
                            String.valueOf(MVEL.getProperty("errorCode", value)),
                            String.valueOf(MVEL.getProperty("errorText", value))
                        );
                    }

                    // если не получилось - выкидываем исключение с объектом
                    throw new EnrFisError(
                        element,
                        "XXX",
                        String.valueOf(value)
                    ); // корневой тег с ошибкой
                }
            }

            //            // корневой тег с ошибкой
            //            // сейчас так, когда они сделали тег с кодами, но не включили его в схему
            //            // вариант ошибки после 11.06.2013
            //            if ("error".equalsIgnoreCase(object.getClass().getSimpleName())) {
            //                throw new EnrFisError(
            //                    object,
            //                    String.valueOf(MVEL.getProperty("errorCode", object)),
            //                    String.valueOf(MVEL.getProperty("errorText", object))
            //                );
            //            }

            // пришло хз что O_o
            // см. выше - ошибки нужно регистировать во всех схемах одинаково (решили, что должен быть объявлен тип TError и его тег <error>, ссылающийся на него)
            // если здесь что-то падает, то скорее всего они прислали xsd где что-то не так, как везде (ее надо переделать руками)
            throw new IllegalStateException(object.getClass().toString());

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static String prettyFormat(final String xml) {
        try {
            final Document doc = DocumentHelper.parseText(xml);
            final StringWriter sw = new StringWriter();
            final OutputFormat format = OutputFormat.createPrettyPrint();
            final XMLWriter xw = new XMLWriter(sw, format);
            xw.write(doc);
            return sw.toString();
        } catch (final Throwable t) {
            // throw CoreExceptionUtils.getRuntimeException(t);
            // возвращать что-то надо, даже если были ошибки
            log.error(t.getMessage(), t);
        }

        // возвращать что-то надо, даже если были ошибки
        return xml;
    }

    public static String prettyFormat(final byte[] bytes) {
        return prettyFormat(new String(bytes));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    private static final Logger log = Logger.getLogger(EnrFisServiceImpl.class);

    private final String proxyHost = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.enr.fis.proxy.host"));
    public String getProxyHost() { return this.proxyHost; }

    private final String proxyPort = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.enr.fis.proxy.port"));
    public String getProxyPort() { return this.proxyPort; }

    private final String baseUrl = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.enr.fis.connection.url"));
    public String getBaseUrl() { return this.baseUrl; }

    // minimum (connect - 10sec, read - 1min)
    // maximum (connect - 5min,  read - 30min)
    private final int timeout = Math.min(1000*60*5, Math.max(1000*10, parseTimeout()));
    public int getTimeout() { return this.timeout; }

    public int parseTimeout() {
        final String timeoutStr = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.enr.fis.connection.timeout"));
        if (null == timeoutStr) { return 0; }
        return Math.max(Integer.parseInt(timeoutStr), 0);
    }

    @Override
    public boolean isEnabled() {
        return (null != EnrFisServiceImpl.this.getBaseUrl());
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public EnrFisConnection connect(final String urlPostfix) {
        return new EnrFisConnection() {

            private String request_raw;
            @Override public String getRequestRaw() {
                return this.request_raw;
            }
            @Override public String getRequest() {
                return prettyFormat(this.getRequestRaw());
            }

            private String response_raw;
            @Override public String getResponseRaw() {
                return this.response_raw;
            }
            @Override public String getResponse() {
                return prettyFormat(this.getResponseRaw());
            }

            private URL url;
            @Override public URL getUrl() {
                return this.url;
            }

            private void write(final OutputStream os, final byte[] byteArray) throws IOException
            {
                this.request_raw = new String(byteArray, "UTF-8");
                IOUtils.write(byteArray, os);
            }

            @SuppressWarnings("unchecked")
            private byte[] read(final InputStream is) throws IOException
            {
                return (this.response_raw = IOUtils.toString(is, "UTF-8")).getBytes();
            }

            private HttpURLConnection getConnection(final String urlPostfix) throws IOException
            {
                final String baseUrl = EnrFisServiceImpl.this.getBaseUrl();
                if (StringUtils.isEmpty(baseUrl)) {
                    throw new IllegalConnectionParametersException("Property 'uni.enr.fis.connection.url' is not specified.");
                }

                this.url = new URL(baseUrl+"/"+urlPostfix);
                final HttpURLConnection conn = openConnection(this.url);
                int timeout = getTimeout();
                if (timeout > 0) {
                    conn.setConnectTimeout(timeout);
                    conn.setReadTimeout(6*timeout);
                }
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "text/xml");

                conn.connect();
                return conn;
            }

            protected HttpURLConnection openConnection(final URL url) throws IOException
            {
                final String proxyHost = getProxyHost();
                if (null != proxyHost) {
                    final int proxyPort = Integer.parseInt(getProxyPort());
                    final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
                    return (HttpURLConnection) url.openConnection(proxy);
                } else {
                    return (HttpURLConnection) url.openConnection();
                }
            }

            @Override
            public byte[] call(final byte[] request) {
                try {
                    final HttpURLConnection conn = this.getConnection(urlPostfix);
                    try {

                        // отправляем туда данные (надо делать buffered - т.к. пихать по одному байту в сокет - зло)
                        final BufferedOutputStream bs = new BufferedOutputStream(conn.getOutputStream());
                        this.write(bs, request);
                        bs.flush();

                        // принимаем данные обратно
                        return this.read(conn.getInputStream());

                    } catch (final Exception t) {

                        // формируем человекочитаемое сообщение об ошибке
                        Debug.exception(t.getMessage()+"\nrequest="+this.getRequest()+"\nresponse="+this.getResponse(), t);

                        // отправляем ошибку дальше
                        throw t;

                    } finally {

                        // соединение надо закрыть
                        conn.disconnect();
                    }
                } catch (final Throwable t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            }

            @Override
            public <T> T call(final Class<T> responseKlass, final byte[] request) {
                return fromXml(responseKlass, call(request));
            }

            @Override public <T> T call(final Class<T> responseKlass, final Object root) {
                return call(responseKlass, toXml(root));
            }

            @Override
            public void writeDebugToLogFile(final String postfix) {
                try {
                    final String timestamp = new DateFormatter("yyyyMMdd-HHmmss").format(new Date());
                    final File file = new File(Paths.get(System.getProperty("catalina.base"), "logs", "debug-"+timestamp+"-FisServiceImpl-"+postfix+".io.xml").toString());
                    if (!file.createNewFile()) {
                        throw new IllegalStateException();
                    }

                    final FileOutputStream ofs = new FileOutputStream(file);
                    final BufferedOutputStream bfs = new BufferedOutputStream(ofs);
                    IOUtils.write(this.getRequestRaw(), bfs);
                    IOUtils.write("\n", bfs);
                    IOUtils.write(this.getResponseRaw(), bfs);
                    IOUtils.write("\n", bfs);
                    bfs.close();

                } catch (final Throwable t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            }

        };
    }


}
