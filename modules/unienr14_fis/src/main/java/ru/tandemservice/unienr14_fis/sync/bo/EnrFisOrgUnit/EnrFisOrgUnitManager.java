package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic.EnrFisOrgUnitDao;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic.IEnrFisOrgUnitDao;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisOrgUnitManager extends BusinessObjectManager {

    public static EnrFisOrgUnitManager instance() {
        return instance(EnrFisOrgUnitManager.class);
    }

    @Bean
    public IEnrFisOrgUnitDao dao() {
        return new EnrFisOrgUnitDao();
    }
}
