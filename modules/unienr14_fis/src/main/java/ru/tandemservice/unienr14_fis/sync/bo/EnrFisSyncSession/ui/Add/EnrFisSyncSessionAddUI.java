/**
 *$Id: EnrDirectionAddUI.java 27166 2013-04-25 09:10:32Z vdanilov $
 */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.Add;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required=true),
    @Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaignId", required = true)
})
public class EnrFisSyncSessionAddUI extends UIPresenter
{

    private Long enrollmentCampaignId;
    public Long getEnrollmentCampaignId() { return this.enrollmentCampaignId; }
    public void setEnrollmentCampaignId(Long enrollmentCampaignId) { this.enrollmentCampaignId = enrollmentCampaignId; }

    private Long orgUnitId;
    public Long getOrgUnitId() { return this.orgUnitId; }
    public void setOrgUnitId(Long orgUnitId) { this.orgUnitId = orgUnitId; }

    private List<EnrFisCatalogItem> statusList;
    public List<EnrFisCatalogItem> getStatusList() { return this.statusList; }
    public void setStatusList(List<EnrFisCatalogItem> statusList) { this.statusList = statusList; }

    private List<DataWrapper> requestStates = Lists.newArrayList(new DataWrapper(2L, "Новое"), new DataWrapper(4L, "Принято"));

    public List<DataWrapper> getRequestStates()
    {
        return requestStates;
    }

    public void setRequestState(DataWrapper value)
    {
        if(value != null)
        {
            getFisSession().setRequestState(value.getId().intValue());
        }
    }

    public DataWrapper getRequestState()
    {
        return  getFisSession().getRequestState() == 2 ? requestStates.get(0) : requestStates.get(1);
    }

    private EnrFisSyncSession fisSession = new EnrFisSyncSession();
    public EnrFisSyncSession getFisSession() { return this.fisSession; }

    private EnrEnrollmentCampaign enrollmentCampaign;
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign) { this.enrollmentCampaign = enrollmentCampaign; }

    public void setFisSession(EnrFisSyncSession fisSession)
    {
        this.fisSession = fisSession;
    }

    @Override
    public void onComponentRefresh() {
        getFisSession().setRequestState(4);
        setEnrollmentCampaign(DataAccessServices.dao().getNotNull(getEnrollmentCampaignId()));
        ICommonDAO dao = DataAccessServices.dao();
        setStatusList(dao.getList(EnrFisCatalogItem.class, EnrFisCatalogItem.fisCatalogCode(), "34", EnrFisCatalogItem.fisItemCode().s()));
    }

    protected boolean validate()
    {
        final EnrFisSyncSession fisSession = getFisSession();
        boolean ret = true;
        if (!(fisSession.isSendEnrCampaignInfo() || fisSession.isSendAdmissionInfo() || fisSession.isSendApplicationsInfo() || fisSession.isSendOrdersOfAdmission())) {
            getSupport().error("Необходимо выбрать хотя бы один пакет.", "cb_sendEnrCampaignInfo", "cb_sendAdmissionInfo", "cb_sendApplicationsInfo", "cb_sendOrdersOfAdmission");
            ret = false;
        }
        return ret;
    }

    public void onClickApply()
    {
        if (!validate()) {
            return;
        }

        List<EnrOrgUnit> enrOrgUnits = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
            .fromEntity(EnrOrgUnit.class, "o").column("o")
            .where(eq(property("o", EnrOrgUnit.enrollmentCampaign()), value(getEnrollmentCampaign())))
            .where(eq(property("o", EnrOrgUnit.institutionOrgUnit().orgUnit().id()), value(getOrgUnitId())))
        );
        if (enrOrgUnits.isEmpty()) {
            throw new IllegalStateException(); // todo refactor it to orgUnits or smth
        }
        fisSession.setEnrOrgUnit(enrOrgUnits.iterator().next());

        if(!fisSession.isSendApplicationsInfo())
        {
            fisSession.setForceAllEntrants(false);
            fisSession.setAcceptedInfo(false);
            fisSession.setSendRequestInOrderInfo(false);
        }

        EnrFisSyncSessionManager.instance().dao().doSave(fisSession);
        deactivate();
    }

    public void onChangeInfoStatus()
    {
        if (getFisSession().getCampaignInfoStatus() == null) {
            return;
        }

        // Заполняем значения по умолчанию в зависимости от состояния ПК

        final EnrFisSyncSession fisSession = getFisSession();
        fisSession.setSendEnrCampaignInfo(true); // Пакет 1 для всех состояний актуален
        fisSession.setSendAdmissionInfo(false);
        fisSession.setSendApplicationsInfo(false);
        fisSession.setSendOrdersOfAdmission(false);

        switch (getFisSession().getCampaignInfoStatus().getFisItemCode())
        {
            case EnrFisCatalogItem.FIS_CAMPAIGN_INFO_STATUS_NOT_STARTED:
                fisSession.setSendAdmissionInfo(true);
                break;
            case EnrFisCatalogItem.FIS_CAMPAIGN_INFO_STATUS_IN_PROGRESS:
                fisSession.setSendAdmissionInfo(true);
                fisSession.setSendApplicationsInfo(true);
                fisSession.setSendOrdersOfAdmission(true);
                break;
            case EnrFisCatalogItem.FIS_CAMPAIGN_INFO_STATUS_FINISHED:
                //
                break;
        }
    }


}
