package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import enr14fis.schema.pkg.result.resp.ImportResultPackage;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.UidRegistryItem;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisSyncPackageReceiveIOLogGen;

import java.util.*;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Лог взаимодействия с сервером ФИС (получение результата)
 */
public class EnrFisSyncPackageReceiveIOLog extends EnrFisSyncPackageReceiveIOLogGen
{
    @Override
    public String getDescription() {
        if (isErrorByZipLog()) {
            if (getFisErrorText() != null && getFisErrorText().toLowerCase().contains("пакет еще не обработан")) {
                return "Пакет еще не обработан ФИС";
            }
            return "Ошибка при запросе результата обработки данных ФИС";
        }
        if (getFailureCount() > 0) { return "Получен результат обработки данных в ФИС, ошибок в данных: " + getFailureCount(); }
        if (getConflictCount() > 0) { return "Получен результат обработки данных в ФИС, конфликтов в данных: " + getConflictCount(); }
        return "Получен результат обработки данных в ФИС: данные корректны";
    }

    @Override
    public boolean isShowStatistic()
    {
        return !isErrorByZipLog() && (getFailureCount() + getConflictCount() + getSuccessfulCount() > 0);
    }

    @Override
    public String getStatistic()
    {
        return "некорректные данные: " + getFailureCount() + ", конфликты: " + getConflictCount() + ", успешно загружено в ФИС: " + getSuccessfulCount();
    }

    @Override
    public String getStatisticCaption()
    {
        return "Результат обработки данных ФИС";
    }

    public void countStatistic(ImportResultPackage result)
    {
        if (null == result) return;
        setConflictCount(countConflictStatistics(result));
        setFailureCount(countFailureStatistics(result));
        setSuccessfulCount(countSuccessStatistics(result));
    }

    protected int countConflictStatistics(ImportResultPackage result)
    {
        int dataNodeCount = 0;
        try { dataNodeCount += result.getConflicts().getApplications().getApplication().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getCompetitiveGroup().getCompetitiveGroupID().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getEntranceTestResults().getEntranceTestsResultID().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getApplicationCommonBenefits().getApplicationCommonBenefitID().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getInstitutionAchievements().getInstitutionAchievementID().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getTargetOrganizations().getTargetOrganizationID().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getOrders().getOrderID().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getConflicts().getApplicationsInOrders().getApplication().size(); } catch (NullPointerException ignored) {}
        return dataNodeCount;
    }

    protected int countFailureStatistics(ImportResultPackage result)
    {
        int dataNodeCount = 0;
        try { dataNodeCount += result.getLog().getFailed().getAdmissionVolumes().getAdmissionVolume().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getDistributedAdmissionVolumes().getDistributedAdmissionVolume().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getInstitutionAchievements().getInstitutionAchievement().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getCompetitiveGroups().getCompetitiveGroup().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getCompetitiveGroupItems().getCompetitiveGroupItem().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getTargetOrganizations().getTargetOrganization().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getTargetOrganizationDirections().getTargetOrganizationDirection().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getEntranceTestItems().getEntranceTestItem().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getCommonBenefit().getCommonBenefitItem().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getEntranceTestBenefits().getEntranceTestBenefitItem().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getApplications().getApplication().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getOlympicDocuments().getOlympicDocument().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getOlympicTotalDocuments().getOlympicTotalDocument().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getDisabilityDocuments().getDisabilityDocument().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getMedicalDocuments().getMedicalDocument().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getAllowEducationDocuments().getAllowEducationDocument().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getCampaigns().getCampaign().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getCustomDocuments().getCustomDocument().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getEntranceTestResults().getEntranceTestResult().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getApplicationCommonBenefits().getApplicationCommonBenefit().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getApplicationsInOrders().getApplicationsInOrder().size(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getFailed().getOrdersOfAdmissions().getOrdersOfAdmission().size(); } catch (NullPointerException ignored) {}
        return dataNodeCount;
    }

    protected int countSuccessStatistics(ImportResultPackage result)
    {
        int dataNodeCount = 0;
        try { dataNodeCount += result.getLog().getSuccessful().getCampaigns(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getAdmissionVolumes(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getCompetitiveGroups(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getCompetitiveGroupItems(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getDistributedAdmissionVolumes(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getInstitutionAchievements(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getTargetOrganizations(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getApplications(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getOrders(); } catch (NullPointerException ignored) {}
        try { dataNodeCount += result.getLog().getSuccessful().getApplicationsInOrders(); } catch (NullPointerException ignored) {}
        return dataNodeCount;
    }

    public static List<Long> getFailedEntrantIds(ImportResultPackage result) {
        ImportResultPackage.Log.Failed.Applications applications = null;
        try {
             applications = result.getLog().getFailed().getApplications();
        }
        catch (NullPointerException e) {
            return Collections.emptyList();
        }


        if (applications == null) return Collections.emptyList();

        Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");

        Set<String> allCandidates = Sets.newHashSet();

        for(ImportResultPackage.Log.Failed.Applications.Application application : applications.getApplication()) {
            Set<String> candidates = new HashSet<String>(Arrays.asList(StringUtils.split(application.getErrorInfo().getMessage(), " ()[]<>,.-_=:{}")));
            for (Iterator<String> it = candidates.iterator(); it.hasNext(); ) {
                String candidate = it.next();
                if (!pattern.matcher(candidate).matches()) {
                    it.remove();
                }
            }
            allCandidates.addAll(candidates);

        }

        if (allCandidates.isEmpty()) return Collections.emptyList();

        final String uidRegistryCode = EnrFisSettingsManager.instance().uidRegistry().getCode();
        final List<Object[]> rows = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
                        .fromEntity(UidRegistryItem.class, "i")
                        .column(property(UidRegistryItem.uid().fromAlias("i")))
                        .column(property(UidRegistryItem.relatedEntityId().fromAlias("i")))
                        .column(property(UidRegistryItem.relatedEntityTitle().fromAlias("i")))
                        .column(property(UidRegistryItem.relatedEntityComment().fromAlias("i")))
                        .where(eq(property(UidRegistryItem.registry().code().fromAlias("i")), value(uidRegistryCode)))
                        .where(in(property(UidRegistryItem.uid().fromAlias("i")), allCandidates))
                        .order(property(UidRegistryItem.id().fromAlias("i")), OrderDirection.desc));

        if (rows.isEmpty()) { return Collections.emptyList(); }

        List<Long> failedEntrantRequestsIds = Lists.newArrayList();
        for(Object[] row : rows)
        {
            failedEntrantRequestsIds.add((Long)row[1]);
        }

        return IUniBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "r")
                .column(property("r", EnrEntrantRequest.entrant().id()))
                .distinct()
                .where(in(property("r", EnrEntrantRequest.id()), failedEntrantRequestsIds)));
    }
}