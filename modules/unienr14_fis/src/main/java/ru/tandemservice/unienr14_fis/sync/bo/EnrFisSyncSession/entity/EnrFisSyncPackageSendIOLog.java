package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import enr14fis.schema.pkg.send.req.Root;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisSyncPackageSendIOLogGen;

/**
 * Лог взаимодействия с сервером ФИС (отправка пакета)
 */
public class EnrFisSyncPackageSendIOLog extends EnrFisSyncPackageSendIOLogGen
{
    @Override
    public String getDescription() {
        if (isErrorByZipLog()) { return "Ошибка при передаче данных в ФИС";}
        return "Передача данных в ФИС выполнена успешно";
    }

    @Override
    public boolean isShowStatistic()
    {
        return !isErrorByZipLog() && (getDataNodeCount() > 0);
    }

    @Override
    public String getStatistic()
    {
        return "передано объектов: " + getDataNodeCount();
    }

    @Override
    public String getStatisticCaption()
    {
        return "Результат передачи данных в ФИС";
    }

    public void countStatistics(Root root)
    {
        int dataNodeCount = 0;
        try {
            dataNodeCount += root.getPackageData().getCampaignInfo().getCampaigns().getCampaign().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getAdmissionInfo().getCompetitiveGroups().getCompetitiveGroup().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getInstitutionAchievements().getInstitutionAchievement().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getTargetOrganizations().getTargetOrganization().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getApplications().getApplication().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getOrders().getOrdersOfAdmission().getOrderOfAdmission().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getOrders().getOrdersOfException().getOrderOfException().size();
        } catch (NullPointerException e) {
            //
        }
        try {
            dataNodeCount += root.getPackageData().getOrders().getApplications().getApplication().size();
        } catch (NullPointerException e) {
            //
        }
        setDataNodeCount(dataNodeCount);
    }
}