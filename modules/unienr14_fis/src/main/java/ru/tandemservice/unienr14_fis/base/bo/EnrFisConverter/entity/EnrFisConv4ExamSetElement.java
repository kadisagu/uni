package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/**
 * ФИС: Сопоставление: Элемент набора вступительных испытаний
 */
public class EnrFisConv4ExamSetElement extends EnrFisConv4ExamSetElementGen
{
    public EnrFisConv4ExamSetElement()
    {
    }

    public EnrFisConv4ExamSetElement(IEnrExamSetElementValue examSetElementValue, EnrFisCatalogItem value)
    {
        setExamSetElement(examSetElementValue);
        setValue(value);
    }
}