package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.ui.EntrantList.EnrFisOrgUnitEntrantList;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.List4OrgUnit.EnrFisSyncSessionList4OrgUnit;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisOrgUnitTab extends BusinessComponentManager {

    // tab panel
    public static final String TAB_PANEL = "tabPanel";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint() {
        return this.tabPanelExtPointBuilder(TAB_PANEL)
        .addTab(componentTab("syncSessionTab", EnrFisSyncSessionList4OrgUnit.class).permissionKey("ui:holder.secModel.orgUnit_viewEnr14FisSyncTab"))
        .addTab(componentTab("entrantSyncStatusTab", EnrFisOrgUnitEntrantList.class).permissionKey("ui:holder.secModel.orgUnit_viewEnr14FisEntrantSyncStatusTab"))
        .create();
    }
}
