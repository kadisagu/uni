package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Индивидуальное достижение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4EntrantAchievementTypeGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4EntrantAchievementTypeGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType";
    public static final String ENTITY_NAME = "enrFisConv4EntrantAchievementType";
    public static final int VERSION_HASH = 924631561;
    private static IEntityMeta ENTITY_META;

    public static final String L_ACHIEVEMENT_TYPE = "achievementType";
    public static final String L_VALUE = "value";

    private EnrEntrantAchievementType _achievementType;     // Индивидуальное достижение
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEntrantAchievementType getAchievementType()
    {
        return _achievementType;
    }

    /**
     * @param achievementType Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     */
    public void setAchievementType(EnrEntrantAchievementType achievementType)
    {
        dirty(_achievementType, achievementType);
        _achievementType = achievementType;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4EntrantAchievementTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setAchievementType(((EnrFisConv4EntrantAchievementType)another).getAchievementType());
            }
            setValue(((EnrFisConv4EntrantAchievementType)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4EntrantAchievementTypeGen> getNaturalId()
    {
        return new NaturalId(getAchievementType());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4EntrantAchievementTypeGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4EntrantAchievementTypeNaturalProxy";

        private Long _achievementType;

        public NaturalId()
        {}

        public NaturalId(EnrEntrantAchievementType achievementType)
        {
            _achievementType = ((IEntity) achievementType).getId();
        }

        public Long getAchievementType()
        {
            return _achievementType;
        }

        public void setAchievementType(Long achievementType)
        {
            _achievementType = achievementType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4EntrantAchievementTypeGen.NaturalId) ) return false;

            EnrFisConv4EntrantAchievementTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getAchievementType(), that.getAchievementType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getAchievementType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getAchievementType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4EntrantAchievementTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4EntrantAchievementType.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4EntrantAchievementType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "achievementType":
                    return obj.getAchievementType();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "achievementType":
                    obj.setAchievementType((EnrEntrantAchievementType) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "achievementType":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "achievementType":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "achievementType":
                    return EnrEntrantAchievementType.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4EntrantAchievementType> _dslPath = new Path<EnrFisConv4EntrantAchievementType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4EntrantAchievementType");
    }
            

    /**
     * @return Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType#getAchievementType()
     */
    public static EnrEntrantAchievementType.Path<EnrEntrantAchievementType> achievementType()
    {
        return _dslPath.achievementType();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4EntrantAchievementType> extends EntityPath<E>
    {
        private EnrEntrantAchievementType.Path<EnrEntrantAchievementType> _achievementType;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType#getAchievementType()
     */
        public EnrEntrantAchievementType.Path<EnrEntrantAchievementType> achievementType()
        {
            if(_achievementType == null )
                _achievementType = new EnrEntrantAchievementType.Path<EnrEntrantAchievementType>(L_ACHIEVEMENT_TYPE, this);
            return _achievementType;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4EntrantAchievementType#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4EntrantAchievementType.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4EntrantAchievementType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
