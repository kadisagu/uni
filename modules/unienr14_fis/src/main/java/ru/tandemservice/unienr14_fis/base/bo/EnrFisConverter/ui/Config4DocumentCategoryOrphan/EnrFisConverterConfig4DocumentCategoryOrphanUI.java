/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategoryOrphan;

import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategory.EnrFisConverterConfig4DocumentCategoryUI;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public class EnrFisConverterConfig4DocumentCategoryOrphanUI extends EnrFisConverterConfig4DocumentCategoryUI
{
    public void onClickAutoSync()
    {
        EnrFisConverterManager.instance().documentCategoryOrphan().doAutoSync();
    }

    protected String getDocumentCategoryTypeCode()
    {
        return "Orphan";
    }
}
