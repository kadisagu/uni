package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.daemon;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

/**
 * @author vdanilov
 */
public interface IEnrFisSyncSessionSendDaemonBean {

    public static final SpringBeanCache<IEnrFisSyncSessionSendDaemonBean> instance = new SpringBeanCache<IEnrFisSyncSessionSendDaemonBean>(IEnrFisSyncSessionSendDaemonBean.class.getName());

    /**
     * Формирует перечень сессий, находящихся в очереди (по одной для каждой ПК), для которых есть неотправленные пакеты
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<EnrFisSyncSession> getQueuedSessions4Send();

    /**
     * Формирует перечень сессий, находящихся в очереди (по одной для каждой ПК), для которых есть пакеты без результата
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<EnrFisSyncSession> getQueuedSessions4Recv();

    /**
     * Выставляет признак архивности для сессий, у которых есть ответы на все пакеты
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doArchiveSessions();

    /**
     * @param fisSession
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Long getNextSendQueuePackageId(EnrFisSyncSession fisSession);

    /**
     * Пробует отправить пакет в ФИС (на время сохранеия объектов открывает транзакции)
     * @param fisSession
     * @param nextPackageId
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean tryToSendPackage(EnrFisSyncSession fisSession, Long nextPackageId);

    /**
     * @param fisSession
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Long getNextRecvQueuePackageId(EnrFisSyncSession fisSession);

    /**
     * Пробует получить ответ из ФИС (на время сохранеия объектов открывает транзакции)
     * @param fisSession
     * @param nextPackageId
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean tryToRecvPackage(EnrFisSyncSession fisSession, Long nextPackageId);

    /**
     * @param fisSessionId
     * @param fatalNow
     * @param message
     * @param error
     */
    @Transactional(propagation=Propagation.REQUIRED)
    void error(Long fisSessionId, boolean fatalNow, String message, Throwable error);

}
