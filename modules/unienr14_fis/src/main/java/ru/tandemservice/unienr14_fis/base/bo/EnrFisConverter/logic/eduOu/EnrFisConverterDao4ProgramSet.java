package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.gen.EnrFisCatalogItemGen;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4ProgramSetGen;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15664814")
public class EnrFisConverterDao4ProgramSet extends EnrFisConverterBaseDao<EnrProgramSetBase, Long> implements IEnrFisConverterDao4ProgramSet
{

    @Override
    public String getCatalogCode() {
        return "10";
    }

    @Override
    public List<EnrProgramSetBase> getEntityList() {
        return new DQLSelectBuilder()
            .fromEntity(EnrProgramSetBase.class, "eduOu")
            .column(property("eduOu"))
            .order(property(EnrProgramSetBase.programSubject().subjectCode().fromAlias("eduOu")))
            .order(property(EnrProgramSetBase.programSubject().title().fromAlias("eduOu")))
            .order(property(EnrProgramSetBase.title().fromAlias("eduOu")))
            .createStatement(this.getSession()).list();
    }

    @Override
    protected Long getItemMapKey(final EnrProgramSetBase entity) {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap() {
        return EnrFisConverterBaseDao.<Long, Long>map(scrollRows(
            new DQLSelectBuilder()
            .fromEntity(EnrFisConv4ProgramSet.class, "x")
            .column(property(EnrFisConv4ProgramSet.programSet().id().fromAlias("x")), "lvl_id")
            .column(property(EnrFisConv4ProgramSet.value().id().fromAlias("x")), "value_id")
            .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<EnrProgramSetBase, EnrFisCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EnrFisConv4ProgramSet> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<EnrProgramSetBase, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4ProgramSet(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4ProgramSetGen.NaturalId, EnrFisConv4ProgramSet>() {
            @Override protected EnrFisConv4ProgramSetGen.NaturalId key(final EnrFisConv4ProgramSet source) {
                return (EnrFisConv4ProgramSetGen.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4ProgramSet buildRow(final EnrFisConv4ProgramSet source) {
                return new EnrFisConv4ProgramSet(source.getProgramSet(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4ProgramSet target, final EnrFisConv4ProgramSet source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4ProgramSet databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4ProgramSet.class), targetRecords);

    }


    private static final Pattern SUBJECT_CODE_PATTERN = Pattern.compile("([0-9]{2}[.][0-9]{2}[.][0-9]{2}) .*");

    @Override
    public void doAutoSync()
    {
        final boolean hard = true; // false использовать только для тестовых целей (чтобы руками не набивать справочник)

        final List<EnrFisCatalogItem> itemList = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "i")
                .where(isNull(property("i", EnrFisCatalogItem.removalDate())))
                .where(eq(property("i", EnrFisCatalogItem.fisCatalogCode()), value(getCatalogCode())))
                .createStatement(getSession()).list();

        final Map<String, List<EnrFisCatalogItem>> code2ItemMap = new HashMap<>(itemList.size());
        for (final EnrFisCatalogItem item: itemList) {
            final Matcher matcher = SUBJECT_CODE_PATTERN.matcher(item.getTitle());
            if (matcher.matches()) {
                final String code = matcher.group(1);
                SafeMap.safeGet(code2ItemMap, code, ArrayList.class).add(item);
            }
        }

        final IEnrFisConverter<EnrProgramSetBase> converter = this.getConverter();
        final List<EnrProgramSetBase> eduOuList = this.getEntityList();
        final Map<EnrProgramSetBase, EnrFisCatalogItem> values = new HashMap<>(eduOuList.size());
        for (final EnrProgramSetBase ps: eduOuList) {
            final EnrFisCatalogItem current = converter.getCatalogItem(ps, false);
            if (null != current) {
                values.put(ps, current);
            } else {
                final List<EnrFisCatalogItem> list = code2ItemMap.get(ps.getProgramSubject().getSubjectCode());
                if ((null != list) && (hard ? (1 == list.size()) : (list.size() > 0))) {
                    // только если нашли единственное направление
                    values.put(ps, list.iterator().next());
                }
            }
        }

        this.update(values, false);
    }


    @Override
    public EnrFisCatalogItem getEnrFisItem4EducationLevel(EnrProgramSetBase programSet)
    {
        final String lvlCode = getEnrFisItemCode4EducationLevel(programSet);
        if (null == lvlCode) { return null; }
        return this.getByNaturalId(new EnrFisCatalogItemGen.NaturalId("2", lvlCode));
    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4ProgramSet.class, "b")
            .column(property(EnrFisConv4ProgramSet.value().fromAlias("b")))
            .where(isNotNull(property(EnrFisConv4ProgramSet.value().fromAlias("b"))))
            .createStatement(getSession()).list();
    }

    private String getEnrFisItemCode4EducationLevel(EnrProgramSetBase programSet)
    {
        /**
            http://wiki/pages/viewpage.action?pageId=20680494
         */
        EduProgramKind programKind = programSet.getProgramKind();

        if (EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(programKind.getCode())) {
            return "2";
        } else if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKind.getCode())) {
            return "5";
        } else if (EduProgramKindCodes.PROGRAMMA_MAGISTRATURY.equals(programKind.getCode())) {
            return "4";
        } else if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKind.getCode()) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKind.getCode())) {
            return "17";
        } else if (EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(programKind.getCode())) {
            return "18";
        } else if (EduProgramKindCodes.PROGRAMMA_ORDINATURY.equals(programKind.getCode())) {
            return "18";
        } else if (EduProgramKindCodes.PROGRAMMA_INTERNATURY.equals(programKind.getCode())) {
            return "18";
        }

        return null;
    }
}
