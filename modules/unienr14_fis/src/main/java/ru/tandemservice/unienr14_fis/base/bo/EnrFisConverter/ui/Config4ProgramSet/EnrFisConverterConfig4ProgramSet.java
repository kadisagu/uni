/**
 *$Id: EnrFisConverterConfig4ProgramSet.java 33760 2014-04-21 14:01:15Z nfedorovskih $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4ProgramSet;

import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 12.07.13
 */
@Configuration
public class EnrFisConverterConfig4ProgramSet extends EnrFisConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String STRUCTURE_EDU_LVL_DS = "structureEduLvlDS";

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String BIND_STRUCTURE_EDU_LVL = "structureEduLvl";
    public static final String BIND_ITEM_TITLE = "itemTitle";

    @Override
    protected IEnrFisConverterDao4ProgramSet getConverterDao() {
        return EnrFisConverterManager.instance().programSet();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(selectDS(STRUCTURE_EDU_LVL_DS, structureEduLvlDSHandler()).addColumn(StructureEducationLevels.shortTitle().s()))
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig()));
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
        .addColumn(textColumn("title", EnrProgramSetBase.title().s()).order())
        .addColumn(textColumn("programSubject", EnrProgramSetBase.programSubject().titleWithCode().s()).order())
        .addColumn(textColumn("developForm", EnrProgramSetBase.programForm().title().s()).order())
        .addColumn(textColumn("programKind", EnrProgramSetBase.programSubject().subjectIndex().programKind().shortTitle().s()).order())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4ProgramSetEdit").hasBlockHeader(true).create())
        .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        return super.fisConverterRecordDSHandler();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrollmentCampaign = context.get(BIND_ENROLLMENT_CAMPAIGN);
        // это понадобится, когда будет селектор if (null == enrollmentCampaign) { return new ArrayList<>(0); }
        if (null == enrollmentCampaign) { return new ArrayList<>(0); }
        final String itemTitle = context.get(BIND_ITEM_TITLE);
        return new ArrayList<>(Collections2.filter(
            entityList,
            input -> {
                if (!(input instanceof EnrProgramSetBase)) { return false; }
                final EnrProgramSetBase programSet = (EnrProgramSetBase)input;
                if (enrollmentCampaign != null && !programSet.getEnrollmentCampaign().equals(enrollmentCampaign)) { return false; }
                if (itemTitle != null && !StringUtils.containsIgnoreCase(programSet.getTitle(), itemTitle)) { return false; }
                return true;
            }
        ));
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return super.fisConverterValueDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> structureEduLvlDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StructureEducationLevels.class)
        .where(StructureEducationLevels.parent(), (Object) null)
        .order(StructureEducationLevels.priority())
        .filter(StructureEducationLevels.shortTitle())
        .pageable(true);
    }
}
