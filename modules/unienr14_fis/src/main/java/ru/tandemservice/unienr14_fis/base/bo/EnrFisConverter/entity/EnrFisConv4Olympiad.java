package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/**
 * ФИС: Сопоставление: Олимпиада
 */
public class EnrFisConv4Olympiad extends EnrFisConv4OlympiadGen
{
    public EnrFisConv4Olympiad()
    {
    }

    public EnrFisConv4Olympiad(EnrOlympiad olympiad, EnrFisCatalogItem fisCatalogItem)
    {
        setOlympiad(olympiad);
        setValue(fisCatalogItem);
    }
}