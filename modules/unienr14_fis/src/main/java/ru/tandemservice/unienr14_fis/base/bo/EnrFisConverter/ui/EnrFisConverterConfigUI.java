package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui;

import org.tandemframework.caf.command.BusinessCommand;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.DispatchSupport;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.UIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

import java.util.List;

/**
 * @author vdanilov
 */
public abstract class EnrFisConverterConfigUI extends UIPresenter {

    private BusinessCommand<DSInput, DSOutput> _commandRecordDS;
    private IUIDataSource fisConverterRecordDS;
    public IUIDataSource getFisConverterRecordDS() { return this.fisConverterRecordDS; }
    protected void setFisConverterRecordDS(final IUIDataSource fisConverterRecordDS) { this.fisConverterRecordDS = fisConverterRecordDS; }

    @Override
    public void onComponentRefresh() {
        this.setFisConverterRecordDS(this.getConfig().getDataSource(EnrFisConverterConfig.FIS_CONVERTER_RECORD_DS));
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        super.onAfterDataSourceFetch(dataSource);

        if (dataSource.getName().equals(EnrFisConverterConfig.FIS_CONVERTER_RECORD_DS))
        {
            _commandRecordDS = ((UIDataSource) dataSource).getDataSourceCommand();
            checkRowWithRemovalDate();
        }
    }

    @SuppressWarnings("unchecked")
    private void checkRowWithRemovalDate()
    {
        if (null == _commandRecordDS) return;
        final EnrFisConverterConfig manager = this.getConfig().getManager();
        final IEnrFisConverter<IEntity> converter = manager.getConverterDao().getConverter();

        ExecutionContext executionContext = new ExecutionContext(_commandRecordDS, new DispatchSupport());
        final List<IEntity> entityList = manager.filterConverterRecordDS(converter.getEntityList(), executionContext);
        for (IEntity entity : entityList)
        {
            final EnrFisCatalogItem fisItem = converter.getCatalogItem(entity, false);
            if (null != fisItem && null != fisItem.getRemovalDate())
            {
                ContextLocal.getInfoCollector().add("Сопоставленные элементы справочников ФИС потеряли актуальность. Проверьте сопоставление данных.");
                return;
            }
        }
    }

    public DataWrapper getCurrentRow() {
        return getFisConverterRecordDS() == null ? null : (DataWrapper) this.getFisConverterRecordDS().getCurrent();
    }

    public Long getCurrentRowId() {
        final DataWrapper currentRow = this.getCurrentRow();
        return (null == currentRow ? null : currentRow.getId());
    }

    public IEntity getCurrentRowValue() {
        final DataWrapper currentRow = this.getCurrentRow();
        return (null == currentRow ? null : (IEntity)currentRow.get(EnrFisConverterConfig.PROPERTY_FIS_VALUE));
    }

    private DataWrapper currentEditRow;
    public DataWrapper getCurrentEditRow() { return this.currentEditRow; }
    protected void setCurrentEditRow(final DataWrapper currentEditRow) { this.currentEditRow = currentEditRow; }

    public boolean isComponentInEditMode() {
        return null != this.getCurrentEditRow();
    }

    public IEntity getCurrentEditRowValue() {
        if (!this.isComponentInEditMode()) { throw new IllegalStateException(); }
        return this.getCurrentEditRow().get(EnrFisConverterConfig.PROPERTY_FIS_VALUE);
    }

    public void setCurrentEditRowValue(final IEntity value) {
        if (!this.isComponentInEditMode()) { throw new IllegalStateException(); }
        this.getCurrentEditRow().put(EnrFisConverterConfig.PROPERTY_FIS_VALUE, value);
    }



    public boolean isCurrentRowInEditMode() {
        final DataWrapper editRow = this.getCurrentEditRow();
        if (null == editRow) { return false; }

        final DataWrapper currentRow = this.getCurrentRow();
        if (null == currentRow) { return false; }

        return editRow.getId().equals(currentRow.getId());
    }

    public void onClickEditRow()
    {
        final Long id = this.getListenerParameterAsLong();
        if (null == id) { return; }

        final IUIDataSource ds = this.getFisConverterRecordDS();

        final DataWrapper current = ds.getRecordById(id);
        if (null == current) { return; }

        final DataWrapper wrapper = this.buildEditRowDataWrapper(current);
        if (null == wrapper) { return; }

        this.setCurrentEditRow(wrapper);
    }

    public void onClickSaveRow()
    {
        final Long id = this.getListenerParameterAsLong();
        if (null == id) { return; }

        final DataWrapper editRow = this.getCurrentEditRow();
        if (null == editRow) { return; }

        if (!id.equals(editRow.getId())) { return; }

        try {
            if (this.doSaveRow(editRow)) {
                this.setCurrentEditRow(null);
            }
        } catch (final Throwable t) {
            this.getSupport().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        ContextLocal.getInfoCollector().clear();
    }

    protected DataWrapper buildEditRowDataWrapper(final DataWrapper current) {
        final DataWrapper editWrapper = new DataWrapper(current.getWrapped());
        editWrapper.put(EnrFisConverterConfig.PROPERTY_FIS_VALUE, current.getProperty(EnrFisConverterConfig.PROPERTY_FIS_VALUE));
        return editWrapper;
    }

    @SuppressWarnings("unchecked")
    protected boolean doSaveRow(final DataWrapper editRow) {
        final EnrFisConverterConfig manager = this.getConfig().getManager();
        final IEnrFisConverterDao<IEntity> converterDao = manager.getConverterDao();
        final EnrFisCatalogItem value = editRow.get(EnrFisConverterConfig.PROPERTY_FIS_VALUE);
        converterDao.update(editRow.<IEntity>getWrapped(), value);
        return true;
    }

}
