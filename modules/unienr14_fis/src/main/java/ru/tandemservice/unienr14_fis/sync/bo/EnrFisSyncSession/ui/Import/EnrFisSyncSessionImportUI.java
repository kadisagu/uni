/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.Import;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 20.07.15
 * Time: 2:08
 */
@Input({
        @Bind(key = "enrCampaignId", binding = "enrCampaignId", required=true),
        @Bind(key = "orgUnitId", binding = "orgUnitId", required=true)
})
public class EnrFisSyncSessionImportUI extends UIPresenter
{
    private Long enrCampaignId;
    private Long orgUnitId;

    private IUploadFile file;

    public void onClickApply()
    {
        EnrFisSyncSessionManager.instance().offlineDao().doImportSessionsXmlResults(file, enrCampaignId, orgUnitId);
        deactivate();
    }

    // getters and setters
    public IUploadFile getFile()
    {
        return file;
    }

    public void setFile(IUploadFile file)
    {
        this.file = file;
    }

    public Long getEnrCampaignId()
    {
        return enrCampaignId;
    }

    public void setEnrCampaignId(Long enrCampaignId)
    {
        this.enrCampaignId = enrCampaignId;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }
}
