/* $Id: DppOrgUnitAddEditUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.EcPeriodEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey;

import java.util.*;

@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")
})
public class EnrFisSettingsEcPeriodEditUI extends UIPresenter
{

    private static final IdentifiableWrapper STAGE_COUNT_OPTIONS[] = {
        wrapper(0),
        wrapper(1),
        wrapper(2),
        //        wrapper(3) В связи с тем, что ФИС в этом году активно отвергает 3 этапа, необходимо в интерфейсе в селекте убрать возможность выбора 3 этапа.
        //                   http://tracker.tandemservice.ru/browse/DEV-1483?focusedCommentId=164629&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-164629
    };

    private static IdentifiableWrapper wrapper(int size) {
        return new IdentifiableWrapper((long) size, EnrFisSettingsECPeriodKey.getSizeTitle(size));
    }

    private final EntityHolder<EnrFisSettingsECPeriodKey> holder = new EntityHolder<EnrFisSettingsECPeriodKey>();

    public EntityHolder<EnrFisSettingsECPeriodKey> getHolder() { return this.holder; }
    public EnrFisSettingsECPeriodKey getKey() { return this.getHolder().getValue(); }

    private List<EnrFisSettingsECPeriodItem> globalItemList = Collections.emptyList();
    public List<EnrFisSettingsECPeriodItem> getGlobalItemList() { return this.globalItemList; }
    public void setGlobalItemList(List<EnrFisSettingsECPeriodItem> globalItemList) { this.globalItemList = globalItemList; }

    public List<EnrFisSettingsECPeriodItem> getItemList() {
        final List<EnrFisSettingsECPeriodItem> list = getGlobalItemList();
        return list.subList(0, Math.min(getKey().getSize(), list.size()));
    }

    private EnrFisSettingsECPeriodItem currentItem;
    public EnrFisSettingsECPeriodItem getCurrentItem() { return this.currentItem; }
    public void setCurrentItem(EnrFisSettingsECPeriodItem currentItem) { this.currentItem = currentItem; }

    private List<IdentifiableWrapper> stageCountOptionList = Arrays.asList(STAGE_COUNT_OPTIONS);
    public List<IdentifiableWrapper> getStageCountOptionList() { return this.stageCountOptionList; }

    public IdentifiableWrapper getStageCount() {
        return wrapper(getKey().getSize());
    }
    public void setStageCount(IdentifiableWrapper stageCount) {
        if (null == stageCount || null == stageCount.getId() || stageCount.getId().intValue() < 0) {
            getKey().setSize(0);
        } else {
            getKey().setSize(stageCount.getId().intValue());
        }
    }

    @Override
    public void onComponentRefresh() {
        EnrFisSettingsECPeriodKey key = this.getHolder().refresh();
        setGlobalItemList(DataAccessServices.dao().getList(EnrFisSettingsECPeriodItem.class, EnrFisSettingsECPeriodItem.key(), key, EnrFisSettingsECPeriodItem.stage().s()));
        onSelectSize();
    }

    public void onSelectSize() {

        EnrFisSettingsECPeriodKey key = getKey();

        // берем все, что уже есть
        final Map<Integer, EnrFisSettingsECPeriodItem> map = new HashMap<Integer, EnrFisSettingsECPeriodItem>();
        for (EnrFisSettingsECPeriodItem item: getGlobalItemList()) {
            map.put(item.getStage(), item);
        }

        // добавляем недостающие
        int size = key.getSize();
        for (int i=1; i<= size; i++) {
            if (null == map.get(i)) {
                EnrFisSettingsECPeriodItem value = new EnrFisSettingsECPeriodItem(key, i);
                value.setId(Long.valueOf(System.identityHashCode(value)));
                map.put(i, value);
            }
        }
        setGlobalItemList(new ArrayList<EnrFisSettingsECPeriodItem>(map.values()));

        // сортируем
        Collections.sort(getGlobalItemList());
    }

    public void onClickApply() {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        if (!EnrFisSettingsManager.instance().dao().doValidateEnrFisSettingsECPeriodKey(getKey(), getItemList(), errorCollector)) {
            return;
        }

        EnrFisSettingsManager.instance().dao().doSaveEnrFisSettingsECPeriodKey(getKey(), getItemList());
        deactivate();
    }




}
