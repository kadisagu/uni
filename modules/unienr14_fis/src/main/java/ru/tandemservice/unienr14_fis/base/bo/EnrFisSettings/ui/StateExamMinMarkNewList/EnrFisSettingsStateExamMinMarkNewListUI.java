/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkNewList;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.OrgUnitCredentialsList.EnrFisSettingsOrgUnitCredentialsList;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkAddEdit.EnrFisSettingsStateExamMinMarkAddEdit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkNewAddEdit.EnrFisSettingsStateExamMinMarkNewAddEdit;

/**
 * @author nvankov
 * @since 7/9/14
 */
public class EnrFisSettingsStateExamMinMarkNewListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh() {
        getSettings().set(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(getEnrollmentCampaign() != null)
        {
            dataSource.put(EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID, getEnrollmentCampaign().getId());
        }
    }

    public boolean isNothingSelected()
    {
        return null == getEnrollmentCampaign();
    }

    public boolean isNotActual()
    {
        return CollectionUtils.exists(_uiConfig.getDataSource(EnrFisSettingsStateExamMinMarkNewList.STATE_EXAM_MIN_MARKS_DS).<EnrFisSettingsStateExamMinMarkNew>getRecords()
                , (o -> o.getSubject().getRemovalDate() != null));
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN);
    }

    public void onClickMigrateFromOld()
    {
        EnrFisSettingsManager.instance().dao().doMigrateStateExamFromOldSetting(getEnrollmentCampaign().getId());
    }

    public void onClickAdd()
    {
         _uiActivation.asRegionDialog(EnrFisSettingsStateExamMinMarkNewAddEdit.class)
                 .parameter(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign().getId())
                 .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EnrFisSettingsStateExamMinMarkNewAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign().getId())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
