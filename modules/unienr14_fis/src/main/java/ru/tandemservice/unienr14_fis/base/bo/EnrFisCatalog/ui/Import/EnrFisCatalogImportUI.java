/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.ui.Import;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.EnrFisCatalogManager;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 20.07.15
 * Time: 2:08
 */
public class EnrFisCatalogImportUI extends UIPresenter
{
    private IUploadFile file;

    public void onClickApply()
    {
        EnrFisCatalogManager.instance().dao().doImportCatalogs(getFile());
        deactivate();
    }

    // getters and setters
    public IUploadFile getFile()
    {
        return file;
    }

    public void setFile(IUploadFile file)
    {
        this.file = file;
    }
}
