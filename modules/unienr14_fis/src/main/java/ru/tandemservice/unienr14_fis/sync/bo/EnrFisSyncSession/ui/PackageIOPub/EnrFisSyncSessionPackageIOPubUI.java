package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.PackageIOPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true)
})
public class EnrFisSyncSessionPackageIOPubUI extends UIPresenter {

    private final EntityHolder<EnrFisSyncPackageIOLog> holder = new EntityHolder<EnrFisSyncPackageIOLog>();
    public EntityHolder<EnrFisSyncPackageIOLog> getHolder() { return this.holder; }
    public EnrFisSyncPackageIOLog getIoLog() { return getHolder().getValue(); }

    public OrgUnit getOrgUnit() {
        return getIoLog().getOwner().getSession().getEnrOrgUnit().getInstitutionOrgUnit().getOrgUnit();
    }

    private OrgUnitSecModel _secModel;
    public  OrgUnitSecModel getSecModel() { return _secModel; }

    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(String selectedTab) { this.selectedTab = selectedTab; }

    @Override
    public void onComponentRefresh() {
        getHolder().refresh();
        _secModel = new OrgUnitSecModel(getOrgUnit());
    }

    public void onClickDownloadRequest() {
        EnrFisSyncPackageIOLog ioLog = getIoLog();
        if (ioLog.getFormattedXmlRequest() == null)
            throw new ApplicationException("«ZIP-XML: отправленные данные в ФИС» пуст");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(getGlobalFileNamePrefix() + " - request.xml").document(ioLog.getFormattedXmlRequest().getBytes()).xml(), false);

    }

    public void onClickDownloadResponse() {
        EnrFisSyncPackageIOLog ioLog = getIoLog();
        if (ioLog.getFormattedXmlResponse() == null)
            throw new ApplicationException("«ZIP-XML: ответ из ФИС» пуст");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(getGlobalFileNamePrefix() + " - response.xml").document(ioLog.getFormattedXmlResponse().getBytes()).xml(), false);

    }

    private String getGlobalFileNamePrefix() {
        EnrFisSyncPackageIOLog ioLog = getIoLog();
        EnrFisSyncSession fisSession = ioLog.getOwner().getSession();
        return fisSession.getEnrOrgUnit().getEnrollmentCampaign().getTitle()+" - "+EnrFisSyncSession.DATE_FORMATTER_FILE.format(fisSession.getFillDate()) + " - " + EnrFisSyncSession.DATE_FORMATTER_FILE.format(ioLog.getOperationDate());
    }

}
