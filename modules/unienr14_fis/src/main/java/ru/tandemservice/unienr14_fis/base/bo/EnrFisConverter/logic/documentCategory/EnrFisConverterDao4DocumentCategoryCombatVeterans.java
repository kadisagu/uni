/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4DocumentCategoryCombatVeterans;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base.EnrFisConverterDao4DocumentCategory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public class EnrFisConverterDao4DocumentCategoryCombatVeterans extends EnrFisConverterDao4DocumentCategory
{
    @Override
    public String getCatalogCode()
    {
        return "45";
    }

    protected List<PersonDocumentCategory> getEnrDocumentCategory(String entrantDocTypeCode)
    {
        return super.getEnrDocumentCategory(PersonDocumentTypeCodes.COMBAT_VETERANS);
    }

    protected Class<? extends IEnrFisConv4DocumentCategory> getEnrFisConv4DocumentCategoryClass()
    {
        return EnrFisConv4DocumentCategoryCombatVeterans.class;
    }

    @Override
    public void update(final Map<PersonDocumentCategory, EnrFisCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        List<EnrFisConv4DocumentCategoryCombatVeterans> targetRecords = values.entrySet().stream()
                .filter(e -> null != e.getValue())
                .map(e -> new EnrFisConv4DocumentCategoryCombatVeterans(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4DocumentCategoryCombatVeterans.NaturalId, EnrFisConv4DocumentCategoryCombatVeterans>()
        {
            @Override protected EnrFisConv4DocumentCategoryCombatVeterans.NaturalId key(final EnrFisConv4DocumentCategoryCombatVeterans source) { return (EnrFisConv4DocumentCategoryCombatVeterans.NaturalId) source.getNaturalId(); }
            @Override protected EnrFisConv4DocumentCategoryCombatVeterans buildRow(final EnrFisConv4DocumentCategoryCombatVeterans source) { return new EnrFisConv4DocumentCategoryCombatVeterans(source.getDocCategory(), source.getValue()); }
            @Override protected void fill(final EnrFisConv4DocumentCategoryCombatVeterans target, final EnrFisConv4DocumentCategoryCombatVeterans source) { target.update(source, false); }
            @Override protected void doDeleteRecord(final EnrFisConv4DocumentCategoryCombatVeterans databaseRecord) { if (clearNulls) { super.doDeleteRecord(databaseRecord); } }
        }.merge(this.getList(EnrFisConv4DocumentCategoryCombatVeterans.class), targetRecords);
    }
}