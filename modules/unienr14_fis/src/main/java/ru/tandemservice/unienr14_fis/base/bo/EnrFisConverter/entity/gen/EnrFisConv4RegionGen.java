package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Регионы РФ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4RegionGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4RegionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region";
    public static final String ENTITY_NAME = "enrFisConv4Region";
    public static final int VERSION_HASH = 329011278;
    private static IEntityMeta ENTITY_META;

    public static final String L_ADDRESS_ITEM = "addressItem";
    public static final String L_VALUE = "value";

    private AddressItem _addressItem;     // Регион
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Регион. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AddressItem getAddressItem()
    {
        return _addressItem;
    }

    /**
     * @param addressItem Регион. Свойство не может быть null и должно быть уникальным.
     */
    public void setAddressItem(AddressItem addressItem)
    {
        dirty(_addressItem, addressItem);
        _addressItem = addressItem;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4RegionGen)
        {
            if (withNaturalIdProperties)
            {
                setAddressItem(((EnrFisConv4Region)another).getAddressItem());
            }
            setValue(((EnrFisConv4Region)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4RegionGen> getNaturalId()
    {
        return new NaturalId(getAddressItem());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4RegionGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4RegionNaturalProxy";

        private Long _addressItem;

        public NaturalId()
        {}

        public NaturalId(AddressItem addressItem)
        {
            _addressItem = ((IEntity) addressItem).getId();
        }

        public Long getAddressItem()
        {
            return _addressItem;
        }

        public void setAddressItem(Long addressItem)
        {
            _addressItem = addressItem;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4RegionGen.NaturalId) ) return false;

            EnrFisConv4RegionGen.NaturalId that = (NaturalId) o;

            if( !equals(getAddressItem(), that.getAddressItem()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getAddressItem());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getAddressItem());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4RegionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4Region.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4Region();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "addressItem":
                    return obj.getAddressItem();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "addressItem":
                    obj.setAddressItem((AddressItem) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "addressItem":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "addressItem":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "addressItem":
                    return AddressItem.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4Region> _dslPath = new Path<EnrFisConv4Region>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4Region");
    }
            

    /**
     * @return Регион. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region#getAddressItem()
     */
    public static AddressItem.Path<AddressItem> addressItem()
    {
        return _dslPath.addressItem();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4Region> extends EntityPath<E>
    {
        private AddressItem.Path<AddressItem> _addressItem;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Регион. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region#getAddressItem()
     */
        public AddressItem.Path<AddressItem> addressItem()
        {
            if(_addressItem == null )
                _addressItem = new AddressItem.Path<AddressItem>(L_ADDRESS_ITEM, this);
            return _addressItem;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4Region#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4Region.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4Region";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
