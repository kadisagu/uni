package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/**
 * ФИС: Сопоставление: Страны
 */
public class EnrFisConv4Country extends EnrFisConv4CountryGen
{
    public EnrFisConv4Country() {}
    public EnrFisConv4Country(AddressCountry addressCountry, EnrFisCatalogItem value) {
        setAddressCountry(addressCountry);
        setValue(value);
    }
}