/**
 *$Id: IEnrFisConverterDao4DiplomaType.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.diplomaType;

import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadHonour;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterStaticDao;

/**
 * @author Alexander Shaburov
 * @since 23.07.13
 */
public interface IEnrFisConverterDao4DiplomaType extends IEnrFisConverterStaticDao<EnrOlympiadHonour>
{
}
