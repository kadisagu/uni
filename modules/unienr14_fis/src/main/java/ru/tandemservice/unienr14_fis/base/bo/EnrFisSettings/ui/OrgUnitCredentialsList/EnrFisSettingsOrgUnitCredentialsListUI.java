/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.OrgUnitCredentialsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.OrgUnitCredentialsEdit.EnrFisSettingsOrgUnitCredentialsEdit;

/**
 * @author oleyba
 * @since 6/2/14
 */
public class EnrFisSettingsOrgUnitCredentialsListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh() {
        getSettings().set(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        
    }

    public void onClickEdit() {
        this._uiActivation.asRegionDialog(EnrFisSettingsOrgUnitCredentialsEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, this.getListenerParameterAsLong())
            .activate();
    }
    
    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
    }

    public boolean isNothingSelected()
    {
        return null == getEnrollmentCampaign();
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN);
    }
}