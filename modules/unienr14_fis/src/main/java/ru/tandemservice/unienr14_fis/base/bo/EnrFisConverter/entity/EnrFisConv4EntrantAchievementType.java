package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4EntrantAchievementTypeGen;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4EntrantAchievementTypeGen */
public class EnrFisConv4EntrantAchievementType extends EnrFisConv4EntrantAchievementTypeGen
{
    public EnrFisConv4EntrantAchievementType()
    {
    }

    public EnrFisConv4EntrantAchievementType(EnrEntrantAchievementType achievementType, EnrFisCatalogItem value)
    {
        setAchievementType(achievementType);
        setValue(value);
    }
}