package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.*;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p1.IPkgDao4CampaignInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p1.PkgDao4CampaignInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p2.IPkgDao4AdmissionInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p2.PkgDao4AdmissionInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p3.IPkgDao4ApplicationsInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p3.PkgDao4ApplicationsInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p4.IPkgDao4OrdersOfAdmission;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p4.PkgDao4OrdersOfAdmission;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisSyncSessionManager extends BusinessObjectManager {

    public static EnrFisSyncSessionManager instance() {
        return instance(EnrFisSyncSessionManager.class);
    }

    @Bean
    public IEnrFisSyncSessionDao dao() {
        return new EnrFisSyncSessionDao();
    }

    @Bean
    public IEnrFisOfflineDao offlineDao() {
        return new EnrFisOfflineDao();
    }

    @Bean
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм формирования пакета в рамках сессии")
    public IPkgDao4CampaignInfo dao4CampaignInfo() {
        return new PkgDao4CampaignInfo();
    }

    @Bean
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм формирования пакета в рамках сессии")
    public IPkgDao4AdmissionInfo dao4AdmissionInfo() {
        return new PkgDao4AdmissionInfo();
    }

    @Bean
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм формирования пакета в рамках сессии")
    public IPkgDao4ApplicationsInfo dao4ApplicationInfo() {
        return new PkgDao4ApplicationsInfo();
    }

    @Bean
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм формирования пакета в рамках сессии")
    public IPkgDao4OrdersOfAdmission dao4OrdersOfAdmission()
    {
        return new PkgDao4OrdersOfAdmission();
    }


    @Bean
    public IEnr14FisAppDeletePackageGenerator deletePackageGenerator()
    {
        return new Enr14FisAppDeletePackageGenerator();
    }
}
