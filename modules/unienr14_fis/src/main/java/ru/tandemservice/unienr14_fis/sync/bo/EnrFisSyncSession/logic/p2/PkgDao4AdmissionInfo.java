package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p2;

import com.google.common.collect.Sets;
import enr14fis.schema.pkg.send.req.PackageData;
import enr14fis.schema.pkg.send.req.PackageData.AdmissionInfo;
import enr14fis.schema.pkg.send.req.PackageData.AdmissionInfo.AdmissionVolume;
import enr14fis.schema.pkg.send.req.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup;
import enr14fis.schema.pkg.send.req.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.TargetOrganizations.TargetOrganization;
import enr14fis.schema.pkg.send.req.Root;
import enr14fis.schema.pkg.send.req.TEntranceTestSubject;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.*;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConvertersSimple;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduForm.IEnrFisConverterDao4EduForm;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.finSource.IEnrFisFinSourceDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4AdmissionInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.PkgDaoBase;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public class PkgDao4AdmissionInfo extends PkgDaoBase implements IPkgDao4AdmissionInfo
{
    private static final String BUDGET_PREFIX = "numberBudget";
    private static final String QUOTA_PREFIX = "numberQuota";
    private static final String PAID_PREFIX = "numberPaid";
    private static final String TARGET_PREFIX = "numberTarget";

    @Override
    public EnrFisSyncPackage4AdmissionInfo doCreatePackage(final EnrFisSyncSession fisSession)
    {
        if (fisSession.isSkipAdmissionInfo()) { return null; }

        final EnrFisSyncPackage4AdmissionInfo dbo = new EnrFisSyncPackage4AdmissionInfo();
        dbo.setCreationDate(new Date());
        dbo.setSession(fisSession);

        Context context = new Context(dbo);

        dbo.setXmlPackage(EnrFisServiceImpl.toXml(context.buildRoot()));
        this.saveOrUpdate(dbo);
        flush(fisSession);
        return dbo;
    }

    private class Context {
        final String PREFIX = EnrFisSyncSessionManager.instance().getProperty("title.admission");
        final EnrFisSyncSession fisSession;

        final EnrOrgUnit enrOrgUnit;
        final EnrEnrollmentCampaign enrollmentCampaign;
        final TopOrgUnit topOrgUnit;
        final boolean taKindsAllowed;

        final List<EnrExamPassForm> passForms;

        final IEnrFisConverterDao4EduForm eduFormDao;
        final IEnrFisConverterDao4ProgramSet eduOrgUnitDao;
        final IEnrFisFinSourceDao finSourceDao;
        final IEnrFisConverter<EnrProgramSetBase> programSetConverter;
        final IEnrFisConverter<EnrExamType> examTypeConverter;
        final IEnrFisConverter<IEnrExamSetElementValue> examSetElementConverter;

        final List<EnrCompetition> competitionList;
        final List<EnrProgramSetOrgUnit> programSetOrgUnitList;
        final Map<EnrCompetition, List<EnrTargetAdmissionCompetitionPlan>> taPlanMap;
        final Map<EnrProgramSetBase, Set<EduProgramProf>> eduProgramProfMap;
        final Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContentMap;
        final Set<Long> competitionsWithEnrolled;

        final AdmissionInfo xmlAdmissionInfo;

        Context(final EnrFisSyncPackage4AdmissionInfo dbo) {

            fisSession = dbo.getSession();

            status(fisSession, PREFIX, 0);

            enrOrgUnit = fisSession.getEnrOrgUnit();
            enrollmentCampaign = enrOrgUnit.getEnrollmentCampaign();
            topOrgUnit = TopOrgUnit.getInstance();

            taKindsAllowed = enrollmentCampaign.getSettings().isTargetAdmissionCompetition();

            passForms = getCatalogItemList(EnrExamPassForm.class);

            eduFormDao = EnrFisConverterManager.instance().educationForm();
            eduOrgUnitDao = EnrFisConverterManager.instance().programSet();
            finSourceDao = EnrFisConverterManager.instance().financingSource();
            programSetConverter = eduOrgUnitDao.getConverter();
            examTypeConverter = EnrFisConverterManager.instance().examType().getConverter();
            examSetElementConverter = EnrFisConverterManager.instance().examSetElement().getConverter();

            competitionList = EnrFisSettingsManager.instance().dao().getCompetitions(enrOrgUnit);
            programSetOrgUnitList = getProgramSetOrgUnitList(competitionList);
            taPlanMap = getTaPlanMap(programSetOrgUnitList);
            eduProgramProfMap = getEduProgramProfMap();
            examSetContentMap = getExamSetContent(competitionList);

            competitionsWithEnrolled = new HashSet<>();
            competitionsWithEnrolled.addAll(new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "r")
                    .where(eq(property("r", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(enrollmentCampaign)))
                    .where(eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                    .column(property("r", EnrRequestedCompetition.competition().id()))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(getSession()).list()
            );

            xmlAdmissionInfo = new AdmissionInfo();
            xmlAdmissionInfo.setAdmissionVolume(new AdmissionVolume());
            xmlAdmissionInfo.setCompetitiveGroups(new AdmissionInfo.CompetitiveGroups());

        }

        final Map<MultiKey, AdmissionVolume.Item> xmlAdmVolumeItemMap = new HashMap<>();
        final Map<EnrProgramSetBase, AdmissionVolume.Item> xmlAdmVolumeMap = SafeMap.get(new SafeMap.Callback<EnrProgramSetBase, AdmissionVolume.Item>() {
            @Override public AdmissionVolume.Item resolve(final EnrProgramSetBase ps) {

                // todo разобраться, что тут происходит с передачей

                final EnrFisCatalogItem fisEduLvl = eduOrgUnitDao.getEnrFisItem4EducationLevel(ps);
                if (null == fisEduLvl) {
                    // здесь ошибку писать не надо - она уже была добавлена при геренации 2ого пакета и будет продублирована ниже для абитуриента
                    // fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-level", getTitle(dir)));
                    final AdmissionVolume.Item errorAdmVolumeItem = new AdmissionVolume.Item();
                    errorAdmVolumeItem.setCampaignUID("-");
                    return errorAdmVolumeItem; // просто возвращаем заглушку
                }

                final EnrFisCatalogItem fisDirection = programSetConverter.getCatalogItem(ps, false);
                if (null == fisDirection) {
                    // здесь ошибку писать не надо - она уже была добавлена при геренации 2ого пакета и будет продублирована ниже для абитуриента
                    // fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-direction", getTitle(dir)));
                    final AdmissionVolume.Item errorAdmVolumeItem = new AdmissionVolume.Item();
                    errorAdmVolumeItem.setCampaignUID("-");
                    return errorAdmVolumeItem; // просто возвращаем заглушку
                }

                MultiKey key = new MultiKey(fisEduLvl.getFisID(), fisDirection.getFisID(), 1);
                AdmissionVolume.Item existingItem = xmlAdmVolumeItemMap.get(key);
                if (null != existingItem) return existingItem;

                final AdmissionVolume.Item item = new AdmissionVolume.Item();
                xmlAdmissionInfo.getAdmissionVolume().getItem().add(item);
                xmlAdmVolumeItemMap.put(key, item);

                item.setCampaignUID(uid(enrollmentCampaign));
                item.setEducationLevelID(fisEduLvl.getFisID());
                item.setDirectionID(fisDirection.getFisID());

                String itemUid = admissionVolumeItemUid(enrollmentCampaign, fisEduLvl, fisDirection, ps.getProgramSubject());
                item.setUID(itemUid);

                return item;
            }
        });

        protected Root buildRoot()
        {
            Collections.sort(competitionList, new EntityComparator<>(new EntityOrder(EnrCompetition.type().code(), OrderDirection.desc)));

            final Map<String, CompetitiveGroup> fisCompGroupMap = new HashMap<>();
            for (EnrCompetition competition : competitionList) {
                processCompetition(fisCompGroupMap, competition);
            }

            // нужно отфильтровать направления (передаем только те, по которым есть примем)
            {
                for (final Map.Entry<EnrProgramSetBase, AdmissionVolume.Item> entry: xmlAdmVolumeMap.entrySet()) {
                    if (!predicate.evaluate(entry.getValue())) {
                        fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-item-plan", entry.getKey().getTitle()));
                    }
                }
                if (xmlAdmVolumeItemMap.isEmpty()) {
                    fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-items"));
                }
            }

            for (PackageData.AdmissionInfo.AdmissionVolume.Item item : xmlAdmissionInfo.getAdmissionVolume().getItem()) {
                if (item.getNumberBudgetO() != null ||
                        item.getNumberBudgetOZ() != null ||
                        item.getNumberBudgetZ() != null ||
                        item.getNumberQuotaO() != null ||
                        item.getNumberQuotaOZ() != null ||
                        item.getNumberQuotaZ() != null ||
                        item.getNumberTargetO() != null ||
                        item.getNumberTargetOZ() != null ||
                        item.getNumberTargetZ() != null)
                {
                    if(xmlAdmissionInfo.getDistributedAdmissionVolume() == null)
                    {
                        xmlAdmissionInfo.setDistributedAdmissionVolume(new AdmissionInfo.DistributedAdmissionVolume());
                    }

                    AdmissionInfo.DistributedAdmissionVolume.Item dItem = new AdmissionInfo.DistributedAdmissionVolume.Item();
                    dItem.setAdmissionVolumeUID(item.getUID());
                    dItem.setLevelBudget(EnrFisConvertersSimple.getLevelBudgetFinancing(enrollmentCampaign.getSettings().getLevelBudgetFinancing()));
                    dItem.setNumberBudgetO(item.getNumberBudgetO());
                    dItem.setNumberBudgetOZ(item.getNumberBudgetOZ());
                    dItem.setNumberBudgetZ(item.getNumberBudgetZ());
                    dItem.setNumberQuotaO(item.getNumberQuotaO());
                    dItem.setNumberQuotaOZ(item.getNumberQuotaOZ());
                    dItem.setNumberQuotaZ(item.getNumberQuotaZ());
                    dItem.setNumberTargetO(item.getNumberTargetO());
                    dItem.setNumberTargetOZ(item.getNumberTargetOZ());
                    dItem.setNumberTargetZ(item.getNumberTargetZ());

                    xmlAdmissionInfo.getDistributedAdmissionVolume().getItem().add(dItem);
                }
            }

            PackageData.InstitutionAchievements xmlAchievements = null;
            List<EnrEntrantAchievementType> achievementTypes = getList(EnrEntrantAchievementType.class, EnrEntrantAchievementType.enrollmentCampaign(), enrollmentCampaign, EnrEntrantAchievementType.achievementKind().title().s());
            if (!achievementTypes.isEmpty()) {
                IEnrFisConverter<EnrEntrantAchievementType> converter = EnrFisConverterManager.instance().entrantAchievementType().getConverter();

                for (EnrEntrantAchievementType achievementType : achievementTypes) {
                    PackageData.InstitutionAchievements.InstitutionAchievement xmlAchievement = new PackageData.InstitutionAchievements.InstitutionAchievement();


                    xmlAchievement.setInstitutionAchievementUID(uid(achievementType));
                    xmlAchievement.setName(achievementType.getAchievementKind().getTitle());
                    xmlAchievement.setMaxValue(achievementType.getAchievementMarkAsBD());
                    xmlAchievement.setCampaignUID(uid(enrollmentCampaign));

                    EnrFisCatalogItem fisAchievementType = converter.getCatalogItem(achievementType, false);
                    if (null != fisAchievementType) {
                        if(xmlAchievements == null)
                        {
                            xmlAchievements = new PackageData.InstitutionAchievements();
                        }

                        xmlAchievement.setIdCategory(fisAchievementType.getFisID());
                        xmlAchievements.getInstitutionAchievement().add(xmlAchievement);
                    } else {
                        fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-ach-type", achievementType.getAchievementKind().getTitle()));
                    }
                }
            }

            PackageData.TargetOrganizations targetOrganizations = null;

            List<EnrCampaignTargetAdmissionKind> targetAdmissionKinds = getList(EnrCampaignTargetAdmissionKind.class, EnrCampaignTargetAdmissionKind.enrollmentCampaign(), enrollmentCampaign);
            if (!targetAdmissionKinds.isEmpty())
            {
                targetOrganizations = new PackageData.TargetOrganizations();
                if (enrollmentCampaign.getSettings().isTargetAdmissionCompetition())
                {
                    for (EnrCampaignTargetAdmissionKind item : targetAdmissionKinds)
                    {
                        PackageData.TargetOrganizations.TargetOrganization targetOrg = new PackageData.TargetOrganizations.TargetOrganization();
                        targetOrg.setUID(uid(topOrgUnit, item.getTargetAdmissionKind()));
                        targetOrg.setName(item.getTargetAdmissionKind().getTitle());

                        targetOrganizations.getTargetOrganization().add(targetOrg);
                    }
                }
                else
                {
                    PackageData.TargetOrganizations.TargetOrganization targetOrg = new PackageData.TargetOrganizations.TargetOrganization();
                    targetOrg.setUID(uid(topOrgUnit, null));
                    targetOrg.setName("Целевой прием");

                    targetOrganizations.getTargetOrganization().add(targetOrg);
                }
            }

            // состояние
            status(fisSession, PREFIX, 100);

            // заполняем root
            final Root root = PkgDao4AdmissionInfo.this.newRoot(enrOrgUnit);
            root.getPackageData().setAdmissionInfo(xmlAdmissionInfo);

            if (null != xmlAchievements) {
                root.getPackageData().setInstitutionAchievements(xmlAchievements);
            }
            if (null != targetOrganizations)
                root.getPackageData().setTargetOrganizations(targetOrganizations);

            return root;
        }

        protected void processCompetition(Map<String, CompetitiveGroup> fisCompGroupMap, EnrCompetition competition)
        {
            EnrProgramSetBase ps = competition.getProgramSetOrgUnit().getProgramSet();

            // форма
            final String eduFormPostfix = eduFormDao.getEduFormPostfix(ps.getProgramForm());
            if (null == eduFormPostfix) {
                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-eduform", ps.getTitle()));
                return;
            }

            // направление
            EnrFisCatalogItem fisDirection = programSetConverter.getCatalogItem(ps, false);
            if (null == fisDirection) {
                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-direction", ps.getTitle()));
                return;
            }
            Long fisDirectionId = fisDirection.getFisID();

            // уровень
            EnrFisCatalogItem fisEduLvl = eduOrgUnitDao.getEnrFisItem4EducationLevel(ps);
            if (null == fisEduLvl) {
                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-level", ps.getTitle()));
                return;
            }
            Long fisEduLevelId = fisEduLvl.getFisID();

            // источник финансирования
            EnrFisCatalogItem fisSource = finSourceDao.getSource(competition);
            if (null == fisSource) {
                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-source", ps.getTitle()));
                return;
            }
            Long fisSourceId = fisSource.getFisID();

            // форма обучения
            final EnrFisCatalogItem fisEduForm = eduFormDao.getConverter().getCatalogItem(ps.getProgramForm(), false);
            if (null == fisEduForm) {
                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-fis-eduform", ps.getTitle()));
                return;
            }
            Long fisEduFormId = fisEduForm.getFisID();

            String planPrefix = getPlanPrefix(competition.getType().getCode(), competition.getId());
            if (null == planPrefix) {
                return;
            }
            String planPropertyName = planPrefix + eduFormPostfix;

            final String uid = competitionGroupUid(competition);

            CompetitiveGroup compGroupNode = fisCompGroupMap.get(uid);
            boolean cgExists = compGroupNode != null;

            if (compGroupNode == null) {
                compGroupNode = new CompetitiveGroup();
                compGroupNode.setDirectionID(fisDirectionId);
                compGroupNode.setEducationLevelID(fisEduLevelId);
                compGroupNode.setEducationSourceID(fisSourceId);
                compGroupNode.setEducationFormID(fisEduFormId);
                compGroupNode.setIsForKrym(ps.isAcceptPeopleResidingInCrimea());

                final Set<CompetitiveGroup.EduPrograms.EduProgram> eduProgramSet = Sets.newHashSet();
                Set<EduProgramProf> eduProgramProfSet = eduProgramProfMap.get(ps);

                if (ps instanceof EnrProgramSetSecondary)
                    eduProgramProfSet.add(((EnrProgramSetSecondary) ps).getProgram());

                if (org.apache.commons.collections.CollectionUtils.isNotEmpty(eduProgramProfSet))
                {
                    for (EduProgramProf eduProgramProf : eduProgramProfSet)
                    {
                        CompetitiveGroup.EduPrograms.EduProgram eduProgram = new CompetitiveGroup.EduPrograms.EduProgram();
                        String eduProgramUid = uid(competition, eduProgramProf);
                        String eduProgramName = eduProgramProf.getShortTitleWithCodeAndConditionsShort() + (fisSession.isEduProgramUniqueTitleForming() ? " [" + eduProgramUid + "]" : "");

                        eduProgram.setUID(eduProgramUid);
                        eduProgram.setName(eduProgramName);
                        eduProgramSet.add(eduProgram);
                    }
                    compGroupNode.setEduPrograms(new CompetitiveGroup.EduPrograms());
                    compGroupNode.getEduPrograms().getEduProgram().addAll(eduProgramSet);
                }

                fisCompGroupMap.put(uid, compGroupNode);
            }

            // планы приема

            int compPlan = Math.max(0, competition.getPlan());
            boolean useBigint = false; // EnrCompetitionTypeCodes.EXCLUSIVE.equals(competition.getType().getCode());

            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competition.getType().getCode())) {
                if (!taKindsAllowed) {
                    compGroupNode.setTargetOrganizations(new CompetitiveGroup.TargetOrganizations());
                    compGroupNode.getTargetOrganizations().getTargetOrganization().add(getTargetOrganization(null, compPlan, eduFormPostfix));
                } else {
                    List<EnrTargetAdmissionCompetitionPlan> taPlans = taPlanMap.get(competition);
                    if (null == taPlans || taPlans.isEmpty()) {
                        fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-ta-plans", competition.getTitle()));
                        return;
                    }
                    compPlan = 0; // берем план из видов ЦП
                    CompetitiveGroup.TargetOrganizations targetOrganizations = new CompetitiveGroup.TargetOrganizations();
                    for (EnrTargetAdmissionCompetitionPlan taPlan : taPlans) {
                        if (taPlan.getPlan() <= 0) continue;
                        EnrTargetAdmissionKind targetAdmissionKind = taPlan.getTargetAdmissionPlan().getEnrCampaignTAKind().getTargetAdmissionKind();
                        targetOrganizations.getTargetOrganization().add(getTargetOrganization(targetAdmissionKind, taPlan.getPlan(),eduFormPostfix));
                        compPlan += taPlan.getPlan();
                    }
                    if (!targetOrganizations.getTargetOrganization().isEmpty()) {
                        compGroupNode.setTargetOrganizations(targetOrganizations);
                    }
                }
            } else {
                CompetitiveGroup.CompetitiveGroupItem compGroupItemNode = compGroupNode.getCompetitiveGroupItem();
                if (null == compGroupItemNode)
                    compGroupNode.setCompetitiveGroupItem(compGroupItemNode = new CompetitiveGroup.CompetitiveGroupItem());

                this.add(compGroupItemNode, planPropertyName, compPlan, useBigint);
            }

            this.add(xmlAdmVolumeMap.get(ps), planPropertyName, compPlan, useBigint);

            if (cgExists) {
                return;
            }

            // данные КГ

            String compGroupTitle = competitionGroupTitle(competition) + " ["+uid+"]";

            xmlAdmissionInfo.getCompetitiveGroups().getCompetitiveGroup().add(compGroupNode);

            compGroupNode.setUID(uid);
            compGroupNode.setCampaignUID(uid(enrollmentCampaign));
            compGroupNode.setName(compGroupTitle);

            // вступительные испытания

            fillEntranceTestItems(compGroupNode, competition, compGroupTitle);

            // льготы по олимпиаде

            fillOlympicDiplomTypes(ps, competition, compGroupNode);
        }

        private String getPlanPrefix(String compTypeCode, Long competitionId)
        {
            switch (compTypeCode) {
                case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL: {
                    if (competitionsWithEnrolled.contains(competitionId)) {
                        return BUDGET_PREFIX;
                    }
                    else {
                        return null;
                    }
                }
                case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT: {
                    if (competitionsWithEnrolled.contains(competitionId)) {
                        return PAID_PREFIX;
                    }
                    else {
                        return null;
                    }
                }
                case EnrCompetitionTypeCodes.EXCLUSIVE: return QUOTA_PREFIX;
                case EnrCompetitionTypeCodes.MINISTERIAL: return BUDGET_PREFIX;
                case EnrCompetitionTypeCodes.CONTRACT: return PAID_PREFIX;
                case EnrCompetitionTypeCodes.TARGET_ADMISSION: return TARGET_PREFIX;
            }
            return null;
        }

        private void fillOlympicDiplomTypes(EnrProgramSetBase ps, EnrCompetition competition, CompetitiveGroup compGroupNode)
        {
            List<EnrFisSettingsStateExamMinMarkNew> minStateExamMarks = new DQLSelectBuilder()
                .fromEntity(EnrFisSettingsStateExamMinMarkNew.class, "m")
                .where(eq(property("m", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase()), value(ps)))
                .where(eq(property("m", EnrFisSettingsStateExamMinMarkNew.benefitType().code()), value(EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS)))
                .createStatement(getSession()).list();

            if (minStateExamMarks.isEmpty()) return;


            compGroupNode.setCommonBenefit(new CompetitiveGroup.CommonBenefit());

            for(EnrFisSettingsStateExamMinMarkNew mark : minStateExamMarks)
            {
                CompetitiveGroup.CommonBenefit.CommonBenefitItem commonBenefitItem = new CompetitiveGroup.CommonBenefit.CommonBenefitItem();
                compGroupNode.getCommonBenefit().getCommonBenefitItem().add(commonBenefitItem);
                commonBenefitItem.setUID(uid(competition, mark));
                commonBenefitItem.setOlympicDiplomTypes(new CompetitiveGroup.CommonBenefit.CommonBenefitItem.OlympicDiplomTypes());
                commonBenefitItem.getOlympicDiplomTypes().getOlympicDiplomTypeID().addAll(Arrays.asList(1L, 2L));
                commonBenefitItem.setBenefitKindID(1);
                commonBenefitItem.setIsForAllOlympics(true);
                commonBenefitItem.setLevelForAllOlympics(255L);
                commonBenefitItem.setProfileForAllOlympics(new CompetitiveGroup.CommonBenefit.CommonBenefitItem.ProfileForAllOlympics());
                commonBenefitItem.getProfileForAllOlympics().getProfileID().addAll(Collections.singletonList(255L));
                commonBenefitItem.setClassForAllOlympics(255L);

                CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks minEgeMarks = new CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks();
                commonBenefitItem.setMinEgeMarks(minEgeMarks);
                CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks.MinMarks xmlMinMark = new CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks.MinMarks();
                minEgeMarks.setMinMarks(xmlMinMark);
                xmlMinMark.setMinMark(mark.getMinMark());
                xmlMinMark.setSubjectID(mark.getSubject().getFisID());
            }
        }

        private TargetOrganization getTargetOrganization(EnrTargetAdmissionKind targetAdmissionKind, int plan, String eduFormPostfix)
        {
            final TargetOrganization targetOrganization = new TargetOrganization();

            targetOrganization.setUID(uid(topOrgUnit, targetAdmissionKind));

            targetOrganization.setCompetitiveGroupTargetItem(new TargetOrganization.CompetitiveGroupTargetItem());
            final TargetOrganization.CompetitiveGroupTargetItem toItem = targetOrganization.getCompetitiveGroupTargetItem();
            this.add(toItem, TARGET_PREFIX + eduFormPostfix, plan);

            return targetOrganization;
        }

        private void fillEntranceTestItems(CompetitiveGroup competitionXmlGroup, EnrCompetition competition, String compGroupTitle)
        {
            // EntranceTestItems / EntranceTestItem

            IEnrExamSetDao.IExamSetSettings examSetContent = examSetContentMap.get(competition.getExamSetVariant().getId());
            if (competition.getExamSetVariant().getExamSet().isEmptySet()){
                fisSession.warn(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.empty-examSet", compGroupTitle));
                return;
            }

            competitionXmlGroup.setEntranceTestItems(new CompetitiveGroup.EntranceTestItems());
            for (final IEnrExamSetDao.IExamSetElementSettings exam : examSetContent.getElementList())
            {
                final CompetitiveGroup.EntranceTestItems.EntranceTestItem entranceTestItem = new CompetitiveGroup.EntranceTestItems.EntranceTestItem();
                competitionXmlGroup.getEntranceTestItems().getEntranceTestItem().add(entranceTestItem);

                if (exam.getCgExam() == null) {
                    fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.exam-set-not-setup", competition.getTitle()));
                    continue;
                }

                String uid = uid(exam.getCgExam(), competition);
                entranceTestItem.setUID(uid);

                final EnrFisCatalogItem fisExamType = examTypeConverter.getCatalogItem(exam.getElement().getType(), false);
                if (fisExamType == null)
                {
                    entranceTestItem.setEntranceTestTypeID(0);
                    fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.admission.no-examType", exam.getElement().getTitle(), competition.getTitle()));
                }
                else
                {
                    entranceTestItem.setEntranceTestTypeID(fisExamType.getFisID());
                }

                entranceTestItem.setMinScore(new BigDecimal(exam.getCgExam().getPassMarkAsDouble()));
                entranceTestItem.setEntranceTestPriority(exam.getPriority());

                // предмет

                entranceTestItem.setEntranceTestSubject(new TEntranceTestSubject());
                final EnrFisCatalogItem fisExamElement = examSetElementConverter.getCatalogItem(exam.getCgExam().getExamSetElement().getValue(), false);
                if (fisExamElement != null)
                {
                    entranceTestItem.getEntranceTestSubject().setSubjectID(fisExamElement.getFisID());

                    // льгота - зачтение за 100 баллов
                    EnrFisSettingsStateExamMinMarkNew minStateExamMark = new DQLSelectBuilder()
                            .fromEntity(EnrFisSettingsStateExamMinMarkNew.class, "m")
                            .where(eq(property("m", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase()), value(competition.getProgramSetOrgUnit().getProgramSet())))
                            .where(eq(property("m", EnrFisSettingsStateExamMinMarkNew.benefitType().code()), value(EnrBenefitTypeCodes.MAX_EXAM_RESULT)))
                            .where(eq(property("m", EnrFisSettingsStateExamMinMarkNew.subject()), value(fisExamElement)))
                            .createStatement(getSession()).uniqueResult();
                    if (minStateExamMark != null)
                    {
                        entranceTestItem.setEntranceTestBenefits(new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits());
                        CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem entranceTestBenefitItem = new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem();
                        entranceTestItem.getEntranceTestBenefits().getEntranceTestBenefitItem().add(entranceTestBenefitItem);
                        entranceTestBenefitItem.setUID(uid);
                        entranceTestBenefitItem.setOlympicDiplomTypes(new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem.OlympicDiplomTypes());
                        entranceTestBenefitItem.getOlympicDiplomTypes().getOlympicDiplomTypeID().addAll(Arrays.asList(1L, 2L));
                        entranceTestBenefitItem.setIsForAllOlympics(true);
                        entranceTestBenefitItem.setLevelForAllOlympics(255L);
                        entranceTestBenefitItem.setProfileForAllOlympics(new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem.ProfileForAllOlympics());
                        entranceTestBenefitItem.getProfileForAllOlympics().getProfileID().addAll(Collections.singletonList(255L));
                        entranceTestBenefitItem.setClassForAllOlympics(255L);
                        entranceTestBenefitItem.setBenefitKindID(3);
                        entranceTestBenefitItem.setMinEgeMark((long) minStateExamMark.getMinMark());
                    }
                }
                else
                {
                    entranceTestItem.getEntranceTestSubject().setSubjectName(exam.getCgExam().getExamSetElement().getValue().getTitle());
                }


                if (exam.getCgExam().getExamSetElement().getValue() instanceof EnrCampaignDiscipline) {

                }
            }

        }

        private Map<EnrCompetition, List<EnrTargetAdmissionCompetitionPlan>> getTaPlanMap(List<EnrProgramSetOrgUnit> psOuList)
        {
            Map<EnrCompetition, List<EnrTargetAdmissionCompetitionPlan>> map = SafeMap.get(ArrayList.class);
            for (EnrTargetAdmissionCompetitionPlan plan : getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition().programSetOrgUnit(), psOuList)) {
                map.get(plan.getCompetition()).add(plan);
            }
            return map;
        }

        private Map<EnrProgramSetBase, Set<EduProgramProf>> getEduProgramProfMap()
        {
            Map<EnrProgramSetBase, Set<EduProgramProf>> map = SafeMap.get(HashSet.class);
            getList(EnrProgramSetItem.class).forEach(item -> map.get(item.getProgramSet()).add(item.getProgram()));
            return map;
        }

        private List<EnrProgramSetOrgUnit> getProgramSetOrgUnitList(List<EnrCompetition> competitionList)
        {
            return new ArrayList<>(new HashSet<>(CollectionUtils.collect(competitionList, EnrCompetition::getProgramSetOrgUnit)));
        }

        private Map<Long, IEnrExamSetDao.IExamSetSettings> getExamSetContent(List<EnrCompetition> competitionList)
        {
            return EnrExamSetManager.instance().dao().getExamSetContent(CollectionUtils.collect(competitionList, competition -> competition.getExamSetVariant().getId()));
        }

        private void add(final Object object, final String property, int i) {
            add(object, property, i, false);
        }


        private void add(final Object object, final String property, int i, boolean bigInt) {
            if (bigInt) {
                addBigint(object, property, i);
            } else {
                addLong(object, property, i);
            }
        }

        private void addLong(final Object object, final String property, int i) {
            final Long value = (Long) FastBeanUtils.getValue(object, property);
            if (null != value) { i += value.intValue(); }
            FastBeanUtils.setValue(object, property, (long) i);
        }

        private void addBigint(final Object object, final String property, int i) {
            final BigInteger value = (BigInteger) FastBeanUtils.getValue(object, property);
            if (null != value) { i += value.intValue(); }
            FastBeanUtils.setValue(object, property, BigInteger.valueOf(i));
        }


    }

    private static boolean check(final Long v) { return ((null != v) && (v > 0)); }

    final static Predicate<AdmissionVolume.Item> predicate =
            object -> check(object.getNumberBudgetO())
                    || check(object.getNumberBudgetOZ())
                    || check(object.getNumberBudgetZ())
                    || check(object.getNumberPaidO())
                    || check(object.getNumberPaidOZ())
                    || check(object.getNumberPaidZ())
                    || check(object.getNumberTargetO())
                    || check(object.getNumberTargetOZ())
                    || check(object.getNumberTargetZ());
}
