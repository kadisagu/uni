package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p3;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4ApplicationsInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

/**
 * @author vdanilov
 */
@DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм формирования пакета в рамках сессии")
public interface IPkgDao4ApplicationsInfo extends INeedPersistenceSupport {

    /**
     * Создает пакет (пакет 3: Заявления абитуриентов)
     * @param fisSession
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    List<EnrFisSyncPackage4ApplicationsInfo> doCreatePackages(EnrFisSyncSession fisSession);

}
