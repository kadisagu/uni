package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEnrFisConverterDao<T extends IEntity> extends INeedPersistenceSupport {

    /** @return converter.catalogCode: код справочника ФИС (из которого будут выбираться значения для сопоставления) */
    String getCatalogCode();

    /** @return IEcfConverter, который хранит в себе данные сопоставления */
    IEnrFisConverter<T> getConverter();

    /** сохраняет сопоставление (полностью) в базу */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void update(Map<T, EnrFisCatalogItem> values, boolean clearNulls);

    /** сохраняет сопоставление (только указанный ключ) в базу */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void update(T key, EnrFisCatalogItem value);


    /**
     * Проверяет на использование элементов справочника в пользовательских конвертерах.
     * Если элемент справочника используется и он изменился или удален, то падает ошибка.
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void validateUsedInUserConverter(Map<Long, String> itemMap);
}
