package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduForm;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterStaticDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
@Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864756")
public class EnrFisConverterDao4EduForm extends EnrFisConverterStaticDao<EduProgramForm, String> implements IEnrFisConverterDao4EduForm {

    /* { uni.code, uni.title, fis.code, fis.title } */
    private static final String[][] DATA = {
        { "1", "Очная",        "11", "Очная форма", "O" },
        { "2", "Заочная",      "10", "Заочная форма", "Z" },
        { "3", "Очно-заочная", "12",  "Очно-заочная (вечерняя)", "OZ" }
    };

    /* { fis.code -> fis.title } */
    private static final Map<String, String> STATIC_TITLE_CHECK_MAP = new HashMap<String, String>();

    /* { uni.code -> fis.code }*/
    private static final Map<String, String> STATIC_ITEM_CODE_MAP = new HashMap<String, String>();

    /* { uni.code -> fis-key }*/
    private static final Map<String, String> STATIC_ITEM_KEY_MAP = new HashMap<String, String>();


    static {
        for (final String[] row: DATA) {
            if (null != row[2]) {
                if (null != STATIC_ITEM_CODE_MAP.put(row[0], row[2])) {
                    throw new IllegalStateException("STATIC_ITEM_CODE_MAP: Duplicate value for key=«"+row[0]+"»");
                }

                if (null != STATIC_ITEM_KEY_MAP.put(row[0], row[4])) {
                    throw new IllegalStateException("STATIC_ITEM_KEY_MAP: Duplicate value for key=«"+row[0]+"»");
                }

                final String prev = STATIC_TITLE_CHECK_MAP.put(row[2], row[3]);
                if (null != prev && !prev.equals(row[3])) {
                    throw new IllegalStateException("COUNTRY_CHECK_MAP: Duplicate value for key=«"+row[2]+"»");
                }
            }
        }
    }


    @Override
    protected Map<String, String> getStaticTitleCheckMap() {
        return STATIC_TITLE_CHECK_MAP;
    }

    @Override
    protected Map<String, String> getStaticItemCodeMap() {
        return STATIC_ITEM_CODE_MAP;
    }


    @Override public String getCatalogCode() {
        return "14";
    }

    @Override protected List<EduProgramForm> getEntityList() {
        return getList(EduProgramForm.class, EduProgramForm.P_CODE);
    }

    @Override
    protected String getItemMapKey(EduProgramForm entity) {
        return entity.getCode();
    }

    @Override
    public String getEduFormPostfix(EduProgramForm developForm) {
        return STATIC_ITEM_KEY_MAP.get(developForm.getCode());
    }

}
