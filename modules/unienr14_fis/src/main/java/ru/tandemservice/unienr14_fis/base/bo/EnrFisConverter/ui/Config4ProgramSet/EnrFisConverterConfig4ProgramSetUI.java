/**
 *$Id: EnrFisConverterConfig4ProgramSetUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4ProgramSet;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author Alexander Shaburov
 * @since 12.07.13
 */
public class EnrFisConverterConfig4ProgramSetUI extends EnrFisConverterConfigUI
{

    private EnrEnrollmentCampaign _enrCampaign;

    public boolean _withoutFisCatalogs;

    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }

    public void onClickAutoSync() {
        EnrFisConverterManager.instance().programSet().doAutoSync();
    }

    public boolean isEmptySelectors() { return _enrCampaign == null; }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
    }


    public void onRefreshSelectors()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisConverterConfig4ProgramSet.BIND_ENROLLMENT_CAMPAIGN, _enrCampaign);
        dataSource.put(EnrFisConverterConfig4ProgramSet.BIND_STRUCTURE_EDU_LVL, getSettings().get("structureEduLvl"));
        dataSource.put(EnrFisConverterConfig4ProgramSet.BIND_ITEM_TITLE, getSettings().get("itemTitle"));
    }

    public EnrEnrollmentCampaign getEnrCampaign() {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign) {
        _enrCampaign = enrCampaign;
    }
}
