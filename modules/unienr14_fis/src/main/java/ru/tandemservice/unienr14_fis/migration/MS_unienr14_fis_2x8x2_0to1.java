package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_unienr14_fis_2x8x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncSession

		// создано обязательное свойство skipEnrCampaignInfo
        if (!tool.columnExists("enr14fis_session_t", "skipenrcampaigninfo_p"))
		{
			// создать колонку
			tool.createColumn("enr14fis_session_t", new DBColumn("skipenrcampaigninfo_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14fis_session_t set skipenrcampaigninfo_p=? where skipenrcampaigninfo_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14fis_session_t", "skipenrcampaigninfo_p", false);

		}


    }
}