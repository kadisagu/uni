/**
 *$Id: IEnrFisConverterDao4Olympiad.java 32223 2014-02-03 10:55:07Z vdanilov $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiad;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
public interface IEnrFisConverterDao4Olympiad extends IEnrFisConverterDao<EnrOlympiad>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync(EducationYear eduYear);
}
