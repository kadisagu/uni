/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympProfileDiscipline;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4OlympiadProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConverterBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 16.08.2016
 */
public class EnrFisConverterDao4OlympProfileDiscipline extends EnrFisConverterBaseDao<EnrOlympiadProfileDiscipline, Long> implements IEnrFisConverterDao4OlympProfileDiscipline
{

    @Override
    public String getCatalogCode()
    {
        return "1";
    }

    @Override
    protected List<EnrOlympiadProfileDiscipline> getEntityList()
    {
        return new DQLSelectBuilder().fromEntity(EnrOlympiadProfileDiscipline.class, "x").column(property("x"))
                .order(property(EnrOlympiadProfileDiscipline.title().fromAlias("x")))
                .createStatement(getSession()).list();
    }

    @Override
    protected Long getItemMapKey(EnrOlympiadProfileDiscipline entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EnrFisConverterBaseDao.map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EnrFisConv4OlympiadProfileDiscipline.class, "x")
                        .column(property(EnrFisConv4OlympiadProfileDiscipline.olympiadProfileDiscipline().id().fromAlias("x")), "dis_id")
                        .column(property(EnrFisConv4OlympiadProfileDiscipline.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())));
    }

    @Override
    public void update(Map<EnrOlympiadProfileDiscipline, EnrFisCatalogItem> values, boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EnrFisConv4OlympiadProfileDiscipline> targetRecords = new ArrayList<>(values.size());
        for (final Map.Entry<EnrOlympiadProfileDiscipline, EnrFisCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EnrFisConv4OlympiadProfileDiscipline(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4OlympiadProfileDiscipline.NaturalId, EnrFisConv4OlympiadProfileDiscipline>() {
            @Override protected EnrFisConv4OlympiadProfileDiscipline.NaturalId key(final EnrFisConv4OlympiadProfileDiscipline source) {
                return (EnrFisConv4OlympiadProfileDiscipline.NaturalId)source.getNaturalId();
            }
            @Override protected EnrFisConv4OlympiadProfileDiscipline buildRow(final EnrFisConv4OlympiadProfileDiscipline source) {
                return new EnrFisConv4OlympiadProfileDiscipline(source.getOlympiadProfileDiscipline(), source.getValue());
            }
            @Override protected void fill(final EnrFisConv4OlympiadProfileDiscipline target, final EnrFisConv4OlympiadProfileDiscipline source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EnrFisConv4OlympiadProfileDiscipline databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EnrFisConv4OlympiadProfileDiscipline.class), targetRecords);
    }

    @Override
    public void doAutoSync()
    {
        // поднимаем профильные дисциплины олимпиад, для которых нет сопоставления с Элементом справочника ФИС
        final DQLSelectBuilder subjectDQL = new DQLSelectBuilder().fromEntity(EnrOlympiadProfileDiscipline.class, "o").column(property("o"))
                .where(notIn(property(EnrOlympiadProfileDiscipline.id().fromAlias("o")),
                             new DQLSelectBuilder().fromEntity(EnrFisConv4OlympiadProfileDiscipline.class, "c").column(property(EnrFisConv4OlympiadProfileDiscipline.olympiadProfileDiscipline().id().fromAlias("c")))
                                     .buildQuery()));

        final DQLSelectBuilder fisCatalogDQL = new DQLSelectBuilder().fromEntity(EnrFisCatalogItem.class, "f").column(property("f"))
                .where(isNull(property("f", EnrFisCatalogItem.removalDate())))
                .where(eq(property(EnrFisCatalogItem.fisCatalogCode().fromAlias("f")), value(getCatalogCode())));

        final Map<String, EnrFisCatalogItem> fisCatalogItemTitleMap = new HashMap<>();
        for (EnrFisCatalogItem item : fisCatalogDQL.createStatement(getSession()).<EnrFisCatalogItem>list())
        {
            fisCatalogItemTitleMap.put(item.getTitle().trim().toLowerCase(), item);
        }

        final Map<EnrOlympiadProfileDiscipline, EnrFisCatalogItem> values = new HashMap<>();
        for (EnrOlympiadProfileDiscipline subject : subjectDQL.createStatement(getSession()).<EnrOlympiadProfileDiscipline>list())
        {
            EnrFisCatalogItem item = fisCatalogItemTitleMap.get(subject.getTitle().toLowerCase());
            if (null == item) { continue; }

            values.put(subject, item);
        }

        // для тех для которых уже выбрали - не меняем
        for (EnrFisConv4OlympiadProfileDiscipline discipline : getList(EnrFisConv4OlympiadProfileDiscipline.class))
            values.put(discipline.getOlympiadProfileDiscipline(), discipline.getValue());

        this.update(values, false);
    }

    @Override
    protected List<EnrFisCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EnrFisConv4OlympiadProfileDiscipline.class, "b").column(property(EnrFisConv4OlympiadProfileDiscipline.value().fromAlias("b")))
                .where(isNotNull(property(EnrFisConv4OlympiadProfileDiscipline.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }
}