package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.4")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrFisSyncSession

        // создано обязательное свойство skippedRecItemCount
        {
            // создать колонку
            if (!tool.columnExists("enr14fis_session_t", "skippedrecitemcount_p")) {
                tool.createColumn("enr14fis_session_t", new DBColumn("skippedrecitemcount_p", DBType.INTEGER));
            }

            // задать значение по умолчанию
            java.lang.Integer defaultSkippedRecItemCount = 0;		// TODO: задайте NOT NULL значение!
            tool.executeUpdate("update enr14fis_session_t set skippedrecitemcount_p=? where skippedrecitemcount_p is null", defaultSkippedRecItemCount);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14fis_session_t", "skippedrecitemcount_p", false);

        }


    }
}