/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4OlympiadSubject;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4ExamSetElement.EnrFisConverterConfig4ExamSetElement;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author oleyba
 * @since 7/17/14
 */
public class EnrFisConverterConfig4OlympiadSubjectUI extends EnrFisConverterConfigUI
{
    private EnrEnrollmentCampaign _enrCampaign;

    public boolean _withoutFisCatalogs;

    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
    }

    public void onClickAutoSync()
    {
        EnrFisConverterManager.instance().olympiadSubject().doAutoSync();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisConverterConfig4OlympiadSubject.BIND_ENROLLMENT_CAMPAIGN, _enrCampaign);
    }

    public void onRefreshSelectors()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    // Getters & Setters

    public boolean isEmptySelectors()
    {
        return _enrCampaign == null;
    }

    // Accessors

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }
}
