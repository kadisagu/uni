/**
 *$Id: UniecFisOrgstructPermissionModifier.java 25938 2013-01-30 12:31:11Z vdanilov $
 */
package ru.tandemservice.unienr14_fis.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 21.01.13
 */
public class EnrFisOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unienr14_fis");
        config.setName("unienr14-fis-sec-orgUnit");
        config.setTitle("");

        ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();
            ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            PermissionGroupMeta pgEcfTab = PermissionMetaUtil.createPermissionGroup(config, code + "Enr14FisTabPermissionGroup", "Вкладка «ФИС»");
            PermissionMetaUtil.createGroupRelation(config, pgEcfTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgEcfTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgEcfTab, "orgUnit_viewEnr14FisTab_" + code, "Просмотр");

            PermissionGroupMeta pgEcfPkgTab = PermissionMetaUtil.createPermissionGroup(pgEcfTab, code + "Enr14FisSessionTabPermissionGroup", "Вкладка «Сессии»");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_viewEnr14FisSyncTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_addEnr14FisSync_" + code, "Добавление сессии ФИС");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_getEnr14FisAppDeletePackage_" + code, "Генерация пакета на удаление всех заявлений");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_getEnr14FisOrderDeletePackage_" + code, "Генерация пакета на удаление всех заявлений из приказов");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_getEnr14FisRecommendedPackage_" + code, "Генерация пакета для передачи рекомендованных");

            PermissionGroupMeta pgEcfEntrantTab = PermissionMetaUtil.createPermissionGroup(pgEcfTab, code + "Enr14FisEntrantSyncStatusTabPermissionGroup", "Вкладка «Статус абитуриентов»");
            PermissionMetaUtil.createPermission(pgEcfEntrantTab, "orgUnit_viewEnr14FisEntrantSyncStatusTab_" + code, "Просмотр");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
