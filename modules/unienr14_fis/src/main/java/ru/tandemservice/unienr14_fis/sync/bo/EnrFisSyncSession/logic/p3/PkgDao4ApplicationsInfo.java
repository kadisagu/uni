package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p3;

import com.beust.jcommander.internal.Maps;
import enr14fis.schema.pkg.send.req.*;
import enr14fis.schema.pkg.send.req.PackageData.Applications.Application;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.DebugBlock;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ValueHolder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.codes.AddressLevelCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.rating.entity.*;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.gen.EnrFisCatalogItemGen;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisEntrantSyncData;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4ApplicationsInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.PkgDaoBase;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public class PkgDao4ApplicationsInfo extends PkgDaoBase implements IPkgDao4ApplicationsInfo
{

    private static final int PROGRESS_GEN = 90;
    private static final int PROGRESS_SAVE = 100-PROGRESS_GEN;

    @Override
    public List<EnrFisSyncPackage4ApplicationsInfo> doCreatePackages(final EnrFisSyncSession fisSession)
    {
        if (fisSession.isSkipApplicationsInfo()) {
            return Collections.emptyList();
        }

        final Map<Long, List<Application>> entrant2appListMap = new Context(fisSession).getApplicationMap();

        final String PREFIX = EnrFisSyncSessionManager.instance().getProperty("title.applications");

        // разбивем абитуриентов по пакетам (по количеству)
        final MutableInt i = new MutableInt();
        final List<EnrFisSyncPackage4ApplicationsInfo> result = new ArrayList<>();
        BatchUtils.execute(entrant2appListMap.keySet(), 512, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(final Collection<Long> entrantIds)
            {

                // формируем пакет
                final Root root = newRoot(fisSession.getEnrOrgUnit());
                root.getPackageData().setApplications(new PackageData.Applications());
                for (final Long entrantId : entrantIds) {

                    // состояние
                    i.increment();
                    status(fisSession, PREFIX, PROGRESS_GEN + (PROGRESS_SAVE * i.intValue() / entrant2appListMap.size()));

                    // добавляем пакет
                    root.getPackageData().getApplications().getApplication().addAll(entrant2appListMap.get(entrantId));
                }

                // создаем пакет в базе
                final EnrFisSyncPackage4ApplicationsInfo dbo = new EnrFisSyncPackage4ApplicationsInfo();
                dbo.setCreationDate(new Date()); // должен быть уникальным
                dbo.setSession(fisSession);
                dbo.setXmlPackage(EnrFisServiceImpl.toXml(root));
                dbo.setComment(getComment(entrantIds));
                saveOrUpdate(dbo);
                result.add(dbo);

                // обновляем ссылки на пакет
                final List<EnrFisEntrantSyncData> syncDataList = getList(EnrFisEntrantSyncData.class, EnrFisEntrantSyncData.entrant().id(), entrantIds);
                for (final EnrFisEntrantSyncData syncData : syncDataList) {
                    syncData.setSyncPackage(dbo);
                    syncData.setSyncCompleteDate(null);
                    syncData.setModificationDate(dbo.getCreationDate());
                    saveOrUpdate(syncData);
                }

                flush(fisSession);

                try { Thread.sleep(1); /* нужно, чтобы сделать время создания пакета уникальным */ }
                catch (InterruptedException ignored) {}
            }

            protected String getComment(Collection<Long> entrantIds)
            {
                StringBuilder sb = new StringBuilder();

                Iterator<Long> iterator = entrantIds.iterator();
                sb.append(get(EnrEntrant.class, iterator.next()).getFullFio());

                if (iterator.hasNext()) {
                    sb.append(" - ");
                    while (true) {
                        Long id = iterator.next();
                        if (iterator.hasNext()) { continue; }
                        sb.append(get(EnrEntrant.class, id).getFullFio());
                        return sb.toString();
                    }
                }

                return sb.toString();
            }
        });

        flush(fisSession);
        return result;
    }

    /* контекст генерации, здесь можно размещать всякие вспомогательные данные */
    protected class Context
    {
        private final String UID_ERROR = "error";
        private final String PREFIX = EnrFisSyncSessionManager.instance().getProperty("title.applications");

        private final EnrFisSyncSession fisSession;
        private final Set<EnrCompetition> competitionSet;
        private final Function<Date, XMLGregorianCalendar> dateTimeFunction;

        final Map<Long, List<Application>> result;
        public Map<Long, List<Application>> getApplicationMap()  { return result; }

        final Date now = new Date();
        final Session session = getSession();
        final int entrantListSize;
        final MutableInt counter = new MutableInt();
        final ValueHolder<String> errorFlag = new ValueHolder<>();

        // перечень из данных синхронизации (для отслеживания того, что изменилось)
        final Map<Long, EnrFisEntrantSyncData> syncDataMap = new HashMap<>();

        public Context(final EnrFisSyncSession fisSession) {
            this.fisSession = fisSession;
            this.competitionSet = new HashSet<>(EnrFisSettingsManager.instance().dao().getCompetitions(fisSession.getEnrOrgUnit()));
            this.dateTimeFunction = EnrFisSyncSessionManager.instance().dao().getDateTimeXmlConverter(fisSession.getEnrOrgUnit().getEnrollmentCampaign(), dataTypeFactory);

            // состояние
            status(fisSession, PREFIX, 0);

            // полный перечень абитуриентов
            final List<Long> entrantFullIdList =  new DQLSelectBuilder()
            .fromEntity(EnrEntrant.class, "e").column(property("e.id"))
            .where(eq(property(EnrEntrant.enrollmentCampaign().fromAlias("e")), value(fisSession.getEnrOrgUnit().getEnrollmentCampaign())))
            .order(property(EnrEntrant.person().identityCard().lastName().fromAlias("e")))
            .order(property(EnrEntrant.person().identityCard().firstName().fromAlias("e")))
            .order(property(EnrEntrant.person().identityCard().middleName().fromAlias("e")))
            .order(property(EnrEntrant.id().fromAlias("e")))
            .createStatement(session).list();

            result = new LinkedHashMap<>(fisSession.isForceAllEntrants() ? entrantFullIdList.size() : 16);
            entrantListSize = entrantFullIdList.size();

            // будем грузить данные по чуть-чуть
            BatchUtils.execute(entrantFullIdList, 128, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> entrantIdList) {

                    List<EnrEntrant> entrantList;

                    // грузим абитуриентов в порядке следования идентификаторов
                    DQLSelectBuilder entrantDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrant.class, "e").column("e")
                        .fetchPath(DQLJoinType.inner, EnrEntrant.person().fromAlias("e"), "p", true)
                        .where(in(property("e.id"), entrantIdList));
                    entrantList = sort(entrantDQL.createStatement(session).list(), entrantIdList);
                    fetchEntrantData(entrantList);


                    // перечень из данных синхронизации (для отслеживания того, что изменилось)
                    final List<EnrFisEntrantSyncData> syncDataList = getList(EnrFisEntrantSyncData.class, EnrFisEntrantSyncData.entrant().id(), entrantIdList);
                    for (final EnrFisEntrantSyncData syncData : syncDataList) {
                        if (null != syncDataMap.put(syncData.getEntrant().getId(), syncData)) {
                            throw new IllegalStateException("Field enrFisEntrantSyncData.entrant is not unique.");
                        }
                    }

                    // поехали
                    for (final EnrEntrant entrant : entrantList) {
                        if (entrant.isArchival()) {
                            continue;
                        }

                        // состояние
                        counter.increment();
                        status(fisSession, PREFIX, (PROGRESS_GEN * counter.intValue() / entrantListSize));

                        List<Application> applicationList;

                        try (DebugBlock appListBlock = Debug.block("get app list")) {
                            applicationList = getApplicationList(entrant);
                        }

                        fillSyncData(entrant, applicationList, syncDataMap.get(entrant.getId()));
                    }

                    flush(fisSession);
                }
            });

            // состояние
            status(fisSession, PREFIX, 100);
        }

        private void fillSyncData(EnrEntrant entrant, List<Application> applications, EnrFisEntrantSyncData syncData)
        {
            final Root root = new Root();
            root.setPackageData(new PackageData());
            root.getPackageData().setApplications(new PackageData.Applications());
            root.getPackageData().getApplications().getApplication().addAll(applications);

            // превращаем это в xml
            final byte[] zipXml = EnrFisEntrantSyncData.zip.compress(EnrFisServiceImpl.toXml(root)); // в любом случае сжимать придется
            final int hash = Arrays.hashCode(zipXml); // в данном случае копирования не будет

            if (null == syncData) {
                syncDataMap.put(entrant.getId(), syncData = new EnrFisEntrantSyncData(entrant, uid(entrant)));
            }

            // нечего включать в пакет
            if (applications.isEmpty()) {
                syncData.setZipXmlPackage(zipXml);
                syncData.setZipXmlPackageHash(0);

                // сбрасываем пакет
                syncData.setSyncPackage(null);
                syncData.setSyncCompleteDate(null);
                syncData.setModificationDate(now);
                syncData.setSkipReason("Нет заявлений.");
                syncData.setSuccessfulImport(null);
                session.saveOrUpdate(syncData);

                fisSession.info(PREFIX, EnrFisSyncSessionManager.instance().getProperty("info.applications.entrant.no-apps", entrant.getFullFio(), applications.size()));
                return;
            }


            // пропускаем упырей с ошибками, хватит ломать ФИС
            if (errorFlag.getValue() != null) {

                syncData.setZipXmlPackage(zipXml);
                syncData.setZipXmlPackageHash(hash);
                syncData.setSyncPackage(null);
                syncData.setSyncCompleteDate(null);
                syncData.setModificationDate(now);
                syncData.setSkipReason(errorFlag.getValue());
                syncData.setSuccessfulImport(null);
                session.saveOrUpdate(syncData);

                fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.applications.entrant.skip-error", entrant.getFullFio()));
                fisSession.setSkippedEntrantCount(fisSession.getSkippedEntrantCount() + 1);

                return;
            }

            // если абитуриента надо отправить в ФИС, включаем его в пакет
            if (fisSession.isForceAllEntrants() || (null == syncData.getSyncCompleteDate())  || !syncData.isSuccessfulImportNullSafe() || (hash != syncData.getZipXmlPackageHash()) || (!Arrays.equals(zipXml, syncData.getZipXmlPackage()))) {

                syncData.setZipXmlPackage(zipXml);
                syncData.setZipXmlPackageHash(hash);
                if (null != result.put(entrant.getId(), applications)) {
                    throw new IllegalStateException("Entrant list is not unique.");
                }

                // сбрасываем пакет (он будет прицеплен позже)
                syncData.setSyncPackage(null);
                syncData.setSyncCompleteDate(null);
                syncData.setModificationDate(now);
                syncData.setSkipReason(null);
                syncData.setSuccessfulImport(null);
                session.saveOrUpdate(syncData);

                fisSession.info(PREFIX, EnrFisSyncSessionManager.instance().getProperty("info.applications.entrant.include-xml", entrant.getFullFio(), applications.size()));

            } else {
                fisSession.info(PREFIX, EnrFisSyncSessionManager.instance().getProperty("info.applications.entrant.skip-no-changes", entrant.getFullFio()));
            }

        }

        private final IEnrFisConverter<AddressCountry> addressCountryConverter = EnrFisConverterManager.instance().addressCountry().getConverter();
        private final IEnrFisConverterDao4ProgramSet programSetDao = EnrFisConverterManager.instance().programSet();
        private final IEnrFisConverter<EnrProgramSetBase> programSetConverter = programSetDao.getConverter();
        private final IEnrFisConverter<EnrExamType> examTypeConverter = EnrFisConverterManager.instance().examType().getConverter();
        private final IEnrFisConverter<IEnrExamSetElementValue> examSetElementConverter = EnrFisConverterManager.instance().examSetElement().getConverter();
        private final IEnrFisConverter<EnrOlympiad> olympiadConverter = EnrFisConverterManager.instance().olympiad().getConverter();
        private final IEnrFisConverter<EnrOlympiadSubject> olympiadSubjectConverter = EnrFisConverterManager.instance().olympiadSubject().getConverter();
        private final IEnrFisConverter<AddressItem> regionConverter = EnrFisConverterManager.instance().region().getConverter();
        private final IEnrFisConverter<PersonDocumentCategory> compatriotConverter = EnrFisConverterManager.instance().documentCategoryBelongingNation().getConverter();
        private final IEnrFisConverter<PersonDocumentCategory> sportsConverter = EnrFisConverterManager.instance().documentCategorySportsDiploma().getConverter();
        private final IEnrFisConverter<PersonDocumentCategory> orphanConverter = EnrFisConverterManager.instance().documentCategoryOrphan().getConverter();
        private final IEnrFisConverter<PersonDocumentCategory> veteranConverter = EnrFisConverterManager.instance().documentCategoryCombatVeterans().getConverter();
        private final IEnrFisConverter<EnrStateExamSubject> stateExamConverter = EnrFisConverterManager.instance().stateExamSubject().getConverter();
        private final IEnrFisConverter<EnrOlympiadProfileDiscipline> profileDisciplineConverter = EnrFisConverterManager.instance().profileDiscipline().getConverter();

        private final Map<EnrCompetition, String> admissionVolumeItemUidMap = SafeMap.get(new SafeMap.Callback<EnrCompetition, String>()
        {
            @Override
            public String resolve(final EnrCompetition competition)
            {
                final EnrFisCatalogItem fisEduLvl = programSetDao.getEnrFisItem4EducationLevel(competition.getProgramSetOrgUnit().getProgramSet());
                if (null == fisEduLvl) {
                    return UID_ERROR; // заглушка
                }

                final EnrFisCatalogItem fisDirection = programSetConverter.getCatalogItem(competition.getProgramSetOrgUnit().getProgramSet(), false);
                if (null == fisDirection) {
                    return UID_ERROR; // заглушка
                }

                return admissionVolumeItemUid(fisSession.getEnrOrgUnit().getEnrollmentCampaign(), fisEduLvl, fisDirection, competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject());
            }
        });


        private final Map<Long, List<EnrEntrantAchievement>> achievementMap = SafeMap.get(ArrayList.class);
        private final Map<Long, List<EnrEntrantRequest>> requestMap = SafeMap.get(ArrayList.class);
        private final Map<Long, Set<Long>> originalReceivedMap = SafeMap.get(HashSet.class);
        private final Map<Long, List<EnrRequestedCompetition>> reqCompMap = SafeMap.get(ArrayList.class);
        private final Map<Long, List<EnrEntrantBenefitProof>> benefitProofMap = SafeMap.get(ArrayList.class);
        private final Map<Long, List<EnrChosenEntranceExam>> examResultMap = SafeMap.get(ArrayList.class);
        private final Map<Long, List<EnrEntrantRequestAttachment>> attachmentMap = SafeMap.get(ArrayList.class);
        private final Map<Long, List<IdentityCard>> idcMap = SafeMap.get(ArrayList.class);

        protected void fetchEntrantData(Collection<EnrEntrant> entrants)
        {
            List<Long> entrantIds = new ArrayList<>();
            List<Long> personIds = new ArrayList<>();
            for (EnrEntrant e : entrants) {
                entrantIds.add(e.getId());
                personIds.add(e.getPerson().getId());
            }

            for (EnrEntrantAchievement achievement : getList(EnrEntrantAchievement.class, EnrEntrantAchievement.entrant().id(), entrantIds, EnrEntrantAchievement.id().s())) {
                achievementMap.get(achievement.getEntrant().getId()).add(achievement);
            }
            DQLSelectBuilder requestDql = new DQLSelectBuilder()
                .fromEntity(EnrEntrantRequest.class, "e").column("e")
                .fetchPath(DQLJoinType.inner, EnrEntrantRequest.eduDocument().fromAlias("e"), "d")
                .fetchPath(DQLJoinType.left, EnrEntrantRequest.eduInstDocOriginalRef().fromAlias("e"), "o")
                .where(in(property(EnrEntrantRequest.entrant().id().fromAlias("e")), entrantIds))
                .order(property(EnrEntrantRequest.id().fromAlias("e")))
                ;
            for (EnrEntrantRequest request : requestDql.createStatement(session).<EnrEntrantRequest>list()) {
                requestMap.get(request.getEntrant().getId()).add(request);
            }
            for (Object[] row : new DQLSelectBuilder()
                .fromEntity(EnrEntrantRequestAttachment.class, "a")
                .column(property("a", EnrEntrantRequestAttachment.entrantRequest().id()))
                .column(property("a", EnrEntrantRequestAttachment.document().id()))
                .where(in(property("a", EnrEntrantRequestAttachment.entrantRequest().entrant().id()), entrantIds))
                .order(property("a", EnrEntrantRequestAttachment.entrantRequest().id().s()))
                .createStatement(session).<Object[]>list()) {
                originalReceivedMap.get((Long) row[0]).add((Long) row[1]);
            }
            for (final EnrRequestedCompetition reqComp: getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant().id(), entrantIds, EnrRequestedCompetition.id().s())) {
                reqCompMap.get(reqComp.getRequest().getId()).add(reqComp);
            }
            for (EnrEntrantBenefitProof proof : getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement().entrant().id(), entrantIds, EnrEntrantBenefitProof.id().s())) {
                benefitProofMap.get(proof.getBenefitStatement().getId()).add(proof);
            }
            DQLSelectBuilder examDql = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExam.class, "e").column("e")
                .fetchPath(DQLJoinType.inner, EnrChosenEntranceExam.maxMarkForm().markSource().fromAlias("e"), "ms") // не передаем без источника балла все равно
                .where(in(property(EnrChosenEntranceExam.requestedCompetition().request().entrant().id().fromAlias("e")), entrantIds))
                .order(property(EnrChosenEntranceExam.requestedCompetition().priority().fromAlias("e")))
                .order(property(EnrChosenEntranceExam.id().fromAlias("e")))
                ;
            for (EnrChosenEntranceExam examResult : examDql.createStatement(session).<EnrChosenEntranceExam>list()) {
                examResultMap.get(examResult.getRequestedCompetition().getRequest().getId()).add(examResult);
            }
            for (EnrEntrantRequestAttachment attachment : getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest().entrant().id(), entrantIds, EnrEntrantRequestAttachment.id().s())) {
                attachmentMap.get(attachment.getEntrantRequest().getId()).add(attachment);
            }
            for (IdentityCard idc : getList(IdentityCard.class, IdentityCard.person().id(), personIds, IdentityCard.id().s())) {
                idcMap.get(idc.getPerson().getId()).add(idc);
            }
        }

        public List<Application> getApplicationList(final EnrEntrant entrant)
        {
            Debug.message("application list: entrant");

            errorFlag.setValue(null);

            List<EnrEntrantAchievement> achievements = achievementMap.get(entrant.getId());

            List<Application> xmlApplications = new ArrayList<>();
            for (EnrEntrantRequest request : requestMap.get(entrant.getId())) {

                final Application.Entrant xmlEntrant = new Application.Entrant();
                IdentityCard iCard = entrant.getPerson().getIdentityCard();

                // данные персоны
                xmlEntrant.setUID(uid(entrant));
                xmlEntrant.setFirstName(iCard.getFirstName());
                xmlEntrant.setLastName(iCard.getLastName());
                xmlEntrant.setMiddleName(iCard.getMiddleName());
                xmlEntrant.setGenderID(getGenderId(iCard));

                if ((null == iCard.getAddress() || !(iCard.getAddress() instanceof AddressDetailed)) && StringUtils.isEmpty(entrant.getPerson().getContactData().getEmail()))
                    error(entrant.getFullFio(), "error.applications.entrant.no-email-or-address");
                else
                {
                    xmlEntrant.setEmailOrMailAddress(new Application.Entrant.EmailOrMailAddress());

                    if(!StringUtils.isEmpty(entrant.getPerson().getContactData().getEmail()))
                    {
                        xmlEntrant.getEmailOrMailAddress().setEmail(entrant.getPerson().getContactData().getEmail());
                    }

                    if(iCard.getAddress() != null)
                    {
                        AddressItem settlement = (iCard.getAddress() instanceof AddressDetailed) ? ((AddressDetailed) iCard.getAddress()).getSettlement() : null;

                        Long townTypeId = EnrFisConverterManager.getTownId(settlement);

                        if(townTypeId == null)
                        {
                            error(entrant.getFullFio(), "error.applications.entrant.undefined-address-town-type-id");
                        }
                        else
                        {
                            Application.Entrant.EmailOrMailAddress.MailAddress mailAddress = new Application.Entrant.EmailOrMailAddress.MailAddress();

                            AddressItem region = settlement;
                            while (region != null && !Objects.equals(AddressLevelCodes.REGION, region.getAddressType().getAddressLevel().getCode()))
                            {
                                region = region.getParent();
                            }

                            EnrFisCatalogItem regionItem = null;

                            if (region != null)
                            {
                                regionItem = regionConverter.getCatalogItem(region, false);
                            }

                            if (regionItem != null)
                            {
                                mailAddress.setRegionID(regionItem.getFisID());
                            }
                            else
                            {
                                if(settlement == null || IKladrDefines.RUSSIA_COUNTRY_CODE == settlement.getCountry().getCode())
                                {
                                    warn(entrant.getFullFio(), "warn.applications.entrant.undefined-address-region");
                                }
                                mailAddress.setRegionID(1000L);
                            }

                            mailAddress.setTownTypeID(townTypeId);
                            mailAddress.setAddress(iCard.getAddress().getTitleWithFlat());

                            xmlEntrant.getEmailOrMailAddress().setMailAddress(mailAddress);
                        }
                    }

                }

                boolean isFromKrym = false;
                IdentityCard identityCardForKrym = null;

                Debug.message("application list: request");

                final Set<Long> originalReceived = originalReceivedMap.get(request.getId());
                final Set<Long> addedDocuments = new HashSet<>();
                final Set<Long> addedCustomDocuments = new HashSet<>();

                final Application xmlApplication = new Application();

                // данные заявления
                xmlApplication.setUID(uid(request)); // cсодержит uid абитуриента
                xmlApplication.setEntrant(xmlEntrant);
                xmlApplication.setApplicationNumber(xmlEntrant.getUID() + "/" + request.getRegNumber());
                xmlApplication.setRegistrationDate(xmlDateTime(request.getRegDate(), dateTimeFunction));
                xmlApplication.setNeedHostel(entrant.getPerson().isNeedDormitory());
                xmlApplication.setStatusID(getFisStatusId4EntrantRequest(request));

                // создаем контейнеры для всякой лажи
                xmlApplication.setFinSourceAndEduForms(new Application.FinSourceAndEduForms());


                Date acceptedDate = null;
                boolean hasMissRequest = false;
                boolean addedCompetition = false;
                final Set<String> preferenceUniqueCompetitionGroups = new HashSet<>();
                final Map<String, Application.FinSourceAndEduForms.FinSourceEduForm> finSourceUniqueCompetitionGroups = Maps.newHashMap();
                // выбранные конкурсы
                for (final EnrRequestedCompetition reqComp: reqCompMap.get(request.getId())) {
                    if (!competitionSet.contains(reqComp.getCompetition())) {
                        continue; // потому что передаем не за все филиалы
                    }

                    if (((entrant.getEnrollmentCampaign().getSettings().getEnrollmentCampaignType() != null
                            && EnrEnrollmentCampaignTypeCodes.ENTRANT_FOREIGN.equals(entrant.getEnrollmentCampaign().getSettings().getEnrollmentCampaignType().getCode()))
                            || CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(reqComp.getCompetition().getType().getCompensationType().getCode()))
                            && null != reqComp.getAcceptedDate() && (null == acceptedDate || acceptedDate.after(reqComp.getAcceptedDate())))
                    {
                        acceptedDate = reqComp.getAcceptedDate();
                    }

                    if (fisSession.isSkipRequestInOrderInfo())
                    {
                        EnrEntrantState state = reqComp.getState();
                        if (null != state && state.getCode().equals(EnrEntrantStateCodes.ENROLLED))
                        {
                            hasMissRequest = true;
                        }
                    }

                    addedCompetition = true;

                    if(!isFromKrym && reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().isAcceptPeopleResidingInCrimea())
                    {
                        isFromKrym = true;
                        identityCardForKrym = request.getIdentityCard();
                    }

                    Debug.message("application list: reqComp");

                    final String compGroupUid = competitionGroupUid(reqComp.getCompetition());

                    // КГ ФИС для выбранного конкурса
                    {
                        String admissionVolumeItemId = admissionVolumeItemUidMap.get(reqComp.getCompetition());
                        if (UID_ERROR.equals(admissionVolumeItemId)) {
                            // заглушка (если флаг ошибки учитывается, то абитуриент не попадет в пакет... если нет - то абитуриента отошьют ФИСовцы)
                            // здесь это именно ошибка
                            error(entrant.getFullFio(), "error.applications.entrant.no-admission-volume-item-id", reqComp.getTitle());
                        }
                    }

                    final TopOrgUnit topOrgUnit = TopOrgUnit.getInstance();

                    /////////

                    if(!finSourceUniqueCompetitionGroups.containsKey(compGroupUid))
                    {
                        // формы освоения, источники финансирования и виды ЦП
                        String targetOrganizationUID = null;
                        if (reqComp instanceof EnrRequestedCompetitionTA) {
                            EnrTargetAdmissionKind targetAdmissionKind = ((EnrRequestedCompetitionTA) reqComp).getTargetAdmissionKind().getTargetAdmissionKind();
                            if (!entrant.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) targetAdmissionKind = null;
                            targetOrganizationUID = uid(topOrgUnit, targetAdmissionKind);
                        }

                        final Application.FinSourceAndEduForms.FinSourceEduForm finSource = new Application.FinSourceAndEduForms.FinSourceEduForm();
                        xmlApplication.getFinSourceAndEduForms().getFinSourceEduForm().add(finSource);
                        finSource.setTargetOrganizationUID(targetOrganizationUID);
                        finSource.setCompetitiveGroupUID(compGroupUid);

                        if(fisSession.isAcceptedInfo())
                        {
                            finSource.setIsAgreedDate(xmlDateTime(reqComp.getAcceptedDate(), dateTimeFunction));
                        }

                        finSourceUniqueCompetitionGroups.put(compGroupUid, finSource);
                    }
                    else
                    {
                        final Application.FinSourceAndEduForms.FinSourceEduForm finSource = finSourceUniqueCompetitionGroups.get(compGroupUid);

                        if(fisSession.isAcceptedInfo() && finSource.getIsAgreedDate() == null)
                        {
                            finSource.setIsAgreedDate(xmlDateTime(reqComp.getAcceptedDate(), dateTimeFunction));
                        }
                    }

                    /////////

                    // для конкурсов без ВИ и по квоте передаем льготу
                    if (reqComp instanceof EnrRequestedCompetitionExclusive || reqComp instanceof EnrRequestedCompetitionNoExams) {

                        // основание для льготы должно быть, иначе ФИС нас не поймет
                        List<EnrEntrantBenefitProof> proofs = benefitProofMap.get(reqComp.getId());
                        if (proofs.isEmpty()) {
                            error(entrant.getFullFio(), "error.applications.entrant.no-benefit-proof", reqComp.getTitle());
                        } else {
                            PairKey<Boolean, Collection<Long>> documentsPairKey = addApplicationBenefit(xmlApplication, request, reqComp, originalReceived, proofs, false);
                            // если кастомный документ
                            if (documentsPairKey.getFirst())
                                addedCustomDocuments.addAll(documentsPairKey.getSecond());
                            else
                                addedDocuments.addAll(documentsPairKey.getSecond());
                        }
                    }

                    // если в заявлении есть преимущественное право, передаем его для всех КГ ФИС
                    if (reqComp.getRequest().getBenefitCategory() != null) {
                        if (preferenceUniqueCompetitionGroups.contains(compGroupUid)) continue;
                        preferenceUniqueCompetitionGroups.add(compGroupUid);

                        // основание для льготы должно быть, иначе ФИС нас не поймет
                        List<EnrEntrantBenefitProof> preferenceProofs = benefitProofMap.get(request.getId());
                        if (preferenceProofs.isEmpty()) {
                            error(entrant.getFullFio(), "error.applications.entrant.no-benefit-proof", reqComp.getTitle());
                        } else {
                            PairKey<Boolean, Collection<Long>> documentsPairKey = addApplicationBenefit(xmlApplication, request, reqComp, originalReceived, preferenceProofs, true);
                            // если кастомный документ
                            if (documentsPairKey.getFirst())
                                addedCustomDocuments.addAll(documentsPairKey.getSecond());
                            else
                                addedDocuments.addAll(documentsPairKey.getSecond());
                        }
                    }
                }

                if (hasMissRequest)
                {
                    fisSession.info(PREFIX, EnrFisSyncSessionManager.instance().getProperty("info.applications.entrant.request-miss", entrant.getFullFio(), request.getRegNumber()));
                    continue;
                }

                if(isFromKrym)
                {
                    xmlEntrant.setIsFromKrym(new Application.Entrant.IsFromKrym());
                    xmlEntrant.getIsFromKrym().setDocumentUID(uid(identityCardForKrym, entrant));
                }

                if (!addedCompetition) continue;

                xmlApplications.add(xmlApplication);

                Debug.message("application list: exams");

                Set<MultiKey> examResultKeys = new HashSet<>();
                List<EnrChosenEntranceExam> examResults = examResultMap.get(request.getId());
                for (EnrChosenEntranceExam examResult : examResults) {
                    if (examResult.getActualExam() == null) {
                        error(entrant.getFullFio(), "error.applications.entrant.non-actual-exam", examResult.getRequestedCompetition().getTitle());
                        continue;
                    }
                    if (examResult.getMarkSource() == null) {
                        continue;
                    }
                    String competitiveGroupUID = competitionGroupUid(examResult.getRequestedCompetition().getCompetition());
                    MultiKey examResultKey = new MultiKey(competitiveGroupUID, examResult.getActualExam().getPriority());
                    if (examResultKeys.contains(examResultKey)) {
                        continue;
                    } else {
                        examResultKeys.add(examResultKey);
                    }
                    if (xmlApplication.getEntranceTestResults() == null) {
                        xmlApplication.setEntranceTestResults(new Application.EntranceTestResults());
                    }
                    Application.EntranceTestResults.EntranceTestResult fisResult = new Application.EntranceTestResults.EntranceTestResult();

                    fisResult.setUID(uid(examResult));
                    fisResult.setCompetitiveGroupUID(competitiveGroupUID);
                    fisResult.setResultValue(BigDecimal.valueOf(examResult.getMarkAsDouble()));
                    fisResult.setResultSourceTypeID(getResultSourceFisId(examResult.getMarkSource()));
                    final EnrFisCatalogItem fisExamType = examTypeConverter.getCatalogItem(examResult.getActualExam().getExamSetElement().getType(), false);
                    if (fisExamType == null) {
                        error(entrant.getFullFio(), "error.applications.entrant.no-exam-type", examResult.getActualExam().getExamSetElement().getTitle());
                        fisResult.setEntranceTestTypeID(0);
                    }
                    else {
                        fisResult.setEntranceTestTypeID(fisExamType.getFisID());
                    }
                    fisResult.setEntranceTestSubject(new TEntranceTestSubject());
                    final EnrFisCatalogItem fisExamElement = examSetElementConverter.getCatalogItem(examResult.getActualExam().getExamSetElement().getValue(), false);
                    if (fisExamElement != null) {
                        fisResult.getEntranceTestSubject().setSubjectID(fisExamElement.getFisID());
                    }
                    else {
                        fisResult.getEntranceTestSubject().setSubjectName(examResult.getActualExam().getExamSetElement().getValue().getTitle());
                    }

                    if (examResult.getMarkSource() instanceof EnrEntrantMarkSourceBenefit && PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(((EnrEntrantMarkSourceBenefit)examResult.getMarkSource()).getMainProof().getDocumentType().getCode())) {

                        EnrEntrantBaseDocument proofDocument = ((EnrEntrantBaseDocument)  ((EnrEntrantMarkSourceBenefit)examResult.getMarkSource()).getMainProof());
                        EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) proofDocument.getDocRelation().getDocument();

                        fisResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());

                        if(EnrOlympiadTypeCodes.ALL_RUSSIA.equals(diploma.getOlympiadType().getCode()))
                        {
                            fisResult.getResultDocument().setOlympicTotalDocument(getOlympicTotalDocument(proofDocument, originalReceived.contains(proofDocument.getId()), request));
                        }
                        else if (EnrOlympiadTypeCodes.UKRAINIAN_OLYMPIAD.equals(diploma.getOlympiadType().getCode()))
                        {
                            fisResult.getResultDocument().setUkraineOlympicDocument(getUkraineOlympicDocument(proofDocument, originalReceived.contains(proofDocument.getId()), request));
                        }
                        else if (EnrOlympiadTypeCodes.INTERNATIONAL_OLYMPIAD.equals(diploma.getOlympiadType().getCode()))
                        {
                            fisResult.getResultDocument().setInternationalOlympiсDocument(getInternationalOlympicDocument(proofDocument, originalReceived.contains(proofDocument.getId()), request));
                        }
                        else if (EnrOlympiadTypeCodes.STATE_LIST.equals(diploma.getOlympiadType().getCode()))
                        {
                            fisResult.getResultDocument().setOlympicDocument(getOlympicDocument(proofDocument, originalReceived.contains(proofDocument.getId()), request));
                        }
                        else
                        {
                            error(entrant.getFullFio(), "error.applications.entrant.no-olympiad-type", diploma.getDisplayableTitle());
                        }
                    }
                    else if (examResult.getMarkSource() instanceof EnrEntrantMarkSourceBenefit && (PersonDocumentTypeCodes.SPORTS_DIPLOMA.equals(((EnrEntrantMarkSourceBenefit)examResult.getMarkSource()).getMainProof().getDocumentType().getCode()))) {

                        IEnrEntrantBenefitProofDocument proofDocument = ((EnrEntrantMarkSourceBenefit)examResult.getMarkSource()).getMainProof();
                        fisResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());
                        fisResult.getResultDocument().setSportDocument(getSportDocument((EnrEntrantBaseDocument) proofDocument, originalReceived.contains(proofDocument.getId()), request));

                    }
                    else if (examResult.getMarkSource() instanceof EnrEntrantMarkSourceStateExam) {
                        // не передаем ЕГЭ, у них целый ФБС есть
                    } else if (examResult.getMarkSource() instanceof EnrEntrantMarkSourceAppeal) {
                        EnrEntrantMarkSourceAppeal appeal = (EnrEntrantMarkSourceAppeal) examResult.getMarkSource();
                        fisResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());
                        TInstitutionDocument institutionDocument = new TInstitutionDocument();
                        fisResult.getResultDocument().setInstitutionDocument(institutionDocument);

                        EnrExamPassDiscipline examPassDiscipline = appeal.getExamPassAppeal().getExamPassDiscipline();
                        institutionDocument.setDocumentNumber(examPassDiscipline.getCode());
                        institutionDocument.setDocumentDate(xmlDate(examPassDiscipline.getMarkDate()));
                        institutionDocument.setDocumentTypeID(3L);
                    } else if (examResult.getMarkSource() instanceof EnrEntrantMarkSourceExamPass) {
                        EnrEntrantMarkSourceExamPass examPass = (EnrEntrantMarkSourceExamPass) examResult.getMarkSource();
                        fisResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());
                        TInstitutionDocument institutionDocument = new TInstitutionDocument();
                        fisResult.getResultDocument().setInstitutionDocument(institutionDocument);

                        EnrExamPassDiscipline examPassDiscipline = examPass.getExamPassDiscipline();
                        institutionDocument.setDocumentNumber(examPassDiscipline.getCode());
                        institutionDocument.setDocumentDate(xmlDate(examPassDiscipline.getMarkDate()));
                        institutionDocument.setDocumentTypeID(1L);
                    }

                    xmlApplication.getEntranceTestResults().getEntranceTestResult().add(fisResult);
                }

                Debug.message("application list: documents, achievs");

                xmlApplication.setApplicationDocuments(new Application.ApplicationDocuments());


                // удостоверение личности
                xmlApplication.getApplicationDocuments().setIdentityDocument(getXmlIdentityCard(request.getIdentityCard(), entrant));

                // остальные УЛ
                List<IdentityCard> idcList = idcMap.get(entrant.getPerson().getId());
                idcList.remove(request.getIdentityCard());
                for (IdentityCard idc : idcList) {
                    if (null == xmlApplication.getApplicationDocuments().getOtherIdentityDocuments()) {
                        xmlApplication.getApplicationDocuments().setOtherIdentityDocuments(new Application.ApplicationDocuments.OtherIdentityDocuments());
                    }
                    xmlApplication.getApplicationDocuments().getOtherIdentityDocuments().getIdentityDocument().add(getXmlIdentityCardOther(idc, entrant));
                }

                // документ о полученом образовании
                xmlApplication.getApplicationDocuments().setEduDocuments(new Application.ApplicationDocuments.EduDocuments());
                xmlApplication.getApplicationDocuments().getEduDocuments().getEduDocument().add(getXmlEduDocument(request, acceptedDate));


                Set<IEnrEntrantRequestAttachable> documents = new LinkedHashSet<>();
                List<EnrEntrantRequestAttachment> attachments = attachmentMap.get(request.getId());
                for (EnrEntrantRequestAttachment attachment : attachments) {
                    documents.add(attachment.getDocument());
                }

                for (EnrEntrantAchievement achievement : achievements) {
                    if (!achievement.getType().getAchievementKind().getRequestType().equals(request.getType())) {
                        continue;
                    }

                    if (achievement.getType().isForRequest() && !request.equals(achievement.getRequest()))
                    {
                        continue;
                    }

                    if (xmlApplication.getIndividualAchievements() == null) {
                        xmlApplication.setIndividualAchievements(new Application.IndividualAchievements());
                    }
                    Application.IndividualAchievements.IndividualAchievement fisAchievement = new Application.IndividualAchievements.IndividualAchievement();
                    xmlApplication.getIndividualAchievements().getIndividualAchievement().add(fisAchievement);
                    fisAchievement.setIAUID(uid(achievement, request));
                    fisAchievement.setInstitutionAchievementUID(uid(achievement.getType()));
                    fisAchievement.setIAMark(Math.round(achievement.getRatingMarkAsLong() / 1000.0));

                    if (null != achievement.getDocument()) {
                        IEnrEntrantAchievementProofDocument document = achievement.getDocument();

                        fisAchievement.setIADocumentUID(attachableUid(document));
                        documents.add(document);

                        if (!addedCustomDocuments.contains(document.getId()))
                        {
                            if (xmlApplication.getApplicationDocuments().getCustomDocuments() == null)
                                xmlApplication.getApplicationDocuments().setCustomDocuments(new Application.ApplicationDocuments.CustomDocuments());

                            xmlApplication.getApplicationDocuments().getCustomDocuments().getCustomDocument().add(getCustomDocument(request, document, originalReceived.contains(document.getId())));
                            addedCustomDocuments.add(document.getId());
                        }
                    }
                    else
                    {
                        fisAchievement.setIADocumentUID("");
                    }

                }
                addedDocuments.addAll(addedCustomDocuments);

                for (IEnrEntrantRequestAttachable document : documents) {
                    if (addedDocuments.contains(document.getId())) {
                        continue;
                    }

                    EnrCampaignEntrantDocument documentType = getByNaturalId(new EnrCampaignEntrantDocument.NaturalId(document.getDocumentType(), entrant.getEnrollmentCampaign()));
                    if(documentType == null || !documentType.isSendFIS())
                    {
                        continue;
                    }

                    if (PersonDocumentTypeCodes.ORPHAN.equals(document.getDocumentType().getCode()))
                    {
                        if (xmlApplication.getApplicationDocuments().getOrphanDocuments() == null) {
                            xmlApplication.getApplicationDocuments().setOrphanDocuments(new Application.ApplicationDocuments.OrphanDocuments());
                        }
                        xmlApplication.getApplicationDocuments().getOrphanDocuments().getOrphanDocument().add(getOrphanDocument((EnrEntrantBaseDocument) document, originalReceived.contains(document.getId()), request));
                    }
                    else if (PersonDocumentTypeCodes.COMBAT_VETERANS.equals(document.getDocumentType().getCode()))
                    {
                        if (xmlApplication.getApplicationDocuments().getVeteranDocuments() == null) {
                            xmlApplication.getApplicationDocuments().setVeteranDocuments(new Application.ApplicationDocuments.VeteranDocuments());
                        }
                        xmlApplication.getApplicationDocuments().getVeteranDocuments().getVeteranDocument().add(getVeteranDocument((EnrEntrantBaseDocument) document, originalReceived.contains(document.getId()), request));
                    }
                    else if (PersonDocumentTypeCodes.SPORTS_DIPLOMA.equals(document.getDocumentType().getCode()))
                    {
                        if (xmlApplication.getApplicationDocuments().getSportDocuments() == null) {
                            xmlApplication.getApplicationDocuments().setSportDocuments(new Application.ApplicationDocuments.SportDocuments());
                        }
                        xmlApplication.getApplicationDocuments().getSportDocuments().getSportDocument().add(getSportDocument((EnrEntrantBaseDocument) document, originalReceived.contains(document.getId()), request));
                    }
                    else if (PersonDocumentTypeCodes.BELONGING_NATION.equals(document.getDocumentType().getCode()))
                    {
                        if (xmlApplication.getApplicationDocuments().getCompatriotDocuments() == null) {
                            xmlApplication.getApplicationDocuments().setCompatriotDocuments(new Application.ApplicationDocuments.CompatriotDocuments());
                        }
                        xmlApplication.getApplicationDocuments().getCompatriotDocuments().getCompatriotDocument().add(getCompatriotDocument((EnrEntrantBaseDocument) document, originalReceived.contains(document.getId()), request));
                    }
                    else
                    {
                        if (xmlApplication.getApplicationDocuments().getCustomDocuments() == null) {
                            xmlApplication.getApplicationDocuments().setCustomDocuments(new Application.ApplicationDocuments.CustomDocuments());
                        }
                        xmlApplication.getApplicationDocuments().getCustomDocuments().getCustomDocument().add(getCustomDocument(request, document, originalReceived.contains(document.getId())));
                    }
                }

                // сортируем всю начинку

                Debug.message("application list: sorting");

                if (null != xmlApplication.getApplicationDocuments() && null != xmlApplication.getApplicationDocuments().getCustomDocuments()) {
                    Collections.sort(xmlApplication.getApplicationDocuments().getCustomDocuments().getCustomDocument(), (o1, o2) -> o1.getUID().compareTo(o2.getUID()));
                }
                if (null != xmlApplication.getEntranceTestResults()) {
                    Collections.sort(xmlApplication.getEntranceTestResults().getEntranceTestResult(), (o1, o2) -> o1.getUID().compareTo(o2.getUID()));
                }
                if (null != xmlApplication.getIndividualAchievements()) {
                    Collections.sort(xmlApplication.getIndividualAchievements().getIndividualAchievement(), (o1, o2) -> o1.getIAUID().compareTo(o2.getIAUID()));
                }
                if (null != xmlApplication.getApplicationCommonBenefits()) {
                    Collections.sort(xmlApplication.getApplicationCommonBenefits().getApplicationCommonBenefit(), (o1, o2) -> o1.getUID().compareTo(o2.getUID()));
                }
            }



            Collections.sort(xmlApplications, (o1, o2) -> o1.getUID().compareTo(o2.getUID()));

            // возвращаем данные наружу
            return xmlApplications;
        }

        protected PairKey<Boolean, Collection<Long>> addApplicationBenefit(Application xmlApplication, EnrEntrantRequest request, EnrRequestedCompetition reqComp, Set<Long> originalReceived, List<EnrEntrantBenefitProof> proofs, boolean preference) {

            EnrEntrant entrant = request.getEntrant();

            if (xmlApplication.getApplicationCommonBenefits() == null) {
                xmlApplication.setApplicationCommonBenefits(new Application.ApplicationCommonBenefits());
            }
            Application.ApplicationCommonBenefits.ApplicationCommonBenefit applicationCommonBenefit = new Application.ApplicationCommonBenefits.ApplicationCommonBenefit();
            xmlApplication.getApplicationCommonBenefits().getApplicationCommonBenefit().add(applicationCommonBenefit);
            applicationCommonBenefit.setUID(preference ? preferenceUid(reqComp) : benefitUid(reqComp));
            applicationCommonBenefit.setCompetitiveGroupUID(competitionGroupUid(reqComp.getCompetition()));

            if (preference) {
                applicationCommonBenefit.setBenefitKindID(5);
            } else if (reqComp.getCompetition().isNoExams()) {
                applicationCommonBenefit.setBenefitKindID(1);
            } else if (reqComp.getCompetition().isExclusive()) {
                applicationCommonBenefit.setBenefitKindID(4);
            } else {
                throw new IllegalArgumentException();
            }

            // выбираем документ для передачи в ФИС - у нас их несколько, а у них один
            Collections.sort(proofs, (o1, o2) -> {
                int result1 = Integer.compare(priority(o1), priority(o2));
                return result1 == 0 ? o1.getDocument().getDisplayableTitle().compareTo(o2.getDocument().getDisplayableTitle()) : result1;
            });
            EnrEntrantBenefitProof mainProof = proofs.get(0);

            IEnrEntrantBenefitProofDocument proofDocument = mainProof.getDocument();
            if (!preference && PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(proofDocument.getDocumentType().getCode())) {
                // диплом олимпиады
                EnrEntrantBaseDocument diplomaDoc = (EnrEntrantBaseDocument) proofDocument;
                EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) proofDocument;
                if(EnrOlympiadTypeCodes.ALL_RUSSIA.equals(diploma.getOlympiadType().getCode()))
                {
                    applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                    applicationCommonBenefit.setDocumentTypeID(10L);
                    TOlympicTotalDocument fisDocument = getOlympicTotalDocument(diplomaDoc, originalReceived.contains(diplomaDoc.getId()), request);
                    applicationCommonBenefit.getDocumentReason().setOlympicTotalDocument(fisDocument);
                }
                else if (EnrOlympiadTypeCodes.UKRAINIAN_OLYMPIAD.equals(diploma.getOlympiadType().getCode()))
                {
                    applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                    applicationCommonBenefit.setDocumentTypeID(27L);
                    TUkraineOlympic fisDocument = getUkraineOlympicDocument(diplomaDoc, originalReceived.contains(diplomaDoc.getId()), request);
                    applicationCommonBenefit.getDocumentReason().setUkraineOlympicDocument(fisDocument);
                }
                else if (EnrOlympiadTypeCodes.INTERNATIONAL_OLYMPIAD.equals(diploma.getOlympiadType().getCode()))
                {
                    applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                    applicationCommonBenefit.setDocumentTypeID(28L);
                    TInternationalOlympic fisDocument = getInternationalOlympicDocument(diplomaDoc, originalReceived.contains(diplomaDoc.getId()), request);
                    applicationCommonBenefit.getDocumentReason().setInternationalOlympiсDocument(fisDocument);
                }
                else if (EnrOlympiadTypeCodes.STATE_LIST.equals(diploma.getOlympiadType().getCode()))
                {
                    applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                    applicationCommonBenefit.setDocumentTypeID(9L);
                    TOlympicDocument fisDocument = getOlympicDocument(diplomaDoc, originalReceived.contains(diplomaDoc.getId()), request);
                    applicationCommonBenefit.getDocumentReason().setOlympicDocument(fisDocument);
                }
                else
                {
                    error(entrant.getFullFio(), "error.applications.entrant.no-olympiad-type", diploma.getDisplayableTitle());
                }

                return PairKey.create(false, Collections.singletonList(diplomaDoc.getId()));
            } else if (!preference && (PersonDocumentTypeCodes.DISABLED_CERT.equals(proofDocument.getDocumentType().getCode()) || PersonDocumentTypeCodes.PSY_CERT.equals(proofDocument.getDocumentType().getCode()))) {
                // всякие инвалиды

                applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments medicalDocuments = new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments();
                applicationCommonBenefit.getDocumentReason().setMedicalDocuments(medicalDocuments);
                medicalDocuments.setBenefitDocument(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments.BenefitDocument());

                boolean disabledDoc = PersonDocumentTypeCodes.DISABLED_CERT.equals(proofDocument.getDocumentType().getCode());
                applicationCommonBenefit.setDocumentTypeID(disabledDoc ? 11L : 12L);

                if ( disabledDoc) {
                    EnrEntrantBaseDocument disabilityDocument = (EnrEntrantBaseDocument) proofDocument;

                    if(StringUtils.isEmpty(disabilityDocument.getSeria()))
                    {
                        error(entrant.getFullFio(), "error.applications.entrant.no-doc-seria", disabilityDocument.getDisplayableTitle());
                    }

                    if(StringUtils.isEmpty(disabilityDocument.getNumber()))
                    {
                        error(entrant.getFullFio(), "error.applications.entrant.no-doc-number", disabilityDocument.getDisplayableTitle());
                    }

                    if (StringUtils.length(proofDocument.getSeria()) > 10 ) {
                        error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", proofDocument.getDisplayableTitle(), 10);
                    }

                    TDisabilityDocument fisDocument = new TDisabilityDocument();
                    fisDocument.setUID(proofDocumentUid(disabilityDocument));
                    medicalDocuments.getBenefitDocument().setDisabilityDocument(fisDocument);
                    if (Boolean.TRUE.equals(originalReceived.contains(proofDocument.getId()))) {
                        fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
                    }
                    fisDocument.setDocumentSeries(proofDocument.getSeria());
                    fisDocument.setDocumentNumber(proofDocument.getNumber());
                    fisDocument.setDocumentDate(xmlDate(proofDocument.getIssuanceDate()));
                    fisDocument.setDisabilityTypeID(getDisabilityType(disabilityDocument.getDocRelation().getDocument().getDocumentCategory()));
                    fisDocument.setDocumentOrganization(disabilityDocument.getIssuancePlace());
                }  else {

                    if(StringUtils.isEmpty(proofDocument.getNumber()))
                    {
                        error(entrant.getFullFio(), "error.applications.entrant.no-doc-number", proofDocument.getDisplayableTitle());
                    }

                    if (StringUtils.length(proofDocument.getSeria()) > 10 ) {
                        error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", proofDocument.getDisplayableTitle(), 10);
                    }

                    TMedicalDocument fisDocument = new TMedicalDocument();
                    fisDocument.setUID(proofDocumentUid(proofDocument));
                    medicalDocuments.getBenefitDocument().setMedicalDocument(fisDocument);
                    if (Boolean.TRUE.equals(originalReceived.contains(proofDocument.getId()))) {
                        fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
                    }
                    fisDocument.setDocumentNumber(proofDocument.getNumber());
                    fisDocument.setDocumentDate(xmlDate(proofDocument.getIssuanceDate()));
                    fisDocument.setDocumentOrganization(proofDocument.getIssuancePlace());
                }

                EnrEntrantBaseDocument allowEduDoc = null;
                for (EnrEntrantBenefitProof proof : proofs) {
                    if (PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT.equals(proof.getDocument().getDocumentType().getCode())) {
                        allowEduDoc = (EnrEntrantBaseDocument) proof.getDocument();
                        break;
                    }
                }
                if (allowEduDoc == null) {
                    error(entrant.getFullFio(), "error.applications.entrant.no-allow-edu-doc", reqComp.getTitle());
                    return PairKey.create(false, Collections.singletonList(proofDocument.getId()));
                } else {

                    if (null == allowEduDoc.getIssuanceDate())
                    {
                        error(entrant.getFullFio(), "error.applications.entrant.no-doc-issuance-date", allowEduDoc.getDisplayableTitle());
                    }

                    if(StringUtils.isEmpty(allowEduDoc.getNumber()))
                    {
                        error(entrant.getFullFio(), "error.applications.entrant.no-doc-number", allowEduDoc.getDisplayableTitle());
                    }

                    TAllowEducationDocument fisDocument = new TAllowEducationDocument();
                    medicalDocuments.setAllowEducationDocument(fisDocument);
                    if (Boolean.TRUE.equals(originalReceived.contains(allowEduDoc.getId()))) {
                        fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
                    }
                    fisDocument.setUID(proofDocumentUid(allowEduDoc));
                    fisDocument.setDocumentNumber(allowEduDoc.getNumber());
                    fisDocument.setDocumentDate(xmlDate(allowEduDoc.getIssuanceDate()));
                    fisDocument.setDocumentOrganization(allowEduDoc.getIssuancePlace());
                    return PairKey.create(false, Arrays.asList(proofDocument.getId(), allowEduDoc.getId()));
                }
            }
            else if (!preference && (PersonDocumentTypeCodes.ORPHAN.equals(proofDocument.getDocumentType().getCode())))
            {
                applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                applicationCommonBenefit.setDocumentTypeID(30L);
                TOrphanDocument fisDocument = getOrphanDocument((EnrEntrantBaseDocument) proofDocument, originalReceived.contains(proofDocument.getId()), request);
                applicationCommonBenefit.getDocumentReason().setOrphanDocument(fisDocument);
            }
            else if (!preference && (PersonDocumentTypeCodes.COMBAT_VETERANS.equals(proofDocument.getDocumentType().getCode())))
            {
                applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                applicationCommonBenefit.setDocumentTypeID(31L);
                TVeteranDocument fisDocument = getVeteranDocument((EnrEntrantBaseDocument) proofDocument, originalReceived.contains(proofDocument.getId()), request);
                applicationCommonBenefit.getDocumentReason().setVeteranDocument(fisDocument);
            }
            else if (!preference && (PersonDocumentTypeCodes.SPORTS_DIPLOMA.equals(proofDocument.getDocumentType().getCode())))
            {
                applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                TSportDocument fisDocument = getSportDocument((EnrEntrantBaseDocument) proofDocument, originalReceived.contains(proofDocument.getId()), request);
                applicationCommonBenefit.setDocumentTypeID(fisDocument.getSportCategoryID());
                applicationCommonBenefit.getDocumentReason().setSportDocument(fisDocument);
            }
            else
            {
                // а теперь всякое все остальное, а также для преимущественного права
                applicationCommonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());
                TCustomDocument fisDocument = getCustomDocument(proofDocument, originalReceived.contains(proofDocument.getId()), request);
                applicationCommonBenefit.getDocumentReason().setCustomDocument(fisDocument);
                applicationCommonBenefit.setDocumentTypeID(15L);
            }

            return PairKey.create(true, Collections.singletonList(proofDocument.getId()));
        }

        protected TCustomDocument getCustomDocument(EnrEntrantRequest request, IEnrEntrantRequestAttachable document, boolean original)
        {
            TCustomDocument fisDocument = new TCustomDocument();
            fisDocument.setUID(attachableUid(document));

            if (StringUtils.length(document.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", document.getDisplayableTitle(), 10);
            }

            if (Boolean.TRUE.equals(original)) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }
            fisDocument.setDocumentSeries(document.getSeria());
            fisDocument.setDocumentNumber(document.getNumber());

            if(document.getIssuanceDate() == null)
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-date", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentDate(xmlDate(document.getIssuanceDate()));
            }

            if(StringUtils.isEmpty(document.getIssuancePlace()))
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-place", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentOrganization(document.getIssuancePlace());
            }

            fisDocument.setDocumentName(document.getDocumentType().getTitle());

            return fisDocument;
        }

        /** @return safeMap { eduInstitution -> xml:eduDocument } */
        protected Application.ApplicationDocuments.EduDocuments.EduDocument getXmlEduDocument(final EnrEntrantRequest request, final Date acceptedDate)
        {
            final PersonEduDocument eduDocument = request.getEduDocument();
            final Application.ApplicationDocuments.EduDocuments.EduDocument xmlEduDocument = new Application.ApplicationDocuments.EduDocuments.EduDocument();

            if (isSchoolCertificateDocument(eduDocument))
            {
                if (StringUtils.length(eduDocument.getSeria()) > 10 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 10);
                }

                TSchoolCertificateDocument fisDocument = new TSchoolCertificateDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setSchoolCertificateDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
                fisDocument.setEndYear((long) eduDocument.getYearEnd());
                fisDocument.setGPA(eduDocument.getAvgMarkAsFloat());
            }
            else if (isSchoolCertificateBasicDocument(eduDocument))
            {
                if (StringUtils.length(eduDocument.getSeria()) > 10 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 10);
                }
                TSchoolCertificateDocument fisDocument = new TSchoolCertificateDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setSchoolCertificateBasicDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
                fisDocument.setEndYear((long) eduDocument.getYearEnd());
                fisDocument.setGPA(eduDocument.getAvgMarkAsFloat());

            }
            else if (isHighEduDiplomaDocument(eduDocument))
            {
                if (StringUtils.length(eduDocument.getSeria()) > 10 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 10);
                }

                if(StringUtils.isEmpty(eduDocument.getSeria()))
                {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.no-doc-seria", eduDocument.getDisplayableTitle());
                }

                THighEduDiplomaDocument fisDocument = new THighEduDiplomaDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setHighEduDiplomaDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
                fisDocument.setEndYear((long) eduDocument.getYearEnd());
                fisDocument.setGPA(eduDocument.getAvgMarkAsFloat());
            }
            else if (isPostgraduateDocument(eduDocument))
            {
                if (StringUtils.length(eduDocument.getSeria()) > 10 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 10);
                }

                if(StringUtils.isEmpty(eduDocument.getSeria()))
                {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.no-doc-seria", eduDocument.getDisplayableTitle());
                }

                TPostGraduateDiplomaDocument fisDocument = new TPostGraduateDiplomaDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setPostGraduateDiplomaDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
                fisDocument.setEndYear((long) eduDocument.getYearEnd());
                fisDocument.setGPA(eduDocument.getAvgMarkAsFloat());
            }
            else if (isMiddleEduDiplomaDocument(eduDocument))
            {
                if (StringUtils.length(eduDocument.getSeria()) > 10 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 10);
                }

                if(StringUtils.isEmpty(eduDocument.getSeria()))
                {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.no-doc-seria", eduDocument.getDisplayableTitle());
                }

                TMiddleEduDiplomaDocument fisDocument = new TMiddleEduDiplomaDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setMiddleEduDiplomaDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
                fisDocument.setEndYear((long) eduDocument.getYearEnd());
                fisDocument.setGPA(eduDocument.getAvgMarkAsFloat());
            }
            else if (isBasicDiplomaDocument(eduDocument))
            {
                if (StringUtils.length(eduDocument.getSeria()) > 10 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 10);
                }

                if(StringUtils.isEmpty(eduDocument.getSeria()))
                {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.no-doc-seria", eduDocument.getDisplayableTitle());
                }

                TBasicDiplomaDocument fisDocument = new TBasicDiplomaDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setBasicDiplomaDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
                fisDocument.setEndYear((long) eduDocument.getYearEnd());
                fisDocument.setGPA(eduDocument.getAvgMarkAsFloat());

            }
            else
            {
                if (StringUtils.length(eduDocument.getSeria()) > 20 ) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", eduDocument.getDisplayableTitle(), 20);
                }

                TEduCustomDocument fisDocument = new TEduCustomDocument();
                fisDocument.setUID(uid(eduDocument, request.getEntrant()));
                xmlEduDocument.setEduCustomDocument(fisDocument);
                fisDocument.setDocumentSeries(eduDocument.getSeria());
                fisDocument.setDocumentNumber(eduDocument.getNumber());
                if (eduDocument.getIssuanceDate() == null) {
                    error(eduDocument.getPerson().getFullFio(), "error.applications.entrant.no-doc-issuance-date", eduDocument.getDisplayableTitle());
                }
                fisDocument.setDocumentDate(xmlDate(eduDocument.getIssuanceDate()));
                fisDocument.setDocumentTypeNameText(eduDocument.getDocumentKindTitle());

                if (Boolean.TRUE.equals(request.isEduInstDocOriginalHandedIn()))
                    fisDocument.setOriginalReceivedDate(xmlDate(request.getEduInstDocOriginalRef().getRegistrationDate()));
                else if (null != acceptedDate)
                    fisDocument.setOriginalReceivedDate(xmlDate(acceptedDate));

                fisDocument.setDocumentOrganization(eduDocument.getEduOrganization());
            }

            return xmlEduDocument;
        }

        /** @return safeMap { identityCard -> xml:identityCard } */
        protected Application.ApplicationDocuments.IdentityDocument getXmlIdentityCard(final IdentityCard identityCard, final EnrEntrant entrant) {
            final Application.ApplicationDocuments.IdentityDocument xmlIdentityCard = new Application.ApplicationDocuments.IdentityDocument();


            if (StringUtils.length(identityCard.getSeria()) > 20 ) {
                error(identityCard.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", identityCard.getDisplayableTitle(), 20);
            }

            EnrFisCatalogItem ecfCitizenship = null;
            if(UniDefines.CITIZENSHIP_NO.equals(identityCard.getCitizenship().getCode()))
            {
                ecfCitizenship = getByNaturalId(new EnrFisCatalogItemGen.NaturalId("7", "237")); // "Не определено"
            }
            else
                ecfCitizenship = addressCountryConverter.getCatalogItem((AddressCountry)identityCard.getCitizenship(), false);

            if (null == ecfCitizenship) {
                error(identityCard.getFullFio(), "error.applications.entrant.undefined-citizenship", identityCard.getCitizenship().getTitle());
            } else {
                xmlIdentityCard.setNationalityTypeID(ecfCitizenship.getFisID());
            }

            if(StringUtils.isEmpty(identityCard.getNumber()))
            {
                error(identityCard.getFullFio(), "error.applications.entrant.no-doc-number", identityCard.getDisplayableTitle());
            }

            if(identityCard.getIssuanceDate() == null)
            {
                error(identityCard.getFullFio(), "error.applications.entrant.no-doc-issuance-date", identityCard.getDisplayableTitle());
            }

            if(identityCard.getBirthDate() == null)
            {
                error(identityCard.getFullFio(), "error.applications.entrant.no-doc-birthdate", identityCard.getDisplayableTitle());
            }

            xmlIdentityCard.setUID(uid(identityCard, entrant));
            xmlIdentityCard.setLastName(identityCard.getLastName());
            xmlIdentityCard.setFirstName(identityCard.getFirstName());
            xmlIdentityCard.setMiddleName(identityCard.getMiddleName());
            xmlIdentityCard.setGenderID(getGenderId(identityCard));
            xmlIdentityCard.setIdentityDocumentTypeID(getIdCardTypeID(identityCard));
            xmlIdentityCard.setBirthDate(xmlDate(identityCard.getBirthDate()));
            xmlIdentityCard.setBirthPlace(identityCard.getBirthPlace());
            xmlIdentityCard.setDocumentSeries(identityCard.getSeria());
            xmlIdentityCard.setDocumentNumber(identityCard.getNumber());
            xmlIdentityCard.setDocumentDate(xmlDate(identityCard.getIssuanceDate()));
            xmlIdentityCard.setDocumentOrganization(identityCard.getIssuancePlace());
            if(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII.equals(identityCard.getCardType().getCode()))
            {
                if(StringUtils.isEmpty(identityCard.getIssuanceCode())
                        || !Pattern.compile("s*\\d\\d\\d\\-\\d\\d\\d\\s*").matcher(identityCard.getIssuanceCode()).matches())
                {
                    warn(identityCard.getFullFio(), "warn.applications.entrant.incorrect-identity-card-issuance-code", identityCard.getDisplayableTitle());
                }
                else
                {
                    xmlIdentityCard.setSubdivisionCode(identityCard.getIssuanceCode());
                }
            }

            return xmlIdentityCard;
        }

        private Application.ApplicationDocuments.OtherIdentityDocuments.IdentityDocument getXmlIdentityCardOther(IdentityCard identityCard, final EnrEntrant entrant)
        {
            final Application.ApplicationDocuments.OtherIdentityDocuments.IdentityDocument xmlIdentityCard = new Application.ApplicationDocuments.OtherIdentityDocuments.IdentityDocument();

            EnrFisCatalogItem ecfCitizenship = null;
            if(UniDefines.CITIZENSHIP_NO.equals(identityCard.getCitizenship().getCode()))
            {
                ecfCitizenship = getByNaturalId(new EnrFisCatalogItemGen.NaturalId("7", "237")); // "Не определено"
            }
            else
                ecfCitizenship = addressCountryConverter.getCatalogItem((AddressCountry)identityCard.getCitizenship(), false);

            if (null == ecfCitizenship) {
                error(identityCard.getFullFio(), "error.applications.entrant.undefined-citizenship", identityCard.getCitizenship().getTitle());
            } else {
                xmlIdentityCard.setNationalityTypeID(ecfCitizenship.getFisID());
            }

            if(StringUtils.isEmpty(identityCard.getNumber()))
            {
                error(identityCard.getFullFio(), "error.applications.entrant.no-doc-number", identityCard.getDisplayableTitle());
            }

            if(identityCard.getIssuanceDate() == null)
            {
                error(identityCard.getFullFio(), "error.applications.entrant.no-doc-issuance-date", identityCard.getDisplayableTitle());
            }

            if(identityCard.getBirthDate() == null)
            {
                error(identityCard.getFullFio(), "error.applications.entrant.no-doc-birthdate", identityCard.getDisplayableTitle());
            }

            if (StringUtils.length(identityCard.getSeria()) > 20 ) {
                error(identityCard.getPerson().getFullFio(), "error.applications.entrant.too-long-document-seria", identityCard.getDisplayableTitle(), 20);
            }

            xmlIdentityCard.setUID(uid(identityCard, entrant));
            xmlIdentityCard.setLastName(identityCard.getLastName());
            xmlIdentityCard.setFirstName(identityCard.getFirstName());
            xmlIdentityCard.setMiddleName(identityCard.getMiddleName());
            xmlIdentityCard.setGenderID(getGenderId(identityCard));
            xmlIdentityCard.setIdentityDocumentTypeID(getIdCardTypeID(identityCard));
            xmlIdentityCard.setBirthDate(xmlDate(identityCard.getBirthDate()));
            xmlIdentityCard.setBirthPlace(identityCard.getBirthPlace());
            xmlIdentityCard.setDocumentSeries(identityCard.getSeria());
            xmlIdentityCard.setDocumentNumber(identityCard.getNumber());
            xmlIdentityCard.setDocumentDate(xmlDate(identityCard.getIssuanceDate()));
            xmlIdentityCard.setDocumentOrganization(identityCard.getIssuancePlace());

            if(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII.equals(identityCard.getCardType().getCode()))
            {
                if(StringUtils.isEmpty(identityCard.getIssuanceCode())
                        || !Pattern.compile("s*\\d\\d\\d\\-\\d\\d\\d\\s*").matcher(identityCard.getIssuanceCode()).matches())
                {
                    warn(identityCard.getFullFio(), "warn.applications.entrant.incorrect-identity-card-issuance-code", identityCard.getDisplayableTitle());
                }
                else
                {
                    xmlIdentityCard.setSubdivisionCode(identityCard.getIssuanceCode());
                }
            }

            return xmlIdentityCard;
        }

        protected long getIdCardTypeID(final IdentityCard identityCard)
        {
            final IdentityCardType cardType = identityCard.getCardType();
            if (null == cardType) { return 0L; }
            switch (cardType.getCode()) {
                case IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII: return 1L;
                case IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT: return 2L;
                case IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA: return 3L;
                case IdentityCardTypeCodes.SVIDETELSTVO_O_ROJDENII:
                {
                    if(IKladrDefines.RUSSIA_COUNTRY_CODE == identityCard.getCitizenship().getCode())
                    {
                        return 4L;
                    }
                    else
                    {
                        return 8L;
                    }
                }
                case IdentityCardTypeCodes.VREMENNOE_UDOSTOVERENIE: return 10L;
                case IdentityCardTypeCodes.VID_NA_JITELSTVO: return 11L;
                case IdentityCardTypeCodes.DRUGOY_DOKUMENT: return 9L;
            }
            error(identityCard.getFullFio(), "error.applications.entrant.undefined-identity-card-type", cardType.getCode() + " " + cardType.getTitle());
            return 0L;
        }

        protected long getGenderId(final IdentityCard iCard)
        {
            final Sex sex = iCard.getSex();
            if (null == sex) {
                error(iCard.getFullFio(), "error.applications.entrant.undefined-sex");
                return 0L;
            }
            switch (sex.getCode()) {
                case SexCodes.MALE: return 1L;
                case SexCodes.FEMALE: return 2L;
            }

            error(iCard.getFullFio(), "error.applications.entrant.unknown-sex", sex.getCode() + " " + sex.getTitle());
            return 0L;
        }

        protected long getFisStatusId4EntrantRequest(final EnrEntrantRequest request) {
            return request.isTakeAwayDocument() ? 6L : fisSession.getRequestState();
        }

        protected int priority(EnrEntrantBenefitProof proof)
        {
            return proof.getDocument().getDocumentType().getPriority();
        }

        protected long getDisabilityType(PersonDocumentCategory disablementType)
        {
            switch (disablementType.getCode()) {
                case PersonDocumentCategoryCodes.FIRST: return 1;
                case PersonDocumentCategoryCodes.SECOND: return 2;
                case PersonDocumentCategoryCodes.THIRD: return 3;
                case PersonDocumentCategoryCodes.CHILD: return 4;
                case PersonDocumentCategoryCodes.CHILDHOOD: return 5;
                case PersonDocumentCategoryCodes.MILITARY_INJURY: return 6;
            }
            throw new IllegalArgumentException();
        }

        protected long getResultSourceFisId(EnrEntrantMarkSource markSource)
        {
            if (markSource instanceof EnrEntrantMarkSourceStateExam) return 1;
            if (markSource instanceof EnrEntrantMarkSourceBenefit) return 3;
            return 2;
        }

        protected TOlympicDocument getOlympicDocument(EnrEntrantBaseDocument diplomaDoc, boolean original, EnrEntrantRequest request)
        {
            EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) diplomaDoc.getDocRelation().getDocument();
            TOlympicDocument fisDocument = new TOlympicDocument();

            if (StringUtils.length(diploma.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", diploma.getDisplayableTitle(), 10);
            }

            fisDocument.setUID(proofDocumentUid(diplomaDoc));
            fisDocument.setDocumentSeries(diploma.getSeria());
            fisDocument.setDocumentNumber(diploma.getNumber());
            fisDocument.setDocumentDate(xmlDate(diploma.getIssuanceDate()));
            fisDocument.setDiplomaTypeID(EnrOlympiadHonourCodes.WINNER.equals(diploma.getHonour().getCode()) ? 1 : 2);

            EnrFisCatalogItem fisOlympiad = olympiadConverter.getCatalogItem(diploma.getOlympiad(), false);
            if (null == fisOlympiad) {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad", diploma.getSeriaAndNumber());
            }
            else
            {
                fisDocument.setOlympicID(fisOlympiad.getFisID());
            }

            if(diploma.getTrainingClass() != null)
            {
                fisDocument.setClassNumber(diploma.getTrainingClass());
            }
            else
            {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad-class", diploma.getSeriaAndNumber());
            }

            EnrFisCatalogItem fisSubject = olympiadSubjectConverter.getCatalogItem(diploma.getSubject(), false);
            if (null == fisSubject) {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad-profile", diploma.getSeriaAndNumber());
            } else {
                fisDocument.setProfileID(fisSubject.getFisID());
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            if (diploma.getOlympiadProfileDiscipline() != null)
            {
                EnrFisCatalogItem profileDisc = profileDisciplineConverter.getCatalogItem(diploma.getOlympiadProfileDiscipline(), false);
                if (profileDisc != null)
                    fisDocument.setOlympicSubjectID(profileDisc.getFisID());
                else
                {
                    warn(diploma.getPerson().getFullFio(), "warn.applications.entrant.no-olympiad-profile-discipline", diploma.getSeriaAndNumber());
                }
            }

            if (diploma.getStateExamSubject() != null)
            {
                EnrFisCatalogItem examSubject = stateExamConverter.getCatalogItem(diploma.getStateExamSubject(), false);
                if (examSubject != null)
                    fisDocument.setEgeSubjectID(examSubject.getFisID());
                else
                {
                    warn(diploma.getPerson().getFullFio(), "warn.applications.entrant.no-olympiad-exam-subject", diploma.getSeriaAndNumber());
                }
            }


            return fisDocument;
        }

        protected TOlympicTotalDocument getOlympicTotalDocument(EnrEntrantBaseDocument diplomaDoc, boolean original, EnrEntrantRequest request)
        {
            EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) diplomaDoc.getDocRelation().getDocument();

            if (StringUtils.length(diploma.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", diploma.getDisplayableTitle(), 10);
            }

            TOlympicTotalDocument fisDocument = new TOlympicTotalDocument();
            fisDocument.setUID(proofDocumentUid(diplomaDoc));
            fisDocument.setDocumentSeries(diploma.getSeria());
            fisDocument.setDocumentNumber(diploma.getNumber());
            fisDocument.setDiplomaTypeID(EnrOlympiadHonourCodes.WINNER.equals(diploma.getHonour().getCode()) ? 1 : 2);
            if(diploma.getTrainingClass() != null)
            {
                fisDocument.setClassNumber(diploma.getTrainingClass());
            }
            else
            {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad-class", diploma.getSeriaAndNumber());
            }

            EnrFisCatalogItem fisOlympiad = olympiadConverter.getCatalogItem(diploma.getOlympiad(), false);
            if (null == fisOlympiad) {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad", diploma.getSeriaAndNumber());
            }
            else
            {
                fisDocument.setOlympicID(fisOlympiad.getFisID());
            }


            fisDocument.setSubjects(new TOlympicTotalDocument.Subjects());

            EnrFisCatalogItem fisSubject = olympiadSubjectConverter.getCatalogItem(diploma.getSubject(), false);
            if (null == fisSubject) {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad-profile", diploma.getSeriaAndNumber());
            } else {
                fisDocument.getSubjects().setSubjectID(fisSubject.getFisID());
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            return fisDocument;
        }

        protected TUkraineOlympic getUkraineOlympicDocument(EnrEntrantBaseDocument diplomaDoc, boolean original, EnrEntrantRequest request)
        {
            EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) diplomaDoc.getDocRelation().getDocument();

            TUkraineOlympic fisDocument = new TUkraineOlympic();
            fisDocument.setUID(proofDocumentUid(diplomaDoc));

            if (StringUtils.length(diploma.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", diploma.getDisplayableTitle(), 10);
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            fisDocument.setDocumentSeries(diploma.getSeria());

            if(!StringUtils.isEmpty(diploma.getNumber()))
            {
                fisDocument.setDocumentNumber(diploma.getNumber());
            }
            else
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-number", diploma.getDisplayableTitle());
            }

            fisDocument.setDiplomaTypeID(EnrOlympiadHonourCodes.WINNER.equals(diploma.getHonour().getCode()) ? 1 : 2);

            fisDocument.setOlympicName(diploma.getOlympiad().getTitleWithYear());

            if(diploma.getSubject() != null)
            {
                fisDocument.setOlympicProfile(diploma.getSubject().getTitle());
            }
            else
            {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad-profile", diploma.getSeriaAndNumber());
            }

            if(diploma.getSettlement() != null)
            {
                fisDocument.setOlympicPlace(diploma.getSettlement().getFullTitle());
            }

            return fisDocument;
        }

        protected TInternationalOlympic getInternationalOlympicDocument(EnrEntrantBaseDocument diplomaDoc, boolean original, EnrEntrantRequest request)
        {
            EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) diplomaDoc.getDocRelation().getDocument();

            TInternationalOlympic fisDocument = new TInternationalOlympic();
            fisDocument.setUID(proofDocumentUid(diplomaDoc));

            if (StringUtils.length(diploma.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", diploma.getDisplayableTitle(), 10);
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            fisDocument.setDocumentSeries(diploma.getSeria());

            if(!StringUtils.isEmpty(diploma.getNumber()))
            {
                fisDocument.setDocumentNumber(diploma.getNumber());
            }
            else
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-number", diploma.getDisplayableTitle());
            }

            if(diploma.getCombinedTeam() != null)
            {
                fisDocument.setCountryID(diploma.getCombinedTeam() ? 1L : 209L);
            }
            else
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-olympiad-combined-team", diploma.getDisplayableTitle());
            }

            fisDocument.setOlympicName(diploma.getOlympiad().getTitleWithYear());

            if(diploma.getSubject() != null)
            {
                fisDocument.setOlympicProfile(diploma.getSubject().getTitle());
            }
            else
            {
                error(diploma.getPerson().getFullFio(), "error.applications.entrant.no-olympiad-profile", diploma.getSeriaAndNumber());
            }

            if(diploma.getSettlement() != null)
            {
                fisDocument.setOlympicPlace(diploma.getSettlement().getFullTitle());
            }

            return fisDocument;
        }

        protected TOrphanDocument getOrphanDocument(EnrEntrantBaseDocument document, boolean original, EnrEntrantRequest request)
        {
            TOrphanDocument fisDocument = new TOrphanDocument();

            fisDocument.setUID(proofDocumentUid(document, false));

            if (StringUtils.length(document.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", document.getDisplayableTitle(), 10);
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            EnrFisCatalogItem fisSubject = orphanConverter.getCatalogItem(document.getDocRelation().getDocument().getDocumentCategory(), false);
            if (null == fisSubject) {
                error(document.getEntrant().getFullFio(), "error.applications.entrant.no-doc-category", document.getDisplayableTitle());
            } else {
                fisDocument.setOrphanCategoryID(fisSubject.getFisID());
            }

            fisDocument.setDocumentName(document.getDocumentType().getTitle());
            fisDocument.setDocumentSeries(document.getSeria());
            fisDocument.setDocumentNumber(document.getNumber());

            if(document.getIssuanceDate() == null)
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-date", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentDate(xmlDate(document.getIssuanceDate()));
            }

            if(StringUtils.isEmpty(document.getIssuancePlace()))
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-place", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentOrganization(document.getIssuancePlace());
            }

            return fisDocument;
        }

        protected TVeteranDocument getVeteranDocument(EnrEntrantBaseDocument document, boolean original, EnrEntrantRequest request)
        {
            TVeteranDocument fisDocument = new TVeteranDocument();

            fisDocument.setUID(proofDocumentUid(document, false));

            if (StringUtils.length(document.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", document.getDisplayableTitle(), 10);
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            EnrFisCatalogItem fisSubject = veteranConverter.getCatalogItem(document.getDocRelation().getDocument().getDocumentCategory(), false);
            if (null == fisSubject) {
                error(document.getEntrant().getFullFio(), "error.applications.entrant.no-doc-category", document.getDisplayableTitle());
            } else {
                fisDocument.setVeteranCategoryID(fisSubject.getFisID());
            }

            fisDocument.setDocumentName(document.getDocumentType().getTitle());
            fisDocument.setDocumentSeries(document.getSeria());
            fisDocument.setDocumentNumber(document.getNumber());

            if(document.getIssuanceDate() == null)
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-date", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentDate(xmlDate(document.getIssuanceDate()));
            }

            if(StringUtils.isEmpty(document.getIssuancePlace()))
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-place", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentOrganization(document.getIssuancePlace());
            }

            return fisDocument;
        }

        protected TSportDocument getSportDocument(EnrEntrantBaseDocument document, boolean original, EnrEntrantRequest request)
        {
            TSportDocument fisDocument = new TSportDocument();

            fisDocument.setUID(proofDocumentUid(document, false));

            if (StringUtils.length(document.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", document.getDisplayableTitle(), 10);
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            EnrFisCatalogItem fisSubject = sportsConverter.getCatalogItem(document.getDocRelation().getDocument().getDocumentCategory(), false);
            if (null == fisSubject) {
                error(document.getEntrant().getFullFio(), "error.applications.entrant.no-doc-category", document.getDisplayableTitle());
            } else {
                fisDocument.setSportCategoryID(fisSubject.getFisID());
            }

            fisDocument.setDocumentName(document.getDocumentType().getTitle());
            fisDocument.setDocumentSeries(document.getSeria());
            fisDocument.setDocumentNumber(document.getNumber());

            if(document.getIssuanceDate() == null)
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-date", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentDate(xmlDate(document.getIssuanceDate()));
            }

            if(StringUtils.isEmpty(document.getIssuancePlace()))
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-place", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentOrganization(document.getIssuancePlace());
            }

            fisDocument.setAdditionalInfo(document.getDescription());
            return fisDocument;
        }

        protected TCompatriotDocument getCompatriotDocument(EnrEntrantBaseDocument document, boolean original, EnrEntrantRequest request)
        {
            TCompatriotDocument fisDocument = new TCompatriotDocument();

            fisDocument.setUID(proofDocumentUid(document, true));

            if (StringUtils.length(document.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", document.getDisplayableTitle(), 10);
            }

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            EnrFisCatalogItem fisSubject = compatriotConverter.getCatalogItem(document.getDocRelation().getDocument().getDocumentCategory(), false);
            if (null == fisSubject) {
                error(document.getEntrant().getFullFio(), "error.applications.entrant.no-doc-category", document.getDisplayableTitle());
            } else {
                fisDocument.setCompariotCategoryID(fisSubject.getFisID());
            }

            fisDocument.setDocumentName(document.getDocumentType().getTitle());
            fisDocument.setDocumentSeries(document.getSeria());
            fisDocument.setDocumentNumber(document.getNumber());

            if(document.getIssuanceDate() == null)
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-date", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentDate(xmlDate(document.getIssuanceDate()));
            }

            if(StringUtils.isEmpty(document.getIssuancePlace()))
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-place", document.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentOrganization(document.getIssuancePlace());
            }

            return fisDocument;
        }


        protected TCustomDocument getCustomDocument(IEnrEntrantBenefitProofDocument proofDocument, boolean original, EnrEntrantRequest request)
        {
            TCustomDocument fisDocument = new TCustomDocument();

            fisDocument.setUID(proofDocumentUid(proofDocument, true));

            if (original) {
                fisDocument.setOriginalReceivedDate(xmlDate(request.getRegDate()));
            }

            if (StringUtils.length(proofDocument.getSeria()) > 10 ) {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.too-long-document-seria", proofDocument.getDisplayableTitle(), 10);
            }

            fisDocument.setDocumentSeries(proofDocument.getSeria());
            fisDocument.setDocumentNumber(proofDocument.getNumber());

            if(proofDocument.getIssuanceDate() == null)
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-date", proofDocument.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentDate(xmlDate(proofDocument.getIssuanceDate()));
            }

            if(StringUtils.isEmpty(proofDocument.getIssuancePlace()))
            {
                error(request.getEntrant().getFullFio(), "error.applications.entrant.no-doc-issuance-place", proofDocument.getDisplayableTitle());
            }
            else
            {
                fisDocument.setDocumentOrganization(proofDocument.getIssuancePlace());
            }

            fisDocument.setDocumentName(proofDocument.getDocumentType().getTitle());
            fisDocument.setAdditionalInfo(proofDocument.getDescription());
            return fisDocument;
        }

        protected boolean isSchoolCertificateDocument(PersonEduDocument document) {
            return EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE.equals(document.getEduLevel().getCode());
        }

        protected boolean isSchoolCertificateBasicDocument(PersonEduDocument document) {
            return EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE.equals(document.getEduLevel().getCode());
        }

        protected boolean isHighEduDiplomaDocument(PersonEduDocument document) {
            if (EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT.equals(document.getEduLevel().getCode())) return true;
            if (EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA.equals(document.getEduLevel().getCode())) return true;
            return false;
        }

        protected boolean isPostgraduateDocument(PersonEduDocument document) {
            return EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII.equals(document.getEduLevel().getCode());
        }


        protected boolean isMiddleEduDiplomaDocument(PersonEduDocument document) {
            return EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE.equals(document.getEduLevel().getCode()) && !EduDocumentKindCodes.DIPLOM_NPO.equals(document.getEduDocumentKind().getCode());
        }

        protected boolean isBasicDiplomaDocument(PersonEduDocument document) {
            return EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE.equals(document.getEduLevel().getCode()) && EduDocumentKindCodes.DIPLOM_NPO.equals(document.getEduDocumentKind().getCode());
        }

        protected void error(String fullFio, String key, Object... argument) {
            String errorMessage = EnrFisSyncSessionManager.instance().getProperty(key, argument);
            fisSession.error(PREFIX, fullFio + ": " + StringUtils.uncapitalize(errorMessage) + ".");
            errorFlag.setValue(errorMessage);
        }


        protected void warn(String fullFio, String key, Object... argument) {
            String warnMessage = EnrFisSyncSessionManager.instance().getProperty(key, argument);
            fisSession.warn(PREFIX, fullFio + ": " + StringUtils.uncapitalize(warnMessage) + ".");
        }
    }

    protected void status(EnrFisSyncSession fisSession, String stageName, int progress) {
        super.status(fisSession, stageName, progress);
    }
}
