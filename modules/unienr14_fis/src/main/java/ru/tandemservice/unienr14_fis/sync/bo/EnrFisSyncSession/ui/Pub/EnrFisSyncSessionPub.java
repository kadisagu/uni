package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.EnrFisSyncSessionPackageDSHandler;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisSyncSessionPub extends BusinessComponentManager {

    public static final String ENR_FIS_PACKAGE_DS = "enrFisPackageDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(ENR_FIS_PACKAGE_DS, this.enrFisPackageDS()).handler(this.enrFisPackageDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint enrFisPackageDS() {
        return this.columnListExtPointBuilder(ENR_FIS_PACKAGE_DS)
        .addColumn(textColumn("title", EnrFisSyncPackage.title()).clickable(true).create())
        .addColumn(textColumn("comment", EnrFisSyncPackage.comment()).create())
        .addColumn(textColumn("stateTitle", EnrFisSyncPackage.stateTitle()).create())
        .addColumn(textColumn("iolog", "iolog").formatter(NewLineFormatter.NOBR_IN_LINES).create())
        .addColumn(actionColumn("fileName", EnrFisSyncPackage.fileName(), "onClickDownloadPackage").create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrFisPackageDSHandler() {
        return new EnrFisSyncSessionPackageDSHandler(this.getName());
    }
}
