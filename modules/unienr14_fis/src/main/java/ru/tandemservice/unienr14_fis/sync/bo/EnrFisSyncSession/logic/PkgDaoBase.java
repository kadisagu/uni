package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import enr14fis.schema.pkg.send.req.PackageData;
import enr14fis.schema.pkg.send.req.Root;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;
import java.util.function.Function;

/**
 * @author vdanilov
 */
public class PkgDaoBase extends UniBaseDao
{
    private static final Map<Long, Status> statusMap = new HashMap<>();

    public static IEnrFisSyncSessionFillStatus getFillStatus(Long fisSessionId) {
        synchronized (statusMap) {
            return statusMap.get(fisSessionId);
        }
    }

    public static void doInPkgDao(Long fisSessionId, Runnable runnable)
    {
        synchronized (statusMap) {
            if (null != statusMap.get(fisSessionId)) {
                throw new IllegalStateException();
            }
            statusMap.put(fisSessionId, newFillStatus(fisSessionId));
        }
        try {
            runnable.run();
        } finally {
            synchronized (statusMap) {
                statusMap.remove(fisSessionId);
            }
        }
    }

    /** @see PkgDaoBase#competitionGroupUid(ru.tandemservice.unienr14.competition.entity.EnrCompetition) */
    public static String competitionGroupTitle(EnrCompetition competition)
    {
        // в соответствии с ключом КГ ФИС

        EnrProgramSetOrgUnit psOu = competition.getProgramSetOrgUnit();

        return psOu.getProgramSet().getTitle() + " (" + UniStringUtils.joinWithSeparator(", ",
            psOu.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle(),
            competition.getFisUidPostfix()
        ) + ")";
    }
    

    /**
     * http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163062&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163062
     * http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163095&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163095
     */
    public static XMLGregorianCalendar xmlDateTime(final Date date, Function<Date, XMLGregorianCalendar> dateTimeFunction) {

        return date != null ? dateTimeFunction.apply(date) : null;
    }

    /**
     * @return xs:date, example: "2002-09-24"
     */
    public static XMLGregorianCalendar xmlDate(final Date date) {
        if (date == null) {
            return null;
        } else {
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            return dataTypeFactory.newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DATE), DatatypeConstants.FIELD_UNDEFINED);
        }
    }

    protected void status(EnrFisSyncSession fisSession, String stageName, int progress) {
        synchronized (statusMap) {
            Status status = statusMap.get(fisSession.getId());
            if (null == status) { return; }
            status.setStageName(stageName);
            status.setProgress(progress);
        }
    }

    /** @return PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.Items.CompetitiveGroupItem.UID = (uid(compGroup)+'.it-'+admissionVolumeItemId) */
    protected static String admissionVolumeGroupItemUid(EnrCompetition competition) {
        return _uid(competitionGroupUidBase(competition) + ".it", competition.getProgramSetOrgUnit().getProgramSet().getId(), trimTitle(competition), "Объем приема по данному конкурсу");
    }


    /** @return compGroup.enrollmentCampaign.id.uid+'.'+compGroup.id.uid */
    protected static String uid(final EnrExamVariant groupExam, EnrCompetition competition) {
        return _uid(competitionGroupUidBase(competition) + "." + groupExam.getPriority(), competition.getProgramSetOrgUnit().getProgramSet().getId(), trimTitle(competition), "Вступительное испытание «" + trimTitle(groupExam) + "» по данному конкурсу");
    }

    protected static String competitionGroupUid(EnrCompetition competition)
    {
        // ключ КГ ФИС: набор ОП + подразделение + бюджет/контракт + СОО/ПО (если есть деление)

        EnrProgramSetOrgUnit psOu = competition.getProgramSetOrgUnit();
        EnrProgramSetBase ps = psOu.getProgramSet();

        return _uid(competitionGroupUidBase(competition), ps.getId(), trimTitle(ps), "КГ ФИС для филиала «" + trimTitle(psOu.getOrgUnit().getDepartmentTitle()) + "», вид возм. затрат «" + competition.getType().getCompensationType().getShortTitle());
    }

    /** @return uid(entrantRequest.entrant)+"/"+entrantRequest.id */
    protected static String uid(final EnrEntrantRequest entrantRequest) {
        return _uid(getUidPrefix(entrantRequest), entrantRequest.getId(), trimTitle(entrantRequest), null);
    }

    /** @return personEduDocument.id.uid */
    protected static String uid(final PersonEduDocument eduDocument, final EnrEntrant entrant) {
        return _uid(getUidPrefix(eduDocument) + "." + getUidPrefix(entrant), entrant.getId(), trimTitle(eduDocument), null);
    }

    /** @return identityCard.id.uid */
    protected static String uid(final IdentityCard iCard, final EnrEntrant entrant) {
        return _uid(getUidPrefix(iCard) + "." + getUidPrefix(entrant), entrant.getId(), trimTitle(iCard.getTitle()), null);
    }

    /** @return todo comment it */
    protected static String benefitUid(final EnrRequestedCompetition requestedCompetition) {
        return uid(requestedCompetition, "b");
    }

    /** @return todo comment it */
    protected static String preferenceUid(final EnrRequestedCompetition requestedCompetition) {
        return uid(requestedCompetition, "p");
    }

    protected static String proofDocumentUid(final IEnrEntrantBenefitProofDocument document) {
        return proofDocumentUid(document, false);
    }

    protected static String proofDocumentUid(final IEnrEntrantBenefitProofDocument document, boolean customDocument) {
        return uid(document, customDocument ? "cd" : "pd");
    }

    protected static String attachableUid(final IEnrEntrantRequestAttachable attachable) {
        return uid(attachable, "cd");
    }

    protected static String uid(final EnrEntrantAchievement achievement, EnrEntrantRequest request) {
        return _uid(getUidPrefix(request) + "." + getUidPrefix(achievement), request.getId(), achievement.getType().getTitle(), null);
    }

    protected static String uid(final EnrEntrantAchievementType achievement) {
        return uid(achievement, "iat");
    }

    protected static String uid(final EnrCompetition competition, final EnrFisSettingsStateExamMinMarkNew minMark) {

        return _uid(competitionGroupUidBase(competition) + "." + getUidPrefix(minMark),
                competition.getProgramSetOrgUnit().getProgramSet().getId(), trimTitle(competition.getProgramSetOrgUnit().getProgramSet().getTitle()),
                "Мин. балл ЕГЭ: " + minMark.getSubject().getTitle() + ", " + minMark.getBenefitType().getTitle() + ", " + minMark.getMinMark());
    }

    protected static String uid(final EnrChosenEntranceExam exam) {
        return uid(exam, "e");
    }

    protected static String uid(final EnrEntrantStateExamResult result) {
        return uid(result, "s");
    }

    protected static String uid(final EnrOrder order) {
        return uid(order, "o");
    }

    /** @return entrant.enrollmentCampaign.id.uid+'.'+entrant.id.uid */
    protected static String uid(final EnrEntrant entrant) {
        return _uid(getUidPrefix(entrant), entrant.getId(), entrant.getFullFio(), null);
    }

    /** @return enrollmentCampaign.id.uid */
    protected static String uid(final EnrEnrollmentCampaign enrollmentCampaign) {
        return _uid(getUidPrefix(enrollmentCampaign), enrollmentCampaign.getId(), trimTitle(enrollmentCampaign), null);
    }

    /** @return PackageData.AdmissionInfo.AdmissionVolume.Item.UID = (uid(enrollmentCampaign) +'.'+(fisEduLvlID+"."+fisDirectionID+"."+course).uid) */
    protected String admissionVolumeItemUid(final EnrEnrollmentCampaign enrollmentCampaign, final EnrFisCatalogItem fisEduLvl, final EnrFisCatalogItem fisDirection, EduProgramSubject programSubject) {
        return _uid(getUidPrefix(enrollmentCampaign) + "." + fisEduLvl.getFisID() + "." + fisDirection.getFisID(), programSubject.getId(), programSubject.getTitleWithCode(), "Объем приема по данному направлению, уровень образования ФИС «" + fisEduLvl.getTitle() + "», направление ФИС «" + fisDirection.getTitle() + "»");
    }

    /** @return 'ta-'+targetAdmissionKind.code.uid */
    protected static String uid(final TopOrgUnit topOrgUnit, final EnrTargetAdmissionKind targetAdmissionKind) {
        return _uid("ta-" + (targetAdmissionKind == null ? "all" : targetAdmissionKind.getCode()), topOrgUnit.getId(), trimTitle(topOrgUnit), targetAdmissionKind == null ? "Вид ЦП для передачи в ФИС" : "Вид ЦП: " + trimTitle(targetAdmissionKind));
    }

    /** @return enrCompetition.id.uid+'.'+eduProgramProf.id.uid */
    protected static String uid(final EnrCompetition competition, final EduProgramProf eduProgramProf) {
        return _uid(competitionGroupUidBase(competition) + "." + getUidPrefix(eduProgramProf), competition.getProgramSetOrgUnit().getProgramSet().getId(), trimTitle(competition), "Образовательная программа «" + eduProgramProf.getTitleWithCodeAndConditions() + "» для данного конкурса");
    }

    private static String competitionGroupUidBase(EnrCompetition competition)
    {
        // ключ КГ ФИС: набор ОП + подразделение + бюджет/контракт + постфикс деления на СОО/ПО/ВО итп

        EnrProgramSetOrgUnit psOu = competition.getProgramSetOrgUnit();
        boolean budget = competition.getType().getCompensationType().isBudget();

        final StringBuilder keyBuilder = new StringBuilder();
        keyBuilder.append(getUidPrefix(psOu));
        keyBuilder.append(budget ? ".b" : ".c");

        if (competition.getProgramSetOrgUnit().getProgramSet().getEnrollmentCampaign().isLessOrEqualThan2014()) {
            boolean isDividedByEduLevel = budget ? psOu.getProgramSet().isMinisterialDividedByEduLevel() : psOu.getProgramSet().isContractDividedByEduLevel();
            boolean prof = EnrEduLevelRequirementCodes.PO.equals(competition.getEduLevelRequirement().getCode());  // те, кто "нет", идут в СОО, в частности, без ВИ
            if (!isDividedByEduLevel) {
                keyBuilder.append(".").append("n");
            } else {
                keyBuilder.append(".").append(prof ? "p" : "s");
            }
        } else {
            String fisUidPostfix = competition.getFisUidPostfix();
            if (fisUidPostfix == null) {
                throw new ApplicationException(EnrFisSyncSessionManager.instance().getProperty("error.admission.competition-with-no-postfix"));
            }
            keyBuilder.append(".").append(fisUidPostfix);
        }

        return keyBuilder.toString();
    }

    private static String getUidPrefix(IEntity entity) { return Long.toString(entity.getId(), 32); }

    private static String trimTitle(String title) { return StringLimitFormatter.INSTANCE.format(title); }

    protected static String trimTitle(ITitled entity) { return trimTitle(entity.getTitle()); }

    private static String _uid(final String data, Long relatedEntityId, String relatedEntityTitle, String comment) {
        return EnrFisSettingsManager.instance().uidRegistry().doGetUid(data, relatedEntityId, relatedEntityTitle, comment);
    }

    private static String uid(IFisUidByIdOwner entity, String postfix) {
        return "UID_"+Long.toHexString(entity.getId()) + "_" + postfix;
    }

    protected Root newRoot(final EnrOrgUnit enrOrgUnit)
    {
        final Root root = new Root();

        // auth data
        root.setAuthData(new Root.AuthData());
        root.getAuthData().setLogin(enrOrgUnit.getLoginSafe());
        root.getAuthData().setPass(enrOrgUnit.getPasswordSafe());
        root.getAuthData().setInstitutionID(enrOrgUnit.getFisUid());

        // package data
        root.setPackageData(new PackageData());
        return root;
    }

    protected static final DatatypeFactory dataTypeFactory;
    static {
        try {
            dataTypeFactory = DatatypeFactory.newInstance();
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected void flush(final EnrFisSyncSession fisSession) {
        final Session s = getSession();
        s.flush();
        s.clear();
        s.update(fisSession);
    }

    private static Status newFillStatus(Long fisSessionId) {
        return new Status();
    }

    private static class Status implements IEnrFisSyncSessionFillStatus {
        String stageName;
        int progress;

        @Override
        public String getStageName() {
            return this.stageName;
        }
        public void setStageName(String stageName) {
            this.stageName = stageName;
        }

        @Override
        public int getProgress() {
            return this.progress;
        }
        public void setProgress(int progress) {
            this.progress = progress;
        }
    }
}
