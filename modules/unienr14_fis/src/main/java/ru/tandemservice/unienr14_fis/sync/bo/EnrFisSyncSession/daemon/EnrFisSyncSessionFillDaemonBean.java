package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.daemon;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.DebugBlock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EnrFisSyncSessionFillDaemonBean extends UniBaseDao implements IEnrFisSyncSessionFillDaemonBean
{
    public static final int ITERATIONS_FILL = 2;
    public static final boolean active = true;

    public static final SyncDaemon DAEMON = new SyncDaemon(EnrFisSyncSessionFillDaemonBean.class.getName(), 15) {
        @Override protected void main() {
            if (!active) { return; }

            final IEnrFisSyncSessionFillDaemonBean dao = IEnrFisSyncSessionFillDaemonBean.instance.get();

            // сначала пробуем заполнить сессию пакетами
            Debug.begin("fill-pkg");
            try {
                int iterations = ITERATIONS_FILL;
                while (iterations --> 0) {
                    final Long unfilledSessionId = dao.getNextUnfilledSessionId();
                    if (null == unfilledSessionId) { break; }
                    try {
                        dao.doFillSession(unfilledSessionId);
                    } catch (final Throwable t) {
                        logger.error(t.getMessage(), t);
                        dao.doError(unfilledSessionId, t);
                    }
                }
            } catch (final Exception t) {
                logger.error(t.getMessage(), t);
                Debug.exception(t.getMessage(), t);
            } finally {
                Debug.end();
            }
        }
    };

    @Override
    protected void initDao() {
        DAEMON.registerWakeUpListener4Class(EnrFisSyncSession.class);
    }

    ///////////////////////////////////////////////////////

    @Override
    public void doError(final Long id, final Throwable t) {
        EnrFisSyncSession fisSession = get(EnrFisSyncSession.class, id);
        fisSession = refresh(fisSession);
        if (t instanceof ApplicationException) {
            fisSession.error(EnrFisSyncSessionManager.instance().getProperty("title.pkggen"), t.getMessage());
        } else {
            fisSession.error(EnrFisSyncSessionManager.instance().getProperty("title.pkggen"), t.getMessage(), t);
        }
        fisSession.setArchiveDate(new Date());
        saveOrUpdate(fisSession);
        getSession().flush();
    }

    @Override
    public Long getNextUnfilledSessionId() {
        return new DQLSelectBuilder()
        .fromEntity(EnrFisSyncSession.class, "s")
        .column(property("s.id"))
        .order(property(EnrFisSyncSession.creationDate().fromAlias("s")))
        .order(property(EnrFisSyncSession.id().fromAlias("s")))
        .where(isNull(property(EnrFisSyncSession.archiveDate().fromAlias("s"))))
        .where(isNull(property(EnrFisSyncSession.fillDate().fromAlias("s"))))
        .createStatement(getSession()).setMaxResults(1).uniqueResult();
    }

    @Override
    public void doFillSession(final Long unfilledSessionId) {

        getSession().clear();

        try (DebugBlock ignored = Debug.block("EnrFisSyncSessionFillDaemonBean.doFillSession")) {
            final EnrFisSyncSession fisSession = get(EnrFisSyncSession.class, unfilledSessionId);
            EnrFisSyncSessionManager.instance().dao().doFill(fisSession);
        } finally {
            if (EnrFisSyncSession.log.isInfoEnabled()) {
                EnrFisSyncSession.log.info(Debug.getRootSection().toFullString());
            }
        }
    }
}
