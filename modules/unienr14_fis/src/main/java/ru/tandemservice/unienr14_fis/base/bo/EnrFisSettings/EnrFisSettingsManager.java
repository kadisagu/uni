package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IUidRegistryDefinition;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.UidRegistryDefinition;
import org.tandemframework.shared.commonbase.base.entity.UidRegistry;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.EnrFisSettingsDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic.IEnrFisSettingsDao;

import java.util.Map;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisSettingsManager extends BusinessObjectManager {

    public static EnrFisSettingsManager instance() {
        return instance(EnrFisSettingsManager.class);
    }

    @Bean
    public IEnrFisSettingsDao dao() {
        return new EnrFisSettingsDao();
    }

    @Bean
    public IUidRegistryDefinition uidRegistry() {
        return new UidRegistryDefinition("enrFisUniRegistry") {
            @Override protected String generateNewUid(UidRegistry registry, Map<String, String> inverseMap, String key) {
                long value = (key.hashCode() & 0xFFFFF); // 5xF, т.к. потому 2^5 = 32 => число битов должно быть кратным 5
                String uid;
                while (inverseMap.containsKey(uid = Long.toString(value, 32))) { value++; }
                return uid;
            }
        };
    }

    public static final Long EXCLUSIVE_OPTION_WITHOUT_EXAMS = 1L;
    public static final Long EXCLUSIVE_OPTION_ONE_HUNDRED_POINTS = 2L;

    @Bean
    public ItemListExtPoint<DataWrapper> exclusiveOptionsItemList()
    {
        return itemList(DataWrapper.class).
                add(EXCLUSIVE_OPTION_WITHOUT_EXAMS.toString(), new DataWrapper(EXCLUSIVE_OPTION_WITHOUT_EXAMS, "ui.exclusive.option.withoutExams")).
                add(EXCLUSIVE_OPTION_ONE_HUNDRED_POINTS.toString(), new DataWrapper(EXCLUSIVE_OPTION_ONE_HUNDRED_POINTS, "ui.exclusive.option.oneHundredPoints")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler exclusiveOptionsDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addItemList(exclusiveOptionsItemList());
    }

}
