package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.daemon;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEnrFisSyncSessionFillDaemonBean {

    public static final SpringBeanCache<IEnrFisSyncSessionFillDaemonBean> instance = new SpringBeanCache<IEnrFisSyncSessionFillDaemonBean>(IEnrFisSyncSessionFillDaemonBean.class.getName());

    /**
     * @return
     */
    @Transactional
    Long getNextUnfilledSessionId();

    /**
     * @param unfilledSessionId
     */
    @Transactional
    void doFillSession(Long unfilledSessionId);

    /**
     * @param unfilledSessionId
     * @param t
     */
    @Transactional
    void doError(Long unfilledSessionId, Throwable t);

}
