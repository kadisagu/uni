/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.EcPeriodItemPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 6/25/14
 */
@Configuration
public class EnrFisSettingsEcPeriodItemPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
}