/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4EntrantAchievementType;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 09.06.2015
 */
@Configuration
public class EnrFisConverterConfig4EntrantAchievementType extends EnrFisConverterConfig
{
    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String BIND_ITEM_TITLE = "itemTitle";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        );
    }

    @Override
    protected IEnrFisConverterDao getConverterDao()
    {
        return EnrFisConverterManager.instance().entrantAchievementType();
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS()
    {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
                .addColumn(textColumn("title", EnrEntrantAchievementType.achievementKind().title()).create())
                .addColumn(textColumn("requestType", EnrEntrantAchievementType.achievementKind().requestType().shortTitle()).create())
                .addColumn(textColumn("achievementMark", EnrEntrantAchievementType.achievementMark()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
                .addColumn(blockColumn("value", "valueBlock").width("460px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4OlympiadEdit").hasBlockHeader(true).create())
                .create();
    }


    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler()
    {
        return super.fisConverterRecordDSHandler();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler()
    {
        return super.fisConverterValueDSHandler();
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrollmentCampaign = context.get(BIND_ENROLLMENT_CAMPAIGN);
        if (null == enrollmentCampaign) return Lists.newArrayList();

        final String itemTitle = context.get(BIND_ITEM_TITLE);

        return new ArrayList<>(Collections2.filter(
                entityList, input -> {
                    EnrEntrantAchievementType achievementType = (EnrEntrantAchievementType) input;
                    if (!achievementType.getEnrollmentCampaign().equals(enrollmentCampaign)) return false;
                    if (null != itemTitle && !StringUtils.containsIgnoreCase(achievementType.getAchievementKind().getTitle(), itemTitle)) return false;
                    return true;
                }
        ));
    }
}
