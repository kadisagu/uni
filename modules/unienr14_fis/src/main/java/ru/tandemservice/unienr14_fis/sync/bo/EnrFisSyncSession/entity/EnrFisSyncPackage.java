package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import java.util.Date;

import com.google.common.base.Strings;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.zip.GZipCompressionInterface;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisSyncPackageGen;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

/**
 * Пакет синхронизации ФИС
 *
 * Пакет взаимодействия с сервисом экспорта данных в ФИС
 */
public abstract class EnrFisSyncPackage extends EnrFisSyncPackageGen implements ITitled
{
    public static final GZipCompressionInterface zip = EnrFisSyncSession.zip;

    /** @return русскоязычное зазвание пакета */
    @Override
    @EntityDSLSupport
    public abstract String getTypeTitle();

    /** @retutn номер пакета (цифра - для обычных пакетов, или буква - для сервисных типа удаления всего) */
    @Override
    @EntityDSLSupport
    public abstract String getTypeNumber();

    @Override
    @EntityDSLSupport
    public String getTitle() {
        return getTypeTitle();
    }

    @Override
    @EntityDSLSupport
    public String getNextOperationThresholdDateAsString() {
        return EnrFisSyncSession.DATE_FORMATTER.format(this.getNextOperationThresholdDate());
    }

    @Override
    @EntityDSLSupport
    public Date getPackageSendDate() {
        EnrFisSyncPackageIOLog io = getPkgSendSuccess();
        return (null == io ? null : io.getOperationDate());
    }

    @Override
    @EntityDSLSupport
    public Date getPackageRecvDate() {
        EnrFisSyncPackageIOLog io = getPkgRecvSuccess();
        return (null == io ? null : io.getOperationDate());
    }

    @Override
    @EntityDSLSupport
    public String getStateTitle() {
        if (null != getPkgRecvSuccess()) { return "Получен ответ из ФИС"; }
        if (null != getPkgSendSuccess()) { return "Данные переданы в ФИС"; }
        return "";
    }

    public byte[] getXmlPackage() {
        return zip.decompress(this.getZipXmlPackage());
    }
    public void setXmlPackage(byte[] xmlPackage) {
        this.setZipXmlPackage(zip.compress(xmlPackage));
    }

    public String getXmlPackageString() {
        final byte[] xmlPackage = getXmlPackage();
        return (null == xmlPackage ? null : new String(xmlPackage));
    }
    public void setXmlPackageString(String xmlPackage) {
        this.setXmlPackage(xmlPackage.getBytes());
    }
    public String getFormattedXmlPackage() {
        String xmlPackage = getXmlPackageString();
        return Strings.isNullOrEmpty(xmlPackage) ? null : EnrFisServiceImpl.prettyFormat(xmlPackage);
    }

    /**
     * @return имя файла для пакета (уникальное с учетом типа пакета и даты генерации)
     * @warn здесь нельзя использовать русские буквы
     */
    @Override
    @EntityDSLSupport
    public String getFileName() {
        return getTypeNumber() +"-"+ Long.toString((getCreationDate().getTime()-getSession().getCreationDate().getTime()), 32)+".xml";
    }

}