package ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.gen.EnrFisCatalogItemGen;

/**
 * Запись справочника ФИС
 */
public class EnrFisCatalogItem extends EnrFisCatalogItemGen implements Comparable<EnrFisCatalogItem>
{
    // Коды справочника ФИС "Состояние ПК"
    public static final String FIS_CAMPAIGN_INFO_STATUS_NOT_STARTED = "0"; // Набор не начался
    public static final String FIS_CAMPAIGN_INFO_STATUS_IN_PROGRESS = "1"; // Идет набор
    public static final String FIS_CAMPAIGN_INFO_STATUS_FINISHED = "2"; // Завершена

    public Long getFisID() {
        return Long.valueOf(getFisItemCode());
    }

    @Override
    public int compareTo(EnrFisCatalogItem o) {
        int i;
        if (0 != (i = this.getFisCatalogCode().compareTo(o.getFisCatalogCode()))) { return i; }
        if (0 != (i = this.getFisItemCode().compareTo(o.getFisItemCode()))) { return i; }
        return Long.compare(this.getId(), o.getId());
    }

    @Override
    @EntityDSLSupport(parts = {P_TITLE, P_REMOVAL_DATE})
    public String getTitleWithRemovalDate()
    {
        return getTitle() + (getRemovalDate() == null ? "" : " (неактуален с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getRemovalDate()) + ")");
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name){
        return new EntityComboDataSourceHandler(name, EnrFisCatalogItem.class)
                .order(EnrFisCatalogItem.removalDate(), OrderDirection.asc)
                .order(EnrFisCatalogItem.title())
                .filter(EnrFisCatalogItem.title());
    }
}