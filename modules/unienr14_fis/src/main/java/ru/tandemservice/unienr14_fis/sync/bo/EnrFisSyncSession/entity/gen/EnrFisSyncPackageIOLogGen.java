package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог взаимодействия с сервером ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSyncPackageIOLogGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog";
    public static final String ENTITY_NAME = "enrFisSyncPackageIOLog";
    public static final int VERSION_HASH = 2036040907;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_OPERATION_DATE = "operationDate";
    public static final String P_OPERATION_URL = "operationUrl";
    public static final String P_ZIP_XML_REQUEST = "zipXmlRequest";
    public static final String P_ZIP_XML_RESPONSE = "zipXmlResponse";
    public static final String P_FIS_ERROR_TEXT = "fisErrorText";
    public static final String P_ERROR = "error";
    public static final String P_ZIP_ERROR_LOG = "zipErrorLog";
    public static final String P_DESCRIPTION = "description";
    public static final String P_OPERATION_DATE_AS_STRING = "operationDateAsString";
    public static final String P_TITLE = "title";

    private EnrFisSyncPackage _owner;     // Пакет
    private Date _operationDate;     // Время проведения операции
    private String _operationUrl;     // Адрес, через который проводилась операция
    private byte[] _zipXmlRequest;     // ZIP-XML: отправленные данные в ФИС
    private byte[] _zipXmlResponse;     // ZIP-XML: ответ из ФИС
    private String _fisErrorText;     // Сообщение об ошибке ФИС
    private boolean _error;     // Ошибка взаимодействия
    private byte[] _zipErrorLog;     // ZIP: полный лог ошибки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Пакет, в тамках которого происходит операция
     *
     * @return Пакет. Свойство не может быть null.
     */
    @NotNull
    public EnrFisSyncPackage getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Пакет. Свойство не может быть null.
     */
    public void setOwner(EnrFisSyncPackage owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * Отметка времени, когда произошла операция
     *
     * @return Время проведения операции. Свойство не может быть null.
     */
    @NotNull
    public Date getOperationDate()
    {
        return _operationDate;
    }

    /**
     * @param operationDate Время проведения операции. Свойство не может быть null.
     */
    public void setOperationDate(Date operationDate)
    {
        dirty(_operationDate, operationDate);
        _operationDate = operationDate;
    }

    /**
     * @return Адрес, через который проводилась операция. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOperationUrl()
    {
        return _operationUrl;
    }

    /**
     * @param operationUrl Адрес, через который проводилась операция. Свойство не может быть null.
     */
    public void setOperationUrl(String operationUrl)
    {
        dirty(_operationUrl, operationUrl);
        _operationUrl = operationUrl;
    }

    /**
     * @return ZIP-XML: отправленные данные в ФИС.
     */
    public byte[] getZipXmlRequest()
    {
        initLazyForGet("zipXmlRequest");
        return _zipXmlRequest;
    }

    /**
     * @param zipXmlRequest ZIP-XML: отправленные данные в ФИС.
     */
    public void setZipXmlRequest(byte[] zipXmlRequest)
    {
        initLazyForSet("zipXmlRequest");
        dirty(_zipXmlRequest, zipXmlRequest);
        _zipXmlRequest = zipXmlRequest;
    }

    /**
     * @return ZIP-XML: ответ из ФИС.
     */
    public byte[] getZipXmlResponse()
    {
        initLazyForGet("zipXmlResponse");
        return _zipXmlResponse;
    }

    /**
     * @param zipXmlResponse ZIP-XML: ответ из ФИС.
     */
    public void setZipXmlResponse(byte[] zipXmlResponse)
    {
        initLazyForSet("zipXmlResponse");
        dirty(_zipXmlResponse, zipXmlResponse);
        _zipXmlResponse = zipXmlResponse;
    }

    /**
     * Если в результате обработки пакета ФИС возвращает ошибку, в данное поле попадает ее текст.
     * При этом в поле zipErrorLog попадает stacktrace ошибки, поле error принимает значение true.
     *
     * @return Сообщение об ошибке ФИС.
     */
    @Length(max=1024)
    public String getFisErrorText()
    {
        return _fisErrorText;
    }

    /**
     * @param fisErrorText Сообщение об ошибке ФИС.
     */
    public void setFisErrorText(String fisErrorText)
    {
        dirty(_fisErrorText, fisErrorText);
        _fisErrorText = fisErrorText;
    }

    /**
     * Формула, вычисляется на основе поля zipErrorLog.
     *
     * @return Ошибка взаимодействия. Свойство не может быть null.
     *
     * Это формула "case when zipErrorLog is not null then true else false end".
     */
    // @NotNull
    public boolean isError()
    {
        return _error;
    }

    /**
     * @param error Ошибка взаимодействия. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setError(boolean error)
    {
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return ZIP: полный лог ошибки.
     */
    public byte[] getZipErrorLog()
    {
        initLazyForGet("zipErrorLog");
        return _zipErrorLog;
    }

    /**
     * @param zipErrorLog ZIP: полный лог ошибки.
     */
    public void setZipErrorLog(byte[] zipErrorLog)
    {
        initLazyForSet("zipErrorLog");
        dirty(_zipErrorLog, zipErrorLog);
        _zipErrorLog = zipErrorLog;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisSyncPackageIOLogGen)
        {
            setOwner(((EnrFisSyncPackageIOLog)another).getOwner());
            setOperationDate(((EnrFisSyncPackageIOLog)another).getOperationDate());
            setOperationUrl(((EnrFisSyncPackageIOLog)another).getOperationUrl());
            setZipXmlRequest(((EnrFisSyncPackageIOLog)another).getZipXmlRequest());
            setZipXmlResponse(((EnrFisSyncPackageIOLog)another).getZipXmlResponse());
            setFisErrorText(((EnrFisSyncPackageIOLog)another).getFisErrorText());
            setError(((EnrFisSyncPackageIOLog)another).isError());
            setZipErrorLog(((EnrFisSyncPackageIOLog)another).getZipErrorLog());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSyncPackageIOLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSyncPackageIOLog.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrFisSyncPackageIOLog is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "operationDate":
                    return obj.getOperationDate();
                case "operationUrl":
                    return obj.getOperationUrl();
                case "zipXmlRequest":
                    return obj.getZipXmlRequest();
                case "zipXmlResponse":
                    return obj.getZipXmlResponse();
                case "fisErrorText":
                    return obj.getFisErrorText();
                case "error":
                    return obj.isError();
                case "zipErrorLog":
                    return obj.getZipErrorLog();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((EnrFisSyncPackage) value);
                    return;
                case "operationDate":
                    obj.setOperationDate((Date) value);
                    return;
                case "operationUrl":
                    obj.setOperationUrl((String) value);
                    return;
                case "zipXmlRequest":
                    obj.setZipXmlRequest((byte[]) value);
                    return;
                case "zipXmlResponse":
                    obj.setZipXmlResponse((byte[]) value);
                    return;
                case "fisErrorText":
                    obj.setFisErrorText((String) value);
                    return;
                case "error":
                    obj.setError((Boolean) value);
                    return;
                case "zipErrorLog":
                    obj.setZipErrorLog((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "operationDate":
                        return true;
                case "operationUrl":
                        return true;
                case "zipXmlRequest":
                        return true;
                case "zipXmlResponse":
                        return true;
                case "fisErrorText":
                        return true;
                case "error":
                        return true;
                case "zipErrorLog":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "operationDate":
                    return true;
                case "operationUrl":
                    return true;
                case "zipXmlRequest":
                    return true;
                case "zipXmlResponse":
                    return true;
                case "fisErrorText":
                    return true;
                case "error":
                    return true;
                case "zipErrorLog":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return EnrFisSyncPackage.class;
                case "operationDate":
                    return Date.class;
                case "operationUrl":
                    return String.class;
                case "zipXmlRequest":
                    return byte[].class;
                case "zipXmlResponse":
                    return byte[].class;
                case "fisErrorText":
                    return String.class;
                case "error":
                    return Boolean.class;
                case "zipErrorLog":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSyncPackageIOLog> _dslPath = new Path<EnrFisSyncPackageIOLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSyncPackageIOLog");
    }
            

    /**
     * Пакет, в тамках которого происходит операция
     *
     * @return Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOwner()
     */
    public static EnrFisSyncPackage.Path<EnrFisSyncPackage> owner()
    {
        return _dslPath.owner();
    }

    /**
     * Отметка времени, когда произошла операция
     *
     * @return Время проведения операции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOperationDate()
     */
    public static PropertyPath<Date> operationDate()
    {
        return _dslPath.operationDate();
    }

    /**
     * @return Адрес, через который проводилась операция. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOperationUrl()
     */
    public static PropertyPath<String> operationUrl()
    {
        return _dslPath.operationUrl();
    }

    /**
     * @return ZIP-XML: отправленные данные в ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getZipXmlRequest()
     */
    public static PropertyPath<byte[]> zipXmlRequest()
    {
        return _dslPath.zipXmlRequest();
    }

    /**
     * @return ZIP-XML: ответ из ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getZipXmlResponse()
     */
    public static PropertyPath<byte[]> zipXmlResponse()
    {
        return _dslPath.zipXmlResponse();
    }

    /**
     * Если в результате обработки пакета ФИС возвращает ошибку, в данное поле попадает ее текст.
     * При этом в поле zipErrorLog попадает stacktrace ошибки, поле error принимает значение true.
     *
     * @return Сообщение об ошибке ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getFisErrorText()
     */
    public static PropertyPath<String> fisErrorText()
    {
        return _dslPath.fisErrorText();
    }

    /**
     * Формула, вычисляется на основе поля zipErrorLog.
     *
     * @return Ошибка взаимодействия. Свойство не может быть null.
     *
     * Это формула "case when zipErrorLog is not null then true else false end".
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#isError()
     */
    public static PropertyPath<Boolean> error()
    {
        return _dslPath.error();
    }

    /**
     * @return ZIP: полный лог ошибки.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getZipErrorLog()
     */
    public static PropertyPath<byte[]> zipErrorLog()
    {
        return _dslPath.zipErrorLog();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getDescription()
     */
    public static SupportedPropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOperationDateAsString()
     */
    public static SupportedPropertyPath<String> operationDateAsString()
    {
        return _dslPath.operationDateAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrFisSyncPackageIOLog> extends EntityPath<E>
    {
        private EnrFisSyncPackage.Path<EnrFisSyncPackage> _owner;
        private PropertyPath<Date> _operationDate;
        private PropertyPath<String> _operationUrl;
        private PropertyPath<byte[]> _zipXmlRequest;
        private PropertyPath<byte[]> _zipXmlResponse;
        private PropertyPath<String> _fisErrorText;
        private PropertyPath<Boolean> _error;
        private PropertyPath<byte[]> _zipErrorLog;
        private SupportedPropertyPath<String> _description;
        private SupportedPropertyPath<String> _operationDateAsString;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Пакет, в тамках которого происходит операция
     *
     * @return Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOwner()
     */
        public EnrFisSyncPackage.Path<EnrFisSyncPackage> owner()
        {
            if(_owner == null )
                _owner = new EnrFisSyncPackage.Path<EnrFisSyncPackage>(L_OWNER, this);
            return _owner;
        }

    /**
     * Отметка времени, когда произошла операция
     *
     * @return Время проведения операции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOperationDate()
     */
        public PropertyPath<Date> operationDate()
        {
            if(_operationDate == null )
                _operationDate = new PropertyPath<Date>(EnrFisSyncPackageIOLogGen.P_OPERATION_DATE, this);
            return _operationDate;
        }

    /**
     * @return Адрес, через который проводилась операция. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOperationUrl()
     */
        public PropertyPath<String> operationUrl()
        {
            if(_operationUrl == null )
                _operationUrl = new PropertyPath<String>(EnrFisSyncPackageIOLogGen.P_OPERATION_URL, this);
            return _operationUrl;
        }

    /**
     * @return ZIP-XML: отправленные данные в ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getZipXmlRequest()
     */
        public PropertyPath<byte[]> zipXmlRequest()
        {
            if(_zipXmlRequest == null )
                _zipXmlRequest = new PropertyPath<byte[]>(EnrFisSyncPackageIOLogGen.P_ZIP_XML_REQUEST, this);
            return _zipXmlRequest;
        }

    /**
     * @return ZIP-XML: ответ из ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getZipXmlResponse()
     */
        public PropertyPath<byte[]> zipXmlResponse()
        {
            if(_zipXmlResponse == null )
                _zipXmlResponse = new PropertyPath<byte[]>(EnrFisSyncPackageIOLogGen.P_ZIP_XML_RESPONSE, this);
            return _zipXmlResponse;
        }

    /**
     * Если в результате обработки пакета ФИС возвращает ошибку, в данное поле попадает ее текст.
     * При этом в поле zipErrorLog попадает stacktrace ошибки, поле error принимает значение true.
     *
     * @return Сообщение об ошибке ФИС.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getFisErrorText()
     */
        public PropertyPath<String> fisErrorText()
        {
            if(_fisErrorText == null )
                _fisErrorText = new PropertyPath<String>(EnrFisSyncPackageIOLogGen.P_FIS_ERROR_TEXT, this);
            return _fisErrorText;
        }

    /**
     * Формула, вычисляется на основе поля zipErrorLog.
     *
     * @return Ошибка взаимодействия. Свойство не может быть null.
     *
     * Это формула "case when zipErrorLog is not null then true else false end".
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#isError()
     */
        public PropertyPath<Boolean> error()
        {
            if(_error == null )
                _error = new PropertyPath<Boolean>(EnrFisSyncPackageIOLogGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return ZIP: полный лог ошибки.
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getZipErrorLog()
     */
        public PropertyPath<byte[]> zipErrorLog()
        {
            if(_zipErrorLog == null )
                _zipErrorLog = new PropertyPath<byte[]>(EnrFisSyncPackageIOLogGen.P_ZIP_ERROR_LOG, this);
            return _zipErrorLog;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getDescription()
     */
        public SupportedPropertyPath<String> description()
        {
            if(_description == null )
                _description = new SupportedPropertyPath<String>(EnrFisSyncPackageIOLogGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getOperationDateAsString()
     */
        public SupportedPropertyPath<String> operationDateAsString()
        {
            if(_operationDateAsString == null )
                _operationDateAsString = new SupportedPropertyPath<String>(EnrFisSyncPackageIOLogGen.P_OPERATION_DATE_AS_STRING, this);
            return _operationDateAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackageIOLog#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrFisSyncPackageIOLogGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrFisSyncPackageIOLog.class;
        }

        public String getEntityName()
        {
            return "enrFisSyncPackageIOLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDescription();

    public abstract String getOperationDateAsString();

    public abstract String getTitle();
}
