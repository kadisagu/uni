/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4DocumentCategorySportsDiploma;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base.EnrFisConverterDao4DocumentCategory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public class EnrFisConverterDao4DocumentCategorySportsDiploma extends EnrFisConverterDao4DocumentCategory
{
    @Override
    public String getCatalogCode()
    {
        return "43";
    }

    protected List<PersonDocumentCategory> getEnrDocumentCategory(String entrantDocTypeCode)
    {
        return super.getEnrDocumentCategory(PersonDocumentTypeCodes.SPORTS_DIPLOMA);
    }

    protected Class<? extends IEnrFisConv4DocumentCategory> getEnrFisConv4DocumentCategoryClass()
    {
        return EnrFisConv4DocumentCategorySportsDiploma.class;
    }

    @Override
    public void update(final Map<PersonDocumentCategory, EnrFisCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        List<EnrFisConv4DocumentCategorySportsDiploma> targetRecords = values.entrySet().stream()
                .filter(e -> null != e.getValue())
                .map(e -> new EnrFisConv4DocumentCategorySportsDiploma(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4DocumentCategorySportsDiploma.NaturalId, EnrFisConv4DocumentCategorySportsDiploma>()
        {
            @Override protected EnrFisConv4DocumentCategorySportsDiploma.NaturalId key(final EnrFisConv4DocumentCategorySportsDiploma source) { return (EnrFisConv4DocumentCategorySportsDiploma.NaturalId) source.getNaturalId(); }
            @Override protected EnrFisConv4DocumentCategorySportsDiploma buildRow(final EnrFisConv4DocumentCategorySportsDiploma source) { return new EnrFisConv4DocumentCategorySportsDiploma(source.getDocCategory(), source.getValue()); }
            @Override protected void fill(final EnrFisConv4DocumentCategorySportsDiploma target, final EnrFisConv4DocumentCategorySportsDiploma source) { target.update(source, false); }
            @Override protected void doDeleteRecord(final EnrFisConv4DocumentCategorySportsDiploma databaseRecord) { if (clearNulls) { super.doDeleteRecord(databaseRecord); } }
        }.merge(this.getList(EnrFisConv4DocumentCategorySportsDiploma.class), targetRecords);
    }
}