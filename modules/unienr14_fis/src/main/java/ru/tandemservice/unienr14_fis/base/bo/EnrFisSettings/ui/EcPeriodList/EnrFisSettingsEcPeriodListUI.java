package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.EcPeriodList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamRoom.EnrExamRoomManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.EcPeriodEdit.EnrFisSettingsEcPeriodEdit;

import java.util.*;

/**
 * @author vdanilov
 */
public class EnrFisSettingsEcPeriodListUI extends UIPresenter {

    public boolean _withoutFisCatalogs;
    private Map<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>> dataMap = Collections.emptyMap();
    private EnrFisSettingsECPeriodKey currentKey;
    private EnrFisSettingsECPeriodItem currentItem;

    @Override
    public void onComponentRefresh() {
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        refreshDataMap();
    }

    public void refreshDataMap()
    {
        getSupport().getSession().clear();

        LinkedHashMap<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>> dataMap = new LinkedHashMap<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>>();
        setDataMap(dataMap);

        EnrEnrollmentCampaign ec = getEnrollmentCampaign();
        if (null == ec) { return; }

        ICommonDAO dao = DataAccessServices.dao();

        // сначала разбираемся с ключами
        List<EnrFisSettingsECPeriodKey> keyList = dao.getList(EnrFisSettingsECPeriodKey.class, EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign(), ec);
        Collections.sort(keyList);

        for (EnrFisSettingsECPeriodKey key: keyList) {
            dataMap.put(key, new ArrayList<EnrFisSettingsECPeriodItem>(4));
        }

        // затем к каждому ключу добавляем элементы (поскольку в нашем несовершенном мире нет блокировок на чтение, даже если засунуть это в один dao, то может случиться npe)
        List<EnrFisSettingsECPeriodItem> itemList = dao.getList(EnrFisSettingsECPeriodItem.class, EnrFisSettingsECPeriodItem.key(), keyList);
        Collections.sort(itemList);

        for (EnrFisSettingsECPeriodItem item: itemList) {
            List<EnrFisSettingsECPeriodItem> list = dataMap.get(item.getKey());
            if (null != list) { list.add(item); }
        }
    }

    public void onSelectEnrollmentCampaign() {

        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        refreshDataMap();
    }

    public void onClickAddKeysFromDirections() {
        EnrEnrollmentCampaign ec = getEnrollmentCampaign();
        if (null == ec) {
            throw new ApplicationException("Чтобы заполнить настройку, выберите приемную кампанию.");
        }

        EnrFisSettingsManager.instance().dao().doCreateEnrFisSettingsECPeriodKeys(ec);
        getSupport().setRefreshScheduled(true);
    }

    public void onClickEdit() {
        getActivationBuilder().asRegionDialog(EnrFisSettingsEcPeriodEdit.class).parameter(PUBLISHER_ID, getSupport().getListenerParameterAsLong()).activate();
    }

    // getters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public boolean isNothingSelected()
    {
        return null == getEnrollmentCampaign();
    }

    public List<EnrFisSettingsECPeriodKey> getKeyList() { return new ArrayList<EnrFisSettingsECPeriodKey>(getDataMap().keySet()); }

    public List<EnrFisSettingsECPeriodItem> getItemList() { return getDataMap().get(getCurrentKey()); }

    // getters and setters
    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }

    public void setDataMap(Map<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>> dataMap) { this.dataMap = dataMap; }

    public Map<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>> getDataMap() { return this.dataMap; }

    public EnrFisSettingsECPeriodKey getCurrentKey() { return this.currentKey; }

    public void setCurrentKey(EnrFisSettingsECPeriodKey currentKey) { this.currentKey = currentKey; }

    public EnrFisSettingsECPeriodItem getCurrentItem() { return this.currentItem; }

    public void setCurrentItem(EnrFisSettingsECPeriodItem currentItem) { this.currentItem = currentItem; }

}
