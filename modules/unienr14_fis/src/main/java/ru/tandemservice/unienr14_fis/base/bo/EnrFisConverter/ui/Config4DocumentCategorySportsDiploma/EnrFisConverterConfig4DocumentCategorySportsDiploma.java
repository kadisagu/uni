/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategorySportsDiploma;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4DocumentCategory.EnrFisConverterConfig4DocumentCategory;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
@Configuration
public class EnrFisConverterConfig4DocumentCategorySportsDiploma extends EnrFisConverterConfig4DocumentCategory
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder().addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS()
    {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
                .addColumn(textColumn("fullTitle", IEnrExamSetElementValue.P_TITLE).create())
                .addColumn(blockColumn("value", "valueBlock").width("460px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("ui:editPermissionKey").hasBlockHeader(true).create())
                .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler()
    {
        return super.fisConverterRecordDSHandler();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler()
    {
        return super.fisConverterValueDSHandler();
    }

    @Override
    protected IEnrFisConverterDao getConverterDao()
    {
        return EnrFisConverterManager.instance().documentCategorySportsDiploma();
    }
}