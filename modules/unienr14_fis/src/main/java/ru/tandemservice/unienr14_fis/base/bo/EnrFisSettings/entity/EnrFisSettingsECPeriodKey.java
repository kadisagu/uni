package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.gen.EnrFisSettingsECPeriodKeyGen;

/**
 * Даты приемной кампании ФИС (ключ)
 */
public class EnrFisSettingsECPeriodKey extends EnrFisSettingsECPeriodKeyGen implements Comparable<EnrFisSettingsECPeriodKey>, ITitled
{
    public EnrFisSettingsECPeriodKey() {
    }

    public EnrFisSettingsECPeriodKey(EnrFisSettingsECPeriodKey source) {
        this.update(source, true);
    }

    @Override
    public int compareTo(EnrFisSettingsECPeriodKey o)
    {
        // если это одно и то же, то 0
        if (this.equals(o)) {
            return 0;
        }

        int result;

        // по подразделению
        if (0 != (result = EnrOrgUnit.ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR.compare(this.getEnrOrgUnit(), o.getEnrOrgUnit()))) {
            return result;
        }

        // по коду ФИС формы обучения
        if (0 != (result = NumberAsStringComparator.INSTANCE.compare(this.getEducationForm().getFisItemCode(), o.getEducationForm().getFisItemCode()))) {
            return result;
        }

        // по коду ФИС уровня образования
        if (0 != (result = NumberAsStringComparator.INSTANCE.compare(this.getEducationLevel().getFisItemCode(), o.getEducationLevel().getFisItemCode()))) {
            return result;
        }

        // по коду ФИС источника финансирования
        if (0 != (result = NumberAsStringComparator.INSTANCE.compare(this.getEducationSource().getFisItemCode(), o.getEducationSource().getFisItemCode()))) {
            return result;
        }

        // по id
        return this.getId().compareTo(o.getId());
    }

    public static String getSizeTitle(int sz) {
        if (sz <= 0) { return "Нет приема"; }
        if (1 == sz) { return "Прием без этапов"; }
        return CommonBaseStringUtil.numberWithPostfixCase(sz, "этап", "этапа", "этапов");
    }

    @Override
    @EntityDSLSupport
    public String getSizeTitle() {
        return getSizeTitle(getSize());
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getEnrOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return new StringBuilder()
        .append(getEnrOrgUnit().getDepartmentTitle()).append(", ")
        .append(getEducationForm().getTitle()).append(", ")
        .append(getEducationLevel().getTitle()).append(", ")
        .append(getEducationSource().getTitle())
        .toString();
    }
}