/* $Id: OrgUnitViewExt.java 22487 2012-04-04 13:16:00Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14_fis.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.EnrFisOrgUnitManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.ui.Tab.EnrFisOrgUnitTab;

@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String TAB_ENR_FIS_ORG_UNIT = "enr14FisOrgUnitTab";

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return this.tabPanelExtensionBuilder(this._orgUnitView.orgUnitTabPanelExtPoint())
        .addTab(
            componentTab(TAB_ENR_FIS_ORG_UNIT, EnrFisOrgUnitTab.class)
            .permissionKey("ui:secModel.orgUnit_viewEnr14FisTab")
            .parameters("mvel:['publisherId':presenter.orgUnit.id]")
            .visible("ognl:@"+OrgUnitViewExt.class.getName()+"@showEnrFisOrgUnitTab(presenter.orgUnit)")
        )
        .create();
    }

    public static boolean showEnrFisOrgUnitTab(final OrgUnit orgUnit) {
        return EnrFisOrgUnitManager.instance().dao().isEnrFisOrgUnit(orgUnit);
    }

}
