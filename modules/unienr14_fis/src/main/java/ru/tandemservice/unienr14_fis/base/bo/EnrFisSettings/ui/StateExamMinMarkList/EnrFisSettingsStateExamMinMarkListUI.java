/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.OrgUnitCredentialsList.EnrFisSettingsOrgUnitCredentialsList;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.ui.StateExamMinMarkAddEdit.EnrFisSettingsStateExamMinMarkAddEdit;

/**
 * @author nvankov
 * @since 7/9/14
 */
public class EnrFisSettingsStateExamMinMarkListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh() {
        getSettings().set(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(getEnrollmentCampaign() != null)
        {
            dataSource.put(EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID, getEnrollmentCampaign().getId());
        }
    }

    public boolean isNothingSelected()
    {
        return null == getEnrollmentCampaign();
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN);
    }

    public void onClickAdd()
    {
         _uiActivation.asRegionDialog(EnrFisSettingsStateExamMinMarkAddEdit.class)
                 .parameter(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign().getId())
                 .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EnrFisSettingsStateExamMinMarkAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter(EnrFisSettingsOrgUnitCredentialsList.BIND_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign().getId())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
