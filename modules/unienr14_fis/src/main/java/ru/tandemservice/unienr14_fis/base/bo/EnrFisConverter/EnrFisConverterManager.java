package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressType;
import org.tandemframework.shared.fias.base.entity.codes.AddressLevelCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.country.EnrFisConverterDao4Country;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.country.IEnrFisConverterDao4Country;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.diplomaType.EnrFisConverterDao4DiplomaType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.diplomaType.IEnrFisConverterDao4DiplomaType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.discipline.EnrFisConverterDao4ExamSetElement;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.discipline.IEnrFisConverterDao4ExamSetElement;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.EnrFisConverterDao4DocumentCategoryBelongingNation;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.EnrFisConverterDao4DocumentCategoryCombatVeterans;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.EnrFisConverterDao4DocumentCategoryOrphan;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.EnrFisConverterDao4DocumentCategorySportsDiploma;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base.IEnrFisConverterDao4DocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduForm.EnrFisConverterDao4EduForm;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduForm.IEnrFisConverterDao4EduForm;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.EnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.enrollmentCampaignType.EnrFisConverterDao4EnrollmentCampaignType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.enrollmentCampaignType.IEnrFisConverterDao4EnrollmentCampaignType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.entrantAchievementType.EnrFisConverterDao4EntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.entrantAchievementType.IEnrFisConverterDao4EntrantAchievementType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examSubject.EnrFisConverterDao4StateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examSubject.IEnrFisConverterDao4StateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examType.EnrFisConverterDao4ExamType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examType.IEnrFisConverterDao4ExamType;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.finSource.EnrFisFinSourceDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.finSource.IEnrFisFinSourceDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympProfileDiscipline.EnrFisConverterDao4OlympProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympProfileDiscipline.IEnrFisConverterDao4OlympProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiad.EnrFisConverterDao4Olympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiad.IEnrFisConverterDao4Olympiad;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiadSubject.EnrFisConverterDao4OlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.olympiadSubject.IEnrFisConverterDao4OlympiadSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.region.EnrFisConverterDao4Region;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.region.IEnrFisConverterDao4Region;

import java.util.Objects;

/**
 * @author vdanilov
 */
@Configuration
public class EnrFisConverterManager extends BusinessObjectManager {

    public static EnrFisConverterManager instance() {
        return instance(EnrFisConverterManager.class);
    }

    @Bean
    public IEnrFisConverterDao4EduForm educationForm() {
        return new EnrFisConverterDao4EduForm();
    }

    @Bean
    public IEnrFisFinSourceDao financingSource() {
        return new EnrFisFinSourceDao();
    }

    @Bean
    public IEnrFisConverterDao4ProgramSet programSet() {
        return new EnrFisConverterDao4ProgramSet();
    }

    @Bean
    public IEnrFisConverterDao4Country addressCountry() {
        return new EnrFisConverterDao4Country();
    }

    @Bean
    public IEnrFisConverterDao4ExamSetElement examSetElement() {
        return new EnrFisConverterDao4ExamSetElement();
    }

    @Bean
    public IEnrFisConverterDao4ExamType examType() {
        return new EnrFisConverterDao4ExamType();
    }

    @Bean
    public IEnrFisConverterDao4DiplomaType diplomaType() {
        return new EnrFisConverterDao4DiplomaType();
    }

    @Bean
    public IEnrFisConverterDao4Olympiad olympiad() {
        return new EnrFisConverterDao4Olympiad();
    }

    @Bean
    public IEnrFisConverterDao4OlympiadSubject olympiadSubject()
    {
        return new EnrFisConverterDao4OlympiadSubject();
    }

    @Bean
    public IEnrFisConverterDao4EntrantAchievementType entrantAchievementType()
    {
        return new EnrFisConverterDao4EntrantAchievementType();
    }

    @Bean
    public IEnrFisConverterDao4EnrollmentCampaignType enrollmentCampaignType()
    {
        return new EnrFisConverterDao4EnrollmentCampaignType();
    }

    @Bean
    public IEnrFisConverterDao4DocumentCategory documentCategoryOrphan()
    {
        return new EnrFisConverterDao4DocumentCategoryOrphan();
    }

    @Bean
    public IEnrFisConverterDao4DocumentCategory documentCategorySportsDiploma()
    {
        return new EnrFisConverterDao4DocumentCategorySportsDiploma();
    }

    @Bean
    public IEnrFisConverterDao4DocumentCategory documentCategoryBelongingNation()
    {
        return new EnrFisConverterDao4DocumentCategoryBelongingNation();
    }

    @Bean
    public IEnrFisConverterDao4DocumentCategory documentCategoryCombatVeterans()
    {
        return new EnrFisConverterDao4DocumentCategoryCombatVeterans();
    }

    @Bean
    public IEnrFisConverterDao4Region region()
    {
        return new EnrFisConverterDao4Region();
    }

    @Bean
    public IEnrFisConverterDao4OlympProfileDiscipline profileDiscipline()
    {
        return new EnrFisConverterDao4OlympProfileDiscipline();
    }

    @Bean
    public IEnrFisConverterDao4StateExamSubject stateExamSubject()
    {
        return new EnrFisConverterDao4StateExamSubject();
    }

    public static Long getTownId(AddressItem addressItem)
    {
        if(addressItem == null)
        {
            return null;
        }

        AddressType addressType = addressItem.getAddressType();

        if(Objects.equals(AddressLevelCodes.REGION, addressType.getAddressLevel().getCode())
                && Objects.equals(103, addressType.getCode()))
        {
            return 1L;
        }
        else if(Objects.equals(AddressLevelCodes.SETTLEMENT, addressType.getAddressLevel().getCode())
                && Objects.equals(301, addressType.getCode()))
        {
            return 2L;
        }
        else if(!addressType.isCountryside())
        {
            return 3L;
        }
        else if(addressType.isCountryside())
        {
            return 4L;
        }

        return null;
    }
}
