/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic;

import com.beust.jcommander.internal.Lists;
import enr14fis.schema.offline.send.*;
import enr14fis.schema.pkg.result.resp.ImportResultPackage;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.hibernate.Session;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.zip.ZipCompressionInterface;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.*;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 25.07.15
 * Time: 1:49
 */
public class EnrFisOfflineDao extends UniBaseDao implements IEnrFisOfflineDao
{
    private static final boolean FIS_OFFLINE_MODE = "true".equals(ApplicationRuntime.getProperty("uni.enr.fis.offline.mode"));

    public static boolean isFisOfflineMode()
    {
        return FIS_OFFLINE_MODE;
    }

    @SuppressWarnings({"all"})
    @Override
    public void getSessionsXml(EnrEnrollmentCampaign enrCampaign, OrgUnit orgUnit)
    {

        final List<EnrFisSyncSession> sessionList = getSessions4Send(enrCampaign, orgUnit);
        if (sessionList.isEmpty()) throw new ApplicationException("Нет данных для отправки в ФИС.");

        EnrFisOfflineSessionListData sessionListData = new EnrFisOfflineSessionListData();
        sessionListData.setEnrollmentCampaignId(enrCampaign.getId());
        sessionListData.setEnrollmentCampaignTitle(enrCampaign.getTitle());
        sessionListData.setOrgUnitId(orgUnit.getId());
        sessionListData.setOrgUnitTitle(!StringUtils.isEmpty(orgUnit.getNominativeCaseTitle()) ? orgUnit.getNominativeCaseTitle() : orgUnit.getTitleWithType());
        sessionListData.setLogin(sessionList.get(0).getEnrOrgUnit().getLoginSafe());
        sessionListData.setPassword(sessionList.get(0).getEnrOrgUnit().getPasswordSafe());
        sessionListData.setSessions(Lists.<EnrFisOfflineSessionData>newArrayList());

        for (EnrFisSyncSession session : sessionList)
        {
            EnrFisOfflineSessionData sessionData = new EnrFisOfflineSessionData();
            sessionData.setSessionId(session.getId());
            sessionData.setSessionTitle(session.getTitle());

            String log = null;
            try
            {
                log = session.getLogString();

            }
            catch (Exception e)
            {

            }

            if (!StringUtils.isEmpty(log))
                sessionData.setLog(session.getLogString());
            sessionData.setCreationDate(session.getCreationDate());
            sessionData.setFillDate(session.getFillDate());
            sessionData.setQueueDate(session.getQueueDate());
            sessionData.setArchiveDate(session.getArchiveDate());
            sessionData.setSkipAdmissionInfo(session.isSkipAdmissionInfo());
            sessionData.setForceAllEntrants(session.isForceAllEntrants());
            sessionData.setAcceptedInfo(session.isAcceptedInfo());
            sessionData.setSendRequestInOrderInfo(session.isSendRequestInOrderInfo());
            sessionData.setSkipEnrCampaignInfo(session.isSkipEnrCampaignInfo());
            sessionData.setSkipApplicationsInfo(session.isSkipApplicationsInfo());
            sessionData.setRequestState(session.getRequestState());
            sessionData.setSkipOrdersOfAdmission(session.isSkipOrdersOfAdmission());
            sessionData.setSkipIllegalDirections(session.isSkipIllegalDirections());
            sessionData.setSkipErrorEntrants(session.isSkipErrorEntrants());
            sessionData.setSkippedEntrantCount(session.getSkippedEntrantCount());
            sessionData.setSkippedExtractCount(session.getSkippedExtractCount());
            sessionData.setSkippedRecItemCount(session.getSkippedRecItemCount());

            List<EnrFisSyncPackage> packages = getSessionPackages(session);
            for (EnrFisSyncPackage syncPackage : packages)
            {
                EnrFisOfflinePacakageData pacakageData = new EnrFisOfflinePacakageData();
                pacakageData.setPackageId(syncPackage.getId());
                pacakageData.setPackageTitle(syncPackage.getTitle());
                pacakageData.setXml(syncPackage.getXmlPackageString());
                pacakageData.setComment(syncPackage.getComment());
                pacakageData.setCreationDate(syncPackage.getCreationDate());
                pacakageData.setFisPackageID(syncPackage.getFisPackageID());
                pacakageData.setFileName(syncPackage.getFileName());

                if (syncPackage instanceof EnrFisSyncPackage4CampaignInfo)
                {
                    sessionData.setCampaignInfo(pacakageData);
                    continue;
                }
                if (syncPackage instanceof EnrFisSyncPackage4AdmissionInfo)
                {
                    sessionData.setAdmissionInfo(pacakageData);
                    continue;
                }
                if (syncPackage instanceof EnrFisSyncPackage4ApplicationsInfo)
                {
                    sessionData.setApplicationsInfo(pacakageData);
                    continue;
                }
                if (syncPackage instanceof EnrFisSyncPackage4AOrdersInfo)
                {
                    sessionData.setOrdersOfAdmission(pacakageData);
                    continue;
                }
                throw new IllegalStateException("enr-fis-offline-unknown-package: " + syncPackage.getClass().getSimpleName());
            }
            sessionListData.getSessions().add(sessionData);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try
        {
            byte[] xml = EnrFisServiceImpl.toXml(sessionListData);
            ZipOutputStream zos = new ZipOutputStream(baos);
            ZipEntry entry = new ZipEntry("sessions.xml");
            entry.setSize(xml.length);
            zos.putNextEntry(entry);
            zos.write(xml);
            zos.closeEntry();
            zos.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException();
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                .zip().fileName(enrCampaign.getTitle().toLowerCase().replaceAll(" ", "_") + "-" + orgUnit.getShortTitle().toLowerCase().replaceAll(" ", "_") + ".zip")
                .document(baos), true);
    }

    @Override
    public void doImportSessionsXmlResults(IUploadFile file, Long enrCampaignId, Long orgUnitId)
    {
        this.logger.info("\n--------------------------------------------------------------------------------\n");
        this.logger.info("doImportSessionsResults.start");
        try
        {
            byte[] xml = ZipCompressionInterface.INSTANCE.decompress(CoreUtils.getBytes(file.getStream()));

            EnrFisOfflineSessionListData sessionListData = EnrFisServiceImpl.fromXml(EnrFisOfflineSessionListData.class, xml);

            if (!orgUnitId.equals(sessionListData.getOrgUnitId()) || !enrCampaignId.equals(sessionListData.getEnrollmentCampaignId()))
                throw new ApplicationException("Импорт не возможен. Импортируемый файл для ПК - " + sessionListData.getEnrollmentCampaignTitle() + " на подразделении - " + sessionListData.getOrgUnitTitle());

            // блокируем всю ПК
            final Session sess = lock("enr-fis-xml-import-" + enrCampaignId);

            Collection<Long> sessionIds = CollectionUtils.collect(sessionListData.getSessions(), EnrFisOfflineSessionData::getSessionId);

            List<Long> archivedSessions = createStatement(new DQLSelectBuilder()
                            .fromEntity(EnrFisSyncSession.class, "s")
                            .column(property("s", EnrFisSyncSession.id()))
                            .where(isNotNull(property("s", EnrFisSyncSession.archiveDate())))
                            .where(in(property("s", EnrFisSyncSession.id()), sessionIds))
            ).list();


            for (EnrFisOfflineSessionData sessionData : sessionListData.getSessions())
            {
                if (archivedSessions.contains(sessionData.getSessionId()))
                    continue; // пропускаем уже обработанные сессии
                EnrFisSyncSession session = get(sessionData.getSessionId());
                if (session == null) continue; // сессии нет, откуда взялась не ясно, пропускаем

                session.setArchiveDate(sessionData.getArchiveDate());
                session.setZipLog(EnrFisSyncSession.zip.compress(sessionData.getLog().getBytes(Charset.forName("UTF-8"))));
                sess.update(session);

                if (sessionData.getCampaignInfo() != null)
                {
                    EnrFisSyncPackage4CampaignInfo package4CampaignInfo = get(sessionData.getCampaignInfo().getPackageId());
                    EnrFisOfflinePacakageData pkg = sessionData.getCampaignInfo();
                    if (package4CampaignInfo != null)
                    {
                        package4CampaignInfo.setFisPackageID(pkg.getFisPackageID());
                        sess.update(package4CampaignInfo);

                        if (sessionData.getCampaignInfo().getPkgSendSuccess() != null)
                        {
                            EnrFisOfflinePackageSendIOLogData pkgSendSuccess = pkg.getPkgSendSuccess();
                            EnrFisSyncPackageSendIOLog sendIOLog = get(EnrFisSyncPackageSendIOLog.class, EnrFisSyncPackageSendIOLog.owner(), package4CampaignInfo);
                            if (sendIOLog == null)
                            {
                                sendIOLog = new EnrFisSyncPackageSendIOLog();
                                sendIOLog.setOwner(package4CampaignInfo);
                                sendIOLog.setOperationDate(pkgSendSuccess.getOperationDate());
                                sendIOLog.setOperationUrl(pkgSendSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlRequest()))
                                    sendIOLog.setZipXmlRequest(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlResponse()))
                                    sendIOLog.setZipXmlResponse(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getErrorLog()))
                                    sendIOLog.setZipErrorLog(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                sendIOLog.setFisErrorText(pkgSendSuccess.getFisErrorText());
                                sendIOLog.setDataNodeCount(pkgSendSuccess.getDataNodeCount());
                                sess.save(sendIOLog);

                                package4CampaignInfo.setPkgSendSuccess(sendIOLog);
                                sess.update(package4CampaignInfo);
                            }
                        }

                        if (sessionData.getCampaignInfo().getPkgRecvSuccess() != null)
                        {
                            EnrFisOfflinePackageReceiveIOLogData pkgRecvSuccess = sessionData.getCampaignInfo().getPkgRecvSuccess();
                            EnrFisSyncPackageReceiveIOLog receiveIOLog = get(EnrFisSyncPackageReceiveIOLog.class, EnrFisSyncPackageReceiveIOLog.owner(), package4CampaignInfo);
                            if (receiveIOLog == null)
                            {
                                receiveIOLog = new EnrFisSyncPackageReceiveIOLog();
                                receiveIOLog.setOwner(package4CampaignInfo);
                                receiveIOLog.setOperationDate(pkgRecvSuccess.getOperationDate());
                                receiveIOLog.setOperationUrl(pkgRecvSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlRequest()))
                                    receiveIOLog.setZipXmlRequest(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                    receiveIOLog.setZipXmlResponse(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getErrorLog()))
                                    receiveIOLog.setZipErrorLog(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                receiveIOLog.setFisErrorText(pkgRecvSuccess.getFisErrorText());
                                receiveIOLog.setSuccessfulCount(pkgRecvSuccess.getSuccessfulCount());
                                receiveIOLog.setConflictCount(pkgRecvSuccess.getConflictCount());
                                receiveIOLog.setFailureCount(pkgRecvSuccess.getFailureCount());
                                sess.save(receiveIOLog);

                                package4CampaignInfo.setPkgRecvSuccess(receiveIOLog);
                                sess.update(package4CampaignInfo);

                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                {
                                    ImportResultPackage result = EnrFisServiceImpl.fromXml(ImportResultPackage.class, pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8")));
                                    doAnalyzePackageResult(receiveIOLog, result);
                                }
                            }
                        }
                    }
                }

                if (sessionData.getAdmissionInfo() != null)
                {
                    EnrFisSyncPackage4AdmissionInfo package4AdmissionInfo = get(sessionData.getAdmissionInfo().getPackageId());
                    EnrFisOfflinePacakageData pkg = sessionData.getAdmissionInfo();
                    if (package4AdmissionInfo != null)
                    {
                        package4AdmissionInfo.setFisPackageID(pkg.getFisPackageID());
                        sess.update(package4AdmissionInfo);
                        if (sessionData.getAdmissionInfo().getPkgSendSuccess() != null)
                        {
                            EnrFisOfflinePackageSendIOLogData pkgSendSuccess = pkg.getPkgSendSuccess();
                            EnrFisSyncPackageSendIOLog sendIOLog = get(EnrFisSyncPackageSendIOLog.class, EnrFisSyncPackageSendIOLog.owner(), package4AdmissionInfo);
                            if (sendIOLog == null)
                            {
                                sendIOLog = new EnrFisSyncPackageSendIOLog();
                                sendIOLog.setOwner(package4AdmissionInfo);
                                sendIOLog.setOperationDate(pkgSendSuccess.getOperationDate());
                                sendIOLog.setOperationUrl(pkgSendSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlRequest()))
                                    sendIOLog.setZipXmlRequest(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlResponse()))
                                    sendIOLog.setZipXmlResponse(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getErrorLog()))
                                    sendIOLog.setZipErrorLog(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                sendIOLog.setFisErrorText(pkgSendSuccess.getFisErrorText());
                                sendIOLog.setDataNodeCount(pkgSendSuccess.getDataNodeCount());
                                sess.save(sendIOLog);

                                package4AdmissionInfo.setPkgSendSuccess(sendIOLog);
                                sess.update(package4AdmissionInfo);
                            }
                        }

                        if (sessionData.getAdmissionInfo().getPkgRecvSuccess() != null)
                        {
                            EnrFisOfflinePackageReceiveIOLogData pkgRecvSuccess = sessionData.getAdmissionInfo().getPkgRecvSuccess();
                            EnrFisSyncPackageReceiveIOLog receiveIOLog = get(EnrFisSyncPackageReceiveIOLog.class, EnrFisSyncPackageReceiveIOLog.owner(), package4AdmissionInfo);
                            if (receiveIOLog == null)
                            {
                                receiveIOLog = new EnrFisSyncPackageReceiveIOLog();
                                receiveIOLog.setOwner(package4AdmissionInfo);
                                receiveIOLog.setOperationDate(pkgRecvSuccess.getOperationDate());
                                receiveIOLog.setOperationUrl(pkgRecvSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlRequest()))
                                    receiveIOLog.setZipXmlRequest(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                    receiveIOLog.setZipXmlResponse(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getErrorLog()))
                                    receiveIOLog.setZipErrorLog(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                receiveIOLog.setFisErrorText(pkgRecvSuccess.getFisErrorText());

                                receiveIOLog.setSuccessfulCount(pkgRecvSuccess.getSuccessfulCount());
                                receiveIOLog.setConflictCount(pkgRecvSuccess.getConflictCount());
                                receiveIOLog.setFailureCount(pkgRecvSuccess.getFailureCount());
                                sess.save(receiveIOLog);

                                package4AdmissionInfo.setPkgRecvSuccess(receiveIOLog);
                                sess.update(package4AdmissionInfo);

                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                {
                                    ImportResultPackage result = EnrFisServiceImpl.fromXml(ImportResultPackage.class, pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8")));
                                    doAnalyzePackageResult(receiveIOLog, result);
                                }
                            }
                        }
                    }
                }

                if (sessionData.getApplicationsInfo() != null)
                {
                    EnrFisSyncPackage4ApplicationsInfo package4ApplicationsInfo = get(sessionData.getApplicationsInfo().getPackageId());
                    EnrFisOfflinePacakageData pkg = sessionData.getApplicationsInfo();
                    if (package4ApplicationsInfo != null)
                    {
                        package4ApplicationsInfo.setFisPackageID(pkg.getFisPackageID());
                        sess.update(package4ApplicationsInfo);

                        if (sessionData.getApplicationsInfo().getPkgSendSuccess() != null)
                        {
                            EnrFisOfflinePackageSendIOLogData pkgSendSuccess = pkg.getPkgSendSuccess();
                            EnrFisSyncPackageSendIOLog sendIOLog = get(EnrFisSyncPackageSendIOLog.class, EnrFisSyncPackageSendIOLog.owner(), package4ApplicationsInfo);
                            if (sendIOLog == null)
                            {
                                sendIOLog = new EnrFisSyncPackageSendIOLog();
                                sendIOLog.setOwner(package4ApplicationsInfo);
                                sendIOLog.setOperationDate(pkgSendSuccess.getOperationDate());
                                sendIOLog.setOperationUrl(pkgSendSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlRequest()))
                                    sendIOLog.setZipXmlRequest(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlResponse()))
                                    sendIOLog.setZipXmlResponse(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getErrorLog()))
                                    sendIOLog.setZipErrorLog(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                sendIOLog.setFisErrorText(pkgSendSuccess.getFisErrorText());
                                sendIOLog.setDataNodeCount(pkgSendSuccess.getDataNodeCount());
                                sess.save(sendIOLog);

                                package4ApplicationsInfo.setPkgSendSuccess(sendIOLog);
                                sess.update(package4ApplicationsInfo);
                            }
                        }

                        if (sessionData.getApplicationsInfo().getPkgRecvSuccess() != null)
                        {
                            EnrFisOfflinePackageReceiveIOLogData pkgRecvSuccess = sessionData.getApplicationsInfo().getPkgRecvSuccess();
                            EnrFisSyncPackageReceiveIOLog receiveIOLog = get(EnrFisSyncPackageReceiveIOLog.class, EnrFisSyncPackageReceiveIOLog.owner(), package4ApplicationsInfo);
                            if (receiveIOLog == null)
                            {
                                receiveIOLog = new EnrFisSyncPackageReceiveIOLog();
                                receiveIOLog.setOwner(package4ApplicationsInfo);
                                receiveIOLog.setOperationDate(pkgRecvSuccess.getOperationDate());
                                receiveIOLog.setOperationUrl(pkgRecvSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlRequest()))
                                    receiveIOLog.setZipXmlRequest(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                    receiveIOLog.setZipXmlResponse(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getErrorLog()))
                                    receiveIOLog.setZipErrorLog(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                receiveIOLog.setFisErrorText(pkgRecvSuccess.getFisErrorText());

                                receiveIOLog.setSuccessfulCount(pkgRecvSuccess.getSuccessfulCount());
                                receiveIOLog.setConflictCount(pkgRecvSuccess.getConflictCount());
                                receiveIOLog.setFailureCount(pkgRecvSuccess.getFailureCount());
                                sess.save(receiveIOLog);

                                package4ApplicationsInfo.setPkgRecvSuccess(receiveIOLog);
                                sess.update(package4ApplicationsInfo);

                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                {
                                    ImportResultPackage result = EnrFisServiceImpl.fromXml(ImportResultPackage.class, pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8")));
                                    doAnalyzePackageResult(receiveIOLog, result);
                                }
                            }
                        }
                    }
                }

                if (sessionData.getOrdersOfAdmission() != null)
                {
                    EnrFisSyncPackage4AOrdersInfo package4AOrdersInfo = get(sessionData.getOrdersOfAdmission().getPackageId());
                    EnrFisOfflinePacakageData pkg = sessionData.getOrdersOfAdmission();
                    if (package4AOrdersInfo != null)
                    {
                        package4AOrdersInfo.setFisPackageID(pkg.getFisPackageID());
                        sess.update(package4AOrdersInfo);

                        if (sessionData.getOrdersOfAdmission().getPkgSendSuccess() != null)
                        {
                            EnrFisOfflinePackageSendIOLogData pkgSendSuccess = pkg.getPkgSendSuccess();
                            EnrFisSyncPackageSendIOLog sendIOLog = get(EnrFisSyncPackageSendIOLog.class, EnrFisSyncPackageSendIOLog.owner(), package4AOrdersInfo);
                            if (sendIOLog == null)
                            {
                                sendIOLog = new EnrFisSyncPackageSendIOLog();
                                sendIOLog.setOwner(package4AOrdersInfo);
                                sendIOLog.setOperationDate(pkgSendSuccess.getOperationDate());
                                sendIOLog.setOperationUrl(pkgSendSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlRequest()))
                                    sendIOLog.setZipXmlRequest(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getXmlResponse()))
                                    sendIOLog.setZipXmlResponse(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgSendSuccess.getErrorLog()))
                                    sendIOLog.setZipErrorLog(EnrFisSyncPackageSendIOLog.zip.compress(pkgSendSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                sendIOLog.setFisErrorText(pkgSendSuccess.getFisErrorText());
                                sendIOLog.setDataNodeCount(pkgSendSuccess.getDataNodeCount());
                                sess.save(sendIOLog);

                                package4AOrdersInfo.setPkgSendSuccess(sendIOLog);
                                sess.update(package4AOrdersInfo);
                            }
                        }

                        if (sessionData.getOrdersOfAdmission().getPkgRecvSuccess() != null)
                        {
                            EnrFisOfflinePackageReceiveIOLogData pkgRecvSuccess = sessionData.getOrdersOfAdmission().getPkgRecvSuccess();
                            EnrFisSyncPackageReceiveIOLog receiveIOLog = get(EnrFisSyncPackageReceiveIOLog.class, EnrFisSyncPackageReceiveIOLog.owner(), package4AOrdersInfo);
                            if (receiveIOLog == null)
                            {
                                receiveIOLog = new EnrFisSyncPackageReceiveIOLog();
                                receiveIOLog.setOwner(package4AOrdersInfo);
                                receiveIOLog.setOperationDate(pkgRecvSuccess.getOperationDate());
                                receiveIOLog.setOperationUrl(pkgRecvSuccess.getOperationUrl());
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlRequest()))
                                    receiveIOLog.setZipXmlRequest(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlRequest().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                    receiveIOLog.setZipXmlResponse(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8"))));
                                if (!StringUtils.isEmpty(pkgRecvSuccess.getErrorLog()))
                                    receiveIOLog.setZipErrorLog(EnrFisSyncPackageReceiveIOLog.zip.compress(pkgRecvSuccess.getErrorLog().getBytes(Charset.forName("UTF-8"))));
                                receiveIOLog.setFisErrorText(pkgRecvSuccess.getFisErrorText());

                                receiveIOLog.setSuccessfulCount(pkgRecvSuccess.getSuccessfulCount());
                                receiveIOLog.setConflictCount(pkgRecvSuccess.getConflictCount());
                                receiveIOLog.setFailureCount(pkgRecvSuccess.getFailureCount());
                                sess.save(receiveIOLog);

                                package4AOrdersInfo.setPkgRecvSuccess(receiveIOLog);
                                sess.update(package4AOrdersInfo);

                                if (!StringUtils.isEmpty(pkgRecvSuccess.getXmlResponse()))
                                {
                                    ImportResultPackage result = EnrFisServiceImpl.fromXml(ImportResultPackage.class, pkgRecvSuccess.getXmlResponse().getBytes(Charset.forName("UTF-8")));
                                    doAnalyzePackageResult(receiveIOLog, result);
                                }
                            }
                        }

                    }
                }
            }

            this.logger.info("doImportSessionsResults.complete\n");
        }
        catch (final Exception t)
        {
            this.logger.error("doImportSessionsResults.error: " + t.getMessage(), t);
            Debug.exception(t.getMessage(), t);
            throw new ApplicationException("Во время обработки файла произошла ошибка. Проверьте корректность файла или обратитесь к администратору системы.", t);
        }
    }

    private List<EnrFisSyncSession> getSessions4Send(EnrEnrollmentCampaign enrCampaign, OrgUnit orgUnit)
    {
        final Date now = new Date();

        final List<EnrFisSyncSession> list = new DQLSelectBuilder()
                .fromEntity(EnrFisSyncSession.class, "s")
                .column(property("s"))
                .order(property(EnrFisSyncSession.creationDate().fromAlias("s")))
                .order(property(EnrFisSyncSession.id().fromAlias("s")))
                .where(eq(property(EnrFisSyncSession.enrOrgUnit().enrollmentCampaign().fromAlias("s")), value(enrCampaign)))
                .where(eq(property(EnrFisSyncSession.enrOrgUnit().institutionOrgUnit().orgUnit().fromAlias("s")), value(orgUnit)))
                .where(isNull(property(EnrFisSyncSession.archiveDate().fromAlias("s"))))
                .where(isNotNull(property(EnrFisSyncSession.fillDate().fromAlias("s"))))

                        // есть пакет, который надо отправлять
                        // при этом в пакете либо нет ограничения на передачу пакетов, либо ограничение уже прошло
                .where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(EnrFisSyncPackage.class, "p")
                                .column(property("p.id"))
                                .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), property("s")))
                                .where(isNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p"))))
                                .where(or(
                                        isNull(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p"))),
                                        le(property(EnrFisSyncPackage.nextOperationThresholdDate().fromAlias("p")), valueTimestamp(now))
                                ))
                                .buildQuery()
                ))
                .createStatement(getSession()).list();

//        // проверяем, что в рамках ПК только одна сессия
//        final Map<EnrOrgUnit, EnrFisSyncSession> testMap = new HashMap<EnrOrgUnit, EnrFisSyncSession>();
//        for (final EnrFisSyncSession s : list)
//        {
//            if (null != testMap.put(s.getEnrOrgUnit(), s))
//            {
//                throw new IllegalStateException("send-queue-is-not-unique: " + s.getEnrOrgUnit().getTitle());
//            }
//        }

        return list;
    }

    private List<EnrFisSyncPackage> getSessionPackages(final EnrFisSyncSession fisSession)
    {
        // нужно получить первый пакет, который можно передать
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrFisSyncPackage.class, "p")
                .where(eq(property(EnrFisSyncPackage.session().fromAlias("p")), value(fisSession)))
                .where(isNull(property(EnrFisSyncPackage.pkgSendSuccess().fromAlias("p")))); /* пакет еще не передан в ФИС */
        return dql.createStatement(getSession()).list();
    }


    protected void doAnalyzePackageResult(EnrFisSyncPackageReceiveIOLog io, ImportResultPackage result)
    {
        Session session = getSession();
        List<EnrFisEntrantSyncData> syncDataList = new DQLSelectBuilder().fromEntity(EnrFisEntrantSyncData.class, "sd")
                .where(eq(property("sd", EnrFisEntrantSyncData.syncPackage().id()), value(io.getOwner().getId())))
                .createStatement(session).list();

        if(syncDataList.isEmpty()) return;

        List<Long> failedEntrantIds = EnrFisSyncPackageReceiveIOLog.getFailedEntrantIds(result);

        Date syncCompleteDate = new Date();
        for(EnrFisEntrantSyncData syncData : syncDataList) {
            syncData.setSyncCompleteDate(syncCompleteDate);
            syncData.setSuccessfulImport(!failedEntrantIds.contains(syncData.getEntrant().getId()));
            session.update(syncData);
        }
    }
}
