package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import java.util.List;
import java.util.NoSuchElementException;


/**
 * @author vdanilov
 */
public interface IEnrFisConverter<T extends IEntity> {

    /** @return код справочника ФИС (из которого будут выбираться значения для сопоставления) */
    String getEcfCatalogCode();

    /** @return список объектов uni, для которых требуется провести сопоставление */
    List<T> getEntityList();

    /**
     * @return элемент справочника ФИС, соответсвующее объекту в uni (соответсвует текущей настройке, кэшируется)
     * @throws java.util.NoSuchElementException если элемента не найдено (если required=true)
     **/
    EnrFisCatalogItem getCatalogItem(T entity, boolean required) throws NoSuchElementException;

}
