/**
 *$Id: EnrFisConverterConfig4OlympiadUI.java 32223 2014-02-03 10:55:07Z vdanilov $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4Olympiad;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
public class EnrFisConverterConfig4OlympiadUI extends EnrFisConverterConfigUI
{
    private EducationYear _educationYear;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _educationYear = DataAccessServices.dao().get(EducationYear.class, EducationYear.intValue(), DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true).getIntValue() + 1);
    }

    public void onClickAutoSync()
    {
        EnrFisConverterManager.instance().olympiad().doAutoSync(_educationYear);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisConverterConfig4Olympiad.BIND_EDUCATION_YEAR, _educationYear);
    }

    // Getters & Setters

    public boolean isEmptySelectors()
    {
        return _educationYear == null;
    }

    // Accessors

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }
}
