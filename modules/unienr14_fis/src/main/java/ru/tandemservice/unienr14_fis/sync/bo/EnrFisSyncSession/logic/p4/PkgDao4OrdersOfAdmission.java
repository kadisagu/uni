/* $Id:$ */
package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.p4;

import com.google.common.collect.Maps;
import enr14fis.schema.pkg.send.req.PackageData;
import enr14fis.schema.pkg.send.req.Root;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.EnrFisConvertersSimple;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.finSource.IEnrFisFinSourceDao;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.EnrFisSettingsManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.gen.EnrFisSettingsECPeriodItemGen;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.EnrFisSyncSessionManager;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage4AOrdersInfo;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncSession;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.PkgDaoBase;
import ru.tandemservice.unienr14_fis.util.EnrFisServiceImpl;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

//import enr14fis.schema.pkg.send.req.PackageData.RecommendedLists.RecommendedList.RecLists.RecList;

/**
 * @author oleyba
 * @since 7/2/14
 */
public class PkgDao4OrdersOfAdmission extends PkgDaoBase implements IPkgDao4OrdersOfAdmission
{
    @Override
    public EnrFisSyncPackage4AOrdersInfo doCreatePackage(EnrFisSyncSession fisSession)
    {
        if (fisSession.isSkipOrdersOfAdmission()) return null;

        Debug.begin("package 4");

        Root root = this.buildRoot(fisSession);
        if (null == root) return null;

        final EnrFisSyncPackage4AOrdersInfo dbo = new EnrFisSyncPackage4AOrdersInfo();
        dbo.setCreationDate(new Date());
        dbo.setSession(fisSession);
        dbo.setXmlPackage(EnrFisServiceImpl.toXml(root));
        this.saveOrUpdate(dbo);
        flush(fisSession);
        return dbo;
    }

//    protected Root buildRecommendedRoot(EnrOrgUnit enrOrgUnit)
//    {
//        //final String PREFIX = EnrFisSyncSessionManager.instance().getProperty("title.orders");
//        final Function<Date, XMLGregorianCalendar> dateTimeFunction = EnrFisSyncSessionManager.instance().dao().getDateTimeXmlConverter(enrOrgUnit.getEnrollmentCampaign(), dataTypeFactory);
//
//        final Root root = this.newRoot(enrOrgUnit);
//
//
//        final IEnrFisConverterDao4ProgramSet programSetDao = EnrFisConverterManager.instance().programSet();
//        final Set<EnrCompetition> competitionSet = new HashSet<>();
//        for (EnrCompetition competition : EnrFisSettingsManager.instance().dao().getCompetitions(enrOrgUnit)) {
//            if (competition.getType().getCompensationType().isBudget()) {
//                competitionSet.add(competition); // не передаем договор
//            }
//        }
//
//
//        final IEnrFisConverter<EnrProgramSetBase> programSetConverter = EnrFisConverterManager.instance().programSet().getConverter();
//        final IEnrFisConverter<EduProgramForm> eduFormConverter = EnrFisConverterManager.instance().educationForm().getConverter();
//
//        final Map<MultiKey, EnrFisSettingsECPeriodItem> periodMap = new HashMap<>();
//        final List<EnrFisSettingsECPeriodItem> items = this.getList(EnrFisSettingsECPeriodItem.class, EnrFisSettingsECPeriodItemGen.key().enrOrgUnit(), enrOrgUnit);
//        for (EnrFisSettingsECPeriodItem item : items) {
//            periodMap.put(key(item), item);
//        }
//
//        // recommendation
//
//        final List<EnrEnrollmentStepItem> recommendedStepItems = new ArrayList<>();
//        BatchUtils.execute(competitionSet, DQL.MAX_VALUES_ROW_NUMBER, elements -> recommendedStepItems.addAll(
//            new DQLSelectBuilder()
//                .fromEntity(EnrEnrollmentStepItem.class, "e").column("e")
//                .where(in(property("e", EnrEnrollmentStepItem.entity().competition()), elements))
//                .where(eq(property("e", EnrEnrollmentStepItem.recommended()), value(Boolean.TRUE)))
//                .createStatement(getSession()).<EnrEnrollmentStepItem>list()
//        ));
//
//        if (recommendedStepItems.size() > 0)
//        {
//            root.getPackageData().setRecommendedLists(new PackageData.RecommendedLists());
//
//            Map<Long, Map<EnrEntrantRequest, PackageData.RecommendedLists.RecommendedList.RecLists.RecList>> map = new HashMap<>();
//
//            for (EnrEnrollmentStepItem item: recommendedStepItems)
//            {
//                EnrRequestedCompetition requestedCompetition = item.getEntity().getRequestedCompetition();
//                EnrEntrantRequest request = requestedCompetition.getRequest();
//                EnrEntrant entrant = request.getEntrant();
//                EnrCompetition competition = requestedCompetition.getCompetition();
//                EnrProgramSetBase ps = competition.getProgramSetOrgUnit().getProgramSet();
//
//                final EnrFisCatalogItem fisDirection = programSetConverter.getCatalogItem(ps, false);
//                if (null == fisDirection) {
//                    throw new ApplicationException("Невозможно определить направление подготовки ФИС для выбранного конкурса «" + competition.getTitle() + "».");
//                }
//
//                final EnrFisCatalogItem fisEduForm = eduFormConverter.getCatalogItem(ps.getProgramForm(), false);
//                final long finSourceId = getFinSourceId(competition);
//
//                final EnrFisCatalogItem fisEduLvl = programSetDao.getEnrFisItem4EducationLevel(ps);
//                if (null == fisEduLvl) {
//                    throw new ApplicationException("Невозможно определить уровень образования ФИС для выбранного конкурса «" + competition.getTitle() + "».");
//                }
//
//                Long stage;
//                EnrFisSettingsECPeriodItem period = periodMap.get(key(fisEduForm, fisEduLvl, finSourceId, item.getStep().getEnrollmentDate()));
//
//                if (competition.isExclusive() || competition.isNoExams()) {
//                    stage = 1L;
//                } else if (null == period) {
//                    throw new ApplicationException("Не найден этап приемной кампании ФИС для выбранного конкурса «" + competition.getTitle() + "».");
//                } else {
//                    stage = (period.getKey().getSize() > 1) ? (long) period.getStage() : 1L;
//                }
//
//                Map<EnrEntrantRequest, RecList> stageMap = SafeMap.safeGet(map, stage, HashMap.class);
//
//                PackageData.RecommendedLists.RecommendedList.RecLists.RecList xmlElement = stageMap.get(request);
//                if (null == xmlElement) {
//                    stageMap.put(request, xmlElement = new PackageData.RecommendedLists.RecommendedList.RecLists.RecList());
//                    xmlElement.setApplication(new PackageData.RecommendedLists.RecommendedList.RecLists.RecList.Application());
//                    xmlElement.getApplication().setApplicationNumber(uid(entrant) + "/" + request.getRegNumber());
//                    xmlElement.getApplication().setRegistrationDate(xmlDateTime(request.getRegDate(), dateTimeFunction));
//                    xmlElement.setFinSourceAndEduForms(new PackageData.RecommendedLists.RecommendedList.RecLists.RecList.FinSourceAndEduForms());
//                }
//
//                PackageData.RecommendedLists.RecommendedList.RecLists.RecList.FinSourceAndEduForms.FinSourceAndEduForm xmlFinSourceAndEduForm = new PackageData.RecommendedLists.RecommendedList.RecLists.RecList.FinSourceAndEduForms.FinSourceAndEduForm();
//                xmlFinSourceAndEduForm.setCompetitiveGroupID(competitionGroupUid(competition));
//                xmlFinSourceAndEduForm.setDirectionID(fisDirection.getFisID());
//                xmlFinSourceAndEduForm.setEducationalFormID(fisEduForm.getFisID());
//                xmlFinSourceAndEduForm.setEducationalLevelID(fisEduLvl.getFisID());
//                xmlElement.getFinSourceAndEduForms().getFinSourceAndEduForm().add(xmlFinSourceAndEduForm);
//            }
//
//            for (Map.Entry<Long, Map<EnrEntrantRequest, PackageData.RecommendedLists.RecommendedList.RecLists.RecList>> e: map.entrySet()) {
//                PackageData.RecommendedLists.RecommendedList xmlRecList = new PackageData.RecommendedLists.RecommendedList();
//                xmlRecList.setStage((int) e.getKey().longValue());
//                xmlRecList.setRecLists(new PackageData.RecommendedLists.RecommendedList.RecLists());
//                for (PackageData.RecommendedLists.RecommendedList.RecLists.RecList rl: e.getValue().values()) {
//                    xmlRecList.getRecLists().getRecList().add(rl);
//                }
//                root.getPackageData().getRecommendedLists().getRecommendedList().add(xmlRecList);
//            }
//        }
//
//
//        return root;
//    }

    protected Root buildRoot(EnrFisSyncSession fisSession)
    {
        final String PREFIX = EnrFisSyncSessionManager.instance().getProperty("title.orders");
        final Function<Date, XMLGregorianCalendar> dateTimeFunction = EnrFisSyncSessionManager.instance().dao().getDateTimeXmlConverter(fisSession.getEnrOrgUnit().getEnrollmentCampaign(), dataTypeFactory);

        // состояние
        status(fisSession, PREFIX, 0);

        final EnrOrgUnit enrOrgUnit = fisSession.getEnrOrgUnit();
        final Root root = this.newRoot(enrOrgUnit);

        long fisLevelBudget = EnrFisConvertersSimple.getLevelBudgetFinancing(enrOrgUnit.getEnrollmentCampaign().getSettings().getLevelBudgetFinancing());

        final Set<EnrCompetition> competitionSet = new HashSet<>(EnrFisSettingsManager.instance().dao().getCompetitions(enrOrgUnit));

        final IEnrFisConverterDao4ProgramSet programSet = EnrFisConverterManager.instance().programSet();
        final IEnrFisConverter<EduProgramForm> eduFormConverter = EnrFisConverterManager.instance().educationForm().getConverter();
        final IEnrFisFinSourceDao finSourceDao = EnrFisConverterManager.instance().financingSource();

        final Map<MultiKey, EnrFisSettingsECPeriodItem> periodMap = new HashMap<>();
        final List<EnrFisSettingsECPeriodItem> items = this.getList(EnrFisSettingsECPeriodItem.class, EnrFisSettingsECPeriodItemGen.key().enrOrgUnit(), enrOrgUnit);
        for (EnrFisSettingsECPeriodItem item : items) {
            periodMap.put(key(item), item);
        }

        boolean something = false;

        // extracts
        {
            final List<EnrEnrollmentExtract> enrollmentExtracts = new ArrayList<>();
            final List<EnrAbstractExtract> allExtracts = new ArrayList<>();
            final Map<Long, List<EnrEnrollmentExtract>> enrollmentExtractsMap = Maps.newHashMap();
            final Map<Long, List<EnrEnrollmentExtract>> canceledExtractsMap = Maps.newHashMap();

            BatchUtils.execute(competitionSet, DQL.MAX_VALUES_ROW_NUMBER, elements ->
            {
                List<EnrEnrollmentExtract> extracts = new DQLSelectBuilder()
                        .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                        .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().fromAlias("e"), "r")
                        .where(in(property("r", EnrRequestedCompetition.competition()), elements))
                        .where(or(
                                eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.ACCEPTED)),
                                eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED))
                        ))
                        .createStatement(getSession()).list();

                extracts.forEach(extract ->
                        SafeMap.safeGet(enrollmentExtractsMap, extract.getOrder().getId(), ArrayList.class).add(extract));

                enrollmentExtracts.addAll(extracts);
                allExtracts.addAll(extracts);
            });

            final List<EnrCancelExtract> cancelledExtracts = new ArrayList<>();
            BatchUtils.execute(competitionSet, DQL.MAX_VALUES_ROW_NUMBER, elements ->
            {
                List<Object[]> extracts = new DQLSelectBuilder()
                        .fromEntity(EnrCancelExtract.class, "e")
                        .joinPath(DQLJoinType.inner, EnrCancelExtract.entity().fromAlias("e"), "r")
                        .joinPath(DQLJoinType.inner, EnrCancelExtract.requestedCompetition().fromAlias("e"), "rc")
                        .joinEntity("e", DQLJoinType.inner, EnrEnrollmentExtract.class, "enr", eq(property("enr"), property("r")))

                        .where(in(property("rc", EnrRequestedCompetition.competition()), elements))
                        .where(eq(property("rc"), property("enr", EnrEnrollmentExtract.entity())))
                        .where(eq(property("e", EnrCancelExtract.cancelled()), value(false)))
                        .where(eq(property("e", EnrCancelExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                        .column(property("e"))
                        .column(property("r"))
                        .createStatement(getSession()).list();

                for (Object[] ext : extracts)
                {
                    EnrCancelExtract cancelExtract = (EnrCancelExtract) ext[0];
                    EnrEnrollmentExtract enrExtract = (EnrEnrollmentExtract) ext[1];
                    cancelledExtracts.add(cancelExtract);
                    allExtracts.add(cancelExtract);
                    SafeMap.safeGet(canceledExtractsMap, cancelExtract.getParagraph().getOrder().getId(), ArrayList.class).add(enrExtract);
                }
            });

            if (enrollmentExtracts.size() > 0)
            {
                something = true;
                if (root.getPackageData().getOrders() == null)
                {
                    root.getPackageData().setOrders(new PackageData.Orders());
                }
                root.getPackageData().getOrders().setOrdersOfAdmission(new PackageData.Orders.OrdersOfAdmission());

                enrollmentExtracts.stream()
                        .map(EnrEnrollmentExtract::getOrder)
                        .distinct()
                        .sorted((o1, o2) -> ObjectUtils.compare(o1.getNumber(), o2.getNumber()))
                        .forEach(order ->
                        {
                            PackageData.Orders.OrdersOfAdmission.OrderOfAdmission fisOrder = new PackageData.Orders.OrdersOfAdmission.OrderOfAdmission();

                            fisOrder.setOrderOfAdmissionUID(uid(order));
                            fisOrder.setCampaignUID(uid(order.getEnrollmentCampaign()));
                            fisOrder.setOrderName(order.getTitleWithOrder());
                            fisOrder.setOrderNumber(order.getNumber());
                            fisOrder.setOrderDate(xmlDate(order.getCommitDate()));

                            List<EnrEnrollmentExtract> extracts = enrollmentExtractsMap.get(order.getId());

                            Set<EduProgramForm> forms = extracts.stream().map(enrEnrollmentExtract -> ((EnrEnrollmentParagraph) enrEnrollmentExtract.getParagraph()).getProgramForm()).collect(Collectors.toSet());

                            if (forms.size() == 1)
                            {
                                EnrFisCatalogItem fisCatalogItem = eduFormConverter.getCatalogItem(forms.iterator().next(), true);
                                fisOrder.setEducationFormID(fisCatalogItem.getFisID());
                            }

                            Set<EnrCompetitionType> competitionTypes = extracts.stream().map(enrEnrollmentExtract -> ((EnrEnrollmentParagraph) enrEnrollmentExtract.getParagraph()).getCompetitionType()).collect(Collectors.toSet());

                            if (competitionTypes.size() == 1)
                            {
                                fisOrder.setFinanceSourceID(finSourceDao.getSource(competitionTypes.iterator().next()).getFisID());
                            }

                            Set<EduLevel> eduLevels = extracts.stream().map(enrEnrollmentExtract -> ((EnrEnrollmentParagraph) enrEnrollmentExtract.getParagraph()).getProgramSubject().getSubjectIndex().getProgramKind().getEduLevel()).collect(Collectors.toSet());

                            if (eduLevels.size() == 1)
                            {
                                EnrFisCatalogItem fisCatalogItem = programSet.getEnrFisItem4EducationLevel(extracts.get(0).getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet());
                                fisOrder.setEducationLevelID(fisCatalogItem.getFisID());
                            }

                            Integer stageInt = null;

                            if (EnrRequestTypeCodes.BS.equals(order.getRequestType().getCode()) && CollectionUtils.exists(forms, eduProgramForm -> EduProgramFormCodes.OCHNAYA.equals(eduProgramForm.getCode()) || EduProgramFormCodes.OCHNO_ZAOCHNAYA.equals(eduProgramForm.getCode())))
                            {
                                Calendar calendar = Calendar.getInstance();

                                calendar.set(2016, Calendar.AUGUST, 3);
                                Date enrollmentDate3Aug = CommonBaseDateUtil.trimTime(calendar.getTime());
                                calendar.set(2016, Calendar.AUGUST, 8);
                                Date enrollmentDate8Aug = CommonBaseDateUtil.trimTime(calendar.getTime());
                                calendar.set(2016, Calendar.JULY, 22);
                                Date enrollmentDate22Jul = CommonBaseDateUtil.trimTime(calendar.getTime());
                                calendar.set(2016, Calendar.JULY, 27);
                                Date enrollmentDate27Jul = CommonBaseDateUtil.trimTime(calendar.getTime());

                                if (enrollmentDate22Jul.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())) || enrollmentDate3Aug.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())))
                                {
                                    stageInt = 1;
                                } else if (enrollmentDate27Jul.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())) || enrollmentDate8Aug.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())))
                                {
                                    stageInt = 2;
                                } else
                                {
                                    stageInt = 0;
                                }
                            }

                            if (stageInt != null)
                            {
                                fisOrder.setStage(stageInt.longValue());
                            }
                            root.getPackageData().getOrders().getOrdersOfAdmission().getOrderOfAdmission().add(fisOrder);
                        });

            }

            if (cancelledExtracts.size() > 0)
            {
                something = true;
                if (root.getPackageData().getOrders() == null)
                {
                    root.getPackageData().setOrders(new PackageData.Orders());
                }
                root.getPackageData().getOrders().setOrdersOfException(new PackageData.Orders.OrdersOfException());

                cancelledExtracts.stream()
                        .map(extract -> extract.getParagraph().getOrder())
                        .distinct()
                        .sorted((o1, o2) -> ObjectUtils.compare(o1.getNumber(), o2.getNumber()))
                        .forEach(ord ->
                        {

                            EnrOrder order = (EnrOrder) ord;
                            PackageData.Orders.OrdersOfException.OrderOfException fisOrder = new PackageData.Orders.OrdersOfException.OrderOfException();

                            fisOrder.setOrderOfExceptionUID(uid(order));
                            fisOrder.setCampaignUID(uid(order.getEnrollmentCampaign()));
                            fisOrder.setOrderName(order.getTitleWithOrder());
                            fisOrder.setOrderNumber(order.getNumber());
                            fisOrder.setOrderDate(xmlDate(order.getCommitDate()));

                            List<EnrEnrollmentExtract> extracts = canceledExtractsMap.get(order.getId());

                            Set<EduProgramForm> forms = extracts.stream().map(enrEnrollmentExtract -> ((EnrEnrollmentParagraph) enrEnrollmentExtract.getParagraph()).getProgramForm()).collect(Collectors.toSet());

                            if (forms.size() == 1)
                            {
                                EnrFisCatalogItem fisCatalogItem = eduFormConverter.getCatalogItem(forms.iterator().next(), true);
                                fisOrder.setEducationFormID(fisCatalogItem.getFisID());
                            }

                            Set<EnrCompetitionType> competitionTypes = extracts.stream().map(enrEnrollmentExtract -> ((EnrEnrollmentParagraph) enrEnrollmentExtract.getParagraph()).getCompetitionType()).collect(Collectors.toSet());

                            if (competitionTypes.size() == 1)
                            {
                                fisOrder.setFinanceSourceID(finSourceDao.getSource(competitionTypes.iterator().next()).getFisID());
                            }

                            Set<EduLevel> eduLevels = extracts.stream().map(enrEnrollmentExtract -> ((EnrEnrollmentParagraph) enrEnrollmentExtract.getParagraph()).getProgramSubject().getSubjectIndex().getProgramKind().getEduLevel()).collect(Collectors.toSet());

                            if (eduLevels.size() == 1)
                            {
                                EnrFisCatalogItem fisCatalogItem = programSet.getEnrFisItem4EducationLevel(extracts.get(0).getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet());
                                fisOrder.setEducationLevelID(fisCatalogItem.getFisID());
                            }

                            Integer stageInt = null;

                            if (EnrRequestTypeCodes.BS.equals(order.getRequestType().getCode()) && CollectionUtils.exists(forms, eduProgramForm -> EduProgramFormCodes.OCHNAYA.equals(eduProgramForm.getCode()) || EduProgramFormCodes.OCHNO_ZAOCHNAYA.equals(eduProgramForm.getCode())))
                            {
                                Calendar calendar = Calendar.getInstance();

                                calendar.set(2016, Calendar.AUGUST, 3);
                                Date enrollmentDate3Aug = CommonBaseDateUtil.trimTime(calendar.getTime());
                                calendar.set(2016, Calendar.AUGUST, 8);
                                Date enrollmentDate8Aug = CommonBaseDateUtil.trimTime(calendar.getTime());
                                calendar.set(2016, Calendar.JULY, 22);
                                Date enrollmentDate22Jul = CommonBaseDateUtil.trimTime(calendar.getTime());
                                calendar.set(2016, Calendar.JULY, 27);
                                Date enrollmentDate27Jul = CommonBaseDateUtil.trimTime(calendar.getTime());

                                if (enrollmentDate22Jul.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())) || enrollmentDate3Aug.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())))
                                {
                                    stageInt = 1;
                                } else if (enrollmentDate27Jul.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())) || enrollmentDate8Aug.equals(CommonBaseDateUtil.trimTime(order.getCommitDate())))
                                {
                                    stageInt = 2;
                                } else
                                {
                                    stageInt = 0;
                                }
                            }

                            if (stageInt != null)
                            {
                                fisOrder.setStage(stageInt.longValue());
                            }
                            root.getPackageData().getOrders().getOrdersOfException().getOrderOfException().add(fisOrder);
                        });

            }

            if (allExtracts.size() > 0)
            {
                something = true;
                if (root.getPackageData().getOrders() == null)
                {
                    root.getPackageData().setOrders(new PackageData.Orders());
                }
                root.getPackageData().getOrders().setApplications(new PackageData.Orders.Applications());

                for (EnrAbstractExtract extract : allExtracts)
                {
                    EnrRequestedCompetition requestedCompetition = extract.getRequestedCompetition();
                    EnrCompetition competition = requestedCompetition.getCompetition();
                    EnrOrder order = (EnrOrder) extract.getParagraph().getOrder();

                    PackageData.Orders.Applications.Application fisApplication = new PackageData.Orders.Applications.Application();
                    fisApplication.setApplicationUID(uid(extract.getRequestedCompetition().getRequest()));
                    fisApplication.setOrderUID(uid(order));

                    if (extract instanceof EnrEnrollmentExtract)
                    {
                        fisApplication.setOrderTypeID(1L);
                    } else if (extract instanceof EnrCancelExtract)
                    {
                        fisApplication.setOrderTypeID(2L);
                    } else
                    {
                        throw new IllegalArgumentException();
                    }

                    fisApplication.setCompetitiveGroupUID(competitionGroupUid(competition));

                    if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(competition.getType().getCompensationType().getCode()))
                    {
                        fisApplication.setOrderIdLevelBudget(fisLevelBudget);
                    }

                    if (requestedCompetition.getCompetition().isNoExams())
                    {
                        fisApplication.setBenefitKindID(1L);
                    }
                    else if (requestedCompetition.getCompetition().isExclusive())
                    {
                        fisApplication.setBenefitKindID(4L);
                    }
                    else if (requestedCompetition.getRequest().getBenefitCategory() != null)
                    {
                        fisApplication.setBenefitKindID(5L);
                    }

                    if(extract instanceof EnrCancelExtract)
                    {
                        fisApplication.setIsDisagreedDate(requestedCompetition.getRefusedDate() != null ? xmlDateTime(requestedCompetition.getRefusedDate(), dateTimeFunction) : xmlDateTime(order.getCommitDate(), dateTimeFunction));
                    }

                    root.getPackageData().getOrders().getApplications().getApplication().add(fisApplication);
                }
            }
        }

        if (!something) {
            fisSession.error(PREFIX, EnrFisSyncSessionManager.instance().getProperty("error.orders.no-applications"));
            return null;
        }

        return root;
    }

    private long getFinSourceId(EnrCompetition competition)
    {
        switch (competition.getCompTypeCode()) {
            // для вида приема выбранного конкурса «без ВИ (КЦП)», «общий» передаем «14»,
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL: return 14;
            case EnrCompetitionTypeCodes.MINISTERIAL: return 14;
            // для «особые права» — «20»,
            case EnrCompetitionTypeCodes.EXCLUSIVE: return 20;
            // для «ЦП» — «16»,
            case EnrCompetitionTypeCodes.TARGET_ADMISSION: return 16;
            // для «без ВИ (договор)», «договор» — «15».
            default: return 15;
        }
    }

    private MultiKey key(EnrFisSettingsECPeriodItem item)
    {
        EnrFisCatalogItem educationForm = item.getKey().getEducationForm();
        EnrFisCatalogItem educationLevel = item.getKey().getEducationLevel();
        EnrFisCatalogItem educationSource = item.getKey().getEducationSource();
        Date orderDate = item.getDateOrder();
        return key(educationForm, educationLevel, educationSource.getFisID(), orderDate);
    }

    private MultiKey key(EnrFisCatalogItem educationForm, EnrFisCatalogItem educationLevel, long finSourceId, Date orderDate)
    {
        return new MultiKey(educationForm.getFisID(), educationLevel.getFisID(), finSourceId, orderDate);
    }
}
