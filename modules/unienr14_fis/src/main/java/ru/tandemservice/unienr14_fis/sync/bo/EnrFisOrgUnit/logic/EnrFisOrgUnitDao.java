package ru.tandemservice.unienr14_fis.sync.bo.EnrFisOrgUnit.logic;

import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade.CacheEntryDefinition;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrFisOrgUnitDao extends UniBaseDao implements IEnrFisOrgUnitDao {

    private final CacheEntryDefinition<Long, Boolean> CACHR_IS_ENR_FIS_ORG_UNIT = new CacheEntryDefinition<Long, Boolean>("EnrFisOrgUnitDao.isEnrFisOrgUnit", 128, false) {
        @Override public void fill(final Map<Long, Boolean> cache, final Collection<Long> ids) {
            final Set<Long> selectedIds = new HashSet<>(new DQLSelectBuilder()
                .fromEntity(EnrOrgUnit.class, "ou")
                .predicate(DQLPredicateType.distinct)
                .column(property(EnrOrgUnit.institutionOrgUnit().orgUnit().id().fromAlias("ou")))
                .where(in(property(EnrOrgUnit.institutionOrgUnit().orgUnit().id().fromAlias("ou")), ids))
                .where(or(
                    eq(property("ou", EnrOrgUnit.fisDataSendingIndependent()), value(Boolean.TRUE)),
                    isNull(property("ou", EnrOrgUnit.institutionOrgUnit().orgUnit().parent()))
                ))
                .createStatement(getSession()).<Long>list());

            for (final Long id: ids) {
                cache.put(id, selectedIds.contains(id));
            }
        }
    };

    @Override
    public boolean isEnrFisOrgUnit(final OrgUnit orgUnit) {
        return DaoCacheFacade.getEntry(CACHR_IS_ENR_FIS_ORG_UNIT).getRecords(Collections.singleton(orgUnit.getId())).get(orgUnit.getId());
    }

}
