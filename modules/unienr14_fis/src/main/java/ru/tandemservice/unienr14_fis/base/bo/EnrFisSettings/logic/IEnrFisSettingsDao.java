package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author vdanilov
 */
public interface IEnrFisSettingsDao extends INeedPersistenceSupport {
//
//    /**
//     * @param enrollmentCampaign
//     * @return Список НП, которые надо передавать в ФИС
//     */
//    Set<EnrDirection> getEnrDirectionSet(EnrEnrollmentCampaign enrollmentCampaign);
//
//    /**
//     * @param enrollmentCampaign
//     * @return Список кНП, которые надо передавать в ФИС
//     */
//    List<EnrDirectionCompetition> getEnrDirectionCompetitionList(EnrEnrollmentCampaign enrollmentCampaign);
//
//
    /**
     * По направлениям приема ПК создает недостающие ключи
     * @param ec
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    List<EnrFisSettingsECPeriodKey> doCreateEnrFisSettingsECPeriodKeys(EnrEnrollmentCampaign ec);

    /**
     * По направлениям приема ПК создает недостающие ключи (только для переданного подразделения)
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    List<EnrFisSettingsECPeriodKey> doCreateEnrFisSettingsECPeriodKeys(EnrOrgUnit enrOrgUnit);

    /**
     *
     * @param key
     * @param itemList
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveEnrFisSettingsECPeriodKey(EnrFisSettingsECPeriodKey key, List<EnrFisSettingsECPeriodItem> itemList);

    /**
     *
     * @param key
     * @param itemList
     * @param errorCollector
     * @return true, если ошибок валидации нет
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean doValidateEnrFisSettingsECPeriodKey(EnrFisSettingsECPeriodKey key, List<EnrFisSettingsECPeriodItem> itemList, ErrorCollector errorCollector);

    /**
     * Конкурсы для передачи в ФИС. Для головного берутся также конкурсы тех подразделений, которые сами не передают данные в ФИС (по настройке реквизитов).
     * @param enrOrgUnit подразделение, ведущее прием
     * @return конкурсы для передачи в ФИС от этого подразделения
     */
    public List<EnrCompetition> getCompetitions(EnrOrgUnit enrOrgUnit);

    /**
     * Заполняет настройку «Минимальные баллы ЕГЭ для предоставления особых прав» по старой настройке
     * @param enrCampaignId id приемной кампании
     */
    void doMigrateStateExamFromOldSetting(Long enrCampaignId);
}
