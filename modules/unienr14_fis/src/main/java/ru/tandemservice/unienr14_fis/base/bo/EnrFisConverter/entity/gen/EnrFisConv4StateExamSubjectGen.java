package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Общеобразовательные предметы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisConv4StateExamSubjectGen extends EntityBase
 implements INaturalIdentifiable<EnrFisConv4StateExamSubjectGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject";
    public static final String ENTITY_NAME = "enrFisConv4StateExamSubject";
    public static final int VERSION_HASH = 707252646;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String L_VALUE = "value";

    private EnrStateExamSubject _stateExamSubject;     // Предмет ЕГЭ
    private EnrFisCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrStateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     */
    public void setStateExamSubject(EnrStateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EnrFisCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisConv4StateExamSubjectGen)
        {
            if (withNaturalIdProperties)
            {
                setStateExamSubject(((EnrFisConv4StateExamSubject)another).getStateExamSubject());
            }
            setValue(((EnrFisConv4StateExamSubject)another).getValue());
        }
    }

    public INaturalId<EnrFisConv4StateExamSubjectGen> getNaturalId()
    {
        return new NaturalId(getStateExamSubject());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisConv4StateExamSubjectGen>
    {
        private static final String PROXY_NAME = "EnrFisConv4StateExamSubjectNaturalProxy";

        private Long _stateExamSubject;

        public NaturalId()
        {}

        public NaturalId(EnrStateExamSubject stateExamSubject)
        {
            _stateExamSubject = ((IEntity) stateExamSubject).getId();
        }

        public Long getStateExamSubject()
        {
            return _stateExamSubject;
        }

        public void setStateExamSubject(Long stateExamSubject)
        {
            _stateExamSubject = stateExamSubject;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisConv4StateExamSubjectGen.NaturalId) ) return false;

            EnrFisConv4StateExamSubjectGen.NaturalId that = (NaturalId) o;

            if( !equals(getStateExamSubject(), that.getStateExamSubject()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStateExamSubject());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStateExamSubject());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisConv4StateExamSubjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisConv4StateExamSubject.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisConv4StateExamSubject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((EnrStateExamSubject) value);
                    return;
                case "value":
                    obj.setValue((EnrFisCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "stateExamSubject":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "stateExamSubject":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "stateExamSubject":
                    return EnrStateExamSubject.class;
                case "value":
                    return EnrFisCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisConv4StateExamSubject> _dslPath = new Path<EnrFisConv4StateExamSubject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisConv4StateExamSubject");
    }
            

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject#getStateExamSubject()
     */
    public static EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject#getValue()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EnrFisConv4StateExamSubject> extends EntityPath<E>
    {
        private EnrStateExamSubject.Path<EnrStateExamSubject> _stateExamSubject;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject#getStateExamSubject()
     */
        public EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new EnrStateExamSubject.Path<EnrStateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4StateExamSubject#getValue()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> value()
        {
            if(_value == null )
                _value = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EnrFisConv4StateExamSubject.class;
        }

        public String getEntityName()
        {
            return "enrFisConv4StateExamSubject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
