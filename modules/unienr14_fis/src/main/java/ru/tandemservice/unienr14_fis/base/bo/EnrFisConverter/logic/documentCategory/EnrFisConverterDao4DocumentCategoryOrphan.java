/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.EnrFisConv4DocumentCategoryOrphan;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.IEnrFisConv4DocumentCategory;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.documentCategory.base.EnrFisConverterDao4DocumentCategory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 13.07.2016
 */
public class EnrFisConverterDao4DocumentCategoryOrphan extends EnrFisConverterDao4DocumentCategory
{
    @Override
    public String getCatalogCode()
    {
        return "42";
    }

    protected List<PersonDocumentCategory> getEnrDocumentCategory(String entrantDocTypeCode)
    {
        return super.getEnrDocumentCategory(PersonDocumentTypeCodes.ORPHAN);
    }

    protected Class<? extends IEnrFisConv4DocumentCategory> getEnrFisConv4DocumentCategoryClass()
    {
        return EnrFisConv4DocumentCategoryOrphan.class;
    }

    @Override
    public void update(final Map<PersonDocumentCategory, EnrFisCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        List<EnrFisConv4DocumentCategoryOrphan> targetRecords = values.entrySet().stream()
                .filter(e -> null != e.getValue())
                .map(e -> new EnrFisConv4DocumentCategoryOrphan(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EnrFisConv4DocumentCategoryOrphan.NaturalId, EnrFisConv4DocumentCategoryOrphan>()
        {
            @Override protected EnrFisConv4DocumentCategoryOrphan.NaturalId key(final EnrFisConv4DocumentCategoryOrphan source) { return (EnrFisConv4DocumentCategoryOrphan.NaturalId) source.getNaturalId(); }
            @Override protected EnrFisConv4DocumentCategoryOrphan buildRow(final EnrFisConv4DocumentCategoryOrphan source) { return new EnrFisConv4DocumentCategoryOrphan(source.getDocCategory(), source.getValue()); }
            @Override protected void fill(final EnrFisConv4DocumentCategoryOrphan target, final EnrFisConv4DocumentCategoryOrphan source) { target.update(source, false); }
            @Override protected void doDeleteRecord(final EnrFisConv4DocumentCategoryOrphan databaseRecord) { if (clearNulls) { super.doDeleteRecord(databaseRecord); } }
        }.merge(this.getList(EnrFisConv4DocumentCategoryOrphan.class), targetRecords);
    }
}