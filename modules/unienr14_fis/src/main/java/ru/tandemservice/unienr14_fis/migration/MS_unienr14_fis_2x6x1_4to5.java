package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisCatalogItem

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_catalog_item_t", "enr14fis_catalog_item_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4Country

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_conv_country_t", "enr14fis_conv_country_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4ExamSetElement

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_conv_examsetelement_t", "enr14fis_conv_examsetelement_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4Olympiad

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_conv_olympiad_t", "enr14fis_conv_olympiad_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4ProgramSet

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_conv_ps_t", "enr14fis_conv_ps_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisEntrantSyncData

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_entrant_data_t", "enr14fis_entrant_data_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSettingsECPeriodItem

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_s_period_item_t", "enr14fis_s_period_item_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSettingsECPeriodKey

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_s_period_key_t", "enr14fis_s_period_key_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackage

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_t", "enr14fis_package_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackage4AOrdersInfo

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_4orders_t", "enr14fis_package_4orders_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackage4AdmissionInfo

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_2admission_t", "enr14fis_package_2adm_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackage4ApplicationsInfo

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_3applications_t", "enr14fis_package_3apps_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackage4CampaignInfo

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_1campaign_t", "enr14fis_package_1camp_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackageIOLog

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_iolog_t", "enr14fis_package_iolog_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackageReceiveIOLog

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_r_iolog_t", "enr14fis_package_r_iolog_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncPackageSendIOLog

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_package_s_iolog_t", "enr14fis_package_s_iolog_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncSession

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("enrfis_session_t", "enr14fis_session_t");

		}


    }
}