/* $Id$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.examSubject;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverterDao;

/**
 * @author Ekaterina Zvereva
 * @since 16.08.2016
 */
public interface IEnrFisConverterDao4StateExamSubject extends IEnrFisConverterDao<EnrStateExamSubject>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync();
}
