package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic;

import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
public interface IEnrFisConverterStaticDao<T extends IEntity> extends IEnrFisConverterDao<T> {

    /**
     * проверяет, что title совпадает с сохраненным в коде
     * @return title
     */
    String checkTitle(String code, String title);

}
