package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.*;

/**
 * Пакет 3: Заявления абитуриентов
 *
 * Пакет 3: Заявления абитуриентов
 * Root / PackageData / Applications
 */
public class EnrFisSyncPackage4ApplicationsInfo extends EnrFisSyncPackage4ApplicationsInfoGen
{
    @Override public String getTypeTitle() {
        return "Пакет 3: Заявления абитуриентов";
    }
    @Override public String getTypeNumber() {
        return "3";
    }
}