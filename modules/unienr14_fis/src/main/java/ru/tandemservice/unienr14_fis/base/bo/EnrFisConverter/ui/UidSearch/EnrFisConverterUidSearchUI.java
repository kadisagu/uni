/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.UidSearch;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;

/**
 * @author oleyba
 * @since 7/14/14
 */
public class EnrFisConverterUidSearchUI extends UIPresenter
{
    private String uid;
    private IFisUidByIdOwner entity;

    @Override
    public void onComponentRefresh()
    {

    }

    public String getEntityTitle() {
        if (getEntity() == null) return "";
        return getEntity().getFisUidTitle();
    }

    public void onClickApply() {
        setEntity(null);
        if (getUid() == null) return;

        String[] split = getUid().split("_");
        if (split.length != 3) return;

        String hex = split[1];
        try {
            Long id = Long.parseLong(hex, 16);
            setEntity((IFisUidByIdOwner) IUniBaseDao.instance.get().get(id));
        } catch (NumberFormatException | ClassCastException e) {
            // ну и не нашли значит
        }
    }

    // getters and setters


    public IFisUidByIdOwner getEntity()
    {
        return entity;
    }

    public void setEntity(IFisUidByIdOwner entity)
    {
        this.entity = entity;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }
}