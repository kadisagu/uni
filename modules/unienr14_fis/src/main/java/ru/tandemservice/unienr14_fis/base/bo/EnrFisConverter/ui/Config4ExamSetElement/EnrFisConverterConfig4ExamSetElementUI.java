/**
 *$Id: EnrFisConverterConfig4ExamSetElementUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4ExamSetElement;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;

import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfigUI;

/**
 * @author Alexander Shaburov
 * @since 18.07.13
 */
public class EnrFisConverterConfig4ExamSetElementUI extends EnrFisConverterConfigUI
{
    private EnrEnrollmentCampaign _enrCampaign;

    public boolean _withoutFisCatalogs;

    public boolean isWithoutFisCatalogs()
    {
        return _withoutFisCatalogs;
    }

    public void setWithoutFisCatalogs(boolean withoutFisCatalogs)
    {
        _withoutFisCatalogs = withoutFisCatalogs;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        _withoutFisCatalogs = !ISharedBaseDao.instance.get().existsEntity(EnrFisCatalogItem.class);
    }

    public void onClickAutoSync()
    {
        EnrFisConverterManager.instance().examSetElement().doAutoSync(_enrCampaign);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrFisConverterConfig4ExamSetElement.BIND_ENROLLMENT_CAMPAIGN, _enrCampaign);
    }

    public void onRefreshSelectors()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    // Getters & Setters

    public boolean isEmptySelectors()
    {
        return _enrCampaign == null;
    }

    // Accessors

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }
}
