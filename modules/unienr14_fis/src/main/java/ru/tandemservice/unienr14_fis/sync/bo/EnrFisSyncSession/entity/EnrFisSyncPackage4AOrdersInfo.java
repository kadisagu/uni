package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.*;

/**
 * Пакет 4: Приказы по абитуриентам
 *
 * Пакет 4: Приказы по абитуриентам
 * Root / PackageData / OrdersOfAdmission
 */
public class EnrFisSyncPackage4AOrdersInfo extends EnrFisSyncPackage4AOrdersInfoGen
{
    @Override public String getTypeTitle() {
        return "Пакет 4: Приказы по абитуриентам";
    }
    @Override public String getTypeNumber() {
        return "4";
    }
}