package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Даты приемной кампании ФИС (ключ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrFisSettingsECPeriodKeyGen extends EntityBase
 implements INaturalIdentifiable<EnrFisSettingsECPeriodKeyGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey";
    public static final String ENTITY_NAME = "enrFisSettingsECPeriodKey";
    public static final int VERSION_HASH = -1894363051;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String L_EDUCATION_LEVEL = "educationLevel";
    public static final String L_EDUCATION_FORM = "educationForm";
    public static final String L_EDUCATION_SOURCE = "educationSource";
    public static final String P_SIZE = "size";
    public static final String P_SIZE_TITLE = "sizeTitle";
    public static final String P_TITLE = "title";

    private EnrOrgUnit _enrOrgUnit;     // Подразделение в рамках ПК
    private EnrFisCatalogItem _educationLevel;     // Уровень образования ФИС
    private EnrFisCatalogItem _educationForm;     // Форма обучения ФИС
    private EnrFisCatalogItem _educationSource;     // Источник финансирования ФИС
    private int _size;     // Количество этапов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение в рамках ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrOrgUnit getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Подразделение в рамках ПК. Свойство не может быть null.
     */
    public void setEnrOrgUnit(EnrOrgUnit enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Уровень образования ФИС. Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel Уровень образования ФИС. Свойство не может быть null.
     */
    public void setEducationLevel(EnrFisCatalogItem educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    /**
     * @return Форма обучения ФИС. Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getEducationForm()
    {
        return _educationForm;
    }

    /**
     * @param educationForm Форма обучения ФИС. Свойство не может быть null.
     */
    public void setEducationForm(EnrFisCatalogItem educationForm)
    {
        dirty(_educationForm, educationForm);
        _educationForm = educationForm;
    }

    /**
     * @return Источник финансирования ФИС. Свойство не может быть null.
     */
    @NotNull
    public EnrFisCatalogItem getEducationSource()
    {
        return _educationSource;
    }

    /**
     * @param educationSource Источник финансирования ФИС. Свойство не может быть null.
     */
    public void setEducationSource(EnrFisCatalogItem educationSource)
    {
        dirty(_educationSource, educationSource);
        _educationSource = educationSource;
    }

    /**
     * 0 - нет приема
     * 1 - прием без этапов (1 единственный этап)
     * 2,3,... - число этапов
     *
     * @return Количество этапов. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Количество этапов. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrFisSettingsECPeriodKeyGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrOrgUnit(((EnrFisSettingsECPeriodKey)another).getEnrOrgUnit());
                setEducationLevel(((EnrFisSettingsECPeriodKey)another).getEducationLevel());
                setEducationForm(((EnrFisSettingsECPeriodKey)another).getEducationForm());
                setEducationSource(((EnrFisSettingsECPeriodKey)another).getEducationSource());
            }
            setSize(((EnrFisSettingsECPeriodKey)another).getSize());
        }
    }

    public INaturalId<EnrFisSettingsECPeriodKeyGen> getNaturalId()
    {
        return new NaturalId(getEnrOrgUnit(), getEducationLevel(), getEducationForm(), getEducationSource());
    }

    public static class NaturalId extends NaturalIdBase<EnrFisSettingsECPeriodKeyGen>
    {
        private static final String PROXY_NAME = "EnrFisSettingsECPeriodKeyNaturalProxy";

        private Long _enrOrgUnit;
        private Long _educationLevel;
        private Long _educationForm;
        private Long _educationSource;

        public NaturalId()
        {}

        public NaturalId(EnrOrgUnit enrOrgUnit, EnrFisCatalogItem educationLevel, EnrFisCatalogItem educationForm, EnrFisCatalogItem educationSource)
        {
            _enrOrgUnit = ((IEntity) enrOrgUnit).getId();
            _educationLevel = ((IEntity) educationLevel).getId();
            _educationForm = ((IEntity) educationForm).getId();
            _educationSource = ((IEntity) educationSource).getId();
        }

        public Long getEnrOrgUnit()
        {
            return _enrOrgUnit;
        }

        public void setEnrOrgUnit(Long enrOrgUnit)
        {
            _enrOrgUnit = enrOrgUnit;
        }

        public Long getEducationLevel()
        {
            return _educationLevel;
        }

        public void setEducationLevel(Long educationLevel)
        {
            _educationLevel = educationLevel;
        }

        public Long getEducationForm()
        {
            return _educationForm;
        }

        public void setEducationForm(Long educationForm)
        {
            _educationForm = educationForm;
        }

        public Long getEducationSource()
        {
            return _educationSource;
        }

        public void setEducationSource(Long educationSource)
        {
            _educationSource = educationSource;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrFisSettingsECPeriodKeyGen.NaturalId) ) return false;

            EnrFisSettingsECPeriodKeyGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrOrgUnit(), that.getEnrOrgUnit()) ) return false;
            if( !equals(getEducationLevel(), that.getEducationLevel()) ) return false;
            if( !equals(getEducationForm(), that.getEducationForm()) ) return false;
            if( !equals(getEducationSource(), that.getEducationSource()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrOrgUnit());
            result = hashCode(result, getEducationLevel());
            result = hashCode(result, getEducationForm());
            result = hashCode(result, getEducationSource());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrOrgUnit());
            sb.append("/");
            sb.append(getEducationLevel());
            sb.append("/");
            sb.append(getEducationForm());
            sb.append("/");
            sb.append(getEducationSource());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrFisSettingsECPeriodKeyGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrFisSettingsECPeriodKey.class;
        }

        public T newInstance()
        {
            return (T) new EnrFisSettingsECPeriodKey();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "educationLevel":
                    return obj.getEducationLevel();
                case "educationForm":
                    return obj.getEducationForm();
                case "educationSource":
                    return obj.getEducationSource();
                case "size":
                    return obj.getSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((EnrOrgUnit) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((EnrFisCatalogItem) value);
                    return;
                case "educationForm":
                    obj.setEducationForm((EnrFisCatalogItem) value);
                    return;
                case "educationSource":
                    obj.setEducationSource((EnrFisCatalogItem) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "educationLevel":
                        return true;
                case "educationForm":
                        return true;
                case "educationSource":
                        return true;
                case "size":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "educationLevel":
                    return true;
                case "educationForm":
                    return true;
                case "educationSource":
                    return true;
                case "size":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrOrgUnit":
                    return EnrOrgUnit.class;
                case "educationLevel":
                    return EnrFisCatalogItem.class;
                case "educationForm":
                    return EnrFisCatalogItem.class;
                case "educationSource":
                    return EnrFisCatalogItem.class;
                case "size":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrFisSettingsECPeriodKey> _dslPath = new Path<EnrFisSettingsECPeriodKey>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrFisSettingsECPeriodKey");
    }
            

    /**
     * @return Подразделение в рамках ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEnrOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Уровень образования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEducationLevel()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    /**
     * @return Форма обучения ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEducationForm()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> educationForm()
    {
        return _dslPath.educationForm();
    }

    /**
     * @return Источник финансирования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEducationSource()
     */
    public static EnrFisCatalogItem.Path<EnrFisCatalogItem> educationSource()
    {
        return _dslPath.educationSource();
    }

    /**
     * 0 - нет приема
     * 1 - прием без этапов (1 единственный этап)
     * 2,3,... - число этапов
     *
     * @return Количество этапов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getSizeTitle()
     */
    public static SupportedPropertyPath<String> sizeTitle()
    {
        return _dslPath.sizeTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrFisSettingsECPeriodKey> extends EntityPath<E>
    {
        private EnrOrgUnit.Path<EnrOrgUnit> _enrOrgUnit;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _educationLevel;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _educationForm;
        private EnrFisCatalogItem.Path<EnrFisCatalogItem> _educationSource;
        private PropertyPath<Integer> _size;
        private SupportedPropertyPath<String> _sizeTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение в рамках ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEnrOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Уровень образования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEducationLevel()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

    /**
     * @return Форма обучения ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEducationForm()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> educationForm()
        {
            if(_educationForm == null )
                _educationForm = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_EDUCATION_FORM, this);
            return _educationForm;
        }

    /**
     * @return Источник финансирования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getEducationSource()
     */
        public EnrFisCatalogItem.Path<EnrFisCatalogItem> educationSource()
        {
            if(_educationSource == null )
                _educationSource = new EnrFisCatalogItem.Path<EnrFisCatalogItem>(L_EDUCATION_SOURCE, this);
            return _educationSource;
        }

    /**
     * 0 - нет приема
     * 1 - прием без этапов (1 единственный этап)
     * 2,3,... - число этапов
     *
     * @return Количество этапов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EnrFisSettingsECPeriodKeyGen.P_SIZE, this);
            return _size;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getSizeTitle()
     */
        public SupportedPropertyPath<String> sizeTitle()
        {
            if(_sizeTitle == null )
                _sizeTitle = new SupportedPropertyPath<String>(EnrFisSettingsECPeriodKeyGen.P_SIZE_TITLE, this);
            return _sizeTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrFisSettingsECPeriodKeyGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrFisSettingsECPeriodKey.class;
        }

        public String getEntityName()
        {
            return "enrFisSettingsECPeriodKey";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getSizeTitle();

    public abstract String getTitle();
}
