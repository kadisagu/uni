package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_fis_2x10x4_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4DocumentCategoryBelongingNation

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14fis_conv_doc_c_bn_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_9464ca58"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("doccategory_id", DBType.LONG).setNullable(false), 
				new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrFisConv4DocumentCategoryBelongingNation");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4DocumentCategoryCombatVeterans

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14fis_conv_doc_c_cv_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_44c0f7fa"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("doccategory_id", DBType.LONG).setNullable(false), 
				new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrFisConv4DocumentCategoryCombatVeterans");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisConv4DocumentCategorySportsDiploma

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14fis_conv_doc_c_sd_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_a2f20f51"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("doccategory_id", DBType.LONG).setNullable(false), 
				new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrFisConv4DocumentCategorySportsDiploma");
		}
    }
}