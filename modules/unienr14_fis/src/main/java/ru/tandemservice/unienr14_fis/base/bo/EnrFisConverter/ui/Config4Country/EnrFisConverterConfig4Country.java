/* $Id:$ */
package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.Config4Country;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.fias.base.entity.AddressCountry;

import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.country.IEnrFisConverterDao4Country;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.ui.EnrFisConverterConfig;

/**
 * @author oleyba
 * @since 6/23/14
 */
@Configuration
public class EnrFisConverterConfig4Country extends EnrFisConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    protected IEnrFisConverterDao4Country getConverterDao() {
        return EnrFisConverterManager.instance().addressCountry();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint fisConverterRecordDS() {
        return this.columnListExtPointBuilder(FIS_CONVERTER_RECORD_DS)
        .addColumn(textColumn("title", AddressCountry.P_TITLE).create())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").permissionKey("enr14FisConverterConfig4CountryEdit").hasBlockHeader(true).create())
        .create();
    }


    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterRecordDSHandler() {
        final AbstractSearchDataSourceHandler handler = (AbstractSearchDataSourceHandler) super.fisConverterRecordDSHandler();
        return handler.setPageable(false);
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrollmentCampaign = context.get(BIND_ENROLLMENT_CAMPAIGN);
        if (null == enrollmentCampaign) { return new ArrayList<>(0); }

        // фильтруем по селектору ПК
        // todo фильтровать страны, а то замумукаются сопоставлять
        return entityList;
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> fisConverterValueDSHandler() {
        return super.fisConverterValueDSHandler();
    }
}
