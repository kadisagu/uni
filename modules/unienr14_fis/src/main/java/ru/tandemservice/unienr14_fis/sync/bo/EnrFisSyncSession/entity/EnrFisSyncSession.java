package ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.zip.GZipCompressionInterface;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.gen.EnrFisSyncSessionGen;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.IEnrFisSyncSessionFillStatus;
import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.logic.PkgDaoBase;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Paths;
import java.util.Date;

/**
 * Сессия синхронизации ФИС
 */
public class EnrFisSyncSession extends EnrFisSyncSessionGen implements ITitled
{
    protected static Logger initFileLogger(final Class<?> klass, final String name) {
        final Logger logger = Logger.getLogger(klass);
        try {
            final FileAppender appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), Paths.get(System.getProperty("catalina.base"), "logs", name + ".log").toString());
            appender.setName("file-"+name+"-appender");
            appender.setThreshold(Level.INFO);
            logger.addAppender(appender);
            logger.setAdditivity(false);
            logger.setLevel(Level.INFO);
            return logger;
        } catch (final Throwable t) {
            logger.error(t.getMessage(), t);
            return null;
        }
    }

    public static final Logger log = initFileLogger(EnrFisSyncSession.class, "enrFis-session");
    public static final GZipCompressionInterface zip = GZipCompressionInterface.INSTANCE;
    public static final DateFormatter DATE_FORMATTER_LOG = new DateFormatter("yyyyMMdd HH:mm:ss,SSS");
    public static final DateFormatter DATE_FORMATTER_FILE = new DateFormatter("yyyyMMddHHmmssSSS");
    public static final DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yyyy HH:mm:ss");


    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getCampaignInfoStatus() == null) {
            return this.getClass().getSimpleName();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(getCreationDateAsString());
        sb.append(" (").append(getCampaignInfoStatus().getTitle());
        // if (isSkipAdmissionInfo()) {
        //     sb.append(", без второго пакета");
        // }
        sb.append(")");
        return sb.toString();
    }

    @Override
    @EntityDSLSupport
    public String getCreationDateAsString() {
        return DATE_FORMATTER.format(getCreationDate());
    }

    @Override
    @EntityDSLSupport
    public String getArchiveDateAsString() {
        return DATE_FORMATTER.format(getArchiveDate());
    }


    @Override
    @EntityDSLSupport
    public String getStateTitle() {
        if (isArchived()) { return "Обработка завершена"; }
        if (isQueued()) { return "Обрабатывается"; }
        return "";
    }


    @Override
    @EntityDSLSupport
    public boolean isQueued() {
        return (null != this.getQueueDate());
    }

    @Override
    @EntityDSLSupport
    public boolean isArchived() {
        return (null != this.getArchiveDate());
    }

    @Override
    @EntityDSLSupport
    public boolean isAlive() {
        return (isQueued()) && !isArchived();
    }

    @Override
    @EntityDSLSupport
    public boolean isQueueDisabled() {
        return isQueued() || isArchived();
    }

    @Override
    @EntityDSLSupport
    public boolean isArchiveDisabled() {
        return isArchived();
    }

    @Override
    @EntityDSLSupport
    public boolean isDeleteDisabled() {
        return !isArchived();
    }

    @Override
    @EntityDSLSupport
    public String getDateLog()
    {
        StringBuilder l = new StringBuilder();

        if (null != getCreationDate()) {
            if (l.length() > 0) { l.append("\n"); }
            l.append("Создание: ").append(getCreationDateAsString());
        }

        if (null != getFillDate()) {
            if (l.length() > 0) { l.append("\n"); }
            l.append("Заполнение: ").append(DATE_FORMATTER.format(getFillDate()));
        } else if (null == getArchiveDate()) {
            // только по тем сессиям, которые еще не заполнены (и не в архиве)
            IEnrFisSyncSessionFillStatus status = PkgDaoBase.getFillStatus(getId());
            if (null != status) {
                if (l.length() > 0) { l.append("\n"); }
                l.append("Заполнение: ").append(status.getStageName()).append(" - ").append(status.getProgress()).append('%');
            }
        }

        if (null != getQueueDate()) {
            if (l.length() > 0) { l.append("\n"); }
            l.append("Постановка в очередь: ").append(DATE_FORMATTER.format(getQueueDate()));
        }

        if (null != getArchiveDate()) {
            if (l.length() > 0) { l.append("\n"); }
            l.append("Завершение обработки: ").append(DATE_FORMATTER.format(getArchiveDate()));
        }

        return l.toString();
    }

    @Override
    @EntityDSLSupport
    public String getContentStatus() {
        if (null == getFillDate()) { return ""; }

        StringBuilder status = new StringBuilder();
        if (IUniBaseDao.instance.get().existsEntity(EnrFisSyncPackage4CampaignInfo.class, EnrFisSyncPackage4CampaignInfo.session().s(), this)) {
            status.append("Пакет 1: включен");
        }
        if (IUniBaseDao.instance.get().existsEntity(EnrFisSyncPackage4AdmissionInfo.class, EnrFisSyncPackage4AdmissionInfo.session().s(), this)) {
            status.append("\nПакет 2: включен");
        } else if (isSendAdmissionInfo()) {
            status.append("\nПакет 2: не включен, проверьте журнал");
        }
        if (IUniBaseDao.instance.get().existsEntity(EnrFisSyncPackage4ApplicationsInfo.class, EnrFisSyncPackage4ApplicationsInfo.session().s(), this)) {
            status.append("\nПакет 3: включен");
            if (isForceAllEntrants()) {
                status.append("\nПринудительно включены в пакеты все абитуриенты");
            }
            if (getSkippedEntrantCount() > 0) {
                status.append("\nНе может быть отправлено в ФИС абитуриентов: ").append(getSkippedEntrantCount());
            }
        } else if (isSendApplicationsInfo()) {
            status.append("\nПакет 3: не включен, проверьте журнал");
        }
        if (IUniBaseDao.instance.get().existsEntity(EnrFisSyncPackage4AOrdersInfo.class, EnrFisSyncPackage4AOrdersInfo.session().s(), this)) {
            status.append("\nПакет 4: включен");
            if (getSkippedExtractCount() > 0) {
                status.append("\nНе может быть отправлено в ФИС выписок: ").append(getSkippedExtractCount());
            }
        } else if (isSendOrdersOfAdmission()) {
            status.append("\nПакет 4: не включен, проверьте журнал");
        }
        return status.toString();
    }


    public byte[] getLog() {
        try {
            return zip.decompress(this.getZipLog());
        } catch (Exception e) {
            return "Не удалось открыть сохраненный файл лога.".getBytes();
        }
    }
    public void setLog(final byte[] log) {
        this.setZipLog(zip.compress(log));
    }

    public String getLogString() {
        final byte[] log = getLog();
        return (null == log ? null : new String(log));
    }
    public void setLogString(final String log) {
        this.setLog(log.getBytes());
    }

    @Override
    public byte[] getZipLog() {
        synchronized (this) {
            if (null != logBuffer) {
                try {
                    final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    final BufferedOutputStream stream = new BufferedOutputStream(bytes, 1024);
                    byte[] log = zip.decompress(super.getZipLog());
                    if (null != log) { stream.write(log); }
                    stream.write(logBuffer.toString().getBytes());
                    stream.flush();
                    this.setLog(bytes.toByteArray());
                    logBuffer = null;
                } catch (final Throwable t) {
                    log.error(t.getMessage(), t);
                }
            }
        }
        return super.getZipLog();
    }


    private StringBuilder logBuffer = null;
    private void log(final String level, final String prefix, final Object message, final Throwable t)
    {
        // инициализируем лог при ображении
        initLazyForGet("zipLog");

        synchronized (this) {
            if (null == logBuffer) { logBuffer = new StringBuilder(); }
            logBuffer
            .append(DATE_FORMATTER_LOG.format(new Date())).append(' ')
            .append(level).append(" - ")
            .append(prefix).append(": ")
            .append(message);

            if (null != t) {
                try {
                    final StringWriter w = new StringWriter();
                    final PrintWriter printer = new PrintWriter(w);
                    t.printStackTrace(printer);
                    printer.flush();
                    logBuffer.append('\n').append(w.toString());
                } catch (Throwable tt) {
                    log.error(tt.getMessage(), tt);
                }
            }

            logBuffer.append('\n');
        }
    }

    public void info(final String prefix, final Object message, final Throwable t) {
        log("I", prefix, message, t);
        log.info(message(prefix, message, t), t);
    }
    public void info(final String prefix, final Object message) {
        this.info(prefix, message, null);
    }

    public void warn(final String prefix, final Object message, final Throwable t) {
        log("W", prefix, message, t);
        log.warn(message(prefix, message, t), t);
    }
    public void warn(final String prefix, final Object message) {
        this.warn(prefix, message, null);
    }

    public void error(final String prefix, final Object message, final Throwable t) {
        log("E", prefix, message, t);
        log.error(message(prefix, message, t), t);
    }
    public void error(final String prefix, final Object message) {
        this.error(prefix, message, null);
    }

    protected String message(final String prefix, final Object message, final Throwable t) {
        if (null == t) { return (Long.toHexString(getId())+" "+prefix+" "+message); }
        return (Long.toHexString(getId())+" "+prefix+" "+message+" "+t.getMessage());
    }

    public boolean isSendEnrCampaignInfo()
    {
        return !isSkipEnrCampaignInfo();
    }

    public void setSendEnrCampaignInfo(boolean sendEnrCampaignInfo)
    {
        setSkipEnrCampaignInfo(!sendEnrCampaignInfo);
    }

    public boolean isSendAdmissionInfo()
    {
        return !isSkipAdmissionInfo();
    }

    public void setSendAdmissionInfo(boolean sendAdmissionInfo)
    {
        setSkipAdmissionInfo(!sendAdmissionInfo);
    }

    public boolean isSendApplicationsInfo()
    {
        return !isSkipApplicationsInfo();
    }

    public void setSendApplicationsInfo(boolean sendApplicationsInfo)
    {
        setSkipApplicationsInfo(!sendApplicationsInfo);
    }

    public boolean isSendOrdersOfAdmission()
    {
        return !isSkipOrdersOfAdmission();
    }

    public void setSendOrdersOfAdmission(boolean sendOrdersOfAdmission)
    {
        setSkipOrdersOfAdmission(!sendOrdersOfAdmission);
    }

    public boolean isSkipRequestInOrderInfo()
    {
        return !isSendRequestInOrderInfo();
    }
}