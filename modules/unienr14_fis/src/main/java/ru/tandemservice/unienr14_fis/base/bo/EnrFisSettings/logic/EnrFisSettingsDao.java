package ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.logic;

import org.hibernate.Session;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.EnrFisConverterManager;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.IEnrFisConverter;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.logic.eduOu.IEnrFisConverterDao4ProgramSet;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMark;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsStateExamMinMarkNew;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrFisSettingsDao extends UniBaseDao implements IEnrFisSettingsDao {

    @Override
    public List<EnrFisSettingsECPeriodKey> doCreateEnrFisSettingsECPeriodKeys(final EnrEnrollmentCampaign ec)
    {
        List<EnrFisSettingsECPeriodKey> result = new ArrayList<>();
        for (EnrOrgUnit enrOrgUnit : getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), ec)) {
            result.addAll(doCreateEnrFisSettingsECPeriodKeys(enrOrgUnit));
        }
        return result;
    }

    @Override
    public List<EnrFisSettingsECPeriodKey> doCreateEnrFisSettingsECPeriodKeys(final EnrOrgUnit enrOrgUnit)
    {
        List<EnrCompetition> competitions = enrOrgUnit.isTop() || enrOrgUnit.isFisDataSendingIndependent() ? getCompetitions(enrOrgUnit) : Collections.<EnrCompetition>emptyList();

        final InfoCollector infoCollector = (ContextLocal.isRun() ? ContextLocal.getInfoCollector() : null);

        final IEnrFisConverterDao4ProgramSet eduOuDao = EnrFisConverterManager.instance().programSet();
        final IEnrFisConverter<EduProgramForm> eduFormConverter = EnrFisConverterManager.instance().educationForm().getConverter();
        final Map<EnrCompetition, EnrFisCatalogItem> finSourceMap = EnrFisConverterManager.instance().financingSource().getSourceMap(competitions);

        final Map<INaturalId, EnrFisSettingsECPeriodKey> target = new HashMap<>();
        for (final EnrCompetition competition : competitions) {
            if(!EnrCompetitionTypeCodes.EXCLUSIVE.equals(competition.getType().getCode()))
            {
                final EnrFisCatalogItem fisEduLvl = eduOuDao.getEnrFisItem4EducationLevel(competition.getProgramSetOrgUnit().getProgramSet());
                if (null == fisEduLvl)
                {
                    String message = "Направление приема «" + competition.getTitle() + "» не было обработано (не удалось определить уровень образования ФИС).";
                    if (null != infoCollector)
                    {
                        infoCollector.add(message);
                    }
                    continue;
                }

                final EnrFisCatalogItem fisEduForm = eduFormConverter.getCatalogItem(competition.getProgramSetOrgUnit().getProgramSet().getProgramForm(), false);
                if (null == fisEduForm)
                {
                    String message = "Направление приема «" + competition.getTitle() + "» не было обработано (не удалось определить форму обучения ФИС).";
                    if (null != infoCollector)
                    {
                        infoCollector.add(message);
                    }
                    continue;
                }

                final EnrFisCatalogItem educationSource = finSourceMap.get(competition);
                if (null == educationSource)
                {
                    String message = "Направление приема «" + competition.getTitle() + "» не было обработано (не удалось определить источники финансирования ФИС).";
                    if (null != infoCollector)
                    {
                        infoCollector.add(message);
                    }
                    continue;
                }

                final EnrFisSettingsECPeriodKey key = new EnrFisSettingsECPeriodKey();
                key.setEnrOrgUnit(enrOrgUnit);
                key.setEducationLevel(fisEduLvl);
                key.setEducationForm(fisEduForm);
                key.setEducationSource(educationSource);
                target.put(key.getNaturalId(), key);
            }
        }

        List<EnrFisSettingsECPeriodKey> mergeResult = new MergeAction.SessionMergeAction<INaturalId, EnrFisSettingsECPeriodKey>() {
            @Override
            protected INaturalId key(final EnrFisSettingsECPeriodKey source) {
                return source.getNaturalId();
            }

            @Override
            protected EnrFisSettingsECPeriodKey buildRow(final EnrFisSettingsECPeriodKey source) {
                return source;
            }

            @Override
            protected void fill(final EnrFisSettingsECPeriodKey target, final EnrFisSettingsECPeriodKey source) {
            /**/
            }
        }.merge(this.getList(EnrFisSettingsECPeriodKey.class, EnrFisSettingsECPeriodKey.enrOrgUnit(), enrOrgUnit), target.values());

        return mergeResult;

    }

    @Override
    public List<EnrCompetition> getCompetitions(EnrOrgUnit enrOrgUnit)
    {
        List<EnrCompetition> competitions = getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().orgUnit(), enrOrgUnit);
        if (enrOrgUnit.isTop()) {
            List<EnrCompetition> list = new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "c")
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign()), value(enrOrgUnit.getEnrollmentCampaign())))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().orgUnit().fisDataSendingIndependent()), value(Boolean.FALSE)))
                .where(isNotNull(property("c", EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().parent())))
                .createStatement(this.getSession()).list();

            competitions.addAll(list);
        }
        return competitions;
    }


    @Override
    public void doSaveEnrFisSettingsECPeriodKey(final EnrFisSettingsECPeriodKey key, final List<EnrFisSettingsECPeriodItem> itemList)
    {
        if (key.getSize() != itemList.size()) {
            throw new IllegalStateException("key.size ≠ itemList.size");
        }

        for (int i=0; i<key.getSize(); i++) {
            final EnrFisSettingsECPeriodItem item = itemList.get(i);
            if (!key.equals(item.getKey())) {
                throw new IllegalStateException("∃ i ∊ itemList | i.key ≠ key");
            }
            if (item.getStage() != (1+i)) {
                throw new IllegalStateException("∃ i ∊ N | itemList[i].stage ≠ 1+i");
            }
        }

        saveOrUpdate(key);
        new MergeAction.SessionMergeAction<INaturalId, EnrFisSettingsECPeriodItem>() {
            @Override protected INaturalId key(final EnrFisSettingsECPeriodItem source) { return source.getNaturalId(); }
            @Override protected EnrFisSettingsECPeriodItem buildRow(final EnrFisSettingsECPeriodItem source) { return new EnrFisSettingsECPeriodItem(source); }
            @Override protected void fill(final EnrFisSettingsECPeriodItem target, final EnrFisSettingsECPeriodItem source) { target.update(source, false); }
        }.merge(this.getList(EnrFisSettingsECPeriodItem.class, EnrFisSettingsECPeriodItem.key(), key), itemList);

    }

    @Override
    public boolean doValidateEnrFisSettingsECPeriodKey(final EnrFisSettingsECPeriodKey key, final List<EnrFisSettingsECPeriodItem> itemList, final ErrorCollector errorCollector)
    {
        boolean ok = true;
        for (final EnrFisSettingsECPeriodItem item : itemList) {
            if (item.getDateEnd().getTime() < item.getDateStart().getTime()) {
                errorCollector.add("«Окончание приема» должна быть больше либо равна дате «Начало приема».", "date1_" + item.getId(), "date2_" + item.getId());
                ok = false;
            }
        }
        return ok;
    }

    @Override
    public void doMigrateStateExamFromOldSetting(Long enrCampaignId)
    {
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "MigrateStateExamFromOldSettingLock_" + enrCampaignId);

        DQLSelectBuilder markOldBuilder = new DQLSelectBuilder().fromEntity(EnrFisSettingsStateExamMinMark.class, "markOld")
                .where(eq(property("markOld", EnrFisSettingsStateExamMinMark.enrollmentCampaign().id()), value(enrCampaignId)));
        for(EnrFisSettingsStateExamMinMark markOld : this.<EnrFisSettingsStateExamMinMark>getList(markOldBuilder))
        {
            List<EnrProgramSetBase> programSets = getList(new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "ps")
                    .where(eq(property("ps", EnrProgramSetBase.enrollmentCampaign().id()), value(enrCampaignId)))
                    .where(eq(property("ps", EnrProgramSetBase.programSubject()), value(markOld.getEduProgramSubject()))));
            EnrFisCatalogItem subject = getByNaturalId(new EnrFisCatalogItem.NaturalId("1", markOld.getStateExamSubject().getSubjectFISCode()));

            if(subject == null) continue;

            EnrBenefitType benefitType;

            if (markOld.getDiscipline() == null)
            {
                benefitType = getCatalogItem(EnrBenefitType.class, EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS);

            } else
            {
                benefitType = getCatalogItem(EnrBenefitType.class, EnrBenefitTypeCodes.MAX_EXAM_RESULT);
            }

            if(benefitType == null) continue;

            for(EnrProgramSetBase programSetBase : programSets)
            {
                if(!existsEntity(new DQLSelectBuilder().fromEntity(EnrFisSettingsStateExamMinMarkNew.class, "markNew")
                        .where(eq(property("markNew", EnrFisSettingsStateExamMinMarkNew.enrProgramSetBase()), value(programSetBase)))
                        .where(eq(property("markNew", EnrFisSettingsStateExamMinMarkNew.subject()), value(subject)))
                        .where(eq(property("markNew", EnrFisSettingsStateExamMinMarkNew.benefitType()), value(benefitType)))
                        .buildQuery()))
                {
                    EnrFisSettingsStateExamMinMarkNew markNew = new EnrFisSettingsStateExamMinMarkNew();
                    markNew.setEnrProgramSetBase(programSetBase);
                    markNew.setSubject(subject);
                    markNew.setBenefitType(benefitType);
                    markNew.setMinMark(markOld.getMinMark());

                    session.save(markNew);
                }
            }
        }
    }
}
