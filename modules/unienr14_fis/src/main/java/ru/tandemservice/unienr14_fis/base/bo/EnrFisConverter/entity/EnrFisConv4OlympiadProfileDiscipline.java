package ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadProfileDiscipline;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.*;

/** @see ru.tandemservice.unienr14_fis.base.bo.EnrFisConverter.entity.gen.EnrFisConv4OlympiadProfileDisciplineGen */
public class EnrFisConv4OlympiadProfileDiscipline extends EnrFisConv4OlympiadProfileDisciplineGen
{

    public EnrFisConv4OlympiadProfileDiscipline()
    {
    }

    public EnrFisConv4OlympiadProfileDiscipline(EnrOlympiadProfileDiscipline subject, EnrFisCatalogItem fisCatalogItem)
    {
        setOlympiadProfileDiscipline(subject);
        setValue(fisCatalogItem);
    }
}