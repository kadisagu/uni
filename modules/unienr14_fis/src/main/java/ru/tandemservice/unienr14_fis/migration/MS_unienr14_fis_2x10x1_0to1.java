package ru.tandemservice.unienr14_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_fis_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrFisSyncSession

		// создано обязательное свойство eduProgramUniqueTitleForming
		{
			// создать колонку
			tool.createColumn("enr14fis_session_t", new DBColumn("eduprogramuniquetitleforming_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultEduProgramUniqueTitleForming = false;
			tool.executeUpdate("update enr14fis_session_t set eduprogramuniquetitleforming_p=? where eduprogramuniquetitleforming_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14fis_session_t", "eduprogramuniquetitleforming_p", false);
		}
    }
}