package ru.tandemservice.unienr14_fis.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


/**
 * @author vdanilov
 */
public interface IEnrFisService {

    public static final SpringBeanCache<IEnrFisService> instance = new SpringBeanCache<IEnrFisService>(IEnrFisService.class.getName());

    interface EnrFisConnection {

        byte[] call(final byte[] request);
        <T> T call(Class<T> responseKlass, final byte[] request);
        <T> T call(Class<T> responseKlass, final Object root);

        String getRequestRaw();
        String getRequest();

        String getResponseRaw();
        String getResponse();

        URL getUrl();

        void writeDebugToLogFile(String postfix);

    }

    /** @return true, если параметры передачи заполнены */
    boolean isEnabled();

    /** @return интерфейс запроса к ФИС (по указанному сервису) */
    EnrFisConnection connect(String urlPostfix);

    // обертка для сообщения об ошибке <Error>...</Error> из ФИС
    @SuppressWarnings("unchecked")
    class EnrFisError extends RuntimeException
    {
        private static final long serialVersionUID = 1L;

        // TODO: использовать это для парсинга ошибок (такие ошибки не должны влиять на количество попыток)
        private static final Set<String> ERROR_MESSAGE__TIMEOUT_ERROR__LOWER_CASE = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
            "Пакет еще не обработан".toLowerCase()
        )));

        private final Object errorObject;
        public <T> T getErrorObject() { return (T)this.errorObject; }

        private final String errorCode;
        public String getErrorCode() { return this.errorCode; }

        private final boolean timeoutError;
        public boolean isTimeoutError() { return timeoutError; }

        public boolean isFatal() {
            // здесь может быть более сложная логика
//            return !isTimeoutError();
            return false;
        }

        public EnrFisError(Object error, String errorCode, String errorText) {
            super(errorCode+": " + errorText);
            this.errorObject = error;
            this.errorCode = errorCode;

            String errorMessageLowerCase = StringUtils.trimToEmpty(errorText).toLowerCase();
            this.timeoutError = ERROR_MESSAGE__TIMEOUT_ERROR__LOWER_CASE.contains(errorMessageLowerCase);
        }

    }


}
