package ru.tandemservice.unienr14_fis.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_fis_2x6x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.createTable(new DBTable("enrfis_package_r_iolog_t",
            new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
            new DBColumn("failurecount_p", DBType.INTEGER).setNullable(false),
            new DBColumn("conflictcount_p", DBType.INTEGER).setNullable(false),
            new DBColumn("successfulcount_p", DBType.INTEGER).setNullable(false)
        ));
        short recvEntityCode = tool.entityCodes().ensure("enrFisSyncPackageReceiveIOLog");

        tool.createTable(new DBTable("enrfis_package_s_iolog_t",
            new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
            new DBColumn("datanodecount_p", DBType.INTEGER).setNullable(false)
        ));
        short sendEntityCode = tool.entityCodes().ensure("enrFisSyncPackageSendIOLog");

        tool.table("enrfis_package_iolog_t").triggers().clear();
        tool.table("enrfis_package_iolog_t").constraints().clear();
        tool.table("enrfis_package_t").triggers().clear();
        tool.table("enrfis_package_t").constraints().clear();


        // переносим данные из таблицы enrfis_package_iolog_t
        Statement statement = tool.getConnection().createStatement();
        statement.execute("select id, operationUrl_p from enrfis_package_iolog_t");

        PreparedStatement insertSend = tool.prepareStatement("insert into enrfis_package_s_iolog_t (id, datanodecount_p) values (?, ?)");
        PreparedStatement insertRecv = tool.prepareStatement("insert into enrfis_package_r_iolog_t (id, failurecount_p, conflictcount_p, successfulcount_p) values (?, ?, ?, ?)");
        PreparedStatement updateBase = tool.prepareStatement("update enrfis_package_iolog_t set id=?, discriminator=? where id=?");

        PreparedStatement updatePackage1 = tool.prepareStatement("update enrfis_package_t set pkgSendSuccess_id=? where pkgSendSuccess_id=?");
        PreparedStatement updatePackage2 = tool.prepareStatement("update enrfis_package_t set pkgRecvSuccess_id=? where pkgRecvSuccess_id=?");
        PreparedStatement[] updagePackages = new PreparedStatement[] {updatePackage1, updatePackage2};

        ResultSet srs = statement.getResultSet();

        while (srs.next()) {
            Long id = srs.getLong(1);
            String operationUrl = srs.getString(2);

            boolean send = "import".equals(StringUtils.substringAfterLast(operationUrl, "/"));
            short entityCode = send ? sendEntityCode : recvEntityCode;
            Long newId = EntityIDGenerator.generateNewId(entityCode);

            updateBase.clearParameters();
            updateBase.setLong(1, newId);
            updateBase.setShort(2, entityCode);
            updateBase.setLong(3, id);
            updateBase.execute();

            for (PreparedStatement updatePackage : updagePackages) {
                updatePackage.clearParameters();
                updatePackage.setLong(1, newId);
                updatePackage.setLong(2, id);
                updatePackage.execute();
            }

            PreparedStatement insert = send ? insertSend : insertRecv;
            insert.clearParameters();
            insert.setLong(1, newId);
            insert.setInt(2, 0);

            if (!send) {
                insert.setInt(3, 0);
                insert.setInt(4, 0);
            }

            insert.execute();
        }
    }
}