//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.05 at 04:07:17 PM YEKT 
//


package enr14fis.schema.pkg.del.result.req;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getResultDeleteApplication",
    "authData"
})
@XmlRootElement(name = "Root")
public class Root {

    @XmlElement(name = "GetResultDeleteApplication", required = true)
    protected GetResultDeleteApplication getResultDeleteApplication;
    @XmlElement(name = "AuthData", required = true)
    protected AuthData authData;

    /**
     * Gets the value of the getResultDeleteApplication property.
     * 
     * @return
     *     possible object is
     *     {@link GetResultDeleteApplication }
     *     
     */
    public GetResultDeleteApplication getGetResultDeleteApplication() {
        return getResultDeleteApplication;
    }

    /**
     * Sets the value of the getResultDeleteApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetResultDeleteApplication }
     *     
     */
    public void setGetResultDeleteApplication(GetResultDeleteApplication value) {
        this.getResultDeleteApplication = value;
    }

    /**
     * Gets the value of the authData property.
     * 
     * @return
     *     possible object is
     *     {@link AuthData }
     *     
     */
    public AuthData getAuthData() {
        return authData;
    }

    /**
     * Sets the value of the authData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthData }
     *     
     */
    public void setAuthData(AuthData value) {
        this.authData = value;
    }

}
