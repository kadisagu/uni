//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.05 at 04:07:15 PM YEKT 
//


package enr14fis.schema.offline.send;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import java.util.Date;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the enrfis.schema.pkg.result.req package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{
    private final static QName _enrollmentCampaignId_QNAME = new QName("", "enrollmentCampaignId");
    private final static QName _enrollmentCampaignTitle_QNAME = new QName("", "enrollmentCampaignTitle");
    private final static QName _orgUnitId_QNAME = new QName("", "orgUnitId");
    private final static QName _orgUnitTitle_QNAME = new QName("", "orgUnitTitle");
    private final static QName _login_QNAME = new QName("", "login");
    private final static QName _password_QNAME = new QName("", "password");

    private final static QName _sessionId_QNAME = new QName("", "sessionId");
    private final static QName _sessionTitle_QNAME = new QName("", "sessionTitle");
    private final static QName _log_QNAME = new QName("", "log");
    private final static QName _creationDate_QNAME = new QName("", "creationDate");
    private final static QName _fillDate_QNAME = new QName("", "fillDate");
    private final static QName _queueDate_QNAME = new QName("", "queueDate");
    private final static QName _archiveDate_QNAME = new QName("", "archiveDate");
    private final static QName _skipAdmissionInfo_QNAME = new QName("", "skipAdmissionInfo");
    private final static QName _forceAllEntrants_QNAME = new QName("", "forceAllEntrants");
    private final static QName _skipEnrCampaignInfo_QNAME = new QName("", "skipEnrCampaignInfo");
    private final static QName _skipApplicationsInfo_QNAME = new QName("", "skipApplicationsInfo");
    private final static QName _skipOrdersOfAdmission_QNAME = new QName("", "skipOrdersOfAdmission");
    private final static QName _skipIllegalDirections_QNAME = new QName("", "skipIllegalDirections");
    private final static QName _skipErrorEntrants_QNAME = new QName("", "skipErrorEntrants");
    private final static QName _skippedEntrantCount_QNAME = new QName("", "skippedEntrantCount");
    private final static QName _skippedExtractCount_QNAME = new QName("", "skippedExtractCount");
    private final static QName _skippedRecItemCount_QNAME = new QName("", "skippedRecItemCount");

    private final static QName _packageId_QNAME = new QName("", "packageId");
    private final static QName _packageTitle_QNAME = new QName("", "packageTitle");
    private final static QName _xml_QNAME = new QName("", "xml");
    private final static QName _comment_QNAME = new QName("", "comment");
    private final static QName _fisPackageID_QNAME = new QName("", "fisPackageID");
    private final static QName _fileName_QNAME = new QName("", "fileName");

    private final static QName _operationDate_QNAME = new QName("", "operationDate");
    private final static QName _operationUrl_QNAME = new QName("", "operationUrl");
    private final static QName _xmlRequest_QNAME = new QName("", "xmlRequest");
    private final static QName _xmlResponse_QNAME = new QName("", "xmlResponse");
    private final static QName _fisErrorText_QNAME = new QName("", "fisErrorText");
    private final static QName _error_QNAME = new QName("", "error");
    private final static QName _errorLog_QNAME = new QName("", "errorLog");
    private final static QName _failureCount_QNAME = new QName("", "failureCount");
    private final static QName _conflictCount_QNAME = new QName("", "conflictCount");
    private final static QName _successfulCount_QNAME = new QName("", "successfulCount");
    private final static QName _dataNodeCount_QNAME = new QName("", "dataNodeCount");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: enrfis.schema.pkg.result.req
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EnrFisOfflineSessionListData }
     * 
     */
    public EnrFisOfflineSessionListData createEnrFisOfflineSessionListData() {
        return new EnrFisOfflineSessionListData();
    }

    /**
     * Create an instance of {@link EnrFisOfflineSessionData }
     * 
     */
    public EnrFisOfflineSessionData createEnrFisOfflineSessionData() {
        return new EnrFisOfflineSessionData();
    }

    /**
     * Create an instance of {@link EnrFisOfflinePacakageData }
     * 
     */
    public EnrFisOfflinePacakageData createEnrFisOfflinePacakageData() {
        return new EnrFisOfflinePacakageData();
    }

    /**
     * Create an instance of {@link EnrFisOfflinePackageSendIOLogData }
     *
     */
    public EnrFisOfflinePackageSendIOLogData createEnrFisOfflinePackageSendIOLogData()
    {
        return new EnrFisOfflinePackageSendIOLogData();
    }

    /**
     * Create an instance of {@link EnrFisOfflinePackageReceiveIOLogData }
     *
     */
    public EnrFisOfflinePackageReceiveIOLogData createEnrFisOfflinePackageRecieveIOLogData()
    {
        return new EnrFisOfflinePackageReceiveIOLogData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "enrollmentCampaignId")
    public JAXBElement<Long> createEnrollmentCampaignId(Long value) {
        return new JAXBElement<Long>(_enrollmentCampaignId_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "enrollmentCampaignTitle")
    public JAXBElement<String> createEnrollmentCampaignTitle(String value) {
        return new JAXBElement<String>(_enrollmentCampaignTitle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "orgUnitId")
    public JAXBElement<Long> createOrgUnitId(Long value) {
        return new JAXBElement<Long>(_orgUnitId_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "orgUnitTitle")
    public JAXBElement<String> createOrgUnitTitle(String value) {
        return new JAXBElement<String>(_orgUnitTitle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "login")
    public JAXBElement<String> createLogin(String value) {
        return new JAXBElement<String>(_login_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_password_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "sessionId")
    public JAXBElement<Long> createSessionId(Long value) {
        return new JAXBElement<Long>(_sessionId_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "sessionTitle")
    public JAXBElement<String> createSessionTitle(String value) {
        return new JAXBElement<String>(_sessionTitle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "log")
    public JAXBElement<String> createLog(String value) {
        return new JAXBElement<String>(_log_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "creationDate")
    public JAXBElement<Date> createCreationDate(Date value) {
        return new JAXBElement<Date>(_creationDate_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "fillDate")
    public JAXBElement<Date> createFillDate(Date value) {
        return new JAXBElement<Date>(_fillDate_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "queueDate")
    public JAXBElement<Date> createQueueDate(Date value) {
        return new JAXBElement<Date>(_queueDate_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "archiveDate")
    public JAXBElement<Date> createArchiveDate(Date value) {
        return new JAXBElement<Date>(_archiveDate_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skipAdmissionInfo")
    public JAXBElement<Boolean> createSkipAdmissionInfo(Boolean value) {
        return new JAXBElement<Boolean>(_skipAdmissionInfo_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "forceAllEntrants")
    public JAXBElement<Boolean> createForceAllEntrants(Boolean value) {
        return new JAXBElement<Boolean>(_forceAllEntrants_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skipEnrCampaignInfo")
    public JAXBElement<Boolean> createSkipEnrCampaignInfo(Boolean value) {
        return new JAXBElement<Boolean>(_skipEnrCampaignInfo_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skipApplicationsInfo")
    public JAXBElement<Boolean> createSkipApplicationsInfo(Boolean value) {
        return new JAXBElement<Boolean>(_skipApplicationsInfo_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skipOrdersOfAdmission")
    public JAXBElement<Boolean> createSkipOrdersOfAdmission(Boolean value) {
        return new JAXBElement<Boolean>(_skipOrdersOfAdmission_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skipIllegalDirections")
    public JAXBElement<Boolean> createSkipIllegalDirections(Boolean value) {
        return new JAXBElement<Boolean>(_skipIllegalDirections_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skipErrorEntrants")
    public JAXBElement<Boolean> createSkipErrorEntrants(Boolean value) {
        return new JAXBElement<Boolean>(_skipErrorEntrants_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skippedEntrantCount")
    public JAXBElement<Integer> createSkippedEntrantCount(Integer value) {
        return new JAXBElement<Integer>(_skippedEntrantCount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skippedExtractCount")
    public JAXBElement<Integer> createSkippedExtractCount(Integer value) {
        return new JAXBElement<Integer>(_skippedExtractCount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "skippedRecItemCount")
    public JAXBElement<Integer> createSkippedRecItemCount(Integer value) {
        return new JAXBElement<Integer>(_skippedRecItemCount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "packageId")
    public JAXBElement<Long> createPackageId(Long value) {
        return new JAXBElement<Long>(_packageId_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "packageTitle")
    public JAXBElement<String> createPackageTitle(String value) {
        return new JAXBElement<String>(_packageTitle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "xml")
    public JAXBElement<String> createXml(String value) {
        return new JAXBElement<String>(_xml_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "comment")
    public JAXBElement<String> createComment(String value) {
        return new JAXBElement<String>(_comment_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "fisPackageID")
    public JAXBElement<Long> createFisPackageID(Long value) {
        return new JAXBElement<Long>(_fisPackageID_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "fileName")
    public JAXBElement<String> createFileName(String value) {
        return new JAXBElement<String>(_fileName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "operationDate")
    public JAXBElement<Date> createOperationDate(Date value) {
        return new JAXBElement<Date>(_operationDate_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "operationUrl")
    public JAXBElement<String> createOperationUrl(String value) {
        return new JAXBElement<String>(_operationUrl_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "xmlRequest")
    public JAXBElement<String> createXmlRequest(String value) {
        return new JAXBElement<String>(_xmlRequest_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "xmlResponse")
    public JAXBElement<String> createXmlResponse(String value) {
        return new JAXBElement<String>(_xmlResponse_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "fisErrorText")
    public JAXBElement<String> createFisErrorText(String value) {
        return new JAXBElement<String>(_fisErrorText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "error")
    public JAXBElement<Boolean> createError(Boolean value) {
        return new JAXBElement<Boolean>(_error_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "errorLog")
    public JAXBElement<String> createErrorLog(String value) {
        return new JAXBElement<String>(_errorLog_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "failureCount")
    public JAXBElement<Integer> createFailureCount(Integer value) {
        return new JAXBElement<Integer>(_failureCount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "conflictCount")
    public JAXBElement<Integer> createConflictCount(Integer value) {
        return new JAXBElement<Integer>(_conflictCount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "successfulCount")
    public JAXBElement<Integer> createSuccessfulCount(Integer value) {
        return new JAXBElement<Integer>(_successfulCount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "dataNodeCount")
    public JAXBElement<Integer> createDataNodeCount(Integer value) {
        return new JAXBElement<Integer>(_dataNodeCount_QNAME, Integer.class, null, value);
    }
}
