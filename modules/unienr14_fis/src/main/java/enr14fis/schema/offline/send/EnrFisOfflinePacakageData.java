/* $Id:$ */
package enr14fis.schema.offline.send;

import javax.xml.bind.annotation.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 25.07.15
 * Time: 2:27
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnrFisOfflinePacakageData")
public class EnrFisOfflinePacakageData
{
    @XmlElement(name = "packageId", required = true)
    protected Long packageId;

    @XmlElement(name = "packageTitle", required = true)
    protected String packageTitle;

    @XmlElement(name = "xml")
    protected String xml;

    @XmlElement(name = "comment")
    protected String comment;

    @XmlElement(name = "creationDate")
    protected Date creationDate;

    @XmlElement(name = "fisPackageID")
    protected Long fisPackageID;

    @XmlElement(name = "fileName")
    protected String fileName;

    @XmlElement(name = "pkgSendSuccess")
    protected EnrFisOfflinePackageSendIOLogData pkgSendSuccess;

    @XmlElement(name = "pkgRecvSuccess")
    protected EnrFisOfflinePackageReceiveIOLogData pkgRecvSuccess;

    public Long getPackageId()
    {
        return packageId;
    }

    public void setPackageId(Long packageId)
    {
        this.packageId = packageId;
    }

    public String getPackageTitle()
    {
        return packageTitle;
    }

    public void setPackageTitle(String packageTitle)
    {
        this.packageTitle = packageTitle;
    }

    public String getXml()
    {
        return xml;
    }

    public void setXml(String xml)
    {
        this.xml = xml;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public Long getFisPackageID()
    {
        return fisPackageID;
    }

    public void setFisPackageID(Long fisPackageID)
    {
        this.fisPackageID = fisPackageID;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public EnrFisOfflinePackageSendIOLogData getPkgSendSuccess()
    {
        return pkgSendSuccess;
    }

    public void setPkgSendSuccess(EnrFisOfflinePackageSendIOLogData pkgSendSuccess)
    {
        this.pkgSendSuccess = pkgSendSuccess;
    }

    public EnrFisOfflinePackageReceiveIOLogData getPkgRecvSuccess()
    {
        return pkgRecvSuccess;
    }

    public void setPkgRecvSuccess(EnrFisOfflinePackageReceiveIOLogData pkgRecvSuccess)
    {
        this.pkgRecvSuccess = pkgRecvSuccess;
    }
}
