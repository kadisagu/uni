/* $Id:$ */
package enr14fis.schema.offline.send;

import javax.xml.bind.annotation.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 27.07.15
 * Time: 6:58
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnrFisOfflinePackageReceiveIOLogData")
public class EnrFisOfflinePackageReceiveIOLogData
{
    @XmlElement(name = "operationDate")
    protected Date operationDate;     // Время проведения операции

    @XmlElement(name = "operationUrl")
    protected String operationUrl;     // Адрес, через который проводилась операция

    @XmlElement(name = "xmlRequest")
    protected String xmlRequest;     // ZIP-XML: отправленные данные в ФИС

    @XmlElement(name = "xmlResponse")
    protected String xmlResponse;     // ZIP-XML: ответ из ФИС

    @XmlElement(name = "fisErrorText")
    protected String fisErrorText;     // Сообщение об ошибке ФИС

    @XmlElement(name = "error")
    protected Boolean error;     // Ошибка взаимодействия

    @XmlElement(name = "errorLog")
    protected String errorLog;     // ZIP: полный лог ошибки

    @XmlElement(name = "failureCount")
    protected Integer failureCount;     // Число тегов Failure

    @XmlElement(name = "conflictCount")
    protected Integer conflictCount;     // Число тегов Conflict

    @XmlElement(name = "successfulCount")
    protected Integer successfulCount;     // Число тегов Successful

    public Date getOperationDate()
    {
        return operationDate;
    }

    public void setOperationDate(Date operationDate)
    {
        this.operationDate = operationDate;
    }

    public String getOperationUrl()
    {
        return operationUrl;
    }

    public void setOperationUrl(String operationUrl)
    {
        this.operationUrl = operationUrl;
    }

    public String getXmlRequest()
    {
        return xmlRequest;
    }

    public void setXmlRequest(String xmlRequest)
    {
        this.xmlRequest = xmlRequest;
    }

    public String getXmlResponse()
    {
        return xmlResponse;
    }

    public void setXmlResponse(String xmlResponse)
    {
        this.xmlResponse = xmlResponse;
    }

    public String getFisErrorText()
    {
        return fisErrorText;
    }

    public void setFisErrorText(String fisErrorText)
    {
        this.fisErrorText = fisErrorText;
    }

    public Boolean getError()
    {
        return error;
    }

    public void setError(Boolean error)
    {
        this.error = error;
    }

    public String getErrorLog()
    {
        return errorLog;
    }

    public void setErrorLog(String errorLog)
    {
        this.errorLog = errorLog;
    }

    public Integer getFailureCount()
    {
        return failureCount;
    }

    public void setFailureCount(Integer failureCount)
    {
        this.failureCount = failureCount;
    }

    public Integer getConflictCount()
    {
        return conflictCount;
    }

    public void setConflictCount(Integer conflictCount)
    {
        this.conflictCount = conflictCount;
    }

    public Integer getSuccessfulCount()
    {
        return successfulCount;
    }

    public void setSuccessfulCount(Integer successfulCount)
    {
        this.successfulCount = successfulCount;
    }
}
