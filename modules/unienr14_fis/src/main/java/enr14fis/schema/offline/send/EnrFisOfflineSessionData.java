/* $Id:$ */
package enr14fis.schema.offline.send;

import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_fis.base.bo.EnrFisCatalog.entity.EnrFisCatalogItem;

import javax.xml.bind.annotation.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 25.07.15
 * Time: 2:27
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnrFisOfflineSessionData")
public class EnrFisOfflineSessionData
{
    @XmlElement(name = "sessionId", required = true)
    protected Long sessionId;

    @XmlElement(name = "sessionTitle", required = true)
    protected String sessionTitle;

    @XmlElement(name = "log")
    protected String log;

    @XmlElement(name = "creationDate")
    protected Date creationDate;

    @XmlElement(name = "fillDate")
    protected Date fillDate;

    @XmlElement(name = "queueDate")
    protected Date queueDate;

    @XmlElement(name = "archiveDate")
    protected  Date archiveDate;     // Дата помещения сессии в архив

    @XmlElement(name = "skipAdmissionInfo")
    protected  Boolean skipAdmissionInfo;     // Не передавать сведения об объеме и структуре приема

    @XmlElement(name = "forceAllEntrants")
    protected  Boolean forceAllEntrants;     // Выгружать заявления всех абитуриентов

    @XmlElement(name = "acceptedInfo")
    protected  Boolean acceptedInfo;     // Передавать данные о согласиях на зачисление

    @XmlElement(name = "sendRequestInOrderInfo")
    protected  Boolean sendRequestInOrderInfo;     // Пропускать заявления ФИС, в которых есть хотя бы один выбранный конкурс в состоянии «Зачислен».

    @XmlElement(name = "skipEnrCampaignInfo")
    protected  Boolean skipEnrCampaignInfo;     // Не формировать пакет с данными приемной кампании

    @XmlElement(name = "skipApplicationsInfo")
    protected  Boolean skipApplicationsInfo;     // Не формировать пакеты с заявлениями абитуриентов

    @XmlElement(name = "requestState")
    protected  Integer requestState;     // Состояние неотозванных заявлений

    @XmlElement(name = "skipOrdersOfAdmission")
    protected  Boolean skipOrdersOfAdmission;     // Не формировать пакет с приказами

    @XmlElement(name = "skipIllegalDirections")
    protected  Boolean skipIllegalDirections;     // Пропускать направления, не имеющие аналогов в ФИС

    @XmlElement(name = "skipErrorEntrants")
    protected  Boolean skipErrorEntrants;     // Пропускать абитуриентов с ошибками

    @XmlElement(name = "skippedEntrantCount")
    protected  Integer skippedEntrantCount;     // Число пропущенных при формировании 3 пакета абитуриентов

    @XmlElement(name = "skippedExtractCount")
    protected  Integer skippedExtractCount;     // Число пропущенных при формировании 4 пакета выписок

    @XmlElement(name = "skippedRecItemCount")
    protected  Integer skippedRecItemCount;     // Число пропущенных при формировании 4 пакета рекомендованных

    @XmlElement(name = "campaignInfo")
    protected EnrFisOfflinePacakageData campaignInfo;

    @XmlElement(name = "admissionInfo")
    protected EnrFisOfflinePacakageData admissionInfo;

    @XmlElement(name = "applicationsInfo")
    protected EnrFisOfflinePacakageData applicationsInfo;

    @XmlElement(name = "ordersOfAdmission")
    protected EnrFisOfflinePacakageData ordersOfAdmission;

    public Long getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(Long sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getSessionTitle()
    {
        return sessionTitle;
    }

    public void setSessionTitle(String sessionTitle)
    {
        this.sessionTitle = sessionTitle;
    }

    public EnrFisOfflinePacakageData getCampaignInfo()
    {
        return campaignInfo;
    }

    public void setCampaignInfo(EnrFisOfflinePacakageData campaignInfo)
    {
        this.campaignInfo = campaignInfo;
    }

    public EnrFisOfflinePacakageData getAdmissionInfo()
    {
        return admissionInfo;
    }

    public void setAdmissionInfo(EnrFisOfflinePacakageData admissionInfo)
    {
        this.admissionInfo = admissionInfo;
    }

    public EnrFisOfflinePacakageData getApplicationsInfo()
    {
        return applicationsInfo;
    }

    public void setApplicationsInfo(EnrFisOfflinePacakageData applicationsInfo)
    {
        this.applicationsInfo = applicationsInfo;
    }

    public EnrFisOfflinePacakageData getOrdersOfAdmission()
    {
        return ordersOfAdmission;
    }

    public void setOrdersOfAdmission(EnrFisOfflinePacakageData ordersOfAdmission)
    {
        this.ordersOfAdmission = ordersOfAdmission;
    }

    public String getLog()
    {
        return log;
    }

    public void setLog(String log)
    {
        this.log = log;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public Date getFillDate()
    {
        return fillDate;
    }

    public void setFillDate(Date fillDate)
    {
        this.fillDate = fillDate;
    }

    public Date getQueueDate()
    {
        return queueDate;
    }

    public void setQueueDate(Date queueDate)
    {
        this.queueDate = queueDate;
    }

    public Date getArchiveDate()
    {
        return archiveDate;
    }

    public void setArchiveDate(Date archiveDate)
    {
        this.archiveDate = archiveDate;
    }

    public Boolean getSkipAdmissionInfo()
    {
        return skipAdmissionInfo;
    }

    public void setSkipAdmissionInfo(Boolean skipAdmissionInfo)
    {
        this.skipAdmissionInfo = skipAdmissionInfo;
    }

    public Boolean getForceAllEntrants()
    {
        return forceAllEntrants;
    }

    public void setForceAllEntrants(Boolean forceAllEntrants)
    {
        this.forceAllEntrants = forceAllEntrants;
    }

    public Boolean getSkipEnrCampaignInfo()
    {
        return skipEnrCampaignInfo;
    }

    public void setSkipEnrCampaignInfo(Boolean skipEnrCampaignInfo)
    {
        this.skipEnrCampaignInfo = skipEnrCampaignInfo;
    }

    public Boolean getSkipApplicationsInfo()
    {
        return skipApplicationsInfo;
    }

    public void setSkipApplicationsInfo(Boolean skipApplicationsInfo)
    {
        this.skipApplicationsInfo = skipApplicationsInfo;
    }

    public Boolean getSkipOrdersOfAdmission()
    {
        return skipOrdersOfAdmission;
    }

    public void setSkipOrdersOfAdmission(Boolean skipOrdersOfAdmission)
    {
        this.skipOrdersOfAdmission = skipOrdersOfAdmission;
    }

    public Boolean getSkipIllegalDirections()
    {
        return skipIllegalDirections;
    }

    public void setSkipIllegalDirections(Boolean skipIllegalDirections)
    {
        this.skipIllegalDirections = skipIllegalDirections;
    }

    public Boolean getSkipErrorEntrants()
    {
        return skipErrorEntrants;
    }

    public void setSkipErrorEntrants(Boolean skipErrorEntrants)
    {
        this.skipErrorEntrants = skipErrorEntrants;
    }

    public Integer getSkippedEntrantCount()
    {
        return skippedEntrantCount;
    }

    public void setSkippedEntrantCount(Integer skippedEntrantCount)
    {
        this.skippedEntrantCount = skippedEntrantCount;
    }

    public Integer getSkippedExtractCount()
    {
        return skippedExtractCount;
    }

    public void setSkippedExtractCount(Integer skippedExtractCount)
    {
        this.skippedExtractCount = skippedExtractCount;
    }

    public Integer getSkippedRecItemCount()
    {
        return skippedRecItemCount;
    }

    public void setSkippedRecItemCount(Integer skippedRecItemCount)
    {
        this.skippedRecItemCount = skippedRecItemCount;
    }

    public Boolean getAcceptedInfo()
    {
        return acceptedInfo;
    }

    public void setAcceptedInfo(Boolean acceptedInfo)
    {
        this.acceptedInfo = acceptedInfo;
    }

    public Integer getRequestState()
    {
        return requestState;
    }

    public void setRequestState(Integer requestState)
    {
        this.requestState = requestState;
    }

    public Boolean getSendRequestInOrderInfo()
    {
        return sendRequestInOrderInfo;
    }

    public void setSendRequestInOrderInfo(Boolean sendRequestInOrderInfo)
    {
        this.sendRequestInOrderInfo = sendRequestInOrderInfo;
    }
}
