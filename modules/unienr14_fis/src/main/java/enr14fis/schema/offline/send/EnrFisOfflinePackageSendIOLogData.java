/* $Id:$ */
package enr14fis.schema.offline.send;

import ru.tandemservice.unienr14_fis.sync.bo.EnrFisSyncSession.entity.EnrFisSyncPackage;

import javax.xml.bind.annotation.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 27.07.15
 * Time: 6:58
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnrFisOfflinePackageSendIOLogData")
public class EnrFisOfflinePackageSendIOLogData
{
    @XmlElement(name = "operationDate")
    protected Date operationDate;     // Время проведения операции

    @XmlElement(name = "operationUrl")
    protected String operationUrl;     // Адрес, через который проводилась операция

    @XmlElement(name = "xmlRequest")
    protected String xmlRequest;     // ZIP-XML: отправленные данные в ФИС

    @XmlElement(name = "xmlResponse")
    protected String xmlResponse;     // ZIP-XML: ответ из ФИС

    @XmlElement(name = "fisErrorText")
    protected String fisErrorText;     // Сообщение об ошибке ФИС

    @XmlElement(name = "error")
    protected Boolean error;     // Ошибка взаимодействия

    @XmlElement(name = "errorLog")
    protected String errorLog;     // ZIP: полный лог ошибки

    @XmlElement(name = "dataNodeCount")
    private Integer dataNodeCount;     // Число отправленных значимых тегов (даты ПК, КГ, абитуриенты, приказы)

    public Date getOperationDate()
    {
        return operationDate;
    }

    public void setOperationDate(Date operationDate)
    {
        this.operationDate = operationDate;
    }

    public String getOperationUrl()
    {
        return operationUrl;
    }

    public void setOperationUrl(String operationUrl)
    {
        this.operationUrl = operationUrl;
    }

    public String getXmlRequest()
    {
        return xmlRequest;
    }

    public void setXmlRequest(String xmlRequest)
    {
        this.xmlRequest = xmlRequest;
    }

    public String getXmlResponse()
    {
        return xmlResponse;
    }

    public void setXmlResponse(String xmlResponse)
    {
        this.xmlResponse = xmlResponse;
    }

    public String getFisErrorText()
    {
        return fisErrorText;
    }

    public void setFisErrorText(String fisErrorText)
    {
        this.fisErrorText = fisErrorText;
    }

    public Boolean getError()
    {
        return error;
    }

    public void setError(Boolean error)
    {
        this.error = error;
    }

    public String getErrorLog()
    {
        return errorLog;
    }

    public void setErrorLog(String errorLog)
    {
        this.errorLog = errorLog;
    }

    public Integer getDataNodeCount()
    {
        return dataNodeCount;
    }

    public void setDataNodeCount(Integer dataNodeCount)
    {
        this.dataNodeCount = dataNodeCount;
    }
}
