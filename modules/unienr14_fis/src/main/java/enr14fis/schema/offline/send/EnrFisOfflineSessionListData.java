/* $Id:$ */
package enr14fis.schema.offline.send;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 25.07.15
 * Time: 2:27
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnrFisOfflineSessionListData")
public class EnrFisOfflineSessionListData
{
    @XmlElement(name = "enrollmentCampaignId", required = true)
    protected Long enrollmentCampaignId;

    @XmlElement(name = "enrollmentCampaignTitle", required = true)
    protected String enrollmentCampaignTitle;

    @XmlElement(name = "orgUnitId", required = true)
    protected Long orgUnitId;

    @XmlElement(name = "orgUnitTitle", required = true)
    protected String orgUnitTitle;

    @XmlElement(name = "login")
    protected String login;

    @XmlElement(name = "password")
    protected String password;

    @XmlElementWrapper(name="sessions")
    @XmlElements(@XmlElement(name = "session", type = EnrFisOfflineSessionData.class))
    protected List<EnrFisOfflineSessionData> sessions;

    public Long getEnrollmentCampaignId()
    {
        return enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        this.enrollmentCampaignId = enrollmentCampaignId;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public String getEnrollmentCampaignTitle()
    {
        return enrollmentCampaignTitle;
    }

    public void setEnrollmentCampaignTitle(String enrollmentCampaignTitle)
    {
        this.enrollmentCampaignTitle = enrollmentCampaignTitle;
    }

    public String getOrgUnitTitle()
    {
        return orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        this.orgUnitTitle = orgUnitTitle;
    }

    public void setSessions(List<EnrFisOfflineSessionData> sessions)
    {
        this.sessions = sessions;
    }

    public List<EnrFisOfflineSessionData> getSessions()
    {
        return sessions;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
