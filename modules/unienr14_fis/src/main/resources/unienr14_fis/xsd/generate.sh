#!/bin/bash

if true; then

XJC="xjc -enableIntrospection -contentForWildcard"

# справочники
# $XJC -p enr14fis.schema.catalog.list.req catalog-list-req.xsd
# $XJC -p enr14fis.schema.catalog.list.resp catalog-list-resp.xsd
# $XJC -p enr14fis.schema.catalog.items.req catalog-items-req.xsd
# $XJC -p enr14fis.schema.catalog.items.resp catalog-items-resp.xsd
# $XJC -p enr14fis.schema.catalog.items.resp.olymp catalog-items-resp-olymp.xsd
# $XJC -p enr14fis.schema.catalog.items.resp.spec catalog-items-resp-spec.xsd

# пакет (отправка)
$XJC -p enr14fis.schema.pkg.send.req pkg-send-req.xsd
# $XJC -p enr14fis.schema.pkg.send.resp pkg-send-resp.xsd

# пакет (ответ)
# $XJC -p enr14fis.schema.pkg.result.req pkg-result-req.xsd
# $XJC -p enr14fis.schema.pkg.result.resp pkg-result-resp.xsd

fi

# убираем огромные комменты в классах (чтобы ide не снесло бошку)

cd enr14fis
find -type f | grep -E "[.]java$" | while read f; do
 cp "$f" "$f.orig" && cat "$f.orig" | sed '/<pre>/,/<[/]pre>/d' > "$f"
done
