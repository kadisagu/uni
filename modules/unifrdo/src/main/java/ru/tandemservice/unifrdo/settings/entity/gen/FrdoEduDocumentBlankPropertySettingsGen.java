package ru.tandemservice.unifrdo.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;
import ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка поля бланка документа об образовании (ФРДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FrdoEduDocumentBlankPropertySettingsGen extends EntityBase
 implements INaturalIdentifiable<FrdoEduDocumentBlankPropertySettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings";
    public static final String ENTITY_NAME = "frdoEduDocumentBlankPropertySettings";
    public static final int VERSION_HASH = -902785544;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_DOCUMENT_BLANK = "eduDocumentBlank";
    public static final String L_EDU_DOCUMENT_BLANK_PROPERTY = "eduDocumentBlankProperty";
    public static final String P_REQUIRED = "required";

    private FrdoEduDocumentBlank _eduDocumentBlank;     // Бланк документа об образовании (ФРДО)
    private FrdoEduDocumentBlankProperty _eduDocumentBlankProperty;     // Поле бланка документа об образовании (ФРДО)
    private boolean _required;     // Обязательное

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Бланк документа об образовании (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEduDocumentBlank getEduDocumentBlank()
    {
        return _eduDocumentBlank;
    }

    /**
     * @param eduDocumentBlank Бланк документа об образовании (ФРДО). Свойство не может быть null.
     */
    public void setEduDocumentBlank(FrdoEduDocumentBlank eduDocumentBlank)
    {
        dirty(_eduDocumentBlank, eduDocumentBlank);
        _eduDocumentBlank = eduDocumentBlank;
    }

    /**
     * @return Поле бланка документа об образовании (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEduDocumentBlankProperty getEduDocumentBlankProperty()
    {
        return _eduDocumentBlankProperty;
    }

    /**
     * @param eduDocumentBlankProperty Поле бланка документа об образовании (ФРДО). Свойство не может быть null.
     */
    public void setEduDocumentBlankProperty(FrdoEduDocumentBlankProperty eduDocumentBlankProperty)
    {
        dirty(_eduDocumentBlankProperty, eduDocumentBlankProperty);
        _eduDocumentBlankProperty = eduDocumentBlankProperty;
    }

    /**
     * @return Обязательное. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequired()
    {
        return _required;
    }

    /**
     * @param required Обязательное. Свойство не может быть null.
     */
    public void setRequired(boolean required)
    {
        dirty(_required, required);
        _required = required;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FrdoEduDocumentBlankPropertySettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setEduDocumentBlank(((FrdoEduDocumentBlankPropertySettings)another).getEduDocumentBlank());
                setEduDocumentBlankProperty(((FrdoEduDocumentBlankPropertySettings)another).getEduDocumentBlankProperty());
            }
            setRequired(((FrdoEduDocumentBlankPropertySettings)another).isRequired());
        }
    }

    public INaturalId<FrdoEduDocumentBlankPropertySettingsGen> getNaturalId()
    {
        return new NaturalId(getEduDocumentBlank(), getEduDocumentBlankProperty());
    }

    public static class NaturalId extends NaturalIdBase<FrdoEduDocumentBlankPropertySettingsGen>
    {
        private static final String PROXY_NAME = "FrdoEduDocumentBlankPropertySettingsNaturalProxy";

        private Long _eduDocumentBlank;
        private Long _eduDocumentBlankProperty;

        public NaturalId()
        {}

        public NaturalId(FrdoEduDocumentBlank eduDocumentBlank, FrdoEduDocumentBlankProperty eduDocumentBlankProperty)
        {
            _eduDocumentBlank = ((IEntity) eduDocumentBlank).getId();
            _eduDocumentBlankProperty = ((IEntity) eduDocumentBlankProperty).getId();
        }

        public Long getEduDocumentBlank()
        {
            return _eduDocumentBlank;
        }

        public void setEduDocumentBlank(Long eduDocumentBlank)
        {
            _eduDocumentBlank = eduDocumentBlank;
        }

        public Long getEduDocumentBlankProperty()
        {
            return _eduDocumentBlankProperty;
        }

        public void setEduDocumentBlankProperty(Long eduDocumentBlankProperty)
        {
            _eduDocumentBlankProperty = eduDocumentBlankProperty;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FrdoEduDocumentBlankPropertySettingsGen.NaturalId) ) return false;

            FrdoEduDocumentBlankPropertySettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduDocumentBlank(), that.getEduDocumentBlank()) ) return false;
            if( !equals(getEduDocumentBlankProperty(), that.getEduDocumentBlankProperty()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduDocumentBlank());
            result = hashCode(result, getEduDocumentBlankProperty());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduDocumentBlank());
            sb.append("/");
            sb.append(getEduDocumentBlankProperty());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FrdoEduDocumentBlankPropertySettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FrdoEduDocumentBlankPropertySettings.class;
        }

        public T newInstance()
        {
            return (T) new FrdoEduDocumentBlankPropertySettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduDocumentBlank":
                    return obj.getEduDocumentBlank();
                case "eduDocumentBlankProperty":
                    return obj.getEduDocumentBlankProperty();
                case "required":
                    return obj.isRequired();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduDocumentBlank":
                    obj.setEduDocumentBlank((FrdoEduDocumentBlank) value);
                    return;
                case "eduDocumentBlankProperty":
                    obj.setEduDocumentBlankProperty((FrdoEduDocumentBlankProperty) value);
                    return;
                case "required":
                    obj.setRequired((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduDocumentBlank":
                        return true;
                case "eduDocumentBlankProperty":
                        return true;
                case "required":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduDocumentBlank":
                    return true;
                case "eduDocumentBlankProperty":
                    return true;
                case "required":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduDocumentBlank":
                    return FrdoEduDocumentBlank.class;
                case "eduDocumentBlankProperty":
                    return FrdoEduDocumentBlankProperty.class;
                case "required":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FrdoEduDocumentBlankPropertySettings> _dslPath = new Path<FrdoEduDocumentBlankPropertySettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FrdoEduDocumentBlankPropertySettings");
    }
            

    /**
     * @return Бланк документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings#getEduDocumentBlank()
     */
    public static FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank> eduDocumentBlank()
    {
        return _dslPath.eduDocumentBlank();
    }

    /**
     * @return Поле бланка документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings#getEduDocumentBlankProperty()
     */
    public static FrdoEduDocumentBlankProperty.Path<FrdoEduDocumentBlankProperty> eduDocumentBlankProperty()
    {
        return _dslPath.eduDocumentBlankProperty();
    }

    /**
     * @return Обязательное. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings#isRequired()
     */
    public static PropertyPath<Boolean> required()
    {
        return _dslPath.required();
    }

    public static class Path<E extends FrdoEduDocumentBlankPropertySettings> extends EntityPath<E>
    {
        private FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank> _eduDocumentBlank;
        private FrdoEduDocumentBlankProperty.Path<FrdoEduDocumentBlankProperty> _eduDocumentBlankProperty;
        private PropertyPath<Boolean> _required;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Бланк документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings#getEduDocumentBlank()
     */
        public FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank> eduDocumentBlank()
        {
            if(_eduDocumentBlank == null )
                _eduDocumentBlank = new FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank>(L_EDU_DOCUMENT_BLANK, this);
            return _eduDocumentBlank;
        }

    /**
     * @return Поле бланка документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings#getEduDocumentBlankProperty()
     */
        public FrdoEduDocumentBlankProperty.Path<FrdoEduDocumentBlankProperty> eduDocumentBlankProperty()
        {
            if(_eduDocumentBlankProperty == null )
                _eduDocumentBlankProperty = new FrdoEduDocumentBlankProperty.Path<FrdoEduDocumentBlankProperty>(L_EDU_DOCUMENT_BLANK_PROPERTY, this);
            return _eduDocumentBlankProperty;
        }

    /**
     * @return Обязательное. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings#isRequired()
     */
        public PropertyPath<Boolean> required()
        {
            if(_required == null )
                _required = new PropertyPath<Boolean>(FrdoEduDocumentBlankPropertySettingsGen.P_REQUIRED, this);
            return _required;
        }

        public Class getEntityClass()
        {
            return FrdoEduDocumentBlankPropertySettings.class;
        }

        public String getEntityName()
        {
            return "frdoEduDocumentBlankPropertySettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
