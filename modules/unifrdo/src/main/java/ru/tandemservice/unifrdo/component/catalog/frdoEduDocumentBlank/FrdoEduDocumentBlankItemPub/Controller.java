package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlank.FrdoEduDocumentBlankItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class Controller extends DefaultCatalogItemPubController<FrdoEduDocumentBlank, Model, IDAO>
{
}
