package ru.tandemservice.unifrdo.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Поле бланка документа об образовании (ФРДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FrdoEduDocumentBlankPropertyGen extends EntityBase
 implements INaturalIdentifiable<FrdoEduDocumentBlankPropertyGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty";
    public static final String ENTITY_NAME = "frdoEduDocumentBlankProperty";
    public static final int VERSION_HASH = -518629875;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_PRIORITY = "priority";
    public static final String P_NAME = "name";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _priority;     // Порядковый номер
    private String _name;     // Имя
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Порядковый номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Порядковый номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getName()
    {
        return _name;
    }

    /**
     * @param name Имя. Свойство не может быть null и должно быть уникальным.
     */
    public void setName(String name)
    {
        dirty(_name, name);
        _name = name;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FrdoEduDocumentBlankPropertyGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FrdoEduDocumentBlankProperty)another).getCode());
            }
            setPriority(((FrdoEduDocumentBlankProperty)another).getPriority());
            setName(((FrdoEduDocumentBlankProperty)another).getName());
            setTitle(((FrdoEduDocumentBlankProperty)another).getTitle());
        }
    }

    public INaturalId<FrdoEduDocumentBlankPropertyGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FrdoEduDocumentBlankPropertyGen>
    {
        private static final String PROXY_NAME = "FrdoEduDocumentBlankPropertyNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FrdoEduDocumentBlankPropertyGen.NaturalId) ) return false;

            FrdoEduDocumentBlankPropertyGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FrdoEduDocumentBlankPropertyGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FrdoEduDocumentBlankProperty.class;
        }

        public T newInstance()
        {
            return (T) new FrdoEduDocumentBlankProperty();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "priority":
                    return obj.getPriority();
                case "name":
                    return obj.getName();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "name":
                    obj.setName((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "priority":
                        return true;
                case "name":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "priority":
                    return true;
                case "name":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "name":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FrdoEduDocumentBlankProperty> _dslPath = new Path<FrdoEduDocumentBlankProperty>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FrdoEduDocumentBlankProperty");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Порядковый номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getName()
     */
    public static PropertyPath<String> name()
    {
        return _dslPath.name();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends FrdoEduDocumentBlankProperty> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _name;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FrdoEduDocumentBlankPropertyGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Порядковый номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(FrdoEduDocumentBlankPropertyGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getName()
     */
        public PropertyPath<String> name()
        {
            if(_name == null )
                _name = new PropertyPath<String>(FrdoEduDocumentBlankPropertyGen.P_NAME, this);
            return _name;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FrdoEduDocumentBlankPropertyGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return FrdoEduDocumentBlankProperty.class;
        }

        public String getEntityName()
        {
            return "frdoEduDocumentBlankProperty";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
