package ru.tandemservice.unifrdo.catalog.entity;

import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentDataTypeCodes;
import ru.tandemservice.unifrdo.catalog.entity.gen.*;

import java.util.Arrays;

/**
 * Типы данных документов об образовании
 */
public class FrdoEduDocumentDataType extends FrdoEduDocumentDataTypeGen
{
    public boolean isShowInsteadDocumentField()
    {
        return Arrays.asList(FrdoEduDocumentDataTypeCodes.DUPLICATE, FrdoEduDocumentDataTypeCodes.INSTEAD, FrdoEduDocumentDataTypeCodes.CORRECTION).contains(getCode());
    }

    public boolean isShowDuplicateField()
    {
        return FrdoEduDocumentDataTypeCodes.DUPLICATE.equals(getCode());
    }

    public boolean isShowReplaceDateField()
    {
        return FrdoEduDocumentDataTypeCodes.INSTEAD.equals(getCode());
    }
}