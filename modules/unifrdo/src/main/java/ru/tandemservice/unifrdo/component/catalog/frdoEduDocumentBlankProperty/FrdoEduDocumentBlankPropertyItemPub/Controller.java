package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class Controller extends DefaultCatalogItemPubController<FrdoEduDocumentBlankProperty, Model, IDAO>
{
}
