package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

/**
 * @author Alexander Shaburov
 * @since 21.11.13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id", required = true)
})
public class FrdoEduDocumentsPackagePubUI extends UIPresenter
{
    private EntityHolder<FrdoEduDocumentsPackage> _entityHolder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();
    }

    public void onClickPrint()
    {
        byte[] content = _entityHolder.getValue().getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл пакета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().document(content).fileName("DocumentsPackage.xml"), true);
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_entityHolder.getId());
        deactivate();
    }

    // Accessers

    public EntityHolder<FrdoEduDocumentsPackage> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<FrdoEduDocumentsPackage> entityHolder)
    {
        _entityHolder = entityHolder;
    }
}
