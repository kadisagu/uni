package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.FrdoEduDocumentManager;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;
import ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 07.11.13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class FrdoEduDocumentAddEditUI extends UIPresenter
{
    private EntityHolder<FrdoEduDocument> _entityHolder = new EntityHolder<>(new FrdoEduDocument());

    private FrdoEducationLevel _educationLevel;
    private FrdoEduDocumentType _eduDocumentType;
    private FrdoEduDocument _insteadEduDocument;
    private AddressCountry _country;

    private Map<String, FrdoEduDocumentBlankPropertySettings> _fieldSettingsMap;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();

        prepare();
    }

    public void prepare()
    {
        if (isEditForm())
        {
            _eduDocumentType = _entityHolder.getValue().getBlank().getEduDocumentType();
            _educationLevel = _entityHolder.getValue().getBlank().getEduDocumentType().getEducationLevel();
            if (_entityHolder.getValue().getIssueAddress() != null)
                _country = _entityHolder.getValue().getIssueAddress().getCountry();
        }
        else
        {
            _entityHolder.getValue().setFormingDate(new Date());
        }

        preparePropertySettings();
    }

    public void preparePropertySettings()
    {
        final List<FrdoEduDocumentBlankPropertySettings> settingsList = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlankPropertySettings.class, "s").column(property("s"))
            .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlank().fromAlias("s")), value(_entityHolder.getValue().getBlank()))));

        _fieldSettingsMap = new HashMap<>();
        for (FrdoEduDocumentBlankPropertySettings settings : settingsList)
            _fieldSettingsMap.put(settings.getEduDocumentBlankProperty().getName(), settings);
    }

    public void onChangeInsteadEduDocument()
    {
        if (_entityHolder.getValue().getEduDocument() == null)
            return;

        _insteadEduDocument = _entityHolder.getValue().getEduDocument();

        for (String propName : _fieldSettingsMap.keySet())
        {
            final Object propInsteadValue = _insteadEduDocument.getProperty(propName);
            final Object propValue = _entityHolder.getValue().getProperty(propName);
            if (propValue == null && propInsteadValue != null)
                _entityHolder.getValue().setProperty(propName, propInsteadValue);
        }
    }

    public void onClickApply()
    {
        FrdoEduDocumentManager.instance().dao().saveOrUpdateEduDocument(_entityHolder.getValue());
        deactivate();
    }

    // Getters & Setters

    public String getSticker()
    {
        return isEditForm() ? getConfig().getProperty("sticker.edit") : getConfig().getProperty("sticker.add");
    }

    public boolean isEditForm()
    {
        return _entityHolder.getId() != null;
    }

    public boolean isPropertyUses(String field)
    {
        return _fieldSettingsMap.containsKey(field);
    }

    public boolean isPropertyRequired(String field)
    {
        return _fieldSettingsMap.containsKey(field) && _fieldSettingsMap.get(field).isRequired();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
	    switch (dataSource.getName())
	    {
		    case FrdoEduDocumentAddEdit.ISSUE_ADDRESS_SELECT_DS:
			    dataSource.put(AddressItem.PARAM_COUNTRY, _country);
			    break;
		    case FrdoEduDocumentAddEdit.BLANK_SELECT_DS:
			    dataSource.put(FrdoEduDocumentAddEdit.PARAM_EDU_DOCUMENT_TYPE,  _eduDocumentType);
			    break;
		    case FrdoEduDocumentAddEdit.EDU_DOCUMENT_TYPE_SELECT_DS:
			    dataSource.put(FrdoEduDocumentAddEdit.PARAM_EDU_LEVEL, _educationLevel);
			    break;
	    }
    }

    // Accessers

    public AddressCountry getCountry()
    {
        return _country;
    }

    public void setCountry(AddressCountry country)
    {
        _country = country;
    }

    public EntityHolder<FrdoEduDocument> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<FrdoEduDocument> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public FrdoEducationLevel getEducationLevel()
    {
        return _educationLevel;
    }

    public void setEducationLevel(FrdoEducationLevel educationLevel)
    {
        _educationLevel = educationLevel;
    }

    public FrdoEduDocumentType getEduDocumentType()
    {
        return _eduDocumentType;
    }

    public void setEduDocumentType(FrdoEduDocumentType eduDocumentType)
    {
        _eduDocumentType = eduDocumentType;
    }

    public Map<String, FrdoEduDocumentBlankPropertySettings> getFieldSettingsMap()
    {
        return _fieldSettingsMap;
    }

    public void setFieldSettingsMap(Map<String, FrdoEduDocumentBlankPropertySettings> fieldSettingsMap)
    {
        _fieldSettingsMap = fieldSettingsMap;
    }

    public FrdoEduDocument getInsteadEduDocument()
    {
        return _insteadEduDocument;
    }

    public void setInsteadEduDocument(FrdoEduDocument insteadEduDocument)
    {
        _insteadEduDocument = insteadEduDocument;
    }
}
