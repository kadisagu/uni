package ru.tandemservice.unifrdo.document.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifrdo.document.entity.gen.*;

/**
 * Документ об образовании (ФРДО)
 */
public class FrdoEduDocument extends FrdoEduDocumentGen
{
    @EntityDSLSupport
    public String getDocumentTitle()
    {
        return getBlank().getEduDocumentType().getTitle() +
                " №" + (getSerialRegNumber() == null ? "" : getSerialRegNumber()) +
                ", выдан " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getIssueDate());
    }

    @EntityDSLSupport(parts = {P_LAST_NAME_OF_OWNER, P_FIRST_NAME_OF_OWNER, P_MIDDLE_NAME_OF_OWNER})
    public String getFioOfOwner()
    {
        StringBuilder builder = new StringBuilder();
        if (getLastNameOfOwner() != null)
            builder.append(getLastNameOfOwner());
        if (getFirstNameOfOwner() != null)
            builder.append(builder.length() > 0 ? " " : "").append(getFirstNameOfOwner());
        if (getMiddleNameOfOwner() != null)
            builder.append(builder.length() > 0 ? " " : "").append(getMiddleNameOfOwner());

        return builder.toString();
    }

    @Override
    public void setSerialRegNumber(String serialRegNumber)
    {
        super.setSerialRegNumber(serialRegNumber);
        updateSerialRegNumberUniqKey();
    }

    @Override
    public void setRegIndex(String regIndex)
    {
        super.setRegIndex(regIndex);
        updateSerialRegNumberUniqKey();
    }

    @Override
    public void setYear(int year)
    {
        super.setYear(year);
        updateSerialRegNumberUniqKey();
    }

    /**
     * Обновляет ключ уникальности serialRegNumberUniqKey, если заполнены regIndex и serialRegNumber и blank не null.<p/>
     * <code>[serialRegNumber].[regIndex].[year].[blank.eduDocumentType.educationLevel.code]</code>
     */
    private void updateSerialRegNumberUniqKey()
    {
        if (getRegIndex() == null || getSerialRegNumber() == null || getBlank() == null)
        {
            setSerialRegNumberUniqKey(null);
            return;
        }

        setSerialRegNumberUniqKey(getSerialRegNumber() + "." + getRegIndex() + "." + getYear() + "." + getBlank().getEduDocumentType().getEducationLevel().getCode());
    }
}