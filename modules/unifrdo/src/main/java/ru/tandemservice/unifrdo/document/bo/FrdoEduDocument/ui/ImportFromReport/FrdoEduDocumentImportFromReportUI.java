package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.InfoCollector;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.FrdoEduDocumentManager;

import java.io.IOException;

/**
 * @author avedernikov
 * @since 05.04.2017
 */
public class FrdoEduDocumentImportFromReportUI extends UIPresenter
{
	private IUploadFile reportFile;

	public void onClickApply()
	{
		final InfoCollector infoCollector = ContextLocal.getInfoCollector();
		try
		{
			byte[] content = IOUtils.toByteArray(reportFile.getStream());
			FrdoEduDocumentManager.instance().reportImportDao().importReports(content, infoCollector);
		}
		catch (IOException e)
		{
			throw new ApplicationException(FrdoEduDocumentManager.instance().getProperty("ui.wrongFileMessage"), e);
		}
		deactivate();
	}

	public IUploadFile getReportFile()
	{
		return reportFile;
	}

	public void setReportFile(IUploadFile reportFile)
	{
		this.reportFile = reportFile;
	}
}