package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.logic.FrdoEduDocumentBlankPropertyDao;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.logic.IFrdoEduDocumentBlankPropertyDao;

/**
 * @author Alexander Shaburov
 * @since 28.10.13
 */
@Configuration
public class FrdoEduDocumentBlankPropertyManager extends BusinessObjectManager
{
    public static FrdoEduDocumentBlankPropertyManager instance()
    {
        return instance(FrdoEduDocumentBlankPropertyManager.class);
    }

    @Bean
    public IFrdoEduDocumentBlankPropertyDao dao()
    {
        return new FrdoEduDocumentBlankPropertyDao();
    }
}
