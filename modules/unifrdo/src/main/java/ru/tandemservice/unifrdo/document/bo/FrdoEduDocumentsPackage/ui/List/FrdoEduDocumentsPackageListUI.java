package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic.FrdoEduDocumentPackageSearchDSHandler;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.Add.FrdoEduDocumentsPackageAdd;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

/**
 * @author Alexander Shaburov
 * @since 18.11.13
 */
public class FrdoEduDocumentsPackageListUI extends UIPresenter
{
    public void onClickAddPackage()
    {
        getActivationBuilder().asRegion(FrdoEduDocumentsPackageAdd.class)
                .activate();
    }

    public void onClickPrintPackage()
    {
        final FrdoEduDocumentsPackage pkg = getEntityByListenerParameterAsLong();
        byte[] content = pkg.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл пакета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().document(content).fileName("DocumentsPackage.xml"), true);
    }

    public void onClickDeletePackage()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FrdoEduDocumentPackageSearchDSHandler.BIND_FORMING_DATE, getSettings().get("formingDate"));
        dataSource.put(FrdoEduDocumentPackageSearchDSHandler.BIND_TITLE, getSettings().get("title"));
    }
}
