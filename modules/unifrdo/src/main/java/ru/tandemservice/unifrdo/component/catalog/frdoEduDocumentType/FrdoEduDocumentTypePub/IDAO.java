package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentType.FrdoEduDocumentTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;

/**
 * @author Alexander Shaburov
 * @since 25.10.13
 */
public interface IDAO extends IDefaultCatalogPubDAO<FrdoEduDocumentType, Model>
{
}
