package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.ui.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 28.10.13
 */
public class BlankPropertyWrapper extends IdentifiableWrapper<FrdoEduDocumentBlankProperty>
{
    public static final String P_USES = "uses";
    public static final String P_REQUIRED = "required";
    public static final String L_PROPERTY = "property";
    public static final String P_TITLE = "title";

    public BlankPropertyWrapper(FrdoEduDocumentBlankProperty property, boolean uses, boolean required)
    {
        super(property);
        _property = property;
        _uses = uses;
        _required = required;
    }

    private FrdoEduDocumentBlankProperty _property;
    private boolean _uses;
    private boolean _required;

    // Accessors

    public FrdoEduDocumentBlankProperty getProperty()
    {
        return _property;
    }

    public void setProperty(FrdoEduDocumentBlankProperty property)
    {
        _property = property;
    }

    public boolean isUses()
    {
        return _uses;
    }

    public void setUses(boolean uses)
    {
        _uses = uses;
    }

    public boolean isRequired()
    {
        return _required;
    }

    public void setRequired(boolean required)
    {
        _required = required;
    }
}
