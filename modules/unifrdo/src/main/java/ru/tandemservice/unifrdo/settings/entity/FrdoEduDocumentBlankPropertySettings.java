package ru.tandemservice.unifrdo.settings.entity;

import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;
import ru.tandemservice.unifrdo.settings.entity.gen.*;

/**
 * Настройка поля бланка документа об образовании
 */
public class FrdoEduDocumentBlankPropertySettings extends FrdoEduDocumentBlankPropertySettingsGen
{
    public FrdoEduDocumentBlankPropertySettings()
    {
    }

    public FrdoEduDocumentBlankPropertySettings(FrdoEduDocumentBlank blank, FrdoEduDocumentBlankProperty property)
    {
        setEduDocumentBlank(blank);
        setEduDocumentBlankProperty(property);
    }
}