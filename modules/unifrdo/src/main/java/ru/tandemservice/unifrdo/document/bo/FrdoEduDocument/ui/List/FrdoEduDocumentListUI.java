package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.AddEdit.FrdoEduDocumentAddEdit;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ChangeState.FrdoEduDocumentChangeState;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ChangeState.FrdoEduDocumentChangeStateUI;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.FrdoEduDocumentImportFromReport;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 01.11.13
 */
public class FrdoEduDocumentListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {

    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(FrdoEduDocumentList.EDU_DOCUMENT_SEARCH_DS).getOptionColumnSelectedObjects("check");
            selected.clear();
        }
    }

    public void onClickAddEduDocument()
    {
        getActivationBuilder().asRegion(FrdoEduDocumentAddEdit.class)
                .activate();
    }

    public void onClickChangeState()
    {
        final Collection<IEntity> selectedObjects = getConfig().<BaseSearchListDataSource>getDataSource(FrdoEduDocumentList.EDU_DOCUMENT_SEARCH_DS).getOptionColumnSelectedObjects("check");
        final Iterator<IEntity> iterator = selectedObjects.iterator();

        if (!iterator.hasNext())
            throw new ApplicationException(getConfig().getProperty("onClickChangeStatus.emptyException"));

        final FrdoEduDocument entity = (FrdoEduDocument) iterator.next();
        for (FrdoEduDocument next; iterator.hasNext(); )
        {
            next = (FrdoEduDocument) iterator.next();
            if (!entity.getState().getCode().equals(next.getState().getCode()))
                throw new ApplicationException(getConfig().getProperty("onClickChangeStatus.inconsistentStateException"));
        }

        getActivationBuilder()
                .asRegionDialog(FrdoEduDocumentChangeState.class)
                .parameter(FrdoEduDocumentChangeStateUI.INPUT_BIND_ID_LIST, UniBaseDao.ids(selectedObjects))
                .activate();
    }

	public void onClickImportReports()
	{
		_uiActivation.asRegionDialog(FrdoEduDocumentImportFromReport.class)
				.activate();
	}

    public void onClickEditEduDocument()
    {
        getActivationBuilder().asRegion(FrdoEduDocumentAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteEduDocument()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAsMap("educationLevel", "year", "regIndex", "serialRegNumber", "lastNameOfOwner", "firstNameOfOwner", "middleNameOfOwner", "eduDocumentType", "blank", "numberBlank",
                "issueDate", "educationLevelTitle", "nameOfdegreeOrQualification", "dateOfStateCertificationCommision", "numberOfStateCertificationCommision", "numberOfStudentExcludeOrder", "state", "dataType",
                "dateOfDuplicateDocumentOrder", "numberOfDuplicateDocumentOrder", "replaceDate"));
    }
}
