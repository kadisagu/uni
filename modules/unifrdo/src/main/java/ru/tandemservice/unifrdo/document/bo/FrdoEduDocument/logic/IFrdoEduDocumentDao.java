package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

import java.util.Collection;

/**
 * @author Alexander Shaburov
 * @since 06.11.13
 */
public interface IFrdoEduDocumentDao extends INeedPersistenceSupport
{
    /**
     * Массово меняются состояния у объектов с id из ids на заданное состояние.
     * @param ids колекция id объектов FrdoEduDocument
     * @param state целевое состояние
     */
    void doBatchChangeStatus(Collection<Long> ids, FrdoEduDocumentState state);

    /**
     * Создает или обновляет Документ об образовании (ФРДО).
     * Обнуляет поля, которые не предусмотренный для выбранного Типа данных документа об образовании.
     * @param eduDocument документ
     * @return id документа
     */
    Long saveOrUpdateEduDocument(FrdoEduDocument eduDocument);
}
