package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class DAO extends DefaultCatalogAddEditDAO<FrdoEduDocumentBlankProperty, Model> implements IDAO
{
}
