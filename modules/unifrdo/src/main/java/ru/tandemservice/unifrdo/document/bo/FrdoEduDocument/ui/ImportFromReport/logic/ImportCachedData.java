package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import ru.tandemservice.unifrdo.catalog.entity.FrdoDevelopForm;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentDataType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

import static ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic.FrdoReportParseDao.PropertyType;
import static ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic.FrdoReportParseDao.ColumnType;
import static ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic.FrdoReportParseDao.SeriaNumber;

/**
 * @author avedernikov
 * @since 10.04.2017
 */
class ImportCachedData
{
	public final ImmutableMap<String, FrdoEduDocumentBlank> docType2Blank;
	public final ImmutableMap<String, FrdoEduDocumentDataType> docStatus2DataType;
	public final ImmutableMap<String, FrdoDevelopForm> devFormTitle2DevForm;
	public final ImmutableMultimap<SeriaNumber, FrdoEduDocument> originalSeriaAndNumber2OriginalDoc;
	public final FrdoEduDocumentState issuedState;
	public final ImmutableMap<FrdoEduDocumentBlank, FrdoRequiredPropSettings> blank2RequiredPropSettings;

	public ImportCachedData(ImmutableMap<String, FrdoEduDocumentBlank> docType2Blank, ImmutableMap<String, FrdoEduDocumentDataType> docStatus2DataType,
							ImmutableMap<String, FrdoDevelopForm> devFormTitle2DevForm, ImmutableMultimap<SeriaNumber, FrdoEduDocument> originalSeriaAndNumber2OriginalDoc,
							FrdoEduDocumentState issuedState, ImmutableMap<FrdoEduDocumentBlank, FrdoRequiredPropSettings> blank2RequiredPropSettings)
	{
		this.docType2Blank = docType2Blank;
		this.docStatus2DataType = docStatus2DataType;
		this.devFormTitle2DevForm = devFormTitle2DevForm;
		this.originalSeriaAndNumber2OriginalDoc = originalSeriaAndNumber2OriginalDoc;
		this.issuedState = issuedState;
		this.blank2RequiredPropSettings = blank2RequiredPropSettings;
	}

	static class FrdoRequiredPropSettings
	{
		public final ImmutableMap<ColumnType, PropertyType> requiredColumn2Type;
		public final boolean existNotFilledAtAllRequiredProp;

		public FrdoRequiredPropSettings(ImmutableMap<ColumnType, PropertyType> requiredColumn2Type, boolean existNotFilledAtAllRequiredProp)
		{
			this.requiredColumn2Type = requiredColumn2Type;
			this.existNotFilledAtAllRequiredProp = existNotFilledAtAllRequiredProp;
		}
	}
}
