package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ChangeState;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.FrdoEduDocumentManager;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

import java.util.Collection;

/**
 * @author Alexander Shaburov
 * @since 06.11.13
 */
@Input({
        @Bind(key = FrdoEduDocumentChangeStateUI.INPUT_BIND_ID_LIST, binding = "idList", required = true)
})
public class FrdoEduDocumentChangeStateUI extends UIPresenter
{
    public static final String INPUT_BIND_ID_LIST = "inputBindIdList";

    private Collection<Long> _idList;

    private FrdoEduDocumentState _state;

    @Override
    public void onComponentRefresh()
    {
        if (CollectionUtils.isEmpty(_idList))
            throw new IllegalStateException("IdList is empty.");

        if (getCurrentState().getPossibleStateCodes().size() == 1)
            _state = DataAccessServices.dao().get(FrdoEduDocumentState.class, FrdoEduDocumentState.code(), getCurrentState().getPossibleStateCodes().iterator().next());
    }

    public void onClickApply()
    {
        FrdoEduDocumentManager.instance().dao().doBatchChangeStatus(_idList, _state);
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("possibleStateCodes", getCurrentState().getPossibleStateCodes());
    }

    // Getters & Setters

    public FrdoEduDocumentState getCurrentState()
    {
        return DataAccessServices.dao().get(FrdoEduDocument.class, _idList.iterator().next()).getState();
    }

    // Accessors

    public FrdoEduDocumentState getState()
    {
        return _state;
    }

    public void setState(FrdoEduDocumentState state)
    {
        _state = state;
    }

    public Collection<Long> getIdList()
    {
        return _idList;
    }

    public void setIdList(Collection<Long> idList)
    {
        _idList = idList;
    }
}
