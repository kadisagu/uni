package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 18.11.13
 */
public class FrdoEduDocumentPackageSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public FrdoEduDocumentPackageSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_FORMING_DATE = "bindFormingDate";
    public static final String BIND_TITLE = "bindTitle";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FrdoEduDocumentsPackage.class, "p").column(property("p"));
        filter(dql, "p", input, context);
        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().pageable(true).build();
    }

    /**
     * @param dql builder from FrdoEduDocumentsPackage
     */
    protected void filter(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final Date formingDate = context.get(BIND_FORMING_DATE);
        final String title = context.get(BIND_TITLE);

        if (formingDate != null)
        {
            dql.where(ge(property(FrdoEduDocumentsPackage.formingDate().fromAlias(alias)), value(formingDate, PropertyType.TIMESTAMP)));

            Calendar c = GregorianCalendar.getInstance();
            c.setTime(formingDate);
            c.add(Calendar.DATE, 1);
            dql.where(lt(property(FrdoEduDocumentsPackage.formingDate().fromAlias(alias)), value(c.getTime(), PropertyType.TIMESTAMP)));
        }
        if (StringUtils.isNotEmpty(title))
            dql.where(likeUpper(property(FrdoEduDocumentsPackage.title().fromAlias(alias)), value(CoreStringUtils.escapeLike(title, true))));
    }
}
