package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentType.FrdoEduDocumentTypeItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubDAO;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;

/**
 * @author Alexander Shaburov
 * @since 25.10.13
 */
public class DAO extends DefaultCatalogItemPubDAO<FrdoEduDocumentType, Model> implements IDAO
{
}
