package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentDetails.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentDetails.FrdoEduDocumentDetailsManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 14.11.13
 */
public class FrdoEduDocumentDetailsListUI extends UIPresenter
{
    private DataWrapper _currentRow;
    private DataWrapper _editedRow;
    private Integer _editedRowIndex;
    private String _currentEditRowValue;
    private List<DataWrapper> _rowList;

    @Override
    public void onComponentRefresh()
    {
        refreshList();
    }

    public void refreshList()
    {
        prepareList();

        _editedRow = null;
        _editedRowIndex = null;
        _currentEditRowValue = null;
    }

    public void prepareList()
    {
        _rowList = new ArrayList<>();
        final Map<String, String> detailsValueMap = FrdoEduDocumentDetailsManager.instance().getDetailsValueMap();

        long id = 1;
        for (Map.Entry<String, String> detailsEntry : FrdoEduDocumentDetailsManager.instance().getName2TitleDetailsMap().entrySet())
        {
            final DataWrapper wrapper = new DataWrapper(id, detailsEntry.getValue());
            wrapper.setProperty("number", id++);
            wrapper.setProperty("name", detailsEntry.getKey());
            wrapper.setProperty("value", detailsValueMap.containsKey(detailsEntry.getKey()) ? detailsValueMap.get(detailsEntry.getKey()) : null);
            _rowList.add(wrapper);
        }
    }

    public void onClickEditRow()
    {
        if (_editedRow != null)
            return;

        DataWrapper baseRow = findRow();

        _editedRow = new DataWrapper(baseRow);
        _editedRow.setProperty("name", baseRow.getProperty("name"));
        _editedRow.setProperty("value", baseRow.getProperty("value"));
        _editedRowIndex = _rowList.indexOf(baseRow);

        String currentValue = (String) baseRow.getProperty("value");
        _currentEditRowValue = currentValue != null ? currentValue : null ;
    }

    public void onClickSaveRow()
    {
        if (_editedRowIndex != null && _rowList.size() > _editedRowIndex && _editedRowIndex >= 0)
            FrdoEduDocumentDetailsManager.instance().saveDetailsValue((String) _editedRow.getProperty("name"), getCurrentValue());

        refreshList();
    }

    public void onClickCancelEdit()
    {
        refreshList();
    }

    // Getters & Setters

    public String getCurrentValue()
    {
        if (!isEditMode())
            throw new IllegalStateException();

        return _currentEditRowValue;
    }

    public boolean isEditMode()
    {
        return _editedRow != null;
    }

    public boolean isCurrentRowInEditMode()
    {
        return _editedRow != null && _editedRowIndex.equals(_rowList.indexOf(_currentRow));
    }

    private DataWrapper findRow()
    {
        final Long id = getListenerParameterAsLong();
        DataWrapper row = null;

        for (DataWrapper r : _rowList)
        {
            if (r.getId().equals(id))
                row = r;
        }

        return row;
    }

    // Accessors

    public DataWrapper getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(DataWrapper currentRow)
    {
        _currentRow = currentRow;
    }

    public DataWrapper getEditedRow()
    {
        return _editedRow;
    }

    public void setEditedRow(DataWrapper editedRow)
    {
        _editedRow = editedRow;
    }

    public Integer getEditedRowIndex()
    {
        return _editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        _editedRowIndex = editedRowIndex;
    }

    public String getCurrentEditRowValue()
    {
        return _currentEditRowValue;
    }

    public void setCurrentEditRowValue(String currentEditRowValue)
    {
        _currentEditRowValue = currentEditRowValue;
    }

    public List<DataWrapper> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<DataWrapper> rowList)
    {
        _rowList = rowList;
    }
}
