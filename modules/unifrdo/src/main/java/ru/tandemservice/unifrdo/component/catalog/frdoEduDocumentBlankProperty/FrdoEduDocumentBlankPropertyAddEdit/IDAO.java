package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<FrdoEduDocumentBlankProperty, Model>
{
}
