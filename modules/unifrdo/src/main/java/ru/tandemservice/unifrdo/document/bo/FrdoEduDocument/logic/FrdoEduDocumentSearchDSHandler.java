package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.11.13
 */
public class FrdoEduDocumentSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public FrdoEduDocumentSearchDSHandler(String ownerId)
    {
        super(ownerId, FrdoEduDocument.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FrdoEduDocument.class, "d").column(property("d"));
        filter(dql, "d", input, context);
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();
        return wrap(output, input, context);
    }

    /**
     * @param dql builder from FrdoEduDocument
     */
    protected void filter(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final String[] simpleFilters = new String[]{"year", "regIndex", "serialRegNumber", "blank", "numberBlank",
                "issueDate", "dateOfStateCertificationCommision", "numberOfStateCertificationCommision", "numberOfStudentExcludeOrder", "state", "dataType",
                "dateOfDuplicateDocumentOrder", "numberOfDuplicateDocumentOrder", "replaceDate"};

        final FrdoEducationLevel educationLevel = context.get("educationLevel");
        final String lastNameOfOwner = context.get("lastNameOfOwner");
        final String firstNameOfOwner = context.get("firstNameOfOwner");
        final String middleNameOfOwner = context.get("middleNameOfOwner");
        final FrdoEduDocumentType eduDocumentType = context.get("eduDocumentType");
        final String nameOfdegreeOrQualification = context.get("nameOfdegreeOrQualification");
        final String educationLevelTitle = context.get("educationLevelTitle");

        if (educationLevel != null)
            dql.where(eq(property(FrdoEduDocument.blank().eduDocumentType().educationLevel().fromAlias(alias)), value(educationLevel)));
        if (lastNameOfOwner != null)
            dql.where(likeUpper(property(FrdoEduDocument.lastNameOfOwner().fromAlias(alias)), value(CoreStringUtils.escapeLike(lastNameOfOwner))));
        if (firstNameOfOwner != null)
            dql.where(likeUpper(property(FrdoEduDocument.firstNameOfOwner().fromAlias(alias)), value(CoreStringUtils.escapeLike(firstNameOfOwner))));
        if (middleNameOfOwner != null)
            dql.where(likeUpper(property(FrdoEduDocument.middleNameOfOwner().fromAlias(alias)), value(CoreStringUtils.escapeLike(middleNameOfOwner))));
        if (eduDocumentType != null)
            dql.where(eq(property(FrdoEduDocument.blank().eduDocumentType().fromAlias(alias)), value(eduDocumentType)));
        if (nameOfdegreeOrQualification != null)
            dql.where(likeUpper(property(FrdoEduDocument.nameOfdegreeOrQualification().fromAlias(alias)), value(CoreStringUtils.escapeLike(nameOfdegreeOrQualification))));
        if (educationLevelTitle != null)
            dql.where(likeUpper(property(FrdoEduDocument.educationLevelTitle().fromAlias(alias)), value(CoreStringUtils.escapeLike(educationLevelTitle))));

        for (String filterName : simpleFilters)
        {
            final Object value = context.get(filterName);
            if (value == null)
                continue;

            dql.where(eq(property(alias, filterName), commonValue(value)));
        }
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        return output;
    }
}
