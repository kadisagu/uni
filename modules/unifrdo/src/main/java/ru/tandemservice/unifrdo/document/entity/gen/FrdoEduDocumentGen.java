package ru.tandemservice.unifrdo.document.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unifrdo.catalog.entity.FrdoDevelopForm;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentDataType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ об образовании (ФРДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FrdoEduDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifrdo.document.entity.FrdoEduDocument";
    public static final String ENTITY_NAME = "frdoEduDocument";
    public static final int VERSION_HASH = -1660838979;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLANK = "blank";
    public static final String P_SERIA_BLANK = "seriaBlank";
    public static final String P_NUMBER_BLANK = "numberBlank";
    public static final String L_STATE = "state";
    public static final String L_DATA_TYPE = "dataType";
    public static final String P_YEAR = "year";
    public static final String P_REG_INDEX = "regIndex";
    public static final String P_SERIAL_REG_NUMBER = "serialRegNumber";
    public static final String P_LAST_NAME_OF_OWNER = "lastNameOfOwner";
    public static final String P_FIRST_NAME_OF_OWNER = "firstNameOfOwner";
    public static final String P_MIDDLE_NAME_OF_OWNER = "middleNameOfOwner";
    public static final String P_ISSUE_DATE = "issueDate";
    public static final String L_ISSUE_ADDRESS = "issueAddress";
    public static final String P_EDUCATION_LEVEL_TITLE = "educationLevelTitle";
    public static final String P_OKSO_QUALIFICATION_CODE = "oksoQualificationCode";
    public static final String P_CATALOG_SPECIALITY_CODE = "catalogSpecialityCode";
    public static final String P_NAME_OFDEGREE_OR_QUALIFICATION = "nameOfdegreeOrQualification";
    public static final String P_DATE_OF_STATE_CERTIFICATION_COMMISION = "dateOfStateCertificationCommision";
    public static final String P_NUMBER_OF_STATE_CERTIFICATION_COMMISION = "numberOfStateCertificationCommision";
    public static final String P_FIO_OF_COMMISSION_CHAIRMAN = "fioOfCommissionChairman";
    public static final String P_STANDART_EDU_PERIOD = "standartEduPeriod";
    public static final String P_NUMBER_OF_STUDENT_EXCLUDE_ORDER = "numberOfStudentExcludeOrder";
    public static final String P_END_DATE = "endDate";
    public static final String P_ENTRANCE_DATE = "entranceDate";
    public static final String P_ENTRANCE_YEAR = "entranceYear";
    public static final String P_END_YEAR = "endYear";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String P_SPECIALIZATION = "specialization";
    public static final String P_EDU_INSTITUTION = "eduInstitution";
    public static final String P_LAST_EDU_INSTITUTION_DOCUMENT = "lastEduInstitutionDocument";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String P_DATE_OF_DUPLICATE_DOCUMENT_ORDER = "dateOfDuplicateDocumentOrder";
    public static final String P_NUMBER_OF_DUPLICATE_DOCUMENT_ORDER = "numberOfDuplicateDocumentOrder";
    public static final String P_REPLACE_DATE = "replaceDate";
    public static final String P_CORRUPTED_DATE = "corruptedDate";
    public static final String P_SPECIAL_RANK = "specialRank";
    public static final String P_FIO_OF_ORG_UNIT_HEAD = "fioOfOrgUnitHead";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_LAST_CHANGE_DATE = "lastChangeDate";
    public static final String P_CREATOR_NAME = "creatorName";
    public static final String P_SERIAL_REG_NUMBER_UNIQ_KEY = "serialRegNumberUniqKey";
    public static final String P_DOCUMENT_TITLE = "documentTitle";
    public static final String P_FIO_OF_OWNER = "fioOfOwner";

    private FrdoEduDocumentBlank _blank;     // Бланк документа об образовании (ФРДО)
    private String _seriaBlank;     // Серия бланка документа
    private String _numberBlank;     // Номер бланка документа
    private FrdoEduDocumentState _state;     // Состояние документа об образовании (ФРДО)
    private FrdoEduDocumentDataType _dataType;     // Тип данных документа об образовании (ФРДО)
    private int _year;     // Год
    private String _regIndex;     // Индекс книги регистрации
    private String _serialRegNumber;     // Порядковый регистрационный номер
    private String _lastNameOfOwner;     // Фамилия лица, получившего документ
    private String _firstNameOfOwner;     // Имя лица, получившего документ
    private String _middleNameOfOwner;     // Отчество лица, получившего документ
    private Date _issueDate;     // Дата выдачи документа
    private AddressItem _issueAddress;     // Город выдачи
    private String _educationLevelTitle;     // Наименование направления подготовки (специальности)
    private String _oksoQualificationCode;     // Код квалификации по ОКСО
    private String _catalogSpecialityCode;     // Код специальности по справочнику
    private String _nameOfdegreeOrQualification;     // Наименование присвоенной степени или квалификации
    private Date _dateOfStateCertificationCommision;     // Дата протокола государственной аттестационной комиссии
    private String _numberOfStateCertificationCommision;     // Номер протокола государственной аттестационной комиссии
    private String _fioOfCommissionChairman;     // ФИО председателя комиссии
    private String _standartEduPeriod;     // Нормативный период обучения
    private String _numberOfStudentExcludeOrder;     // Номер приказа об отчислении студента (слушателя)
    private Date _endDate;     // Дата окончания
    private Date _entranceDate;     // Дата поступления
    private Integer _entranceYear;     // Год поступления
    private Integer _endYear;     // Год окончания
    private FrdoDevelopForm _developForm;     // Форма освоения (ФРДО)
    private Date _birthDate;     // Дата рождения
    private String _specialization;     // Специализация
    private String _eduInstitution;     // Образовательное учреждение
    private String _lastEduInstitutionDocument;     // Предыдущий документ об образовании
    private FrdoEduDocument _eduDocument;     // Документ об образовании (ФРДО)
    private Date _dateOfDuplicateDocumentOrder;     // Дата приказа о выдаче дубликата документа
    private String _numberOfDuplicateDocumentOrder;     // Номер приказа о выдаче дубликата документа
    private Date _replaceDate;     // Дата замены документа
    private Date _corruptedDate;     // Дата списания испорченного бланка
    private String _specialRank;     // Специальное звание
    private String _fioOfOrgUnitHead;     // ФИО руководителя ОУ
    private Date _formingDate;     // Дата формирования документа
    private Date _lastChangeDate;     // Дата последнего изменения
    private String _creatorName;     // Имя пользователя, создавшего или изменившего запись
    private String _serialRegNumberUniqKey;     // Ключ уникальности для serialRegNumber

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Бланк документа об образовании (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEduDocumentBlank getBlank()
    {
        return _blank;
    }

    /**
     * @param blank Бланк документа об образовании (ФРДО). Свойство не может быть null.
     */
    public void setBlank(FrdoEduDocumentBlank blank)
    {
        dirty(_blank, blank);
        _blank = blank;
    }

    /**
     * @return Серия бланка документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSeriaBlank()
    {
        return _seriaBlank;
    }

    /**
     * @param seriaBlank Серия бланка документа. Свойство не может быть null.
     */
    public void setSeriaBlank(String seriaBlank)
    {
        dirty(_seriaBlank, seriaBlank);
        _seriaBlank = seriaBlank;
    }

    /**
     * @return Номер бланка документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumberBlank()
    {
        return _numberBlank;
    }

    /**
     * @param numberBlank Номер бланка документа. Свойство не может быть null.
     */
    public void setNumberBlank(String numberBlank)
    {
        dirty(_numberBlank, numberBlank);
        _numberBlank = numberBlank;
    }

    /**
     * @return Состояние документа об образовании (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEduDocumentState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние документа об образовании (ФРДО). Свойство не может быть null.
     */
    public void setState(FrdoEduDocumentState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Тип данных документа об образовании (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEduDocumentDataType getDataType()
    {
        return _dataType;
    }

    /**
     * @param dataType Тип данных документа об образовании (ФРДО). Свойство не может быть null.
     */
    public void setDataType(FrdoEduDocumentDataType dataType)
    {
        dirty(_dataType, dataType);
        _dataType = dataType;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Индекс книги регистрации.
     */
    @Length(max=255)
    public String getRegIndex()
    {
        return _regIndex;
    }

    /**
     * @param regIndex Индекс книги регистрации.
     */
    public void setRegIndex(String regIndex)
    {
        dirty(_regIndex, regIndex);
        _regIndex = regIndex;
    }

    /**
     * @return Порядковый регистрационный номер.
     */
    @Length(max=255)
    public String getSerialRegNumber()
    {
        return _serialRegNumber;
    }

    /**
     * @param serialRegNumber Порядковый регистрационный номер.
     */
    public void setSerialRegNumber(String serialRegNumber)
    {
        dirty(_serialRegNumber, serialRegNumber);
        _serialRegNumber = serialRegNumber;
    }

    /**
     * @return Фамилия лица, получившего документ.
     */
    @Length(max=255)
    public String getLastNameOfOwner()
    {
        return _lastNameOfOwner;
    }

    /**
     * @param lastNameOfOwner Фамилия лица, получившего документ.
     */
    public void setLastNameOfOwner(String lastNameOfOwner)
    {
        dirty(_lastNameOfOwner, lastNameOfOwner);
        _lastNameOfOwner = lastNameOfOwner;
    }

    /**
     * @return Имя лица, получившего документ.
     */
    @Length(max=255)
    public String getFirstNameOfOwner()
    {
        return _firstNameOfOwner;
    }

    /**
     * @param firstNameOfOwner Имя лица, получившего документ.
     */
    public void setFirstNameOfOwner(String firstNameOfOwner)
    {
        dirty(_firstNameOfOwner, firstNameOfOwner);
        _firstNameOfOwner = firstNameOfOwner;
    }

    /**
     * @return Отчество лица, получившего документ.
     */
    @Length(max=255)
    public String getMiddleNameOfOwner()
    {
        return _middleNameOfOwner;
    }

    /**
     * @param middleNameOfOwner Отчество лица, получившего документ.
     */
    public void setMiddleNameOfOwner(String middleNameOfOwner)
    {
        dirty(_middleNameOfOwner, middleNameOfOwner);
        _middleNameOfOwner = middleNameOfOwner;
    }

    /**
     * @return Дата выдачи документа.
     */
    public Date getIssueDate()
    {
        return _issueDate;
    }

    /**
     * @param issueDate Дата выдачи документа.
     */
    public void setIssueDate(Date issueDate)
    {
        dirty(_issueDate, issueDate);
        _issueDate = issueDate;
    }

    /**
     * @return Город выдачи.
     */
    public AddressItem getIssueAddress()
    {
        return _issueAddress;
    }

    /**
     * @param issueAddress Город выдачи.
     */
    public void setIssueAddress(AddressItem issueAddress)
    {
        dirty(_issueAddress, issueAddress);
        _issueAddress = issueAddress;
    }

    /**
     * @return Наименование направления подготовки (специальности).
     */
    @Length(max=255)
    public String getEducationLevelTitle()
    {
        return _educationLevelTitle;
    }

    /**
     * @param educationLevelTitle Наименование направления подготовки (специальности).
     */
    public void setEducationLevelTitle(String educationLevelTitle)
    {
        dirty(_educationLevelTitle, educationLevelTitle);
        _educationLevelTitle = educationLevelTitle;
    }

    /**
     * @return Код квалификации по ОКСО.
     */
    @Length(max=255)
    public String getOksoQualificationCode()
    {
        return _oksoQualificationCode;
    }

    /**
     * @param oksoQualificationCode Код квалификации по ОКСО.
     */
    public void setOksoQualificationCode(String oksoQualificationCode)
    {
        dirty(_oksoQualificationCode, oksoQualificationCode);
        _oksoQualificationCode = oksoQualificationCode;
    }

    /**
     * @return Код специальности по справочнику.
     */
    @Length(max=255)
    public String getCatalogSpecialityCode()
    {
        return _catalogSpecialityCode;
    }

    /**
     * @param catalogSpecialityCode Код специальности по справочнику.
     */
    public void setCatalogSpecialityCode(String catalogSpecialityCode)
    {
        dirty(_catalogSpecialityCode, catalogSpecialityCode);
        _catalogSpecialityCode = catalogSpecialityCode;
    }

    /**
     * @return Наименование присвоенной степени или квалификации.
     */
    @Length(max=255)
    public String getNameOfdegreeOrQualification()
    {
        return _nameOfdegreeOrQualification;
    }

    /**
     * @param nameOfdegreeOrQualification Наименование присвоенной степени или квалификации.
     */
    public void setNameOfdegreeOrQualification(String nameOfdegreeOrQualification)
    {
        dirty(_nameOfdegreeOrQualification, nameOfdegreeOrQualification);
        _nameOfdegreeOrQualification = nameOfdegreeOrQualification;
    }

    /**
     * @return Дата протокола государственной аттестационной комиссии.
     */
    public Date getDateOfStateCertificationCommision()
    {
        return _dateOfStateCertificationCommision;
    }

    /**
     * @param dateOfStateCertificationCommision Дата протокола государственной аттестационной комиссии.
     */
    public void setDateOfStateCertificationCommision(Date dateOfStateCertificationCommision)
    {
        dirty(_dateOfStateCertificationCommision, dateOfStateCertificationCommision);
        _dateOfStateCertificationCommision = dateOfStateCertificationCommision;
    }

    /**
     * @return Номер протокола государственной аттестационной комиссии.
     */
    @Length(max=255)
    public String getNumberOfStateCertificationCommision()
    {
        return _numberOfStateCertificationCommision;
    }

    /**
     * @param numberOfStateCertificationCommision Номер протокола государственной аттестационной комиссии.
     */
    public void setNumberOfStateCertificationCommision(String numberOfStateCertificationCommision)
    {
        dirty(_numberOfStateCertificationCommision, numberOfStateCertificationCommision);
        _numberOfStateCertificationCommision = numberOfStateCertificationCommision;
    }

    /**
     * @return ФИО председателя комиссии.
     */
    @Length(max=255)
    public String getFioOfCommissionChairman()
    {
        return _fioOfCommissionChairman;
    }

    /**
     * @param fioOfCommissionChairman ФИО председателя комиссии.
     */
    public void setFioOfCommissionChairman(String fioOfCommissionChairman)
    {
        dirty(_fioOfCommissionChairman, fioOfCommissionChairman);
        _fioOfCommissionChairman = fioOfCommissionChairman;
    }

    /**
     * @return Нормативный период обучения.
     */
    @Length(max=255)
    public String getStandartEduPeriod()
    {
        return _standartEduPeriod;
    }

    /**
     * @param standartEduPeriod Нормативный период обучения.
     */
    public void setStandartEduPeriod(String standartEduPeriod)
    {
        dirty(_standartEduPeriod, standartEduPeriod);
        _standartEduPeriod = standartEduPeriod;
    }

    /**
     * @return Номер приказа об отчислении студента (слушателя).
     */
    @Length(max=255)
    public String getNumberOfStudentExcludeOrder()
    {
        return _numberOfStudentExcludeOrder;
    }

    /**
     * @param numberOfStudentExcludeOrder Номер приказа об отчислении студента (слушателя).
     */
    public void setNumberOfStudentExcludeOrder(String numberOfStudentExcludeOrder)
    {
        dirty(_numberOfStudentExcludeOrder, numberOfStudentExcludeOrder);
        _numberOfStudentExcludeOrder = numberOfStudentExcludeOrder;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Дата поступления.
     */
    public Date getEntranceDate()
    {
        return _entranceDate;
    }

    /**
     * @param entranceDate Дата поступления.
     */
    public void setEntranceDate(Date entranceDate)
    {
        dirty(_entranceDate, entranceDate);
        _entranceDate = entranceDate;
    }

    /**
     * @return Год поступления.
     */
    public Integer getEntranceYear()
    {
        return _entranceYear;
    }

    /**
     * @param entranceYear Год поступления.
     */
    public void setEntranceYear(Integer entranceYear)
    {
        dirty(_entranceYear, entranceYear);
        _entranceYear = entranceYear;
    }

    /**
     * @return Год окончания.
     */
    public Integer getEndYear()
    {
        return _endYear;
    }

    /**
     * @param endYear Год окончания.
     */
    public void setEndYear(Integer endYear)
    {
        dirty(_endYear, endYear);
        _endYear = endYear;
    }

    /**
     * @return Форма освоения (ФРДО).
     */
    public FrdoDevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения (ФРДО).
     */
    public void setDevelopForm(FrdoDevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Специализация.
     */
    @Length(max=255)
    public String getSpecialization()
    {
        return _specialization;
    }

    /**
     * @param specialization Специализация.
     */
    public void setSpecialization(String specialization)
    {
        dirty(_specialization, specialization);
        _specialization = specialization;
    }

    /**
     * @return Образовательное учреждение.
     */
    @Length(max=255)
    public String getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательное учреждение.
     */
    public void setEduInstitution(String eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    /**
     * @return Предыдущий документ об образовании.
     */
    @Length(max=255)
    public String getLastEduInstitutionDocument()
    {
        return _lastEduInstitutionDocument;
    }

    /**
     * @param lastEduInstitutionDocument Предыдущий документ об образовании.
     */
    public void setLastEduInstitutionDocument(String lastEduInstitutionDocument)
    {
        dirty(_lastEduInstitutionDocument, lastEduInstitutionDocument);
        _lastEduInstitutionDocument = lastEduInstitutionDocument;
    }

    /**
     * @return Документ об образовании (ФРДО).
     */
    public FrdoEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании (ФРДО).
     */
    public void setEduDocument(FrdoEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Дата приказа о выдаче дубликата документа.
     */
    public Date getDateOfDuplicateDocumentOrder()
    {
        return _dateOfDuplicateDocumentOrder;
    }

    /**
     * @param dateOfDuplicateDocumentOrder Дата приказа о выдаче дубликата документа.
     */
    public void setDateOfDuplicateDocumentOrder(Date dateOfDuplicateDocumentOrder)
    {
        dirty(_dateOfDuplicateDocumentOrder, dateOfDuplicateDocumentOrder);
        _dateOfDuplicateDocumentOrder = dateOfDuplicateDocumentOrder;
    }

    /**
     * @return Номер приказа о выдаче дубликата документа.
     */
    @Length(max=255)
    public String getNumberOfDuplicateDocumentOrder()
    {
        return _numberOfDuplicateDocumentOrder;
    }

    /**
     * @param numberOfDuplicateDocumentOrder Номер приказа о выдаче дубликата документа.
     */
    public void setNumberOfDuplicateDocumentOrder(String numberOfDuplicateDocumentOrder)
    {
        dirty(_numberOfDuplicateDocumentOrder, numberOfDuplicateDocumentOrder);
        _numberOfDuplicateDocumentOrder = numberOfDuplicateDocumentOrder;
    }

    /**
     * @return Дата замены документа.
     */
    public Date getReplaceDate()
    {
        return _replaceDate;
    }

    /**
     * @param replaceDate Дата замены документа.
     */
    public void setReplaceDate(Date replaceDate)
    {
        dirty(_replaceDate, replaceDate);
        _replaceDate = replaceDate;
    }

    /**
     * @return Дата списания испорченного бланка.
     */
    public Date getCorruptedDate()
    {
        return _corruptedDate;
    }

    /**
     * @param corruptedDate Дата списания испорченного бланка.
     */
    public void setCorruptedDate(Date corruptedDate)
    {
        dirty(_corruptedDate, corruptedDate);
        _corruptedDate = corruptedDate;
    }

    /**
     * @return Специальное звание.
     */
    @Length(max=255)
    public String getSpecialRank()
    {
        return _specialRank;
    }

    /**
     * @param specialRank Специальное звание.
     */
    public void setSpecialRank(String specialRank)
    {
        dirty(_specialRank, specialRank);
        _specialRank = specialRank;
    }

    /**
     * @return ФИО руководителя ОУ.
     */
    @Length(max=255)
    public String getFioOfOrgUnitHead()
    {
        return _fioOfOrgUnitHead;
    }

    /**
     * @param fioOfOrgUnitHead ФИО руководителя ОУ.
     */
    public void setFioOfOrgUnitHead(String fioOfOrgUnitHead)
    {
        dirty(_fioOfOrgUnitHead, fioOfOrgUnitHead);
        _fioOfOrgUnitHead = fioOfOrgUnitHead;
    }

    /**
     * @return Дата формирования документа. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования документа. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата последнего изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getLastChangeDate()
    {
        return _lastChangeDate;
    }

    /**
     * @param lastChangeDate Дата последнего изменения. Свойство не может быть null.
     */
    public void setLastChangeDate(Date lastChangeDate)
    {
        dirty(_lastChangeDate, lastChangeDate);
        _lastChangeDate = lastChangeDate;
    }

    /**
     * @return Имя пользователя, создавшего или изменившего запись.
     */
    @Length(max=255)
    public String getCreatorName()
    {
        return _creatorName;
    }

    /**
     * @param creatorName Имя пользователя, создавшего или изменившего запись.
     */
    public void setCreatorName(String creatorName)
    {
        dirty(_creatorName, creatorName);
        _creatorName = creatorName;
    }

    /**
     * Составной ключ уникальности для полей serialRegNumber, regIndex, year, blank.eduDocumentType.educationLevel.code.
     * Учитывается, если (regIndex is not null) and (serialRegNumber is not null).
     * Обновляется при изменении полей serialRegNumber, regIndex, year, .
     *
     * @return Ключ уникальности для serialRegNumber. Свойство должно быть уникальным.
     */
    @Length(max=255)
    public String getSerialRegNumberUniqKey()
    {
        return _serialRegNumberUniqKey;
    }

    /**
     * @param serialRegNumberUniqKey Ключ уникальности для serialRegNumber. Свойство должно быть уникальным.
     */
    public void setSerialRegNumberUniqKey(String serialRegNumberUniqKey)
    {
        dirty(_serialRegNumberUniqKey, serialRegNumberUniqKey);
        _serialRegNumberUniqKey = serialRegNumberUniqKey;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FrdoEduDocumentGen)
        {
            setBlank(((FrdoEduDocument)another).getBlank());
            setSeriaBlank(((FrdoEduDocument)another).getSeriaBlank());
            setNumberBlank(((FrdoEduDocument)another).getNumberBlank());
            setState(((FrdoEduDocument)another).getState());
            setDataType(((FrdoEduDocument)another).getDataType());
            setYear(((FrdoEduDocument)another).getYear());
            setRegIndex(((FrdoEduDocument)another).getRegIndex());
            setSerialRegNumber(((FrdoEduDocument)another).getSerialRegNumber());
            setLastNameOfOwner(((FrdoEduDocument)another).getLastNameOfOwner());
            setFirstNameOfOwner(((FrdoEduDocument)another).getFirstNameOfOwner());
            setMiddleNameOfOwner(((FrdoEduDocument)another).getMiddleNameOfOwner());
            setIssueDate(((FrdoEduDocument)another).getIssueDate());
            setIssueAddress(((FrdoEduDocument)another).getIssueAddress());
            setEducationLevelTitle(((FrdoEduDocument)another).getEducationLevelTitle());
            setOksoQualificationCode(((FrdoEduDocument)another).getOksoQualificationCode());
            setCatalogSpecialityCode(((FrdoEduDocument)another).getCatalogSpecialityCode());
            setNameOfdegreeOrQualification(((FrdoEduDocument)another).getNameOfdegreeOrQualification());
            setDateOfStateCertificationCommision(((FrdoEduDocument)another).getDateOfStateCertificationCommision());
            setNumberOfStateCertificationCommision(((FrdoEduDocument)another).getNumberOfStateCertificationCommision());
            setFioOfCommissionChairman(((FrdoEduDocument)another).getFioOfCommissionChairman());
            setStandartEduPeriod(((FrdoEduDocument)another).getStandartEduPeriod());
            setNumberOfStudentExcludeOrder(((FrdoEduDocument)another).getNumberOfStudentExcludeOrder());
            setEndDate(((FrdoEduDocument)another).getEndDate());
            setEntranceDate(((FrdoEduDocument)another).getEntranceDate());
            setEntranceYear(((FrdoEduDocument)another).getEntranceYear());
            setEndYear(((FrdoEduDocument)another).getEndYear());
            setDevelopForm(((FrdoEduDocument)another).getDevelopForm());
            setBirthDate(((FrdoEduDocument)another).getBirthDate());
            setSpecialization(((FrdoEduDocument)another).getSpecialization());
            setEduInstitution(((FrdoEduDocument)another).getEduInstitution());
            setLastEduInstitutionDocument(((FrdoEduDocument)another).getLastEduInstitutionDocument());
            setEduDocument(((FrdoEduDocument)another).getEduDocument());
            setDateOfDuplicateDocumentOrder(((FrdoEduDocument)another).getDateOfDuplicateDocumentOrder());
            setNumberOfDuplicateDocumentOrder(((FrdoEduDocument)another).getNumberOfDuplicateDocumentOrder());
            setReplaceDate(((FrdoEduDocument)another).getReplaceDate());
            setCorruptedDate(((FrdoEduDocument)another).getCorruptedDate());
            setSpecialRank(((FrdoEduDocument)another).getSpecialRank());
            setFioOfOrgUnitHead(((FrdoEduDocument)another).getFioOfOrgUnitHead());
            setFormingDate(((FrdoEduDocument)another).getFormingDate());
            setLastChangeDate(((FrdoEduDocument)another).getLastChangeDate());
            setCreatorName(((FrdoEduDocument)another).getCreatorName());
            setSerialRegNumberUniqKey(((FrdoEduDocument)another).getSerialRegNumberUniqKey());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FrdoEduDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FrdoEduDocument.class;
        }

        public T newInstance()
        {
            return (T) new FrdoEduDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "blank":
                    return obj.getBlank();
                case "seriaBlank":
                    return obj.getSeriaBlank();
                case "numberBlank":
                    return obj.getNumberBlank();
                case "state":
                    return obj.getState();
                case "dataType":
                    return obj.getDataType();
                case "year":
                    return obj.getYear();
                case "regIndex":
                    return obj.getRegIndex();
                case "serialRegNumber":
                    return obj.getSerialRegNumber();
                case "lastNameOfOwner":
                    return obj.getLastNameOfOwner();
                case "firstNameOfOwner":
                    return obj.getFirstNameOfOwner();
                case "middleNameOfOwner":
                    return obj.getMiddleNameOfOwner();
                case "issueDate":
                    return obj.getIssueDate();
                case "issueAddress":
                    return obj.getIssueAddress();
                case "educationLevelTitle":
                    return obj.getEducationLevelTitle();
                case "oksoQualificationCode":
                    return obj.getOksoQualificationCode();
                case "catalogSpecialityCode":
                    return obj.getCatalogSpecialityCode();
                case "nameOfdegreeOrQualification":
                    return obj.getNameOfdegreeOrQualification();
                case "dateOfStateCertificationCommision":
                    return obj.getDateOfStateCertificationCommision();
                case "numberOfStateCertificationCommision":
                    return obj.getNumberOfStateCertificationCommision();
                case "fioOfCommissionChairman":
                    return obj.getFioOfCommissionChairman();
                case "standartEduPeriod":
                    return obj.getStandartEduPeriod();
                case "numberOfStudentExcludeOrder":
                    return obj.getNumberOfStudentExcludeOrder();
                case "endDate":
                    return obj.getEndDate();
                case "entranceDate":
                    return obj.getEntranceDate();
                case "entranceYear":
                    return obj.getEntranceYear();
                case "endYear":
                    return obj.getEndYear();
                case "developForm":
                    return obj.getDevelopForm();
                case "birthDate":
                    return obj.getBirthDate();
                case "specialization":
                    return obj.getSpecialization();
                case "eduInstitution":
                    return obj.getEduInstitution();
                case "lastEduInstitutionDocument":
                    return obj.getLastEduInstitutionDocument();
                case "eduDocument":
                    return obj.getEduDocument();
                case "dateOfDuplicateDocumentOrder":
                    return obj.getDateOfDuplicateDocumentOrder();
                case "numberOfDuplicateDocumentOrder":
                    return obj.getNumberOfDuplicateDocumentOrder();
                case "replaceDate":
                    return obj.getReplaceDate();
                case "corruptedDate":
                    return obj.getCorruptedDate();
                case "specialRank":
                    return obj.getSpecialRank();
                case "fioOfOrgUnitHead":
                    return obj.getFioOfOrgUnitHead();
                case "formingDate":
                    return obj.getFormingDate();
                case "lastChangeDate":
                    return obj.getLastChangeDate();
                case "creatorName":
                    return obj.getCreatorName();
                case "serialRegNumberUniqKey":
                    return obj.getSerialRegNumberUniqKey();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "blank":
                    obj.setBlank((FrdoEduDocumentBlank) value);
                    return;
                case "seriaBlank":
                    obj.setSeriaBlank((String) value);
                    return;
                case "numberBlank":
                    obj.setNumberBlank((String) value);
                    return;
                case "state":
                    obj.setState((FrdoEduDocumentState) value);
                    return;
                case "dataType":
                    obj.setDataType((FrdoEduDocumentDataType) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "regIndex":
                    obj.setRegIndex((String) value);
                    return;
                case "serialRegNumber":
                    obj.setSerialRegNumber((String) value);
                    return;
                case "lastNameOfOwner":
                    obj.setLastNameOfOwner((String) value);
                    return;
                case "firstNameOfOwner":
                    obj.setFirstNameOfOwner((String) value);
                    return;
                case "middleNameOfOwner":
                    obj.setMiddleNameOfOwner((String) value);
                    return;
                case "issueDate":
                    obj.setIssueDate((Date) value);
                    return;
                case "issueAddress":
                    obj.setIssueAddress((AddressItem) value);
                    return;
                case "educationLevelTitle":
                    obj.setEducationLevelTitle((String) value);
                    return;
                case "oksoQualificationCode":
                    obj.setOksoQualificationCode((String) value);
                    return;
                case "catalogSpecialityCode":
                    obj.setCatalogSpecialityCode((String) value);
                    return;
                case "nameOfdegreeOrQualification":
                    obj.setNameOfdegreeOrQualification((String) value);
                    return;
                case "dateOfStateCertificationCommision":
                    obj.setDateOfStateCertificationCommision((Date) value);
                    return;
                case "numberOfStateCertificationCommision":
                    obj.setNumberOfStateCertificationCommision((String) value);
                    return;
                case "fioOfCommissionChairman":
                    obj.setFioOfCommissionChairman((String) value);
                    return;
                case "standartEduPeriod":
                    obj.setStandartEduPeriod((String) value);
                    return;
                case "numberOfStudentExcludeOrder":
                    obj.setNumberOfStudentExcludeOrder((String) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "entranceDate":
                    obj.setEntranceDate((Date) value);
                    return;
                case "entranceYear":
                    obj.setEntranceYear((Integer) value);
                    return;
                case "endYear":
                    obj.setEndYear((Integer) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((FrdoDevelopForm) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "specialization":
                    obj.setSpecialization((String) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((String) value);
                    return;
                case "lastEduInstitutionDocument":
                    obj.setLastEduInstitutionDocument((String) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((FrdoEduDocument) value);
                    return;
                case "dateOfDuplicateDocumentOrder":
                    obj.setDateOfDuplicateDocumentOrder((Date) value);
                    return;
                case "numberOfDuplicateDocumentOrder":
                    obj.setNumberOfDuplicateDocumentOrder((String) value);
                    return;
                case "replaceDate":
                    obj.setReplaceDate((Date) value);
                    return;
                case "corruptedDate":
                    obj.setCorruptedDate((Date) value);
                    return;
                case "specialRank":
                    obj.setSpecialRank((String) value);
                    return;
                case "fioOfOrgUnitHead":
                    obj.setFioOfOrgUnitHead((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "lastChangeDate":
                    obj.setLastChangeDate((Date) value);
                    return;
                case "creatorName":
                    obj.setCreatorName((String) value);
                    return;
                case "serialRegNumberUniqKey":
                    obj.setSerialRegNumberUniqKey((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "blank":
                        return true;
                case "seriaBlank":
                        return true;
                case "numberBlank":
                        return true;
                case "state":
                        return true;
                case "dataType":
                        return true;
                case "year":
                        return true;
                case "regIndex":
                        return true;
                case "serialRegNumber":
                        return true;
                case "lastNameOfOwner":
                        return true;
                case "firstNameOfOwner":
                        return true;
                case "middleNameOfOwner":
                        return true;
                case "issueDate":
                        return true;
                case "issueAddress":
                        return true;
                case "educationLevelTitle":
                        return true;
                case "oksoQualificationCode":
                        return true;
                case "catalogSpecialityCode":
                        return true;
                case "nameOfdegreeOrQualification":
                        return true;
                case "dateOfStateCertificationCommision":
                        return true;
                case "numberOfStateCertificationCommision":
                        return true;
                case "fioOfCommissionChairman":
                        return true;
                case "standartEduPeriod":
                        return true;
                case "numberOfStudentExcludeOrder":
                        return true;
                case "endDate":
                        return true;
                case "entranceDate":
                        return true;
                case "entranceYear":
                        return true;
                case "endYear":
                        return true;
                case "developForm":
                        return true;
                case "birthDate":
                        return true;
                case "specialization":
                        return true;
                case "eduInstitution":
                        return true;
                case "lastEduInstitutionDocument":
                        return true;
                case "eduDocument":
                        return true;
                case "dateOfDuplicateDocumentOrder":
                        return true;
                case "numberOfDuplicateDocumentOrder":
                        return true;
                case "replaceDate":
                        return true;
                case "corruptedDate":
                        return true;
                case "specialRank":
                        return true;
                case "fioOfOrgUnitHead":
                        return true;
                case "formingDate":
                        return true;
                case "lastChangeDate":
                        return true;
                case "creatorName":
                        return true;
                case "serialRegNumberUniqKey":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "blank":
                    return true;
                case "seriaBlank":
                    return true;
                case "numberBlank":
                    return true;
                case "state":
                    return true;
                case "dataType":
                    return true;
                case "year":
                    return true;
                case "regIndex":
                    return true;
                case "serialRegNumber":
                    return true;
                case "lastNameOfOwner":
                    return true;
                case "firstNameOfOwner":
                    return true;
                case "middleNameOfOwner":
                    return true;
                case "issueDate":
                    return true;
                case "issueAddress":
                    return true;
                case "educationLevelTitle":
                    return true;
                case "oksoQualificationCode":
                    return true;
                case "catalogSpecialityCode":
                    return true;
                case "nameOfdegreeOrQualification":
                    return true;
                case "dateOfStateCertificationCommision":
                    return true;
                case "numberOfStateCertificationCommision":
                    return true;
                case "fioOfCommissionChairman":
                    return true;
                case "standartEduPeriod":
                    return true;
                case "numberOfStudentExcludeOrder":
                    return true;
                case "endDate":
                    return true;
                case "entranceDate":
                    return true;
                case "entranceYear":
                    return true;
                case "endYear":
                    return true;
                case "developForm":
                    return true;
                case "birthDate":
                    return true;
                case "specialization":
                    return true;
                case "eduInstitution":
                    return true;
                case "lastEduInstitutionDocument":
                    return true;
                case "eduDocument":
                    return true;
                case "dateOfDuplicateDocumentOrder":
                    return true;
                case "numberOfDuplicateDocumentOrder":
                    return true;
                case "replaceDate":
                    return true;
                case "corruptedDate":
                    return true;
                case "specialRank":
                    return true;
                case "fioOfOrgUnitHead":
                    return true;
                case "formingDate":
                    return true;
                case "lastChangeDate":
                    return true;
                case "creatorName":
                    return true;
                case "serialRegNumberUniqKey":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "blank":
                    return FrdoEduDocumentBlank.class;
                case "seriaBlank":
                    return String.class;
                case "numberBlank":
                    return String.class;
                case "state":
                    return FrdoEduDocumentState.class;
                case "dataType":
                    return FrdoEduDocumentDataType.class;
                case "year":
                    return Integer.class;
                case "regIndex":
                    return String.class;
                case "serialRegNumber":
                    return String.class;
                case "lastNameOfOwner":
                    return String.class;
                case "firstNameOfOwner":
                    return String.class;
                case "middleNameOfOwner":
                    return String.class;
                case "issueDate":
                    return Date.class;
                case "issueAddress":
                    return AddressItem.class;
                case "educationLevelTitle":
                    return String.class;
                case "oksoQualificationCode":
                    return String.class;
                case "catalogSpecialityCode":
                    return String.class;
                case "nameOfdegreeOrQualification":
                    return String.class;
                case "dateOfStateCertificationCommision":
                    return Date.class;
                case "numberOfStateCertificationCommision":
                    return String.class;
                case "fioOfCommissionChairman":
                    return String.class;
                case "standartEduPeriod":
                    return String.class;
                case "numberOfStudentExcludeOrder":
                    return String.class;
                case "endDate":
                    return Date.class;
                case "entranceDate":
                    return Date.class;
                case "entranceYear":
                    return Integer.class;
                case "endYear":
                    return Integer.class;
                case "developForm":
                    return FrdoDevelopForm.class;
                case "birthDate":
                    return Date.class;
                case "specialization":
                    return String.class;
                case "eduInstitution":
                    return String.class;
                case "lastEduInstitutionDocument":
                    return String.class;
                case "eduDocument":
                    return FrdoEduDocument.class;
                case "dateOfDuplicateDocumentOrder":
                    return Date.class;
                case "numberOfDuplicateDocumentOrder":
                    return String.class;
                case "replaceDate":
                    return Date.class;
                case "corruptedDate":
                    return Date.class;
                case "specialRank":
                    return String.class;
                case "fioOfOrgUnitHead":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "lastChangeDate":
                    return Date.class;
                case "creatorName":
                    return String.class;
                case "serialRegNumberUniqKey":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FrdoEduDocument> _dslPath = new Path<FrdoEduDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FrdoEduDocument");
    }
            

    /**
     * @return Бланк документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getBlank()
     */
    public static FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank> blank()
    {
        return _dslPath.blank();
    }

    /**
     * @return Серия бланка документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSeriaBlank()
     */
    public static PropertyPath<String> seriaBlank()
    {
        return _dslPath.seriaBlank();
    }

    /**
     * @return Номер бланка документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberBlank()
     */
    public static PropertyPath<String> numberBlank()
    {
        return _dslPath.numberBlank();
    }

    /**
     * @return Состояние документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getState()
     */
    public static FrdoEduDocumentState.Path<FrdoEduDocumentState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Тип данных документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDataType()
     */
    public static FrdoEduDocumentDataType.Path<FrdoEduDocumentDataType> dataType()
    {
        return _dslPath.dataType();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Индекс книги регистрации.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getRegIndex()
     */
    public static PropertyPath<String> regIndex()
    {
        return _dslPath.regIndex();
    }

    /**
     * @return Порядковый регистрационный номер.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSerialRegNumber()
     */
    public static PropertyPath<String> serialRegNumber()
    {
        return _dslPath.serialRegNumber();
    }

    /**
     * @return Фамилия лица, получившего документ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getLastNameOfOwner()
     */
    public static PropertyPath<String> lastNameOfOwner()
    {
        return _dslPath.lastNameOfOwner();
    }

    /**
     * @return Имя лица, получившего документ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFirstNameOfOwner()
     */
    public static PropertyPath<String> firstNameOfOwner()
    {
        return _dslPath.firstNameOfOwner();
    }

    /**
     * @return Отчество лица, получившего документ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getMiddleNameOfOwner()
     */
    public static PropertyPath<String> middleNameOfOwner()
    {
        return _dslPath.middleNameOfOwner();
    }

    /**
     * @return Дата выдачи документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getIssueDate()
     */
    public static PropertyPath<Date> issueDate()
    {
        return _dslPath.issueDate();
    }

    /**
     * @return Город выдачи.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getIssueAddress()
     */
    public static AddressItem.Path<AddressItem> issueAddress()
    {
        return _dslPath.issueAddress();
    }

    /**
     * @return Наименование направления подготовки (специальности).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEducationLevelTitle()
     */
    public static PropertyPath<String> educationLevelTitle()
    {
        return _dslPath.educationLevelTitle();
    }

    /**
     * @return Код квалификации по ОКСО.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getOksoQualificationCode()
     */
    public static PropertyPath<String> oksoQualificationCode()
    {
        return _dslPath.oksoQualificationCode();
    }

    /**
     * @return Код специальности по справочнику.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getCatalogSpecialityCode()
     */
    public static PropertyPath<String> catalogSpecialityCode()
    {
        return _dslPath.catalogSpecialityCode();
    }

    /**
     * @return Наименование присвоенной степени или квалификации.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNameOfdegreeOrQualification()
     */
    public static PropertyPath<String> nameOfdegreeOrQualification()
    {
        return _dslPath.nameOfdegreeOrQualification();
    }

    /**
     * @return Дата протокола государственной аттестационной комиссии.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDateOfStateCertificationCommision()
     */
    public static PropertyPath<Date> dateOfStateCertificationCommision()
    {
        return _dslPath.dateOfStateCertificationCommision();
    }

    /**
     * @return Номер протокола государственной аттестационной комиссии.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberOfStateCertificationCommision()
     */
    public static PropertyPath<String> numberOfStateCertificationCommision()
    {
        return _dslPath.numberOfStateCertificationCommision();
    }

    /**
     * @return ФИО председателя комиссии.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFioOfCommissionChairman()
     */
    public static PropertyPath<String> fioOfCommissionChairman()
    {
        return _dslPath.fioOfCommissionChairman();
    }

    /**
     * @return Нормативный период обучения.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getStandartEduPeriod()
     */
    public static PropertyPath<String> standartEduPeriod()
    {
        return _dslPath.standartEduPeriod();
    }

    /**
     * @return Номер приказа об отчислении студента (слушателя).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberOfStudentExcludeOrder()
     */
    public static PropertyPath<String> numberOfStudentExcludeOrder()
    {
        return _dslPath.numberOfStudentExcludeOrder();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Дата поступления.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEntranceDate()
     */
    public static PropertyPath<Date> entranceDate()
    {
        return _dslPath.entranceDate();
    }

    /**
     * @return Год поступления.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEntranceYear()
     */
    public static PropertyPath<Integer> entranceYear()
    {
        return _dslPath.entranceYear();
    }

    /**
     * @return Год окончания.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEndYear()
     */
    public static PropertyPath<Integer> endYear()
    {
        return _dslPath.endYear();
    }

    /**
     * @return Форма освоения (ФРДО).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDevelopForm()
     */
    public static FrdoDevelopForm.Path<FrdoDevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Специализация.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSpecialization()
     */
    public static PropertyPath<String> specialization()
    {
        return _dslPath.specialization();
    }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEduInstitution()
     */
    public static PropertyPath<String> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    /**
     * @return Предыдущий документ об образовании.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getLastEduInstitutionDocument()
     */
    public static PropertyPath<String> lastEduInstitutionDocument()
    {
        return _dslPath.lastEduInstitutionDocument();
    }

    /**
     * @return Документ об образовании (ФРДО).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEduDocument()
     */
    public static FrdoEduDocument.Path<FrdoEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Дата приказа о выдаче дубликата документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDateOfDuplicateDocumentOrder()
     */
    public static PropertyPath<Date> dateOfDuplicateDocumentOrder()
    {
        return _dslPath.dateOfDuplicateDocumentOrder();
    }

    /**
     * @return Номер приказа о выдаче дубликата документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberOfDuplicateDocumentOrder()
     */
    public static PropertyPath<String> numberOfDuplicateDocumentOrder()
    {
        return _dslPath.numberOfDuplicateDocumentOrder();
    }

    /**
     * @return Дата замены документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getReplaceDate()
     */
    public static PropertyPath<Date> replaceDate()
    {
        return _dslPath.replaceDate();
    }

    /**
     * @return Дата списания испорченного бланка.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getCorruptedDate()
     */
    public static PropertyPath<Date> corruptedDate()
    {
        return _dslPath.corruptedDate();
    }

    /**
     * @return Специальное звание.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSpecialRank()
     */
    public static PropertyPath<String> specialRank()
    {
        return _dslPath.specialRank();
    }

    /**
     * @return ФИО руководителя ОУ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFioOfOrgUnitHead()
     */
    public static PropertyPath<String> fioOfOrgUnitHead()
    {
        return _dslPath.fioOfOrgUnitHead();
    }

    /**
     * @return Дата формирования документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата последнего изменения. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getLastChangeDate()
     */
    public static PropertyPath<Date> lastChangeDate()
    {
        return _dslPath.lastChangeDate();
    }

    /**
     * @return Имя пользователя, создавшего или изменившего запись.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getCreatorName()
     */
    public static PropertyPath<String> creatorName()
    {
        return _dslPath.creatorName();
    }

    /**
     * Составной ключ уникальности для полей serialRegNumber, regIndex, year, blank.eduDocumentType.educationLevel.code.
     * Учитывается, если (regIndex is not null) and (serialRegNumber is not null).
     * Обновляется при изменении полей serialRegNumber, regIndex, year, .
     *
     * @return Ключ уникальности для serialRegNumber. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSerialRegNumberUniqKey()
     */
    public static PropertyPath<String> serialRegNumberUniqKey()
    {
        return _dslPath.serialRegNumberUniqKey();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDocumentTitle()
     */
    public static SupportedPropertyPath<String> documentTitle()
    {
        return _dslPath.documentTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFioOfOwner()
     */
    public static SupportedPropertyPath<String> fioOfOwner()
    {
        return _dslPath.fioOfOwner();
    }

    public static class Path<E extends FrdoEduDocument> extends EntityPath<E>
    {
        private FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank> _blank;
        private PropertyPath<String> _seriaBlank;
        private PropertyPath<String> _numberBlank;
        private FrdoEduDocumentState.Path<FrdoEduDocumentState> _state;
        private FrdoEduDocumentDataType.Path<FrdoEduDocumentDataType> _dataType;
        private PropertyPath<Integer> _year;
        private PropertyPath<String> _regIndex;
        private PropertyPath<String> _serialRegNumber;
        private PropertyPath<String> _lastNameOfOwner;
        private PropertyPath<String> _firstNameOfOwner;
        private PropertyPath<String> _middleNameOfOwner;
        private PropertyPath<Date> _issueDate;
        private AddressItem.Path<AddressItem> _issueAddress;
        private PropertyPath<String> _educationLevelTitle;
        private PropertyPath<String> _oksoQualificationCode;
        private PropertyPath<String> _catalogSpecialityCode;
        private PropertyPath<String> _nameOfdegreeOrQualification;
        private PropertyPath<Date> _dateOfStateCertificationCommision;
        private PropertyPath<String> _numberOfStateCertificationCommision;
        private PropertyPath<String> _fioOfCommissionChairman;
        private PropertyPath<String> _standartEduPeriod;
        private PropertyPath<String> _numberOfStudentExcludeOrder;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Date> _entranceDate;
        private PropertyPath<Integer> _entranceYear;
        private PropertyPath<Integer> _endYear;
        private FrdoDevelopForm.Path<FrdoDevelopForm> _developForm;
        private PropertyPath<Date> _birthDate;
        private PropertyPath<String> _specialization;
        private PropertyPath<String> _eduInstitution;
        private PropertyPath<String> _lastEduInstitutionDocument;
        private FrdoEduDocument.Path<FrdoEduDocument> _eduDocument;
        private PropertyPath<Date> _dateOfDuplicateDocumentOrder;
        private PropertyPath<String> _numberOfDuplicateDocumentOrder;
        private PropertyPath<Date> _replaceDate;
        private PropertyPath<Date> _corruptedDate;
        private PropertyPath<String> _specialRank;
        private PropertyPath<String> _fioOfOrgUnitHead;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _lastChangeDate;
        private PropertyPath<String> _creatorName;
        private PropertyPath<String> _serialRegNumberUniqKey;
        private SupportedPropertyPath<String> _documentTitle;
        private SupportedPropertyPath<String> _fioOfOwner;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Бланк документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getBlank()
     */
        public FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank> blank()
        {
            if(_blank == null )
                _blank = new FrdoEduDocumentBlank.Path<FrdoEduDocumentBlank>(L_BLANK, this);
            return _blank;
        }

    /**
     * @return Серия бланка документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSeriaBlank()
     */
        public PropertyPath<String> seriaBlank()
        {
            if(_seriaBlank == null )
                _seriaBlank = new PropertyPath<String>(FrdoEduDocumentGen.P_SERIA_BLANK, this);
            return _seriaBlank;
        }

    /**
     * @return Номер бланка документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberBlank()
     */
        public PropertyPath<String> numberBlank()
        {
            if(_numberBlank == null )
                _numberBlank = new PropertyPath<String>(FrdoEduDocumentGen.P_NUMBER_BLANK, this);
            return _numberBlank;
        }

    /**
     * @return Состояние документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getState()
     */
        public FrdoEduDocumentState.Path<FrdoEduDocumentState> state()
        {
            if(_state == null )
                _state = new FrdoEduDocumentState.Path<FrdoEduDocumentState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Тип данных документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDataType()
     */
        public FrdoEduDocumentDataType.Path<FrdoEduDocumentDataType> dataType()
        {
            if(_dataType == null )
                _dataType = new FrdoEduDocumentDataType.Path<FrdoEduDocumentDataType>(L_DATA_TYPE, this);
            return _dataType;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(FrdoEduDocumentGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Индекс книги регистрации.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getRegIndex()
     */
        public PropertyPath<String> regIndex()
        {
            if(_regIndex == null )
                _regIndex = new PropertyPath<String>(FrdoEduDocumentGen.P_REG_INDEX, this);
            return _regIndex;
        }

    /**
     * @return Порядковый регистрационный номер.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSerialRegNumber()
     */
        public PropertyPath<String> serialRegNumber()
        {
            if(_serialRegNumber == null )
                _serialRegNumber = new PropertyPath<String>(FrdoEduDocumentGen.P_SERIAL_REG_NUMBER, this);
            return _serialRegNumber;
        }

    /**
     * @return Фамилия лица, получившего документ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getLastNameOfOwner()
     */
        public PropertyPath<String> lastNameOfOwner()
        {
            if(_lastNameOfOwner == null )
                _lastNameOfOwner = new PropertyPath<String>(FrdoEduDocumentGen.P_LAST_NAME_OF_OWNER, this);
            return _lastNameOfOwner;
        }

    /**
     * @return Имя лица, получившего документ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFirstNameOfOwner()
     */
        public PropertyPath<String> firstNameOfOwner()
        {
            if(_firstNameOfOwner == null )
                _firstNameOfOwner = new PropertyPath<String>(FrdoEduDocumentGen.P_FIRST_NAME_OF_OWNER, this);
            return _firstNameOfOwner;
        }

    /**
     * @return Отчество лица, получившего документ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getMiddleNameOfOwner()
     */
        public PropertyPath<String> middleNameOfOwner()
        {
            if(_middleNameOfOwner == null )
                _middleNameOfOwner = new PropertyPath<String>(FrdoEduDocumentGen.P_MIDDLE_NAME_OF_OWNER, this);
            return _middleNameOfOwner;
        }

    /**
     * @return Дата выдачи документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getIssueDate()
     */
        public PropertyPath<Date> issueDate()
        {
            if(_issueDate == null )
                _issueDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_ISSUE_DATE, this);
            return _issueDate;
        }

    /**
     * @return Город выдачи.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getIssueAddress()
     */
        public AddressItem.Path<AddressItem> issueAddress()
        {
            if(_issueAddress == null )
                _issueAddress = new AddressItem.Path<AddressItem>(L_ISSUE_ADDRESS, this);
            return _issueAddress;
        }

    /**
     * @return Наименование направления подготовки (специальности).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEducationLevelTitle()
     */
        public PropertyPath<String> educationLevelTitle()
        {
            if(_educationLevelTitle == null )
                _educationLevelTitle = new PropertyPath<String>(FrdoEduDocumentGen.P_EDUCATION_LEVEL_TITLE, this);
            return _educationLevelTitle;
        }

    /**
     * @return Код квалификации по ОКСО.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getOksoQualificationCode()
     */
        public PropertyPath<String> oksoQualificationCode()
        {
            if(_oksoQualificationCode == null )
                _oksoQualificationCode = new PropertyPath<String>(FrdoEduDocumentGen.P_OKSO_QUALIFICATION_CODE, this);
            return _oksoQualificationCode;
        }

    /**
     * @return Код специальности по справочнику.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getCatalogSpecialityCode()
     */
        public PropertyPath<String> catalogSpecialityCode()
        {
            if(_catalogSpecialityCode == null )
                _catalogSpecialityCode = new PropertyPath<String>(FrdoEduDocumentGen.P_CATALOG_SPECIALITY_CODE, this);
            return _catalogSpecialityCode;
        }

    /**
     * @return Наименование присвоенной степени или квалификации.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNameOfdegreeOrQualification()
     */
        public PropertyPath<String> nameOfdegreeOrQualification()
        {
            if(_nameOfdegreeOrQualification == null )
                _nameOfdegreeOrQualification = new PropertyPath<String>(FrdoEduDocumentGen.P_NAME_OFDEGREE_OR_QUALIFICATION, this);
            return _nameOfdegreeOrQualification;
        }

    /**
     * @return Дата протокола государственной аттестационной комиссии.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDateOfStateCertificationCommision()
     */
        public PropertyPath<Date> dateOfStateCertificationCommision()
        {
            if(_dateOfStateCertificationCommision == null )
                _dateOfStateCertificationCommision = new PropertyPath<Date>(FrdoEduDocumentGen.P_DATE_OF_STATE_CERTIFICATION_COMMISION, this);
            return _dateOfStateCertificationCommision;
        }

    /**
     * @return Номер протокола государственной аттестационной комиссии.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberOfStateCertificationCommision()
     */
        public PropertyPath<String> numberOfStateCertificationCommision()
        {
            if(_numberOfStateCertificationCommision == null )
                _numberOfStateCertificationCommision = new PropertyPath<String>(FrdoEduDocumentGen.P_NUMBER_OF_STATE_CERTIFICATION_COMMISION, this);
            return _numberOfStateCertificationCommision;
        }

    /**
     * @return ФИО председателя комиссии.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFioOfCommissionChairman()
     */
        public PropertyPath<String> fioOfCommissionChairman()
        {
            if(_fioOfCommissionChairman == null )
                _fioOfCommissionChairman = new PropertyPath<String>(FrdoEduDocumentGen.P_FIO_OF_COMMISSION_CHAIRMAN, this);
            return _fioOfCommissionChairman;
        }

    /**
     * @return Нормативный период обучения.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getStandartEduPeriod()
     */
        public PropertyPath<String> standartEduPeriod()
        {
            if(_standartEduPeriod == null )
                _standartEduPeriod = new PropertyPath<String>(FrdoEduDocumentGen.P_STANDART_EDU_PERIOD, this);
            return _standartEduPeriod;
        }

    /**
     * @return Номер приказа об отчислении студента (слушателя).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberOfStudentExcludeOrder()
     */
        public PropertyPath<String> numberOfStudentExcludeOrder()
        {
            if(_numberOfStudentExcludeOrder == null )
                _numberOfStudentExcludeOrder = new PropertyPath<String>(FrdoEduDocumentGen.P_NUMBER_OF_STUDENT_EXCLUDE_ORDER, this);
            return _numberOfStudentExcludeOrder;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Дата поступления.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEntranceDate()
     */
        public PropertyPath<Date> entranceDate()
        {
            if(_entranceDate == null )
                _entranceDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_ENTRANCE_DATE, this);
            return _entranceDate;
        }

    /**
     * @return Год поступления.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEntranceYear()
     */
        public PropertyPath<Integer> entranceYear()
        {
            if(_entranceYear == null )
                _entranceYear = new PropertyPath<Integer>(FrdoEduDocumentGen.P_ENTRANCE_YEAR, this);
            return _entranceYear;
        }

    /**
     * @return Год окончания.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEndYear()
     */
        public PropertyPath<Integer> endYear()
        {
            if(_endYear == null )
                _endYear = new PropertyPath<Integer>(FrdoEduDocumentGen.P_END_YEAR, this);
            return _endYear;
        }

    /**
     * @return Форма освоения (ФРДО).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDevelopForm()
     */
        public FrdoDevelopForm.Path<FrdoDevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new FrdoDevelopForm.Path<FrdoDevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Специализация.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSpecialization()
     */
        public PropertyPath<String> specialization()
        {
            if(_specialization == null )
                _specialization = new PropertyPath<String>(FrdoEduDocumentGen.P_SPECIALIZATION, this);
            return _specialization;
        }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEduInstitution()
     */
        public PropertyPath<String> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new PropertyPath<String>(FrdoEduDocumentGen.P_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

    /**
     * @return Предыдущий документ об образовании.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getLastEduInstitutionDocument()
     */
        public PropertyPath<String> lastEduInstitutionDocument()
        {
            if(_lastEduInstitutionDocument == null )
                _lastEduInstitutionDocument = new PropertyPath<String>(FrdoEduDocumentGen.P_LAST_EDU_INSTITUTION_DOCUMENT, this);
            return _lastEduInstitutionDocument;
        }

    /**
     * @return Документ об образовании (ФРДО).
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getEduDocument()
     */
        public FrdoEduDocument.Path<FrdoEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new FrdoEduDocument.Path<FrdoEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Дата приказа о выдаче дубликата документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDateOfDuplicateDocumentOrder()
     */
        public PropertyPath<Date> dateOfDuplicateDocumentOrder()
        {
            if(_dateOfDuplicateDocumentOrder == null )
                _dateOfDuplicateDocumentOrder = new PropertyPath<Date>(FrdoEduDocumentGen.P_DATE_OF_DUPLICATE_DOCUMENT_ORDER, this);
            return _dateOfDuplicateDocumentOrder;
        }

    /**
     * @return Номер приказа о выдаче дубликата документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getNumberOfDuplicateDocumentOrder()
     */
        public PropertyPath<String> numberOfDuplicateDocumentOrder()
        {
            if(_numberOfDuplicateDocumentOrder == null )
                _numberOfDuplicateDocumentOrder = new PropertyPath<String>(FrdoEduDocumentGen.P_NUMBER_OF_DUPLICATE_DOCUMENT_ORDER, this);
            return _numberOfDuplicateDocumentOrder;
        }

    /**
     * @return Дата замены документа.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getReplaceDate()
     */
        public PropertyPath<Date> replaceDate()
        {
            if(_replaceDate == null )
                _replaceDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_REPLACE_DATE, this);
            return _replaceDate;
        }

    /**
     * @return Дата списания испорченного бланка.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getCorruptedDate()
     */
        public PropertyPath<Date> corruptedDate()
        {
            if(_corruptedDate == null )
                _corruptedDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_CORRUPTED_DATE, this);
            return _corruptedDate;
        }

    /**
     * @return Специальное звание.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSpecialRank()
     */
        public PropertyPath<String> specialRank()
        {
            if(_specialRank == null )
                _specialRank = new PropertyPath<String>(FrdoEduDocumentGen.P_SPECIAL_RANK, this);
            return _specialRank;
        }

    /**
     * @return ФИО руководителя ОУ.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFioOfOrgUnitHead()
     */
        public PropertyPath<String> fioOfOrgUnitHead()
        {
            if(_fioOfOrgUnitHead == null )
                _fioOfOrgUnitHead = new PropertyPath<String>(FrdoEduDocumentGen.P_FIO_OF_ORG_UNIT_HEAD, this);
            return _fioOfOrgUnitHead;
        }

    /**
     * @return Дата формирования документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата последнего изменения. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getLastChangeDate()
     */
        public PropertyPath<Date> lastChangeDate()
        {
            if(_lastChangeDate == null )
                _lastChangeDate = new PropertyPath<Date>(FrdoEduDocumentGen.P_LAST_CHANGE_DATE, this);
            return _lastChangeDate;
        }

    /**
     * @return Имя пользователя, создавшего или изменившего запись.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getCreatorName()
     */
        public PropertyPath<String> creatorName()
        {
            if(_creatorName == null )
                _creatorName = new PropertyPath<String>(FrdoEduDocumentGen.P_CREATOR_NAME, this);
            return _creatorName;
        }

    /**
     * Составной ключ уникальности для полей serialRegNumber, regIndex, year, blank.eduDocumentType.educationLevel.code.
     * Учитывается, если (regIndex is not null) and (serialRegNumber is not null).
     * Обновляется при изменении полей serialRegNumber, regIndex, year, .
     *
     * @return Ключ уникальности для serialRegNumber. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getSerialRegNumberUniqKey()
     */
        public PropertyPath<String> serialRegNumberUniqKey()
        {
            if(_serialRegNumberUniqKey == null )
                _serialRegNumberUniqKey = new PropertyPath<String>(FrdoEduDocumentGen.P_SERIAL_REG_NUMBER_UNIQ_KEY, this);
            return _serialRegNumberUniqKey;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getDocumentTitle()
     */
        public SupportedPropertyPath<String> documentTitle()
        {
            if(_documentTitle == null )
                _documentTitle = new SupportedPropertyPath<String>(FrdoEduDocumentGen.P_DOCUMENT_TITLE, this);
            return _documentTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocument#getFioOfOwner()
     */
        public SupportedPropertyPath<String> fioOfOwner()
        {
            if(_fioOfOwner == null )
                _fioOfOwner = new SupportedPropertyPath<String>(FrdoEduDocumentGen.P_FIO_OF_OWNER, this);
            return _fioOfOwner;
        }

        public Class getEntityClass()
        {
            return FrdoEduDocument.class;
        }

        public String getEntityName()
        {
            return "frdoEduDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDocumentTitle();

    public abstract String getFioOfOwner();
}
