package ru.tandemservice.unifrdo.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип документа об образовании (ФРДО)"
 * Имя сущности : frdoEduDocumentType
 * Файл data.xml : unifrdo.catalog.data.xml
 */
public interface FrdoEduDocumentTypeCodes
{
    /** Константа кода (code) элемента : Академическая справка (title) */
    String AKADEMICHESKAYA_SPRAVKA_VPO = "1";
    /** Константа кода (code) элемента : Диплом бакалавра (title) */
    String DIPLOM_BAKALAVRA = "2";
    /** Константа кода (code) элемента : Диплом бакалавра с отличием (title) */
    String DIPLOM_BAKALAVRA_S_OTLICHIEM = "3";
    /** Константа кода (code) элемента : Диплом магистра (title) */
    String DIPLOM_MAGISTRA = "4";
    /** Константа кода (code) элемента : Диплом магистра с отличием (title) */
    String DIPLOM_MAGISTRA_S_OTLICHIEM = "5";
    /** Константа кода (code) элемента : Диплом МГУ (title) */
    String DIPLOM_M_G_U = "6";
    /** Константа кода (code) элемента : Диплом о неполном высшем образовании (title) */
    String DIPLOM_O_NEPOLNOM_VYSSHEM_OBRAZOVANII = "7";
    /** Константа кода (code) элемента : Диплом специалиста (title) */
    String DIPLOM_SPETSIALISTA = "8";
    /** Константа кода (code) элемента : Диплом специалиста с отличием (title) */
    String DIPLOM_SPETSIALISTA_S_OTLICHIEM = "9";
    /** Константа кода (code) элемента : Академическая справка (title) */
    String AKADEMICHESKAYA_SPRAVKA_SPO = "10";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании (или базовый уровень до 2009) (title) */
    String DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_ILI_BAZOVYY_UROVEN_DO_2009_ = "11";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании (повышенный уровень) (title) */
    String DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_POVYSHENNYY_UROVEN_ = "12";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании с отличием (или базовый уровень до 2009) (title) */
    String DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_S_OTLICHIEM_ILI_BAZOVYY_UROVEN_DO_2009_ = "13";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании с отличием (повышенный уровень) (title) */
    String DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_S_OTLICHIEM_POVYSHENNYY_UROVEN_ = "14";

    Set<String> CODES = ImmutableSet.of(AKADEMICHESKAYA_SPRAVKA_VPO, DIPLOM_BAKALAVRA, DIPLOM_BAKALAVRA_S_OTLICHIEM, DIPLOM_MAGISTRA, DIPLOM_MAGISTRA_S_OTLICHIEM, DIPLOM_M_G_U, DIPLOM_O_NEPOLNOM_VYSSHEM_OBRAZOVANII, DIPLOM_SPETSIALISTA, DIPLOM_SPETSIALISTA_S_OTLICHIEM, AKADEMICHESKAYA_SPRAVKA_SPO, DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_ILI_BAZOVYY_UROVEN_DO_2009_, DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_POVYSHENNYY_UROVEN_, DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_S_OTLICHIEM_ILI_BAZOVYY_UROVEN_DO_2009_, DIPLOM_O_SREDNEM_PROFESSIONALNOM_OBRAZOVANII_S_OTLICHIEM_POVYSHENNYY_UROVEN_);
}
