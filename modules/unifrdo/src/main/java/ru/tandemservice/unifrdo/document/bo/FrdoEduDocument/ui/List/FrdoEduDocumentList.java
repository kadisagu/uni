package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unifrdo.catalog.entity.*;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.logic.FrdoEduDocumentSearchDSHandler;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

/**
 * @author Alexander Shaburov
 * @since 01.11.13
 */
@Configuration
public class FrdoEduDocumentList extends BusinessComponentManager
{
    public static final String EDU_DOCUMENT_SEARCH_DS = "eduDocumentSearchDS";
    public static final String EDUCATION_LEVEL_SELECT_DS = "educationLevelSelectDS";
    public static final String EDU_DOCUMENT_TYPE_SELECT_DS = "eduDocumentTypeSelectDS";
    public static final String EDU_DOCUMENT_BLANK_SELECT_DS = "eduDocumentBlankSelectDS";
    public static final String EDU_DOCUMENT_STATE_SELECT_DS = "eduDocumentStateSelectDS";
    public static final String EDU_DOCUMENT_DATA_TYPE_SELECT_DS = "eduDocumentDataTypeSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_DOCUMENT_SEARCH_DS, eduDocumentSearchDSColumns(), eduDocumentSearchDSHandler()))
                .addDataSource(selectDS(EDUCATION_LEVEL_SELECT_DS, educationLevelSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_TYPE_SELECT_DS, eduDocumentTypeSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_BLANK_SELECT_DS, eduDocumentBlankSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_STATE_SELECT_DS, eduDocumentStateSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_DATA_TYPE_SELECT_DS, eduDocumentDataTypeSelectDSHandler()).addColumn(FrdoEduDocumentDataType.shortTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentDataTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentDataType.class)
                .order(FrdoEduDocumentDataType.shortTitle())
                .filter(FrdoEduDocumentDataType.shortTitle())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentStateSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentState.class)
                .order(FrdoEduDocumentState.title())
                .filter(FrdoEduDocumentState.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentBlankSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentBlank.class)
                .order(FrdoEduDocumentBlank.title())
                .filter(FrdoEduDocumentBlank.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentType.class)
                .where(FrdoEduDocumentType.educationLevel(), "educationLevel")
                .order(FrdoEduDocumentType.title())
                .filter(FrdoEduDocumentType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> educationLevelSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEducationLevel.class)
                .order(FrdoEducationLevel.title())
                .filter(FrdoEducationLevel.title())
                .pageable(true);
    }

    @Bean
    public ColumnListExtPoint eduDocumentSearchDSColumns()
    {
        return columnListExtPointBuilder(EDU_DOCUMENT_SEARCH_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("formingDate", FrdoEduDocument.formingDate()).order().formatter(DateFormatter.DATE_FORMATTER_WITH_TIME))
                .addColumn(textColumn("educationLevel", FrdoEduDocument.blank().eduDocumentType().educationLevel().title()).order())
                .addColumn(textColumn("year", FrdoEduDocument.year()).order())
                .addColumn(textColumn("regIndex", FrdoEduDocument.regIndex()).order())
                .addColumn(textColumn("serialRegNumber", FrdoEduDocument.serialRegNumber()).order())
                .addColumn(textColumn("fioOfOwner", FrdoEduDocument.fioOfOwner()).order())
                .addColumn(textColumn("eduDocumentType", FrdoEduDocument.blank().eduDocumentType().title()))
                .addColumn(textColumn("seriaBlank", FrdoEduDocument.seriaBlank()))
                .addColumn(textColumn("numberBlank", FrdoEduDocument.numberBlank()))
                .addColumn(dateColumn("issueDate", FrdoEduDocument.issueDate()).order())
                .addColumn(textColumn("educationLevelTitle", FrdoEduDocument.educationLevelTitle()).order())
                .addColumn(textColumn("nameOfdegreeOrQualification", FrdoEduDocument.nameOfdegreeOrQualification()))
                .addColumn(dateColumn("dateOfStateCertificationCommision", FrdoEduDocument.dateOfStateCertificationCommision()).order())
                .addColumn(textColumn("numberOfStateCertificationCommision", FrdoEduDocument.numberOfStateCertificationCommision()).order())
                .addColumn(textColumn("numberOfStudentExcludeOrder", FrdoEduDocument.numberOfStudentExcludeOrder()).order())
                .addColumn(textColumn("dataType", FrdoEduDocument.dataType().title()))
                .addColumn(textColumn("eduDocument", FrdoEduDocument.eduDocument().documentTitle()))
                .addColumn(dateColumn("dateOfDuplicateDocumentOrder", FrdoEduDocument.dateOfDuplicateDocumentOrder()))
                .addColumn(textColumn("numberOfDuplicateDocumentOrder", FrdoEduDocument.numberOfDuplicateDocumentOrder()))
                .addColumn(dateColumn("replaceDate", FrdoEduDocument.replaceDate()))
                .addColumn(dateColumn("lastChangeDate", FrdoEduDocument.lastChangeDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME))
                .addColumn(textColumn("state", FrdoEduDocument.state().title()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditEduDocument").disabled("mvel:!getPresenter().getConfig().getDataSource('" + EDU_DOCUMENT_SEARCH_DS + "').getCurrent().getState().getCode().equals('" + FrdoEduDocumentStateCodes.FORMIRUETSYA + "')").permissionKey("frdoEduDocumentListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteEduDocument").disabled("mvel:!getPresenter().getConfig().getDataSource('" + EDU_DOCUMENT_SEARCH_DS + "').getCurrent().getState().getCode().equals('" + FrdoEduDocumentStateCodes.FORMIRUETSYA + "')").alert(new FormattedMessage("eduDocumentSearchDS.delete.alert", FrdoEduDocument.documentTitle().s())).permissionKey("frdoEduDocumentListDelete"))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentSearchDSHandler()
    {
        return new FrdoEduDocumentSearchDSHandler(getName());
    }
}
