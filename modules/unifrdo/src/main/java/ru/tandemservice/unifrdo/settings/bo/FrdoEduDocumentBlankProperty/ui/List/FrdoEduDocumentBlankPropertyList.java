package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.logic.FrdoEduDocumentBlankPropertySearchDSHandler;

/**
 * @author Alexander Shaburov
 * @since 28.10.13
 */
@Configuration
public class FrdoEduDocumentBlankPropertyList extends BusinessComponentManager
{
    public static final String EDUCATION_LEVEL_SELECT_DS = "educationLevelSelectDS";
    public static final String EDU_DOCUMENT_TYPE_SELECT_DS = "eduDocumentTypeSelectDS";
    public static final String EDU_DOCUMENT_BLANK_SELECT_DS = "eduDocumentBlankSelectDS";

    public static final String EDU_DOCUMENT_BLANK_PROPERTY_SEARCH_DS = "eduDocumentBlankPropertySearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_LEVEL_SELECT_DS, educationLevelSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_TYPE_SELECT_DS, eduDocumentTypeSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_BLANK_SELECT_DS, eduDocumentBlankSelectDSHandler()))
                .addDataSource(searchListDS(EDU_DOCUMENT_BLANK_PROPERTY_SEARCH_DS, eduDocumentBlankPropertySearchDSColumns(), eduDocumentBlankPropertySearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduDocumentBlankPropertySearchDSColumns()
    {
        return columnListExtPointBuilder(EDU_DOCUMENT_BLANK_PROPERTY_SEARCH_DS)
                .addColumn(textColumn("title", BlankPropertyWrapper.P_TITLE))
                .addColumn(toggleColumn("uses", BlankPropertyWrapper.P_USES)
                        .toggleOnListener("onToggleUses")
                        .toggleOffListener("onToggleUses"))
                .addColumn(toggleColumn("required", BlankPropertyWrapper.P_REQUIRED)
                        .toggleOnListener("onToggleRequired")
                        .toggleOffListener("onToggleRequired")
                        .disabled("ui:requiredDisabled"))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentBlankPropertySearchDSHandler()
    {
        return new FrdoEduDocumentBlankPropertySearchDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> educationLevelSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEducationLevel.class)
                .filter(FrdoEducationLevel.title())
                .order(FrdoEducationLevel.title());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentType.class)
                .where(FrdoEduDocumentType.educationLevel(), "educationLevel")
                .filter(FrdoEduDocumentType.title())
                .order(FrdoEduDocumentType.title());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentBlankSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentBlank.class)
                .where(FrdoEduDocumentBlank.eduDocumentType(), "eduDocumentType")
                .filter(FrdoEduDocumentBlank.title())
                .order(FrdoEduDocumentBlank.title());
    }
}
