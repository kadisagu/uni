package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.Pub;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Alexander Shaburov
 * @since 21.11.13
 */
@Configuration
public class FrdoEduDocumentsPackagePub extends BusinessComponentManager
{
}
