package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ChangeState;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;

/**
 * @author Alexander Shaburov
 * @since 06.11.13
 */
@Configuration
public class FrdoEduDocumentChangeState extends BusinessComponentManager
{
    public static final String STATE_SELECT_DS = "stateSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(STATE_SELECT_DS, stateSelectDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> stateSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentState.class)
                .where(FrdoEduDocumentState.code(), "possibleStateCodes")
                .order(FrdoEduDocumentState.code())
                .filter(FrdoEduDocumentState.title())
                .pageable(true);
    }
}
