package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class Controller extends DefaultCatalogPubController<FrdoEduDocumentBlankProperty, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<FrdoEduDocumentBlankProperty> createListDataSource(IBusinessComponent context)
    {
        final DynamicListDataSource<FrdoEduDocumentBlankProperty> dataSource = super.createListDataSource(context);

        for (AbstractColumn column : dataSource.getColumns())
            column.setOrderable(false);

        return dataSource;
    }
}
