/* $Id:$ */
package ru.tandemservice.unifrdo.base.ext.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unifrdo", () -> {
                final IUniBaseDao dao = IUniBaseDao.instance.get();
                return Arrays.asList(
                        "Число документов: " + dao.getCount(FrdoEduDocument.class),
                        "Число испорченных: " + dao.getCount(new DQLSelectBuilder().fromEntity(FrdoEduDocument.class, "a")
                                .where(eq(property("a", FrdoEduDocument.blank().title()), value("Испорченный бланк")))
                        ),
                        "Число пакетов: " + dao.getCount(FrdoEduDocumentsPackage.class)
                );
            })
            .create();
    }
}
