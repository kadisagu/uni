package ru.tandemservice.unifrdo.document.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет документов об образовании (ФРДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FrdoEduDocumentsPackageGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage";
    public static final String ENTITY_NAME = "frdoEduDocumentsPackage";
    public static final int VERSION_HASH = 1736926701;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_ISSUE_YEAR = "issueYear";
    public static final String L_EDUCATION_LEVEL = "educationLevel";
    public static final String P_FILE_VERSION = "fileVersion";
    public static final String P_FIO_PERSON_CREATED = "fioPersonCreated";
    public static final String P_FIO_PERSON_APPROVED = "fioPersonApproved";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_FROMAT_VERSION = "fromatVersion";
    public static final String P_NAME_ORGANIZATION = "nameOrganization";
    public static final String P_ID_ORGANIZATION = "idOrganization";
    public static final String P_PLACE_ORGANIZATION = "placeOrganization";
    public static final String P_INN_ORGANIZATION = "innOrganization";
    public static final String P_KPP_ORGANIZATION = "kppOrganization";
    public static final String P_OGRN_ORGANIZATION = "ogrnOrganization";
    public static final String L_CONTENT = "content";
    public static final String P_MANUFACTURE_DATE = "manufactureDate";
    public static final String P_EDU_DOCUMENT_FROM = "eduDocumentFrom";
    public static final String P_EDU_DOCUMENT_TO = "eduDocumentTo";

    private String _title;     // Наименование документа
    private int _issueYear;     // Год выдачи
    private FrdoEducationLevel _educationLevel;     // Уровень образования
    private String _fileVersion;     // Версия файла
    private String _fioPersonCreated;     // Ф.И.О. лица, создавшего документ
    private String _fioPersonApproved;     // Ф.И.О. лица, утвердившего документ
    private Date _formingDate;     // Дата формирования документа
    private String _fromatVersion;     // Версия формата пакета
    private String _nameOrganization;     // Наименование организации создателя документа
    private String _idOrganization;     // ИД организации создателя документа
    private String _placeOrganization;     // Местонахождение организации-создателя документа (почтовый адрес)
    private String _innOrganization;     // ИНН организации создателя документа
    private String _kppOrganization;     // КПП организации создателя документа
    private String _ogrnOrganization;     // ОГРН организации создателя документа
    private DatabaseFile _content;     // Пакет
    private Date _manufactureDate;     // Дата изготовления документа
    private Date _eduDocumentFrom;     // Документы об образовании, выданные от
    private Date _eduDocumentTo;     // Документы об образовании, выданные по

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наименование документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование документа. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Год выдачи. Свойство не может быть null.
     */
    @NotNull
    public int getIssueYear()
    {
        return _issueYear;
    }

    /**
     * @param issueYear Год выдачи. Свойство не может быть null.
     */
    public void setIssueYear(int issueYear)
    {
        dirty(_issueYear, issueYear);
        _issueYear = issueYear;
    }

    /**
     * @return Уровень образования. Свойство не может быть null.
     */
    @NotNull
    public FrdoEducationLevel getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel Уровень образования. Свойство не может быть null.
     */
    public void setEducationLevel(FrdoEducationLevel educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    /**
     * @return Версия файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileVersion()
    {
        return _fileVersion;
    }

    /**
     * @param fileVersion Версия файла. Свойство не может быть null.
     */
    public void setFileVersion(String fileVersion)
    {
        dirty(_fileVersion, fileVersion);
        _fileVersion = fileVersion;
    }

    /**
     * @return Ф.И.О. лица, создавшего документ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFioPersonCreated()
    {
        return _fioPersonCreated;
    }

    /**
     * @param fioPersonCreated Ф.И.О. лица, создавшего документ. Свойство не может быть null.
     */
    public void setFioPersonCreated(String fioPersonCreated)
    {
        dirty(_fioPersonCreated, fioPersonCreated);
        _fioPersonCreated = fioPersonCreated;
    }

    /**
     * @return Ф.И.О. лица, утвердившего документ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFioPersonApproved()
    {
        return _fioPersonApproved;
    }

    /**
     * @param fioPersonApproved Ф.И.О. лица, утвердившего документ. Свойство не может быть null.
     */
    public void setFioPersonApproved(String fioPersonApproved)
    {
        dirty(_fioPersonApproved, fioPersonApproved);
        _fioPersonApproved = fioPersonApproved;
    }

    /**
     * @return Дата формирования документа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования документа. Свойство не может быть null и должно быть уникальным.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Версия формата пакета. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFromatVersion()
    {
        return _fromatVersion;
    }

    /**
     * @param fromatVersion Версия формата пакета. Свойство не может быть null.
     */
    public void setFromatVersion(String fromatVersion)
    {
        dirty(_fromatVersion, fromatVersion);
        _fromatVersion = fromatVersion;
    }

    /**
     * @return Наименование организации создателя документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNameOrganization()
    {
        return _nameOrganization;
    }

    /**
     * @param nameOrganization Наименование организации создателя документа. Свойство не может быть null.
     */
    public void setNameOrganization(String nameOrganization)
    {
        dirty(_nameOrganization, nameOrganization);
        _nameOrganization = nameOrganization;
    }

    /**
     * @return ИД организации создателя документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdOrganization()
    {
        return _idOrganization;
    }

    /**
     * @param idOrganization ИД организации создателя документа. Свойство не может быть null.
     */
    public void setIdOrganization(String idOrganization)
    {
        dirty(_idOrganization, idOrganization);
        _idOrganization = idOrganization;
    }

    /**
     * @return Местонахождение организации-создателя документа (почтовый адрес). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlaceOrganization()
    {
        return _placeOrganization;
    }

    /**
     * @param placeOrganization Местонахождение организации-создателя документа (почтовый адрес). Свойство не может быть null.
     */
    public void setPlaceOrganization(String placeOrganization)
    {
        dirty(_placeOrganization, placeOrganization);
        _placeOrganization = placeOrganization;
    }

    /**
     * @return ИНН организации создателя документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getInnOrganization()
    {
        return _innOrganization;
    }

    /**
     * @param innOrganization ИНН организации создателя документа. Свойство не может быть null.
     */
    public void setInnOrganization(String innOrganization)
    {
        dirty(_innOrganization, innOrganization);
        _innOrganization = innOrganization;
    }

    /**
     * @return КПП организации создателя документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getKppOrganization()
    {
        return _kppOrganization;
    }

    /**
     * @param kppOrganization КПП организации создателя документа. Свойство не может быть null.
     */
    public void setKppOrganization(String kppOrganization)
    {
        dirty(_kppOrganization, kppOrganization);
        _kppOrganization = kppOrganization;
    }

    /**
     * @return ОГРН организации создателя документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOgrnOrganization()
    {
        return _ogrnOrganization;
    }

    /**
     * @param ogrnOrganization ОГРН организации создателя документа. Свойство не может быть null.
     */
    public void setOgrnOrganization(String ogrnOrganization)
    {
        dirty(_ogrnOrganization, ogrnOrganization);
        _ogrnOrganization = ogrnOrganization;
    }

    /**
     * @return Пакет. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Пакет. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата изготовления документа. Свойство не может быть null.
     */
    @NotNull
    public Date getManufactureDate()
    {
        return _manufactureDate;
    }

    /**
     * @param manufactureDate Дата изготовления документа. Свойство не может быть null.
     */
    public void setManufactureDate(Date manufactureDate)
    {
        dirty(_manufactureDate, manufactureDate);
        _manufactureDate = manufactureDate;
    }

    /**
     * @return Документы об образовании, выданные от.
     */
    public Date getEduDocumentFrom()
    {
        return _eduDocumentFrom;
    }

    /**
     * @param eduDocumentFrom Документы об образовании, выданные от.
     */
    public void setEduDocumentFrom(Date eduDocumentFrom)
    {
        dirty(_eduDocumentFrom, eduDocumentFrom);
        _eduDocumentFrom = eduDocumentFrom;
    }

    /**
     * @return Документы об образовании, выданные по.
     */
    public Date getEduDocumentTo()
    {
        return _eduDocumentTo;
    }

    /**
     * @param eduDocumentTo Документы об образовании, выданные по.
     */
    public void setEduDocumentTo(Date eduDocumentTo)
    {
        dirty(_eduDocumentTo, eduDocumentTo);
        _eduDocumentTo = eduDocumentTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FrdoEduDocumentsPackageGen)
        {
            setTitle(((FrdoEduDocumentsPackage)another).getTitle());
            setIssueYear(((FrdoEduDocumentsPackage)another).getIssueYear());
            setEducationLevel(((FrdoEduDocumentsPackage)another).getEducationLevel());
            setFileVersion(((FrdoEduDocumentsPackage)another).getFileVersion());
            setFioPersonCreated(((FrdoEduDocumentsPackage)another).getFioPersonCreated());
            setFioPersonApproved(((FrdoEduDocumentsPackage)another).getFioPersonApproved());
            setFormingDate(((FrdoEduDocumentsPackage)another).getFormingDate());
            setFromatVersion(((FrdoEduDocumentsPackage)another).getFromatVersion());
            setNameOrganization(((FrdoEduDocumentsPackage)another).getNameOrganization());
            setIdOrganization(((FrdoEduDocumentsPackage)another).getIdOrganization());
            setPlaceOrganization(((FrdoEduDocumentsPackage)another).getPlaceOrganization());
            setInnOrganization(((FrdoEduDocumentsPackage)another).getInnOrganization());
            setKppOrganization(((FrdoEduDocumentsPackage)another).getKppOrganization());
            setOgrnOrganization(((FrdoEduDocumentsPackage)another).getOgrnOrganization());
            setContent(((FrdoEduDocumentsPackage)another).getContent());
            setManufactureDate(((FrdoEduDocumentsPackage)another).getManufactureDate());
            setEduDocumentFrom(((FrdoEduDocumentsPackage)another).getEduDocumentFrom());
            setEduDocumentTo(((FrdoEduDocumentsPackage)another).getEduDocumentTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FrdoEduDocumentsPackageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FrdoEduDocumentsPackage.class;
        }

        public T newInstance()
        {
            return (T) new FrdoEduDocumentsPackage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "issueYear":
                    return obj.getIssueYear();
                case "educationLevel":
                    return obj.getEducationLevel();
                case "fileVersion":
                    return obj.getFileVersion();
                case "fioPersonCreated":
                    return obj.getFioPersonCreated();
                case "fioPersonApproved":
                    return obj.getFioPersonApproved();
                case "formingDate":
                    return obj.getFormingDate();
                case "fromatVersion":
                    return obj.getFromatVersion();
                case "nameOrganization":
                    return obj.getNameOrganization();
                case "idOrganization":
                    return obj.getIdOrganization();
                case "placeOrganization":
                    return obj.getPlaceOrganization();
                case "innOrganization":
                    return obj.getInnOrganization();
                case "kppOrganization":
                    return obj.getKppOrganization();
                case "ogrnOrganization":
                    return obj.getOgrnOrganization();
                case "content":
                    return obj.getContent();
                case "manufactureDate":
                    return obj.getManufactureDate();
                case "eduDocumentFrom":
                    return obj.getEduDocumentFrom();
                case "eduDocumentTo":
                    return obj.getEduDocumentTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "issueYear":
                    obj.setIssueYear((Integer) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((FrdoEducationLevel) value);
                    return;
                case "fileVersion":
                    obj.setFileVersion((String) value);
                    return;
                case "fioPersonCreated":
                    obj.setFioPersonCreated((String) value);
                    return;
                case "fioPersonApproved":
                    obj.setFioPersonApproved((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "fromatVersion":
                    obj.setFromatVersion((String) value);
                    return;
                case "nameOrganization":
                    obj.setNameOrganization((String) value);
                    return;
                case "idOrganization":
                    obj.setIdOrganization((String) value);
                    return;
                case "placeOrganization":
                    obj.setPlaceOrganization((String) value);
                    return;
                case "innOrganization":
                    obj.setInnOrganization((String) value);
                    return;
                case "kppOrganization":
                    obj.setKppOrganization((String) value);
                    return;
                case "ogrnOrganization":
                    obj.setOgrnOrganization((String) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "manufactureDate":
                    obj.setManufactureDate((Date) value);
                    return;
                case "eduDocumentFrom":
                    obj.setEduDocumentFrom((Date) value);
                    return;
                case "eduDocumentTo":
                    obj.setEduDocumentTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "issueYear":
                        return true;
                case "educationLevel":
                        return true;
                case "fileVersion":
                        return true;
                case "fioPersonCreated":
                        return true;
                case "fioPersonApproved":
                        return true;
                case "formingDate":
                        return true;
                case "fromatVersion":
                        return true;
                case "nameOrganization":
                        return true;
                case "idOrganization":
                        return true;
                case "placeOrganization":
                        return true;
                case "innOrganization":
                        return true;
                case "kppOrganization":
                        return true;
                case "ogrnOrganization":
                        return true;
                case "content":
                        return true;
                case "manufactureDate":
                        return true;
                case "eduDocumentFrom":
                        return true;
                case "eduDocumentTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "issueYear":
                    return true;
                case "educationLevel":
                    return true;
                case "fileVersion":
                    return true;
                case "fioPersonCreated":
                    return true;
                case "fioPersonApproved":
                    return true;
                case "formingDate":
                    return true;
                case "fromatVersion":
                    return true;
                case "nameOrganization":
                    return true;
                case "idOrganization":
                    return true;
                case "placeOrganization":
                    return true;
                case "innOrganization":
                    return true;
                case "kppOrganization":
                    return true;
                case "ogrnOrganization":
                    return true;
                case "content":
                    return true;
                case "manufactureDate":
                    return true;
                case "eduDocumentFrom":
                    return true;
                case "eduDocumentTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "issueYear":
                    return Integer.class;
                case "educationLevel":
                    return FrdoEducationLevel.class;
                case "fileVersion":
                    return String.class;
                case "fioPersonCreated":
                    return String.class;
                case "fioPersonApproved":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "fromatVersion":
                    return String.class;
                case "nameOrganization":
                    return String.class;
                case "idOrganization":
                    return String.class;
                case "placeOrganization":
                    return String.class;
                case "innOrganization":
                    return String.class;
                case "kppOrganization":
                    return String.class;
                case "ogrnOrganization":
                    return String.class;
                case "content":
                    return DatabaseFile.class;
                case "manufactureDate":
                    return Date.class;
                case "eduDocumentFrom":
                    return Date.class;
                case "eduDocumentTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FrdoEduDocumentsPackage> _dslPath = new Path<FrdoEduDocumentsPackage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FrdoEduDocumentsPackage");
    }
            

    /**
     * @return Наименование документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Год выдачи. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getIssueYear()
     */
    public static PropertyPath<Integer> issueYear()
    {
        return _dslPath.issueYear();
    }

    /**
     * @return Уровень образования. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getEducationLevel()
     */
    public static FrdoEducationLevel.Path<FrdoEducationLevel> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    /**
     * @return Версия файла. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFileVersion()
     */
    public static PropertyPath<String> fileVersion()
    {
        return _dslPath.fileVersion();
    }

    /**
     * @return Ф.И.О. лица, создавшего документ. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFioPersonCreated()
     */
    public static PropertyPath<String> fioPersonCreated()
    {
        return _dslPath.fioPersonCreated();
    }

    /**
     * @return Ф.И.О. лица, утвердившего документ. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFioPersonApproved()
     */
    public static PropertyPath<String> fioPersonApproved()
    {
        return _dslPath.fioPersonApproved();
    }

    /**
     * @return Дата формирования документа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Версия формата пакета. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFromatVersion()
     */
    public static PropertyPath<String> fromatVersion()
    {
        return _dslPath.fromatVersion();
    }

    /**
     * @return Наименование организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getNameOrganization()
     */
    public static PropertyPath<String> nameOrganization()
    {
        return _dslPath.nameOrganization();
    }

    /**
     * @return ИД организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getIdOrganization()
     */
    public static PropertyPath<String> idOrganization()
    {
        return _dslPath.idOrganization();
    }

    /**
     * @return Местонахождение организации-создателя документа (почтовый адрес). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getPlaceOrganization()
     */
    public static PropertyPath<String> placeOrganization()
    {
        return _dslPath.placeOrganization();
    }

    /**
     * @return ИНН организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getInnOrganization()
     */
    public static PropertyPath<String> innOrganization()
    {
        return _dslPath.innOrganization();
    }

    /**
     * @return КПП организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getKppOrganization()
     */
    public static PropertyPath<String> kppOrganization()
    {
        return _dslPath.kppOrganization();
    }

    /**
     * @return ОГРН организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getOgrnOrganization()
     */
    public static PropertyPath<String> ogrnOrganization()
    {
        return _dslPath.ogrnOrganization();
    }

    /**
     * @return Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата изготовления документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getManufactureDate()
     */
    public static PropertyPath<Date> manufactureDate()
    {
        return _dslPath.manufactureDate();
    }

    /**
     * @return Документы об образовании, выданные от.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getEduDocumentFrom()
     */
    public static PropertyPath<Date> eduDocumentFrom()
    {
        return _dslPath.eduDocumentFrom();
    }

    /**
     * @return Документы об образовании, выданные по.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getEduDocumentTo()
     */
    public static PropertyPath<Date> eduDocumentTo()
    {
        return _dslPath.eduDocumentTo();
    }

    public static class Path<E extends FrdoEduDocumentsPackage> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _issueYear;
        private FrdoEducationLevel.Path<FrdoEducationLevel> _educationLevel;
        private PropertyPath<String> _fileVersion;
        private PropertyPath<String> _fioPersonCreated;
        private PropertyPath<String> _fioPersonApproved;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _fromatVersion;
        private PropertyPath<String> _nameOrganization;
        private PropertyPath<String> _idOrganization;
        private PropertyPath<String> _placeOrganization;
        private PropertyPath<String> _innOrganization;
        private PropertyPath<String> _kppOrganization;
        private PropertyPath<String> _ogrnOrganization;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _manufactureDate;
        private PropertyPath<Date> _eduDocumentFrom;
        private PropertyPath<Date> _eduDocumentTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наименование документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Год выдачи. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getIssueYear()
     */
        public PropertyPath<Integer> issueYear()
        {
            if(_issueYear == null )
                _issueYear = new PropertyPath<Integer>(FrdoEduDocumentsPackageGen.P_ISSUE_YEAR, this);
            return _issueYear;
        }

    /**
     * @return Уровень образования. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getEducationLevel()
     */
        public FrdoEducationLevel.Path<FrdoEducationLevel> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new FrdoEducationLevel.Path<FrdoEducationLevel>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

    /**
     * @return Версия файла. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFileVersion()
     */
        public PropertyPath<String> fileVersion()
        {
            if(_fileVersion == null )
                _fileVersion = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_FILE_VERSION, this);
            return _fileVersion;
        }

    /**
     * @return Ф.И.О. лица, создавшего документ. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFioPersonCreated()
     */
        public PropertyPath<String> fioPersonCreated()
        {
            if(_fioPersonCreated == null )
                _fioPersonCreated = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_FIO_PERSON_CREATED, this);
            return _fioPersonCreated;
        }

    /**
     * @return Ф.И.О. лица, утвердившего документ. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFioPersonApproved()
     */
        public PropertyPath<String> fioPersonApproved()
        {
            if(_fioPersonApproved == null )
                _fioPersonApproved = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_FIO_PERSON_APPROVED, this);
            return _fioPersonApproved;
        }

    /**
     * @return Дата формирования документа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FrdoEduDocumentsPackageGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Версия формата пакета. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getFromatVersion()
     */
        public PropertyPath<String> fromatVersion()
        {
            if(_fromatVersion == null )
                _fromatVersion = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_FROMAT_VERSION, this);
            return _fromatVersion;
        }

    /**
     * @return Наименование организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getNameOrganization()
     */
        public PropertyPath<String> nameOrganization()
        {
            if(_nameOrganization == null )
                _nameOrganization = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_NAME_ORGANIZATION, this);
            return _nameOrganization;
        }

    /**
     * @return ИД организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getIdOrganization()
     */
        public PropertyPath<String> idOrganization()
        {
            if(_idOrganization == null )
                _idOrganization = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_ID_ORGANIZATION, this);
            return _idOrganization;
        }

    /**
     * @return Местонахождение организации-создателя документа (почтовый адрес). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getPlaceOrganization()
     */
        public PropertyPath<String> placeOrganization()
        {
            if(_placeOrganization == null )
                _placeOrganization = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_PLACE_ORGANIZATION, this);
            return _placeOrganization;
        }

    /**
     * @return ИНН организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getInnOrganization()
     */
        public PropertyPath<String> innOrganization()
        {
            if(_innOrganization == null )
                _innOrganization = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_INN_ORGANIZATION, this);
            return _innOrganization;
        }

    /**
     * @return КПП организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getKppOrganization()
     */
        public PropertyPath<String> kppOrganization()
        {
            if(_kppOrganization == null )
                _kppOrganization = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_KPP_ORGANIZATION, this);
            return _kppOrganization;
        }

    /**
     * @return ОГРН организации создателя документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getOgrnOrganization()
     */
        public PropertyPath<String> ogrnOrganization()
        {
            if(_ogrnOrganization == null )
                _ogrnOrganization = new PropertyPath<String>(FrdoEduDocumentsPackageGen.P_OGRN_ORGANIZATION, this);
            return _ogrnOrganization;
        }

    /**
     * @return Пакет. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата изготовления документа. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getManufactureDate()
     */
        public PropertyPath<Date> manufactureDate()
        {
            if(_manufactureDate == null )
                _manufactureDate = new PropertyPath<Date>(FrdoEduDocumentsPackageGen.P_MANUFACTURE_DATE, this);
            return _manufactureDate;
        }

    /**
     * @return Документы об образовании, выданные от.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getEduDocumentFrom()
     */
        public PropertyPath<Date> eduDocumentFrom()
        {
            if(_eduDocumentFrom == null )
                _eduDocumentFrom = new PropertyPath<Date>(FrdoEduDocumentsPackageGen.P_EDU_DOCUMENT_FROM, this);
            return _eduDocumentFrom;
        }

    /**
     * @return Документы об образовании, выданные по.
     * @see ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage#getEduDocumentTo()
     */
        public PropertyPath<Date> eduDocumentTo()
        {
            if(_eduDocumentTo == null )
                _eduDocumentTo = new PropertyPath<Date>(FrdoEduDocumentsPackageGen.P_EDU_DOCUMENT_TO, this);
            return _eduDocumentTo;
        }

        public Class getEntityClass()
        {
            return FrdoEduDocumentsPackage.class;
        }

        public String getEntityName()
        {
            return "frdoEduDocumentsPackage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
