package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.logic;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentDataTypeCodes;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.FrdoEduDocumentManager;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 06.11.13
 */
public class FrdoEduDocumentDao extends UniBaseDao implements IFrdoEduDocumentDao
{
    @Override
    public void doBatchChangeStatus(Collection<Long> ids, FrdoEduDocumentState state)
    {
        final List<FrdoEduDocument> list = new DQLSelectBuilder().fromEntity(FrdoEduDocument.class, "d").column(property("d"))
                .where(in(property(FrdoEduDocument.id().fromAlias("d")), ids))
                .createStatement(getSession()).list();

        // тип данных = "дубликат", "замена", "корректировка",
        final Collection<String> specilalCodes = Arrays.asList(FrdoEduDocumentDataTypeCodes.DUPLICATE, FrdoEduDocumentDataTypeCodes.INSTEAD, FrdoEduDocumentDataTypeCodes.CORRECTION);
        for (FrdoEduDocument eduDocument : list)
        {
            if (specilalCodes.contains(eduDocument.getDataType().getCode()) && state.getCode().equals(FrdoEduDocumentStateCodes.VYDAN))
                if (eduDocument.getEduDocument() != null)
                    eduDocument.getEduDocument().setState(getByCode(FrdoEduDocumentState.class, FrdoEduDocumentStateCodes.ANNULIROVAN));

            eduDocument.setState(state);
            update(eduDocument);
        }
    }

    @Override
    public Long saveOrUpdateEduDocument(FrdoEduDocument eduDocument)
    {
        if (validate(eduDocument).hasErrors())
            return null;

        if (!eduDocument.getDataType().isShowInsteadDocumentField())
            eduDocument.setEduDocument(null);

        if (!eduDocument.getDataType().isShowDuplicateField())
        {
            eduDocument.setDateOfDuplicateDocumentOrder(null);
            eduDocument.setNumberOfDuplicateDocumentOrder(null);
        }

        if (!eduDocument.getDataType().isShowReplaceDateField())
            eduDocument.setReplaceDate(null);

        eduDocument.setState(getCatalogItem(FrdoEduDocumentState.class, FrdoEduDocumentStateCodes.FORMIRUETSYA));
        saveOrUpdate(eduDocument);
        return eduDocument.getId();
    }

    protected ErrorCollector validate(FrdoEduDocument eduDocument)
    {
        final ErrorCollector errorCollector = ContextLocal.getErrorCollector();

        if (eduDocument.getIssueDate() != null)
        {
            final Calendar issueDate = GregorianCalendar.getInstance();
            issueDate.setTime(eduDocument.getIssueDate());
            if (issueDate.get(Calendar.YEAR) != eduDocument.getYear())
                errorCollector.add(FrdoEduDocumentManager.instance().getProperty("frdoEduDocument.validate"), "issueDate", "year");
        }
        return errorCollector;
    }
}
