package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.logic.FrdoEduDocumentDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.logic.IFrdoEduDocumentDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic.FrdoReportParseDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic.IFrdoReportParseDao;

/**
 * @author Alexander Shaburov
 * @since 01.11.13
 */
@Configuration
public class FrdoEduDocumentManager extends BusinessObjectManager
{
    public static FrdoEduDocumentManager instance()
    {
        return instance(FrdoEduDocumentManager.class);
    }

    @Bean
    public IFrdoEduDocumentDao dao()
    {
        return new FrdoEduDocumentDao();
    }

	@Bean
	public IFrdoReportParseDao reportImportDao()
	{
		return new FrdoReportParseDao();
	}
}
