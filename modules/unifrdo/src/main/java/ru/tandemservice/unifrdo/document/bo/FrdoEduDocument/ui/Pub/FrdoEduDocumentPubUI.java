package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.AddEdit.FrdoEduDocumentAddEdit;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ChangeState.FrdoEduDocumentChangeState;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ChangeState.FrdoEduDocumentChangeStateUI;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;
import ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 12.11.13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id", required = true)
})
public class FrdoEduDocumentPubUI extends UIPresenter
{
    private EntityHolder<FrdoEduDocument> _entityHolder = new EntityHolder<>(new FrdoEduDocument());

    private Map<String, FrdoEduDocumentBlankPropertySettings> _fieldSettingsMap;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();

        preparePropertySettings();
    }

    public void preparePropertySettings()
    {
        final List<FrdoEduDocumentBlankPropertySettings> settingsList = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlankPropertySettings.class, "s").column(property("s"))
            .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlank().fromAlias("s")), value(_entityHolder.getValue().getBlank()))));

        _fieldSettingsMap = new HashMap<>();
        for (FrdoEduDocumentBlankPropertySettings settings : settingsList)
            _fieldSettingsMap.put(settings.getEduDocumentBlankProperty().getName(), settings);
    }

    public void onClickEdit()
    {
        getActivationBuilder().asRegion(FrdoEduDocumentAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, _entityHolder.getId())
                .activate();
    }

    public void onClickChangeState()
    {
        getActivationBuilder()
                .asRegionDialog(FrdoEduDocumentChangeState.class)
                .parameter(FrdoEduDocumentChangeStateUI.INPUT_BIND_ID_LIST, Collections.singletonList(_entityHolder.getId()))
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_entityHolder.getValue());
        deactivate();
    }

    // Getters & Setters

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker", _entityHolder.getValue().getDocumentTitle());
    }

    public boolean isFieldUses(String field)
    {
        return _fieldSettingsMap.containsKey(field);
    }

    public boolean isFieldRequired(String field)
    {
        return _fieldSettingsMap.containsKey(field) && _fieldSettingsMap.get(field).isRequired();
    }


    // Accessors

    public EntityHolder<FrdoEduDocument> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<FrdoEduDocument> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public Map<String, FrdoEduDocumentBlankPropertySettings> getFieldSettingsMap()
    {
        return _fieldSettingsMap;
    }

    public void setFieldSettingsMap(Map<String, FrdoEduDocumentBlankPropertySettings> fieldSettingsMap)
    {
        _fieldSettingsMap = fieldSettingsMap;
    }
}
