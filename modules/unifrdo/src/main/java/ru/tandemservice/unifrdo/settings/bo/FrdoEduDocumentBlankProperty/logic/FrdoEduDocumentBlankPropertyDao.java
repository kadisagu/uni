package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;
import ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class FrdoEduDocumentBlankPropertyDao extends UniBaseDao implements IFrdoEduDocumentBlankPropertyDao
{
    @Override
    public void doToggleUses(FrdoEduDocumentBlank blank, FrdoEduDocumentBlankProperty property, boolean uses)
    {
        final FrdoEduDocumentBlankPropertySettings settings = new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlankPropertySettings.class, "s").column(property("s"))
                .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlank().fromAlias("s")), value(blank)))
                .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlankProperty().fromAlias("s")), value(property)))
                .createStatement(getSession()).uniqueResult();

        if (settings == null && uses)
        {
            final FrdoEduDocumentBlankPropertySettings newSettings = new FrdoEduDocumentBlankPropertySettings(blank, property);
            save(newSettings);
        }
        else if (settings != null && !uses)
        {
            delete(settings);
        }
    }

    @Override
    public void doToggleRequired(FrdoEduDocumentBlank blank, FrdoEduDocumentBlankProperty property, boolean required)
    {
        final FrdoEduDocumentBlankPropertySettings settings = new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlankPropertySettings.class, "s").column(property("s"))
                .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlank().fromAlias("s")), value(blank)))
                .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlankProperty().fromAlias("s")), value(property)))
                .createStatement(getSession()).uniqueResult();

        if (settings == null)
            return;

        settings.setRequired(required);
        save(settings);
    }
}
