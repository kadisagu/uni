package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentDataType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

/**
 * @author Alexander Shaburov
 * @since 07.11.13
 */
@Configuration
public class FrdoEduDocumentAddEdit extends BusinessComponentManager
{
    public static final String EDUCATION_LEVEL_SELECT_DS = "educationLevelSelectDS";
    public static final String EDU_DOCUMENT_TYPE_SELECT_DS = "eduDocumentTypeSelectDS";
    public static final String BLANK_SELECT_DS = "blankSelectDS";
    public static final String DATA_TYPE_SELECT_DS = "dataTypeSelectDS";
    public static final String INSTEAD_EDU_DOCUMENT_SELECT_DS = "insteadEduDocumentSelectDS";
    public static final String COUNTRY_SELECT_DS = "countrySelectDS";
    public static final String ISSUE_ADDRESS_SELECT_DS = "issueAddressSelectDS";

	public static final String PARAM_EDU_DOCUMENT_TYPE = "eduDocumentType";
	public static final String PARAM_EDU_LEVEL = "educationLevel";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_LEVEL_SELECT_DS, educationLevelSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_TYPE_SELECT_DS, eduDocumentTypeSelectDSHandler()))
                .addDataSource(selectDS(BLANK_SELECT_DS, blankSelectDSHandler()))
                .addDataSource(selectDS(DATA_TYPE_SELECT_DS, dataTypeSelectDSHandler()))
                .addDataSource(selectDS(INSTEAD_EDU_DOCUMENT_SELECT_DS, insteadEduDocumentSelectDSHandler()).addColumn(FrdoEduDocument.documentTitle().s()))
                .addDataSource(selectDS(COUNTRY_SELECT_DS, countrySelectDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ISSUE_ADDRESS_SELECT_DS, getName(), AddressItem.settlementComboDSHandler(getName())))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> countrySelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), AddressCountry.class)
                .order(AddressCountry.title())
                .filter(AddressCountry.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> insteadEduDocumentSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocument.class)
                .where(FrdoEduDocument.state().code(), (Object) FrdoEduDocumentStateCodes.VYDAN)
                .order(FrdoEduDocument.blank().eduDocumentType().title())
                .order(FrdoEduDocument.serialRegNumber())
                .order(FrdoEduDocument.issueDate())
                .filter(FrdoEduDocument.serialRegNumber())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> dataTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentDataType.class)
                .order(FrdoEduDocumentDataType.title())
                .filter(FrdoEduDocumentDataType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> blankSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentBlank.class)
                .where(FrdoEduDocumentBlank.eduDocumentType().id(), PARAM_EDU_DOCUMENT_TYPE)
                .order(FrdoEduDocumentBlank.title())
                .filter(FrdoEduDocumentBlank.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentType.class)
                .where(FrdoEduDocumentType.educationLevel().id(), PARAM_EDU_LEVEL)
                .order(FrdoEduDocumentType.title())
                .filter(FrdoEduDocumentType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> educationLevelSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEducationLevel.class)
                .order(FrdoEducationLevel.title())
                .filter(FrdoEducationLevel.title())
                .pageable(true);
    }
}
