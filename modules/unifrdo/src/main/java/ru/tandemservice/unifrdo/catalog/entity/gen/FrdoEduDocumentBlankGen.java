package ru.tandemservice.unifrdo.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Бланк документа об образовании (ФРДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FrdoEduDocumentBlankGen extends EntityBase
 implements INaturalIdentifiable<FrdoEduDocumentBlankGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank";
    public static final String ENTITY_NAME = "frdoEduDocumentBlank";
    public static final int VERSION_HASH = -1984404813;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_EDU_DOCUMENT_TYPE = "eduDocumentType";

    private String _code;     // Системный код
    private String _title;     // Название
    private FrdoEduDocumentType _eduDocumentType;     // Тип документа об образовании (ФРДО)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Тип документа об образовании (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEduDocumentType getEduDocumentType()
    {
        return _eduDocumentType;
    }

    /**
     * @param eduDocumentType Тип документа об образовании (ФРДО). Свойство не может быть null.
     */
    public void setEduDocumentType(FrdoEduDocumentType eduDocumentType)
    {
        dirty(_eduDocumentType, eduDocumentType);
        _eduDocumentType = eduDocumentType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FrdoEduDocumentBlankGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FrdoEduDocumentBlank)another).getCode());
            }
            setTitle(((FrdoEduDocumentBlank)another).getTitle());
            setEduDocumentType(((FrdoEduDocumentBlank)another).getEduDocumentType());
        }
    }

    public INaturalId<FrdoEduDocumentBlankGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FrdoEduDocumentBlankGen>
    {
        private static final String PROXY_NAME = "FrdoEduDocumentBlankNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FrdoEduDocumentBlankGen.NaturalId) ) return false;

            FrdoEduDocumentBlankGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FrdoEduDocumentBlankGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FrdoEduDocumentBlank.class;
        }

        public T newInstance()
        {
            return (T) new FrdoEduDocumentBlank();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "eduDocumentType":
                    return obj.getEduDocumentType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "eduDocumentType":
                    obj.setEduDocumentType((FrdoEduDocumentType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "eduDocumentType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "eduDocumentType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "eduDocumentType":
                    return FrdoEduDocumentType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FrdoEduDocumentBlank> _dslPath = new Path<FrdoEduDocumentBlank>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FrdoEduDocumentBlank");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Тип документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank#getEduDocumentType()
     */
    public static FrdoEduDocumentType.Path<FrdoEduDocumentType> eduDocumentType()
    {
        return _dslPath.eduDocumentType();
    }

    public static class Path<E extends FrdoEduDocumentBlank> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private FrdoEduDocumentType.Path<FrdoEduDocumentType> _eduDocumentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FrdoEduDocumentBlankGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FrdoEduDocumentBlankGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Тип документа об образовании (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank#getEduDocumentType()
     */
        public FrdoEduDocumentType.Path<FrdoEduDocumentType> eduDocumentType()
        {
            if(_eduDocumentType == null )
                _eduDocumentType = new FrdoEduDocumentType.Path<FrdoEduDocumentType>(L_EDU_DOCUMENT_TYPE, this);
            return _eduDocumentType;
        }

        public Class getEntityClass()
        {
            return FrdoEduDocumentBlank.class;
        }

        public String getEntityName()
        {
            return "frdoEduDocumentBlank";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
