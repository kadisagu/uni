package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic.FrdoEduDocumentsPackageDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic.IFrdoEduDocumentsPackageDao;

/**
 * @author Alexander Shaburov
 * @since 18.11.13
 */
@Configuration
public class FrdoEduDocumentsPackageManager extends BusinessObjectManager
{
    public static FrdoEduDocumentsPackageManager instance()
    {
        return instance(FrdoEduDocumentsPackageManager.class);
    }

    @Bean
    public IFrdoEduDocumentsPackageDao dao()
    {
        return new FrdoEduDocumentsPackageDao();
    }
}
