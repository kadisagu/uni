package ru.tandemservice.unifrdo.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Поле бланка документа об образовании (ФРДО)"
 * Имя сущности : frdoEduDocumentBlankProperty
 * Файл data.xml : unifrdo.catalog.data.xml
 */
public interface FrdoEduDocumentBlankPropertyCodes
{
    /** Константа кода (code) элемента : Фамилия (title) */
    String FAMILIYA = "1";
    /** Константа кода (code) элемента : Имя (title) */
    String IMYA = "2";
    /** Константа кода (code) элемента : Отчество (title) */
    String OTCHESTVO = "3";
    /** Константа кода (code) элемента : Регистрационный номер (title) */
    String REGISTRATSIONNYY_NOMER = "4";
    /** Константа кода (code) элемента : Образовательное учреждение (title) */
    String OBRAZOVATELNOE_UCHREJDENIE = "5";
    /** Константа кода (code) элемента : Наименование направления подготовки (специальности) (title) */
    String NAIMENOVANIE_NAPRAVLENIYA_PODGOTOVKI_SPETSIALNOSTI_ = "6";
    /** Константа кода (code) элемента : Дата протокола ГАК/ГЭК (title) */
    String DATA_PROTOKOLA_G_A_K_G_E_K = "7";
    /** Константа кода (code) элемента : Дата выдачи (title) */
    String DATA_VYDACHI = "8";
    /** Константа кода (code) элемента : Квалификация/степень (title) */
    String KVALIFIKATSIYA_STEPEN = "9";
    /** Константа кода (code) элемента : Дата поступления (title) */
    String DATA_POSTUPLENIYA = "10";
    /** Константа кода (code) элемента : Дата окончания (title) */
    String DATA_OKONCHANIYA = "11";
    /** Константа кода (code) элемента : Год поступления (title) */
    String GOD_POSTUPLENIYA = "12";
    /** Константа кода (code) элемента : Год окончания (title) */
    String GOD_OKONCHANIYA = "13";
    /** Константа кода (code) элемента : Дата рождения (title) */
    String DATA_ROJDENIYA = "14";
    /** Константа кода (code) элемента : Специализация (title) */
    String SPETSIALIZATSIYA = "15";
    /** Константа кода (code) элемента : Предыдущий документ об образовании (title) */
    String PREDYDUTSHIY_DOKUMENT_OB_OBRAZOVANII = "16";
    /** Константа кода (code) элемента : Нормативный период обучения (title) */
    String NORMATIVNYY_PERIOD_OBUCHENIYA = "17";
    /** Константа кода (code) элемента : Город выдачи (title) */
    String GOROD_VYDACHI = "18";
    /** Константа кода (code) элемента : Индекс книги регистрации (title) */
    String INDEKS_KNIGI_REGISTRATSII = "19";
    /** Константа кода (code) элемента : Код специальности по справочнику (title) */
    String KOD_SPETSIALNOSTI_PO_SPRAVOCHNIKU = "20";
    /** Константа кода (code) элемента : Код квалификации по ОКСО (title) */
    String KOD_KVALIFIKATSII_PO_O_K_S_O = "21";
    /** Константа кода (code) элемента : Номер протокола ГАК/ГЭК (title) */
    String NOMER_PROTOKOLA_G_A_K_G_E_K = "22";
    /** Константа кода (code) элемента : Номер приказа об отчислении (title) */
    String NOMER_PRIKAZA_OB_OTCHISLENII = "23";
    /** Константа кода (code) элемента : Дата списания испорченного бланка (title) */
    String DATA_SPISANIYA_ISPORCHENNOGO_BLANKA = "24";
    /** Константа кода (code) элемента : Специальное звание (title) */
    String SPETSIALNOE_ZVANIE = "25";
    /** Константа кода (code) элемента : Форма освоения (title) */
    String FORMA_OSVOENIYA = "26";
    /** Константа кода (code) элемента : ФИО руководителя ОУ (title) */
    String F_I_O_RUKOVODITELYA_O_U = "27";
    /** Константа кода (code) элемента : ФИО председателя комиссии (title) */
    String F_I_O_PREDSEDATELYA_KOMISSII = "28";

    Set<String> CODES = ImmutableSet.of(FAMILIYA, IMYA, OTCHESTVO, REGISTRATSIONNYY_NOMER, OBRAZOVATELNOE_UCHREJDENIE, NAIMENOVANIE_NAPRAVLENIYA_PODGOTOVKI_SPETSIALNOSTI_, DATA_PROTOKOLA_G_A_K_G_E_K, DATA_VYDACHI, KVALIFIKATSIYA_STEPEN, DATA_POSTUPLENIYA, DATA_OKONCHANIYA, GOD_POSTUPLENIYA, GOD_OKONCHANIYA, DATA_ROJDENIYA, SPETSIALIZATSIYA, PREDYDUTSHIY_DOKUMENT_OB_OBRAZOVANII, NORMATIVNYY_PERIOD_OBUCHENIYA, GOROD_VYDACHI, INDEKS_KNIGI_REGISTRATSII, KOD_SPETSIALNOSTI_PO_SPRAVOCHNIKU, KOD_KVALIFIKATSII_PO_O_K_S_O, NOMER_PROTOKOLA_G_A_K_G_E_K, NOMER_PRIKAZA_OB_OTCHISLENII, DATA_SPISANIYA_ISPORCHENNOGO_BLANKA, SPETSIALNOE_ZVANIE, FORMA_OSVOENIYA, F_I_O_RUKOVODITELYA_O_U, F_I_O_PREDSEDATELYA_KOMISSII);
}
