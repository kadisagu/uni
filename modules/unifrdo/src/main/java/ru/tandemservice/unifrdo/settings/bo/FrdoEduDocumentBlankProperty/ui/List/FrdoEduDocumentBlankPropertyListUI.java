package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyManager;

/**
 * @author Alexander Shaburov
 * @since 28.10.13
 */
public class FrdoEduDocumentBlankPropertyListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {

    }

    public void onChangeSelectors()
    {
        getSettings().save();
    }

    public boolean isRequiredDisabled()
    {
        final BlankPropertyWrapper current = getConfig().getDataSource(FrdoEduDocumentBlankPropertyList.EDU_DOCUMENT_BLANK_PROPERTY_SEARCH_DS).getCurrent();
        return !current.isUses();
    }

    public void onToggleUses()
    {
        final BlankPropertyWrapper current = getConfig().getDataSource(FrdoEduDocumentBlankPropertyList.EDU_DOCUMENT_BLANK_PROPERTY_SEARCH_DS).getRecordById(getListenerParameterAsLong());
        final FrdoEduDocumentBlank blank = getSettings().get("eduDocumentBlank");

        FrdoEduDocumentBlankPropertyManager.instance().dao().doToggleUses(blank, current.getProperty(), !current.isUses());
    }

    public void onToggleRequired()
    {
        final BlankPropertyWrapper current = getConfig().getDataSource(FrdoEduDocumentBlankPropertyList.EDU_DOCUMENT_BLANK_PROPERTY_SEARCH_DS).getRecordById(getListenerParameterAsLong());
        final FrdoEduDocumentBlank blank = getSettings().get("eduDocumentBlank");

        FrdoEduDocumentBlankPropertyManager.instance().dao().doToggleRequired(blank, current.getProperty(), !current.isRequired());
    }

    public boolean isNothingSelected()
    {
        return getSettings().get("educationLevel") == null || getSettings().get("eduDocumentType") == null || getSettings().get("eduDocumentBlank") == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("educationLevel", getSettings().get("educationLevel"));
        dataSource.put("eduDocumentType", getSettings().get("eduDocumentType"));
        dataSource.put("eduDocumentBlank", getSettings().get("eduDocumentBlank"));
    }
}
