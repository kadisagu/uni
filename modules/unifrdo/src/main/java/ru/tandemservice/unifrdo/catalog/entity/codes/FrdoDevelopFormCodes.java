package ru.tandemservice.unifrdo.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма освоения (ФРДО)"
 * Имя сущности : frdoDevelopForm
 * Файл data.xml : unifrdo.catalog.data.xml
 */
public interface FrdoDevelopFormCodes
{
    /** Константа кода (code) элемента : Очная (title) */
    String OCHNAYA = "1";
    /** Константа кода (code) элемента : Заочная (title) */
    String ZAOCHNAYA = "2";
    /** Константа кода (code) элемента : Очно-заочная (title) */
    String OCHNO_ZAOCHNAYA = "3";
    /** Константа кода (code) элемента : Экстернат (title) */
    String EKSTERNAT = "4";

    Set<String> CODES = ImmutableSet.of(OCHNAYA, ZAOCHNAYA, OCHNO_ZAOCHNAYA, EKSTERNAT);
}
