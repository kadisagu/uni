package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class Model extends DefaultCatalogItemPubModel<FrdoEduDocumentBlankProperty>
{
}
