package ru.tandemservice.unifrdo.catalog.entity;

import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;
import ru.tandemservice.unifrdo.catalog.entity.gen.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * Состояния документов об образовании
 */
public class FrdoEduDocumentState extends FrdoEduDocumentStateGen
{
    /**
     * Состояния, в которые можно перейти из текущего.
     * @return коды состояний
     */
    public Collection<String> getPossibleStateCodes()
    {
        if (getCode().equals(FrdoEduDocumentStateCodes.FORMIRUETSYA))
            return Arrays.asList(FrdoEduDocumentStateCodes.VYDAN);
        if (getCode().equals(FrdoEduDocumentStateCodes.VYDAN))
            return Arrays.asList(FrdoEduDocumentStateCodes.FORMIRUETSYA, FrdoEduDocumentStateCodes.ANNULIROVAN);
        if (getCode().equals(FrdoEduDocumentStateCodes.ANNULIROVAN))
            return Arrays.asList(FrdoEduDocumentStateCodes.FORMIRUETSYA);

        throw new IllegalStateException("Invalid state");
    }
}