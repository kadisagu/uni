package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEducationLevelCodes;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 19.11.13
 */
public class FrdoEduDocumentsPackageDao extends UniBaseDao implements IFrdoEduDocumentsPackageDao
{
    protected static final DatatypeFactory dataTypeFactory;
    static
    {
        try
        {
            dataTypeFactory = DatatypeFactory.newInstance();
        }
        catch (DatatypeConfigurationException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    @Override
    public ByteArrayOutputStream generatePackage(IPackageSettings settings) throws IOException
    {
        final Context context = prepareVelocityContext(settings);

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");

        final Template template = Velocity.getTemplate(getVelocityTemplatePath(settings.getEduDocumentsPackage().getEducationLevel().getCode()), "UTF-8");
        template.merge(context, writer);

        writer.flush();
        writer.close();

        return out;
    }

    protected String getVelocityTemplatePath(String frdoEducationLevelCode)
    {
        if (FrdoEducationLevelCodes.VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE.equals(frdoEducationLevelCode))
            return "unifrdo/template/packageVPO.vm";
        else if (FrdoEducationLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE.equals(frdoEducationLevelCode))
            return "unifrdo/template/packageSPO.vm";

        throw new IllegalArgumentException("Illegal code of FrdoEducationLevel");
    }

    protected Context prepareVelocityContext(IPackageSettings settings)
    {
        final List<FrdoEduDocument> eduDocumentList = prepareEduDocuments(settings);
        VelocityContext context = new VelocityContext();

        // данные о пакете
        final String[] pkg = new String[14];
        pkg[0] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getFromatVersion());
        pkg[1] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getNameOrganization());
        pkg[2] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getIdOrganization());
        pkg[3] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getPlaceOrganization());
        pkg[4] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getInnOrganization());
        pkg[5] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getKppOrganization());
        pkg[6] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getOgrnOrganization());
        pkg[7] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getTitle());
        pkg[8] = String.valueOf(settings.getEduDocumentsPackage().getIssueYear());
        pkg[9] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getEducationLevel().getTitle());
        pkg[10] = StringUtils.trimToEmpty(xmlDateTime(settings.getEduDocumentsPackage().getManufactureDate()));
        pkg[11] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getFileVersion());
        pkg[12] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getFioPersonCreated());
        pkg[13] = StringUtils.trimToEmpty(settings.getEduDocumentsPackage().getFioPersonApproved());
        context.put("package", pkg);


        // строки по документам пакета
        final List<String[]> documentRows = new ArrayList<>();
        for (FrdoEduDocument eduDocument : eduDocumentList)
        {
            if (FrdoEducationLevelCodes.VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE.equals(settings.getEduDocumentsPackage().getEducationLevel().getCode()))
                documentRows.add(prepareRowVPO(eduDocument));
            else if (FrdoEducationLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE.equals(settings.getEduDocumentsPackage().getEducationLevel().getCode()))
                documentRows.add(prepareRowSPO(eduDocument));
            else
                throw new IllegalStateException();
        }
        context.put("documentRows", documentRows);

        return context;
    }

    protected String[] prepareRowVPO(FrdoEduDocument eduDocument)
    {
        final String[] row = new String[33];

        row[0] = StringUtils.trimToEmpty(eduDocument.getRegIndex());
        row[1] = StringUtils.trimToEmpty(eduDocument.getSerialRegNumber());
        row[2] = StringUtils.trimToEmpty(eduDocument.getLastNameOfOwner());
        row[3] = StringUtils.trimToEmpty(eduDocument.getFirstNameOfOwner());
        row[4] = StringUtils.trimToEmpty(eduDocument.getMiddleNameOfOwner());
        row[5] = StringUtils.trimToEmpty(eduDocument.getSeriaBlank());
        row[6] = StringUtils.trimToEmpty(eduDocument.getNumberBlank());
        row[7] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getIssueDate()));
        row[8] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getCorruptedDate()));
        row[9] = StringUtils.trimToEmpty(eduDocument.getEducationLevelTitle());
        row[10] = StringUtils.trimToEmpty(eduDocument.getSpecialRank());
        row[11] = StringUtils.trimToEmpty(eduDocument.getCatalogSpecialityCode());
        row[12] = StringUtils.trimToEmpty(eduDocument.getNameOfdegreeOrQualification());
        row[13] = StringUtils.trimToEmpty(eduDocument.getOksoQualificationCode());
        row[14] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getDateOfStateCertificationCommision()));
        row[15] = StringUtils.trimToEmpty(eduDocument.getNumberOfStateCertificationCommision());
        row[16] = String.valueOf(eduDocument.getEntranceYear() == null ? "" : eduDocument.getEntranceYear());
        row[17] = String.valueOf(eduDocument.getEndYear() == null ? "" : eduDocument.getEndYear());
        row[18] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getEndDate()));
        row[19] = StringUtils.trimToEmpty(eduDocument.getNumberOfStudentExcludeOrder());
        row[20] = StringUtils.trimToEmpty(eduDocument.getSpecialization());
        row[21] = StringUtils.trimToEmpty(eduDocument.getLastEduInstitutionDocument());
        row[22] = StringUtils.trimToEmpty(eduDocument.getStandartEduPeriod());
        row[23] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getBirthDate()));
        row[24] = StringUtils.trimToEmpty(eduDocument.getBlank().getEduDocumentType().getEducationLevel().getTitle());
        row[25] = StringUtils.trimToEmpty(eduDocument.getBlank().getEduDocumentType().getTitle());
        row[26] = StringUtils.trimToEmpty(eduDocument.getBlank().getTitle());
        row[27] = StringUtils.trimToEmpty(eduDocument.getEduInstitution());
        row[28] = StringUtils.trimToEmpty(eduDocument.getIssueAddress() == null ? "" : eduDocument.getIssueAddress().getTitle());
        row[29] = StringUtils.trimToEmpty(eduDocument.getDataType().getShortTitle());
        row[30] = StringUtils.trimToEmpty(eduDocument.getEduDocument() == null ? "" : eduDocument.getEduDocument().getSerialRegNumber());
        row[31] = StringUtils.trimToEmpty(eduDocument.getEduDocument() == null ? "" : eduDocument.getEduDocument().getSeriaBlank());
        row[32] = StringUtils.trimToEmpty(eduDocument.getEduDocument() == null ? "" : eduDocument.getEduDocument().getNumberBlank());

        return row;
    }

    protected String[] prepareRowSPO(FrdoEduDocument eduDocument)
    {
        final String[] row = new String[30];

        row[0] = StringUtils.trimToEmpty(eduDocument.getSerialRegNumber());
        row[1] = StringUtils.trimToEmpty(eduDocument.getLastNameOfOwner());
        row[2] = StringUtils.trimToEmpty(eduDocument.getFirstNameOfOwner());
        row[3] = StringUtils.trimToEmpty(eduDocument.getMiddleNameOfOwner());
        row[4] = StringUtils.trimToEmpty(eduDocument.getSeriaBlank());
        row[5] = StringUtils.trimToEmpty(eduDocument.getNumberBlank());
        row[6] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getIssueDate()));
        row[7] = StringUtils.trimToEmpty(eduDocument.getEducationLevelTitle());
        row[8] = StringUtils.trimToEmpty(eduDocument.getNameOfdegreeOrQualification());
        row[9] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getDateOfStateCertificationCommision()));
        row[10] = String.valueOf(eduDocument.getEntranceYear() == null ? "" : eduDocument.getEntranceYear());
        row[11] = String.valueOf(eduDocument.getEndYear() == null ? "" : eduDocument.getEndYear());
        row[12] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getEndDate()));
        row[13] = StringUtils.trimToEmpty(eduDocument.getNumberOfStudentExcludeOrder());
        row[14] = StringUtils.trimToEmpty(eduDocument.getSpecialization());
        row[15] = StringUtils.trimToEmpty(eduDocument.getLastEduInstitutionDocument());
        row[16] = StringUtils.trimToEmpty(eduDocument.getDevelopForm() == null ? "" : eduDocument.getDevelopForm().getTitle());
        row[17] = StringUtils.trimToEmpty(xmlDateTime(eduDocument.getBirthDate()));
        row[18] = StringUtils.trimToEmpty(eduDocument.getFioOfOrgUnitHead());
        row[19] = StringUtils.trimToEmpty(eduDocument.getFioOfCommissionChairman());
        row[20] = StringUtils.trimToEmpty(eduDocument.getBlank().getEduDocumentType().getEducationLevel().getTitle());
        row[21] = StringUtils.trimToEmpty(eduDocument.getBlank().getEduDocumentType().getTitle());
        row[22] = StringUtils.trimToEmpty(eduDocument.getBlank().getTitle());
        row[23] = StringUtils.trimToEmpty(eduDocument.getEduInstitution());
        row[24] = StringUtils.trimToEmpty(eduDocument.getIssueAddress() == null ? "" : eduDocument.getIssueAddress().getTitle());
        row[25] = StringUtils.trimToEmpty(eduDocument.getDataType().getShortTitle());
        row[26] = StringUtils.trimToEmpty(eduDocument.getEduDocument() == null ? "" : eduDocument.getEduDocument().getSerialRegNumber());
        row[27] = StringUtils.trimToEmpty(eduDocument.getEduDocument() == null ? "" : eduDocument.getEduDocument().getSeriaBlank());
        row[28] = StringUtils.trimToEmpty(eduDocument.getEduDocument() == null ? "" : eduDocument.getEduDocument().getNumberBlank());

        return row;
    }

    protected List<FrdoEduDocument> prepareEduDocuments(IPackageSettings settings)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FrdoEduDocument.class, "d").column(property("d"));
        if (settings.getEduDocumentsPackage().getEducationLevel() != null)
            dql.where(eq(property(FrdoEduDocument.blank().eduDocumentType().educationLevel().fromAlias("d")), value(settings.getEduDocumentsPackage().getEducationLevel())));
        if (settings.getEduDocumentState() != null)
            dql.where(eq(property(FrdoEduDocument.state().fromAlias("d")), value(settings.getEduDocumentState())));
        if (CollectionUtils.isNotEmpty(settings.getEduDocumentDataTypeList()))
            dql.where(in(property(FrdoEduDocument.dataType().fromAlias("d")), settings.getEduDocumentDataTypeList()));
        if (CollectionUtils.isNotEmpty(settings.getEduDocumentTypeList()))
            dql.where(in(property(FrdoEduDocument.blank().eduDocumentType().fromAlias("d")), settings.getEduDocumentTypeList()));
        dql.where(ge(property(FrdoEduDocument.issueDate().fromAlias("d")), value(settings.getEduDocumentsPackage().getEduDocumentFrom(), PropertyType.TIMESTAMP)));
        dql.where(lt(property(FrdoEduDocument.issueDate().fromAlias("d")), value(CoreDateUtils.add(settings.getEduDocumentsPackage().getEduDocumentTo(), Calendar.DATE, 1), PropertyType.TIMESTAMP)));

        return dql.createStatement(getSession()).list();
    }

    @Override
    public Long savePackage(FrdoEduDocumentsPackage eduDocumentsPackage, ByteArrayOutputStream content)
    {
        if (content == null || content.toByteArray().length == 0)
            throw new NullPointerException("Content must not be empty.");

        final DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(content.toByteArray());
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_XML);
        databaseFile.setFilename("frdoEduDocumentsPackage.xml");
        save(databaseFile);

        eduDocumentsPackage.setContent(databaseFile);
        save(eduDocumentsPackage);

        return eduDocumentsPackage.getId();
    }

    public static String xmlDateTime(final Date date)
    {
        if (date == null)
        {
            return null;
        }
        else
        {
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            return dataTypeFactory.newXMLGregorianCalendar(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DATE), gc.get(Calendar.HOUR_OF_DAY), gc.get(Calendar.MINUTE), gc.get(Calendar.SECOND), gc.get(Calendar.MILLISECOND), DatatypeConstants.FIELD_UNDEFINED).toXMLFormat();
        }
    }
}
