package ru.tandemservice.unifrdo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifrdo_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность frdoEduProgramSubjectComparison

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("frdo_edu_pr_subject_compare_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("oldprogramsubject_id", DBType.LONG).setNullable(false), 
				new DBColumn("newprogramsubject_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("frdoEduProgramSubjectComparison");

		}


    }
}