package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlank.FrdoEduDocumentBlankAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<FrdoEduDocumentBlank, Model>
{
}
