package ru.tandemservice.unifrdo.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Уровень образования (ФРДО)"
 * Имя сущности : frdoEducationLevel
 * Файл data.xml : unifrdo.catalog.data.xml
 */
public interface FrdoEducationLevelCodes
{
    /** Константа кода (code) элемента : Высшее профессиональное образование (title) */
    String VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE = "1";
    /** Константа кода (code) элемента : Среднее профессиональное образование (title) */
    String SREDNEE_PROFESSIONALNOE_OBRAZOVANIE = "2";

    Set<String> CODES = ImmutableSet.of(VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE, SREDNEE_PROFESSIONALNOE_OBRAZOVANIE);
}
