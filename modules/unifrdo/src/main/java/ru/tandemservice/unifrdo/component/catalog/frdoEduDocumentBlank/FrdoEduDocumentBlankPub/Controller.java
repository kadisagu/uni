package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlank.FrdoEduDocumentBlankPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class Controller extends DefaultCatalogPubController<FrdoEduDocumentBlank, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<FrdoEduDocumentBlank> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Тип документа об образовании (ФРДО)", FrdoEduDocumentBlank.eduDocumentType().title().s()));
    }
}
