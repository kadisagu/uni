package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlankProperty.FrdoEduDocumentBlankPropertyPub;

import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class DAO extends DefaultCatalogPubDAO<FrdoEduDocumentBlankProperty, Model> implements IDAO
{
    @Override
    protected OrderDescriptionRegistry getOrderDescriptionRegistry()
    {
        return new OrderDescriptionRegistry()
        {
            @Override
            public void applyOrder(MQBuilder builder, EntityOrder entityOrder)
            {
                builder.addOrder("ci", "priority", OrderDirection.asc);
            }
        };
    }
}
