package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentDetails;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.json.JSONObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.settings.DataSettingsManager;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 14.11.13
 */
@Configuration
public class FrdoEduDocumentDetailsManager extends BusinessObjectManager
{
    public static final String EDU_DOCUMENT_DETAILS_SETTINGS_OWNER = "frdoEduDocumentDetails";

    public static FrdoEduDocumentDetailsManager instance()
    {
        return instance(FrdoEduDocumentDetailsManager.class);
    }

    @Bean
    public String getEduDocumentDetailsSettingsKeyPrefix()
    {
        return "unifrdo";
    }

    /**
     * Подготавливает мапу с именами и названиями Реквизитов документов для передачи в ФРДО.
     * @return [name -> title]
     */
    public Map<String, String> getName2TitleDetailsMap()
    {
        final String detailsJSONString = FrdoEduDocumentDetailsManager.instance().getProperty("eduDocumentDetailsJSON");
        JSONObject json;
        try
        {
            json = new JSONObject(detailsJSONString);
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException("Property 'eduDocumentDetailsJSON' in BO FrdoEduDocumentDetails is invalid JSON object");
        }

        final Iterator namesIterator = json.keys();
        final Map<String, String> resultMap = new HashMap<>();
        for (; namesIterator.hasNext();)
        {
            final String name = (String) namesIterator.next();
            resultMap.put(name, json.getString(name));
        }

        return resultMap;
    }

    /**
     * Поднимает данные сохраненных значений реквизитов относительно имен.
     * @return [name -> value]
     */
    public Map<String, String> getDetailsValueMap()
    {
        final IDataSettings settings = DataSettingsFacade.getSettings(EDU_DOCUMENT_DETAILS_SETTINGS_OWNER, getEduDocumentDetailsSettingsKeyPrefix());

        final Map<String, String> resultMap = new HashMap<>();
        for (Map.Entry<String, String> entry : getName2TitleDetailsMap().entrySet())
            resultMap.put(entry.getKey(), settings.<String>get(entry.getKey()));

        return resultMap;
    }

    /**
     * Сохраняет значения реквизитов по имени.
     * @param valueMap [name -> value]
     */
    public void saveDetailsValueMap(@NotNull Map<String, String> valueMap)
    {
        final IDataSettings settings = DataSettingsFacade.getSettings(EDU_DOCUMENT_DETAILS_SETTINGS_OWNER, getEduDocumentDetailsSettingsKeyPrefix());

        for (Map.Entry<String, String> entry : valueMap.entrySet())
        {
            final String value = entry.getValue();
            if (StringUtils.isBlank(value))
                settings.remove(value);
            else
                settings.set(entry.getKey(), value);
        }

        DataSettingsFacade.saveSettings(settings);
    }

    /**
     * Сохраняет значения реквизита.
     * @param name имя реквизита
     * @param value значение
     */
    public void saveDetailsValue(@NotNull String name, String value)
    {
        final IDataSettings settings = DataSettingsFacade.getSettings(EDU_DOCUMENT_DETAILS_SETTINGS_OWNER, getEduDocumentDetailsSettingsKeyPrefix());

        if (StringUtils.isBlank(value))
            settings.remove(name);
        else
            settings.set(name, value);

        DataSettingsFacade.saveSettings(settings);
    }

}
