package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentDataType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;

/**
 * @author Alexander Shaburov
 * @since 18.11.13
 */
@Configuration
public class FrdoEduDocumentsPackageAdd extends BusinessComponentManager
{
    public static final String EDUCATION_LEVEL_SELECT_DS = "educationLevelSelectDS";
    public static final String EDU_DOC_STATE_SELECT_DS = "eduDocStateSelectDS";
    public static final String EDU_DOC_DATA_TYPE_SELECT_DS = "eduDocDataTypeSelectDS";
    public static final String EDU_DOC_TYPE_SELECT_DS = "eduDocTypeSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_LEVEL_SELECT_DS, educationLevelSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOC_STATE_SELECT_DS, eduDocStateSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOC_DATA_TYPE_SELECT_DS, eduDocDataTypeSelectDSHandler()))
                .addDataSource(selectDS(EDU_DOC_TYPE_SELECT_DS, eduDocTypeSelectDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentType.class)
                .where(FrdoEduDocumentType.educationLevel(), "educationLevel")
                .order(FrdoEduDocumentType.title())
                .filter(FrdoEduDocumentType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocDataTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentDataType.class)
                .order(FrdoEduDocumentDataType.title())
                .filter(FrdoEduDocumentDataType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocStateSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEduDocumentState.class)
                .order(FrdoEduDocumentState.title())
                .filter(FrdoEduDocumentState.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> educationLevelSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FrdoEducationLevel.class)
                .order(FrdoEducationLevel.title())
                .filter(FrdoEducationLevel.title())
                .pageable(true);
    }
}
