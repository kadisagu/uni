package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.InfoCollector;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;

/**
 * @author avedernikov
 * @since 06.04.2017
 */
public interface IFrdoReportParseDao extends INeedPersistenceSupport
{
	/**
	 * Загрузить данные из файла, составленного по форме {@link ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.logic.DiplomaSendFisFrdoReportDao}
	 * и сохранить в объекты {@link FrdoEduDocument}.
	 * @param fileContent Содержимое файла.
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void importReports(byte[] fileContent, InfoCollector infoCollector);
}
