package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.ui.List.BlankPropertyWrapper;
import ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 28.10.13
 */
public class FrdoEduDocumentBlankPropertySearchDSHandler extends DefaultSearchDataSourceHandler
{
    public FrdoEduDocumentBlankPropertySearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlankProperty.class, "bp").column(property("bp"))
                .order(property(FrdoEduDocumentBlankProperty.priority().fromAlias("bp")));
        filter(dql, input, context, "bp");
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build();
        return wrap(output, input, context);
    }

    /**
     * @param dql builder from FrdoEduDocumentBlankProperty
     */
    protected void filter(DQLSelectBuilder dql, DSInput input, ExecutionContext context, String alias)
    {
        final FrdoEduDocumentBlank blank = context.get("eduDocumentBlank");
        if (blank == null)
        {
            dql.where(eq(property(FrdoEduDocumentBlankProperty.id().fromAlias(alias)), value(-1)));
            return;
        }
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final FrdoEduDocumentBlank blank = context.get("eduDocumentBlank");
        if (blank == null)
            return ListOutputBuilder.get(input, new ArrayList()).pageable(false).build();

        final List<FrdoEduDocumentBlankPropertySettings> settingsList = new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlankPropertySettings.class, "bps").column(property("bps"))
                .where(eq(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlank().fromAlias("bps")), value(blank)))
                .where(in(property(FrdoEduDocumentBlankPropertySettings.eduDocumentBlankProperty().id().fromAlias("bps")), output.getRecordIds()))
                .createStatement(context.getSession()).list();

        final Map<FrdoEduDocumentBlankProperty, FrdoEduDocumentBlankPropertySettings> settingsMap = new HashMap<>();
        for (FrdoEduDocumentBlankPropertySettings settings : settingsList)
            settingsMap.put(settings.getEduDocumentBlankProperty(), settings);

        final List<BlankPropertyWrapper> resultWrapperList = new ArrayList<>();
        for (FrdoEduDocumentBlankProperty property : output.<FrdoEduDocumentBlankProperty>getRecordList())
        {
            final FrdoEduDocumentBlankPropertySettings settings = settingsMap.get(property);
            resultWrapperList.add(new BlankPropertyWrapper(property, settings != null, settings != null && settings.isRequired()));
        }

        return ListOutputBuilder.get(input, resultWrapperList).pageable(false).build();
    }
}
