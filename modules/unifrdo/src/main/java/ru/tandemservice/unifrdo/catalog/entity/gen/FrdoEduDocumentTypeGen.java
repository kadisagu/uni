package ru.tandemservice.unifrdo.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип документа об образовании (ФРДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FrdoEduDocumentTypeGen extends EntityBase
 implements INaturalIdentifiable<FrdoEduDocumentTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType";
    public static final String ENTITY_NAME = "frdoEduDocumentType";
    public static final int VERSION_HASH = 1786859348;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_EDUCATION_LEVEL = "educationLevel";

    private String _code;     // Системный код
    private String _title;     // Название
    private FrdoEducationLevel _educationLevel;     // Уровень образования (ФРДО)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Уровень образования (ФРДО). Свойство не может быть null.
     */
    @NotNull
    public FrdoEducationLevel getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel Уровень образования (ФРДО). Свойство не может быть null.
     */
    public void setEducationLevel(FrdoEducationLevel educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FrdoEduDocumentTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FrdoEduDocumentType)another).getCode());
            }
            setTitle(((FrdoEduDocumentType)another).getTitle());
            setEducationLevel(((FrdoEduDocumentType)another).getEducationLevel());
        }
    }

    public INaturalId<FrdoEduDocumentTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FrdoEduDocumentTypeGen>
    {
        private static final String PROXY_NAME = "FrdoEduDocumentTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FrdoEduDocumentTypeGen.NaturalId) ) return false;

            FrdoEduDocumentTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FrdoEduDocumentTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FrdoEduDocumentType.class;
        }

        public T newInstance()
        {
            return (T) new FrdoEduDocumentType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "educationLevel":
                    return obj.getEducationLevel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((FrdoEducationLevel) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "educationLevel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "educationLevel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "educationLevel":
                    return FrdoEducationLevel.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FrdoEduDocumentType> _dslPath = new Path<FrdoEduDocumentType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FrdoEduDocumentType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Уровень образования (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType#getEducationLevel()
     */
    public static FrdoEducationLevel.Path<FrdoEducationLevel> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    public static class Path<E extends FrdoEduDocumentType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private FrdoEducationLevel.Path<FrdoEducationLevel> _educationLevel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FrdoEduDocumentTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FrdoEduDocumentTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Уровень образования (ФРДО). Свойство не может быть null.
     * @see ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType#getEducationLevel()
     */
        public FrdoEducationLevel.Path<FrdoEducationLevel> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new FrdoEducationLevel.Path<FrdoEducationLevel>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

        public Class getEntityClass()
        {
            return FrdoEduDocumentType.class;
        }

        public String getEntityName()
        {
            return "frdoEduDocumentType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
