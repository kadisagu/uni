package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentDataType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 19.11.13
 */
public interface IFrdoEduDocumentsPackageDao extends INeedPersistenceSupport
{
    /**
     * Интерфейс обертки для передачи данных необходимых для генерации контента Пакета документов об образовании.
     */
    public static interface IPackageSettings
    {
        FrdoEduDocumentsPackage getEduDocumentsPackage();
        FrdoEduDocumentState getEduDocumentState();
        List<FrdoEduDocumentDataType> getEduDocumentDataTypeList();
        List<FrdoEduDocumentType> getEduDocumentTypeList();
    }

    /**
     * Генерирует контент Пакета документов об образовании по заданым параметрам.
     * @param settings параметры для генерации пакета
     * @return xml
     */
    ByteArrayOutputStream generatePackage(IPackageSettings settings) throws IOException;

    /**
     * Сохраняет Пакет документов об образовании с указаным контетном.
     * @param eduDocumentsPackage пакет на сохранение
     * @param content контент
     * @return id пакета
     */
    Long savePackage(FrdoEduDocumentsPackage eduDocumentsPackage, ByteArrayOutputStream content);
}
