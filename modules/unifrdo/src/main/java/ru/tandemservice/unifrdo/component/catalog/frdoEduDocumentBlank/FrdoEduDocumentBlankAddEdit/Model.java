package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentBlank.FrdoEduDocumentBlankAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public class Model extends DefaultCatalogAddEditModel<FrdoEduDocumentBlank>
{
}
