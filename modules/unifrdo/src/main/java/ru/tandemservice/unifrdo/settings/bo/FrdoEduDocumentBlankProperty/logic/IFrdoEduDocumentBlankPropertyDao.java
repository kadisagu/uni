package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentBlankProperty.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlank;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentBlankProperty;

/**
 * @author Alexander Shaburov
 * @since 29.10.13
 */
public interface IFrdoEduDocumentBlankPropertyDao extends INeedPersistenceSupport
{
    void doToggleUses(FrdoEduDocumentBlank blank, FrdoEduDocumentBlankProperty property, boolean uses);

    void doToggleRequired(FrdoEduDocumentBlank blank, FrdoEduDocumentBlankProperty property, boolean required);
}
