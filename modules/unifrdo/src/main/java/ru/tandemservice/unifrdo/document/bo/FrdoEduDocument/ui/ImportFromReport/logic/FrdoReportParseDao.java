package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic;

import com.google.common.collect.*;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.unifrdo.catalog.entity.*;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentDataTypeCodes;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEducationLevelCodes;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.FrdoEduDocumentManager;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocument;
import ru.tandemservice.unifrdo.settings.entity.FrdoEduDocumentBlankPropertySettings;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentBlankPropertyCodes.*;

/**
 * @author avedernikov
 * @since 06.04.2017
 */
public class FrdoReportParseDao extends SharedBaseDao implements IFrdoReportParseDao
{
	enum ColumnType {
		TITLE, TYPE, STATUS, IS_LOST, IS_DUPLICATE, EDU_LEVEL, DOC_SERIA, DOC_NUMBER, ISSUANCE_DATE, REG_NUMBER,
		PROGRAM_SUBJECT_CODE, PROGRAM_SUBJECT_TITLE, PROGRAM_QUALIFICATION, EDU_ELEMENT_TITLE, ENTRANCE_YEAR, FINISH_YEAR, DEVELOP_PERIOD,
		LAST_NAME, FIRST_NAME, MIDDLE_NAME, BIRTH_DATE, SEX, CITIZENSHIP, PROGRAM_FORM, IS_FIRST_HIGH_EDU, COMPENSATION_TYPE,
		ORIGINAL_TITLE, ORIGINAL_DOC_SERIA, ORIGINAL_DOC_NUMBER, ORIGINAL_REG_NUMBER, ORIGINAL_ISSUANCE_DATE, ORIGINAL_LAST_NAME, ORIGINAL_FIRST_NAME, ORIGINAL_MIDDLE_NAME
	}

	private static final ImmutableMap<String, ColumnType> columnTitle2Type = initColumnTitle2Type();

	enum PropertyType { STRING, DATE, INT, PROGRAM_FORM_FAKE_TYPE }

	private static final ImmutableMap<String, PairKey<ColumnType, PropertyType>> blankPropCode2Column = initBlankPropCode2Column();

	private static final ImmutableSet<String> unfilledBlankPropCodes = ImmutableSet.of(DATA_PROTOKOLA_G_A_K_G_E_K, OBRAZOVATELNOE_UCHREJDENIE, DATA_OKONCHANIYA, DATA_POSTUPLENIYA,
			F_I_O_PREDSEDATELYA_KOMISSII, F_I_O_RUKOVODITELYA_O_U, NOMER_PROTOKOLA_G_A_K_G_E_K, NOMER_PRIKAZA_OB_OTCHISLENII, KOD_KVALIFIKATSII_PO_O_K_S_O,
			INDEKS_KNIGI_REGISTRATSII, SPETSIALNOE_ZVANIE, SPETSIALIZATSIYA, PREDYDUTSHIY_DOKUMENT_OB_OBRAZOVANII, GOROD_VYDACHI, DATA_SPISANIYA_ISPORCHENNOGO_BLANKA);

	private static ImmutableMap<String, ColumnType> initColumnTitle2Type()
	{
		return ImmutableMap.<String, ColumnType>builder()
				.put("Название документа", ColumnType.TITLE)
				.put("Вид документа", ColumnType.TYPE)
				.put("Статус документа", ColumnType.STATUS)
				.put("Подтверждение утраты", ColumnType.IS_LOST)
				.put("Подтверждение обмена", ColumnType.IS_DUPLICATE)
				.put("Уровень образования", ColumnType.EDU_LEVEL)
				.put("Серия документа", ColumnType.DOC_SERIA)
				.put("Номер документа", ColumnType.DOC_NUMBER)
				.put("Дата выдачи", ColumnType.ISSUANCE_DATE)
				.put("Регистрационный номер", ColumnType.REG_NUMBER)
				.put("Код специальности, направления подготовки", ColumnType.PROGRAM_SUBJECT_CODE)
				.put("Наименование специальности", ColumnType.PROGRAM_SUBJECT_TITLE)
				.put("Наименование квалификации", ColumnType.PROGRAM_QUALIFICATION)
				.put("Образовательная программа", ColumnType.EDU_ELEMENT_TITLE)
				.put("Год поступления", ColumnType.ENTRANCE_YEAR)
				.put("Год окончания", ColumnType.FINISH_YEAR)
				.put("Срок обучения", ColumnType.DEVELOP_PERIOD)
				.put("Фамилия получателя", ColumnType.LAST_NAME)
				.put("Имя получателя", ColumnType.FIRST_NAME)
				.put("Отчество получателя", ColumnType.MIDDLE_NAME)
				.put("Дата рождения получателя", ColumnType.BIRTH_DATE)
				.put("Пол получателя", ColumnType.SEX)
				.put("Гражданин иностранного государства", ColumnType.CITIZENSHIP)
				.put("Форма обучения", ColumnType.PROGRAM_FORM)
				.put("Высшее образование, получаемое впервые", ColumnType.IS_FIRST_HIGH_EDU)
				.put("Источник финансирования обучения", ColumnType.COMPENSATION_TYPE)
				.put("Наименование документа об образовании (оригинала)", ColumnType.ORIGINAL_TITLE)
				.put("Серия (оригинала)", ColumnType.ORIGINAL_DOC_SERIA)
				.put("Номер (оригинала)", ColumnType.ORIGINAL_DOC_NUMBER)
				.put("Регистрационный N (оригинала)", ColumnType.ORIGINAL_REG_NUMBER)
				.put("Дата выдачи (оригинала)", ColumnType.ORIGINAL_ISSUANCE_DATE)
				.put("Фамилия получателя (оригинала)", ColumnType.ORIGINAL_LAST_NAME)
				.put("Имя получателя (оригинала)", ColumnType.ORIGINAL_FIRST_NAME)
				.put("Отчество получателя (оригинала)", ColumnType.ORIGINAL_MIDDLE_NAME)
				.build();
	}

	private static ImmutableMap<String, PairKey<ColumnType, PropertyType>> initBlankPropCode2Column()
	{
		return ImmutableMap.<String, PairKey<ColumnType, PropertyType>>builder()
				.put(FAMILIYA, new PairKey<>(ColumnType.LAST_NAME, PropertyType.STRING))
				.put(IMYA, new PairKey<>(ColumnType.FIRST_NAME, PropertyType.STRING))
				.put(OTCHESTVO, new PairKey<>(ColumnType.MIDDLE_NAME, PropertyType.STRING))
				.put(REGISTRATSIONNYY_NOMER, new PairKey<>(ColumnType.REG_NUMBER, PropertyType.STRING))
				.put(NAIMENOVANIE_NAPRAVLENIYA_PODGOTOVKI_SPETSIALNOSTI_, new PairKey<>(ColumnType.PROGRAM_SUBJECT_TITLE, PropertyType.STRING))
				.put(DATA_VYDACHI, new PairKey<>(ColumnType.ISSUANCE_DATE, PropertyType.DATE))
				.put(KVALIFIKATSIYA_STEPEN, new PairKey<>(ColumnType.PROGRAM_QUALIFICATION, PropertyType.STRING))
				.put(GOD_POSTUPLENIYA, new PairKey<>(ColumnType.ENTRANCE_YEAR, PropertyType.INT))
				.put(GOD_OKONCHANIYA, new PairKey<>(ColumnType.FINISH_YEAR, PropertyType.INT))
				.put(DATA_ROJDENIYA, new PairKey<>(ColumnType.BIRTH_DATE, PropertyType.DATE))
				.put(NORMATIVNYY_PERIOD_OBUCHENIYA, new PairKey<>(ColumnType.DEVELOP_PERIOD, PropertyType.STRING))
				.put(KOD_SPETSIALNOSTI_PO_SPRAVOCHNIKU, new PairKey<>(ColumnType.PROGRAM_SUBJECT_CODE, PropertyType.STRING))
				.put(FORMA_OSVOENIYA, new PairKey<>(ColumnType.PROGRAM_FORM, PropertyType.PROGRAM_FORM_FAKE_TYPE))
				.build();
	}

	@Override
	public void importReports(byte[] fileContent, InfoCollector infoCollector)
	{
		ByteArrayInputStream inStream = new ByteArrayInputStream(fileContent);
		Workbook workbook;
		try
		{
			workbook = Workbook.getWorkbook(inStream);
		}
		catch (IOException | BiffException e)
		{
			throw new ApplicationException(FrdoEduDocumentManager.instance().getProperty("ui.wrongFileMessage"), e);
		}
		Sheet sheet = workbook.getSheet(0);
		List<ColumnType> columns = parseColumnsFromHeader(sheet);
		Collection<FrdoReportRow> reportRows = parseReportRows(sheet, columns);
		Map<SeriaNumber, FrdoReportRow> seriaNumber2ReportRows = Maps.uniqueIndex(reportRows, row -> new SeriaNumber(row.getSeria(), row.getNumber()));
		Collection<FrdoEduDocument> existingFrdoDocs = getExistingDocuments(reportRows);
		Collection<FrdoReportRow> newReportRows = skipRowsWithExistingDocuments(reportRows, existingFrdoDocs);
		Collection<FrdoEduDocument> newEmptyFrdoDocs = newReportRows.stream().map(FrdoReportParseDao::createFrdoEduDocument).collect(Collectors.toList());

		ImportCachedData cachedData = loadCachedData(reportRows);

		Collection<FrdoEduDocument> updatedFrdoDocs = fillAllPossibleFrdoDocuments(existingFrdoDocs, seriaNumber2ReportRows, cachedData);
		Collection<FrdoEduDocument> newFrdoDocs = fillAllPossibleFrdoDocuments(newEmptyFrdoDocs, seriaNumber2ReportRows, cachedData);

		Collection<FrdoEduDocument> annulledDocuments = Stream.concat(updatedFrdoDocs.stream(), newFrdoDocs.stream())
				.filter(document -> document.getDataType().getCode().equals(FrdoEduDocumentDataTypeCodes.DUPLICATE))
				.filter(document -> document.getEduDocument() != null)
				.map(FrdoEduDocument::getEduDocument)
				.collect(Collectors.toList());
		annulDocuments(annulledDocuments);

		updatedFrdoDocs.forEach(this::update);
		newFrdoDocs.forEach(this::save);

		int updatedCount = updatedFrdoDocs.size(), createdCount = newFrdoDocs.size();
		int skippedCount = reportRows.size() - (updatedCount + createdCount);
		infoCollector.add("Загрузка успешно завершена. Создано " + createdCount + ", обновлено " + updatedCount + ", пропущено " + skippedCount + " документов об образовании.");
	}

	private static List<ColumnType> parseColumnsFromHeader(Sheet sheet)
	{
		final int headerRowIdx = 0;
		List<ColumnType> result = new ArrayList<>();
		for (int colIdx = 0; colIdx < columnTitle2Type.size(); colIdx++)
		{
			String columnTitle = sheet.getCell(colIdx, headerRowIdx).getContents();
			ColumnType columnType = columnTitle2Type.get(columnTitle);
			if (columnType == null)
				throw new ApplicationException(FrdoEduDocumentManager.instance().getProperty("ui.wrongFileMessage"));
			result.add(columnType);
		}
		return result;
	}

	private static Collection<FrdoReportRow> parseReportRows(Sheet sheet, List<ColumnType> columns)
	{
		final int firstRowIdx = 1;
		final int rowsCount = sheet.getRows() - firstRowIdx;
		return IntStream.range(firstRowIdx, firstRowIdx + rowsCount).mapToObj(idx -> parseRow(sheet, idx, columns)).filter(row -> row != null).collect(Collectors.toList());
	}

	private static FrdoReportRow parseRow(Sheet sheet, int rowIdx, List<ColumnType> columns)
	{
		Map<ColumnType, String> rowContent = new HashMap<>();
		for (int colIdx = 0; colIdx < columns.size(); colIdx++)
		{
			ColumnType column = columns.get(colIdx);
			String propValue = sheet.getCell(colIdx, rowIdx).getContents();
			rowContent.put(column, propValue);
		}
		return new FrdoReportRow(rowContent);
	}

	private Collection<FrdoEduDocument> getExistingDocuments(Collection<FrdoReportRow> reportRows)
	{
		DQLValuesBuilder seriaNumberValues = new DQLValuesBuilder("seria", "number");
		reportRows.forEach(row -> seriaNumberValues.rowValues(row.getSeria(), row.getNumber()));

		return getDocumentsBySeriaAndNumber(seriaNumberValues, "seria", "number");
	}

	private Collection<FrdoEduDocument> getDocumentsBySeriaAndNumber(DQLValuesBuilder seriaNumberValues, String seriaProp, String numberProp)
	{
		final String frdoDocAlias = "frdoEduDocument";
		return new DQLSelectBuilder().fromEntity(FrdoEduDocument.class, frdoDocAlias)
				.column(property(frdoDocAlias))
				.fromDataSource(seriaNumberValues.build(), "repRow")
				.where(eq(property(frdoDocAlias, FrdoEduDocument.seriaBlank()), property("repRow", seriaProp)))
				.where(eq(property(frdoDocAlias, FrdoEduDocument.numberBlank()), property("repRow", numberProp)))
				.createStatement(getSession()).list();
	}

	private static Collection<FrdoReportRow> skipRowsWithExistingDocuments(Collection<FrdoReportRow> reportRows, Collection<FrdoEduDocument> existingDocuments)
	{
		Set<SeriaNumber> existingSeriaNumbers = existingDocuments.stream().map(frdoDoc -> new SeriaNumber(frdoDoc.getSeriaBlank(), frdoDoc.getNumberBlank())).collect(Collectors.toSet());
		return reportRows.stream().filter(row -> frdoDocNotExist(row, existingSeriaNumbers)).collect(Collectors.toList());
	}

	private static boolean frdoDocNotExist(FrdoReportRow row, Set<SeriaNumber> existingSeriaNumbers)
	{
		SeriaNumber key = new SeriaNumber(row.getSeria(), row.getNumber());
		return ! existingSeriaNumbers.contains(key);
	}

	private ImportCachedData loadCachedData(Collection<FrdoReportRow> reportRows)
	{
		Collection<String> docTypeTitles = reportRows.stream().map(row -> row.getString(ColumnType.TYPE)).collect(Collectors.toList());
		ImmutableMap<String, FrdoEduDocumentBlank> docType2Blank = loadDocType2Blank(docTypeTitles);

		ImmutableMap<String, FrdoEduDocumentDataType> docStatus2DataType = loadDocStatus2DataType();

		ImmutableMap<String, FrdoDevelopForm> devFormTitle2DevForm = loadDevFormTitle2DevForm();

		Collection<SeriaNumber> originalSeriaAndNumbers = reportRows.stream()
				.map(row -> new SeriaNumber(row.getString(ColumnType.ORIGINAL_DOC_SERIA), row.getString(ColumnType.ORIGINAL_DOC_NUMBER)))
				.collect(Collectors.toList());
		ImmutableMultimap<SeriaNumber, FrdoEduDocument> originalSeriaAndNumber2OriginalDoc = loadOriginalSeriaAndNumber2OriginalDoc(originalSeriaAndNumbers);

		FrdoEduDocumentState issuedState = getByCode(FrdoEduDocumentState.class, FrdoEduDocumentStateCodes.VYDAN);

		ImmutableMap<FrdoEduDocumentBlank, ImportCachedData.FrdoRequiredPropSettings> blank2RequiredPropSettings = loadBlank2RequiredPropSettings();

		return new ImportCachedData(docType2Blank, docStatus2DataType, devFormTitle2DevForm, originalSeriaAndNumber2OriginalDoc, issuedState, blank2RequiredPropSettings);
	}

	private ImmutableMap<String, FrdoEduDocumentBlank> loadDocType2Blank(Collection<String> docTypeTitles)
	{
		final String docTypeAlias = "frdoDocType";
		DQLSelectBuilder docTypes = new DQLSelectBuilder().fromEntity(FrdoEduDocumentType.class, docTypeAlias)
				.where(in(property(docTypeAlias, FrdoEduDocumentType.title()), docTypeTitles));

		final String blankAlias = "frdoBlank";
		final String blankHigherTitlePattern = "%образец 2012 года%".toUpperCase();
		final String blankSecondaryTitlePattern = "%образец 2010 года%".toUpperCase();
		IDQLExpression blankHigherPredicate = and(
				eq(property(blankAlias, FrdoEduDocumentBlank.eduDocumentType().educationLevel().code()), value(FrdoEducationLevelCodes.VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE)),
				likeUpper(property(blankAlias, FrdoEduDocumentBlank.title()), value(blankHigherTitlePattern)));
		IDQLExpression blankSecondaryPredicate = and(
				eq(property(blankAlias, FrdoEduDocumentBlank.eduDocumentType().educationLevel().code()), value(FrdoEducationLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE)),
				likeUpper(property(blankAlias, FrdoEduDocumentBlank.title()), value(blankSecondaryTitlePattern)));
		Collection<FrdoEduDocumentBlank> blanks = new DQLSelectBuilder().fromEntity(FrdoEduDocumentBlank.class, blankAlias)
				.where(in(property(blankAlias, FrdoEduDocumentBlank.eduDocumentType()), docTypes.buildQuery()))
				.where(or(blankHigherPredicate, blankSecondaryPredicate))
				.createStatement(getSession()).list();
		return ImmutableMap.copyOf(Maps.uniqueIndex(blanks, blank -> blank.getEduDocumentType().getTitle()));
	}

	private ImmutableMap<String, FrdoEduDocumentDataType> loadDocStatus2DataType()
	{
		FrdoEduDocumentDataType originalType = getByCode(FrdoEduDocumentDataType.class, FrdoEduDocumentDataTypeCodes.ISSUED);
		FrdoEduDocumentDataType duplicateType = getByCode(FrdoEduDocumentDataType.class, FrdoEduDocumentDataTypeCodes.DUPLICATE);
		FrdoEduDocumentDataType lostType = getByCode(FrdoEduDocumentDataType.class, FrdoEduDocumentDataTypeCodes.INVALID);
		return ImmutableMap.of("Оригинал", originalType,   "Дубликат", duplicateType,   "Утерян", lostType);
	}

	private ImmutableMap<String, FrdoDevelopForm> loadDevFormTitle2DevForm()
	{
		Collection<FrdoDevelopForm> developForms = getList(FrdoDevelopForm.class);
		return ImmutableMap.copyOf(Maps.uniqueIndex(developForms, FrdoDevelopForm::getTitle));
	}

	private ImmutableMultimap<SeriaNumber, FrdoEduDocument> loadOriginalSeriaAndNumber2OriginalDoc(Collection<SeriaNumber> originalSeriaAndNumbers)
	{
		DQLValuesBuilder seriaNumberValues = new DQLValuesBuilder("seria", "number");
		originalSeriaAndNumbers.forEach(seriaNumber -> seriaNumberValues.rowValues(seriaNumber.seria, seriaNumber.number));
		Collection<FrdoEduDocument> originalDocuments = getDocumentsBySeriaAndNumber(seriaNumberValues, "seria", "number");
		return ImmutableMultimap.copyOf(Multimaps.index(originalDocuments, document -> new SeriaNumber(document.getSeriaBlank(), document.getNumberBlank())));
	}

	private ImmutableMap<FrdoEduDocumentBlank, ImportCachedData.FrdoRequiredPropSettings> loadBlank2RequiredPropSettings()
	{
		final List<FrdoEduDocumentBlankPropertySettings> requiredProps = getList(FrdoEduDocumentBlankPropertySettings.class, FrdoEduDocumentBlankPropertySettings.required(), true);
		Multimap<FrdoEduDocumentBlank, FrdoEduDocumentBlankPropertySettings> blank2RequiredProps = Multimaps.index(requiredProps, FrdoEduDocumentBlankPropertySettings::getEduDocumentBlank);

		ImmutableMap.Builder<FrdoEduDocumentBlank, ImportCachedData.FrdoRequiredPropSettings> resultBuilder = ImmutableMap.builder();
		for (FrdoEduDocumentBlank blank : blank2RequiredProps.keySet())
		{
			Set<String> requiredPropCodes = blank2RequiredProps.get(blank).stream()
					.map(settings -> settings.getEduDocumentBlankProperty().getCode()).collect(Collectors.toSet());

			boolean existNotFilledAtAllRequiredProp = ! Sets.intersection(requiredPropCodes, unfilledBlankPropCodes).isEmpty();

			ImmutableMap.Builder<ColumnType, PropertyType> requiredColumn2TypeBuilder = ImmutableMap.builder();
			blankPropCode2Column.entrySet().stream()
					.filter(entry -> requiredPropCodes.contains(entry.getKey()))
					.map(Map.Entry::getValue)
					.forEach(key -> requiredColumn2TypeBuilder.put(key.getFirst(), key.getSecond()));
			ImportCachedData.FrdoRequiredPropSettings settings = new ImportCachedData.FrdoRequiredPropSettings(requiredColumn2TypeBuilder.build(), existNotFilledAtAllRequiredProp);
			resultBuilder.put(blank, settings);
		}
		return resultBuilder.build();
	}

	private Collection<FrdoEduDocument> fillAllPossibleFrdoDocuments(Collection<FrdoEduDocument> targetDocuments, Map<SeriaNumber, FrdoReportRow> seriaNumber2ReportRows, ImportCachedData cachedData)
	{
		Collection<FrdoEduDocument> result = new ArrayList<>();
		for (FrdoEduDocument frdoDocument : targetDocuments)
		{
			FrdoReportRow prototypeRow = seriaNumber2ReportRows.get(new SeriaNumber(frdoDocument.getSeriaBlank(), frdoDocument.getNumberBlank()));
			FrdoEduDocument filledDocument = fillDocIfPossible(frdoDocument, prototypeRow, cachedData);
			if (filledDocument != null)
				result.add(filledDocument);
		}
		return result;
	}

	private FrdoEduDocument fillDocIfPossible(FrdoEduDocument frdoDocument, FrdoReportRow prototypeRow, ImportCachedData cachedData)
	{
		if (frdoDocument.getSeriaBlank() == null || frdoDocument.getNumberBlank() == null)
			return null;

		FrdoEduDocumentBlank blank = cachedData.docType2Blank.get(prototypeRow.getString(ColumnType.TYPE));
		if (blank == null)
			return null;

		FrdoEduDocumentDataType dataType = cachedData.docStatus2DataType.get(prototypeRow.getString(ColumnType.STATUS));
		if (dataType == null)
			return null;

		Date issueData = prototypeRow.getDate(ColumnType.ISSUANCE_DATE);
		if (issueData == null)
			return null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(issueData);
		int year = calendar.get(Calendar.YEAR);

		final ImportCachedData.FrdoRequiredPropSettings requiredPropSettings = cachedData.blank2RequiredPropSettings.get(blank);

		FrdoDevelopForm developForm = cachedData.devFormTitle2DevForm.get(prototypeRow.getString(ColumnType.PROGRAM_FORM));
		boolean devFormRequired = requiredPropSettings.requiredColumn2Type.keySet().contains(ColumnType.PROGRAM_FORM);
		if ((developForm == null) && devFormRequired)
			return null;

		if (requiredPropSettings.existNotFilledAtAllRequiredProp)
			return null;

		if (! allRequiredPropsPresent(prototypeRow, requiredPropSettings))
			return null;

		frdoDocument.setBirthDate(prototypeRow.getDate(ColumnType.BIRTH_DATE));
		frdoDocument.setBlank(blank);
		frdoDocument.setCatalogSpecialityCode(prototypeRow.getString(ColumnType.PROGRAM_SUBJECT_CODE));
		frdoDocument.setCreatorName(getPrincipalContextFio());
		frdoDocument.setDataType(dataType);
		frdoDocument.setDevelopForm(developForm);
		frdoDocument.setEduDocument(getEduDocumentProp(prototypeRow, cachedData));
		frdoDocument.setEducationLevelTitle(prototypeRow.getString(ColumnType.PROGRAM_SUBJECT_TITLE));
		frdoDocument.setEndYear(prototypeRow.getInt(ColumnType.FINISH_YEAR));
		frdoDocument.setEntranceYear(prototypeRow.getInt(ColumnType.ENTRANCE_YEAR));
		frdoDocument.setFirstNameOfOwner(prototypeRow.getString(ColumnType.FIRST_NAME));
		frdoDocument.setLastNameOfOwner(prototypeRow.getString(ColumnType.LAST_NAME));
		frdoDocument.setMiddleNameOfOwner(prototypeRow.getString(ColumnType.MIDDLE_NAME));
		frdoDocument.setFormingDate(new Date());
		frdoDocument.setNameOfdegreeOrQualification(prototypeRow.getString(ColumnType.PROGRAM_QUALIFICATION));
		frdoDocument.setIssueDate(prototypeRow.getDate(ColumnType.ISSUANCE_DATE));
		frdoDocument.setLastChangeDate(new Date());
		frdoDocument.setReplaceDate(prototypeRow.getDate(ColumnType.ORIGINAL_ISSUANCE_DATE));
		frdoDocument.setSerialRegNumber(prototypeRow.getString(ColumnType.REG_NUMBER));
		frdoDocument.setStandartEduPeriod(prototypeRow.getString(ColumnType.DEVELOP_PERIOD));
		frdoDocument.setState(cachedData.issuedState);
		frdoDocument.setYear(year);

		return frdoDocument;
	}

	private static boolean allRequiredPropsPresent(FrdoReportRow reportRow, ImportCachedData.FrdoRequiredPropSettings requiredPropSettings)
	{
		return requiredPropSettings.requiredColumn2Type.entrySet().stream()
				.allMatch(entry -> !isNullProperty(reportRow, entry.getValue(), entry.getKey()));
	}

	private static boolean isNullProperty(FrdoReportRow reportRow, PropertyType propertyType, ColumnType column)
	{
		switch (propertyType)
		{
			case STRING: return reportRow.getString(column) == null;
			case DATE: return reportRow.getDate(column) == null;
			case INT: return reportRow.getInt(column) == null;
			case PROGRAM_FORM_FAKE_TYPE: return false;
		}
		return true;
	}

	private String getPrincipalContextFio()
	{
		IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
		return principalContext.getFio();
	}

	private FrdoEduDocument getEduDocumentProp(FrdoReportRow reportRow, ImportCachedData cachedData)
	{
		if (!reportRow.getString(ColumnType.STATUS).equals("Дубликат"))
			return null;

		final SeriaNumber originalSeriaNumber = new SeriaNumber(reportRow.getString(ColumnType.ORIGINAL_DOC_SERIA), reportRow.getString(ColumnType.ORIGINAL_DOC_NUMBER));
		Collection<FrdoEduDocument> originals = cachedData.originalSeriaAndNumber2OriginalDoc.get(originalSeriaNumber);
		if (originals.size() != 1)
			return null;
		return originals.iterator().next();
	}

	private static FrdoEduDocument createFrdoEduDocument(FrdoReportRow reportRow)
	{
		FrdoEduDocument result = new FrdoEduDocument();
		result.setSeriaBlank(reportRow.getSeria());
		result.setNumberBlank(reportRow.getNumber());
		return result;
	}

	private void annulDocuments(Collection<FrdoEduDocument> documentsToAnnul)
	{
		FrdoEduDocumentState annulledState = getByCode(FrdoEduDocumentState.class, FrdoEduDocumentStateCodes.ANNULIROVAN);
		Collection<Long> documentIds = documentsToAnnul.stream().map(IEntity::getId).collect(Collectors.toList());
		new DQLUpdateBuilder(FrdoEduDocument.class)
				.set(FrdoEduDocument.L_STATE, value(annulledState))
				.where(in(property("id"), documentIds))
				.createStatement(getSession()).execute();
	}

	static class SeriaNumber
	{
		public final String seria;
		public final String number;

		private int hashCode;

		public SeriaNumber(String seria, String number)
		{
			this.seria = seria;
			this.number = number;
		}

		@Override
		public int hashCode()
		{
			if (hashCode == 0)
			{
				hashCode = new HashCodeBuilder()
						.append(seria)
						.append(number)
						.hashCode();
			}
			return hashCode;
		}

		@Override
		public boolean equals(Object o)
		{
			if (!(o instanceof SeriaNumber))
				return false;
			SeriaNumber other = (SeriaNumber)o;
			return StringUtils.equals(seria, other.seria) && StringUtils.equals(number, other.number);
		}
	}
}
