package ru.tandemservice.unifrdo.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип данных документа об образовании (ФРДО)"
 * Имя сущности : frdoEduDocumentDataType
 * Файл data.xml : unifrdo.catalog.data.xml
 */
public interface FrdoEduDocumentDataTypeCodes
{
    /** Константа кода (code) элемента : Сведения о выданном документе (title) */
    String ISSUED = "1";
    /** Константа кода (code) элемента : Сведения о выданном дубликате (title) */
    String DUPLICATE = "2";
    /** Константа кода (code) элемента : Сведения о документе, выданном взамен испорченного или по иным причинам (title) */
    String INSTEAD = "3";
    /** Константа кода (code) элемента : Сведения о недействительном документе (title) */
    String INVALID = "4";
    /** Константа кода (code) элемента : Корректировка сведений, поданных ранее (title) */
    String CORRECTION = "5";
    /** Константа кода (code) элемента : Сведения об испорченном бланке (title) */
    String DAMAGED = "6";

    Set<String> CODES = ImmutableSet.of(ISSUED, DUPLICATE, INSTEAD, INVALID, CORRECTION, DAMAGED);
}
