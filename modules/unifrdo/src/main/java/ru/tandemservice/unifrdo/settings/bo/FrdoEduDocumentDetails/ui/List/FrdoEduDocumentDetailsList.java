package ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentDetails.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexander Shaburov
 * @since 14.11.13
 */
@Configuration
public class FrdoEduDocumentDetailsList extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
