package ru.tandemservice.unifrdo.component.catalog.frdoEduDocumentType.FrdoEduDocumentTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;

/**
 * @author Alexander Shaburov
 * @since 25.10.13
 */
public class Controller extends DefaultCatalogPubController<FrdoEduDocumentType, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<FrdoEduDocumentType> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Уровень образования (ФРДО)", FrdoEduDocumentType.educationLevel().title().s()));
    }
}
