package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.ImportFromReport.logic.FrdoReportParseDao.ColumnType;

/**
 * @author avedernikov
 * @since 10.04.2017
 */
class FrdoReportRow
{
	private Map<ColumnType, String> values;

	public FrdoReportRow(Map<ColumnType, String> values)
	{
		this.values = values;
	}

	public String getSeria()
	{
		return getString(ColumnType.DOC_SERIA);
	}

	public String getNumber()
	{
		return getString(ColumnType.DOC_NUMBER);
	}

	public Integer getInt(ColumnType key)
	{
		String str = values.get(key);
		return str == null ? null : Integer.parseInt(str);
	}

	public Date getDate(ColumnType key)
	{
		String str = values.get(key);
		SimpleDateFormat parser = new SimpleDateFormat(DateFormatter.PATTERN_DEFAULT);
		try
		{
			return parser.parse(str);
		}
		catch (ParseException e)
		{
			return null;
		}
	}

	public String getString(ColumnType key)
	{
		return values.get(key);
	}
}
