package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic.FrdoEduDocumentPackageSearchDSHandler;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;

/**
 * @author Alexander Shaburov
 * @since 18.11.13
 */
@Configuration
public class FrdoEduDocumentsPackageList extends BusinessComponentManager
{
    public static final String EDU_DOCUMENT_PACKAGE_SEARCH_DS = "eduDocumentPackageSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_DOCUMENT_PACKAGE_SEARCH_DS, eduDocumentPackageSearchDSColumns(), eduDocumentPackageSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduDocumentPackageSearchDSColumns()
    {
        return columnListExtPointBuilder(EDU_DOCUMENT_PACKAGE_SEARCH_DS)
                .addColumn(publisherColumn("formingDate", FrdoEduDocumentsPackage.formingDate()).order().formatter(DateFormatter.DATE_FORMATTER_WITH_TIME))
                .addColumn(dateColumn("manufactureDate", FrdoEduDocumentsPackage.manufactureDate()).order())
                .addColumn(textColumn("title", FrdoEduDocumentsPackage.title()).order())
                .addColumn(textColumn("issueYear", FrdoEduDocumentsPackage.issueYear()))
                .addColumn(textColumn("educationLevel", FrdoEduDocumentsPackage.educationLevel().title()))
                .addColumn(textColumn("fileVersion", FrdoEduDocumentsPackage.fileVersion()))
                .addColumn(dateColumn("eduDocumentFrom", FrdoEduDocumentsPackage.eduDocumentFrom()))
                .addColumn(dateColumn("eduDocumentTo", FrdoEduDocumentsPackage.eduDocumentTo()))
                .addColumn(actionColumn("print", new Icon("printer"), "onClickPrintPackage").permissionKey("frdoEduDocumentsPackageListPrint"))
                .addColumn(actionColumn("delete", new Icon("delete"), "onClickDeletePackage").alert(new FormattedMessage("eduDocumentPackageSearchDS.delete.alert", FrdoEduDocumentsPackage.title(), FrdoEduDocumentsPackage.formingDate())).permissionKey("frdoEduDocumentsPackageListDelete"))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduDocumentPackageSearchDSHandler()
    {
        return new FrdoEduDocumentPackageSearchDSHandler(getName());
    }
}
