package ru.tandemservice.unifrdo.document.bo.FrdoEduDocument.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;

/**
 * @author Alexander Shaburov
 * @since 12.11.13
 */
@Configuration
public class FrdoEduDocumentPub extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
