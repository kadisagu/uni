package ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentDataType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentState;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEduDocumentType;
import ru.tandemservice.unifrdo.catalog.entity.FrdoEducationLevel;
import ru.tandemservice.unifrdo.catalog.entity.codes.FrdoEduDocumentStateCodes;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.FrdoEduDocumentsPackageManager;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.logic.IFrdoEduDocumentsPackageDao;
import ru.tandemservice.unifrdo.document.bo.FrdoEduDocumentsPackage.ui.Pub.FrdoEduDocumentsPackagePub;
import ru.tandemservice.unifrdo.document.entity.FrdoEduDocumentsPackage;
import ru.tandemservice.unifrdo.settings.bo.FrdoEduDocumentDetails.FrdoEduDocumentDetailsManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 18.11.13
 */
public class FrdoEduDocumentsPackageAddUI extends UIPresenter
{
    private FrdoEduDocumentsPackage _package = new FrdoEduDocumentsPackage();

    private FrdoEduDocumentState _eduDocState;
    private List<FrdoEduDocumentDataType> _eduDocDataTypeList;
    private List<FrdoEduDocumentType> _eduDocTypeList;

    @Override
    public void onComponentRefresh()
    {
        prepare();
    }

    public void prepare()
    {
        _eduDocState = DataAccessServices.dao().getByCode(FrdoEduDocumentState.class, FrdoEduDocumentStateCodes.VYDAN);
        _package.setFormingDate(new Date());

        for (Map.Entry<String,String> entry : FrdoEduDocumentDetailsManager.instance().getDetailsValueMap().entrySet())
            _package.setProperty(entry.getKey(), entry.getValue());
    }

    public void onClickApply()
    {
        ByteArrayOutputStream content;
        try
        {
            content = FrdoEduDocumentsPackageManager.instance().dao().generatePackage(new PackageSettings());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        final Long id = FrdoEduDocumentsPackageManager.instance().dao().savePackage(_package, content);

        deactivate();
        getActivationBuilder().asDesktopRoot(FrdoEduDocumentsPackagePub.class)
                .parameter(UIPresenter.PUBLISHER_ID, id)
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("educationLevel", _package.getEducationLevel());
    }

    public class PackageSettings implements IFrdoEduDocumentsPackageDao.IPackageSettings
    {
        @Override
        public FrdoEduDocumentsPackage getEduDocumentsPackage()
        {
            return _package;
        }

        @Override
        public FrdoEduDocumentState getEduDocumentState()
        {
            return _eduDocState;
        }

        @Override
        public List<FrdoEduDocumentDataType> getEduDocumentDataTypeList()
        {
            return _eduDocDataTypeList;
        }

        @Override
        public List<FrdoEduDocumentType> getEduDocumentTypeList()
        {
            return _eduDocTypeList;
        }
    }

    // Accessors

    public FrdoEduDocumentsPackage getPackage()
    {
        return _package;
    }

    public void setPackage(FrdoEduDocumentsPackage aPackage)
    {
        _package = aPackage;
    }

    public FrdoEduDocumentState getEduDocState()
    {
        return _eduDocState;
    }

    public void setEduDocState(FrdoEduDocumentState eduDocState)
    {
        _eduDocState = eduDocState;
    }

    public List<FrdoEduDocumentDataType> getEduDocDataTypeList()
    {
        return _eduDocDataTypeList;
    }

    public void setEduDocDataTypeList(List<FrdoEduDocumentDataType> eduDocDataTypeList)
    {
        _eduDocDataTypeList = eduDocDataTypeList;
    }

    public List<FrdoEduDocumentType> getEduDocTypeList()
    {
        return _eduDocTypeList;
    }

    public void setEduDocTypeList(List<FrdoEduDocumentType> eduDocTypeList)
    {
        _eduDocTypeList = eduDocTypeList;
    }
}
