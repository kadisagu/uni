package ru.tandemservice.unifrdo.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние документа об образовании (ФРДО)"
 * Имя сущности : frdoEduDocumentState
 * Файл data.xml : unifrdo.catalog.data.xml
 */
public interface FrdoEduDocumentStateCodes
{
    /** Константа кода (code) элемента : Формируется (title) */
    String FORMIRUETSYA = "1";
    /** Константа кода (code) элемента : Выдан (title) */
    String VYDAN = "2";
    /** Константа кода (code) элемента : Аннулирован (title) */
    String ANNULIROVAN = "3";

    Set<String> CODES = ImmutableSet.of(FORMIRUETSYA, VYDAN, ANNULIROVAN);
}
