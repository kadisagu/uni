<entity-config
        name="unifrdo-registr-config" package-name="ru.tandemservice.unifrdo.document.entity"
        xmlns="http://www.tandemframework.org/meta/entity"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.tandemframework.org/meta/entity http://www.tandemframework.org/schema/meta/meta-entity.xsd">

    <entity name="frdoEduDocument" title="Документ об образовании (ФРДО)"  table-name="frdo_edu_doc_t">
        <many-to-one name="blank" entity-ref="frdoEduDocumentBlank" required="true"/>
        <property name="seriaBlank" type="trimmedstring" title="Серия бланка документа" required="true"/>
        <property name="numberBlank" type="trimmedstring" title="Номер бланка документа" required="true"/>
        <many-to-one name="state" entity-ref="frdoEduDocumentState" required="true"/>
        <many-to-one name="dataType" entity-ref="frdoEduDocumentDataType" required="true"/>
        <property name="year" type="integer" title="Год" required="true"/>
        <property name="regIndex" type="trimmedstring" title="Индекс книги регистрации"/>
        <property name="serialRegNumber" type="trimmedstring" title="Порядковый регистрационный номер"/>
        <property name="lastNameOfOwner" type="trimmedstring" title="Фамилия лица, получившего документ"/>
        <property name="firstNameOfOwner" type="trimmedstring" title="Имя лица, получившего документ"/>
        <property name="middleNameOfOwner" type="trimmedstring" title="Отчество лица, получившего документ"/>
        <property name="issueDate" type="date" title="Дата выдачи документа"/>
        <many-to-one name="issueAddress" entity-ref="addressItem" title="Город выдачи"/>
        <property name="educationLevelTitle" type="trimmedstring" title="Наименование направления подготовки (специальности)"/>
        <property name="oksoQualificationCode" type="trimmedstring" title="Код квалификации по ОКСО"/>
        <property name="catalogSpecialityCode" type="trimmedstring" title="Код специальности по справочнику"/>
        <property name="nameOfdegreeOrQualification" type="trimmedstring" title="Наименование присвоенной степени или квалификации"/>
        <property name="dateOfStateCertificationCommision" type="date" title="Дата протокола государственной аттестационной комиссии"/>
        <property name="numberOfStateCertificationCommision" type="trimmedstring" title="Номер протокола государственной аттестационной комиссии"/>
        <property name="fioOfCommissionChairman" type="trimmedstring" title="ФИО председателя комиссии"/>
        <property name="standartEduPeriod" type="trimmedstring" title="Нормативный период обучения"/>
        <property name="numberOfStudentExcludeOrder" type="trimmedstring" title="Номер приказа об отчислении студента (слушателя)"/>
        <property name="endDate" type="date" title="Дата окончания"/>
        <property name="entranceDate" type="date" title="Дата поступления"/>
        <property name="entranceYear" type="integer" title="Год поступления"/>
        <property name="endYear" type="integer" title="Год окончания"/>
        <many-to-one name="developForm" entity-ref="frdoDevelopForm"/>
        <property name="birthDate" type="date" title="Дата рождения"/>
        <property name="specialization" type="trimmedstring" title="Специализация"/>
        <property name="eduInstitution" type="trimmedstring" title="Образовательное учреждение"/>
        <property name="lastEduInstitutionDocument" type="trimmedstring" title="Предыдущий документ об образовании"/>
        <many-to-one name="eduDocument" entity-ref="frdoEduDocument"/>
        <property name="dateOfDuplicateDocumentOrder" type="date" title="Дата приказа о выдаче дубликата документа"/>
        <property name="numberOfDuplicateDocumentOrder" type="trimmedstring" title="Номер приказа о выдаче дубликата документа"/>
        <property name="replaceDate" type="date" title="Дата замены документа"/>
        <property name="corruptedDate" type="date" title="Дата списания испорченного бланка"/>
        <property name="specialRank" type="trimmedstring" title="Специальное звание"/>
        <property name="fioOfOrgUnitHead" type="trimmedstring" title="ФИО руководителя ОУ"/>
        <property name="formingDate" type="timestamp" title="Дата формирования документа" required="true"/>
        <property name="lastChangeDate" type="timestamp" title="Дата последнего изменения" required="true" update-generator="currentDate" insert-generator="currentDate"/>
        <property name="creatorName" type="trimmedstring" title="Имя пользователя, создавшего или изменившего запись" update-generator="currentPrincipalContextFio" insert-generator="currentPrincipalContextFio"/>

        <!--все это нужно для выполнения требования:
        если заполнены регистрационный номер и индекс книги регистрации, то рег.номер должен быть уникален в рамках комбинации: уровень образования (ФРДО), год, индекс книги регистрации-->
        <property name="serialRegNumberUniqKey" type="trimmedstring" title="Ключ уникальности для serialRegNumber">
            <comment>
                Составной ключ уникальности для полей serialRegNumber, regIndex, year, blank.eduDocumentType.educationLevel.code.
                Учитывается, если (regIndex is not null) and (serialRegNumber is not null).
                Обновляется при изменении полей serialRegNumber, regIndex, year, .
            </comment>
        </property>

        <unique-constraint name="uk_seria_number" message="Пара серия и номер должна быть уникальная.">
            <property-link name="seriaBlank"/>
            <property-link name="numberBlank"/>
        </unique-constraint>
        <unique-constraint name="uk_serial_reg_number" message="Ключ уникальности регистрационного номера должен быть уникальным.">
            <property-link name="serialRegNumberUniqKey"/>
            <condition>(regIndex is not null) and (serialRegNumber is not null)</condition>
        </unique-constraint>
        <check-constraint name="ck_serial_reg_number" message="Необходимо заполнить ключ уникальности регистрационного номера.">
            ((regIndex is not null and serialRegNumber is not null) and (serialRegNumberUniqKey is not null))
            or
            (regIndex is null or serialRegNumber is null)
        </check-constraint>
    </entity>

    <entity name="frdoEduDocumentsPackage" title="Пакет документов об образовании (ФРДО)"  table-name="frdo_edu_doc_pck_t">
        <property name="title" type="trimmedstring" title="Наименование документа" required="true"/>
        <property name="issueYear" type="integer" title="Год выдачи" required="true"/>
        <many-to-one name="educationLevel" entity-ref="frdoEducationLevel" title="Уровень образования" required="true"/>
        <property name="fileVersion" type="trimmedstring" title="Версия файла" required="true"/>
        <property name="fioPersonCreated" type="trimmedstring" title="Ф.И.О. лица, создавшего документ" required="true"/>
        <property name="fioPersonApproved" type="trimmedstring" title="Ф.И.О. лица, утвердившего документ" required="true"/>
        <property name="formingDate" type="timestamp" title="Дата формирования документа" required="true" unique="true"/>
        <property name="fromatVersion" type="trimmedstring" title="Версия формата пакета" required="true"/>
        <property name="nameOrganization" type="trimmedstring" title="Наименование организации создателя документа" required="true"/>
        <property name="idOrganization" type="trimmedstring" title="ИД организации создателя документа" required="true"/>
        <property name="placeOrganization" type="trimmedstring" title="Местонахождение организации-создателя документа (почтовый адрес)" required="true"/>
        <property name="innOrganization" type="trimmedstring" title="ИНН организации создателя документа" required="true"/>
        <property name="kppOrganization" type="trimmedstring" title="КПП организации создателя документа" required="true"/>
        <property name="ogrnOrganization" type="trimmedstring" title="ОГРН организации создателя документа" required="true"/>
        <many-to-one name="content" entity-ref="databaseFile" title="Пакет" required="true" forward-cascade="delete"/>
        <property name="manufactureDate" type="date" title="Дата изготовления документа" required="true"/>
        <property name="eduDocumentFrom" type="date" title="Документы об образовании, выданные от"/>
        <property name="eduDocumentTo" type="date" title="Документы об образовании, выданные по"/>
    </entity>

</entity-config>