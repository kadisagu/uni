#!/bin/bash

set -e
#set -v

#export JAVA_HOME=/home/ssd/dev/jdks/jdk7
#export MAVEN_HOME=/home/ssd/dev/maven3
#export PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:/usr/bin:/bin
export TANDEM_ADD_PATH="../../shared"
export TANDEM_UNI_PATH="../../tandem.uni"

# краткая инструкция по заполнению
#
# JAVA_HOME=/opt/java/jdk170 (нужно написать свой путь)
# - это путь до явы (там, где лежит папка bin, в которой лежит исполняемый файл java, это обязательно должен быть jdk, не jre)
#
# MAVEN_HOME=/opt/projects/maven (нужно написать свой путь)
# - это путь до мавена (там, где лежит папка bin, в которой лежит исполняемый файл maven)
#
# PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:/usr/bin:/bin 
# - это заполняется автоматом, если каких-то бинарников не хватает, то можно дописать пути до них
#
# TANDEM_UNI_PATH="../../tandem-uni" (нужно написать свой путь)
# - путь до места, куда извлечен транк uni (чтобы себя проверить - внутри этой папки должна быть папка product-parent)
# - в двнном случае это относительный путь от папки, где лежит скрипт, 
# - подразумевается, что uni извлечен в папку tandem-uni 
# - можно использовать абсолютные пути, например /projects/uni (pom от нашего product-parent лежит в /projects/uni/pruduct-parent/pom.xml)
#
# TANDEM_ADD_PATH="../../tandem-add" (нужно написать свой путь)
# - путь до места, куда извлечен транк shared (чтобы себя проверить - внутри этой папки должна быть папка shared-parent)
# - в данном случае это относительный путь от папки, где лежит скрипт, 
# - подразумевается, что shared извлечен в папку tandem-add, которая лежит рядом с tandem-uni (можно написать другую)
# - можно использовать абсолютные пути, например /projects/shared (pom от нашего shared-parent лежит в /projects/shared/shared-parent/pom.xml)

##########################################################################
##########################################################################
##########################################################################

read version < <(cat ../projects/pom.xml | grep -E "<version>.*-UNI-SNAPSHOT</version>" |
 sed -e "s|.*<version>||" | sed -e "s|-UNI-SNAPSHOT</version>.*||" | sort | uniq)

[ -d "$version" ] || mkdir "$version"

read timestamp < <(date +%Y%m%d%H%M%S)

BASE="$version/$timestamp"

[ -d "$BASE" ] || mkdir "$BASE"

##########################################################################
##########################################################################
##########################################################################


# информация по модулям
declare -A module_info

function module_path {
  case $1 in
    org.tandemframework.shared)
      if [ -d "${TANDEM_ADD_PATH}/$2" ]; then
        echo "SH ${TANDEM_ADD_PATH}/$2"
      fi
    ;;
    ru.tandemservice.uni.product)
      if [ -d "${TANDEM_UNI_PATH}/modules/$2" ]; then
        echo "UNI ${TANDEM_UNI_PATH}/modules/$2"
      elif [ -d "${TANDEM_UNI_PATH}/modules/deprecated/$2" ]; then
        echo "UNI(Dep) ${TANDEM_UNI_PATH}/modules/deprecated/$2"
      else
        echo ""
      fi
    ;;
  esac
  echo ""
}

function load_module_info {

 # проверяем, что модуль еще не загружен
 mg="${1}"
 mn="${2}"
 if [ "${module_info[${mn}:name]}" = "" ]; then

  # пробуем получить расположение модуля
  read tp mp < <(module_path $mg $mn)
  if [ -n "$tp" -a -n "$mp" ]; then

   # вгружаем описание модуля
   module_info+=(["${mn}"]="$mn" ["${mn}:group"]="$mg" ["${mn}:name"]="${mn}" ["${mn}:mtp"]="$tp")

   # парсим файлы модулей
   if [ -f "$mp/module.info.properties" ]; then
     while read -r k v || [ -n "$k" ]; do
       module_info+=(["${mn}:$k"]="$(echo -e "$v")") # encode ascii2native
     done < <(cat "$mp/module.info.properties")

     if [ "${module_info[${mn}:wiki-title]}" = "" ]; then
       module_info+=(["${mn}:wiki-title"]="М ${module_info[${mn}:real-title]}")
     fi
   fi
  fi
 fi

}



##########################################################################
################### по файлам проектов в svn #############################
##########################################################################

# поехали (по проектам)
while read prj; do

 prj_dir="../projects/$prj"

 # создаем dep.list со списком всего, что наше (наше отличается от не нашего тем, что там есть -SNAPSHOT)
 # это очень долго (т.к. мы запускаем maven в каждой сборке) 
 # поэтому результат кэшируется (если хочется пересобрать - нужно просто удалить все dep.list)
 [ -f "$prj_dir/dep.list" ] || (
  echo $prj && (
  cd $prj_dir/$prj && mvn dependency:list |
    grep -E "[[]INFO[]]" | grep -E "[:]" | grep -E "[-]SNAPSHOT" |
    sed -e s/"[[]INFO[]] "/""/ |  sed -e s/"[-\|+ ]*"/""/ |
    sort | uniq |
    sed -e s/"[:]\(jar\|pom\|war\).*"/""/ |
    sort | uniq 
  ) > $prj_dir/dep.list
 )

 # парсим сборку (если информации нет - пропускаем)
 [ -f "$prj_dir/project.info.properties" ] || continue

 # грузим данные проектной сборки
 declare -A prjinfo
 prjinfo=([name]="$prj")
 while read -r k v || [ -n "$k" ]; do
  prjinfo+=(["$k"]="$(echo -e "$v")") # encode ascii2native
 done < <(cat "$prj_dir/project.info.properties")

 # название для клиентского модуля
 prj_x_title=""
 if [ "client" = "${prjinfo[type]}" ]; then
  prj_x_title="${prjinfo[title]}"
  if [ "$prj_x_title" = "unknown" -o "$prj_x_title" = "" ]; then
   prj_x_title="unknown-${prjinfo[name]}"
  fi
 fi

 # запись проекта в файл с проектами: HEADER
 (
  echo "|| Сборка || Название || Тип || Wiki ||"
  echo "| ${prjinfo[name]} | ${prjinfo[title]} | ${prjinfo[type]} | ${prjinfo[wiki]} |"
  echo ""
  echo "|| Группа || Модуль (в коде) || Модуль || Входит в маркетинговый модуль || Карточка модуля в wiki ||"
 ) >> "$BASE/projects.${prjinfo[type]}.wiki"

 # выводим маркетинговую сохраняем информацию по сборкам: HEADER
 [ -n "${prj_x_title}" ] && (
  echo "----"
  echo "Клиент: ${prj_x_title}"
  echo "----"
  echo ""
  echo "${prjinfo[name]}"
  [ "${prjinfo[wiki]}" = "todo" ] || echo "${prjinfo[wiki]}"
 ) > "$BASE/tmp.prj-market-header.${prj_x_title}.wiki"

 # бежим по перечню модулей проектной сборки
 while read mg mn x; do

  # загружаем информацию о модуле
  load_module_info $mg $mn

  # пропускаем модуль, если о нем нет информации, либо явно сказано, что надо пропускать
  mtp="${module_info[${mn}:mtp]}"
  wiki_url="${module_info[${mn}:wiki]}"
  wiki_space="${module_info[${mn}:wiki-space]}"
  wiki_title="${module_info[${mn}:wiki-title]}"
  wiki_link="[${wiki_space}:${wiki_title}]"
  title_r="${module_info[${mn}:real-title]}"
  title_m="${module_info[${mn}:market-title]}"

  [ "${mtp}" = "" ] && continue
  [ "${module_info[${mn}:skip]}" = "true" ] && continue

  # запись проекта в файл с проектами: DATA
  (
   echo "| ${mtp} | ${mn} | ${title_r} | ${title_m} | ${wiki_link} |"
  ) >> "$BASE/projects.${prjinfo[type]}.wiki"

  # выводим информацию по маркетинговому модулю проектной сборки: DATA
  [ -n "${prj_x_title}" ] && (
   echo "${title_m}"
  ) >> "$BASE/tmp.prj-market-modules.${prj_x_title}.wiki"

 done < <(cat $prj_dir/dep.list | sed -e 'y|:| |')

 # запись проекта в файл с проектами: FOOTER
 (
  echo ""
  echo "----"
  echo ""
 ) >> "$BASE/projects.${prjinfo[type]}.wiki"

done < <(cd ../projects && find -maxdepth 1 -type d | grep -E "[.]/" | sed -e 's|[.]/||'| sort)

# собираем projects.market.wiki
(
cd $BASE &&
ls | grep -E "^tmp[.]prj-market-header[.].*[.]wiki$" | sort | while read f; do
 prj_x_title=`echo "$f" | sed -e 's|^tmp[.]prj-market-header[.]||' | sed -e 's|[.]wiki$||'`
 cat "tmp.prj-market-header.${prj_x_title}.wiki"
 echo ""
 cat "tmp.prj-market-modules.${prj_x_title}.wiki" | sort | uniq
 echo ""
done >> "projects.market.wiki"
)

# очищаем (tmp-файлы)
(
cd $BASE &&
ls | grep -E "^tmp[.].*[.]wiki$" | while read f; do
 rm "$f"
done
)


##########################################################################
############## по данным сервера обновлений ##############################
##########################################################################

# скачиваем с сервера обновлений файлик
[ -f "update.config.xml" ] || (
 scp update@update.tandemservice.ru:/storage/update/updateserver/.config.xml update.config.xml || exit 0
)

cp update.config.xml $BASE/update.config.xml
cp update.properties $BASE/update.properties

# обрабатываем update.properties
declare -A update_info
while read -r k v || [ -n "$k" ]; do
 if [ -n "$k" ]; then
  update_info+=(["$k"]="$(echo -e "$v")") # encode ascii2native
 fi
done < <(cat "$BASE/update.properties")

# в project.partner.wiki выводить все инсталляции, алиас и название, 
# и перечень всех модулей с указанием названия и маркетингового названия
# project.partner.wiki

# теперь надо парсить файл с сервера обновлений
while read l; do
 name=`echo $l | sed -e 's|.*name="\([^"]*\)".*|\1|'`
 partner=`echo $l | sed -e 's|.*partner="\([^"]*\)".*|\1|'`
 modules=`echo $l | sed -e 's|.*modules="\([^"]*\)".*|\1|'`

 partner_skip="${update_info[${partner}.skip]}"
 [ "${partner_skip}" = "true" ] && continue

 partner_title="${update_info[${partner}.title]}"
 client_name="${update_info[${name}]}"
 prj_x_title="${partner_title}, клиент: ${client_name}"

 # запись проекта в файл с партнерами: HEADER
 (
  echo "|| Партнер || Алиас || Название ||"
  echo "| ${partner_title} | ${name} | ${client_name} |"
  echo ""
  echo "|| Группа || Модуль (в коде) || Модуль || Входит в маркетинговый модуль || Карточка модуля в wiki ||"
 ) >> "$BASE/projects.partner.wiki"

 # выводим маркетинговую сохраняем информацию по сборкам: HEADER
 [ -n "${prj_x_title}" ] && (
  echo "----"
  echo "Партнер: ${partner_title}, Клиент: ${client_name}"
  echo "----"
 ) > "$BASE/tmp.prj-market-header.${prj_x_title}.wiki"

 while read mg mn x; do

  # загружаем информацию о модуле
  load_module_info $mg $mn

  # пропускаем модуль, если о нем нет информации, либо явно сказано, что надо пропускать
  mtp="${module_info[${mn}:mtp]}"
  wiki_url="${module_info[${mn}:wiki]}"
  wiki_space="${module_info[${mn}:wiki-space]}"
  wiki_title="${module_info[${mn}:wiki-title]}"
  wiki_link="[${wiki_space}:${wiki_title}]"
  title_r="${module_info[${mn}:real-title]}"
  title_m="${module_info[${mn}:market-title]}"

  [ "${mtp}" = "" ] && continue
  [ "${module_info[${mn}:skip]}" = "true" ] && continue

  # запись проекта в файл с партнерами: DATA
  (
   echo "| ${mtp} | ${mn} | ${title_r} | ${title_m} | ${wiki_link} |"
  ) >> "$BASE/projects.partner.wiki"

  # выводим информацию по маркетинговому модулю проектной сборки: DATA
  [ -n "${prj_x_title}" ] && (
   echo "${title_m}"
  ) >> "$BASE/tmp.prj-market-modules.${prj_x_title}.wiki"

 done < <(
  echo $modules  | tr ";" "\n" | sed -e s'|:| |' | while read a b; do
   echo $b | tr "," "\n" | while read x; do
    echo "$a $x"
   done
  done | sort
 )

 # запись проекта в файл с партнерами: FOOTER
 (
  echo ""
  echo "----"
  echo ""
 ) >> "$BASE/projects.partner.wiki"

done < <(
 cat "$BASE/update.config.xml" | grep "client" | grep "partner" | grep "modules"
)

# собираем projects.market.wiki
(
cd $BASE &&
ls | grep -E "^tmp[.]prj-market-header[.].*[.]wiki$" | sort | while read f; do
 prj_x_title=`echo "$f" | sed -e 's|^tmp[.]prj-market-header[.]||' | sed -e 's|[.]wiki$||'`
 cat "tmp.prj-market-header.${prj_x_title}.wiki"
 echo ""
 cat "tmp.prj-market-modules.${prj_x_title}.wiki" | sort | uniq
 echo ""
done >> "projects.market.wiki"
)

# очищаем (tmp-файлы)
(
cd $BASE &&
ls | grep -E "^tmp[.].*[.]wiki$" | while read f; do
 rm "$f"
done
)