/* $Id: ReceiptPrintScript.groovy 23062 2016-05-26 15:45:53Z nsvetlov $ */
// Copyright 2006-2016 Tandem Service Software
package uniszipa.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantDocumentType
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument

return new ReceiptPrint(                                          // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author oleyba
 * @since 5/13/13
 */
class ReceiptPrint
{
  Session session
  byte[] template
  EnrEntrantRequest entrantRequest
  def im = new RtfInjectModifier()
  def tm = new RtfTableModifier()

  def print()
  {
    def person = entrantRequest.entrant.person

    im.put('FIO', person.fullFio)
    im.put('receiptNumber', entrantRequest.stringNumber)

    def date = entrantRequest.regDate.toCalendar();
    im.put('day', date.get(Calendar.DAY_OF_MONTH) as String);
    im.put('month', date.format("MMMM") );
    im.put('year', date.get(Calendar.YEAR) as String);

    def fio
    def identityCardDeclinability = IUniBaseDao.instance.get().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), person.identityCard.id)
    if (identityCardDeclinability != null)
    {
      def lastName = person.identityCard.lastName
      if (identityCardDeclinability.lastNameDeclinable)
        lastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastName, GrammaCase.GENITIVE, person.isMale())

      def firstName = person.identityCard.firstName
      if (identityCardDeclinability.firstNameDeclinable)
        firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstName, GrammaCase.GENITIVE, person.isMale())

      def middleName = person.identityCard.middleName
      if (identityCardDeclinability.middleNameDeclinable)
        middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, person.isMale())

      def result = new ArrayList<String>()
      if (!StringUtils.isEmpty(lastName)) result.add(lastName)
      if (!StringUtils.isEmpty(firstName)) result.add(firstName)
      if (!StringUtils.isEmpty(middleName)) result.add(middleName)

      fio = StringUtils.join(result, ' ')

    } else
    {
      fio = PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.GENITIVE)
    }

    im.put('fromFIO', fio)

    // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок'
    def titles = getTitles()

    int i = 1
    tm.put('T', titles.collect {[(i++) + '. '+ it]} as String[][])
    tm.put('T',getBoldUnderlineIntercepter())

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(template, im, tm),
            fileName: "Расписка абитуриента ${person.identityCard.fullFio}.rtf"]
  }

  def getTitles()
  {
    def entrant = entrantRequest.entrant    // абитуриент
    def idc = entrantRequest.identityCard   // выбранное в заявлении УЛ
    def edu = entrantRequest.eduDocument // выбранный док. об образовании

    // получаем список документов из заявления в порядке приоритета
    def attachments = DQL.createStatement(session, /
                from ${EnrEntrantRequestAttachment.class.simpleName}
                where ${EnrEntrantRequestAttachment.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EnrEntrantRequestAttachment.id()}
        /).<EnrEntrantRequestAttachment> list()

    attachments.sort() // в объекте реализован корректный compareTo() - сначала по типу по настройке порядка документов, затем по дате создания документа

    def titles = new ArrayList<String>()

    titles.add(idc.getDisplayableTitle() + " (копия)")
    titles.add(edu.getDisplayableTitle() + (entrantRequest.eduInstDocOriginalHandedIn ? " (оригинал)" : " (копия)"))

    for (def attachment: attachments) {
      titles.add(attachment.document.displayableTitle + (attachment.originalHandedIn ? " (оригинал)" : " (копия)"))
    }

      //Выводить строку в случае, если в заявлении нет конкурса с особыми правами
      if (entrantRequest.benefitCategory == null) {
        // Префикс \\bu используется для выделения последующего текста жирным подчеркнутым шрифтом
          titles.add("Копия документов подтверждающих особые права \\buНЕТ");
      }

      //Выводить строку в случае, если у абитуриента нет индивидуальных достижений
      def achievments = DQL.createStatement(session, /
                from ${EnrEntrantAchievement.class.simpleName}
                where ${EnrEntrantAchievement.entrant().id()} = ${entrant.id}
        /).<EnrEntrantAchievement> list()

      if(achievments.isEmpty())
      {
          titles.add("Копия документов подтверждающих индивидуальные достижения \\buНЕТ");
      }

    return titles
  }

  private static IRtfRowIntercepter getBoldUnderlineIntercepter() {
    return new IRtfRowIntercepter() {
      @Override
      public void beforeModify(RtfTable table, int currentRowIndex) {

      }

      @Override
      public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {

        if (value != null && (value.contains("\\bu"))) {
          String[] parts = value.split("\\\\bu")

          return new RtfString()
                  .append(parts[0])
                  .boldBegin()
                  .append(1722) //Подчеркивание
                  .append(parts[1])
                  .append(1722, 0)
                  .boldEnd().toList();
        }
        return null;
      }

      @Override
      public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

      }
    }
  }

}

