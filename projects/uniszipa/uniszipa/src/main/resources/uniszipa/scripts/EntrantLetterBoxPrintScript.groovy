/* $Id$ */
package uniszipa.scripts

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams

return new EntrantLetterBoxPrint(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Denis Katkov
 * @since 26.05.2016
 */

class EntrantLetterBoxPrint {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    EnrEntrantBaseDocument personBenefit
    boolean ege = true
    List<EnrOlympiadDiploma> diplomaList
    List<EnrEntrantAchievement> achievementList

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().entrant().id()}=${entrantRequest.entrant.id}
                order by ${EnrRequestedCompetition.request().regDate()}, ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        //получаем первую льготу абитуриента
        def benefits = DQL.createStatement(session, /
                from ${EnrEntrantBaseDocument.class.simpleName}
                where ${EnrEntrantBaseDocument.entrant().id()}=${entrantRequest.entrant.id}
                /).<EnrEntrantBaseDocument> list();

        if (CollectionUtils.isNotEmpty(benefits)) {
            personBenefit = benefits.get(0);
        }
        //получаем олимпиады
        diplomaList = DQL.createStatement(session, /
                from ${EnrOlympiadDiploma.class.simpleName}
                where ${EnrOlympiadDiploma.entrant().id()}=${entrantRequest.entrant.id}
                /).<EnrOlympiadDiploma> list();
        //получаем индивидуальные достижения
        achievementList = DQL.createStatement(session, /
                from ${EnrEntrantAchievement.class.simpleName}
                where ${EnrEntrantAchievement.entrant().id()}=${entrantRequest.entrant.id}
                /).<EnrEntrantAchievement> list();

        fillRequestedDirectionList(directions)
        fillInjectParameters()

        RtfDocument document = new RtfReader().read(template);

        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Титульный лист ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillRequestedDirectionList(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def rows = new ArrayList<String[]>()
        short i = 1
        for (EnrRequestedCompetition requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из восьми столбцов
            String[] row = new String[8]

            row[0] = i++
            def programSet = requestedCompetition.competition.programSetOrgUnit.programSet
            def programSubject = programSet.programSubject
            def programForm = programSet.programForm
            def competitionType = requestedCompetition.competition.type
            def compensationType = competitionType.compensationType

            row[1] = programSubject.title + "-" + programForm.shortTitle + "-" + compensationType.shortTitle.substring(0, 3)
            row[2] = requestedCompetition.request.stringNumber
            row[3] = DateFormatter.DEFAULT_DATE_FORMATTER.format(requestedCompetition.request.regDate)

            def choosenEntranceExam = DQL.createStatement(session, /
                    from ${EnrChosenEntranceExam.class.simpleName}
                    where ${EnrChosenEntranceExam.requestedCompetition().id()}=${requestedCompetition.id}
                    order by ${EnrChosenEntranceExam.discipline().id()}
                    /).<EnrChosenEntranceExam> list()

            def discipResult = new StringBuilder()
            def fl = true
            def hasNotMark = false
            def sum = 0
            def olymp = ""
            for (EnrChosenEntranceExam exam : choosenEntranceExam) {
                discipResult.append((!fl ? "[PAR]" : ""))
                        .append(exam.discipline.discipline.title)
                if (null != exam.maxMarkForm && exam.maxMarkForm.markAsLong > 0) {
                    discipResult.append("-" + exam.maxMarkForm.markAsString)
                    sum += exam.maxMarkForm.markAsLong

                    if (exam.markSource instanceof EnrEntrantMarkSourceBenefit) {
                        olymp = ((EnrEntrantMarkSourceBenefit) exam.markSource).benefitCategory.shortTitle
                    }
                } else
                    hasNotMark = true
                fl = false

                if (exam.maxMarkForm == null || !exam.maxMarkForm.passForm.code.equals(EnrExamPassFormCodes.STATE_EXAM)) {
                    ege = false;
                }
            }
            row[4] = discipResult as String
            row[5] = (!hasNotMark && sum > 0 ? String.valueOf(sum / 1000) : "")

            def coExcl = ""
            def competitionsExcl = requestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }.collect { e -> (EnrRequestedCompetitionExclusive) e }
            if (!competitionsExcl.isEmpty()) {
                coExcl = ((EnrRequestedCompetitionExclusive) competitionsExcl.get(0)).benefitCategory.shortTitle
            }
            def coNoEx = ""
            def competitionsNoExam = requestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }.collect { e -> (EnrRequestedCompetitionNoExams) e }
            if (!competitionsNoExam.isEmpty()) {
                coNoEx = ((EnrRequestedCompetitionNoExams) competitionsNoExam.get(0)).benefitCategory.shortTitle
            }
            row[6] = (!olymp.empty || !coExcl.empty || !coNoEx.empty) ? "Преим. " + [olymp, coExcl, coNoEx].toSet().toList().join(", ") : ""

            row[7] = (EnrCompetitionTypeCodes.MINISTERIAL.equals(competitionType.code) ? "" : competitionType.printTitle)
            rows.add(row)
        }
        tm.put("T", rows as String[][])
        // внедряем параграфы в 5 столбце
        tm.put("T", new RtfRowIntercepterBase() {
            @Override
            List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if (colIndex == 4) {
                    if (value.contains("[PAR]")) {
                        def newStr = new RtfString()
                        def a = value.indexOf("[PAR]", 0)
                        def b = 0
                        while (a > 0) {
                            newStr.append(value.substring(b, a)).par()
                            b = a + 5
                            a = value.indexOf("[PAR]", a + 1)
                        }
                        newStr.append(value.substring(b))
                        return newStr.toList()
                    }
                }
            }
        })
    }

    def fillInjectParameters() {
        def entrant = entrantRequest.entrant
        def card = entrantRequest.identityCard

        StringBuilder diplomaTypes = new StringBuilder();
        diplomaList.each { diploma ->
            diplomaTypes.append(diploma.olympiad.title);
        };

        double sum;
        achievementList.each { achievement ->
            sum += achievement.getMark();
        };

        String citizenship = "";
        if (entrant.person.identityCard.citizenship != null) {
            if (entrant.person.identityCard.citizenship.code == 0) {
                citizenship = "РФ";
            } else {
                citizenship = "НЕ РФ";
            }
        }

        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("lastName", StringUtils.upperCase(card.lastName))
        im.put("entrantNumber", entrant.personalNumber)
        im.put("passport", card.fullNumber)
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("certificate", entrantRequest.eduDocument.eduDocumentKind.title)
        im.put("certificateYear", entrantRequest.eduDocument.yearEnd as String)
        im.put("benefit", personBenefit != null ? personBenefit.documentType.title : "")
        im.put("ege", ege ? "ЕГЭ" : "")
        im.put("olympiadType", diplomaTypes.toString())
        im.put("citizenship", citizenship)
        im.put("withHonours", entrantRequest.eduDocument.graduationHonour != null ? "С ОТЛ." : "")
        im.put("achievementResult", sum > 0 ? sum as String : "")
        im.put("operator", entrantRequest.acceptRequest != null ? "Ф.И.О оператора: " + entrantRequest.acceptRequest : "")
    }
}