/** 
 *$Id$
 */
// Copyright 2006-2012 Tandem Service Software
package uniszipa.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection

return new OnlineEntrantRequestPrint(                           // стандартные входные параметры скрипта
        session: session,                                       // сессия
        template: template,                                     // шаблон
        onlineEntrant: session.get(OnlineEntrant.class, object) // объект печати
).print()

/**
 * Алгоритм печати онлайн-заявления абитуриента (СЗИ РАНХиГС)
 *
 * @author Alexander Shaburov
 * @since 14.06.12
 */
class OnlineEntrantRequestPrint
{
  Session session
  byte[] template
  OnlineEntrant onlineEntrant
  def modifier = new RtfInjectModifier()
  def tableModifier = new RtfTableModifier()
  def document

  /**
   * Основной метод печати.
   * @return <code>[document: byte[], fileName: String]</code>
   */
  def print()
  {
    def directions = DQL.createStatement(session, /
                from ${OnlineRequestedEnrollmentDirection.class.simpleName}
                where ${OnlineRequestedEnrollmentDirection.entrant().id()} = ${onlineEntrant.id}
                order by ${OnlineRequestedEnrollmentDirection.id()}
    /).<OnlineRequestedEnrollmentDirection>list();

    document = new RtfReader().read(template);

    prepareInjectModifier(directions)
    prepareTableModifier(directions)

    modifier.modify(document);
    tableModifier.modify(document);

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(document),
            fileName: "Онлайн-заявление абитуриента ${onlineEntrant.fullFio}.rtf"]
  }

  /**
   * Подготавливает и заполняет метки для печати.
   * @param directionList Выбранные онлайн направление приема
   */
  def prepareInjectModifier(List<OnlineRequestedEnrollmentDirection> directionList)
  {
    def dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER

    modifier.put('requestNumber', String.valueOf(onlineEntrant.personalNumber))
    modifier.put('address', onlineEntrant.actualAddress.titleWithFlat)
    modifier.put('registrationAddress', onlineEntrant.registrationAddress != null ? onlineEntrant.registrationAddress.titleWithFlat : '')
    modifier.put('foreignLanguages', onlineEntrant.foreignLanguage != null ? onlineEntrant.foreignLanguage.title : '')
    modifier.put('benefits', onlineEntrant.benefit != null ? onlineEntrant.benefit.title : '')
    modifier.put('father', getNextOfKin(RelationDegreeCodes.FATHER));
    modifier.put('mother', getNextOfKin(RelationDegreeCodes.MOTHER));
    modifier.put('lastName', onlineEntrant.lastName);
    modifier.put('firstName', onlineEntrant.firstName);
    modifier.put('middleName', onlineEntrant.middleName != null ? onlineEntrant.middleName : '');
    modifier.put('sex', onlineEntrant.sex.male ? 'М' : 'Ж');
    modifier.put('birthDate', dateFormatter.format(onlineEntrant.birthDate));
    modifier.put('birthPlace', onlineEntrant.birthPlace);
    modifier.put('passportCitizenship', onlineEntrant.passportCitizenship != null ? onlineEntrant.passportCitizenship.title : '');
    modifier.put('passport', onlineEntrant.passportType.title + ': ' + (onlineEntrant.passportSeria != null ? onlineEntrant.passportSeria : '') + ' ' + (onlineEntrant.passportNumber != null ? onlineEntrant.passportNumber : ''));
    modifier.put('passportIssuanceDate', dateFormatter.format(onlineEntrant.passportIssuanceDate));
    modifier.put('passportIssuancePlace', onlineEntrant.passportIssuancePlace);
    modifier.put('a', onlineEntrant.sex.male ? 'ий' : 'ая');
    modifier.put('f', onlineEntrant.sex.male ? '' : 'а');
    modifier.put('h', onlineEntrant.sex.male ? 'ен' : 'на');
    modifier.put('phoneNumbers', [onlineEntrant.phoneFact, onlineEntrant.phoneMobile].grep().join(', '))
    modifier.put('email', onlineEntrant.email);

    //если выбранных направлений приема нет, то метки использующие эти данные делаем пустыми
    if (directionList.size() > 0)
    {
      def priorityDirection = directionList.get(0);
      modifier.put('developForm_A', priorityDirection.enrollmentDirection.educationOrgUnit.developForm.accCaseTitle);
      modifier.put('b', priorityDirection.compensationType.code == CompensationTypeCodes.COMPENSATION_TYPE_BUDGET ? 'x' : '');
      modifier.put('c', priorityDirection.compensationType.code == CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT ? 'x' : '')
    }
    else
    {
      modifier.put('developForm_A', '');
      modifier.put('b', '');
      modifier.put('c', '')
    }

    modifier.put('edEndYear', onlineEntrant.eduDocumentYearEnd != null ? String.valueOf(onlineEntrant.eduDocumentYearEnd) : '');
    modifier.put('eduInstitution', onlineEntrant.eduInstitution != null ? onlineEntrant.eduInstitution.titleWithAddress : '');
    modifier.put('certificate', onlineEntrant.eduDocumentType.title + ', ' + onlineEntrant.eduDocumentLevel.title);
    modifier.put('certificateDetail', onlineEntrant.eduDocumentGraduationHonour != null ? onlineEntrant.eduDocumentGraduationHonour.title : '');
    modifier.put('edSeria', onlineEntrant.eduDocumentSeria);
    modifier.put('edNumber', onlineEntrant.eduDocumentNumber);
    modifier.put('edIssuanceDate', onlineEntrant.eduDocumentDate != null ? dateFormatter.format(onlineEntrant.eduDocumentDate) : '');

    modifier.put('needHotel', onlineEntrant.needHotel ? 'нуждаюсь' : 'не нуждаюсь');

    modifier.put('olympiadStr', getOlympiad());
    modifier.put('benefitsDetail', getBenefit());

    def middleGos23 = true//среди выбранных направлений приема есть направления СПО (гос 2 или гос 3)
    def bachelorOrSpecialty = true//выбранные направления только с квалификацией 62 (бакалавр) или 65 (специалист)
    def master = false//есть хотя бы одного направление магистратуры
    def middle = false//есть хотя бы одно направление СПО
    for (def direction : directionList)
    {
      if (direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.levelType.middleGos2 ||
              direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.levelType.middleGos3)
      {
        middleGos23 = false
      }

      if (direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.levelType.middle)
      {
        middle = true
      }

      if (direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification != null)
      {
        if (!QualificationsCodes.BAKALAVR.equals(direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code) ||
                !QualificationsCodes.SPETSIALIST.equals(direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code))
        {
          bachelorOrSpecialty = false
        }

        if (QualificationsCodes.MAGISTR.equals(direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code))
        {
          master = true
        }
      }
    }

    //если выбранных направлений приема нет, то метки использующие эти данные делаем пустыми
    if (directionList.size() == 0)
    {
      modifier.put('eduLevelStr', '');
      modifier.put('eduLevelTypeStr_G', '');
      tableModifier.put('fiveVuz', (String[]) null);
      tableModifier.put('firstHighEducation', (String[]) null);
    }
    else
    {
      modifier.put('eduLevelStr', middleGos23 ? 'Северо-Западном институте - филиале РАНХиГС' : 'социальном техникуме Северо-Западного института - филиала РАНХиГС');
      modifier.put('eduLevelTypeStr_G', middle ? 'среднего' : 'высшего');

      if (bachelorOrSpecialty)
      {
        def rtfString = new RtfString()
                .append('Подтверждаю подачу заявления не более чем в пять вузов.')
                .par()
                .append('Заявление в Академию является _______ по счету.');

        modifier.put('fiveVuz', rtfString);
      }
      else
      {
        tableModifier.put('fiveVuz', (String[]) null);//удаляем строку таблицы, содержащую метку
      }

      if (master)
      {
        tableModifier.put('firstHighEducation', (String[]) null);//удаляем строку таблицы, содержащую метку
      }
      else
      {
        modifier.put('firstHighEducation', middle ? 'Среднее профессиональное образование получаю впервые' : 'Высшее профессиональное образование получаю впервые');
      }
    }
  }

  /**
   * Подготавливает и заполняет табличные метки для печати.
   * @param directionList Выбранные онлайн направление приема
   */
  def prepareTableModifier(def directionList)
  {
    //таблица с данными одного Направления приема, с наивысшим приоритетом
    if (directionList.size() > 0)
    {
      def primaryDirection = directionList.get(0);
      def directionData = new ArrayList<String[]>()//список строк

      directionData.add([primaryDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.okso + '.' + primaryDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code,
              primaryDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.title] as String[])

      tableModifier.put('T', directionData as String[][]);
    }
    else
    {
      tableModifier.put('T', [['', '', '']] as String[][])
    }

    //таблица с данными Вступительных испытаний
    tableModifier.put('T1', [['','','']] as String[][])
  }

  /**
   * Вычисляет данные по ближайшему родственнику.
   * @return ФИО, Место работы, Должность, Телефон
   */
  def getNextOfKin(def relationDegreeCode)
  {
    def relationDegree = onlineEntrant.nextOfKinRelationDegree
    if (relationDegree)
    {
      if (!relationDegree.code.equals(relationDegreeCode))
        return ''

      def fullFio = [onlineEntrant.nextOfKinLastName,
              onlineEntrant.nextOfKinFirstName,
              onlineEntrant.nextOfKinMiddleName].grep().join(' ')
      def description = [fullFio,
              onlineEntrant.nextOfKinWorkPlace,
              onlineEntrant.nextOfKinWorkPost,
              onlineEntrant.nextOfKinPhone].grep().join(', ')

      return description
    }
    else
    {
      return ''
    }
  }

  /**
   * Вычисляет строку с данным олимпиады.
   * @return Название предмета (Название олимпиады, уровень, степень)
   */
  def getOlympiad()
  {
    def result = new StringBuilder();
    if (onlineEntrant.olympiadDiplomaTitle != null)
      result.append(onlineEntrant.olympiadDiplomaSubject)

    if ((onlineEntrant.olympiadDiplomaTitle != null) || (onlineEntrant.olympiadDiplomaType != null && !onlineEntrant.olympiadDiplomaType.code.equals(UniecDefines.DIPLOMA_TYPE_OTHER)) || (onlineEntrant.olympiadDiplomaDegree != null))
      result.append('( ')

    if (onlineEntrant.olympiadDiplomaTitle != null)
      result.append(onlineEntrant.olympiadDiplomaTitle);

    if (onlineEntrant.olympiadDiplomaType != null && !onlineEntrant.olympiadDiplomaType.code.equals(UniecDefines.DIPLOMA_TYPE_OTHER))
    {
      if (onlineEntrant.olympiadDiplomaTitle != null)
        result.append(', ');

      result.append(onlineEntrant.olympiadDiplomaType.title);
    }

    if (onlineEntrant.olympiadDiplomaDegree != null)
    {
      if (onlineEntrant.olympiadDiplomaTitle != null || (onlineEntrant.olympiadDiplomaType != null && !onlineEntrant.olympiadDiplomaType.code.equals(UniecDefines.DIPLOMA_TYPE_OTHER)))
        result.append(', ');

      result.append(onlineEntrant.olympiadDiplomaDegree.title);
    }

    if ((onlineEntrant.olympiadDiplomaTitle != null) || (onlineEntrant.olympiadDiplomaType != null && !onlineEntrant.olympiadDiplomaType.code.equals(UniecDefines.DIPLOMA_TYPE_OTHER)) || (onlineEntrant.olympiadDiplomaDegree != null))
      result.append(')')

    return result.toString();
  }


  def getBenefit()
  {
    def result = new StringBuilder()
    if (onlineEntrant.benefitDocument != null)
      result.append(onlineEntrant.benefitDocument);

    if (onlineEntrant.benefitDate != null)
      result.append(' от ').append(DateFormatter.DEFAULT_DATE_FORMATTER.format(onlineEntrant.benefitDate));

    return result.toString();
  }
}