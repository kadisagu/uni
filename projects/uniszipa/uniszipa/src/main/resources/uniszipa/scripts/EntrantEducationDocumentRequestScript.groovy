/* $Id$ */
package uniszipa.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant

return new EntrantEducationDocumentRequest(
        session: session,
        template: template,
        eduDocument: eduDocument,
        entrant: entrant
).print()
/**
 * @author Denis Katkov
 * @since 08.06.2016
 */

class EntrantEducationDocumentRequest {
    Session session
    byte[] template
    PersonEduDocument eduDocument
    EnrEntrant entrant

    def im = new RtfInjectModifier()

    def print() {
        RtfDocument document = new RtfReader().read(template)

        if (StringUtils.isNotEmpty(eduDocument.eduOrganization)) {
            im.put("eduOrgUnitTitle", eduDocument.eduOrganization)
            im.put("eduOrgUnitTitleUnderlined", eduDocument.eduOrganization)
        } else {
            im.put("eduOrgUnitTitle", "")
            im.put("eduOrgUnitTitleUnderlined", "________________________________")
        }
        im.put("documentIssueDate", eduDocument.issuanceDate != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.issuanceDate) : "___.___.______")
        im.put("documentType", eduDocument.eduDocumentKind.title)
        im.put("entrantFio", entrant.fullFio)

        im.modify(document)

        return [document: RtfUtil.toByteArray(document),
                fileName: "Запрос по документу об образовании - ${entrant.fio}.rtf"]
    }
}