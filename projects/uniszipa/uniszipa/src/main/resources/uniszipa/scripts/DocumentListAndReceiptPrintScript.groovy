/* $Id: DocumentListAndReceiptPrintScript.groovy 23062 2012-05-30 10:40:53Z azhebko $ */
// Copyright 2006-2012 Tandem Service Software
package uniszipa.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonBenefit
import org.tandemframework.shared.person.base.entity.PersonEduInstitution
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.util.EntrantDataUtil

return new DocumentListAndReceiptPrint(                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати описи и расписки абитуриента
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class DocumentListAndReceiptPrint
{
  Session session
  byte[] template
  EntrantRequest entrantRequest
  def im = new RtfInjectModifier()
  def tm = new RtfTableModifier()

  def print()
  {
    // получаем список выбранных направлений приема
    def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().entrant().id()} = ${entrantRequest.entrant.id}
                order by ${RequestedEnrollmentDirection.entrantRequest().regDate()}, ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()

    fillRequestedDirectionList(directions)
    fillInjectParameters(directions[0])

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(template, im, tm),
            fileName: "Расписка абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
  }

  void fillRequestedDirectionList(List<RequestedEnrollmentDirection> directions)
  {
    def fpd = directions.get(0);

    def query = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, 'd').add(MQExpression.in("d", "id", directions.collect { it.id }))
    def dataUtil = new EntrantDataUtil(session, fpd.entrantRequest.entrant.enrollmentCampaign, query)

    def benefits = getBenefits(fpd.entrantRequest.entrant.person);

    // заполняем блок с выбранными направлениями приема
    def i = 1
    String[][] directionData = directions.collect {
      def ou = it.enrollmentDirection.educationOrgUnit
      def hs = ou.educationLevelHighSchool;
      def chosen = dataUtil.getChosenEntranceDisciplineSet(it).sort({it.id});
      def sum = chosen.collect({it.finalMark == null ? 1 : 0}).sum() == 0 ? chosen.collect({it.finalMark}).sum() : null
      [
              i++,
              getCode(it),
              it.entrantRequest.stringNumber,
              DateFormatter.DEFAULT_DATE_FORMATTER.format(it.entrantRequest.regDate),
              sum,
              it.competitionKind.code.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) ? benefits : "",
              it.competitionKind.code.equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION) ? "" : it.competitionKind.title]
    }
    tm.put('T', directionData)
  }

  String getCode(RequestedEnrollmentDirection direction)
  {
    def ou = direction.enrollmentDirection.educationOrgUnit
    return ([ou.educationLevelHighSchool.shortTitle,
            ou.developForm.shortTitle,
            ou.developCondition.code.equals(UniDefines.DEVELOP_CONDITION_FULL_TIME) ? "" : ou.developCondition.shortTitle,
            direction.compensationType.shortTitle]).grep().join('-')
  }

  void fillInjectParameters(RequestedEnrollmentDirection firstPriorityDirection)
  {
    def entrant = entrantRequest.entrant
    def person = entrant.person
    def card = person.identityCard
    def lastEduInstitution = person.personEduInstitution

    im.put('lastName', card.lastName)
    im.put('firstName', card.firstName)
    im.put('middleName', card.middleName)
    im.put('passport', card.title)
    im.put('certificate', getCertificate(lastEduInstitution))
  }

  def getCertificate(PersonEduInstitution personEduInstitution)
  {
      return personEduInstitution ? [personEduInstitution.documentType.title,
              personEduInstitution.educationLevel?.title,
              [[personEduInstitution.seria, personEduInstitution.number].grep().join(' '), DateFormatter.DEFAULT_DATE_FORMATTER.format(personEduInstitution.issuanceDate)].grep().join(" от ")
      ].grep().join(', ') : null;
  }

  def getBenefits(Person person)
  {
    def benetifs = DQL.createStatement(session, /
                select ${PersonBenefit.benefit().title()}
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                order by ${PersonBenefit.benefit().title()}
        /).<String> list()

    return benetifs.empty ? 'Льгот нет' : benetifs.join(', ')
  }
}
