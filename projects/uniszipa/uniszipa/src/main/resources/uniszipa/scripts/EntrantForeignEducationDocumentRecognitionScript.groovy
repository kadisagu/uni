package uniszipa.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument

return new EntrantForeignEducationDocumentRecognition(
        session: session,
        template: template,
        eduForeignDocument: DataAccessServices.dao().get(SzipaForeignEducationDocument.class, eduForeignDocumentId),
        entrant: entrant
).print()

/**
 * @author Igor Belanov
 * @since 15.07.2016
 */
class EntrantForeignEducationDocumentRecognition
{
    Session session
    private byte[] template
    private SzipaForeignEducationDocument eduForeignDocument
    private EnrEntrant entrant

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        RtfDocument rtfDocument = new RtfReader().read(template)

        def isMale = entrant.person.identityCard.sex.isMale()

        // заполнение данных абитуриента
        im.put('lastName', entrant.person.identityCard.lastName)
        im.put('firstName', entrant.person.identityCard.firstName)
        im.put('middleName', entrant.person.identityCard.middleName)
        im.put('sex', entrant.person.identityCard.sex.title)
        im.put('birthDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.person.identityCard.birthDate))
        im.put('birthPlace', entrant.person.identityCard.birthPlace)
        im.put('passportCitizenship', entrant.person.identityCard.citizenship.title)
        im.put('passport', entrant.person.identityCard.cardType.title)
        im.put('pSeria', entrant.person.identityCard.seria)
        im.put('pNumber', entrant.person.identityCard.number)
        im.put('passportIssuanceDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.person.identityCard.issuanceDate))
        im.put('passportIssuancePlace', entrant.person.identityCard.issuancePlace)

        // заполнение прочих данных
        PersonEduDocument eduDocument = eduForeignDocument.personEduDocument
        im.put('eduDocumentTitle', eduDocument.documentKindTitle)
        im.put('eduDocumentSeria', eduDocument.seria)
        im.put('eduDocumentNumber', eduDocument.number)
        im.put('eduDocumentDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.creationDate))
        im.put('numberOfPages', eduForeignDocument.numberOfPages != null ? eduForeignDocument.numberOfPages : "__________")
        im.put('eduDocumentFinishYear', Integer.toString(eduDocument.yearEnd))
        im.put('eduDocumentOrgUnit', eduDocument.eduOrganization)
        im.put('eduDocumentCountry', eduDocument.eduOrganizationAddressItem.country.title)
        im.put('fio', eduForeignDocument.fio)
        im.put('educationPeriod', eduForeignDocument.educationPeriod)
        im.put('finished', isMale ? 'Окончил' : 'Окончила')
        im.put('familiar', isMale ? 'ознакомлен' : 'ознакомлена')
        im.put('prevent', isMale ? 'предупреждён' : 'предупреждена')
        im.put('date', DateFormatter.DEFAULT_DATE_FORMATTER.format(eduForeignDocument.createDate))

        // заполнение дополнений
        final int DEFAULT_NUMBER_OF_ROWS = 5
        def attachmentsList = new ArrayList<String[]>()
        int i = 0
        if (eduForeignDocument.attachments != null)
        {
            String[] attachments = eduForeignDocument.attachments.split('\n')
            for (String attachment : attachments)
                attachmentsList.add([++i, attachment] as String[])
        }
        if (attachmentsList.size() < DEFAULT_NUMBER_OF_ROWS) {
            def numberOfEmptyRows = DEFAULT_NUMBER_OF_ROWS - attachmentsList.size()
            for (int j = 0; j < numberOfEmptyRows; ++j)
                attachmentsList.add([++i, ''] as String[])
        }
        tm.put('T', attachmentsList as String[][])

        im.modify(rtfDocument)
        tm.modify(rtfDocument)

        return [document: RtfUtil.toByteArray(rtfDocument),
                fileName: "Заявление абитуриента на признание иностранного образования ${entrant.fio}.rtf"]
    }
}