package uniszipa.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.fias.base.entity.AddressInter
import org.tandemframework.shared.fias.base.entity.AddressRu
import org.tandemframework.shared.fias.base.entity.AddressString
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.*
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition

return new EnrEntrantStudentFormPrint(
        session: session,
        template: template,
        reqComp: session.get(EnrRequestedCompetition.class, object)
).print()
/**
 * @author Denis Perminov
 * @since 19.08.2014
 */
class EnrEntrantStudentFormPrint {
    Session session
    byte[] template
    EnrRequestedCompetition reqComp

    def im = new RtfInjectModifier()

    def print() {
        RtfDocument document = new RtfReader().read(template)

        EnrEntrantRequest request = reqComp.request
        EnrEntrant entrant = request.entrant
        Person person = entrant.person
        IdentityCard idCard = person.identityCard

        im.put("lastName", idCard.lastName)
        im.put("firstName", idCard.firstName)
        im.put("middleName", idCard.middleName)
        im.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(idCard.birthDate))
        im.put("birthPlace", idCard.birthPlace)
        im.put("sex", idCard.sex.title)
        im.put("citizenship", idCard.citizenship?.title)
        im.put("familyStatus", person.familyStatus?.title)

        def personBenefits = DQL.createStatement(session, /
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                /).<PersonBenefit> list()

        if (!personBenefits.empty)
            im.put("benefits", personBenefits.collect { e -> e.benefit.title }.toSet().join(", "))
        else
            im.put("benefits", "")

        im.put("seria", idCard.seria)
        im.put("number", idCard.number)
        im.put("issuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(idCard.issuanceDate))
        im.put("issuancePlace", idCard.issuancePlace)

        if ((null != idCard.address) && (idCard.address instanceof AddressRu)) {
            def address = (AddressRu) idCard.address
            im.put("regPostCode", address.inheritedPostCode)
            im.put("regCountry", address.country?.title)
            im.put("regRegion", getRegion((AddressRu) idCard.address))
            im.put("regDistrict", address.district?.title)
            im.put("regSettlement", address.settlement?.title)
            im.put("regAddress", address.shortTitleWithFlat)
        } else if ((null != idCard.address) && (idCard.address instanceof AddressInter)) {
            def address = (AddressInter) idCard.address
            im.put("regPostCode", address.postCode)
            im.put("regCountry", address.country?.title)
            im.put("regRegion", address.region)
            im.put("regDistrict", address.district)
            im.put("regSettlement", address.settlement?.title)
            im.put("regAddress", address.shortTitleWithFlat)
        } else if ((null != idCard.address) && (idCard.address instanceof AddressString)) {
            im.put("regPostCode", "")
            im.put("regCountry", "")
            im.put("regRegion", "")
            im.put("regDistrict", "")
            im.put("regSettlement", "")
            im.put("regAddress", idCard.address as String)
        } else {
            im.put("regPostCode", "")
            im.put("regCountry", "")
            im.put("regRegion", "")
            im.put("regDistrict", "")
            im.put("regSettlement", "")
            im.put("regAddress", "")
        }
        im.put("homePhoneNumber", person.contactData.phoneReg)

        if ((null != person.address) && (person.address instanceof AddressRu)) {
            def address = (AddressRu) person.address
            im.put("postCode", address.inheritedPostCode)
            im.put("country", address.country?.title)
            im.put("region", getRegion((AddressRu) person.address))
            im.put("district", address.district?.title)
            im.put("settlement", address.settlement?.title)
            im.put("address", address.shortTitleWithFlat)
        } else if ((null != person.address) && (person.address instanceof AddressInter)) {
            def address = (AddressInter) person.address
            im.put("postCode", address.postCode)
            im.put("country", address.country?.title)
            im.put("region", address.region)
            im.put("district", address.district)
            im.put("settlement", address.settlement?.title)
            im.put("address", address.shortTitleWithFlat)
        } else if ((null != person.address) && (person.address instanceof AddressString)) {
            im.put("postCode", "")
            im.put("country", "")
            im.put("region", "")
            im.put("district", "")
            im.put("settlement", "")
            im.put("address", person.address as String)
        } else {
            im.put("postCode", "")
            im.put("country", "")
            im.put("region", "")
            im.put("district", "")
            im.put("settlement", "")
            im.put("address", "")
        }
        im.put("mobilePhoneNumber", person.contactData.phoneMobile)

        def eduDocument = request.eduDocument
        im.put("education", eduDocument.eduLevel?.title)
        im.put("year", eduDocument.yearEnd as String)
        im.put("certificate", eduDocument.documentKindTitle)
        im.put("SerCert", eduDocument.seria)
        im.put("NumCert", eduDocument.number)
        im.put("OutputCert", eduDocument.eduOrganisationWithAddress)

        im.put("eduInstitutionKind", eduDocument.eduOrganization)
        im.put("docType", eduDocument.documentKindTitle)
        im.put("graduationHonour", eduDocument.graduationHonour?.title)
        im.put("docSeria", eduDocument.seria)
        im.put("docNumber", eduDocument.number)
        im.put("docIssuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.issuanceDate))
        im.put("docIssuancePlace", eduDocument.eduOrganisationWithAddress)
        im.put("employeeSpeciality", eduDocument.eduProgramSubject)

        def languageList = DQL.createStatement(session, /
                select ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.main()} desc, ${PersonForeignLanguage.language().title()}
                /).<String> list()

        im.put("language", languageList.join(", "))

        im.put("workPlace", person.workPlace)
        im.put("workPlacePosition", person.workPlacePosition)

        def nextOfKin = getNextOfKin(person, RelationDegreeCodes.FATHER)
        if (nextOfKin) {
            im.put("fatherLastName", nextOfKin.lastName)
            im.put("fatherFirstName", nextOfKin.firstName)
            im.put("fatherMiddleName", nextOfKin.middleName)
            im.put("fatherBirthYear", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(nextOfKin.birthDate))
            im.put("fatherWorkPlace", nextOfKin.employmentPlace)
            im.put("fatherWorkPlacePosition", nextOfKin.post)
            im.put("fatherPhone", nextOfKin.phones)
        } else {
            im.put("fatherLastName", "")
            im.put("fatherFirstName", "")
            im.put("fatherMiddleName", "")
            im.put("fatherBirthYear", "")
            im.put("fatherWorkPlace", "")
            im.put("fatherWorkPlacePosition", "")
            im.put("fatherPhone", "")
        }

        nextOfKin = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        if (nextOfKin) {
            im.put("motherLastName", nextOfKin.lastName)
            im.put("motherFirstName", nextOfKin.firstName)
            im.put("motherMiddleName", nextOfKin.middleName)
            im.put("motherBirthYear", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(nextOfKin.birthDate))
            im.put("motherWorkPlace", nextOfKin.employmentPlace)
            im.put("motherWorkPlacePosition", nextOfKin.post)
            im.put("motherPhone", nextOfKin.phones)
        } else {
            im.put("motherLastName", "")
            im.put("motherFirstName", "")
            im.put("motherMiddleName", "")
            im.put("motherBirthYear", "")
            im.put("motherWorkPlace", "")
            im.put("motherWorkPlacePosition", "")
            im.put("motherPhone", "")
        }

        PersonMilitaryStatus military = DataAccessServices.dao().get(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, person)
        if (null != military) {
            def militaryOffice = military?.militaryOffice
            if (null != militaryOffice ) {
                boolean incomer = !militaryOffice.settlement.equals(TopOrgUnit.instance.address.settlement)
                im.put("militaryOffice", !incomer ? militaryOffice.titleWithAddress : "")
                im.put("incomerMilitaryOffice", incomer ? militaryOffice.titleWithAddress : "")
            } else {
                im.put("militaryOffice", "")
                im.put("incomerMilitaryOffice", "")
            }
        } else {
            im.put("militaryOffice", "")
            im.put("incomerMilitaryOffice", "")
        }

        EnrCompetition competition = reqComp.competition
        im.put("isContract", (competition.type.compensationType.budget ? "Нет" : "Да"))
        im.put("isTargetAdmission", competition.targetAdmission ? "Да" : "Нет")

        im.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))

/**
        EnrEnrollmentExtract extract = DataAccessServices.dao().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.L_ENTITY, reqComp)
        if (null != extract) {
            def enrollOrder = extract?.paragraph?.order
            if (null != enrollOrder) {
                im.put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(enrollOrder?.commitDate))
                im.put("orderNumber", enrollOrder?.number)
            } else {
                im.put("commitDate", "")
                im.put("orderNumber", "")
            }
        } else {
            im.put("commitDate", "")
            im.put("orderNumber", "")
        }
*/
        im.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Анкета для зачисленных абитуриентов ${idCard.fullFio}.rtf"]
    }

    static String getRegion(AddressRu address) {
        def region = ""
        def parent = address?.settlement
        while (parent != null) {
            region = parent.title
            parent = parent.parent
        }
        return region
    }

    PersonNextOfKin getNextOfKin(Person person, String code) {
        return DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${code}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()
    }
}