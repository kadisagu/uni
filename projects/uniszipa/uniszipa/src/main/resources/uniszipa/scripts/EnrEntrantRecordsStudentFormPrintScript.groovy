/* $Id$ */
package uniszipa.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.RtfSettings
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition

return new EnrEntrantRecordsStudentFormPrint(
        session: session,
        template: template,
        reqComp: session.get(EnrRequestedCompetition.class, object)
).print()
/**
 * @author Denis Katkov
 * @since 30.05.2016
 */
class EnrEntrantRecordsStudentFormPrint {
    Session session
    byte[] template
    EnrRequestedCompetition reqComp

    def im = new RtfInjectModifier()

    def print() {
        RtfDocument document = new RtfReader().read(template)
        RtfSettings settings = document.getSettings()

        //скрипт меняет отступы, приходится вручную выставлять
        settings.marginBottom = 1133
        settings.marginTop = 1133
        settings.marginLeft = 1700
        settings.marginRight = 850

        EnrEntrantRequest request = reqComp.request

        //выписка по выбранному конкурсу
        def enrEnrollmentExtract = DQL.createStatement(session, /
        from ${EnrEnrollmentExtract.class.simpleName}
        where ${EnrEnrollmentExtract.entity().id()}=${reqComp.id}
        /).<EnrEnrollmentExtract> uniqueResult()

        im.put("FIO", request.entrant.person.fullFio)
        im.put("orderNumber", enrEnrollmentExtract.order.number)
        im.put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(enrEnrollmentExtract.order.commitDate))
        im.put("orderDateStartFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(enrEnrollmentExtract.order.actionDate))
        im.put("course", enrEnrollmentExtract.student != null ? enrEnrollmentExtract.student.course.title : "")

        im.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Печать описей и выписок для зачисленных абитуриентов.rtf"]
    }
}