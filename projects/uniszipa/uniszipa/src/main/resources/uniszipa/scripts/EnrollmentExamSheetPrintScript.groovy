/**
 * $Id:$
 */
// Copyright 2006-2012 Tandem Service Software

package uniszipa.scripts

import org.apache.commons.collections.keyvalue.MultiKey
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.field.RtfField
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfGroup
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.util.EntrantDataUtil

return new EnrollmentExamSheetPrint(                              // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати экзаменационного листа абитуриента (СЗИ РАНХиГС)
 *
 * @author Alexander Shaburov
 * @since 06.06.12
 */
class EnrollmentExamSheetPrint
{
  byte[] template
  Session session
  EntrantRequest entrantRequest
  def modifier = new RtfInjectModifier()

  /**
   * Основной метод печати.
   * @return <code>[document: byte[], fileName: String]</code>
   */
  def print()
  {
    def entrant = entrantRequest.entrant
    def card = entrant.person.identityCard

    def enrollmentDirectionList = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().entrant().id()} = ${entrantRequest.entrant.id}
                order by ${RequestedEnrollmentDirection.priority()}
    /).<RequestedEnrollmentDirection>list();

    modifier.put('lastName', card.lastName);
    modifier.put('firstName', card.firstName);
    modifier.put('middleName', card.middleName);

    modifier.put('entrantNumber', entrant.personalNumber);
    modifier.put('grNumRegNum', getGrNumRegNum(enrollmentDirectionList))

    def passport = new StringBuilder();
    passport.append(card.cardType.title).append(': ').append(card.seria).append(' ').append(card.number).append('. ')
            .append('Выдан: ').append(card.issuancePlace).append('. ')
            .append('Дата выдачи: ').append(DateFormatter.DEFAULT_DATE_FORMATTER.format(card.issuanceDate)).append(' г.');

    modifier.put('passport', passport.toString());

    if (entrant.person.personEduInstitution != null)
    {
      modifier.put('eduInstitution', entrant.person.personEduInstitution.eduInstitutionTitle);
      modifier.put('edEndYear', String.valueOf(entrant.person.personEduInstitution.yearEnd));

      def certificate = new StringBuilder();
      certificate.append(entrant.person.personEduInstitution.documentType.title).append(', ')
              .append(entrant.person.personEduInstitution.educationLevel.title).append(', ')
              .append(entrant.person.personEduInstitution.seria != null ? entrant.person.personEduInstitution.seria : '').append(' ').append(entrant.person.personEduInstitution.number != null ? entrant.person.personEduInstitution.number : '');

      modifier.put('certificate', certificate.toString());

      if (entrant.person.personEduInstitution.employeeSpeciality != null)
        modifier.put('speciality', entrant.person.personEduInstitution.employeeSpeciality.title)
      else
        modifier.put('speciality', '')
    }
    else
    {
      def emptyString = '';
      modifier.put('eduInstitution', emptyString);
      modifier.put('edEndYear', emptyString);
      modifier.put('certificate', emptyString);
      modifier.put('speciality', emptyString)
    }

    RtfDocument document = new RtfReader().read(template);

    modifier.modify(document);

    prepareEntranceDisciplineTable(entrant, document)

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(document),
            fileName: "Экзаменационный лист абитуриента ${entrant.person.identityCard.fullFio}.rtf"]
  }

  /**
   * Вычисляет метку с названиями экз. групп и номерами заявлений абитуриента.
   * @param enrollmentDirectionList выбранные направления подготовки абитуриента
   * @return Строка метки
   */
  def static getGrNumRegNum(List<RequestedEnrollmentDirection> enrollmentDirectionList)
  {
    def title = new ArrayList<String>();
    def dataMap = new HashMap<String, Set<String>>();

    for (def direction : enrollmentDirectionList)
    {
      def reqList = dataMap.get(direction.group)

      if (reqList == null)
        reqList = new HashSet<String>();
      if (direction.entrantRequest.stringNumber != null)
        reqList.add(direction.entrantRequest.stringNumber);


      dataMap.put(direction.group, reqList);
    }

    for (def entry : dataMap.entrySet())
    {
      title.add(entry.key + (entry.value.isEmpty() ? '' : ' (' + StringUtils.join(entry.value, ", ") + ')'));
    }

    return StringUtils.join(title, ", ");
  }

  /**
   * Подготавливает и заполняет табличную метку с Выбранными вступительными испытаними.
   * @param entrant абитуриент
   * @param enrollmentDirectionList список Выбранных направлений приема
   */
  def prepareEntranceDisciplineTable(def entrant, RtfDocument document)
  {
    def builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d")
            .add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().s(), entrant));

    def dataUtil = new EntrantDataUtil(session, entrant.enrollmentCampaign, builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS)

    def uniqueList = getUniqueChosenList(dataUtil)

    //нет уникальных комбинаций выбранных вступительных испытаний
    if (uniqueList.isEmpty())
    {
      new RtfTableModifier().put("T", new String[1][]).modify(document);

      return
    }

    // ищем в шаблоне элемент, соответствующий таблице с оценками
    def emptyMarkTable = null;
    // содержащий его список элементов
    def tableElementList = null;
    // и позицию элемента в этом списке
    int index = 0;
    boolean found = false;
    def sourceList = new ArrayList<List<IRtfElement>>();
    sourceList.add(document.getElementList());
    def newSourceList = new ArrayList<List<IRtfElement>>();
    while (!sourceList.isEmpty() && !found)
    {
      for (def elementList : sourceList)
      {
        for (int i = 0; i < elementList.size(); i++)
        {
          def element = elementList.get(i);
          if (element instanceof IRtfGroup)
            newSourceList.add(((IRtfGroup) element).elementList);
          if (element instanceof RtfTable)
          {
            def table = (RtfTable) element;
            for (int currentRowIndex = 0; currentRowIndex < table.rowList.size(); currentRowIndex++)
            {
              def currentRow = table.rowList.get(currentRowIndex);
              def rowName = getRowName(currentRow);
              if ('T'.equals(rowName))
              {
                emptyMarkTable = table.clone;
                tableElementList = elementList;
                index = i;
                found = true;
              }
            }
          }
        }
      }
      if (!found)
      {
        sourceList.clear();
        sourceList.addAll(newSourceList);
        newSourceList.clear();
      }
    }

    // не нашли
    if (!found)
      return document;

    // для каждого уникального набора Выбранных вступительных испытаний печатаем свою таблицу с оценками
    def iterator = uniqueList.iterator();
    while (iterator.hasNext())
    {
      def chosenEntranceDisciplineList = iterator.next();
      def tableData = new ArrayList<String[]>();
      int num = 1;
      // общее число баллов показываем только в том случае, если есть оценки по всем предметам
      boolean showPointsTotal = true;
      def pointsTotal = 0l;
      for (def chosen : chosenEntranceDisciplineList)
      {
        def finalMark = ''
        def finalMarkStr = ''
        def source = ''
        def date
        if (chosen.finalMark != null)
        {
          // округляем, запоминаем итоговую оценку, записываем число прописью, считаем сумму баллов в таблице
          def mark = Math.round(chosen.finalMark);
          finalMark = String.valueOf(mark);
          finalMarkStr = NumberSpellingUtil.spellNumberMasculineGender(mark);
          pointsTotal = pointsTotal + mark;
        }
        else
        {
          showPointsTotal = false;
        }

        //форма сдачи
        if (chosen.finalMarkSource != null && (UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 == chosen.finalMarkSource || UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2 == chosen.finalMarkSource))
        {
          source = 'ЕГЭ'
        }
        else
        {
          source = dataUtil.getExamPassDisciplineSet(chosen).iterator().hasNext() ? dataUtil.getExamPassDisciplineSet(chosen).iterator().next().subjectPassForm.title : '';
        }

        //дата сдачи, может иметься если внутренний тип дисциплины
        date = dataUtil.getExamPassDisciplineSet(chosen).iterator().hasNext() ? dataUtil.getExamPassDisciplineSet(chosen).iterator().next().passDate : null;

        //заполняем строчку
        tableData.add([String.valueOf(num++), chosen.enrollmentCampaignDiscipline.title + " (${source})", DateFormatter.DEFAULT_DATE_FORMATTER.format(date), finalMark, finalMarkStr] as String[]);
      }

      //простовляем табличную метку
      new RtfTableModifier().put('T', tableData as String[][]).modify(document);

      //простовляем итоговый бал, если это необходимо
      if (showPointsTotal)
      {
        new RtfInjectModifier().put('pointsTotal', String.valueOf(pointsTotal) + ' (' + NumberSpellingUtil.spellNumberMasculineGender(pointsTotal) + ')').modify(document);
      }
      else
      {
        new RtfInjectModifier().put('pointsTotal', '').modify(document);
      }

      // клонируем таблицу с метками, если она еще будет нужна на следующей итерации
      if (iterator.hasNext())
        tableElementList.add(++index, emptyMarkTable.clone);
    }

    return document;
  }

  /**
   * Вычисляет уникальные наборы Выбранных вступительных испытаний по всем направлениям приема во всех зявлениях абитурьента.
   * @param dataUtil <code>EntrantDataUtil</code>
   * @return Список со списком уникальных наборов Выбранных вступительных испытаний.
   */
  def static getUniqueChosenList(def dataUtil)
  {
    def used = new HashSet<MultiKey>();
    def result = new ArrayList<List<ChosenEntranceDiscipline>>();
    for (def direction : dataUtil.getDirectionSet())
    {
      def chosenList = new ArrayList<ChosenEntranceDiscipline>(dataUtil.getChosenEntranceDisciplineSet(direction));
      Collections.sort(chosenList, new Comparator(){
        public int compare(def o1, def o2)
        {
          return o1.enrollmentCampaignDiscipline.title.compareTo(o2.enrollmentCampaignDiscipline.title);
        }
      });

      def idList = new ArrayList<Long>();
      for (def chosen : chosenList)
        idList.add(chosen.enrollmentCampaignDiscipline.id);

      if (!idList.isEmpty())
      {
        def id = new MultiKey(idList.toArray(new Object[idList.size()]));
        if (!used.contains(id))
        {
          used.add(id);
          result.add(chosenList);
        }
      }
    }

    return result;
  }

  /**
   * Ищет "имя" строки - метку в первой ячейке
   * @param row строка таблицы
   * @return метка
   */
  def static getRowName(def row)
  {
    def cellList = row.cellList;

    if (cellList.size() == 0)
      throw new RuntimeException("Invalid document: Row has no cells!");

    for (def element : cellList.get(0).elementList)
      if (element instanceof RtfField)
      {
        def field = (RtfField) element;
        if (field.mark)
          return field.fieldName;
      }
    return null;
  }
}



