package uniszipa.scripts

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.SharedRtfUtil
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOriginalSubmissionAndReturnWayCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintBS(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Denis Perminov
 * @since 30.06.2014
 */
class EntrantRequestPrintBS {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    RtfDocument document;
    def deleteLabels = Lists.newArrayList()
    def List<EnrRequestedCompetition> requestedCompetitions

    def print() {
        document = new RtfReader().read(template)
        fillCompetitions(); //направления
        fillInjectParameters()
        fillCompetitionExclusive(requestedCompetitions) // Особые права, преимущественное право
        fillEnrEntrantStateExam() //ЕГЭ
        fillDDetail(requestedCompetitions) //экзамены
        fillAchievements() //инд.достижения
        fillOlympiads() //таблица с олимпиадами
        fillAcceptedContract() //согласие на зачисление

        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    /** Таблицы с выбранными конкурсами (обычное и параллельное обучение) */
    void fillCompetitions() {
        final simpleRows = new ArrayList<String[]>()
        final additionalRows = new ArrayList<String[]>()
        int i=0
        for (requestedCompetition in getRequestedCompetitions())
        {
            def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
            final row = [++i as String,
                         subject.subjectCode,
                         subject.title,
                         requestedCompetition.competition.programSetOrgUnit.orgUnit.institutionOrgUnit.orgUnit.shortTitle,
                         requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                         getPlaces(requestedCompetition)]
            simpleRows.add(row as String[])
            final addRow = [i,
                            subject.subjectCode,
                            subject.title,
                            getProgramSubjectStr(requestedCompetition),
                            requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                            requestedCompetition.priority as String
                            ]
            additionalRows.add(addRow as String[])
        }

        this.tm.put('T', simpleRows as String[][])
        tm.put('T6', additionalRows as String[][])
    }

    /** @return список выбранных конкурсов по зявлению, сортировка по приоритету */
    def List<EnrRequestedCompetition> getRequestedCompetitions() {

        if (this.requestedCompetitions == null) {
            this.requestedCompetitions = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, 'e')
                    .column(property('e'))
                    .where(eq(property('e', EnrRequestedCompetition.request()), value(this.entrantRequest)))
                    .order(property('e', EnrRequestedCompetition.priority()), OrderDirection.asc)
                    .createStatement(this.session).<EnrRequestedCompetition>list()
        }
        return requestedCompetitions
    }

    /** ОП приема, факультет */
    def String getProgramSubjectStr(EnrRequestedCompetition requestedCompetition)
    {
        return requestedCompetition.competition.programSetOrgUnit.programSet.title
    }

    /** @return "на места ..." */
    static String getPlaces(EnrRequestedCompetition requestedCompetition) {
        def String str
        switch (requestedCompetition.competition.type.code)
        {
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
            case EnrCompetitionTypeCodes.CONTRACT:
                str = 'по договорам об оказании платных образовательных услуг'
                break
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
            case EnrCompetitionTypeCodes.MINISTERIAL:
                str = 'финансируемые из федерального бюджета'
                break
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                str = 'в пределах квоты целевого приема'
                break
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                str = 'в пределах квоты приема лиц, имеющих особое право'
                break
            default:
                str = ''
        }
        if (requestedCompetition instanceof EnrRequestedCompetitionTA) {
            // Если конкурс по ЦП, дописываем название вида ЦП в скобках
            str += ' (' + (requestedCompetition as EnrRequestedCompetitionTA).targetAdmissionKind.targetAdmissionKind.title + ')'
        }
        return str
    }

    void fillInjectParameters() {
        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = entrantRequest.identityCard
        def sex = card.sex
        def personAddress = person.address

        //вид компенсации затрат
        List<EnrRequestedCompetition> reqList = getRequestedCompetitions()
        List<String> compTypeList = reqList.collect {e -> e.competition.type.compensationType.code}
        StringBuilder compTypeTextBuilder = new StringBuilder()
        if (compTypeList.contains(UniDefines.COMPENSATION_TYPE_BUDGET))
            compTypeTextBuilder.append("за счёт бюджетных ассигнований федерального бюджета")

        if (compTypeList.contains(UniDefines.COMPENSATION_TYPE_CONTRACT))
            compTypeTextBuilder.append(compTypeTextBuilder.size() >0? " и " : "").append("по договорам об образовании, заключаемым при приеме на обучение за счёт средств физических и (или) юридических лиц")

        im.put('compensationType', compTypeTextBuilder.toString())

        //базовое образование
        EnrEduLevelRequirement baseEduLevel = reqList.get(0).competition.eduLevelRequirement
        InflectorVariant variant = IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE);
        String eduLevelRequirementGenetiveTitle = DeclinableManager.instance().dao().getPropertyValue(baseEduLevel, EnrEduLevelRequirement.P_TITLE, variant);
        im.put('eduBase', baseEduLevel.code.equals(EnrEduLevelRequirementCodes.NO)? "cреднего общего образования" : eduLevelRequirementGenetiveTitle)

        im.put("requestNumber", entrantRequest.stringNumber)
        im.put("regDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT.format(entrantRequest.regDate) + " года");

        im.put("lastName", card.lastName)
        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("sex", sex.male ? "М" : "Ж")
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("birthPlace", card.birthPlace)

        im.put("passportCitizenship", card.citizenship.title)
        im.put("passport", card.cardType.title + ", " + (null != card.seria ? card.seria : "") + " " + (null != card.number ? card.number : ""))
        im.put("passportIssuanceDate", card.issuanceDate?.format("dd.MM.yyyy"))
        im.put("passportIssuancePlace", card.issuancePlace)

        im.put("a", sex.male ? "ий" : "ая")
        im.put("registrationAddress", card.address?.titleWithFlat)
        im.put("address", personAddress?.titleWithFlat)
        im.put("phoneNumbers", person.contactData.mainPhones)
        im.put("email", person.email)

        //данные об образовании
        def eduDocument = entrantRequest.eduDocument
        im.put("edEndYear", eduDocument.yearEnd as String)
        im.put("eduInstitution", eduDocument.eduOrganisationWithAddress)
        im.put("certificate", eduDocument.documentKindTitle)
        im.put("edSeria", eduDocument?.seria)
        im.put("edNumber", eduDocument?.number)
        im.put("edIssuanceDate", eduDocument?.issuanceDate?.format("dd.MM.yyyy"))
        im.put("certificateDetail", eduDocument?.graduationHonour?.title)

        def foreignLanguageList = DQL.createStatement(session, /
                select distinct ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                and ${PersonForeignLanguage.main()}=${true}
                /).<String> list()

        im.put("foreignLanguages", foreignLanguageList.join(", "))

        //родственники
        im.put("father", getNextOfKin(person, RelationDegreeCodes.FATHER))
        im.put("mother", getNextOfKin(person, RelationDegreeCodes.MOTHER))

        im.put("f", sex.male ? "" : "а")

        //спец. условия прохождения ВИ
        im.put('sr', '')
        im.put('nsr', '')
        im.put((entrant.isNeedSpecialExamConditions()? 'sr' : 'nsr'), 'x')

        //Способ возврата
        im.put('r1', '')
        im.put('r2', '')
        im.put('r3', '')
        switch(entrantRequest.originalReturnWay.code)
        {
            case EnrOriginalSubmissionAndReturnWayCodes.LICHNO:
                im.put('r1', 'x')
                break
            case EnrOriginalSubmissionAndReturnWayCodes.PO_DOVERENNOSTI:
                im.put('r2', 'x')
                break
            case EnrOriginalSubmissionAndReturnWayCodes.PO_POCHTE:
                im.put('r3', 'x')
                break;
        }

        // нуждается ли в общежитии
        im.put('h', person.needDormitory ? 'x' : '')
        im.put('nh', person.needDormitory? '' : 'x')

        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(card, GrammaCase.NOMINATIVE))
        im.put('shortFIO', person.fio)
    }

    static getProgramFormDeclinate(EduProgramForm programForm) {
        switch(programForm.code) {
            case DevelopFormCodes.FULL_TIME_FORM:
                return "очную"
            case DevelopFormCodes.CORESP_FORM:
                return "заочную"
            case DevelopFormCodes.PART_TIME_FORM:
                return "очно-заочную"
            case DevelopFormCodes.EXTERNAL_FORM:
                return "экстернат"
            case DevelopFormCodes.APPLICANT_FORM:
                return "самостоятельную"
        }
        return ""
    }

    String getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,[nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null
    }

    /** Результаты ЕГЭ */
    void fillEnrEntrantStateExam() {
        final stateExamMarks = new DQLSelectBuilder().fromEntity(EnrEntrantStateExamResult.class, 'ser')
                .column(property('ser'))
                .where(exists(EnrEntrantMarkSourceStateExam.class,
                EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().s(), getRequestedCompetitions(),
                EnrEntrantMarkSourceStateExam.stateExamResult().s(), property('ser')
        ))
                .order(property('ser', EnrEntrantStateExamResult.subject().title()))
                .createStatement(this.session).<EnrEntrantStateExamResult>list()
        if (stateExamMarks.isEmpty())
        {
            tm.remove('T1')
            im.put('eg', "")
        }
        else
        {
            tm.put('T1', stateExamMarks.collect { e ->[e.getSubject().getTitle(),e.getMark()]} as String[][])
            im.put('eg', 'x')
        }
    }


    /**
     * Особые права, преимущественные права
     */
    def fillCompetitionExclusive(List<EnrRequestedCompetition> enrRequestedCompetitions)
    {
        def rows = Lists.newArrayList()
        //конкурс по квоте
        def exclusiveCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }
        if (!exclusiveCompetitions.isEmpty())
        {
//            def benefitCategories = exclusiveCompetitions.collect { e -> e.benefitCategory }.toSet().toList().sort()
            Map<Long, List<IEnrEntrantBenefitProofDocument>> benefiteDocMap = getBenefitProofDocuments(exclusiveCompetitions)


            int i = 0
            for (EnrRequestedCompetitionExclusive item : exclusiveCompetitions)
            {
                // формируем строку из 4-x столбцов
                String[] row = new String[4]
                row[0] = ++i as String
                row[1] = item.benefitCategory.title
                row[2] = benefiteDocMap.get(item).collect { e -> e.documentType.title }.join(', ')
                row[3] = UniStringUtils.join(benefiteDocMap.get(item.id), IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE, ", ")
                rows.add(row)
            }
        }

        // конкурсы без ВИ
        def noExamsCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }
        if (!noExamsCompetitions.isEmpty())
        {

//            def benefitCategories = noExamsCompetitions.collect { e -> e.benefitCategory }.toSet().toList().sort()
            Map<Long, List<IEnrEntrantBenefitProofDocument>> benefiteDocMap = getBenefitProofDocuments(noExamsCompetitions)


            int i = 0
            for (EnrRequestedCompetitionNoExams item : noExamsCompetitions)
            {
                String[] row = new String[4]
                row[0] = ++i as String
                row[1] = item.benefitCategory.title
                row[2] =  benefiteDocMap.get(item).collect { e -> e.documentType.title }.join(', ')
                row[3] = UniStringUtils.join(benefiteDocMap.get(item.id), IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE, ", ")
                rows.add(row)
            }
        }

        if (!rows.isEmpty())
        {
            im.put('isSpecRights', "Да")
            tm.put('T3', rows as String[][])
         }

        else
            tm.remove("T3")

           //преимущественное право
        if (entrantRequest.benefitCategory != null)
        {
            int i=0
            def rowList = Lists.newArrayList()
            List<IEnrEntrantBenefitProofDocument> benefiteDocs = getBenefitProofDocs(this.entrantRequest)
                String[] row = new String[4]
                row[0] = ++i as String
                row[1] = entrantRequest.benefitCategory.title
                row[2] = UniStringUtils.join(benefiteDocs, IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE, ", ")
            rowList.add(row)
            tm.put('T4', rowList as String[][])
        }
        else
            tm.remove('T4')
    }


    def fillDDetail(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id}
        // внутренние испытания кроме вузовского ЕГЭ
        final internalExams =  new DQLSelectBuilder().fromEntity(EnrChosenEntranceExamForm.class, 'e')
                .predicate(DQLPredicateType.distinct)
                .column(property('e', EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .where(DQLExpressions.in(property('e', EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition()), getRequestedCompetitions()))
                .where(eq(property('e', EnrChosenEntranceExamForm.passForm().internal()), value(true)))
                .order(property('e', EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .createStatement(this.session).<String>list()

        im.put("dDetail", !internalExams.empty ? internalExams.join(', ') : "")
        im.put('vi', internalExams.empty? '' : 'x')
    }

    static Map<Long, List<IEnrEntrantBenefitProofDocument>> getBenefitProofDocuments(List<? extends IEnrEntrantBenefitStatement> competitions) {
        List<Long> ids = competitions.collect { e -> e.id }
        List<EnrEntrantBenefitProof> benefitProofs =  DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                    .fromEntity(EnrEntrantBenefitProof.class, "bp")
                    .column(property("bp"))
                    .where(DQLExpressions.in(property("bp", EnrEntrantBenefitProof.benefitStatement().id()), ids))
                )
        Map<Long, List<IEnrEntrantBenefitProofDocument>> result = SafeMap.get(ArrayList.class)
        for (def benefitProof : benefitProofs)
            result.get(benefitProof.benefitStatement.id).add(benefitProof.document)
        return result
    }

    def List<IEnrEntrantBenefitProofDocument> getBenefitProofDocs(final IEnrEntrantBenefitStatement benefitStatement)
    {
        return new DQLSelectBuilder().fromEntity(EnrEntrantBenefitProof.class, 'bp')
                .column(property('bp', EnrEntrantBenefitProof.document()))
                .where(eq(property('bp', EnrEntrantBenefitProof.benefitStatement()), value(benefitStatement)))
                .order(property('bp', EnrEntrantBenefitProof.document().registrationDate()))
                .createStatement(this.getSession()).<IEnrEntrantBenefitProofDocument>list()
    }


    /** Индвидиуальные достижения */
    void fillAchievements()
    {
        final achievementList = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'e')
                .column(property('e'))
                .where(eq(property('e', EnrEntrantAchievement.entrant()), value(this.entrantRequest.entrant)))
                .createStatement(this.session).<EnrEntrantAchievement>list()
        if (achievementList.isEmpty())
        {
            im.put('hasIndAchievements', "об отсутствии ")
            tm.remove('T5')
        }
        else
        {
            im.put('hasIndAchievements', "о наличии ")
            List<String[]> rowsList = new ArrayList<>(achievementList.size())
            int i=0
            for (EnrEntrantAchievement item : achievementList)
            {
               def row = [++i, item.type.title, item.document.displayableTitle]
               rowsList.add(row as String[])
            }
            tm.put('T5', rowsList as String[][])
        }

    }

    //Олимпиады
    def fillOlympiads()
    {
        // Если для выбранных конкурсов заявления есть ВВИ, у которых источник балла — основание балла (особое право), то выводить таблицу с данными олимпиад
        final chosenExamList = new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, 'cee')
                .column(property('cee'))
                .where(DQLExpressions.in(property('cee', EnrChosenEntranceExam.requestedCompetition()), getRequestedCompetitions()))
                .where(exists(EnrEntrantMarkSourceBenefit.class, 'id', property('cee', EnrChosenEntranceExam.maxMarkForm().markSource().id())))
                .createStatement(getSession()).<EnrChosenEntranceExam>list()
        if(chosenExamList.empty)
            tm.remove('T2')
        else
        {
            List<String[]> rowsList = new ArrayList<>(chosenExamList.size())
            int i=0
            for (EnrChosenEntranceExam item : chosenExamList)
            {
                EnrOlympiadDiploma diploma = (item.markSource as EnrEntrantMarkSourceBenefit).mainProof as EnrOlympiadDiploma
                def row = [++i,
                           diploma.olympiad.title,
                           diploma.displayableTitle,
                           item.discipline.title,
                           diploma.olympiadType.title,
                           diploma.honour.title,
                           (item.markSource as EnrEntrantMarkSourceBenefit).benefitCategory.title,
                           item.maxMarkForm.markAsString]
                rowsList.add(row as String[])
            }
            tm.put('T2', rowsList as String[][])
        }
    }

    /** Выбранные конкурсы по договору (в т.ч. без ви). Если таких нет, то страница не выводится. */
    void fillAcceptedContract()
    {
        deleteLabels.add("acceptedContract")
        if (!SharedRtfUtil.hasFields(document.elementList, ['acceptedContract'].toSet()))
            return;

        final acceptedContractCompetitions = getRequestedCompetitions().findAll {it.competition.type.compensationType.code.equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)}

        if (!acceptedContractCompetitions.empty) {

            final item = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT) // Метка заменяется на документ из отдельного шаблона
            final injectedDoc = new RtfReader().read(item.getCurrentTemplate())
            String eduLevels = acceptedContractCompetitions.collect { e -> e.competition.programSetOrgUnit.programSet.programSubject.subjectIndex.programKind.getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_DATIVE).toLowerCase()}.toSet().join(', ')
            String programSubject= acceptedContractCompetitions.collect {e -> getProgramSubjectStr(e)}.join(', ')
            im.put('educationlevelsHighSchool', programSubject)
            im.put('eduLevelType', eduLevels)


            // Вставляем в основной документ вместо метки "acceptedContract"
            RtfUtil.modifySourceList(this.document.header, injectedDoc.header, injectedDoc.elementList)
            this.document.elementList.addAll(injectedDoc.elementList)
        }
    }
}