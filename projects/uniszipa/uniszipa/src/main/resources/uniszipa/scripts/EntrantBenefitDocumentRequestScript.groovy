/* $Id$ */
package uniszipa.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument

return new EntrantBenefitDocumentRequest(
        session: session,
        template: template,
        benefitDocument: benefitDocument,
        entrant: entrant
).print()

/**
 * @author Denis Katkov
 * @since 08.06.2016
 */
class EntrantBenefitDocumentRequest {
    Session session
    byte[] template
    IEnrEntrantBenefitProofDocument benefitDocument
    EnrEntrant entrant


    def im = new RtfInjectModifier()

    def print() {
        RtfDocument document = new RtfReader().read(template)

        if (StringUtils.isNotEmpty(benefitDocument.issuancePlace)) {
            im.put("organizationTitle", benefitDocument.issuancePlace)
            im.put("organizationTitleUnderlined", benefitDocument.issuancePlace)
        } else {
            im.put("organizationTitle", "")
            im.put("organizationTitleUnderlined", "________________________________")
        }
        im.put("documentIssueDate", benefitDocument.issuanceDate != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(benefitDocument.issuanceDate) : "___.___.______")
        im.put("documentType", benefitDocument.documentType.title)
        im.put("entrantFio", entrant.fullFio)

        im.modify(document)

        return [document: RtfUtil.toByteArray(document),
                fileName: "Запрос по документу, подтверждающему льготу - ${entrant.fio}.rtf"]
    }
}