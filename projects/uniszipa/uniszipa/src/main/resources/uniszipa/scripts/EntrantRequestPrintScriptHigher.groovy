package uniszipa.scripts

import com.google.common.collect.Lists
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintHigher(                                // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Denis Perminov
 * @since 11.07.2014
 */
class EntrantRequestPrintHigher {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    List<String> olympiadStr = new ArrayList<>()

    def print() {
        // получаем направление приема с наивысшим приоритетом
        def direction = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()}=${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
                /).setMaxResults(1).<EnrRequestedCompetition> uniqueResult()

        fillInjectParameters(direction)
        fillEnrEntrantStateExam(direction)
        fillDDetail(direction)
        fillBenefits(direction)

        RtfDocument document = new RtfReader().read(template)
        im.modify(document)
        tm.modify(document)
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    void fillInjectParameters(EnrRequestedCompetition requestedCompetition) {
        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = entrantRequest.identityCard
        def sex = card.sex
        def personAddress = person.address

        im.put("requestNumber", entrantRequest.stringNumber)
        im.put("lastName", card.lastName)
        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("sex", sex.male ? "М" : "Ж")
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("birthPlace", card.birthPlace)

        im.put("passportCitizenship", card.citizenship.title)
        im.put("passport", card.cardType.title + ", " + (null != card.seria ? card.seria : "") + " " + (null != card.number ? card.number : ""))
        im.put("passportIssuanceDate", card.issuanceDate?.format("dd.MM.yyyy"))
        im.put("passportIssuancePlace", card.issuancePlace)

        im.put("a", sex.male ? "ий" : "ая")
        im.put("registrationAddress", card.address?.titleWithFlat)
        im.put("address", personAddress?.titleWithFlat)
        im.put("phoneNumbers", person.contactData.mainPhones)
        im.put("email", person.email)

        def competition = requestedCompetition.competition
        im.put("developForm_A", getProgramFormDeclinate(competition.programSetOrgUnit.programSet.programForm))
        im.put("b", competition.type.compensationType.code.equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "X" :"")
        im.put("c", competition.type.compensationType.code.equals(UniDefines.COMPENSATION_TYPE_CONTRACT) ? "X" :"")

        def isSPO = false
        def isHigh = true
        def isFive = false
        def isMaster = false

        def programKind = competition.programSetOrgUnit.programSet.getProgramKind()
        isSPO = isSPO || programKind.programSecondaryProf
        isHigh = isHigh && programKind.programHigherProf
        isFive = isFive || (programKind.programBachelorDegree || programKind.programSpecialistDegree)
        isMaster = isMaster || programKind.programMasterDegree

        im.put("eduLevelStr", isSPO ? "социальном техникуме Северо-Западного института - филиала РАНХиГС" : "Северо-Западном институте - филиале РАНХиГС")
        im.put("eduLevelTypeStr_G", isHigh ? "высшего" : "среднего")

        def rows = Lists.newArrayList()
        def subject = competition.programSetOrgUnit.programSet.programSubject
        rows.add([String.valueOf(requestedCompetition.priority),subject.subjectCode, subject.title])
        tm.put("T", rows as String[][])

        def eduDocument = entrantRequest.eduDocument
        im.put("edEndYear", eduDocument.yearEnd as String)
        im.put("eduInstitution", eduDocument.eduOrganisationWithAddress)
        im.put("certificate", eduDocument.documentKindTitle)
        im.put("edSeria", eduDocument?.seria)
        im.put("edNumber", eduDocument?.number)
        im.put("edIssuanceDate", eduDocument?.issuanceDate?.format("dd.MM.yyyy"))
        im.put("certificateDetail", eduDocument?.graduationHonour?.title)

        def foreignLanguageList = DQL.createStatement(session, /
                select distinct ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                and ${PersonForeignLanguage.main()}=${true}
                /).<String> list()

        im.put("foreignLanguages", foreignLanguageList.join(", "))

//        def personBenefits = DQL.createStatement(session, /
//                from ${PersonBenefit.class.simpleName}
//                where ${PersonBenefit.person().id()}=${person.id}
//                /).<PersonBenefit> list()
//
//        if (!personBenefits.isEmpty()) {
//            im.put("benefits", personBenefits.collect { e -> e.benefit.title }.toSet().join(", "))
//            im.put("benefitsDetail", personBenefits.collect { e -> ((null != e.document ? e.document : "") + (null != e.date ? " от " + e.date.format("dd.MM.yyyy") + " г." : "")) }.toSet().join(", "))
//        } else {
//            im.put("benefits", "")
//            im.put("benefitsDetail", "")
//        }

        im.put("father", getNextOfKin(person, RelationDegreeCodes.FATHER))
        im.put("mother", getNextOfKin(person, RelationDegreeCodes.MOTHER))

        im.put("f", sex.male ? "" : "а")
        im.put("h", sex.male ? "ен" : "на")

        if (isFive)
            im.put("fiveVuz", new RtfString().append("Подтверждаю подачу заявления не более чем в пять вузов.").par().append("Заявление в Академию является _______ по счету."))
        else
            deleteLabels.add("fiveVuz")

        if (isMaster)
            deleteLabels.add("firstHighEducation")
        else
            im.put("firstHighEducation", isHigh ? "Высшее профессиональное образование получаю впервые" : "Среднее профессиональное образование получаю впервые")

        im.put("needSpecialConditions", entrant.isNeedSpecialExamConditions() ? "нуждаюсь" + (null != entrant.getSpecialExamConditionsDetails() ? "" : " (" + entrant.getSpecialExamConditionsDetails() + ")") : "не нуждаюсь")
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().title)
        im.put("needHotel", entrant.person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(card, GrammaCase.NOMINATIVE))
    }

    static getProgramFormDeclinate(EduProgramForm programForm) {
        switch(programForm.code) {
            case DevelopFormCodes.FULL_TIME_FORM:
                return "очную"
            case DevelopFormCodes.CORESP_FORM:
                return "заочную"
            case DevelopFormCodes.PART_TIME_FORM:
                return "очно-заочную"
            case DevelopFormCodes.EXTERNAL_FORM:
                return "экстернат"
            case DevelopFormCodes.APPLICANT_FORM:
                return "самостоятельную"
        }
        return ""
    }

    String getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,[nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null
    }

    def fillEnrEntrantStateExam(EnrRequestedCompetition enrRequestedCompetition) {
        // все ЕГЭ по абитуриенту
        def stateExamResultList = DQL.createStatement(session, /
                from ${EnrEntrantStateExamResult.class.simpleName}
                where ${EnrEntrantStateExamResult.entrant().id()}=${entrantRequest.entrant.id}
                order by ${EnrEntrantStateExamResult.subject().title()}
                /).<EnrEntrantStateExamResult> list()
        // ВИ максимального приоритета, у которых в зачет или ЕГЭ или олимпиада
        def chosenEntranceExamList = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExam.class, "ecee")
                .column(property("ecee"))
                .fetchPath(DQLJoinType.inner, EnrChosenEntranceExam.maxMarkForm().markSource().fromAlias("ecee"), "eemsse")
                .where(or(instanceOf("eemsse", EnrEntrantMarkSourceStateExam.class), instanceOf("eemsse", EnrEntrantMarkSourceBenefit.class)))
                .where(eq(property("ecee", EnrChosenEntranceExam.L_REQUESTED_COMPETITION), value(enrRequestedCompetition)))
                .order(property("ecee", EnrChosenEntranceExam.discipline().discipline().title()))
                .createStatement(session).<EnrChosenEntranceExam> list()

        def rows = Lists.newArrayList()
        List<String> olympiadStr = new ArrayList<>()

        for (def chosenEntranceExam : chosenEntranceExamList) {
            EnrChosenEntranceExamForm maxMarkForm = chosenEntranceExam.maxMarkForm
            if (null != maxMarkForm && maxMarkForm.markAsLong > 0) {

                // есть балл - учитываем
                def mark = maxMarkForm.markAsLong
                def stateExamDoc = null
                EnrStateExamSubject stateExamSubject = chosenEntranceExam.discipline.stateExamSubject

                if (chosenEntranceExam.getMarkSource() instanceof EnrEntrantMarkSourceBenefit) {
                    // олимпиада в зачет?
                    EnrEntrantMarkSourceBenefit markSourceBenefit = (EnrEntrantMarkSourceBenefit) chosenEntranceExam.markSource
                    IEnrEntrantBenefitProofDocument doc = markSourceBenefit.mainProof
                    if (doc instanceof EnrEntrantBaseDocument) {
                        if (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code)) {
                            def customDoc =  doc.docRelation.document as EnrOlympiadDiploma
                            stateExamDoc = UniStringUtils.joinWithSeparator(" ", customDoc.seria, customDoc.number)
                            olympiadStr.add(chosenEntranceExam.discipline.title + ", " + customDoc.olympiad.title +
                                    " (" + customDoc.subject.title + "), " + customDoc.honour.title.toLowerCase())
                        } else {
                            def customDoc = (EnrEntrantBaseDocument) doc
                            stateExamDoc = UniStringUtils.joinWithSeparator(" ", customDoc.seria, customDoc.number)
                        }
                    } else
                        stateExamDoc = UniStringUtils.joinWithSeparator(" ", doc.seria, doc.number)
                } else {
                    // ЕГЭ в зачет
                    for (EnrEntrantStateExamResult stateExamResult : stateExamResultList) {
                        if (stateExamResult.subject.equals(stateExamSubject)) {
                            stateExamDoc = stateExamResult.documentNumber
                            break
                        }
                    }
                }
                rows.add([(null != stateExamSubject ? stateExamSubject.title : chosenEntranceExam.discipline.discipline.title),
                          String.valueOf(mark / 1000),
                          stateExamDoc])
            }
        }
        tm.put("T1", rows as String[][])
    }

    def fillDDetail(EnrRequestedCompetition enrRequestedCompetition) {
        // внутренние испытания окромя вузовского ЕГЭ
        def enrChosenEntranceExam = DQL.createStatement(session, /
                select distinct ${EnrChosenEntranceExamForm.chosenEntranceExam()}
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()}=${enrRequestedCompetition.id}
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                and ${EnrChosenEntranceExamForm.passForm().code()}!='${EnrExamPassFormCodes.STATE_EXAM}'
                /).<EnrChosenEntranceExam> list()

        im.put("dDetail", !enrChosenEntranceExam.empty ? enrChosenEntranceExam.collect { e -> e.discipline.title }.join(", ") : "")
    }

    def fillBenefits(EnrRequestedCompetition enrRequestedCompetition) {
        Set<String> benefitsSet = new TreeSet<>()
        Set<String> benefitsDetail = new TreeSet<>()

        // собираем особые права
        def exclusiveCompetitions = enrRequestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }.collect { e -> (EnrRequestedCompetitionExclusive) e }
        if (!exclusiveCompetitions.empty) {
            for (EnrRequestedCompetitionExclusive competition : exclusiveCompetitions)
                benefitsSet.add(competition.benefitCategory.shortTitle)

            def benefitProofDocList = new ArrayList<>()
            for (def bpDocs : getBenefitProofDocuments(exclusiveCompetitions).values())
                benefitProofDocList.addAll(bpDocs)
            for (def doc : benefitProofDocList)
                benefitsDetail.add((doc instanceof EnrEntrantBaseDocument) ?  (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code)
                        ? olympDocTitle(doc.docRelation.document as EnrOlympiadDiploma)  : baseDocTitle(doc)) :
                        doc.extendedTitle)
        }
        // собираем без ВИ
        def noExamCompetitions = enrRequestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionNoExams}.collect { e -> (EnrRequestedCompetitionNoExams) e }
        if (!noExamCompetitions.empty) {
            for (EnrRequestedCompetitionNoExams competition : noExamCompetitions)
                benefitsSet.add("Без ВИ:" + competition.benefitCategory.shortTitle)

            def benefitProofDocList = new ArrayList<>()
            for (def bpDocs : getBenefitProofDocuments(noExamCompetitions).values())
                benefitProofDocList.addAll(bpDocs)
            for (def doc : benefitProofDocList) {
                if(doc instanceof EnrEntrantBaseDocument)
                {
                    if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code))
                    {
                        def diploma = doc.docRelation.document as EnrOlympiadDiploma

                        olympiadStr.add(diploma.olympiad.title + " (" + diploma.subject.title + ")," + diploma.honour.title.toLowerCase())
                        benefitsDetail.add("Без ВИ:" + olympDocTitle(diploma))
                    }
                    else
                    {
                        benefitsDetail.add("Без ВИ:" + baseDocTitle(doc))
                    }
                }
                else
                    benefitsDetail.add("Без ВИ:" + doc.extendedTitle)
            }
        }
        // целевой прием
        def compCode = enrRequestedCompetition.competition.type.code
        if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compCode) ||
                EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compCode)) {
            def requestedCompetitionTAList = enrRequestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionTA}.collect { e -> (EnrRequestedCompetitionTA) e }

            for (EnrRequestedCompetitionTA competitionTA : requestedCompetitionTAList) {
                benefitsSet.add("ЦП:" + competitionTA.targetAdmissionKind.title)
                StringBuilder contractStr = new StringBuilder()
                if (null != competitionTA.contractNumber || null != competitionTA.contractDate) {
                    contractStr.append(", договор")
                    if (null != competitionTA.contractNumber)
                        contractStr.append(" №").append(competitionTA.contractNumber)
                    if (null != competitionTA.contractDate)
                        contractStr.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(competitionTA.contractDate))
                }
                benefitsDetail.add("ЦП:" + competitionTA.targetAdmissionKind.title + ", " + competitionTA.targetAdmissionOrgUnit.titleWithLegalForm + contractStr.toString())
            }
        }
        im.put("olympiadStr", olympiadStr.join("; "))
        im.put("benefits", !benefitsSet.empty ? StringUtils.join(benefitsSet.iterator(), "; ") : "")
        im.put("benefitsDetail", !benefitsDetail.empty ? StringUtils.join(benefitsDetail.iterator(), "; ") : "")
    }

    static Map<Long, List<IEnrEntrantBenefitProofDocument>> getBenefitProofDocuments(List<? extends IEnrEntrantBenefitStatement> competitions) {
        List<Long> ids = competitions.collect { e -> e.id }
        List<EnrEntrantBenefitProof> benefitProofs =  DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrEntrantBenefitProof.class, "bp")
                        .column(property("bp"))
                        .where(DQLExpressions.in(property("bp", EnrEntrantBenefitProof.benefitStatement().id()), ids))
        )
        Map<Long, List<IEnrEntrantBenefitProofDocument>> result = SafeMap.get(ArrayList.class)
        for (def benefitProof : benefitProofs)
            result.get(benefitProof.benefitStatement.id).add(benefitProof.document)
        return result
    }

    static def olympDocTitle(EnrOlympiadDiploma doc) {
        return doc.documentType.shortTitle + " " + doc.seriaAndNumber + " (" + doc.olympiad.title + ") по предмету " +
                doc.subject.title.toLowerCase() + ", " + doc.honour.title.toLowerCase()
    }

    static def baseDocTitle(EnrEntrantBaseDocument doc) {
        return doc.documentType.shortTitle + UniStringUtils.joinWithSeparator(" ", doc.seria, doc.number) +
                ", выдан: " + (StringUtils.isEmpty(doc.issuancePlace) ? "" : doc.issuancePlace) +
                (null != doc.issuanceDate ? " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.issuanceDate) : "") +
                (null != doc.expirationDate ? ", действителен до: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.expirationDate) : "")
    }
}