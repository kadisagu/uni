/** 
 *$Id:$
 */
// Copyright 2006-2012 Tandem Service Software
package uniszipa.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.PersonBenefit
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
import ru.tandemservice.uniec.util.EntrantDataUtil

return new EntrantRequestPrint(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати заявления абитуриента (СЗИ РАНХиГС)
 *
 * @author Alexander Shaburov
 * @since 09.06.12
 */
class EntrantRequestPrint
{
  Session session
  byte[] template
  EntrantRequest entrantRequest
  def modifier = new RtfInjectModifier()
  def tableModifier = new RtfTableModifier()
  def document

  /**
   * Основной метод печати.
   * @return <code>[document: byte[], fileName: String]</code>
   */
  def print()
  {
    def directionList = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.priority()}
    /).<RequestedEnrollmentDirection> list();

    def builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d")
            .add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().id().s(), entrantRequest.id));

    def dataUtil = new EntrantDataUtil(session, entrantRequest.entrant.enrollmentCampaign, builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS)
    document = new RtfReader().read(template);

    prepareInjectModifier(dataUtil, directionList)
    prepareTableModifier(dataUtil, directionList)

    modifier.modify(document);
    tableModifier.modify(document);

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(document),
            fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
  }

  /**
   * Подготавливает и заполняет метки для печати.
   * @param dataUtil <code>EntrantDataUtil</code>
   * @param directionList список направлений приема
   */
  def prepareInjectModifier(EntrantDataUtil dataUtil, def directionList)
  {
    def entrant = entrantRequest.entrant
    def person = entrant.person
    def card = person.identityCard
    def personAddress = person.address

    def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
    def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
    def priorityDirection = directionList.get(0);

    def dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER

    modifier.put('requestNumber', entrantRequest.stringNumber)
    modifier.put('address', personAddress?.titleWithFlat)
    modifier.put('registrationAddress', person.identityCard.address?.titleWithFlat)
    modifier.put('foreignLanguages', getForeignLanguages(person))
    modifier.put('benefits', getBenefits(person))
    modifier.put('father', father != null ? father : '');
    modifier.put('mother', mother != null ? mother : '');
    modifier.put('lastName', card.lastName);
    modifier.put('firstName', card.firstName);
    modifier.put('middleName', card.middleName);
    modifier.put('sex', card.sex.male ? 'М' : 'Ж');
    modifier.put('birthDate', dateFormatter.format(card.birthDate));
    modifier.put('birthPlace', card.birthPlace);
    modifier.put('passportCitizenship', card.citizenship.title);
    modifier.put('passport', card.cardType.title + ': ' + (card.seria != null ? card.seria : '') + ' ' + (card.number != null ? card.number : ''));
    modifier.put('passportIssuanceDate', dateFormatter.format(card.issuanceDate));
    modifier.put('passportIssuancePlace', card.issuancePlace);
    modifier.put('a', card.sex.male ? 'ий' : 'ая');
    modifier.put('f', card.sex.male ? '' : 'а');
    modifier.put('h', card.sex.male ? 'ен' : 'на');
    modifier.put('phoneNumbers', person.contactData.mainPhones)
    modifier.put('email', person.email);

    modifier.put('developForm_A', priorityDirection.enrollmentDirection.educationOrgUnit.developForm.accCaseTitle);
    modifier.put('b', priorityDirection.compensationType.code == CompensationTypeCodes.COMPENSATION_TYPE_BUDGET ? 'x' : '');
    modifier.put('c', priorityDirection.compensationType.code == CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT ? 'x' : '')

    def nonEgeList = new HashSet<String>();
    for (def direction : directionList)
    {
      for (def chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
      {
        for (def passDiscipline : dataUtil.getExamPassDisciplineSet(chosen))
        {
          if (!passDiscipline.subjectPassForm.code.equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
            nonEgeList.add(passDiscipline.enrollmentCampaignDiscipline.educationSubject.title);
        }
      }
    }
    modifier.put('dDetail', StringUtils.join(nonEgeList, ", "));

    if (person.personEduInstitution != null)
    {
      modifier.put('edEndYear', String.valueOf(person.personEduInstitution.yearEnd));
      modifier.put('eduInstitution', person.personEduInstitution.eduInstitutionTitle);
      modifier.put('certificate', person.personEduInstitution.documentType.title + ', ' + person.personEduInstitution.educationLevel.title);
      modifier.put('certificateDetail', person.personEduInstitution.graduationHonour != null ? person.personEduInstitution.graduationHonour.title : '');
      modifier.put('edSeria', person.personEduInstitution.seria);
      modifier.put('edNumber', person.personEduInstitution.number);
      modifier.put('edIssuanceDate', dateFormatter.format(person.personEduInstitution.issuanceDate));
    }
    else
    {
      modifier.put('edEndYear', '');
      modifier.put('eduInstitution', '');
      modifier.put('certificate', '');
      modifier.put('certificateDetail', '');
      modifier.put('edSeria', '');
      modifier.put('edNumber', '');
      modifier.put('edIssuanceDate', '');
    }

    modifier.put('needHotel', entrant.person.needDormitory ? 'нуждаюсь' : 'не нуждаюсь');

    def olympiadList = new ArrayList<String>();
    for (def direction : dataUtil.directionSet)
      for (def discipline : dataUtil.getChosenEntranceDisciplineSet(direction))
      {
        def statistic = dataUtil.getMarkStatistic(discipline);
        if (statistic.diplomaSet != null)
          for (def diploma : statistic.diplomaSet)
          {
            def record = new StringBuilder(discipline.title);
            record.append(' (').append(diploma.olympiad).append(', ');

            if (!diploma.diplomaType.code.equals(UniecDefines.DIPLOMA_TYPE_OTHER))
              record.append(diploma.diplomaType.title).append(', ');

            record.append(diploma.degree.title).append(')');

            olympiadList.add(record.toString());
          }
      }
    modifier.put('olympiadStr', StringUtils.join(olympiadList, ', '));

    def builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d")
            .add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().id().s(), entrantRequest.id))
    ;
    def benefitMap = dataUtil.getPersonBenefitMap(session, builder)
    def benefitSet = benefitMap.get(entrant.person)
    def benefitData = new ArrayList<String>()
    for (def benefit : benefitSet)
    {
      def record = null
      if (benefit.document != null)
        record = benefit.document
      if (benefit.date != null)
        record += ' от ' + dateFormatter.format(benefit.date);

      if (record != null)
        benefitData.add(record);
    }
    modifier.put('benefitsDetail', StringUtils.join(benefitData, ', '));

    def middleGos23 = true//среди выбранных направлений приема есть направления СПО (гос 2 или гос 3)
    def bachelorOrSpecialty = true//выбранные направления только с квалификацией 62 (бакалавр) или 65 (специалист)
    def master = false//есть хотя бы одного направление магистратуры
    def middle = false//есть хотя бы одно направление СПО
    for (def direction : dataUtil.directionSet)
    {
      if (direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.levelType.middleGos2 ||
              direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.levelType.middleGos3)
      {
        middleGos23 = false
      }

      if (direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.levelType.middle)
      {
        middle = true
      }

      if (direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification != null)
      {
        if (!QualificationsCodes.BAKALAVR.equals(direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code) ||
                !QualificationsCodes.SPETSIALIST.equals(direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code))
        {
          bachelorOrSpecialty = false
        }

        if (QualificationsCodes.MAGISTR.equals(direction.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code))
        {
          master = true
        }
      }
    }

    modifier.put('eduLevelStr', middleGos23 ? 'Северо-Западном институте - филиале РАНХиГС' : 'социальном техникуме Северо-Западного института - филиала РАНХиГС');
    modifier.put('eduLevelTypeStr_G', middle ? 'среднего' : 'высшего');

    if (bachelorOrSpecialty)
    {
      def rtfString = new RtfString()
              .append('Подтверждаю подачу заявления не более чем в пять вузов.')
              .par()
              .append('Заявление в Академию является _______ по счету.');

      modifier.put('fiveVuz', rtfString);
    }
    else
    {
      tableModifier.put('fiveVuz', (String[]) null);//удаляем строку таблицы, содержащую метку
    }

    if (master)
    {
      tableModifier.put('firstHighEducation', (String[]) null);//удаляем строку таблицы, содержащую метку
    }
    else
    {
      modifier.put('firstHighEducation', middle ? 'Среднее профессиональное образование получаю впервые' : 'Высшее профессиональное образование получаю впервые');
    }
  }

  /**
   * Подготавливает и заполняет табличные метки для печати.
   * @param dataUtil <code>EntrantDataUtil</code>
   * @param directionList список направлений приема
   */
  def prepareTableModifier(EntrantDataUtil dataUtil, List<RequestedEnrollmentDirection> directionList)
  {
    //таблица с данными одного Направления приема, с наивысшим приоритетом
    def primaryDirection = directionList.get(0);
    def directionData = new ArrayList<String[]>()//список строк

    directionData.add([primaryDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.okso + '.' + primaryDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code,
            primaryDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.title] as String[])

    tableModifier.put('T', directionData as String[][]);

    //таблица с данными Вступительных испытаний
    //список строк
    def disciplineData = new TreeMap<Discipline2RealizationWayRelation, String[]>(new Comparator(){
      @Override
      int compare(def o1, def o2)
      {
        return o1.educationSubject.title.compareTo(o2.educationSubject.title)
      }
    })
    for (def direction : directionList)
      for (def discipline : dataUtil.getChosenEntranceDisciplineSet(direction))
      {
        if (discipline.finalMarkSource != null && (discipline.finalMarkSource.equals(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1) || discipline.finalMarkSource.equals(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2)))
        {
          if (!disciplineData.containsKey(discipline.enrollmentCampaignDiscipline))
          {
            def statistic = dataUtil.getMarkStatistic(discipline)
            disciplineData.put(discipline.enrollmentCampaignDiscipline, [discipline.title, DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(statistic.scaledStateExamMark), statistic.stateExamMark.certificate.number] as String[])
          }
        }
        else if (discipline.finalMarkSource != null && discipline.finalMarkSource.equals(UniecDefines.FINAL_MARK_OLYMPIAD))
        {
          if (!disciplineData.containsKey(discipline.enrollmentCampaignDiscipline))
          {
            def statistic = dataUtil.getMarkStatistic(discipline)
            disciplineData.put(discipline.enrollmentCampaignDiscipline, [discipline.title, discipline.enrollmentCampaignDiscipline.maxMark, statistic.diplomaSet.iterator().hasNext() ? statistic.diplomaSet.iterator().next().number : ''] as String[])
          }
        }
      }

    tableModifier.put('T1', disciplineData.values() as String[][])
  }

  /**
   * Вычисляет строку с Иностранными языками персоны, если они есть.
   * @param person персона абитуриента
   * @return Строка с названиями языков через запятую, если они есть.
   * Иначе <code>'Нет'<code/>.
   */
  def getForeignLanguages(def person)
  {
    def foreignLanguages = DQL.createStatement(session, /
                select ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.language().title()}
    /).<String> list()

    return foreignLanguages.empty ? 'Нет' : foreignLanguages.join(', ')
  }

  /**
   * Вычисляет строку с Льготами для персоны, если они есть.
   * @param person персона абитуриента
   * @return Строка с названиями льгот через запятую, если они есть.
   * Иначе <code>'Льгот нет'<code/>.
   */
  def getBenefits(def person)
  {
    def benetifs = DQL.createStatement(session, /
                select ${PersonBenefit.benefit().title()}
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                order by ${PersonBenefit.benefit().title()}
    /).<String> list()

    return benetifs.empty ? 'Льгот нет' : benetifs.join(', ')
  }

  /**
   * Вычисляет строку с Ближайшим родственником, если он есть.
   * @param person персона абитуриента
   * @param relationDegreeCode код Степени родства
   * @return Строка с данными ближайшего родственника.
   */
  def getNextOfKin(def person, def relationDegreeCode)
  {
    def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
        /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

    return nextOfKin ? [nextOfKin.fullFio,
            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(' '),
            nextOfKin.phones
    ].grep().join(', ') : null;
  }
}
