package uniszipa.scripts

import com.google.common.base.Strings
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.fias.IKladrDefines
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrEnrollmentOrderPrintUtil
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign
import ru.tandemservice.unimove.IAbstractExtract
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao
import ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber
import ru.tandemservice.uniszipa.order.ext.EnrOrder.logic.EnrEnrollmentExtractWrapper
import ru.tandemservice.uniszipa.order.ext.EnrOrder.logic.EnrEntrantParagraphWrapper
import ru.tandemservice.uniszipa.order.ext.EnrOrder.logic.EnrEntrantSubParagraphWrapper

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

return new EntrantEnrollmentOrderBase(
        session: session,
        template: template,
        order: session.get(EnrOrder.class, object)
).print()

/**
 * @author Igor Belanov
 * @since 08.08.2016
 */
class EntrantEnrollmentOrderBase
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    List<EnrEnrollmentExtract> extractList
    List<EnrEntrantParagraphWrapper> printParagraphWrapperList
    EnrCompetitionType enrCompetitionType
    Boolean isFirstStage

    def print()
    {
        def document = new RtfReader().read(template)

        init()
        prepareParagraphs()

        // заполнение меток
        im.put('commitDate', (order.commitDate != null ? DateFormatter.STRING_MONTHS_AND_QUOTES.format(order.commitDate) : '«___» _____________ _____') + ' года')
        im.put('orderNumber', order.number != null ? order.number : '_____')
        // нужно заполнить текст наверху
        im.put('enrollmentInfo', getAboveText())
        im.put('orderProtocolInfo', getOrderProtocolInfoString())

        fillParagraphs(document)
        fillVisas()

        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    def init() {
        // заполним массив выписок
        extractList = new ArrayList<>()
        for (IAbstractParagraph paragraph : order.getParagraphList())
            for (IAbstractExtract extract : paragraph.getExtractList())
                extractList.add((EnrEnrollmentExtract)extract)

        // вычислим вид приёма, если он null, то в приказе есть абитуриенты с разными видами приёма
        enrCompetitionType = null
        for(IAbstractParagraph paragraph : order.getParagraphList())
        {
            if (!(paragraph instanceof EnrEnrollmentParagraph))
                throw new ApplicationException("Параграф №${paragraph.number} в приказе «" + paragraph.order.title + "» не может быть напечатан, так как не является параграфом о зачислении.")

            if (enrCompetitionType != null && !enrCompetitionType.equals(paragraph.getCompetitionType()))
            {
                enrCompetitionType = null;
                break;
            }
            enrCompetitionType = ((EnrEnrollmentParagraph)paragraph).getCompetitionType();
        }

        // если у приказа есть дата, то попробуем сосчитать этап
        isFirstStage = null
        if (enrCompetitionType != null && order.createDate != null && EnrCompetitionTypeCodes.MINISTERIAL.equals(enrCompetitionType.code))
        {
            List<EnrCampaignEnrollmentStage> listOfDates = new DQLSelectBuilder()
                    .fromEntity(EnrCampaignEnrollmentStage.class, 'eces')
                    .where(eq(property(EnrCampaignEnrollmentStage.enrollmentCampaign().fromAlias('eces')), value(order.enrollmentCampaign)))
                    .where(eq(property(EnrCampaignEnrollmentStage.competitionType().code().fromAlias('eces')), value(EnrCompetitionTypeCodes.MINISTERIAL)))
                    .where(eq(property(EnrCampaignEnrollmentStage.programForm().code().fromAlias('eces')), value(EduProgramFormCodes.OCHNAYA)))
                    .where(eq(property(EnrCampaignEnrollmentStage.requestType().code().fromAlias('eces')), value(EnrRequestTypeCodes.BS)))
                    .order(property(EnrCampaignEnrollmentStage.date().fromAlias('eces')))
                    .createStatement(session).list()
            isFirstStage = listOfDates.size() <= 1 ? null : (listOfDates.get(1).date.before(order.createDate))
        }
    }

    private String getAboveText()
    {
        String aboveTextDefault = 'О зачислении в РАНХиГС'
        String stageInfo = (isFirstStage == null ? '' : (' на' + (isFirstStage ? ' первом' : ' втором') + ' этапе'))
        if (enrCompetitionType == null) return aboveTextDefault

        String requestType = '';
        switch (order.requestType.code) {
            case EnrRequestTypeCodes.BS : requestType = ' на программы бакалавриата/специалитета'; break;
            case EnrRequestTypeCodes.MASTER : requestType = ' на программы магистратуры'; break;
            case EnrRequestTypeCodes.HIGHER : requestType = ' на программы подготовки кадров высшей квалификации'; break;
            case EnrRequestTypeCodes.SPO : requestType = ' на программы среднего профессионального образования'; break;
            case EnrRequestTypeCodes.POSTGRADUATE : requestType = ' на программы аспирантуры'; break;
            case EnrRequestTypeCodes.TRAINEESHIP : requestType = ' на программы ординатуры'; break;
            case EnrRequestTypeCodes.INTERNSHIP : requestType = ' на программы интернатуры'; break;
        }

        switch (enrCompetitionType.getCode())
        {
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL : return 'О зачислении в РАНХиГС лиц, имеющих право на поступление без вступительных испытаний';
            case EnrCompetitionTypeCodes.EXCLUSIVE : return 'О зачислении в РАНХиГС лиц, имеющих особые права, на места в пределах квоты приема за счет бюджетных ассигнований федерального бюджета';
            case EnrCompetitionTypeCodes.TARGET_ADMISSION : return 'О зачислении в РАНХиГС для обучения на места в пределах квоты целевого приема за счет бюджетных ассигнований федерального бюджета';
            case EnrCompetitionTypeCodes.CONTRACT :
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT : return 'О зачислении в РАНХиГС' + requestType + ' по договорам об оказании платных образовательных услуг';
            case EnrCompetitionTypeCodes.MINISTERIAL : return 'О зачислении' + stageInfo + ' по общему конкурсу в РАНХиГС' + requestType + ' за счет бюджетных ассигнований федерального бюджета';
            default : return aboveTextDefault;
        }
    }

    /**
     * Печатные параграфы не совпадают с экранными!
     * Группировка в этих приказах происходит так:
     *     1. первая группировка по виду приёма (+ первая или вторая стадия если это очный бюджет), и иностранец или нет
     *     2. вторая группировка по направлению подготовки
     * Все критерии группировки и сортировки вынесены в обёртки
     */
    def prepareParagraphs()
    {
        // группировка
        printParagraphWrapperList = new ArrayList<>();
        for(EnrEnrollmentExtract extract : extractList)
        {
            EnrEntrantParagraphWrapper printParagraphWrapper = new EnrEntrantParagraphWrapper(
                    extract.entity.competition.type,
                    IKladrDefines.RUSSIA_COUNTRY_CODE != extract.entity.request.entrant.person.identityCard.citizenship.code
            )
            int ind = printParagraphWrapperList.indexOf(printParagraphWrapper)
            if (ind < 0) printParagraphWrapperList.add(printParagraphWrapper)
            else printParagraphWrapper = printParagraphWrapperList.get(ind)

            EnrEntrantSubParagraphWrapper printSubParagraphWrapper = new EnrEntrantSubParagraphWrapper(
                    ((EnrEnrollmentParagraph)extract.paragraph).programSubject
            )
            ind = printParagraphWrapper.getSubParagraphWrapperList().indexOf(printSubParagraphWrapper)
            if (ind < 0) printParagraphWrapper.getSubParagraphWrapperList().add(printSubParagraphWrapper)
            else printSubParagraphWrapper = printParagraphWrapper.getSubParagraphWrapperList().get(ind)

            printSubParagraphWrapper.getExtractsWrappers().add(getExtractWrapper(extract))
        }

        // сортировка параграфов, подпараграфов и списков внутри подпараграфов
        for (EnrEntrantParagraphWrapper printParagraphWrapper : printParagraphWrapperList)
        {
            for(EnrEntrantSubParagraphWrapper printSubParagraphWrapper : printParagraphWrapper.getSubParagraphWrapperList())
                Collections.sort(printSubParagraphWrapper.getExtractsWrappers())
            Collections.sort(printParagraphWrapper.getSubParagraphWrapperList())
        }
        Collections.sort(printParagraphWrapperList)
    }

    EnrEnrollmentExtractWrapper getExtractWrapper(EnrEnrollmentExtract extract)
    {
        // возьмём ФИО
        String fullFio = extract.entity.request.entrant.person.fullFio

        // сосчитаем оценки
        EnrRatingItem enrRatingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
        long sumMark = enrRatingItem != null ? Math.round(enrRatingItem.getTotalMarkAsDouble()) : 0

        // предыдущее образование
        String previousEduLevel = extract.entity.request.eduDocument.eduLevel.title

        // проверим имеет ли абитуриенты внутринние сдачи
        boolean isHaveInternalPass = false
        List<EnrExamPassForm> passForms = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "epd")
                .column(property(EnrExamPassDiscipline.passForm().fromAlias("epd")))
                .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("epd")), value(extract.entrant)))
                .joinPath(DQLJoinType.inner, EnrExamPassDiscipline.discipline().enrollmentCampaign().fromAlias("epd"), "ec")
                .where(eq(property(EnrEnrollmentCampaign.id().fromAlias("ec")), value(order.enrollmentCampaign.id)))
                .createStatement(session).list()
        for(EnrExamPassForm passForm : passForms)
            if (passForm.internal)
            {
                isHaveInternalPass = true
                break
            }

        // основание (реквизиты договора) (null, если не целевики)
        String contractBasisTitle = null;
        if (enrCompetitionType != null && EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(enrCompetitionType.getCode()))
            contractBasisTitle = ((EnrRequestedCompetitionTA)extract.entity).parametersTitle;

        return new EnrEnrollmentExtractWrapper(extract, fullFio, previousEduLevel, sumMark, isHaveInternalPass, contractBasisTitle)
    }

    /**
     * печатные параграфы
     */
    def fillParagraphs(RtfDocument document)
    {
        byte[] paragraphTemplate = EnrEnrollmentOrderPrintUtil.getEnrollmentOrderParagraphTemplate(order.getParagraphList().get(0).getId())
        def paragraphDocument = new RtfReader().read(paragraphTemplate)
        RtfUtil.modifySourceList(document.header, paragraphDocument.header, paragraphDocument.elementList)

        RtfTable topTable = removeTable(paragraphDocument, 'TOP', true)
        RtfTable headerTable = removeTable(paragraphDocument, 'HEADER', true)
        RtfTable entrantTable = removeTable(paragraphDocument,'ST', false)
        RtfTable bottomTable = removeTable(paragraphDocument, 'BOTTOM', true)

        List<IRtfElement> rtfParagraphList = []

        int paragraphCount = 0;
        for(EnrEntrantParagraphWrapper printParagraphWrapper : printParagraphWrapperList)
        {
            List<IRtfElement> rtfParagraphTop = []
            RtfInjectModifier injectModifier = new RtfInjectModifier()

            boolean isForeignEntrant = printParagraphWrapper.isForeignEntrant()

            // проверим формы обучения (а вдруг абитуриенты с разными формами обучения попали в параграф)
            EduProgramForm programForm = null
            for (EnrEntrantSubParagraphWrapper enrEntrantSubParagraphWrapper : printParagraphWrapper.getSubParagraphWrapperList())
                for(EnrEnrollmentExtractWrapper extractWrapper : enrEntrantSubParagraphWrapper.getExtractsWrappers())
                {
                    EnrEnrollmentExtract extract = extractWrapper.getExtract()
                    if (programForm != null && !programForm.equals(((EnrEnrollmentParagraph)extract.paragraph).programForm))
                        throw new ApplicationException('В сгруппированном печатном параграфе присутствуют абитуриенты разных форм обучения')
                    programForm = ((EnrEnrollmentParagraph)extract.paragraph).programForm
                }
            printParagraphWrapper.setProgramForm(programForm)

            String programFormTitle_G = ''
            switch (programForm.getCode()) {
                case EduProgramFormCodes.OCHNAYA : programFormTitle_G = 'очной'; break;
                case EduProgramFormCodes.ZAOCHNAYA : programFormTitle_G = 'заочной'; break;
                case EduProgramFormCodes.OCHNO_ZAOCHNAYA : programFormTitle_G = 'очно-заочной'; break;
            }

            String competitionType = ''
            String commaOrAnd = isForeignEntrant ? ',' : ' и'
            switch (printParagraphWrapper.getCompetitionType().getCode())
            {
                case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL : competitionType = 'имеющих право на прием без вступительных испытаний'; break;
                case EnrCompetitionTypeCodes.EXCLUSIVE : competitionType = 'успешно сдавших вступительные испытания' + commaOrAnd + ' прошедших по конкурсу на места в пределах квоты приема лиц, имеющих особые права'; break;
                case EnrCompetitionTypeCodes.TARGET_ADMISSION : competitionType = 'поступающих на места в пределах квоты целевого приёма' + commaOrAnd + ' заключивших договор о целевом обучении в соответствии с Постановлением Правительства Российской Федерации от 27 ноября 2013 №1076 «О порядке заключения и расторжения договора о целевом приеме и договора о целевом обучении»'; break;
                case EnrCompetitionTypeCodes.MINISTERIAL : competitionType  = 'успешно сдавших вступительные испытания' + commaOrAnd + ' прошедших по конкурсу'; break;
                case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT : 'прошедших по конкурсу'; break;
                case EnrCompetitionTypeCodes.CONTRACT : competitionType = 'успешно сдавших вступительные испытания' + commaOrAnd + ' прошедших по конкурсу'; break;
            }

            injectModifier.put('parNumber', ++paragraphCount as String)
            injectModifier.put('onStage', isFirstStage == null ? '' : (isFirstStage ? ' на первом этапе' : ' на втором этапе'))
            injectModifier.put('enrollmentDate', (order.commitDate != null ?  DateFormatter.STRING_MONTHS_AND_QUOTES.format(order.commitDate) : '«___» _____________ _____') + ' года')
            injectModifier.put('developFormTitle_G', programFormTitle_G)
            injectModifier.put('compensationType', order.compensationType.budget ? 'за счет бюджетных ассигнований федерального бюджета' : 'по договорам об оказании платных образовательных услуг, заключаемым при приеме на обучение за счёт средств физических и (или) юридических лиц')
            injectModifier.put('szipaByPassedOrFree', competitionType)
            injectModifier.put('szipaForeignStudents', isForeignEntrant ? ' и относящихся к категории поступающих – иностранные граждане' : '')

            rtfParagraphTop.add(topTable.clone)
            injectModifier.modify(rtfParagraphTop)

            def groupParagraphTop = RtfBean.elementFactory.createRtfGroup()
            groupParagraphTop.elementList = rtfParagraphTop
            rtfParagraphList.add(groupParagraphTop)

            List<IRtfElement> subParagraphs = fillSubParagraphs(printParagraphWrapper, headerTable, entrantTable, bottomTable)
            def groupSubParagraphs = RtfBean.elementFactory.createRtfGroup()
            groupSubParagraphs.elementList = subParagraphs
            rtfParagraphList.add(groupSubParagraphs)

            rtfParagraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            rtfParagraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
            rtfParagraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        }

        im.put('PARAGRAPHS', rtfParagraphList)
    }

    List<IRtfElement> fillSubParagraphs(EnrEntrantParagraphWrapper paragraphWrapper, RtfTable headerTable, RtfTable entrantTable, RtfTable bottomTable)
    {
        List<IRtfElement> rtfSubParagraphList = []
        // добавление подпараграфов
        for (EnrEntrantSubParagraphWrapper subParagraphWrapper : paragraphWrapper.getSubParagraphWrapperList())
        {
            List<IRtfElement> rtfSubParagraph = []
            RtfInjectModifier injectModifier = new RtfInjectModifier()
            RtfTableModifier tableModifier = new RtfTableModifier()

            injectModifier.put('programSubjectInfo', getProgramSubjectInfo(subParagraphWrapper.programSubject, paragraphWrapper.programForm))
            rtfSubParagraph.add(headerTable.clone)
            rtfSubParagraph.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))

            // будем собирать параграфы (экранные), в которых есть абитуриенты, которые сдали внутренние экзамены
            Set<EnrEnrollmentParagraph> paragraphsWithEntrantWithInternalExams = new LinkedHashSet<>()

            List<String[]> entrantList = new ArrayList<String[]>()
            int extractCount = 0
            for(EnrEnrollmentExtractWrapper extractWrapper : subParagraphWrapper.getExtractsWrappers())
            {
                int numberOfAsterisk = 0
                if (extractWrapper.isHaveInternalPass())
                {
                    paragraphsWithEntrantWithInternalExams.add((EnrEnrollmentParagraph)extractWrapper.extract.paragraph)
                    Iterator<EnrEnrollmentParagraph> iter = paragraphsWithEntrantWithInternalExams.iterator()
                    while (iter.hasNext())
                    {
                        ++numberOfAsterisk
                        if (iter.next().equals((EnrEnrollmentParagraph)extractWrapper.extract.paragraph))
                            break;
                    }
                }

                ArrayList<String> row = new ArrayList<String>()

                row.add(String.valueOf(++extractCount))
                row.add(extractWrapper.fullFio)
                row.add(extractWrapper.previousEduLevelTitle)
                row.add(Long.toString(extractWrapper.getSumMark()) + Strings.repeat('*', numberOfAsterisk))

                if (extractWrapper.isTargetAdmission())
                    row.add(extractWrapper.contractBasisTitle)

                entrantList.add(row.toArray(new String[row.size()]))
            }
            injectModifier.put('markAddText', (enrCompetitionType != null && EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(enrCompetitionType.getCode())) ? ' ЕГЭ по предмету, соответствующему профилю олимпиады' : '')
            tableModifier.put('ST', entrantList as String[][])

            rtfSubParagraph.add(entrantTable.clone)
            rtfSubParagraph.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))

            tableModifier.put('paragraphProtocolInfo', getParagraphProtocolInfoStrings(paragraphsWithEntrantWithInternalExams))
            rtfSubParagraph.add(bottomTable.clone)
            rtfSubParagraph.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))

            tableModifier.modify(rtfSubParagraph)
            injectModifier.modify(rtfSubParagraph)

            def groupSubParagraph = RtfBean.elementFactory.createRtfGroup()
            groupSubParagraph.elementList = rtfSubParagraph
            rtfSubParagraphList.add(groupSubParagraph)

            rtfSubParagraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        }
        return rtfSubParagraphList
    }

    String getProgramSubjectInfo(EduProgramSubject programSubject, EduProgramForm programForm)
    {
        StringBuilder sb = new StringBuilder()
        sb.append('- по ' + programSubject.subjectIndex.subjectTypeTitleDative)
        sb.append(' ' + programSubject.titleWithCode)

        // в скобках нужно заполнить совокупность образовательных программ
        List<EduProgramHigherProf> programHigherProfList = getProgramList(programSubject, programForm)
        Set<EduProgramSpecialization> specializationSet = new LinkedHashSet<>()
        for (EduProgramHigherProf programHigherProf : programHigherProfList)
            specializationSet.add(programHigherProf.programSpecialization)
        sb.append(' (')
            if (specializationSet.size() > 1) sb.append('совокупность образовательных программ – ')
        else sb.append('образовательная программа – ')
        // если результат пуст, то вместо ОП запишем название специальности
        if (specializationSet.isEmpty())
            sb.append(programSubject.title)
        else
        {
            Iterator<EduProgramSpecialization> iter = specializationSet.iterator()
            while (iter.hasNext())
            {
                sb.append(iter.next().title)
                if (iter.hasNext()) sb.append('; ')
            }
        }
        sb.append(')')

        return sb.toString()
    }

    List<EduProgramHigherProf> getProgramList(EduProgramSubject programSubject, EduProgramForm programForm) {
        List<EduProgramHigherProf> programHigherProfList = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, "item")
                .joinEntity("item", DQLJoinType.inner, EnrProgramSetBase.class, "set", eq(property("item", EnrProgramSetItem.programSet().id()), property("set", EnrProgramSetBase.id())))
                .joinEntity("item", DQLJoinType.inner, EduProgramHigherProf.class, "pr", eq(property("item", EnrProgramSetItem.program().id()), property("pr", EduProgramHigherProf.id())))
                .where(eq(property(EnrProgramSetBase.programSubject().id().fromAlias("set")), value(programSubject.id)))
                .where(eq(property(EnrProgramSetBase.programForm().id().fromAlias("set")), value(programForm.id)))
                .where(eq(property(EnrProgramSetBase.enrollmentCampaign().id().fromAlias("set")), value(order.enrollmentCampaign.id)))
                .column("pr")
                .createStatement(session).list()
        return programHigherProfList
    }

    def fillVisas()
    {
        def coreDao = IUniBaseDao.instance.get()
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])
    }

    // заполним данные протокола из приказа
    String getOrderProtocolInfoString()
    {
        Iterator<SzipaEnrOrderProtocolDateAndNumber> protocolInfoIter = getOrderProtocolInfo(order).listIterator()
        StringBuilder result = new StringBuilder()
        result.append('Приемной комиссии РАНХиГС (')
        appendProtocolInfo(result, protocolInfoIter.hasNext() ? protocolInfoIter.next() : null)
        result.append(')')
        result.append(' и Приемной подкомиссии Северо-Западного института управления - филиала РАНХиГ (')
        appendProtocolInfo(result, protocolInfoIter.hasNext() ? protocolInfoIter.next() : null)
        while(protocolInfoIter.hasNext())
            appendProtocolInfo(result.append(', '), protocolInfoIter.next())
        result.append(')')
        return result.toString()
    }

    String[][] getParagraphProtocolInfoStrings(LinkedHashSet<EnrEnrollmentParagraph> paragraphs)
    {
        List<String[]> result = new ArrayList<>()
        int count = 0
        Iterator<EnrEnrollmentParagraph> iter = paragraphs.iterator()
        while (iter.hasNext())
        {
            String[] row = new String[1]
            StringBuilder sb = new StringBuilder()
            SzipaEnrOrderProtocolDateAndNumber protocolInfo = getParagraphProtocolInfo(iter.next())
            sb.append(Strings.repeat('*', ++count))
            sb.append(' - по вступительным испытаниям, проводимым Северо-Западным институтом управления – филиалом РАНХиГС самостоятельно')
            sb.append(' (')
            appendProtocolInfo(sb, protocolInfo)
            sb.append(')')
            row[0] = sb.toString()
            result.add(row)
        }
        return result as String[][]
    }

    // прицепить необходимую информацию о протоколах приказа
    static def appendProtocolInfo(StringBuilder builder, SzipaEnrOrderProtocolDateAndNumber protocolInfo)
    {
        builder.append('протокол от ')
        builder.append(((protocolInfo != null && protocolInfo.protocolDate != null) ?  DateFormatter.STRING_MONTHS_AND_QUOTES.format(protocolInfo.protocolDate) : '«___» _____________ _____') + ' г.')
        builder.append(' № ')
        builder.append((protocolInfo != null && protocolInfo.protocolNumber != null) ? protocolInfo.protocolNumber : '_____')
    }

    List<SzipaEnrOrderProtocolDateAndNumber> getOrderProtocolInfo(EnrOrder order) {
        List<SzipaEnrOrderProtocolDateAndNumber> result = new ArrayList<>()
        if (order.id != null)
        {
            result = new DQLSelectBuilder()
                    .fromEntity(SzipaEnrOrderProtocolDateAndNumber.class, "seopi")
                    .where(eq(property(SzipaEnrOrderProtocolDateAndNumber.enrOrder().id().fromAlias("seopi")), value(order.id)))
                    .order(property(SzipaEnrOrderProtocolDateAndNumber.priority().fromAlias("seopi")))
                    .createStatement(session).list();
        }
        return result;
    }

    SzipaEnrOrderProtocolDateAndNumber getParagraphProtocolInfo(EnrEnrollmentParagraph paragraph)
    {
        SzipaEnrOrderProtocolDateAndNumber result = null;
        if (paragraph.id != null)
        {
            result = new DQLSelectBuilder()
                    .fromEntity(SzipaEnrOrderProtocolDateAndNumber.class, "seopi")
                    .where(eq(property(SzipaEnrOrderProtocolDateAndNumber.enrEnrollmentParagraph().id().fromAlias("seopi")), value(paragraph.id)))
                    .createStatement(session).uniqueResult();
        }
        return result;
    }

    private static RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel)
            for (RtfRow row : table.getRowList())
                for (RtfCell cell : row.getCellList())
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                        UniRtfUtil.fillCell(cell, "");

        document.getElementList().remove(table);

        return table;
    }
}