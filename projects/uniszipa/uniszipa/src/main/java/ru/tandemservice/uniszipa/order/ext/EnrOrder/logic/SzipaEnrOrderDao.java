/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderDao;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Igor Belanov
 * @since 30.08.2016
 */
public class SzipaEnrOrderDao extends EnrOrderDao implements ISzipaEnrOrderDao
{
    @Override
    public void updateOrderProtocolInfo(EnrOrder enrOrder, List<SzipaEnrOrderProtocolDateAndNumber> newProtocolInfo)
    {
        if (enrOrder.getId() != null)
        {
            // поудаляем всю предыдущую инфу о протоколах по этому приказу из базы
            List<SzipaEnrOrderProtocolDateAndNumber> oldProtocolInfo = getList(new DQLSelectBuilder()
                    .fromEntity(SzipaEnrOrderProtocolDateAndNumber.class, "seopi")
                    .where(eq(property(SzipaEnrOrderProtocolDateAndNumber.enrOrder().id().fromAlias("seopi")), value(enrOrder.getId()))));
            oldProtocolInfo.forEach(this::delete);

            // и поставим новые, если они есть
            if (newProtocolInfo != null)
                for (SzipaEnrOrderProtocolDateAndNumber protocolInfo : newProtocolInfo)
                {
                    protocolInfo.setEnrOrder(enrOrder);
                    save(protocolInfo);
                }
        }
    }
}
