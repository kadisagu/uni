/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.logic.EnrEntrantForeignEducationDocumentDSHandler;

/**
 * @author Igor Belanov
 * @since 11.07.2016
 */
@Configuration
public class SzipaEnrEntrantForeignEduDocumentTabList extends BusinessComponentManager
{
    public static final String DS_FOREIGN_EDUCATION_DOCUMENTS = "foreignEducationDocumentsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(DS_FOREIGN_EDUCATION_DOCUMENTS, foreignEducationDSColumns(), foreignEducationDSHandler()))
                .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ColumnListExtPoint foreignEducationDSColumns()
    {
        return columnListExtPointBuilder(DS_FOREIGN_EDUCATION_DOCUMENTS)
                .addColumn(textColumn("documentType", SzipaForeignEducationDocument.documentTypeTitle()).order())
                .addColumn(textColumn("seriaAndNumber", SzipaForeignEducationDocument.seriaAndNumber()).order())
                .addColumn(textColumn("country", SzipaForeignEducationDocument.countryTitle()).order())
                .addColumn(actionColumn("downloadRecognition", CommonDefines.ICON_PRINT, "onClickDownloadRecognition")
                        .permissionKey("szipaEnr14EntrantPubForeignEducationTabPrintDocument"))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEdit")
                        .permissionKey("szipaEnr14EntrantPubForeignEducationTabEditDocument"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDelete", new FormattedMessage(DS_FOREIGN_EDUCATION_DOCUMENTS + ".delete.alert", SzipaForeignEducationDocument.personEduDocument().title().s()))
                        .permissionKey("szipaEnr14EntrantPubForeignEducationTabDeleteDocument"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> foreignEducationDSHandler()
    {
        return new EnrEntrantForeignEducationDocumentDSHandler(getName());
    }
}
