/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.logic;

import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber;

import java.util.List;

/**
 * @author Igor Belanov
 * @since 30.08.2016
 */
public interface ISzipaEnrOrderDao extends IEnrOrderDao
{
    /**
     * Обновляет данные протокола прицепленные к приказу
     * @param enrOrder приказ
     * @param newProtocolInfo новые данные протокола (null, если нужно просто всё поудалять и ничего не добавлять)
     */
    void updateOrderProtocolInfo(EnrOrder enrOrder, List<SzipaEnrOrderProtocolDateAndNumber> newProtocolInfo);
}
