/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 11.07.2016
 */
public class EnrEntrantForeignEducationDocumentDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String BIND_ENTRANT = "entrant";

    public EnrEntrantForeignEducationDocumentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEntrant entrant = context.get(BIND_ENTRANT);

        final DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SzipaForeignEducationDocument.class, "fe").column(property("fe"))
                .where(eq(property(SzipaForeignEducationDocument.personEduDocument().person().id().fromAlias("fe")), value(entrant.getPerson().getId())));

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().preserveCountRecord().build();

        if (output.getRecordIds().size() < 5)
            output.setCountRecord(5);

        return output;
    }
}
