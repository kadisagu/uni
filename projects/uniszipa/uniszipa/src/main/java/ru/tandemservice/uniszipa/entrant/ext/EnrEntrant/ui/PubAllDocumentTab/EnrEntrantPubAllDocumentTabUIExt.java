/* $Id$ */
package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant.ui.PubAllDocumentTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab.EnrEntrantPubAllDocumentTabUI;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrant.ui.BenefitDocumentRequestAdd.SzipaEnrEntrantBenefitDocumentRequestAdd;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrant.ui.EducationDocumentRequestAdd.SzipaEnrEntrantEducationDocumentRequestAdd;

/**
 * @author Denis Katkov
 * @since 08.06.2016
 */
public class EnrEntrantPubAllDocumentTabUIExt extends UIAddon
{
    public EnrEntrantPubAllDocumentTabUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickPrintEducationDocumentRequest(){
        getActivationBuilder().asRegionDialog(SzipaEnrEntrantEducationDocumentRequestAdd.class)
                .parameter(IEnrEntrantBenefitProofDocumentGen.L_ENTRANT, getParentPresenter().getEntrant())
                .activate();
    }

    public void onClickPrintBenefitDocumentRequest(){
        getActivationBuilder().asRegionDialog(SzipaEnrEntrantBenefitDocumentRequestAdd.class)
                .parameter(IEnrEntrantBenefitProofDocumentGen.L_ENTRANT, getParentPresenter().getEntrant())
                .activate();
    }

    public EnrEntrantPubAllDocumentTabUI getParentPresenter(){
        return this.getPresenter();
    }
}