/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.logic;

import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 08.08.2016
 */
public class EnrEntrantParagraphWrapper implements Comparable<EnrEntrantParagraphWrapper>
{
    private final List<EnrEntrantSubParagraphWrapper> _subParagraphWrapperList;
    private final EnrCompetitionType _competitionType;
    private final boolean _isForeignEntrant;
    private EduProgramForm _programForm;

    public EnrEntrantParagraphWrapper(EnrCompetitionType competitionType, boolean isForeignEntrant)
    {
        _subParagraphWrapperList = new ArrayList<>();
        _competitionType = competitionType;
        _isForeignEntrant = isForeignEntrant;
    }

    @Override
    public int hashCode()
    {
        return _competitionType.hashCode() & Boolean.hashCode(_isForeignEntrant);
    }

    @Override
    public boolean equals(Object anotherEnrEntrantParagraphWrapper)
    {
        return anotherEnrEntrantParagraphWrapper instanceof EnrEntrantParagraphWrapper &&
                _competitionType.equals(((EnrEntrantParagraphWrapper) anotherEnrEntrantParagraphWrapper).getCompetitionType()) &&
                (_isForeignEntrant == ((EnrEntrantParagraphWrapper) anotherEnrEntrantParagraphWrapper).isForeignEntrant());
    }

    @Override
    public int compareTo(EnrEntrantParagraphWrapper anotherEnrEntrantParagraphWrapper)
    {
        int foreignEntrantCompare = Boolean.compare(_isForeignEntrant, anotherEnrEntrantParagraphWrapper.isForeignEntrant());
        if (foreignEntrantCompare != 0) return foreignEntrantCompare;

        return _competitionType.getCode().compareTo(anotherEnrEntrantParagraphWrapper.getCompetitionType().getCode());
    }

    public List<EnrEntrantSubParagraphWrapper> getSubParagraphWrapperList()
    {
        return _subParagraphWrapperList;
    }

    public EnrCompetitionType getCompetitionType()
    {
        return _competitionType;
    }

    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    public void setProgramForm(EduProgramForm programForm)
    {
        _programForm = programForm;
    }

    public boolean isForeignEntrant()
    {
        return _isForeignEntrant;
    }
}
