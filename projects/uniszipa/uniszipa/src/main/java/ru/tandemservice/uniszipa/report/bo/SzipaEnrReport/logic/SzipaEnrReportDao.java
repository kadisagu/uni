/* $Id:$ */
package ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.uniszipa.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uniszipa.entity.report.EntrantDailyRequestsByCompetitions;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantDailyRequestsByCompetitionsAdd.SzipaEnrReportEntrantDailyRequestsByCompetitionsAddUI;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantRecordsStudentFormAdd.SzipaEnrReportEntrantRecordsStudentFormAddUI;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantStudentFormAdd.SzipaEnrReportEntrantStudentFormAddUI;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 12.08.2014
 */
public class SzipaEnrReportDao extends UniBaseDao implements ISzipaEnrReportDao
{
    // ---------- begin of EntrantDailyRequestsByCompetitions ---------------------------------------------------------
    @Override
    public Long createReportEntrantDailyRequestsByCompetitions(SzipaEnrReportEntrantDailyRequestsByCompetitionsAddUI model)
    {
        EntrantDailyRequestsByCompetitions report = new EntrantDailyRequestsByCompetitions();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EntrantDailyRequestsByCompetitions.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EntrantDailyRequestsByCompetitions.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EntrantDailyRequestsByCompetitions.P_PROGRAM_FORM, "title");
//        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EntrantDailyRequestsByCompetitions.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EntrantDailyRequestsByCompetitions.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EntrantDailyRequestsByCompetitions.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EntrantDailyRequestsByCompetitions.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EntrantDailyRequestsByCompetitions.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EntrantDailyRequestsByCompetitions.P_PROGRAM_SET, "title");

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReportEntrantDailyRequestsByCompetitions(model, EnrScriptItemCodes.REPORT_ENTRANT_DAILY_REQUESTS_BY_COMPETITIONS));
        content.setFilename("Сводка по заявлениям с указанием конкурса " + DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate()) + ".rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    public byte[] buildReportEntrantDailyRequestsByCompetitions(final SzipaEnrReportEntrantDailyRequestsByCompetitionsAddUI model, String templateCatalogCode)
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, templateCatalogCode);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        // нам нужны только из выбранной приемной компании и зарегистрировавшиеся в указанные даты
        EnrEntrantDQLSelectBuilder builder = new EnrEntrantDQLSelectBuilder(true, false)
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());
        // удовлетворяющие условиям дополнительных фильтров
        filterAddon.applyFilters(builder, builder.competition());
        // не архивные и не забравшие документы
        builder.where(eq(property(EnrEntrant.archival().fromAlias(builder.entrant())), value(Boolean.FALSE)))
               .where(ne(property(EnrRequestedCompetition.state().code().fromAlias(builder.reqComp())), value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)));
        // абитуриенты
        builder.column(builder.reqComp());

        List<EnrRequestedCompetition> reqCompList = getList(builder);

        Comparator<OrgUnit> formativeOrgUnitComparator = CommonCollator.comparing(OrgUnit::getPrintTitle, true);
        Comparator<EduProgramSubject> programSubjectComparator = (o1, o2) -> o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
        Comparator<EnrCompetition> compComparator = (o1, o2) -> {
            return - o1.getTitle().compareTo(o2.getTitle());
        };

        Comparator<EnrProgramSetItem> itemComparator = (o1, o2) -> {
            if (o1 == o2) return 0;
            if (o1 == null) return 1;
            if (o2 == null) return -1;

            int result = o1.getProgram().getForm().getCode().compareTo(o2.getProgram().getForm().getCode());
            if (result != 0) return result;

            return Integer.compare(o1.getProgram().getDuration().getNumberOfYears() * 12 + o1.getProgram().getDuration().getNumberOfMonths(),
                    o2.getProgram().getDuration().getNumberOfYears() * 12 + o2.getProgram().getDuration().getNumberOfMonths());
        };

        // ТриМапа Формирующее подразделение, Направление подготовки, Конкурс, Выбранный конкурс
        Map<OrgUnit, Map<EduProgramSubject, Map<EnrCompetition, Set<EnrRequestedCompetition>>>> dataMap = new TreeMap<>(formativeOrgUnitComparator);
        // нам еще будут нужны ОП
        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);

//      Формирующее подразделение-Направление подготовки, у которого планы приема ненулевые
        List<EnrProgramSetOrgUnit> programSetOrgUnitList = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetOrgUnit.class.getSimpleName(), "epsou")
                .column("epsou")
                .where(eqValue(property("epsou", EnrProgramSetOrgUnit.programSet().enrollmentCampaign()), model.getEnrollmentCampaign()))
//                .where(or(gt(property("epsou", EnrProgramSetOrgUnit.ministerialPlan()), value(0)),
//                        gt(property("epsou", EnrProgramSetOrgUnit.contractPlan()), value(0))))
                .order(property("epsou", EnrProgramSetOrgUnit.formativeOrgUnit().title()))
                .order(property("epsou", EnrProgramSetOrgUnit.programSet().programSubject().code()))
                .order(property("epsou", EnrProgramSetOrgUnit.programSet().programSubject().title()))
                .createStatement(getSession()).list();

        for(EnrProgramSetOrgUnit psOU : programSetOrgUnitList)
        {
            // формирующее подразделение
            OrgUnit formativeOrgUnit = psOU.getFormativeOrgUnit();
            // набор ОП на подразделении
            EnrProgramSetBase programSet = psOU.getProgramSet();
            // направление подготовки
            EduProgramSubject programSubject = programSet.getProgramSubject();
            // на самом верху - формирующее подразделение
            if (!dataMap.containsKey(formativeOrgUnit))
                dataMap.put(formativeOrgUnit, new TreeMap<>(programSubjectComparator));
            // дальше - направление подготовки
            if(!dataMap.get(formativeOrgUnit).containsKey(programSubject))
            {
                dataMap.get(formativeOrgUnit).put(programSubject, new TreeMap<>(compComparator));
                for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSet))
                    programMap.get(item.getProgramSet()).add(item.getProgram());
                if (programSet instanceof EnrProgramSetSecondary)
                    programMap.get(programSet).add(((EnrProgramSetSecondary)programSet).getProgram());
            }
        }

        //Выбираем конкурсы выбранного набора
        DQLSelectBuilder enrCompetitionSelectBuilder =  new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class.getSimpleName(), "comp")
                .column("comp")
                .where(eqValue(property("comp", EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), model.getEnrollmentCampaign()));
        // Применяем те же фильтры, что и для заявок на конкурсы
        filterAddon.applyFilters(enrCompetitionSelectBuilder, "comp");
        List<EnrCompetition> enrCompetitionList = getList(enrCompetitionSelectBuilder);

        // Заполняем dataMap конкурсами и заявками на них
        for (EnrCompetition competition : enrCompetitionList)
        {
            // аккредитованное подразделение ОУ
            EnrProgramSetOrgUnit programSetOrgUnit = competition.getProgramSetOrgUnit();
            // формирующее подразделение
            OrgUnit formativeOrgUnit = programSetOrgUnit.getFormativeOrgUnit();
            // набор ОП на подразделении
            EnrProgramSetBase programSet = programSetOrgUnit.getProgramSet();
            // направление подготовки
            EduProgramSubject programSubject = programSet.getProgramSubject();
            // Вставляем текущий конкурс
            dataMap.get(formativeOrgUnit).get(programSubject).put(competition, new HashSet<>());

            // Добавляем все заявления, с конкурсом competition
            dataMap.get(formativeOrgUnit).get(programSubject).get(competition)
                    .addAll(reqCompList.stream()
                            .filter(enrReqComp -> enrReqComp.getCompetition().equals(competition))
                            .collect(Collectors.toSet()));
        }
        reqCompList.clear();

        // достаем формирующие подразделения, они уже отсортированы
        List<OrgUnit> formativeOrgUnitList = new ArrayList<>(dataMap.keySet());

        List<String[]> tableData = new ArrayList<>();
        final Set<Integer> grayRowIndexes = new HashSet<>();
        final Set<Integer> boldHeaderRowIndexes = new HashSet<>();
        final Set<Integer> totalsRowIndexes = new HashSet<>();
        final int grayColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);

        List<EduProgramForm> programForms = UniDaoFacade.getCoreDao().getCatalogItemList(EduProgramForm.class);

        Map<EduProgramForm, RowData> ouTotals = new HashMap<>();
        ouTotals.put(null, new RowData());
        for (EduProgramForm programForm : programForms)
            ouTotals.put(programForm, new RowData("Итого по " + getProgramFormGenCaseTitle(programForm.getCode()) + ":"));

        Map<Object, RowData> totals = new HashMap<>();
        int rowIndex = 0;
        for (OrgUnit formOrgUnit : formativeOrgUnitList)
        {
            // заголовочная строка
            tableData.add(new String[] {formOrgUnit.getTitleWithType()});
            grayRowIndexes.add(rowIndex++);
            // итоговые строки
            totals.put(formOrgUnit, new RowData("", "", "", "Итого по факультету:"));
            for (EduProgramForm programForm : programForms)
                totals.put(programForm, new RowData("", "", "", "Итого по " + getProgramFormGenCaseTitle(programForm.getCode()) + ":"));

            // достаем направление подготовки, они уже отсортированы
            Map<EduProgramSubject, Map<EnrCompetition, Set<EnrRequestedCompetition>>> programSubjectMap = dataMap.get(formOrgUnit);
            List<EduProgramSubject> programSubjectList = new ArrayList<>(programSubjectMap.keySet());

            for (EduProgramSubject progSubj : programSubjectList)
            {
                // заголовочная строка
                tableData.add(new String[] {progSubj.getEduProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_NOMINATIVE) + " " + progSubj.getTitleWithCode()});
                boldHeaderRowIndexes.add(rowIndex++);

                // достаем конкурсы, они тоже отсортированы
                Map<EnrCompetition, Set<EnrRequestedCompetition>> competitionMap = programSubjectMap.get(progSubj);
                List<EnrCompetition> competitionList = new ArrayList<>(competitionMap.keySet());

                if (competitionList.isEmpty())
                {
                    // зато план приема ненулевой
                    for (EnrProgramSetOrgUnit psOU: programSetOrgUnitList)
                    {
                        if (psOU.getFormativeOrgUnit().equals(formOrgUnit) && (psOU.getProgramSet().getProgramSubject().equals(progSubj)))
                        {
                            EnrProgramSetBase progSetBaseThis = psOU.getProgramSet();
                            List<EduProgramProf> progProfList = programMap.get(progSetBaseThis);
                            EduProgramProf progProfThis = (progProfList.size() > 0 ? programMap.get(progSetBaseThis).get(0) : null);

                            if (psOU.isMinisterialOpen())
                            {
                                // есть прием на бюджет
                                RowData row = new RowData(
                                        progSubj.getCode() + " " + progSubj.getTitle() + ", " + progSubj.getEduProgramKind().getShortTitle(),
                                        "полный срок",
                                        getProgramFormNomCaseTitle(progSetBaseThis.getProgramForm().getCode()) + ", бюджет",
                                        (null != progProfThis ? progProfThis.getDuration().getTitle() : "н/д")
                                );
                                row.plan = psOU.getMinisterialPlan();
                                row.reqCount = 0;
                                row.origCount = 0;
                                row.atLastDay = 0;
                                tableData.add(row.printRow());
                                rowIndex++;
                                ouTotals.get(null).addRow(row);
                                ouTotals.get(progSetBaseThis.getProgramForm()).addRow(row);
                                totals.get(formOrgUnit).addRow(row);
                                totals.get(progSetBaseThis.getProgramForm()).addRow(row);
                            }
                            else if (psOU.isContractOpen())
                            {
                                // есть прием на платное
                                RowData row = new RowData(
                                        progSubj.getCode() + " " + progSubj.getTitle() + ", " + progSubj.getEduProgramKind().getShortTitle(),
                                        "полный срок",
                                        getProgramFormNomCaseTitle(progSetBaseThis.getProgramForm().getCode()) + ", платная",
                                        (null != progProfThis ? progProfThis.getDuration().getTitle() : "н/д")
                                );
                                row.plan = psOU.getContractPlan();
                                row.reqCount = 0;
                                row.origCount = 0;
                                row.atLastDay = 0;
                                tableData.add(row.printRow());
                                rowIndex++;
                                ouTotals.get(null).addRow(row);
                                ouTotals.get(progSetBaseThis.getProgramForm()).addRow(row);
                                totals.get(formOrgUnit).addRow(row);
                                totals.get(progSetBaseThis.getProgramForm()).addRow(row);
                            }
                        }
                    }
                }
                else
                {
                    for (EnrCompetition comp : competitionList)
                    {
                        Set<EnrRequestedCompetition> compSetThis = competitionMap.get(comp);
                        EnrProgramSetBase progSetBaseThis = comp.getProgramSetOrgUnit().getProgramSet();

                        EduProgramSubject progSubjThis = progSetBaseThis.getProgramSubject();
                        List<EduProgramProf> progProfList = programMap.get(progSetBaseThis);
                        EduProgramProf progProfThis = (progProfList.size() > 0 ? programMap.get(progSetBaseThis).get(0) : null);

                        EduProgramKind progKindThis = progSetBaseThis.getProgramKind();
                        CompensationType compenTypeThis = comp.getType().getCompensationType();

                        RowData row = new RowData(
                                progSubjThis.getCode() + " " + progSubjThis.getTitle() + ", " + progKindThis.getShortTitle(),
                                "полный срок",
                                getProgramFormNomCaseTitle(progSetBaseThis.getProgramForm().getCode()) + ", " +
                                        (compenTypeThis.isBudget() ? "бюджет" : "платная"),
                                (null != progProfThis ? progProfThis.getDuration().getTitle() : "н/д")
                        );

                        row.plan = compenTypeThis.isBudget() ? comp.getProgramSetOrgUnit().getMinisterialPlan() : comp.getProgramSetOrgUnit().getContractPlan();
                        row.reqCount = (null == compSetThis) ? 0 : compSetThis.size();
                        row.origCount = (null == compSetThis) ? 0 : CollectionUtils.countMatches(compSetThis, enrRequestedCompetition -> enrRequestedCompetition.isOriginalDocumentHandedIn());
                        row.atLastDay = (null == compSetThis) ? 0 : CollectionUtils.countMatches(compSetThis, enrRequestedCompetition -> DateUtils.isSameDay(model.getDateTo(), enrRequestedCompetition.getRequest().getRegDate()));
                        tableData.add(row.printRow());
                        rowIndex++;
                        ouTotals.get(null).addRow(row);
                        ouTotals.get(progSetBaseThis.getProgramForm()).addRow(row);
                        totals.get(formOrgUnit).addRow(row);
                        totals.get(progSetBaseThis.getProgramForm()).addRow(row);
                    }
                }
            }

            for (EduProgramForm programForm : programForms)
            {
                tableData.add(totals.get(programForm).printRow());
                totalsRowIndexes.add(rowIndex++);
            }
            tableData.add(totals.get(formOrgUnit).printRow());
            totalsRowIndexes.add(rowIndex++);
        }

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.toArray(new String[tableData.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (grayRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                else if (boldHeaderRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                else if (totalsRowIndexes.contains(rowIndex) && colIndex == 3)
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                else if (totalsRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList) {
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QL);
                    for (int i = 1; i < row.getCellList().size(); i++)
                        SharedRtfUtil.setCellAlignment(row.getCellList().get(i), IRtfData.QC);
                }

                for (Integer rowIndex : grayRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                    RtfCell cell = row.getCellList().get(0);
                    cell.setBackgroundColorIndex(grayColorIndex);
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                }

                for (Integer rowIndex : boldHeaderRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                }

                for (Integer rowIndex : totalsRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.deleteCellNextIncrease(row, 0);
                    RtfUtil.deleteCellNextIncrease(row, 0);
                    RtfUtil.deleteCellNextIncrease(row, 0);

                    RtfCell cell = row.getCellList().get(0);
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QR);
                }
            }
        });
        tableModifier.modify(document);

        tableData = new ArrayList<>();
        for (EduProgramForm programForm : programForms)
            tableData.add(ouTotals.get(programForm).printRow());

        tableModifier = new RtfTableModifier().put("T1", tableData.toArray(new String[tableData.size()][]));
        tableModifier.modify(document);

        RowData totalData = ouTotals.get(null);
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateTo()));
        injectModifier.put("T2", String.valueOf(totalData.plan));
        injectModifier.put("T3", String.valueOf(totalData.reqCount));
        injectModifier.put("T4", String.valueOf(totalData.atLastDay));
        injectModifier.put("T5", String.valueOf(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalData.plan == 0 ? .0 : (double) totalData.reqCount / totalData.plan)));
        injectModifier.put("T6", String.valueOf(totalData.origCount));
        injectModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private String getProgramFormNomCaseTitle(String code)
    {
        switch (code)
        {
            case EduProgramFormCodes.OCHNAYA:
                return "очная";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "заочная";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "очно-заочная";
        }
        return "";
    }

    private String getProgramFormGenCaseTitle(String code)
    {
        switch (code)
        {
            case EduProgramFormCodes.OCHNAYA:
                return "очной";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "заочной";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "очно-заочной";
        }
        return "";
    }

    private static class RowData
    {
        List<String> result = new ArrayList<>();
        int reqCount;
        int origCount;
        int plan;
        int atLastDay;

        private RowData(String... data)
        {
            Collections.addAll(result, data);
        }

        private String[] printRow()
        {
            result.add(String.valueOf(plan));
            result.add(String.valueOf(reqCount));
            result.add(String.valueOf(atLastDay));
            result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(plan == 0 ? .0 : (double) reqCount / plan));
            result.add(String.valueOf(origCount));
            return result.toArray(new String[result.size()]);
        }

        private void addRow(RowData row)
        {
            reqCount += row.reqCount;
            origCount += row.origCount;
            plan += row.plan;
            atLastDay += row.atLastDay;
        }
    }
    // ---------- end of EntrantDailyRequestsByCompetitions ------------------------------------------------------------

    // ---------- begin of EntrantStudentForm --------------------------------------------------------------------------
    @Override
    public Long createReportEntrantStudentForm(SzipaEnrReportEntrantStudentFormAddUI model)
    {
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        // нам нужны только из выбранной приемной компании и зарегистрировавшиеся в указанные даты
        EnrEntrantDQLSelectBuilder builder = new EnrEntrantDQLSelectBuilder(false, false)
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());
        // удовлетворяющие условиям дополнительных фильтров
        filterAddon.applyFilters(builder, builder.competition());
        // желающие обучаться параллельно или не очень
        if (model.isParallelActive())
        {
            if (1L == model.getParallel().getId())
            {
                builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(builder.reqComp())), value(Boolean.TRUE)));
            }
            else if (0L == model.getParallel().getId())
            {
                builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(builder.reqComp())), value(Boolean.FALSE)));
            }
        }
        // и уже зачисленные
        builder.where(eq(property(EnrRequestedCompetition.state().code().fromAlias(builder.reqComp())), value(EnrEntrantStateCodes.ENROLLED)));
        builder.where(exists(EnrEnrollmentExtract.class,
                EnrEnrollmentExtract.entity().s(), property(builder.reqComp()),
                EnrEnrollmentExtract.state().code().s(), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)));

        // и отсортированные по ФИО абитуриенты
        builder.joinPath(DQLJoinType.inner, EnrEntrant.person().identityCard().fromAlias(builder.entrant()), "card", true)
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME));

        builder.column("reqComp.id");

        Iterator<Long> iterator = createStatement(builder).<Long>list().iterator();
        if (!iterator.hasNext())
            throw new ApplicationException("Данные для построения отчета отсутствуют.");

        // дабы не плодить дабл-анкеты
        Long reqCompId = iterator.next();
        EnrEntrant entrant = get(EnrRequestedCompetition.class, reqCompId).getRequest().getEntrant();

        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        final IRtfControl pageBreak = elementFactory.createRtfControl(IRtfData.PAGE);
        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_STUDENT_FORM);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, reqCompId);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        final List<IRtfElement> mainElementList = mainDoc.getElementList();

        while (iterator.hasNext())
        {
            Long nextReqCompId = iterator.next();
            EnrEntrant nextEntrant = get(EnrRequestedCompetition.class, nextReqCompId).getRequest().getEntrant();
            if (!nextEntrant.equals(entrant))
            {
                mainElementList.add(pageBreak);
                scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, nextReqCompId);
                RtfDocument nextDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
                mainElementList.addAll(nextDoc.getElementList());
            }
            entrant = nextEntrant;
        }
        model.setReport(mainDoc);
        return 1L;
    }
    // ---------- end of EntrantStudentForm ----------------------------------------------------------------------------

    @Override
    public Long createReportEntrantRecordsStudentForm(SzipaEnrReportEntrantRecordsStudentFormAddUI model)
    {
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        // нам нужны только из выбранной приемной компании и зарегистрировавшиеся в указанные даты
        EnrEntrantDQLSelectBuilder builder = new EnrEntrantDQLSelectBuilder(false, false)
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());
        // удовлетворяющие условиям дополнительных фильтров
        filterAddon.applyFilters(builder, builder.competition());
        // желающие обучаться параллельно или не очень
        if (model.isParallelActive()) {
            if (1L == model.getParallel().getId()) {
                builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(builder.reqComp())), value(Boolean.TRUE)));
            } else if (0L == model.getParallel().getId()) {
                builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(builder.reqComp())), value(Boolean.FALSE)));
            }
        }
        // и уже зачисленные
        builder.where(eq(property(EnrRequestedCompetition.state().code().fromAlias(builder.reqComp())), value(EnrEntrantStateCodes.ENROLLED)));
        builder.where(exists(EnrEnrollmentExtract.class,
                EnrEnrollmentExtract.entity().s(), property(builder.reqComp()),
                EnrEnrollmentExtract.state().code().s(), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)));

        // и отсортированные по ФИО абитуриенты
        builder.joinPath(DQLJoinType.inner, EnrEntrant.person().identityCard().fromAlias(builder.entrant()), "card", true)
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME));

        builder.column("reqComp.id");

        Iterator<Long> iterator = createStatement(builder).<Long>list().iterator();
        if (!iterator.hasNext())
            throw new ApplicationException("Данные для построения отчета отсутствуют.");

        // дабы не плодить дабл-анкеты
        Long reqCompId = iterator.next();
        EnrEntrant entrant = get(EnrRequestedCompetition.class, reqCompId).getRequest().getEntrant();

        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        final IRtfControl pageBreak = elementFactory.createRtfControl(IRtfData.PAGE);
        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_RECORDS_AND_STATEMENTS_ENTRANT_STUDENT_FORM);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, reqCompId);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        final List<IRtfElement> mainElementList = mainDoc.getElementList();

        while (iterator.hasNext()) {
            Long nextReqCompId = iterator.next();
            EnrEntrant nextEntrant = get(EnrRequestedCompetition.class, nextReqCompId).getRequest().getEntrant();
            if (!nextEntrant.equals(entrant)) {
                mainElementList.add(pageBreak);
                scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, nextReqCompId);
                RtfDocument nextDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
                mainElementList.addAll(nextDoc.getElementList());
            }
            entrant = nextEntrant;
        }
        model.setReport(mainDoc);
        return 1L;
    }
}
