/* $Id:$ */
package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTab;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;

/**
 * @author Denis Perminov
 * @since 27.06.2014
 */
@Configuration
public class EnrEntrantPubRequestTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantPubRequestTab _enrEntrantPubRequestTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantPubRequestTab.presenterExtPoint())
                .replaceAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, SzipaEntrantRequestActionsAddon.class))
                .addAction(new EntrantRecieptActionPrint("onClickPrintReceipt"))
                .create();
    }
}