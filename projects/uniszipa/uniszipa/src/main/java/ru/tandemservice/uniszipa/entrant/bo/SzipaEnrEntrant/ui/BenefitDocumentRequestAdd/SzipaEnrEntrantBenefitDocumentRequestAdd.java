/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrant.ui.BenefitDocumentRequestAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 08.06.2016
 */
@Configuration
public class SzipaEnrEntrantBenefitDocumentRequestAdd extends BusinessComponentManager
{
    public static final String BENEFIT_DOCUMENT_DS = "benefitDocumentDS";
    public static final String BENEFIT_DOCUMENT = "benefitDocument";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(benefitDocumentDSConfig())
                .create();
    }

    @Bean
    public UIDataSourceConfig benefitDocumentDSConfig()
    {
        return SelectDSConfig.with(BENEFIT_DOCUMENT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(benefitDocumentDSHandler())
                .addColumn(IEnrEntrantBenefitProofDocumentGen.documentType().title().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler benefitDocumentDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), IEnrEntrantBenefitProofDocument.class)
                .customize((alias, dql, context, filter) -> {
                    EnrEntrant entrant = context.get(IEnrEntrantBenefitProofDocumentGen.L_ENTRANT);
                    return dql.where(eq(property(IEnrEntrantBenefitProofDocumentGen.entrant().id().fromAlias(alias)), value(entrant.getId())));
                })
                .order(IEnrEntrantBenefitProofDocumentGen.documentType().title())
                .filter(IEnrEntrantBenefitProofDocumentGen.documentType().title());
    }
}