/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEdit;

/**
 * @author Igor Belanov
 * @since 04.08.2016
 */
@Configuration
public class EnrOrderAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniszipa" + EnrOrderAddEditUIExt.class.getSimpleName();
    public static final String APPLY_BUTTON_LISTENER = ADDON_NAME + ":onClickApply";

    @Autowired
    private EnrOrderAddEdit _enrOrderAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrOrderAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrOrderAddEditUIExt.class))
                .create();
    }

    @Bean
    public ButtonListExtension applyButtonList() {
        return buttonListExtensionBuilder(_enrOrderAddEdit.applyButtonList())
                .overwriteButton(submitButton(EnrOrderAddEdit.APPLY_BUTTON).listener(APPLY_BUTTON_LISTENER))
                .create();
    }
}
