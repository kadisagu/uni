package ru.tandemservice.uniszipa.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Подвид"
 * Имя сущности : ctrPrintTemplate
 * Файл data.xml : uniszipa-templates.data.xml
 */
public interface CtrPrintTemplateCodes
{
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_VO_2_SIDES = "edu.ctr.template.vo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON = "edu.ctr.template.vo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG = "edu.ctr.template.vo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_SPO_2_SIDES = "edu.ctr.template.spo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON = "edu.ctr.template.spo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG = "edu.ctr.template.spo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_DPO_2_SIDES = "edu.ctr.template.dpo.2s";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_DPO_3_SIDES_PERSON = "edu.ctr.template.dpo.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CONTRACT_TEMPLATE_DPO_3_SIDES_ORG = "edu.ctr.template.dpo.3so";
    /** Константа кода (code) элемента : Основной (title) */
    String TERM_EDU_COST_TEMPLATE_2_SIDES = "edu.ctr.template.termeducost.2s";
    /** Константа кода (code) элемента : Основной (title) */
    String TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON = "edu.ctr.template.termeducost.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    String TERM_EDU_COST_TEMPLATE_3_SIDES_ORG = "edu.ctr.template.termeducost.3so";
    /** Константа кода (code) элемента : Основной (title) */
    String PROGRAM_CHANGE_TEMPLATE_2_SIDES = "edu.ctr.template.programchange.2s";
    /** Константа кода (code) элемента : Основной (title) */
    String PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON = "edu.ctr.template.programchange.3sp";
    /** Константа кода (code) элемента : Основной (title) */
    String PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG = "edu.ctr.template.programchange.3so";
    /** Константа кода (code) элемента : Основной (title) */
    String EDU_CTR_STOP_TEMPLATE_CONTRACT = "edu.ctr.template.stopcontract";

    Set<String> CODES = ImmutableSet.of(EDU_CONTRACT_TEMPLATE_VO_2_SIDES, EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON, EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG, EDU_CONTRACT_TEMPLATE_SPO_2_SIDES, EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON, EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG, EDU_CONTRACT_TEMPLATE_DPO_2_SIDES, EDU_CONTRACT_TEMPLATE_DPO_3_SIDES_PERSON, EDU_CONTRACT_TEMPLATE_DPO_3_SIDES_ORG, TERM_EDU_COST_TEMPLATE_2_SIDES, TERM_EDU_COST_TEMPLATE_3_SIDES_PERSON, TERM_EDU_COST_TEMPLATE_3_SIDES_ORG, PROGRAM_CHANGE_TEMPLATE_2_SIDES, PROGRAM_CHANGE_TEMPLATE_3_SIDES_PERSON, PROGRAM_CHANGE_TEMPLATE_3_SIDES_ORG, EDU_CTR_STOP_TEMPLATE_CONTRACT);
}
