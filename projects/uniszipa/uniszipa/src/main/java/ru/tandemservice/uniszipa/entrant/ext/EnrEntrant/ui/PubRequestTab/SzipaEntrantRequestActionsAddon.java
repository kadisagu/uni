package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.IUIPresenter;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;

/**
 * Created by nsvetlov on 26.05.2016.
 */
public class SzipaEntrantRequestActionsAddon extends EnrEntrantRequestActionsAddon {
    public SzipaEntrantRequestActionsAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}
