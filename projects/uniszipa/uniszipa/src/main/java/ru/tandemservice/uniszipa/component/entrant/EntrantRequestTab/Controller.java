/* $Id:$ */
package ru.tandemservice.uniszipa.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author oleyba
 * @since 6/6/12
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Controller
{
    @Override
    public void onClickPrintEnrollmentExamSheet(IBusinessComponent component)
    {
        EntrantRequest entrantRequest = getDao().get(EntrantRequest.class, component.<Long>getListenerParameter());

        if (entrantRequest.getEntrant().getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_REQUEST))
            throw new ApplicationException("Нельзя напечатать заявление абитуриента, т.к. в опциях приемной кампании выбрано формирование экзаменационного листа для абитуриента по каждому заявлению отдельно.");

        super.onClickPrintEnrollmentExamSheet(component);
    }
}
