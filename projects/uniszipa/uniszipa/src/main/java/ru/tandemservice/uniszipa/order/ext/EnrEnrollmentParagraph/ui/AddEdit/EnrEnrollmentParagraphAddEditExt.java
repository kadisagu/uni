/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrEnrollmentParagraph.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEdit;

/**
 * @author Igor Belanov
 * @since 06.08.2016
 */
@Configuration
public class EnrEnrollmentParagraphAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniszipa" + EnrEnrollmentParagraphAddEditUIExt.class.getSimpleName();

    @Autowired
    private EnrEnrollmentParagraphAddEdit _enrEnrollmentParagraphAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEnrollmentParagraphAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEnrollmentParagraphAddEditUIExt.class))
                .create();
    }
}
