/* $Id:$ */
package ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantStudentFormAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.process.*;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.GlobalList.EnrReportBaseGlobalList;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.SzipaEnrReportManager;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 19.08.2014
 */
public class SzipaEnrReportEntrantStudentFormAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign enrollmentCampaign;
    private Date dateFrom;
    private Date dateTo;
    private boolean parallelActive;
    private IdentifiableWrapper<IEntity> parallel;
    private List<IdentifiableWrapper> parallelList;

    private long reportId;
    private RtfDocument report;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SzipaEnrReportEntrantStudentFormAdd.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (reportId != 0)
        {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                    .rtf()
                    .fileName("Анкеты зачисленных абитуриентов.rtf")
                    .document(getReport()), true);
            reportId = 0L;
        }
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        util.saveSettings();

        configUtilWhere(util);
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setDateFrom(null == getEnrollmentCampaign() ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(null == getEnrollmentCampaign() ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());

        setParallelList(Arrays.asList(new IdentifiableWrapper[] {
                new IdentifiableWrapper<>(0L, "Не учитывать выбранные параллельно условия поступления"),
                new IdentifiableWrapper<>(1L, "Включать в отчет только выбранные параллельно условия поступления")
                }));
    }

    public void onClickApply()
    {
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                setReportId(SzipaEnrReportManager.instance().dao().createReportEntrantStudentForm(SzipaEnrReportEntrantStudentFormAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    public void onClickCancel()
    {
        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    public boolean isParallelActive()
    {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        this.parallelActive = parallelActive;
    }

    public IdentifiableWrapper<IEntity> getParallel()
    {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper<IEntity> parallel)
    {
        this.parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        this.parallelList = parallelList;
    }

    public long getReportId()
    {
        return reportId;
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    public RtfDocument getReport()
    {
        return report;
    }

    public void setReport(RtfDocument report)
    {
        this.report = report;
    }
}
