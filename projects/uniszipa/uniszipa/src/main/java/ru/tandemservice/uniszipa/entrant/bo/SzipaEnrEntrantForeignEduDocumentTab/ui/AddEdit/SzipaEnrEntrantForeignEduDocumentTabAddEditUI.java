/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.logic.EnrEntrantForeignEducationDocumentDSHandler;

import java.util.Date;

/**
 * @author Igor Belanov
 * @since 11.07.2016
 */
@Input({
        @Bind(key = SzipaEnrEntrantForeignEduDocumentTabAddEditUI.BIND_ENTRANT_ID, binding = "entrant.id", required = true),
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "foreignEducationDocument.id")
})
public class SzipaEnrEntrantForeignEduDocumentTabAddEditUI extends UIPresenter
{
    public static final String BIND_ENTRANT_ID = "entrantId";
    private EnrEntrant _entrant = new EnrEntrant();
    private SzipaForeignEducationDocument _foreignEducationDocument = new SzipaForeignEducationDocument();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        if (!isAddForm())
        {
            setForeignEducationDocument(IUniBaseDao.instance.get().getNotNull(SzipaForeignEducationDocument.class, getForeignEducationDocument().getId()));
        } else if (getEntrant() != null)
        {
            getForeignEducationDocument().setFio(getEntrant().getFullFio());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantForeignEducationDocumentDSHandler.BIND_ENTRANT, getEntrant());
    }

    public void onClickApply()
    {
        if (isAddForm()) getForeignEducationDocument().setCreateDate(new Date());
        DataAccessServices.dao().saveOrUpdate(getForeignEducationDocument());
        deactivate();
    }

    public boolean isAddForm()
    {
        return getForeignEducationDocument().getId() == null;
    }

    // getters & setters

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this._entrant = entrant;
    }

    public SzipaForeignEducationDocument getForeignEducationDocument()
    {
        return _foreignEducationDocument;
    }

    public void setForeignEducationDocument(SzipaForeignEducationDocument foreignEducationDocument)
    {
        this._foreignEducationDocument = foreignEducationDocument;
    }
}
