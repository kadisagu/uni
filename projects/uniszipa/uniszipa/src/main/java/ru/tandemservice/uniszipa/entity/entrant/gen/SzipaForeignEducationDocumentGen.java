package ru.tandemservice.uniszipa.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ иностранного образования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SzipaForeignEducationDocumentGen extends EntityBase
 implements INaturalIdentifiable<SzipaForeignEducationDocumentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument";
    public static final String ENTITY_NAME = "szipaForeignEducationDocument";
    public static final int VERSION_HASH = 2058656685;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON_EDU_DOCUMENT = "personEduDocument";
    public static final String P_NUMBER_OF_PAGES = "numberOfPages";
    public static final String P_FIO = "fio";
    public static final String P_EDUCATION_PERIOD = "educationPeriod";
    public static final String P_ATTACHMENTS = "attachments";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_COUNTRY_TITLE = "countryTitle";
    public static final String P_DOCUMENT_TYPE_TITLE = "documentTypeTitle";
    public static final String P_SERIA_AND_NUMBER = "seriaAndNumber";

    private PersonEduDocument _personEduDocument;     // Документ об образовании и (или) квалификации
    private String _numberOfPages;     // Число листов приложения
    private String _fio;     // ФИО
    private String _educationPeriod;     // Период обучения
    private String _attachments;     // Приложения
    private Date _createDate;     // Дата создания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonEduDocument getPersonEduDocument()
    {
        return _personEduDocument;
    }

    /**
     * @param personEduDocument Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     */
    public void setPersonEduDocument(PersonEduDocument personEduDocument)
    {
        dirty(_personEduDocument, personEduDocument);
        _personEduDocument = personEduDocument;
    }

    /**
     * @return Число листов приложения.
     */
    @Length(max=255)
    public String getNumberOfPages()
    {
        return _numberOfPages;
    }

    /**
     * @param numberOfPages Число листов приложения.
     */
    public void setNumberOfPages(String numberOfPages)
    {
        dirty(_numberOfPages, numberOfPages);
        _numberOfPages = numberOfPages;
    }

    /**
     * @return ФИО. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio ФИО. Свойство не может быть null.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Период обучения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationPeriod()
    {
        return _educationPeriod;
    }

    /**
     * @param educationPeriod Период обучения. Свойство не может быть null.
     */
    public void setEducationPeriod(String educationPeriod)
    {
        dirty(_educationPeriod, educationPeriod);
        _educationPeriod = educationPeriod;
    }

    /**
     * @return Приложения.
     */
    @Length(max=255)
    public String getAttachments()
    {
        return _attachments;
    }

    /**
     * @param attachments Приложения.
     */
    public void setAttachments(String attachments)
    {
        dirty(_attachments, attachments);
        _attachments = attachments;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SzipaForeignEducationDocumentGen)
        {
            if (withNaturalIdProperties)
            {
                setPersonEduDocument(((SzipaForeignEducationDocument)another).getPersonEduDocument());
            }
            setNumberOfPages(((SzipaForeignEducationDocument)another).getNumberOfPages());
            setFio(((SzipaForeignEducationDocument)another).getFio());
            setEducationPeriod(((SzipaForeignEducationDocument)another).getEducationPeriod());
            setAttachments(((SzipaForeignEducationDocument)another).getAttachments());
            setCreateDate(((SzipaForeignEducationDocument)another).getCreateDate());
        }
    }

    public INaturalId<SzipaForeignEducationDocumentGen> getNaturalId()
    {
        return new NaturalId(getPersonEduDocument());
    }

    public static class NaturalId extends NaturalIdBase<SzipaForeignEducationDocumentGen>
    {
        private static final String PROXY_NAME = "SzipaForeignEducationDocumentNaturalProxy";

        private Long _personEduDocument;

        public NaturalId()
        {}

        public NaturalId(PersonEduDocument personEduDocument)
        {
            _personEduDocument = ((IEntity) personEduDocument).getId();
        }

        public Long getPersonEduDocument()
        {
            return _personEduDocument;
        }

        public void setPersonEduDocument(Long personEduDocument)
        {
            _personEduDocument = personEduDocument;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SzipaForeignEducationDocumentGen.NaturalId) ) return false;

            SzipaForeignEducationDocumentGen.NaturalId that = (NaturalId) o;

            if( !equals(getPersonEduDocument(), that.getPersonEduDocument()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPersonEduDocument());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPersonEduDocument());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SzipaForeignEducationDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SzipaForeignEducationDocument.class;
        }

        public T newInstance()
        {
            return (T) new SzipaForeignEducationDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "personEduDocument":
                    return obj.getPersonEduDocument();
                case "numberOfPages":
                    return obj.getNumberOfPages();
                case "fio":
                    return obj.getFio();
                case "educationPeriod":
                    return obj.getEducationPeriod();
                case "attachments":
                    return obj.getAttachments();
                case "createDate":
                    return obj.getCreateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "personEduDocument":
                    obj.setPersonEduDocument((PersonEduDocument) value);
                    return;
                case "numberOfPages":
                    obj.setNumberOfPages((String) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "educationPeriod":
                    obj.setEducationPeriod((String) value);
                    return;
                case "attachments":
                    obj.setAttachments((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "personEduDocument":
                        return true;
                case "numberOfPages":
                        return true;
                case "fio":
                        return true;
                case "educationPeriod":
                        return true;
                case "attachments":
                        return true;
                case "createDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "personEduDocument":
                    return true;
                case "numberOfPages":
                    return true;
                case "fio":
                    return true;
                case "educationPeriod":
                    return true;
                case "attachments":
                    return true;
                case "createDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "personEduDocument":
                    return PersonEduDocument.class;
                case "numberOfPages":
                    return String.class;
                case "fio":
                    return String.class;
                case "educationPeriod":
                    return String.class;
                case "attachments":
                    return String.class;
                case "createDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SzipaForeignEducationDocument> _dslPath = new Path<SzipaForeignEducationDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SzipaForeignEducationDocument");
    }
            

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getPersonEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> personEduDocument()
    {
        return _dslPath.personEduDocument();
    }

    /**
     * @return Число листов приложения.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getNumberOfPages()
     */
    public static PropertyPath<String> numberOfPages()
    {
        return _dslPath.numberOfPages();
    }

    /**
     * @return ФИО. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Период обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getEducationPeriod()
     */
    public static PropertyPath<String> educationPeriod()
    {
        return _dslPath.educationPeriod();
    }

    /**
     * @return Приложения.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getAttachments()
     */
    public static PropertyPath<String> attachments()
    {
        return _dslPath.attachments();
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getCountryTitle()
     */
    public static SupportedPropertyPath<String> countryTitle()
    {
        return _dslPath.countryTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getDocumentTypeTitle()
     */
    public static SupportedPropertyPath<String> documentTypeTitle()
    {
        return _dslPath.documentTypeTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getSeriaAndNumber()
     */
    public static SupportedPropertyPath<String> seriaAndNumber()
    {
        return _dslPath.seriaAndNumber();
    }

    public static class Path<E extends SzipaForeignEducationDocument> extends EntityPath<E>
    {
        private PersonEduDocument.Path<PersonEduDocument> _personEduDocument;
        private PropertyPath<String> _numberOfPages;
        private PropertyPath<String> _fio;
        private PropertyPath<String> _educationPeriod;
        private PropertyPath<String> _attachments;
        private PropertyPath<Date> _createDate;
        private SupportedPropertyPath<String> _countryTitle;
        private SupportedPropertyPath<String> _documentTypeTitle;
        private SupportedPropertyPath<String> _seriaAndNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getPersonEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> personEduDocument()
        {
            if(_personEduDocument == null )
                _personEduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_PERSON_EDU_DOCUMENT, this);
            return _personEduDocument;
        }

    /**
     * @return Число листов приложения.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getNumberOfPages()
     */
        public PropertyPath<String> numberOfPages()
        {
            if(_numberOfPages == null )
                _numberOfPages = new PropertyPath<String>(SzipaForeignEducationDocumentGen.P_NUMBER_OF_PAGES, this);
            return _numberOfPages;
        }

    /**
     * @return ФИО. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(SzipaForeignEducationDocumentGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Период обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getEducationPeriod()
     */
        public PropertyPath<String> educationPeriod()
        {
            if(_educationPeriod == null )
                _educationPeriod = new PropertyPath<String>(SzipaForeignEducationDocumentGen.P_EDUCATION_PERIOD, this);
            return _educationPeriod;
        }

    /**
     * @return Приложения.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getAttachments()
     */
        public PropertyPath<String> attachments()
        {
            if(_attachments == null )
                _attachments = new PropertyPath<String>(SzipaForeignEducationDocumentGen.P_ATTACHMENTS, this);
            return _attachments;
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(SzipaForeignEducationDocumentGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getCountryTitle()
     */
        public SupportedPropertyPath<String> countryTitle()
        {
            if(_countryTitle == null )
                _countryTitle = new SupportedPropertyPath<String>(SzipaForeignEducationDocumentGen.P_COUNTRY_TITLE, this);
            return _countryTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getDocumentTypeTitle()
     */
        public SupportedPropertyPath<String> documentTypeTitle()
        {
            if(_documentTypeTitle == null )
                _documentTypeTitle = new SupportedPropertyPath<String>(SzipaForeignEducationDocumentGen.P_DOCUMENT_TYPE_TITLE, this);
            return _documentTypeTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument#getSeriaAndNumber()
     */
        public SupportedPropertyPath<String> seriaAndNumber()
        {
            if(_seriaAndNumber == null )
                _seriaAndNumber = new SupportedPropertyPath<String>(SzipaForeignEducationDocumentGen.P_SERIA_AND_NUMBER, this);
            return _seriaAndNumber;
        }

        public Class getEntityClass()
        {
            return SzipaForeignEducationDocument.class;
        }

        public String getEntityName()
        {
            return "szipaForeignEducationDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getCountryTitle();

    public abstract String getDocumentTypeTitle();

    public abstract String getSeriaAndNumber();
}
