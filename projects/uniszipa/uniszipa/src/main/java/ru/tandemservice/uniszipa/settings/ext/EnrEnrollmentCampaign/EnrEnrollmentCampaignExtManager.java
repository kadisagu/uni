/* $Id$ */
package ru.tandemservice.uniszipa.settings.ext.EnrEnrollmentCampaign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.IEnrCampaignDao;
import ru.tandemservice.uniszipa.settings.ext.EnrEnrollmentCampaign.logic.EnrCampaignExtDao;

/**
 * @author Andrey Avetisov
 * @since 06.04.2015
 */
@Configuration
public class EnrEnrollmentCampaignExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrCampaignDao enrCampaignDAO()
    {
        return new EnrCampaignExtDao();
    }
}
