package ru.tandemservice.uniszipa.entity.entrantorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дата и номер протокола для приказа (или параграфа) о зачислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SzipaEnrOrderProtocolDateAndNumberGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber";
    public static final String ENTITY_NAME = "szipaEnrOrderProtocolDateAndNumber";
    public static final int VERSION_HASH = 1896841845;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ORDER = "enrOrder";
    public static final String L_ENR_ENROLLMENT_PARAGRAPH = "enrEnrollmentParagraph";
    public static final String P_PRIORITY = "priority";
    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";

    private EnrOrder _enrOrder;     // Приказ по абитуриенту
    private EnrEnrollmentParagraph _enrEnrollmentParagraph;     // Параграф приказа по абитуриенту
    private int _priority;     // Порядок следования в тексте
    private Date _protocolDate;     // Дата протокола
    private String _protocolNumber;     // Номер протокола

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ по абитуриенту.
     */
    public EnrOrder getEnrOrder()
    {
        return _enrOrder;
    }

    /**
     * @param enrOrder Приказ по абитуриенту.
     */
    public void setEnrOrder(EnrOrder enrOrder)
    {
        dirty(_enrOrder, enrOrder);
        _enrOrder = enrOrder;
    }

    /**
     * @return Параграф приказа по абитуриенту.
     */
    public EnrEnrollmentParagraph getEnrEnrollmentParagraph()
    {
        return _enrEnrollmentParagraph;
    }

    /**
     * @param enrEnrollmentParagraph Параграф приказа по абитуриенту.
     */
    public void setEnrEnrollmentParagraph(EnrEnrollmentParagraph enrEnrollmentParagraph)
    {
        dirty(_enrEnrollmentParagraph, enrEnrollmentParagraph);
        _enrEnrollmentParagraph = enrEnrollmentParagraph;
    }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Порядок следования в тексте. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Дата протокола.
     */
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SzipaEnrOrderProtocolDateAndNumberGen)
        {
            setEnrOrder(((SzipaEnrOrderProtocolDateAndNumber)another).getEnrOrder());
            setEnrEnrollmentParagraph(((SzipaEnrOrderProtocolDateAndNumber)another).getEnrEnrollmentParagraph());
            setPriority(((SzipaEnrOrderProtocolDateAndNumber)another).getPriority());
            setProtocolDate(((SzipaEnrOrderProtocolDateAndNumber)another).getProtocolDate());
            setProtocolNumber(((SzipaEnrOrderProtocolDateAndNumber)another).getProtocolNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SzipaEnrOrderProtocolDateAndNumberGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SzipaEnrOrderProtocolDateAndNumber.class;
        }

        public T newInstance()
        {
            return (T) new SzipaEnrOrderProtocolDateAndNumber();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrOrder":
                    return obj.getEnrOrder();
                case "enrEnrollmentParagraph":
                    return obj.getEnrEnrollmentParagraph();
                case "priority":
                    return obj.getPriority();
                case "protocolDate":
                    return obj.getProtocolDate();
                case "protocolNumber":
                    return obj.getProtocolNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrOrder":
                    obj.setEnrOrder((EnrOrder) value);
                    return;
                case "enrEnrollmentParagraph":
                    obj.setEnrEnrollmentParagraph((EnrEnrollmentParagraph) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrOrder":
                        return true;
                case "enrEnrollmentParagraph":
                        return true;
                case "priority":
                        return true;
                case "protocolDate":
                        return true;
                case "protocolNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrOrder":
                    return true;
                case "enrEnrollmentParagraph":
                    return true;
                case "priority":
                    return true;
                case "protocolDate":
                    return true;
                case "protocolNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrOrder":
                    return EnrOrder.class;
                case "enrEnrollmentParagraph":
                    return EnrEnrollmentParagraph.class;
                case "priority":
                    return Integer.class;
                case "protocolDate":
                    return Date.class;
                case "protocolNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SzipaEnrOrderProtocolDateAndNumber> _dslPath = new Path<SzipaEnrOrderProtocolDateAndNumber>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SzipaEnrOrderProtocolDateAndNumber");
    }
            

    /**
     * @return Приказ по абитуриенту.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getEnrOrder()
     */
    public static EnrOrder.Path<EnrOrder> enrOrder()
    {
        return _dslPath.enrOrder();
    }

    /**
     * @return Параграф приказа по абитуриенту.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getEnrEnrollmentParagraph()
     */
    public static EnrEnrollmentParagraph.Path<EnrEnrollmentParagraph> enrEnrollmentParagraph()
    {
        return _dslPath.enrEnrollmentParagraph();
    }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    public static class Path<E extends SzipaEnrOrderProtocolDateAndNumber> extends EntityPath<E>
    {
        private EnrOrder.Path<EnrOrder> _enrOrder;
        private EnrEnrollmentParagraph.Path<EnrEnrollmentParagraph> _enrEnrollmentParagraph;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<String> _protocolNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ по абитуриенту.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getEnrOrder()
     */
        public EnrOrder.Path<EnrOrder> enrOrder()
        {
            if(_enrOrder == null )
                _enrOrder = new EnrOrder.Path<EnrOrder>(L_ENR_ORDER, this);
            return _enrOrder;
        }

    /**
     * @return Параграф приказа по абитуриенту.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getEnrEnrollmentParagraph()
     */
        public EnrEnrollmentParagraph.Path<EnrEnrollmentParagraph> enrEnrollmentParagraph()
        {
            if(_enrEnrollmentParagraph == null )
                _enrEnrollmentParagraph = new EnrEnrollmentParagraph.Path<EnrEnrollmentParagraph>(L_ENR_ENROLLMENT_PARAGRAPH, this);
            return _enrEnrollmentParagraph;
        }

    /**
     * @return Порядок следования в тексте. Свойство не может быть null.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(SzipaEnrOrderProtocolDateAndNumberGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(SzipaEnrOrderProtocolDateAndNumberGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(SzipaEnrOrderProtocolDateAndNumberGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

        public Class getEntityClass()
        {
            return SzipaEnrOrderProtocolDateAndNumber.class;
        }

        public String getEntityName()
        {
            return "szipaEnrOrderProtocolDateAndNumber";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
