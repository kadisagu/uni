/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrant.ui.EducationDocumentRequestAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 08.06.2016
 */
@Configuration
public class SzipaEnrEntrantEducationDocumentRequestAdd extends BusinessComponentManager
{
    public static final String EDU_DOCUMENT_DS = "eduDocumentDS";
    public static final String EDU_DOCUMENT = "eduDocument";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(eduDocumentDSConfig())
                .create();
    }

    @Bean
    public UIDataSourceConfig eduDocumentDSConfig()
    {
        return SelectDSConfig.with(EDU_DOCUMENT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eduDocumentDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduDocumentDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PersonEduDocument.class)
                .customize((alias, dql, context, filter) -> {
                    EnrEntrant entrant = context.get(IEnrEntrantBenefitProofDocumentGen.L_ENTRANT);
                    return dql.where(eq(property(PersonEduDocument.person().id().fromAlias(alias)), value(entrant.getPerson().getId())));
                })
                .order(PersonEduDocument.eduDocumentKind().title())
                .filter(PersonEduDocument.eduDocumentKind().title());
    }
}