/* $Id:$ */
package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Denis Perminov
 * @since 27.06.2014
 */
@Configuration
public class EnrEntrantExtManager extends BusinessObjectExtensionManager
{
}