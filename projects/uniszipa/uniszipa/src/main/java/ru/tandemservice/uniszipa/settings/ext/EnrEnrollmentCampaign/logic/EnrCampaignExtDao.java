/* $Id$ */
package ru.tandemservice.uniszipa.settings.ext.EnrEnrollmentCampaign.logic;

import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrCampaignDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Andrey Avetisov
 * @since 06.04.2015
 */
public class EnrCampaignExtDao extends EnrCampaignDao
{
    @Override
    public void doChangeOpen(Long ecId)
    {
        EnrEnrollmentCampaign campaign = getNotNull(EnrEnrollmentCampaign.class, ecId);
        campaign.setOpen(!campaign.isOpen());
        update(campaign);
    }
}
