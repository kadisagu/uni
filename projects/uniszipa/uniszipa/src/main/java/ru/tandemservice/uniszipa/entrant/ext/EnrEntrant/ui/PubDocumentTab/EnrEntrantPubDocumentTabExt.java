/* $Id$ */
package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant.ui.PubDocumentTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentTab.EnrEntrantPubDocumentTab;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.List.SzipaEnrEntrantForeignEduDocumentTabList;

/**
 * @author Igor Belanov
 * @since 08.07.2016
 */
@Configuration
public class EnrEntrantPubDocumentTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantPubDocumentTab _enrEntrantPubDocumentTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantPubDocumentTab.presenterExtPoint()).create();
    }

    @Bean
    public TabPanelExtension entrantDocumentTabPanelExtPoint()
    {
        return tabPanelExtensionBuilder(_enrEntrantPubDocumentTab.entrantDocumentTabPanelExtPoint())
                .addTab(componentTab("foreignEducationDocument", SzipaEnrEntrantForeignEduDocumentTabList.class)
                .permissionKey("szipaEnr14EntrantPubForeignEducationTabView"))
                .create();
    }
}
