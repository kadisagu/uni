/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrEnrollmentParagraph;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Igor Belanov
 * @since 04.08.2016
 */
@Configuration
public class EnrEnrollmentParagraphExtManager extends BusinessObjectExtensionManager
{
}
