/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrant.ui.BenefitDocumentRequestAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import ru.tandemservice.uniszipa.catalog.entity.codes.EnrScriptItemCodes;

/**
 * @author Denis Katkov
 * @since 08.06.2016
 */
@Input({
        @Bind(key = "entrant", binding = "entrant", required = true)
})
public class SzipaEnrEntrantBenefitDocumentRequestAddUI extends UIPresenter
{
    private EnrEntrant entrant;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(IEnrEntrantBenefitProofDocumentGen.L_ENTRANT, getEntrant());
    }

    public void onClickApply()
    {
        IEnrEntrantBenefitProofDocument benefitDocument = getSettings().get(SzipaEnrEntrantBenefitDocumentRequestAdd.BENEFIT_DOCUMENT);
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_DOCUMENT_OF_BENEFIT_REQUEST);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem,
                SzipaEnrEntrantBenefitDocumentRequestAdd.BENEFIT_DOCUMENT, benefitDocument,
                IEnrEntrantBenefitProofDocumentGen.L_ENTRANT, getEntrant());
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }
}