/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Igor Belanov
 * @since 08.07.2016
 */
@Configuration
public class EnrEntrantForeignEduDocumentManager extends BusinessObjectManager
{
    public static EnrEntrantForeignEduDocumentManager instance()
    {
        return instance(EnrEntrantForeignEduDocumentManager.class);
    }
}
