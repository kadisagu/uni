/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrEnrollmentParagraph.ui.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.EnrEnrollmentParagraphManager;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEditUI;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 06.08.2016
 */
public class EnrEnrollmentParagraphAddEditUIExt extends UIAddon
{
    private SzipaEnrOrderProtocolDateAndNumber _protocolInfo;

    // если с параграфом нужно сохранять информацию о протоколе
    private boolean _needToSaveProtocolInfo = false;

    public EnrEnrollmentParagraphAddEditUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        // попробуем вытащить инфу протокола из базы
        if (getParentPresenter().getParagraph().getId() != null)
            _protocolInfo = DataAccessServices.dao().getUnique(SzipaEnrOrderProtocolDateAndNumber.class, SzipaEnrOrderProtocolDateAndNumber.enrEnrollmentParagraph().s(), getParentPresenter().getParagraph());

        // если её нет, то создадим
        if (_protocolInfo == null)
        {
            _protocolInfo = new SzipaEnrOrderProtocolDateAndNumber();
            _protocolInfo.setPriority(1);
        }
        else
            setNeedToSaveProtocolInfo(true);
    }

    public void onClickApply()
    {
        try
        {
            DynamicListDataSource ds = ((PageableSearchListDataSource) getPresenterConfig().getDataSource("entrantDS")).getLegacyDataSource();
            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrEnrollmentParagraphAddEdit.CHECKBOX_COLUMN));
            RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EnrEnrollmentParagraphAddEdit.RADIOBOX_COLUMN));

            List<EnrRequestedCompetition> preStudents = new ArrayList<>();
            if (checkboxColumn != null)
            {
                for (IEntity entity : checkboxColumn.getSelectedObjects())
                {
                    preStudents.add(((DataWrapper) entity).getWrapped());
                    getParentPresenter().getSelectedEntrantIds().add(entity.getId());
                }
            }

            EnrRequestedCompetition manager = null;
            if (getParentPresenter().getOrder().getPrintFormType().isSelectHeadman() && radioboxColumn != null && radioboxColumn.getSelectedEntity() != null)
                manager = ((DataWrapper) radioboxColumn.getSelectedEntity()).getWrapped();

            // сохраняем или обновляем параграф о зачислении
            EnrEnrollmentParagraphManager.instance().dao().saveOrUpdateParagraph(getParentPresenter().getOrder(), getParentPresenter().getParagraph(), preStudents, manager, getParentPresenter().getGroupTitle());

            // ========================= изменение в методе =========================

            if (getParentPresenter().getParagraph().getId() != null)
            {
                // если нужно сохранить, то сохраняем
                if (isNeedToSaveProtocolInfo())
                {
                    _protocolInfo.setEnrEnrollmentParagraph(getParentPresenter().getParagraph());
                    DataAccessServices.dao().saveOrUpdate(_protocolInfo);
                } else
                    DataAccessServices.dao().delete(_protocolInfo);
            }

            // ======================================================================

            // закрываем форму
            getParentPresenter().deactivate();

        } catch (Throwable t)
        {
            Debug.exception(t);
            getParentPresenter().getSupport().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public SzipaEnrOrderProtocolDateAndNumber getProtocolInfo()
    {
        return _protocolInfo;
    }

    public void setProtocolInfo(SzipaEnrOrderProtocolDateAndNumber protocolInfo)
    {
        _protocolInfo = protocolInfo;
    }

    public boolean isNeedToSaveProtocolInfo()
    {
        return _needToSaveProtocolInfo;
    }

    public void setNeedToSaveProtocolInfo(boolean needToSaveProtocolInfo)
    {
        _needToSaveProtocolInfo = needToSaveProtocolInfo;
    }

    public EnrEnrollmentParagraphAddEditUI getParentPresenter()
    {
        return getPresenter();
    }
}
