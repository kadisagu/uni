/* $Id:$ */
package ru.tandemservice.uniszipa.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.uniszipa.entity.report.EntrantDailyRequestsByCompetitions;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantRecordsStudentFormAdd.SzipaEnrReportEntrantRecordsStudentFormAdd;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantStudentFormAdd.SzipaEnrReportEntrantStudentFormAdd;

/**
 * @author Denis Perminov
 * @since 11.08.2014
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
                .add(EntrantDailyRequestsByCompetitions.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(EntrantDailyRequestsByCompetitions.REPORT_KEY))
                .add(SzipaEnrReportEntrantStudentFormAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(SzipaEnrReportEntrantStudentFormAdd.REPORT_KEY, SzipaEnrReportEntrantStudentFormAdd.class))
                .add(SzipaEnrReportEntrantRecordsStudentFormAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(SzipaEnrReportEntrantRecordsStudentFormAdd.REPORT_KEY, SzipaEnrReportEntrantRecordsStudentFormAdd.class))
                .create();
    }

    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_enrReportBaseManager.storableReportDescExtPoint())
                .add(EntrantDailyRequestsByCompetitions.REPORT_KEY, EntrantDailyRequestsByCompetitions.getDescription())
                .create();
    }
}