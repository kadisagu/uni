package ru.tandemservice.uniszipa.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные формы параграфов приказов о зачислении"
 * Имя сущности : enrEnrollmentOrderParagraphPrintFormType
 * Файл data.xml : uniszipa-templates.data.xml
 */
public interface EnrEnrollmentOrderParagraphPrintFormTypeCodes
{
    /** Константа кода (code) элемента : Базовый шаблон параграфа приказа о зачислении (title) */
    String BASE = "base";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении (целевой прием) (title) */
    String TARGET_ADMISSION = "target_admission";

    Set<String> CODES = ImmutableSet.of(BASE, TARGET_ADMISSION);
}
