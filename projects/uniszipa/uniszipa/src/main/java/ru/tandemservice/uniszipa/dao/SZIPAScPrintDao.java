package ru.tandemservice.uniszipa.dao;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.dao.print.UniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * @author Dmitry Seleznev
 */
public class SZIPAScPrintDao extends UniscPrintDAO
{

    @Override
    public RtfInjectModifier getRtfInjectorModifier(final UniscEduAgreementBase agreementBase, final Person person)
    {
        final RtfInjectModifier modifier = super.getRtfInjectorModifier(agreementBase, person);

        final UniscEduOrgUnit uniscEduOrgUnit = agreementBase.getConfig();
        final EducationOrgUnit educationOrgUnit = uniscEduOrgUnit.getEducationOrgUnit();
        final EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();

        if (educationLevelHighSchool.getEducationLevel().getLevelType().isHigh())
        {
            modifier.put("pay_till_day", "20");
            modifier.put("pay_till_month", "января");
        } else if (educationLevelHighSchool.getEducationLevel().getLevelType().isMiddle())
        {
            modifier.put("pay_till_day", "31");
            modifier.put("pay_till_month", "декабря");
        } else
        {
            modifier.put("pay_till_day", "");
            modifier.put("pay_till_month", "");
        }

        return modifier;
    }

}