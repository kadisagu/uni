package ru.tandemservice.uniszipa.entity.entrant;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniszipa.entity.entrant.gen.*;

/** @see ru.tandemservice.uniszipa.entity.entrant.gen.SzipaForeignEducationDocumentGen */
public class SzipaForeignEducationDocument extends SzipaForeignEducationDocumentGen
{
    @Override
    @EntityDSLSupport(parts = {L_PERSON_EDU_DOCUMENT + "." + PersonEduDocument.P_SERIA, L_PERSON_EDU_DOCUMENT + "." + PersonEduDocument.P_NUMBER})
    public String getSeriaAndNumber()
    {
        return UniStringUtils.joinWithSeparator(" ", getPersonEduDocument().getSeria(), getPersonEduDocument().getNumber());
    }

    @Override
    @EntityDSLSupport(parts = L_PERSON_EDU_DOCUMENT + "." + PersonEduDocument.P_DOCUMENT_KIND_TITLE)
    public String getDocumentTypeTitle()
    {
        return getPersonEduDocument().getDocumentKindTitle();
    }

    @Override
    @EntityDSLSupport(parts = L_PERSON_EDU_DOCUMENT + "." + PersonEduDocument.L_EDU_ORGANIZATION_ADDRESS_ITEM + "." + AddressItem.L_COUNTRY + "." + AddressItem.P_TITLE)
    public String getCountryTitle()
    {
        return getPersonEduDocument().getEduOrganizationAddressItem().getCountry().getTitle();
    }
}