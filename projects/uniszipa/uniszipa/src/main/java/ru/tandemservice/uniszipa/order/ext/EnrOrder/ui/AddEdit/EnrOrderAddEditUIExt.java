/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.uniszipa.entity.entrantorder.SzipaEnrOrderProtocolDateAndNumber;
import ru.tandemservice.uniszipa.order.ext.EnrOrder.EnrOrderExtManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 04.08.2016
 */
public class EnrOrderAddEditUIExt extends UIAddon
{
    private List<SzipaEnrOrderProtocolDateAndNumber> _protocolInfoList;
    private SzipaEnrOrderProtocolDateAndNumber _currentProtocolInfo;

    public EnrOrderAddEditUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        _protocolInfoList = new ArrayList<>();
        if (getParentPresenter().getOrder().getId() != null)
        {
            _protocolInfoList = DataAccessServices.dao().getList(new DQLSelectBuilder()
                    .fromEntity(SzipaEnrOrderProtocolDateAndNumber.class, "seopi")
                    .where(eq(property(SzipaEnrOrderProtocolDateAndNumber.enrOrder().id().fromAlias("seopi")), value(getParentPresenter().getOrder().getId())))
                    .order(property(SzipaEnrOrderProtocolDateAndNumber.priority().fromAlias("seopi"))));
        }

        // добавим пару пустых строк к списку, если в базе не оказалось информации по протоколам
        if (_protocolInfoList.isEmpty())
            for (int p = 1; p <= 2; ++p)
            {
                SzipaEnrOrderProtocolDateAndNumber protocolInfo = new SzipaEnrOrderProtocolDateAndNumber();
                protocolInfo.setPriority(p);
                _protocolInfoList.add(protocolInfo);
            }
    }

    public void onClickApply()
    {
        if(getParentPresenter().isAddForm())
        {
            EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getParentPresenter().getEnrollmentCampaign());
            EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getParentPresenter().getOrder().getRequestType(), getParentPresenter().getEnrollmentCampaign());
        }
        EnrOrder order = getParentPresenter().getOrder();
        Date createDate = getParentPresenter().getCreateDate();
        boolean addForm = getParentPresenter().isAddForm();
        String executor = OrderExecutorSelectModel.getExecutor(getParentPresenter().getEmployeePost());

        EnrOrder enrOrder = EnrOrderManager.instance().dao().saveOrUpdateOrder(order, createDate, addForm, executor);

        // ========================= изменение в методе =========================

        EnrOrderExtManager.instance().dao().updateOrderProtocolInfo(enrOrder, (isShowProtocolInfoBlock() ? _protocolInfoList : null));

        // ======================================================================

        getParentPresenter().deactivate();

        if (getParentPresenter().isAddForm())
            getActivationBuilder().asDesktopRoot(EnrOrderPub.class)
                    .parameter(UIPresenter.PUBLISHER_ID, enrOrder.getId())
                    .activate();
    }

    public boolean isShowProtocolInfoBlock()
    {
        return getParentPresenter().getOrder().getType() != null
                && EnrOrderTypeCodes.ENROLLMENT.equals(getParentPresenter().getOrder().getType().getCode());
    }

    public void onClickAddProtocolInfo()
    {
        SzipaEnrOrderProtocolDateAndNumber protocolInfo = new SzipaEnrOrderProtocolDateAndNumber();
        int nextPriority = _protocolInfoList.isEmpty() ? 1 : _protocolInfoList.stream()
                .max((p1, p2) -> Integer.compare(p1.getPriority(), p2.getPriority())).get().getPriority() + 1;
        protocolInfo.setPriority(nextPriority);
        _protocolInfoList.add(protocolInfo);
    }

    public void onClickDeleteProtocolInfo()
    {
        SzipaEnrOrderProtocolDateAndNumber deletedItem = getListenerParameter();
        if (deletedItem.getPriority() <= 2)
            deletedItem.clear();
        else
        {
            _protocolInfoList.remove(deletedItem);
            // нужно расставить приоритеты заново, чтобы они стояли плотно от 1
            if (_protocolInfoList.indexOf(deletedItem) < _protocolInfoList.size() - 1)
            {
                int priority = 1;
                for (SzipaEnrOrderProtocolDateAndNumber protocolInfo : _protocolInfoList)
                    protocolInfo.setPriority(priority++);
            }
        }
    }

    public List<SzipaEnrOrderProtocolDateAndNumber> getProtocolInfoList()
    {
        return _protocolInfoList;
    }

    public void setProtocolInfoList(List<SzipaEnrOrderProtocolDateAndNumber> protocolInfoList)
    {
        _protocolInfoList = protocolInfoList;
    }

    public SzipaEnrOrderProtocolDateAndNumber getCurrentProtocolInfo()
    {
        return _currentProtocolInfo;
    }

    public void setCurrentProtocolInfo(SzipaEnrOrderProtocolDateAndNumber currentProtocolInfo)
    {
        _currentProtocolInfo = currentProtocolInfo;
    }

    public EnrOrderAddEditUI getParentPresenter()
    {
        return getPresenter();
    }
}
