/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uniszipa.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uniszipa.entity.entrant.SzipaForeignEducationDocument;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.logic.EnrEntrantForeignEducationDocumentDSHandler;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.AddEdit.SzipaEnrEntrantForeignEduDocumentTabAddEdit;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.AddEdit.SzipaEnrEntrantForeignEduDocumentTabAddEditUI;

/**
 * @author Igor Belanov
 * @since 11.07.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrant.id", required = true)
})
public class SzipaEnrEntrantForeignEduDocumentTabListUI extends UIPresenter
{
    private EnrEntrant _entrant = new EnrEntrant();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantForeignEducationDocumentDSHandler.BIND_ENTRANT, getEntrant());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(SzipaEnrEntrantForeignEduDocumentTabAddEdit.class.getSimpleName())
                .parameter(SzipaEnrEntrantForeignEduDocumentTabAddEditUI.BIND_ENTRANT_ID, getEntrant().getId())
                .parameter(PUBLISHER_ID, null)
                .activate();
    }

    public void onClickDownloadRecognition()
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_FOREIGN_EDUCATION_DOCUMENT_RECOGNITION);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem,
                "eduForeignDocumentId", getListenerParameterAsLong(),
                "entrant", getEntrant());
    }

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(SzipaEnrEntrantForeignEduDocumentTabAddEdit.class.getSimpleName())
                .parameter(SzipaEnrEntrantForeignEduDocumentTabAddEditUI.BIND_ENTRANT_ID, getEntrant().getId())
                .parameter(PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete((SzipaForeignEducationDocument) getEntityByListenerParameterAsLong());
    }

    // getters and setters
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this._entrant = entrant;
    }
}
