/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.logic;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;

/**
 * @author Igor Belanov
 * @since 10.08.2016
 */
public class EnrEnrollmentExtractWrapper implements Comparable<EnrEnrollmentExtractWrapper>
{
    private final EnrEnrollmentExtract _extract;

    private final String _fullFio;
    private final String _previousEduLevelTitle;
    private final long _sumMark;
    private final boolean _isHaveInternalPass;
    private final String _contractBasisTitle;

    public EnrEnrollmentExtractWrapper(EnrEnrollmentExtract extract, String fullFio, String previousEduLevelTitle, long sumMark, boolean isHaveInternalPass, String contractBasisTitle)
    {
        _extract = extract;
        _fullFio = fullFio;
        _previousEduLevelTitle = previousEduLevelTitle;
        _sumMark = sumMark;
        _isHaveInternalPass = isHaveInternalPass;
        _contractBasisTitle = contractBasisTitle;
    }

    @Override
    public int hashCode()
    {
        return _extract.hashCode();
    }

    @Override
    public boolean equals(Object anotherExtWr)
    {
        return (anotherExtWr instanceof EnrEnrollmentExtractWrapper) && _extract.equals(((EnrEnrollmentExtractWrapper) anotherExtWr).getExtract());
    }

    @Override
    public int compareTo(EnrEnrollmentExtractWrapper anotherExtWr)
    {
        if (anotherExtWr == null) return 1;

        int markSumCompare = Long.compare(anotherExtWr.getSumMark(), _sumMark);
        if (markSumCompare != 0) return markSumCompare;

        return CommonCollator.RUSSIAN_COLLATOR.compare(_fullFio, anotherExtWr.getFullFio());
    }

    public EnrEnrollmentExtract getExtract()
    {
        return _extract;
    }

    public String getFullFio()
    {
        return _fullFio;
    }

    public long getSumMark()
    {
        return _sumMark;
    }

    public String getPreviousEduLevelTitle()
    {
        return _previousEduLevelTitle;
    }

    public boolean isHaveInternalPass()
    {
        return _isHaveInternalPass;
    }

    public boolean isTargetAdmission()
    {
        return _contractBasisTitle != null;
    }

    public String getContractBasisTitle()
    {
        return _contractBasisTitle;
    }
}
