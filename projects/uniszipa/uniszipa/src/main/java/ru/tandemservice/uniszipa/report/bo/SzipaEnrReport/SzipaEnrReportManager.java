/* $Id:$ */
package ru.tandemservice.uniszipa.report.bo.SzipaEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.logic.ISzipaEnrReportDao;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.logic.SzipaEnrReportDao;

/**
 * @author Denis Perminov
 * @since 12.08.2014
 */
@Configuration
public class SzipaEnrReportManager extends BusinessObjectManager
{
    public static SzipaEnrReportManager instance()
    {
        return instance(SzipaEnrReportManager.class);
    }

    @Bean
    public ISzipaEnrReportDao dao() { return new SzipaEnrReportDao(); }
}
