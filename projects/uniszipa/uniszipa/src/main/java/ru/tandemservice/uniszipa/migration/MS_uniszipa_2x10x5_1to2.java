package ru.tandemservice.uniszipa.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniszipa_2x10x5_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.5"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность szipaEnrOrderProtocolDateAndNumber

		// создана новая сущность
		if (!tool.tableExists("szpenrordrprtcldtandnmbr_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("szpenrordrprtcldtandnmbr_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_4607b1c2"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("enrorder_id", DBType.LONG), 
				new DBColumn("enrenrollmentparagraph_id", DBType.LONG), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("protocoldate_p", DBType.DATE), 
				new DBColumn("protocolnumber_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("szipaEnrOrderProtocolDateAndNumber");

		}


    }
}