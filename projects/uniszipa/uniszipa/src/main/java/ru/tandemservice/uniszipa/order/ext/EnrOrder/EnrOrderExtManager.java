/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import ru.tandemservice.uniszipa.order.ext.EnrOrder.logic.ISzipaEnrOrderDao;
import ru.tandemservice.uniszipa.order.ext.EnrOrder.logic.SzipaEnrOrderDao;

/**
 * @author Igor Belanov
 * @since 04.08.2016
 */
@Configuration
public class EnrOrderExtManager extends BusinessObjectExtensionManager
{
    public static EnrOrderExtManager instance()
    {
        return instance(EnrOrderExtManager.class);
    }

    @Bean
    public ISzipaEnrOrderDao dao()
    {
        return new SzipaEnrOrderDao();
    }
}
