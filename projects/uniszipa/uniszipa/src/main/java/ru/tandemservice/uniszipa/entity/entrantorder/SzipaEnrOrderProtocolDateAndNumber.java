package ru.tandemservice.uniszipa.entity.entrantorder;

import ru.tandemservice.uniszipa.entity.entrantorder.gen.*;

/** @see ru.tandemservice.uniszipa.entity.entrantorder.gen.SzipaEnrOrderProtocolDateAndNumberGen */
/**
 * (эта сущность прицепляется либо к приказу либо к параграфу,
 * поэтому при селекте, изменении или удаленни ВСЕГДА проверять ссылки на параграф или приказ на null)
 */
public class SzipaEnrOrderProtocolDateAndNumber extends SzipaEnrOrderProtocolDateAndNumberGen
{
    /**
     * очистка значений
     */
    public void clear()
    {
        setProtocolDate(null);
        setProtocolNumber(null);
    }

    /**
     * позиция (приоритет) определяет где будут находится данные в документе,
     * т.е. к какой приёмной комиссии они будут привязаны
     * @return название приёмной комиссии
     */
    public String getEnrollmentComissionTitle()
    {
        if (enrOrder() == null)
            return "";
        else if (getPriority() == 1)
            return "Приемной комиссии РАНХиГС";
        else
            return "Приемной подкомиссии Северо-Западного института управления - филиала РАНХиГС";
    }
}