package ru.tandemservice.uniszipa.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид печатной формы приказа по абитуриентам"
 * Имя сущности : enrOrderPrintFormType
 * Файл data.xml : uniszipa-templates.data.xml
 */
public interface EnrOrderPrintFormTypeCodes
{
    /** Константа кода (code) элемента : Базовый приказ о зачислении (title) */
    String BASE_ENROLLMENT = "base_enrollment";
    /** Константа кода (code) элемента : Базовый приказ об отмене зачисления (title) */
    String BASE_CANCEL_ENROLLMENT = "base_cancel_enrollment";
    /** Константа кода (code) элемента : Базовый приказ о распределении (title) */
    String BASE_ALLOCATION = "base_allocation";
    /** Константа кода (code) элемента : Базовый приказ о зачислении по направлению от Минобрнауки (title) */
    String BASE_ENROLLMENT_MINISTERY = "base_enrollment_ministery";

    Set<String> CODES = ImmutableSet.of(BASE_ENROLLMENT, BASE_CANCEL_ENROLLMENT, BASE_ALLOCATION, BASE_ENROLLMENT_MINISTERY);
}
