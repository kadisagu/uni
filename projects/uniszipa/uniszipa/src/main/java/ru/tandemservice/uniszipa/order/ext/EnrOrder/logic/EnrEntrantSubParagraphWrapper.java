/* $Id$ */
package ru.tandemservice.uniszipa.order.ext.EnrOrder.logic;

import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 08.08.2016
 */
public class EnrEntrantSubParagraphWrapper implements Comparable<EnrEntrantSubParagraphWrapper>
{
    private final List<EnrEnrollmentExtractWrapper> _extractsWrappers;
    private final EduProgramSubject _programSubject;

    public EnrEntrantSubParagraphWrapper(EduProgramSubject programSubject)
    {
        _extractsWrappers = new ArrayList<>();
        _programSubject = programSubject;
    }

    @Override
    public int hashCode()
    {
        return _programSubject.hashCode();
    }

    @Override
    public boolean equals(Object anotherEnrEntrantSubParagraphWrapper)
    {
        return anotherEnrEntrantSubParagraphWrapper instanceof EnrEntrantSubParagraphWrapper &&
                _programSubject.equals(((EnrEntrantSubParagraphWrapper) anotherEnrEntrantSubParagraphWrapper).getProgramSubject());
    }

    @Override
    public int compareTo(EnrEntrantSubParagraphWrapper anotherEnrEntrantSubParagraphWrapper)
    {
        return _programSubject.getCode().compareTo(anotherEnrEntrantSubParagraphWrapper.getProgramSubject().getCode());
    }

    public List<EnrEnrollmentExtractWrapper> getExtractsWrappers()
    {
        return _extractsWrappers;
    }

    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }
}
