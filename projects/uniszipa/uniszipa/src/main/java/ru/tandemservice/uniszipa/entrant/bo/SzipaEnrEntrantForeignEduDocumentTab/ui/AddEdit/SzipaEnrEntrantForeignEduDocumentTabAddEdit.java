/* $Id$ */
package ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uniszipa.entrant.bo.SzipaEnrEntrantForeignEduDocumentTab.logic.EnrEntrantForeignEducationDocumentDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 11.07.2016
 */
@Configuration
public class SzipaEnrEntrantForeignEduDocumentTabAddEdit extends BusinessComponentManager
{
    public static final String DS_EDUCATION_DOCS = "educationDocsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(DS_EDUCATION_DOCS, educationDocsDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationDocsDSHandler()
    {
        return new EntityComboDataSourceHandler(DS_EDUCATION_DOCS, PersonEduDocument.class)
                .customize((alias, dql, context, filter) -> {
                    EnrEntrant entrant = context.get(EnrEntrantForeignEducationDocumentDSHandler.BIND_ENTRANT);
                    return dql == null ? null : dql.where(eq(property(PersonEduDocument.person().id().fromAlias(alias)), value(entrant.getPerson().getId())));
                });
    }

}
