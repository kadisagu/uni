/* $Id:$ */
package ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantDailyRequestsByCompetitionsAdd.SzipaEnrReportEntrantDailyRequestsByCompetitionsAddUI;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantRecordsStudentFormAdd.SzipaEnrReportEntrantRecordsStudentFormAddUI;
import ru.tandemservice.uniszipa.report.bo.SzipaEnrReport.ui.EntrantStudentFormAdd.SzipaEnrReportEntrantStudentFormAddUI;

/**
 * @author Denis Perminov
 * @since 12.08.2014
 */
public interface ISzipaEnrReportDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createReportEntrantDailyRequestsByCompetitions(SzipaEnrReportEntrantDailyRequestsByCompetitionsAddUI model);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Long createReportEntrantStudentForm(SzipaEnrReportEntrantStudentFormAddUI model);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Long createReportEntrantRecordsStudentForm(SzipaEnrReportEntrantRecordsStudentFormAddUI model);
}
