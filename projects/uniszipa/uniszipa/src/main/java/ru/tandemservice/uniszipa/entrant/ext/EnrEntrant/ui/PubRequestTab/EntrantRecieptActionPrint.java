package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.uniszipa.catalog.entity.codes.EnrScriptItemCodes;

/**
 * Created by nsvetlov on 27.05.2016.
 */
public class EntrantRecieptActionPrint extends NamedUIAction {
    public EntrantRecieptActionPrint(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_RECEIPT);
        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, presenter.getListenerParameterAsLong()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, presenter.getListenerParameterAsLong());
    }
}
