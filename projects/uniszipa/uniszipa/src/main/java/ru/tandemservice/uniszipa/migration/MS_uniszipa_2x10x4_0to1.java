/* $Id$ */
package ru.tandemservice.uniszipa.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Igor Belanov
 * @since 15.07.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniszipa_2x10x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность szipaForeignEducationDocument

        // создана новая сущность
        if (!tool.tableExists("szpfrgnedctndcmnt_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("szpfrgnedctndcmnt_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_f0cd2de2"),
                    new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                    new DBColumn("personedudocument_id", DBType.LONG).setNullable(false),
                    new DBColumn("numberofpages_p", DBType.createVarchar(255)),
                    new DBColumn("fio_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("educationperiod_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("attachments_p", DBType.createVarchar(255)),
                    new DBColumn("createdate_p", DBType.DATE).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("szipaForeignEducationDocument");

        }


    }
}
