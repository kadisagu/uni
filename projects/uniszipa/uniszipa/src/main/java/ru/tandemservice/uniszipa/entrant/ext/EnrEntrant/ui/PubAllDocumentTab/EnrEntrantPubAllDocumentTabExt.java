/* $Id$ */
package ru.tandemservice.uniszipa.entrant.ext.EnrEntrant.ui.PubAllDocumentTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab.EnrEntrantPubAllDocumentTab;

/**
 * @author Denis Katkov
 * @since 08.06.2016
 */
@Configuration
public class EnrEntrantPubAllDocumentTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniszipa"+EnrEntrantPubAllDocumentTabUIExt.class.getSimpleName();

    @Autowired
    private EnrEntrantPubAllDocumentTab enrEntrantPubAllDocumentTab;

    @Bean
    public PresenterExtension presenterExtension(){
        return presenterExtensionBuilder(enrEntrantPubAllDocumentTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantPubAllDocumentTabUIExt.class))
                .create();
    }
}