/**
 * $Id$
 */
package ru.tandemservice.uniurbin.component.reports.printMarkForDiploma.AddNew;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.Comparator;

/**
 * Create by: ashaburov
 * Date: 13.04.11
 */
public class DisciplineWrapper
{
    private EppStudentWpeCAction wpca;
    private SessionSlotRegularMark sourceMark;
    private SessionProjectTheme projectTheme;
    IEppEpvRowWrapper epvRow;

    public DisciplineWrapper(EppStudentWpeCAction wpca, SessionMark mark)
    {
        this.wpca = wpca;
        if (mark instanceof SessionSlotRegularMark)
            this.sourceMark = (SessionSlotRegularMark) mark;
        if (mark instanceof SessionSlotLinkMark)
            this.sourceMark = ((SessionSlotLinkMark) mark).getTarget();
    }

    public static DisciplineComparator DISCIPLINE_COMPARATOR = new DisciplineComparator();

    public static CourseWorkComparator COURSE_WORK_COMPARATOR = new CourseWorkComparator();

    public String getWeeks()
    {
        if (null == epvRow)
            return "";

        double weeks = epvRow.getTotalLoad(wpca.getStudentWpe().getTerm().getIntValue(), EppLoadType.FULL_CODE_WEEKS);
        return UniEppUtils.eq(weeks, 0) ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(weeks);
    }

    private static class CourseWorkComparator implements Comparator<DisciplineWrapper>
    {
        @Override
        public int compare(DisciplineWrapper o1, DisciplineWrapper o2)
        {
            int result = o1.getTerm().compareTo(o2.getTerm());
            if (0 == result) result = o1.getDisciplineTitle().compareTo(o2.getDisciplineTitle());
            if (0 == result) result = o1.wpca.getId().compareTo(o2.wpca.getId());

            return result;
        }
    }

    private static class DisciplineComparator implements Comparator<DisciplineWrapper>
    {
        @Override
        public int compare(DisciplineWrapper o1, DisciplineWrapper o2)
        {
            Integer p1 = o1.getIndexPriority();
            Integer p2 = o2.getIndexPriority();
            if (p1 == null && p2 != null)
                return 1;
            else if (p1 != null && p2 == null)
                return -1;

            int result = (p1 != null) ? p1 - p2 : 0;

            if (0 == result) result = o1.getDisciplineTitle().compareTo(o2.getDisciplineTitle());
            if (0 == result) result = o1.getDiscipline().getNumber().compareTo(o2.getDiscipline().getNumber());
            if (0 == result) result = o1.getDisciplinePart().getNumber() - o2.getDisciplinePart().getNumber();
            if (0 == result) result = o1.wpca.getType().getPriority() - o2.wpca.getType().getPriority();
            if (0 == result) result = o1.wpca.getId().compareTo(o2.wpca.getId());

            return result;
        }
    }

    //Getter & Setter

    public String getIndexStr()
    {
        return epvRow == null ? "" : epvRow.getIndex();
    }

    public String getCourseWorkTitle()
    {
        return projectTheme == null ? "" : projectTheme.getTheme();
    }

    public String getDisciplineTitle()
    {
        return getDiscipline().getTitle();
    }

    private EppRegistryElement getDiscipline()
    {
        return getDisciplinePart().getRegistryElement();
    }

    private EppRegistryElementPart getDisciplinePart() {return wpca.getStudentWpe().getRegistryElementPart();}

    public Integer getTerm()
    {
        return wpca.getStudentWpe().getTerm().getIntValue();
    }

    public Double getTitalHour()
    {
        return getDiscipline().getSizeAsDouble();
    }

    public String getActionResult()
    {
        if (sourceMark != null)
            return sourceMark.getValueItem().getPrintTitle();
        else
            return "";
    }

    public void setProjectTheme(SessionProjectTheme projectTheme)
    {
        this.projectTheme = projectTheme;
    }

    public boolean isMandatory()
    {
        EppWorkPlanRow sourceRow = wpca.getStudentWpe().getSourceRow();
        if (sourceRow != null && !sourceRow.getKind().getCode().equals(EppWorkPlanRowKindCodes.MAIN))
            return false;
        IEppEpvRowWrapper cycleRow = getCycle(epvRow);
        if (null != cycleRow) {
            EppEpvStructureRow structureRow = (EppEpvStructureRow) cycleRow.getRow();
            if (EppPlanStructure.OPTIONAL_DISCIPLINES.contains(structureRow.getValue().getCode()))
                return false;
        }
        return true;
    }

    public boolean isPractice()
    {
        return getDiscipline().getParent().isPracticeElement();
    }

    public boolean isProject()
    {
        return EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT.equals(wpca.getType().getCode()) || EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK.equals(wpca.getType().getCode());
    }

    public EppStudentWpeCAction getWpca()
    {
        return wpca;
    }

    public IEppEpvRowWrapper getEpvRow()
    {
        return epvRow;
    }

    public void setEpvRow(IEppEpvRowWrapper epvRow)
    {
        this.epvRow = epvRow;
    }
    
    public Integer getIndexPriority() 
    {
        IEppEpvRowWrapper cycleRow = getCycle(getEpvRow());
        if (null != cycleRow) {
            EppEpvStructureRow structureRow = (EppEpvStructureRow) cycleRow.getRow();
            return structureRow.getValue().getPriority();
        }
        return null;
    }
    
    private static IEppEpvRowWrapper getCycle(IEppEpvRowWrapper row)
    {
        if (null == row)
            return null;
        if (row.getRow() instanceof EppEpvStructureRow) {
            EppEpvStructureRow structureRow = (EppEpvStructureRow) row.getRow();
            if (isCycle(structureRow.getValue()))
                return row;
        }
        return getCycle(row.getHierarhyParent());
    }

    private static boolean isCycle(EppPlanStructure structure)
    {
        if (EppPlanStructureCodes.GOS_CYCLES.equals(structure.getCode()) || EppPlanStructureCodes.FGOS_CYCLES.equals(structure.getCode()))
            return true;

        return structure.getParent() != null && isCycle(structure.getParent());
    }
    
    public String getComment()
    {
        if (sourceMark.getSlot().getDocument() instanceof SessionTransferOutsideDocument) {
            SessionTransferOutsideDocument document = (SessionTransferOutsideDocument) sourceMark.getSlot().getDocument();
            if (document.getEduInstitutionTitle() != null)
                return document.getEduInstitutionTitle();
            return "Внешнее перезачтение";
        }
        return "";
    }
}
