/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniurbin.dao;

import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnitTypeRestriction;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 16.07.2009
 */
public class URBINOrgstructDao extends SharedBaseDao implements IURBINOrgstructDao
{
    public void createOrgstructHierarhy()
    {
        TopOrgUnit academy = TopOrgUnit.getInstance();
        if (academy == null)
        {
            academy = new TopOrgUnit();
            academy.setTitle("Институт урбанистики");
            academy.setShortTitle("ИУ");
            academy.setOrgUnitType(UniDaoFacade.getCoreDao().get(OrgUnitType.class, OrgUnitType.P_CODE, "academy"));
            getSession().save(academy);
        }

        if (!existsEntity(OrgUnitTypeRestriction.class))
        {
            List<OrgUnitType> list = getList(OrgUnitType.class);
            Map<String, OrgUnitType> name2type = new HashMap<>();

            for (OrgUnitType orgUnitType : list)
                name2type.put(orgUnitType.getCode(), orgUnitType);

            //тип подразделения -> список вложенный типов
            String[][] config = new String[][]{
                    {"academy", "college", "faculty", "cathedra", "branch", "representation", "division", "divisionManagement", "divisionCenter"},
                    {"faculty", "cathedra", "division"},
                    {"cathedra", "laboratory"},
                    {"branch", "cathedra", "division"},
                    {"divisionManagement", "division", "divisionManagement", "laboratory"},
                    {"division", "division"},
                    {"divisionCenter", "divisionCenter", "laboratory"},
                    {"laboratory", "laboratory"}
            };
            for (String[] row : config)
            {
                OrgUnitType orgUnitType = name2type.get(row[0]);
                for (int i = 1; i < row.length; i++)
                {
                    OrgUnitTypeRestriction restriction = new OrgUnitTypeRestriction();
                    restriction.setParent(orgUnitType);
                    restriction.setChild(name2type.get(row[i]));
                    getSession().save(restriction);
                }
            }
        }
    }
}