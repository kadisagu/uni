/**
 * $Id$
 */
package ru.tandemservice.uniurbin.component.reports.printMarkForDiploma.AddNew;

import jxl.write.WriteException;
import org.tandemframework.core.document.IDocumentRenderer;
import ru.tandemservice.uni.dao.IUniDao;

import java.io.IOException;

/**
 * Create by: Shaburov
 * Date: 12.04.11
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareGroup(Model model);

    IDocumentRenderer prepareReports(Model model) throws IOException, WriteException;
}
