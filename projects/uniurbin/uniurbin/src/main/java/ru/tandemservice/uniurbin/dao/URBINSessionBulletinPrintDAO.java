package ru.tandemservice.uniurbin.dao;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 26.12.12
 */
public class URBINSessionBulletinPrintDAO extends SessionBulletinPrintDAO {

    protected void printAdditionalInfo(final BulletinPrintInfo printInfo, MarkAmountData markAmountData)
    {
        String sDiscHours = "";
        RtfDocument document = printInfo.getDocument();
        RtfInjectModifier modifier = new RtfInjectModifier();

        SessionBulletinDocument sessionBulletinDocument = printInfo.getBulletin();

        EppRegistryElementPart eppRegistryElementPart = sessionBulletinDocument.getGroup().getActivityPart();
        String curActionCode = sessionBulletinDocument.getGroup().getActionType().getCode();

         if (isActionExamSetoff(curActionCode)) { // текущий ИК - экзамен или зачет
           sDiscHours = getDiscHours(eppRegistryElementPart, curActionCode);
        }

        modifier.put("discHours", sDiscHours);
        modifier.modify(document);
    }

    private String getDiscHours (EppRegistryElementPart eppRegistryElementPart, String curActionCode){

        if (!isActionExam(curActionCode)) { // текущий ИК - не экзамен
            // проверим, был ли ИК экзамен для этой дисциплиночасти
            List<EppRegistryElementPartFControlAction> actions = this.getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part(), eppRegistryElementPart);
            for (EppRegistryElementPartFControlAction type: actions) {
                String actionCode = type.getControlAction().getCode();
                if (isActionExam(actionCode)) {
                    return "";
                }
            }
        }
        Double curHours = eppRegistryElementPart.getSizeAsDouble();      // нагрузка текущего семестра
        Double discHours  = getPreviousPartsSize(eppRegistryElementPart, curHours);  // нагрузка с учетом нагрузки предыдущих семестров
        String sDiscHours = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(discHours);
        return sDiscHours;
    }


    private Double getPreviousPartsSize(EppRegistryElementPart eppRegistryElementPart, Double discHours) {

        EppRegistryElementPart  previousPart = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPart.class, "part")
                    .where(eq(property(EppRegistryElementPart.registryElement().fromAlias("part")), value(eppRegistryElementPart.getRegistryElement())))
                    .where(lt(property(EppRegistryElementPart.number().fromAlias("part")), value(eppRegistryElementPart.getNumber())))
                    .order(property(EppRegistryElementPart.number().fromAlias("part")), OrderDirection.desc)
                    .createStatement(getSession()).setMaxResults(1).uniqueResult();

        if (null == previousPart) return discHours;

        List<EppRegistryElementPartFControlAction> actions = this.getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part(), previousPart);
        for (EppRegistryElementPartFControlAction type: actions) {
            String actionCode = type.getControlAction().getCode();
            if (isActionExamSetoff(actionCode))
                return discHours;
        }
        discHours += previousPart.getSizeAsDouble();
        return getPreviousPartsSize(previousPart, discHours);
    }

    private boolean isActionExam (String actionCode) {
        return actionCode.equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM) || actionCode.equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM);
    }

    private boolean isActionExamSetoff (String actionCode) {
        return isActionExam(actionCode)
                || actionCode.equals(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF)
                || actionCode.equals(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF);
    }
}
