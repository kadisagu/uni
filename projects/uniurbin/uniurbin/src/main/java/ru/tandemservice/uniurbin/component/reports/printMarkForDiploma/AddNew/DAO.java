/**
 * $Id$
 */
package ru.tandemservice.uniurbin.component.reports.printMarkForDiploma.AddNew;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;
import java.util.zip.Deflater;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Create by: Shaburov
 * Date: 12.04.11
 */
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setCourseList(DevelopGridDAO.getCourseList());
    }

    @Override
    public void prepareGroup(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(Group.class, "g").column("g").order(property(Group.title().fromAlias("g")))
            .where(or(
                eq(property(Group.educationOrgUnit().formativeOrgUnit().id().fromAlias("g")), value(model.getOrgUnitId())),
                eq(property(Group.educationOrgUnit().groupOrgUnit().id().fromAlias("g")), value(model.getOrgUnitId()))))
            .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)));
        if (model.getCourse() != null) 
            dql.where(eq(property(Group.course().fromAlias("g")), value(model.getCourse())));
        model.setGroupList(dql.createStatement(getSession()).<Group>list());
    }

    @Override
    public IDocumentRenderer prepareReports(Model model) throws IOException, WriteException
    {
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream();
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        zipOut.setEncoding("CP866");

        for (Student student : getList(Student.class, Student.group(), model.getGroup()))
        {
            byte[] bytes = prepareReport(student, model.isOnceDiscipline());

            zipOut.putNextEntry(new ZipEntry(CoreStringUtils.transliterate(student.getPerson().getFio()) + ".xls"));
            zipOut.write(bytes);
            zipOut.closeEntry();
        }

        zipBuffer.close();
        zipStream.close();
        zipOut.close();
        return new CommonBaseRenderer().zip().fileName(model.getGroup().getTitle() + ".zip").document(zipStream.toByteArray());
    }

    public byte[] prepareReport(Student student, boolean onceDisc) throws IOException, WriteException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BufferedOutputStream outBuffer = new BufferedOutputStream(out);
        WorkbookSettings ws = new WorkbookSettings();
        WritableWorkbook workbook = Workbook.createWorkbook(outBuffer, ws);

        CellFormats cellFormats = new CellFormats();

        List<DisciplineWrapper> allDisciplines = getDisciplineWrapperList(student);

        writeDisciplines(student, workbook, cellFormats, allDisciplines, onceDisc);
        writeProjects(workbook, cellFormats, allDisciplines);
        writePractice(workbook, cellFormats, allDisciplines);

        workbook.write();
        workbook.close();
        out.close();
        outBuffer.close();
        return out.toByteArray();
    }

    private void writePractice(WritableWorkbook workbook, CellFormats cellFormats, List<DisciplineWrapper> allDisciplines) throws WriteException
    {
        WritableSheet practiceSheet = workbook.createSheet("Практики", 2);
        practiceSheet.getSettings().setCopies(1);

        int row = 0;
        int column = 0;

        practiceSheet.setColumnView(column++, 46);
        practiceSheet.setColumnView(column++, calculateColumnWidth(" Семестр "));
        practiceSheet.setColumnView(column++, calculateColumnWidth("Количество недель"));
        practiceSheet.setColumnView(column, calculateColumnWidth(" контрольного мероприятия"));

        column = 0;
        practiceSheet.addCell(new Label(column++, row, "Практика", cellFormats.tableHeader));
        practiceSheet.addCell(new Label(column++, row, "Семестр", cellFormats.tableHeader));
        practiceSheet.addCell(new Label(column++, row, "Количество недель", cellFormats.tableHeader));
        practiceSheet.addCell(new Label(column, row++, "Результат итогового контрольного мероприятия", cellFormats.tableHeader));

        //вписываем практики

        for (DisciplineWrapper wrapper : CollectionUtils.select(allDisciplines, DisciplineWrapper::isPractice))
        {
            column = 0;
            practiceSheet.addCell(new Label(column++, row, wrapper.getDisciplineTitle(), cellFormats.def));
            practiceSheet.addCell(new Number(column++, row, wrapper.getTerm(), cellFormats.def));
            practiceSheet.addCell(new Label(column++, row, wrapper.getWeeks(), cellFormats.def));
            practiceSheet.addCell(new Label(column, row++, wrapper.getActionResult(), cellFormats.def));
        }
    }

    private void writeProjects(WritableWorkbook workbook, CellFormats cellFormats, List<DisciplineWrapper> allDisciplines) throws WriteException
    {
        WritableSheet projectSheet = workbook.createSheet("Курсовые работы", 1);
        projectSheet.getSettings().setCopies(1);

        int row = 0;
        int column = 0;

        projectSheet.setColumnView(column++, calculateColumnWidth("  Индекс  "));
        projectSheet.setColumnView(column++, 46);
        projectSheet.setColumnView(column++, calculateColumnWidth(" Семестр "));
        projectSheet.setColumnView(column++, calculateColumnWidth(" контрольного мероприятия"));
        projectSheet.setColumnView(column, 46);

        column = 0;
        projectSheet.addCell(new Label(column++, row, "Индекс", cellFormats.tableHeader));
        projectSheet.addCell(new Label(column++, row, "Дисциплина курсовой работы", cellFormats.tableHeader));
        projectSheet.addCell(new Label(column++, row, "Семестр", cellFormats.tableHeader));
        projectSheet.addCell(new Label(column++, row, "Результат итогового контрольного мероприятия", cellFormats.tableHeader));
        projectSheet.addCell(new Label(column, row, "Название курсовой работы", cellFormats.tableHeader));

        //вписываем курсовые работы
        List<DisciplineWrapper> projects = new ArrayList<>(CollectionUtils.select(allDisciplines, DisciplineWrapper::isProject));
        Collections.sort(projects, DisciplineWrapper.COURSE_WORK_COMPARATOR);

        for (DisciplineWrapper wrapper : projects)
        {
            column = 0;
            projectSheet.addCell(new Label(column++, ++row, (wrapper.getIndexStr() != null ? wrapper.getIndexStr() : ""), cellFormats.def));
            projectSheet.addCell(new Label(column++, row, wrapper.getDisciplineTitle(), cellFormats.def));
            projectSheet.addCell(new Number(column++, row, wrapper.getTerm(), cellFormats.def));
            projectSheet.addCell(new Label(column++, row, wrapper.getActionResult(), cellFormats.def));
            projectSheet.addCell(new Label(column, row, wrapper.getCourseWorkTitle(), cellFormats.def));
        }
    }

    private void writeDisciplines(Student student, WritableWorkbook workbook, CellFormats cellFormats, List<DisciplineWrapper> allDisciplines, final boolean onceDisc) throws WriteException
    {
        WritableSheet discSheet = workbook.createSheet("Дисциплины", 0);
        discSheet.getSettings().setCopies(1);

        int row = 0;
        int column = 0;

        discSheet.setColumnView(column++, calculateColumnWidth("  Индекс  "));
        discSheet.setColumnView(column++, 46);
        discSheet.setColumnView(column++, calculateColumnWidth(" Семестр "));
        discSheet.setColumnView(column++, calculateColumnWidth("Количество часов"));
        discSheet.setColumnView(column++, calculateColumnWidth(" контрольного мероприятия"));
        discSheet.setColumnView(column++, calculateColumnWidth("Аттестат о среднем (полном) общем образовании"));

        column = 0;
        discSheet.addCell(new Label(column++, ++row, "", cellFormats.personalHeader));
        discSheet.addCell(new Label(column++, row, "ФИО студента", cellFormats.personalHeader));
        discSheet.addCell(new Label(column++, row, "ВПО", cellFormats.personalHeader));
        discSheet.addCell(new Label(column++, row, "СПО", cellFormats.personalHeader));
        discSheet.addCell(new Label(column++, row, "НПО", cellFormats.personalHeader));
        discSheet.addCell(new Label(column, row, "Аттестат о среднем (полном) общем образовании", cellFormats.personalHeader));

        discSheet.addCell(new Label(1, ++row, student.getPerson().getFullFio(), cellFormats.def));

        // вписываем год выдачи документа о предыдущем образовании
        PersonEduInstitution eduInstitution = student.getPerson().getPersonEduInstitution();
        if (eduInstitution != null)
        {
            if (eduInstitution.getEducationLevel().isHighProf())
                discSheet.addCell(new Label(2, row, Integer.toString(eduInstitution.getYearEnd()) + " г.", cellFormats.def));
            else if (eduInstitution.getEducationLevel().isMiddleProf())
                discSheet.addCell(new Label(3, row, Integer.toString(eduInstitution.getYearEnd()) + " г.", cellFormats.def));
            else if (eduInstitution.getEducationLevel().isBeginProf())
                discSheet.addCell(new Label(4, row, Integer.toString(eduInstitution.getYearEnd()) + " г.", cellFormats.def));
            else
                discSheet.addCell(new Label(5, row, Integer.toString(eduInstitution.getYearEnd()) + " г.", cellFormats.def));
        }

        ++row; column = 0;
        discSheet.addCell(new Label(column++, ++row, "Индекс", cellFormats.tableHeader));
        discSheet.addCell(new Label(column++, row, "Дисциплина", cellFormats.tableHeader));
        discSheet.addCell(new Label(column++, row, "Семестр", cellFormats.tableHeader));
        discSheet.addCell(new Label(column++, row, "Количество часов всего по УП", cellFormats.tableHeader));
        discSheet.addCell(new Label(column++, row, "Результат итогового контрольного мероприятия", cellFormats.tableHeader));
        discSheet.addCell(new Label(column, row, "Примечание", cellFormats.tableHeader));

        //вписываем список дисцилин
        for (DisciplineWrapper wrapper : CollectionUtils.select(allDisciplines, disciplineWrapper ->
                disciplineWrapper.isMandatory() && !disciplineWrapper.isPractice() && !disciplineWrapper.isProject() && (!onceDisc ||
            disciplineWrapper.getWpca().getStudentWpe().getRegistryElementPart().getNumber() == disciplineWrapper.getWpca().getStudentWpe().getRegistryElementPart().getRegistryElement().getParts())))
        {
            column = 0;
            discSheet.addCell(new Label(column++, ++row, (wrapper.getIndexStr() != null ? wrapper.getIndexStr() : ""), cellFormats.def));
            discSheet.addCell(new Label(column++, row, wrapper.getDisciplineTitle(), cellFormats.def));
            discSheet.addCell(new Number(column++, row, wrapper.getTerm(), cellFormats.def));
            discSheet.addCell(new Number(column++, row, wrapper.getTitalHour(), cellFormats.def));
            discSheet.addCell(new Label(column++, row, wrapper.getActionResult(), cellFormats.def));
            discSheet.addCell(new Label(column, row, wrapper.getComment(), cellFormats.def));
        }

        //вписываем факультативные дисциплины
        row = row + 2;
        int i = 1;

        for (DisciplineWrapper wrapper : CollectionUtils.select(allDisciplines, disciplineWrapper -> !disciplineWrapper.isMandatory() && !disciplineWrapper.isPractice() && !disciplineWrapper.isProject()))
        {
            column = 0;
            discSheet.addCell(new Label(column++, ++row, (wrapper.getIndexStr() != null ? wrapper.getIndexStr() : ""), cellFormats.def));
            discSheet.addCell(new Label(column++, row, "Факультативная дисциплина " + i++, cellFormats.def));
            discSheet.addCell(new Number(column++, row, wrapper.getTerm(), cellFormats.def));
            discSheet.addCell(new Number(column++, row, wrapper.getTitalHour(), cellFormats.def));
            discSheet.addCell(new Label(column++, row, wrapper.getActionResult(), cellFormats.def));
            discSheet.addCell(new Label(column, row, wrapper.getDisciplineTitle(), cellFormats.def));
        }
    }

    private int calculateColumnWidth(String string)
    {
        return Math.min(string.length() * 9, 400) / 7;
    }

    public List<DisciplineWrapper> getDisciplineWrapperList(Student student)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionMark.class, "m").column("m")
            .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("m"), "slot")
            .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("m")), value(student)))
            .where(isNull(property(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))))
            .where(eq(property(SessionDocumentSlot.inSession().fromAlias("slot")), value(Boolean.FALSE)))
            .joinEntity("m", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "book",
                eq(property("book"), property(SessionDocumentSlot.document().fromAlias("slot"))))
            ;
        
        Map<EppStudentWpeCAction, DisciplineWrapper> wrapperMap = new HashMap<>();
        List<DisciplineWrapper> wrappers = new ArrayList<>();
        for (SessionMark mark : dql.createStatement(getSession()).<SessionMark>list()) {
            DisciplineWrapper wrapper = new DisciplineWrapper(mark.getSlot().getStudentWpeCAction(), mark);
            wrappers.add(wrapper);
            wrapperMap.put(mark.getSlot().getStudentWpeCAction(), wrapper);
        }

        Collections.sort(wrappers, DisciplineWrapper.DISCIPLINE_COMPARATOR);

        if (wrappers.isEmpty())
            return wrappers;

        // учебные планы

        final EppStudent2EduPlanVersion s2epv = wrappers.get(0).getWpca().getStudentWpe().getStudentEduPlanVersion();
        //EppEduPlanVersion version = s2epv.getEduPlanVersion();
        IEppEpvBlockWrapper versionWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(s2epv.getBlock().getId(), true);

        Collection<IEppEpvRowWrapper> epvRowWrappers = EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), object -> object.getOwner().isRootBlock() || object.getOwner().equals(s2epv.getBlock()), true);
        
        for (DisciplineWrapper disciplineWrapper : wrappers) {
            for (IEppEpvRowWrapper epvRowWrapper : epvRowWrappers) {
                if (epvRowWrapper.getRow() instanceof EppEpvRegistryRow) {
                    EppRegistryElement registryElement = ((EppEpvRegistryRow) epvRowWrapper.getRow()).getRegistryElement();
                    if (disciplineWrapper.getWpca().getStudentWpe().getRegistryElementPart().getRegistryElement().equals(registryElement))
                        disciplineWrapper.setEpvRow(epvRowWrapper);
                }
            }
        }

        // темы курсовых работ

        for (SessionProjectTheme theme : getList(SessionProjectTheme.class, SessionProjectTheme.slot().studentWpeCAction().studentWpe().student(), student)) {
            DisciplineWrapper wrapper = wrapperMap.get(theme.getSlot().getStudentWpeCAction());
            if (null != wrapper)
                wrapper.setProjectTheme(theme);
        }
        
        return wrappers;
    }

    private class CellFormats
    {
        WritableCellFormat personalHeader;
        WritableCellFormat tableHeader;
        WritableCellFormat def;
        
        private CellFormats() throws WriteException
        {
            personalHeader = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 11, WritableFont.NO_BOLD));
            personalHeader.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
            personalHeader.setBackground(jxl.format.Colour.GREY_25_PERCENT);
            personalHeader.setAlignment(jxl.format.Alignment.CENTRE);
            personalHeader.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

            tableHeader = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD));
            tableHeader.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
            tableHeader.setBackground(jxl.format.Colour.GREY_25_PERCENT);
            tableHeader.setAlignment(jxl.format.Alignment.CENTRE);
            tableHeader.setWrap(true);
            tableHeader.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

            def = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 11, WritableFont.NO_BOLD));
            def.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
            def.setAlignment(jxl.format.Alignment.LEFT);
            def.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THIN);
            def.setWrap(true);
        }
    }
}
