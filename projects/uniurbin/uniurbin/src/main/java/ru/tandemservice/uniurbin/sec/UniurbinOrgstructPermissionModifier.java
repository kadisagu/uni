/**
 *:$
 */
package ru.tandemservice.uniurbin.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.base.util.IOrgUnitLPGVisibilityResolver;
import ru.tandemservice.uni.dao.IOrgstructDAO;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Create by: ashaburov
 * Date: 15.04.11
 */
public class UniurbinOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier, IOrgUnitLPGVisibilityResolver
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniurbin");
        config.setName("uniurbin-ds-sec-config");
        config.setTitle("");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "UnidsReportPermissionGroup", "Отчеты модуля «Сессия»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "printMarkForDiploma_" + code, "Просмотр отчета «Печать оценок для формирования приложения к диплому»");

        }
        securityConfigMetaMap.put(config.getName(), config);
    }

    @Override
    public List<ILPGVisibilityResolver> getResolverList()
    {
        return Arrays.<ILPGVisibilityResolver>asList(new ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "unidsLocal";
            }

            @Override
            public boolean isHidden(OrgUnitType orgUnitType)
            {
                return IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
            }
        });
    }
}
