/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniurbin.dao;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.group.IAbstractGroupTitleDAO;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author dseleznev
 * Created on: 16.07.2009
 */
public class GroupTitleAlgorithmURBIN extends UniBaseDao implements IAbstractGroupTitleDAO
{
    public String getTitle(Group group, int number)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append(group.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());
        buffer.append('-');
        buffer.append(group.getStartEducationYear().getIntValue() % 10);
        buffer.append(group.getEducationOrgUnit().getFormativeOrgUnit().getDivisionCode());
        buffer.append(group.getCourse().getCode());
        buffer.append(String.format("%02d", number % getMaxNumber()));
        return buffer.toString();
    }

    public int getMaxNumber()
    {
        return 100;
    }
}