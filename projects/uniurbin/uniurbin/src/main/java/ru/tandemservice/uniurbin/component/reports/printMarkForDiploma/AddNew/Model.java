/**
 * $Id$
 */
package ru.tandemservice.uniurbin.component.reports.printMarkForDiploma.AddNew;

import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * Create by: Shaburov
 * Date: 12.04.11
 */
@Input(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model
{
    private List<Course> _courseList;
    private List<Group> _groupList;

    private Course _course;
    private Group _group;

    private Long _orgUnitId;

    private boolean onceDiscipline;

    public static String[] EDUCATION_LEVEL_STAGE_HIGH_PROFILE = new String[]
            {
                    UniDefines.EDUCATION_LEVEL_STAGE_HIGH_PROFILE,
                    UniDefines.EDUCATION_LEVEL_STAGE_HALF_HIGH,
                    UniDefines.EDUCATION_LEVEL_STAGE_BACHELORS_DEGREE,
                    UniDefines.EDUCATION_LEVEL_STAGE_CERTIFICATED_SPEICALISTS_TRAINING,
                    UniDefines.EDUCATION_LEVEL_STAGE_MASTERS_DEGREE
            };

    //Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public boolean isOnceDiscipline()
    {
        return onceDiscipline;
    }

    public void setOnceDiscipline(boolean onceDiscipline)
    {
        this.onceDiscipline = onceDiscipline;
    }
}
