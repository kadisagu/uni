package ru.tandemservice.uniurbin.dao;

import jxl.format.Colour;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionSummaryBulletinPrintDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 27.12.12
 */
public class URBINSessionSummaryBulletinPrintDAO extends SessionSummaryBulletinPrintDAO {
    @Override
    protected List<IRtfElement> highlightMarkCell(RtfDocument document, RtfCell cell, SessionMark sessionMark, String value)
    {
        if (null == sessionMark)
            return null;

        boolean highlight = false;
        SessionMarkCatalogItem markCatalogItem = sessionMark.getValueItem();
        if (markCatalogItem instanceof SessionMarkGradeValueCatalogItem)
        {
            SessionMarkGradeValueCatalogItem markValue = (SessionMarkGradeValueCatalogItem) markCatalogItem;
            highlight = !markValue.isPositive();
        }
        if (markCatalogItem instanceof SessionMarkStateCatalogItem)
        {
            SessionMarkStateCatalogItem markValue = (SessionMarkStateCatalogItem) markCatalogItem;
            highlight = ! markValue.isRemarkable();
        }

        if (highlight) {
            final int backgroundColorIndex = document.getHeader().getColorTable().addColor(Colour.PINK.getDefaultRGB().getRed(), Colour.PINK.getDefaultRGB().getGreen(), Colour.PINK.getDefaultRGB().getBlue());
            RtfString string = new RtfString();
            string.append(value);
            cell.setBackgroundColorIndex(backgroundColorIndex);
            return string.toList();
        }
        else
            return null;
    }
}
