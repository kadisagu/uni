/**
 * $Id$
 */
package ru.tandemservice.uniurbin.component.reports.printMarkForDiploma.AddNew;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;

/**
 * Create by: Shaburov
 * Date: 12.04.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onChangeCourse(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepareGroup(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDocumentRenderer content;
        try
        {
            content = getDao().prepareReports(getModel(component));
            BusinessComponentUtils.downloadDocument(content, false);
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }

    }
}
