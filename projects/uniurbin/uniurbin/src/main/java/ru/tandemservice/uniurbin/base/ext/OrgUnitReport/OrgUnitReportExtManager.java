/* $Id:$ */
package ru.tandemservice.uniurbin.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;

/**
 * @author rsizonenko
 * @since 06.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("printMarkForDiploma", new OrgUnitReportDefinition("Печать оценок для формирования приложения к диплому", "printMarkForDiploma", ru.tandemservice.unisession.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNISESSION_ORG_UNIT_REPORT_BLOCK, "ru.tandemservice.uniurbin.component.reports.printMarkForDiploma.AddNew"))
                .create();
    }
}
