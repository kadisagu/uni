package ru.tandemservice.uniurbin.dao;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd.SessionReportGroupMarksDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 27.12.12
 */
public class URBINSessionReportGroupMarksDAO extends SessionReportGroupMarksDAO {

    protected Map<SessionMarkCatalogItem, WritableCellFormat> initCellFormatsForMarkCells(final WritableCellFormat defaultFormat) throws WriteException
    {
        final WritableCellFormat negativeMarksFormat = new WritableCellFormat();
        negativeMarksFormat.setFont(new WritableFont(defaultFormat.getFont()));
        negativeMarksFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        negativeMarksFormat.setAlignment(defaultFormat.getAlignment());
        negativeMarksFormat.setBackground(Colour.PINK);
        
        return SafeMap.get(key -> isMarkNegative(key) ? negativeMarksFormat : defaultFormat);
    }

    private static boolean isMarkNegative(SessionMarkCatalogItem mark)
    {
        if (mark != null) {
            if (mark instanceof SessionMarkGradeValueCatalogItem)
            {
                SessionMarkGradeValueCatalogItem markValue = (SessionMarkGradeValueCatalogItem) mark;
                if (!markValue.isPositive()) return true;
            }
            if (mark instanceof SessionMarkStateCatalogItem)
            {
                SessionMarkStateCatalogItem markValue = (SessionMarkStateCatalogItem) mark;
                if (markValue.isRemarkable()) return true;
            }
        }
        return false;
    }
}
