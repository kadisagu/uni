/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniurbin.scripts

import org.apache.commons.collections.keyvalue.MultiKey
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uniec.dao.examgroup.IExamGroupSetDao
import ru.tandemservice.uniec.entity.entrant.*

//import org.apache.commons.lang.StringUtils

return new EnrollmentExamSheetPrint(                              // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати экзаменационного листа абитуриента (УралГАХА)
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class EnrollmentExamSheetPrint
{
  Session session
  byte[] template
  EntrantRequest entrantRequest
  def im = new RtfInjectModifier()
  def tm = new RtfTableModifier()

  def print()
  {
    def entrant = entrantRequest.entrant
    def card = entrant.person.identityCard
    def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().entrant().id()} = ${entrantRequest.entrant.id}
                order by ${RequestedEnrollmentDirection.priority()}
    /).<RequestedEnrollmentDirection> list();
    def firstDirection = directions.get(0);
    def ou = firstDirection.enrollmentDirection.educationOrgUnit;
    def hasBudgetDirection = directions.collect {it.compensationType.budget}.contains(true);

    im.put('highSchoolTitle', TopOrgUnit.instance.title);
    im.put('contract', hasBudgetDirection ? '' : 'Д');
    im.put('number', entrantRequest.stringNumber);
    im.put('lastName', card.lastName);
    im.put('firstName', card.firstName);
    im.put('middleName', card.middleName);
    im.put('date', entrantRequest.regDate.format('dd.MM.yyyy'));
    im.put('examGroup', firstDirection.group);
    im.put('educationLevelTitlePrimary', ou.educationLevelHighSchool.educationLevel.displayableTitle);
    //im.put('groupFromDirection', StringUtils.trimToEmpty(firstDirection.group))

    // находим все экзаменационные группы, в которых содержится заявление абитуриента
    List<ExamGroup> examGroupList = IExamGroupSetDao.instance.get().getEntrantRequestGroups(entrantRequest);
    im.put('group', examGroupList.collect {it.title}.join(', '))

    fillEnrollmentResults(directions);

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(template, im, tm),
            fileName: "Экзаменационный лист абитуриента ${entrant.person.identityCard.fullFio}.rtf"]
  }

  def fillEnrollmentResults(List<RequestedEnrollmentDirection> directions)
  {
    // формируем блоки уникальных комбинаций выбранных вступительных испытаний
    def blocks = new Hashtable<MultiKey, List<ChosenEntranceDiscipline>>();

    for (def direction : directions)
    {
      def disciplines = DQL.createStatement(session, /
                    from ${ChosenEntranceDiscipline.class.simpleName}
                    where ${ChosenEntranceDiscipline.chosenEnrollmentDirection().id()} = ${direction.id}
                    order by ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title()}
      /).<ChosenEntranceDiscipline> list();

      if (!disciplines.empty)
      {
        def ids = new ArrayList<Long>();
        for (def item : disciplines)
          ids.add(item.enrollmentCampaignDiscipline.id);
        MultiKey key = new MultiKey(ids as Long[]);
        if (!blocks.containsKey(key))
          blocks.put(key, disciplines);
      }
    }

    // вычисляем дисциплины для сдачи
    def entrant = entrantRequest.entrant;
    def formingForEntrant = entrant.enrollmentCampaign.entrantExamListsFormingFeature.formingForEntrant;
    def examListTargetId = formingForEntrant ? entrant.id : entrantRequest.id
    def examPassDisciplines = DQL.createStatement(session, /
                from ${ExamPassDiscipline.class.simpleName}
                where ${ExamPassDiscipline.entrantExamList().entrant().id()} = ${examListTargetId}
                order by ${ExamPassDiscipline.subjectPassForm().code()}
    /).<ExamPassDiscipline> list();

    // вычисляем даты вступительных испытаний
    def discipline2PassDate = examPassDisciplines.<ExamPassDiscipline, Date> collectEntries {
      [it.enrollmentCampaignDiscipline, it.passDate]
    }

    // границы блоков
    final List<Integer> bounds = new ArrayList<Integer>();
    int bound = 0;

    // список строк
    final List<String[]> rows = new ArrayList<String[]>();

    // будем сохранять индексы строк, в которых будет сумма балло
    def indexList = new ArrayList<Integer>()

    // для каждого блока вступительных испытаний
    for (def block : blocks.values())
    {
      boolean allDisciplinesHaveFinalMark = true;
      double totalMark = 0.0;

      // для каждого вступительного испытания в нем
      for (int i = 0; i < block.size(); i++)
      {
        def chosenEntranceDiscipline = block.get(i);
        def enrollmentCampaignDiscipline = chosenEntranceDiscipline.enrollmentCampaignDiscipline;

        // сформировать строку
        String[] row = new String[6];
        row[0] = i + 1;
        row[1] = enrollmentCampaignDiscipline.title;
        row[2] = discipline2PassDate.get(enrollmentCampaignDiscipline)?.format('dd.MM.yyyy');

        // итоговый балл цифрами и прописью
        def finalMark = chosenEntranceDiscipline.finalMark;
        if (finalMark != null)
        {
          long mark = Math.round(finalMark);
          totalMark += finalMark;

          row[3] = mark;
          row[4] = NumberSpellingUtil.spellNumberMasculineGender(mark)
        } else
        {
          allDisciplinesHaveFinalMark = false;
        }

        rows.add(row);
      }

      // вычислим границу блока
      bound += block.size();

      // если итоговый балл есть по всем вступительным испытаниям, то запишем общую сумму
      if (allDisciplinesHaveFinalMark)
      {
        Long total = Math.round(totalMark);
        indexList.add(rows.size())
        rows.add(["Общее количество баллов: ${total} (${NumberSpellingUtil.spellNumberMasculineGender(total)})"] as String[]);
        bound++;
      }

      // запомним границу блока
      bounds.add(bound);

      // между блоками вставим пустые строки
      if (bounds.size() < blocks.size())
      {
        rows.add(null as String[]);
        bound++;
      }
    }
    tm.put('T', rows as String[][]);
    tm.put('T', new RtfRowIntercepterBase(){
      @Override
      public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
      {
        // объединим все ячейки в строках с итоговым баллом
        for (def i : indexList)
        {
          RtfRow row = newRowList.get(startIndex + i)
          RtfUtil.unitAllCell(row, 0);

          // и уберем границу блока
          RtfCellBorder cellBorder = row.cellList.get(0).cellBorder;
          cellBorder.left = null;
          cellBorder.bottom = null;
          cellBorder.right = null;
        }

        // для каждого блока
        for (int i = 0; i < bounds.size(); i++)
        {
          // вычислим номер строки после блока с учетом уже вставленных строк
          int index = startIndex + i + bounds.get(i);

          // если блок не последний
          if (i != bounds.size() - 1)
          {
            // убрать границы для строки после блока
            for (RtfCell cell : newRowList.get(index).cellList)
              cell.cellBorder = null

            // и вставить строку с шапкой
            newRowList.add(index + 1, newRowList.get(0).clone);
          }
        }
      }
    });
  }
}
