/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.logic.IRostgmuEnrReportDao;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.logic.RostgmuEnrReportDao;

/**
 * @author Andrey Avetisov
 * @since 12.02.2015
 */
@Configuration
public class RostgmuEnrReportManager extends BusinessObjectManager
{
    public static RostgmuEnrReportManager instance()
    {
        return instance(RostgmuEnrReportManager.class);
    }
    @Bean
    public IRostgmuEnrReportDao dao()
    {
        return new RostgmuEnrReportDao();
    }

}