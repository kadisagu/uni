package ru.tandemservice.unirostgmu.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirostgmu.entity.gen.*;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.RequestCompetitionSummaryAdd.RostgmuEnrReportRequestCompetitionSummaryAdd;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.unirostgmu.entity.gen.RostgmuEnrReportRequestCompetitionSummaryGen */
public class RostgmuEnrReportRequestCompetitionSummary extends RostgmuEnrReportRequestCompetitionSummaryGen implements IEnrReport
{
    public static final String REPORT_KEY = "rostgmuEnr14ReportReqCompSummary";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_REQUEST_TYPE,
                                                           P_COMPENSATION_TYPE,
                                                           P_PROGRAM_FORM,
                                                           P_COMPETITION_TYPE,
                                                           P_ENR_ORG_UNIT,
                                                           P_FORMATIVE_ORG_UNIT,
                                                           P_PROGRAM_SUBJECT,
                                                           P_EDU_PROGRAM,
                                                           P_PROGRAM_SET,
                                                           P_PARALLEL,
                                                           P_ENTRANT_STATE,
                                                           P_INCLUDE_TOOK_AWAY_DOCUMENTS);

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return RostgmuEnrReportRequestCompetitionSummary.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return RostgmuEnrReportRequestCompetitionSummaryAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводная информация о приеме заявлений и конкурсе (РостГМУ)»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводная информация о приеме заявлений и конкурсе (РостГМУ)»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}