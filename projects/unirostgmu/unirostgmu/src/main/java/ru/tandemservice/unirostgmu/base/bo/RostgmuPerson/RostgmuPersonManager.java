/* $Id$ */
package ru.tandemservice.unirostgmu.base.bo.RostgmuPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
@Configuration
public class RostgmuPersonManager extends BusinessObjectManager
{
}