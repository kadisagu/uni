package ru.tandemservice.unirostgmu.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirostgmu.entity.gen.*;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EstimatedPassingScoreAdd.RostgmuEnrReportEstimatedPassingScoreAdd;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.unirostgmu.entity.gen.RostgmuEnrReportEstimatedPassingScoreListGen */
public class RostgmuEnrReportEstimatedPassingScoreList extends RostgmuEnrReportEstimatedPassingScoreListGen implements IEnrReport
{
    public static final String REPORT_KEY = "rostgmuEnr14ReportEstimatePassingScoreList";

    private static List<String> properties = Arrays.asList(P_REQUEST_TYPE,
                                                           P_COMPENSATION_TYPE,
                                                           P_PROGRAM_FORM,
                                                           P_COMPETITION_TYPE,
                                                           P_ENR_ORG_UNIT,
                                                           P_FORMATIVE_ORG_UNIT,
                                                           P_PROGRAM_SUBJECT,
                                                           P_EDU_PROGRAM,
                                                           P_PROGRAM_SET,
                                                           P_HAS_ORIGINAL_DOC);

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return RostgmuEnrReportEstimatedPassingScoreList.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return RostgmuEnrReportEstimatedPassingScoreAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводный рейтинг поступающих (РостГМУ)»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводный рейтинг поступающих (РостГМУ)»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}