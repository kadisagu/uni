/* $Id$ */
package ru.tandemservice.unirostgmu.util;

import com.google.common.net.InetAddresses;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
public class IPAddressUtils
{
    private static Pattern VALID_IPV4_PATTERN = null;
    private static Pattern VALID_IPV6_PATTERN = null;
    private static final String ipv4Pattern = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
    private static final String ipv6Pattern = "([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}";

    static {
        try {
            VALID_IPV4_PATTERN = Pattern.compile(ipv4Pattern, Pattern.CASE_INSENSITIVE);
            VALID_IPV6_PATTERN = Pattern.compile(ipv6Pattern, Pattern.CASE_INSENSITIVE);
        } catch (PatternSyntaxException e) {
            //logger.severe("Unable to compile pattern", e);
        }
    }

    /**
     * Проверка, что адрес IPV6
     */
    public static boolean isIpV6Address(String ipAddress)
    {
        return (VALID_IPV6_PATTERN != null && VALID_IPV6_PATTERN.matcher(ipAddress).matches());
    }


    /**
     * Validate ip address with regular expression
     * Допускаем только IPv4
     * @param ip ip address for validation
     * @return true valid ip address, false invalid ip address
     */
    public static boolean isIpV4Address(final String ip)
    {
        return (VALID_IPV4_PATTERN != null && VALID_IPV4_PATTERN.matcher(ip).matches());
    }


    public static String ipAddressV6ToV4(String addressV6)
    {
        InetAddress address = InetAddresses.forString(addressV6);
        String result = null;
        if (address instanceof Inet6Address)
        {
            Inet4Address address4 = InetAddresses.getCoercedIPv4Address(address);
            result = address4.getHostAddress();
        }

        return result;
    }


        //Переводит ip-адрес в десятичную форму
    public static Long ipToLong(String ipAddress) throws NumberFormatException
    {
        String[] ipAddressInArray = ipAddress.replaceAll(" ", "").split("\\.");
        long result = 0;
        try
        {
            for (int i = 0; i < ipAddressInArray.length; i++)
            {
                int power = 3 - i;
                int ip = Integer.parseInt(ipAddressInArray[i]);
                result += ip * Math.pow(256, power);
            }
        }
        catch(NumberFormatException e)
        {
            result = 0;
        }
        return result;
    }



    //Ищет находится ли IP-адрес в заданном диапаозне вида xxxx.xxxx.xxxx.xxxx-xxxx.xxxx.xxxx.xxx
    public static boolean isIpInRange(String currentIP, String ipRange)
    {
        String[] ipRangeEdges = ipRange.replaceAll(" ", "").split("-", 2);
        long currentIpInDecimalFormat = ipToLong(currentIP);
        long fistIpInDecimalFormat = ipToLong(ipRangeEdges[0]);
        long secondIpInDecimalFormat = ipToLong(ipRangeEdges[1]);
        return currentIpInDecimalFormat >= fistIpInDecimalFormat && currentIpInDecimalFormat <= secondIpInDecimalFormat;
    }


    /**
     * Ищет находится ли ip-адрес в списке ip-адресов и диапазонов ip-адресов
     *
     * @param currentIP искомый ip-адресс
     * @param ipList    список ip-адресов и диапазонов ip-адресов
     * @return находится ли ip-адрес в списке ip-адресов и диапазонов ip-адресов
     */
    public static boolean isIpInList(String currentIP, List<String> ipList)
    {
        boolean correctIp = false;

        for (String ipAddress : ipList)
        {
            if ((ipAddress.contains("-") && IPAddressUtils.isIpInRange(currentIP, ipAddress)) || ipAddress.replaceAll(" ", "").equals(currentIP))
            {
                correctIp = true;
                break;
            }
        }

        return correctIp;
    }
}