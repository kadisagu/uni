package ru.tandemservice.unirostgmu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unirostgmu_2x7x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность rostgmuEnrReportEntrantList

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("rostgmu_enr14_rep_entr_list_t"))
            {
                DBTable dbt = new DBTable("rostgmu_enr14_rep_entr_list_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                          new DBColumn("datefrom_p", DBType.TIMESTAMP).setNullable(false),
                                          new DBColumn("dateto_p", DBType.TIMESTAMP).setNullable(false),
                                          new DBColumn("requesttype_p", DBType.createVarchar(255)),
                                          new DBColumn("compensationtype_p", DBType.createVarchar(255)),
                                          new DBColumn("programform_p", DBType.createVarchar(255)),
                                          new DBColumn("competitiontype_p", DBType.createVarchar(255)),
                                          new DBColumn("enrorgunit_p", DBType.TEXT),
                                          new DBColumn("formativeorgunit_p", DBType.TEXT),
                                          new DBColumn("programsubject_p", DBType.TEXT),
                                          new DBColumn("eduprogram_p", DBType.TEXT),
                                          new DBColumn("programset_p", DBType.TEXT),
                                          new DBColumn("parallel_p", DBType.TEXT)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("rostgmuEnrReportEntrantList");
            }
		}


    }
}