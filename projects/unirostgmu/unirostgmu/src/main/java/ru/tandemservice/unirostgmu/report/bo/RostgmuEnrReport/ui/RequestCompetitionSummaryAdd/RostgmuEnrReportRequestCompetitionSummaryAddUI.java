/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.RequestCompetitionSummaryAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.RostgmuEnrReportManager;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 16.02.2015
 */
public class RostgmuEnrReportRequestCompetitionSummaryAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private Date _dateFrom;
    private Date _dateTo;
    private boolean _parallelActive;
    private IdentifiableWrapper _parallel;
    private List<IdentifiableWrapper> _parallelList;
    private boolean _entrantStateActive;
    private List<EnrEntrantState> _entrantStateList;
    private boolean _includeTookAwayDocuments;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
        configUtil(getCompetitionFilterAddon());

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "включать в отчет только выбранные параллельно условия поступления")));
    }

    //listeners
    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
    }

    public void onClickApply()
    {
        validate();
        Long reportId = RostgmuEnrReportManager.instance().dao().createRequestCompetitionSummaryReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if (getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }
    // utils
    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util
                .clearWhereFilter()
                .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, true, true, false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, new CommonFilterFormConfig(true, false, true, false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, new CommonFilterFormConfig(true, true, true, false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, new CommonFilterFormConfig(true, true, true, false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, new CommonFilterFormConfig(true, true, true, false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, new CommonFilterFormConfig(true, true, true, false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, new CommonFilterFormConfig(true, true, true, false, false, false));

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }


    // for report builder

    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }
    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }

    //getters & setters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public IdentifiableWrapper getParallel()
    {
        return _parallel;
    }

    public void setParallel(IdentifiableWrapper parallel)
    {
        _parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        _parallelList = parallelList;
    }

    public boolean isEntrantStateActive()
    {
        return _entrantStateActive;
    }

    public void setEntrantStateActive(boolean entrantStateActive)
    {
        _entrantStateActive = entrantStateActive;
    }

    public List<EnrEntrantState> getEntrantStateList()
    {
        return _entrantStateList;
    }

    public void setEntrantStateList(List<EnrEntrantState> entrantStateList)
    {
        _entrantStateList = entrantStateList;
    }

    public boolean isIncludeTookAwayDocuments()
    {
        return _includeTookAwayDocuments;
    }

    public void setIncludeTookAwayDocuments(boolean includeTookAwayDocuments)
    {
        _includeTookAwayDocuments = includeTookAwayDocuments;
    }
}
