/* $Id$ */
package ru.tandemservice.unirostgmu.base.ext.Person.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.person.base.bo.Person.logic.PersonDao;

/**
 * @author Igor Belanov
 * @since 23.11.2016
 */
public class RostgmuPersonDao extends PersonDao
{
    // TODO: 23.11.16 нужно будет перенести это в шаред и сделать настройку типа "выполнять валидацию логинов на пробелы по бокам"
    @Override
    public void updatePersonLogin(Long personId, String newLogin, String newPassword)
    {
        if (newLogin != null && !newLogin.equals(StringUtils.trim(newLogin)))
            throw new ApplicationException("Нельзя создать логин с пробелами в начале или в конце");
        super.updatePersonLogin(personId, newLogin, newPassword);
    }

    @Override
    public void updatePersonLogin(Long personId, String newLogin, String newPassword, AuthenticationType authenticationType)
    {
        if (newLogin != null && !newLogin.equals(StringUtils.trim(newLogin)))
            throw new ApplicationException("Нельзя создать логин с пробелами в начале или в конце");
        super.updatePersonLogin(personId, newLogin, newPassword, authenticationType);
    }
}
