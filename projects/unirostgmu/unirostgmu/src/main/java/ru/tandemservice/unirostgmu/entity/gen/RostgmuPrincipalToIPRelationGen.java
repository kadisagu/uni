package ru.tandemservice.unirostgmu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь пользователя с IP-адресами
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RostgmuPrincipalToIPRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation";
    public static final String ENTITY_NAME = "rostgmuPrincipalToIPRelation";
    public static final int VERSION_HASH = -1714100617;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRINCIPAL = "principal";
    public static final String P_IP_ADDRESS = "ipAddress";

    private Principal _principal;     // Пользователь
    private String _ipAddress;     // ip-адрес

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Пользователь. Свойство не может быть null.
     */
    @NotNull
    public Principal getPrincipal()
    {
        return _principal;
    }

    /**
     * @param principal Пользователь. Свойство не может быть null.
     */
    public void setPrincipal(Principal principal)
    {
        dirty(_principal, principal);
        _principal = principal;
    }

    /**
     * @return ip-адрес. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIpAddress()
    {
        return _ipAddress;
    }

    /**
     * @param ipAddress ip-адрес. Свойство не может быть null.
     */
    public void setIpAddress(String ipAddress)
    {
        dirty(_ipAddress, ipAddress);
        _ipAddress = ipAddress;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RostgmuPrincipalToIPRelationGen)
        {
            setPrincipal(((RostgmuPrincipalToIPRelation)another).getPrincipal());
            setIpAddress(((RostgmuPrincipalToIPRelation)another).getIpAddress());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RostgmuPrincipalToIPRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RostgmuPrincipalToIPRelation.class;
        }

        public T newInstance()
        {
            return (T) new RostgmuPrincipalToIPRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "principal":
                    return obj.getPrincipal();
                case "ipAddress":
                    return obj.getIpAddress();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "principal":
                    obj.setPrincipal((Principal) value);
                    return;
                case "ipAddress":
                    obj.setIpAddress((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "principal":
                        return true;
                case "ipAddress":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "principal":
                    return true;
                case "ipAddress":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "principal":
                    return Principal.class;
                case "ipAddress":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RostgmuPrincipalToIPRelation> _dslPath = new Path<RostgmuPrincipalToIPRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RostgmuPrincipalToIPRelation");
    }
            

    /**
     * @return Пользователь. Свойство не может быть null.
     * @see ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation#getPrincipal()
     */
    public static Principal.Path<Principal> principal()
    {
        return _dslPath.principal();
    }

    /**
     * @return ip-адрес. Свойство не может быть null.
     * @see ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation#getIpAddress()
     */
    public static PropertyPath<String> ipAddress()
    {
        return _dslPath.ipAddress();
    }

    public static class Path<E extends RostgmuPrincipalToIPRelation> extends EntityPath<E>
    {
        private Principal.Path<Principal> _principal;
        private PropertyPath<String> _ipAddress;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Пользователь. Свойство не может быть null.
     * @see ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation#getPrincipal()
     */
        public Principal.Path<Principal> principal()
        {
            if(_principal == null )
                _principal = new Principal.Path<Principal>(L_PRINCIPAL, this);
            return _principal;
        }

    /**
     * @return ip-адрес. Свойство не может быть null.
     * @see ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation#getIpAddress()
     */
        public PropertyPath<String> ipAddress()
        {
            if(_ipAddress == null )
                _ipAddress = new PropertyPath<String>(RostgmuPrincipalToIPRelationGen.P_IP_ADDRESS, this);
            return _ipAddress;
        }

        public Class getEntityClass()
        {
            return RostgmuPrincipalToIPRelation.class;
        }

        public String getEntityName()
        {
            return "rostgmuPrincipalToIPRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
