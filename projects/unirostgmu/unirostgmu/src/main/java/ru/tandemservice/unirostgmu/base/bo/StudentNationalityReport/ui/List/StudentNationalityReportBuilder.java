/*$Id$*/
package ru.tandemservice.unirostgmu.base.bo.StudentNationalityReport.ui.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.*;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 10.11.2015
 */
final class StudentNationalityReportBuilder
{
    final List<Student> students;
    final Collection<OrgUnit> orgUnitList;

    public StudentNationalityReportBuilder(List<Student> students, Collection<OrgUnit> orgUnitList)
    {
        this.students = students;
        this.orgUnitList = orgUnitList;
    }

    public IDocumentRenderer buildDocumentRenderer(String fileName) throws IOException, WriteException
    {
        return new CommonBaseRenderer().xml().fileName(fileName + ".xls").document(buildDocument());
    }

    public ByteArrayOutputStream buildDocument() throws IOException, WriteException
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        //Создает страницу
        WritableSheet sheet = workbook.createSheet("1", workbook.getSheets().length);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        addSheet(sheet);

        //посылаем отчет
        workbook.write();
        workbook.close();
        return out;
    }

    private void addSheet(WritableSheet sheet) throws WriteException
    {
        final int lineLength = 11;
        int currentRow = 0;
        //-------------------------------------------------------
        for (int i = 0; i < lineLength; i++)
        {
            sheet.setColumnView(i, 20);
        }
        //HEADER-------------------------------------------------
        {
            final WritableFont times14b = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
            final WritableCellFormat cellFormat14 = new WritableCellFormat(times14b);
            cellFormat14.setAlignment(Alignment.CENTRE);
            cellFormat14.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat14.setWrap(true);

            fillLine(currentRow, sheet, lineLength, "Сведения", cellFormat14);
            sheet.setRowView(currentRow, 500);
            currentRow++;
            fillLine(currentRow, sheet, lineLength, getFormativeOrgUnitTitles("о студентах "), cellFormat14);
            sheet.setRowView(currentRow, 600);
            currentRow++;
            fillLine(currentRow, sheet, lineLength, "по состоянию на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()), cellFormat14);
            sheet.setRowView(currentRow, 500);
            currentRow += 2;
        }
        //-------------------------------------------------------
        final WritableFont times12b = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        final WritableCellFormat cellFormat12 = new WritableCellFormat(times12b);
        cellFormat12.setAlignment(Alignment.CENTRE);
        cellFormat12.setVerticalAlignment(VerticalAlignment.CENTRE);
        cellFormat12.setBorder(Border.ALL, jxl.format.BorderLineStyle.THIN, Colour.BLACK);
        cellFormat12.setWrap(true);
        //TABLE HEADER-------------------------------------------
        {
            currentRow++;
            String[] labels = {
                    "№ п/п",//1
                    "Фамилия, имя, отчество (полностью)",//2
                    "Формирующее подразделение",//3
                    "Курс",//4
                    "Дата рождения",//5
                    "Паспортные данные",//6
                    "Адрес места жительства (регистрация по паспорту)",//7
                    "Адрес места фактического проживания",//8
                    "Национальность",//9
                    "Успеваемость (посещаемость), количество пропусков за текущий учебный год",//10
                    "Сотрудник, ответственный за воспитательную работу"//11
            };
            fillRow(currentRow, sheet, lineLength, labels, cellFormat12);
            currentRow++;
            fillRowNumbers(currentRow, sheet, lineLength, cellFormat12);
            currentRow++;
        }
        //TABLE BODY---------------------------------------------
        for (String[] labels : prepareStudents(students))
        {
            fillRow(currentRow++, sheet, lineLength, labels, cellFormat12);
        }
        //-------------------------------------------------------
    }

    private List<String[]> prepareStudents(List<Student> studentList)
    {
        final ArrayList<String[]> result = new ArrayList<>();
        int counter = 1;
        for (final Student student : studentList)
        {
            final Person person = student.getPerson();
            final IdentityCard identityCard = person.getIdentityCard();
            String[] line = {
                    String.valueOf(counter),//1
                    student.getFullFio(),//2
                    student.getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle(),//3
                    student.getCourse().getTitle(),//4
                    person.getBirthDateStr(),//5
                    identityCard.getTitle(),//6
                    person.getAddressRegistration() == null ? "" : person.getAddressRegistration().getTitleWithFlat(),//7
                    person.getAddress() == null ? "" : person.getAddress().getTitleWithFlat(),//8
                    identityCard.getNationality() == null ? "" : identityCard.getNationality().getTitle(),//9
            };
            result.add(line);
            counter += 1;
        }
        return result;
    }


    private String getFormativeOrgUnitTitles(String prefix)
    {
        StringBuilder sb = new StringBuilder(prefix);
        if (orgUnitList != null && !orgUnitList.isEmpty())
        {
            StringJoiner titles = new StringJoiner(",");
            for (OrgUnit orgUnit : orgUnitList)
            {
                String label = orgUnit.getGenitiveCaseTitle();
                if (label != null) titles.add(label);
            }
            sb.append(titles.toString());
        }
        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance();
        sb.append(" ");
        sb.append(topOrgUnit.getGenitiveCaseTitle());
        return sb.toString();
    }

    private void fillRow(int currentRow, WritableSheet sheet, int rowLength, String[] labels, CellFormat cf) throws WriteException
    {
        String label;
        for (int i = 0; i < rowLength; i++)
        {
            label = i < labels.length ? labels[i] : "";
            sheet.addCell(new Label(i, currentRow, label, cf));
        }
    }

    private void fillRowNumbers(int currentRow, WritableSheet sheet, int rowLength, CellFormat cf) throws WriteException
    {
        String label;
        for (int i = 0; i < rowLength; i++)
        {
            label = String.valueOf(i + 1);
            sheet.addCell(new Label(i, currentRow, label, cf));
        }
    }

    private void fillLine(int currentRow, WritableSheet sheet, int rowLength, String label, CellFormat cf) throws WriteException
    {
        int firstColumn = 0;
        sheet.addCell(new Label(firstColumn, currentRow, label, cf));
        sheet.mergeCells(firstColumn, currentRow, firstColumn + --rowLength, currentRow);
    }
}
