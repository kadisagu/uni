/* $Id$ */
package ru.tandemservice.unirostgmu.base.bo.RostgmuPerson.ui.AccessRestrictionAddEdit;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation;
import ru.tandemservice.unirostgmu.util.IPAddressUtils;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
@Input({
        @Bind(key = RostgmuPersonAccessRestrictionAddEditUI.PERSON_ID, binding = "personId", required = true),
        @Bind(key = RostgmuPersonAccessRestrictionAddEditUI.PRINCIPAL_TO_IP_RELATION_ID, binding = "principalToIPRelation.id")
})
public class RostgmuPersonAccessRestrictionAddEditUI extends UIPresenter
{
        public static final String PERSON_ID = "personId";
        public static final String PRINCIPAL_TO_IP_RELATION_ID = "";

        private RostgmuPrincipalToIPRelation _principalToIPRelation = new RostgmuPrincipalToIPRelation();
        private String _ipAddress;
        private Long _personId;
        private Principal _principal;
        private boolean _editForm;

        @Override
        public void onComponentRefresh()
        {
            setPrincipal(DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, getPersonId()).getPrincipal());
            if (getPrincipalToIPRelation().getId() != null)
            {
                setEditForm(false);
                setPrincipalToIPRelation(DataAccessServices.dao().get(RostgmuPrincipalToIPRelation.class,
                                                                      RostgmuPrincipalToIPRelation.P_ID, getPrincipalToIPRelation().getId()));

                setIpAddress(getPrincipalToIPRelation().getIpAddress());
            }
        }

        @Override
        public void onComponentRender()
        {
            ContextLocal.beginPageTitlePart(_uiConfig.getProperty(isEditForm() ? "ui.sticker.edit" : "ui.sticker.add"));
        }

        //listeners
        public void onClickApply()
        {
            getPrincipalToIPRelation().setPrincipal(getPrincipal());
            getPrincipalToIPRelation().setIpAddress(getIpAddress());
            DataAccessServices.dao().saveOrUpdate(getPrincipalToIPRelation());
            deactivate();
        }



        //validator
        //проверяет, что введен текст в формате xxx.xxx.xxx.xxx, где вместо xxx может присутствовать любое число от 0 до 255
        public BaseValidator getIpValidator()
        {
            return new BaseValidator()
            {
                @Override
                public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
                {
                    boolean incorrectIP = false;
                    String[] ipAddresses = ((String) object).replace(" ", "").split("-", 2);


                    Long firstIpInDecimalFormat;
                    Long secondIpInDecimalFormat = Long.MAX_VALUE;
                    for (String ipAddress : ipAddresses)
                    {
                        if (!IPAddressUtils.isIpV4Address(ipAddress))
                        {
                            incorrectIP = true;
                            break;
                        }
                    }

                    if (!incorrectIP)
                    {
                        try
                        {
                            firstIpInDecimalFormat = IPAddressUtils.ipToLong(ipAddresses[0]);
                            if (ipAddresses.length > 1)
                            {
                                secondIpInDecimalFormat = IPAddressUtils.ipToLong(ipAddresses[1]);
                            }
                            if (firstIpInDecimalFormat > secondIpInDecimalFormat)
                            {
                                throw new ValidatorException("Первый IP-адрес диапазона должен быть меньше второго.");
                            }
                        }
                        catch (NumberFormatException e)
                        {
                            incorrectIP = true;
                        }
                    }
                    if (incorrectIP)
                    {
                        throw new ValidatorException("Введен некорректный IP-адрес.");
                    }
                }
            };
        }

        //getters and setters
        public String getIpAddress()
        {
            return _ipAddress;
        }

        public void setIpAddress(String ipAddress)
        {
            this._ipAddress = ipAddress;
        }

        public Long getPersonId()
        {
            return _personId;
        }

        public void setPersonId(Long personId)
        {
            _personId = personId;
        }

        public Principal getPrincipal()
        {
            return _principal;
        }

        public void setPrincipal(Principal principal)
        {
            _principal = principal;
        }

        public boolean isEditForm()
        {
            return _editForm;
        }

        public void setEditForm(boolean editForm)
        {
            _editForm = editForm;
        }

        public RostgmuPrincipalToIPRelation getPrincipalToIPRelation()
        {
            return _principalToIPRelation;
        }

        public void setPrincipalToIPRelation(RostgmuPrincipalToIPRelation principalToIPRelation)
        {
            _principalToIPRelation = principalToIPRelation;
        }
}