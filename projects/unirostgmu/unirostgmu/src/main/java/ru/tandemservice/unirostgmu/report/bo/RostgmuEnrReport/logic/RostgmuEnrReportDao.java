/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unirostgmu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportEnrollmentMeetingList;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportEntrantList;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportEstimatedPassingScoreList;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportRequestCompetitionSummary;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EnrollmentMeetingAdd.RostgmuEnrReportEnrollmentMeetingAddUI;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EntrantListAdd.RostgmuEnrReportEntrantListAddUI;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EstimatedPassingScoreAdd.RostgmuEnrReportEstimatedPassingScoreAddUI;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.RequestCompetitionSummaryAdd.RostgmuEnrReportRequestCompetitionSummaryAddUI;

import java.util.Date;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 13.02.2015
 */
public class RostgmuEnrReportDao extends UniBaseDao implements IRostgmuEnrReportDao
{
    @Override
    public Long createEstimatedPassingScoreReport(RostgmuEnrReportEstimatedPassingScoreAddUI model)
    {
        RostgmuEnrReportEstimatedPassingScoreList report = new RostgmuEnrReportEstimatedPassingScoreList();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, RostgmuEnrReportEstimatedPassingScoreList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, RostgmuEnrReportEstimatedPassingScoreList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, RostgmuEnrReportEstimatedPassingScoreList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, RostgmuEnrReportEstimatedPassingScoreList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, RostgmuEnrReportEstimatedPassingScoreList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RostgmuEnrReportEstimatedPassingScoreList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RostgmuEnrReportEstimatedPassingScoreList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RostgmuEnrReportEstimatedPassingScoreList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RostgmuEnrReportEstimatedPassingScoreList.P_PROGRAM_SET, "title");

        if (model.isOriginalDocActive()) {
            report.setHasOriginalDoc(model.getOriginalDoc().getTitle());
        }


        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.ROSTGMU_REPORT_ESTIMATED_PASSING_SCORE_LIST);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));


        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(mainDoc));
        content.setFilename("RostgmuEnrEstimatedPassingScoreReport.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }

    @Override
    public Long createRequestCompetitionSummaryReport(RostgmuEnrReportRequestCompetitionSummaryAddUI model)
    {
        RostgmuEnrReportRequestCompetitionSummary report = new RostgmuEnrReportRequestCompetitionSummary();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, RostgmuEnrReportEstimatedPassingScoreList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, RostgmuEnrReportEstimatedPassingScoreList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, RostgmuEnrReportEstimatedPassingScoreList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RostgmuEnrReportEstimatedPassingScoreList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RostgmuEnrReportEstimatedPassingScoreList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RostgmuEnrReportEstimatedPassingScoreList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RostgmuEnrReportEstimatedPassingScoreList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }
        if (model.isEntrantStateActive())
        {
            String entrantStates = StringUtils.join(CommonBaseUtil.<String>getPropertiesList(model.getEntrantStateList(), "title"), ";");
            report.setEntrantState(entrantStates);
        }
        report.setIncludeTookAwayDocuments(model.isIncludeTookAwayDocuments() ? "Включены в отчет" : "Не включены в отчет");

        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.ROSTGMU_REPORT_REQUEST_COMPETITION_SUMMARY);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));


        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(mainDoc));

        content.setFilename("EnrReportRequestCompetitionSummary.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    @Override
    public Long createEnrollmentMeetingListReport(RostgmuEnrReportEnrollmentMeetingAddUI model)
    {
        RostgmuEnrReportEnrollmentMeetingList report = new RostgmuEnrReportEnrollmentMeetingList();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, RostgmuEnrReportEnrollmentMeetingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, RostgmuEnrReportEnrollmentMeetingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, RostgmuEnrReportEnrollmentMeetingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, RostgmuEnrReportEnrollmentMeetingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, RostgmuEnrReportEnrollmentMeetingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RostgmuEnrReportEnrollmentMeetingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RostgmuEnrReportEnrollmentMeetingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RostgmuEnrReportEnrollmentMeetingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RostgmuEnrReportEnrollmentMeetingList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }
        if (model.isOriginalDocActive())
        {
            report.setHasOriginalDoc(model.getOriginalDoc().getTitle());
        }

        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.ROSTGMU_REPORT_ENROLLMENT_MEETING_LIST);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));

        content.setFilename("EnrReportEnrollmentMeetingList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    @Override
    public Long createEntrantListReport(RostgmuEnrReportEntrantListAddUI model)
    {
        RostgmuEnrReportEntrantList report = new RostgmuEnrReportEntrantList();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, RostgmuEnrReportEntrantList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, RostgmuEnrReportEntrantList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, RostgmuEnrReportEntrantList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, RostgmuEnrReportEntrantList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, RostgmuEnrReportEntrantList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RostgmuEnrReportEntrantList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RostgmuEnrReportEntrantList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RostgmuEnrReportEntrantList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RostgmuEnrReportEntrantList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }


        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.ROSTGMU_REPORT_ENTRANT_LIST);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));

        content.setFilename("EnrReportEntrantList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }
}
