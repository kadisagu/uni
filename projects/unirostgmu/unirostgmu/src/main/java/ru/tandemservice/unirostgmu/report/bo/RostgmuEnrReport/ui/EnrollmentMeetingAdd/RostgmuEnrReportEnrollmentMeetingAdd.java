/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EnrollmentMeetingAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

/**
 * @author Andrey Avetisov
 * @since 17.02.2015
 */
@Configuration
public class RostgmuEnrReportEnrollmentMeetingAdd extends BusinessComponentManager
{
    public static final String ACHIEVEMENT_TYPE_DS = "achievementTypeDS";
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(ACHIEVEMENT_TYPE_DS, achievementTypeDSHandler()).addColumn("achievementTitle", EnrEntrantAchievementType.achievementKind().title().s()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler achievementTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantAchievementType.class)
                .where(EnrEntrantAchievementType.enrollmentCampaign(), ENROLLMENT_CAMPAIGN)
                .where(EnrEntrantAchievementType.marked(), true)
                .order(EnrEntrantAchievementType.achievementKind().title())
                .filter(EnrEntrantAchievementType.achievementKind().title());
    }
}
