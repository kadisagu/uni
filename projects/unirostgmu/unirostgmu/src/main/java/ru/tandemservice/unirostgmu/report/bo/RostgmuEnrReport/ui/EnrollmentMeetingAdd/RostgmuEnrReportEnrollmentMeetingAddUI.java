/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EnrollmentMeetingAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.RostgmuEnrReportManager;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 17.02.2015
 */
public class RostgmuEnrReportEnrollmentMeetingAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date _dateFrom;
    private Date _dateTo;

    private boolean _parallelActive;
    private IdentifiableWrapper _parallel;
    private List<IdentifiableWrapper> _parallelList;

    private boolean _originalDocActive;
    private DataWrapper _originalDoc;

    private boolean _skipEmptyList;
    private boolean _skipEmptyCompetition;
    private boolean _replaceProgramSetTitle;
    private boolean _skipProgramSetTitle;

    private boolean _printPriorityColumn;
    private boolean _entrantAchievementActive;
    private List<EnrEntrantAchievementType>_achievementTypeList;

    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if (getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "включать в отчет только выбранные параллельно условия поступления")));

        setParallelActive(false);
        setParallel(null);
        setAchievementTypeList(null);
        setEntrantAchievementActive(false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (RostgmuEnrReportEnrollmentMeetingAdd.ACHIEVEMENT_TYPE_DS.equals(dataSource.getName()))
        {
            dataSource.put(RostgmuEnrReportEnrollmentMeetingAdd.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
        }
    }


    public void onClickApply()
    {
        validate();
        Long reportId = RostgmuEnrReportManager.instance().dao().createEnrollmentMeetingListReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();

    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    // for report builder

    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }

    // utils

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util
                .clearWhereFilter()
                .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    //getters & setters
    public boolean isPrintPriorityColumn()
    {
        return _printPriorityColumn;
    }

    public void setPrintPriorityColumn(boolean printPriorityColumn)
    {
        _printPriorityColumn = printPriorityColumn;
    }

    public List<EnrEntrantAchievementType> getAchievementTypeList()
    {
        return _achievementTypeList;
    }

    public void setAchievementTypeList(List<EnrEntrantAchievementType> achievementTypeList)
    {
        _achievementTypeList = achievementTypeList;
    }

    public boolean isEntrantAchievementActive()
    {
        return _entrantAchievementActive;
    }

    public void setEntrantAchievementActive(boolean entrantAchievementActive)
    {
        _entrantAchievementActive = entrantAchievementActive;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public IdentifiableWrapper getParallel()
    {
        return _parallel;
    }

    public void setParallel(IdentifiableWrapper parallel)
    {
        _parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        _parallelList = parallelList;
    }

    public boolean isSkipEmptyList()
    {
        return _skipEmptyList;
    }

    public void setSkipEmptyList(boolean skipEmptyList)
    {
        _skipEmptyList = skipEmptyList;
    }

    public boolean isSkipEmptyCompetition()
    {
        return _skipEmptyCompetition;
    }

    public void setSkipEmptyCompetition(boolean skipEmptyCompetition)
    {
        _skipEmptyCompetition = skipEmptyCompetition;
    }

    public boolean isReplaceProgramSetTitle()
    {
        return _replaceProgramSetTitle;
    }

    public void setReplaceProgramSetTitle(boolean replaceProgramSetTitle)
    {
        _replaceProgramSetTitle = replaceProgramSetTitle;
    }

    public boolean isSkipProgramSetTitle()
    {
        return _skipProgramSetTitle;
    }

    public void setSkipProgramSetTitle(boolean skipProgramSetTitle)
    {
        _skipProgramSetTitle = skipProgramSetTitle;
    }

    public boolean isOriginalDocActive()
    {
        return _originalDocActive;
    }

    public void setOriginalDocActive(boolean originalDocActive)
    {
        _originalDocActive = originalDocActive;
    }

    public DataWrapper getOriginalDoc()
    {
        return _originalDoc;
    }

    public void setOriginalDoc(DataWrapper originalDoc)
    {
        _originalDoc = originalDoc;
    }
}
