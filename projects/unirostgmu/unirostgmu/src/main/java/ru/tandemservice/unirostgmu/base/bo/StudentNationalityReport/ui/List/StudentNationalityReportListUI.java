/*$Id$*/
package ru.tandemservice.unirostgmu.base.bo.StudentNationalityReport.ui.List;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import jxl.write.WriteException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author DMITRY KNYAZEV
 * @since 09.11.2015
 */
@State(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class StudentNationalityReportListUI extends UIPresenter
{
    private Long orgUnitId;
    private List<OrgUnit> formativeOrgUnits = new ArrayList<>();
    private List<Course> course = new ArrayList<>();
    private List<Nationality> nationality = new ArrayList<>();
    private List<StudentStatus> studentStatus = new ArrayList<>();
    private IReportParam<List<AddressCountry>> regCountry = new ReportParam<>();
    private IReportParam<List<AddressItem>> regSettlement = new ReportParam<>();
    private IReportParam<String> regStreet = new ReportParam<>();

    private CommonPostfixPermissionModel secModel;

    @Override
    public void onComponentRefresh()
    {
        if (getOrgUnit() != null)
        {
            formativeOrgUnits.add(getOrgUnit());
            secModel = new OrgUnitSecModel(getOrgUnit());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (StudentNationalityReportList.REG_SETTLEMENT_DS.equals(dataSource.getName()))
        {
            getRegCountry().putParamIfActive(dataSource, AddressItem.PARAM_COUNTRY);
        }
    }

    //sort list
    private List<Student> getStudentList()
    {
        final String alias = "s";
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, alias);

        if (!isNullOrEmpty(getFormativeOrgUnits()))
            builder.where(in(property(alias, Student.educationOrgUnit().formativeOrgUnit()), getFormativeOrgUnits()));

        if (!isNullOrEmpty(getCourse()))
            builder.where(in(property(alias, Student.course()), getCourse()));

        if (!isNullOrEmpty(getNationality()))
            builder.where(in(property(alias, Student.person().identityCard().nationality()), getNationality()));

        if (!isNullOrEmpty(getStudentStatus()))
            builder.where(in(property(alias, Student.status()), getStudentStatus()));

        //"Адрес регистрации"
        {
            final String personAlias = "p";
            if ((regCountry.isActive() && !regCountry.getData().isEmpty()) || regSettlement.isActive() || regStreet.isActive())
            {
                builder.joinPath(DQLJoinType.inner, Student.person().fromAlias(alias), personAlias);
                builder.joinPath(DQLJoinType.left, Person.identityCard().fromAlias(personAlias), "regAddressIc");
                builder.joinEntity("regAddressIc", DQLJoinType.left, AddressRu.class, "regAddressRu", DQLExpressions.eq(DQLExpressions.property("regAddressRu", AddressRu.id()), DQLExpressions.property("regAddressIc", IdentityCard.address().id())));
                builder.joinEntity("regAddressIc", DQLJoinType.left, AddressInter.class, "regAddressInter", DQLExpressions.eq(DQLExpressions.property("regAddressInter", AddressInter.id()), DQLExpressions.property("regAddressIc", IdentityCard.address().id())));
                builder.joinEntity("regAddressIc", DQLJoinType.left, AddressString.class, "regAddressString", DQLExpressions.eq(DQLExpressions.property("regAddressString", AddressString.id()), DQLExpressions.property("regAddressIc", IdentityCard.address().id())));
                builder.joinPath(DQLJoinType.left, AddressRu.street().fromAlias("regAddressRu"), "regAddressRuStreet");
            }
            //"Страна"
            if (regCountry.isActive())
            {
                if (regCountry.getData().isEmpty())
                    builder.where(DQLExpressions.isNull(DQLExpressions.property(Person.identityCard().address().fromAlias(personAlias))));
                else
                {
                    List<String> countryTitles = CommonBaseEntityUtil.getPropertiesList(regCountry.getData(), AddressCountry.title());
                    List<IDQLExpression> expresions = Lists.newArrayList();
                    for (String countryTitle : countryTitles)
                    {
                        expresions.add(DQLExpressions.likeUpper(DQLExpressions.property("regAddressString", AddressString.address()), DQLExpressions.value(CoreStringUtils.escapeLike(countryTitle))));
                    }
                    expresions.add(DQLExpressions.in(DQLExpressions.property("regAddressRu", AddressRu.country()), regCountry.getData()));
                    expresions.add(DQLExpressions.in(DQLExpressions.property("regAddressInter", AddressInter.country()), regCountry.getData()));

                    builder.where(DQLExpressions.or(expresions.toArray(new IDQLExpression[expresions.size()])));
                }
            }
            //"Населенный пункт"
            if (regSettlement.isActive())
            {
                List<String> settlementTitles = CommonBaseEntityUtil.getPropertiesList(regSettlement.getData(), AddressItem.title());
                List<IDQLExpression> expresions = Lists.newArrayList();
                for (String settlementTitle : settlementTitles)
                {
                    expresions.add(DQLExpressions.likeUpper(DQLExpressions.property("regAddressString", AddressString.address()), DQLExpressions.value(CoreStringUtils.escapeLike(settlementTitle))));
                }
                expresions.add(DQLExpressions.in(DQLExpressions.property("regAddressRu", AddressRu.settlement()), regSettlement.getData()));
                expresions.add(DQLExpressions.in(DQLExpressions.property("regAddressInter", AddressInter.settlement()), regSettlement.getData()));
                builder.where(DQLExpressions.or(expresions.toArray(new IDQLExpression[expresions.size()])));
            }
            //"Улица"
            if (regStreet.isActive())
            {
                List<IDQLExpression> expresions = Lists.newArrayList();
                expresions.add(DQLExpressions.likeUpper(DQLExpressions.property("regAddressRuStreet", AddressStreet.title()), DQLExpressions.value(CoreStringUtils.escapeLike(regStreet.getData(), true))));
                expresions.add(DQLExpressions.likeUpper(DQLExpressions.property("regAddressInter", AddressInter.addressLocation()), DQLExpressions.value(CoreStringUtils.escapeLike(regStreet.getData(), true))));
                expresions.add(DQLExpressions.likeUpper(DQLExpressions.property("regAddressString", AddressString.address()), DQLExpressions.value(CoreStringUtils.escapeLike(regStreet.getData(), true))));
                builder.where(DQLExpressions.or(expresions.toArray(new IDQLExpression[expresions.size()])));
            }
        }

        return DataAccessServices.dao().<Student>getList(builder)
                .stream()
                .sorted((s1, s2) ->
                        ComparisonChain.start()
                                .compare(s1.getEducationOrgUnit().getFormativeOrgUnit().getTitle(), s2.getEducationOrgUnit().getFormativeOrgUnit().getTitle())
                                .compare(s1.getCourse().getIntValue(), s2.getCourse().getIntValue())
                                .compare(s1.getFullFio(), s2.getFullFio())
                                .result())
                .collect(Collectors.toList());
    }

    private boolean isNullOrEmpty(@Nullable Collection collection)
    {
        return collection == null || collection.isEmpty();
    }

    //Handlers
    public void onClickApply()
    {
        try
        {
            StudentNationalityReportBuilder reportBuilder = new StudentNationalityReportBuilder(getStudentList(), getFormativeOrgUnits());
            BusinessComponentUtils.downloadDocument(reportBuilder.buildDocumentRenderer("StudentNationalityReport"), true);
        } catch (IOException | WriteException ex)
        {
            throw CoreExceptionUtils.getRuntimeException(ex);
        }
    }

    //Getters/Setters
    public Collection<StudentStatus> getStudentStatus()
    {
        return studentStatus;
    }

    public void setStudentStatus(List<StudentStatus> studentStatus)
    {
        this.studentStatus = studentStatus;
    }

    public Collection<Nationality> getNationality()
    {
        return nationality;
    }

    public void setNationality(List<Nationality> nationality)
    {
        this.nationality = nationality;
    }

    public List<OrgUnit> getFormativeOrgUnits()
    {
        return formativeOrgUnits;
    }

    public void setFormativeOrgUnits(List<OrgUnit> formativeOrgUnits)
    {
        this.formativeOrgUnits = formativeOrgUnits;
    }

    public List<Course> getCourse()
    {
        return course;
    }

    public void setCourse(List<Course> course)
    {
        this.course = course;
    }

    public IReportParam<String> getRegStreet()
    {
        return regStreet;
    }

    public void setRegStreet(IReportParam<String> regStreet)
    {
        this.regStreet = regStreet;
    }

    public IReportParam<List<AddressItem>> getRegSettlement()
    {
        return regSettlement;
    }

    public void setRegSettlement(IReportParam<List<AddressItem>> regSettlement)
    {
        this.regSettlement = regSettlement;
    }

    public IReportParam<List<AddressCountry>> getRegCountry()
    {
        return regCountry;
    }

    public void setRegCountry(IReportParam<List<AddressCountry>> regCountry)
    {
        this.regCountry = regCountry;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    private OrgUnit getOrgUnit()
    {
        if (getOrgUnitId() != null)
            return DataAccessServices.dao().get(getOrgUnitId());
        return null;
    }

    public String getPermissionKey()
    {
        return null == getOrgUnit() ? "studentNationalityReport" : getSecModel().getPermission("orgUnit_viewStudentNationalityReport");
    }

    public ISecured getSecuredObject()
    {
        return null == getOrgUnit() ? super.getSecuredObject() : getOrgUnit();
    }

    public boolean isDisabled()
    {
        return getOrgUnitId() != null;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }
}
