/*$Id$*/
package ru.tandemservice.unirostgmu.base.bo.StudentNationalityReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 09.11.2015
 */
@Configuration
public class StudentNationalityReportList extends BusinessComponentManager
{
    public static final String FORMATIVE_ORG_UNITS_DS = "formativeOrgUnitsDS";
    public static final String COURSE_DS = "courseDS";
    public static final String NATIONALITY_DS = "nationalityDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";
    public static final String REG_COUNTRY_DS = "regCountryDS";
    public static final String REG_SETTLEMENT_DS = "regSettlementDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_ORG_UNITS_DS, formativeOrgUnitsDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(NATIONALITY_DS, nationalityDSHandler()))
                .addDataSource(selectDS(STUDENT_STATUS_DS, studentStatusDSHandler()))
                .addDataSource(selectDS(REG_COUNTRY_DS, countryDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(REG_SETTLEMENT_DS, getName(), AddressItem.settlementComboDSHandler(getName())))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler countryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), AddressCountry.class)
                .order(AddressCountry.title())
                .filter(AddressCountry.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentStatusDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StudentStatus.class)
                .order(StudentStatus.title())
                .filter(StudentStatus.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler nationalityDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Nationality.class)
                .order(Nationality.title())
                .filter(Nationality.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class)
                .order(Course.intValue())
                .filter(Course.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .customize((alias, dql, context, filter) -> {
                    dql.joinEntity(alias,
                            DQLJoinType.inner,
                            OrgUnitToKindRelation.class,
                            "ou2kr",
                            eq(property(alias, OrgUnit.id()), property("ou2kr", OrgUnitToKindRelation.orgUnit().id())));
                    dql.joinEntity("ou2kr",
                            DQLJoinType.inner,
                            OrgUnitKind.class,
                            "ouk",
                            eq(property("ouk", OrgUnitKind.code()), value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)));
                    dql.distinct();
                    return dql;
                })
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }
}
