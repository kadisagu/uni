/* $Id$ */
package ru.tandemservice.unirostgmu.base.bo.RostgmuPerson.ui.AccessRestrictionList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Principal;
import ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
@Configuration
public class RostgmuPersonAccessRestrictionList extends BusinessComponentManager
{
    public static final String IP_ADDRESSES_DS = "ipAddressesDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(this.searchListDS(IP_ADDRESSES_DS, ipAddressesDSColumns(), ipAddressesDSColumnsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint ipAddressesDSColumns()
    {
        return columnListExtPointBuilder(IP_ADDRESSES_DS)
                .addColumn(publisherColumn("ipAddress", RostgmuPrincipalToIPRelation.ipAddress()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditIPAddress").permissionKey("ui:secModel.editRestrictionAccess"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
                                   .alert(alert("ipAddressesDS.delete.alert", "ipAddress"))
                                   .permissionKey("ui:secModel.deleteRestrictionAccess"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler ipAddressesDSColumnsDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), RostgmuPrincipalToIPRelation.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final Principal principal = context.get("principal");
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RostgmuPrincipalToIPRelation.class, "rel").column(property("rel"));
                builder.where(eq(property("rel", RostgmuPrincipalToIPRelation.principal()), value(principal)));

                new DQLOrderDescriptionRegistry(RostgmuPrincipalToIPRelation.class, "rel").applyOrder(builder, input.getEntityOrder());
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}