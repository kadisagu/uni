/*$Id$*/
package ru.tandemservice.unirostgmu.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;

/**
 * @author DMITRY KNYAZEV
 * @since 07.12.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private OrgUnitReportManager orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(orgUnitReportManager.reportListExtPoint());

        return itemList
                .add("studentNationalityReportList",
                        new OrgUnitReportDefinition(
                                "Сведения о студентах с выводом национальности",
                                "studentNationalityReportList",
                                ru.tandemservice.uni.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNI_ORG_UNIT_STUDENT_REPORT_BLOCK,
                                "StudentNationalityReportList",
                                "orgUnit_viewStudentNationalityReport")
                )
                .create();
    }
}
