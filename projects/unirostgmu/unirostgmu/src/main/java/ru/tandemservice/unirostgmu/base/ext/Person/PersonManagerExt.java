/*$Id$*/
package ru.tandemservice.unirostgmu.base.ext.Person;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import ru.tandemservice.unirostgmu.base.ext.Person.logic.RostgmuPersonDao;

/**
 * @author DMITRY KNYAZEV
 * @since 27.10.2015
 */
@Configuration
public class PersonManagerExt extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IPersonDao dao()
    {
        return new RostgmuPersonDao();
    }
}
