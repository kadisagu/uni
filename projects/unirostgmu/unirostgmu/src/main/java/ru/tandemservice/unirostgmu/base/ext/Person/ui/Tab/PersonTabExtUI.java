/* $Id$ */
package ru.tandemservice.unirostgmu.base.ext.Person.ui.Tab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
public class PersonTabExtUI extends UIAddon
{
    public PersonTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}