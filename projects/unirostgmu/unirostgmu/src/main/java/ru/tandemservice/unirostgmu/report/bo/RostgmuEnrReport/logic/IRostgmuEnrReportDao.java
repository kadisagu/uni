/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EnrollmentMeetingAdd.RostgmuEnrReportEnrollmentMeetingAddUI;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EntrantListAdd.RostgmuEnrReportEntrantListAddUI;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EstimatedPassingScoreAdd.RostgmuEnrReportEstimatedPassingScoreAddUI;
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.RequestCompetitionSummaryAdd.RostgmuEnrReportRequestCompetitionSummaryAddUI;

/**
 * @author Andrey Avetisov
 * @since 13.02.2015
 */
public interface IRostgmuEnrReportDao extends INeedPersistenceSupport
{
    /**
     * Создает отчет «Сводный рейтинг поступающих (РостГМУ)»
     * @param model модель данных для формирования отчета
     * @return идентификатор созданного отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEstimatedPassingScoreReport(RostgmuEnrReportEstimatedPassingScoreAddUI model);

    /**
     * Создает отчет «Сводная информация о приеме заявлений и конкурсе (РостГМУ)»
     * @param model модель данных для формирования отчета
     * @return идентификатор созданного отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createRequestCompetitionSummaryReport(RostgmuEnrReportRequestCompetitionSummaryAddUI model);

    /**
     * Создает отчет «Список для заседания комиссии по зачислению (РостГМУ)»
     * @param model модель данных для формирования отчета
     * @return идентификатор созданного отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEnrollmentMeetingListReport(RostgmuEnrReportEnrollmentMeetingAddUI model);

    /**
     * Создает отчет «Список поступающих (РостГМУ)»
     * @param model модель данных для формирования отчета
     * @return идентификатор созданного отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEntrantListReport(RostgmuEnrReportEntrantListAddUI model);
}
