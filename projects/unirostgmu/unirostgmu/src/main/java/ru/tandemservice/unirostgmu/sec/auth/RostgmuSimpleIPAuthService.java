/* $Id$ */
package ru.tandemservice.unirostgmu.sec.auth;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.auth.SimpleAuthService;
import ru.tandemservice.unirostgmu.entity.RostgmuPrincipalToIPRelation;
import ru.tandemservice.unirostgmu.util.IPAddressUtils;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
public class RostgmuSimpleIPAuthService extends SimpleAuthService
{
    @Override
    public boolean authenticate(IPrincipal principal, String password)
    {
        boolean correctPassword = super.authenticate(principal, password);
        boolean correctIp = false;
        if (correctPassword)
        {
            DQLSelectBuilder ipListDql = new DQLSelectBuilder().fromEntity(RostgmuPrincipalToIPRelation.class, "p")
                    .column(property("p", RostgmuPrincipalToIPRelation.P_IP_ADDRESS), "ip")
                    .where(eqValue(property("p", RostgmuPrincipalToIPRelation.L_PRINCIPAL), principal));

            String currentIP = UserContext.getInstance().getUserIpAddress();
            if (IPAddressUtils.isIpV6Address(currentIP))
                currentIP = IPAddressUtils.ipAddressV6ToV4(currentIP);
            List<String> ipList = DataAccessServices.dao().getList(ipListDql);
            correctIp = ipList.isEmpty() || IPAddressUtils.isIpInList(currentIP, ipList);

        }
        if (correctPassword && !correctIp)
        {
            throw new ApplicationException("Невозможно выполнить вход с текущего IP-адреса.");
        }
        return correctPassword;
    }



}