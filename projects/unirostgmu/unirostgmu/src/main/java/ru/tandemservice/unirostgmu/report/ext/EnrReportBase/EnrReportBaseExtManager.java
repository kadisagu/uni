/* $Id$ */
package ru.tandemservice.unirostgmu.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportEnrollmentMeetingList;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportEntrantList;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportEstimatedPassingScoreList;
import ru.tandemservice.unirostgmu.entity.RostgmuEnrReportRequestCompetitionSummary;

/**
 * @author Andrey Avetisov
 * @since 12.02.2015
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
                .add(RostgmuEnrReportEstimatedPassingScoreList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RostgmuEnrReportEstimatedPassingScoreList.REPORT_KEY))
                .add(RostgmuEnrReportRequestCompetitionSummary.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RostgmuEnrReportRequestCompetitionSummary.REPORT_KEY))
                .add(RostgmuEnrReportEnrollmentMeetingList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RostgmuEnrReportEnrollmentMeetingList.REPORT_KEY))
                .add(RostgmuEnrReportEntrantList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RostgmuEnrReportEntrantList.REPORT_KEY))

                .create();
    }

    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_enrReportBaseManager.storableReportDescExtPoint())
                .add(RostgmuEnrReportEstimatedPassingScoreList.REPORT_KEY, RostgmuEnrReportEstimatedPassingScoreList.getDescription())
                .add(RostgmuEnrReportRequestCompetitionSummary.REPORT_KEY, RostgmuEnrReportRequestCompetitionSummary.getDescription())
                .add(RostgmuEnrReportEnrollmentMeetingList.REPORT_KEY, RostgmuEnrReportEnrollmentMeetingList.getDescription())
                .add(RostgmuEnrReportEntrantList.REPORT_KEY, RostgmuEnrReportEntrantList.getDescription())
                .create();
    }
}
