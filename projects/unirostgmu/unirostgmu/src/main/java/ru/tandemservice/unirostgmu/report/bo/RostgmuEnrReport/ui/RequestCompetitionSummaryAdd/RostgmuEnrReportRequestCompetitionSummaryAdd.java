/* $Id$ */
package ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.RequestCompetitionSummaryAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Andrey Avetisov
 * @since 16.02.2015
 */
@Configuration
public class RostgmuEnrReportRequestCompetitionSummaryAdd extends BusinessComponentManager
{
    public static final String ENTRANT_STATE_DS = "entrantStateDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(ENTRANT_STATE_DS, entrantStateDSHandler()))
                .create();
    }


    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantState.class)
                .order(EnrEntrantState.title())
                .filter(EnrEntrantState.title());
    }
}
