/*$Id$*/
package ru.tandemservice.unirostgmu.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 08.12.2015
 */
public class RostgmuPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unirostgmu");
        config.setName("unirostgmu-sec-config");
        config.setTitle("");

        //
        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();
            // Вкладка «Отчеты»
            final PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta studentReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "StudentReportPermissionGroup", "Отчеты модуля «Студенты»");
            PermissionMetaUtil.createPermission(studentReports, "orgUnit_viewStudentNationalityReport_" + code, "Просмотр отчета «Сведения о студентах с выводом национальности»");
        }
        //
        securityConfigMetaMap.put(config.getName(), config);
    }
}
