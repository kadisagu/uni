/* $Id$ */
package ru.tandemservice.unirostgmu.base.bo.RostgmuPerson.ui.AccessRestrictionAddEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Ekaterina Zvereva
 * @since 07.04.2016
 */
@Configuration
public class RostgmuPersonAccessRestrictionAddEdit extends BusinessComponentManager
{
}