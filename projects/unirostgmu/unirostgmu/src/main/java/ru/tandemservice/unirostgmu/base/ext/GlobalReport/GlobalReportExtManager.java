/*$Id$*/
package ru.tandemservice.unirostgmu.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unirostgmu.base.bo.StudentNationalityReport.ui.List.StudentNationalityReportList;

/**
 * @author DMITRY KNYAZEV
 * @since 09.11.2015
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(globalReportManager.reportListExtPoint())
                .add("studentNationalityReport", new GlobalReportDefinition("student", "studentNationality", StudentNationalityReportList.class.getSimpleName()))
                .create();
    }
}
