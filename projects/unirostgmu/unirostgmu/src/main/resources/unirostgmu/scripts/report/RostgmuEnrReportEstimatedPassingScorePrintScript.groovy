package unirostgmu.scripts.report

import org.hibernate.Session
import org.tandemframework.caf.logic.wrapper.DataWrapper
import org.tandemframework.core.entity.EntityComparator
import org.tandemframework.core.entity.EntityOrder
//import org.tandemframework.core.entity.IIdentifiableWrapper
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler
import org.tandemframework.shared.commonbase.base.util.key.PairKey
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EstimatedPassingScoreAdd.RostgmuEnrReportEstimatedPassingScoreAddUI

import java.text.SimpleDateFormat

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 13.02.2015
 */

return new RostgmuEnrReportEstimatedPassingScorePrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class RostgmuEnrReportEstimatedPassingScorePrint
{
    Session session
    byte[] template
    RostgmuEnrReportEstimatedPassingScoreAddUI model

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def document = new RtfReader().read(template)

        SimpleDateFormat df = new SimpleDateFormat("dd MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU);
        im.put("date", df.format(model.dateTo))
        // Выбираем абитуриентов, подходящих под параметры отчета
        Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();



        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, model.competitionFilterAddon, true, model.isOriginalDocActive());

        List<EnrRatingItem> ratingItemList = requestedCompDQL.createStatement(getSession()).<EnrRatingItem> list();
        for (EnrRatingItem ratingItem : ratingItemList)
        {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
        }
        Collection<EnrCompetition> competitions = model.competitionFilterAddon.getFilteredList();

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition())
        {
            competitions = entrantMap.keySet();
        }
        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions.size());
        for (EnrCompetition competition : competitions)
        {
            if (competition.plan > 0)
            {
                competitionList.add(competition)
            }
        }
        Collections.sort(competitionList, new EntityComparator<EnrCompetition>(new EntityOrder(EnrCompetition.type().code().s())));

        //в целевом приеме абитуриенты группируются по выбранному конкурсу ЦП
        Map<EnrCompetition, List<EnrTargetAdmissionPlan>> targetPlan = getTargetPlan(competitionList)
        Map<PairKey<EnrCompetition, EnrTargetAdmissionPlan>, List<EnrRatingItem>> targetEntrantMap = getTargetEntrantMap(targetPlan, ratingItemList);



        List<String[]> dataList = new ArrayList<String[]>();
        int rowNumber = 1

        for (EnrCompetition competition : competitionList)
        {
            def score = "";
            if (!competition.targetAdmission)
            {
                if (competition.noExams)
                {
                    score = "Без ВИ"
                }
                else if (entrantMap.get(competition) == null || competition.plan > entrantMap.get(competition).size() || competition.plan == 0)
                {
                    score = "-"
                }
                else
                {
                    score = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(entrantMap.get(competition).get(competition.plan - 1).totalMarkAsDouble)  //список уже отсортирован по рейтингу
                }
                String[] row = [rowNumber, competition.title, score]
                dataList.add(row)
                rowNumber++
            }
            else
            {
                for (EnrTargetAdmissionPlan plan : targetPlan.get(competition))
                {
                    PairKey<EnrCompetition, EnrTargetAdmissionPlan> key = new PairKey<>(competition, plan);
                    if ((model.isSkipEmptyCompetition() && plan.plan==0) || targetEntrantMap.get(key)==null)
                    {
                        continue
                    }
                    if (plan.plan > targetEntrantMap.get(key).size() || competition.plan == 0)
                    {
                        score = "-"
                    }
                    else
                    {
                        score = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(targetEntrantMap.get(key).get(plan.plan - 1).totalMarkAsDouble)  //список уже отсортирован по рейтингу
                    }

                    String[] row = [rowNumber, competition.title+" "+plan.enrCampaignTAKind.title, score]
                    dataList.add(row)
                    rowNumber++
                }
            }
        }
        tm.put("T", dataList as String[][])
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'RostgmuEnrEstimatedPassingScoreReport.rtf']
    }


    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(RostgmuEnrReportEstimatedPassingScoreAddUI model, EnrCompetitionFilterAddon filterAddon, boolean fetch, boolean originalDocActive)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());

        if (originalDocActive)
        {
            requestedCompDQL.where(eq(property(requestedCompDQL.rating(), EnrRatingItem.requestedCompetition().request().eduInstDocOriginalHandedIn()), value(getBooleanFromWrapper(model.originalDoc))));
        }

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

//        requestedCompDQL.where(eqValue(property(EnrRatingItem.requestedCompetition().state().code().fromAlias(requestedCompDQL.rating())), EnrEntrantStateCodes.EXAMS_PASSED));
        requestedCompDQL.where(notIn(property(EnrRatingItem.requestedCompetition().state().code().fromAlias(requestedCompDQL.rating())), Arrays.asList(EnrEntrantStateCodes.OUT_OF_COMPETITION, EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)))
        requestedCompDQL.column(requestedCompDQL.rating());
        requestedCompDQL.order(property(requestedCompDQL.rating(), EnrRatingItem.totalMarkAsLong()), OrderDirection.desc)

        return requestedCompDQL;
    }

    private static Map<EnrCompetition, List<EnrTargetAdmissionPlan>> getTargetPlan(List<EnrCompetition> competitionList)
    {
        Map<EnrCompetition, List<EnrTargetAdmissionPlan>> taPlan = new HashMap<>()
        List taList = DataAccessServices.dao().getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.L_COMPETITION, competitionList)

        Collections.sort(taList, new Comparator<EnrTargetAdmissionCompetitionPlan>() {
            @Override
            int compare(EnrTargetAdmissionCompetitionPlan o1, EnrTargetAdmissionCompetitionPlan o2)
            {
                return o1.targetAdmissionPlan.enrCampaignTAKind.title.compareTo(o2.targetAdmissionPlan.enrCampaignTAKind.title)
            }
        })
        for (EnrTargetAdmissionCompetitionPlan ta : taList)
        {
            SafeMap.safeGet(taPlan, ta.competition, ArrayList.class).add(ta.targetAdmissionPlan);
        }
        return taPlan
    }

    //абитуриенты группируются по видам ЦП (позвоялет сравнить план ЦП и кол-во реально поступающих абитуриентов)
    private static Map<PairKey<EnrCompetition,EnrTargetAdmissionPlan>, List<EnrRatingItem>> getTargetEntrantMap(Map<EnrCompetition, List<EnrTargetAdmissionPlan>> targetPlan, List<EnrRatingItem> ratingItemList)
    {
        Map<PairKey<EnrCompetition,EnrTargetAdmissionPlan>, List<EnrRatingItem>> targetEntrantMap = new HashMap<>();
        for (EnrRatingItem ratingItem : ratingItemList)
        {
            if (ratingItem.getRequestedCompetition() instanceof EnrRequestedCompetitionTA)
            {
                EnrRequestedCompetitionTA requestedCompetitionTA = (EnrRequestedCompetitionTA) ratingItem.requestedCompetition
                for (EnrCompetition comp : targetPlan.keySet())
                {
                    for (EnrTargetAdmissionPlan plan : targetPlan.get(comp))
                    {
                        if (requestedCompetitionTA.getTargetAdmissionKind().equals(plan.getEnrCampaignTAKind()) && ratingItem.competition.equals(comp))
                        {
                            PairKey<EnrCompetition, EnrTargetAdmissionPlan> key = new PairKey<>(comp, plan);
                            if(!SafeMap.safeGet(targetEntrantMap,key, ArrayList.class).contains(ratingItem))
                                SafeMap.safeGet(targetEntrantMap, key, ArrayList.class).add(ratingItem)
                        }
                    }
                }
            }
        }

        return targetEntrantMap
    }

    //Utils
    private static boolean getBooleanFromWrapper(DataWrapper o)
    {
        return TwinComboDataSourceHandler.getSelectedValueNotNull(o);
    }
}