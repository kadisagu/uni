/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package unirostgmu.scripts.order

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.ListMultimap
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.utils.CommonCollator
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentOrderParagraphPrintFormTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrEnrollmentOrderPrintUtil
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unimove.IAbstractExtract
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao

return new EnrollmentOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 */
class EnrollmentOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def elementFactory = RtfBean.getElementFactory()
    def par = elementFactory.createRtfControl(IRtfData.PAR)
    def pard = elementFactory.createRtfControl(IRtfData.PARD)


    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        def academy = TopOrgUnit.instance

        // заполнение меток
        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.actionDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)

        im.put('reasonText', order.orderBasicText)
        im.put('orderDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))
        im.put('executorFio', order.executor)

        List<IAbstractParagraph> paragraphList = order.paragraphList
        if (paragraphList.size() > 0 && paragraphList[0] instanceof EnrEnrollmentParagraph)
        {
            EnrEnrollmentOrderPrintUtil.initOrgUnit(im,
                    ((EnrEnrollmentParagraph) paragraphList[0]).formativeOrgUnit, 'formativeOrgUnit', '')
        }

        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        def document = new RtfReader().read(template)
        fillParagraphs(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    void fillParagraphs(RtfHeader header)
    {
        List<IRtfElement> paragraphList = []
        for (IAbstractParagraph abstractParagraph : order.paragraphList)
        {
            if (!(abstractParagraph instanceof EnrEnrollmentParagraph))
                throw new ApplicationException("Параграф №${abstractParagraph.number} в приказе «" + abstractParagraph.order.title + "» не может быть напечатан, так как не является параграфом о зачислении.")

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph

            def enrollmentExtractList = IUniBaseDao.instance.get().<EnrEnrollmentExtract> getList(EnrEnrollmentExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph)
            if (enrollmentExtractList.empty)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «" + paragraph.order.title + "» не может быть напечатан.")

            // Получаем рейтинги для абитуриентов
            Map<EnrRequestedCompetition, EnrRatingItem> ratingMap = [:]
            IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), enrollmentExtractList.collect { e -> e.entity }).each {
                ratingMap.put(it.requestedCompetition, it)
            }

            // Сортируем список выписок по рейтингу (по убыванию), в рамках рейтинга по ФИО (с использованием специального коллатора, который правильно сортирует букву Ё)
            enrollmentExtractList.sort({ a, b ->
                ratingMap.get(b.entity)?.totalMarkAsLong <=> ratingMap.get(a.entity)?.totalMarkAsLong ?:
                        ratingMap.get(a.entity)?.position <=> ratingMap.get(b.entity)?.position ?:
                        CommonCollator.RUSSIAN_COLLATOR.compare(a.entity.request.entrant.fullFio, b.entity.request.entrant.fullFio)
            })

            def firstExtract = enrollmentExtractList.get(0)
            def competition = ((EnrRequestedCompetition) firstExtract.entity).competition
            List<EnrProgramSetItem> programs = IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), competition.programSetOrgUnit.programSet)
            // заполняем метки в шаблоне параграфа
            def injectModifier = new RtfInjectModifier()
                    .put('parNumber', String.valueOf(paragraph.number))
                    .put('developForm', paragraph.programForm.title)
                    .put('developPeriod', UniStringUtils.joinUniqueSorted(programs, EnrProgramSetItem.program().duration().title().s(), ", "))
                    .put('educationOrgUnit', paragraph.programSubject.titleWithCode)

            final String label = "educationType";
            String programKindCode = paragraph.programSubject.eduProgramKind.code
            final String[] types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ? UniRtfUtil.SPECIALITY_CASES : (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ? UniRtfUtil.EDU_PROFESSION_CASES : UniRtfUtil.EDU_DIRECTION_CASES)
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                injectModifier.put(label + UniRtfUtil.CASE_POSTFIX.get(i), types[i])
            }

            int counter = 1
            byte[] paragraphTemplate;
            if (!paragraph.competitionType.code.equals(EnrCompetitionTypeCodes.TARGET_ADMISSION))
            {
                paragraphTemplate = EnrEnrollmentOrderPrintUtil.getEnrollmentOrderParagraphTemplate(paragraph.id)

                // получаем rtf-строку студентов из параграфа
                def rtf = new RtfString().par()
                for (def extract : enrollmentExtractList)
                {
                    def requestedEnrollmentDirection = extract.entity
                    def person = requestedEnrollmentDirection.request.entrant.person
                    rtf.append((counter++).toString()).append('.  ').append(person.fullFio)

                    //               def levelTitle = requestedEnrollmentDirection.request.eduDocument.eduLevel?.shortTitle
                    //               if (levelTitle)
                    //                   rtf.append(' (').append(levelTitle).append(')')

                    def sumMark = ratingMap[extract.entity]?.totalMarkAsDouble
                    if (sumMark)
                        rtf.append(' ').append(IRtfData.ENDASH).append(' ').append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark))

                    def exclusiveCompetitions = requestedEnrollmentDirection.find { e -> e instanceof EnrRequestedCompetitionExclusive }
                    if (null != exclusiveCompetitions) {
                        EnrRequestedCompetitionExclusive comp = (EnrRequestedCompetitionExclusive) exclusiveCompetitions
                        rtf.append(' (').append(comp.benefitCategory.shortTitle).append(')')
                    }
                    rtf.par()

                }

                injectModifier.put('STUDENT_LIST', rtf)
            }
            else
            {
                // Получаем шаблон параграфа для ЦП
                paragraphTemplate = IUniBaseDao.instance.get().getCatalogItem(EnrEnrollmentOrderParagraphPrintFormType.class, EnrEnrollmentOrderParagraphPrintFormTypeCodes.TARGET_ADMISSION).content
            }

            def paragraphDocument = new RtfReader().read(paragraphTemplate)

            // подготавливаем клон шаблона параграфа для вставки в печатную форму приказа
            RtfUtil.modifySourceList(header, paragraphDocument.header, paragraphDocument.elementList)
            injectModifier.modify(paragraphDocument)

            // Для целевого приема своя логика: необходимо размножить таблицу с меткой "STUDENT_LIST" на каждый вид ЦП
            if (paragraph.competitionType.code.equals(EnrCompetitionTypeCodes.TARGET_ADMISSION))
            {
                // Группируем студетов по названию вида ЦП
                def ListMultimap<String, EnrEnrollmentExtract> taMap = ArrayListMultimap.create()
                for (def extract : enrollmentExtractList)
                {
                    taMap.put(((EnrRequestedCompetitionTA) extract.entity).targetAdmissionKind.targetAdmissionKind.fullTitle, extract)
                }

                // Находим таблицу с меткой STUDENT_LIST
                def srcTable = UniRtfUtil.findElement(paragraphDocument.elementList, 'STUDENT_LIST')

                // Запоминаем позицию в шаблоне, где была таблица, чтобы туда же начать вставку её клонов
                def insertIndex = paragraphDocument.elementList.indexOf(srcTable)

                // Клонируем таблицу для последующего размножения
                srcTable = srcTable.clone

                // Удаляем исходную таблицу из шаблона
                paragraphDocument.elementList.remove(insertIndex)

                // Вставляем по очереди все виды ЦР + таблицу с абитуриентами для данного вида ЦП. Виды ЦП сортируем по названию
                for (String taTitle : taMap.keySet().sort())
                {
                    // Получаем список выписок для данного вида ЦП
                    List<EnrEnrollmentExtract> extractList = taMap.get(taTitle)

                    // Выводим название вида ЦП
                    List<IRtfElement> subParList = new RtfString().append(IRtfData.QL).boldBegin().append(taTitle).boldEnd().par().toList()
                    subParList.addAll(pard, par)

                    def newTable = srcTable.clone as RtfTable

                    // Подготавливаем данные для заполнения таблицы
                    String[][] table = new String[extractList.size()][3]
                    for (int i = 0; i < extractList.size(); i++)
                    {
                        def extract = extractList.get(i)
                        def rating = ratingMap.get(extract.entity)
                        int col = -1
                        table[i][++col] = String.valueOf(counter++)
                        table[i][++col] = extract.entity.request.entrant.fullFio
                        table[i][++col] = rating != null ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(rating.totalMarkAsDouble) : ""
                    }

                    // Вставляем данные в таблицу (вместо метки)
                    UniRtfUtil.modify(newTable, table)

                    subParList.addAll([newTable, pard, par])
                    paragraphDocument.elementList.addAll(insertIndex, subParList)
                    insertIndex += subParList.size()
                }
            }

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = paragraphDocument.elementList
            paragraphList.add(group)
        }

        im.put('PARAGRAPHS', paragraphList)
    }
}
