package unirostgmu.scripts.common

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.SharedRtfUtil
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfGroup
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.text.StrBuilder
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import ru.tandemservice.unienr14.entrant.entity.*
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.core.view.formatter.DateFormatter.DEFAULT_DATE_FORMATTER
import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintBS(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * скрипт печати заявления абитуриента (Бакалавриат, Специалитет)
 * @author Denis Perminov
 * @since 04.06.2014
 */
class EntrantRequestPrintBS {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()
    boolean isExam = false
    Long indAchievementsMark;

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()} = ${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillCompetitionNoParallel(directions)
        fillCompetitionParallel(directions)
        indAchievementsMark = fillAchievements() // Индивидуальные достижения добавлено
        fillEnrEntrantStateExam(directions)
        fillInternalStateExams(directions)
        fillInternalExams(directions)
        fillCompetitionExclusive(directions)
        fillCompetitionNoExams(directions)
        fillOlymps(directions)
        fillInjectParameters()
        fillNextOfKin()
        fillExam(directions)
        fillViExams(directions)
        fillEGEExams(directions)
        fillOlympiads(directions)


        // стандартные выходные параметры скрипта
        RtfDocument document = new RtfReader().read(template)
        if (null != im)
            im.modify(document)
        if (null != tm)
            tm.modify(document)
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, deleteLabels, false, false)
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }


    /** Индвидиуальные достижения */
    Long fillAchievements()
    {
        final achievementList = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'e')
                .column(property('e'))
                .where(eq(property('e', EnrEntrantAchievement.entrant()), value(this.entrantRequest.entrant)))
                .createStatement(this.session).<EnrEntrantAchievement> list()
        Long mark = 0
        for (EnrEntrantAchievement item : achievementList)
            mark += item.mark

        // Удаляем таблицу с ИД, если ИД нет. Иначе заполняем её индивидуальными достижениями
        if (!achievementList.isEmpty())
        {
            im.put('noAchievements', '')
            tm.put('T10', achievementList.collect { e -> [e.type.achievementKind.shortTitle, e.markAsLong / 1000] } as String[][]) // [сокр.название вида инд. достижения], [количество баллов]
        }
        else
        {
            tm.remove("T10", 1, 0)
            im.put('noAchievements', 'Индивидуальных достижений не имею.')
        }
        return mark
    }


    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        // пока нет признака параллельности считаем, что все не параллельные
        def dirMap = getDirMap(enrRequestedCompetitions)
        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort { e -> e.priority }

        // для каждого выбранного конкурса
        for (def requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority

            def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((null != specs && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
            row[1] = programSubject
            row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
            row[3] = ((EnrCompetitionTypeCodes.CONTRACT.equals(requestedCompetition.competition.type.code)
                    || EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(requestedCompetition.competition.type.code))
                    ? "по договорам"
                    : "бесплатные места (КЦП)")

            if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(requestedCompetition.competition.type.code)) {
                def exclusiveCompetitions = requestedCompetition.find { e -> e instanceof EnrRequestedCompetitionExclusive }
                if (null != exclusiveCompetitions) {
                    EnrRequestedCompetitionExclusive comp = (EnrRequestedCompetitionExclusive) exclusiveCompetitions
                    row[4] = comp.benefitCategory.shortTitle
                }
            } else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(requestedCompetition.competition.type.code)) {
                def targetAdmission = requestedCompetition.find { e -> e instanceof EnrRequestedCompetitionTA }
                if (null != targetAdmission) {
                    EnrRequestedCompetitionTA comp = (EnrRequestedCompetitionTA) targetAdmission
                    row[4] = comp.targetAdmissionKind.title
                }

            } else {
                row[4] = ""
            }
            rows.add(row)
        }
        tm.put("T1", rows as String[][])
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }

        def programs = DQL.createStatement(session, /
                from ${EnrRequestedProgram.class.simpleName}
                where ${EnrRequestedProgram.requestedCompetition().id()} in (${ids.join(", ")})
                /).<EnrRequestedProgram> list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id;
            if (!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if (!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        return dirMap
    }

    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        tm.remove("T2") // TODO: параллельные конкурсы
    }

    def fillEnrEntrantStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def results = DQL.createStatement(session, /
                from ${EnrEntrantMarkSourceStateExam.class.simpleName}
                where ${EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                /).<EnrEntrantMarkSourceStateExam> list()

        Map<Long, EnrEntrantMarkSourceStateExam> disMap = Maps.newHashMap()
        for (EnrEntrantMarkSourceStateExam result : results) {
            Long id = result.stateExamResult.subject.id
            if (!disMap.containsKey(id)) {
                disMap.put(id, result)
            }
        }
        if (null != disMap && !disMap.isEmpty()) {
            def rows = Lists.newArrayList()
            // для каждого результата ЕГЭ
            for (Map.Entry<Long, EnrEntrantMarkSourceStateExam> e : disMap.entrySet()) {
                // формируем строку из трех столбцов
                String[] row = new String[3]
                EnrEntrantMarkSourceStateExam result = e.getValue()
                row[0] = result.chosenEntranceExamForm.chosenEntranceExam.discipline.title
                row[1] = result.markAsString
                row[2] = result.stateExamResult.year as String
                rows.add(row)
            }
            tm.put("T3", rows as String[][])

        } else {
            tm.remove("T3", 2, 0)
        }
    }

    def fillOlympiads(List<EnrRequestedCompetition> requestedCompetitions)
    {
        List<Long> ids = requestedCompetitions.collect { e -> e.id }
        // Если для выбранных конкурсов заявления есть ВВИ, у которых источник балла — основание балла (особое право), то выводить таблицу олимпиад
        final chosenExamList = new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, 'cee')
                .column(property('cee'))
                .where(DQLExpressions.in(property('cee', EnrChosenEntranceExam.requestedCompetition().id()), ids))
                .where(exists(EnrEntrantMarkSourceBenefit.class, 'id', property('cee', EnrChosenEntranceExam.maxMarkForm().markSource().id())))
                .createStatement(getSession()).<EnrChosenEntranceExam>list()

        if (!chosenExamList.isEmpty())
        {
            chosenExamList = chosenExamList.collect{ e -> e.discipline.title}.toSet().sort()
            tm.put("T11", chosenExamList.collect{ e -> [e, '100 баллов']} as String[][])
        }
        else tm.remove('T11', 1, 0)
    }


    def fillInternalStateExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().code()}='${EnrExamPassFormCodes.STATE_EXAM}'
                /).<EnrChosenEntranceExamForm> list()

        def subjectIds = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.stateExamSubject.id }.findAll().toSet().toList()
        def stateExamResultsPending = subjectIds.isEmpty() ? Collections.emptyList() : DQL.createStatement(session, /
                from ${EnrEntrantStateExamResult.class.simpleName}
                where ${EnrEntrantStateExamResult.subject().id()} in (${subjectIds.join(", ")})
                and ${EnrEntrantStateExamResult.entrant().id()}=${entrantRequest.entrant.id}
                and ${EnrEntrantStateExamResult.secondWave()}=${true}
                /).<EnrEntrantStateExamResult> list()

        if (!stateExamResultsPending.isEmpty()) {
            def placeEGE = new StringBuilder()
            List<String> titles = stateExamResultsPending.collect { e -> e.subject.title }.toSet().toList().sort()
            im.put("internalStateExams", "Заявляю сдачу ЕГЭ в дополнительные сроки по следующим предметам:   " + StringUtils.join(titles, ", "))

            boolean fl = true
            List<String> title = stateExamResultsPending.collect { e -> e.secondWaveExamPlace.title }.toSet().toList().sort()
            // for (EnrEntrantStateExamResult place : stateExamResultsPending) {
            //     placeEGE.append((!fl ? ", " : "")).append("(" + place.subject.title + ") " + place.secondWaveExamPlace.title)
            //     fl = false
            // }
            im.put("PlaceEGE", StringUtils.join(title, ", "))
        } else {
            deleteLabels.add("internalStateExams")
            deleteLabels.add("PlaceEGE")
        }
    }


    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.isEmpty()) {
            Set<String> titles = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.title }
            im.put("internalExams", "Заявляю сдачу вступительных испытаний, проводимых РостГМУ самостоятельно:   " + StringUtils.join(titles, ", "))
            if (null != entrantRequest.internalExamReason){
                im.put("text1", entrantRequest.internalExamReason.title)
            } else {
                im.put("text1", "")
            }
            im.put("text2", " (основание для участия в конкурсе по результатам вступительных испытаний, проводимых РостГМУ самостоятельно)" )
            this.isExam = true
            //   def rows = Lists.newArrayList()
            //   for (def exam : enrChosenEntranceExamForms)
            //  {
            // сформировать строку
            //    String[] row = new String[2];
            //   row[0] = exam.chosenEntranceExam.discipline.title;
            //   row[0] = StringUtils.join(titles, ", ")
            //   row[1] = 0;
            //   rows.add(row);
            //}
            //
            //   tm.put('T8', rows as String[][]);

        } else {
            deleteLabels.add("internalExams")
            deleteLabels.add("text1")
            deleteLabels.add("text2")
            //  tm.remove("T8", 1, 0)
        }
    }

    def fillCompetitionExclusive(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def exclusiveCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }
        if (!exclusiveCompetitions.isEmpty()) {
            EnrRequestedCompetitionExclusive comp = (EnrRequestedCompetitionExclusive) exclusiveCompetitions.get(0)
            def proof = getBenefitProofDocument(comp.benefitCategory, comp.entrant)
            if (null != proof) {
                def doc = proof.getDocument()
                im.put("benefitCategory", comp.benefitCategory.shortTitle)

                def benefitStatement = "";

                if(doc instanceof EnrEntrantBaseDocument)
                {
                    if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code))
                    {
                        benefitStatement = olympDocTitle((EnrOlympiadDiploma)doc.docRelation.document)
                    }
                    else
                    {
                        benefitStatement = docTitle(doc)
                    }
                }
                else
                {
                    benefitStatement = doc.getTitle()
                }

                im.put("benefitStatement", benefitStatement)
                im.put("usingBenefit", "Использование особого права, указанного в данном заявлении, только при подаче заявления в " + TopOrgUnit.instance.shortTitle + ", подтверждаю");
            } else {
                deleteLabels.add("benefitCategory")
                deleteLabels.add("benefitStatement")
                deleteLabels.add("usingBenefit")
            }
        } else {
            deleteLabels.add("benefitCategory")
            deleteLabels.add("benefitStatement")
            deleteLabels.add("usingBenefit")
        }
    }

    EnrEntrantBenefitProof getBenefitProofDocument(EnrBenefitCategory benefitCategory, EnrEntrant entrant) {
        def proofs = DQL.createStatement(session, /
                from ${EnrEntrantBenefitProof.class.simpleName}
                where ${EnrEntrantBenefitProof.benefitStatement().benefitCategory().id()}=${benefitCategory.id}
                and ${EnrEntrantBenefitProof.document().entrant().id()}=${entrant.id}
                /).<EnrEntrantBenefitProof> list()
        proofs.isEmpty() ? null : proofs.get(0) as EnrEntrantBenefitProof
    }

    def fillCompetitionNoExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def noExamCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }
        if (!noExamCompetitions.isEmpty()) {
            noExamCompetitions = noExamCompetitions.sort { e -> e.priority }

            def dirMap = getDirMap(noExamCompetitions)

            def item = DataAccessServices.dao().get(ScriptItem.class, ScriptItem.code(), EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_BENEFITS);
            def temp = item.getCurrentTemplate()
            def document = new RtfReader().read(temp)
            List<IRtfElement> parList = new ArrayList<>();

            for (EnrRequestedCompetition requestedCompetition : noExamCompetitions) {
                def noExamCompetition = (EnrRequestedCompetitionNoExams) requestedCompetition;
                def tempDoc = document.clone;
                def modifier = new RtfInjectModifier();
                def subject = noExamCompetition.competition.programSetOrgUnit.programSet.programSubject

                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)

                def benefitStatement = getBenefitProofDocument(noExamCompetition.benefitCategory, noExamCompetition.entrant)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

                modifier.put("programSubject", programSubject)
                modifier.put("programForm", noExamCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase())
                modifier.put("places", getPlaces(noExamCompetition.competition.type))

                if (benefitStatement != null) {

                    def doc = benefitStatement.document;

                    def benefitStatementStr = "";

                    if(doc instanceof EnrEntrantBaseDocument)
                    {
                        if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code))
                        {
                            benefitStatementStr = olympDocTitle((EnrOlympiadDiploma)doc.docRelation.document)
                        }
                        else
                        {
                            benefitStatementStr = docTitle(doc)
                        }
                    }
                    else
                    {
                        benefitStatementStr = doc.getTitle()
                    }

                    modifier.put("benefitStatement", benefitStatementStr)
                } else {
                    modifier.put("benefitStatement", "")
                }

                modifier.modify(tempDoc)

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(tempDoc.getElementList());
                parList.add(rtfGroup);
            }
            im.put("CompetitionNoExams", parList)
        } else {
            deleteLabels.add("CompetitionNoExams")
        }
    }

    def fillOlymps(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def results = DQL.createStatement(session, /
                from ${EnrEntrantMarkSourceBenefit.class.simpleName}
                where ${EnrEntrantMarkSourceBenefit.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                /).<EnrEntrantMarkSourceBenefit> list().mainProof.findAll { e -> PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(e.documentType.code) }.collect { e -> (EnrOlympiadDiploma) ((EnrEntrantBaseDocument)e).docRelation.document  }

        if (!results.isEmpty()) {
            // Всероссийские (тип 1)
            def rosRtfString = new RtfString()
            rosRtfString.append("Всероссийская олимпиада:")
            // проводимые Минобрнауки (тип 2)
            def minRtfString = new RtfString()
            minRtfString.append("Олимпиады школьников, проводимые Минобрнауки России:")

            List<Long> olympiadId = new ArrayList<>()
            boolean flR = false
            boolean flM = false

            for (EnrOlympiadDiploma olympiadDiploma : results) {
                Long idOl = olympiadDiploma.olympiad.id
                if (!olympiadId.contains(idOl)) {
                    olympiadId.add(idOl)
                    if (olympiadDiploma.olympiad.olympiadType.code == '1') {
                        rosRtfString.par().append(olympiadDiploma.honour.title.toLowerCase() +
                                ", " + olympiadDiploma.olympiad.title +
                                ", " + olympiadDiploma.subject.title.toLowerCase())
                        flR = true
                    }
                    if (olympiadDiploma.olympiad.olympiadType.code == '2') {
                        minRtfString.par().append(olympiadDiploma.honour.title.toLowerCase() +
                                ", " + olympiadDiploma.olympiad.title +
                                ", " + olympiadDiploma.subject.title.toLowerCase() +
                                ", диплом " + olympiadDiploma.seriaAndNumber)
                        flM = true
                    }
                }
            }
            if (flR)
                im.put("RosOlimp", rosRtfString)
            else
                deleteLabels.add("RosOlimp")

            if (flM)
                im.put("MinistOlimp", minRtfString)
            else
                deleteLabels.add("MinistOlimp")
        } else {
            deleteLabels.add("RosOlimp")
            deleteLabels.add("MinistOlimp")
        }
    }

    static def olympDocTitle(EnrOlympiadDiploma doc) {
        return doc.documentType.shortTitle + " " + doc.seriaAndNumber + " (" + doc.olympiad.title + ") по предмету " + doc.subject.title.toLowerCase() + ", " +

                doc.honour.title.toLowerCase()
    }

    static def docTitle(EnrEntrantBaseDocument doc) {
        doc.documentType.shortTitle + (null != doc.seria ? " " + doc.seria : "") + (null != doc.number ? " " + doc.number : "") + ", выдан: " +
                (StringUtils.isEmpty(doc.issuancePlace) ? "                                                            ": doc.issuancePlace) +
                (null != doc.issuanceDate ? " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.issuanceDate) : "") +
                (null != doc.expirationDate ? ", действителен до: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.expirationDate) : "")
    }

    static def getPlaces(EnrCompetitionType type) {
        if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) || EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "по договорам об оказании платных образовательных услуг"
        else if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) || EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code))
            return "финансируемые из федерального бюджета"
        else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    void fillInjectParameters() {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def sex = card.sex
        def personAddress = person.address
        def registrAddress = card.address
        def avMark = entrantRequest.eduDocument.avgMarkAsLong

        im.put("regNumber", entrantRequest.stringNumber)
        im.put("highSchoolTitleShort", TopOrgUnit.instance.shortTitle)
        //FOA
        if (avMark != null) {
            im.put("AvMark", avMark / 1000 as String)

        } else {
            im.put("AvMark", "")
        }


        def headers = DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${TopOrgUnit.instance.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
                /).<EmployeePost> list()

        IdentityCard headCard = (headers != null && !headers.isEmpty()) ? headers.get(0).person.identityCard : null;
        StringBuilder headIof = new StringBuilder()
        if (headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(" ").append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }
        im.put("rector_G", headIof.toString())
        im.put("NamePK", entrant.enrollmentCampaign.title)
        im.put("FIO", card.fullFio)
        im.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(card.birthDate))
        im.put("birthPlace", card.birthPlace)
        im.put("sex", sex.title)
        im.put("citizenship", card.citizenship.fullTitle)
        im.put("identityCardTitle", card.title)
        im.put("identityCardPlaceAndDate", [card.issuancePlace, DateFormatter.DEFAULT_DATE_FORMATTER.format(card.issuanceDate)].grep().join(", "))
        im.put("adressTitleWithFlat", personAddress != null ? personAddress.titleWithFlat : "")
        im.put("adressRegWithFlat", registrAddress != null ? registrAddress.titleWithFlat : "")

        if (person.contactData.phoneFact != null)
            im.put("adressPhonesTitle", [person.contactData.phoneFact, person.contactData.phoneRegTemp, person.contactData.phoneReg].grep().join("  , "))
        else {
            im.put("adressPhonesTitle", "            ")
        }

        //FOA 
        if (person.contactData.phoneMobile != null)
            im.put("adressPhonesTitleMob", person.contactData.phoneMobile)
        else {
            im.put("adressPhonesTitleMob", "            ")
        }
        //FOA
        im.put("email", person.contactData.email)
        im.put("age", card.age as String)

        im.put("education", entrantRequest.eduDocument.eduLevel?.title)
        im.put("year", entrantRequest.eduDocument.yearEnd as String)
        im.put("certificate", entrantRequest.eduDocument.documentKindTitle)
        im.put("SerCert", entrantRequest.eduDocument.seria)
        im.put("NumCert", entrantRequest.eduDocument.number)
        im.put("OutputCert", entrantRequest.eduDocument.eduOrganisationWithAddress)
        im.put("DateCert", DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.eduDocument.issuanceDate))

        def fLangs = DQL.createStatement(session, /
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.main()}
                /).<PersonForeignLanguage> list()

        StringBuilder fL = new StringBuilder()
        if (!fLangs.isEmpty()) {
            Set<String> titles = fLangs.collect { e -> e.language.title }
            fL.append(StringUtils.join(titles, ", "))
        }
        im.put("foreignLanguages", fL.toString())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())
        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("additionalInfo", entrant.additionalInfo)
        im.put("ExamReq", this.isExam ? "вступительным испытаниям и участию в конкурсе " : "участию в конкурсе")
        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))
    }

    def fillNextOfKin() {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put("father", father)
        else {
            im.put("father", "")
        }

        if (mother != null)
            im.put("mother", mother)
        else {
            im.put("mother", "")
        }

        if (tutor != null)
            im.put("tutor", tutor)
        else {
            deleteLabels.add("tutor")
        }
    }

    //Титульный лист
    def fillExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {

        def rows = Lists.newArrayList()
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def examDisciplines = DQL.createStatement(session, /
                from ${EnrChosenEntranceExam.class.simpleName}
                where ${EnrChosenEntranceExam. requestedCompetition().id()} in (${ids.join(", ")})
                order by ${EnrChosenEntranceExam.discipline().discipline().id()}, ${EnrChosenEntranceExam.maxMarkForm().passForm().code()}
                /).<EnrChosenEntranceExam> list()
        def examDisp = ""
        // для каждого выбранного конкурса
        long Mark = 0
        if (!examDisciplines.isEmpty()) {
            for (def exams : examDisciplines) {
                // формируем строку
                String[] row = new String[2]
                if (examDisp == exams.discipline.discipline.title) {
                    examDisp =  exams.discipline.discipline.title
                } else {
                    row[0] = exams.discipline.discipline.title
                    examDisp =  exams.discipline.discipline.title
                    row[1] = exams.maxMarkForm.markSource.markAsLong / 1000
                    Mark = Mark + exams.maxMarkForm.markSource.markAsLong / 1000
                    rows.add(row);
                }
            }
            Mark += indAchievementsMark
            tm.put('T7', rows as String[][]);
            im.put("TotalMark", Mark as String)

        } else {
            tm.remove("T7", 1, 0)
            im.put("TotalMark", indAchievementsMark as String)
        }
    }
    // ЕГЭ в доп сроки
    def fillEGEExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def rows = Lists.newArrayList()
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().code()}='${EnrExamPassFormCodes.STATE_EXAM}'
                /).<EnrChosenEntranceExamForm> list()

        def subjectIds = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.stateExamSubject.id }.findAll().toSet().toList()
        def stateExamResultsPending = subjectIds.isEmpty() ? Collections.emptyList() : DQL.createStatement(session, /
                from ${EnrEntrantStateExamResult.class.simpleName}
                where ${EnrEntrantStateExamResult.subject().id()} in (${subjectIds.join(", ")})
                and ${EnrEntrantStateExamResult.entrant().id()}=${entrantRequest.entrant.id}
                and ${EnrEntrantStateExamResult.secondWave()}=${true}
                /).<EnrEntrantStateExamResult> list()

        if (!stateExamResultsPending.isEmpty()) {
            for (EnrEntrantStateExamResult place : stateExamResultsPending){
                String[] row = new String[2]
                row [0] = place.subject.title
                row[1] = 0
                rows.add(row);
            }
            tm.put('T9', rows as String[][]);

        } else {
            tm.remove("T9", 1, 0)
        }
    }
    // Список ВИ
    def fillViExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def rows = Lists.newArrayList()
        def ViExam = ""
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                order by ${EnrChosenEntranceExamForm.chosenEntranceExam().discipline()}
                 /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.isEmpty()) {
            for (EnrChosenEntranceExamForm ViExams : enrChosenEntranceExamForms){
                String[] row = new String[2]
                if (ViExam == ViExams.chosenEntranceExam.discipline.title) {
                    ViExam =  ViExams.chosenEntranceExam.discipline.title
                } else {
                    row [0] = ViExams.chosenEntranceExam.discipline.title
                    ViExam =  ViExams.chosenEntranceExam.discipline.title
                    row[1] = 0
                    rows.add(row);
                }
            }
            tm.put('T8', rows as String[][]);

        } else {
            tm.remove("T8", 1, 0)
        }

    }


    def getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null;
    }
// добавлено начало
    /**
     * Вывод документа абитуриента в нужном формате.
     *
     * @param doc документ, подтверждающий особое право или индивидуальное достижение
     * @return строка в нужном формате
     */
    static String getEntrantDocumentStr(def doc)
    {
        final str = new StrBuilder()
        if (doc instanceof EnrPersonEduDocumentRel) {
            // документ об образовании — <сокр. наз. вида документа об образовании> (<название степени отличия>) - <серия> <номер> (<сокр. наз. вида/уровня образования>, <год окончания> г., <населенный пункт>)
            doc = doc.eduDocument

            str.append(doc.eduDocumentKind.shortTitle)
            if (doc.graduationHonour) { str.append(' (').append(doc.graduationHonour.title).append(')') }
            str.append(' -')
            if (doc.seria) { str.append(' ').append(doc.seria) }
            if (doc.number) { str.append(' ').append(doc.number) }
            str.append(' (')
            if (doc.eduLevel) { str.append(doc.eduLevel.shortTitle).append(', ') }
            str.append(doc.yearEnd).append(' г., ')
            str.append(doc.eduOrganizationAddressItem.titleWithType).append(')')

        } else if (doc instanceof EnrEntrantBaseDocument) {
            def personDoc = doc.getDocRelation().document

            if (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code)) {
                def diploma = (EnrOlympiadDiploma) personDoc

                // диплом участника олимпиады — <сокр. название типа документа> <серия> <номер> (<название олимпиады>) по предмету <предмет>, <степень отличия>
                str.append(diploma.documentType.shortTitle)
                if (doc.seria) { str.append(' ').append(diploma.seria) }
                if (doc.number) { str.append(' ').append(diploma.number) }
                str.append(' (').append(diploma.olympiad.title)
                str.append(') по предмету ').append(diploma.subject.title)
                str.append(', ').append(StringUtils.uncapitalize(diploma.honour.title))

            } else if (PersonDocumentTypeCodes.DISABLED_CERT.equals(doc.documentType.code)) {

                // справка об инвалидности — <сокр. название типа документа> (<сокр. название категории инвалидности>) <серия> <номер>, выдан: <кем выдан> <когда выдан>, действителен до: <действителен до>
                str.append(personDoc.documentType.shortTitle)
                str.append(' (').append(personDoc.documentCategory.shortTitle).append(')')
                if (personDoc.seria) { str.append(' ').append(personDoc.seria) }
                if (personDoc.number) { str.append(' ').append(personDoc.number) }
                if (personDoc.issuanceDate || personDoc.issuancePlace) {
                    str.append(', выдан:')
                    if (personDoc.issuancePlace) { str.append(' ').append(personDoc.issuancePlace) }
                    if (personDoc.issuanceDate) { str.append(' ').append(DEFAULT_DATE_FORMATTER.format(personDoc.issuanceDate)) }
                }
                if (personDoc.expirationDate) {
                    str.append(', действителен до: ').append(DEFAULT_DATE_FORMATTER.format(personDoc.expirationDate))
                }

            }
            else {
                // другие документы — <сокр. название типа документа> <серия> <номер>, выдан: <кем выдан> <когда выдан>, действителен до: <действителен до>
                str.append(personDoc.documentType.shortTitle)
                if (personDoc.seria) {
                    str.append(' ').append(personDoc.seria)
                }
                if (personDoc.number) {
                    str.append(' ').append(personDoc.number)
                }
                if (personDoc.issuanceDate || personDoc.issuancePlace) {
                    str.append(', выдан:')
                    if (personDoc.issuancePlace) {
                        str.append(' ').append(personDoc.issuancePlace)
                    }
                    if (personDoc.issuanceDate) {
                        str.append(' ').append(DEFAULT_DATE_FORMATTER.format(personDoc.issuanceDate))
                    }
                }
                if (personDoc.expirationDate) {
                    str.append(', действителен до: ').append(DEFAULT_DATE_FORMATTER.format(personDoc.expirationDate))
                }
            }
        } else if (doc instanceof IEnrEntrantBenefitProofDocument || doc instanceof IEnrEntrantRequestAttachable) {
            // Что-то доселе неизвестное
            return doc.displayableTitle
        } else {
            throw new IllegalArgumentException()
        }

        return str.toString()
    }
// добавлено конец

}