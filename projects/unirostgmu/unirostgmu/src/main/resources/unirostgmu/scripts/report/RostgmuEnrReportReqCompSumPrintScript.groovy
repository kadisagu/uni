package unirostgmu.scripts.report

import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.RequestCompetitionSummaryAdd.RostgmuEnrReportRequestCompetitionSummaryAddUI

import java.math.RoundingMode

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

/**
 * @author Andrey Avetisov
 * @since 16.02.2015
 */
return new RostgmuEnrReportReqCompSumPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class RostgmuEnrReportReqCompSumPrint
{
    Session session
    byte[] template
    RostgmuEnrReportRequestCompetitionSummaryAddUI model

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def document = new RtfReader().read(template)
        im.put("year", model.dateTo.format("yyyy"))
        im.put("date", model.dateTo.format("dd.MM.yyyy"))

        final Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();
        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, true);
        Set<EnrEntrant> entrants = new HashSet<>()

        List<EnrRatingItem> ratingItemList = requestedCompDQL.createStatement(getSession()).<EnrRatingItem> list();
        for (EnrRatingItem ratingItem : ratingItemList)
        {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
            entrants.add(ratingItem.entrant)
        }

        List<EnrCompetition> competitions = model.competitionFilterAddon.getFilteredList();
        fillTableRows(tm, entrantMap, competitions)
        im.put("personCount", String.valueOf(entrants.size()))
        im.modify(document)
        tm.modify(document)
        return [document: RtfUtil.toByteArray(document), fileName: 'RostgmuEnrReqCompSummaryPrint.rtf']
    }

    private static void fillTableRows(RtfTableModifier tm, Map<EnrCompetition, List<EnrRatingItem>> entrantMap, List<EnrCompetition> competitions)
    {
        Map<EduProgramSubject, List<EnrCompetition>> subjectCompMap = new HashMap<>();
        for (EnrCompetition comp : competitions)
        {
            SafeMap.safeGet(subjectCompMap, comp.programSetOrgUnit.programSet.programSubject, ArrayList.class).add(comp);
        }


        List<String[]> budgetRows = new ArrayList<>();
        List<String[]> contractAndTotalRows = new ArrayList<>();

        int totalBudgetBenefitPlan = 0;
        int totalBudgetBenefitRequest = 0;
        int totalBudgetTargetPlan = 0;
        int totalBudgetTargetRequest = 0;
        int totalBudgetCommonPlan = 0;
        int totalBudgetCommonRequest = 0;

        int totalContractCommonPlan = 0;
        int totalContractCommonRequest = 0;

        int totalBenefitPlan = 0;
        int totalBenefitRequest = 0;
        int totalTargetPlan = 0;
        int totalTargetRequest = 0;
        int totalCommonPlan = 0;
        int totalCommonRequest = 0;

        for (EduProgramSubject subject : subjectCompMap.keySet())
        {
            int budgetBenefitQuotaPlan = 0;
            int budgetTargetQuotaPlan = 0;
            int budgetCommonQuotaPlan = 0;
            int budgetBenefitQuotaRequest = 0;
            int budgetTargetQuotaRequest = 0;
            int budgetCommonQuotaRequest = 0;

            int contractCommonQuotaPlan = 0;
            int contractCommonQuotaRequest = 0;


            for (EnrCompetition competition : subjectCompMap.get(subject))
            {
                int size = entrantMap.get(competition) != null ? entrantMap.get(competition).size() : 0;
                boolean isBudget = competition.type.compensationType.budget
                if (competition.isExclusive())
                {
                    if (isBudget)
                    {
                        budgetBenefitQuotaPlan += competition.plan
                        totalBudgetBenefitPlan += competition.plan

                        budgetBenefitQuotaRequest += size
                        totalBudgetBenefitRequest += size
                    }
                    totalBenefitPlan += competition.plan
                    totalBenefitRequest += size

                }
                else if (competition.isTargetAdmission())
                {
                    if (isBudget)
                    {
                        budgetTargetQuotaPlan += competition.plan
                        totalBudgetTargetPlan += competition.plan

                        budgetTargetQuotaRequest += size
                        totalBudgetTargetRequest += size
                    }
                    totalTargetPlan += competition.plan
                    totalTargetRequest += size

                }
                else if(!competition.noExams)
                {
                    if (isBudget)
                    {
                        budgetCommonQuotaPlan += competition.plan
                        totalBudgetCommonPlan += competition.plan

                        budgetCommonQuotaRequest += size
                        totalBudgetCommonRequest += size
                    }
                    else
                    {
                        contractCommonQuotaPlan += competition.plan
                        totalContractCommonPlan += competition.plan

                        contractCommonQuotaRequest += size
                        totalContractCommonRequest += size
                    }

                    totalCommonPlan += competition.plan
                    totalCommonRequest += size

                }
            }

            int budgetPlan = budgetBenefitQuotaPlan + budgetTargetQuotaPlan + budgetCommonQuotaPlan
            int budgetRequest = budgetBenefitQuotaRequest + budgetTargetQuotaRequest + budgetCommonQuotaRequest
            //конкурс (человек/место)
            String budgetRatio = budgetPlan != 0 ? new BigDecimal(budgetRequest / budgetPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-";
            String budgetBenefitRatio = budgetBenefitQuotaPlan != 0 ? new BigDecimal(budgetBenefitQuotaRequest / budgetBenefitQuotaPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"
            String budgetTargetRatio = budgetTargetQuotaPlan != 0 ? new BigDecimal(budgetTargetQuotaRequest / budgetTargetQuotaPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"
            String budgetCommonRatio = budgetCommonQuotaPlan != 0 ? new BigDecimal(budgetCommonQuotaRequest / budgetCommonQuotaPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"

            budgetRows.add([subject.title, budgetPlan, budgetBenefitQuotaPlan, budgetTargetQuotaPlan, budgetCommonQuotaPlan,
                            budgetRequest, budgetBenefitQuotaRequest, budgetTargetQuotaRequest, budgetCommonQuotaRequest,
                            budgetRatio, budgetBenefitRatio, budgetTargetRatio, budgetCommonRatio] as String[])

            String contractRatio = contractCommonQuotaPlan != 0 ? new BigDecimal(contractCommonQuotaRequest / contractCommonQuotaPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"
            contractAndTotalRows.add([subject.title, contractCommonQuotaPlan, "-", "-", contractCommonQuotaPlan,
                                      contractCommonQuotaRequest, "-", "-", contractCommonQuotaRequest,
                                      contractRatio, "-", "-", contractRatio] as String[])
        }

        int totalBudgetPlan = totalBudgetBenefitPlan + totalBudgetTargetPlan + totalBudgetCommonPlan
        int totalBudgetRequest = totalBudgetBenefitRequest + totalBudgetTargetRequest + totalBudgetCommonRequest
        //конкурс (человек/место)
        String totalBudgetRatio = totalBudgetPlan != 0 ? new BigDecimal(totalBudgetRequest / totalBudgetPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-";
        String totalBudgetBenefitRatio = totalBudgetBenefitPlan != 0 ? new BigDecimal(totalBudgetBenefitRequest / totalBudgetBenefitPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"
        String totalBudgetTargetRatio = totalBudgetTargetPlan != 0 ? new BigDecimal(totalBudgetTargetRequest / totalBudgetTargetPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"
        String totalBudgetCommonRatio = totalBudgetCommonPlan != 0 ? new BigDecimal(totalBudgetCommonRequest / totalBudgetCommonPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-"

        budgetRows.add(["ИТОГО ПО БЮДЖЕТУ", totalBudgetPlan, totalBudgetBenefitPlan, totalBudgetTargetPlan, totalBudgetCommonPlan,
                        totalBudgetRequest, totalBudgetBenefitRequest, totalBudgetTargetRequest, totalBudgetCommonRequest,
                        totalBudgetRatio, totalBudgetBenefitRatio, totalBudgetTargetRatio, totalBudgetCommonRatio] as String[])

        //конкурс (человек/место)
        String totalContractRatio = totalContractCommonPlan != 0 ? new BigDecimal(totalContractCommonRequest / totalContractCommonPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-";
        contractAndTotalRows.add(["ИТОГО ПО КОММЕРЦИИ", totalContractCommonPlan, "-", "-", totalContractCommonPlan,
                                  totalContractCommonRequest, "-", "-", totalContractCommonRequest, totalContractRatio, "-", "-", totalContractRatio] as String[])

        int totalPlan = totalBenefitPlan + totalTargetPlan + totalCommonPlan
        int totalRequest = totalBenefitRequest + totalTargetRequest + totalCommonRequest
        String totalRatio = totalPlan != 0 ? new BigDecimal(totalRequest / totalPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-";
        String totalCommonRatio = totalCommonPlan != 0 ? new BigDecimal(totalCommonRequest / totalCommonPlan).setScale(2, RoundingMode.HALF_UP).toString() : "-";
        contractAndTotalRows.add(["ВСЕГО (бюджет + комм.)", totalPlan, totalBenefitPlan, totalTargetPlan, totalCommonPlan,
                                  totalRequest, totalBenefitRequest, totalTargetRequest, totalCommonRequest,
                                  totalRatio, totalBudgetBenefitRatio, totalBudgetTargetRatio, totalCommonRatio] as String[])

        tm.put("T1", budgetRows as String[][]);
        tm.put("T1", getLastCellBoldIntercepter());
        tm.put("T2", contractAndTotalRows as String[][]);
        tm.put("T2", getLastCellBoldIntercepter());
    }


    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(RostgmuEnrReportRequestCompetitionSummaryAddUI model, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());

        if (model.isParallelOnly())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.isSkipParallel())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        if (model.isEntrantStateActive())
        {
            requestedCompDQL.where(DQLExpressions.in(property(EnrRequestedCompetition.request().entrant().state().fromAlias(requestedCompDQL.reqComp())), model.entrantStateList));
        }
        else
        {
            if (model.isIncludeTookAwayDocuments())
            {
                requestedCompDQL.where(ne(property(EnrRequestedCompetition.state().code().fromAlias(requestedCompDQL.reqComp())), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));
            }
            else
            {
                requestedCompDQL.defaultActiveFilter();
            }
        }

        model.competitionFilterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL.column(requestedCompDQL.rating());
        requestedCompDQL.order(property(requestedCompDQL.rating(), EnrRatingItem.totalMarkAsLong()), OrderDirection.desc)

        return requestedCompDQL;
    }

    // Делает надпись Итого болдовой
    private static IRtfRowIntercepter getLastCellBoldIntercepter()
    {
        return new IRtfRowIntercepter() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {

            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {

                if (colIndex == 0 && value != null && (value.contains("ИТОГО")))
                {
                    RtfString string = new RtfString();
                    string
                            .fontSize(24)
                            .boldBegin()
                            .append(value)
                            .boldEnd();
                    return string.toList();
                }
                else if (colIndex == 0 && value != null && (value.contains("ВСЕГО (бюджет + комм.)")))
                {
                    cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QC));
                    cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.CLVERTALC));
                    RtfString string = new RtfString();
                    string
                            .fontSize(28)
                            .boldBegin()
                            .append("ВСЕГО ")
                            .append(IRtfData.LINE)
                            .append("(бюджет + комм.)")
                            .boldEnd();
                    return string.toList();
                }

                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {

            }
        }
    }
}