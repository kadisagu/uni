package unirostgmu.scripts.report

import com.google.common.collect.Lists
import org.apache.commons.collections15.CollectionUtils
import org.apache.commons.collections15.Transformer
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.caf.logic.wrapper.DataWrapper
import org.tandemframework.core.entity.EntityComparator
import org.tandemframework.core.entity.EntityOrder
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.core.view.formatter.YesNoFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfBorder
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfText
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.entity.catalog.CompensationType
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.program.entity.EduProgram
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon
import ru.tandemservice.unienr14.competition.entity.*
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EnrollmentMeetingAdd.RostgmuEnrReportEnrollmentMeetingAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 17.02.2015
 */

return new RostgmuEnrReportEnrollmentMeetingPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class RostgmuEnrReportEnrollmentMeetingPrint
{
    Session session
    byte[] template
    RostgmuEnrReportEnrollmentMeetingAddUI model

    def print()
    {
        def document = createReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'RostgmuEnrEnrollmentMeetingPrint.rtf']
    }

    private RtfDocument createReport(RtfDocument document)
    {
        // Выбираем абитуриентов, подходящих под параметры отчета
        final Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();
        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, true);
        requestedCompDQL.order(property(EnrRatingItem.position().fromAlias(requestedCompDQL.rating())));
        requestedCompDQL.column(requestedCompDQL.rating());

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        List<EnrEntrant> entrants = new ArrayList<>();
        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(getSession()).<EnrRatingItem> list())
        {
            SafeMap.safeGet(entrantMap, ratingItem.competition, ArrayList.class).add(ratingItem)
            nonEmptyBlocks.add(ratingItem.competition.programSetOrgUnit)
            entrants.add(ratingItem.entrant)

        }

        //Баллы за индивидуальные достижения
        Map<EnrEntrant, List<EnrEntrantAchievement>> entrantAchievementMap = new HashMap<>()
            List<EnrEntrantAchievement> achievementList = new DQLSelectBuilder()
                    .fromEntity(EnrEntrantAchievement.class, "a")
                    .column(property("a"))
                    .where(DQLExpressions.in(property("a", EnrEntrantAchievement.entrant()), entrants)).createStatement(session).list()
            for (EnrEntrantAchievement entrantAchievement : achievementList)
            {
                SafeMap.safeGet(entrantAchievementMap, entrantAchievement.getEntrant(), ArrayList.class).add(entrantAchievement)
            }

        //Баллы по ВИ абитуриентов
        Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap = SafeMap.get(HashMap.class);

        EnrEntrantDQLSelectBuilder examDQL = prepareEntrantDQL(model, false);
        examDQL.fromEntity(EnrChosenEntranceExam.class, "exam");
        examDQL.where(eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("exam")), property(examDQL.reqComp())));
        examDQL.column("exam");
        for (EnrChosenEntranceExam exam : examDQL.createStatement(getSession()).<EnrChosenEntranceExam> list())
        {
            if (exam.getActualExam() != null && exam.getMarkAsDouble() != null)
            {
                entrantExamMap.get(exam.getRequestedCompetition()).put(exam.getActualExam(), exam);
            }
        }

        Map<EnrRequestedCompetition, EnrOrder> orderMap = new HashMap<>();
        EnrEntrantDQLSelectBuilder orderDQL = prepareEntrantDQL(model, false);
        orderDQL.fromEntity(EnrEnrollmentExtract.class, "e");
        orderDQL.where(eq(property(EnrEnrollmentExtract.entity().fromAlias("e")), property(orderDQL.reqComp())));
        orderDQL.column(orderDQL.reqComp());
        orderDQL.column(property(EnrEnrollmentExtract.paragraph().order().fromAlias("e")));
        for (Object[] row : orderDQL.createStatement(getSession()).<Object[]> list())
        {
            orderMap.put((EnrRequestedCompetition) row[0], (EnrOrder) row[1]);
        }

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов.
        Collection<EnrCompetition> competitions = model.competitionFilterAddon.getFilteredList();
        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition())
        {
            competitions = entrantMap.keySet();
        }

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<EnrCompetition>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList)
        {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // Сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());

        Collections.sort(blockList, new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
            {
                int result;
                result = -Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
                if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
                if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
                if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
                if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
                return result;
            }
        })

        // подгрузим наборы ВИ конкурсов
        Set<Long> examSetVariantIds = new HashSet<>();
        for (EnrCompetition competition : competitions)
        {
            examSetVariantIds.add(competition.getExamSetVariant().getId());
        }

        Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(examSetVariantIds);

        // образовательные программы наборов
        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(blockList, new Transformer<EnrProgramSetOrgUnit, EnrProgramSetBase>() {
            @Override
            EnrProgramSetBase transform(EnrProgramSetOrgUnit enrProgramSetOrgUnit)
            {
                return enrProgramSetOrgUnit.getProgramSet();
            }
        })


        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : DataAccessServices.dao().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets))
        {
            programMap.get(item.getProgramSet()).add(item.getProgram());
        }
        for (EnrProgramSetBase programSetBase : programSets)
        {
            if (programSetBase instanceof EnrProgramSetSecondary)
            {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary) programSetBase).getProgram());
            }
        }

        // определим, какие данные выводить, исходя из параметров отчета
        boolean printMinisterial = true;
        boolean printContract = true;
        CompensationType compensationType = (CompensationType) model.competitionFilterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        if (null != compensationType)
        {
            printMinisterial = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            printContract = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        }

        // заменим метку даты
        new RtfInjectModifier().put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);

        //берем из шаблона таблицы
        Map<String, RtfTable> headerTables = new HashMap<>();
        headerTables.put(EnrRequestTypeCodes.BS, removeTable(document, "BS", true));
        headerTables.put(EnrRequestTypeCodes.MASTER, removeTable(document, "MASTER", true));
        RtfTable higher = removeTable(document, "HIGHER", true)
        headerTables.put(EnrRequestTypeCodes.HIGHER, higher)
        headerTables.put(EnrRequestTypeCodes.POSTGRADUATE, higher)
        headerTables.put(EnrRequestTypeCodes.TRAINEESHIP, higher)
        headerTables.put(EnrRequestTypeCodes.INTERNSHIP, higher)
        headerTables.put(EnrRequestTypeCodes.SPO, removeTable(document, "SPO", true));

        Map<String, RtfTable> contentTables = new HashMap<>();
        contentTables.put("T1", removeTable(document, "T1", false));
        contentTables.put("T2", removeTable(document, "T2", false));
        contentTables.put("T3", removeTable(document, "T3", false));
        contentTables.put("T4", removeTable(document, "T4", false));
        contentTables.put("T5", removeTable(document, "T5", false));

        // и чистим шаблон, чтобы не было ненужных переводов строк
        document.getElementList().clear();
        // выводим блоки отчета
        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList)
        {
            List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);

            RtfTable headerTable = headerTables.get(programSetOrgUnit.getProgramSet().getRequestType().getCode()).getClone();
            document.getElementList().add(headerTable);
            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
            boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);
            boolean skipProgramsRow = (model.isSkipProgramSetTitle() || model.isReplaceProgramSetTitle()) && programs.size() == 1;
            boolean skipProgramSetRow = model.isSkipProgramSetTitle();
            if (skipProgramsRow && headerTable.getRowList().size() >= 6)
            {
                headerTable.getRowList().remove(5);
            }
            if (skipProgramSetRow && headerTable.getRowList().size() >= 4)
            {
                headerTable.getRowList().remove(3);
            }

            List<EnrCompetition> enrCompetitions = blockMap.get(programSetOrgUnit);

            boolean hasRecommendedOther = false;
            fillHeaderData(document, programSetOrgUnit, programs, printMinisterial, printContract, printOP, hasRecommendedOther);
            // теперь списки по каждому конкурсу
            Set<EnrEntrant> ministerialEntrantTotal = new HashSet();
            Set<EnrEntrant> contractEntrantTotal = new HashSet<>();
            for (EnrCompetition competition : enrCompetitions)
            {
                IEnrExamSetDao.IExamSetSettings examSet = examSetContent.get(competition.getExamSetVariant().getId())
                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0)
                {
                    // если прием по ЦП отдельно, делим списки по видам ЦП
                    for (EnrTargetAdmissionPlan taPlan : taPlans)
                    {
                        List<EnrRatingItem> entrantList = new ArrayList<>();
                        for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class))
                        {
                            if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA) entrant.getRequestedCompetition()).getTargetAdmissionKind()))
                            {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty())
                        {
                            continue;
                        }
                        final String tableHeader = getTableHeader(competition, taPlan, entrantList.size());
                        printContentTable(competition, entrantList, document, tableHeader, ministerialEntrantTotal, contractEntrantTotal,
                                contentTables, examSet, entrantExamMap,
                                model.printPriorityColumn, model.entrantAchievementActive, model.achievementTypeList, entrantAchievementMap, taPlan.plan)
                    }
                }
                else
                {
                    List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    final String tableHeader = getTableHeader(competition, null, entrantList.size());
                    printContentTable(competition, entrantList, document, tableHeader, ministerialEntrantTotal, contractEntrantTotal,
                            contentTables, examSet, entrantExamMap,
                            model.printPriorityColumn, model.entrantAchievementActive, model.achievementTypeList, entrantAchievementMap, 0);
                }
            }

            RtfString entrantTotal = new RtfString();
            if (printMinisterial)
            {
                entrantTotal.append("    на бюджет (КЦП) – ").append(String.valueOf(ministerialEntrantTotal.size()));
            }
            if (printContract)
            {
                if (entrantTotal.toList().size() != 0) entrantTotal.par();
                entrantTotal.append("    на места с оплатой стоимости обучения – ").append(String.valueOf(contractEntrantTotal.size()));
            }
            new RtfInjectModifier().put("entrantsTotal", entrantTotal).modify(document);

            if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
        }
        return document
    }

    private static List<EnrTargetAdmissionPlan> getTaPlans(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        return programSetOrgUnit.getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition() ?
                DataAccessServices.dao().getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit,
                        EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s()) : null;
    }

    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(RostgmuEnrReportEnrollmentMeetingAddUI model, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());

        requestedCompDQL.defaultActiveFilter();

        model.competitionFilterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.isSkipParallel())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        if (model.isOriginalDocActive())
        {
            requestedCompDQL.where(eq(property(requestedCompDQL.rating(), EnrRatingItem.requestedCompetition().request().eduInstDocOriginalHandedIn()), value(getBooleanFromWrapper(model.originalDoc))));
        }
        return requestedCompDQL;
    }

    private static RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel)
        {
            for (RtfRow row : table.getRowList())
            {
                for (RtfCell cell : row.getCellList())
                {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                    {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }

        document.getElementList().remove(table);

        return table;
    }

    private static void fillHeaderData(RtfDocument document, EnrProgramSetOrgUnit programSetOrgUnit, List<EduProgramProf> programs,
                                       boolean printMinisterial, boolean printContract, boolean printOP, boolean hasRecommendedOther)
    {
        Collections.sort(programs, new Comparator<EduProgramProf>() {
            @Override
            int compare(EduProgramProf o1, EduProgramProf o2)
            {
                return o1.getTitleAndConditionsShort().compareTo(o2.getTitleAndConditionsShort());
            }
        });

        String duration = null; boolean diffDuration = false;
        for (EduProgram program : programs)
        {
            if (null == duration)
            {
                duration = program.getDuration().getTitle();
            }
            else
            {
                diffDuration = diffDuration || !program.getDuration().getTitle().equals(duration);
            }
        }
        String programConditions = null; boolean diffConditions = false;
        for (EduProgramProf program : programs)
        {
            if (null == programConditions)
            {
                programConditions = StringUtils.trimToEmpty(program.getImplConditionsShort());
            }
            else
            {
                diffConditions = diffConditions || !StringUtils.trimToEmpty(program.getImplConditionsShort()).equals(programConditions);
            }
        }

        OrgUnit filial = programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
                .put("filial", filial.getPrintTitle())
                .put("formOrgUnit", programSetOrgUnit.getFormativeOrgUnit() == null || programSetOrgUnit.getFormativeOrgUnit().equals(filial) ? "" : programSetOrgUnit.getFormativeOrgUnit().getPrintTitle())
                .put("eduSubjectKind", programSetOrgUnit.getProgramSet().getProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_NOMINATIVE))
                .put("eduSubject", programSetOrgUnit.getProgramSet().getProgramSubject().getTitleWithCode())
                .put("programForm", programSetOrgUnit.getProgramSet().getProgramForm().getTitle())
                .put("duration", diffDuration ? "" : duration)
                .put("programTrait", programConditions)
                .put("eduProgramKind", programSetOrgUnit.getProgramSet().getProgramKind().getShortTitle())
                .put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
                .put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getTitle())
        ;

        RtfString eduProgramTitles = new RtfString();
        for (EduProgramProf program : programs)
        {
            if (eduProgramTitles.toList().size() > 0) eduProgramTitles.par();
            eduProgramTitles.append(program.getShortTitle() + " - " + program.getTitleAndConditionsShort());
        }
        modifier.put("eduProgram", eduProgramTitles);

        //numberOfPlaces - число мест для приема (для комбинации набор ОП, филиал); не печатаем те строчки, в которых план 0 (не задан);
        // если план с квотой ЦП бьется на подквоты (более чем на одну), то выводить детализацию по видам ЦП;
        // например, если есть КЦП и нет квоты особых прав и ЦП, то будет строка "Число мест на бюджет (КЦП) - 50";
        // если отчет строится только по бюджету - строки с планом по договору не будет, если отчет по договору - строк по бюджету не будет (включая расшифровку по квотам и видам ЦП)
        //Число мест на бюджет (КЦП) – 50, из них:
        //       квота лиц с особыми правами – 5
        //       квота целевого приема – 10
        //(из них по видам целевого приема: Южный фед. округ – 2, ФГУП Спецмнотаж – 3, МО Курганской обл. – 1, Тюменский район – 3)
        //Число мест с оплатой стоимости обучения – 100

        boolean printMinisterialPlan = printMinisterial && programSetOrgUnit.getMinisterialPlan() > 0;
        boolean printExclusivePlan = EnrRequestTypeCodes.BS.equals(programSetOrgUnit.getProgramSet().getRequestType().getCode());
        boolean printTaPlan = programSetOrgUnit.getTargetAdmPlan() > 0;
        boolean printContractPlan = printContract && programSetOrgUnit.getContractPlan() > 0;

        List<String> planLines = new ArrayList<>();
        if (printMinisterialPlan)
        {
            StringBuilder minPlan = new StringBuilder();
            minPlan.append("Число мест на бюджет (КЦП) – ").append(String.valueOf(programSetOrgUnit.getMinisterialPlan()));
            if (printExclusivePlan || printTaPlan)
            {
                minPlan.append(", из них:");
            }
            planLines.add(minPlan.toString());

            if (printExclusivePlan)
            {
                int plan;
                plan = programSetOrgUnit.getExclusivePlan();

                planLines.add("       квота лиц с особыми правами - " + String.valueOf(plan));
            }
            if (printTaPlan)
            {
                int plan;
                plan = programSetOrgUnit.getTargetAdmPlan();

                planLines.add("       квота целевого приема – " + String.valueOf(plan));
                List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);
                if (taPlans != null && taPlans.size() > 0)
                {
                    planLines.add("(из них по видам целевого приема: " + UniStringUtils.join(taPlans, EnrTargetAdmissionPlan.planString().s(), ", ") + ")");
                }
            }
        }
        if (printContractPlan)
        {
            planLines.add("Число мест с оплатой стоимости обучения – " + String.valueOf(programSetOrgUnit.getContractPlan()));
        }

        RtfString plan = new RtfString();
        for (String planLine : planLines)
        {
            if (planLines.indexOf(planLine) > 0) plan.par();
            plan.append(planLine);
        }

        modifier.put("plan", plan);

        if (hasRecommendedOther)
        {
            modifier.put("hasRecommended", "* - рекомендован в рамках другого конкурсного списка по более высокому приоритету");
        }
        else
        {
            UniRtfUtil.deleteRowsWithLabels(document, Lists.newArrayList("hasRecommended"));
        }
        modifier.modify(document);
    }

    private static String getTableHeader(EnrCompetition competition, EnrTargetAdmissionPlan taPlan, int size)
    {
        StringBuilder header = new StringBuilder();
        header.append(competition.getType().getPrintTitle());
        if (null != taPlan)
        {
            header.append(" – ").append(taPlan.getEnrCampaignTAKind().getTitle());
        }
        if (!EnrEduLevelRequirementCodes.NO.equals(competition.getEduLevelRequirement().getCode()))
        {
            header.append(", на базе ").append(competition.getEduLevelRequirement().getShortTitle());
        }
        header.append(" (заявлений – ").append(size);
        header.append(")");
        return header.toString();
    }

    private static void printContentTable(EnrCompetition competition, List<EnrRatingItem> entrantList, RtfDocument document, final String tableHeader,
                                          Set<EnrEntrant> ministerialRequestTotal, Set<EnrEntrant> contractRequestTotal,
                                          Map<String, RtfTable> contentTables,
                                          IEnrExamSetDao.IExamSetSettings examSet,
                                          Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap,
                                          boolean printPriority, boolean printAchievement, List<EnrEntrantAchievementType> achievementList,
                                          Map<EnrEntrant, List<EnrEntrantAchievement>> entrantAchievementMap, int taPlan)
    {
        final String tableName = chooseRtfTable(competition);
        RtfTable contentTable = contentTables.get(tableName).getClone();

        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(contentTable);

        final List<String[]> tableContent = new ArrayList<>();
        int number = 1;
        if (entrantList != null)
        {
            for (EnrRatingItem entrant : entrantList)
            {
                List<Double> marks = new ArrayList<>();
                List<EnrEntrantAchievement> entrantAchievementList = entrantAchievementMap.get(entrant.entrant);
                for (IEnrExamSetDao.IExamSetElementSettings exam : examSet.getElementList())
                {
                    EnrChosenEntranceExam chosenEntranceExam = entrantExamMap.get(entrant.getRequestedCompetition()).get(exam.getCgExam());
                    marks.add(chosenEntranceExam == null ? null : chosenEntranceExam.getMarkAsDouble());
                }


                tableContent.add(printEntrant(number++, entrant, tableName, marks, entrantAchievementList, achievementList, printPriority, printAchievement));

                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()))
                {
                    ministerialRequestTotal.add(entrant.entrant);
                }
                if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(entrant.getCompetition().getType().getCompensationType().getCode()))
                {
                    contractRequestTotal.add(entrant.entrant);
                }
            }
        }

        if (tableContent.isEmpty())
            tableContent.add(new String[1]);

        new RtfTableModifier()
                .put(tableName, tableContent.toArray(new String[tableContent.size()][]))
                .put(tableName, new RtfRowIntercepterBase() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (examSet.getElementList().isEmpty()) return;


                final int[] marksColumns = new int[examSet.getElementList().size()];
                Arrays.fill(marksColumns, 1);

                //удаляем ненужные столбцы
                if (!"T2".equals(tableName) && !"T4".equals(tableName))
                {
                    int colNumber = 3
                    if (!printAchievement)
                    {
                        //при необходимости удаляем столбец с индивидуальными достижениям
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 2), colNumber + 1)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 1), colNumber + 1)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex), colNumber + 1)
                    }

                    if (!printPriority)
                    {
                        //при необходимости удаляем столбец с приоритетами
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 2), colNumber)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 1), colNumber)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex), colNumber)
                    }
                }

                if ("T2".equals(tableName) || "T4".equals(tableName))
                {
                    int colNumber = 4
                    //при необходимости удаляем столбец с приоритетами

                    //при необходимости удаляем столбец с индивидуальными достижениям
                    if (!printAchievement)
                    {
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 2), colNumber + 1)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 1), colNumber + 1)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex), colNumber + 1)
                    }

                    if (!printPriority)
                    {
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 2), colNumber)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 1), colNumber)
                        RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex), colNumber)
                    }
                }

                if (printAchievement)
                {
                    final int[] achievementsColumns = new int[achievementList.size()];
                    Arrays.fill(achievementsColumns, 1);
                    int achievementColNumber = 4
                    if (!printPriority)
                        achievementColNumber--
                    if ("T2".equals(tableName) || "T4".equals(tableName))
                        achievementColNumber += 1

                    // разбиваем ячейку заголовка таблицы для названий индивидуальных достижений на нужное число элементов
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), achievementColNumber, new IRtfRowSplitInterceptor() {
                        @Override
                        void intercept(RtfCell newCell, int index)
                        {
                            String content = achievementList.get(index).achievementKind.title;
                            newCell.getElementList().addAll(new RtfString().append(content).toList())
                        }
                    }, achievementsColumns)


                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), achievementColNumber, new IRtfRowSplitInterceptor() {
                        @Override
                        void intercept(RtfCell newCell, int index)
                        {
                            newCell.getElementList().addAll(new RtfString().toList())
                        }
                    }, achievementsColumns)
                }



                if ("T2".equals(tableName) || "T4".equals(tableName))
                {
                    // разбиваем ячейку заголовка таблицы для названий оценок на нужное число элементов
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 3, new IRtfRowSplitInterceptor() {
                        @Override
                        void intercept(RtfCell newCell, int index)
                        {
                            String content = examSet.getElementList().get(index).getElement().getShortTitle();
                            newCell.getElementList().addAll(new RtfString().append(content).toList())
                        }
                    }, marksColumns)
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 3, new IRtfRowSplitInterceptor() {
                        @Override
                        void intercept(RtfCell newCell, int index)
                        {
                            newCell.getElementList().addAll(new RtfString().toList())
                        }
                    }, marksColumns)
                }

            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                List<IRtfElement> list = new ArrayList<>();
                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                text.setRaw(true);
                list.add(text);
                return list;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                newRowList.get(0).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());

                //проводим черту (3 - для учета заголовка)
                int bottomBorder = 3;
                if (!competition.isTargetAdmission())
                {
                     bottomBorder += competition.plan
                }
                else
                {
                    bottomBorder += taPlan
                }

                drawPlanLine(newRowList, bottomBorder)


            }
        }).modify(document);
    }

    private static String chooseRtfTable(EnrCompetition competition)
    {
        String requestTypeCode = competition.getRequestType().getCode();
        String compTypeCode = competition.getType().getCode();
        // Бакалавриат, специалитет:
        if (EnrRequestTypeCodes.BS.equals(requestTypeCode))
        {
            // Без ВИ в рамках КЦП - "T1"
            if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compTypeCode)) return "T1";
            // В рамках квоты лиц, имеющих особые права - "T2"
            if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(compTypeCode)) return "T2";
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return "T2";
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return "T2";
            // Без ВИ по договору - T3
            if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compTypeCode)) return "T3";
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return "T4";
        }
        // Магистратура:
        if (EnrRequestTypeCodes.MASTER.equals(requestTypeCode))
        {
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return "T2";
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return "T2";
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return "T4";
        }
        // Аспирантура:
        if (EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) || EnrRequestTypeCodes.POSTGRADUATE.equals(requestTypeCode) || EnrRequestTypeCodes.TRAINEESHIP.equals(requestTypeCode) || EnrRequestTypeCodes.INTERNSHIP.equals(requestTypeCode))
        {
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return "T2";
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return "T4";
        }
        // СПО:
        if (EnrRequestTypeCodes.SPO.equals(requestTypeCode))
        {
            // Общий конкурс - T5
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return "T5";
            // По договору - T5
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return "T5";
        }
        throw new IllegalArgumentException();
    }

    private static String[] printEntrant(int number, EnrRatingItem ratingItem, String tableName, List<Double> marks,
                                         List<EnrEntrantAchievement> entrantAchievementList, List<EnrEntrantAchievementType> achievementColumn,
                                         boolean printPriority, boolean printAchievement)
    {
        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();

        List<String> row = new ArrayList<>();
        // номер
        row.add(String.valueOf(number));
        // фио
        row.add(reqComp.getRequest().getEntrant().getFullFio());
        // категория
        if ("T1".equals(tableName) || "T3".equals(tableName))
        {
            if (reqComp instanceof IEnrEntrantBenefitStatement)
            {
                row.add(((IEnrEntrantBenefitStatement) reqComp).getBenefitCategory().getShortTitle());
            }
            else row.add("");
        }
        // ср. балл
        if ("T5".equals(tableName))
        {
            Double avgMark = reqComp.getRequest().getEduDocument().getAvgMarkAsDouble();
            row.add(avgMark == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(avgMark));
        }
        // сумма и результаты
        if ("T2".equals(tableName) || "T4".equals(tableName))
        {
            double summMarks = ratingItem.getTotalMarkAsDouble()
            // Для бакалавриата, специалитета и магистратуры индивидуальные достижения уже учтены в итоговой оценке
            if (!(EnrRequestTypeCodes.BS.equals(reqComp.competition.requestType.code) || EnrRequestTypeCodes.MASTER.equals(reqComp.competition.requestType.code)))
            {
                for (EnrEntrantAchievement achievement : entrantAchievementList)
                {
                    summMarks += achievement.mark
                }
            }
            row.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(summMarks));
            for (Double mark : marks)
            {
                row.add(mark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(mark));
            }
            if (marks.isEmpty()) row.add(""); // иначе таблица поедет
        }
        // приоритет
        if (printPriority)
        {
            row.add(String.valueOf(reqComp.getPriority()));
        }
        if (printAchievement)
        {
            for (EnrEntrantAchievementType type : achievementColumn)
            {
                String achMark = ""
                for (EnrEntrantAchievement achievement : entrantAchievementList)
                {
                    if (type.equals(achievement.type))
                    {
                        achMark += achMark.length()>0?", ":""
                        achMark+=DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(achievement.mark)
                    }
                }
                       row.add(achMark)
               }
        }
        // оригинал
        row.add(YesNoFormatter.INSTANCE.format(reqComp.isOriginalDocumentHandedIn()));

        return row.toArray(new String[row.size()]);
    }

    private static void drawPlanLine(List<RtfRow> newRowList, int bottomBorder)
    {
        if (newRowList.size() > bottomBorder)
        {
            for(RtfCell cell : newRowList.get(bottomBorder).cellList)
            {
                RtfBorder oldBottom = cell.getCellBorder().bottom
                RtfBorder newBottom = RtfBorder.getInstance(60, oldBottom.colorIndex, oldBottom.style)
                cell.getCellBorder().setBottom(newBottom)

            }
        }
    }

    //Utils
    private static boolean getBooleanFromWrapper(DataWrapper o)
    {
        return TwinComboDataSourceHandler.getSelectedValueNotNull(o);
    }

}