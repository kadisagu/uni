package unirostgmu.scripts.report

import org.apache.commons.collections15.CollectionUtils
import org.apache.commons.collections15.Transformer
import org.hibernate.Session
import org.tandemframework.core.entity.EntityComparator
import org.tandemframework.core.entity.EntityOrder
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon
import ru.tandemservice.unienr14.competition.entity.*
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unirostgmu.report.bo.RostgmuEnrReport.ui.EntrantListAdd.RostgmuEnrReportEntrantListAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 24.02.2015
 */

return new RostgmuEnrReportEntrantListPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class RostgmuEnrReportEntrantListPrint
{
    Session session
    byte[] template
    RostgmuEnrReportEntrantListAddUI model

    def print()
    {
        def document = createReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'RostgmuEntrantListPrint.rtf']
    }

    private RtfDocument createReport(RtfDocument document)
    {
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // Выбираем абитуриентов, подходящих под параметры отчета
        final Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();

        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, filterAddon, true);
        requestedCompDQL.column(requestedCompDQL.rating());

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(session).<EnrRatingItem> list())
        {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
            nonEmptyBlocks.add(ratingItem.getCompetition().getProgramSetOrgUnit());
        }

        //Выбранные вступительные испытания
        Map<EnrRequestedCompetition, List<EnrChosenEntranceExam>> enrCompExamMap = new HashMap<>();

        //Формы сдачи ВИ
        Map<EnrChosenEntranceExam, String> examPassFormMap = new HashMap<>();

        EnrEntrantDQLSelectBuilder examDQL = prepareEntrantDQL(model, filterAddon, false);
        examDQL.fromEntity(EnrChosenEntranceExam.class, "exam");
        examDQL.where(eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("exam")), property(examDQL.reqComp())));
        examDQL.column("exam")
        List<EnrChosenEntranceExam> examList = examDQL.createStatement(getSession()).<EnrChosenEntranceExam> list()

        List<Object[]> examFormList = new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, "exam")
                .where(DQLExpressions.in(property("exam", EnrChosenEntranceExam.P_ID), examList))
                .column("exam")
                .column(property("exam", EnrChosenEntranceExam.maxMarkForm().markSource().chosenEntranceExamForm().passForm().title()), "passForm").createStatement(getSession()).list();

        for (Object[] examForm : examFormList)
        {
            examPassFormMap.put((EnrChosenEntranceExam) examForm[0], (String) examForm[1])
        }

        for (EnrChosenEntranceExam exam : examList)
        {
            SafeMap.safeGet(enrCompExamMap, exam.getRequestedCompetition(), ArrayList.class).add(exam);
            String passForm = examPassFormMap.get(exam) == null ? "" : examPassFormMap.get(exam)
            examPassFormMap.put(exam, passForm)
        }

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов (не путать с массивом выбранных конкурсов, о котором уже было выше написано).

        Collection<EnrCompetition> competitions = filterAddon.getFilteredList();

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition())
        {
            competitions = entrantMap.keySet();
        }

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<EnrCompetition>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList)
        {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // Сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());
        Collections.sort(blockList, new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
            {
                int result;
                result = -Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
                if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
                if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
                if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
                if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
                return result;
            }
        })
        // подгрузим образовательные программы наборов

        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(blockList, new Transformer<EnrProgramSetOrgUnit, EnrProgramSetBase>() {
            @Override
            EnrProgramSetBase transform(EnrProgramSetOrgUnit enrProgramSetOrgUnit)
            {
                return enrProgramSetOrgUnit.getProgramSet();

            }
        });

        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : DataAccessServices.dao().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets))
        {
            programMap.get(item.getProgramSet()).add(item.getProgram());
        }
        for (EnrProgramSetBase programSetBase : programSets)
        {
            if (programSetBase instanceof EnrProgramSetSecondary)
            {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary) programSetBase).getProgram());
            }
        }

        // сразу заменим метку даты
        new RtfInjectModifier().put("dateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.dateTo)).modify(document);
        //берем из шаблона таблицы
        RtfTable emptyHeaderTable = removeTable(document, "programSetOrProgram", false)
        RtfTable emptyContentTable = removeTable(document, "T", false)

        // и чистим шаблон, чтобы не было ненужных переводов строк. Предварительно копируем заголовок
        IRtfElement reportHeader = document.getElementList().get(0)
        document.getElementList().clear();
        document.getElementList().add(reportHeader)
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

        // выводим блоки отчета
        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList)
        {
            RtfTable headerTable = emptyHeaderTable.clone;
            document.getElementList().add(headerTable);
            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
            boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);

            List<EnrCompetition> enrCompetitions = blockMap.get(programSetOrgUnit);
            fillHeaderData(document, programSetOrgUnit, programs, printOP);

            List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);

            for (EnrCompetition competition : enrCompetitions)
            {

                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0)
                {
                    // если прием по ЦП отдельно, делим списки по видам ЦП
                    for (EnrTargetAdmissionPlan taPlan : taPlans)
                    {
                        List<EnrRatingItem> entrantList = new ArrayList<>();
                        for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class))
                        {
                            if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA) entrant.getRequestedCompetition()).getTargetAdmissionKind()))
                            {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty())
                        {
                            continue;
                        }
                        final String tableHeader = getTableHeader(competition, taPlan, entrantList.size());
                        printContentTable(entrantList, document, emptyContentTable, enrCompExamMap, examPassFormMap, tableHeader);
                    }
                }
                else
                {
                    List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    final String tableHeader = getTableHeader(competition, null, entrantList.size());
                    printContentTable(entrantList, document, emptyContentTable, enrCompExamMap, examPassFormMap, tableHeader);
                }
            }


            if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }

        }

        return document
    }

    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(RostgmuEnrReportEntrantListAddUI model, EnrCompetitionFilterAddon filterAddon, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());

        requestedCompDQL.defaultActiveFilter();



        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.isSkipParallel())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        requestedCompDQL.order(property(EnrEntrant.person().identityCard().lastName().fromAlias(requestedCompDQL.entrant())))
        requestedCompDQL.order(property(EnrEntrant.person().identityCard().firstName().fromAlias(requestedCompDQL.entrant())))
        requestedCompDQL.order(property(EnrEntrant.person().identityCard().middleName().fromAlias(requestedCompDQL.entrant())))
        return requestedCompDQL;
    }

    private static RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel)
        {
            for (RtfRow row : table.getRowList())
            {
                for (RtfCell cell : row.getCellList())
                {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                    {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }

        document.getElementList().remove(table);

        return table;
    }

    private static List<EnrTargetAdmissionPlan> getTaPlans(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        return programSetOrgUnit.getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition() ?
                DataAccessServices.dao().getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit,
                        EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s()) : null;
    }

    private static void printContentTable(List<EnrRatingItem> entrantList, RtfDocument document,
                                          RtfTable emptyContentTable, Map<EnrRequestedCompetition, List<EnrChosenEntranceExam>> enrCompExamMap, Map<EnrChosenEntranceExam, String> examPassFormMap, String tableHeader)
    {
        final String tableName = "T";
        RtfTable contentTable = emptyContentTable.getClone();
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

        document.getElementList().add(contentTable);

        final List<String[]> tableContent = new ArrayList<>();
        int number = 1;
        if (entrantList != null)
        {
            for (EnrRatingItem entrant : entrantList)
            {
                String examDisc = ""

                for (EnrChosenEntranceExam exam : SafeMap.safeGet(enrCompExamMap, entrant.requestedCompetition, ArrayList.class))
                {
                    examDisc += examDisc.length() > 0 ? ", " : "";
                    examDisc += exam.discipline.title + " (" + examPassFormMap.get(exam) + ")"
                }

                String[] row = [String.valueOf(number++), entrant.entrant.fullFio, entrant.requestedCompetition.request.regDate.format("dd.MM.yyyy"), examDisc]
                tableContent.add(row)

            }
        }

        if (tableContent.isEmpty())
            tableContent.add(new String[1]);

        new RtfTableModifier()
                .put(tableName, tableContent as String[][])
                .put(tableName, new RtfRowIntercepterBase() {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                newRowList.get(0).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());
            }
        })
                .modify(document)
    }

    private static String getTableHeader(EnrCompetition competition, EnrTargetAdmissionPlan taPlan, int size)
    {
        StringBuilder header = new StringBuilder();
        header.append(competition.getType().getPrintTitle());
        if (null != taPlan)
        {
            header.append(" – ").append(taPlan.getEnrCampaignTAKind().getTitle());
        }
        if (!EnrEduLevelRequirementCodes.NO.equals(competition.getEduLevelRequirement().getCode()))
        {
            header.append(", на базе ").append(competition.getEduLevelRequirement().getShortTitle());
        }
        header.append(" (заявлений – ").append(size);
        header.append(")");
        return header.toString();
    }

    private static void fillHeaderData(RtfDocument document, EnrProgramSetOrgUnit programSetOrgUnit, List<EduProgramProf> programs,
                                       boolean printOP)
    {
        Collections.sort(programs, new Comparator<EduProgramProf>() {
            @Override
            int compare(EduProgramProf o1, EduProgramProf o2)
            {
                return o1.getTitleAndConditionsShort().compareTo(o2.getTitleAndConditionsShort());
            }
        });


        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
                .put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
                .put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getTitle());
        modifier.modify(document);
    }
}