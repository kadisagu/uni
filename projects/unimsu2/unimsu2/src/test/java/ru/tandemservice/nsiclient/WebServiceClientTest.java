/* $Id:$ */
package ru.tandemservice.nsiclient;

import org.apache.axis.message.MessageElement;
import org.testng.annotations.Test;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.datagram.RelDegreeType;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.UUID;

/**
 * @author Andrey Nikonov
 * @since 20.03.2015
 */
public class WebServiceClientTest
{
    private static RelDegreeType createRelDegreeType()
    {
        RelDegreeType entity = new RelDegreeType();
        entity.setID("cc5d937a-3202-3c23-8a77-xxxxxxxxxxxX");
        entity.setRelDegreeName("Мужик");
        entity.setRelDegreeID("77");
        return entity;
    }

    @Test
    public void clientServiceTest() throws Exception
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId("OB");
        header.setDestinationId("NSI");
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        Datagram datagram = new Datagram();

        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().add(createRelDegreeType());

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));
        MessageElement datagramOut = new org.apache.axis.message.SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);

        ServiceSoapImplService service = new ServiceSoapImplService(new URL("http://localhost:8090/services/NSIService?WSDL"), ServiceSoapImplService.SERVICE);
        service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).insert(request);
    }
}