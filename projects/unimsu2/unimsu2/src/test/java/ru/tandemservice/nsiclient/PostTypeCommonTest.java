/* $Id:$ */
package ru.tandemservice.nsiclient;

import org.apache.axis.message.MessageElement;
import org.tandemframework.hibsupport.DataAccessServices;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.datagram.PostType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

/**
 * @author Andrey Nikonov
 * @since 15.05.2015
 */
public class PostTypeCommonTest {
    public static final String WS_URL = "http://localhost:8090/services/NSIService?WSDL";
    public static final Integer CATALOG_CODE_FOR_TEST = 99999;

    @DataProvider
    private Object[][] generateData()
    {
        return new Object[][]{
//                {ID, PostID, PostName, PostAUP, PostKind},
                //{"", "", "", ""},
                {"cc5d937a-3202-3c23-8a11-postTypexxxX", "122", "PostNameTest", "1", "PostKindTest"},
                //{"Name", "Lastname", "Surname", "n.s.lastname"},
                //{"Имя", "Фамилия", "Отчество", "i.o.familiya"},
                //{"имя", "фамилия", "отчество", "i.o.familiya"},
                //{"имя", "фамилия", "", "i.familiya"},
                //{"имя", "", "", "i."},
                //{"", "фамилия", "", "familiya"},
                //{"Яков", "Щёткин", "Ильич", "ya.i.tshyotkin"},
        };
    }

    private PostType createPostType(String id, String postId, String postName, String postAUP, String postKind)
    {
        PostType entity = new PostType();
        entity.setID(id);
        entity.setPostID(postId);
        entity.setPostName(postName);
        entity.setPostAUP(postAUP);
        entity.setPostKind(postKind);
        return entity;
    }

    @Test(dataProvider = "generateData")
    public void testGenerateLogin(String id, String postId, String postName, String postAUP, String postKind) throws MalformedURLException {
        PostType entity = createPostType(id, postId, postName, postAUP, postKind);
        ServiceRequestType request = getRequest(entity);
        ServiceSoapImplService service = new ServiceSoapImplService(new URL(WS_URL), ServiceSoapImplService.SERVICE);
        ServiceResponseType responseType = service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).insert(request);
        Assert.assertNotNull(responseType);
        NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.P_GUID, entity.getID());
        Assert.assertNotNull(DataAccessServices.dao().get(nsiEntity.getEntityId()));
    }

    protected static ServiceRequestType getRequest(IDatagramObject datagramObject)
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId("OB");
        header.setDestinationId("NSI");
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        Datagram datagram = new Datagram();

        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().add(datagramObject);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));

        MessageElement datagramOut = new org.apache.axis.message.SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);
        return request;
    }

    protected ServiceResponseType obtain(IDatagramObject datagramObject) throws MalformedURLException {
        ServiceRequestType request = getRequest(datagramObject);
        ServiceSoapImplService service = new ServiceSoapImplService(new URL(WS_URL), ServiceSoapImplService.SERVICE);
        return service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).insert(request);
    }
}
