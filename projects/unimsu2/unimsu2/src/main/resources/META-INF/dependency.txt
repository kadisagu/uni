org.tandemframework core 1.6.18-SNAPSHOT 
org.tandemframework db-support 1.6.18-SNAPSHOT 
org.tandemframework hib-support 1.6.18-SNAPSHOT 
org.tandemframework rtf 1.6.18-SNAPSHOT 
org.tandemframework caf 1.6.18-SNAPSHOT 
org.tandemframework __classreplace 1.6.18-SNAPSHOT 
org.tandemframework tap-support 1.6.18-SNAPSHOT 
org.tandemframework common 1.6.18-SNAPSHOT 
org.tandemframework sec 1.6.18-SNAPSHOT 
org.tandemframework.shared sandbox 1.11.4-SH-SNAPSHOT Sandbox
org.tandemframework.shared commonbase 1.11.4-SH-SNAPSHOT Общие прикладные функции
org.tandemframework.shared fias 1.11.4-SH-SNAPSHOT Реестр адресов
org.tandemframework.shared organization 1.11.4-SH-SNAPSHOT Оргструктура
org.tandemframework.shared person 1.11.4-SH-SNAPSHOT Персоны
org.tandemframework.shared cxfws 1.11.4-SH-SNAPSHOT Веб-сервисы CXF
org.tandemframework.shared image 1.11.4-SH-SNAPSHOT Работа с изображениями
org.tandemframework.shared archive 1.11.4-SH-SNAPSHOT Архив
org.tandemframework ldap 1.6.18-SNAPSHOT 
ru.tandemservice.uni.product unibase 2.11.4-UNI-SNAPSHOT Базовый модуль
ru.tandemservice.uni.product uniedu 2.11.4-UNI-SNAPSHOT Образовательные программы
org.tandemframework.shared employeebase 1.11.4-SH-SNAPSHOT Кадровый реестр
org.tandemframework.shared ctr 1.11.4-SH-SNAPSHOT Базовые договоры
org.tandemframework.shared survey 1.11.4-SH-SNAPSHOT Анкетирование
ru.tandemservice.uni.product uni 2.11.4-UNI-SNAPSHOT Студенты
org.tandemframework.shared __replace_alfresco 1.11.4-SH-SNAPSHOT Replace some alfresco classes
org.tandemframework.shared dsign 1.11.4-SH-SNAPSHOT Цифровая подпись
org.tandemframework.shared bpms 1.11.4-SH-SNAPSHOT СЭД
ru.tandemservice.nsiclient nsiclient 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (базовые механизмы для обмена с НСИ)
ru.tandemservice.nsiclient nsifias 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Реестр адресов)
ru.tandemservice.nsiclient nsiperson 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Персоны)
ru.tandemservice.nsiclient nsiorganization 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Оргструктура)
ru.tandemservice.nsiclient nsiemployeebase 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Кадровый реестр)
ru.tandemservice.nsiclient nsibpms 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль СЭД)
ru.tandemservice.uni.project unimsu2 2.11.4-UNI-SNAPSHOT Проектный модуль МГУ
