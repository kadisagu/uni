package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;

import java.io.File;
import java.util.Map;

public interface IPostIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.IPostIODao
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Map<String, Post> importPostTable(final File csvFile, EmployeeType defaultEmployeeType) throws Exception;
}
