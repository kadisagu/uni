package ru.tandemservice.unimsu2.base.bo.Unimsu2Employee.ui.Add;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.Add.EmployeeAddUI;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;

public class Unimsu2EmployeeAddUI extends EmployeeAddUI
{
    @Override
    public void onComponentActivate()
    {
        super.onComponentActivate();
        getIdentityCard().setCardType(ICommonDAO.DAO_CACHE.get().getByCode(IdentityCardType.class, IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA));
        AddressCountry russia = DataAccessServices.dao().get(AddressCountry.class, AddressCountry.code(), IKladrDefines.RUSSIA_COUNTRY_CODE);
        getIdentityCard().setCitizenship(russia);
        Sex sex = DataAccessServices.dao().get(Sex.class, Sex.code().s(), SexCodes.MALE);
        getIdentityCard().setSex(sex);
    }
}
