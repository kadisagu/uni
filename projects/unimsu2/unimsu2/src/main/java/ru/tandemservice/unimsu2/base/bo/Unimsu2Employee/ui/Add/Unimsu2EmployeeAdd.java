/* $Id: EmployeeAdd.java 1562 2012-12-12 08:47:29Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unimsu2.base.bo.Unimsu2Employee.ui.Add;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.component.meta.BusinessComponentInit;

@Configuration
@BusinessComponentInit(overridenComponent = "EmployeeAdd")
public class Unimsu2EmployeeAdd extends BusinessComponentManager
{
}
