/* $Id:$ */
package ru.tandemservice.unimsu2.base.bo.BpmUserSettings.ui.RequestPassword;

import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import org.tandemframework.bpms.util.BpmCommonUtils;
import org.tandemframework.core.auth.IAuthService;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ModuleRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unimsu2.base.bo.BpmUserSettings.ui.ChangeLogin.BpmUserSettingsChangeLogin;

public class BpmUserSettingsRequestPasswordUI extends AbstractBpmUIPresenter
{
    private String _password;

    public String getPassword()
    {
        return _password;
    }

    public void setPassword(String password)
    {
        _password = password;
    }

    public void onPasswordCheck()
    {
        IAuthService authService = (IAuthService) ModuleRuntime.getBean("simpleAuthService");
        Person2PrincipalRelation relation = DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.person().id(), BpmCommonUtils.getCurrentPerson().getId());
        if(!authService.authenticate(relation.getPrincipal(), _password))
            throw new ApplicationException("Неверный пароль");
        _uiActivation.asCurrent(BpmUserSettingsChangeLogin.class).activate();
    }
}
