package ru.tandemservice.unimsu2.reports.bo.BpmRegAdministrativeListReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.reports.bo.BpmReportBase.logic.IBpmReportManager;
import org.tandemframework.bpms.reports.bo.BpmReportBase.support.ListReportParams;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.document.IDocumentRenderer;
import ru.tandemservice.unimsu2.reports.bo.BpmRegAdministrativeListReport.logic.BpmRegAdministrativeListReport;
import ru.tandemservice.unimsu2.reports.bo.BpmRegAdministrativeListReport.ui.Add.BpmRegAdministrativeListReportAdd;
import ru.tandemservice.unimsu2.reports.entity.codes.BpmReportTypeCodes;

@Configuration
public class BpmRegAdministrativeListReportManager extends BusinessObjectManager implements IBpmReportManager<ListReportParams>
{
    public static BpmRegAdministrativeListReportManager instance()
    {
        return instance(BpmRegAdministrativeListReportManager.class);
    }

    @Override
    public IDocumentRenderer createReport(ListReportParams params, boolean saveReport, String reportName) throws Exception {
        return new BpmRegAdministrativeListReport(params).createReport(this, BpmReportTypeCodes.BPM_REG_ADMINISTRATIVE_LIST_REPORT, saveReport, reportName);
    }

    @Override
    public Class<? extends BusinessComponentManager> getComponentClass()
    {
        return BpmRegAdministrativeListReportAdd.class;
    }
}
