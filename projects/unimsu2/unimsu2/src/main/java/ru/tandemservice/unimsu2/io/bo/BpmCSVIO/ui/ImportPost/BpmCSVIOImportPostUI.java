/* $Id:$ */
package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportPost;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.BpmCSVIOManager;

import java.io.IOException;

public class BpmCSVIOImportPostUI extends AbstractBpmUIPresenter
{
    private IUploadFile _uploadFile;
    private EmployeeType _employeeType;

    public void onClickImportPostCSVFromClient() {
        try
        {
            BpmCSVIOManager.instance().dao().importPostsFromClientCSVFile(IOUtils.toByteArray(getUploadFile().getStream()), _employeeType);
            deactivate();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile _uploadFile)
    {
        this._uploadFile = _uploadFile;
    }

    public EmployeeType getEmployeeType()
    {
        return _employeeType;
    }

    public void setEmployeeType(EmployeeType _employeeType)
    {
        this._employeeType = _employeeType;
    }
}
