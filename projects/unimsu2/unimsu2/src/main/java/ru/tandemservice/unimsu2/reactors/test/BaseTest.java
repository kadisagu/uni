/* $Id:$ */
package ru.tandemservice.unimsu2.reactors.test;

import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.tandemframework.core.context.ILinkFactoryService;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.testng.Assert;
import org.testng.IMethodInstance;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.codes.NsiSubSystemCodes;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * @author Andrey Nikonov
 * @since 15.05.2015
 */
public class BaseTest {
    public static final String INSERT_GROUP = NsiUtils.OPERATION_TYPE_INSERT;
    public static final String RETRIEVE_GROUP = NsiUtils.OPERATION_TYPE_RETRIEVE;
    public static final String DELETE_GROUP = NsiUtils.OPERATION_TYPE_DELETE;

    public List<IMethodInstance> intercept(List<IMethodInstance> methods, ITestContext context) {
        List<IMethodInstance> result = new ArrayList<>();
        for (IMethodInstance m : methods) {
            Test test = m.getMethod().getConstructorOrMethod().getClass().getAnnotation(Test.class);
            Set<String> groups = new HashSet<>();
            for (String group : test.groups()) {
                groups.add(group);
            }
            if (groups.contains(INSERT_GROUP)) {
                result.add(0, m);
            }
            else {
                result.add(m);
            }
        }
        return result;
    }

    protected static ServiceSoap getPort() throws MalformedURLException {
        URL url = new URL(ApplicationRuntime.getProperty(ILinkFactoryService.LINK_FACTORY_SERVICE_URL_BASE) + "/services/NSIService?WSDL");
        Assert.assertNotNull(url);
        ServiceSoapImplService service = new ServiceSoapImplService(url);
        Assert.assertNotNull(service);
        return service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class);
    }

    protected static ServiceRequestType getRequest(IDatagramObject datagramObject)
    {
        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().add(datagramObject);
        return getRequest(xDatagram);
    }

    protected static ServiceRequestType getRequest(List<IDatagramObject> datagrams)
    {
        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().addAll(datagrams);
        return getRequest(xDatagram);
    }

    private static ServiceRequestType getRequest(IXDatagram xDatagram)
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();

        NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.code(), NsiSubSystemCodes.LOCAL);
        String code = subSystem.getUserCode();

        header.setSourceId("test");
        header.setDestinationId(code);
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        Datagram datagram = new Datagram();

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));

        MessageElement datagramOut = new SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);
        return request;
    }

//    public static boolean retrieveTest(IDatagramObject datagramObject) throws Exception
//    {
//        ServiceRequestType request = getRequest(datagramObject);
//        ServiceSoapImplService service = new ServiceSoapImplService(new URL(WS_URL), ServiceSoapImplService.SERVICE);
//        ServiceResponseType responseType = service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).retrieve(request);
//        List<IDatagramObject> retrivedDatagram = NsiUtils.getDatagramElements(responseType);
//        return true;
//    }
//
//
//    public static boolean deleteTest(IDatagramObject datagramObject) throws Exception
//    {
//        ServiceRequestType request = getRequest(datagramObject);
//        ServiceSoapImplService service = new ServiceSoapImplService(new URL(WS_URL), ServiceSoapImplService.SERVICE);
//        service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).delete(request);
//        NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.P_GUID, datagramObject.getID());
//        return DataAccessServices.dao().get(nsiEntity.getEntityId()) == null;
//    }
}
