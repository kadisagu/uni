package ru.tandemservice.unimsu2.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатный шаблон"
 * Имя сущности : templateDocument
 * Файл data.xml : bpm.reports.data.xml
 */
public interface TemplateDocumentCodes
{
    /** Константа кода (code) элемента : bpmSummaryDocFlowReport (code). Название (title) : Сводка по документообороту */
    String BPM_SUMMARY_DOC_FLOW_REPORT = "bpmSummaryDocFlowReport";
    /** Константа кода (code) элемента : bpmAnalysisPerformanceDisciplineReport (code). Название (title) : Анализ по исполнению поручений */
    String BPM_ANALYSIS_PERFORMANCE_DISCIPLINE_REPORT = "bpmAnalysisPerformanceDisciplineReport";
    /** Константа кода (code) элемента : bpmPrintProperties (code). Название (title) : Карточка документа */
    String BPM_PRINT_PROPERTIES = "bpmPrintProperties";

    Set<String> CODES = ImmutableSet.of(BPM_SUMMARY_DOC_FLOW_REPORT, BPM_ANALYSIS_PERFORMANCE_DISCIPLINE_REPORT, BPM_PRINT_PROPERTIES);
}
