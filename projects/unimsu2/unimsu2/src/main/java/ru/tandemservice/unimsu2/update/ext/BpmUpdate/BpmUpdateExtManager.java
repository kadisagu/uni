package ru.tandemservice.unimsu2.update.ext.BpmUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.update.bo.BpmUpdate.BpmUpdateManager;
import org.tandemframework.bpms.update.support.IBpmUpdate;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unimsu2.base.bo.BpmUpdateUnimsu2.BpmUpdateUnimsu2Manager;
import ru.tandemservice.unimsu2.update.updates.BpmUpdate_unimsu_1x9x1_0;

@Configuration
public class BpmUpdateExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private BpmUpdateManager _bpmUpdateManager;

    @Bean
    public ItemListExtension<IBpmUpdate> bpmUpdatesExtPoint()
    {
        return itemListExtension(_bpmUpdateManager.bpmUpdatesExtPoint()).
                add(BpmUpdate_unimsu_1x9x1_0.class.getSimpleName(), BpmUpdateUnimsu2Manager.instance().update_unimsu_1x9x1_0()).
                create();
    }
}
