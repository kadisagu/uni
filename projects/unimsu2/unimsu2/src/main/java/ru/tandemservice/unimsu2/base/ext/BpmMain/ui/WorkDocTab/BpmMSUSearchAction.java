/* $Id:$ */
package ru.tandemservice.unimsu2.base.ext.BpmMain.ui.WorkDocTab;

import org.tandemframework.bpms.base.bo.BpmMain.ui.WorkDocTab.BpmMainWorkDocTabUI;
import org.tandemframework.bpms.base.bo.BpmSearch.BpmSearchManager;
import org.tandemframework.bpms.base.bo.BpmSearch.ui.Simple.BpmSearchSimpleAddon;
import org.tandemframework.bpms.util.BpmCommonUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unimsu2.util.BpmLoginListener;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 12/21/12
 * Time: 7:19 PM
 */
public class BpmMSUSearchAction extends NamedUIAction
{
    protected BpmMSUSearchAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof BpmMainWorkDocTabUI)
        {
            BpmMainWorkDocTabUI prsnt = (BpmMainWorkDocTabUI) presenter;
            BpmSearchSimpleAddon addon = (BpmSearchSimpleAddon) prsnt.getConfig().getAddon(BpmSearchManager.ADDON_SEARCH_SIMPLE);
            addon.onApply();
        }
        IPrincipalContext context = BpmCommonUtils.getCurrentPrincipalContext();
        if(context != null) {
            IDataSettings settings = DataSettingsFacade.getSettings(context.getId().toString());
            settings.set(BpmLoginListener.LOGIN_FOR_BPM_MAIN_TAB, false);
            DataSettingsFacade.saveSettings(settings);
        }
    }
}
