/* $Id:$ */
package ru.tandemservice.unimsu2.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import ru.tandemservice.nsiclient.datagram.GradeType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultWrapper;
import ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync.Unimsu2SyncManager;

import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class GradeTypeReactor extends ru.tandemservice.nsiemployeebase.reactors.GradeTypeReactor
{
    private static final String NSI_CODE = "nsi";

    @Override
    public ChangedWrapper<EtksLevels> processDatagramObject(GradeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        assert datagramObject.getID() != null;

        // GradeID init
        String code = StringUtils.trimToNull(datagramObject.getGradeID());
        // GradeName init
        String name = StringUtils.trimToNull(datagramObject.getGradeName());

        boolean isNew = false;
        boolean isChanged = false;
        EtksLevels entity = null;
        CoreCollectionUtils.Pair<EtksLevels, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findEtksLevels(code, name);
        if(entity == null)
        {
            entity = new EtksLevels();
            isNew = true;
            isChanged = true;
            entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EtksLevels.class));
        }

        // GradeID set
        StringBuilder warning = new StringBuilder();
        // Поле code immutable, поэтому оставляем как есть
        if(isNew)
        {
            if(code == null || !Unimsu2SyncManager.instance().dao().isPropertyUnique(EtksLevels.class, EtksLevels.P_CODE, entity.getId(), code)) {
                if(code == null)
                    warning.append("[GradeType object without GradeID!");
                else
                    warning.append("[GradeID is not unique!");
                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EtksLevels.class);
                warning.append(" GradeID was set to ").append(code).append(".]");
            }
            entity.setCode(code);
        } else if(code != null && !code.equals(entity.getCode()))
            warning.append("[GradeID is immutable!]");

        // GradeName set
        boolean isNameChanged = false;
        if (null == name && isNew)
        {
            name = Unimsu2SyncManager.instance().dao().findUniqueProperty(EtksLevels.class, EtksLevels.P_TITLE, entity.getId(), NSI_CODE);
            warning.append("[GradeType object without GradeName! GradeName was set to ").append(name).append(".]");
            isNameChanged = true;
        }
        if(name != null)
        {
            if(!isNameChanged && !Unimsu2SyncManager.instance().dao().isPropertyUnique(EtksLevels.class, EtksLevels.P_TITLE, entity.getId(), name)) {
                //throw new ProcessingDatagramObjectException("GradeName is not unique!");
                warning.append("[GradeName is not unique!");
                if(isNew) {
                    name = Unimsu2SyncManager.instance().dao().findUniqueProperty(EtksLevels.class, EtksLevels.P_TITLE, entity.getId(), NSI_CODE);
                    warning.append(" GradeName was set to ").append(name).append(".]");
                    entity.setTitle(name);
                } else
                    warning.append("]");
            }
            else {
                entity.setTitle(name);
                if(!name.equals(entity.getTitle()))
                    isChanged = true;
            }
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
