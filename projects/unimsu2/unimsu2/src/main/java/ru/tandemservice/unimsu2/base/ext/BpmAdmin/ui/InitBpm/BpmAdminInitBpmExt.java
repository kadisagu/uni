package ru.tandemservice.unimsu2.base.ext.BpmAdmin.ui.InitBpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmAdmin.ui.InitBpm.BpmAdminInitBpm;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;

@Configuration
public class BpmAdminInitBpmExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unimsu2" + BpmAdminInitBpmExtUI.class.getSimpleName();

    @Autowired
    private BpmAdminInitBpm _bpmAdminInitBpm;

    @Bean
    public ButtonListExtension blockListExtension()
    {
        return buttonListExtensionBuilder(_bpmAdminInitBpm.initButtonExtPoint())
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_bpmAdminInitBpm.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, BpmAdminInitBpmExtUI.class))
                .create();
    }
}
