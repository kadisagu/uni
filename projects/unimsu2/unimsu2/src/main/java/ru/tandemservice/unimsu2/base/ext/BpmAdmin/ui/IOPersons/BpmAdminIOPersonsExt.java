/* $Id:$ */
package ru.tandemservice.unimsu2.base.ext.BpmAdmin.ui.IOPersons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmAdmin.ui.IOPersons.BpmAdminIOPersons;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;

@Configuration
public class BpmAdminIOPersonsExt extends BusinessComponentExtensionManager
{
    public static final String IO_IMPORT_ORG_UNIT_CSV_FROM_CLIENT_BLOCK = "ioImportOrgUnitFromCSVClientBlock";
    public static final String IO_IMPORT_POST_CSV_FROM_CLIENT_BLOCK = "ioImportPostFromCSVClientBlock";
    public static final String IO_IMPORT_EMPLOYEE_POST_CSV_FROM_CLIENT_BLOCK = "ioImportEmployeePostFromCSVClientBlock";

    @Autowired
    private BpmAdminIOPersons _bpmAdminIOPersons;

    @Bean
    public BlockListExtension blockListExtension()
    {
        return blockListExtensionBuilder(_bpmAdminIOPersons.ioBlockListExtPoint())
                .addBlock(htmlBlock(IO_IMPORT_ORG_UNIT_CSV_FROM_CLIENT_BLOCK, "ioImportOrgUnitFromCSVClientBlock"))
                .addBlock(htmlBlock(IO_IMPORT_POST_CSV_FROM_CLIENT_BLOCK, "ioImportPostFromCSVClientBlock"))
                .addBlock(htmlBlock(IO_IMPORT_EMPLOYEE_POST_CSV_FROM_CLIENT_BLOCK, "ioImportEmployeePostFromCSVClientBlock"))
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_bpmAdminIOPersons.presenterExtPoint())
                .addAddon(uiAddon("unimsu2BpmAdminIOPersonsExtUI", BpmAdminIOPersonsExtUI.class))
                .addAction(new BpmAdminIOPersonsExtUI.ImportPostsAction("onClickLaunchImportPostsCSVFromClient"))
                .addAction(new BpmAdminIOPersonsExtUI.ImportEmployeePostsAction("onClickLaunchImportEmployeePostsCSVFromClient"))
                .addAction(new BpmAdminIOPersonsExtUI.ImportOrgUnitsAction("onClickLaunchImportOrgUnitsCSVFromClient"))
                .create();
    }
}
