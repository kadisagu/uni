/* $Id:$ */
package ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync.logic;

import org.tandemframework.core.entity.IEntity;

import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 19.05.2015
 */
public interface IUnimsu2SyncDAO
{
    /**
     * Возвращает список идентификаторов принципиалов по идентификаторам персон
     * @param entityIds идентификаторы персон
     * @return список идентификаторов принципиалов
     */
    List<Long> getPrincipalIdsByPersonIds(Collection<Long> entityIds);

    /**
     * Удаление порций сущностей запросами
     * @param entityClass класс сущности
     * @param entityIds идентификаторы сущностей
     */
    void batchDeleteEntities(Class<? extends IEntity> entityClass, Collection<Long> entityIds);

    /**
     * Находит незанятое значение свойства в сущности.
     * При наличии заданного значения свойства новое генерится как prefix + инкремент
     * @param entityClass класс, для которого нужно найти свободное поле
     * @param property свойство, для которого ищется значение
     * @param id идентификатор сущности, для которой генерится значение свойства, необязателен
     * @param prefix префикс для значения свойства
     * @return свободное значение свойства
     */
    String findUniqueProperty(Class<? extends IEntity> entityClass, String property, Long id, String prefix);

    boolean isPropertyUnique(Class<? extends IEntity> entityClass, String property, Long id, String title);
}
