/* $Id:$ */
package ru.tandemservice.unimsu2.base.bo.BpmUpdateUnimsu2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.update.bo.BpmUpdate.logic.BpmUpdateDAO;
import org.tandemframework.bpms.update.bo.BpmUpdate.logic.IBpmUpdateDAO;
import org.tandemframework.bpms.update.support.IBpmUpdate;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimsu2.update.updates.BpmUpdate_unimsu_1x9x1_0;

@Configuration
public class BpmUpdateUnimsu2Manager extends BusinessObjectManager
{
    public static BpmUpdateUnimsu2Manager instance()
    {
        return instance(BpmUpdateUnimsu2Manager.class);
    }

    @Bean
    public IBpmUpdateDAO dao()
    {
        return new BpmUpdateDAO();
    }

    @Bean
    public IBpmUpdate update_unimsu_1x9x1_0()
    {
        return new BpmUpdate_unimsu_1x9x1_0();
    }
}
