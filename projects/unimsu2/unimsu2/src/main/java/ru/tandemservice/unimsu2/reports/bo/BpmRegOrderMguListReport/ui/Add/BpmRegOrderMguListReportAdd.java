package ru.tandemservice.unimsu2.reports.bo.BpmRegOrderMguListReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmRegOrgUnit.BpmRegOrgUnitManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

@Configuration
public class BpmRegOrderMguListReportAdd extends BusinessComponentManager
{
    public static final String REG_ORG_UNIT_DS = "regOrgUnitDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().
                addDataSource(selectDS(REG_ORG_UNIT_DS, BpmRegOrgUnitManager.instance().regOrgUnitComboDSHandler())).
                create();
    }
}
