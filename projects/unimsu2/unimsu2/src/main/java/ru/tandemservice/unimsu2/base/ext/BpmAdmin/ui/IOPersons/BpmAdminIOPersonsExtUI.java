package ru.tandemservice.unimsu2.base.ext.BpmAdmin.ui.IOPersons;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.bpms.document.bo.BpmDocBase.ui.View.BpmDocBaseView;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportEmployeePost.BpmCSVIOImportEmployeePost;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportOrgUnit.BpmCSVIOImportOrgUnit;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportPost.BpmCSVIOImportPost;

public class BpmAdminIOPersonsExtUI extends UIAddon
{
    protected IUploadFile _uploadOrgUnitFile;

    public BpmAdminIOPersonsExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public IUploadFile getUploadOrgUnitFile()
    {
        return _uploadOrgUnitFile;
    }

    public void setUploadOrgUnitFile(IUploadFile uploadFile)
    {
        _uploadOrgUnitFile = uploadFile;
    }

    public static class ImportPostsAction extends NamedUIAction
    {
        public ImportPostsAction(String name)
        {
            super(name);
        }

        @Override
        public void execute(IUIPresenter presenter)
        {
            presenter.getActivationBuilder().asCurrent(BpmCSVIOImportPost.class).parameter(BpmDocBaseView.DEACTIVATE_ON_CLOSE, true).activate();
        }
    }

    public static class ImportEmployeePostsAction extends NamedUIAction
    {
        public ImportEmployeePostsAction(String name)
        {
            super(name);
        }

        @Override
        public void execute(IUIPresenter presenter)
        {
            presenter.getActivationBuilder().asCurrent(BpmCSVIOImportEmployeePost.class).parameter(BpmDocBaseView.DEACTIVATE_ON_CLOSE, true).activate();
        }
    }

    public static class ImportOrgUnitsAction extends NamedUIAction
    {
        public ImportOrgUnitsAction(String name)
        {
            super(name);
        }

        @Override
        public void execute(IUIPresenter presenter)
        {
            presenter.getActivationBuilder().asCurrent(BpmCSVIOImportOrgUnit.class).parameter(BpmDocBaseView.DEACTIVATE_ON_CLOSE, true).activate();
        }
    }
}
