/* $Id:$ */
package ru.tandemservice.unimsu2.util;

import org.tandemframework.core.context.LoginEvent;
import org.tandemframework.core.event.impl.IEventServiceListener;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 12/21/12
 * Time: 4:25 PM
 */
public class BpmLoginListener implements IEventServiceListener<LoginEvent>
{
    public static String LOGIN_FOR_BPM_MAIN_TAB = "bpmMainTab_login";

    @Override
    public void onEvent(LoginEvent loginEvent)
    {
        Long id = loginEvent.getPrincipalContext().getId();
        IDataSettings settings = DataSettingsFacade.getSettings(id.toString());
        settings.set(LOGIN_FOR_BPM_MAIN_TAB, true);
        DataSettingsFacade.saveSettings(settings);
    }

    @Override
    public Class<LoginEvent> getEventClass()
    {
        return LoginEvent.class;
    }
}
