package ru.tandemservice.unimsu2.base.ext.BpmAdmin.ui.InitBpm;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

public class BpmAdminInitBpmExtUI extends UIAddon
{
    public BpmAdminInitBpmExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}
