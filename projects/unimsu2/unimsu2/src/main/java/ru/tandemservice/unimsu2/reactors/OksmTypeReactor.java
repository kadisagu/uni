package ru.tandemservice.unimsu2.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.nsiclient.datagram.OksmType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultWrapper;
import ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync.Unimsu2SyncManager;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

public class OksmTypeReactor extends ru.tandemservice.nsifias.reactors.OksmTypeReactor
{
    @Override
    public ChangedWrapper<AddressCountry> processDatagramObject(OksmType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // OksmID init
        String code = StringUtils.trimToNull(datagramObject.getOksmID());
        Integer codeInt = null;
        if(code != null)
        {
            try {
                codeInt = Integer.parseInt(code);
            } catch (NumberFormatException e) {
                throw new ProcessingDatagramObjectException("OksmID is not number: " + code + '!');
            }
        }

        // OksmNameFull init
        String fullName = StringUtils.trimToNull(datagramObject.getOksmNameFull());
        // OksmNameShort init
        String shortName = StringUtils.trimToNull(datagramObject.getOksmNameShort());

        boolean isNew = false;
        boolean isChanged = false;
        AddressCountry entity = null;
        CoreCollectionUtils.Pair<AddressCountry, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findAddressCountry(codeInt, shortName, fullName);
        if(entity == null)
        {
            entity = new AddressCountry();
            isNew = true;
            isChanged = true;
        }

        // OksmNameFull set
        if(fullName != null && !fullName.equals(entity.getFullTitle()))
        {
            isChanged = true;
            entity.setFullTitle(fullName);
        }

        // OksmNameShort set
        if (null == shortName && isNew)
        {
            shortName = fullName;
            if(shortName == null)
                throw new ProcessingDatagramObjectException("OksmType object without OksmNameShort and OksmNameFull!");
        }
        if(shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }

        StringBuilder warning = new StringBuilder();
        String title = entity.getFullTitle();
        if(StringUtils.isEmpty(entity.getTitle()))
        {
            isChanged = true;
            title = Unimsu2SyncManager.instance().dao().findUniqueProperty(AddressCountry.class, AddressCountry.P_TITLE, entity.getId(), title);
            entity.setTitle(title);
        }

        // OksmID set
        // Поле code immutable, поэтому оставляем как есть
        if(isNew)
        {
            if(!DataAccessServices.dao().isUnique(AddressCountry.class, entity, AddressCountry.P_CODE)) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressCountry.class, "s")
                        .column(max(property("s", AddressCountry.code())));
                List<Integer> max = DataAccessServices.dao().getList(builder);
                codeInt = max.get(0) + 1;
                warning.append(" OksmID was set to ").append(codeInt).append(".]");
                entity.setCode(codeInt);
                entity.setDigitalCode(codeInt);
            }
        } else if(codeInt != null && codeInt != entity.getCode())
            warning.append("[OksmID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
