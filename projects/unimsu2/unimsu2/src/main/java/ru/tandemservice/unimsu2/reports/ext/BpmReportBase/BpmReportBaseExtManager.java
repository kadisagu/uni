package ru.tandemservice.unimsu2.reports.ext.BpmReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.reports.bo.BpmReportBase.BpmReportBaseManager;
import org.tandemframework.bpms.reports.bo.BpmReportBase.logic.IBpmReportManager;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unimsu2.reports.bo.BpmRegAdministrativeListReport.BpmRegAdministrativeListReportManager;
import ru.tandemservice.unimsu2.reports.bo.BpmRegOrderMguListReport.BpmRegOrderMguListReportManager;
import ru.tandemservice.unimsu2.reports.entity.codes.BpmReportTypeCodes;

@Configuration
public class BpmReportBaseExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private BpmReportBaseManager _bpmReportBaseManager;

    @Bean
    public ItemListExtension<IBpmReportManager> bpmReportTypeExtPoint()
    {
        return itemListExtension(_bpmReportBaseManager.bpmReportTypeExtPoint()).
                add(BpmReportTypeCodes.BPM_REG_ADMINISTRATIVE_LIST_REPORT, BpmRegAdministrativeListReportManager.instance()).
                add(BpmReportTypeCodes.BPM_REG_ORDER_MGU_LIST_REPORT, BpmRegOrderMguListReportManager.instance()).
                create();
    }
}
