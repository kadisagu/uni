package ru.tandemservice.unimsu2.reports.bo.BpmRegOrderMguListReport.logic;

import jxl.write.*;
import jxl.write.Number;
import org.tandemframework.bpms.catalog.entity.codes.BpmCaseTaskKindCodes;
import org.tandemframework.bpms.document.bo.BpmDocAdministrative.logic.BpmDocAdministrativeModifyDAO;
import org.tandemframework.bpms.document.entity.BpmDocAdministrative;
import org.tandemframework.bpms.document.entity.BpmDocBase;
import org.tandemframework.bpms.repo.entity.RepoNode;
import org.tandemframework.bpms.reports.bo.BpmReportBase.logic.AbstractBpmRegListReport;
import org.tandemframework.bpms.reports.bo.BpmReportBase.support.ListReportParams;
import org.tandemframework.bpms.task.bo.BpmCaseTaskConfig.BpmCaseTaskConfigManager;
import org.tandemframework.bpms.task.entity.BpmRouteStep;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class BpmRegOrderMguListReport extends AbstractBpmRegListReport
{
    private static final String[] COLUMN_TITLES = new String[]{
            "п/п", "Дата регистрации", "Рег. номер", "Заголовок", "Подписал",
            "Контроль по существу", "Исполнитель"
    };
    private static final int[] COLUMN_WIDTHS = new int[]{
            20, 60, 100, 250, 160,
            160, 160
    };

    public BpmRegOrderMguListReport(ListReportParams params) {
        super(params);
    }

    @Override
    protected List<String> getParams()
    {
        List<String> params = new ArrayList<>(1);
        params.add("Регистрирующее подразделение: " + (_regOrgUnit != null ? _regOrgUnit.getTitle() : "<Не выбрано>"));
        return params;
    }

    protected List<Object[]> getPreparedRows()
    {
        Object[] orderTitles = {
                BpmDocAdministrativeModifyDAO.ORDER_ACADEMIC,
                BpmDocAdministrativeModifyDAO.ORDER_CORE_BUSINESS,
                BpmDocAdministrativeModifyDAO.ORDER_FOREIGN_SENDING,
                BpmDocAdministrativeModifyDAO.ORDER_PERSONNEL,
                BpmDocAdministrativeModifyDAO.ORDER_RUSSIA_SENDING
        };
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(BpmDocAdministrative.class, "d")
                .joinEntity("d", DQLJoinType.left, EmployeePost.class, "cempl", eq(property("d", BpmDocAdministrative.controllerEmpl()), property("cempl", EmployeePost.id())))
                .joinEntity("d", DQLJoinType.left, EmployeePost.class, "pempl", eq(property("d", BpmDocAdministrative.performerEmpl()), property("pempl", EmployeePost.id())))
                .column(property("d", BpmDocBase.id()))
                .column(property("d", BpmDocBase.regDate()))
                .column(property("d", BpmDocBase.regNumber()))
                .column(property("d", BpmDocBase.title()))
                .column(property("d", BpmDocBase.node()))
                .column(property("cempl"))
                .column(property("pempl"))
                .column(property("d", BpmDocBase.node().kind().title()))
                .where(eq(property("d", BpmDocBase.zaregist()), value(true)))
                .where(isNull(property("d", BpmDocBase.node().deletedByEmpl())))
                .where(in(property("d", BpmDocBase.node().kind().title()), orderTitles))
                .order(property("d", BpmDocBase.node().kind().title()), OrderDirection.asc)
                .order(property("d", BpmDocBase.regDate()), OrderDirection.asc)
                .order(property("d", BpmDocBase.regNumber()), OrderDirection.asc);
        if(_regOrgUnit != null)
            builder.where(eq(property("d", BpmDocBase.regOrgUnit()), value(_regOrgUnit)));

        addCommonConditions(builder, "d");
        return DataAccessServices.dao().getList(builder);
    }

    @Override
    protected String[] getColumnTitles()
    {
        return COLUMN_TITLES;
    }

    @Override
    protected int[] getColumnWidths()
    {
        return COLUMN_WIDTHS;
    }

    @Override
    protected void renderRows(WritableSheet sheet, int rowIndex, Map<String, WritableCellFormat> formats) throws Exception
    {
        WritableCellFormat headerFormat = formats.get(FORMAT_HEADER);
        WritableCellFormat rowFormat = formats.get(FORMAT_ROW);

        int i = 1;
        List<Object[]> objects = getPreparedRows();
        String kindCode = null;
        // рисуем строки
        for (Object[] obj : objects)
        {
            if (kindCode == null || !kindCode.equals(obj[7]))
            {
                kindCode = (String) obj[7];
                sheet.addCell(new Label(0, rowIndex, kindCode, headerFormat));
                sheet.mergeCells(0, rowIndex, getColumnTitles().length - 1, rowIndex);
                rowIndex++;
            }
            int columnIndex = 0;

            // п/п
            sheet.addCell(new Number(columnIndex++, rowIndex, i++, rowFormat));

            // Дата регистрации
            sheet.addCell(new Label(columnIndex++, rowIndex, DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) obj[1]), rowFormat));

            // Рег. номер
            sheet.addCell(new Label(columnIndex++, rowIndex, (String) obj[2], rowFormat));

            // Заголовок
            String url = getDocumentUrl((Long) obj[0]);
            WritableHyperlink hl = new WritableHyperlink(columnIndex, rowIndex, new URL(url));
            sheet.addHyperlink(hl);
            sheet.addCell(new Label(columnIndex++, rowIndex, (String) obj[3], rowFormat));

            //Подписал
            BpmRouteStep step = BpmCaseTaskConfigManager.instance().dao().findStepByTaskKind(((RepoNode) obj[4]).getId(), BpmCaseTaskKindCodes.DOC_PODPISANIE_ROOT);
            sheet.addCell(new Label(columnIndex++, rowIndex, getParticipants(step), rowFormat));

            // Контроль по существу
            sheet.addCell(new Label(columnIndex++, rowIndex, getEmployeePostTitle((EmployeePost) obj[5]), rowFormat));

            // Исполнитель
            sheet.addCell(new Label(columnIndex, rowIndex, getEmployeePostTitle((EmployeePost) obj[6]), rowFormat));

            rowIndex++;
        }
    }
}