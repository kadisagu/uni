package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.io.File;
import java.util.Map;

public interface IOrgUnitIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.IOrgUnitIODao
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Map<String, OrgUnit> importOrgStructureTable(final File csvFile) throws Exception;
}
