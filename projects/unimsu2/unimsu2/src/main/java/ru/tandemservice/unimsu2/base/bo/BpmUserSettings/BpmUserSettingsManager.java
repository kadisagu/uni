/* $Id:$ */
package ru.tandemservice.unimsu2.base.bo.BpmUserSettings;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 9/24/12
 * Time: 6:31 PM
 */
@Configuration
public class BpmUserSettingsManager extends BusinessObjectManager
{
    public static BpmUserSettingsManager instance()
    {
        return instance(BpmUserSettingsManager.class);
    }
}
