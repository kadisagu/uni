package ru.tandemservice.unimsu2.reports.bo.BpmRegOrderMguListReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.reports.bo.BpmReportBase.logic.IBpmReportManager;
import org.tandemframework.bpms.reports.bo.BpmReportBase.support.ListReportParams;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.document.IDocumentRenderer;
import ru.tandemservice.unimsu2.reports.bo.BpmRegOrderMguListReport.logic.BpmRegOrderMguListReport;
import ru.tandemservice.unimsu2.reports.bo.BpmRegOrderMguListReport.ui.Add.BpmRegOrderMguListReportAdd;
import ru.tandemservice.unimsu2.reports.entity.codes.BpmReportTypeCodes;

@Configuration
public class BpmRegOrderMguListReportManager extends BusinessObjectManager implements IBpmReportManager<ListReportParams>
{
    public static BpmRegOrderMguListReportManager instance()
    {
        return instance(BpmRegOrderMguListReportManager.class);
    }

    @Override
    public IDocumentRenderer createReport(ListReportParams params, boolean saveReport, String reportName) throws Exception {
        return new BpmRegOrderMguListReport(params).createReport(this, BpmReportTypeCodes.BPM_REG_ORDER_MGU_LIST_REPORT, saveReport, reportName);
    }

    @Override
    public Class<? extends BusinessComponentManager> getComponentClass()
    {
        return BpmRegOrderMguListReportAdd.class;
    }
}
