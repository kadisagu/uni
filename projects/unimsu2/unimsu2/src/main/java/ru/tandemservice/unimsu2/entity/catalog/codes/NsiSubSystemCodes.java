package ru.tandemservice.unimsu2.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Подсистемы НСИ"
 * Имя сущности : nsiSubSystem
 * Файл data.xml : nsi.catalog.data.xml
 */
public interface NsiSubSystemCodes
{
    /** Константа кода (code) элемента : НСИ (title) */
    String NSI = "nsi";
    /** Константа кода (code) элемента : Локальная система (title) */
    String LOCAL = "local";

    Set<String> CODES = ImmutableSet.of(NSI, LOCAL);
}
