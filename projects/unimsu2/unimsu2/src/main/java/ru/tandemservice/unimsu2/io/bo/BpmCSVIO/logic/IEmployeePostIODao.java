package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;

import java.io.File;
import java.util.Map;

public interface IEmployeePostIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.IEmployeePostIODao
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Map<String, EmployeePost> importEmployeePostTable(File csvFile, Sex defaultSex, AddressCountry defaultAddressCountry,
                                                             IdentityCardType defaultCardType, PostType defaultPostType,
                                                             EmployeePostStatus defaultPostStatus, QualificationLevel defaultQualificationLevel,
                                                             ProfQualificationGroup defaultProfQualificationGroup, AuthenticationType defaultAuthenticationType) throws Exception;
}
