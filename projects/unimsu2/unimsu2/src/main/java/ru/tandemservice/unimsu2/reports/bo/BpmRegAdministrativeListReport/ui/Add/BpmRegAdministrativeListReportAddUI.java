package ru.tandemservice.unimsu2.reports.bo.BpmRegAdministrativeListReport.ui.Add;

import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import org.tandemframework.bpms.base.bo.BpmRegOrgUnit.logic.BpmRegOrgUnitComboDSHandler;
import org.tandemframework.bpms.base.bo.BpmRegOrgUnit.logic.BpmRegOrgUnitCommonParams;
import org.tandemframework.bpms.base.bo.BpmRegOrgUnit.ui.Select.AbstractBpmRegOrgUnitSelectAction;
import org.tandemframework.bpms.base.bo.BpmRegOrgUnit.ui.Select.BpmRegOrgUnitSelect;
import org.tandemframework.bpms.reports.bo.BpmReportBase.support.IReportUI;
import org.tandemframework.bpms.reports.bo.BpmReportBase.support.ListReportParams;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;

public class BpmRegAdministrativeListReportAddUI extends AbstractBpmUIPresenter implements IReportUI<ListReportParams>
{
    @Override
    public ListReportParams getParameters() {
        return new ListReportParams(_uiSettings.get("regDateFrom"), _uiSettings.get("regDateTo"), _uiSettings.get("regOrgUnit"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(BpmRegAdministrativeListReportAdd.REG_ORG_UNIT_DS.equals(dataSource.getName()))
            dataSource.put(BpmRegOrgUnitComboDSHandler.COMMON_PARAMS, createBpmRegOrgUnitCommonParams());
    }

    public void onRegOrgUnitSelect()
    {
        _uiActivation.asDesktopRoot(BpmRegOrgUnitSelect.class)
                .parameter(BpmRegOrgUnitSelect.SELECT_ACTION, new AbstractBpmRegOrgUnitSelectAction() {
                    @Override
                    public void selectRegOrgUnit(Long selectedRegOrgUnitId) {
                        _uiSettings.get("regOrgUnit", DataAccessServices.dao().get(selectedRegOrgUnitId));
                        _uiSettings.save();
                    }
                })
                .activate();
    }

    protected BpmRegOrgUnitCommonParams createBpmRegOrgUnitCommonParams()
    {
        return new BpmRegOrgUnitCommonParams();
    }
}
