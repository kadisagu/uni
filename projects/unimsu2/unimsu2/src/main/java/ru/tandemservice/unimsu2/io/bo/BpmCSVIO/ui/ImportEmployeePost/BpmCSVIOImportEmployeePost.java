/* $Id:$ */
package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportEmployeePost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;

@Configuration
public class BpmCSVIOImportEmployeePost extends BusinessComponentManager
{
    public static final String SEX_DS = "sexDS";
    public static final String ADDRESS_COUNTRY_DS = "addressCountryDS";
    public static final String IDENTITY_CARD_TYPE_DS = "identityCardTypeDS";
    public static final String POST_TYPE_DS = "postTypeDS";
    public static final String EMPLOYEE_POST_STATUS_DS = "employeePostStatusDS";
    public static final String QUALIFICATION_LEVEL_DS = "qualificationLevelDS";
    public static final String PROF_QUALIFICATION_GRIOUP_DS = "profQualificationGroupDS";
    public static final String AUTHENTICATION_TYPE_DS = "authenticationTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SEX_DS, sexComboDSHandler()))
                .addDataSource(selectDS(ADDRESS_COUNTRY_DS, addressCountryComboDSHandler()))
                .addDataSource(selectDS(IDENTITY_CARD_TYPE_DS, identityCardTypeComboDSHandler()))
                .addDataSource(selectDS(POST_TYPE_DS, postTypeComboDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_POST_STATUS_DS, employeePostStatusComboDSHandler()))
                .addDataSource(selectDS(QUALIFICATION_LEVEL_DS, qualificationLevelComboDSHandler()))
                .addDataSource(selectDS(PROF_QUALIFICATION_GRIOUP_DS, profQualificationGroupComboDSHandler()))
                .addDataSource(selectDS(AUTHENTICATION_TYPE_DS, authenticationTypeComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler sexComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), Sex.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler addressCountryComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), AddressCountry.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler identityCardTypeComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), IdentityCardType.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler postTypeComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), PostType.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler employeePostStatusComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EmployeePostStatus.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationLevelComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), QualificationLevel.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler profQualificationGroupComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), ProfQualificationGroup.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler authenticationTypeComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), AuthenticationType.class);
    }
}
