/* $Id:$ */
package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportEmployeePost;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.BpmCSVIOManager;

import java.io.IOException;

public class BpmCSVIOImportEmployeePostUI extends AbstractBpmUIPresenter
{
    private IUploadFile _uploadFile;
    private Sex _sex;
    private AddressCountry _addressCountry;
    private IdentityCardType _identityCardType;
    private PostType _postType;
    private EmployeePostStatus _employeePostStatus;
    private QualificationLevel _qualificationLevel;
    private ProfQualificationGroup _profQualificationGroup;
    private AuthenticationType _authenticationType;

    public void onClickImportEmployeePostCSVFromClient() {
        try
        {
            BpmCSVIOManager.instance().dao().importEmployeePostsFromClientCSVFile(IOUtils.toByteArray(getUploadFile().getStream()),
                    getSex(), getAddressCountry(), getIdentityCardType(), getPostType(), getEmployeePostStatus(), getQualificationLevel(), getProfQualificationGroup(), getAuthenticationType());
            deactivate();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile _uploadFile)
    {
        this._uploadFile = _uploadFile;
    }

    public Sex getSex()
    {
        return _sex;
    }

    public void setSex(Sex _sex)
    {
        this._sex = _sex;
    }

    public AddressCountry getAddressCountry()
    {
        return _addressCountry;
    }

    public void setAddressCountry(AddressCountry _addressCountry)
    {
        this._addressCountry = _addressCountry;
    }

    public IdentityCardType getIdentityCardType()
    {
        return _identityCardType;
    }

    public void setIdentityCardType(IdentityCardType _identityCardType)
    {
        this._identityCardType = _identityCardType;
    }

    public PostType getPostType()
    {
        return _postType;
    }

    public void setPostType(PostType _postType)
    {
        this._postType = _postType;
    }

    public EmployeePostStatus getEmployeePostStatus()
    {
        return _employeePostStatus;
    }

    public void setEmployeePostStatus(EmployeePostStatus _employeePostStatus)
    {
        this._employeePostStatus = _employeePostStatus;
    }

    public QualificationLevel getQualificationLevel()
    {
        return _qualificationLevel;
    }

    public void setQualificationLevel(QualificationLevel _qualificationLevel)
    {
        this._qualificationLevel = _qualificationLevel;
    }

    public ProfQualificationGroup getProfQualificationGroup()
    {
        return _profQualificationGroup;
    }

    public void setProfQualificationGroup(ProfQualificationGroup _profQualificationGroup)
    {
        this._profQualificationGroup = _profQualificationGroup;
    }

    public AuthenticationType getAuthenticationType()
    {
        return _authenticationType;
    }

    public void setAuthenticationType(AuthenticationType _authenticationType)
    {
        this._authenticationType = _authenticationType;
    }
}