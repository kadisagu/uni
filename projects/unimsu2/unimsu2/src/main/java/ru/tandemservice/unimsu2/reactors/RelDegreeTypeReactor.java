/* $Id:$ */
package ru.tandemservice.unimsu2.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.nsiclient.datagram.RelDegreeType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultWrapper;

import java.util.Map;
import java.util.Set;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class RelDegreeTypeReactor extends ru.tandemservice.nsiperson.reactors.RelDegreeTypeReactor
{
    @Override
    public ChangedWrapper<RelationDegree> processDatagramObject(RelDegreeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // RelDegreeID init
        String code = StringUtils.trimToNull(datagramObject.getRelDegreeID());
        // RelDegreeName init
        String name = StringUtils.trimToNull(datagramObject.getRelDegreeName());

        boolean isNew = false;
        boolean isChanged = false;
        RelationDegree entity = null;
        CoreCollectionUtils.Pair<RelationDegree, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findRelationDegree(code, name);
        if(entity == null)
        {
            entity = new RelationDegree();
            isNew = true;
            isChanged = true;
            entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(RelationDegree.class));
        }

        // RelDegreeID set
        if(code != null)
        {
            isChanged = true;
            entity.setUserCode(getUserCodeChecked((Map<Class<? extends IEntity>, Set<String>>) reactorCache.get(USED_CODES), code));
        }

        // RelDegreeName set
        if (null == name && isNew)
        {
            throw new ProcessingDatagramObjectException("RelDegreeType object without RelDegreeName!");
        }
        if(name != null)
        {
            isChanged = true;
            entity.setTitle(name);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, null);
    }
}
