/* $Id:$ */
package ru.tandemservice.unimsu2.reactors.test;

import org.tandemframework.hibsupport.DataAccessServices;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.datagram.PostType;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceRequestType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceResponseType;

import java.net.MalformedURLException;

/**
 * @author Andrey Nikonov
 * @since 15.05.2015
 */
public class PostTypeCommonTest extends BaseTest {
    /**
     * @testng.data-provider
     */
    private Object[][] generateData()
    {
        return new Object[][]{
//                {ID, PostID, PostName, PostAUP, PostKind,                                             result},
                {"cc5d937a-3202-3c23-8a11-postTypexxxX", "122", "PostNameTest", "1", "PostKindTest",    NsiUtils.RESPONSE_CODE_NUMBER_SUCCESS.intValue()},
                {"", "", "", "", "",                                                                    NsiUtils.RESPONSE_CODE_NUMBER_ERROR.intValue()},
                {"cc5d937a-3202-3c23-8a11-postTypexxxX", "122", "PostNameTest", "0", "PostKindTest",    NsiUtils.RESPONSE_CODE_NUMBER_WARN.intValue()},
                {"cc5d937a-3202-3c23-8a11-postTypexxxX", "122", "PostNameTest", "0", "PostKindTest",    NsiUtils.RESPONSE_CODE_NUMBER_WARN.intValue()},
                //{"Name", "Lastname", "Surname", "n.s.lastname"},
                //{"Имя", "Фамилия", "Отчество", "i.o.familiya"},
                //{"имя", "фамилия", "отчество", "i.o.familiya"},
                //{"имя", "фамилия", "", "i.familiya"},
                //{"имя", "", "", "i."},
                //{"", "фамилия", "", "familiya"},
                //{"Яков", "Щёткин", "Ильич", "ya.i.tshyotkin"},
        };
    }

    private PostType createPostType(String id, String postId, String postName, String postAUP, String postKind)
    {
        PostType entity = new PostType();
        entity.setID(id);
        entity.setPostID(postId);
        entity.setPostName(postName);
        entity.setPostAUP(postAUP);
        entity.setPostKind(postKind);
        return entity;
    }

    @Test(dataProvider = "generateData")
    public void insertTest(String id, String postId, String postName, String postAUP, String postKind, int resultCode) throws MalformedURLException
    {
        PostType entity = createPostType(id, postId, postName, postAUP, postKind);
        ServiceRequestType request = getRequest(entity);
        ServiceResponseType responseType = getPort().insert(request);
        Assert.assertNotNull(responseType);
        Assert.assertEquals(responseType.getRoutingHeader().getMessageId(), request.getRoutingHeader().getMessageId());
        Assert.assertNotNull(responseType.getCallCC());
        Assert.assertEquals(responseType.getCallCC().intValue(), resultCode);

        if(resultCode != NsiUtils.RESPONSE_CODE_NUMBER_ERROR.intValue())
        {
            NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.P_GUID, entity.getID());
            Assert.assertNotNull(DataAccessServices.dao().get(nsiEntity.getEntityId()));
        }
    }
}
