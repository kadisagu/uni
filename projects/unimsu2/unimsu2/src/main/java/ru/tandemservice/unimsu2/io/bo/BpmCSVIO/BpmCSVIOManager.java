package ru.tandemservice.unimsu2.io.bo.BpmCSVIO;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic.*;

@Configuration
public class BpmCSVIOManager extends BusinessObjectManager
{
    public static BpmCSVIOManager instance()
    {
        return instance(BpmCSVIOManager.class);
    }

    @Bean
    public IBpmIODao dao()
    {
        return new BpmIODao();
    }

    @Bean
    public IOrgUnitIODao orgUnitIODao()
    {
        return new OrgUnitIODao();
    }

    @Bean
    public IPostIODao postIODao()
    {
        return new PostIODao();
    }

    @Bean
    public IEmployeePostIODao employeePostIODao()
    {
        return new EmployeePostIODao();
    }
}
