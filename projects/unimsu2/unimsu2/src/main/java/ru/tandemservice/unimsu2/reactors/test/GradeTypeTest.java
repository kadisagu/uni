/* $Id:$ */
package ru.tandemservice.unimsu2.reactors.test;

import org.tandemframework.hibsupport.DataAccessServices;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.datagram.GradeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceRequestType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceResponseType;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 15.05.2015
 */
public class GradeTypeTest extends BaseTest {

    private GradeType createGradeType(String id, String gradeId, String name)
    {
        GradeType entity = new GradeType();
        entity.setID(id);
        entity.setGradeID(gradeId);
        entity.setGradeName(name);
        return entity;
    }

    /*
     * Простая проверка добавления элементов по одному
     */
    @Test(dataProvider = "generateData", groups = INSERT_GROUP, priority = 2)
    public void insertTest(String id, String gradeId, String name, int resultCode) throws MalformedURLException
    {
        GradeType entity = createGradeType(id, gradeId, name);
        ServiceRequestType request = getRequest(entity);
        ServiceResponseType responseType = getPort().insert(request);
        Assert.assertNotNull(responseType, "Response is null!");
        Assert.assertEquals(responseType.getRoutingHeader().getMessageId(), request.getRoutingHeader().getMessageId(), "Wrong response message id!");
        Assert.assertNotNull(responseType.getCallCC(), "Response response code is null!");
        Assert.assertEquals(responseType.getCallCC().intValue(), resultCode, "Wrong response code!");

        if(resultCode != NsiUtils.RESPONSE_CODE_NUMBER_ERROR.intValue())
        {
            NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.P_GUID, entity.getID());
            Assert.assertNotNull(DataAccessServices.dao().get(nsiEntity.getEntityId()));
        }
    }

    @DataProvider
    private Object[][] generateData()
    {
        return new Object[][]{
//                {ID, GradeID, GradeName,                                             result},
                {"cc5d937a-3202-3c23-8a11-gradeTypexxX", "122", "nsi_test",             NsiUtils.RESPONSE_CODE_NUMBER_SUCCESS.intValue()},
                {"", "", "",                                                            NsiUtils.RESPONSE_CODE_NUMBER_ERROR.intValue()},  // без id
                {"cc5d937a-3202-3c23-8a11-gradeTypexxX", "", "",                        NsiUtils.RESPONSE_CODE_NUMBER_SUCCESS.intValue()}, // с таким id уже есть
//                {"cc5d937a-3202-3c23-8a11-gradeType4", "", "",                        NsiUtils.RESPONSE_CODE_NUMBER_WARN.intValue()}, // а с таким нет, поэтому warning на пустой title
                {"cc5d937a-3202-3c23-8a11-gradeType2", "150", "nsi_test",               NsiUtils.RESPONSE_CODE_NUMBER_WARN.intValue()}, // с таким названием уже есть
                {"cc5d937a-3202-3c23-8a11-gradeType3", "150", "nsi_test",               NsiUtils.RESPONSE_CODE_NUMBER_WARN.intValue()}, // с таким названием уже есть, должен создаться новый
        };
    }

    /*
     Добавления списка элементов
     */
    @Test(groups = INSERT_GROUP, priority = 3)
    public void multipleInsertTest() throws MalformedURLException
    {
        List<IDatagramObject> datagramObjects = new ArrayList<>();
        datagramObjects.add(createGradeType("cc5d937a-3202-3c23-8a11-grade1", "122", "nsi_test"));
        datagramObjects.add(createGradeType("cc5d937a-3202-3c23-8a11-grade2", "122", "nsi_test"));
        datagramObjects.add(createGradeType("cc5d937a-3202-3c23-8a11-grade3", "123", "nsi_test"));
        datagramObjects.add(createGradeType("cc5d937a-3202-3c23-8a11-grade4", "122", "nsi_test_test"));
        datagramObjects.add(createGradeType("cc5d937a-3202-3c23-8a11-grade1", "122", "nsi_test_test"));
        ServiceRequestType request = getRequest(datagramObjects);
        ServiceResponseType responseType = getPort().insert(request);
        Assert.assertNotNull(responseType, "Response is null!");
        Assert.assertEquals(responseType.getRoutingHeader().getMessageId(), request.getRoutingHeader().getMessageId(), "Wrong response message id!");
        Assert.assertNotNull(responseType.getCallCC(), "Response response code is null!");
        Assert.assertNotEquals(responseType.getCallCC().intValue(), NsiUtils.RESPONSE_CODE_NUMBER_ERROR.intValue(), "Wrong response code!");
        List<IDatagramObject> retrievedDatagram = null;
        try {
            retrievedDatagram = NsiUtils.getDatagramElements(responseType.getDatagram());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(retrievedDatagram, "Datagram is null!");
    }

    //@Test(groups = RETRIEVE_GROUP, priority = 4)
    public void retrieveTest() throws MalformedURLException
    {
        GradeType entity = createGradeType(null, null, null);
        ServiceRequestType request = getRequest(entity);
        ServiceResponseType responseType = getPort().retrieve(request);
        Assert.assertEquals(responseType.getRoutingHeader().getMessageId(), request.getRoutingHeader().getMessageId());
        Assert.assertNotNull(responseType.getCallCC());

        List<IDatagramObject> retrievedDatagram = null;
        try {
            retrievedDatagram = NsiUtils.getDatagramElements(responseType.getDatagram());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(retrievedDatagram, "Datagram is null!");
    }
}
