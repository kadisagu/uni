package ru.tandemservice.unimsu2.base.ext.BpmAdmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmAdmin.BpmAdminManager;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;

@Configuration
public class BpmAdminExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private BpmAdminManager _bpmAdminManager;
}
