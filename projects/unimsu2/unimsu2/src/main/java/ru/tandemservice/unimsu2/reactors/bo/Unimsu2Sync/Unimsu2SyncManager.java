/* $Id:$ */
package ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync.logic.IUnimsu2SyncDAO;
import ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync.logic.Unimsu2SyncDAO;

/**
 * @author Andrey Nikonov
 * @since 19.05.2015
 */
@Configuration
public class Unimsu2SyncManager extends BusinessObjectManager
{
    public static Unimsu2SyncManager instance()
    {
        return instance(Unimsu2SyncManager.class);
    }

    @Bean
    public IUnimsu2SyncDAO dao()
    {
        return new Unimsu2SyncDAO();
    }
}
