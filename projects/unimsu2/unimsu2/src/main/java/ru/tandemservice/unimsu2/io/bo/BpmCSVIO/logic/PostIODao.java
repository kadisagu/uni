package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostIODao extends  org.tandemframework.bpms.io.bo.BpmIO.logic.PostIODao implements IPostIODao
{
    private final String CODE_COLUMN = "Код должности";
    private final String TITLE_COLUMN = "Наименование должности";

    private class TempPost {
        private String title;
        private String code;
        private EmployeeType type;

        public TempPost(String code, EmployeeType type, String title) {
            if(title == null)
                throw new ApplicationException("«" + TITLE_COLUMN + "» обязателен для заполнения");
            if(code == null)
                throw new ApplicationException("«" + CODE_COLUMN + "» обязателен для заполнения");
            if(type == null)
                throw new ApplicationException("Тип подразделения обязателен для заполнения");
            this.code = code;
            this.type = type;
            this.title = title;
        }
    }

    public Map<String, Post> importPostTable(final File csvFile, EmployeeType defaultEmployeeType) throws Exception
    {
        final String CACHE_KEY = "PostIODao.importPostTable";
        Map<String, Post> postIdMap = DaoCache.get(CACHE_KEY);
        if (null != postIdMap)
            return postIdMap;

        final Map<String, Post> createdPostMap = new HashMap<String, Post>();
        if (null == csvFile)
            return createdPostMap;

        postIdMap = new HashMap<String, Post>();

        BufferedReader rd = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "CP1251"));
        String row = rd.readLine();
        if(row == null)
            return createdPostMap;

        String[] cells = StringUtils.splitPreserveAllTokens(row, ';');
        int codeIndex = findColumnWithName(cells, CODE_COLUMN);
        if(codeIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + CODE_COLUMN + "»");
        int titleIndex = findColumnWithName(cells, TITLE_COLUMN);
        if(titleIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + TITLE_COLUMN + "»");

        final Map<String, Object> localPostMap = new HashMap<String, Object>();
        List<TempPost> units = new ArrayList<TempPost>();
        while( true )
        {
            row = rd.readLine();
            if(row == null)
                break;
            cells = StringUtils.splitPreserveAllTokens(row, ';');
            String code = cells[codeIndex];
            String title = cells[titleIndex];
            TempPost tempPost = new TempPost(code, defaultEmployeeType, title);
            localPostMap.put(code, code);
            units.add(tempPost);
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Post.class, "p")
                .column(DQLExpressions.property("p"))
                .where(DQLExpressions.in(DQLExpressions.property("p.code"), localPostMap.values()));
        for (final Post post : dql.createStatement(getSession()).<Post>list())
            localPostMap.put(post.getCode(), post);

        for(TempPost tempPost: units) {
            Object p = localPostMap.get(tempPost.code);
            if (!(p instanceof Post))
                localPostMap.put(tempPost.code, p = new Post());
            Post post = (Post) p;

            post.setCode(tempPost.code);
            post.setTitle(tempPost.title);
            post.setEmployeeType(tempPost.type);
            getSession().saveOrUpdate(post);

            postIdMap.put(tempPost.code, post);
        }

        getSession().flush();
        getSession().clear();

        return createdPostMap;
    }
}
