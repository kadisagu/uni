package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.apache.log4j.Logger;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.BpmCSVIOManager;

import java.io.File;

public class BpmIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.BpmIODao implements IBpmIODao
{
    public void importOrgUnitsFromClientCSVFile(byte[] bytes)
    {
        new ImportFromClient() {
            @Override
            public void doImport(File file) {
                try
                {
                    BpmCSVIOManager.instance().orgUnitIODao().importOrgStructureTable(file);
                }
                catch (ApplicationException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Logger.getLogger(BpmIODao.class).error("Error occured during getting import file, see exception info below. ", e);
                    throw new ApplicationException("Не удалось открыть файл для импорта, обратитесь к логу для получения информации об ошибке.");
                }
            }

            @Override
            public String getFileNameSuffix() {
                return ".csv";
            }
        }.importFromClientFile(bytes);
    }

    public void importPostsFromClientCSVFile(byte[] bytes, final EmployeeType defaultEmployeeType)
    {
        new ImportFromClient() {
            @Override
            public void doImport(File file) {
                try
                {
                    BpmCSVIOManager.instance().postIODao().importPostTable(file, defaultEmployeeType);
                }
                catch (ApplicationException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Logger.getLogger(BpmIODao.class).error("Error occured during getting import file, see exception info below. ", e);
                    throw new ApplicationException("Не удалось открыть файл для импорта, обратитесь к логу для получения информации об ошибке.");
                }
            }

            @Override
            public String getFileNameSuffix() {
                return ".csv";
            }
        }.importFromClientFile(bytes);
    }

    public void importEmployeePostsFromClientCSVFile(byte[] bytes, final Sex defaultSex, final AddressCountry defaultAddressCountry,
                                                     final IdentityCardType defaultCardType, final PostType defaultPostType,
                                                     final EmployeePostStatus defaultPostStatus, final QualificationLevel defaultQualificationLevel,
                                                     final ProfQualificationGroup defaultProfQualificationGroup, final AuthenticationType defaultAuthenticationType)
    {
        new ImportFromClient() {
            @Override
            public void doImport(File file) {
                try
                {
                    BpmCSVIOManager.instance().employeePostIODao().importEmployeePostTable(file, defaultSex, defaultAddressCountry,
                            defaultCardType, defaultPostType, defaultPostStatus, defaultQualificationLevel, defaultProfQualificationGroup, defaultAuthenticationType);
                }
                catch (ApplicationException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    Logger.getLogger(BpmIODao.class).error("Error occured during getting import file, see exception info below. ", e);
                    throw new ApplicationException("Не удалось открыть файл для импорта, обратитесь к логу для получения информации об ошибке.");
                }
            }

            @Override
            public String getFileNameSuffix() {
                return ".csv";
            }
        }.importFromClientFile(bytes);
    }
}