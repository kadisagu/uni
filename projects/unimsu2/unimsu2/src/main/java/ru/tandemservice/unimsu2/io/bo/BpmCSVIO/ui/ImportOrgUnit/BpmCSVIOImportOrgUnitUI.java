/* $Id:$ */
package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportOrgUnit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import ru.tandemservice.unimsu2.io.bo.BpmCSVIO.BpmCSVIOManager;

import java.io.IOException;

public class BpmCSVIOImportOrgUnitUI extends AbstractBpmUIPresenter
{
    private IUploadFile _uploadFile;

    public void onClickImportPostCSVFromClient() {
        try
        {
            BpmCSVIOManager.instance().dao().importOrgUnitsFromClientCSVFile(IOUtils.toByteArray(getUploadFile().getStream()));
            deactivate();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile _uploadFile)
    {
        this._uploadFile = _uploadFile;
    }
}
