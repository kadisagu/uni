/* $Id:$ */
package ru.tandemservice.unimsu2.base.bo.BpmUserSettings.ui.Login;

import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import org.tandemframework.bpms.util.BpmCommonUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.sec.entity.codes.AuthenticationTypeCodes;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unimsu2.base.bo.BpmUserSettings.ui.RequestPassword.BpmUserSettingsRequestPassword;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 9/24/12
 * Time: 6:34 PM
 */
public class BpmUserSettingsLoginUI extends AbstractBpmUIPresenter
{
    private Person _person;
    private Principal _principal;

    public Boolean getLdapAuthType()
    {
        if(null == _person)
        {
            _person = BpmCommonUtils.getCurrentPerson();
            Person2PrincipalRelation relation = DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.person().id(), _person.getId());
            _principal = relation.getPrincipal();
        }
        if(null != _principal.getAuthenticationType())
            return AuthenticationTypeCodes.LDAP.equals(_principal.getAuthenticationType().getCode());
        else
            return Boolean.FALSE;
    }

    public void onChange()
    {
        _uiActivation.asCurrent(BpmUserSettingsRequestPassword.class).activate();
    }
}
