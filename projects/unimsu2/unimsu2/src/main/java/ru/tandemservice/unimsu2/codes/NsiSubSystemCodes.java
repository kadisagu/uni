package ru.tandemservice.unimsu2.codes;

import java.util.*;

/**
 * Константы кодов сущности "Подсистемы НСИ"
 * Имя сущности : nsiSubSystem
 * Файл data.xml : nsi.catalog.data.xml
 */
public interface NsiSubSystemCodes
{
    /** Константа кода(code) элемента : НСИ(title) */
    String NSI = "nsi";
    /** Константа кода(code) элемента : Локальная система(title) */
    String LOCAL = "local";

    final static Set<String> CODES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(NSI, LOCAL)));
}