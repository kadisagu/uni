/* $Id:$ */
package ru.tandemservice.unimsu2.base.ext.BpmMain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmMain.BpmMainManager;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 9/24/12
 * Time: 2:05 PM
 */
@Configuration
public class BpmMainExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private BpmMainManager _bpmMainManager;

//    public static final Long USER_CAB_USER_SETTINGS = 11000L;
//
//    @Bean
//    public ItemListExtension<UserCabOptionVO> userCabOptionsExtPointExt()
//    {
//       return itemListExtension(_bpmMainManager.userCabOptionsExtPoint()).
//                add(USER_CAB_USER_SETTINGS.toString(), new UserCabOptionVO(USER_CAB_USER_SETTINGS, "Личные настройки", BpmUserSettingsLogin.class, null)).
//                create();
//    }
}
