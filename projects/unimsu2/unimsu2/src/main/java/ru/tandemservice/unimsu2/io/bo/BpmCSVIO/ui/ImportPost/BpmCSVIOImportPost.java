/* $Id:$ */
package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.ui.ImportPost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;

@Configuration
public class BpmCSVIOImportPost extends BusinessComponentManager
{
    public static final String EMPLOYEE_TYPE_DS = "employeeTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().
                addDataSource(selectDS(EMPLOYEE_TYPE_DS, employeeTypeComboDSHandler())).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler employeeTypeComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EmployeeType.class);
    }
}
