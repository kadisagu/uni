package ru.tandemservice.unimsu2.base.ext.BpmMain.ui.UserSettings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmMain.ui.UserSettings.BpmMainUserSettings;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.unimsu2.base.bo.BpmUserSettings.ui.Login.BpmUserSettingsLogin;

@Configuration
public class BpmMainUserSettingsExt extends BusinessComponentExtensionManager
{
    public static final String USER_SETTINGS_LOGIN_TAB = "userSettingsLoginTab";

    @Autowired
    public BpmMainUserSettings _bpmMainUserSettings;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_bpmMainUserSettings.tabPanelExtPoint())
                .addTab(componentTab(USER_SETTINGS_LOGIN_TAB, BpmUserSettingsLogin.class))
                .create();
    }
}
