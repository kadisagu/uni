package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.bpms.io.bo.BpmIO.BpmIOManager;
import org.tandemframework.bpms.io.bo.BpmIO.logic.ICatalogIODao;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.IOrgUnitDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class OrgUnitIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.OrgUnitIODao implements IOrgUnitIODao
{
    private static final String CODE_COLUMN = "Код подразделения";
    private static final String PARENT_COLUMN = "Код вышестоящего подразделения";
    private static final String TYPE_COLUMN = "Тип подразделения";
    private static final String TITLE_COLUMN = "Название подразделения";

    private class TempOrgUnit {
        private String title;
        private String code;
        private String terr;
        private OrgUnitType type;
        private String parentOrgUnitCode;

        public TempOrgUnit(String code, String parentOrgUnitCode, OrgUnitType type, String title, String terr) {
            if(title == null)
                throw new ApplicationException("«" + TITLE_COLUMN + "» обязателен для заполнения");
            if(code == null)
                throw new ApplicationException("«" + CODE_COLUMN + "» обязателен для заполнения");
            if(type == null)
                throw new ApplicationException("«" + TYPE_COLUMN + "» обязателен для заполнения");
            if(terr == null)
                throw new ApplicationException("Территориальный номер обязателен для заполнения");
            this.code = code;
            this.parentOrgUnitCode = parentOrgUnitCode;
            this.type = type;
            this.title = title;
            this.terr = terr;
        }
    }

    @Override
    public Map<String, OrgUnit> importOrgStructureTable(final File csvFile) throws Exception
    {
        final String CACHE_KEY = "OrgStructIODao.importOrgStructureTable";
        final Map<String, OrgUnit> personIdMap = DaoCache.get(CACHE_KEY);
        if (null != personIdMap)
            return personIdMap;

        final Map<String, OrgUnit> createdOrgUnitMap = new HashMap<>();
        if (null == csvFile)
            return createdOrgUnitMap;

        final ICatalogIODao catalogIO = BpmIOManager.instance().catalogIODao();
        final Map<String, OrgUnitType> orgUnitTypeMap = catalogIO.catalogOrgUnitType().lookup(true);

        NamedSyncInTransactionCheckLocker.register(getSession(), IOrgUnitDao.ORG_UNIT_HIERARCHY_LOCK);
        final List<CoreCollectionUtils.Pair<OrgUnit, String>> orgUnitIdparentIdList = new ArrayList<>();

        BufferedReader rd = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "CP1251"));
        String row = rd.readLine();
        if(row == null)
            return createdOrgUnitMap;

        String[] cells = StringUtils.splitPreserveAllTokens(row, ';');
        int codeIndex = findColumnWithName(cells, CODE_COLUMN);
        if(codeIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + CODE_COLUMN + "»");
        int parentIndex = findColumnWithName(cells, PARENT_COLUMN);
        if(parentIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + PARENT_COLUMN + "»");
        int typeIndex = findColumnWithName(cells, TYPE_COLUMN);
        if(typeIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + TYPE_COLUMN + "»");
        int titleIndex = findColumnWithName(cells, TITLE_COLUMN);
        if(titleIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + TITLE_COLUMN + "»");

        Map<String, Object> localOrgUnitMap = new HashMap<>();
        List<TempOrgUnit> units = new ArrayList<>();
        while( true )
        {
            row = rd.readLine();
            if(row == null)
                break;
            cells = StringUtils.splitPreserveAllTokens(row, ';');
            String code = cells[codeIndex];
            String parentOrgUnitCode = cells[parentIndex];
            OrgUnitType type = orgUnitTypeMap.get(cells[typeIndex]);
            String title = cells[titleIndex];
            TempOrgUnit tempOrgUnit = new TempOrgUnit(code, parentOrgUnitCode, type, title, "г.Москва");
            localOrgUnitMap.put(code, Long.parseLong(code, 16));
            units.add(tempOrgUnit);
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, "p")
                .column(property("p"))
                .where(in(property("p.id"), localOrgUnitMap.values()))
                ;
        for (final OrgUnit orgUnit : dql.createStatement(getSession()).<OrgUnit>list())
            localOrgUnitMap.put(Long.toHexString(orgUnit.getId()), orgUnit);

        for(TempOrgUnit tempOrgUnit: units) {
            Object p = localOrgUnitMap.get(tempOrgUnit.code);
            if (!(p instanceof OrgUnit))
                localOrgUnitMap.put(tempOrgUnit.code, p = new OrgUnit());
            OrgUnit orgUnit = (OrgUnit) p;

            orgUnit.setOrgUnitType(tempOrgUnit.type);
            OrgUnitManager.instance().dao().updateOrgUnitChangeParent(orgUnit, findParentOrgUnit(orgUnit, tempOrgUnit.parentOrgUnitCode, localOrgUnitMap, orgUnitIdparentIdList, createdOrgUnitMap));
            orgUnit.setTitle(tempOrgUnit.title);
            orgUnit.setShortTitle(tempOrgUnit.title);
            orgUnit.setFullTitle(tempOrgUnit.title);
            orgUnit.setTerritorialTitle(tempOrgUnit.terr);
            orgUnit.setTerritorialFullTitle(tempOrgUnit.terr);
            orgUnit.setTerritorialShortTitle(tempOrgUnit.terr);
            getSession().saveOrUpdate(orgUnit);
            createdOrgUnitMap.put(tempOrgUnit.code, orgUnit);
        }

        // Устанавливаем загруженные родительские подразделения
        for(CoreCollectionUtils.Pair<OrgUnit, String> pair: orgUnitIdparentIdList) {
            OrgUnit orgUnit = pair.getX();
            orgUnit.setParent(createdOrgUnitMap.get(pair.getY()));
            getSession().saveOrUpdate(orgUnit);
        }

        getSession().flush();
        getSession().clear();

        DaoCache.put(CACHE_KEY, createdOrgUnitMap);
        return createdOrgUnitMap;
    }

    @Override
    protected OrgUnit findParentOrgUnit(OrgUnit orgUnit, String parentId, Map<String, Object> localOrgUnitMap, List<CoreCollectionUtils.Pair<OrgUnit, String>> orgUnitIdparentIdList, Map<String, OrgUnit> orgUnitIdMap)
    {
        if(StringUtils.isEmpty(parentId) || parentId.equals("0"))
            return TopOrgUnit.getInstance();
        return super.findParentOrgUnit(orgUnit, parentId, localOrgUnitMap, orgUnitIdparentIdList, orgUnitIdMap);
    }
}
