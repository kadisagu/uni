/* $Id:$ */
package ru.tandemservice.unimsu2.reactors.bo.Unimsu2Sync.logic;

import org.apache.log4j.Level;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 19.05.2015
 */
@Transactional
public class Unimsu2SyncDAO extends BaseModifyAggregateDAO implements IUnimsu2SyncDAO
{
    @Override
    @Transactional(readOnly = true)
    public List<Long> getPrincipalIdsByPersonIds(Collection<Long> entityIds)
    {
        final List<Long> principals = new ArrayList<>(entityIds.size());

        BatchUtils.execute(entityIds, 200, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> entityIdsPortion)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Person2PrincipalRelation.class, "e")
                        .column(property("e", Person2PrincipalRelation.principal().id()))
                        .where(in(property("e", Person2PrincipalRelation.person().id()), entityIdsPortion));
                principals.addAll(createStatement(builder).<Long>list());
            }
        });
        return principals;
    }

    @Override
    public void batchDeleteEntities(final Class<? extends IEntity> entityClass, Collection<Long> entityIds)
    {
        BatchUtils.execute(entityIds, 200, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> entityIdsPortion)
            {
                new DQLDeleteBuilder(entityClass)
                        //.where(new DQLCanDeleteExpressionBuilder(entityClass, "id").getExpression())
                        .where(in(property(IEntity.P_ID), entityIdsPortion))
                        .createStatement(getSession()).execute();
            }
        });
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isPropertyUnique(Class<? extends IEntity> entityClass, String property, Long id, String title)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(entityClass, "e")
                .column(DQLFunctions.count(property("e")))
                .where(eq(property("e", property), value(title)));
        if (id != null)
            builder.where(ne(property("e", IEntity.P_ID), value(id)));
        Number cnt = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        return cnt == null || cnt.intValue() == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public String findUniqueProperty(Class<? extends IEntity> entityClass, String property, Long id, String prefix)
    {
        if(isPropertyUnique(entityClass, property, id, prefix))
            return prefix;
        int count = DataAccessServices.dao().getCount(entityClass);
        int i = count;
        while(!isPropertyUnique(entityClass, property, id, prefix + i))
            i++;
        NsiRequestHandlerService.instance().getLogger().log(Level.INFO, (i - count + 1) + " count for search title with prefix " + prefix + " for type " + entityClass.getSimpleName());
        return prefix + i;
    }
}
