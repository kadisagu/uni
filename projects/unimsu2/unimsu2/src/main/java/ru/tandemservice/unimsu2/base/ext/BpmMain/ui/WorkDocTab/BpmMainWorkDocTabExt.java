/* $Id:$ */
package ru.tandemservice.unimsu2.base.ext.BpmMain.ui.WorkDocTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmMain.ui.WorkDocTab.BpmMainWorkDocTab;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 12/21/12
 * Time: 7:00 PM
 */
@Configuration
public class BpmMainWorkDocTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private BpmMainWorkDocTab _bpmMainWorkDocTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_bpmMainWorkDocTab.presenterExtPoint()).
                addAction(new BpmMSUSearchAction("onApply")).
                create();
    }
}
