package ru.tandemservice.unimsu2.reports.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип отчета"
 * Имя сущности : bpmReportType
 * Файл data.xml : bpm.reports.data.xml
 */
public interface BpmReportTypeCodes
{
    /** Константа кода (code) элемента : bpmRegIncomingListReport (code). Название (title) : Журнал регистрации входящих документов */
    String BPM_REG_INCOMING_LIST_REPORT = "bpmRegIncomingListReport";
    /** Константа кода (code) элемента : bpmRegOutgoingListReport (code). Название (title) : Журнал регистрации исходящих документов */
    String BPM_REG_OUTGOING_LIST_REPORT = "bpmRegOutgoingListReport";
    /** Константа кода (code) элемента : bpmRegOrganizationalListReport (code). Название (title) : Журнал регистрации организационных документов */
    String BPM_REG_ORGANIZATIONAL_LIST_REPORT = "bpmRegOrganizationalListReport";
    /** Константа кода (code) элемента : bpmRegInternalListReport (code). Название (title) : Журнал регистрации информационно-справочных документов */
    String BPM_REG_INTERNAL_LIST_REPORT = "bpmRegInternalListReport";
    /** Константа кода (code) элемента : bpmRegOrderListReport (code). Название (title) : Журнал регистрации распорядительных документов */
    String BPM_REG_ORDER_LIST_REPORT = "bpmRegOrderListReport";
    /** Константа кода (code) элемента : bpmSummaryDocFlowReport (code). Название (title) : Сводка по документообороту */
    String BPM_SUMMARY_DOC_FLOW_REPORT = "bpmSummaryDocFlowReport";
    /** Константа кода (code) элемента : bpmAnalysisPerformanceDisciplineReport (code). Название (title) : Анализ по исполнению поручений */
    String BPM_ANALYSIS_PERFORMANCE_DISCIPLINE_REPORT = "bpmAnalysisPerformanceDisciplineReport";
    /** Константа кода (code) элемента : bpmPerformanceDisciplineReport (code). Название (title) : Отчёт по исполнению поручений */
    String BPM_PERFORMANCE_DISCIPLINE_REPORT = "bpmPerformanceDisciplineReport";
    /** Константа кода (code) элемента : bpmTasksReport (code). Название (title) : Отчёт по исполнению задач */
    String BPM_TASKS_REPORT = "bpmTasksReport";
    /** Константа кода (code) элемента : bpmRegAdministrativeListReport (code). Название (title) : Журнал регистрации распоряжений */
    String BPM_REG_ADMINISTRATIVE_LIST_REPORT = "bpmRegAdministrativeListReport";
    /** Константа кода (code) элемента : bpmRegOrderMguListReport (code). Название (title) : Журнал регистрации приказов */
    String BPM_REG_ORDER_MGU_LIST_REPORT = "bpmRegOrderMguListReport";

    Set<String> CODES = ImmutableSet.of(BPM_REG_INCOMING_LIST_REPORT, BPM_REG_OUTGOING_LIST_REPORT, BPM_REG_ORGANIZATIONAL_LIST_REPORT, BPM_REG_INTERNAL_LIST_REPORT, BPM_REG_ORDER_LIST_REPORT, BPM_SUMMARY_DOC_FLOW_REPORT, BPM_ANALYSIS_PERFORMANCE_DISCIPLINE_REPORT, BPM_PERFORMANCE_DISCIPLINE_REPORT, BPM_TASKS_REPORT, BPM_REG_ADMINISTRATIVE_LIST_REPORT, BPM_REG_ORDER_MGU_LIST_REPORT);
}
