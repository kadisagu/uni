package ru.tandemservice.unimsu2.reports.ext.BpmReportBase.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.reports.bo.BpmReportBase.ui.List.BpmReportBaseList;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;

@Configuration
public class BpmReportBaseListExt extends BusinessComponentExtensionManager
{
    @Autowired
    public BpmReportBaseList _bpmReportBaseList;
}
