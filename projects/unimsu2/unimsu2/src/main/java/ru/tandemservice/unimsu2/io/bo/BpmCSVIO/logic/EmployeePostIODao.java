package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

public class EmployeePostIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.EmployeePostIODao implements IEmployeePostIODao
{
    private final String LOGIN_COLUMN = "Логин в LDAP";
    private final String CODE_COLUMN = "Табельный номер";
    private final String LAST_NAME_COLUMN = "Фамилия";
    private final String FIRST_NAME_COLUMN = "Имя";
    private final String MIDDLE_NAME_COLUMN = "Отчество";
    private final String EMAIL_COLUMN = "Адрес электронной почты";
    private final String DEPARTMENT_COLUMN = "Код подразделения";
    private final String POST_COLUMN = "Код должности";

    private class TempEmployeePost {
        private String login;
        private String code;
        private String email;
        private String firstName;
        private String lastName;
        private String middleName;
        private String orgUnitCode;
        private String postCode;
        private Sex sex;
        private AddressCountry addressCountry;
        private IdentityCardType cardType;
        private PostType postType;
        private EmployeePostStatus postStatus;

        public TempEmployeePost(String login, String code, String email, String firstName, String lastName, String middleName,
                                String orgUnitCode, String postCode, Sex sex, AddressCountry addressCountry, IdentityCardType cardType,
                                PostType postType, EmployeePostStatus postStatus) {
            if(login == null)
                throw new ApplicationException("«" + LOGIN_COLUMN + "» обязателен для заполнения");
            if(firstName == null)
                throw new ApplicationException("«" + FIRST_NAME_COLUMN + "» обязателен для заполнения");
            if(lastName == null)
                throw new ApplicationException("«" + LAST_NAME_COLUMN + "» обязателен для заполнения");
            if(orgUnitCode == null)
                throw new ApplicationException("«" + DEPARTMENT_COLUMN + "» обязателен для заполнения");
            if(postCode == null)
                throw new ApplicationException("«" + POST_COLUMN + "» обязателен для заполнения");
            this.login = login;
            this.code = code;
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
            this.middleName = middleName;
            this.orgUnitCode = orgUnitCode;
            this.postCode = postCode;
            this.sex = sex;
            this.addressCountry = addressCountry;
            this.cardType = cardType;
            this.postType = postType;
            this.postStatus = postStatus;
        }
    }

    @Override
    public Map<String, EmployeePost> importEmployeePostTable(File csvFile, Sex defaultSex, AddressCountry defaultAddressCountry,
                                                             IdentityCardType defaultCardType, PostType defaultPostType,
                                                             EmployeePostStatus defaultPostStatus, QualificationLevel defaultQualificationLevel,
                                                             ProfQualificationGroup defaultProfQualificationGroup, AuthenticationType defaultAuthenticationType) throws Exception
    {
        final String CACHE_KEY = "EmployeePostIODao.importEmployeePostList";
        final Map<String, EmployeePost> personIdMap = DaoCache.get(CACHE_KEY);
        if (null != personIdMap)
            return personIdMap;

        final Map<String, EmployeePost> createdEmployeePostMap = new HashMap<String, EmployeePost>();
        if (null == csvFile)
            return createdEmployeePostMap;

        BufferedReader rd = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "CP1251"));
        String row = rd.readLine();
        if(row == null)
            return createdEmployeePostMap;

        String[] cells = StringUtils.splitPreserveAllTokens(row, ';');
        int codeIndex = findColumnWithName(cells, CODE_COLUMN);
        if(codeIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + CODE_COLUMN + "»");
        int loginIndex = findColumnWithName(cells, LOGIN_COLUMN);
        if(loginIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + LOGIN_COLUMN + "»");
        int lastNameIndex = findColumnWithName(cells, LAST_NAME_COLUMN);
        if(lastNameIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + LAST_NAME_COLUMN + "»");
        int firstNameIndex = findColumnWithName(cells, FIRST_NAME_COLUMN);
        if(firstNameIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + FIRST_NAME_COLUMN + "»");
        int middleNameIndex = findColumnWithName(cells, MIDDLE_NAME_COLUMN);
        if(middleNameIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + MIDDLE_NAME_COLUMN + "»");
        int emailIndex = findColumnWithName(cells, EMAIL_COLUMN);
        if(emailIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + EMAIL_COLUMN + "»");
        int orgUnitIndex = findColumnWithName(cells, DEPARTMENT_COLUMN);
        if(orgUnitIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + DEPARTMENT_COLUMN + "»");
        int postIndex = findColumnWithName(cells, POST_COLUMN);
        if(postIndex == -1)
            throw new ApplicationException("Не найдена колонка «" + POST_COLUMN + "»");

        Map<String, Object> localEmployeePostMap = new HashMap<String, Object>();
        List<TempEmployeePost> units = new ArrayList<TempEmployeePost>();
        while( true )
        {
            row = rd.readLine();
            if(row == null)
                break;
            cells = StringUtils.splitPreserveAllTokens(row, ';');
            String code = cells[codeIndex];
            String login = cells[loginIndex];
            String email = cells[emailIndex];
            String firstName = cells[firstNameIndex];
            String lastName = cells[lastNameIndex];
            String middleName = cells[middleNameIndex];
            String orgUnitCode = cells[orgUnitIndex];
            String postCode = cells[postIndex];
            TempEmployeePost tempEmployeePost = new TempEmployeePost(login, code, email, firstName, lastName, middleName,
                    orgUnitCode, postCode, defaultSex, defaultAddressCountry, defaultCardType,
                    defaultPostType, defaultPostStatus);
            localEmployeePostMap.put(code, code);
            units.add(tempEmployeePost);
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EmployeePost.class, "p")
                .column(DQLExpressions.property("p"))
                .fetchPath(DQLJoinType.left, EmployeePost.employee().fromAlias("p"), "employee")
                .fetchPath(DQLJoinType.left, EmployeePost.employee().principal().fromAlias("p"), "principal")
                .fetchPath(DQLJoinType.left, EmployeePost.person().fromAlias("p"), "person")
                .fetchPath(DQLJoinType.left, EmployeePost.person().identityCard().fromAlias("p"), "identityCard")
                .fetchPath(DQLJoinType.left, EmployeePost.person().address().fromAlias("p"), "address")
                .fetchPath(DQLJoinType.left, EmployeePost.postRelation().fromAlias("p"), "postRelation")
                .where(DQLExpressions.in(DQLExpressions.property("p.employee.employeeCode"), localEmployeePostMap.values()));

        for (final EmployeePost employeePost : dql.createStatement(getSession()).<EmployeePost>list())
            localEmployeePostMap.put(employeePost.getEmployee().getEmployeeCode(), employeePost);

        for(TempEmployeePost tempEmployeePost: units) {
            Object p = localEmployeePostMap.get(tempEmployeePost.code);
            if (!(p instanceof EmployeePost))
                localEmployeePostMap.put(tempEmployeePost.code, p = new EmployeePost());

            EmployeePost employeePost = (EmployeePost) p;

            Employee employee = employeePost.getEmployee();
            if (employee == null)
            {
                employee = new Employee();
                employee.setPrincipal(new Principal());
                employee.setPerson(new Person());
            }
            employee.setEmployeeCode(tempEmployeePost.code);

            Person person = employeePost.getPerson();
            Principal principal;
            boolean isCreatedPerson = false;
            if (person == null)
            {
                person = new Person();
                person.setIdentityCard(new IdentityCard());
                isCreatedPerson = true;
            }

            if (isCreatedPerson)
                principal = new Principal();
            else
                principal = employee.getPrincipal();
            principal.setAuthenticationType(defaultAuthenticationType);
            String login = tempEmployeePost.login;
            if (!login.equals(principal.getLogin()))
            {
                Principal tempPricipal = get(Principal.class, Principal.P_LOGIN, login);
                if (tempPricipal != null && !tempPricipal.equals(principal))
                    throw new ApplicationException("Логин «" + login + "» уже занят. Пожалуйста, введите другой логин.");

                principal.setLogin(login);
                principal.assignNewPassword(login);
                getSession().saveOrUpdate(principal);
            }

            PersonContactData contactData = person.getContactData();
            if (contactData == null) {
                person.setContactData(contactData = new PersonContactData());
            }

            contactData.setEmail(tempEmployeePost.email);

            getSession().saveOrUpdate(contactData);

            IdentityCard identityCard = person.getIdentityCard();
            if (identityCard == null)
                identityCard = new IdentityCard();
            identityCard.setCardType(tempEmployeePost.cardType);
            identityCard.setSex(tempEmployeePost.sex);
            identityCard.setFirstName(tempEmployeePost.firstName);
            identityCard.setLastName(tempEmployeePost.lastName);
            identityCard.setMiddleName(tempEmployeePost.middleName);
            identityCard.setCitizenship(tempEmployeePost.addressCountry);

            getSession().saveOrUpdate(identityCard);
            person.setIdentityCard(identityCard);

            getSession().saveOrUpdate(person);

            identityCard.setPerson(person);
            getSession().update(identityCard);

            if (isCreatedPerson)
                createPerson2PrincipalRelation(principal, person);

            employee.setPrincipal(principal);
            employee.setPerson(person);
            getSession().saveOrUpdate(employee);

            employeePost.setEmployee(employee);

            PostBoundedWithQGandQL postBoundedWithQGandQL = findPostBoundedWithQGandQL(tempEmployeePost.postCode, defaultQualificationLevel, defaultProfQualificationGroup);
            OrgUnit orgUnit = findOrgUnit(tempEmployeePost.orgUnitCode, new HashMap<String, OrgUnit>());

            OrgUnitTypePostRelation relation = employeePost.getPostRelation();
            if (relation == null)
            {
                final List<OrgUnitTypePostRelation> ids = new DQLSelectBuilder().fromEntity(OrgUnitTypePostRelation.class, "e").column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property("e", OrgUnitTypePostRelation.orgUnitType()), DQLExpressions.value(orgUnit.getOrgUnitType())))
                        .where(DQLExpressions.eq(DQLExpressions.property("e", OrgUnitTypePostRelation.postBoundedWithQGandQL()), DQLExpressions.value(postBoundedWithQGandQL)))
                        .createStatement(getSession()).list();
                if (ids != null && ids.size() > 0)
                    relation = ids.get(0);
                else
                {
                    relation = new OrgUnitTypePostRelation();
                    relation.setOrgUnitType(orgUnit.getOrgUnitType());
                    relation.setPostBoundedWithQGandQL(postBoundedWithQGandQL);
                }
            }
            else
            {
                relation.setOrgUnitType(orgUnit.getOrgUnitType());
                relation.setPostBoundedWithQGandQL(postBoundedWithQGandQL);
            }

            relation.setMultiPost(true);
            getSession().saveOrUpdate(relation);

            employeePost.setOrgUnit(orgUnit);
            employeePost.setPostRelation(relation);
            employeePost.setPostType(tempEmployeePost.postType);
            employeePost.setPostStatus(tempEmployeePost.postStatus);
            employeePost.setMainJob(true);
            employeePost.setPostDate(new Date());
            getSession().saveOrUpdate(employeePost);

            createdEmployeePostMap.put(login, employeePost);
        }

        getSession().flush();
        getSession().clear();

        return createdEmployeePostMap;
    }

    protected PostBoundedWithQGandQL findPostBoundedWithQGandQL(String postCode, QualificationLevel defaultQualificationLevel, ProfQualificationGroup defaultProfQualificationGroup)
    {
        Post post = get(Post.class, Post.code(), postCode);
        if (post == null)
            throw new ApplicationException("Должность с кодом «" + postCode + "» не существует в базе данных.");
        PostBoundedWithQGandQL postBoundedWithQGandQL;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PostBoundedWithQGandQL.class, "p")
                .column(DQLExpressions.property("p"))
                .where(DQLExpressions.eqValue(DQLExpressions.property("p.post.id"), post.getId()));
        List<PostBoundedWithQGandQL> postBoundedWithQGandQLs = dql.createStatement(getSession()).list();
        if(postBoundedWithQGandQLs.size() == 0)
        {
            postBoundedWithQGandQL = new PostBoundedWithQGandQL();

            postBoundedWithQGandQL.setCode(postCode);
            postBoundedWithQGandQL.setTitle(post.getTitle());
            postBoundedWithQGandQL.setQualificationLevel(defaultQualificationLevel);
            postBoundedWithQGandQL.setProfQualificationGroup(defaultProfQualificationGroup);
            postBoundedWithQGandQL.setPost(post);
            getSession().saveOrUpdate(postBoundedWithQGandQL);
        }
        else
            postBoundedWithQGandQL = postBoundedWithQGandQLs.get(0);

        return postBoundedWithQGandQL;
    }
}
