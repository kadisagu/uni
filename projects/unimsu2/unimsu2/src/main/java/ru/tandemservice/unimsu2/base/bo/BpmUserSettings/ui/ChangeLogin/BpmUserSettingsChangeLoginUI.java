/* $Id:$ */
package ru.tandemservice.unimsu2.base.bo.BpmUserSettings.ui.ChangeLogin;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.bpms.base.bo.BpmMain.support.AbstractBpmUIPresenter;
import org.tandemframework.bpms.util.BpmCommonUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;

public class BpmUserSettingsChangeLoginUI extends AbstractBpmUIPresenter
{
    private String _login;
    private String _password;
    private String _validatePassword;

    private Person _person;
    private Principal _principal;

    public String getPassword()
    {
        return _password;
    }

    public void setPassword(String password)
    {
        _password = password;
    }

    public String getValidatePassword()
    {
        return _validatePassword;
    }

    public void setValidatePassword(String validatePassword)
    {
        _validatePassword = validatePassword;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _person)
        {
            _person = BpmCommonUtils.getCurrentPerson();
            Person2PrincipalRelation relation = DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.person().id(), _person.getId());
            _principal = relation.getPrincipal();
            _login = _principal.getLogin();
        }
    }

    public void onSave()
    {
        if(!StringUtils.isEmpty(_password))
            if(!_password.equals(_validatePassword))
                throw new ApplicationException("Подтверждение пароля не совпадает с паролем");
            else
                PersonManager.instance().dao().updatePersonLogin(_person.getId(), _login, _password);
        else
            PersonManager.instance().dao().updatePersonLogin(_person.getId(), _login, "");
        deactivate();
    }
}
