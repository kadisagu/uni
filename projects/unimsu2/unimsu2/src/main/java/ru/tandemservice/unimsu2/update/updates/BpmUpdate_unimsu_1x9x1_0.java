package ru.tandemservice.unimsu2.update.updates;

import org.tandemframework.bpms.document.entity.BpmDocBase;
import org.tandemframework.bpms.document.entity.BpmDocRegNumberReserved;
import org.tandemframework.bpms.repo.entity.RepoNode;
import org.tandemframework.bpms.task.entity.BpmCaseTask;
import org.tandemframework.bpms.update.support.BpmUpdateBase;
import org.tandemframework.bpms.update.support.BpmUpdateException;
import org.tandemframework.hibsupport.DataAccessServices;

import java.util.Calendar;
import java.util.Date;

/**
 * SED-1022 - удаляем лишний процесс для документа, создаем резерв для потерянного номера
 * todo Удалить после выполнения
 */
public class BpmUpdate_unimsu_1x9x1_0 extends BpmUpdateBase
{
    @Override
    public void onUpdate() throws BpmUpdateException
    {
//        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(BpmCaseTask.class, "p")
//                .column(property("p", BpmCaseTask.node().id()))
//                .fetchPath(DQLJoinType.inner, BpmCaseTask.node().fromAlias("p"), "node")
//                .where(isNull(property("p", BpmCaseTask.parent())))
//                .where(eq(property("p", BpmCaseTask.kind().code()), value(BpmCaseTaskKindCodes.DOC_CASE)))
//                .group(property("p", BpmCaseTask.node().id()))
//                .having(gt(DQLFunctions.count(property("p", BpmCaseTask.node().id())), value(1)));
//        List<Long> nodes = createStatement(builder).list();
//        for(Long nodeId: nodes) {
//            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(BpmCaseTask.class, "p").
//                    column(property("p", BpmCaseTask.id())).
//                    where(eq(property("p", BpmCaseTask.node().id()), value(nodeId))).
//                    where(isNull(property("p", BpmCaseTask.parent()))).
//                    where(eq(property("p", BpmCaseTask.kind().code()), value(BpmCaseTaskKindCodes.DOC_CASE)));
//            List<Long> roots = createStatement(b).list();
//            for(Long root: roots)
//                DataAccessServices.dao().delete(root);
//        }

        // 1473152277259692212 788863            № «682-14/010-ОСН» от «24.07.2014» -----------------
        // 1507379966081181876 1416511           № «1845-15/010-АС» от «30.07.2015»  «1836-15/010-АС» от «29.07.2015»
        // 1475139729823312052 831070            № «2311-14/010-АС» от «01.08.2014» -----------------

        RepoNode node = DataAccessServices.dao().get(RepoNode.class, RepoNode.id(), 1507379966081181876L);
        if(node != null) {
            BpmDocBase doc = node.getNodeDoc();
            Calendar calendar = Calendar.getInstance();
            calendar.set(2015, 6, 29);

            BpmDocRegNumberReserved reserv = new BpmDocRegNumberReserved();
            reserv.setRegDate(calendar.getTime());
            reserv.setRegNumber(1836);
            reserv.setDesc("Восстановлен, был использован для документа 1507379966081181876");
            reserv.setUpdateDate(new Date());
            reserv.setRepoNodeKind(node.getKind());
            reserv.setRegOrgUnit(doc.getRegOrgUnit());
            reserv.setNomenclatureCase(doc.getNomenclatureCase());
            reserv.setDocument(doc);
            reserv.setCounter(doc.getRegCounter());
            baseCreate(reserv);

            BpmCaseTask task = DataAccessServices.dao().get(BpmCaseTask.class, BpmCaseTask.serviceNumber().s(), 788863);
            if(task != null)
                DataAccessServices.dao().delete(task.getParent());

            task = DataAccessServices.dao().get(BpmCaseTask.class, BpmCaseTask.serviceNumber().s(), 1416511);
            if(task != null)
                DataAccessServices.dao().delete(task.getParent());

            task = DataAccessServices.dao().get(BpmCaseTask.class, BpmCaseTask.serviceNumber().s(), 831070);
            if(task != null)
                DataAccessServices.dao().delete(task.getParent());
        }
    }

    @Override
    public boolean isPerformUpdate()
    {
        return true;
    }
}