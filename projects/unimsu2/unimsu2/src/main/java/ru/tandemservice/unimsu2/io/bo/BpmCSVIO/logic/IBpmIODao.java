package ru.tandemservice.unimsu2.io.bo.BpmCSVIO.logic;

import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;

public interface IBpmIODao extends org.tandemframework.bpms.io.bo.BpmIO.logic.IBpmIODao
{
    public void importOrgUnitsFromClientCSVFile(byte[] bytes);
    public void importPostsFromClientCSVFile(byte[] bytes, final EmployeeType defaultEmployeeType);
    public void importEmployeePostsFromClientCSVFile(byte[] bytes, final Sex defaultSex, final AddressCountry defaultAddressCountry,
                                                     final IdentityCardType defaultCardType, final PostType defaultPostType,
                                                     final EmployeePostStatus defaultPostStatus, final QualificationLevel defaultQualificationLevel,
                                                     final ProfQualificationGroup defaultProfQualificationGroup, final AuthenticationType defaultAuthenticationType);
}
