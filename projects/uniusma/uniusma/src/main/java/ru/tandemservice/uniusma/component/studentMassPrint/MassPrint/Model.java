/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.MassPrint;

import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class Model extends ru.tandemservice.uniusma.component.studentMassPrint.Base.Model
{
    /**
     * шаблоны документов для массовой печати
     */
    private ISelectModel studentTemplateDocumentListModel;

    private OrgUnitSecModel _secModel;

    public void setStudentTemplateDocumentListModel(ISelectModel studentTemplateDocumentListModel)
    {
        this.studentTemplateDocumentListModel = studentTemplateDocumentListModel;
    }

    public ISelectModel getStudentTemplateDocumentListModel()
    {
        return studentTemplateDocumentListModel;
    }

    public void setSecModel(OrgUnitSecModel secModel)
    {
        _secModel = secModel;
    }

    public String getViewPermissionKey()
    {
        return getOrgUnitId() == null ? "menuStudentMassPrintDocuments" : _secModel.getPermission("orgUnit_usmaViewMassPrintTabPermissionKey");
    }

    public boolean isNotOrgUnitPage()
    {
        return getOrgUnitId() == null;
    }
}
