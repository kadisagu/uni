/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.Employee.ui.PostPrintReferenceForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostPrintReferenceForm.EmployeePostPrintReferenceForm;

/**
 * @author Alexander Shaburov
 * @since 16.07.12
 */
@Configuration
public class EmployeePostPrintReferenceFormExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniusma" + EmployeePostPrintReferenceFormExtUI.class.getSimpleName();

    @Autowired
    private EmployeePostPrintReferenceForm _employeePostPrintReferenceForm;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostPrintReferenceForm.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostPrintReferenceFormExtUI.class))
                .create();
    }
}
