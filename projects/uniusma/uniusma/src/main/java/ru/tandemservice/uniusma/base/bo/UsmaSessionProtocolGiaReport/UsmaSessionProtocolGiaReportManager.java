/*$Id:$*/
package ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.logic.ISessionProtocolGiaReportDao;
import ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.logic.SessionProtocolGiaReportDao;

/**
 * @author DMITRY KNYAZEV
 * @since 17.03.2016
 */
@Configuration
public class UsmaSessionProtocolGiaReportManager extends BusinessObjectManager
{

    public static UsmaSessionProtocolGiaReportManager instance()
    {
        return instance(UsmaSessionProtocolGiaReportManager.class);
    }

    @Bean
    public ISessionProtocolGiaReportDao dao()
    {
        return new SessionProtocolGiaReportDao();
    }
}
