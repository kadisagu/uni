/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public class Controller extends ModularStudentExtractPubController<UsmaExcludeEduPlanStuExtract, IDAO, Model>
{
}
