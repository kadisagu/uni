/*$Id$*/
package ru.tandemservice.uniusma.events.eduplan;

import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaChangedWorkPlan;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 16.10.2014
 */
public class UsmaEpvRowChangeListenerBean implements IDSetEventListener
{
	public void init()
	{
		DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppWorkPlanRow.class, this);
		DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, EppWorkPlanRow.class, this);
		DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EppWorkPlanRow.class, this);
	}

	@Override
	public void onEvent(DSetEvent event)
	{
		if (event.getEventType().isInsert()) onInsertEvent(event);
		if (event.getEventType().isUpdate() || event.getEventType().isDelete()) onDeleteOrUpdateEvent(event);
	}

	private void onInsertEvent(DSetEvent event)
	{
		DQLExecutionContext context = event.getContext();
		if (event.getMultitude().isSingular())
		{
			EppWorkPlanRow workPlanRow = (EppWorkPlanRow) event.getMultitude().getSingularEntity();
			if (workPlanRow.getWorkPlan() instanceof EppWorkPlan)
			{
				EppWorkPlan workPlan = (EppWorkPlan) workPlanRow.getWorkPlan();
                UsmaChangedWorkPlan changedWorkPlan = getWorkPlanChangeInfo(workPlan.getId(), context);
				if (changedWorkPlan == null || changedWorkPlan.isChanged()) return;
				setWorkPlanChanged(workPlanRow.getWorkPlan().getId(), context);
			}
		}
	}

	private void onDeleteOrUpdateEvent(DSetEvent event)
	{
		DQLExecutionContext context = event.getContext();

		List<EppWorkPlanRow> workPlanRowList = new ArrayList<>();
		List<Long> versionBlockIdList = new ArrayList<>();

		//alias
		String wpr = "wpr";
		String wp = "wp";
		List<Object[]> res = new DQLSelectBuilder().fromEntity(EppWorkPlanRow.class, wpr)
				.column(wpr)
				.column(property(wp, EppWorkPlan.parent().id()))
				.where(in(property(wpr, EppWorkPlanRow.P_ID), getEntityId(event)))
				.joinEntity(wpr, DQLJoinType.inner, EppWorkPlan.class, wp, eq(property(wpr, EppWorkPlanRow.L_WORK_PLAN), property(wp, EppWorkPlan.P_ID)))
				.createStatement(context).list();

		for (Object[] item : res)
		{
			EppWorkPlanRow workPlanRow = (EppWorkPlanRow) item[0];
			workPlanRowList.add(workPlanRow);
			if (event.getEventType().isDelete())
			{
				Long versionBlockId = (Long) item[1];
				versionBlockIdList.add(versionBlockId);
			}
		}

		if (!versionBlockIdList.isEmpty())
			workPlanRowList = deleteNotGroupImRow(workPlanRowList, versionBlockIdList, context);


		for (EppWorkPlanRow workPlanRow : workPlanRowList)
		{
			EppWorkPlan workPlan = (EppWorkPlan) workPlanRow.getWorkPlan();

            UsmaChangedWorkPlan changedWorkPlan = getWorkPlanChangeInfo(workPlan.getId(), context);
			if (changedWorkPlan == null || changedWorkPlan.isChanged()) return;
			setWorkPlanChanged(workPlan.getId(), context);
		}
	}

	private void setWorkPlanChanged(Long workPlanId, DQLExecutionContext context)
	{
		new DQLUpdateBuilder(UsmaChangedWorkPlan.class)
				.set(UsmaChangedWorkPlan.P_CHANGED, value(true))
				.where(eq(property(UsmaChangedWorkPlan.L_EPP_WORK_PLAN), value(workPlanId)))
				.createStatement(context)
				.execute();
	}

	private UsmaChangedWorkPlan getWorkPlanChangeInfo(Long workPlanId, DQLExecutionContext context)
	{
		return new DQLSelectBuilder().fromEntity(UsmaChangedWorkPlan.class, "cp")
				.column("cp")
				.where(eq(property("cp", UsmaChangedWorkPlan.L_EPP_WORK_PLAN), value(workPlanId)))
				.createStatement(context).uniqueResult();
	}

	private List<EppWorkPlanRow> deleteNotGroupImRow(Collection<EppWorkPlanRow> workPlanRows, Collection<Long> versionBlockIdList, DQLExecutionContext context)
	{
		String eer = "eer";
		DQLSelectBuilder epvRowPlanBuilder = new DQLSelectBuilder().fromEntity(EppEpvGroupImRow.class, eer)
				.column(property(eer, EppEpvGroupImRow.storedIndex()))
				.column(property(eer, EppEpvGroupImRow.owner().id()))
				.where(in(property(eer, EppEpvGroupImRow.L_OWNER), versionBlockIdList))
				.order(property(eer, EppEpvGroupImRow.P_STORED_INDEX));
		List<Object[]> eppEpvList = epvRowPlanBuilder.createStatement(context).list();

		Map<Long, List<String>> epvRowPlanMap = new HashMap<>();
		for (Object[] item : eppEpvList)
		{
			String index = (String) item[0];
			Long versionBlockId = (Long) item[1];
			List<String> list = epvRowPlanMap.get(versionBlockId);
			if (list == null) list = new ArrayList<>();
			list.add(index);
			epvRowPlanMap.put(versionBlockId, list);
		}

		List<EppWorkPlanRow> cleanEppWorkPlanRowList = new ArrayList<>();
		for (EppWorkPlanRow row : workPlanRows)
		{
			String title = row.getNumber();
			EppWorkPlan workPlanRow = (EppWorkPlan) row.getWorkPlan();
			List<String> indexList = epvRowPlanMap.get(workPlanRow.getParent().getId());
			if (indexList != null)
			{
				boolean isEpvGroupImRow = false;
				for (String index : indexList)
				{
					if (title.startsWith(index))
					{
						isEpvGroupImRow = true;
						break;
					}
				}
				if (!isEpvGroupImRow) cleanEppWorkPlanRowList.add(row);
			}
		}
		return cleanEppWorkPlanRowList;
	}


	public IDQLSelectableQuery getEntityId(final DSetEvent event)
	{
		return new DQLSelectBuilder()
				.fromDataSource(event.getMultitude().getSource(), "s").column(property("s.id"))
				.buildQuery();
	}
}
