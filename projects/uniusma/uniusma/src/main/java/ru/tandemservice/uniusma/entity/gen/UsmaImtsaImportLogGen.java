package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaImtsaImportLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сообщения об ошибках импорта ИМЦА
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaImtsaImportLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaImtsaImportLog";
    public static final String ENTITY_NAME = "usmaImtsaImportLog";
    public static final int VERSION_HASH = 413285158;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String P_MESSAGE = "message";

    private EppEduPlanVersionBlock _block;     // Блок УПв
    private String _message;     // Сообщения об ошибках

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null и должно быть уникальным.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Сообщения об ошибках.
     */
    public String getMessage()
    {
        return _message;
    }

    /**
     * @param message Сообщения об ошибках.
     */
    public void setMessage(String message)
    {
        dirty(_message, message);
        _message = message;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaImtsaImportLogGen)
        {
            setBlock(((UsmaImtsaImportLog)another).getBlock());
            setMessage(((UsmaImtsaImportLog)another).getMessage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaImtsaImportLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaImtsaImportLog.class;
        }

        public T newInstance()
        {
            return (T) new UsmaImtsaImportLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "message":
                    return obj.getMessage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "message":
                    obj.setMessage((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "message":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "message":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "message":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaImtsaImportLog> _dslPath = new Path<UsmaImtsaImportLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaImtsaImportLog");
    }
            

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaImportLog#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Сообщения об ошибках.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaImportLog#getMessage()
     */
    public static PropertyPath<String> message()
    {
        return _dslPath.message();
    }

    public static class Path<E extends UsmaImtsaImportLog> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<String> _message;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaImportLog#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Сообщения об ошибках.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaImportLog#getMessage()
     */
        public PropertyPath<String> message()
        {
            if(_message == null )
                _message = new PropertyPath<String>(UsmaImtsaImportLogGen.P_MESSAGE, this);
            return _message;
        }

        public Class getEntityClass()
        {
            return UsmaImtsaImportLog.class;
        }

        public String getEntityName()
        {
            return "usmaImtsaImportLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
