package ru.tandemservice.uniusma.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Компетенции (УГМА)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaCompetenceGen extends EntityBase
 implements INaturalIdentifiable<UsmaCompetenceGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.catalog.UsmaCompetence";
    public static final String ENTITY_NAME = "usmaCompetence";
    public static final int VERSION_HASH = -566569910;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_EPP_SKILL_GROUP = "eppSkillGroup";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EppSkillGroup _eppSkillGroup;     // Группы компетенций
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Группы компетенций. Свойство не может быть null.
     */
    @NotNull
    public EppSkillGroup getEppSkillGroup()
    {
        return _eppSkillGroup;
    }

    /**
     * @param eppSkillGroup Группы компетенций. Свойство не может быть null.
     */
    public void setEppSkillGroup(EppSkillGroup eppSkillGroup)
    {
        dirty(_eppSkillGroup, eppSkillGroup);
        _eppSkillGroup = eppSkillGroup;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=4000)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaCompetenceGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((UsmaCompetence)another).getCode());
            }
            setEppSkillGroup(((UsmaCompetence)another).getEppSkillGroup());
            setTitle(((UsmaCompetence)another).getTitle());
        }
    }

    public INaturalId<UsmaCompetenceGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<UsmaCompetenceGen>
    {
        private static final String PROXY_NAME = "UsmaCompetenceNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsmaCompetenceGen.NaturalId) ) return false;

            UsmaCompetenceGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaCompetenceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaCompetence.class;
        }

        public T newInstance()
        {
            return (T) new UsmaCompetence();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "eppSkillGroup":
                    return obj.getEppSkillGroup();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "eppSkillGroup":
                    obj.setEppSkillGroup((EppSkillGroup) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "eppSkillGroup":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "eppSkillGroup":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "eppSkillGroup":
                    return EppSkillGroup.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaCompetence> _dslPath = new Path<UsmaCompetence>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaCompetence");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaCompetence#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Группы компетенций. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaCompetence#getEppSkillGroup()
     */
    public static EppSkillGroup.Path<EppSkillGroup> eppSkillGroup()
    {
        return _dslPath.eppSkillGroup();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaCompetence#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UsmaCompetence> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EppSkillGroup.Path<EppSkillGroup> _eppSkillGroup;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaCompetence#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(UsmaCompetenceGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Группы компетенций. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaCompetence#getEppSkillGroup()
     */
        public EppSkillGroup.Path<EppSkillGroup> eppSkillGroup()
        {
            if(_eppSkillGroup == null )
                _eppSkillGroup = new EppSkillGroup.Path<EppSkillGroup>(L_EPP_SKILL_GROUP, this);
            return _eppSkillGroup;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaCompetence#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UsmaCompetenceGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UsmaCompetence.class;
        }

        public String getEntityName()
        {
            return "usmaCompetence";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
