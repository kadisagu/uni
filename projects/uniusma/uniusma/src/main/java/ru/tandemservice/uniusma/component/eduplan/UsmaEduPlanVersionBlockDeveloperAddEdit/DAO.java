/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockDeveloperAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaDeveloper;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getDeveloperId() == null)
        {
            UsmaDeveloper developer = new UsmaDeveloper();
            developer.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
            model.setDeveloper(developer);

        } else
        {
            model.setDeveloper(get(UsmaDeveloper.class, model.getDeveloperId()));
        }
    }
}