/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.settings.ImtsaCyclesConfiguration;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model, boolean gos2)
    {
        Model.GosGenerationBlock block = gos2 ? model.getBlockGos2() : model.getBlockGos3();
        IDataSettings dataSettings = block.getSettings();
        String imtsaCycle = dataSettings.get(Model.CYCLE_FILTER_NAME);

        List<UsmaImtsaCyclePlanStructureRel> imtsaCyclePlanStructureRels = new DQLSelectBuilder()
                .fromEntity(UsmaImtsaCyclePlanStructureRel.class, "ic")
                .where(likeUpper(property("ic", UsmaImtsaCyclePlanStructureRel.imtsaCycle()), value(CoreStringUtils.escapeLike(imtsaCycle))))
                .where(eq(property("ic", UsmaImtsaCyclePlanStructureRel.gos2()), value(gos2)))
                .order(property("ic", UsmaImtsaCyclePlanStructureRel.imtsaCycle()))
                .createStatement(getSession())
                .list();

        UniBaseUtils.createPage(block.getDataSource(), imtsaCyclePlanStructureRels);
    }
}