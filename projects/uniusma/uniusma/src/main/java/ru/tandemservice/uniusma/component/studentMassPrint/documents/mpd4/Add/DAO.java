/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd4.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uni.util.formatters.PersonIofFormatter;

import java.util.*;
/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class DAO extends ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.DAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        List<Student> studentList = model.getStudentList();

        Student firstStudent = studentList.get(0);
        model.setFirstStudent(firstStudent);
        model.setWarningMessage("");

        TopOrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();
        Calendar currentYear = Calendar.getInstance();

        model.setCurrentYear(currentYear.get(Calendar.YEAR));
        model.setFormingDate(new Date());

        model.setCertificateSeria(academyData.getCertificateSeria());
        model.setCertificateNumber(academyData.getCertificateNumber());
        model.setCertificateRegNumber(academyData.getCertificateRegNumber());
        model.setCertificateDate(academyData.getCertificateDate());
        model.setCertificateExpiriationDate(academyData.getCertificateExpiriationDate());
        model.setCertificationAgency(academyData.getCertificationAgency());

        model.setLicenceSeria(academyData.getLicenceSeria());
        model.setLicenceNumber(academyData.getLicenceNumber());
        model.setLicenceRegNumber(academyData.getLicenceRegNumber());
        model.setLicenceDate(academyData.getLicenceDate());
        model.setLicensingAgency(academyData.getLicensingAgency());

        if (academy.getHead() != null)
            model.setRectorAltStr(PersonIofFormatter.INSTANCE.format((IdentityCard) academy.getHead().getEmployee().getPerson().getIdentityCard()));

        model.setUniTitleAtTimeAdmission(academy.getTitle());
        model.setEducationLevelStageHint("Сведения об образовании призывника до поступления в данное образовательное учреждение заполняются только образовательными учреждениями среднего профессионального образования и начального профессионального образования.");

        List<IdentifiableWrapper> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++)
            monthList.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));
        model.setMonthList(monthList);

        model.setDisplayWarning(model.getWarningBuilder().length() > 0);
    }
}
