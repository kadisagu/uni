/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public class DAO extends ModularStudentExtractPubDAO<UsmaExcludeDebtStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
    }
}
