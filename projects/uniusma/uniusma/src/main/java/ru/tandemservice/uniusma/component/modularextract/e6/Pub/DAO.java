/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e6.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.TransferCompTypeStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e6.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e6.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        TransferCompTypeStuExtractUsmaExt extractExt = getByNaturalId(new TransferCompTypeStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        m.setExtUsmaExt(extractExt);
    }
}
