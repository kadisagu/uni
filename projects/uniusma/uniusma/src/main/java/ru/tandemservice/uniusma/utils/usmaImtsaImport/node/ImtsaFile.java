/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node;

import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniusma.UniusmaDefines;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaPlanRow;

import java.util.Collection;
import java.util.Map;
import java.util.Set;


/**
 * Данные ИМЦА для импорта.
 * @author Alexander Zhebko
 * @since 18.07.2013
 */
public class ImtsaFile
{
    // загружаемые данные
    private ImtsaTitle _title;
    private ImtsaCompetences _competences;
    private ImtsaPlanRow _rootPlanRow;
    private Map<String, Collection<ImtsaPractice>> _practiceMap;
    private Map<Integer, Map<Integer, PairKey<Double, Double>>> _dissertationLaborMap;
    private Map<Integer, Map<Integer, PairKey<Double, Double>>> _stateExamLaborMap;
    private Set<String> _attestationCompetenceCodes;
    private Map<Integer, Integer> _minCoursePartNumberMap;
    private Map<Integer, Integer> _maxCoursePartNumberMap;

    public ImtsaTitle getTitle(){ return _title; }
    public void setTitle(ImtsaTitle title){ _title = title; }

    public ImtsaCompetences getCompetences(){ return _competences; }
    public void setCompetences(ImtsaCompetences competences){ _competences = competences; }

    public ImtsaPlanRow getRootPlanRow(){ return _rootPlanRow; }
    public void setRootPlanRow(ImtsaPlanRow rootPlanRow){ _rootPlanRow = rootPlanRow; }

    public Map<String, Collection<ImtsaPractice>> getPracticeMap(){ return _practiceMap; }
    public void setPracticeMap(Map<String, Collection<ImtsaPractice>> practiceMap){ _practiceMap = practiceMap; }

    public Map<Integer, Map<Integer, PairKey<Double, Double>>> getDissertationLaborMap(){ return _dissertationLaborMap; }
    public void setDissertationLaborMap(Map<Integer, Map<Integer, PairKey<Double, Double>>> dissertationLaborMap){ _dissertationLaborMap = dissertationLaborMap; }

    public Map<Integer, Map<Integer, PairKey<Double, Double>>> getStateExamLaborMap(){ return _stateExamLaborMap; }
    public void setStateExamLaborMap(Map<Integer, Map<Integer, PairKey<Double, Double>>> stateExamLaborMap){ _stateExamLaborMap = stateExamLaborMap; }

    public Set<String> getAttestationCompetenceCodes(){ return _attestationCompetenceCodes; }
    public void setAttestationCompetenceCodes(Set<String> attestationCompetenceCodes){ _attestationCompetenceCodes = attestationCompetenceCodes; }

    public Map<Integer, Integer> getMinCoursePartNumberMap(){ return _minCoursePartNumberMap; }
    public void setMinCoursePartNumberMap(Map<Integer, Integer> minCoursePartNumberMap){ _minCoursePartNumberMap = minCoursePartNumberMap; }

    public Map<Integer, Integer> getMaxCoursePartNumberMap(){ return _maxCoursePartNumberMap; }
    public void setMaxCoursePartNumberMap(Map<Integer, Integer> maxCoursePartNumberMap){ _maxCoursePartNumberMap = maxCoursePartNumberMap; }


    // вспомогательные данные
    public static String getPlanNodeName(){ return PLAN_NODE_NAME; }
    public static String getTitleNodeName(){ return TITLE_NODE_NAME; }
    public static String getCompetencesNodeName(){ return COMPETENCES_NODE_NAME; }
    public static String getPlanRowsNodeName(){ return PLAN_ROWS_NODE_NAME; }
    public static String getAdditionalCompetenceNodeName(){ return ADDITIONAL_COMPETENCE_NODE_NAME; }
    public static String getAttestationCompetenceAttribute(){ return ATTESTATION_COMPETENCE_ATTRIBUTE; }

    public static String getGos2SpecialWorkKindsNodeName(){ return GOS2_SPECIAL_WORK_KINDS_NODE_NAME; }
    public static String getGos2TutoringPracticeGroupNodeName(){ return GOS2_TUTORING_PRACTICE_GROUP_NODE_NAME; }
    public static String getGos2OtherPracticeGroupNodeName(){ return GOS2_OTHER_PRACTICE_GROUP_NODE_NAME; }

    public static String getGos3SpecialWorkKindsNodeName(){ return GOS3_SPECIAL_WORK_KINDS_NODE_NAME; }
    public static String getGos3TutoringPracticeGroupNodeName(){ return GOS3_TUTORING_PRACTICE_GROUP_NODE_NAME; }
    public static String getGos3NirPracticeGroupNodeName(){ return GOS3_NIR_PRACTICE_GROUP_NODE_NAME; }
    public static String getGos3OtherPracticeGroupNodeName(){ return GOS3_OTHER_PRACTICE_GROUP_NODE_NAME; }

    private static final String PLAN_NODE_NAME = "План";
    private static final String TITLE_NODE_NAME = "Титул";
    private static final String COMPETENCES_NODE_NAME = "Компетенции";
    private static final String PLAN_ROWS_NODE_NAME = "СтрокиПлана";

    private static final String ADDITIONAL_COMPETENCE_NODE_NAME = "ДопКомпетенции";
    private static final String ATTESTATION_COMPETENCE_ATTRIBUTE = "ИГА";

    private static final String GOS2_SPECIAL_WORK_KINDS_NODE_NAME = "СпецВидыРабот";
    private static final String GOS2_TUTORING_PRACTICE_GROUP_NODE_NAME = "УчебПрактики";
    private static final String GOS2_OTHER_PRACTICE_GROUP_NODE_NAME = "ПрочиеПрактики";

    private static final String GOS3_SPECIAL_WORK_KINDS_NODE_NAME = "СпецВидыРаботНов";
    private static final String GOS3_TUTORING_PRACTICE_GROUP_NODE_NAME = "УчебПрактики";
    private static final String GOS3_NIR_PRACTICE_GROUP_NODE_NAME = "НИР";
    private static final String GOS3_OTHER_PRACTICE_GROUP_NODE_NAME = "ПрочиеПрактики";

    public static String getDissertationTitle(){ return DISSERTATION_TITLE; }
    public static String getStateExamTitle(){ return STATE_EXAM_TITLE; }

    private static final String DISSERTATION_TITLE = "ВКР";
    private static final String STATE_EXAM_TITLE = "ГЭ";


    public static Character getDissertationWeekType(){ return DISSERTATION_WEEK_TYPE; }
    public static Character getStateExamWeekType(){ return STATE_EXAM_WEEK_TYPE; }

    private static final Character DISSERTATION_WEEK_TYPE = UniusmaDefines.IMTSA_QUALIFICATION_WORK;
    private static final Character STATE_EXAM_WEEK_TYPE = UniusmaDefines.IMTSA_STATE_EXAMINATION;
}