package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.TransferDevFormStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О переводе с одной формы обучения на другую
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferDevFormStuExtractUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<TransferDevFormStuExtractUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt";
    public static final String ENTITY_NAME = "transferDevFormStuExtractUsmaExt";
    public static final int VERSION_HASH = 715822685;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_ADMISSION_DATE = "admissionDate";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_GRANTS_PAYING_DATE = "stopGrantsPayingDate";
    public static final String P_HAS_DEBTS = "hasDebts";
    public static final String P_DEADLINE = "deadline";

    private TransferDevFormStuExtract _extractExt;     // Выписка из сборного приказа по студенту. О переводе на другую форму освоения
    private Date _admissionDate;     // Дата допуска к занятиям
    private boolean _stopGrantsPaying = false;     // Отменить выплату академической стипендии
    private Date _stopGrantsPayingDate;     // Дата прекращения выплаты стипендии
    private boolean _hasDebts = false;     // Разница в учебных планах
    private Date _deadline;     // Срок ликвидации задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другую форму освоения. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public TransferDevFormStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О переводе на другую форму освоения. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(TransferDevFormStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Дата допуска к занятиям. Свойство не может быть null.
     */
    @NotNull
    public Date getAdmissionDate()
    {
        return _admissionDate;
    }

    /**
     * @param admissionDate Дата допуска к занятиям. Свойство не может быть null.
     */
    public void setAdmissionDate(Date admissionDate)
    {
        dirty(_admissionDate, admissionDate);
        _admissionDate = admissionDate;
    }

    /**
     * @return Отменить выплату академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Отменить выплату академической стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     */
    public Date getStopGrantsPayingDate()
    {
        return _stopGrantsPayingDate;
    }

    /**
     * @param stopGrantsPayingDate Дата прекращения выплаты стипендии.
     */
    public void setStopGrantsPayingDate(Date stopGrantsPayingDate)
    {
        dirty(_stopGrantsPayingDate, stopGrantsPayingDate);
        _stopGrantsPayingDate = stopGrantsPayingDate;
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasDebts()
    {
        return _hasDebts;
    }

    /**
     * @param hasDebts Разница в учебных планах. Свойство не может быть null.
     */
    public void setHasDebts(boolean hasDebts)
    {
        dirty(_hasDebts, hasDebts);
        _hasDebts = hasDebts;
    }

    /**
     * @return Срок ликвидации задолженности.
     */
    public Date getDeadline()
    {
        return _deadline;
    }

    /**
     * @param deadline Срок ликвидации задолженности.
     */
    public void setDeadline(Date deadline)
    {
        dirty(_deadline, deadline);
        _deadline = deadline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TransferDevFormStuExtractUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((TransferDevFormStuExtractUsmaExt)another).getExtractExt());
            }
            setAdmissionDate(((TransferDevFormStuExtractUsmaExt)another).getAdmissionDate());
            setStopGrantsPaying(((TransferDevFormStuExtractUsmaExt)another).isStopGrantsPaying());
            setStopGrantsPayingDate(((TransferDevFormStuExtractUsmaExt)another).getStopGrantsPayingDate());
            setHasDebts(((TransferDevFormStuExtractUsmaExt)another).isHasDebts());
            setDeadline(((TransferDevFormStuExtractUsmaExt)another).getDeadline());
        }
    }

    public INaturalId<TransferDevFormStuExtractUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<TransferDevFormStuExtractUsmaExtGen>
    {
        private static final String PROXY_NAME = "TransferDevFormStuExtractUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(TransferDevFormStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TransferDevFormStuExtractUsmaExtGen.NaturalId) ) return false;

            TransferDevFormStuExtractUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferDevFormStuExtractUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferDevFormStuExtractUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new TransferDevFormStuExtractUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "admissionDate":
                    return obj.getAdmissionDate();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopGrantsPayingDate":
                    return obj.getStopGrantsPayingDate();
                case "hasDebts":
                    return obj.isHasDebts();
                case "deadline":
                    return obj.getDeadline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((TransferDevFormStuExtract) value);
                    return;
                case "admissionDate":
                    obj.setAdmissionDate((Date) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopGrantsPayingDate":
                    obj.setStopGrantsPayingDate((Date) value);
                    return;
                case "hasDebts":
                    obj.setHasDebts((Boolean) value);
                    return;
                case "deadline":
                    obj.setDeadline((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "admissionDate":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopGrantsPayingDate":
                        return true;
                case "hasDebts":
                        return true;
                case "deadline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "admissionDate":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopGrantsPayingDate":
                    return true;
                case "hasDebts":
                    return true;
                case "deadline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return TransferDevFormStuExtract.class;
                case "admissionDate":
                    return Date.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopGrantsPayingDate":
                    return Date.class;
                case "hasDebts":
                    return Boolean.class;
                case "deadline":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferDevFormStuExtractUsmaExt> _dslPath = new Path<TransferDevFormStuExtractUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferDevFormStuExtractUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другую форму освоения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getExtractExt()
     */
    public static TransferDevFormStuExtract.Path<TransferDevFormStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Дата допуска к занятиям. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getAdmissionDate()
     */
    public static PropertyPath<Date> admissionDate()
    {
        return _dslPath.admissionDate();
    }

    /**
     * @return Отменить выплату академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getStopGrantsPayingDate()
     */
    public static PropertyPath<Date> stopGrantsPayingDate()
    {
        return _dslPath.stopGrantsPayingDate();
    }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#isHasDebts()
     */
    public static PropertyPath<Boolean> hasDebts()
    {
        return _dslPath.hasDebts();
    }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getDeadline()
     */
    public static PropertyPath<Date> deadline()
    {
        return _dslPath.deadline();
    }

    public static class Path<E extends TransferDevFormStuExtractUsmaExt> extends EntityPath<E>
    {
        private TransferDevFormStuExtract.Path<TransferDevFormStuExtract> _extractExt;
        private PropertyPath<Date> _admissionDate;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopGrantsPayingDate;
        private PropertyPath<Boolean> _hasDebts;
        private PropertyPath<Date> _deadline;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другую форму освоения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getExtractExt()
     */
        public TransferDevFormStuExtract.Path<TransferDevFormStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new TransferDevFormStuExtract.Path<TransferDevFormStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Дата допуска к занятиям. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getAdmissionDate()
     */
        public PropertyPath<Date> admissionDate()
        {
            if(_admissionDate == null )
                _admissionDate = new PropertyPath<Date>(TransferDevFormStuExtractUsmaExtGen.P_ADMISSION_DATE, this);
            return _admissionDate;
        }

    /**
     * @return Отменить выплату академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(TransferDevFormStuExtractUsmaExtGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getStopGrantsPayingDate()
     */
        public PropertyPath<Date> stopGrantsPayingDate()
        {
            if(_stopGrantsPayingDate == null )
                _stopGrantsPayingDate = new PropertyPath<Date>(TransferDevFormStuExtractUsmaExtGen.P_STOP_GRANTS_PAYING_DATE, this);
            return _stopGrantsPayingDate;
        }

    /**
     * @return Разница в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#isHasDebts()
     */
        public PropertyPath<Boolean> hasDebts()
        {
            if(_hasDebts == null )
                _hasDebts = new PropertyPath<Boolean>(TransferDevFormStuExtractUsmaExtGen.P_HAS_DEBTS, this);
            return _hasDebts;
        }

    /**
     * @return Срок ликвидации задолженности.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt#getDeadline()
     */
        public PropertyPath<Date> deadline()
        {
            if(_deadline == null )
                _deadline = new PropertyPath<Date>(TransferDevFormStuExtractUsmaExtGen.P_DEADLINE, this);
            return _deadline;
        }

        public Class getEntityClass()
        {
            return TransferDevFormStuExtractUsmaExt.class;
        }

        public String getEntityName()
        {
            return "transferDevFormStuExtractUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
