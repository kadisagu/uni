package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * XML файл ИМЦА
 */
public class UsmaImtsaXml extends UsmaImtsaXmlGen
{
    public UsmaImtsaXml()
    {

    }

    public UsmaImtsaXml(EppEduPlanVersionBlock block, String fileName, String encoding, byte[] xml)
    {
        this(block, fileName, encoding, xml, 1);
    }

    public UsmaImtsaXml(EppEduPlanVersionBlock block, String fileName, String encoding, byte[] xml, int number)
    {
        this.setBlock(block);
        this.setFileName(fileName);
        this.setEncoding(encoding);
        this.setXml(xml);
        this.setNumber(number);
    }
}