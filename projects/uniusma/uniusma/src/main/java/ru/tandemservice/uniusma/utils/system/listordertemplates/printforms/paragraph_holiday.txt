\qc{parNumberRoman}. Предоставить каникулы следующим студентам {course} курса {orgUnit_G} {educationType} \'ab{educationOrgUnit}\'bb 
форма обучения \'ab{developForm}\'bb группа \'ab{group}\'bb c {beginDate} по {endDate} согласно списку:
\par \fi350\qj {STUDENT_LIST}
\par \fi0\ql Всего {extractCount} ({extractCountStr}) {people}.\par\par