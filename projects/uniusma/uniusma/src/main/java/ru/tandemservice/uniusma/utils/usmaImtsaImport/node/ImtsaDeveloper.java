/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniusma.entity.UsmaDeveloper;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.Validator;

import java.util.*;

/**
 * Разработчик ИМЦА.
 * @author Alexander Zhebko
 * @since 24.07.2013
 */
public class ImtsaDeveloper
{
    // представления
    public static final IAttributeView ATTRIBUTE_VIEW = new DeveloperAttributeView();
    public static final INodeView NODE_VIEW = new DeveloperNodeView();


    // загружаемые данные
    private final Map<String, Object> _attributes;
    public Map<String, Object> getAttributes(){ return _attributes; }

    private ImtsaDeveloper(Map<String, Object> attributes)
    {
        _attributes = attributes;
    }

    public static ImtsaDeveloper createDeveloper(Map<String, Object> attributes)
    {
        return new ImtsaDeveloper(attributes);
    }


    // вспомогательные данные
    public static Map<String, String> getAttributePropertyMap(){ return DeveloperAttributeView.ATTRIBUTE_PROPERTY_MAP; }
    public static String getDeveloperFioAttribute(){ return DeveloperAttributeView.A_FIO; }


    /**
     * Представление атрибутов нода "Разработчик".
     */
    private static class DeveloperAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String A_NUMBER = "Ном";
        private static final String A_FIO = "ФИО";
        private static final String A_POST = "Должность";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(A_NUMBER, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                A_NUMBER,
                A_FIO,
                A_POST
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(A_NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                A_NUMBER
        );


        // 7. поля сущности
        private static final String P_NUMBER = UsmaDeveloper.P_NUMBER;
        private static final String P_FIO = UsmaDeveloper.P_FIO;
        private static final String P_POST = UsmaDeveloper.P_POST;


        // 8. отображение названия атрибута на название поля сущности
        private static final Map<String, String> ATTRIBUTE_PROPERTY_MAP = new HashMap<>();
        static
        {
            ATTRIBUTE_PROPERTY_MAP.put(A_NUMBER, P_NUMBER);
            ATTRIBUTE_PROPERTY_MAP.put(A_FIO, P_FIO);
            ATTRIBUTE_PROPERTY_MAP.put(A_POST, P_POST);
        }
    }


    /**
     * Представление нода "Разработчик".
     */
    private static class DeveloperNodeView implements INodeView
    {
        @Override
        public String getNodeName()
        {
            return DEVELOPER_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return DeveloperAttributeView.UNIQUE_ATTRIBUTES;
        }

        // ноды
        private static final String DEVELOPER_NODE_NAME = "Разработчик";
    }
}