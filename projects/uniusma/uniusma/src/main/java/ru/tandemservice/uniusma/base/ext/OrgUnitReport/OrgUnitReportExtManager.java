/* $Id:$ */
package ru.tandemservice.uniusma.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;

/**
 * @author rsizonenko
 * @since 06.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("usmaMedCardReport", new OrgUnitReportDefinition("Выборка для здравпункта",
                        "usmaMedCardReport",
                        ru.tandemservice.uni.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNI_ORG_UNIT_STUDENT_REPORT_BLOCK,
                        "UsmaMedCardReportAdd",
                        "orgUnit_usmaMedCardReportViewPermissionKey")
                )
                .add("usmaStudentCardAttachmentReport", new OrgUnitReportDefinition("Печать \"вкладышей\" для учебной карточки студента",
                        "usmaStudentCardAttachmentReport",
                        ru.tandemservice.uni.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNI_ORG_UNIT_STUDENT_REPORT_BLOCK,
                        "UsmaStudentCardAttachmentReportPub",
                        "orgUnit_usmaStudentCardAttachmentReportViewPermissionKey")
                )
                .add("usmaSessionProtocolGiaReport", new OrgUnitReportDefinition("Протокол ГИА (на академ. группу)",
                        "usmaSessionProtocolGiaReport",
                        ru.tandemservice.unisession.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNISESSION_ORG_UNIT_REPORT_BLOCK,
                        "UsmaSessionProtocolGiaReportPub",
                        "orgUnit_usmaSessionProtocolGiaReportViewPermissionKey")
                )
                .create();
    }
}
