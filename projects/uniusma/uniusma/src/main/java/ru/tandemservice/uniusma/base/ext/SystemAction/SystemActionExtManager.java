/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniusma.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("unisc_importStudents", new SystemActionDefinition("uniusma", "importStudents", "onClickImportStudents", SystemActionPubExt.USMA_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_createEmpLabourContract", new SystemActionDefinition("uniusma", "createEmpLabourContract", "onClickCreateEmpLabourContract", SystemActionPubExt.USMA_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniusma_changeStudentLogins", new SystemActionDefinition("uniusma", "usmaChangeStudentLogins", "onClickChangeStudentLogins", SystemActionPubExt.USMA_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
