package ru.tandemservice.uniusma.entity.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.entity.catalog.UsmaHealthGroup;
import ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Другие данные
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaStudentOtherInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo";
    public static final String ENTITY_NAME = "usmaStudentOtherInfo";
    public static final int VERSION_HASH = 843583505;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_HEALTH_GROUP = "healthGroup";
    public static final String P_DISPENSARY = "dispensary";

    private Student _student;     // Студент
    private UsmaHealthGroup _healthGroup;     // Группы здоровья
    private boolean _dispensary;     // Диспансерный учет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Группы здоровья.
     */
    public UsmaHealthGroup getHealthGroup()
    {
        return _healthGroup;
    }

    /**
     * @param healthGroup Группы здоровья.
     */
    public void setHealthGroup(UsmaHealthGroup healthGroup)
    {
        dirty(_healthGroup, healthGroup);
        _healthGroup = healthGroup;
    }

    /**
     * @return Диспансерный учет. Свойство не может быть null.
     */
    @NotNull
    public boolean isDispensary()
    {
        return _dispensary;
    }

    /**
     * @param dispensary Диспансерный учет. Свойство не может быть null.
     */
    public void setDispensary(boolean dispensary)
    {
        dirty(_dispensary, dispensary);
        _dispensary = dispensary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaStudentOtherInfoGen)
        {
            setStudent(((UsmaStudentOtherInfo)another).getStudent());
            setHealthGroup(((UsmaStudentOtherInfo)another).getHealthGroup());
            setDispensary(((UsmaStudentOtherInfo)another).isDispensary());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaStudentOtherInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaStudentOtherInfo.class;
        }

        public T newInstance()
        {
            return (T) new UsmaStudentOtherInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "healthGroup":
                    return obj.getHealthGroup();
                case "dispensary":
                    return obj.isDispensary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "healthGroup":
                    obj.setHealthGroup((UsmaHealthGroup) value);
                    return;
                case "dispensary":
                    obj.setDispensary((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "healthGroup":
                        return true;
                case "dispensary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "healthGroup":
                    return true;
                case "dispensary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "healthGroup":
                    return UsmaHealthGroup.class;
                case "dispensary":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaStudentOtherInfo> _dslPath = new Path<UsmaStudentOtherInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaStudentOtherInfo");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Группы здоровья.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo#getHealthGroup()
     */
    public static UsmaHealthGroup.Path<UsmaHealthGroup> healthGroup()
    {
        return _dslPath.healthGroup();
    }

    /**
     * @return Диспансерный учет. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo#isDispensary()
     */
    public static PropertyPath<Boolean> dispensary()
    {
        return _dslPath.dispensary();
    }

    public static class Path<E extends UsmaStudentOtherInfo> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private UsmaHealthGroup.Path<UsmaHealthGroup> _healthGroup;
        private PropertyPath<Boolean> _dispensary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Группы здоровья.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo#getHealthGroup()
     */
        public UsmaHealthGroup.Path<UsmaHealthGroup> healthGroup()
        {
            if(_healthGroup == null )
                _healthGroup = new UsmaHealthGroup.Path<UsmaHealthGroup>(L_HEALTH_GROUP, this);
            return _healthGroup;
        }

    /**
     * @return Диспансерный учет. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo#isDispensary()
     */
        public PropertyPath<Boolean> dispensary()
        {
            if(_dispensary == null )
                _dispensary = new PropertyPath<Boolean>(UsmaStudentOtherInfoGen.P_DISPENSARY, this);
            return _dispensary;
        }

        public Class getEntityClass()
        {
            return UsmaStudentOtherInfo.class;
        }

        public String getEntityName()
        {
            return "usmaStudentOtherInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
