package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x9_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaCompetence

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmacompetence_t"))
            {
                DBTable dbt = new DBTable("usmacompetence_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("eppskillgroup_id", DBType.LONG).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(4000)).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaCompetence");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaCompetence2EduPlanVersionBlockRel

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("smcmptnc2edplnvrsnblckrl_t"))
            {
                DBTable dbt = new DBTable("smcmptnc2edplnvrsnblckrl_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("usmacompetence_id", DBType.LONG).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("number_p", DBType.INTEGER).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaCompetence2EduPlanVersionBlockRel");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaCompetence2EpvRegistryRowRel

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("smcmptnc2epvrgstryrwrl_t"))
            {
                DBTable dbt = new DBTable("smcmptnc2epvrgstryrwrl_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("usmacompetence_id", DBType.LONG).setNullable(false),
                                          new DBColumn("registryrow_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaCompetence2EpvRegistryRowRel");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaDeveloper

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmadeveloper_t"))
            {
                DBTable dbt = new DBTable("usmadeveloper_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("number_p", DBType.INTEGER).setNullable(false),
                                          new DBColumn("fio_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("post_p", DBType.createVarchar(255)).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaDeveloper");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEduPlanVersionPartitionType

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("smedplnvrsnprttntyp_t"))
            {
                DBTable dbt = new DBTable("smedplnvrsnprttntyp_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("version_id", DBType.LONG).setNullable(false),
                                          new DBColumn("partitiontype_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEduPlanVersionPartitionType");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEduPlanVersionWeek

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaeduplanversionweek_t"))
            {
                DBTable dbt = new DBTable("usmaeduplanversionweek_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("version_id", DBType.LONG).setNullable(false),
                                          new DBColumn("course_id", DBType.LONG).setNullable(false),
                                          new DBColumn("week_id", DBType.LONG).setNullable(false),
                                          new DBColumn("term_id", DBType.LONG).setNullable(false),
                                          new DBColumn("weektype_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEduPlanVersionWeek");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEduPlanVersionWeekPart

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaeduplanversionweekpart_t"))
            {
                DBTable dbt = new DBTable("usmaeduplanversionweekpart_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("eduplanversionweek_id", DBType.LONG).setNullable(false),
                                          new DBColumn("partitionelementnumber_p", DBType.INTEGER).setNullable(false),
                                          new DBColumn("weektype_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEduPlanVersionWeekPart");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEppRegistryModuleALoadExt

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("smepprgstrymdlaldext_t"))
            {
                DBTable dbt = new DBTable("smepprgstrymdlaldext_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("module_id", DBType.LONG).setNullable(false),
                                          new DBColumn("loadtype_id", DBType.LONG).setNullable(false),
                                          new DBColumn("load_p", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEppRegistryModuleALoadExt");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEppRegistryModuleELoad

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaeppregistrymoduleeload_t"))
            {
                DBTable dbt = new DBTable("usmaeppregistrymoduleeload_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("module_id", DBType.LONG).setNullable(false),
                                          new DBColumn("loadtype_id", DBType.LONG).setNullable(false),
                                          new DBColumn("load_p", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEppRegistryModuleELoad");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEpvBlockTitle

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaepvblocktitle_t"))
            {
                DBTable dbt = new DBTable("usmaepvblocktitle_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("planfulltitle_p", DBType.createVarchar(255)),
                                          new DBColumn("plantitle_p", DBType.createVarchar(255)),
                                          new DBColumn("usernumber_p", DBType.createVarchar(255)),
                                          new DBColumn("academy_p", DBType.createVarchar(255)),
                                          new DBColumn("orgunit_p", DBType.createVarchar(255)),
                                          new DBColumn("higherechelon_p", DBType.createVarchar(255)),
                                          new DBColumn("producingorgunit_p", DBType.createVarchar(255)),
                                          new DBColumn("formativeorgunit_p", DBType.createVarchar(255)),
                                          new DBColumn("educationdirectioncode_p", DBType.createVarchar(255)),
                                          new DBColumn("startyear_p", DBType.INTEGER),
                                          new DBColumn("includeexamsinhoursamount_p", DBType.BOOLEAN),
                                          new DBColumn("dissertationasattestation_p", DBType.BOOLEAN),
                                          new DBColumn("stateexamasattestation_p", DBType.BOOLEAN),
                                          new DBColumn("ksrorindividuallessons_p", DBType.createVarchar(255)),
                                          new DBColumn("zetinweekatt_p", DBType.DOUBLE),
                                          new DBColumn("hoursamountinzetatt_p", DBType.DOUBLE),
                                          new DBColumn("programlabourunits_p", DBType.DOUBLE),
                                          new DBColumn("interactivelessonspercent_p", DBType.DOUBLE),
                                          new DBColumn("lecturespercent_p", DBType.DOUBLE),
                                          new DBColumn("choicedisciplinespercent_p", DBType.DOUBLE),
                                          new DBColumn("maxsize_p", DBType.DOUBLE),
                                          new DBColumn("plankind_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("levelcode_p", DBType.createVarchar(255)),
                                          new DBColumn("level_p", DBType.createVarchar(255)),
                                          new DBColumn("termsincourse_p", DBType.INTEGER),
                                          new DBColumn("elementsinweek_p", DBType.INTEGER),
                                          new DBColumn("stateexamdate_p", DBType.TIMESTAMP),
                                          new DBColumn("stateexamdoc_p", DBType.createVarchar(255)),
                                          new DBColumn("stateexamtype_p", DBType.createVarchar(255)),
                                          new DBColumn("application_p", DBType.createVarchar(255)),
                                          new DBColumn("applicationdate_p", DBType.TIMESTAMP),
                                          new DBColumn("applicationversion_p", DBType.createVarchar(255)),
                                          new DBColumn("zetinyear_p", DBType.DOUBLE),
                                          new DBColumn("zetinweek_p", DBType.DOUBLE),
                                          new DBColumn("hoursamountinzet_p", DBType.DOUBLE),
                                          new DBColumn("totalzet_p", DBType.DOUBLE),
                                          new DBColumn("certificatedate_p", DBType.TIMESTAMP)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEpvBlockTitle");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEpvRowTermLoad

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaepvrowtermload_t"))
            {
                DBTable dbt = new DBTable("usmaepvrowtermload_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("rowterm_id", DBType.LONG).setNullable(false),
                                          new DBColumn("loadtype_id", DBType.LONG).setNullable(false),
                                          new DBColumn("load_p", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEpvRowTermLoad");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaLoadType

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaloadtype_t"))
            {
                DBTable dbt = new DBTable("usmaloadtype_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("abbreviation_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(1200))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaLoadType");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaPracticeDispersion

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmapracticedispersion_t"))
            {
                DBTable dbt = new DBTable("usmapracticedispersion_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("practice_id", DBType.LONG).setNullable(false),
                                          new DBColumn("dispersed_p", DBType.BOOLEAN).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaPracticeDispersion");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaQualification

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaqualification_t"))
            {
                DBTable dbt = new DBTable("usmaqualification_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("number_p", DBType.INTEGER).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("developperiod_p", DBType.createVarchar(255))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaQualification");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaSchedulePartitionType

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaschedulepartitiontype_t"))
            {
                DBTable dbt = new DBTable("usmaschedulepartitiontype_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("partsnumber_p", DBType.INTEGER).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaSchedulePartitionType");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaSpeciality

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("usmaspeciality_t"))
            {
                DBTable dbt = new DBTable("usmaspeciality_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("number_p", DBType.INTEGER).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaSpeciality");
            }
		}


    }
}