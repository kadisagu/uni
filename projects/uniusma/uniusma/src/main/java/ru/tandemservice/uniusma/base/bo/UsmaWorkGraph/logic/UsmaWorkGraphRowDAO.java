/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic;

import org.hibernate.Session;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleUtils;
import ru.tandemservice.uniusma.entity.eduPlan.*;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniusma.utils.UsmaEduPlanVersionScheduleUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 15.10.2013
 */
public class UsmaWorkGraphRowDAO extends CommonDAO implements IUsmaWorkGraphRowDAO, INeedPersistenceSupport
{
    @Override
    public void prepareEditRow(Long rowId, UsmaWorkGraph workGraph, Course course, UsmaRangeSelectionWeekTypeListDataSource<UsmaWorkGraphRow> graphDataSource)
    {
        if (getRelationList(workGraph, course, rowId).isEmpty())
        {
            DevelopGrid developGrid = workGraph.getDevelopGrid();
            graphDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, EppEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(developGrid, course)));
        }
        else
        {
            graphDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, graphDataSource.getRow2points().get(rowId)));
        }
    }

    private List<UsmaWorkGraphRowWeek> getRelationList(UsmaWorkGraph workGraph, Course course, Long rowId)
    {
        MQBuilder builder = new MQBuilder(UsmaWorkGraphRowWeek.ENTITY_CLASS, "rw");
        builder.addJoin("rw", UsmaWorkGraphRowWeek.L_ROW, "r");
        builder.add(MQExpression.eq("r", UsmaWorkGraphRow.L_GRAPH, workGraph));
        builder.add(MQExpression.eq("r", UsmaWorkGraphRow.L_COURSE, course));
        if (rowId != null)
        {
            builder.add(MQExpression.eq("r", IEntity.P_ID, rowId));
        }
        return builder.getResultList(getSession());
    }

    @Override
    public void updateWorkGraphRow(Long rowId, Map<Long, Map<Integer, EppWeekType>> dataMap, int[] ranges)
    {
        UsmaWorkGraphRow workGraphRow = get(UsmaWorkGraphRow.class, rowId);
        EppWeekType theory = getByNaturalId(new EppWeekType.NaturalId(EppWeekTypeCodes.THEORY));

        Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        List<Integer> terms = UsmaEduPlanVersionScheduleUtils.getDevelopGridDetailTerms(workGraphRow.getGraph().getDevelopGrid(), workGraphRow.getCourse());


        Map<Long, Map<Integer, String>> oldDataMap = SafeMap.get(HashMap.class);
        //Map<String, EppWeekType> weekTypeMap = SafeMap.get(new SafeMap.Callback<String, EppWeekType>(){ @Override public EppWeekType resolve(String key){ return getByNaturalId(new EppWeekType.NaturalId(key)); }});

        IDQLSelectableQuery weekIdQuery = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .column(property("w", UsmaWorkGraphRowWeek.id()))
                .where(eq(property("w", UsmaWorkGraphRowWeek.row().id()), value(rowId)))
                .buildQuery();

        Map<Long, Map<Integer, String>> weekPartsMap = SafeMap.get(HashMap.class);
        DQLSelectBuilder weekPartsBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeekPart.class, "wp")
                .column(property("wp", UsmaWorkGraphRowWeekPart.week().id()))
                .column(property("wp", UsmaWorkGraphRowWeekPart.part()))
                .column(property("wp", UsmaWorkGraphRowWeekPart.type().code()))
                .where(in(property("wp", UsmaWorkGraphRowWeekPart.week().id()), weekIdQuery));

        for (Object[] row: this.<Object[]>getList(weekPartsBuilder))
        {
            weekPartsMap.get((Long) row[0]).put((Integer) row[1], (String) row[2]);
        }

        DQLSelectBuilder weeksBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .joinPath(DQLJoinType.left, UsmaWorkGraphRowWeek.type().fromAlias("w"), "wt")
                .where(in(property("w", UsmaWorkGraphRowWeek.id()), weekIdQuery))
                .column(property("w", UsmaWorkGraphRowWeek.id()))
                .column(property("w", UsmaWorkGraphRowWeek.week()))
                .column(property("wt", EppWeekType.code()));

        Map<Integer, Long> weekNumberToIdMap = new HashMap<>();
        for (EppYearEducationWeek educationWeek: getList(EppYearEducationWeek.class, EppYearEducationWeek.year(), workGraphRow.getGraph().getYear()))
        {
            weekNumberToIdMap.put(educationWeek.getNumber(), educationWeek.getId());
        }

        Map<Long, Integer> weekIdToNumber = new HashMap<>();
        for (Map.Entry<Integer, Long> weekEntry: weekNumberToIdMap.entrySet())
        {
            weekIdToNumber.put(weekEntry.getValue(), weekEntry.getKey());
        }

        for (Object[] row: this.<Object[]>getList(weeksBuilder))
        {
            Long weekId = (Long) row[0];
            Integer weekNumber = (Integer) row[1];
            String weekTypeCode = (String) row[2];
            Map<Integer, String> partTypeMap;

            if (weekTypeCode == null)
            {
                partTypeMap = weekPartsMap.get(weekId);

            } else
            {
                partTypeMap = new HashMap<>();
                partTypeMap.put(0, weekTypeCode);
            }

            oldDataMap.put(weekNumberToIdMap.get(weekNumber), partTypeMap);
        }

        List<Long> rowWeekIds = new ArrayList<>();
        List<Long> rowWeekPartToDelWeekIds = new ArrayList<>();
        List<Long> rowWeekToCreateIds = new ArrayList<>();

        Map<Long, Map<Integer, EppWeekType>> toSaveMap = new HashMap<>();

        Map<Integer, Term> weekTermMap = new HashMap<>();
        for (Map.Entry<Long, Map<Integer, EppWeekType>> weekEntry : dataMap.entrySet())
        {
            Long weekId = weekEntry.getKey();

            Integer weekNumber = weekIdToNumber.get(weekId);
            int partNumber = ranges[weekNumber - 1];
            if (partNumber == -1)
            {
                continue;
            }

            rowWeekIds.add(weekId);
            weekTermMap.put(weekNumber, termMap.get(terms.get(partNumber)));

            Map<Integer, EppWeekType> partMap = weekEntry.getValue();
            Map<Integer, String> oldPartMap = oldDataMap.get(weekId);

            if (oldPartMap == null || partMap.size() != oldPartMap.size())
            {
                if (oldPartMap == null)
                {
                    rowWeekToCreateIds.add(weekId);
                }

                toSaveMap.put(weekId, partMap);
                if (partMap.size() == 1)
                {
                    rowWeekPartToDelWeekIds.add(weekId);
                }

                continue;
            }

            if (!partMap.keySet().containsAll(oldPartMap.keySet()))
            {
                throw new IllegalStateException("Incompatible partitions.");
            }

            Map<Integer, EppWeekType> toSaveParts = new HashMap<>();
            for (Map.Entry<Integer, EppWeekType> partEntry: partMap.entrySet())
            {
                Integer part = partEntry.getKey();
                EppWeekType weekType = partEntry.getValue();
                if (weekType == null || !weekType.getCode().equals(oldPartMap.get(part)))
                {
                    toSaveParts.put(part, weekType == null ? theory : weekType);
                }
            }

            toSaveMap.put(weekId, toSaveParts);
        }

        List<Long> ids = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .column(property("w", UsmaWorkGraphRowWeek.id()))
                .where(in(property("w", UsmaWorkGraphRowWeek.id()), weekIdQuery))
                .createStatement(getSession()).list();

        List<Long> weekIds = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .column(property("w", UsmaWorkGraphRowWeek.id()))
                .where(in(property("w", UsmaWorkGraphRowWeek.id()), weekIdQuery))
                .where(in(
                        property("w", UsmaWorkGraphRowWeek.week()),
                        new DQLSelectBuilder()
                                .fromEntity(EppYearEducationWeek.class, "ew")
                                .column(property("ew", EppYearEducationWeek.number()))
                                .where(in(property("ew", EppYearEducationWeek.id()), rowWeekIds)).buildQuery()))
                .createStatement(getSession()).list();

        ids.removeAll(weekIds);

        new DQLDeleteBuilder(UsmaWorkGraphRowWeek.class).where(in(property(UsmaWorkGraphRowWeek.id()), ids)).createStatement(getSession()).execute();
        new DQLDeleteBuilder(UsmaWorkGraphRowWeekPart.class).where(in(property(UsmaWorkGraphRowWeekPart.week().id()), rowWeekPartToDelWeekIds)).createStatement(getSession()).execute();

        Map<Integer, UsmaWorkGraphRowWeek> weekNumberToWeekMap = new HashMap<>();
        DQLSelectBuilder weekBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .where(in(property("w", UsmaWorkGraphRowWeek.id()), weekIdQuery));

        for (UsmaWorkGraphRowWeek week: weekBuilder.createStatement(getSession()).<UsmaWorkGraphRowWeek>list())
        {
            weekNumberToWeekMap.put(week.getWeek(), week);
        }

        Map<UsmaWorkGraphRowWeek, Map<Integer, UsmaWorkGraphRowWeekPart>> oldWeekParts = new HashMap<>();
        DQLSelectBuilder weekPartBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeekPart.class, "wp")
                .where(in(property("wp", UsmaWorkGraphRowWeekPart.week().id()), weekIdQuery));

        for (UsmaWorkGraphRowWeekPart weekPart: weekPartBuilder.createStatement(getSession()).<UsmaWorkGraphRowWeekPart>list())
        {
            SafeMap.safeGet(oldWeekParts, weekPart.getWeek(), HashMap.class).put(weekPart.getPart(), weekPart);
        }

        for (Map.Entry<Long, Map<Integer, EppWeekType>> weekEntry: toSaveMap.entrySet())
        {
            Long weekId = weekEntry.getKey();
            Map<Integer, EppWeekType> partMap = weekEntry.getValue();
            if (partMap.isEmpty()){continue;}
            Integer weekNumber = weekIdToNumber.get(weekId);

            UsmaWorkGraphRowWeek week = weekNumberToWeekMap.get(weekNumber);
            if (week == null)
            {
                week = new UsmaWorkGraphRowWeek(workGraphRow, weekNumber);
                if (!rowWeekToCreateIds.contains(weekId))
                {
                    week.setType(theory);
                }
            }

            Term newTerm = weekTermMap.get(weekNumber);
            if (week.getTerm() == null || !week.getTerm().equals(newTerm))
            {
                week.setTerm(newTerm);
            }

            if (partMap.containsKey(0))
            {
                week.setType(partMap.get(0) == null ? theory : partMap.get(0));
                saveOrUpdate(week);

            } else
            {
                if (week.getType() != null)
                {
                    week.setType(null);
                    update(week);
                }

                Map<Integer, UsmaWorkGraphRowWeekPart> weekPartMap = oldWeekParts.get(weekNumberToWeekMap.get(weekIdToNumber.get(weekId)));
                for (Map.Entry<Integer, EppWeekType> weekTypeEntry: partMap.entrySet())
                {
                    Integer part = weekTypeEntry.getKey();
                    UsmaWorkGraphRowWeekPart weekPart = weekPartMap == null ? new UsmaWorkGraphRowWeekPart(week, part) : weekPartMap.get(part);
                    weekPart.setType(weekTypeEntry.getValue());
                    saveOrUpdate(weekPart);
                }
            }
        }
    }


    @Override
    public void updateCombineRows(Long templateRowId, Set<Long> selectedIds)
    {
        if (selectedIds.size() <= 1)
        {
            throw new ApplicationException("Объединяемых строк должно быть больше одной.");
        }
        if (templateRowId == null)
        {
            throw new ApplicationException("Не выбран шаблон объединения.");
        }
        if (!selectedIds.contains(templateRowId))
        {
            throw new ApplicationException("Шаблон строки не входит в список объединяемых строк.");
        }

        Session session = getSession();
        List<IEntity> forSave = new ArrayList<>();
        List<UsmaWorkGraphRowWeekPart> partsForSave = new ArrayList<>();

        UsmaWorkGraphRow templateRow = this.getNotNull(UsmaWorkGraphRow.class, templateRowId);

        // создаем новую строку ГУП.
        UsmaWorkGraphRow row = new UsmaWorkGraphRow();
        row.setGraph(templateRow.getGraph());
        row.setCourse(templateRow.getCourse());
        forSave.add(row);

        // получаем все EduPlanVersion у объединяемых строк
        List<EppEduPlanVersion> eduPlanVersionList = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, "epv")
                .where(in(
                        property("epv.id"),
                        new DQLSelectBuilder()
                                .fromEntity(UsmaWorkGraphRow2EduPlan.class, "r")
                                .column(property(UsmaWorkGraphRow2EduPlan.eduPlanVersion().id().fromAlias("r")))
                                .where(in(property(UsmaWorkGraphRow2EduPlan.row().id().fromAlias("r")), selectedIds))
                                .buildQuery()))
                .createStatement(session).list();

        // сохраняем такие связи
        for (EppEduPlanVersion eduPlanVersion : eduPlanVersionList)
        {
            UsmaWorkGraphRow2EduPlan row2EduPlan = new UsmaWorkGraphRow2EduPlan();
            row2EduPlan.setRow(row);
            row2EduPlan.setEduPlanVersion(eduPlanVersion);
            forSave.add(row2EduPlan);
        }

        IDQLSelectableQuery weekQuery = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .column(property("w", UsmaWorkGraphRowWeek.id()))
                .where(eq(property("w", UsmaWorkGraphRowWeek.row().id()), value(templateRowId)))
                .buildQuery();

        Map<Integer, Map<Integer, EppWeekType>> weekPartsTypeMap = SafeMap.get(HashMap.class);
        DQLSelectBuilder weekPartsBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeekPart.class, "wp")
                .where(in(property("wp", UsmaWorkGraphRowWeekPart.week().id()), weekQuery));

        for (UsmaWorkGraphRowWeekPart weekPart: this.<UsmaWorkGraphRowWeekPart>getList(weekPartsBuilder))
        {
            weekPartsTypeMap.get(weekPart.getWeek().getWeek()).put(weekPart.getPart(), weekPart.getType());
        }

        // получаем ячейки шаблонной строки
        List<UsmaWorkGraphRowWeek> templateWeekList = getList(new DQLSelectBuilder().fromEntity(UsmaWorkGraphRowWeek.class, "w").where(in(property("w", UsmaWorkGraphRowWeek.id()), weekQuery)));

        // создаем ячейки в новой строке ГУП
        for (UsmaWorkGraphRowWeek item : templateWeekList)
        {
            UsmaWorkGraphRowWeek rel = new UsmaWorkGraphRowWeek();
            rel.setRow(row);
            rel.setWeek(item.getWeek());
            rel.setTerm(item.getTerm());
            rel.setType(item.getType());
            forSave.add(rel);

            if (item.getType() == null)
            {
                for (Map.Entry<Integer, EppWeekType> weekPartEntry: weekPartsTypeMap.get(item.getWeek()).entrySet())
                {
                    UsmaWorkGraphRowWeekPart weekPart = new UsmaWorkGraphRowWeekPart(rel, weekPartEntry.getKey());
                    weekPart.setType(weekPartEntry.getValue());

                    partsForSave.add(weekPart);
                }
            }
        }

        // удаляем старые строки
        new DQLDeleteBuilder(UsmaWorkGraphRow.class).where(in(property(UsmaWorkGraphRow.id()), selectedIds)).createStatement(session).execute();

        // запускаем удаление в базе + выполнение всех каскадов
        session.flush();

        // сохраняем все созданные объекты
        for (IEntity entity : forSave)
        {
            session.save(entity);
        }

        for (UsmaWorkGraphRowWeekPart part: partsForSave)
        {
            session.save(part);
        }
    }
    
    @Override
    public void updateExcludeRow(Long excludeRowId)
    {
        Session session = this.getSession();
        List<IEntity> forSave = new ArrayList<>();
        List<UsmaWorkGraphRowWeekPart> partsToSave = new ArrayList<>();

        UsmaWorkGraphRow2EduPlan row2EduPlan = this.getNotNull(UsmaWorkGraphRow2EduPlan.class, excludeRowId);

        // получаем ячейки шаблонной строки
        List<UsmaWorkGraphRowWeek> templateWeekList = getList(UsmaWorkGraphRowWeek.class, UsmaWorkGraphRowWeek.row(), row2EduPlan.getRow());
        Map<Integer, Map<Integer, EppWeekType>> weekPartsTypeMap = SafeMap.get(HashMap.class);
        for (UsmaWorkGraphRowWeekPart weekPart: getList(UsmaWorkGraphRowWeekPart.class, UsmaWorkGraphRowWeekPart.week(), templateWeekList))
        {
            weekPartsTypeMap.get(weekPart.getWeek().getWeek()).put(weekPart.getPart(), weekPart.getType());
        }

        // создаем новую строку ГУП.
        UsmaWorkGraphRow row = new UsmaWorkGraphRow();
        row.setGraph(row2EduPlan.getRow().getGraph());
        row.setCourse(row2EduPlan.getRow().getCourse());
        forSave.add(row);

        UsmaWorkGraphRow2EduPlan row2EduPlanRel = new UsmaWorkGraphRow2EduPlan();
        row2EduPlanRel.setRow(row);
        row2EduPlanRel.setEduPlanVersion(row2EduPlan.getEduPlanVersion());
        forSave.add(row2EduPlanRel);

        // создаем ячейки в новой строке ГУП
        for (UsmaWorkGraphRowWeek item : templateWeekList)
        {
            UsmaWorkGraphRowWeek rel = new UsmaWorkGraphRowWeek();
            rel.setRow(row);
            rel.setWeek(item.getWeek());
            rel.setTerm(item.getTerm());
            rel.setType(item.getType());
            forSave.add(rel);

            if (item.getType() == null)
            {
                for (Map.Entry<Integer, EppWeekType> weekPartEntry: weekPartsTypeMap.get(item.getWeek()).entrySet())
                {
                    UsmaWorkGraphRowWeekPart weekPart = new UsmaWorkGraphRowWeekPart(rel, weekPartEntry.getKey());
                    weekPart.setType(weekPartEntry.getValue());

                    partsToSave.add(weekPart);
                }
            }
        }

        // удаляем старые связи
        session.delete(row2EduPlan);

        // запускаем удаление в базе + выполнение всех каскадов
        session.flush();

        // сохраняем все созданные объекты
        for (IEntity entity : forSave)
        {
            session.save(entity);
        }

        for (UsmaWorkGraphRowWeekPart weekPart: partsToSave)
        {
            session.save(weekPart);
        }
    }
}