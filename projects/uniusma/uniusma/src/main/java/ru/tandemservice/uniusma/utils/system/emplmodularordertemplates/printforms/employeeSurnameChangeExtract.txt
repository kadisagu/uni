 \qc\fs24О смене фамилии (имени, отчества)\par\par
\fs28\fi709\qj {reasonPrint} \par\fi0\par
\ql\expnd12\expndtw60ПРИКАЗЫВАЮ:\expndtw0\expnd0\par
\par

\keep\keepn

\pard\par\fi500\qj

1.\~Считать \b{Fio}\b0 – {post_A} {orgUnit_G}, по{lastName}\b{lastNameNew_N}\b0{firstName}\b{firstNameNew_N}\b0{middleName}\b{middleNameNew_N}\b0.\par
\par
2.\~Управлению кадров и Управлению экономики, бухгалтерского учета и отчетности внести изменения в личные документы и учетные данные.

\par
\par
\'ce\'f1\'ed\'ee\'e2\'e0\'ed\'e8\'e5: {listBasics}.
\par
\par
С приказом ознакомлен (на) ______________ « ___ » _________20__ г.
\par