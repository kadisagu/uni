package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaDiseasesKind

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmadiseaseskind_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaDiseasesKind");

			// создано обязательное свойство diseasesKind
			// создать колонку
			tool.createColumn("usmastudentdiseaseinfo_t", new DBColumn("diseaseskind_id", DBType.LONG));

			Statement statement = tool.getConnection().createStatement();
			statement.execute("select distinct usmavaccinekind_t.title_p, usmavaccinekind_t.id from usmavaccinekind_t inner join usmastudentdiseaseinfo_t on usmavaccinekind_t.id = usmastudentdiseaseinfo_t.vaccinekind_id order by title_p");

			PreparedStatement insertKind = tool.prepareStatement("insert into usmadiseaseskind_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)");
			PreparedStatement updateStmt = tool.prepareStatement("update usmastudentdiseaseinfo_t set diseaseskind_id = ? where usmastudentdiseaseinfo_t.vaccinekind_id = ?");

			ResultSet insertRow = statement.getResultSet();

			Integer code = 0;
			int counter = 0;
			// Заполняем справочник "usmadiseaseskind_t" записями из таблицы
			while (insertRow.next())
			{
				String title = insertRow.getString(1);
				Long vacKindId = insertRow.getLong(2);

				Long id = EntityIDGenerator.generateNewId(entityCode);
				insertKind.setLong(1, id);
				insertKind.setShort(2, entityCode);
				insertKind.setString(3, String.valueOf(code++));
				insertKind.setString(4, title);
				insertKind.addBatch();
				// Заполняем поле diseaseskind_id значениями из справочника
				updateStmt.setLong(1, id);
				updateStmt.setLong(2, vacKindId);
				updateStmt.addBatch();

				if (++counter % 100 == 0)
				{
					insertKind.executeBatch();
					updateStmt.executeBatch();
				}
			}
			insertKind.executeBatch();
			updateStmt.executeBatch();

			// сделать колонку NOT NULL
			tool.setColumnNullable("usmastudentdiseaseinfo_t", "diseaseskind_id", false);
		}

	    ////////////////////////////////////////////////////////////////////////////////
	    // сущность usmaStudentDiseaseInfo

	    // удалено свойство vaccineKind
	    {
		    // удалить колонку
		    tool.dropColumn("usmastudentdiseaseinfo_t", "vaccinekind_id");

	    }

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaStudentFluorographyInfo

		// создано свойство pictureNumber
		{
			// создать колонку
			tool.createColumn("usmastudentfluorographyinfo_t", new DBColumn("picturenumber_p", DBType.createVarchar(255)));

		}


    }
}