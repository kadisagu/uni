/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionTitleTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Collections;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        updateBlocks(component);
    }

    public void updateBlocks(IBusinessComponent component)
    {
        Model model = getModel(component);
        EppEduPlanVersionBlock block = model.getSelectedEpvBlock();

        if (block == null)
        {
            model.setBlocks(model.getBlockMap().values());

        } else
        {
            model.setBlocks(Collections.singletonList(model.getBlockMap().get(block.getId())));
        }
    }

    // Титул
    public void onClickEditTitle(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockTitleEdit",
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    // Разработчики
    public void onClickAddDeveloper(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockDeveloperAddEdit",
                new ParametersMap().add("blockId", component.getListenerParameter())));
    }

    public void onClickEditDeveloper(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockDeveloperAddEdit",
                new ParametersMap().add("developerId", component.getListenerParameter())));
    }

    public void onClickDeleteDeveloper(IBusinessComponent component)
    {
        getDao().delete(component.<Long>getListenerParameter());
        component.refresh();
    }

    // Квалификации
    public void onClickAddQualification(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockQualificationAddEdit",
                new ParametersMap().add("blockId", component.getListenerParameter())));
    }

    public void onClickEditQualification(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockQualificationAddEdit",
                new ParametersMap().add("qualificationId", component.getListenerParameter())));
    }

    public void onClickDeleteQualification(IBusinessComponent component)
    {
        getDao().delete(component.<Long>getListenerParameter());
        component.refresh();
    }

    // Специальности
    public void onClickAddSpeciality(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockSpecialityAddEdit",
                new ParametersMap().add("blockId", component.getListenerParameter())));
    }

    public void onClickEditSpeciality(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockSpecialityAddEdit",
                new ParametersMap().add("specialityId", component.getListenerParameter())));
    }

    public void onClickDeleteSpeciality(IBusinessComponent component)
    {
        getDao().delete(component.<Long>getListenerParameter());
        component.refresh();
    }
}