/**
 *$Id$
 */
package ru.tandemservice.uniusma.events.dset;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public class UsmaCompetence2EpvRegistryRowRelListener extends AbstractUsmaCompetenceRelListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, UsmaCompetence2EpvRegistryRowRel.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, UsmaCompetence2EpvRegistryRowRel.class, this);
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "row")
                .joinEntity("row", DQLJoinType.inner, UsmaCompetence2EpvRegistryRowRel.class, "comp2row", eq(property(UsmaCompetence2EpvRegistryRowRel.registryRow().id().fromAlias("comp2row")), property(EppEpvRegistryRow.id().fromAlias("row"))))
                .where(in(property(UsmaCompetence2EpvRegistryRowRel.id().fromAlias("comp2row")), event.getMultitude().getInExpression()))
                .column(property(EppEpvRegistryRow.owner().id().fromAlias("row")))
                .createStatement(event.getContext()).list();
    }
}