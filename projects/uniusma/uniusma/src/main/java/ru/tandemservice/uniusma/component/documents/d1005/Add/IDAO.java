/* $Id$ */
package ru.tandemservice.uniusma.component.documents.d1005.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public interface IDAO extends IDocumentAddBaseDAO<Model>
{

    Date getDate(Student student, DevelopGridTerm term, boolean isStartDate);
}
