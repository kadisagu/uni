/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.vo;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.utils.CommonCollator;

import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.Date;

/**
 * @author DMITRY KNYAZEV
 * @since 22.07.2014
 */
public class UsmaReportDataWrapper implements Comparator<UsmaReportDataWrapper>, Comparable<UsmaReportDataWrapper>
{
    private String _fullFio;
    private String _groupTitle;
    private String _birthDate;
    private String _flgDate;

    public UsmaReportDataWrapper(String fullFio, String groupTitle, Date birthDate, Date flgDate)
    {
        this._fullFio = fullFio == null ? "" : fullFio;
        this._groupTitle = groupTitle == null ? "" : groupTitle;
        this._birthDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(birthDate);
        this._flgDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(flgDate);
    }

    public String getFullFio()
    {
        return _fullFio;
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public String getBirthDate()
    {
        return _birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this._birthDate = birthDate;
    }

    public String getFlgDate()
    {
        return _flgDate;
    }

    @Override
    public int compare(@NotNull UsmaReportDataWrapper o1, @NotNull UsmaReportDataWrapper o2)
    {
        return o1.compareTo(o2);
    }

    @Override
    public int compareTo(UsmaReportDataWrapper o)
    {
        int res = this.getGroupTitle().compareToIgnoreCase(o.getGroupTitle());
        if (res == 0)
            return CommonCollator.RUSSIAN_COLLATOR.compare(this.getFullFio(), o.getFullFio());
        return res;
    }
}
