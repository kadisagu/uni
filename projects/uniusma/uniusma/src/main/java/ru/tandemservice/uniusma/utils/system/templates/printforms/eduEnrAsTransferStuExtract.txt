\fs28\qc\b{orderType} из другой образовательной организации Российской Федерации\b0\par
\par\fi709\qj\keep\keepn
На основании решения аттестационной комиссии (протокол № {protocolNumber} от {protocolDate} г.), в соответствии с ч. 2 ст. 30, ч. 1 ст. 34 Федерального закона от 29.12.2012 г. № 273-ФЗ «Об образовании в РФ», п. 4, 5, 7, 8 Порядка перевода студентов из одного высшего учебного заведения РФ в другое (утв. Приказом Министерства общего и профессионального образования РФ от 24.02.1998 г. № 501), Положением о порядке перевода и восстановления студентов в ГБОУ ВПО УГМУ Минздрава России,\par\par
\expnd8\expndtw40\b приказываю: \expndtw0\expnd0\b0\par\par
1. Зачислить {fio} {birthDate} года рождения, на {courseNew} курс {orgUnitNew_G} {developFormNew_GF} формы обучения на {compensationTypeStrNew_A_Alt} в порядке перевода из {university} и определить в группу {groupNew}.\line
К занятиям допустить с {entryIntoForceDate} г.\par
{debts}\par\par
\fi0\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx376
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6330
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8031
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10299
\pard\intbl\qc\'b9\cell
\'c4\'e8\'f1\'f6\'e8\'ef\'eb\'e8\'ed\'e0\cell
\'ca\'ee\'eb\'e8\'f7\'e5\'f1\'f2\'e2\'ee \'f7\'e0\'f1\'ee\'e2\cell\'c2\'e8\'e4 \'ee\'f2\'f7\'e5\'f2\'ed\'ee\'f1\'f2\'e8\cell\row
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx376
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6330
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8031
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10299
\pard\intbl\qj {T}\cell
\cell
\pard\intbl\qc\cell
\cell\row
\pard
\par\fi709\qj\keep\keepn
Основание: {listBasics}