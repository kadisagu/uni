package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Разработчик
 */
public class UsmaDeveloper extends UsmaDeveloperGen
{
    public UsmaDeveloper()
    {

    }

    public UsmaDeveloper(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}