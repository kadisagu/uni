/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionTitleTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaDeveloper;
import ru.tandemservice.uniusma.entity.UsmaEpvBlockTitle;
import ru.tandemservice.uniusma.entity.UsmaQualification;
import ru.tandemservice.uniusma.entity.UsmaSpeciality;

import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private ISelectModel _epvBlockModel;
    private EppEduPlanVersionBlock _selectedEpvBlock;
    private Map<Long, Block> _blockMap;
    private Collection<Block> _blocks;
    private Block _currentBlock;


    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public ISelectModel getEpvBlockModel()
    {
        return _epvBlockModel;
    }

    public void setEpvBlockModel(ISelectModel epvBlockModel)
    {
        _epvBlockModel = epvBlockModel;
    }

    public EppEduPlanVersionBlock getSelectedEpvBlock()
    {
        return _selectedEpvBlock;
    }

    public void setSelectedEpvBlock(EppEduPlanVersionBlock selectedEpvBlock)
    {
        _selectedEpvBlock = selectedEpvBlock;
    }

    public Map<Long, Block> getBlockMap()
    {
        return _blockMap;
    }

    public void setBlockMap(Map<Long, Block> blockMap)
    {
        _blockMap = blockMap;
    }

    public Collection<Block> getBlocks()
    {
        return _blocks;
    }

    public void setBlocks(Collection<Block> blocks)
    {
        _blocks = blocks;
    }

    public Block getCurrentBlock()
    {
        return _currentBlock;
    }

    public void setCurrentBlock(Block currentBlock)
    {
        _currentBlock = currentBlock;
    }

    public static class Block
    {
        private Long _id;
        private String _caption;
        private UsmaEpvBlockTitle _blockTitle;
        private String _level;
        private StaticListDataSource<UsmaDeveloper> _developerDataSource;
        private StaticListDataSource<UsmaQualification> _qualificationDataSource;
        private StaticListDataSource<UsmaSpeciality> _specialityDataSource;

        public Block(Long id, String caption, UsmaEpvBlockTitle blockTitle, String level, StaticListDataSource<UsmaDeveloper> developerDataSource, StaticListDataSource<UsmaQualification> qualificationDataSource, StaticListDataSource<UsmaSpeciality> specialityDataSource)
        {
            _id = id;
            _caption = caption;
            _blockTitle = blockTitle;
            _level = level;
            _developerDataSource = developerDataSource;
            _qualificationDataSource = qualificationDataSource;
            _specialityDataSource = specialityDataSource;
        }


        public Long getId()
        {
            return _id;
        }

        public String getCaption()
        {
            return _caption;
        }

        public UsmaEpvBlockTitle getBlockTitle()
        {
            return _blockTitle;
        }

        public String getLevel()
        {
            return _level;
        }

        public StaticListDataSource<UsmaDeveloper> getDeveloperDataSource()
        {
            return _developerDataSource;
        }

        public StaticListDataSource<UsmaQualification> getQualificationDataSource()
        {
            return _qualificationDataSource;
        }

        public StaticListDataSource<UsmaSpeciality> getSpecialityDataSource()
        {
            return _specialityDataSource;
        }
    }
}
