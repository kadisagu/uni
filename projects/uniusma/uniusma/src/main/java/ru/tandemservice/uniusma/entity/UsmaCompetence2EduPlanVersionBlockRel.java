package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Связь компетенции с блоком УП(в)
 */
public class UsmaCompetence2EduPlanVersionBlockRel extends UsmaCompetence2EduPlanVersionBlockRelGen
{
    public UsmaCompetence2EduPlanVersionBlockRel()
    {
    }

    public UsmaCompetence2EduPlanVersionBlockRel(EppEduPlanVersionBlock block, UsmaCompetence usmaCompetence, int number)
    {
        this.setBlock(block);
        this.setUsmaCompetence(usmaCompetence);
        this.setNumber(number);
    }
}