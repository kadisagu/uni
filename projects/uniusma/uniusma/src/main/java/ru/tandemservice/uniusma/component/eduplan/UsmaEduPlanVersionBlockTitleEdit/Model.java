/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockTitleEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniusma.entity.UsmaEpvBlockTitle;

import java.util.Collection;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private UsmaEpvBlockTitle _blockTitle;

    private Collection<IdentifiableWrapper> _levels;
    private IdentifiableWrapper _level;
    private Collection<IdentifiableWrapper> _selfWorks;
    private IdentifiableWrapper _selfWork;


    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public UsmaEpvBlockTitle getBlockTitle()
    {
        return _blockTitle;
    }

    public void setBlockTitle(UsmaEpvBlockTitle blockTitle)
    {
        _blockTitle = blockTitle;
    }

    public Collection<IdentifiableWrapper> getLevels()
    {
        return _levels;
    }

    public void setLevels(Collection<IdentifiableWrapper> levels)
    {
        _levels = levels;
    }

    public IdentifiableWrapper getLevel()
    {
        return _level;
    }

    public void setLevel(IdentifiableWrapper level)
    {
        _level = level;
    }

    public Collection<IdentifiableWrapper> getSelfWorks()
    {
        return _selfWorks;
    }

    public void setSelfWorks(Collection<IdentifiableWrapper> selfWorks)
    {
        _selfWorks = selfWorks;
    }

    public IdentifiableWrapper getSelfWork()
    {
        return _selfWork;
    }

    public void setSelfWork(IdentifiableWrapper selfWork)
    {
        _selfWork = selfWork;
    }
}