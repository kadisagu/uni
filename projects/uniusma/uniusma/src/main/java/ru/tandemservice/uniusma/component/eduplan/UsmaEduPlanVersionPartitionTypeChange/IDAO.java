/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionPartitionTypeChange;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 03.06.2013
 */
public interface IDAO extends IUniDao<Model>
{
}