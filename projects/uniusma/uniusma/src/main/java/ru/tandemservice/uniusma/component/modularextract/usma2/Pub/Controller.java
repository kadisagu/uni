/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public class Controller extends ModularStudentExtractPubController<UsmaExcludeDebtStuExtract, IDAO, Model>
{
}
