/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.enrollmentextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfComponents;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.TextAlignment;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.*;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.component.enrollmentextract.EnrollmentOrderPrint;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author vip_delete
 * @since 22.07.2009
 */
public class USMAEnrollmentOrderPrint extends EnrollmentOrderPrint
{
    @Override
    @SuppressWarnings("unchecked")
    protected void injectParagraphs(final RtfDocument document, EnrollmentOrder order)
    {
        UsmaEnrollmentOrderCard card = UniDaoFacade.getCoreDao().get(UsmaEnrollmentOrderCard.class, UsmaEnrollmentOrderCard.L_ORDER, order);

        new RtfInjectModifier()
        .put("enrollmentDate", card.getEnrollmentDate() != null ? new SimpleDateFormat("d MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(card.getEnrollmentDate()) : "")
        .put("protocolTitle", card.getProtocolTitle())
        .put("contractWith", card.getContractWith())
        .modify(document);

        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        //final Set<OrgUnit> orgUnitList = new LinkedHashSet<OrgUnit>();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfElement> parList = new ArrayList<>();

            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                List<EnrollmentExtract> enrollmentExtractList = (List<EnrollmentExtract>) paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0)
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");

                // первая выписка из параграфа
                EnrollmentExtract extract = enrollmentExtractList.get(0);

                //orgUnitList.add(extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit());

                // разбиваем на 2 группы: целевой и общий прием
                List<PreliminaryEnrollmentStudent> list = new ArrayList<>();
                for (EnrollmentExtract item : enrollmentExtractList)
                    list.add(item.getEntity());

                OrgUnit orgUnit = extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit();
                RtfInjectModifier injectModifier = new RtfInjectModifier()
                .put("parNumber", Integer.toString(paragraph.getNumber()))
                .put("formativeOrgUnit", StringUtils.isEmpty(orgUnit.getNominativeCaseTitle()) ? orgUnit.getFullTitle() : orgUnit.getNominativeCaseTitle())
                .put("developForm", extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("developPeriod", extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("educationOrgUnit", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
                UniRtfUtil.initEducationType(injectModifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");

                String buffer = getText(1, list).toString();

                // клонируем шаблон параграфа
                RtfDocument paragraphPart = parargraphDocument.getClone();
                injectModifier.modify(paragraphPart);

                RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, STUDENT_LIST);
                if (searchResult.isFound())
                {
                    IRtfText text = RtfBean.getElementFactory().createRtfText(buffer);
                    text.setRaw(true);

                    searchResult.getElementList().set(searchResult.getIndex(), text);
                }

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }

        // добавляем табличку с визой ректора

        IRtfElementFactory factory = RtfBean.getElementFactory();
        IRtfControl b = factory.createRtfControl(IRtfData.B);
        IRtfText textA = factory.createRtfText("Ректор");
        IRtfText textB = factory.createRtfText("С.М.Кутепов");
        IRtfGroup groupA = factory.createRtfGroup();
        IRtfGroup groupB = factory.createRtfGroup();
        groupA.getElementList().addAll(Arrays.asList(b, textA));
        groupB.getElementList().addAll(Arrays.asList(b, textB));

        RtfTable table = (RtfTable) factory.create(RtfComponents.TABLE);
        RtfRow row = new RtfRow();
        table.getRowList().add(row);
        RtfCell cellA = new RtfCell();
        cellA.setWidth(4643); // 8.19 см
        cellA.setElementList(Arrays.asList((IRtfElement)groupA));
        RtfCell cellB = new RtfCell();
        cellB.setWidth(4643); // 8.19 см
        cellB.setTextAlignment(TextAlignment.BOTTOM);
        cellB.setElementList(Arrays.asList(factory.createRtfControl(IRtfData.QR), groupB));
        row.getCellList().add(cellA);
        row.getCellList().add(cellB);

        document.getElementList().add(table);
    }

    private StringBuilder getText(int startIndex, List<PreliminaryEnrollmentStudent> list)
    {
        StringBuilder buffer = new StringBuilder();
        int counter = startIndex;
        for (PreliminaryEnrollmentStudent preStudent : list)
        {
            Person person = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson();

            buffer.append("\\par    ").append(counter).append(". ").append(counter < 10 ? " " : "");
            buffer.append(person.getFullFio());
            counter++;
        }
        buffer.append("\\par");
        return buffer;
    }
}
