// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.documents.d1.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;

import java.util.Calendar;

/**
 * @author oleyba
 * @since 18.02.2010
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        boolean male = model.getStudent().getPerson().getIdentityCard().getSex().isMale();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        Calendar c = Calendar.getInstance();

	    String admissionType;
	    if (model.getStudent().isTargetAdmission()) admissionType = "целевой ";
	    else admissionType = "";

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_G", academy.getGenitiveCaseTitle())
                .put("number", Integer.toString(model.getNumber()))
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("birthDate", model.getStudent().getPerson().getBirthDateStr())
                .put("sexTitle", male ? "он" : "она")
                .put("studentCase", male ? "студентом" : "студенткой")
                .put("course", model.getCourse().getTitle())
                .put("departmentTitle", model.getDepartment().getId() == 0L ? "дневном" : "вечернем")
                .put("developFormTitle", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("orgUnitTitle", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle())
                .put("orgUnitTitleCase", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())
		        .put("compensationTypeStr", model.getStudent().getCompensationType().isBudget() ? admissionType + "бюджетной" : admissionType + "договорной")
		        .put("developPeriodStr", model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("term", model.getTerm() == null ? "" : model.getTerm().toString())
                .put("secretarTitle", model.getSecretarTitle())
                .put("phoneTitle", model.getPhone())
                .put("documentForTitle", model.getDocumentForTitle())
                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())
                .put("dateStart", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEnrollmentDate()))
                .put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduEndDate()))
                .put("oN", StringUtils.trimToEmpty(model.getEnrollmentOrderNumber()));


        if (null == model.getEnrollmentOrderDate())
        {
            injectModifier
                    .put("oD", "")
                    .put("oM", "")
                    .put("oY", "");
        }
        else
        {
            c.setTime(model.getEnrollmentOrderDate());
            String day = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
            if (day.length() == 1)
                day = "0" + day;
            injectModifier
                    .put("oD", day)
                    .put("oM", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                    .put("oY", Integer.toString(c.get(Calendar.YEAR)));
        }
        {
            c.setTime(model.getFormingDate());
            String day = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
            if (day.length() == 1)
                day = "0" + day;
            injectModifier.put("day", day)
                    .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                    .put("year", Integer.toString(c.get(Calendar.YEAR)));
        }

        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "очной");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "заочной");
        else
            injectModifier.put("developFormTitle_G", "очно-заочной");

        return injectModifier;
    }
}
