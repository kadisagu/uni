package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaStudentDiseaseInfo

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmastudentdiseaseinfo_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("vaccinekind_id", DBType.LONG).setNullable(false), 
				new DBColumn("begindate_p", DBType.DATE), 
				new DBColumn("enddate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaStudentDiseaseInfo");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaStudentFluorographyInfo

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmastudentfluorographyinfo_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("date_p", DBType.DATE).setNullable(false), 
				new DBColumn("result_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaStudentFluorographyInfo");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaStudentVaccineInfo

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmastudentvaccineinfo_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("vaccinekind_id", DBType.LONG).setNullable(false), 
				new DBColumn("sign_p", DBType.createVarchar(255)), 
				new DBColumn("date_p", DBType.DATE), 
				new DBColumn("part_p", DBType.createVarchar(255)), 
				new DBColumn("periodical_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaStudentVaccineInfo");

		}


    }
}