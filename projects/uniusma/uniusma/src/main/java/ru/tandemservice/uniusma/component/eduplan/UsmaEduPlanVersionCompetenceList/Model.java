/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionCompetenceList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model
{
    private Long _id;
    private List<EppEduPlanVersionBlock> _blocks;
    private Map<EppEduPlanVersionBlock, StaticListDataSource<UsmaCompetenceListWrapper>> _dataSourceMap = new HashMap<>();
    private EppEduPlanVersionBlock _currentBlock;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public Map<EppEduPlanVersionBlock, StaticListDataSource<UsmaCompetenceListWrapper>> getDataSourceMap()
    {
        return _dataSourceMap;
    }

    public void setDataSourceMap(Map<EppEduPlanVersionBlock, StaticListDataSource<UsmaCompetenceListWrapper>> dataSourceMap)
    {
        _dataSourceMap = dataSourceMap;
    }

    public List<EppEduPlanVersionBlock> getBlocks()
    {
        return _blocks;
    }

    public void setBlocks(List<EppEduPlanVersionBlock> blocks)
    {
        _blocks = blocks;
    }

    public EppEduPlanVersionBlock getCurrentBlock()
    {
        return _currentBlock;
    }

    public void setCurrentBlock(EppEduPlanVersionBlock currentBlock)
    {
        _currentBlock = currentBlock;
    }
}