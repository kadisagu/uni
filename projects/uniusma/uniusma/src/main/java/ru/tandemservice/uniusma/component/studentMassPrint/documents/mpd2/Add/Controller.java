/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd2.Add;

import org.tandemframework.core.component.IBusinessComponent;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class Controller extends ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.Controller<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
    }
}
