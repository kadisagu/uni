/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e16;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendStuExtractExt;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 21.05.2014
 */
public class WeekendStuExtractExtPrint implements IPrintFormCreator<WeekendStuExtractExt>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendStuExtractExt extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();

        short i = 1;

        modifier.put("reason", "в связи " + extract.getReasonStr().toLowerCase());

        if (extract.isStopGrantsPaying())
        {
            modifier.put("stopGrantPayingStr", String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " прекратить выплату стипендии с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingFrom()) + " г. по " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingTo()) + " г.");
        }
        else
            tagsToDel.add("stopGrantPayingStr");

        if (extract.isAssignPayment())
        {
            modifier.put("assignPaymentStr", String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " назначить ежемесячную компенсационную выплату в соответствии с Постановлением Правительства РФ от 03.11.1994 г. № 1206 «Об утверждении порядка назначения и выплаты ежемесячных компенсационных выплат отдельным категориям граждан» в размере " +
                    extract.getPaymentValue() + " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPaymentAssignDate()) + " г.");
        }
        else
            tagsToDel.add("assignPaymentStr");

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}
