/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.imtsa;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SafeMap;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Zhebko
 * @since 17.05.2013
 */
public class ImtsaLoader extends ImtsaLogable
{
    private static final List<String> SUMMABLE_LOAD_TYPES = Arrays.asList(ImtsaFile.TYPE_LOAD_A_LECTURES, ImtsaFile.TYPE_LOAD_A_PRACTICE, ImtsaFile.TYPE_LOAD_A_LABORATORY, ImtsaFile.TYPE_LOAD_SELFWORK);
    private static final Integer TERM_0 = 0;
    private static final String PATH_SEPARATOR = "::";
    private static final Pattern FILE_PATTERN = Pattern.compile("^([0-9A-Za-z]+)-([0-9A-Za-z]+)[.]xml$");

    // костыль
    static final String PATH = ApplicationRuntime.getAppInstallPath() + "/data";
    static final String XML_PATH = PATH + "/xml";

    private static File localConfig;
    static File getLocalConfig()
    {
        try
        {
            if (localConfig == null)
            {
                File dir = new File(XML_PATH);
                if (!dir.exists() && !dir.mkdir())
                {
                    throw new IllegalStateException("No «xml» folder found.");
                }

                File file = new File(dir, "codes.properties");
                if (!file.exists())
                {
                    file.createNewFile();
                }

                localConfig = file;
            }

            return localConfig;

        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static void loadSingle(String epvId, String eduHsId, InputStream imtsaInputStream) throws IOException, ConfigurationException
    {
        // грузим mdb (создаем бакап)
        MdbFile mdb = prepareMdbForOpen();
        boolean importSchedule = !mdb.getIgnoredScheduleEpvIds().contains(epvId);

        try
        {
            // грузим данные из mdb
            final MdbFile.MdbEpv epv = mdb.get(epvId);

            ImtsaFile.errorList.clear();
            ImtsaFile imtsa = ImtsaFile.parse(imtsaInputStream, importSchedule);
            processImtsa(imtsa, epv, epvId, eduHsId, mdb);

        }  finally
        {
            ImtsaFile.saveGlobalNodeCodeMap();
        }

        saveImtsa(mdb, Collections.<String>emptySet(), Collections.<String>emptyList());
    }

    public static void load()
    {
        final File dir = new File(XML_PATH);

        if (!dir.isDirectory())
        {
            throw new IllegalStateException("No «xml» folder found.");
        }

        // грузим mdb (создаем бакап)
        final MdbFile mdb = prepareMdbForOpen();

        // список файлов для импорта
        final FilenameFilter filter = new FilenameFilter()
        {
            @Override
            public boolean accept(final File dir, final String name)
            {
                return (StringUtils.trimToEmpty(name).endsWith(".xml") && FILE_PATTERN.matcher(name).find());
            }
        };


        log4j_logger.info("\n\n\n====================================================================\n-    ОБРАБОТКА ФАЙЛОВ    -------------------------------------------\n");

        // поехали обрабатывать файлы
        final Set<String> epvIgnore = new HashSet<>();
        final List<String> errorFiles = new ArrayList<>();

        try
        {
            // грузим файлы, группируем их о УП(в), сортируем внутри УП(в) так, что основной блок был первым

            log4j_logger.info("\n\n\n====================================================================\n-    ОБРАБОТКА ФАЙЛОВ (1/2)    -------------------------------------\n");

            final Map<MdbFile.MdbEpv, Map<String, File>> epvMap = new HashMap<>();
            for (final File file : dir.listFiles(filter))
            {
                log4j_logger.info("\n\n");

                final Matcher matcher = FILE_PATTERN.matcher(file.getName());
                if (!matcher.matches())
                {
                    // wtf - что за левый файл (не ошибка, просто не сработал фильтр)
                    continue;
                }

                final String epvId = matcher.group(1);
                final String eduHsId = matcher.group(2);

                try
                {

                    // грузим данные из mdb
                    final MdbFile.MdbEpv epv = mdb.get(epvId);

                    // сортируем УПВ и их блоки
                    Map<String, File> eduHsMap;
                    if (null == (eduHsMap = epvMap.get(epv)))
                    {
                        epvMap.put(epv, eduHsMap = new TreeMap<>());
                    }

                    final String key = (epv.getEduHsRoot().equals(eduHsId) ? StringUtils.EMPTY : eduHsId); // пустая строка будет в самом начале
                    if (null != eduHsMap.put(key, file))
                    {
                        throw new IllegalStateException();
                    }

                } catch (final Throwable t)
                {
                    // если происходит ошибка, то мы игнорируем ВСЕ файлы УПВ (УПВ либо грузится полностью, либо не грузится вообще)
                    log4j_logger.error("Ошибка при обработке файла «" + file.getAbsolutePath() + "», УПВ «" + epvId + "» не будет загружен:\n" + t.getMessage(), t);
                    epvIgnore.add(epvId);
                    errorFiles.add(file.getAbsolutePath());
                }
            }

            // а теперь грузим то, что выбрали

            log4j_logger.info("\n\n\n====================================================================\n-    ОБРАБОТКА ФАЙЛОВ (2/2)    -------------------------------------\n");

            for (final Map.Entry<MdbFile.MdbEpv, Map<String, File>> epvEntry : epvMap.entrySet())
            {
                final MdbFile.MdbEpv epv = epvEntry.getKey();
                final String epvId = epv.getEpvId();
                if (epvIgnore.contains(epvId))
                {
                    // уже баги, пропускаем
                    continue;
                }

                for (final Map.Entry<String, File> eduHsEntry : epvEntry.getValue().entrySet())
                {
                    final File file = eduHsEntry.getValue();
                    final String eduHsId = (StringUtils.isEmpty(eduHsEntry.getKey()) ? epv.getEduHsRoot() : eduHsEntry.getKey());
                    final boolean importSchedule = !mdb.getIgnoredScheduleEpvIds().contains(epvId);
                    final Map<Integer, Integer> termsCount = mdb.getDevelopGridMetaMap().get(epvId);

                    try
                    {
                        // парсим ИМЦА
                        final ImtsaFile imtsa = ImtsaFile.parse(file, importSchedule);
                        processImtsa(imtsa, epv, epvId, eduHsId, mdb);

                    } catch (final Throwable t)
                    {
                        // если происходит ошибка, то мы игнорируем ВСЕ файлы УПВ (УПВ либо грузится полностью, либо не грузится вообще)
                        log4j_logger.error("Ошибка при обработке файла «" + file.getAbsolutePath() + "», УПВ «" + epvId + "» не будет загружен:\n" + t.getMessage(), t);
                        epvIgnore.add(epvId);
                        errorFiles.add(file.getAbsolutePath());
                    }

                    log4j_logger.info("\n\n");
                }
            }

        } finally
        {
            ImtsaFile.saveGlobalNodeCodeMap();
        }

        log4j_logger.info("\n\n\n-    ОБРАБОТКА ФАЙЛОВ: завершена    --------------------------------\n====================================================================\n");

        log4j_logger.info("\n\n\n\n");

        saveImtsa(mdb, epvIgnore, errorFiles);
    }


    private static void processImtsa(ImtsaFile imtsa, MdbFile.MdbEpv epv, String epvId, String eduHsId, MdbFile mdb)
    {
        if (null == imtsa)
        {
            // увы, распарчить не смогли - смотрим лог файл
            throw new IllegalStateException("Ошибка при обработке файла ИМЦА.");
        }


        // компетенции
        for (ImtsaFile.ImtsaCompetence competence : imtsa.competences)
        {
            epv.addCompetence(eduHsId, competence.getSkillGroup(), competence.getNumber(), competence.getContent());
        }


        // учебный график
        Set<MdbFile.MdbSchedule> schedules = new HashSet<>();
        Map<Integer, Set<Integer>> course2termsMap = SafeMap.get(HashSet.class);
        for (ImtsaFile.ImtsaSchedule imtsaSchedule: imtsa.schedules)
        {
            MdbFile.MdbSchedule schedule = MdbFile.MdbSchedule.get(imtsaSchedule.getCourse(), imtsaSchedule.getTerm(), imtsaSchedule.getPart(), imtsaSchedule.getSchedule());
            schedules.add(schedule);

            course2termsMap.get(imtsaSchedule.getCourse()).add(imtsaSchedule.getTerm());
        }

        Map<Integer, Integer> epvDGMetaMap = mdb.getDevelopGridMetaMap().get(epvId);
        for (Map.Entry<Integer, Set<Integer>> courseEntry: course2termsMap.entrySet())
        {
            Integer course = courseEntry.getKey();
            if (courseEntry.getValue().size() != epvDGMetaMap.get(course))
            {
                throw new IllegalStateException("Набор семестров " + course + " курса не совпадает с учебной сеткой учебного плана.");
            }
        }

        if (!epv.checkScheduleAndFillIfEmpty(schedules))
        {
            throw new IllegalStateException("В разных файлах указан разный учебный график для одной версии учебного плана.");
        }

        // титул
        epv.addTitle(eduHsId, imtsa.blockTitle);

        // разработчики
        for (ImtsaFile.ImtsaDeveloper developer: imtsa.developers)
        {
            epv.addDeveloper(eduHsId, developer.getNumber(), developer.getFio(), developer.getPost());
        }

        // квалификации
        for (ImtsaFile.ImtsaQualification qualification: imtsa.qualifications)
        {
            epv.addQualification(eduHsId, qualification.getNumber(), qualification.getTitle(), qualification.getDevelopPeriod());
        }

        // специальности
        for (ImtsaFile.ImtsaSpeciality speciality: imtsa.specialities)
        {
            epv.addSpeciality(eduHsId, speciality.getNumber(), speciality.getTitle());
        }

        List<ImtsaFile.ImtsaNode> variantDisciplineNodeList = new ArrayList<>();

        // обрабатываем ноды
        for (final ImtsaFile.ImtsaNode node : imtsa)
        {
            final String path = path(node, epvId, eduHsId);

            if (node instanceof ImtsaFile.ImtsaDiscipline)
            {
                if (node.getParent() instanceof ImtsaFile.ImtsaVariant)
                {
                    variantDisciplineNodeList.add(node);
                    continue;
                }

                final MdbFile.MdbRowDiscipline row = epv.add(eduHsId, path.split(PATH_SEPARATOR), MdbFile.MdbRowDiscipline.class);
                final ImtsaFile.ImtsaDiscipline imtsaDiscipline = (ImtsaFile.ImtsaDiscipline) node;
                row.setKind(imtsaDiscipline.getKind());
                row.setTitle(imtsaDiscipline.getTitle());

                // код подразделения
                final String orgUnitCode = StringUtils.trimToNull(imtsaDiscipline.getOrgUnitCode());
                if (null != orgUnitCode)
                {
                    final String ownerId = mdb.getOwnerId(orgUnitCode);
                    if (null == ownerId)
                    {
                        log4j_logger.warn("Не удается найти читающее подразделение с кодом «" + orgUnitCode + "»: " + node.getImtsaIndex() + ".");
                    }
                    row.setOwnerId(ownerId);
                }

                row.getCompetences().addAll(imtsaDiscipline.getCompetences());

                // заполняем нагрузку
                fill(row, imtsaDiscipline.getTerm2loadMap());
            } else if (node instanceof ImtsaFile.ImtsaVariant)
            {
                final MdbFile.MdbRowVariant row = epv.add(eduHsId, path.split(PATH_SEPARATOR), MdbFile.MdbRowVariant.class);
                final ImtsaFile.ImtsaVariant imtsaVariant = (ImtsaFile.ImtsaVariant) node;
                row.setTitle(imtsaVariant.getTitle());

                // нагрузка по каждой дисциплине будет загружена отдельно (при обработке строк дисциплин)

                // здесь же надо продублировать эту нагрузку в дисциплину по выбору
                // подразумевается, что дисциплина по выбору - это выбор из группы одной дисциплины
                // проблема в том, что часы могут отличаться - в этом случае берем максимальные часы (если часы совпадают - то они же будут и в самой дисциплине по выбору)

                final Map<Integer, Map<String, Number>> variantTerm2loadMap = new TreeMap<>();
                for (final ImtsaFile.ImtsaNode child : imtsaVariant.getChildNodes())
                {
                    if (child instanceof ImtsaFile.ImtsaDiscipline)
                    {
                        final ImtsaFile.ImtsaDiscipline childDiscipline = (ImtsaFile.ImtsaDiscipline) child;
                        for (final Map.Entry<Integer, Map<String, Number>> e : childDiscipline.getTerm2loadMap().entrySet())
                        {
                            if ((e.getKey().intValue() > 0) && (null != e.getValue()))
                            {
                                for (final Map.Entry<String, Number> ee : e.getValue().entrySet())
                                {
                                    if ((null != ee.getValue()) && (ee.getValue().doubleValue() > 0))
                                    {
                                        Map<String, Number> map = variantTerm2loadMap.get(e.getKey());
                                        if (null == map)
                                        {
                                            variantTerm2loadMap.put(e.getKey(), map = new LinkedHashMap<>());
                                        }

                                        Number v = map.get(ee.getKey());
                                        if (null == v)
                                        {
                                            v = ee.getValue();
                                        } else if (v instanceof Double)
                                        {
                                            if (!v.equals(v = Math.max(v.doubleValue(), ee.getValue().doubleValue())))
                                            {
                                                log4j_logger.warn("Не совпадают часы в дисциплинах по выбору: " + imtsaVariant.getImtsaIndex() + ".");
                                            }
                                        } else if (v instanceof Integer)
                                        {
                                            if (!v.equals(v = Math.max(v.intValue(), ee.getValue().intValue())))
                                            {
                                                log4j_logger.warn("Не совпадают часы в дисциплинах по выбору: " + imtsaVariant.getImtsaIndex() + ".");
                                            }
                                        } else
                                        {
                                            throw new ClassCastException("Unexpected class:" + v.getClass().toString() + " .");
                                        }
                                        map.put(ee.getKey(), v);
                                    }
                                }
                            }
                        }
                    } else
                    {
                        throw new IllegalStateException("В группе дисциплин по выбору «" + imtsaVariant.getImtsaIndex() + "» содержится недопустимая строка «" + child.getImtsaIndex() + "»: " + child.getClass().getSimpleName() + "».");
                    }
                }

                for (final ImtsaFile.ImtsaNode child : imtsaVariant.getChildNodes())
                {

                    if (child instanceof ImtsaFile.ImtsaDiscipline)
                    {
                        final ImtsaFile.ImtsaDiscipline childDiscipline = (ImtsaFile.ImtsaDiscipline) child;

                        Map<Integer, Map<String, Number>> childDisciplineTerm2LoadMap = childDiscipline.getTerm2loadMap();
                        for (Map.Entry<Integer, Map<String, Number>> e: variantTerm2loadMap.entrySet())
                        {
                            Integer termNumber = e.getKey();
                            Map<String, Number> termLoadMap = SafeMap.safeGet(childDisciplineTerm2LoadMap, termNumber, HashMap.class);
                            for (Map.Entry<String, Number> ee: e.getValue().entrySet())
                            {
                                termLoadMap.put(ee.getKey(), ee.getValue());
                            }
                        }

                    } else
                    {
                        throw new IllegalStateException("В группе дисциплин по выбору «" + imtsaVariant.getImtsaIndex() + "» содержится недопустимая строка «" + child.getImtsaIndex() + "»: " + child.getClass().getSimpleName() + "».");
                    }
                }

                // заполняем нагрузку
                fill(row, variantTerm2loadMap);
            } else if (node instanceof ImtsaFile.ImtsaGroup)
            {
                final MdbFile.MdbRowGroup row = epv.add(eduHsId, path.split(PATH_SEPARATOR), MdbFile.MdbRowGroup.class);
                row.setTitle(((ImtsaFile.ImtsaGroup) node).getTitle());
            } else
            {
                final MdbFile.MdbRowStructure row = epv.add(eduHsId, path.split(PATH_SEPARATOR), MdbFile.MdbRowStructure.class);
                row.setCode(mdb.checkStructureCode(getStructureNodeCatalogCode(node)));
            }
        }

        for (ImtsaFile.ImtsaNode node: variantDisciplineNodeList)
        {
            final String path = path(node, epvId, eduHsId);

            final MdbFile.MdbRowDiscipline row = epv.add(eduHsId, path.split(PATH_SEPARATOR), MdbFile.MdbRowDiscipline.class);
            final ImtsaFile.ImtsaDiscipline imtsaDiscipline = (ImtsaFile.ImtsaDiscipline) node;
            row.setKind(imtsaDiscipline.getKind());
            row.setTitle(imtsaDiscipline.getTitle());

            // код подразделения
            final String orgUnitCode = StringUtils.trimToNull(imtsaDiscipline.getOrgUnitCode());
            if (null != orgUnitCode)
            {
                final String ownerId = mdb.getOwnerId(orgUnitCode);
                if (null == ownerId)
                {
                    log4j_logger.warn("Не удается найти читающее подразделение с кодом «" + orgUnitCode + "»: " + node.getImtsaIndex() + ".");
                }
                row.setOwnerId(ownerId);
            }

            row.getCompetences().addAll(imtsaDiscipline.getCompetences());

            // заполняем нагрузку
            fill(row, imtsaDiscipline.getTerm2loadMap());
        }
    }

    private static void saveImtsa(MdbFile mdb, Set<String> epvIgnore, List<String> errorFiles)
    {
        // глобальная ошибка
        boolean error;

        // выводим коды
        ImtsaFile.printNodeCodeMap();

        // выводим коды
        error = ImtsaFile.printNodeCodeMapErrors();

        log4j_logger.info("\n\n\n====================================================================\n-    СОХРАНЕНИЕ    -------------------------------------------------\n");

        for (final MdbFile.MdbEpv epv : mdb.getEpvMap().values())
        {
            if (epvIgnore.contains(epv.getEpvId()))
            {
                error |= true;
                continue;
            }

            epv.flush();
            log4j_logger.info("УПВ «" + epv.getEpvId() + "» сохранен");
        }

        log4j_logger.info("\n\n\n-    СОХРАНЕНИЕ: завершено    --------------------------------------\n====================================================================\n");


        // это УПВ с ошибками
        if (epvIgnore.size() > 0)
        {
            log4j_logger.error("");
            log4j_logger.error("---- УПВ, содержащие ошибки ----------------------------------------");
            for (final String f : epvIgnore)
            {
                log4j_logger.error(f);
                error = true;
            }
            log4j_logger.error("");
        }

        // это файлы с ошибками
        if (errorFiles.size() > 0)
        {
            log4j_logger.error("");
            log4j_logger.error("---- файлы, содержащие ошибки --------------------------------------");
            for (final String f : errorFiles)
            {
                log4j_logger.error(f);
                error = true;
            }
            log4j_logger.error("");
        }
    }


    private static MdbFile prepareMdbForOpen()
    {
        try
        {
            // открываем оригинал
            final File mdb_orig_file = new File(PATH, "eduplan_template.mdb");
            if (!mdb_orig_file.exists())
            {
                throw new IllegalStateException("Np «" + mdb_orig_file.getName() + "» file exists.");
            }

            // копируем его в рабочую копию
            final File mdb_target_file = new File(PATH, "eduplan_data.mdb");
            if (mdb_target_file.exists())
            {
                mdb_target_file.delete();
            }

            // копируем файл (далее - работаем с копией)
            FileUtils.copyFile(mdb_orig_file, mdb_target_file);

            // загружаем базу данных
            return new MdbFile(mdb_target_file);

        } catch (final Throwable t)
        {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    private static void fill(final MdbFile.MdbRow row, final Map<Integer, Map<String, Number>> term2loadMap)
    {
        // суммируем нагрузку по семестрам (по строке)
        {
            Map<String, Number> term0map = term2loadMap.get(TERM_0);
            if (null == term0map)
            {
                term2loadMap.put(TERM_0, term0map = new LinkedHashMap<>());
            }

            for (final String code : SUMMABLE_LOAD_TYPES)
            {
                term0map.put(code, 0d);
            }
            for (final Map.Entry<Integer, Map<String, Number>> e : term2loadMap.entrySet())
            {
                if (e.getKey().intValue() > 0)
                {
                    for (final String code : SUMMABLE_LOAD_TYPES)
                    {
                        final Number number = e.getValue().get(code);
                        if (null != number)
                        {
                            term0map.put(code, term0map.get(code).doubleValue() + number.doubleValue());
                        }
                    }
                }
            }
        }

        // сохраняем нагрузку и формы контроля в mdb
        for (final Map.Entry<Integer, Map<String, Number>> e : term2loadMap.entrySet())
        {
            final Map<String, Object> termLoadMap = row.getTermLoadMap(e.getKey());
            load(termLoadMap, "size_p", e.getValue(), ImtsaFile.TYPE_LOAD_TOTAL);
            load(termLoadMap, "labor_p", e.getValue(), ImtsaFile.TYPE_LOAD_LABOR);
            load(termLoadMap, "e_self_p", e.getValue(), ImtsaFile.TYPE_LOAD_SELFWORK);
            load(termLoadMap, "weeks_p", e.getValue(), ImtsaFile.TYPE_LOAD_WEEKS);

            final double alec = load(termLoadMap, "a_lect_p", e.getValue(), ImtsaFile.TYPE_LOAD_A_LECTURES);
            final double apra = load(termLoadMap, "a_pract_p", e.getValue(), ImtsaFile.TYPE_LOAD_A_PRACTICE);
            final double alab = load(termLoadMap, "a_lab_p", e.getValue(), ImtsaFile.TYPE_LOAD_A_LABORATORY);
            termLoadMap.put("e_audit_p", Double.valueOf(alec + apra + alab).toString());

            // ? weeks ? //

            action(
                    termLoadMap,
                    "actions_p",
                    e.getValue(),
                    ImtsaFile.TYPE_CONTROL_EXAM + ":Э",
                    ImtsaFile.TYPE_CONTROL_TEST + ":З",
                    ImtsaFile.TYPE_CONTROL_COURSE_PROJECT + ":КП",
                    ImtsaFile.TYPE_CONTROL_COURSE_WORK + ":КР"
            );
        }
    }

    private static double load(final Map<String, Object> termLoadMap, final String column, final Map<String, Number> value, final String param)
    {
        Double v = (Double) value.get(param);
        if (null == v)
        {
            v = 0d;
        }

        if (null != termLoadMap.put(column, v.toString()))
        {
            throw new IllegalStateException();
        }

        return v.doubleValue();
    }

    private static void action(final Map<String, Object> termLoadMap, final String column, final Map<String, Number> value, final String... params)
    {
        final StringBuilder sb = new StringBuilder();
        for (final String param : params)
        {
            final String[] p = param.split(":");

            final Number n = value.get(p[0]);
            if ((null != n) && (n.intValue() > 0))
            {
                for (int i = 0; i < n.intValue(); i++)
                {
                    if (sb.length() > 0)
                    {
                        sb.append(", ");
                    }
                    sb.append(p[1]);
                }
            }
        }

        if (null != termLoadMap.put(column, sb.toString()))
        {
            throw new IllegalStateException();
        }
    }

    /**
     * @return уникальный путь (по которому будет построен иерархический индекс)
     */
    private static String path(final ImtsaFile.ImtsaNode node, final String epvId, final String eduHsId)
    {
        if (null == node)
        {
            return "";
        }
        if (StringUtils.isBlank(node.getHierarchyIndex()))
        {
            return "";
        }

        final String path = StringUtils.trimToNull(path(node.getParent(), epvId, eduHsId));

        if (node instanceof ImtsaFile.ImtsaDiscipline)
        {
            return ((null == path ? "" : (path + PATH_SEPARATOR)) + eduHsId + "." + node.getNumber());
        } else if (node instanceof ImtsaFile.ImtsaGroup)
        {
            return ((null == path ? "" : (path + PATH_SEPARATOR)) + eduHsId + "." + node.getNumber());
        } else if (node instanceof ImtsaFile.ImtsaVariant)
        {
            return ((null == path ? "" : (path + PATH_SEPARATOR)) + eduHsId + "." + node.getNumber());
        }

        // здесь именно id-УПВ (т.к. структурные элементы общие и не бъются по блокам)
        return ((null == path ? "" : (path + PATH_SEPARATOR)) + epvId + "." + getStructureNodeCatalogCode(node));
    }


    /**
     * @return для структурный элементов вытаскивает код справочника (согласно правилам иментования таких элементов в ImtsaFile)
     */
    private static String getStructureNodeCatalogCode(final ImtsaFile.ImtsaNode node)
    {
        final String codePath = StringUtils.substringBefore(node.getTitle(), ";");
        final String[] paths = codePath.split(",");
        return paths[paths.length - 1].trim();
    }
}