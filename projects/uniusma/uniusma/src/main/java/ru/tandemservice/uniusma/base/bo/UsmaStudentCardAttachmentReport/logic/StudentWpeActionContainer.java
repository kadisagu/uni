/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 21.01.2016
 */
public final class StudentWpeActionContainer
{

    private final EppYearEducationProcess eduYear;
    private final Course course;
    private final Student student;
    private final Map<Long, SessionMark> sessionMarkMap = new HashMap<>();
    private final Map<Long, SessionDocument> sessionDocument = new HashMap<>();

    private final Collection<EppStudentWpeCAction> firstTermWpe;
    private final Collection<EppStudentWpeCAction> secondTermWpe;
    private final int firstTerm;
    private final int secondTerm;

    StudentWpeActionContainer(EppYearEducationProcess eduYear, Course course, Student student, Term term)
    {
        this.eduYear = eduYear;
        this.course = course;
        this.student = student;
        this.firstTermWpe = new ArrayList<>();
        this.secondTermWpe = new ArrayList<>();
        if (isFistTerm(term.getIntValue()))
        {
            this.firstTerm = term.getIntValue();
            this.secondTerm = this.firstTerm + 1;
        } else
        {
            this.secondTerm = term.getIntValue();
            this.firstTerm = this.secondTerm - 1;
        }
    }

    void add(EppStudentWpeCAction studentWpeAction, SessionDocument sessionBulletinDocument, SessionMark sessionMark)
    {
        sessionMarkMap.put(studentWpeAction.getId(), sessionMark);
        sessionDocument.put(studentWpeAction.getId(), sessionBulletinDocument);
        final EppStudentWorkPlanElement studentWpe = studentWpeAction.getStudentWpe();
        final int term = studentWpe.getTerm().getIntValue();

        if (isFistTerm(term))
            this.firstTermWpe.add(studentWpeAction);
        else
            this.secondTermWpe.add(studentWpeAction);

    }

    public Student getStudent()
    {
        return student;
    }

    public EppYearEducationProcess getEduYear()
    {
        return eduYear;
    }

    public Course getCourse()
    {
        return course;
    }

    public Map<Long, SessionMark> getSessionMarkMap()
    {
        return Collections.unmodifiableMap(sessionMarkMap);
    }

    public Map<Long, SessionDocument> getSessionDocumentMap()
    {
        return Collections.unmodifiableMap(sessionDocument);
    }

    private List<EppStudentWpeCAction> getSecondTermWpe()
    {
        return new ArrayList<>(secondTermWpe);
    }

    public List<EppStudentWpeCAction> getSecondTermWpeSorted(Comparator<EppStudentWpeCAction> comparator)
    {
        final List<EppStudentWpeCAction> newList = getSecondTermWpe();
        Collections.sort(newList, comparator);
        return newList;
    }

    private List<EppStudentWpeCAction> getFirstTermWpe()
    {
        return new ArrayList<>(firstTermWpe);
    }

    public Collection<EppStudentWpeCAction> getFirstTermWpeSorted(Comparator<EppStudentWpeCAction> comparator)
    {
        final List<EppStudentWpeCAction> newList = getFirstTermWpe();
        Collections.sort(newList, comparator);
        return newList;
    }

    public Collection<EppStudentWpeCAction> getAllWpe()
    {
        return new ArrayList<EppStudentWpeCAction>()
        {{
            addAll(firstTermWpe);
            addAll(secondTermWpe);
        }};
    }

    public int getFirstTerm()
    {
        return firstTerm;
    }

    public int getSecondTerm()
    {
        return secondTerm;
    }

    private boolean isFistTerm(int term)
    {
        return term % 2 != 0;
    }
}
