/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.Pub;

import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class Model extends ModularStudentExtractPubModel<UsmaExcludeAfterAcadWeekendStuExtract>
{
    private StudentStatus _studentStatusNew;
    private ISingleSelectModel _acadWeekendOrderModel;

    public ISingleSelectModel getAcadWeekendOrderModel()
    {
        return _acadWeekendOrderModel;
    }

    public void setAcadWeekendOrderModel(ISingleSelectModel acadWeekendOrderModel)
    {
        _acadWeekendOrderModel = acadWeekendOrderModel;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }
}
