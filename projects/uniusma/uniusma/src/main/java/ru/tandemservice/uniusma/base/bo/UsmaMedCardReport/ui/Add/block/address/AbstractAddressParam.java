/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressStreet;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.UsmaMedCardReportPrinter;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.util.IUsmaDQLReportModifier;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 04.02.2015
 */
abstract class AbstractAddressParam implements IUsmaDQLReportModifier
{
    private IReportParam<List<AddressCountry>> _country = new ReportParam<>();
    private IReportParam<List<AddressItem>> _settlement = new ReportParam<>();
    private IReportParam<List<AddressStreet>> _street = new ReportParam<>();

    private String _house = null;//дом
    private String _houseUnit = null;//корпус -почти улица-
    private String _flat = null;//квартира -фонарь-

    abstract protected MetaDSLPath getStudentAddressPath();

    abstract protected String getParameterName();

    @Override
    public void modify(String alias, DQLSelectBuilder dql)
    {
        if (addressUsed())
        {
            String aliasAddressRu = "addressRu";
            DQLSelectBuilder addressBuilder = new DQLSelectBuilder().fromEntity(AddressRu.class, aliasAddressRu).where(eq(property(aliasAddressRu, AddressRu.P_ID), property(alias, getStudentAddressPath())));

            if (getCountry().isActive())
                addressBuilder.where(in(property(aliasAddressRu, AddressRu.country()), getCountry().getData()));

            if (getSettlement().isActive())
                addressBuilder.where(in(property(aliasAddressRu, AddressRu.settlement()), getSettlement().getData()));

            if (getStreet().isActive())
                addressBuilder.where(in(property(aliasAddressRu, AddressRu.street()), getStreet().getData()));

            if (getStreet().isActive() && getHouse() != null)
                addressBuilder.where(likeUpper(property(aliasAddressRu, AddressRu.houseNumber()), value(CoreStringUtils.escapeLike(getHouse()))));

            if (getStreet().isActive() && getHouseUnit() != null)
                addressBuilder.where(likeUpper(property(aliasAddressRu, AddressRu.houseUnitNumber()), value(CoreStringUtils.escapeLike(getHouseUnit()))));

            if (getStreet().isActive() && getFlat() != null)
                addressBuilder.where(likeUpper(property(aliasAddressRu, AddressRu.flatNumber()), value(CoreStringUtils.escapeLike(getFlat()))));

            dql.where(exists(addressBuilder.buildQuery()));
        }
    }

    @Override
    public void addParameters(List<String> parameters)
    {
        if (addressUsed())
        {
            StringBuilder sb = new StringBuilder(getParameterName()).append(UsmaMedCardReportPrinter.CSV_SEPARATOR);
            if (getCountry().isActive())
                sb.append("Страна-").append(CommonBaseStringUtil.join(getCountry().getData(), AddressCountry.P_TITLE, ", ")).append(";");

            if (getSettlement().isActive())
                sb.append("Населенный пункт-").append(CommonBaseStringUtil.join(getSettlement().getData(), AddressItem.P_TITLE, ", ")).append(";");

            if (getStreet().isActive())
                sb.append("Улица-").append(CommonBaseStringUtil.join(getStreet().getData(), AddressStreet.P_TITLE, ", ")).append(";");

            if (getStreet().isActive() && getHouse() != null)
                sb.append("Дом-").append(getHouse()).append(";");

            if (getStreet().isActive() && getHouseUnit() != null)
                sb.append("Корпус-").append(getHouseUnit()).append(";");

            if (getStreet().isActive() && getFlat() != null)
                sb.append("Квартира-").append(getFlat()).append(";");

            parameters.add(sb.toString());
        }
    }

    private boolean addressUsed()
    {
        return isActiveAndNotEmpty(getCountry()) || isActiveAndNotEmpty(getSettlement()) || getStreet().isActive();
    }

    private <T extends Collection> boolean isActiveAndNotEmpty(IReportParam<T> param)
    {
        return param.isActive() && !param.getData().isEmpty();
    }

    //getters
    public IReportParam<List<AddressCountry>> getCountry()
    {
        return _country;
    }

    public IReportParam<List<AddressItem>> getSettlement()
    {
        return _settlement;
    }

    public IReportParam<List<AddressStreet>> getStreet()
    {
        return _street;
    }

    public String getHouse()
    {
        return _house;
    }

    public String getHouseUnit()
    {
        return _houseUnit;
    }

    public String getFlat()
    {
        return _flat;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setFlat(String flat)
    {
        _flat = flat;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setHouseUnit(String houseUnit)
    {
        _houseUnit = houseUnit;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setHouse(String house)
    {
        _house = house;
    }
}
