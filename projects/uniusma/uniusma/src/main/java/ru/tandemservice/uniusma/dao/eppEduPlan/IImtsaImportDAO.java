/**
 *$Id$
 */
package ru.tandemservice.uniusma.dao.eppEduPlan;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.ImtsaFile;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 19.07.2013
 */
public interface IImtsaImportDAO
{
    public static final SpringBeanCache<IImtsaImportDAO> instance = new SpringBeanCache<>(IImtsaImportDAO.class.getName());

    /**
     * Сохраняет данные импортирумеого файла ИМЦА.
     * @param block блок УПв
     * @param imtsa данные ИМЦА
     * @param cyclesMap конфигурационная мапа название цикла на элемент справочника
     * @param partsMap конфигурационная мапа название части на элемент справочника
     * @param errors список ошибок загрузки
     */
    public void saveImtsa(EppEduPlanVersionBlock block, ImtsaFile imtsa, Map<String, EppPlanStructure> cyclesMap, Map<String, EppPlanStructure> partsMap, List<String> errors);

    /**
     * Меняет учебный график в соответствии с учебным графиком ДВФУ. Если неделя разбта выбирает самый частый тип недели среди элементов разбиения.
     * @param eduPlanVersionId id УПв
     * @param courseId id курса, null если меняются недели всех курсов
     */
    public void updateEduPlanVersionWeekTypes(Long eduPlanVersionId, @Nullable Long courseId);

    /**
     * Создает для выбраных строк УП дисциплины реестра.
     * @param registryRowIds идентификаторы строк учебного плана
     */
    public void createRegistryElements(Collection<Long> registryRowIds);

    /**
     * Меняет учебный график при изменении рассредоточенности практики.
     * @param dispersions набор практик
     */
    public void updateScheduleDispersedWeekTypes(Collection<UsmaPracticeDispersion> dispersions);
}