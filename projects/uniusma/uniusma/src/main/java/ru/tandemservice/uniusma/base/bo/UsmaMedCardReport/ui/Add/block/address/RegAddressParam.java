/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address;

import org.tandemframework.core.entity.dsl.MetaDSLPath;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author DMITRY KNYAZEV
 * @since 03.02.2015
 */
public class RegAddressParam extends AbstractAddressParam
{
    //DATA SOURCE
    public static final String REG_COUNTRY_DS = "regCountryDS";
    public static final String REG_SETTLEMENT_DS = "regSettlementDS";
    public static final String REG_STREET_DS = "regStreetDS";

    @Override
    protected MetaDSLPath getStudentAddressPath()
    {
        return Student.person().identityCard().address();
    }

    @Override
    protected String getParameterName()
    {
        return "Место регистрации";
    }
}
