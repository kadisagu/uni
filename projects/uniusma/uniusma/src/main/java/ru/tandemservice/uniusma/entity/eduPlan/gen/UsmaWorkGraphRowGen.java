package ru.tandemservice.uniusma.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Строка для курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaWorkGraphRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow";
    public static final String ENTITY_NAME = "usmaWorkGraphRow";
    public static final int VERSION_HASH = -713386367;
    private static IEntityMeta ENTITY_META;

    public static final String L_GRAPH = "graph";
    public static final String L_COURSE = "course";

    private UsmaWorkGraph _graph;     // ГУП
    private Course _course;     // КУРС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГУП. Свойство не может быть null.
     */
    @NotNull
    public UsmaWorkGraph getGraph()
    {
        return _graph;
    }

    /**
     * @param graph ГУП. Свойство не может быть null.
     */
    public void setGraph(UsmaWorkGraph graph)
    {
        dirty(_graph, graph);
        _graph = graph;
    }

    /**
     * @return КУРС. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course КУРС. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaWorkGraphRowGen)
        {
            setGraph(((UsmaWorkGraphRow)another).getGraph());
            setCourse(((UsmaWorkGraphRow)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaWorkGraphRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaWorkGraphRow.class;
        }

        public T newInstance()
        {
            return (T) new UsmaWorkGraphRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "graph":
                    return obj.getGraph();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "graph":
                    obj.setGraph((UsmaWorkGraph) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "graph":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "graph":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "graph":
                    return UsmaWorkGraph.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaWorkGraphRow> _dslPath = new Path<UsmaWorkGraphRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaWorkGraphRow");
    }
            

    /**
     * @return ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow#getGraph()
     */
    public static UsmaWorkGraph.Path<UsmaWorkGraph> graph()
    {
        return _dslPath.graph();
    }

    /**
     * @return КУРС. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends UsmaWorkGraphRow> extends EntityPath<E>
    {
        private UsmaWorkGraph.Path<UsmaWorkGraph> _graph;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow#getGraph()
     */
        public UsmaWorkGraph.Path<UsmaWorkGraph> graph()
        {
            if(_graph == null )
                _graph = new UsmaWorkGraph.Path<UsmaWorkGraph>(L_GRAPH, this);
            return _graph;
        }

    /**
     * @return КУРС. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return UsmaWorkGraphRow.class;
        }

        public String getEntityName()
        {
            return "usmaWorkGraphRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
