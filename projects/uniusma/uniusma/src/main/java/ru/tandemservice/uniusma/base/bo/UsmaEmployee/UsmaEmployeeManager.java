/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEmployee;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.base.bo.UsmaEmployee.logic.IUsmaEmployeeDAO;
import ru.tandemservice.uniusma.base.bo.UsmaEmployee.logic.UsmaEmployeeDAO;

/**
 * @author Alexander Shaburov
 * @since 16.07.12
 */
@Configuration
public class UsmaEmployeeManager extends BusinessObjectManager
{
    public static UsmaEmployeeManager instance()
    {
        return instance(UsmaEmployeeManager.class);
    }

    @Bean
    public IUsmaEmployeeDAO usmaEmployeeDAO()
    {
        return new UsmaEmployeeDAO();
    }
}
