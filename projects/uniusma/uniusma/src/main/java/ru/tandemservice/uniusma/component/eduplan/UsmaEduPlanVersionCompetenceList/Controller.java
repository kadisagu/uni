/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionCompetenceList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickNumberUp(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        getDao().doNumberUp(id);
        component.refresh();
    }

    public void onClickNumberDown(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        getDao().doNumberDown(id);
        component.refresh();
    }
}