/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.Base;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uniusma.component.studentMassPrint.MassPrint.WrapStudentDocument;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.IModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.year;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public abstract class AbstractStudentMassPrint<M extends IModel> extends UniBaseDao implements IStudentMassPrint<M>
{
    @Override
    public void doMassPrint(List<Student> students, WrapStudentDocument doc)
    {
        String componentName = getComponentName(doc);
        if (componentName != null)
        {
            // активируем компонент, через который запрашиваем параметры
            ParametersMap parametersMap = new ParametersMap()
                    .add(IModel.DOC_INDEX_PARAM, doc.getIndex())
                    .add(IModel.STUDENT_LIST_PARAM, students)
                    .add(IModel.STUDENT_DOCUMENT_TYPE_ID_PARAM, doc.getId());
            ContextLocal.createDesktop("PersonShellDialog", new ComponentActivator(componentName, parametersMap));
        }
    }

    /**
     * Имя компонента, который должен открыться до прорисовки UI,
     * если null, то UI не открываем
     * работаем через bean печати
     */
    protected String getComponentName(WrapStudentDocument doc)
    {
        return "mpd" + doc.getIndex() + ".Add";
    }

    protected int getNextDocumentNumber(Long studentDocumentTypeId, Student stud)
    {
        Student student = get(Student.class, Student.id(), stud.getId());
        int year = CoreDateUtils.getYear(new Date());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StudentDocument.class, "sd")
                .column(property("sd", StudentDocument.number()))
                .order(property("sd", StudentDocument.number()), OrderDirection.desc)
                .where(eq(year(property("sd", StudentDocument.formingDate())), value(year)))
                .where(eq(property("sd", StudentDocument.studentDocumentType().id()), value(studentDocumentTypeId)))
                .where(eq(property("sd", StudentDocument.student().educationOrgUnit().formativeOrgUnit()),
                          value(student.getEducationOrgUnit().getFormativeOrgUnit())))
                .top(1);

        Integer maxNumber = builder.createStatement(getSession()).uniqueResult();
        return maxNumber == null ? 1 : maxNumber + 1;
    }

    @Override
    public void initModel(M model)
    {
        model.setFormingDate(new Date());
        model.setDoSame(false);
        model.setNeedSaveDocument(true);
    }

    @Override
    public byte[] getDocument(List<Student> students, M model, int docIndex, Long studentDocumentTypeId)
    {

        // год текущей даты должен соответствовать дате в модели
        int yearCurrent = CoreDateUtils.getYear(new Date());
        int yearModel = CoreDateUtils.getYear(model.getFormingDate());

        if (yearCurrent != yearModel)
            throw new ApplicationException("Массовая печать документов возможна только в рамках текущего года");

        StudentDocumentType studentDocumentType = get(StudentDocumentType.class, studentDocumentTypeId);
        final List<IRtfElement> mainElementList = new ArrayList<>();
        boolean first = true;
        RtfDocument document = new RtfDocument();
        for (Student student : students)
        {
            int docNumber = getNextDocumentNumber(studentDocumentTypeId, student);

            // а если такой документ сущ? На эту дату, то по этой дате тянем готовый документ
            byte[] existDocument = null;
            if (!model.isDoSame())                // нужно искать аналогичный документ
                existDocument = getExistDocument(studentDocumentTypeId, student);

            RtfDocument documentStudent;
            if (existDocument != null)
            {
                documentStudent = RtfDocument.fromByteArray(existDocument);
            }
            else
            {
                initModel4Student(student, docNumber, model);
                final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(studentDocumentType.getScriptItem(), model);
                documentStudent = RtfDocument.fromByteArray((byte[]) scriptResult.get("document"));
            }

            // нужно сохранить документ на студенте
            if (existDocument == null && model.isNeedSaveDocument())
                saveStudentDocument(studentDocumentTypeId, student, documentStudent, model);

            if (first)
            {
                document.setHeader(documentStudent.getHeader());
                document.setSettings(documentStudent.getSettings());
            }
            else
                insertPageBreak(mainElementList);
            first = false;

            mainElementList.addAll(documentStudent.getElementList());
        }

        document.setElementList(mainElementList);
        return document.toByteArray();
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    protected StudentDocument saveStudentDocument(Long studentDocumentTypeId, Student stud, RtfDocument document, M model)
    {
        Student student = get(Student.class, Student.id(), stud.getId());
        Session session = getSession();
        byte[] bts = RtfUtil.toByteArray(document);

        // файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(bts);
        save(databaseFile);

        StudentDocumentType docType = get(StudentDocumentType.class, studentDocumentTypeId);

        // Дока студента
        StudentDocument studentDocument = new StudentDocument();
        studentDocument.setNumber(model.getNumber());
        studentDocument.setFormingDate(model.getFormingDate());
        studentDocument.setStudent(student);
        studentDocument.setContent(databaseFile);
        studentDocument.setStudentDocumentType(docType);

        save(studentDocument);
        session.flush();

        return studentDocument;
    }


    protected byte[] getExistDocument(Long studentDocumentTypeId, Student student)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentDocument.class, "sd")
                .column(property("sd"))
                .where(eq(property(StudentDocument.student().fromAlias("sd")), value(student)))
                .where(eq(property(StudentDocument.studentDocumentType().id().fromAlias("sd")), value(studentDocumentTypeId)))
                .order(property(StudentDocument.formingDate().fromAlias("sd")), OrderDirection.desc)
                .top(1);

        StudentDocument studentDocument = dql.createStatement(getSession()).uniqueResult();

        return studentDocument != null ? studentDocument.getContent().getContent() : null;
    }


    public abstract boolean isNeedNewPage();

    @Override
    public void initModel4Student(Student student, int documentNumber, M model)
    {
        // базовая инициализация
        model.setFormingDate(new Date());

        Student st = get(Student.class, student.getId());

        // иначе студент получен в другой сессии хиберната
        model.setStudent(st);
        model.setNumber(documentNumber);
    }

    protected void insertPageBreak(List<IRtfElement> elementList)
    {
        if (CollectionUtils.isEmpty(elementList)) return;

        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }
}