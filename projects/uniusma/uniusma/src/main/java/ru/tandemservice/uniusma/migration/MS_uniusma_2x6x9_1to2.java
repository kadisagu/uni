package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x9_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaChangedWorkPlan

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usma_workplan_changed_t"))
            {
                DBTable dbt = new DBTable("usma_workplan_changed_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("eppworkplan_id", DBType.LONG).setNullable(false),
                                          new DBColumn("changed_p", DBType.BOOLEAN).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaChangedWorkPlan");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaCompetence2RegistryElementRel

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("smcmptnc2rgstryelmntrl_t"))
            {
                DBTable dbt = new DBTable("smcmptnc2rgstryelmntrl_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("usmacompetence_id", DBType.LONG).setNullable(false),
                                          new DBColumn("registryelement_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaCompetence2RegistryElementRel");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaEpvCheckState

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaepvcheckstate_t"))
            {
                DBTable dbt = new DBTable("usmaepvcheckstate_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("version_id", DBType.LONG).setNullable(false),
                                          new DBColumn("checkedbyumu_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("checkedbyumudate_p", DBType.TIMESTAMP),
                                          new DBColumn("checkedbycrk_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("checkedbycrkdate_p", DBType.TIMESTAMP),
                                          new DBColumn("checkedbyoop_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("checkedbyoopdate_p", DBType.TIMESTAMP),
                                          new DBColumn("accepteddate_p", DBType.TIMESTAMP),
                                          new DBColumn("acceptedby_p", DBType.createVarchar(255)),
                                          new DBColumn("rejectedcomment_p", DBType.createVarchar(255)),
                                          new DBColumn("eduplanaccepteddate_p", DBType.TIMESTAMP),
                                          new DBColumn("eduplanacceptedby_p", DBType.createVarchar(255))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaEpvCheckState");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaImtsaCyclePlanStructureRel

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("smimtscyclplnstrctrrl_t"))
            {
                DBTable dbt = new DBTable("smimtscyclplnstrctrrl_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("imtsacycle_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("gos2_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("planstructure_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaImtsaCyclePlanStructureRel");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaImtsaImportLog

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaimtsaimportlog_t"))
            {
                DBTable dbt = new DBTable("usmaimtsaimportlog_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("message_p", DBType.TEXT)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaImtsaImportLog");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaImtsaXml

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaimtsaxml_t"))
            {
                DBTable dbt = new DBTable("usmaimtsaxml_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("block_id", DBType.LONG).setNullable(false),
                                          new DBColumn("filename_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("encoding_p", DBType.createVarchar(255)),
                                          new DBColumn("xml_p", DBType.BLOB).setNullable(false),
                                          new DBColumn("number_p", DBType.INTEGER).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaImtsaXml");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaWorkGraph

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaworkgraph_t"))
            {
                DBTable dbt = new DBTable("usmaworkgraph_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("year_id", DBType.LONG).setNullable(false),
                                          new DBColumn("developform_id", DBType.LONG).setNullable(false),
                                          new DBColumn("developtech_id", DBType.LONG).setNullable(false),
                                          new DBColumn("developcondition_id", DBType.LONG).setNullable(false),
                                          new DBColumn("developgrid_id", DBType.LONG).setNullable(false),
                                          new DBColumn("partitiontype_id", DBType.LONG).setNullable(false),
                                          new DBColumn("state_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaWorkGraph");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaWorkGraphRow

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaworkgraphrow_t"))
            {
                DBTable dbt = new DBTable("usmaworkgraphrow_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("graph_id", DBType.LONG).setNullable(false),
                                          new DBColumn("course_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaWorkGraphRow");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaWorkGraphRow2EduPlan

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaworkgraphrow2eduplan_t"))
            {
                DBTable dbt = new DBTable("usmaworkgraphrow2eduplan_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("row_id", DBType.LONG).setNullable(false),
                                          new DBColumn("eduplanversion_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaWorkGraphRow2EduPlan");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaWorkGraphRowWeek

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaworkgraphrowweek_t"))
            {
                DBTable dbt = new DBTable("usmaworkgraphrowweek_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("row_id", DBType.LONG).setNullable(false),
                                          new DBColumn("week_p", DBType.INTEGER).setNullable(false),
                                          new DBColumn("type_id", DBType.LONG),
                                          new DBColumn("term_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaWorkGraphRowWeek");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaWorkGraphRowWeekPart

		// создана новая сущность
		{
			// создать таблицу
            if(!tool.tableExists("usmaworkgraphrowweekpart_t"))
            {
                DBTable dbt = new DBTable("usmaworkgraphrowweekpart_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("week_id", DBType.LONG).setNullable(false),
                                          new DBColumn("part_p", DBType.INTEGER).setNullable(false),
                                          new DBColumn("type_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("usmaWorkGraphRowWeekPart");
            }
		}


    }
}