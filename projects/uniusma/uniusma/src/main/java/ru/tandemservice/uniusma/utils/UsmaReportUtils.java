/* $Id$ */
package ru.tandemservice.uniusma.utils;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;

/**
 * @author Stanislav Shibarshin
 * @since 01.09.2016
 */
public class UsmaReportUtils
{

    /**
     * Удаляет сокращенное название НПв в скобках, если оно присутсвтует в
     * injectModifier по ключу educationOrgUnit.
     * <p> «31.05.01 Лечебное дело (ЛПФ)» = «31.05.01 Лечебное дело».
     *
     * @param injectModifier RtfInjectModifier не null.
     */
    public static void removeEduOrgUnitShortFormExtract(RtfInjectModifier injectModifier)
    {
        String educationOrgUnitValue = injectModifier.getStringValue("educationOrgUnit");
        if (StringUtils.isNotEmpty(educationOrgUnitValue))
        {
            // Если присутвствует сокращенная версия
            if (educationOrgUnitValue.contains("(") && educationOrgUnitValue.contains(")"))
            {
                String newValue = educationOrgUnitValue.substring(0, educationOrgUnitValue.indexOf("(")).trim();
                injectModifier.put("educationOrgUnit", newValue);
            }
        }
    }

}
