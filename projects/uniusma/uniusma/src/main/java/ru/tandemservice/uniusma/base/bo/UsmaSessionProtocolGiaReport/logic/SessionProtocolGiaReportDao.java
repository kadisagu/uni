/*$Id:$*/
package ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.uniusma.entity.catalog.codes.UnisessionCommonTemplateCodes;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 17.03.2016
 */
public class SessionProtocolGiaReportDao extends CommonDAO implements ISessionProtocolGiaReportDao
{

    public RtfDocument getDocument(
            final List<Group> groupList,
            final List<PpsEntry> chairmanPpsList,
            final List<PpsEntry> membersPpsList,
            final List<PpsEntry> clerkPpsList)
    {
        if (CollectionUtils.isEmpty(groupList))
            throw new ApplicationException("Нет данных для печати");

        final RtfDocument template = new RtfReader().read(
                getByCode(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.USMA_SESSION_PROTOCOL_GIA_PRINT_SCRIPT)
                        .getContent());

        final RtfDocument result = new RtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());
        for (Group group : groupList)
        {
            result.getElementList().addAll(getGroupDocument(
                    template.getClone(),
                    group,
                    chairmanPpsList,
                    membersPpsList,
                    clerkPpsList
            ).getElementList());
        }
        return result;
    }

    private RtfDocument getGroupDocument(
            RtfDocument document,
            final Group group,
            final List<PpsEntry> chairmanPpsList,
            final List<PpsEntry> membersPpsList,
            final List<PpsEntry> clerkPpsList
    )
    {
        final RtfInjectModifier im = new RtfInjectModifier();
        final RtfTableModifier tm = new RtfTableModifier();

        final EducationOrgUnit eduOrgUnit = group.getEducationOrgUnit();
        im.put("educationOrgUnitTitle", eduOrgUnit.getTitle());

        tm.put("T", getStudents(group));
        im.put("tutor", chairmanPpsList.stream()
                .map(PpsEntry::getShortTitle)
                .collect(Collectors.joining(", "))
        );
        im.put("tutors", membersPpsList.stream()
                .map(PpsEntry::getShortTitle)
                .collect(Collectors.joining(", "))
        );
        im.put("tutor_tech", clerkPpsList.stream()
                .map(PpsEntry::getShortTitle)
                .collect(Collectors.joining(", "))
        );

        im.modify(document);
        tm.modify(document);

        return document;
    }

    private String[][] getStudents(final Group group)
    {
        final String alias = "s";
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, alias)
                .column(DQLExpressions.property(alias))
                .where(DQLExpressions.eq(DQLExpressions.property(alias, Student.group()), DQLExpressions.value(group)))
                .where(DQLExpressions.eq(DQLExpressions.property(alias, Student.status().active()), DQLExpressions.value(Boolean.TRUE)));
        final List<Student> studentList = builder.createStatement(getSession()).list();
        studentList.sort(Student.FULL_FIO_AND_ID_COMPARATOR);

        Integer counter = 0;
        final String[][] result = new String[studentList.size()][];
        for (Student student : studentList)
        {
            result[counter++] = new String[]{counter.toString(), student.getFullFio(), "", ""};
        }
        return result;
    }
}
