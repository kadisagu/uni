/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.WorkPlanAutoCreate;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.event.IEventServiceLock;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 14.11.2013
 */
public class UsmaWorkGraphWorkPlanAutoCreateUI extends UIPresenter
{
    public static final IdentifiableWrapper<IEntity> OVERCOPY_ALL = new IdentifiableWrapper<>(1L, "Копировать РУП всегда");
    public static final IdentifiableWrapper<IEntity> OVERCOPY_NEW = new IdentifiableWrapper<>(2L, "Копировать только новые РУП");

    public static final IdentifiableWrapper<IEntity> LOOKUP_LOOKUP = new IdentifiableWrapper<>(1L, "Создать на основе РУП прошлых лет");
    public static final IdentifiableWrapper<IEntity> LOOKUP_CREATE = new IdentifiableWrapper<>(2L, "Всегда создавать на основе УП");

    private EppYearEducationProcess _year;
    private UsmaWorkGraph _workGraph;
    private String _comment;

    public EppYearEducationProcess getYear(){ return _year; }
    public void setYear(EppYearEducationProcess year){ _year = year; }

    public UsmaWorkGraph getWorkGraph(){ return _workGraph; }
    public void setWorkGraph(UsmaWorkGraph workGraph){ _workGraph = workGraph; }

    public String getComment(){ return _comment; }
    public void setComment(String comment){ _comment = comment; }


    @SuppressWarnings("unchecked")
    private final SelectModel<IdentifiableWrapper<IEntity>> _overcopySelectModel = new SelectModel<IdentifiableWrapper<IEntity>>()
    {
        {
            this.setSource(OVERCOPY_ALL, OVERCOPY_NEW);
            this.setValue(OVERCOPY_ALL);
        }
    };

    @SuppressWarnings("unchecked")
    private final SelectModel<IdentifiableWrapper<IEntity>> _lookupSelectModel = new SelectModel<IdentifiableWrapper<IEntity>>()
    {
        {
            this.setSource(LOOKUP_LOOKUP, LOOKUP_CREATE);
            this.setValue(LOOKUP_LOOKUP);
        }
    };

    public SelectModel<IdentifiableWrapper<IEntity>> getOvercopySelectModel() { return _overcopySelectModel; }
    public SelectModel<IdentifiableWrapper<IEntity>> getLookupSelectModel() { return _lookupSelectModel; }


    public void onClickApply()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            UsmaWorkGraphManager.instance().dao().saveWorkPlanByWorkGraphs(_workGraph, _lookupSelectModel.getValue().equals(LOOKUP_LOOKUP), _overcopySelectModel.getValue().equals(OVERCOPY_NEW), _comment);

        } finally
        {
            eventLock.release();
        }

        if (UserContext.getInstance().getErrorCollector().hasErrors()) { return; }
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UsmaWorkGraphWorkPlanAutoCreate.WORK_GRAPH_DS.equals(dataSource.getName()))
        {
            dataSource.put(UsmaWorkGraphWorkPlanAutoCreate.PUPNAG, _year.getId());
        }
    }
}