/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.io.FileUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.util.UsmaEpvRowWrapper;
import ru.tandemservice.uniusma.entity.*;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.catalog.UsmaLoadType;
import ru.tandemservice.uniusma.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniusma.entity.catalog.codes.UsmaLoadTypeCodes;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvRowTermLoad;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public class UsmaEduPlanDAO extends CommonDAO implements IUsmaEduPlanDAO
{
    @Override
    public Map<Term, Map<ICatalogItem, Double>> getEpvRowExtLoadDataMap(Long rowId)
    {
        Map<Term, Map<ICatalogItem, Double>> result = new LinkedHashMap<>();

        List<ICatalogItem> loadTypes = new ArrayList<>();
        Map<String, ICatalogItem> interactiveLoadTypeMap = new HashMap<>();
        Map<String, ICatalogItem> baseLoadTypeMap = new HashMap<>();

        for (UsmaLoadType loadType: getList(UsmaLoadType.class))
        {
            interactiveLoadTypeMap.put(loadType.getCode(), loadType);
        }

        for (EppALoadType loadType: getList(EppALoadType.class))
        {
            baseLoadTypeMap.put(loadType.getCode(), loadType);
        }

        {
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LECTURES));
            loadTypes.add(interactiveLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_LECTURES_INTER));

            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
            loadTypes.add(interactiveLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_PRACTICE_INTER));

            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LABS));
            loadTypes.add(interactiveLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_LABS_INTER));

            loadTypes.add(interactiveLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_EXAM_HOURS));
        }

        List<Term> terms = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .column(property("dgt", DevelopGridTerm.term()))
                .where(eq(property("dgt", DevelopGridTerm.developGrid().id()),
                          new DQLSelectBuilder()
                                  .fromEntity(EppEpvRegistryRow.class, "rr")
                                  .column(property("rr", EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developGrid().id()))
                                  .where(eq(property("rr", EppEpvRegistryRow.id()), value(rowId)))
                                  .buildQuery()))

                .order(property("dgt", DevelopGridTerm.term().intValue()))
                .createStatement(getSession())
                .list();

        for (ICatalogItem loadType: loadTypes)
        {
            for (Term term: terms)
            {
                SafeMap.safeGet(result, term, LinkedHashMap.class).put(loadType, null);
            }
        }

        for (UsmaEpvRowTermLoad termLoad: getList(UsmaEpvRowTermLoad.class, UsmaEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            SafeMap.safeGet(result, termLoad.getRowTerm().getTerm(), LinkedHashMap.class).put(termLoad.getLoadType(), UniEppUtils.wrap(termLoad.getLoad()));
        }

        for (EppEpvRowTermLoad rowTermLoad: getList(EppEpvRowTermLoad.class, EppEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            if (rowTermLoad.getLoadType() != null) {
                 SafeMap.safeGet(result, rowTermLoad.getRowTerm().getTerm(), LinkedHashMap.class).put(rowTermLoad.getLoadType() , UniEppUtils.wrap(rowTermLoad.getHours()));
            }
        }

        return result;
    }

    @Override
    public void updateEpvRowExtLoad(Long rowId, Map<Term, Map<ICatalogItem, Double>> dataMap, ErrorCollector errorCollector)
    {
        final EppEpvTermDistributedRow termDistributedRow = get(rowId);

        Map<String, EppALoadType> loadTypeMap = new HashMap<>();
        for (EppALoadType loadType: getList(EppALoadType.class)){ loadTypeMap.put(loadType.getCode(), loadType); }

        Map<String, String> usmaLoad2baseLoadMap = new HashMap<>();
        {
            usmaLoad2baseLoadMap.put(UsmaLoadTypeCodes.TYPE_LECTURES_INTER, EppALoadTypeCodes.TYPE_LECTURES);
            usmaLoad2baseLoadMap.put(UsmaLoadTypeCodes.TYPE_PRACTICE_INTER, EppALoadTypeCodes.TYPE_PRACTICE);
            usmaLoad2baseLoadMap.put(UsmaLoadTypeCodes.TYPE_LABS_INTER, EppALoadTypeCodes.TYPE_LABS);
        }

        Map<Long, Map<Long, UsmaEpvRowTermLoad>> oldValueMap = SafeMap.get(HashMap.class);
        for (UsmaEpvRowTermLoad rowTermLoad: getList(UsmaEpvRowTermLoad.class, UsmaEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            oldValueMap.get(rowTermLoad.getRowTerm().getTerm().getId()).put(rowTermLoad.getLoadType().getId(), rowTermLoad);
        }

        Map<Term, EppEpvRowTerm> rowTermMap = SafeMap.get(key -> {
            EppEpvRowTerm rowTerm = getByNaturalId(new EppEpvRowTerm.NaturalId(termDistributedRow, key));
            if (rowTerm == null) {
                save(rowTerm = new EppEpvRowTerm(termDistributedRow, key));
            }
            return rowTerm;
        });

        Map<PairKey<EppEpvRowTerm, UsmaLoadType>, UsmaEpvRowTermLoad> rowTermLoadMap = new HashMap<>();
        for (UsmaEpvRowTermLoad rowTermLoad: getList(UsmaEpvRowTermLoad.class, UsmaEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            rowTermLoadMap.put(PairKey.create(rowTermLoad.getRowTerm(), rowTermLoad.getLoadType()), rowTermLoad);
        }

        for (EppEpvRowTerm rowTerm: getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id(), rowId))
        {
            rowTermMap.put(rowTerm.getTerm(), rowTerm);
        }

        String emptyBase = "Невозможно задать часы интерактивной нагрузки. Не задана общая нагрузка.";
        String lessBase = "Невозможно задать часы интерактивной нагрузки. Значение не должно превосходить общую.";

        for (Map.Entry<Term, Map<ICatalogItem, Double>> termEntry: new HashMap<>(dataMap).entrySet())
        {
            Term term = termEntry.getKey();
            Map<ICatalogItem, Double> termLoadTypeMap = dataMap.get(term);
            for (Map.Entry<ICatalogItem, Double> loadTypeEntry: termEntry.getValue().entrySet())
            {
                ICatalogItem key = loadTypeEntry.getKey();
                if (key instanceof EppALoadType){ continue; }

                UsmaLoadType loadType = (UsmaLoadType) key;

                Double value = loadTypeEntry.getValue();
                EppALoadType boundedType = loadTypeMap.get(usmaLoad2baseLoadMap.get(loadType.getCode()));

                boolean bounded = boundedType != null;
                Double maxValue = bounded ? termLoadTypeMap.get(boundedType) : null;


                /* VALIDATE */
                if (bounded && value != null)
                {
                    if (maxValue == null || maxValue == 0.0d)
                    {
                        if (value != 0.0d)
                        {
                            errorCollector.add(emptyBase, String.valueOf(term.getId()) + "_" + loadType.getId());
                            continue;
                        }

                    } else
                    {
                        if (value > maxValue)
                        {
                            errorCollector.add(lessBase, String.valueOf(term.getId()) + "_" + loadType.getId());
                            continue;
                        }
                    }
                }

                /* UPDATE */
                if (value == null)
                {
                    UsmaEpvRowTermLoad rowTermLoad;
                    if (rowTermMap.containsKey(term) && (rowTermLoad = rowTermLoadMap.get(PairKey.create(rowTermMap.get(term), loadType))) != null)
                    {
                        delete(rowTermLoad);
                    }

                } else
                {
                    Long valueAsLong = UniEppUtils.unwrap(value);
                    UsmaEpvRowTermLoad rowTermLoad = oldValueMap.get(term.getId()).get(loadType.getId());
                    if (rowTermLoad == null)
                    {
                        rowTermLoad = new UsmaEpvRowTermLoad(rowTermMap.get(term), loadType);
                        rowTermLoad.setLoad(valueAsLong);

                        save(rowTermLoad);

                    } else if (rowTermLoad.getLoad() != valueAsLong)
                    {
                        rowTermLoad.setLoad(valueAsLong);

                        update(rowTermLoad);
                    }
                }
            }
        }
    }


    @Override
    public Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<UsmaEpvRowWrapper>, StaticListDataSource<UsmaEpvRowWrapper>>> getEduPlanVersionBlockDataSourceMap(Long versionId)
    {
        Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<UsmaEpvRowWrapper>, StaticListDataSource<UsmaEpvRowWrapper>>> result = new HashMap<>();

        DQLSelectBuilder developGridTermBuilder = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .where(eq(property("dgt", DevelopGridTerm.developGrid().id()),
                          new DQLSelectBuilder()
                                  .fromEntity(EppEduPlanVersion.class, "v")
                                  .column(property("v", EppEduPlanVersion.eduPlan().developGrid().id()))
                                  .where(eq(property("v", EppEduPlanVersion.id()), value(versionId)))
                                  .buildQuery()))
                .order(property("dgt", DevelopGridTerm.course().intValue()))
                .order(property("dgt", DevelopGridTerm.part().number()));

        Map<Course, List<Term>> courseTermMap = new LinkedHashMap<>();
        for (DevelopGridTerm developGridTerm: this.<DevelopGridTerm>getList(developGridTermBuilder))
        {
            SafeMap.safeGet(courseTermMap, developGridTerm.getCourse(), ArrayList.class).add(developGridTerm.getTerm());
        }

        List<EppEduPlanVersionBlock> blocks = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), versionId);
        for (EppEduPlanVersionBlock block: blocks)
        {
            final Map<Long, PairKey<String, Integer>> competenceId2SkillGroupNumberMap = new HashMap<>();
            for (UsmaCompetence2EduPlanVersionBlockRel rel: getList(UsmaCompetence2EduPlanVersionBlockRel.class, UsmaCompetence2EduPlanVersionBlockRel.block(), block))
            {
                UsmaCompetence competence = rel.getUsmaCompetence();
                competenceId2SkillGroupNumberMap.put(competence.getId(), PairKey.create(competence.getEppSkillGroup().getShortTitle(), rel.getNumber()));
            }

            Map<Long, Set<Long>> row2competenceMap = SafeMap.get(HashSet.class);
            DQLSelectBuilder rowCompetenceBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaCompetence2EpvRegistryRowRel.class, "rrr")
                    .column(property("rrr", UsmaCompetence2EpvRegistryRowRel.registryRow().id()))
                    .column(property("rrr", UsmaCompetence2EpvRegistryRowRel.usmaCompetence().id()))
                    .where(eq(property("rrr", UsmaCompetence2EpvRegistryRowRel.registryRow().owner()), value(block)));

            DQLSelectBuilder registryCompetenceBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaCompetence2RegistryElementRel.class, "rer")
                    .joinEntity("rer", DQLJoinType.inner, EppEpvRegistryRow.class, "rr", eq(property("rr", EppEpvRegistryRow.registryElement()), property("rer", UsmaCompetence2RegistryElementRel.registryElement())))
                    .column(property("rr", EppEpvRegistryRow.id()))
                    .column(property("rer", UsmaCompetence2RegistryElementRel.usmaCompetence().id()))
                    .where(eq(property("rr", EppEpvRegistryRow.owner()), value(block)));

            for (Object[] row: this.<Object[]>getList(rowCompetenceBuilder)){ row2competenceMap.get((Long) row[0]).add((Long) row[1]); }
            for (Object[] row: this.<Object[]>getList(registryCompetenceBuilder)){ row2competenceMap.get((Long) row[0]).add((Long) row[1]); }

            // row.id -> term.id -> loadtype.id -> load value
            Map<Long, Map<Long, Map<Long, Double>>> fullDataMap = SafeMap.get(key -> SafeMap.get(HashMap.class));

            DQLSelectBuilder baseLoadBuilder = new DQLSelectBuilder()
                    .fromEntity(EppEpvRowTermLoad.class, "rtl")
                    .column(property("rtl", EppEpvRowTermLoad.rowTerm().row().id()))
                    .column(property("rtl", EppEpvRowTermLoad.rowTerm().term().id()))
                    .column(property("rtl", EppEpvRowTermLoad.loadType().id()))
                    .column(property("rtl", EppEpvRowTermLoad.hours()))
                    .where(eq(property("rtl", EppEpvRowTermLoad.rowTerm().row().owner()), value(block)));

            for (Object[] row: this.<Object[]>getList(baseLoadBuilder)){ fullDataMap.get((Long) row[0]).get((Long) row[1]).put((Long) row[2], UniEppUtils.wrap((Long) row[3])); }

            DQLSelectBuilder extLoadBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaEpvRowTermLoad.class, "rtl")
                    .joinPath(DQLJoinType.inner, UsmaEpvRowTermLoad.rowTerm().fromAlias("rtl"), "rt")
                    .column(property("rt", EppEpvRowTerm.row().id()))
                    .column(property("rt", EppEpvRowTerm.term().id()))
                    .column(property("rtl", UsmaEpvRowTermLoad.loadType().id()))
                    .column(property("rtl", UsmaEpvRowTermLoad.load()))
                    .where(eq(property("rt", EppEpvRowTerm.row().owner()), value(block)));

            for (Object[] row: this.<Object[]>getList(extLoadBuilder)){ fullDataMap.get((Long) row[0]).get((Long) row[1]).put((Long) row[2], UniEppUtils.wrap((Long) row[3])); }

            Map<EppEpvRow, List<EppEpvRow>> parentMap = SafeMap.get(ArrayList.class);
            for (EppEpvRow row: getList(EppEpvRow.class, EppEpvRow.owner(), block))
            {
                parentMap.get(row.getHierarhyParent()).add(row);
            }

            if (block instanceof EppEduPlanVersionSpecializationBlock)
            {
                // добавляем структурные узлы общего блока
                EppEduPlanVersionBlock rootBlock = get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.eduPlanVersion().id(), versionId);
                for (EppEpvHierarchyRow row: getList(EppEpvHierarchyRow.class, EppEpvRow.owner(), rootBlock))
                {
                    parentMap.get(row.getHierarhyParent()).add(row);
                }
            }

            final Map<Long, String> competenceId2indexMap = SafeMap.get(key -> {
                PairKey<String, Integer> pairKey = competenceId2SkillGroupNumberMap.get(key);
                return pairKey.getFirst() + "-" + pairKey.getSecond();
            });
            Transformer<Long, String> competenceTransformer = competenceId2indexMap::get;
            Comparator<Long> competenceComparator = (o1, o2) -> {
                PairKey<String, Integer> key1 = competenceId2SkillGroupNumberMap.get(o1);
                PairKey<String, Integer> key2 = competenceId2SkillGroupNumberMap.get(o2);

                int result1;
                if ((result1 = key1.getFirst().compareTo(key2.getFirst())) == 0)
                {
                    result1 = key1.getSecond().compareTo(key2.getSecond());
                }

                return result1;
            };

            List<UsmaEpvRowWrapper> wrappers = new ArrayList<>();
            Model model = new Model(parentMap, row2competenceMap, competenceTransformer, competenceComparator, fullDataMap);

            wrap(wrappers, null, model);

            List<UsmaEpvRowWrapper> actionWrappers = removeActionWrappers(wrappers);

            StaticListDataSource<UsmaEpvRowWrapper> dataSource = getDataSource(courseTermMap, true, block);
            dataSource.setRowList(wrappers);

            StaticListDataSource<UsmaEpvRowWrapper> actionDataSource = getDataSource(courseTermMap, false, block);
            actionDataSource.setRowList(actionWrappers);

            result.put(block,PairKey.create(dataSource, actionDataSource));
        }

        return result;
    }


    private List<UsmaEpvRowWrapper> removeActionWrappers(List<UsmaEpvRowWrapper> wrappers)
    {
        Map<Long, UsmaEpvRowWrapper> wrapperMap = new LinkedHashMap<>();
        for (UsmaEpvRowWrapper wrapper: new ArrayList<>(wrappers))
        {
            if (wrapper.getRow() instanceof EppEpvRegistryRow)
            {
                EppRegistryStructure structure = ((EppEpvRegistryRow) wrapper.getRow()).getRegistryElementType();
                while (structure.getHierarhyParent() != null)
                {
                    structure = structure.getParent();
                }

                if (!structure.getCode().equals(EppRegistryStructureCodes.REGISTRY_DISCIPLINE))
                {
                    wrappers.remove(wrapper);
                    UsmaEpvRowWrapper parentWrapper = wrapper;
                    while (parentWrapper != null)
                    {
                        wrapperMap.put(parentWrapper.getId(), parentWrapper);
                        parentWrapper = (UsmaEpvRowWrapper) parentWrapper.getHierarhyParent();
                    }
                }
            }
        }

        return new ArrayList<>(wrapperMap.values());
    }

    private Map<Long, Map<Long, Double>> wrap(List<UsmaEpvRowWrapper> wrappers, @Nullable final UsmaEpvRowWrapper parentWrapper, Model model)
    {
        boolean distrRow = parentWrapper != null && model._dataMap.containsKey(parentWrapper.getRow().getId());
        Map<Long, Map<Long, Double>> localDataMap = distrRow ? model._dataMap.get(parentWrapper.getRow().getId()) : SafeMap.<Long, Map<Long,Double>>get(HashMap.class);

        for (final EppEpvRow row: model._parentMap.get(parentWrapper == null ? null : parentWrapper.getRow()))
        {
            List<Long> competenceIds = new ArrayList<>(model._row2competenceMap.get(row.getId()));
            Collections.sort(competenceIds, model._competenceComparator);

            UsmaEpvRowWrapper wrapper = new UsmaEpvRowWrapper(row, CollectionUtils.collect(competenceIds, model._competenceTransformer))
            {
                @Override
                public IHierarchyItem getHierarhyParent()
                {
                    return parentWrapper;
                }
            };

            wrappers.add(wrapper);

            Map<Long, Map<Long, Double>> childDataMap = wrap(wrappers, wrapper, model);

            if (distrRow){ continue; }

            for (Map.Entry<Long, Map<Long, Double>> loadTypeEntry: childDataMap.entrySet())
            {
                Long typeId = loadTypeEntry.getKey();
                Map<Long, Double> termDataMap = SafeMap.safeGet(localDataMap, typeId, HashMap.class);
                for (Map.Entry<Long, Double> termEntry: loadTypeEntry.getValue().entrySet())
                {

                    Double oldValue = termDataMap.get(termEntry.getKey());
                    termDataMap.put(termEntry.getKey(), termEntry.getValue() + (oldValue == null ? 0.0d : oldValue));
                }
            }
        }

        if (parentWrapper != null)
            parentWrapper.setDataMap(localDataMap);

        return localDataMap;
    }

    private StaticListDataSource<UsmaEpvRowWrapper> getDataSource(Map<Course, List<Term>> courseTermMap, boolean discipline, final EppEduPlanVersionBlock block)
    {
        class ShortTitlePresenter
        {
            public String shortTitle(ICatalogItem catalogItem)
            {
                if (catalogItem instanceof UsmaLoadType || catalogItem instanceof EppALoadType)
                {
                    return catalogItem instanceof UsmaLoadType ? ((UsmaLoadType) catalogItem).getShortTitle() : ((EppALoadType) catalogItem).getShortTitle();
                }

                throw new IllegalArgumentException("Unknown catalog item " + catalogItem.getClass());
            }
        }

        ShortTitlePresenter shortTitlePresenter = new ShortTitlePresenter();
        String headerAlignCenter = "center";

        Map<String, ICatalogItem> baseLoadTypeMap = new HashMap<>();
        Map<String, ICatalogItem> extLoadTypeMap = new HashMap<>();

        for (EppALoadType loadType: getList(EppALoadType.class)){ baseLoadTypeMap.put(loadType.getCode(), loadType); }
        for (UsmaLoadType loadType: getList(UsmaLoadType.class)){ extLoadTypeMap.put(loadType.getCode(), loadType); }

        List<ICatalogItem> loadTypes = new ArrayList<>();
        {
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LECTURES));
            loadTypes.add(extLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_LECTURES_INTER));
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
            loadTypes.add(extLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_PRACTICE_INTER));
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LABS));
            loadTypes.add(extLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_LABS_INTER));

            loadTypes.add(extLoadTypeMap.get(UsmaLoadTypeCodes.TYPE_EXAM_HOURS));
        }

        StaticListDataSource<UsmaEpvRowWrapper> dataSource = new StaticListDataSource<>();
        dataSource.addColumn(new SimpleColumn("Название", UsmaEpvRowWrapper.TITLE).setTreeColumn(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип", "type")
        {
            @Override
            public String getContent(IEntity entity)
            {
                return ((UsmaEpvRowWrapper) entity).getRow().getRowType();
            }
        }.setOrderable(false));

        if (discipline)
        {
            HeadColumn loadColumn = new HeadColumn("load", "Нагрузка");
            loadColumn.setHeaderAlign(headerAlignCenter).setOrderable(false);
            dataSource.addColumn(loadColumn);

            for (final ICatalogItem loadType: loadTypes)
            {
                SimpleColumn column = new SimpleColumn(shortTitlePresenter.shortTitle(loadType), loadType.getTitle())
                {
                    @Override
                    public String getContent(IEntity entity)
                    {
                        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((UsmaEpvRowWrapper) entity).getDataMap().get(null).get(loadType.getId()));
                    }
                };
                column.setVerticalHeader(true).setOrderable(false);
                loadColumn.addColumn(column);
            }

            HeadColumn termDetailColumn = new HeadColumn("termDetail", "Нагрузка по семестрам");
            termDetailColumn.setHeaderAlign(headerAlignCenter).setOrderable(false);
            dataSource.addColumn(termDetailColumn);

            for (Map.Entry<Course, List<Term>> courseEntry: courseTermMap.entrySet())
            {
                Course course = courseEntry.getKey();
                HeadColumn courseColumn = new HeadColumn("course" + course.getIntValue(), course.getTitle() + " курс");
                courseColumn.setOrderable(false).setHeaderAlign(headerAlignCenter);
                termDetailColumn.addColumn(courseColumn);

                for (final Term term: courseEntry.getValue())
                {
                    HeadColumn termColumn = new HeadColumn("term" + term.getTitle(), "Сем " + term.getTitle());
                    termColumn.setHeaderAlign(headerAlignCenter).setOrderable(false);
                    courseColumn.addColumn(termColumn);

                    for (final ICatalogItem loadType: loadTypes)
                    {
                        SimpleColumn column = new SimpleColumn(shortTitlePresenter.shortTitle(loadType), term.getTitle() + loadType.getTitle())
                        {
                            @Override
                            public String getContent(IEntity entity)
                            {
                                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((UsmaEpvRowWrapper) entity).getDataMap().get(term.getId()).get(loadType.getId()));
                            }
                        };
                        column.setVerticalHeader(true).setOrderable(false);
                        termColumn.addColumn(column);
                    }
                }
            }

            dataSource.setRowCustomizer(new SimpleRowCustomizer<UsmaEpvRowWrapper>()
            {
                @Override
                public String getRowStyle(UsmaEpvRowWrapper wrapper)
                {
                    return wrapper.getRow() instanceof EppEpvTermDistributedRow ? "" : "font-weight: bold;";
                }
            });
        }

        dataSource.addColumn(new SimpleColumn("Компетенции", UsmaEpvRowWrapper.COMPETENCES).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRow").setDisabledProperty(UsmaEpvRowWrapper.EDIT_DISABLED).setPermissionKey("edit_epvRowExt"));
        IEntityHandler competencesEditDisableHandler = entity -> {
            final UsmaEpvRowWrapper row = (UsmaEpvRowWrapper) entity;
            return !((row.getRow() instanceof EppEpvRegistryRow) && block.equals(row.getRow().getOwner()));
        };
        dataSource.addColumn(new ActionColumn("Редактировать компетенции", ActionColumn.EDIT, "onClickEditCompetences")
                                     .setPermissionKey("edit_epvRowExt")
                                     .setDisableHandler(competencesEditDisableHandler));
        return dataSource;
    }

    private static class Model
    {
        private Map<EppEpvRow, List<EppEpvRow>> _parentMap;
        private Map<Long, Set<Long>> _row2competenceMap;
        private Transformer<Long, String> _competenceTransformer;
        private Comparator<Long> _competenceComparator;
        private Map<Long, Map<Long, Map<Long, Double>>> _dataMap;

        private Model(Map<EppEpvRow, List<EppEpvRow>> parentMap, Map<Long, Set<Long>> row2competenceMap, Transformer<Long, String> competenceTransformer, Comparator<Long> competenceComparator, Map<Long, Map<Long, Map<Long, Double>>> dataMap)
        {
            _parentMap = parentMap;
            _row2competenceMap = row2competenceMap;
            _competenceTransformer = competenceTransformer;
            _competenceComparator = competenceComparator;
            _dataMap = dataMap;
        }
    }

    @Override
    public void saveImtsaXml(EppEduPlanVersionBlock block, IUploadFile source, String fileName, String encoding, int number)
    {
        UsmaImtsaXml xml = get(UsmaImtsaXml.class, UsmaImtsaXml.block(), block);
        if (xml != null)
        {
            delete(xml);
            getSession().flush();
            getSession().clear();
        }

        File file;
        byte[] xmlTemplate;
        try
        {
            file = File.createTempFile("usmaTemp-", "xml");
            try
            {
                source.write(file);
                xmlTemplate = FileUtils.readFileToString(file, encoding).getBytes();

            } finally
            {
                file.delete();
            }

        } catch (IOException e)
        {
            throw new ApplicationException("Невозможно сохранить файл XML.");
        }

        xml = new UsmaImtsaXml(block, fileName, encoding, xmlTemplate, number);
        save(xml);
    }

    @Override
    public UsmaEpvCheckState getEpvCheckState(Long versionId)
    {
        EppEduPlanVersion version = get(EppEduPlanVersion.class, versionId);
        UsmaEpvCheckState checkState = getByNaturalId(new UsmaEpvCheckState.NaturalId(version));
        return checkState == null ? new UsmaEpvCheckState(version) : checkState;
    }

    public void doCopyEduPlanVersionSchedule(@NotNull EppEduPlanVersion source, @NotNull EppEduPlanVersion target, @NotNull Collection<Course> courses)
    {
        checkNotNull(source);
        checkNotNull(target);
        checkNotNull(courses);
        checkArgument(!courses.isEmpty());
        checkArgument(source.getEduPlan().getDevelopGrid().equals(target.getEduPlan().getDevelopGrid()));

        UsmaEduPlanVersionPartitionType srcPartitionType = this.get(UsmaEduPlanVersionPartitionType.class, UsmaEduPlanVersionPartitionType.version(), source);
        int srcPartsNumber = srcPartitionType == null ? 1 : srcPartitionType.getPartitionType().getPartsNumber();
        if (srcPartsNumber != 1)
        {
            // если разбиение учебного графика источника отлично от недельного, оно должно совпадать с учебным графиком цели
            UsmaEduPlanVersionPartitionType trgPartitionType = this.get(UsmaEduPlanVersionPartitionType.class, UsmaEduPlanVersionPartitionType.version(), target);
            int trgPartsNumber = trgPartitionType == null ? 1 : trgPartitionType.getPartitionType().getPartsNumber();

            if (srcPartsNumber != trgPartsNumber)
                throw new ApplicationException("Разбиения учебного графика выбранной и целевой УПв должны совпадать.");
        }

        // удаляем учебный график выбранных курсов
        new DQLDeleteBuilder(UsmaEduPlanVersionWeek.class)
                .where(eq(property(UsmaEduPlanVersionWeek.version()), value(target)))
                .where(in(property(UsmaEduPlanVersionWeek.course()), courses))
                .createStatement(this.getSession())
                .execute();

        {
            short scheduleWeekEntityCode = EntityRuntime.getMeta(UsmaEduPlanVersionWeek.class).getEntityCode();
            DQLSelectBuilder fromBuilder = new DQLSelectBuilder()
                    .column(DQLFunctions.createNewIdFromId(property("w.id"), scheduleWeekEntityCode))
                    .column(value(scheduleWeekEntityCode))
                    .column(value(target))
                    .column(property("w", UsmaEduPlanVersionWeek.course()))
                    .column(property("w", UsmaEduPlanVersionWeek.week()))
                    .column(property("w", UsmaEduPlanVersionWeek.term()))
                    .column(property("w", UsmaEduPlanVersionWeek.weekType()))

                    .fromEntity(UsmaEduPlanVersionWeek.class, "w")
                    .where(eq(property("w", UsmaEduPlanVersionWeek.version()), value(source)))
                    .where(in(property("w", UsmaEduPlanVersionWeek.course()), courses));

            DQLInsertSelectBuilder insertBuilder = new DQLInsertSelectBuilder(UsmaEduPlanVersionWeek.class);
            insertBuilder.setSelectRule(fromBuilder.buildSelectRule());
            insertBuilder.properties(UsmaEduPlanVersionWeek.P_ID, UsmaEduPlanVersionWeek.P_CLASS, UsmaEduPlanVersionWeek.L_VERSION, UsmaEduPlanVersionWeek.L_COURSE, UsmaEduPlanVersionWeek.L_WEEK, UsmaEduPlanVersionWeek.L_TERM, UsmaEduPlanVersionWeek.L_WEEK_TYPE);
            insertBuilder.createStatement(this.getSession()).execute();
        }

        {
            short scheduleWeekPartEntityCode = EntityRuntime.getMeta(UsmaEduPlanVersionWeekPart.class).getEntityCode();
            DQLSelectBuilder fromBuilder = new DQLSelectBuilder()
                    .column(DQLFunctions.createNewIdFromId(property("p.id"), scheduleWeekPartEntityCode))
                    .column(value(scheduleWeekPartEntityCode))
                    .column(property("wt"))
                    .column(property("p", UsmaEduPlanVersionWeekPart.partitionElementNumber()))
                    .column(property("p", UsmaEduPlanVersionWeekPart.weekType()))

                    .fromEntity(UsmaEduPlanVersionWeekPart.class, "p")
                    .joinPath(DQLJoinType.inner, UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().fromAlias("p"), "ws")
                    .where(eq(property("p", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(source)))
                    .where(in(property("p", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().course()), courses))

                    .fromEntity(UsmaEduPlanVersionWeek.class, "wt") // учебный график цели
                    .where(eq(property("wt", UsmaEduPlanVersionWeek.course()), property("ws", UsmaEduPlanVersionWeek.course())))
                    .where(eq(property("wt", UsmaEduPlanVersionWeek.week()), property("ws", UsmaEduPlanVersionWeek.week())))
                    .where(eq(property("wt", UsmaEduPlanVersionWeek.version()), value(target)));

            DQLInsertSelectBuilder insertBuilder = new DQLInsertSelectBuilder(UsmaEduPlanVersionWeekPart.class);
            insertBuilder.setSelectRule(fromBuilder.buildSelectRule());
            insertBuilder.properties(UsmaEduPlanVersionWeekPart.P_ID, UsmaEduPlanVersionWeekPart.P_CLASS, UsmaEduPlanVersionWeekPart.L_EDU_PLAN_VERSION_WEEK, UsmaEduPlanVersionWeekPart.P_PARTITION_ELEMENT_NUMBER, UsmaEduPlanVersionWeekPart.L_WEEK_TYPE);
            insertBuilder.createStatement(this.getSession()).execute();
        }
    }
}