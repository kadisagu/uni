/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionCompetenceList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.IRawFormatter;
import ru.tandemservice.uni.entity.catalog.IColoredEntity;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
public class UsmaCompetenceListWrapper extends IdentifiableWrapper<UsmaCompetence>
{
    public static final Comparator<UsmaCompetenceListWrapper> COMPETENCE_COMPARATOR = new Comparator<UsmaCompetenceListWrapper>()
    {
        @Override
        public int compare(UsmaCompetenceListWrapper o1, UsmaCompetenceListWrapper o2)
        {
            int result = o1._eppSkillGroupShortTitle.compareTo(o2._eppSkillGroupShortTitle);
            if (result == 0)
            {
                result = o1._competenceNumber.compareTo(o2._competenceNumber);
            }

            return result;
        }
    };

    private static final Comparator<UsmaEpvRowWrapper> USMA_EPV_ROW_TITLE_COMPARATOR = (o1, o2) -> o1._rowTitle.compareTo(o2._rowTitle);

    public static final IRawFormatter<Collection<UsmaEpvRowWrapper>> USMA_EPV_ROW_FORMATTER = source -> {
        if (source == null || source.isEmpty())
        {
            return "";
        }

        StringBuilder tableBuilder = new StringBuilder("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        for (UsmaEpvRowWrapper rowWrapper : source)
        {
            tableBuilder.append("<tr><td style=\"white-space: nowrap;\"><div style=\"color:");
            tableBuilder.append(rowWrapper.getHtmlColor());
            tableBuilder.append("\">");
            tableBuilder.append(rowWrapper.getRowTitle());
            tableBuilder.append("</div></td></tr>");
        }
        tableBuilder.append("</table>");
        return tableBuilder.toString();
    };

    public static final String COMPETENCE_TITLE = "competenceTitle";
    public static final String COMPETENCE_CODE = "competenceCode";
    public static final String DISCIPLINE_TITLES = "disciplineTitles";

    private String _competenceTitle;
    private String _eppSkillGroupShortTitle;
    private Integer _competenceNumber;
    private Set<UsmaEpvRowWrapper> _epvRows= new TreeSet<>(USMA_EPV_ROW_TITLE_COMPARATOR);


    public UsmaCompetenceListWrapper(Long id, String eppSkillGroupShortTitle, Integer competenceNumber, String competenceTitle)
    {
        super(id, competenceTitle);
        _competenceNumber = competenceNumber;
        _competenceTitle = competenceTitle;
        _eppSkillGroupShortTitle = eppSkillGroupShortTitle;
    }

    public void addRow(String rowTitle, String htmlColor)
    {
        _epvRows.add(new UsmaEpvRowWrapper(rowTitle, htmlColor));
    }

    public String getEppSkillGroupShortTitle()
    {
        return _eppSkillGroupShortTitle;
    }

    public void setEppSkillGroupShortTitle(String eppSkillGroupShortTitle)
    {
        _eppSkillGroupShortTitle = eppSkillGroupShortTitle;
    }

    public Integer getCompetenceNumber()
    {
        return _competenceNumber;
    }

    public void setCompetenceNumber(Integer competenceNumber)
    {
        _competenceNumber = competenceNumber;
    }

    public String getCompetenceTitle()
    {
        return _competenceTitle;
    }

    public void setCompetenceTitle(String competenceTitle)
    {
        _competenceTitle = competenceTitle;
    }


    // calculated method
    public String getCompetenceCode()
    {
        return _eppSkillGroupShortTitle + "-" + String.valueOf(_competenceNumber);
    }

    public Set<UsmaEpvRowWrapper> getDisciplineTitles()
    {
        return _epvRows;
    }


    private class UsmaEpvRowWrapper implements IColoredEntity
    {
        private String _rowTitle;
        private String _htmlColor;

        private UsmaEpvRowWrapper(String rowTitle, String htmlColor)
        {
            _rowTitle = rowTitle;
            _htmlColor = htmlColor;
        }

        public String getRowTitle()
        {
            return _rowTitle;
        }

        public String getHtmlColor()
        {
            return _htmlColor;
        }
    }
}