/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.modularextract.e32;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AdmissionStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.03.2010
 */
public class AdmissionStuExtractPrint implements IPrintFormCreator<AdmissionStuExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, AdmissionStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        List<String> tagsToDel = new ArrayList<>();
        List<String> tagsToDelWithLineAfter = new ArrayList<>();

        Person person = extract.getEntity().getPerson();

        boolean isMale = person.isMale();

        if(extract.isPaymentsDebt())
        {
            modifier.put("debtPresence", "1. " + modifier.getStringValue("Student") + " " + modifier.getStringValue("fio_N") +
                    " " + modifier.getStringValue("course") + " курса" +
                    " " + modifier.getStringValue("orgUnit_G") +
                    " " + modifier.getStringValue("developForm_GF") + " формы обучения" +
                    " " + modifier.getStringValue("compensationTypeStr_G_Alt") +
                    " приказом ректора № " + extract.getDischargeOrderNumber() +
                    " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDischargeOrderDate()) +
                    " не допущен" + (isMale ? "" : "а") + " к учебному процессу в связи с отсутствием оплаты за обучение.");
            modifier.put("payingOff", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPayingOffDate()) +
                    " г. задолженность по оплате за обучение оплачена в полном объеме.");
        }
        else
        {
            modifier.put("debtPresence", "1. Приказ об отстранении от занятий отменить.");
            tagsToDelWithLineAfter.add("payingOff");
        }
        modifier.put("entryIntoForceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getAdmissionDate()));

        short number = 2;
        RtfTableModifier table = new RtfTableModifier();
        if(extract.isHasDebts())
        {
            modifier.put("debts", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline()) + " г.");

            short i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for(StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[] {String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        }
        else
        {
            tagsToDel.add("debts");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        table.modify(document);
        
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDelWithLineAfter, false, true);
        modifier.modify(document);
        return document;
    }
}