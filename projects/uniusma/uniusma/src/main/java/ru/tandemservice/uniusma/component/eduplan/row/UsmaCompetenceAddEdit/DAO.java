/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.row.UsmaCompetenceAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        long id = model.getRow().getId();
        model.setRow(getNotNull(EppEpvRegistryRow.class, id));

        if (Arrays.asList(EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR, EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION).contains(model.getRow().getType().getCode()))
        {
            model.setPractice(true);
            UsmaPracticeDispersion dispersion = get(UsmaPracticeDispersion.class, UsmaPracticeDispersion.practice(), model.getRow());
            if (dispersion == null)
            {
                dispersion = new UsmaPracticeDispersion(model.getRow(), false);
            }

            model.setDispersion(dispersion);
        }

        model.setCompetenceModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(UsmaCompetence.class, "comp")
                        .where(likeUpper(property(UsmaCompetence.title().fromAlias("comp")), value(CoreStringUtils.escapeLike(filter))));

                if (!model.getValues().isEmpty())
                {
                    builder.where(notIn(property("comp"), model.getValues()));
                }

                if (set != null)
                    builder.where(in(property(UsmaCompetence.id().fromAlias("comp")), set));

                return new DQLListResultBuilder<UsmaCompetence>(builder);
            }
        });

        model.getValues().addAll(new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EpvRegistryRowRel.class, "comp2row")
                .where(eq(property(UsmaCompetence2EpvRegistryRowRel.registryRow().fromAlias("comp2row")), value(model.getRow())))
                .column(property(UsmaCompetence2EpvRegistryRowRel.usmaCompetence().fromAlias("comp2row")))
                .createStatement(getSession())
                .<UsmaCompetence>list());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        model.getValues().addAll(model.getSelectedValues());
        model.getDataSource().setCountRow(model.getValues().size());

        UniBaseUtils.createPage(model.getDataSource(), new ArrayList<>(model.getValues()));
    }

    @Override
    public void update(Model model)
    {
        new DQLDeleteBuilder(UsmaCompetence2EpvRegistryRowRel.class)
                .where(eq(property(UsmaCompetence2EpvRegistryRowRel.registryRow()), value(model.getRow())))
                .createStatement(getSession())
                .execute();

        getSession().flush();

        for (UsmaCompetence competence: model.getValues())
        {
            save(new UsmaCompetence2EpvRegistryRowRel(model.getRow(), competence));
        }


        if (model.isPractice())
        {
            saveOrUpdate(model.getDispersion());

            getSession().flush();
            getSession().clear();

            if (model.getDispersion().isDispersed())
            {
                IImtsaImportDAO.instance.get().updateScheduleDispersedWeekTypes(Collections.singleton(model.getDispersion()));
            }
        }
    }

    @Override
    public void updateCompetence(Model model)
    {
        model.getValues().addAll(new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2RegistryElementRel.class, "comp2regel")
                .column(property(UsmaCompetence2RegistryElementRel.usmaCompetence().fromAlias("comp2regel")))
                .where(eq(property(UsmaCompetence2RegistryElementRel.registryElement().fromAlias("comp2regel")), value(model.getRow().getRegistryElement())))
                .where(notIn(UsmaCompetence2RegistryElementRel.usmaCompetence().id().fromAlias("comp2regel"), new DQLSelectBuilder()
                        .fromEntity(UsmaCompetence2EpvRegistryRowRel.class, "comp2row")
                        .column(property(UsmaCompetence2EpvRegistryRowRel.usmaCompetence().id().fromAlias("comp2row")))
                        .where(eq(property(UsmaCompetence2EpvRegistryRowRel.registryRow().fromAlias("comp2row")), value(model.getRow())))
                        .buildQuery()))
                .createStatement(getSession())
                .<UsmaCompetence>list());
    }
}
