/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockQualificationAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        if (model.getQualificationId() != null){ model.setNumber(model.getQualification().getNumber()); }
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getQualification().setNumber(model.getNumber());
        getDao().saveOrUpdate(model.getQualification());
        deactivate(component);
    }
}