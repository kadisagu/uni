/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Shaburov
 * @since 16.07.12
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}
