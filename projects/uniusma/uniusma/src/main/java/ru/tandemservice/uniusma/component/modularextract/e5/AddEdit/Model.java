/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e5.AddEdit;

import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 29.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e5.AddEdit.Model
{
    TransferDevFormStuExtractUsmaExt _extUsmaExt;
    StuExtractToDebtRelation _currentDebt;
    private List<StuExtractToDebtRelation> _debtsList = new ArrayList<StuExtractToDebtRelation>();
    private List<StuExtractToDebtRelation> _debtsToDel = new ArrayList<StuExtractToDebtRelation>();

    public TransferDevFormStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(TransferDevFormStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }

    public StuExtractToDebtRelation getCurrentDebt()
    {
        return _currentDebt;
    }

    public void setCurrentDebt(StuExtractToDebtRelation currentDebt)
    {
        _currentDebt = currentDebt;
    }

    public List<StuExtractToDebtRelation> getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(List<StuExtractToDebtRelation> debtsList)
    {
        _debtsList = debtsList;
    }

    public List<StuExtractToDebtRelation> getDebtsToDel()
    {
        return _debtsToDel;
    }

    public void setDebtsToDel(List<StuExtractToDebtRelation> debtsToDel)
    {
        _debtsToDel = debtsToDel;
    }
}
