/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.entrant.EntrantRequestTab;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.base.entity.codes.AddressCountryTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author oleyba
 * @since 10.06.2009
 */
@SuppressWarnings("deprecation")
public class EntrantRequestPrint extends UniBaseDao implements IEntrantRequestPrint
{
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        final RtfDocument document = new RtfReader().read(template);

        new DateFormatter(DateFormatter.PATTERN_JUST_YEAR);

        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        final Entrant entrant = entrantRequest.getEntrant(); // абитуриент
        final IdentityCard entrantIdCard = entrant.getPerson().getIdentityCard(); // удостоверение личности

        // направления подготовки, выбранные в заявлении
        List<RequestedEnrollmentDirection> directions = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);
        // первое - общая информация определяется по нему
        RequestedEnrollmentDirection firstPriorityDirection = directions.get(0);
        final EducationOrgUnit eduOrgUnit = firstPriorityDirection.getEnrollmentDirection().getEducationOrgUnit();
        // последнее законченное учебное заведение - общая информация определяется по нему
        PersonEduInstitution lastEduInstitution = entrant.getPerson().getPersonEduInstitution();

        // всякие данные абитуриента в ассортименте
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("regNum", entrantRequest.getStringNumber());
        injectModifier.put("entrantNumber", entrantRequest.getEntrant().getPersonalNumber());
        injectModifier.put("lastName", entrantIdCard.getLastName());
        injectModifier.put("firstName", entrantIdCard.getFirstName());
        injectModifier.put("middleName", entrantIdCard.getMiddleName());
        injectModifier.put("birthdate", entrant.getPerson().getBirthDateStr());
        injectModifier.put("birthplace", entrantIdCard.getBirthPlace());
        injectModifier.put("citizenship", entrantIdCard.getCitizenship().getTitle());
        injectModifier.put("documentNumberWithType", getDocumentNumberWithType(entrantIdCard));
        injectModifier.put("issuance", getIssuance(entrantIdCard));
        injectModifier.put("livingAddress", getLivingAddress(entrant));
        injectModifier.put("phones", entrant.getPerson().getContactData().getMainPhones());
        injectModifier.put("orgUnit", eduOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle());
        injectModifier.put("eduLevelType", getEduLevelType(eduOrgUnit));
        injectModifier.put("eduLevelTitle", eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());
        injectModifier.put("developForm", getDevelopForm(eduOrgUnit.getDevelopForm().getCode()));
        injectModifier.put("compensationType", getCompensationType(firstPriorityDirection));
        injectModifier.put("eduInstitution", getEduInstitution(lastEduInstitution));
        injectModifier.put("foreignLanguage", getForeignLanguage(entrant));
        injectModifier.put("profileWorkExperience", getProfileWorkExperience(firstPriorityDirection));
        injectModifier.put("benefit", getBenefit(entrant, true));
        injectModifier.put("needHotel", entrant.getPerson().isNeedDormitory() ? "нуждаюсь" : "не нуждаюсь");
        injectModifier.put("additionalInfo", entrant.getAdditionalInfo());
        injectModifier.put("regDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.getRegDate()));
        injectModifier.put("female", SexCodes.FEMALE.equals(entrantIdCard.getSex().getCode()) ? "а" : "");
        injectModifier.put("orgUnitWithoutType", eduOrgUnit.getFormativeOrgUnit().getTitle());
        injectModifier.put("FIO", entrantIdCard.getFullFio());
        injectModifier.put("sex", StringUtils.lowerCase(entrantIdCard.getSex().getTitle()));
        injectModifier.put("birthdateExtended", getBirthdateExtended(entrantIdCard));
        injectModifier.put("livingPlace", getLivingPlace(entrantIdCard.getAddress()));
        injectModifier.put("address", entrantIdCard.getAddress() == null ? "" : entrantIdCard.getAddress().getTitleWithFlat());
        injectModifier.put("certificate", lastEduInstitution == null ? "" : getCertificate(lastEduInstitution));
        injectModifier.put("schoolType", lastEduInstitution == null ? "нет" : lastEduInstitution.getEduInstitutionKind().getTitle());
        injectModifier.put("averageMark", getAverageMark(lastEduInstitution));
        injectModifier.put("medalExist", lastEduInstitution == null ? "нет" : (lastEduInstitution.getGraduationHonour() == null ? "нет" : "да"));
        injectModifier.put("graduationYear", lastEduInstitution == null ? "" : Integer.toString(lastEduInstitution.getYearEnd()));
        injectModifier.put("entrantAccessCourse", getEntrantAccessCourse(entrant));
        injectModifier.put("contract", getContract(firstPriorityDirection));
        injectModifier.put("targetAdmission", firstPriorityDirection.isTargetAdmission() ? "да" : "нет");
        injectModifier.put("competitionKindExtended", getCompetitionKindExtended(entrant, firstPriorityDirection));
        injectModifier.put("documentType", entrantIdCard.getCardType().getTitle());
        injectModifier.put("documentNumberWithIssuance", getDocumentNumberWithIssuance(entrantIdCard));
        injectModifier.put("disciplines", getDisciplines(entrantRequest));
        injectModifier.modify(document);

        // таблица с данными о родителях
        List<String[]> personNextOfKinList = new ArrayList<>();
        {
            MQBuilder builder = new MQBuilder(PersonNextOfKin.ENTITY_CLASS, "pnk");
            builder.add(MQExpression.eq("pnk", PersonNextOfKin.L_PERSON, entrant.getPerson()));
            for (PersonNextOfKin personNextOfKin : builder.<PersonNextOfKin>getResultList(getSession()))
                personNextOfKinList.add(new String[]{personNextOfKin.getRelationDegree().getTitle(), getNextOfKinDetails(personNextOfKin)});
        }
        if (personNextOfKinList.isEmpty()) personNextOfKinList.add(new String[]{""});
        RtfTableModifier nextOfKinTableModifier = new RtfTableModifier().put("TNOK", personNextOfKinList.toArray(new String[personNextOfKinList.size()][]));
        nextOfKinTableModifier.modify(document);

        // таблица с результатами ЕГЭ и дипломами олимпиад
        List<String[]> markTable = new ArrayList<>();
        // таблица с результатами ЕГЭ
        List<String[]> stateExamTable = new ArrayList<>();
        {
            // заполняем кучу всяких карт для вычисления и пересчета имеющихся оценок
            // по дисциплинам первого выбранного направления
            Map<Discipline2RealizationWayRelation, Set<String>> discipline2passFormCode = new HashMap<>();
            for (Discipline2RealizationFormRelation formRelation : getList(Discipline2RealizationFormRelation.class))
            {
                Set<String> formSet = discipline2passFormCode.get(formRelation.getDiscipline());
                if (formSet == null)
                    discipline2passFormCode.put(formRelation.getDiscipline(), formSet = new HashSet<>());
                formSet.add(formRelation.getSubjectPassForm().getCode());
            }
            Map<Discipline2RealizationWayRelation, ConversionScale> discipline2scaleMap = UniecDAOFacade.getEntrantDAO().getConversionScales(entrant.getEnrollmentCampaign());
            Map<StateExamSubject, Set<StateExamSubjectMark>> discipline2stateExamMarkMap = UniecDAOFacade.getEntrantDAO().getStateExamMarks(entrant);
            Map<Discipline2RealizationWayRelation, OlympiadDiploma> discipline2olympiadMap = getOlympiadDiplomas(entrant);
            // дисциплины первого направления в заявлении
            MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "c", new String[]{ChosenEntranceDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE});
            builder.add(MQExpression.eq("c", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, firstPriorityDirection));
            builder.setNeedDistinct(true);
            List<Discipline2RealizationWayRelation> list = builder.getResultList(getSession());
            // вычисляем оценки по ЕГЭ и дипломам для дисциплин
            for (Discipline2RealizationWayRelation discipline : list)
            {
                Double stateExamScaledMark = null;
                StateExamSubjectMark stateExamMark = null;
                Double olympiadMark = null;
                // вычисляем возможный балл по ЕГЭ
                Set<String> subjectPassFormSet = discipline2passFormCode.get(discipline);
                if (subjectPassFormSet != null && subjectPassFormSet.contains(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM))
                {
                    ConversionScale conversionScale = discipline2scaleMap.get(discipline);
                    if (conversionScale != null && conversionScale.getSubject() != null)
                    {
                        Set<StateExamSubjectMark> stateExamMarkSet = discipline2stateExamMarkMap.get(conversionScale.getSubject());
                        if (stateExamMarkSet != null)
                        {
                            // вычисляем максимальную оценку
                            for (StateExamSubjectMark mark : stateExamMarkSet)
                                stateExamMark = (stateExamMark == null ? mark : (mark.getMark() > stateExamMark.getMark() ? mark : stateExamMark));

                            Double budgetMark = conversionScale.getInternalMark(stateExamMark.getMark(), true);
                            Double contractMark = conversionScale.getInternalMark(stateExamMark.getMark(), false);
                            if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(firstPriorityDirection.getCompensationType().getCode()))
                                stateExamScaledMark = budgetMark;
                            else
                                stateExamScaledMark = contractMark;
                        }
                    }
                }
                // вычисляем зачтение по олимпиаде
                OlympiadDiploma diploma = discipline2olympiadMap.get(discipline);
                if (diploma != null)
                    olympiadMark = (double) discipline.getMaxMark();

                // формируем строку, если нашли оценку
                if (stateExamScaledMark == null && olympiadMark == null) continue;
                Double mark = .0;
                if (stateExamScaledMark != null && olympiadMark != null)
                    mark = Math.max(stateExamScaledMark, olympiadMark);
                if (stateExamScaledMark == null) mark = olympiadMark;
                if (olympiadMark == null) mark = stateExamScaledMark;

                DoubleFormatter markFormatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;

                // в первой таблице:
                // дисциплина, балл, балл (если ЕГЭ), место (если олимпиада), номер документа
                // во второй таблице (только результаты ЕГЭ):
                // дисциплина, балл
                if (mark.equals(olympiadMark))
                {
                    markTable.add(new String[]{
                            discipline.getTitle(),
                            markFormatter.format(mark),
                            "",
                            diploma.getDegree().getTitle(),
                            diploma.getNumber()
                    });
                }
                if (mark.equals(stateExamScaledMark))
                {
                    markTable.add(new String[]{
                            stateExamMark.getSubject().getTitle(),
                            markFormatter.format(mark),
                            Integer.toString(stateExamMark.getMark()),
                            "",
                            EntrantStateExamCertificateNumberFormatter.formatStatic(stateExamMark.getCertificate().getNumber())
                    });
                    stateExamTable.add(new String[]{
                            stateExamMark.getSubject().getTitle(),
                            markFormatter.format(mark),
                    });
                }
            }
        }

        // если таблицы пустые - печатаем по одной пустой строке
        if (markTable.isEmpty()) markTable.add(new String[]{""});
        if (stateExamTable.isEmpty()) stateExamTable.add(new String[]{""});

        RtfTableModifier markTableModifier = new RtfTableModifier().put("MT", markTable.toArray(new String[markTable.size()][]));
        markTableModifier.modify(document);
        RtfTableModifier stateExamTableModifier = new RtfTableModifier().put("SET", stateExamTable.toArray(new String[stateExamTable.size()][]));
        stateExamTableModifier.modify(document);

        return document;
    }

    /**
     * @param entrantRequest заявление абитуриента
     * @return дисциплины
     */
    @SuppressWarnings("unchecked")
    private String getDisciplines(EntrantRequest entrantRequest)
    {
        Entrant entrant = entrantRequest.getEntrant();
        boolean formingForEntrant = entrant.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);

        Criteria criteria = getSession().createCriteria(ExamPassDiscipline.class);
        criteria.createAlias(ExamPassDiscipline.L_ENTRANT_EXAM_LIST, "examList");
        criteria.add(Restrictions.eq("examList." + EntrantExamList.L_ENTRANT, (formingForEntrant) ? entrant : entrantRequest));
        criteria.setProjection(Projections.distinct(Projections.property(ExamPassDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE)));

        List<Discipline2RealizationWayRelation> result = criteria.list();
        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        return UniStringUtils.join(result, Discipline2RealizationWayRelation.P_TITLE, ", ");
    }

    private String getBirthdateExtended(IdentityCard entrantIdCard)
    {
        if (entrantIdCard.getBirthDate() == null) return "";
        SimpleDateFormat MONTH_STRING_DATE_FORMAT = new SimpleDateFormat("dd MMMMM yyyy г.", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU);
        return MONTH_STRING_DATE_FORMAT.format(entrantIdCard.getBirthDate());
    }

    /**
     * @param entrant абитуриент
     * @return адрес проживания
     */
    private String getLivingAddress(Entrant entrant)
    {
        AddressBase address = entrant.getPerson().getAddress();
        if (address == null) return "";
        return address.getTitleWithFlat();
    }

    /**
     * @param entrant                абитуриент
     * @param firstPriorityDirection выбранное направление подготовки
     * @return основание - вид конкурса + льготы, если вне конкурса
     */
    private String getCompetitionKindExtended(Entrant entrant, RequestedEnrollmentDirection firstPriorityDirection)
    {
        String result = StringUtils.lowerCase(firstPriorityDirection.getCompetitionKind().getTitle());
        if (!UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(firstPriorityDirection.getCompetitionKind().getCode()))
            return result;
        String benefit = StringUtils.trimToNull(getBenefit(entrant, false));
        if (null != benefit)
            result = result + " (" + benefit + ")";
        return result;
    }

    /**
     * @param entrant абитуриент
     * @return подготовительные курсы
     */
    private String getEntrantAccessCourse(Entrant entrant)
    {
        String entrantAccessCourse = StringUtils.join(CommonBaseUtil.getPropertiesList(getList(EntrantAccessCourse.class, EntrantAccessCourse.L_ENTRANT, entrant), "title"), ", ");
        if (StringUtils.isBlank(entrantAccessCourse)) entrantAccessCourse = "нет";
        return entrantAccessCourse;
    }

    /**
     * @param entrant  абитуриент
     * @param notEmpty заменять ли пустое значение словом "нет"
     * @return льготы
     */
    private String getBenefit(Entrant entrant, boolean notEmpty)
    {
        String benefit = StringUtils.lowerCase(StringUtils.join(CommonBaseUtil.getPropertiesList(getList(PersonBenefit.class, PersonBenefit.L_PERSON, entrant.getPerson()), "title"), ", "));
        if (StringUtils.isBlank(benefit) && notEmpty) benefit = "нет";
        return benefit;
    }

    /**
     * @param entrant абитуриент
     * @return иностранные языки
     */
    private String getForeignLanguage(Entrant entrant)
    {
        String foreignLanguage = StringUtils.lowerCase(StringUtils.join(CommonBaseUtil.getPropertiesList(getList(PersonForeignLanguage.class, PersonForeignLanguage.L_PERSON, entrant.getPerson()), PersonForeignLanguage.L_LANGUAGE + ".title"), ", "));
        if (StringUtils.isBlank(foreignLanguage)) foreignLanguage = "нет";
        return foreignLanguage;
    }

    /**
     * @param lastEduInstitution учебное заведение
     * @return средний балл по аттестату
     */
    private String getAverageMark(PersonEduInstitution lastEduInstitution)
    {
        if (lastEduInstitution == null) return "";
        Double mark = .0;
        Double marksCount = .0;
        mark = mark + 3 * (lastEduInstitution.getMark3() == null ? 0 : lastEduInstitution.getMark3());
        marksCount = marksCount + (lastEduInstitution.getMark3() == null ? 0 : lastEduInstitution.getMark3());
        mark = mark + 4 * (lastEduInstitution.getMark4() == null ? 0 : lastEduInstitution.getMark4());
        marksCount = marksCount + (lastEduInstitution.getMark4() == null ? 0 : lastEduInstitution.getMark4());
        mark = mark + 5 * (lastEduInstitution.getMark5() == null ? 0 : lastEduInstitution.getMark5());
        marksCount = marksCount + (lastEduInstitution.getMark5() == null ? 0 : lastEduInstitution.getMark5());
        return mark == 0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark / marksCount);
    }

    /**
     * @param address адрес
     * @return место жительства - конкретизация относительно ОУ
     */
    private String getLivingPlace(AddressBase address)
    {
        if (address == null) return "";
        // загружаем местоположение ОУ
        AddressDetailed highSchoolAdress = TopOrgUnit.getInstance().getLegalAddress();

        // если абитуриент живет в том же городе - печатаем город
        if (highSchoolAdress != null && highSchoolAdress.getSettlement() != null && address instanceof AddressDetailed)
        {
            if (highSchoolAdress.getSettlement().equals(((AddressDetailed)address).getSettlement()))
                return highSchoolAdress.getSettlement().getTitle();
            // аналогично - область
            if (highSchoolAdress.getSettlement().getParent() != null && ((AddressDetailed)address).getSettlement() != null && highSchoolAdress.getSettlement().getParent().equals(((AddressDetailed)address).getSettlement().getParent()))
                return highSchoolAdress.getSettlement().getParent().getTitle();
            // аналогично - страну
            if (highSchoolAdress.getSettlement().getCountry() != null && ((AddressDetailed)address).getSettlement() != null && highSchoolAdress.getSettlement().getCountry().equals(((AddressDetailed)address).getSettlement().getCountry()))
                return highSchoolAdress.getSettlement().getCountry().getTitle();
        }
        // для ближнего зарубежья - СНГ
        Object countryCode = address.getProperty(Address.L_COUNTRY + "." + AddressCountry.L_COUNTRY_TYPE + "." + AddressCountryType.P_CODE);
        if (countryCode != null && ((Integer) countryCode).equals(AddressCountryTypeCodes.NEAR))
            return "СНГ";
        // отчаялись
        return "";
    }

    /**
     * @param nextOfKin родственник
     * @return данные ближайшего родственника абитуриента
     */
    private String getNextOfKinDetails(PersonNextOfKin nextOfKin)
    {
        StringBuilder result = new StringBuilder();
        result.append(nextOfKin.getFio());
        if (nextOfKin.getAddress() != null)
            result.append("; ").append(nextOfKin.getAddress().getTitleWithFlat());
        if (nextOfKin.getEmploymentPlace() != null)
        {
            result.append("; ").append(nextOfKin.getEmploymentPlace());
            if (nextOfKin.getPost() != null)
                result.append(" (").append(nextOfKin.getPost()).append(")");
        } else if (nextOfKin.getPost() != null)
            result.append("; ").append(nextOfKin.getPost());
        return result.toString();
    }

    /**
     * @param firstPriorityDirection выбранное направление подготовки
     * @return опыт работы по профильной специальности
     */
    private String getProfileWorkExperience(RequestedEnrollmentDirection firstPriorityDirection)
    {
        StringBuilder result = new StringBuilder(CommonBaseDateUtil.getDatePeriodWithNames(firstPriorityDirection.getProfileWorkExperienceYears(), firstPriorityDirection.getProfileWorkExperienceMonths(), firstPriorityDirection.getProfileWorkExperienceDays()));
        if (!StringUtils.isEmpty(firstPriorityDirection.getProfilePost()))
            result.append(" (").append(firstPriorityDirection.getProfilePost()).append(")");
        return result.length() == 0 ? "" : StringUtils.lowerCase(result.toString());
    }

    /**
     * @param lastEduInstitution учебное заведение
     * @return информация об учебном заведении
     */
    private String getEduInstitution(PersonEduInstitution lastEduInstitution)
    {
        if (lastEduInstitution == null) return "нет";
        String title = lastEduInstitution.getEduInstitutionTitle();
        if (lastEduInstitution.getGraduationHonour() != null)
            title = title + " (" + StringUtils.lowerCase(lastEduInstitution.getGraduationHonour().getTitle()) + ")";
        title = title + ", " + lastEduInstitution.getYearEnd() + " год";
        return title;
    }

    /**
     * @param firstPriorityDirection выбранное направление подготовки
     * @return фраза про вариант возмещения затрат
     */
    private String getCompensationType(RequestedEnrollmentDirection firstPriorityDirection)
    {
        if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(firstPriorityDirection.getCompensationType().getCode()))
            return "финансируемые из федерального бюджета";
        else
            return "с полным возмещением затрат";
    }

    /**
     * @param firstPriorityDirection выбранное направление подготовки
     * @return контракт - да\нет
     */
    private String getContract(RequestedEnrollmentDirection firstPriorityDirection)
    {
        return (firstPriorityDirection.getCompensationType().isBudget()) ? "нет" : "да";
    }

    /**
     * @param developFormCode код формы освоения
     * @return фраза про форму освоения
     */
    private String getDevelopForm(String developFormCode)
    {
        if (DevelopFormCodes.FULL_TIME_FORM.equals(developFormCode))
            return "по очной форме обучения";
        else if (DevelopFormCodes.CORESP_FORM.equals(developFormCode))
            return "по заочной форме обучения";
        else if (DevelopFormCodes.PART_TIME_FORM.equals(developFormCode))
            return "по очно-заочной форме обучения";
        else if (DevelopFormCodes.EXTERNAL_FORM.equals(developFormCode))
            return "по форме обучения «экстернат»";
        else if (DevelopFormCodes.APPLICANT_FORM.equals(developFormCode))
            return "по форме обучения «самостоятельное обучение и итоговая аттестация»";
        return "";
    }

    /**
     * @param eduOrgUnit - направление подготовки
     * @return тип уровня образования - направление, специальность - в дательном падеже
     */
    private String getEduLevelType(EducationOrgUnit eduOrgUnit)
    {
        return eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType().getDativeCaseSimpleShortTitle();
    }

    /**
     * @param entrantIdCard удостоверение личности абитуриента
     * @return когда и кем выдан документ
     */
    private String getIssuance(IdentityCard entrantIdCard)
    {
        String issuance = entrantIdCard.getIssuancePlace();
        if (StringUtils.isNotBlank(issuance) && entrantIdCard.getIssuanceCode() != null)
            issuance = issuance + ", код подразделения " + entrantIdCard.getIssuanceCode();
        if (StringUtils.isNotBlank(issuance) && entrantIdCard.getIssuanceDate() != null)
            issuance = issuance + ", " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantIdCard.getIssuanceDate());
        return issuance;
    }

    /**
     * @param entrantIdCard удостоверение личности абитуриента
     * @return название документа с типом документа
     */
    private String getDocumentNumberWithType(IdentityCard entrantIdCard)
    {
        String documentFullNumber = entrantIdCard.getCardType().getShortTitle();
        if (entrantIdCard.getSeria() != null || entrantIdCard.getNumber() != null)
            documentFullNumber = documentFullNumber + ", ";
        documentFullNumber = documentFullNumber + getDocumentNumber(entrantIdCard);
        return documentFullNumber;
    }

    /**
     * @param entrantIdCard удостоверение личности абитуриента
     * @return название документа с данными о выдавшем учреждении и дате выдачи
     */
    private String getDocumentNumberWithIssuance(IdentityCard entrantIdCard)
    {
        String result = getDocumentNumber(entrantIdCard);
        String issuance = getIssuance(entrantIdCard);
        if (StringUtils.isNotBlank(result) && StringUtils.isNotBlank(issuance))
            result = result + ", ";
        if (StringUtils.isNotBlank(issuance)) result = result + issuance;
        return result;
    }

    /**
     * @param entrantIdCard удостоверение личности абитуриента
     * @return название документа
     */
    private String getDocumentNumber(IdentityCard entrantIdCard)
    {
        String documentFullNumber = "";
        if (entrantIdCard.getSeria() != null)
            documentFullNumber = documentFullNumber + "серия: " + entrantIdCard.getSeria() + " ";
        if (entrantIdCard.getNumber() != null)
            documentFullNumber = documentFullNumber + "№ " + entrantIdCard.getNumber();
        return documentFullNumber;
    }

    /**
     * @param lastEduInstitution - последнее законченное учебное заведение
     * @return данные о полученном образовании
     */
    private String getCertificate(PersonEduInstitution lastEduInstitution)
    {
        StringBuilder result = new StringBuilder();
        result.append(lastEduInstitution.getDocumentType().getTitle()).append(", ");
        if (null != lastEduInstitution.getSeria())
            result.append(lastEduInstitution.getSeria()).append(", ");
        if (null != lastEduInstitution.getNumber())
            result.append(lastEduInstitution.getNumber()).append(", ");
        result.append(" ").append(lastEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }

    private Map<Discipline2RealizationWayRelation, OlympiadDiploma> getOlympiadDiplomas(Entrant entrant)
    {
        Map<Discipline2RealizationWayRelation, OlympiadDiploma> map = new HashMap<>();
        MQBuilder builder = new MQBuilder(Discipline2OlympiadDiplomaRelation.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", Discipline2OlympiadDiplomaRelation.diploma().entrant(), entrant));
        for (Discipline2OlympiadDiplomaRelation relation : builder.<Discipline2OlympiadDiplomaRelation>getResultList(getSession()))
            map.put(relation.getDiscipline(), relation.getDiploma());
        return map;
    }
}
