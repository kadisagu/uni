/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e7.AddEdit;

import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferEduTypeStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.TransferEduTypeStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 29.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;

        // обращаемся к списку задолженностей
        if (m.isEditForm())
            m.setDebtsList(MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(m.getExtract()));

        // даже если задолженностей нет - они будут
        if (m.getDebtsList().isEmpty())
            m.getDebtsList().add(new StuExtractToDebtRelation());

        TransferEduTypeStuExtractUsmaExt extractExt = getByNaturalId(new TransferEduTypeStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            // если расширения нет - создадим его
            extractExt = new TransferEduTypeStuExtractUsmaExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setExtUsmaExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);

        for (StuExtractToDebtRelation rel : m.getDebtsList())
        {
            if (m.getExtUsmaExt().isHasDebts())
            {
                rel.setExtract(m.getExtract());
                saveOrUpdate(rel);
            }
            else if (null != rel.getId())
                delete(rel);
        }

        for(StuExtractToDebtRelation rel : m.getDebtsToDel())
        {
            if (null != rel.getId())
                delete(rel);
        }

        saveOrUpdate(m.getExtUsmaExt());
    }
}