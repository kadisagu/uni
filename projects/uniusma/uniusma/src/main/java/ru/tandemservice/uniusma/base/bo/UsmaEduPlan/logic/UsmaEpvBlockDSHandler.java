/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ImtsaReImport.UsmaEduPlanImtsaReImport;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.util.UsmaEpvBlockWrapper;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public class UsmaEpvBlockDSHandler extends DefaultSearchDataSourceHandler
{
    public UsmaEpvBlockDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String number = context.get(UsmaEduPlanImtsaReImport.BIND_NUMBER);
        Collection<EduProgramSubject> programSubjects = context.get(UsmaEduPlanImtsaReImport.BIND_PROGRAM_SUBJECT);
        Collection<EduProgramForm> programForms = context.get(UsmaEduPlanImtsaReImport.BIND_PROGRAM_FORM);
        Collection<DevelopCondition> developConditions = context.get(UsmaEduPlanImtsaReImport.BIND_DEVELOP_CONDITION);
        Collection<EduProgramTrait> programTraits = context.get(UsmaEduPlanImtsaReImport.BIND_PROGRAM_TRAIT);
        Collection<DevelopGrid> developGrids = context.get(UsmaEduPlanImtsaReImport.BIND_DEVELOP_GRID);

        IDQLSelectableQuery xmlBlockVersionQuery = new DQLSelectBuilder()
                .fromEntity(UsmaImtsaXml.class, "x")
                .column(property("x", UsmaImtsaXml.block().eduPlanVersion().id()))
                .predicate(DQLPredicateType.distinct)
                .buildQuery();

        DQLSelectBuilder versionBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, "v")
                .fromEntity(EppEduPlanProf.class, "p")
                 .column(property("v", EppEduPlanVersion.id()))
                .where(in(property("v", EppEduPlanVersion.id()), xmlBlockVersionQuery))
                .where(eq(property("v", EppEduPlanVersion.eduPlan()), property("p")));

        if (number != null)
        {
            versionBuilder.where(likeUpper(
                    DQLFunctions.concat(
                            property("p", EppEduPlan.number()),
                            value("."),
                            property("v", EppEduPlanVersion.number())),
                    value(CoreStringUtils.escapeLike(number, true))));
        }

        FilterUtils.applySelectFilter(versionBuilder, EppEduPlanProf.programSubject().fromAlias("p"), programSubjects);
        FilterUtils.applySelectFilter(versionBuilder, EppEduPlanProf.programForm().fromAlias("p"), programForms);
        FilterUtils.applySelectFilter(versionBuilder, EppEduPlanProf.developCondition().fromAlias("p"), developConditions);
        FilterUtils.applySelectFilter(versionBuilder, EppEduPlanProf.programTrait().fromAlias("p"), programTraits);
        FilterUtils.applySelectFilter(versionBuilder, EppEduPlanProf.developGrid().fromAlias("p"), developGrids);

        Map<EppEduPlanVersion, List<EppEduPlanVersionBlock>> versionBlockMap = SafeMap.get(ArrayList.class);
        DQLSelectBuilder blockBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, "b")
                .where(in(property("b", EppEduPlanVersionBlock.eduPlanVersion().id()), versionBuilder.buildQuery()));

        for (EppEduPlanVersionBlock block: blockBuilder.createStatement(context.getSession()).<EppEduPlanVersionBlock>list())
        {
            versionBlockMap.get(block.getEduPlanVersion()).add(block);
        }

        Transformer<EppEduPlanVersionBlock, String> blockEduLevelTitleTransformer = EppEduPlanVersionBlock::getTitle;

        List<UsmaEpvBlockWrapper> wrappers = new ArrayList<>();
        for (Map.Entry<EppEduPlanVersion, List<EppEduPlanVersionBlock>> versionEntry: versionBlockMap.entrySet())
        {
            EppEduPlanVersion version = versionEntry.getKey();
            List<EppEduPlanVersionBlock> blocks = versionEntry.getValue();

            Collections.sort(blocks, EppEduPlanVersionBlock.COMPARATOR);

            wrappers.add(new UsmaEpvBlockWrapper(version, CollectionUtils.collect(blocks, blockEduLevelTitleTransformer)));
        }

        Collections.sort(wrappers, UsmaEpvBlockWrapper.COMPARATOR);

        return ListOutputBuilder.get(input, wrappers).pageable(true).build();
    }
}