package ru.tandemservice.uniusma.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniusma.entity.catalog.gen.UsmaVaccinationSignGen;

/**
 * Признаки вакцинации
 */
public class UsmaVaccinationSign extends UsmaVaccinationSignGen implements IDynamicCatalogItem
{
}