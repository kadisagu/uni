package ru.tandemservice.uniusma.entity.studentmodularorder;

import ru.tandemservice.uniusma.entity.studentmodularorder.gen.*;

/**
 * Расширение выписки из сборного приказа по студенту. Об отстранении от занятий в связи с задолженностью по оплате за обучение
 */
public class DischargingStuExtractUsmaExt extends DischargingStuExtractUsmaExtGen
{
}