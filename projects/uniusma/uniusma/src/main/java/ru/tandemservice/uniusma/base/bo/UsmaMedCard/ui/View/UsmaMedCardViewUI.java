/* $Id$ */
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.logic.UsmaMedCardPrinter;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.DiseaseAddEdit.UsmaMedCardDiseaseAddEdit;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.FluorographyAddEdit.UsmaMedCardFluorographyAddEdit;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.OtherAddEdit.UsmaMedCardOtherAddEdit;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.VaccineAddEdit.UsmaMedCardVaccineAddEdit;
import ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dao.CommonDAO.proxy;


/**
 * @author DMITRY KNYAZEV
 * @since 24.06.2014
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entityId", required = true)})
public class UsmaMedCardViewUI extends UIPresenter
{
    private Long entityId;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UsmaMedCardView.STUDENT_ID, getEntityId());
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onEditEntityFromList()
    {
        Object o = proxy(getListenerParameterAsLong());
        if (o instanceof UsmaStudentVaccineInfo)
            onAddEditVaccineInfoFromList(getListenerParameterAsLong());
        if (o instanceof UsmaStudentFluorographyInfo)
            onAddEditFluorographyInfoFromList(getListenerParameterAsLong());
        if (o instanceof UsmaStudentDiseaseInfo)
            onAddEditDiseaseInfoFromList(getListenerParameterAsLong());
        if (o instanceof UsmaStudentOtherInfo)
            onAddEditOtherInfoFromList(getListenerParameterAsLong());
    }

    public void onAddEditVaccineInfoFromList(Long entityId)
    {
        _uiActivation.asCurrent(UsmaMedCardVaccineAddEdit.class).parameter("entityId", entityId).activate();
    }

    public void onAddVaccineInfoFromList()
    {
        onAddEditVaccineInfoFromList(null);
    }

    public void onAddEditFluorographyInfoFromList(Long entityId)
    {
        _uiActivation.asCurrent(UsmaMedCardFluorographyAddEdit.class).parameter("entityId", entityId).activate();
    }

    public void onAddFluorographyInfoFromList()
    {
        onAddEditFluorographyInfoFromList(null);
    }

    public void onAddEditDiseaseInfoFromList(Long entityId)
    {
        _uiActivation.asCurrent(UsmaMedCardDiseaseAddEdit.class).parameter("entityId", entityId).activate();
    }

    public void onAddDiseaseInfoFromList()
    {
        onAddEditDiseaseInfoFromList(null);
    }

    public void onAddEditOtherInfoFromList(Long entityId)
    {
        _uiActivation.asCurrent(UsmaMedCardOtherAddEdit.class).parameter("entityId", entityId).activate();
    }

    public void onAddOtherInfoFromList()
    {
        onAddEditOtherInfoFromList(null);
    }

    public void onPrintList()
    {
        new UsmaMedCardPrinter().printMedCard(Arrays.asList((Student) DataAccessServices.dao().getNotNull(getEntityId())));
    }

    @SuppressWarnings("UnusedDeclaration")
    public String getCurrentItemAlert()
    {
        return "Удалить сведения?";
    }

    public Long getEntityId()
    {
        return entityId;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setEntityId(Long entityId)
    {
        this.entityId = entityId;
    }
}