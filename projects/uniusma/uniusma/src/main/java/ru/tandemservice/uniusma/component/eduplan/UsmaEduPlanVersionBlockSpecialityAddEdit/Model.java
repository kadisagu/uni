/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockSpecialityAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniusma.entity.UsmaSpeciality;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input({
    @Bind(key = "specialityId", binding = "specialityId"),
    @Bind(key = "blockId", binding = "blockId")
})
public class Model
{
    private Long _specialityId;
    private Long _blockId;
    private UsmaSpeciality _speciality;
    private Integer _number;

    public Long getSpecialityId()
    {
        return _specialityId;
    }

    public void setSpecialityId(Long specialityId)
    {
        _specialityId = specialityId;
    }

    public Long getBlockId()
    {
        return _blockId;
    }

    public void setBlockId(Long blockId)
    {
        _blockId = blockId;
    }

    public UsmaSpeciality getSpeciality()
    {
        return _speciality;
    }

    public void setSpeciality(UsmaSpeciality speciality)
    {
        _speciality = speciality;
    }

    public Integer getNumber()
    {
        return _number;
    }

    public void setNumber(Integer number)
    {
        _number = number;
    }
}