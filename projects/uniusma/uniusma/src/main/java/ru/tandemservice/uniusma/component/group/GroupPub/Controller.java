package ru.tandemservice.uniusma.component.group.GroupPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.logic.UsmaMedCardPrinter;
import ru.tandemservice.uniusma.entity.catalog.codes.UniScriptItemCodes;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 24.01.14
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends ru.tandemservice.uni.component.group.GroupPub.Controller
{

    public void onPrintUSMAStudentsReport(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.group.GroupPub.GroupListPrintDialog", Collections.singletonMap("groupId", (Object) getModel(component).getGroupId())));
    }

    public void onClickPrintGroupStudentMedCardsList(final IBusinessComponent component)
    {
        new UsmaMedCardPrinter().printMedCard(getModel(component).getGroup().getId());
    }

    public void onClickPrintGroupStudentPersonalCardTitlesList(final IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_TITLE_PRINT_SCRIPT);
            final RtfDocument rtfDocument = new RtfDocument();
            rtfDocument.getElementList().clear();
            final List<Student> list = DataAccessServices.dao().getList(Student.class, Student.group(), getModel(component).getGroup())
                    .stream()
                    .sorted((student1, student2) -> student1.getFullFio().compareTo(student2.getFullFio()))
                    .collect(Collectors.toList());
            for (Student student : list)
            {
                final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, student.getId());
                final RtfDocument document = (RtfDocument) scriptResult.get("rtf");
                rtfDocument.addElement(document);
                if (list.indexOf(student) < list.size() - 1)
                    rtfDocument.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                else
                {
                    rtfDocument.setSettings(document.getSettings());
                    rtfDocument.setHeader(document.getHeader());
                }
            }
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("StudentCardTitle.rtf").document(rtfDocument), true);
        } else
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARDS_GROUP_LIST, Collections.singletonMap("groupId", (Object) getModel(component).getGroup().getId())));
    }

    public void onClickPrintGroupStudentPersonalCardDatasList(final IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_DATA_PRINT_SCRIPT);
            final RtfDocument rtfDocument = new RtfDocument();
            rtfDocument.getElementList().clear();
            final List<Student> list = DataAccessServices.dao().getList(Student.class, Student.group(), getModel(component).getGroup())
                    .stream()
                    .sorted((student1, student2) -> student1.getFullFio().compareTo(student2.getFullFio()))
                    .collect(Collectors.toList());
            for (Student student : list)
            {
                final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, student.getId());
                final RtfDocument document = (RtfDocument) scriptResult.get("rtf");
                rtfDocument.addElement(document);
                if (list.indexOf(student) < list.size() - 1)
                    rtfDocument.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                else
                {
                    rtfDocument.setSettings(document.getSettings());
                    rtfDocument.setHeader(document.getHeader());
                }
            }
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("StudentCardData.rtf").document(rtfDocument), true);
        } else
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARDS_GROUP_LIST, Collections.singletonMap("groupId", (Object) getModel(component).getGroup().getId())));
    }


    @Override
    public void onClickPrintGroupStudentPersonalCardsList(final IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes.STUDENT_PERSONAL_CARD_PRINT_SCRIPT);
            final RtfDocument rtfDocument = new RtfDocument();
            rtfDocument.getElementList().clear();
            final List<Student> list = DataAccessServices.dao().getList(Student.class, Student.group(), getModel(component).getGroup())
                    .stream()
                    .sorted((student1, student2) -> student1.getFullFio().compareTo(student2.getFullFio()))
                    .collect(Collectors.toList());
            for (Student student : list)
            {
                final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, student.getId());
                final RtfDocument document = (RtfDocument) scriptResult.get("rtf");
                rtfDocument.addElement(document);
                if (list.indexOf(student) < list.size() - 1)
                    rtfDocument.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                else
                {
                    rtfDocument.setSettings(document.getSettings());
                    rtfDocument.setHeader(document.getHeader());
                }
            }
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("StudentCard.rtf").document(rtfDocument), true);
        } else
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARDS_GROUP_LIST, Collections.singletonMap("groupId", (Object) getModel(component).getGroup().getId())));
    }

}
