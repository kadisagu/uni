package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Связь компетенции со строкой УП
 */
public class UsmaCompetence2EpvRegistryRowRel extends UsmaCompetence2EpvRegistryRowRelGen
{
    public UsmaCompetence2EpvRegistryRowRel()
    {
    }

    public UsmaCompetence2EpvRegistryRowRel(EppEpvRegistryRow registryRow, UsmaCompetence competence)
    {
        setRegistryRow(registryRow);
        setUsmaCompetence(competence);
    }
}