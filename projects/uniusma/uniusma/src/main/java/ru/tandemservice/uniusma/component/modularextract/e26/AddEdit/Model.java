/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e26.AddEdit;

/**
 * @author Denis Perminov
 * @since 13.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e26.AddEdit.Model
{
    private boolean _preambulaVisible;

    public boolean isPreambulaVisible()
    {
        return _preambulaVisible;
    }

    public void setPreambulaVisible(boolean preambulaVisible)
    {
        _preambulaVisible = preambulaVisible;
    }
}
