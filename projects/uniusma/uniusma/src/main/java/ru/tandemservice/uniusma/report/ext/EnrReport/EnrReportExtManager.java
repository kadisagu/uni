/* $Id$ */
package ru.tandemservice.uniusma.report.ext.EnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.IEnrReportRatingListDao;
import ru.tandemservice.uniusma.report.ext.EnrReport.RatingListAdd.logic.UsmaEnrReportRatingListDao;

/**
 * @author Stanislav Shibarshin
 * @since 01.09.2016
 */
@Configuration
public class EnrReportExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IEnrReportRatingListDao ratingListDao()
    {
        return new UsmaEnrReportRatingListDao();
    }

}
