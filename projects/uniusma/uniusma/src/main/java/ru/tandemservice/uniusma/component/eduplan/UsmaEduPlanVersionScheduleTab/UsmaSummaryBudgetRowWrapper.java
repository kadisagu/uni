/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionScheduleTab;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 17.10.2013
 */
public class UsmaSummaryBudgetRowWrapper extends IdentifiableWrapper<IEntity>
{
    public static final String TITLE = "title";

    private static final String DISPERSED_PREFIX = "dispersed_";

    public static final String THEORY = EppWeekTypeCodes.THEORY;
    public static final String EXAM = EppWeekTypeCodes.EXAMINATION_SESSION;
    public static final String TEACH = EppWeekTypeCodes.TEACH_PRACTICE;
    public static final String TEACH_DISPERSED = DISPERSED_PREFIX + EppWeekTypeCodes.TEACH_PRACTICE;
    public static final String NIR = EppWeekTypeCodes.RESEARCH_WORK;
    public static final String NIR_DISPERSED = DISPERSED_PREFIX + EppWeekTypeCodes.RESEARCH_WORK;
    public static final String WORK = EppWeekTypeCodes.WORK_PRACTICE;
    public static final String WORK_DISPERSED = DISPERSED_PREFIX + EppWeekTypeCodes.WORK_PRACTICE;
    public static final String DISSERTATION = EppWeekTypeCodes.PREPARE_QUALIFICATION_WORK;
    public static final String STATE_EXAM = EppWeekTypeCodes.STATE_EXAMINATION;
    public static final String HOLIDAYS = EppWeekTypeCodes.HOLIDAYS;
    public static final String TOTAL = "total";

    private static final Map<String, String> TITLE_MAP = new HashMap<>();
    static
    {
        TITLE_MAP.put(THEORY, "Теоретическое обучение");
        TITLE_MAP.put(EXAM, "Экзаменационные сессии");
        TITLE_MAP.put(TEACH, "Учебная практика (концентр.)");
        TITLE_MAP.put(TEACH_DISPERSED, "Учебная практика (рассред.)");
        TITLE_MAP.put(NIR, "Научно-исслед. работа (концентр.)");
        TITLE_MAP.put(NIR_DISPERSED, "Научно-исслед. работа (рассред.)");
        TITLE_MAP.put(WORK, "Производственная практика (концентр.)");
        TITLE_MAP.put(WORK_DISPERSED, "Производственная практика (рассред.)");
        TITLE_MAP.put(DISSERTATION, "Диссертация");
        TITLE_MAP.put(STATE_EXAM, "Гос. экзамены");
        TITLE_MAP.put(HOLIDAYS, "Каникулы");
        TITLE_MAP.put(TOTAL, "Итого");
    }

    private static final List<String> ORDER_LIST = Collections.unmodifiableList(Arrays.asList(
            THEORY,
            EXAM,
            TEACH,
            TEACH_DISPERSED,
            NIR,
            NIR_DISPERSED,
            WORK,
            WORK_DISPERSED,
            DISSERTATION,
            STATE_EXAM,
            HOLIDAYS,
            TOTAL
    ));

    public static final Set<String> CODES = UsmaSummaryBudgetRowWrapper.TITLE_MAP.keySet();

    public static final Comparator<UsmaSummaryBudgetRowWrapper> COMPARATOR = new Comparator<UsmaSummaryBudgetRowWrapper>()
    {
        @Override
        public int compare(UsmaSummaryBudgetRowWrapper o1, UsmaSummaryBudgetRowWrapper o2)
        {
            return ORDER_LIST.indexOf(o1._code) - ORDER_LIST.indexOf(o2._code);
        }
    };

    private String _code;


    public String getCode(){ return _code; }
    @Override public String getTitle(){ return TITLE_MAP.get(_code); }

    public UsmaSummaryBudgetRowWrapper(Long id, String code)
    {
        super(id, code);
        _code = code;
    }
}