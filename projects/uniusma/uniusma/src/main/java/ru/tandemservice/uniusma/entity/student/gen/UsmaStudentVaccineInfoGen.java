package ru.tandemservice.uniusma.entity.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccinationSign;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;
import ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сведения о вакцинации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaStudentVaccineInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo";
    public static final String ENTITY_NAME = "usmaStudentVaccineInfo";
    public static final int VERSION_HASH = 304813722;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_VACCINE_KIND = "vaccineKind";
    public static final String L_VACCINATION_SIGN = "vaccinationSign";
    public static final String P_DATE = "date";
    public static final String P_PART = "part";
    public static final String P_PERIODICAL = "periodical";

    private Student _student;     // Студент
    private UsmaVaccineKind _vaccineKind;     // Наименование вакцины
    private UsmaVaccinationSign _vaccinationSign;     // Признак вакцинации/ревакцинации
    private Date _date;     // Дата вакцинации
    private String _part;     // Серия вакцины
    private String _periodical;     // Периодичность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Наименование вакцины. Свойство не может быть null.
     */
    @NotNull
    public UsmaVaccineKind getVaccineKind()
    {
        return _vaccineKind;
    }

    /**
     * @param vaccineKind Наименование вакцины. Свойство не может быть null.
     */
    public void setVaccineKind(UsmaVaccineKind vaccineKind)
    {
        dirty(_vaccineKind, vaccineKind);
        _vaccineKind = vaccineKind;
    }

    /**
     * @return Признак вакцинации/ревакцинации.
     */
    public UsmaVaccinationSign getVaccinationSign()
    {
        return _vaccinationSign;
    }

    /**
     * @param vaccinationSign Признак вакцинации/ревакцинации.
     */
    public void setVaccinationSign(UsmaVaccinationSign vaccinationSign)
    {
        dirty(_vaccinationSign, vaccinationSign);
        _vaccinationSign = vaccinationSign;
    }

    /**
     * @return Дата вакцинации.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата вакцинации.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Серия вакцины.
     */
    @Length(max=255)
    public String getPart()
    {
        return _part;
    }

    /**
     * @param part Серия вакцины.
     */
    public void setPart(String part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Периодичность.
     */
    @Length(max=255)
    public String getPeriodical()
    {
        return _periodical;
    }

    /**
     * @param periodical Периодичность.
     */
    public void setPeriodical(String periodical)
    {
        dirty(_periodical, periodical);
        _periodical = periodical;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaStudentVaccineInfoGen)
        {
            setStudent(((UsmaStudentVaccineInfo)another).getStudent());
            setVaccineKind(((UsmaStudentVaccineInfo)another).getVaccineKind());
            setVaccinationSign(((UsmaStudentVaccineInfo)another).getVaccinationSign());
            setDate(((UsmaStudentVaccineInfo)another).getDate());
            setPart(((UsmaStudentVaccineInfo)another).getPart());
            setPeriodical(((UsmaStudentVaccineInfo)another).getPeriodical());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaStudentVaccineInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaStudentVaccineInfo.class;
        }

        public T newInstance()
        {
            return (T) new UsmaStudentVaccineInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "vaccineKind":
                    return obj.getVaccineKind();
                case "vaccinationSign":
                    return obj.getVaccinationSign();
                case "date":
                    return obj.getDate();
                case "part":
                    return obj.getPart();
                case "periodical":
                    return obj.getPeriodical();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "vaccineKind":
                    obj.setVaccineKind((UsmaVaccineKind) value);
                    return;
                case "vaccinationSign":
                    obj.setVaccinationSign((UsmaVaccinationSign) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "part":
                    obj.setPart((String) value);
                    return;
                case "periodical":
                    obj.setPeriodical((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "vaccineKind":
                        return true;
                case "vaccinationSign":
                        return true;
                case "date":
                        return true;
                case "part":
                        return true;
                case "periodical":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "vaccineKind":
                    return true;
                case "vaccinationSign":
                    return true;
                case "date":
                    return true;
                case "part":
                    return true;
                case "periodical":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "vaccineKind":
                    return UsmaVaccineKind.class;
                case "vaccinationSign":
                    return UsmaVaccinationSign.class;
                case "date":
                    return Date.class;
                case "part":
                    return String.class;
                case "periodical":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaStudentVaccineInfo> _dslPath = new Path<UsmaStudentVaccineInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaStudentVaccineInfo");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Наименование вакцины. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getVaccineKind()
     */
    public static UsmaVaccineKind.Path<UsmaVaccineKind> vaccineKind()
    {
        return _dslPath.vaccineKind();
    }

    /**
     * @return Признак вакцинации/ревакцинации.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getVaccinationSign()
     */
    public static UsmaVaccinationSign.Path<UsmaVaccinationSign> vaccinationSign()
    {
        return _dslPath.vaccinationSign();
    }

    /**
     * @return Дата вакцинации.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Серия вакцины.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getPart()
     */
    public static PropertyPath<String> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Периодичность.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getPeriodical()
     */
    public static PropertyPath<String> periodical()
    {
        return _dslPath.periodical();
    }

    public static class Path<E extends UsmaStudentVaccineInfo> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private UsmaVaccineKind.Path<UsmaVaccineKind> _vaccineKind;
        private UsmaVaccinationSign.Path<UsmaVaccinationSign> _vaccinationSign;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _part;
        private PropertyPath<String> _periodical;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Наименование вакцины. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getVaccineKind()
     */
        public UsmaVaccineKind.Path<UsmaVaccineKind> vaccineKind()
        {
            if(_vaccineKind == null )
                _vaccineKind = new UsmaVaccineKind.Path<UsmaVaccineKind>(L_VACCINE_KIND, this);
            return _vaccineKind;
        }

    /**
     * @return Признак вакцинации/ревакцинации.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getVaccinationSign()
     */
        public UsmaVaccinationSign.Path<UsmaVaccinationSign> vaccinationSign()
        {
            if(_vaccinationSign == null )
                _vaccinationSign = new UsmaVaccinationSign.Path<UsmaVaccinationSign>(L_VACCINATION_SIGN, this);
            return _vaccinationSign;
        }

    /**
     * @return Дата вакцинации.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UsmaStudentVaccineInfoGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Серия вакцины.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getPart()
     */
        public PropertyPath<String> part()
        {
            if(_part == null )
                _part = new PropertyPath<String>(UsmaStudentVaccineInfoGen.P_PART, this);
            return _part;
        }

    /**
     * @return Периодичность.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo#getPeriodical()
     */
        public PropertyPath<String> periodical()
        {
            if(_periodical == null )
                _periodical = new PropertyPath<String>(UsmaStudentVaccineInfoGen.P_PERIODICAL, this);
            return _periodical;
        }

        public Class getEntityClass()
        {
            return UsmaStudentVaccineInfo.class;
        }

        public String getEntityName()
        {
            return "usmaStudentVaccineInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
