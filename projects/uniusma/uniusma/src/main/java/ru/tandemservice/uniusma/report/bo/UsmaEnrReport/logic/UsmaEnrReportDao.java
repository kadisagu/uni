/* $Id:$ */
package ru.tandemservice.uniusma.report.bo.UsmaEnrReport.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.uniusma.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uniusma.report.bo.UsmaEnrReport.ui.EntrantStudentCardAdd.UsmaEnrReportEntrantStudentCardAddUI;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 27.08.2014
 */
public class UsmaEnrReportDao  extends UniBaseDao implements IUsmaEnrReportDao
{
    // ---------- begin of EntrantStudentCard --------------------------------------------------------------------------
    @Override
    public Long createReportEntrantStudentCard(UsmaEnrReportEntrantStudentCardAddUI model)
    {
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        // нам нужны только из выбранной приемной компании и зарегистрировавшиеся в указанные даты
        EnrEntrantDQLSelectBuilder builder = new EnrEntrantDQLSelectBuilder(false, false)
                .filter(model.getDateFrom(), model.getDateTo())
                .filter(model.getEnrollmentCampaign());
        // удовлетворяющие условиям дополнительных фильтров
        filterAddon.applyFilters(builder, builder.competition());
        // желающие обучаться параллельно или не очень
        if (model.isParallelActive())
        {
            if (1L == model.getParallel().getId())
            {
                builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(builder.reqComp())), value(Boolean.TRUE)));
            }
            else if (0L == model.getParallel().getId())
            {
                builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(builder.reqComp())), value(Boolean.FALSE)));
            }
        }
        // и уже зачисленные
        builder.where(eq(property(EnrRequestedCompetition.state().code().fromAlias(builder.reqComp())), value(EnrEntrantStateCodes.ENROLLED)));
        builder.where(exists(EnrEnrollmentExtract.class,
                EnrEnrollmentExtract.entity().s(), property(builder.reqComp()),
                EnrEnrollmentExtract.state().code().s(), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)));

        // и отсортированные по ФИО абитуриенты
        builder.joinPath(DQLJoinType.inner, EnrEntrant.person().identityCard().fromAlias(builder.entrant()), "card", true)
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME));

        builder.column("reqComp.id");

        Iterator<Long> iterator = createStatement(builder).<Long>list().iterator();
        if (!iterator.hasNext())
            throw new ApplicationException("Данные для построения отчета отсутствуют.");

        // дабы не плодить дабл-карточки
        Long reqCompId = iterator.next();
        EnrEntrant entrant = get(EnrRequestedCompetition.class, reqCompId).getRequest().getEntrant();

        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_STUDENT_CARD);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, reqCompId);
        // groovy-скрипт должен возвращать не byte[], а RtfDocument, ибо rtf-движок не может разобрать то, что он уже однажды разобрал!
        RtfDocument mainDoc = (RtfDocument) scriptResult.get(IScriptExecutor.DOCUMENT);
        List<IRtfElement> mainElementList = mainDoc.getElementList();

        while (iterator.hasNext())
        {
            Long nextReqCompId = iterator.next();
            EnrEntrant nextEntrant = get(EnrRequestedCompetition.class, nextReqCompId).getRequest().getEntrant();
            if (!nextEntrant.equals(entrant))
            {
                insertPageBreak(mainElementList);
                scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, nextReqCompId);
                RtfDocument nextDoc = (RtfDocument) scriptResult.get(IScriptExecutor.DOCUMENT);
                mainElementList.addAll(nextDoc.getElementList());
            }
            entrant = nextEntrant;
        }
        model.setReport(mainDoc);
        return 1L;
    }

    private void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }

    // ---------- end of EntrantStudentCard ----------------------------------------------------------------------------
}
