/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e13;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendChildStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.WeekendChildStuExtractUsmaExtGen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 22.05.2014
 */
public class WeekendChildStuExtractPrint implements IPrintFormCreator<WeekendChildStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();
        short i = 1;

        modifier.put("dateStart", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("dateFinish", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));

        WeekendChildStuExtractUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new WeekendChildStuExtractUsmaExtGen.NaturalId(extract));
        if (null != extractExt && extractExt.isFreeAttendance())
        {
            modifier.put("freeAttendanceStr", String.valueOf(++i) + ". Разрешить " + modifier.getStringValue("student_D") +
                    " " + modifier.getStringValue("fio_D") + " свободное посещение занятий.");
        }
        else
            tagsToDel.add("freeAttendanceStr");

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}
