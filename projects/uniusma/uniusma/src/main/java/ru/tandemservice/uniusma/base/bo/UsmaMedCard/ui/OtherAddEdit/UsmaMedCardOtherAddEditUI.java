/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.OtherAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo;

import static org.tandemframework.hibsupport.dao.CommonDAO.proxy;

/**
 * @author DMITRY KNYAZEV
 * @since 06.08.2014
 */
@State({
		       @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentId", required = true),
		       @Bind(key = "entityId", binding = "entityId")
       })
public class UsmaMedCardOtherAddEditUI extends UIPresenter
{
	private Long _entityId;
	private Long _studentId;
	private UsmaStudentOtherInfo _entity;
	private DataWrapper _dispensary;

	@Override
	public void onComponentRefresh()
	{
		if(getEntity() == null)
		{
			if (isEditForm())
			{
				setEntity(DataAccessServices.dao().<UsmaStudentOtherInfo>getNotNull(getEntityId()));
				setDispensary(getWrapperFromBoolean(getEntity().isDispensary()));
			}
			else
			{
				Student student = proxy(getStudentId());
				UsmaStudentOtherInfo entity = new UsmaStudentOtherInfo();
				entity.setStudent(student);
				setEntity(entity);
			}
		}
	}
	//Utils
	private boolean getBooleanFromWrapper (DataWrapper o)
	{
		return TwinComboDataSourceHandler.getSelectedValueNotNull(o);
	}

	private DataWrapper getWrapperFromBoolean(boolean o)
	{
		if(o)
			return TwinComboDataSourceHandler.getYesOption();
		else
			return TwinComboDataSourceHandler.getNoOption();
	}

	//Handlers
	public void onClickSave()
	{
		ErrorCollector errorCollector = ContextLocal.getErrorCollector();
		if(!getBooleanFromWrapper(getDispensary()) && getEntity().getHealthGroup() == null)
			errorCollector.add("Если Вы хотите сохранить в поле \"Диспансерный учет\" занчение \"нет\", выбирете группу здоровья","healthGroupList");
		if (errorCollector.hasErrors())
			return;

		getEntity().setDispensary(getBooleanFromWrapper(getDispensary()));
		DataAccessServices.dao().saveOrUpdate(getEntity());
		deactivate();
	}

	public void onClickCancel()
	{
		deactivate();
	}

	//Getters/Setters
	public boolean isEditForm()
	{
		return _entityId != null;
	}

	public Long getEntityId()
	{
		return _entityId;
	}

	public void setEntityId(Long entityId)
	{
		_entityId = entityId;
	}

	public Long getStudentId()
	{
		return _studentId;
	}

	public void setStudentId(Long studentId)
	{
		_studentId = studentId;
	}

	public UsmaStudentOtherInfo getEntity()
	{
		return _entity;
	}

	public void setEntity(UsmaStudentOtherInfo entity)
	{
		_entity = entity;
	}

	public DataWrapper getDispensary()
	{
		return _dispensary;
	}

	public void setDispensary(DataWrapper dispensary)
	{
		_dispensary = dispensary;
	}
}
