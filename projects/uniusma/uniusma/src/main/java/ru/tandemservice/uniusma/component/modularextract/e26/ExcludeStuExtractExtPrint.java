/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.modularextract.e26;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeStuExtractExt;

/**
 * @author dseleznev
 * Created on: 23.03.2010
 */
public class ExcludeStuExtractExtPrint implements IPrintFormCreator<ExcludeStuExtractExt>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeStuExtractExt extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Person person = extract.getEntity().getPerson();

        boolean isMale = person.isMale();
        short i = 1;

        modifier.put("preambula", (extract.isPrintPreambula() ?
                (modifier.getStringValue("Student") + " " + modifier.getStringValue("fio") +
                 " обучается на " + modifier.getStringValue("course") + " курсе " +
                 modifier.getStringValue("orgUnit_G") + " " +
                 modifier.getStringValue("developForm_GF") + " формы обучения " +
                 modifier.getStringValue("compensationTypeStr_G_Alt") + ".") : ""));
        modifier.put("textCause", extract.getReasonStr());
        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDate()));
        modifier.put("proved", "подтвердил" + (isMale ? "" : "а"));
        modifier.put("notPassed_A", "не прошедш" + (isMale ? "его" : "ую"));
        modifier.put("isStudying", "обучающе" + (isMale ? "гося" : "йся"));
        modifier.put("wasStudying", "обучавше" + (isMale ? "гося" : "йся"));
        modifier.put("stopPayments", (extract.isStopGrantsPaying() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                        " отменить выплату академической стипендии с " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingDate()) + " г." ) : ""));
        modifier.put("giveDiploma", (extract.isGiveDiploma() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                        " выдать диплом о незаконченном высшем образовании.") : ""));

        boolean budget = extract.getEntity().getCompensationType().isBudget();
        modifier.put("compensationTypeStr_G", budget ? "бюджетной основы" : "внебюджетной основы");

        modifier.modify(document);
        return document;
    }
}