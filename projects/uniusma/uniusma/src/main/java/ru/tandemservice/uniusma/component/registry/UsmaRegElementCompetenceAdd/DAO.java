/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.registry.UsmaRegElementCompetenceAdd;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

import java.util.Set;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setCompetenceModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(UsmaCompetence2RegistryElementRel.class, "rel")
                        .column(DQLExpressions.property(UsmaCompetence2RegistryElementRel.usmaCompetence().id().fromAlias("rel")))
                        .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2RegistryElementRel.registryElement().id().fromAlias("rel")), DQLExpressions.value(model.getId())));

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(UsmaCompetence.class, "fc")
                        .where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(UsmaCompetence.title().fromAlias("fc"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))))
                        .where(DQLExpressions.notIn(DQLExpressions.property(UsmaCompetence.id().fromAlias("fc")), subBuilder.buildQuery()));

                if (set != null)
                {
                    builder.where(DQLExpressions.in(DQLExpressions.property(UsmaCompetence.id().fromAlias("fc")), set));
                }

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                UsmaCompetence competence = (UsmaCompetence) value;

                return competence.getEppSkillGroup().getShortTitle() + ". " + competence.getTitle();
            }
        });
    }

    @Override
    public void update(Model model)
    {
        EppRegistryElement registryElement = get(model.getId());

        for (UsmaCompetence competence : model.getPickedCompetences())
        {
            save(new UsmaCompetence2RegistryElementRel(registryElement, competence));
        }

        super.update(model);
    }
}