/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ImtsaIdReImport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Alexander Zhebko
 * @since 18.11.2013
 */
@Configuration
public class UsmaEduPlanImtsaIdReImport extends BusinessComponentManager
{
}