/* $Id$ */
package ru.tandemservice.uniusma.component.listextract.e4;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.e4.ExcludeStuListExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.uniusma.utils.UsmaReportUtils;

/**
 * @author Stanislav Shibarshin
 * @since 01.09.2016
 */
public class UsmaExcludeStuListExtractPrint extends ExcludeStuListExtractPrint
{

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ExcludeStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = super.createParagraphInjectModifier(paragraph, firstExtract);
        UsmaReportUtils.removeEduOrgUnitShortFormExtract(injectModifier);
        return injectModifier;
    }

}