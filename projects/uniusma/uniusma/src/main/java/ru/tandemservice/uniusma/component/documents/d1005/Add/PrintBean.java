/* $Id$ */
package ru.tandemservice.uniusma.component.documents.d1005.Add;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.fias.base.bo.util.AddressRuTitleBuilder;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.YearDistributionPartCodes;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;


import java.time.Month;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Model model)
    {
        RtfDocument document = super.createPrintForm(template, model);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("branchShortTitle"), true, false);
        return document;
    }

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();

        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        EducationLevels educationLevel = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();

        injectModifier.put("orgUnitTitle", formativeOrgUnit.getPrintTitle());
        injectModifier.put("orgUnitTitle_P", formativeOrgUnit.getPrepositionalCaseTitle());
        fillAcademy(injectModifier);
        injectModifier.put("address", getAddress(formativeOrgUnit));

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(model.getFormingDate());
        injectModifier.put("day", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)))
                .put("month", Month.of(calendar.get(Calendar.MONTH)).getDisplayName(TextStyle.FULL, new Locale("ru")))
                .put("year", String.valueOf(calendar.get(Calendar.YEAR)))
                .put("number", String.valueOf(model.getNumber()));

        injectModifier.put("FIO", model.getStudentTitleStr());
        injectModifier.put("student_D", student.getPerson().isMale() ? "Студенту" : "Студентке");
        injectModifier.put("course", model.getCourse().getTitle());
        injectModifier.put("eduSpec", educationLevel.getLevelType().getDativeCaseShortTitle() + " " + educationLevel.getProgramSubjectTitleWithCode());
        injectModifier.put("sem", getSemester(model.getTerm()));
        injectModifier.put("eduYear", EducationYear.getCurrentRequired().getTitle());
        injectModifier.put("dateN", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateStartCertification()));
        injectModifier.put("dateF", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateEndCertification()));

        injectModifier.put("managerPostTitle", model.getManagerPostTitle());
        injectModifier.put("managerFio", model.getManagerFio());

        return injectModifier;
    }

    private RtfInjectModifier fillAcademy(RtfInjectModifier injectModifier)
    {
        AcademyData academyData = AcademyData.getInstance();

        injectModifier.put("certSer", academyData.getCertificateSeria() == null ? "-" : academyData.getCertificateSeria());
        injectModifier.put("certNum", academyData.getCertificateNumber() == null ? "-" : academyData.getCertificateNumber());
        injectModifier.put("certregNum", academyData.getCertificateRegNumber() == null ? "-" : academyData.getCertificateRegNumber());
        injectModifier.put("certDate", academyData.getCertificateDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()));
        injectModifier.put("certAg", academyData.getCertificationAgency() == null ? "-" : academyData.getCertificationAgency().replace("Федеральная", "Федеральной").replace("служба", "службой"));
        injectModifier.put("licSer", academyData.getLicenceSeria() == null ? "-" : academyData.getLicenceSeria());
        injectModifier.put("licNum ", academyData.getLicenceNumber() == null ? "-" : academyData.getLicenceNumber());
        injectModifier.put("licregNum", academyData.getLicenceRegNumber() == null ? "-" : academyData.getLicenceRegNumber());
        injectModifier.put("licDate", academyData.getLicenceDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()));
        injectModifier.put("licAg", academyData.getLicensingAgency() == null ? "-" : academyData.getLicensingAgency());

        return injectModifier;
    }

    private String getSemester(DevelopGridTerm term)
    {
        if (term == null) return "";

        String sem;
        switch (term.getPart().getCode())
        {
            case YearDistributionPartCodes.WINTER_SEMESTER:
                sem = "зимнего";
                break;
            case YearDistributionPartCodes.SPRING_TERM:
                sem = "летнего";
                break;
            default:
                sem = "";
        }
        return sem;
    }

    private String getAddress(OrgUnit orgUnit)
    {
        if (orgUnit == null)
            return null;

        AddressDetailed address = orgUnit.getAddress();
        if (address == null)
            return getAddress(orgUnit.getParent());

        return AddressRuTitleBuilder.getTitleWithFlat((AddressRu) address);
    }
}
