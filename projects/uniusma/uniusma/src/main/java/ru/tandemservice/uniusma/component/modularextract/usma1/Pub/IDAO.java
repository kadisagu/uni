/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public interface IDAO extends IModularStudentExtractPubDAO<UsmaExcludeAfterAcadWeekendStuExtract, Model>
{
}
