/*$Id$*/
package ru.tandemservice.uniusma.component.student.StudentPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uniusma.entity.catalog.codes.UniScriptItemCodes;

/**
 * @author DMITRY KNYAZEV
 * @since 09.02.2016
 */
public class Controller extends ru.tandemservice.uni.component.student.StudentPub.Controller
{
    public void onClickPrintStudentPersonalCardTitle(IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_TITLE_PRINT_SCRIPT);
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, getModel(component).getStudent().getId());
        } else
        {
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARD, new ParametersMap()
                    .add("studentId", getModel(component).getStudent().getId())
            ));
        }
    }

    public void onClickPrintStudentPersonalCardData(IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_DATA_PRINT_SCRIPT);
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, getModel(component).getStudent().getId());
        } else
        {
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARD, new ParametersMap()
                    .add("studentId", getModel(component).getStudent().getId())
            ));
        }
    }
}
