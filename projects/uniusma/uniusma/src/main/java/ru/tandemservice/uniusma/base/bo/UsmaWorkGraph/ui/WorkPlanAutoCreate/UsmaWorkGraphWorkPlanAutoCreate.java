/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.WorkPlanAutoCreate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 14.11.2013
 */
@Configuration
public class UsmaWorkGraphWorkPlanAutoCreate extends BusinessComponentManager
{
    public static final String PUPNAG_DS = "pupnagDS";
    public static final String WORK_GRAPH_DS = "workGraphDS";

    public static final String PUPNAG = "pupnag";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
                .addDataSource(selectDS(WORK_GRAPH_DS, workGraphDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler pupnagDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
                .order(EppYearEducationProcess.educationYear().intValue());
    }

    @Bean
    public IDefaultComboDataSourceHandler workGraphDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UsmaWorkGraph.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                IDQLExpression title = DQLFunctions.concat(
                        DQLExpressions.property(alias, UsmaWorkGraph.developForm().shortTitle()),
                        DQLExpressions.property(alias, UsmaWorkGraph.developCondition().shortTitle()),
                        DQLFunctions.coalesce(DQLExpressions.property(alias, UsmaWorkGraph.developTech().shortTitle()), DQLExpressions.value("")),
                        DQLExpressions.property(alias, UsmaWorkGraph.developGrid().title()));

                return super.query(alias, filter).where(DQLExpressions.likeUpper(title, DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(DQLExpressions.eq(DQLExpressions.property(alias, UsmaWorkGraph.year().id()), DQLExpressions.commonValue(context.get(UsmaWorkGraphWorkPlanAutoCreate.PUPNAG))));
                super.applyWhereConditions(alias, dql, context);
            }
        };
    }
}