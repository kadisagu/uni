/**
 * $Id$
 */
package ru.tandemservice.uniusma.dao.eppEduPlan;

import com.google.common.collect.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppEpvRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniusma.UniusmaDefines;
import ru.tandemservice.uniusma.entity.*;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;
import ru.tandemservice.uniusma.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEppRegistryModuleALoadExt;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEppRegistryModuleELoad;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvRowTermLoad;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.*;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.*;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.parser.ImtsaParser;

import javax.annotation.Nullable;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 19.07.2013
 */

public class ImtsaImportDAO extends UniBaseDao implements IImtsaImportDAO
{
    public void saveImtsa(EppEduPlanVersionBlock block, ImtsaFile imtsa, Map<String, EppPlanStructure> cyclesMap, Map<String, EppPlanStructure> partsMap, List<String> errors)
    {
        new DQLDeleteBuilder(UsmaCompetence2EduPlanVersionBlockRel.class).where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block()), value(block))).createStatement(getSession()).execute();

        EppEduPlanVersionBlock rootBlock = block;
        EppEduPlanVersion eduPlanVersion = block.getEduPlanVersion();
        if (!block.isRootBlock())
        {
            rootBlock = this.get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), eduPlanVersion);
        }

        Map<Integer, Integer> courseParts = new HashMap<>();
        DevelopGrid developGrid = eduPlanVersion.getEduPlan().getDevelopGrid();
        DQLSelectBuilder coursePartsBuilder = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .where(eq(property("dgt", DevelopGridTerm.developGrid()), value(developGrid)))
        /*1*/   .column(property("dgt", DevelopGridTerm.course().intValue()), "c")
        /*2*/   .column(DQLFunctions.cast(DQLFunctions.count(property("dgt", DevelopGridTerm.id())), PropertyType.INTEGER))
                .group(property("dgt", DevelopGridTerm.course().intValue()));

        for (Object[] row : coursePartsBuilder.createStatement(getSession()).<Object[]>list())
        {
            courseParts.put((Integer) row[0], (Integer) row[1]);
        }

        Map<Integer, Integer> coursePartsShiftMap = new HashMap<>();

        for (Map.Entry<Integer, Integer> courseEntry : imtsa.getMaxCoursePartNumberMap().entrySet())
        {
            Integer courseNumber = courseEntry.getKey();
            Integer coursePartMaxNumber = courseEntry.getValue();
            Integer gridMaxCoursePartNumber = courseParts.get(courseNumber);
            if (gridMaxCoursePartNumber != null && coursePartMaxNumber > gridMaxCoursePartNumber)
            {
                if (coursePartMaxNumber == 3 && gridMaxCoursePartNumber == 2 && imtsa.getMinCoursePartNumberMap().get(courseNumber) != 1)
                {
                    coursePartsShiftMap.put(courseNumber, -1);

                }
                else if (coursePartMaxNumber == 3 && gridMaxCoursePartNumber == 1 && imtsa.getMinCoursePartNumberMap().get(courseNumber) == 3)
                {
                    coursePartsShiftMap.put(courseNumber, -2);

                }
                else if (gridMaxCoursePartNumber != 1)
                {
                    throw new ApplicationException("Неподходящая учебная сетка.");
                }
            }
        }

        Map<Integer, Map<Integer, Term>> developGridTermMap = new HashMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), developGrid))
        {
            SafeMap.safeGet(developGridTermMap, developGridTerm.getCourse().getIntValue(), HashMap.class)
                    .put(developGridTerm.getPart().getNumber(), developGridTerm.getTerm());
        }


        Model model = new Model(cyclesMap, partsMap, coursePartsShiftMap, developGridTermMap, rootBlock, errors);
        saveTitle(block, imtsa.getTitle(), model);
        saveCompetences(imtsa.getCompetences(), block, model);
        savePlanRows(block, imtsa.getRootPlanRow(), model);
        savePractices(block, imtsa.getPracticeMap(), model);

        /*todo временно, чтобы не падали гос2*/
        if (block.getEduPlanVersion().getEduPlan().getGeneration().getNumber() != 2)
        {
            OrgUnit orgUnit;

            // Учебные планы ВПО
            EppEduPlanHigherProf eduPlan = ((EppEduPlanHigherProf) eduPlanVersion.getEduPlan());
            if (block instanceof EppEduPlanVersionSpecializationBlock)
            {
                EppEduPlanVersionSpecializationBlock specializationBlock = (EppEduPlanVersionSpecializationBlock) block;
                Collection<EducationLevelsHighSchool> eduLevelsHS = new HashSet<>(new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, "lhs")
                        .column(property("lhs"))
                        .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().eduProgramSpecialization().fromAlias("lhs"), "sp")
                        .where(eq(property("sp"), value(specializationBlock.getProgramSpecialization())))
                        .where(eq(property("lhs", EducationLevelsHighSchool.assignedQualification()), value(eduPlan.getProgramQualification())))
                        .where(eq(property("lhs", EducationLevelsHighSchool.programOrientation()), value(eduPlan.getProgramOrientation())))
                        .createStatement(this.getSession()).<EducationLevelsHighSchool>list());

                if (eduLevelsHS.size() == 1)
                {
                    // одно НПв, берем подразделение из него
                    orgUnit = eduLevelsHS.iterator().next().getOrgUnit();

                }
                else
                {
                    // НПв нет, либо больше одного - берем выпускающее подр. блока общей направленности
                    List<EppEduPlanVersionSpecializationBlock> specBlockList = this.getList(EppEduPlanVersionSpecializationBlock.class, EppEduPlanVersionSpecializationBlock.eduPlanVersion(), eduPlanVersion);
                    if (specBlockList.isEmpty())
                    {
                        // не задан блок общей направленности - указываем ОУ
                        orgUnit = TopOrgUnit.getInstance();

                    }
                    else
                    {
                        orgUnit = specBlockList.get(0).getOwnerOrgUnit().getOrgUnit();
                    }
                }

            }
            else // block instanceof EppEduPlanVersionRootBlock
            {
                // ищем общую направленность
                Collection<EducationLevelsHighSchool> eduLevelsHS = new HashSet<>(new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, "lhs")
                        .column(property("lhs"))
                        .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().eduProgramSpecialization().fromAlias("lhs"), "sp")
                        .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                        .joinPath(DQLJoinType.inner, EppEduPlanVersionSpecializationBlock.programSpecialization().fromAlias("b"), "cs")
                        .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion()), value(eduPlanVersion)))
                        .where(instanceOf("cs", EduProgramSpecializationRoot.class))
                        .where(eq(property("sp"), property("cs")))
                        .where(eq(property("lhs", EducationLevelsHighSchool.assignedQualification()), value(eduPlan.getProgramQualification())))
                        .where(eq(property("lhs", EducationLevelsHighSchool.programOrientation()), value(eduPlan.getProgramOrientation())))
                        .createStatement(this.getSession()).<EducationLevelsHighSchool>list());

                if (eduLevelsHS.size() == 1)
                {
                    // одно НПв, берем подразделение из него
                    orgUnit = eduLevelsHS.iterator().next().getOrgUnit();

                }
                else
                {
                    // НПв нет, либо больше одного - берем выпускающее подр. блока общей направленности
                    List<EppEduPlanVersionSpecializationBlock> specBlockList = this.getList(EppEduPlanVersionSpecializationBlock.class, EppEduPlanVersionSpecializationBlock.eduPlanVersion(), eduPlanVersion);
                    if (specBlockList.isEmpty())
                    {
                        // не задан блок общей направленности - указываем ОУ
                        orgUnit = TopOrgUnit.getInstance();

                    }
                    else
                    {
                        orgUnit = specBlockList.get(0).getOwnerOrgUnit().getOrgUnit();
                    }
                }
            }

            boolean dissertationAsAttestation = true;
            boolean stateExamAsAsAttestation = true;
            if (imtsa.getTitle().getAttributes().get(ImtsaTitle.getDissertationAsAttestationAttribute()) != null)
                dissertationAsAttestation = (Boolean) imtsa.getTitle().getAttributes().get(ImtsaTitle.getDissertationAsAttestationAttribute());

            if (imtsa.getTitle().getAttributes().get(ImtsaTitle.getStateExamAsAttestationAttribute()) != null)
                stateExamAsAsAttestation = (Boolean) imtsa.getTitle().getAttributes().get(ImtsaTitle.getStateExamAsAttestationAttribute());

            EppRegistryStructure attestation = getCatalogItem(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_ATTESTATION);
            EppRegistryStructure nir = getCatalogItem(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR);

            Map<Integer, PairKey<Double, Double>> totalDissertationLabor = imtsa.getDissertationLaborMap().remove(0);

            EppEpvRegistryRow dissertation = new EppEpvRegistryRow();
            dissertation.setExcludedFromActions(true);
            dissertation.setExcludedFromLoad(true);
            dissertation.setRegistryElementType(dissertationAsAttestation ? attestation : nir);
            dissertation.setNumber(String.valueOf(model.nextNumber(1L)));
            dissertation.setTitle(ImtsaFile.getDissertationTitle());
            dissertation.setOwner(block);
            dissertation.setTotalLaborAsDouble(totalDissertationLabor.get(0).getFirst());
            dissertation.setHoursTotalAsDouble(totalDissertationLabor.get(0).getSecond());
            dissertation.setRegistryElementOwner(orgUnit);
            dissertation.setStoredIndex("01");
            dissertation.setUserIndex("01");

            save(dissertation);


            for (Map.Entry<Integer, Map<Integer, PairKey<Double, Double>>> courseEntry : imtsa.getDissertationLaborMap().entrySet())
            {
                Integer courseNumber = courseEntry.getKey();
                if (!developGridTermMap.containsKey(courseNumber))
                {
                    errors.add("Предупреждение загрузки плана. Неподходящая учебная сетка: " + courseNumber + " курс. Нагрузка ВКР пропущена.");
                    continue;
                }
                for (Map.Entry<Integer, PairKey<Double, Double>> termEntry : courseEntry.getValue().entrySet())
                {
                    Double labor = termEntry.getValue().getFirst();
                    Double load = termEntry.getValue().getSecond();

                    if (labor == 0.0d && load == 0.0d)
                    {
                        continue;
                    }
                    Term term = developGridTermMap.get(courseNumber).get(termEntry.getKey());
                    if (term == null)
                    {
                        model.errors.add("Предупреждение загрузки плана. Неподходящая учебная сетка: " + termEntry.getKey() + " семестр. Нагрузка ВКР пропущена.");
                        continue;
                    }
                    EppEpvRowTerm rowTerm = new EppEpvRowTerm(dissertation, term);
                    rowTerm.setLaborAsDouble(labor);
                    rowTerm.setHoursTotalAsDouble(load);

                    save(rowTerm);
                }
            }

            if (dissertationAsAttestation && imtsa.getAttestationCompetenceCodes() != null)
            {
                for (String code : imtsa.getAttestationCompetenceCodes())
                {
                    String index;
                    UsmaCompetence competence;
                    if ((index = model.codeIndexCompetenceMap.get(code)) == null || (competence = model.indexCompetenceMap.get(index)) == null)
                    {
                        errors.add("Неизвестная компетенция " + code + ".");
                        continue;
                    }

                    UsmaCompetence2EpvRegistryRowRel rel = new UsmaCompetence2EpvRegistryRowRel(dissertation, competence);
                    save(rel);
                }
            }

            Map<Integer, PairKey<Double, Double>> totalStateExamLabor = imtsa.getStateExamLaborMap().remove(0);

            EppEpvRegistryRow stateExam = new EppEpvRegistryRow();
            stateExam.setExcludedFromActions(true);
            stateExam.setExcludedFromLoad(true);
            stateExam.setRegistryElementType(stateExamAsAsAttestation ? attestation : nir);
            stateExam.setNumber(String.valueOf(model.nextNumber(1L)));
            stateExam.setTitle(ImtsaFile.getStateExamTitle());
            stateExam.setOwner(block);
            stateExam.setTotalLaborAsDouble(totalStateExamLabor.get(0).getFirst());
            stateExam.setHoursTotalAsDouble(totalStateExamLabor.get(0).getSecond());
            stateExam.setRegistryElementOwner(orgUnit);
            stateExam.setStoredIndex("02");
            stateExam.setUserIndex("02");

            save(stateExam);

            for (Map.Entry<Integer, Map<Integer, PairKey<Double, Double>>> courseEntry : imtsa.getStateExamLaborMap().entrySet())
            {
                Integer courseNumber = courseEntry.getKey();
                if (!developGridTermMap.containsKey(courseNumber))
                {
                    errors.add("Предупреждение загрузки плана. Неподходящая учебная сетка: " + courseNumber + " курс. Нагрузка ГЭ пропущена.");
                    continue;
                }
                for (Map.Entry<Integer, PairKey<Double, Double>> termEntry : courseEntry.getValue().entrySet())
                {
                    Double labor = termEntry.getValue().getFirst();
                    Double load = termEntry.getValue().getSecond();

                    if (labor == 0.0d && load == 0.0d)
                    {
                        continue;
                    }
                    Term term = developGridTermMap.get(courseNumber).get(termEntry.getKey());
                    if (term == null)
                    {
                        model.errors.add("Предупреждение загрузки плана. Неподходящая учебная сетка: " + termEntry.getKey() + " семестр. Нагрузка ГЭ пропущена.");
                    }
                    EppEpvRowTerm rowTerm = new EppEpvRowTerm(stateExam, term);
                    rowTerm.setLaborAsDouble(labor);
                    rowTerm.setHoursTotalAsDouble(load);

                    save(rowTerm);
                }
            }

            if (stateExamAsAsAttestation && imtsa.getAttestationCompetenceCodes() != null)
            {
                for (String code : imtsa.getAttestationCompetenceCodes())
                {
                    String index;
                    UsmaCompetence competence;
                    if ((index = model.codeIndexCompetenceMap.get(code)) == null || (competence = model.indexCompetenceMap.get(index)) == null)
                    {
                        errors.add("Неизвестная компетенция " + code + ".");
                        continue;
                    }

                    UsmaCompetence2EpvRegistryRowRel rel = new UsmaCompetence2EpvRegistryRowRel(stateExam, competence);
                    save(rel);
                }
            }
        }
    }

    private void saveTitle(EppEduPlanVersionBlock block, ImtsaTitle imtsaTitle, Model model)
    {
        new DQLDeleteBuilder(UsmaEpvBlockTitle.class).where(eq(property(UsmaEpvBlockTitle.block()), value(block))).createStatement(getSession()).execute();
        new DQLDeleteBuilder(UsmaDeveloper.class).where(eq(property(UsmaDeveloper.block()), value(block))).createStatement(getSession()).execute();
        new DQLDeleteBuilder(UsmaQualification.class).where(eq(property(UsmaQualification.block()), value(block))).createStatement(getSession()).execute();
        new DQLDeleteBuilder(UsmaSpeciality.class).where(eq(property(UsmaSpeciality.block()), value(block))).createStatement(getSession()).execute();

        {
            @SuppressWarnings("unchecked")
            IFastBean<UsmaEpvBlockTitle> fastBean = UsmaEpvBlockTitle.FAST_BEAN;
            Map<String, Object> attributes = imtsaTitle.getAttributes();
            Map<String, String> attributePropertyMap = ImtsaTitle.getAttributePropertyMap();
            saveEntityWithAttributes(new UsmaEpvBlockTitle(block), fastBean, attributePropertyMap, attributes);
        }
        {
            @SuppressWarnings("unchecked")
            IFastBean<UsmaDeveloper> fastBean = UsmaDeveloper.FAST_BEAN;
            for (ImtsaDeveloper imtsaDeveloper : imtsaTitle.getDeveloperList())
            {
                Map<String, Object> attributes = imtsaDeveloper.getAttributes();
                Map<String, String> attributePropertyMap = ImtsaDeveloper.getAttributePropertyMap();
                saveEntityWithAttributes(new UsmaDeveloper(block), fastBean, attributePropertyMap, attributes);
            }
        }
        {
            @SuppressWarnings("unchecked")
            IFastBean<UsmaQualification> fastBean = UsmaQualification.FAST_BEAN;
            for (ImtsaQualification imtsaQualification : imtsaTitle.getQualificationList())
            {
                Map<String, Object> attributes = imtsaQualification.getAttributes();
                Map<String, String> attributePropertyMap = ImtsaQualification.getAttributePropertyMap();
                saveEntityWithAttributes(new UsmaQualification(block), fastBean, attributePropertyMap, attributes);
            }
        }
        {
            @SuppressWarnings("unchecked")
            IFastBean<UsmaSpeciality> fastBean = UsmaSpeciality.FAST_BEAN;
            for (ImtsaSpeciality imtsaSpeciality : imtsaTitle.getSpecialityList())
            {
                Map<String, Object> attributes = imtsaSpeciality.getAttributes();
                Map<String, String> attributePropertyMap = ImtsaSpeciality.getAttributePropertyMap();
                saveEntityWithAttributes(new UsmaSpeciality(block), fastBean, attributePropertyMap, attributes);
            }
        }

        saveSchedule(block, imtsaTitle.getSchedule(), model);
    }

    private <T extends IEntity> void saveEntityWithAttributes(T entity, IFastBean<T> fastBean, Map<String, String> attributePropertyMap, Map<String, Object> attributes)
    {
        for (Map.Entry<String, String> entry : attributePropertyMap.entrySet())
        {
            String propertyName = entry.getValue();
            Object propertyValue = attributes.get(entry.getKey());
            fastBean.setPropertyValue(entity, propertyName, propertyValue);
        }

        save(entity);
    }


    private void saveSchedule(EppEduPlanVersionBlock block, ImtsaSchedule imtsaSchedule, Model model)
    {
        if (imtsaSchedule == null && ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return;
        }

        boolean root = model.rootBlock.getId().equals(block.getId());
        boolean empty = !existsEntity(UsmaEduPlanVersionWeek.class, UsmaEduPlanVersionWeek.L_VERSION, block.getEduPlanVersion());
        EppEduPlanVersion eduPlanVersion = block.getEduPlanVersion();
        DevelopGrid developGrid = eduPlanVersion.getEduPlan().getDevelopGrid();

        Map<Integer, Map<Integer, Integer>> coursePartMap = SafeMap.get(HashMap.class);
        Map<Integer, Map<Integer, Map<Integer, String>>> weekTypeMap = SafeMap.get(key -> SafeMap.get(HashMap.class));

        Map<Course, Map<Integer, Term>> developGridTermMap = SafeMap.get(HashMap.class);
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), developGrid))
        {
            developGridTermMap
                    .get(developGridTerm.getCourse())
                    .put(developGridTerm.getPart().getNumber(), developGridTerm.getTerm());
        }

        Map<Integer, EppWeek> weekMap = new HashMap<>();
        for (EppWeek week : getCatalogItemList(EppWeek.class))
        {
            weekMap.put(week.getNumber(), week);
        }

        if (empty || root)
        {
            new DQLDeleteBuilder(UsmaEduPlanVersionPartitionType.class).where(eq(property(UsmaEduPlanVersionPartitionType.version()), value(block.getEduPlanVersion()))).createStatement(getSession()).execute();
            IDQLSelectableQuery epvWeekQuery = new DQLSelectBuilder().fromEntity(UsmaEduPlanVersionWeek.class, "w").column(property("w", UsmaEduPlanVersionWeek.id())).where(eq(property("w", UsmaEduPlanVersionWeek.version()), value(block.getEduPlanVersion()))).buildQuery();
            new DQLDeleteBuilder(UsmaEduPlanVersionWeekPart.class).where(in(property(UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().id()), epvWeekQuery)).createStatement(getSession()).execute();
            new DQLDeleteBuilder(UsmaEduPlanVersionWeek.class).where(in(property(UsmaEduPlanVersionWeek.id()), epvWeekQuery)).createStatement(getSession()).execute();

        }
        else
        {
            DQLSelectBuilder weekBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaEduPlanVersionWeek.class, "w")
                    .where(eq(property("w", UsmaEduPlanVersionWeek.version()), value(eduPlanVersion)));

            IDQLSelectQuery weekQuery = weekBuilder.buildQuery();

            List<UsmaEduPlanVersionWeekPart> weekParts = new DQLSelectBuilder()
                    .fromEntity(UsmaEduPlanVersionWeekPart.class, "wp")
                    .where(in(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek()), weekQuery))
                    .createStatement(getSession()).list();

            List<UsmaEduPlanVersionWeek> weekList = weekBuilder.createStatement(getSession()).list();

            Map<UsmaEduPlanVersionWeek, Map<Integer, String>> weekPartTypeMap = SafeMap.get(HashMap.class);
            for (UsmaEduPlanVersionWeekPart weekPart : weekParts)
            {
                weekPartTypeMap.get(weekPart.getEduPlanVersionWeek()).put(weekPart.getPartitionElementNumber(), weekPart.getWeekType().getCode());
            }

            for (UsmaEduPlanVersionWeek week : weekList)
            {
                Map<Integer, String> localPartMap;
                if (week.getWeekType() == null)
                {
                    localPartMap = weekPartTypeMap.get(week);

                }
                else
                {
                    localPartMap = new HashMap<>();
                    localPartMap.put(1, week.getWeekType().getCode());
                }

                int courseNumber = week.getCourse().getIntValue();
                int weekNumber = week.getWeek().getNumber();
                int termNumber = week.getTerm().getIntValue();

                weekTypeMap.get(courseNumber).put(weekNumber, localPartMap);
                coursePartMap.get(courseNumber).put(weekNumber, termNumber);
            }
        }

        int developGridSize = developGridTermMap.size();
        Map<Integer, Map<Integer, UsmaEduPlanVersionWeek>> courseWeekMap = SafeMap.get(HashMap.class);
        if (imtsaSchedule == null || imtsaSchedule.getCourseTermsScheduleMap() == null)
        {
            return;
        }
        for (Map.Entry<Integer, Map<Integer, Map<Integer, String>>> courseEntry : imtsaSchedule.getCourseTermsScheduleMap().entrySet())
        {
            Integer courseNumber = courseEntry.getKey();
            if (courseNumber > developGridSize)
            {
                if (ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                {
                    model.errors.add("Предупреждение загрузки плана. Учебный график задан для " + courseNumber + " курса, которого нет в учебной сетке.");
                    continue;
                }
                throw new ApplicationException("Учебный график задан для " + courseNumber + " курса, которого нет в учебной сетке.");
            }

            Course course = get(Course.class, Course.intValue(), courseNumber);
            Map<Integer, Map<Integer, String>> termScheduleMap = courseEntry.getValue();

            int dGTMapSize = developGridTermMap.get(course).size();
            if (dGTMapSize < termScheduleMap.size())
            {
                if (ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                {
                    model.errors.add("Предупреждение загрузки плана. Число семестров в " + courseNumber + " курсе не соответствует учебной сетке.");
                }
                else throw new ApplicationException("Число семестров в " + courseNumber + " курсе не соответствует учебной сетке.");
            }

            Map<Integer, Map<Integer, String>> localTermMap = termScheduleMap;
            if (termScheduleMap.size() == 1 && dGTMapSize > 1)
            {
                if (ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                {
                    model.errors.add("Предупреждение загрузки плана. Учебный график " + courseNumber + " курса задан на один семестр. Разделен вручную.");
                }
                localTermMap = new HashMap<>();
                for (Map.Entry<Integer, String> partEntry : termScheduleMap.values().iterator().next().entrySet())
                {
                    Integer partNumber = partEntry.getKey();
                    String schedule = partEntry.getValue();
                    int termSize = schedule.length() / dGTMapSize;
                    int i = 0;
                    for (; i < dGTMapSize - 1; i++)
                    {
                        localTermMap.put(i + 1, Collections.singletonMap(partNumber, schedule.substring(i * termSize, (i + 1) * termSize)));
                    }

                    localTermMap.put(dGTMapSize, Collections.singletonMap(partNumber, schedule.substring(i * termSize)));
                }
            }

            int courseWeeks = 0;
            int termWeeks = 0;
            for (Map.Entry<Integer, Map<Integer, String>> termEntry : localTermMap.entrySet())
            {
                Integer termNumber = termEntry.getKey();
                Term term = developGridTermMap.get(course).get(termNumber);
                if (term == null && ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                {
                    continue;
                }
                int parts = termEntry.getValue().size();
                for (Map.Entry<Integer, String> partEntry : termEntry.getValue().entrySet())
                {
                    Integer partNumber = partEntry.getKey();
                    String schedulePart = partEntry.getValue();
                    for (int i = 0, length = schedulePart.length(); i < length; i++)
                    {
                        int weekNumber = courseWeeks + i + 1;
                        if (weekNumber > ImtsaSchedule.getMaxWeeksInCourse())
                        {
                            model.errors.add("Число недель в учебном графике превышает " + ImtsaSchedule.getMaxWeeksInCourse() + " - пропущено.");
                            break;
                        }

                        char c = schedulePart.charAt(i);

                        if (empty || root)
                        {
                            EppWeekType weekType = model.weekTypeMap.get(c);
                            if (null == weekType)
                            {
                                if (ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                                {
                                    model.errors.add("Предупреждение загрузки плана. Импортируемый учебный график содержит игнорируемые типы недель.");
                                    continue;
                                }
                                //throw new ApplicationException("Импортируемый учебный график не соответствует имеющемуся.");
                            }

                            UsmaEduPlanVersionWeek week = courseWeekMap.get(courseNumber).get(weekNumber);
                            if (week == null)
                            {
                                week = new UsmaEduPlanVersionWeek(eduPlanVersion, course, weekMap.get(weekNumber));
                                week.setTerm(term);
                                if (parts == 1)
                                {
                                    week.setWeekType(weekType);
                                }

                                save(week);
                                courseWeekMap.get(courseNumber).put(weekNumber, week);
                            }

                            if (parts > 1)
                            {
                                UsmaEduPlanVersionWeekPart weekPart = new UsmaEduPlanVersionWeekPart(week, partNumber, weekType);
                                save(weekPart);
                            }

                        }
                        else
                        {
                            String weeTypeCode = IMTSA_WEEK_TYPE_MAP.get(c);
                            String oldWeekTypeCode = weekTypeMap.get(courseNumber).get(weekNumber).get(partNumber);
                            if (oldWeekTypeCode == null || "-".equals(weeTypeCode) || !oldWeekTypeCode.equals(weeTypeCode) || term.getIntValue() != coursePartMap.get(courseNumber).get(weekNumber))
                            {
                                if (ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                                {
                                    model.errors.add("Предупреждение загрузки плана. Импортируемый учебный график не соответствует имеющемуся.");
                                    continue;
                                }
                                throw new ApplicationException("Импортируемый учебный график не соответствует имеющемуся.");
                            }
                        }
                    }

                    termWeeks = schedulePart.length();
                }

                courseWeeks += termWeeks;
            }
        }

        if (empty || root)
        {
            UsmaSchedulePartitionType partitionType = get(UsmaSchedulePartitionType.class, UsmaSchedulePartitionType.partsNumber(), imtsaSchedule.getPartitionNumber());
            UsmaEduPlanVersionPartitionType versionPartitionType = new UsmaEduPlanVersionPartitionType(eduPlanVersion, partitionType);
            save(versionPartitionType);
        }

        updateEduPlanVersionWeekTypes(eduPlanVersion.getId(), null);
    }


    private void saveCompetences(ImtsaCompetences imtsaCompetences, EppEduPlanVersionBlock block, Model model)
    {
        if (imtsaCompetences == null)
        {
            return;
        }

        Map<String, PairKey<String, Integer>> groupNumberMap = imtsaCompetences.getCompetenceIndex2GroupCodeAndNumberMap();
        Map<String, String> indexContentMap = imtsaCompetences.getCompetencesIndex2ContentMap();

        Map<String, String> codeIndexCompetenceMap = imtsaCompetences.getCompetenceCode2IndexMap();
        model.setCodeIndexCompetenceMap(codeIndexCompetenceMap);

        String maxImpCode = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "c")
                .column(DQLFunctions.max(property(UsmaCompetence.code().fromAlias("c"))))
                .where(likeUpper(property(UsmaCompetence.code().fromAlias("c")), value(CoreStringUtils.escapeLike(IMPORTED_CODE_PREFIX))))
                .createStatement(getSession())
                .uniqueResult();

        model.competenceCode = maxImpCode != null ? Integer.valueOf(maxImpCode.substring(IMPORTED_CODE_PREFIX.length(), maxImpCode.length())) : 0;

        for (Map.Entry<String, String> indexContentEntry : indexContentMap.entrySet())
        {
            String index = indexContentEntry.getKey();
            String title = indexContentEntry.getValue();

            if (title == null/* && ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE */) // if uncomment -> NPE
            {
                continue;
            }
            if (!groupNumberMap.containsKey(index) && ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
            {
                continue;
            }
            String skillGroupShortTitle = groupNumberMap.get(index).getFirst();
            Integer competenceNumber = groupNumberMap.get(index).getSecond();

            EppSkillGroup eppSkillGroup = model.eppSkillGroupMap.get(skillGroupShortTitle);
            if (eppSkillGroup == null)
            {
                continue;
            }
            UsmaCompetence competence = new DQLSelectBuilder()
                    .fromEntity(UsmaCompetence.class, "c")
                    .where(eq(DQLFunctions.upper(DQLFunctions.replace(property(UsmaCompetence.title().fromAlias("c")), " ", "")), value(title.replaceAll("[ ]+", "").toUpperCase())))
                    .where(eq(property(UsmaCompetence.eppSkillGroup().fromAlias("c")), value(eppSkillGroup)))
                    .createStatement(getSession())
                    .setMaxResults(1)
                    .uniqueResult();

            if (competence == null)
            {
                competence = new UsmaCompetence(eppSkillGroup, model.competenceCode(), title);
                save(competence);
            }

            model.indexCompetenceMap.put(index, competence);

            UsmaCompetence2EduPlanVersionBlockRel blockRel = new UsmaCompetence2EduPlanVersionBlockRel(block, competence, competenceNumber);
            save(blockRel);
        }
    }

    private void savePlanRows(EppEduPlanVersionBlock block, ImtsaPlanRow root, Model model)
    {
        new DQLDeleteBuilder(EppEpvRow.class).where(eq(property(EppEpvRow.owner()), value(block))).createStatement(getSession()).execute();

        List<EppEpvStructureRow> cycleRows = new DQLSelectBuilder()
                .fromEntity(EppEpvStructureRow.class, "sr")
                .where(eq(property("sr", EppEpvStructureRow.owner()), value(model.rootBlock)))
                .where(isNull(property("sr", EppEpvStructureRow.parent())))
                .createStatement(getSession()).list();

        Map<EppPlanStructure, EppEpvStructureRow> cycleRowsMap = new HashMap<>();
        for (EppEpvStructureRow structureRow : cycleRows)
        {
            cycleRowsMap.put(structureRow.getValue(), structureRow);
        }

        List<EppEpvStructureRow> partRows = new DQLSelectBuilder()
                .fromEntity(EppEpvStructureRow.class, "sr")
                .where(in(property("sr", EppEpvStructureRow.parent()), cycleRows))
                .createStatement(getSession()).list();

        Map<EppEpvStructureRow, Map<EppPlanStructure, EppEpvStructureRow>> partRowsMap = SafeMap.get(HashMap.class);
        for (EppEpvStructureRow partRow : partRows)
        {
            partRowsMap.get(partRow.getParent()).put(partRow.getValue(), partRow);
        }

        model.setPartRowsMap(partRowsMap);

        for (ImtsaPlanRow imtsaPlanRow : root.getChildMap().values())
        {
            ImtsaStructure hierarchy = (ImtsaStructure) imtsaPlanRow;
            String id = hierarchy.getId();
            EppPlanStructure planStructure = model.cyclesMap.get(id);
            EppEpvStructureRow row = cycleRowsMap.get(planStructure);
            if (row == null)
            {
                row = new EppEpvStructureRow();
                row.setValue(planStructure);
                row.setOwner(model.rootBlock);
                row.setStoredIndex(id);
                row.setUserIndex(id);

                save(row);

                cycleRowsMap.put(planStructure, row);
            }

            model.setCurrentCycle(row);
            for (ImtsaPlanRow childPlanRow : imtsaPlanRow.getChildMap().values())
            {
                savePlanRow(childPlanRow, row, block, model);
            }
        }
    }

    private void savePlanRow(ImtsaPlanRow imtsaPlanRow, EppEpvRow parentRow, EppEduPlanVersionBlock block, Model model)
    {
        EppEpvRow result;
        if (imtsaPlanRow instanceof ImtsaStructure)
        {
            ImtsaStructure hierarchy = (ImtsaStructure) imtsaPlanRow;
            String id = hierarchy.getId();
            EppPlanStructure part = model.partsMap.get(id);
            EppEpvStructureRow row = model.getPartRowsMap().get((EppEpvStructureRow) parentRow).get(part);
            if (row == null)
            {
                while (parentRow.getHierarhyParent() != null)
                {
                    // все узлы станут частями
                    parentRow = parentRow.getHierarhyParent();
                }

                row = new EppEpvStructureRow();
                row.setValue(part);
                row.setHierarhyParent(parentRow);
                row.setOwner(model.rootBlock);
                row.setStoredIndex(hierarchy.getId());
                row.setUserIndex(hierarchy.getId());

                save(row);

                model.getPartRowsMap().get((EppEpvStructureRow) parentRow).put(part, row);
            }

            result = row;

        }
        else if (imtsaPlanRow instanceof ImtsaGroup)
        {
            EppEpvGroupReRow row = new EppEpvGroupReRow();
            row.setHierarhyParent(parentRow);
            row.setTitle(imtsaPlanRow.getTitle());
            row.setNumber(String.valueOf(model.nextNumber(parentRow.getId())));
            row.setOwner(block);
            row.setStoredIndex(imtsaPlanRow.getId());
            row.setUserIndex(imtsaPlanRow.getId());

            save(row);

            result = row;

        }
        else
        {
            EppEpvTermDistributedRow termDistributedRow;
            Double selfWork;
            Double totalLabor;
            Map<Integer, Map<ImtsaLoadType, Double>> term2LoadSizeMap;
            Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> term2ControlSizeMap;
            Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> courseSessionLoadSizeMap = null;
            Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> courseSessionControlSizeMap = null;
            if (imtsaPlanRow instanceof ImtsaVariant)
            {
                ImtsaVariant variant = (ImtsaVariant) imtsaPlanRow;
                EppEpvGroupImRow row = new EppEpvGroupImRow();
                row.setSize(1);
                row.setHierarhyParent(parentRow);
                row.setNumber(String.valueOf(model.nextNumber(parentRow.getId())));
                row.setTitle(imtsaPlanRow.getTitle());
                row.setHoursTotalAsDouble(variant.getTotalSize());
                row.setTotalLaborAsDouble(((ImtsaVariant) imtsaPlanRow).getTotalLabor());
                row.setOwner(block);
                row.setStoredIndex(variant.getId());
                row.setUserIndex(variant.getId());

                save(row);

                term2ControlSizeMap = variant.getTerm2ControlSizeMap();
                term2LoadSizeMap = variant.getTerm2LoadSizeMap();
                if (term2ControlSizeMap == null && term2LoadSizeMap == null)
                {
                    courseSessionControlSizeMap = variant.getCourseTermControlSizeMap();
                    courseSessionLoadSizeMap = variant.getCourseTermLoadSizeMap();
                }

                selfWork = variant.getSelfWork();
                totalLabor = variant.getTotalLabor();

                termDistributedRow = row;

            }
            else
            {
                ImtsaDiscipline discipline = (ImtsaDiscipline) imtsaPlanRow;
                EppEpvRegistryRow row = new EppEpvRegistryRow();
                row.setParent(parentRow);
                row.setTitle(imtsaPlanRow.getTitle());
                row.setRegistryElementType(model.disciplineElementType);
                row.setNumber(String.valueOf(model.nextNumber(parentRow.getId())));
                row.setTitle(imtsaPlanRow.getTitle());
                row.setHoursTotalAsDouble(discipline.getTotalSize());
                row.setTotalLaborAsDouble(discipline.getTotalLabor());
                row.setOwner(block);
                OrgUnit owner = model.orgUnitCodeMap.get(discipline.getOwnerCode());
                row.setRegistryElementOwner(owner == null ? model.topOrgUnit : owner);
                row.setStoredIndex(discipline.getId());
                row.setUserIndex(discipline.getId());

                save(row);

                if (discipline.getCompetenceIndexes() != null)
                {
                    for (String compIndex : discipline.getCompetenceIndexes())
                    {
                        UsmaCompetence competence = model.indexCompetenceMap.get(compIndex);
                        if (competence == null && ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                        {
                            continue;
                        }

                        UsmaCompetence2EpvRegistryRowRel rowRel = new UsmaCompetence2EpvRegistryRowRel(row, competence);
                        save(rowRel);
                    }
                }

                term2ControlSizeMap = discipline.getTerm2ControlSizeMap();
                term2LoadSizeMap = discipline.getTerm2LoadSizeMap();
                if (term2ControlSizeMap == null && term2LoadSizeMap == null)
                {
                    courseSessionControlSizeMap = discipline.getCourseTermControlSizeMap();
                    courseSessionLoadSizeMap = discipline.getCourseTermLoadSizeMap();
                }

                selfWork = discipline.getSelfWork();
                totalLabor = discipline.getTotalLabor();

                termDistributedRow = row;
            }

            if (model.getCurrentCycle() != null)
            {
                String code = model.getCurrentCycle().getValue().getCode();

                if (EXCLUDE_CYCLE_CODES.contains(code))
                {
                    termDistributedRow.setExcludedFromActions(true);
                    termDistributedRow.setExcludedFromLoad(true);
                }
            }

            Map<Term, EppEpvRowTerm> rowTermMap = new HashMap<>();

            if (term2LoadSizeMap != null)
            {
                for (Map.Entry<Integer, Map<ImtsaLoadType, Double>> termEntry : term2LoadSizeMap.entrySet())
                {
                    Integer termNumber = termEntry.getKey();
                    boolean total = termNumber == 0;
                    Term term = total ? null : model.termMap.get(termNumber);
                    EppEpvRowTerm rowTerm = null;
                    if (!total)
                    {
                        if (term == null)
                        {
                            continue;
                        }
                        rowTerm = new EppEpvRowTerm(termDistributedRow, term);
                        save(rowTerm);
                        rowTermMap.put(term, rowTerm);
                    }

                    Double totalALoadSize = 0.0d;
                    Double totalControl = 0.0d;
                    Map<ImtsaLoadType, Double> loadMap = termEntry.getValue();
                    Map<String, Double> epvRowTermMap = Maps.newHashMap();

                    for (Map.Entry<ImtsaLoadType, Double> loadSizeEntry : loadMap.entrySet())
                    {
                        ImtsaLoadType imtsaLoadType = loadSizeEntry.getKey();
                        Double loadSize = loadSizeEntry.getValue();

                        if (!total && imtsaLoadType == null)
                        {
                            epvRowTermMap.put(EppLoadType.FULL_CODE_LABOR, loadSize);
                        }
                        else
                        {
                            ICatalogItem loadType = model.loadTypeMap.get(imtsaLoadType);

                            if (imtsaLoadType.isControl())
                            {
                                totalControl += loadSize;
                            }
                            else if (loadType instanceof EppLoadType)
                            {
                                EppLoadType baseLoadType = (EppLoadType) loadType;
                                totalALoadSize += (!imtsaLoadType.isInter() && baseLoadType instanceof EppALoadType) ? loadSize : 0.0d;

                                if (total)
                                {
                                    if (baseLoadType instanceof EppALoadType)
                                    {
                                        EppEpvRowLoad epvRowLoad = getByNaturalId(new EppEpvRowLoad.NaturalId(termDistributedRow, (EppALoadType) baseLoadType));
                                        if (null == epvRowLoad)
                                            epvRowLoad = new EppEpvRowLoad(termDistributedRow, (EppALoadType) baseLoadType);

                                        if (imtsaLoadType.isInter())
                                            epvRowLoad.setHoursI(EppEpvRowLoad.unwrap(loadSize));
                                        else
                                            epvRowLoad.setHoursAsDouble(loadSize);

                                        saveOrUpdate(epvRowLoad);
                                    }
                                    else
                                    {
                                        Map<String, Double> eLoadTypeLoadMap = Maps.newHashMap();
                                        eLoadTypeLoadMap.put(baseLoadType.getFullCode(), loadSize);
                                        termDistributedRow.updateLoad(eLoadTypeLoadMap, false);
                                    }

                                }
                                else
                                {
                                    if (baseLoadType instanceof EppALoadType)
                                    {
                                        EppEpvRowTermLoad rowTermLoad = getByNaturalId(new EppEpvRowTermLoad.NaturalId(rowTerm, (EppALoadType) baseLoadType));
                                        if (null == rowTermLoad)
                                            rowTermLoad = new EppEpvRowTermLoad(rowTerm, (EppALoadType) baseLoadType);

                                        if (imtsaLoadType.isInter())
                                            rowTermLoad.setHoursI(EppEpvRowLoad.unwrap(loadSize));
                                        else
                                            rowTermLoad.setHoursAsDouble(loadSize);

                                        saveOrUpdate(rowTermLoad);
                                    }
                                    else
                                    {
                                        epvRowTermMap.put(baseLoadType.getFullCode(), loadSize);
                                        rowTerm.updateLoad(epvRowTermMap, false);
                                    }
                                }

                            }
                        }
                    }

                    Double selfWorkLoadSize = loadMap.get(ImtsaLoadType.SELF_WORK);
                    if (selfWorkLoadSize == null)
                    {
                        selfWorkLoadSize = 0.0d;
                    }

                    if (total)
                    {
                        termDistributedRow.setHoursTotalAsDouble(selfWorkLoadSize + totalALoadSize);
                        termDistributedRow.setHoursAuditAsDouble(totalALoadSize);
                        termDistributedRow.setHoursControl(EppEpvRowTerm.unwrap(totalControl));
                    }
                    else if (null != rowTerm)
                    {
                        rowTerm.setHoursTotalAsDouble(selfWorkLoadSize + totalALoadSize);
                        rowTerm.setHoursAuditAsDouble(totalALoadSize);
                        rowTerm.setHoursControl(EppEpvRowTerm.unwrap(totalControl));
                        update(rowTerm);
                    }
                }
            }

            if (courseSessionLoadSizeMap != null)
            {
                for (Map.Entry<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> courseEntry : courseSessionLoadSizeMap.entrySet())
                {
                    Integer courseNumber = courseEntry.getKey();
                    boolean total = courseNumber == 0;
                    for (Map.Entry<Integer, Map<ImtsaLoadType, Double>> sessionEntry : courseEntry.getValue().entrySet())
                    {
                        Integer sessionNumber = sessionEntry.getKey();
                        Term term = total ? null : model.getTermByCourseAndPart(courseNumber, sessionNumber);
                        EppEpvRowTerm rowTerm = null;
                        if (!total)
                        {
                            if (term == null)
                            {
                                model.errors.add("Предупреждение загрузки плана. Неподходящая учебная сетка: " + courseNumber + " курс, " + sessionNumber + " сессия. Нагрузка дисциплины «" + termDistributedRow.getTitle() + "» пропущена.");
                                continue;
                            }
                            rowTerm = new EppEpvRowTerm(termDistributedRow, term);
                            save(rowTerm);
                            rowTermMap.put(term, rowTerm);
                        }

                        Double totalALoadSize = 0.0d;
                        Double totalControl = 0.0d;
                        Map<ImtsaLoadType, Double> loadMap = sessionEntry.getValue();
                        Map<String, Double> epvRowTermMap = Maps.newHashMap();

                        for (Map.Entry<ImtsaLoadType, Double> loadSizeEntry : loadMap.entrySet())
                        {
                            ImtsaLoadType imtsaLoadType = loadSizeEntry.getKey();
                            Double loadSize = loadSizeEntry.getValue();

                            if (!total && imtsaLoadType == null)
                            {
                                epvRowTermMap.put(EppLoadType.FULL_CODE_LABOR, loadSize);
                            }
                            else
                            {
                                ICatalogItem loadType = model.loadTypeMap.get(imtsaLoadType);

                                if (imtsaLoadType.isControl())
                                {
                                    totalControl += loadSize;
                                }
                                else if (loadType instanceof EppLoadType)
                                {
                                    EppLoadType baseLoadType = (EppLoadType) loadType;
                                    totalALoadSize += (!imtsaLoadType.isInter() && baseLoadType instanceof EppALoadType) ? loadSize : 0.0d;

                                    if (total)
                                    {
                                        if (baseLoadType instanceof EppALoadType)
                                        {
                                            EppEpvRowLoad epvRowLoad = getByNaturalId(new EppEpvRowLoad.NaturalId(termDistributedRow, (EppALoadType) baseLoadType));
                                            if (null == epvRowLoad)
                                                epvRowLoad = new EppEpvRowLoad(termDistributedRow, (EppALoadType) baseLoadType);

                                            if (imtsaLoadType.isInter())
                                                epvRowLoad.setHoursI(EppEpvRowLoad.unwrap(loadSize));
                                            else
                                                epvRowLoad.setHoursAsDouble(loadSize);

                                            saveOrUpdate(epvRowLoad);
                                        }
                                        else
                                        {
                                            Map<String, Double> eLoadTypeloadMap = Maps.newHashMap();
                                            eLoadTypeloadMap.put(baseLoadType.getFullCode(), loadSize);
                                            termDistributedRow.updateLoad(eLoadTypeloadMap, false);
                                        }

                                    }
                                    else
                                    {
                                        if (baseLoadType instanceof EppALoadType)
                                        {
                                            EppEpvRowTermLoad rowTermLoad = getByNaturalId(new EppEpvRowTermLoad.NaturalId(rowTerm, (EppALoadType) baseLoadType));
                                            if (null == rowTermLoad)
                                                rowTermLoad = new EppEpvRowTermLoad(rowTerm, (EppALoadType) baseLoadType);

                                            if (imtsaLoadType.isInter())
                                                rowTermLoad.setHoursI(EppEpvRowLoad.unwrap(loadSize));
                                            else
                                                rowTermLoad.setHoursAsDouble(loadSize);

                                            saveOrUpdate(rowTermLoad);
                                        }
                                        else
                                        {
                                            epvRowTermMap.put(baseLoadType.getFullCode(), loadSize);
                                            rowTerm.updateLoad(epvRowTermMap, false);
                                        }
                                    }

                                }
                            }
                        }

                        Double selfWorkLoadSize = loadMap.get(ImtsaLoadType.SELF_WORK);
                        if (selfWorkLoadSize == null)
                        {
                            selfWorkLoadSize = 0.0d;
                        }

                        if (total)
                        {
                            termDistributedRow.setHoursTotalAsDouble(selfWorkLoadSize + totalALoadSize);
                            termDistributedRow.setHoursAuditAsDouble(totalALoadSize);
                            termDistributedRow.setHoursControl(EppEpvRowTerm.unwrap(totalControl));
                        }
                        else if (null != rowTerm)
                        {
                            rowTerm.setHoursTotalAsDouble(selfWorkLoadSize + totalALoadSize);
                            rowTerm.setHoursAuditAsDouble(totalALoadSize);
                            rowTerm.setHoursControl(EppEpvRowTerm.unwrap(totalControl));
                            update(rowTerm);
                        }
                    }
                }
            }

            if (term2ControlSizeMap != null)
            {
                for (Map.Entry<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> term2ControlActionEntry : term2ControlSizeMap.entrySet())
                {
                    Integer termNumber = term2ControlActionEntry.getKey();
                    Term term = model.termMap.get(termNumber);
                    EppEpvRowTerm rowTerm = rowTermMap.get(term);
                    if (rowTerm == null)
                    {
                        rowTerm = new EppEpvRowTerm(termDistributedRow, term);
                        save(rowTerm);
                        rowTermMap.put(term, rowTerm);
                    }

                    for (Map.Entry<Class<? extends EppControlActionType>, Map<String, Integer>> actionTypeEntry : term2ControlActionEntry.getValue().entrySet())
                    {
                        Class<? extends EppControlActionType> clazz = actionTypeEntry.getKey();
                        for (Map.Entry<String, Integer> controlActionEntry : actionTypeEntry.getValue().entrySet())
                        {
                            String controlCode = controlActionEntry.getKey();
                            EppControlActionType controlActionType = getCatalogItem(clazz, controlCode);

                            EppEpvRowTermAction rowTermAction = new EppEpvRowTermAction();
                            rowTermAction.setRowTerm(rowTerm);
                            rowTermAction.setControlActionType(controlActionType);
                            rowTermAction.setSize(controlActionEntry.getValue());

                            save(rowTermAction);
                        }
                    }
                }
            }

            if (courseSessionControlSizeMap != null)
            {
                for (Map.Entry<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> courseEntry : courseSessionControlSizeMap.entrySet())
                {
                    Integer courseNumber = courseEntry.getKey();
                    for (Map.Entry<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> sessionEntry : courseEntry.getValue().entrySet())
                    {
                        Integer sessionNumber = sessionEntry.getKey();
                        Term term = model.getTermByCourseAndPart(courseNumber, sessionNumber);
                        if (term == null)
                        {
                            model.errors.add("Предупреждение загрузки плана. Неподходящая учебная сетка: " + courseNumber + " курс, " + sessionNumber + " сессия. Форма контроля дисциплины «" + termDistributedRow.getTitle() + "» пропущена.");
                            continue;
                        }

                        EppEpvRowTerm rowTerm = rowTermMap.get(term);
                        if (rowTerm == null)
                        {
                            rowTerm = new EppEpvRowTerm(termDistributedRow, term);
                            save(rowTerm);
                            rowTermMap.put(term, rowTerm);
                        }

                        for (Map.Entry<Class<? extends EppControlActionType>, Map<String, Integer>> actionTypeEntry : sessionEntry.getValue().entrySet())
                        {
                            Class<? extends EppControlActionType> clazz = actionTypeEntry.getKey();
                            for (Map.Entry<String, Integer> controlActionEntry : actionTypeEntry.getValue().entrySet())
                            {
                                String controlCode = controlActionEntry.getKey();
                                EppControlActionType controlActionType = getCatalogItem(clazz, controlCode);

                                EppEpvRowTermAction rowTermAction = new EppEpvRowTermAction();
                                rowTermAction.setRowTerm(rowTerm);
                                rowTermAction.setControlActionType(controlActionType);
                                rowTermAction.setSize(controlActionEntry.getValue());

                                save(rowTermAction);
                            }
                        }
                    }
                }
            }

            if (null != selfWork)
                termDistributedRow.setHoursSelfworkAsDouble(selfWork);
            if (null != totalLabor)
                termDistributedRow.setTotalLaborAsDouble(totalLabor);

            result = termDistributedRow;
        }

        List<ImtsaPlanRow> childPlanRows = new ArrayList<>(imtsaPlanRow.getChildMap().values());
        Collections.sort(childPlanRows, PLAN_ROWS_COMPARATOR);
        for (ImtsaPlanRow childPlanRow : childPlanRows)
        {
            savePlanRow(childPlanRow, result, block, model);
        }
    }


    private void savePractices(EppEduPlanVersionBlock block, Map<String, Collection<ImtsaPractice>> practiceMap, Model model)
    {
        List<UsmaPracticeDispersion> practiceDispersions = new ArrayList<>();
        boolean isGos2 = block.getEduPlanVersion().getEduPlan().getGeneration().getNumber() == 2;
        Map<String, String> practiceTypeMap = isGos2 ? GOS2_PRACTICE_TYPE_MAP : GOS3_PRACTICE_TYPE_MAP;

        int index = 1;
        String indexStr = StringUtils.leftPad(String.valueOf(index++), 2, '0');
        if (isGos2)
        {
            for (Map.Entry<String, Collection<ImtsaPractice>> practiceTypeEntry : practiceMap.entrySet())
            {
                String practiceTypeName = practiceTypeEntry.getKey();
                EppRegistryStructure practiceType = model.practiceMap.get(practiceTypeMap.get(practiceTypeName));
                for (ImtsaPractice imtsaPractice : practiceTypeEntry.getValue())
                {
                    EppEpvRegistryRow practice = new EppEpvRegistryRow();
                    practice.setExcludedFromActions(true);
                    practice.setExcludedFromLoad(true);
                    practice.setRegistryElementType(practiceType);
                    practice.setNumber(String.valueOf(model.nextNumber(1L)));
                    practice.setTitle(imtsaPractice.getTitle());
                    practice.setOwner(block);
                    practice.setStoredIndex(indexStr);
                    practice.setUserIndex(indexStr);

                    OrgUnit owner = model.orgUnitCodeMap.get(imtsaPractice.getOwnerCode());
                    practice.setRegistryElementOwner(owner == null ? model.topOrgUnit : owner);

                    save(practice);

                    for (Map.Entry<Integer, Double> termEntry : imtsaPractice.getTerm2WeeksMap().entrySet())
                    {
                        Integer termNumber = termEntry.getKey();
                        Double weeksNumber = termEntry.getValue();
                        Term term = model.termMap.get(termNumber);

                        if (term == null)
                        {
                            continue;
                        }
                        EppEpvRowTerm rowTerm = new EppEpvRowTerm();
                        rowTerm.setTerm(term);
                        rowTerm.setRow(practice);
                        rowTerm.setWeeksAsDouble(weeksNumber);

                        save(rowTerm);
                    }
                }
            }

        }
        else if (practiceMap != null)
        {
            boolean root = block.getId().equals(model.rootBlock.getId());
            for (Map.Entry<String, Collection<ImtsaPractice>> practiceTypeEntry : practiceMap.entrySet())
            {
                String practiceTypeName = practiceTypeEntry.getKey();
                EppRegistryStructure practiceType = model.practiceMap.get(practiceTypeMap.get(practiceTypeName));
                for (ImtsaPractice imtsaPractice : practiceTypeEntry.getValue())
                {
                    EppEpvRegistryRow practice = new EppEpvRegistryRow();
                    practice.setExcludedFromActions(true);
                    practice.setExcludedFromLoad(true);
                    practice.setRegistryElementType(practiceType);
                    practice.setNumber(String.valueOf(model.nextNumber(1L)));
                    practice.setTitle(imtsaPractice.getTitle());
                    practice.setOwner(block);
                    practice.setStoredIndex(indexStr);
                    practice.setUserIndex(indexStr);

                    OrgUnit owner = model.orgUnitCodeMap.get(imtsaPractice.getOwnerCode());
                    practice.setRegistryElementOwner(owner == null ? model.topOrgUnit : owner);

                    save(practice);

                    if (imtsaPractice.isDispersed() && root)
                    {
                        UsmaPracticeDispersion practiceDispersion = new UsmaPracticeDispersion(practice, true);
                        save(practiceDispersion);

                        practiceDispersions.add(practiceDispersion);
                    }

                    Map<Term, EppEpvRowTerm> rowTermMap = new HashMap<>();
                    for (Map.Entry<Integer, Double> termEntry : imtsaPractice.getTerm2WeeksMap().entrySet())
                    {
                        Integer termNumber = termEntry.getKey();
                        Double weeksNumber = termEntry.getValue();
                        Term term = model.termMap.get(termNumber);

                        if (term == null)
                        {
                            continue;
                        }
                        EppEpvRowTerm rowTerm = new EppEpvRowTerm();
                        rowTerm.setTerm(term);
                        rowTerm.setRow(practice);
                        rowTerm.setWeeksAsDouble(weeksNumber);

                        save(rowTerm);

                        rowTermMap.put(term, rowTerm);
                    }

                    Double totalLoadSize = 0.0d;
                    Double totalALoadSize = 0.0d;
                    Double totalSelfWork = 0.0d;

                    for (Map.Entry<Integer, Map<String, Double>> term2LoadSizeEntry : imtsaPractice.getTerm2LoadSizeMap().entrySet())
                    {
                        Term term = model.termMap.get(term2LoadSizeEntry.getKey());
                        if (term == null)
                        {
                            continue;
                        }

                        EppEpvRowTerm rowTerm = rowTermMap.get(term);
                        if (rowTerm == null)
                        {
                            rowTerm = new EppEpvRowTerm(practice, term);
                            save(rowTerm);
                            rowTermMap.put(term, rowTerm);
                        }

                        Map<String, Double> eLoadTypeloadMap = Maps.newHashMap();
                        eLoadTypeloadMap.put(EppLoadType.FULL_CODE_WEEKS, rowTerm.getWeeksAsDouble());

                        for (Map.Entry<String, Double> loadSizeEntry : term2LoadSizeEntry.getValue().entrySet())
                        {
                            String loadCode = loadSizeEntry.getKey();
                            Double loadSize = loadSizeEntry.getValue();
                            int dotIndex = loadCode.indexOf('.');
                            if (dotIndex == -1)
                            {
                                if (ImtsaPractice.getTotalLoadSizeCode().equals(loadCode))
                                {
                                    eLoadTypeloadMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, loadSize);
                                    totalLoadSize += loadSize;
                                }
                                else
                                    eLoadTypeloadMap.put(EppLoadType.FULL_CODE_LABOR, loadSize);
                            }
                            else
                            {
                                String loadTypeCode = loadCode.substring(dotIndex + 1);
                                EppELoadType loadType = getCatalogItem(EppELoadType.class, loadTypeCode);

                                if (loadTypeCode.equals(EppELoadType.TYPE_TOTAL_AUDIT))
                                    totalALoadSize += loadSize;
                                else if (loadTypeCode.equals(EppELoadType.TYPE_TOTAL_SELFWORK))
                                    totalSelfWork += loadSize;

                                eLoadTypeloadMap.put(loadType.getFullCode(), loadSize);
                            }
                        }
                        rowTerm.updateLoad(eLoadTypeloadMap, false);
                    }

                    Double expertZet = imtsaPractice.getTerm2LoadSizeMap().get(0).get(ImtsaPractice.getTotalLaborCode());
                    if (expertZet != null)
                    {
                        practice.setTotalLaborAsDouble(expertZet);
                    }

                    practice.setHoursTotalAsDouble(totalLoadSize);
                    practice.setHoursAuditAsDouble(totalALoadSize);
                    practice.setHoursSelfworkAsDouble(totalSelfWork);
                    update(practice);

                    for (Map.Entry<String, Set<Integer>> controlActionEntry : imtsaPractice.getControlAction2TermSetMap().entrySet())
                    {
                        EppFControlActionType controlActionType = getCatalogItem(EppFControlActionType.class, controlActionEntry.getKey());
                        for (Integer termNumber : controlActionEntry.getValue())
                        {
                            Term term = model.termMap.get(termNumber);
                            if (term == null)
                            {
                                continue;
                            }

                            EppEpvRowTerm rowTerm = rowTermMap.get(term);
                            if (rowTerm == null)
                            {
                                rowTerm = new EppEpvRowTerm(practice, term);
                                save(rowTerm);
                                rowTermMap.put(term, rowTerm);
                            }

                            EppEpvRowTermAction rowTermAction = new EppEpvRowTermAction();
                            rowTermAction.setRowTerm(rowTerm);
                            rowTermAction.setControlActionType(controlActionType);
                            rowTermAction.setSize(1);

                            save(rowTermAction);
                        }
                    }

                    Set<String> competenceCodes = imtsaPractice.getCompetenceCodes();
                    if (competenceCodes != null)
                    {
                        for (String competenceCode : competenceCodes)
                        {
                            String competenceIndex = model.getCodeIndexCompetenceMap().get(competenceCode);
                            UsmaCompetence competence = model.indexCompetenceMap.get(competenceIndex);

                            if (competence != null)
                            {
                                save(new UsmaCompetence2EpvRegistryRowRel(practice, competence));
                            }
                        }
                    }
                }
            }
        }

        updateScheduleDispersedWeekTypes(practiceDispersions);
    }


    private static final String IMPORTED_CODE_PREFIX = "imp";

    public static Map<Character, String> IMTSA_WEEK_TYPE_MAP = new HashMap<>();

    static
    {
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_EXAMINATION_SESSION, EppWeekTypeCodes.EXAMINATION_SESSION);        //    "Э" - Экзаменационные сессии (в Uni "э" - Экзаменационная сессия)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_TEACH_PRACTICE, EppWeekTypeCodes.TEACH_PRACTICE);                   //    "У" - Учебная пркатика (концентр.) (в Uni "у" - Учебная практика)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_RESEARCH_WORK, EppWeekTypeCodes.RESEARCH_WORK);                     //    "Н" - Научно-исслед. работа (концентр.) (в Uni "н" - Научно-исследовательская работа)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_WORK_PRACTICE, EppWeekTypeCodes.WORK_PRACTICE);                     //    "П" - Производственная практика (концентр.) (в Uni "п" - Производственная практика)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_QUALIFICATION_WORK, EppWeekTypeCodes.PREPARE_QUALIFICATION_WORK);//    "Д" - Диссертация (в Uni "пд" - Подготовка диссертации)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_STATE_EXAMINATION, EppWeekTypeCodes.STATE_EXAMINATION);           //    "Г" - Гос. экзамены (в Uni "г" - Государственные экзамены)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_HOLIDAYS, EppWeekTypeCodes.HOLIDAYS);                                //    "К" - Каникулы (в Uni "к" - Каникулы)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_UNUSED, EppWeekTypeCodes.UNUSED);                                    //    "=" - неделя отсутствует (в Uni "нн" - Неиспользуемые недели)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_THEORY, EppWeekTypeCodes.THEORY);                                    //    "Т" - Теоретическое обучение и рассредоточенные практики (в Uni "т" - Теоретическое обучение)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_ATTESTATION, EppWeekTypeCodes.RESULT_ATTESTATION);                 //    "А" - Итоговая атестация (в Uni "г" - Итоговая атестация)
        IMTSA_WEEK_TYPE_MAP.put(UniusmaDefines.IMTSA_IGNORE, "-");                                                       //    "-" - Игнорируемый тип недели (не создаём)
    }

    private static final Map<String, String> GOS2_PRACTICE_TYPE_MAP = new HashMap<>();

    static
    {
        GOS2_PRACTICE_TYPE_MAP.put(ImtsaPractice.getGos2TutoringPracticeNodeName(), EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING);
        GOS2_PRACTICE_TYPE_MAP.put(ImtsaPractice.getGos2OtherPracticeNodeName(), EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_OTHER);
    }

    private static final Map<String, String> GOS3_PRACTICE_TYPE_MAP = new HashMap<>();

    static
    {
        GOS3_PRACTICE_TYPE_MAP.put(ImtsaFile.getGos3TutoringPracticeGroupNodeName(), EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING);
        GOS3_PRACTICE_TYPE_MAP.put(ImtsaFile.getGos3NirPracticeGroupNodeName(), EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR);
        GOS3_PRACTICE_TYPE_MAP.put(ImtsaFile.getGos3OtherPracticeGroupNodeName(), EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION);
    }


    private static final Comparator<String> TITLE_COMPARATOR = (o1, o2) -> {
        boolean o1Variant = o1.startsWith(ImtsaVariant.getNewVariantPrefix());
        boolean o2Variant = o2.startsWith(ImtsaVariant.getNewVariantPrefix());

        if (o1Variant && o2Variant)
        {
            return o1.length() == o2.length() ? o1.compareTo(o2) : (o1.length() < o2.length() ? -1 : 1);
        }

        if (o1Variant || o2Variant)
        {
            return o1Variant ? 1 : -1;
        }

        return 0;
    };

    private static final Comparator<ImtsaPlanRow> PLAN_ROWS_COMPARATOR = (o1, o2) -> TITLE_COMPARATOR.compare(o1.getTitle(), o2.getTitle());

    private static final Set<String> EXCLUDE_CYCLE_CODES = ImmutableSet.<String>builder()
            .addAll(EppPlanStructure.OPTIONAL_DISCIPLINES)
            .add(EppPlanStructureCodes.FGOS_CYCLES_PHYSICAL_TRAINING)
            .build();


    @Override
    public void updateEduPlanVersionWeekTypes(Long eduPlanVersionId, @Nullable Long courseId)
    {
        new DQLDeleteBuilder(EppEduPlanVersionWeekType.class).where(eq(property(EppEduPlanVersionWeekType.eduPlanVersion().id()), value(eduPlanVersionId))).where(courseId == null ? null : eq(property(EppEduPlanVersionWeekType.course().id()), value(courseId))).createStatement(getSession()).execute();

        EppWeekType theory = getCatalogItem(EppWeekType.class, EppWeekTypeCodes.THEORY);
        List<String> dispersedTypeCodes = Arrays.asList(EppWeekTypeCodes.RESEARCH_WORK_WITH_AUDIT_LOAD, EppWeekTypeCodes.TEACH_PRACTICE_WITH_AUDIT_LOAD, EppWeekTypeCodes.WORK_PRACTICE_WITH_AUDIT_LOAD);

        //Map<Long, Map<Long, PairKey<Term, EppWeekType>>> updateMap = SafeMap.get(HashMap.class);
        List<UsmaEduPlanVersionWeekPart> weekParts = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeekPart.class, "wp")
                .where(eq(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().version().id()), value(eduPlanVersionId)))
                .where(courseId == null ? null : eq(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().course().id()), value(courseId)))
                .createStatement(getSession())
                .list();

        Map<UsmaEduPlanVersionWeek, List<EppWeekType>> weekPartsTypeMap = SafeMap.get(ArrayList.class);
        for (UsmaEduPlanVersionWeekPart weekPart : weekParts)
        {
            weekPartsTypeMap.get(weekPart.getEduPlanVersionWeek()).add(weekPart.getWeekType());
        }

        List<UsmaEduPlanVersionWeek> weekList = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeek.class, "w")
                .where(eq(property("w", UsmaEduPlanVersionWeek.version().id()), value(eduPlanVersionId)))
                .where(courseId == null ? null : eq(property("w", UsmaEduPlanVersionWeek.course().id()), value(courseId)))
                .createStatement(getSession())
                .list();

        EppEduPlanVersion version = get(EppEduPlanVersion.class, eduPlanVersionId);
        for (UsmaEduPlanVersionWeek week : weekList)
        {
            Term term = week.getTerm();
            Course course = week.getCourse();
            EppWeek eppWeek = week.getWeek();
            EppWeekType weekType = week.getWeekType();

            List<EppWeekType> weekTypeList = weekPartsTypeMap.get(week);
            if (weekTypeList != null)
            {
                Map<EppWeekType, Integer> freqMap = SafeMap.get(key -> 0);
                for (EppWeekType eppWeekType : weekTypeList)
                {
                    freqMap.put(eppWeekType, 1 + freqMap.get(eppWeekType));
                }

                int max = 0;
                for (Map.Entry<EppWeekType, Integer> freqEntry : freqMap.entrySet())
                {
                    if (max < (max = Math.max(max, freqEntry.getValue())))
                    {
                        weekType = freqEntry.getKey();
                    }
                }
            }

            EppWeekType resultType = dispersedTypeCodes.contains(weekType.getCode()) ? theory : weekType;

            EppEduPlanVersionWeekType versionWeekType = new EppEduPlanVersionWeekType(version, course, eppWeek);
            versionWeekType.setTerm(term);
            versionWeekType.setWeekType(resultType);

            save(versionWeekType);
        }
    }


    @Override
    public void createRegistryElements(Collection<Long> registryRowIds)
    {
        // интерактивная нагрузка, часы за экзамен УПв на семестр
        final Map<Long, Map<Integer, List<UsmaEpvRowTermLoad>>> termLoadMap = Maps.newHashMap();

        // самостоятельная нагрузка УПв на семестр
        final Map<Long, Map<Integer, List<EppEpvRowTerm>>> selfWorkLoadMap = Maps.newHashMap();

        final EppELoadType selfWorkLoadType = DataAccessServices.dao().get(EppELoadType.class, EppELoadType.code(), EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);
        final List<EppEpvRegistryRow> rows = Lists.newArrayList();

        final Map<Long, IEppEpvBlockWrapper> blockWrapperMap = SafeMap.get(key -> IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(key, true));

        for (List<Long> elements : Iterables.partition(registryRowIds, 32)) {
            // интерактивная нагрузка, часы за экзамен УПв на семестр
            DQLSelectBuilder termLoadBuilder = new DQLSelectBuilder().fromEntity(UsmaEpvRowTermLoad.class, "l");
            termLoadBuilder.where(in(property("l", UsmaEpvRowTermLoad.rowTerm().row().id()), elements));

            for (UsmaEpvRowTermLoad termLoad : termLoadBuilder.createStatement(getSession()).<UsmaEpvRowTermLoad>list())
            {
                Long rowId = termLoad.getRowTerm().getRow().getId();
                Integer term = termLoad.getRowTerm().getTerm().getIntValue();
                if (!termLoadMap.containsKey(rowId))
                    termLoadMap.put(rowId, Maps.<Integer, List<UsmaEpvRowTermLoad>>newHashMap());
                if (!termLoadMap.get(rowId).containsKey(term))
                    termLoadMap.get(rowId).put(term, Lists.<UsmaEpvRowTermLoad>newArrayList());
                if (!termLoadMap.get(rowId).get(term).contains(termLoad))
                    termLoadMap.get(rowId).get(term).add(termLoad);
            }

            // самостоятельная нагрузка УПв на семестр
            DQLSelectBuilder selfWorkLoadBuilder = new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, "rl");
            //selfWorkLoadBuilder.where(eq(property("rl", EppEpvRowTerm.loadType()), value(selfWorkLoadType)));
            selfWorkLoadBuilder.where(in(property("rl", EppEpvRowTerm.row().id()), elements));

            for (EppEpvRowTerm selfWorkLoad : selfWorkLoadBuilder.createStatement(getSession()).<EppEpvRowTerm>list())
            {
                Long rowId = selfWorkLoad.getRow().getId();
                Integer term = selfWorkLoad.getTerm().getIntValue();
                if (!selfWorkLoadMap.containsKey(rowId))
                    selfWorkLoadMap.put(rowId, Maps.<Integer, List<EppEpvRowTerm>>newHashMap());
                if (!selfWorkLoadMap.get(rowId).containsKey(term))
                    selfWorkLoadMap.get(rowId).put(term, Lists.<EppEpvRowTerm>newArrayList());
                if (!selfWorkLoadMap.get(rowId).get(term).contains(selfWorkLoad))
                    selfWorkLoadMap.get(rowId).get(term).add(selfWorkLoad);
            }


            for (EppEpvRegistryRow row : getList(EppEpvRegistryRow.class, elements, EppEpvRegistryRow.P_ID))
            {
                try
                {
                    IEppEpvBlockWrapper blockWrapper = blockWrapperMap.get(row.getOwner().getId());
                    IEppEpvRowWrapper rowWrapper = blockWrapper.getRowMap().get(row.getId());
                    EppRegistryElement element = IEppRegistryDAO.instance.get().doCreateRegistryElement(new EppEpvRegElWrapper(rowWrapper), null, null, null);
                    row.setRegistryElement(element);
                    rows.add(row);
                }
                catch (Exception e)
                {
                    ContextLocal.getErrorCollector().add("Произошла ошибка при обработке строки " + row.getDisplayableTitle() + " учебного плана " + row.getOwner().getEduPlanVersion().getTitle() + ". Пропущено.");
                    logger.error("Error has occured on row " + row.getId(), e);
                }
                update(row);
            }
        }

        Map<Long, Map<Integer, List<EppRegistryModule>>> partModulesMap = getPartModulesMap(rows);
        Set<Long> epbBlockIds = CommonBaseEntityUtil.getPropertiesSet(rows, EppEpvRegistryRow.owner().id());
        Map<Long, IEppEpvBlockWrapper> epvWrapperMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(epbBlockIds, true);
        for (EppEpvRegistryRow row : rows)
        {
            if (null != row.getRegistryElement())
            {
                EppRegistryElement element = row.getRegistryElement();

                // интерактивная нагрузка, часы за экзамен, самостаятельная нагрузка

                Map<Integer, List<EppRegistryModule>> elemPartModulesMap = partModulesMap.get(element.getId());
                if (null != elemPartModulesMap && !elemPartModulesMap.isEmpty()
                        && null != epvWrapperMap.get(row.getOwner().getId())
                        && null != epvWrapperMap.get(row.getOwner().getId()).getRowMap()
                        && null != epvWrapperMap.get(row.getOwner().getId()).getRowMap().get(row.getId()))
                {
                    // грузим оболочку для строки УП
                    final IEppEpvRowWrapper epvWrapper = epvWrapperMap.get(row.getOwner().getId()).getRowMap().get(row.getId());

                    // перечень активных семестров (номера семестров, в которых есть нагрузка)
                    final Set<Integer> activeTermSet = epvWrapper.getActiveTermSet();

                    int partNumber = 1;
                    for (Integer term : activeTermSet)
                    {
                        List<EppRegistryModule> modules = elemPartModulesMap.get(partNumber++);
                        if (null != modules && !modules.isEmpty())
                        {
                            if (null != termLoadMap.get(row.getId()) && null != termLoadMap.get(row.getId()).get(term) && !termLoadMap.get(row.getId()).get(term).isEmpty())
                            {
                                List<UsmaEpvRowTermLoad> termLoads = termLoadMap.get(row.getId()).get(term);

                                for (UsmaEpvRowTermLoad termLoad : termLoads)
                                {
                                    Long load = termLoad.getLoad();
                                    Long loadPart = load / modules.size();
                                    for (EppRegistryModule module : modules)
                                    {
                                        UsmaEppRegistryModuleALoadExt usmaLoadExt = new UsmaEppRegistryModuleALoadExt();
                                        usmaLoadExt.setLoadType(termLoad.getLoadType());
                                        usmaLoadExt.setModule(module);
                                        if (modules.indexOf(module) == modules.size() - 1)
                                            usmaLoadExt.setLoad(load - (loadPart * (modules.size() - 1)));
                                        else
                                            usmaLoadExt.setLoad(loadPart);
                                        save(usmaLoadExt);
                                    }
                                }
                            }

                            if (null != selfWorkLoadMap.get(row.getId()) && null != selfWorkLoadMap.get(row.getId()).get(term) && !selfWorkLoadMap.get(row.getId()).get(term).isEmpty())
                            {
                                List<EppEpvRowTerm> selfWorkLoads = selfWorkLoadMap.get(row.getId()).get(term);

                                for (EppEpvRowTerm selfWorkLoad : selfWorkLoads)
                                {
                                    Long load = selfWorkLoad.getHoursSelfwork();
                                    Long loadPart = load / modules.size();
                                    for (EppRegistryModule module : modules)
                                    {
                                        UsmaEppRegistryModuleELoad usmaELoad = new UsmaEppRegistryModuleELoad();
                                        usmaELoad.setLoadType(selfWorkLoadType);
                                        usmaELoad.setModule(module);
                                        if (modules.indexOf(module) == modules.size() - 1)
                                            usmaELoad.setLoad(load - (loadPart * (modules.size() - 1)));
                                        else
                                            usmaELoad.setLoad(loadPart);
                                        save(usmaELoad);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private Map<Long, Map<Integer, List<EppRegistryModule>>> getPartModulesMap(List<EppEpvRegistryRow> rows)
    {
        List<Long> elementsIds = CommonBaseEntityUtil.getPropertiesList(rows, EppEpvRegistryRow.registryElement().id());

        final Map<Long, Map<Integer, List<EppRegistryModule>>> partModulesMap = Maps.newHashMap();

        for (List<Long> elements : Lists.partition(elementsIds, DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder elementModulesBuilder = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "m");
            elementModulesBuilder.where(isNotNull(property("m", EppRegistryElementPartModule.module())));
            elementModulesBuilder.where(in(property("m", EppRegistryElementPartModule.part().registryElement().id()), elements));

            for (EppRegistryElementPartModule partModule : elementModulesBuilder.createStatement(getSession()).<EppRegistryElementPartModule>list())
            {
                Long elemId = partModule.getPart().getRegistryElement().getId();
                if (!partModulesMap.containsKey(elemId))
                    partModulesMap.put(elemId, Maps.<Integer, List<EppRegistryModule>>newHashMap());
                if (!partModulesMap.get(elemId).containsKey(partModule.getPart().getNumber()))
                    partModulesMap.get(elemId).put(partModule.getPart().getNumber(), Lists.<EppRegistryModule>newArrayList());
                if (!partModulesMap.get(elemId).get(partModule.getPart().getNumber()).contains(partModule.getModule()))
                    partModulesMap.get(elemId).get(partModule.getPart().getNumber()).add(partModule.getModule());
            }
        }

        return partModulesMap;
    }


    @Override
    public void updateScheduleDispersedWeekTypes(Collection<UsmaPracticeDispersion> dispersions)
    {
        Map<String, EppWeekType> practiceTypeWeekTypeMap = ImmutableMap.of(
                EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, getCatalogItem(EppWeekType.class, EppWeekTypeCodes.TEACH_PRACTICE_WITH_AUDIT_LOAD),
                EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR, getCatalogItem(EppWeekType.class, EppWeekTypeCodes.RESEARCH_WORK_WITH_AUDIT_LOAD),
                EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION, getCatalogItem(EppWeekType.class, EppWeekTypeCodes.WORK_PRACTICE_WITH_AUDIT_LOAD));

        EppWeekType theory = getCatalogItem(EppWeekType.class, EppWeekTypeCodes.THEORY);

        for (UsmaPracticeDispersion dispersion : dispersions)
        {
            EppEpvRegistryRow row = dispersion.getPractice();
            EppEduPlanVersion version = row.getOwner().getEduPlanVersion();
            String practiceType = row.getRegistryElementType().getCode();
            EppWeekType weekType = practiceTypeWeekTypeMap.get(practiceType);

            List<String> termCodes = new DQLSelectBuilder()
                    .fromEntity(EppEpvRowTerm.class, "rt")
                    .column(property("rt", EppEpvRowTerm.term().code()))
                    .where(eq(property("rt", EppEpvRowTerm.row()), value(row)))
                    .where(gt(property("rt", EppEpvRowTerm.weeks()), value(0L)))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(getSession()).list();

            if (dispersion.isDispersed())
            {
                new DQLUpdateBuilder(UsmaEduPlanVersionWeek.class)
                        .set(UsmaEduPlanVersionWeek.weekType().s(), value(weekType))
                        .where(in(property(UsmaEduPlanVersionWeek.term().code()), termCodes))
                        .where(eq(property(UsmaEduPlanVersionWeek.version()), value(version)))
                        .where(eq(property(UsmaEduPlanVersionWeek.weekType().code()), value(EppWeekTypeCodes.THEORY)))
                        .createStatement(getSession())
                        .execute();

                new DQLUpdateBuilder(UsmaEduPlanVersionWeekPart.class)
                        .set(UsmaEduPlanVersionWeekPart.weekType().s(), value(weekType))
                        .where(in(property(UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().term().code()), termCodes))
                        .where(eq(property(UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(version)))
                        .where(eq(property(UsmaEduPlanVersionWeekPart.weekType().code()), value(EppWeekTypeCodes.THEORY)))
                        .createStatement(getSession())
                        .execute();

                new DQLUpdateBuilder(EppEduPlanVersionWeekType.class)
                        .set(EppEduPlanVersionWeekType.weekType().s(), value(theory))
                        .where(in(property(EppEduPlanVersionWeekType.term().code()), termCodes))
                        .where(eq(property(EppEduPlanVersionWeekType.eduPlanVersion()), value(version)))

                        /* логика отличается - в продуктовом должна быть только теория */
                        .where(in(property(EppEduPlanVersionWeekType.weekType().code()),
                                  new Object[]{EppWeekTypeCodes.RESEARCH_WORK_WITH_AUDIT_LOAD, EppWeekTypeCodes.TEACH_PRACTICE_WITH_AUDIT_LOAD, EppWeekTypeCodes.WORK_PRACTICE_WITH_AUDIT_LOAD}))
                        .createStatement(getSession())
                        .execute();

            }
            else
            {
                for (String termCode : termCodes)
                {
                    DQLSelectBuilder dql = new DQLSelectBuilder()
                            .fromEntity(UsmaPracticeDispersion.class, "pd")
                            .joinEntity("pd", DQLJoinType.inner, EppEpvRowTerm.class, "rt", eq(property("rt", EppEpvRowTerm.row()), property("pd", UsmaPracticeDispersion.practice())))
                            .where(eq(property("pd", UsmaPracticeDispersion.dispersed()), value(true)))
                            .where(eq(property("pd", UsmaPracticeDispersion.practice().registryElementType().code()), value(practiceType)))
                            .where(eq(property("rt", EppEpvRowTerm.term().code()), value(termCode)))
                            .where(and(
                                    eq(property("pd", UsmaPracticeDispersion.practice().owner().eduPlanVersion()), value(version)),
                                    instanceOf(UsmaPracticeDispersion.practice().owner().fromAlias("pd").s(), EppEduPlanVersionRootBlock.class)));

                    if (!existsEntity(dql.buildQuery()))
                    {
                        new DQLUpdateBuilder(UsmaEduPlanVersionWeek.class)
                                .set(UsmaEduPlanVersionWeek.weekType().s(), value(theory))
                                .where(eq(property(UsmaEduPlanVersionWeek.term().code()), value(termCode)))
                                .where(eq(property(UsmaEduPlanVersionWeek.version()), value(version)))
                                .where(eq(property(UsmaEduPlanVersionWeek.weekType().code()), value(weekType.getCode())))
                                .createStatement(getSession())
                                .execute();

                        new DQLUpdateBuilder(UsmaEduPlanVersionWeekPart.class)
                                .set(UsmaEduPlanVersionWeekPart.weekType().s(), value(theory))
                                .where(eq(property(UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().term().code()), value(termCode)))
                                .where(eq(property(UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(version)))
                                .where(eq(property(UsmaEduPlanVersionWeekPart.weekType().code()), value(weekType.getCode())))
                                .createStatement(getSession())
                                .execute();

                        new DQLUpdateBuilder(EppEduPlanVersionWeekType.class)
                                .set(EppEduPlanVersionWeekType.weekType().s(), value(theory))
                                .where(eq(property(EppEduPlanVersionWeekType.term().code()), value(termCode)))
                                .where(eq(property(EppEduPlanVersionWeekType.eduPlanVersion()), value(version)))
                                .where(eq(property(EppEduPlanVersionWeekType.weekType().code()), value(weekType.getCode())))
                                .createStatement(getSession())
                                .execute();
                    }
                }
            }
        }
    }

    private class Model
    {
        private final OrgUnit topOrgUnit = TopOrgUnit.getInstance();
        private final EppRegistryStructure disciplineElementType = getCatalogItem(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_DISCIPLINE);

        private final Map<String, UsmaCompetence> indexCompetenceMap = new HashMap<>();
        private final Map<String, OrgUnit> orgUnitCodeMap = SafeMap.get(key -> {
            if (key == null) {
                return null;
            }
            EppTutorOrgUnit tutorOrgUnit = get(EppTutorOrgUnit.class, EppTutorOrgUnit.codeImtsa(), key);
            return tutorOrgUnit == null ? null : tutorOrgUnit.getOrgUnit();
        });

        private final Map<Character, EppWeekType> weekTypeMap = SafeMap.get(key -> {
            String weekTypeCode = IMTSA_WEEK_TYPE_MAP.get(key);
            if (weekTypeCode == null) {
                throw new IllegalArgumentException("Неизвестный тип недели: " + '"' + key + '"' + ".");
            }
            return getCatalogItem(EppWeekType.class, weekTypeCode);
        });

        private final Map<String, EppSkillGroup> eppSkillGroupMap = SafeMap.get(new SafeMap.Callback<String, EppSkillGroup>()
        {
            @Override
            public EppSkillGroup resolve(String key)
            {
                EppSkillGroup result = get(EppSkillGroup.class, EppSkillGroup.P_SHORT_TITLE, key);
                if (result == null)
                    if (ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
                        errors.add("Не найдена подходящая группа компетенций: «" + key + "».");

                    else
                        throw new IllegalStateException("Не найдена подходящая группа компетенций: «" + key + "».");

                return result;
            }
        });

        private final Map<Integer, Term> termMap = SafeMap.get(key -> get(Term.class, Term.intValue(), key));
        private final Map<String, EppRegistryStructure> practiceMap = SafeMap.get(key -> getCatalogItem(EppRegistryStructure.class, key));
        private final Map<ImtsaLoadType, ICatalogItem> loadTypeMap = SafeMap.get(key -> {
            if (key == null)
                return null;
            return ImtsaImportDAO.this.getCatalogItem(key.getTypeClass(), key.getCode());
        });

        private final List<String> errors;
        private final Map<String, EppPlanStructure> cyclesMap;
        private final Map<String, EppPlanStructure> partsMap;
        private final Map<Integer, Integer> coursePartsShiftMap;
        private final Map<Integer, Map<Integer, Term>> developGridMap;

        private Map<EppEpvStructureRow, Map<EppPlanStructure, EppEpvStructureRow>> partRowsMap;

        public Map<EppEpvStructureRow, Map<EppPlanStructure, EppEpvStructureRow>> getPartRowsMap()
        {
            return partRowsMap;
        }

        public void setPartRowsMap(Map<EppEpvStructureRow, Map<EppPlanStructure, EppEpvStructureRow>> partRowsMap)
        {
            this.partRowsMap = partRowsMap;
        }

        private EppEduPlanVersionBlock rootBlock; // основной блок данному; данный, если сам является основным

        private Map<String, String> codeIndexCompetenceMap;

        public Map<String, String> getCodeIndexCompetenceMap()
        {
            return codeIndexCompetenceMap;
        }

        public void setCodeIndexCompetenceMap(Map<String, String> codeIndexCompetenceMap)
        {
            this.codeIndexCompetenceMap = codeIndexCompetenceMap;
        }

        private EppEpvStructureRow currentCycle;

        public EppEpvStructureRow getCurrentCycle()
        {
            return currentCycle;
        }

        public void setCurrentCycle(EppEpvStructureRow currentCycle)
        {
            this.currentCycle = currentCycle;
        }

        public Model(Map<String, EppPlanStructure> cyclesMap, Map<String, EppPlanStructure> partsMap, Map<Integer, Integer> coursePartsShiftMap, Map<Integer, Map<Integer, Term>> developGridMap, EppEduPlanVersionBlock rootBlock, List<String> errors)
        {
            this.cyclesMap = Collections.unmodifiableMap(cyclesMap);
            this.partsMap = Collections.unmodifiableMap(partsMap);
            this.coursePartsShiftMap = Collections.unmodifiableMap(coursePartsShiftMap);
            this.developGridMap = Collections.unmodifiableMap(developGridMap);
            this.rootBlock = rootBlock;
            this.errors = errors;
        }

        private int competenceCode;

        private String competenceCode()
        {
            return IMPORTED_CODE_PREFIX + StringUtils.leftPad(String.valueOf(++competenceCode), 4, "0");
        }

        private final Map<Long, MutableInt> rowNumberMap = SafeMap.get(MutableInt.class);

        private int nextNumber(long id)
        {
            MutableInt number = rowNumberMap.get(id);
            number.increment();
            return number.intValue();
        }

        private Term getTermByCourseAndPart(Integer course, Integer part)
        {
            if (coursePartsShiftMap.containsKey(course))
            {
                part += coursePartsShiftMap.get(course);
            }

            return developGridMap.containsKey(course) ? developGridMap.get(course).get(part) : null;
        }
    }
}