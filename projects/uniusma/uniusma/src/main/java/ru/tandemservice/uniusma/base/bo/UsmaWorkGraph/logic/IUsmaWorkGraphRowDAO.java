/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic;

import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;

import java.util.Map;
import java.util.Set;

/**
 * @author Alexander Zhebko
 * @since 15.10.2013
 */
public interface IUsmaWorkGraphRowDAO extends ICommonDAO
{
    /**
     * Приготавливает данные для редактирования строки ГУПа.
     * @param rowId id строки
     * @param workGraph UEG
     * @param course курс
     * @param graphDataSource датасорс
     */
    public void prepareEditRow(Long rowId, UsmaWorkGraph workGraph, Course course, UsmaRangeSelectionWeekTypeListDataSource<UsmaWorkGraphRow> graphDataSource);

    /**
     * Сохраняет изменения в строке ГУПа.
     * @param rowId id строки
     * @param dataMap данные по типам недель
     * @param ranges данные разбиения курса
     */
    public void updateWorkGraphRow(Long rowId, Map<Long, Map<Integer, EppWeekType>> dataMap, int[] ranges);

    /**
     * Объединяте строки графика учебного процесса.
     * @param templateRowId id строки-шаблона
     * @param selectedIds id объединяемых строк
     */
    public void updateCombineRows(Long templateRowId, Set<Long> selectedIds);

    /**
     * Исключает УПв из общей строки графика учебного процесса.
     * @param excludeRowId id связи строки графика учебного процесса с УПв
     */
    public void updateExcludeRow(Long excludeRowId);
}