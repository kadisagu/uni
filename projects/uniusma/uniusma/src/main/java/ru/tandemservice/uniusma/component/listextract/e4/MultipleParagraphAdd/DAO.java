/* $Id$ */
package ru.tandemservice.uniusma.component.listextract.e4.MultipleParagraphAdd;



import ru.tandemservice.movestudent.entity.ExcludeStuListExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author Andrey Avetisov
 * @since 26.12.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.listextract.e4.MultipleParagraphAdd.DAO
{
    @Override
    protected void fillExtract(ExcludeStuListExtract extract, ru.tandemservice.movestudent.component.listextract.e4.MultipleParagraphAdd.Model model, Group group)
    {
        super.fillExtract(extract, model, group);
        extract.setExcludeDate(((Model)model).getExcludeDate());
    }
}
