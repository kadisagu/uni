package ru.tandemservice.uniusma.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состояние проверки УПв
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaEpvCheckStateGen extends EntityBase
 implements INaturalIdentifiable<UsmaEpvCheckStateGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState";
    public static final String ENTITY_NAME = "usmaEpvCheckState";
    public static final int VERSION_HASH = 1487030102;
    private static IEntityMeta ENTITY_META;

    public static final String L_VERSION = "version";
    public static final String P_CHECKED_BY_U_M_U = "checkedByUMU";
    public static final String P_CHECKED_BY_U_M_U_DATE = "checkedByUMUDate";
    public static final String P_CHECKED_BY_C_R_K = "checkedByCRK";
    public static final String P_CHECKED_BY_C_R_K_DATE = "checkedByCRKDate";
    public static final String P_CHECKED_BY_O_O_P = "checkedByOOP";
    public static final String P_CHECKED_BY_O_O_P_DATE = "checkedByOOPDate";
    public static final String P_ACCEPTED_DATE = "acceptedDate";
    public static final String P_ACCEPTED_BY = "acceptedBy";
    public static final String P_REJECTED_COMMENT = "rejectedComment";
    public static final String P_EDU_PLAN_ACCEPTED_DATE = "eduPlanAcceptedDate";
    public static final String P_EDU_PLAN_ACCEPTED_BY = "eduPlanAcceptedBy";

    private EppEduPlanVersion _version;     // УПв
    private boolean _checkedByUMU;     // Проверено УМУ
    private Date _checkedByUMUDate;     // Дата проверки УМУ
    private boolean _checkedByCRK;     // Проверено ЦРК
    private Date _checkedByCRKDate;     // Дата проверки ЦРК
    private boolean _checkedByOOP;     // Проверено ООП
    private Date _checkedByOOPDate;     // Дата проверки ООП
    private Date _acceptedDate;     // Дата согласования
    private String _acceptedBy;     // Кем согласовано
    private String _rejectedComment;     // Комментарий перевода в статус 'отклонено'
    private Date _eduPlanAcceptedDate;     // Дата согласования УП
    private String _eduPlanAcceptedBy;     // Кем согласовано УП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    /**
     * @param version УПв. Свойство не может быть null и должно быть уникальным.
     */
    public void setVersion(EppEduPlanVersion version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Проверено УМУ. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedByUMU()
    {
        return _checkedByUMU;
    }

    /**
     * @param checkedByUMU Проверено УМУ. Свойство не может быть null.
     */
    public void setCheckedByUMU(boolean checkedByUMU)
    {
        dirty(_checkedByUMU, checkedByUMU);
        _checkedByUMU = checkedByUMU;
    }

    /**
     * @return Дата проверки УМУ.
     */
    public Date getCheckedByUMUDate()
    {
        return _checkedByUMUDate;
    }

    /**
     * @param checkedByUMUDate Дата проверки УМУ.
     */
    public void setCheckedByUMUDate(Date checkedByUMUDate)
    {
        dirty(_checkedByUMUDate, checkedByUMUDate);
        _checkedByUMUDate = checkedByUMUDate;
    }

    /**
     * @return Проверено ЦРК. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedByCRK()
    {
        return _checkedByCRK;
    }

    /**
     * @param checkedByCRK Проверено ЦРК. Свойство не может быть null.
     */
    public void setCheckedByCRK(boolean checkedByCRK)
    {
        dirty(_checkedByCRK, checkedByCRK);
        _checkedByCRK = checkedByCRK;
    }

    /**
     * @return Дата проверки ЦРК.
     */
    public Date getCheckedByCRKDate()
    {
        return _checkedByCRKDate;
    }

    /**
     * @param checkedByCRKDate Дата проверки ЦРК.
     */
    public void setCheckedByCRKDate(Date checkedByCRKDate)
    {
        dirty(_checkedByCRKDate, checkedByCRKDate);
        _checkedByCRKDate = checkedByCRKDate;
    }

    /**
     * @return Проверено ООП. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedByOOP()
    {
        return _checkedByOOP;
    }

    /**
     * @param checkedByOOP Проверено ООП. Свойство не может быть null.
     */
    public void setCheckedByOOP(boolean checkedByOOP)
    {
        dirty(_checkedByOOP, checkedByOOP);
        _checkedByOOP = checkedByOOP;
    }

    /**
     * @return Дата проверки ООП.
     */
    public Date getCheckedByOOPDate()
    {
        return _checkedByOOPDate;
    }

    /**
     * @param checkedByOOPDate Дата проверки ООП.
     */
    public void setCheckedByOOPDate(Date checkedByOOPDate)
    {
        dirty(_checkedByOOPDate, checkedByOOPDate);
        _checkedByOOPDate = checkedByOOPDate;
    }

    /**
     * @return Дата согласования.
     */
    public Date getAcceptedDate()
    {
        return _acceptedDate;
    }

    /**
     * @param acceptedDate Дата согласования.
     */
    public void setAcceptedDate(Date acceptedDate)
    {
        dirty(_acceptedDate, acceptedDate);
        _acceptedDate = acceptedDate;
    }

    /**
     * @return Кем согласовано.
     */
    @Length(max=255)
    public String getAcceptedBy()
    {
        return _acceptedBy;
    }

    /**
     * @param acceptedBy Кем согласовано.
     */
    public void setAcceptedBy(String acceptedBy)
    {
        dirty(_acceptedBy, acceptedBy);
        _acceptedBy = acceptedBy;
    }

    /**
     * @return Комментарий перевода в статус 'отклонено'.
     */
    @Length(max=255)
    public String getRejectedComment()
    {
        return _rejectedComment;
    }

    /**
     * @param rejectedComment Комментарий перевода в статус 'отклонено'.
     */
    public void setRejectedComment(String rejectedComment)
    {
        dirty(_rejectedComment, rejectedComment);
        _rejectedComment = rejectedComment;
    }

    /**
     * @return Дата согласования УП.
     */
    public Date getEduPlanAcceptedDate()
    {
        return _eduPlanAcceptedDate;
    }

    /**
     * @param eduPlanAcceptedDate Дата согласования УП.
     */
    public void setEduPlanAcceptedDate(Date eduPlanAcceptedDate)
    {
        dirty(_eduPlanAcceptedDate, eduPlanAcceptedDate);
        _eduPlanAcceptedDate = eduPlanAcceptedDate;
    }

    /**
     * @return Кем согласовано УП.
     */
    @Length(max=255)
    public String getEduPlanAcceptedBy()
    {
        return _eduPlanAcceptedBy;
    }

    /**
     * @param eduPlanAcceptedBy Кем согласовано УП.
     */
    public void setEduPlanAcceptedBy(String eduPlanAcceptedBy)
    {
        dirty(_eduPlanAcceptedBy, eduPlanAcceptedBy);
        _eduPlanAcceptedBy = eduPlanAcceptedBy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaEpvCheckStateGen)
        {
            if (withNaturalIdProperties)
            {
                setVersion(((UsmaEpvCheckState)another).getVersion());
            }
            setCheckedByUMU(((UsmaEpvCheckState)another).isCheckedByUMU());
            setCheckedByUMUDate(((UsmaEpvCheckState)another).getCheckedByUMUDate());
            setCheckedByCRK(((UsmaEpvCheckState)another).isCheckedByCRK());
            setCheckedByCRKDate(((UsmaEpvCheckState)another).getCheckedByCRKDate());
            setCheckedByOOP(((UsmaEpvCheckState)another).isCheckedByOOP());
            setCheckedByOOPDate(((UsmaEpvCheckState)another).getCheckedByOOPDate());
            setAcceptedDate(((UsmaEpvCheckState)another).getAcceptedDate());
            setAcceptedBy(((UsmaEpvCheckState)another).getAcceptedBy());
            setRejectedComment(((UsmaEpvCheckState)another).getRejectedComment());
            setEduPlanAcceptedDate(((UsmaEpvCheckState)another).getEduPlanAcceptedDate());
            setEduPlanAcceptedBy(((UsmaEpvCheckState)another).getEduPlanAcceptedBy());
        }
    }

    public INaturalId<UsmaEpvCheckStateGen> getNaturalId()
    {
        return new NaturalId(getVersion());
    }

    public static class NaturalId extends NaturalIdBase<UsmaEpvCheckStateGen>
    {
        private static final String PROXY_NAME = "UsmaEpvCheckStateNaturalProxy";

        private Long _version;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersion version)
        {
            _version = ((IEntity) version).getId();
        }

        public Long getVersion()
        {
            return _version;
        }

        public void setVersion(Long version)
        {
            _version = version;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsmaEpvCheckStateGen.NaturalId) ) return false;

            UsmaEpvCheckStateGen.NaturalId that = (NaturalId) o;

            if( !equals(getVersion(), that.getVersion()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getVersion());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getVersion());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaEpvCheckStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaEpvCheckState.class;
        }

        public T newInstance()
        {
            return (T) new UsmaEpvCheckState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "checkedByUMU":
                    return obj.isCheckedByUMU();
                case "checkedByUMUDate":
                    return obj.getCheckedByUMUDate();
                case "checkedByCRK":
                    return obj.isCheckedByCRK();
                case "checkedByCRKDate":
                    return obj.getCheckedByCRKDate();
                case "checkedByOOP":
                    return obj.isCheckedByOOP();
                case "checkedByOOPDate":
                    return obj.getCheckedByOOPDate();
                case "acceptedDate":
                    return obj.getAcceptedDate();
                case "acceptedBy":
                    return obj.getAcceptedBy();
                case "rejectedComment":
                    return obj.getRejectedComment();
                case "eduPlanAcceptedDate":
                    return obj.getEduPlanAcceptedDate();
                case "eduPlanAcceptedBy":
                    return obj.getEduPlanAcceptedBy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((EppEduPlanVersion) value);
                    return;
                case "checkedByUMU":
                    obj.setCheckedByUMU((Boolean) value);
                    return;
                case "checkedByUMUDate":
                    obj.setCheckedByUMUDate((Date) value);
                    return;
                case "checkedByCRK":
                    obj.setCheckedByCRK((Boolean) value);
                    return;
                case "checkedByCRKDate":
                    obj.setCheckedByCRKDate((Date) value);
                    return;
                case "checkedByOOP":
                    obj.setCheckedByOOP((Boolean) value);
                    return;
                case "checkedByOOPDate":
                    obj.setCheckedByOOPDate((Date) value);
                    return;
                case "acceptedDate":
                    obj.setAcceptedDate((Date) value);
                    return;
                case "acceptedBy":
                    obj.setAcceptedBy((String) value);
                    return;
                case "rejectedComment":
                    obj.setRejectedComment((String) value);
                    return;
                case "eduPlanAcceptedDate":
                    obj.setEduPlanAcceptedDate((Date) value);
                    return;
                case "eduPlanAcceptedBy":
                    obj.setEduPlanAcceptedBy((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "checkedByUMU":
                        return true;
                case "checkedByUMUDate":
                        return true;
                case "checkedByCRK":
                        return true;
                case "checkedByCRKDate":
                        return true;
                case "checkedByOOP":
                        return true;
                case "checkedByOOPDate":
                        return true;
                case "acceptedDate":
                        return true;
                case "acceptedBy":
                        return true;
                case "rejectedComment":
                        return true;
                case "eduPlanAcceptedDate":
                        return true;
                case "eduPlanAcceptedBy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "checkedByUMU":
                    return true;
                case "checkedByUMUDate":
                    return true;
                case "checkedByCRK":
                    return true;
                case "checkedByCRKDate":
                    return true;
                case "checkedByOOP":
                    return true;
                case "checkedByOOPDate":
                    return true;
                case "acceptedDate":
                    return true;
                case "acceptedBy":
                    return true;
                case "rejectedComment":
                    return true;
                case "eduPlanAcceptedDate":
                    return true;
                case "eduPlanAcceptedBy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return EppEduPlanVersion.class;
                case "checkedByUMU":
                    return Boolean.class;
                case "checkedByUMUDate":
                    return Date.class;
                case "checkedByCRK":
                    return Boolean.class;
                case "checkedByCRKDate":
                    return Date.class;
                case "checkedByOOP":
                    return Boolean.class;
                case "checkedByOOPDate":
                    return Date.class;
                case "acceptedDate":
                    return Date.class;
                case "acceptedBy":
                    return String.class;
                case "rejectedComment":
                    return String.class;
                case "eduPlanAcceptedDate":
                    return Date.class;
                case "eduPlanAcceptedBy":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaEpvCheckState> _dslPath = new Path<UsmaEpvCheckState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaEpvCheckState");
    }
            

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Проверено УМУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#isCheckedByUMU()
     */
    public static PropertyPath<Boolean> checkedByUMU()
    {
        return _dslPath.checkedByUMU();
    }

    /**
     * @return Дата проверки УМУ.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getCheckedByUMUDate()
     */
    public static PropertyPath<Date> checkedByUMUDate()
    {
        return _dslPath.checkedByUMUDate();
    }

    /**
     * @return Проверено ЦРК. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#isCheckedByCRK()
     */
    public static PropertyPath<Boolean> checkedByCRK()
    {
        return _dslPath.checkedByCRK();
    }

    /**
     * @return Дата проверки ЦРК.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getCheckedByCRKDate()
     */
    public static PropertyPath<Date> checkedByCRKDate()
    {
        return _dslPath.checkedByCRKDate();
    }

    /**
     * @return Проверено ООП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#isCheckedByOOP()
     */
    public static PropertyPath<Boolean> checkedByOOP()
    {
        return _dslPath.checkedByOOP();
    }

    /**
     * @return Дата проверки ООП.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getCheckedByOOPDate()
     */
    public static PropertyPath<Date> checkedByOOPDate()
    {
        return _dslPath.checkedByOOPDate();
    }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getAcceptedDate()
     */
    public static PropertyPath<Date> acceptedDate()
    {
        return _dslPath.acceptedDate();
    }

    /**
     * @return Кем согласовано.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getAcceptedBy()
     */
    public static PropertyPath<String> acceptedBy()
    {
        return _dslPath.acceptedBy();
    }

    /**
     * @return Комментарий перевода в статус 'отклонено'.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getRejectedComment()
     */
    public static PropertyPath<String> rejectedComment()
    {
        return _dslPath.rejectedComment();
    }

    /**
     * @return Дата согласования УП.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getEduPlanAcceptedDate()
     */
    public static PropertyPath<Date> eduPlanAcceptedDate()
    {
        return _dslPath.eduPlanAcceptedDate();
    }

    /**
     * @return Кем согласовано УП.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getEduPlanAcceptedBy()
     */
    public static PropertyPath<String> eduPlanAcceptedBy()
    {
        return _dslPath.eduPlanAcceptedBy();
    }

    public static class Path<E extends UsmaEpvCheckState> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _version;
        private PropertyPath<Boolean> _checkedByUMU;
        private PropertyPath<Date> _checkedByUMUDate;
        private PropertyPath<Boolean> _checkedByCRK;
        private PropertyPath<Date> _checkedByCRKDate;
        private PropertyPath<Boolean> _checkedByOOP;
        private PropertyPath<Date> _checkedByOOPDate;
        private PropertyPath<Date> _acceptedDate;
        private PropertyPath<String> _acceptedBy;
        private PropertyPath<String> _rejectedComment;
        private PropertyPath<Date> _eduPlanAcceptedDate;
        private PropertyPath<String> _eduPlanAcceptedBy;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> version()
        {
            if(_version == null )
                _version = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_VERSION, this);
            return _version;
        }

    /**
     * @return Проверено УМУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#isCheckedByUMU()
     */
        public PropertyPath<Boolean> checkedByUMU()
        {
            if(_checkedByUMU == null )
                _checkedByUMU = new PropertyPath<Boolean>(UsmaEpvCheckStateGen.P_CHECKED_BY_U_M_U, this);
            return _checkedByUMU;
        }

    /**
     * @return Дата проверки УМУ.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getCheckedByUMUDate()
     */
        public PropertyPath<Date> checkedByUMUDate()
        {
            if(_checkedByUMUDate == null )
                _checkedByUMUDate = new PropertyPath<Date>(UsmaEpvCheckStateGen.P_CHECKED_BY_U_M_U_DATE, this);
            return _checkedByUMUDate;
        }

    /**
     * @return Проверено ЦРК. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#isCheckedByCRK()
     */
        public PropertyPath<Boolean> checkedByCRK()
        {
            if(_checkedByCRK == null )
                _checkedByCRK = new PropertyPath<Boolean>(UsmaEpvCheckStateGen.P_CHECKED_BY_C_R_K, this);
            return _checkedByCRK;
        }

    /**
     * @return Дата проверки ЦРК.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getCheckedByCRKDate()
     */
        public PropertyPath<Date> checkedByCRKDate()
        {
            if(_checkedByCRKDate == null )
                _checkedByCRKDate = new PropertyPath<Date>(UsmaEpvCheckStateGen.P_CHECKED_BY_C_R_K_DATE, this);
            return _checkedByCRKDate;
        }

    /**
     * @return Проверено ООП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#isCheckedByOOP()
     */
        public PropertyPath<Boolean> checkedByOOP()
        {
            if(_checkedByOOP == null )
                _checkedByOOP = new PropertyPath<Boolean>(UsmaEpvCheckStateGen.P_CHECKED_BY_O_O_P, this);
            return _checkedByOOP;
        }

    /**
     * @return Дата проверки ООП.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getCheckedByOOPDate()
     */
        public PropertyPath<Date> checkedByOOPDate()
        {
            if(_checkedByOOPDate == null )
                _checkedByOOPDate = new PropertyPath<Date>(UsmaEpvCheckStateGen.P_CHECKED_BY_O_O_P_DATE, this);
            return _checkedByOOPDate;
        }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getAcceptedDate()
     */
        public PropertyPath<Date> acceptedDate()
        {
            if(_acceptedDate == null )
                _acceptedDate = new PropertyPath<Date>(UsmaEpvCheckStateGen.P_ACCEPTED_DATE, this);
            return _acceptedDate;
        }

    /**
     * @return Кем согласовано.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getAcceptedBy()
     */
        public PropertyPath<String> acceptedBy()
        {
            if(_acceptedBy == null )
                _acceptedBy = new PropertyPath<String>(UsmaEpvCheckStateGen.P_ACCEPTED_BY, this);
            return _acceptedBy;
        }

    /**
     * @return Комментарий перевода в статус 'отклонено'.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getRejectedComment()
     */
        public PropertyPath<String> rejectedComment()
        {
            if(_rejectedComment == null )
                _rejectedComment = new PropertyPath<String>(UsmaEpvCheckStateGen.P_REJECTED_COMMENT, this);
            return _rejectedComment;
        }

    /**
     * @return Дата согласования УП.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getEduPlanAcceptedDate()
     */
        public PropertyPath<Date> eduPlanAcceptedDate()
        {
            if(_eduPlanAcceptedDate == null )
                _eduPlanAcceptedDate = new PropertyPath<Date>(UsmaEpvCheckStateGen.P_EDU_PLAN_ACCEPTED_DATE, this);
            return _eduPlanAcceptedDate;
        }

    /**
     * @return Кем согласовано УП.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState#getEduPlanAcceptedBy()
     */
        public PropertyPath<String> eduPlanAcceptedBy()
        {
            if(_eduPlanAcceptedBy == null )
                _eduPlanAcceptedBy = new PropertyPath<String>(UsmaEpvCheckStateGen.P_EDU_PLAN_ACCEPTED_BY, this);
            return _eduPlanAcceptedBy;
        }

        public Class getEntityClass()
        {
            return UsmaEpvCheckState.class;
        }

        public String getEntityName()
        {
            return "usmaEpvCheckState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
