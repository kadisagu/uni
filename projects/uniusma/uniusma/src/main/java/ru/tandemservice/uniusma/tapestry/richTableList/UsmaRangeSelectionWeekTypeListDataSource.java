/**
 *$Id$
 */
package ru.tandemservice.uniusma.tapestry.richTableList;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public class UsmaRangeSelectionWeekTypeListDataSource<T extends IEntity> extends RangeSelectionWeekTypeListDataSource<T>
{
    private Map<Long, Map<Long, Map<Integer, EppWeekType>>> _fullDataMap; // мап значений таблицы
    private boolean _splittable;

    public UsmaRangeSelectionWeekTypeListDataSource(AbstractListDataSource dataSource)
    {
        super(dataSource);
    }

    public Map<Long, Map<Long, Map<Integer, EppWeekType>>> getFullDataMap()
    {
        return _fullDataMap;
    }

    public void setFullDataMap(Map<Long, Map<Long, Map<Integer, EppWeekType>>> fullDataMap)
    {
        _fullDataMap = fullDataMap;
    }

    public boolean isSplittable()
    {
        return _splittable;
    }

    public void setSplittable(boolean splittable)
    {
        _splittable = splittable;
    }
}