/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.entrant.EntrantRequestTab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfText;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;

/**
 * @author oleyba
 * @since 09.06.2009
 */
public class DocumentListAndReceiptPrint extends UniBaseDao implements IDocumentListAndReceiptPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long object)
    {
        RtfDocument document = new RtfReader().read(template);
        EntrantRequest entrantRequest = getNotNull(object);
        new RtfInjectModifier()
                .put("fio", entrantRequest.getEntrant().getPerson().getFullFio())
                .put("entrantNumber", entrantRequest.getEntrant().getPersonalNumber())
                .put("requestNumber", entrantRequest.getStringNumber())
                .modify(document);
        List<String[]> tableData = new ArrayList<String[]>();
        final List<String> titleList = new ArrayList<String>();
        MQBuilder builder = new MQBuilder(EntrantEnrollmentDocument.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EntrantEnrollmentDocument.L_ENTRANT_REQUEST, entrantRequest));
        builder.addOrder("d", EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT + "." + EnrollmentDocument.P_PRIORITY);
        int num = 1;
        List<EntrantEnrollmentDocument> documents = builder.<EntrantEnrollmentDocument>getResultList(getSession());

        Map<EnrollmentDocument, UsedEnrollmentDocument> settingsMap = new HashMap<EnrollmentDocument, UsedEnrollmentDocument>();
        if (!documents.isEmpty())
        for (UsedEnrollmentDocument usedEnrollmentDocument : getList(UsedEnrollmentDocument.class, UsedEnrollmentDocument.enrollmentCampaign().s(), documents.get(0).getEntrantRequest().getEntrant().getEnrollmentCampaign()))
            settingsMap.put(usedEnrollmentDocument.getEnrollmentDocument(), usedEnrollmentDocument);

        for (EntrantEnrollmentDocument doc : documents)
        {
            UsedEnrollmentDocument settings = settingsMap.get(doc.getEnrollmentDocument());
            String title = doc.getEnrollmentDocument().getTitle();
            if (null != settings && settings.isPrintEducationDocumentInfo())
            {
                title = title + "\\par" + "№";
                // последний документ об образовании
                {
                    final PersonEduInstitution eduInstitution = entrantRequest.getEntrant().getPerson().getPersonEduInstitution();
                    if (eduInstitution != null)
                    {
                        if (null != eduInstitution.getSeria())
                            title = title + " " + eduInstitution.getSeria();
                        if (null != eduInstitution.getNumber())
                            title = title + " " + eduInstitution.getNumber();
                    }
                }
            }
            titleList.add(title);
            String copy = doc.isCopy() ? "Копия" : "Оригинал";
            tableData.add(new String[]{String.valueOf(num++), "", String.valueOf(doc.getAmount()), copy});
        }
        for (int i=0; i<3; i++)
            tableData.add(new String[]{String.valueOf(num+i)});
        IRtfRowIntercepter intercepter = new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = 0; i < titleList.size(); i++)
                {
                    RtfRow row = newRowList.get(startIndex + i);
                    // вставляем названия документов в ячейки простым текстом,
                    // чтобы не экранировались переводы строки
                    String content = titleList.get(i);
                    IRtfText text = RtfBean.getElementFactory().createRtfText(content);
                    text.setRaw(true);
                    row.getCellList().get(1).getElementList().add(text);
                }
            }
        };
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", tableData.toArray(new String[tableData.size()][]));
        tableModifier.put("T1", intercepter);
        tableModifier.put("T2", tableData.toArray(new String[tableData.size()][]));
        tableModifier.put("T2", intercepter);
        tableModifier.modify(document);
        return document;
    }
}
