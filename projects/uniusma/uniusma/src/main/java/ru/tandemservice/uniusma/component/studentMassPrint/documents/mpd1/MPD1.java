/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.component.documents.d1.Add.DAO;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.AbstractStudentMassPrint;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1.Add.Model;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class MPD1 extends AbstractStudentMassPrint<Model>
{
    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void initModel4Student(Student student, int documentNumber, Model model)
    {
        super.initModel4Student(student, documentNumber, model);

        DAO dao = new DAO();
        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());
        model.setCourse(student.getCourse());
    }

}
