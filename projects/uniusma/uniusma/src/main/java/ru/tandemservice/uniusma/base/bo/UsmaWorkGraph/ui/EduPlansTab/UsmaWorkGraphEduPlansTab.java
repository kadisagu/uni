/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.EduPlansTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.UsmaWorkGraphEpvDSHandler;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.util.UsmaWorkGraphEpvWrapper;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Configuration
public class UsmaWorkGraphEduPlansTab extends BusinessComponentManager
{
    public static final String EDU_PLAN_DS = "eduPlanDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_PLAN_DS, eduPlanDSColumns(), eduPlanDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduPlanDSColumns()
    {
        return columnListExtPointBuilder(EDU_PLAN_DS)
                .addColumn(textColumn("eduPlanTitle", UsmaWorkGraphEpvWrapper.EDU_PLAN_TITLE))
                .addColumn(textColumn("eduPlanVersionTitle", UsmaWorkGraphEpvWrapper.EDU_PLAN_VERSION_TITLE))
                .addColumn(textColumn("courses", UsmaWorkGraphEpvWrapper.COURSES))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduPlanDSHandler()
    {
        return new UsmaWorkGraphEpvDSHandler(getName());
    }
}