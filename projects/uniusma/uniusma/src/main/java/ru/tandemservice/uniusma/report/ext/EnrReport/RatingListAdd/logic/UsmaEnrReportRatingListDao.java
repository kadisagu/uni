/* $Id: $ */
package ru.tandemservice.uniusma.report.ext.EnrReport.RatingListAdd.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.EnrReportRatingListDao;

/**
 * @author Stanislav Shibarshin
 * @since 01.09.2016
 */
public class UsmaEnrReportRatingListDao extends EnrReportRatingListDao
{

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addCustomLabelForHeader(RtfDocument document, RtfInjectModifier modifier, EnrProgramSetOrgUnit programSetOrgUnit)
    {
        String programTraitValue = modifier.getStringValue("programTrait");
        if (StringUtils.isNotEmpty(programTraitValue))
        {
            if (programTraitValue.contains("|"))
            {
                String newValue = StringUtils.split(programTraitValue, "|")[0].trim();
                modifier.put("programTrait", newValue);
            }
        }
        String eduProgramValue = modifier.getStringValue("eduProgram");
        if (StringUtils.isNotEmpty(eduProgramValue))
        {
            if (eduProgramValue.contains("|"))
            {
                // Получаем всё до "|" и закрываем скобку.
                String newValue = eduProgramValue.substring(0, eduProgramValue.indexOf("|")).trim() + ")";
                modifier.put("eduProgram", newValue);
            }
        }

    }
}