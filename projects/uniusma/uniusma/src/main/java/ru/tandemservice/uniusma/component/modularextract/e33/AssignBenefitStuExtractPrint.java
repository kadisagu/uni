/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.modularextract.e33;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AssignBenefitStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.03.2010
 */
public class AssignBenefitStuExtractPrint implements IPrintFormCreator<AssignBenefitStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AssignBenefitStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        int number = 1;
        List<String> tagsToDel = new ArrayList<>();

        if(extract.isAssignBenefit())
        {
            StringBuilder benefitBuilder = new StringBuilder(String.valueOf(++number));
            benefitBuilder.append(". ").append(modifier.getStringValue("Student_D"));
            benefitBuilder.append(" ").append(modifier.getStringValue("course")).append(" курса ");
            benefitBuilder.append(modifier.getStringValue("orgUnit_G")).append(" ");
            benefitBuilder.append(modifier.getStringValue("developForm_GF")).append(" формы обучения ");
            benefitBuilder.append(modifier.getStringValue("compensationTypeStr_G_Alt")).append(" ");
            benefitBuilder.append(modifier.getStringValue("fio")).append(" ").append(modifier.getStringValue("birthDate"));
            benefitBuilder.append(" дата рождения, назначить выплату ежемесячного пособия ");
            benefitBuilder.append(extract.getBenefitType()).append(" c ");
            benefitBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBenefitBeginDate())).append(" г. по ");
            benefitBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBenefitEndDate())).append(" г.");
            modifier.put("assignBenefit", benefitBuilder.toString());
        }
        else
        {
            tagsToDel.add("assignBenefit");
        }
        
        if(extract.isAllowIndependentSchedule())
        {
            StringBuilder indepBuilder = new StringBuilder(String.valueOf(++number));
            indepBuilder.append(". ").append(modifier.getStringValue("Student_D")).append(" ");
            indepBuilder.append(modifier.getStringValue("fio")).append(" предоставить свободное посещение занятий в связи ");
            indepBuilder.append(extract.getReasonStr()).append(" с ");
            indepBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getIndepSchedBeginDate())).append(" г. по ");
            indepBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getIndepSchedEndDate())).append(" г.");
            modifier.put("allowIndependentSchedule", indepBuilder.toString());
        }
        else
        {
            tagsToDel.add("allowIndependentSchedule");
        }
        
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        
        modifier.modify(document);
        return document;
    }
}