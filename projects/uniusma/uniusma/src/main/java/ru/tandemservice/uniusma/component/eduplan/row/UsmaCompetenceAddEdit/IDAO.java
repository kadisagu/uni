/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.row.UsmaCompetenceAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Привязывает к строке компетенции, которые были привязаны к ее дисциплине, но не было у самой строки
     * @param model модель
     */
    public void updateCompetence(Model model);
}