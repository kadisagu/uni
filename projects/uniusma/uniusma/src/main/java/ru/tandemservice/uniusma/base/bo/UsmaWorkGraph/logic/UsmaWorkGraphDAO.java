/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.gen.DevelopGridTermGen;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.List.UsmaWorkGraphList;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.WeeksTab.UsmaWorkGraphWeeksTab;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeek;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart;
import ru.tandemservice.uniusma.entity.eduPlan.*;
import ru.tandemservice.uniusma.entity.eduPlan.gen.UsmaWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
public class UsmaWorkGraphDAO extends CommonDAO implements IUsmaWorkGraphDAO, INeedPersistenceSupport
{
    public static final EntityComparator<EduProgramSubject> PROGRAM_SUBJECT_COMPARATOR = new EntityComparator<>(new EntityOrder(EduProgramSubject.P_TITLE_WITH_CODE));

    @Override
    public void setWorkGraphListFilterDefaultValues(IUISettings settings)
    {
        settings.set(UsmaWorkGraphList.PUPNAG, getNearestYear());
    }

    @Override
    public void setWorkGraphWeeksFilterDefaultValues(IUISettings settings, Long workGraphId)
    {
        settings.set(UsmaWorkGraphWeeksTab.COURSE, getMinWorkGraphCourse(workGraphId));
    }

    private EppYearEducationProcess getNearestYear()
    {
        return new DQLSelectBuilder()
                .fromEntity(EppYearEducationProcess.class, "y")
                .where(ge(
                        property("y", EppYearEducationProcess.educationYear().intValue()),
                        new DQLSelectBuilder()
                                .fromEntity(EducationYear.class, "ey")
                                .column(property("ey", EducationYear.intValue()))
                                .where(eq(property("ey", EducationYear.current()), value(Boolean.TRUE)))
                                .buildQuery()))
                .order(property("y", EppYearEducationProcess.educationYear().intValue()))
                .createStatement(getSession())
                .setMaxResults(1).uniqueResult();
    }

    private Course getMinWorkGraphCourse(Long workGraphId)
    {
        return new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRow.class, "row")
                .column(property("row", UsmaWorkGraphRow.course()))
                .where(eq(property("row", UsmaWorkGraphRow.graph().id()), value(workGraphId)))
                .order(property("row", UsmaWorkGraphRow.course().intValue()))
                .createStatement(getSession())
                .setMaxResults(1).uniqueResult();
    }

    @Override
    public Map<PairKey<Long, String>, Boolean> getWorkGraphEduPlanVersionCourses(Long workGraphId)
    {
        Map<PairKey<Long, String>, Boolean> result = new HashMap<>();

        UsmaWorkGraph workGraph = get(UsmaWorkGraph.class, workGraphId);
        List<Long> epvIds = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, "v")
                .column(property("v.id"))
                .where(and(
                    eq(property("v", EppEduPlanVersion.eduPlan().programForm()), value(workGraph.getDevelopForm().getProgramForm())),
                    eq(property("v", EppEduPlanVersion.eduPlan().developCondition()), value(workGraph.getDevelopCondition())),
                    workGraph.getDevelopTech().getProgramTrait() == null ?
                        isNull(property("v", EppEduPlanVersion.eduPlan().programTrait())) :
                        eq(property("v", EppEduPlanVersion.eduPlan().programTrait()), value(workGraph.getDevelopTech().getProgramTrait())),
                    eq(property("v", EppEduPlanVersion.eduPlan().developGrid()), value(workGraph.getDevelopGrid()))
                ))
                .createStatement(getSession())
                .list();

        for (Course course: getWorkGraphCourses(workGraphId))
        {
            String courseCode = course.getCode();
            for (Long versionId: epvIds)
            {

                result.put(PairKey.create(versionId, courseCode), false);
            }
        }

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRow2EduPlan.class, "wgrep")
                .column(property("wgrep", UsmaWorkGraphRow2EduPlan.eduPlanVersion().id()))
                .column(property("wgrep", UsmaWorkGraphRow2EduPlan.row().course().code()))
                .where(eq(property("wgrep", UsmaWorkGraphRow2EduPlan.row().graph().id()), value(workGraphId)));

        for (Object[] row: builder.createStatement(getSession()).<Object[]>list())
        {
            result.put(PairKey.create((Long) row[0], (String) row[1]), true);
        }

        return result;
    }

    @Override
    public void saveWorkGraphEduPlanVersionCourses(Long workGraphId, Map<Long, Set<String>> versionCourseIdsMap)
    {
        UsmaWorkGraph workGraph = get(UsmaWorkGraph.class, workGraphId);
        int partition = workGraph.getPartitionType().getPartsNumber();

        Map<String, Course> courseMap = SafeMap.get(new SafeMap.Callback<String, Course>(){ @Override public Course resolve(String key){ return UsmaWorkGraphDAO.this.getByNaturalId(new Course.NaturalId(key)); }});
        Map<String, Term> termMap = SafeMap.get(new SafeMap.Callback<String, Term>(){ @Override public Term resolve(String key) { return UsmaWorkGraphDAO.this.getByNaturalId(new Term.NaturalId(key)); }});
        Map<String, EppWeekType> weekTypeMap = SafeMap.get(new SafeMap.Callback<String, EppWeekType>(){ @Override public EppWeekType resolve(String key){ return UsmaWorkGraphDAO.this.getByNaturalId(new EppWeekType.NaturalId(key)); }});

        List<Long> versionIds = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRow2EduPlan.class, "wgrep")
                .column(property("wgrep", UsmaWorkGraphRow2EduPlan.eduPlanVersion().id()))
                .where(eq(property("wgrep", UsmaWorkGraphRow2EduPlan.row().graph().id()), value(workGraphId)))
                .createStatement(getSession()).list();

        versionIds.addAll(versionCourseIdsMap.keySet());
        for (Long versionId: versionIds)
        {
            Set<String> courseCodes = versionCourseIdsMap.get(versionId);

            EppEduPlanVersion version = get(EppEduPlanVersion.class, versionId);

            List<String> oldCourseIds = new DQLSelectBuilder()
                    .fromEntity(UsmaWorkGraphRow2EduPlan.class, "wgrep")
                    .column(property("wgrep", UsmaWorkGraphRow2EduPlan.row().course().code()))
                    .where(eq(property("wgrep", UsmaWorkGraphRow2EduPlan.eduPlanVersion().id()), value(versionId)))
                    .where(eq(property("wgrep", UsmaWorkGraphRow2EduPlan.row().graph().id()), value(workGraphId)))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(getSession()).list();

            Collection<String> toSave = CollectionUtils.subtract(courseCodes, oldCourseIds);
            Collection<String> toDel = CollectionUtils.subtract(oldCourseIds, courseCodes);

            for (String courseCode: toSave)
            {
                Course course = courseMap.get(courseCode);
                UsmaWorkGraphRow workGraphRow = new UsmaWorkGraphRow(workGraph, course);

                save(workGraphRow);

                UsmaWorkGraphRow2EduPlan row2EduPlanRel = new UsmaWorkGraphRow2EduPlan(workGraphRow, version);

                save(row2EduPlanRel);

                Map<Integer, PairKey<String, Map<Integer, String>>> weekTypeSourceMap = getEpvSchedule(versionId, courseCode);
                for (Map.Entry<Integer, PairKey<String, Map<Integer, String>>> weekEntry: weekTypeSourceMap.entrySet())
                {
                    Integer weekNumber = weekEntry.getKey();
                    PairKey<String, Map<Integer, String>> weekKey = weekEntry.getValue();
                    String termCode = weekKey.getFirst();
                    Map<Integer, String> weekPartTypeMap = weekKey.getSecond();

                    Term term = termMap.get(termCode);
                    UsmaWorkGraphRowWeek workGraphRowWeek = new UsmaWorkGraphRowWeek(workGraphRow, weekNumber);
                    workGraphRowWeek.setTerm(term);
                    EppWeekType type = null;
                    if (weekPartTypeMap.size() == 1)
                    {
                        type = weekTypeMap.get(weekPartTypeMap.get(1));

                    } else if (weekPartTypeMap.size() != partition)
                    {
                        int et = -1;
                        String code = null;
                        for (Map.Entry<String, Integer> entry: CollectionUtils.getCardinalityMap(weekPartTypeMap.values()).entrySet())
                        {
                            if (et < entry.getValue()) {
                                code = entry.getKey();
                                et = entry.getValue();
                            }
                        }

                        type = weekTypeMap.get(code);

                    }

                    workGraphRowWeek.setType(type);
                    save(workGraphRowWeek);
                    if (type == null)
                    {
                        for (Map.Entry<Integer, String> partEntry: weekPartTypeMap.entrySet())
                        {
                            Integer part = partEntry.getKey();
                            String weekTypeCode = partEntry.getValue();

                            UsmaWorkGraphRowWeekPart workGraphRowWeekPart = new UsmaWorkGraphRowWeekPart(workGraphRowWeek, part);
                            workGraphRowWeekPart.setType(weekTypeMap.get(weekTypeCode));

                            save(workGraphRowWeekPart);
                        }
                    }
                }
            }

            new DQLDeleteBuilder(UsmaWorkGraphRow2EduPlan.class)
                    .where(eq(property(UsmaWorkGraphRow2EduPlan.eduPlanVersion().id()), value(versionId)))
                    .where(eq(property(UsmaWorkGraphRow2EduPlan.row().graph().id()), value(workGraphId)))
                    .where(in(property(UsmaWorkGraphRow2EduPlan.row().course().code()), toDel))
                    .createStatement(getSession()).execute();
        }

        getSession().flush();
        getSession().clear();

       List<Long> lonelyRowIds = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRow.class, "wgr")
                .where(eq(property("wgr", UsmaWorkGraphRow.graph().id()), value(workGraphId)))
                .where(notIn(
                        property("wgr", UsmaWorkGraphRow.id()),
                        new DQLSelectBuilder()
                            .fromEntity(UsmaWorkGraphRow2EduPlan.class, "wgrep")
                            .column(property("wgrep", UsmaWorkGraphRow2EduPlan.row().id()))
                            .where(eq(property("wgrep", UsmaWorkGraphRow2EduPlan.row().graph().id()), value(workGraphId)))
                            .buildQuery()))
                .column(property("wgr.id"))
                .createStatement(getSession())
                .list();

        new DQLDeleteBuilder(UsmaWorkGraphRow.class).where(in(property(UsmaWorkGraphRow.id()), lonelyRowIds)).createStatement(getSession()).execute();
    }

    private Map<Integer, PairKey<String, Map<Integer, String>>> getEpvSchedule(Long versionId, String courseCode)
    {
        IDQLSelectableQuery weekQuery = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeek.class, "wq")
                .column(property("wq", UsmaEduPlanVersionWeek.id()))
                .where(eq(property("wq", UsmaEduPlanVersionWeek.course().code()), value(courseCode)))
                .where(eq(property("wq", UsmaEduPlanVersionWeek.version().id()), value(versionId)))
                .buildQuery();

        DQLSelectBuilder weekPartBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeekPart.class, "wp")
           /*0*/.column(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().id()))
           /*1*/.column(property("wp", UsmaEduPlanVersionWeekPart.partitionElementNumber()))
           /*2*/.column(property("wp", UsmaEduPlanVersionWeekPart.weekType().code()))
                .where(in(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().id()), weekQuery));

        Map<Long, Map<Integer, String>> weekPartTypeMap = SafeMap.get(HashMap.class);
        for (Object[] row: weekPartBuilder.createStatement(getSession()).<Object[]>list())
        {
            weekPartTypeMap.get((Long)row[0]).put((Integer) row[1], (String) row[2]);
        }

        DQLSelectBuilder weekBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeek.class, "w")
                .joinPath(DQLJoinType.left, UsmaEduPlanVersionWeek.weekType().fromAlias("w"), "wt")
           /*0*/.column(property("w", UsmaEduPlanVersionWeek.id()))
           /*1*/.column(property("w", UsmaEduPlanVersionWeek.week().number()))
           /*2*/.column(property("w", UsmaEduPlanVersionWeek.term().code()))
           /*3*/.column(property("wt", EppWeekType.code()))
                .where(in(property("w", UsmaEduPlanVersionWeek.id()), weekQuery));

        Map<Integer, PairKey<String, Map<Integer, String>>> result = new TreeMap<>();
        for (Object[] row: weekBuilder.createStatement(getSession()).<Object[]>list())
        {
            Long weekId = (Long) row[0];
            Integer weekNumber = (Integer) row[1];
            String termCode = (String) row[2];
            String weekTypeCode = (String) row[3];

            result.put(weekNumber, PairKey.create(termCode, weekTypeCode == null ? weekPartTypeMap.get(weekId) : Collections.singletonMap(1, weekTypeCode)));
        }

        return result;
    }

    @Override
    public List<Course> getWorkGraphCourses(Long workGraphId)
    {
        return new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .column(property("dgt", DevelopGridTerm.course()))
                .joinEntity("dgt", DQLJoinType.inner, UsmaWorkGraph.class, "wg", eq(property("dgt", DevelopGridTerm.developGrid()), property("wg", UsmaWorkGraph.developGrid())))
                .where(eq(property("wg", UsmaWorkGraph.id()), value(workGraphId)))
                .order(property("dgt", DevelopGridTerm.course().intValue()))
                .predicate(DQLPredicateType.distinct)
                .createStatement(getSession()).list();
    }

    @Override
    public Map<EduProgramSubject, UsmaRangeSelectionWeekTypeListDataSource> getWorkGraphEduLevelDataSourceMap(UsmaWorkGraph workGraph, Collection<Course> courses, EduProgramSubject programSubject)
    {
        // rowId -> массив точек
        // rowId -> логическая карта строки
        // заполняем эти два важных массива согласно данным из базы
        final Map<Long, int[]> row2points = new HashMap<>();
        final Map<Long, int[]> row2data = new HashMap<>();
        prepareRowDataPoints(this.getSession(), workGraph, row2points, row2data);

        // создаем все RangeSelectionListDataSource
        final Map<EduProgramSubject, UsmaRangeSelectionWeekTypeListDataSource> id2dataSource = new TreeMap<>(PROGRAM_SUBJECT_COMPARATOR);

        List<Long> wekIds = getList(new DQLSelectBuilder().fromEntity(EppYearEducationWeek.class, "w").column(property("w", EppYearEducationWeek.id())));
        for (final Map.Entry<EduProgramSubject, Map<UsmaWorkGraphRow, Map<EppYearEducationWeek, Map<Integer, EppWeekType>>>> entry : this.getDataMap (workGraph, programSubject, courses).entrySet())
        {
            final Map<Long, Map<Long, Map<Integer, EppWeekType>>> dataMap = new HashMap<>();
            for (final Map.Entry<UsmaWorkGraphRow, Map<EppYearEducationWeek, Map<Integer, EppWeekType>>> subEntry : entry.getValue().entrySet())
            {
                Long courseId = subEntry.getKey().getId();
                for (Long weekId: wekIds)
                {
                    SafeMap.safeGet(dataMap, courseId, HashMap.class).put(weekId, Collections.<Integer, EppWeekType>singletonMap(0, null));
                }

                for (final Map.Entry<EppYearEducationWeek, Map<Integer, EppWeekType>> deepEntry : subEntry.getValue().entrySet())
                {
                    SafeMap.safeGet(dataMap, courseId, HashMap.class).put(deepEntry.getKey().getId(), deepEntry.getValue());
                }
            }

            final SimpleListDataSource<UsmaWorkGraphRow> dataSource = new SimpleListDataSource<>(entry.getValue().keySet());
            final UsmaRangeSelectionWeekTypeListDataSource<UsmaWorkGraphRow> rangeModel = new UsmaRangeSelectionWeekTypeListDataSource<>(dataSource);

            rangeModel.setFullDataMap(dataMap);

            id2dataSource.put(entry.getKey(), rangeModel);

            rangeModel.setRow2points(row2points);
            rangeModel.setRow2data(row2data);
        }

        return id2dataSource;
    }


    private static void prepareRowDataPoints(final Session session, final UsmaWorkGraph workGraph, final Map<Long, int[]> row2points, final Map<Long, int[]> row2data)
    {
        final MQBuilder builder = new MQBuilder(UsmaWorkGraphRowWeek.ENTITY_CLASS, "rw");
        builder.addJoin("rw", UsmaWorkGraphRowWeek.L_ROW, "r");
        builder.add(MQExpression.eq("r", UsmaWorkGraphRow.L_GRAPH, workGraph));
        final List<UsmaWorkGraphRowWeek> list = builder.getResultList(session);

        // row -> номер части в году -> [минимальный номер недели,максимальный номер недели]
        final Map<Long, Map<Integer, int[]>> map = new HashMap<>();
        final Map<Course, Integer[]> gridDetailMap = IDevelopGridDAO.instance.get().getDevelopGridDetail(workGraph.getDevelopGrid());
        for (final UsmaWorkGraphRowWeek item : list)
        {
            // получаем данные
            final Integer[] gridDetail = gridDetailMap.get(item.getRow().getCourse());
            final Long rowId = item.getRow().getId();
            final int term = item.getTerm().getIntValue();
            final int weekNumber = item.getWeek();
            int partNumber = 0;
            while ((partNumber < gridDetail.length) && ((null == gridDetail[partNumber]) || (term != gridDetail[partNumber]))) {
                partNumber++;
            }
            partNumber++;


            // сохраняем в мапе
            Map<Integer, int[]> partMap = map.get(rowId);
            if (partMap == null)
            {
                map.put(rowId, partMap = new TreeMap<>());
            }

            final int[] points = partMap.get(partNumber);
            if (points == null)
            {
                partMap.put(partNumber, new int[]{weekNumber, weekNumber});
            }
            else if (weekNumber < points[0])
            {
                points[0] = weekNumber;
            }
            else if (weekNumber > points[1])
            {
                points[1] = weekNumber;
            }
        }

        for (final Map.Entry<Long, Map<Integer, int[]>> entry : map.entrySet())
        {
            final Map<Integer, int[]> partMap = entry.getValue();
            final int[] points = new int[partMap.size() * 2];
            int i = 0;
            for (final int[] pair : partMap.values())
            {
                points[i++] = pair[0] - 1;
                points[i++] = pair[1] - 1;
            }

            row2points.put(entry.getKey(), points);
            row2data.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, points).getData());
        }
    }



    private Map<EduProgramSubject, Map<UsmaWorkGraphRow, Map<EppYearEducationWeek, Map<Integer, EppWeekType>>>> getDataMap(UsmaWorkGraph workGraph, EduProgramSubject programSubject, Collection<Course> courseSet)
    {
        final Set<Long> ids = this.getFilteredEduPlanRowIds(workGraph, programSubject);

        // EducationLevelsHighSchool ->  List<EppWorkGraphRow2EduPlan>
        final MQBuilder rBuilder = new MQBuilder(UsmaWorkGraphRow2EduPlan.ENTITY_CLASS, "rel");
        rBuilder.add(MQExpression.eq("rel", UsmaWorkGraphRow2EduPlan.row().graph().s(), workGraph));
        if ((courseSet != null) && !courseSet.isEmpty())
        {
            rBuilder.add(MQExpression.in("rel", UsmaWorkGraphRow2EduPlan.row().course().s(), courseSet));
        }
        final List<UsmaWorkGraphRow2EduPlan> eduPlanRelListNonFiltered = rBuilder.getResultList(this.getSession());

        // фильтруем по направлениям подготовки
        List<UsmaWorkGraphRow2EduPlan> eduPlanRelList;
        if (ids == null)
        {
            eduPlanRelList = eduPlanRelListNonFiltered;
        }
        else
        {
            eduPlanRelList = new ArrayList<>();
            for (final UsmaWorkGraphRow2EduPlan rel : eduPlanRelListNonFiltered)
            {
                if (ids.contains(rel.getId()))
                {
                    eduPlanRelList.add(rel);
                }
            }
        }

        final Map<EduProgramSubject, List<UsmaWorkGraphRow2EduPlan>> edu2rows = new HashMap<>();
        for (final UsmaWorkGraphRow2EduPlan rel : eduPlanRelList)
        {
            if (!(rel.getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf))
                continue;

            final EduProgramSubject eduPlanProgramSubject = ((EppEduPlanHigherProf) rel.getEduPlanVersion().getEduPlan()).getProgramSubject();
            List<UsmaWorkGraphRow2EduPlan> list = edu2rows.get(eduPlanProgramSubject);
            if (list == null)
            {
                edu2rows.put(eduPlanProgramSubject, list = new ArrayList<>());
            }
            list.add(rel);
        }

        // EppWorkGraphRow -> List<EppWorkGraphRowWeek>
        final MQBuilder wBuilder = new MQBuilder(UsmaWorkGraphRowWeek.ENTITY_CLASS, "rw");
        wBuilder.add(MQExpression.eq("rw", UsmaWorkGraphRowWeek.row().graph().s(), workGraph));
        if ((courseSet != null) && !courseSet.isEmpty())
        {
            wBuilder.add(MQExpression.in("rw", UsmaWorkGraphRowWeek.row().course().s(), courseSet));
        }
        final List<UsmaWorkGraphRowWeek> rowWeekList = wBuilder.getResultList(this.getSession());

        final Map<UsmaWorkGraphRow, List<UsmaWorkGraphRowWeek>> row2weeks = new HashMap<>();
        for (final UsmaWorkGraphRowWeek rowWeek : rowWeekList)
        {
            List<UsmaWorkGraphRowWeek> list = row2weeks.get(rowWeek.getRow());
            if (list == null)
            {
                row2weeks.put(rowWeek.getRow(), list = new ArrayList<>());
            }
            list.add(rowWeek);
        }

        // массив недель
        final EppYearEducationWeek[] weekData = IEppYearDAO.instance.get().getYearEducationWeeks(workGraph.getYear().getId());

        final Comparator<UsmaWorkGraphRow> WORK_GRAPH_ROW_COMPARATOR = (o1, o2) -> {
            final int result = o1.getCourse().getIntValue() - o2.getCourse().getIntValue();
            if (result != 0) {
                return result;
            }
            return o1.getId().compareTo(o2.getId());
        };

        // все данные готовы. заполняем результирующий мап
        final Map<EduProgramSubject, Map<UsmaWorkGraphRow, Map<EppYearEducationWeek, Map<Integer, EppWeekType>>>> dataMap = new TreeMap<>(PROGRAM_SUBJECT_COMPARATOR);

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeekPart.class, "wp")
                .column(property("wp", UsmaWorkGraphRowWeekPart.week().id()))
                .column(property("wp", UsmaWorkGraphRowWeekPart.part()))
                .column(property("wp", UsmaWorkGraphRowWeekPart.type()))
                .where(eq(property("wp", UsmaWorkGraphRowWeekPart.week().row().graph()), value(workGraph)));

        Map<Long, Map<Integer, EppWeekType>> weekPartMap = SafeMap.get(HashMap.class);
        for (Object[] row: builder.createStatement(getSession()).<Object[]>list())
        {
            weekPartMap.get((Long) row[0]).put((Integer) row[1], (EppWeekType) row[2]);
        }


        for (final Map.Entry<EduProgramSubject, List<UsmaWorkGraphRow2EduPlan>> entry : edu2rows.entrySet())
        {
            Map<UsmaWorkGraphRow, Map<EppYearEducationWeek, Map<Integer, EppWeekType>>> dataValue = dataMap.get(entry.getKey());
            if (dataValue == null)
            {
                dataValue = new TreeMap<>(WORK_GRAPH_ROW_COMPARATOR);
            }

            for (final UsmaWorkGraphRow2EduPlan rel : entry.getValue())
            {
                final List<UsmaWorkGraphRowWeek> rowList = row2weeks.get(rel.getRow());

                Map<EppYearEducationWeek, Map<Integer, EppWeekType>> week2type = dataValue.get(rel.getRow());
                if (week2type == null)
                {
                    dataValue.put(rel.getRow(), week2type = new HashMap<>());
                }

                if (rowList != null)
                {
                    for (final UsmaWorkGraphRowWeek rowWeek : rowList)
                    {
                        Map<Integer, EppWeekType> typeMap;
                        if (rowWeek.getType() == null)
                        {
                            typeMap = weekPartMap.get(rowWeek.getId());
                        } else
                        {
                            typeMap = new HashMap<>();
                            typeMap.put(0, rowWeek.getType());
                        }


                        week2type.put(weekData[rowWeek.getWeek() - 1], typeMap);
                    }
                }
            }

            if (!dataValue.isEmpty())
            {
                dataMap.put(entry.getKey(), dataValue);
            }
        }

        return dataMap;
    }

    @Override
    public Set<Long> getFilteredEduPlanRowIds(final UsmaWorkGraph workGraph, final EduProgramSubject programSubject)
    {
        if ((programSubject == null))
            return null; // фильтровать не надо

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(UsmaWorkGraphRow2EduPlan.class, "rel");
        dql.fromEntity(EppEduPlanVersionSpecializationBlock.class, "epvBlock");
        dql.where(eq(
            property(EppEduPlanVersionSpecializationBlock.eduPlanVersion().fromAlias("epvBlock")),
            property(UsmaWorkGraphRow2EduPlan.eduPlanVersion().fromAlias("rel"))
        ));
        dql.where(eq(property(EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject().fromAlias("epvBlock")), value(programSubject)));
        dql.column(property("rel.id")).predicate(DQLPredicateType.distinct);
        return new HashSet<>(dql.createStatement(this.getSession()).<Long>list());
    }


    @SuppressWarnings("unchecked")
    @Override
    public void prepareGraphDataSource(UsmaRangeSelectionWeekTypeListDataSource<UsmaWorkGraphRow> graphDataSource, UsmaWorkGraph workGraph, Course course, Map<UsmaWorkGraphRow, Collection<UsmaWorkGraphRow2EduPlan>> row2epvs, Set<Long> filteredIds, EppYearEducationWeek[] weekData)
    {
        Session session = this.getSession();
        row2epvs.clear();

        if (course == null)
        {
            graphDataSource.getDataSource().setCountRow(0);
            UniBaseUtils.createPage((DynamicListDataSource<UsmaWorkGraphRow>) graphDataSource.getDataSource(), Collections.<UsmaWorkGraphRow>emptyList());
        }
        else
        {
            MQBuilder builder = new MQBuilder(UsmaWorkGraphRow2EduPlan.ENTITY_CLASS, "rel");
            builder.addJoin("rel", UsmaWorkGraphRow2EduPlan.L_ROW, "row");
            builder.add(MQExpression.eq("row", UsmaWorkGraphRow.L_GRAPH, workGraph));
            builder.add(MQExpression.eq("row", UsmaWorkGraphRow.L_COURSE, course));
            List<UsmaWorkGraphRow2EduPlan> relList = builder.getResultList(session);

            Map<UsmaWorkGraphRow, Object[]> row2epvMap = new HashMap<>();


            int MAP_INDEX = 0;
            int DISABLED_COUNT_INDEX = 1;

            for (UsmaWorkGraphRow2EduPlan rel : relList)
            {
                UsmaWorkGraphRow key = rel.getRow();
                Object[] value = row2epvMap.get(key);
                if (value == null)
                {
                    row2epvMap.put(key, value = new Object[]{new TreeMap<String, UsmaWorkGraphRow2EduPlan>(), 0});
                }

                Map<String, UsmaWorkGraphRow2EduPlan> epvMap = (Map) value[MAP_INDEX];
                int disabledCount = (Integer) value[DISABLED_COUNT_INDEX];

                epvMap.put(rel.getEduPlanVersion().getTitle() + rel.getEduPlanVersion().getId(), rel);

                // учитываем ненужные версии УП в строке ГУП
                if ((filteredIds != null) && !filteredIds.contains(rel.getId()))
                {
                    value[1] = disabledCount + 1;
                }
            }

            // удаляем строки ГУП, которые состоят только из ненужных версий УП
            Set<UsmaWorkGraphRow> forDelete = new HashSet<>();
            for (Map.Entry<UsmaWorkGraphRow, Object[]> row : row2epvMap.entrySet())
            {
                Object[] value = row.getValue();
                Map<String, UsmaWorkGraphRow2EduPlan> epvMap = (Map) value[MAP_INDEX];
                int disabledCount = (Integer) value[DISABLED_COUNT_INDEX];

                if (epvMap.size() == disabledCount)
                {
                    forDelete.add(row.getKey());
                }
            }
            for (UsmaWorkGraphRow key : forDelete)
            {
                row2epvMap.remove(key);
            }

            List<UsmaWorkGraphRow> list = new ArrayList<>(row2epvMap.keySet());

            Collections.sort(list, new EntityComparator<>(new EntityOrder(IEntity.P_ID)));
            graphDataSource.getDataSource().setCountRow(list.size());
            UniBaseUtils.createPage((DynamicListDataSource<UsmaWorkGraphRow>) graphDataSource.getDataSource(), list);

            Map<UsmaWorkGraphRow, Collection<UsmaWorkGraphRow2EduPlan>> row2epvsLocal = new HashMap<>();
            for (Map.Entry<UsmaWorkGraphRow, Object[]> entry : row2epvMap.entrySet())
            {
                row2epvsLocal.put(entry.getKey(), ((Map<String, UsmaWorkGraphRow2EduPlan>) row2epvMap.get(entry.getKey())[MAP_INDEX]).values());
            }

            row2epvs.putAll(row2epvsLocal);

            if (graphDataSource.getEditId() == null)
            prepareGraphDataMap(graphDataSource, workGraph, course, weekData);
        }
    }

    @SuppressWarnings("unchecked")
    private void prepareGraphDataMap(UsmaRangeSelectionWeekTypeListDataSource graphDataSource, UsmaWorkGraph workGraph, Course course, EppYearEducationWeek[] weekData)
    {
        final Session session = this.getSession();
        if (course == null)
        {
            return;
        }

        // значения в ячейках таблицы
        Map<Long, Map<Long, Map< Integer, EppWeekType>>> fullDataMap = SafeMap.get(key -> SafeMap.get(key1 -> {
            Map<Integer, EppWeekType> map = new HashMap<>();
            map.put(0, null);
            return map;
        }));
        graphDataSource.setFullDataMap(fullDataMap);

        Map<String, EppWeekType> weekTypeMap = SafeMap.get(key -> getByNaturalId(new EppWeekType.NaturalId(key)));

        Map<Long, Map<Integer, EppWeekType>> weekPartTypeMap = SafeMap.get(HashMap.class);
        DQLSelectBuilder weekPartBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeekPart.class, "wp")
                .column(property("wp", UsmaWorkGraphRowWeekPart.week().id()))
                .column(property("wp", UsmaWorkGraphRowWeekPart.part()))
                .column(property("wp", UsmaWorkGraphRowWeekPart.type().code()))
                .where(eq(property("wp", UsmaWorkGraphRowWeekPart.week().row().graph()), value(workGraph)))
                .where(eq(property("wp", UsmaWorkGraphRowWeekPart.week().row().course()), value(course)));

        Map<Long, Map<Long, UsmaWorkGraphRowWeek>> weeksMap = SafeMap.get(HashMap.class);
        DQLSelectBuilder weekBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRowWeek.class, "w")
                .where(eq(property("w", UsmaWorkGraphRowWeek.row().course()), value(course)))
                .where(eq(property("w", UsmaWorkGraphRowWeek.row().graph()), value(workGraph)));

        for (UsmaWorkGraphRowWeek rowWeek: this.<UsmaWorkGraphRowWeek>getList(weekBuilder))
        {
            weeksMap.get(rowWeek.getRow().getId()).put(weekData[rowWeek.getWeek() - 1].getId(), rowWeek);
        }

        for (Object[] row: weekPartBuilder.createStatement(getSession()).<Object[]>list())
        {
            weekPartTypeMap.get((Long) row[0]).put((Integer) row[1], weekTypeMap.get((String) row[2]));
        }

        List<UsmaWorkGraphRow> workGraphRows = graphDataSource.getDataSource().getEntityList();
        List<EppYearEducationWeek> weeks = getList(EppYearEducationWeek.class, EppYearEducationWeek.year(), workGraph.getYear());

        // бежим по всем ячейкам из базы и сохраняем в dataMap значения
        Map<Integer, EppWeekType> defaultMap = new HashMap<>();
        defaultMap.put(0, null);
        for (UsmaWorkGraphRow row: workGraphRows)
        {
            Long rowId = row.getId();
            for (EppYearEducationWeek week: weeks)
            {
                Long weekId = week.getId();
                Map<Integer, EppWeekType> partsMap;
                UsmaWorkGraphRowWeek rowWeek = weeksMap.get(rowId).get(weekId);
                if (rowWeek == null)
                {
                    partsMap = defaultMap;

                } else
                {
                    EppWeekType weekType = rowWeek.getType();
                    Map<Integer, EppWeekType> localWeekPartTypeMap = weekPartTypeMap.<Map<Integer, EppWeekType>>get(rowWeek.getId());
                    if (weekType == null)
                        partsMap = localWeekPartTypeMap == null ? new HashMap<>() : localWeekPartTypeMap;
                    else {
                        partsMap = new HashMap<>();
                        partsMap.put(0, weekType);
                    }
                }

                // записываем тип недели в ячейку таблицы
                fullDataMap.get(rowId).put(weekId, partsMap);
            }
        }


        // rowId -> массив точек
        // rowId -> логическая карта строки
        // заполняем эти два важных массива согласно данным из базы
        final Map<Long, int[]> row2points = new HashMap<>();
        final Map<Long, int[]> row2data = new HashMap<>();
        graphDataSource.setRow2points(row2points);
        graphDataSource.setRow2data(row2data);
        prepareRowDataPoints(session, workGraph, course, row2points, row2data);
    }

    private static List<UsmaWorkGraphRowWeek> getRelationList(final Session session, final UsmaWorkGraph workGraph, final Course course, final Long rowId)
    {
        final MQBuilder builder = new MQBuilder(UsmaWorkGraphRowWeek.ENTITY_CLASS, "rw");
        builder.addJoin("rw", UsmaWorkGraphRowWeek.L_ROW, "r");
        builder.add(MQExpression.eq("r", UsmaWorkGraphRow.L_GRAPH, workGraph));
        builder.add(MQExpression.eq("r", UsmaWorkGraphRow.L_COURSE, course));
        if (rowId != null)
        {
            builder.add(MQExpression.eq("r", IEntity.P_ID, rowId));
        }
        return builder.getResultList(session);
    }


    private static void prepareRowDataPoints(final Session session, final UsmaWorkGraph workGraph, final Course course, final Map<Long, int[]> row2points, final Map<Long, int[]> row2data)
    {
        // row -> номер части в году -> [минимальный номер недели,максимальный номер недели]
        final Map<Long, Map<Integer, int[]>> map = new HashMap<>();
        final Integer[] gridDetail = IDevelopGridDAO.instance.get().getDevelopGridDetail(workGraph.getDevelopGrid()).get(course);

        for (final UsmaWorkGraphRowWeek item : getRelationList(session, workGraph, course, null))
        {
            // получаем данные
            final Long rowId = item.getRow().getId();
            final int term = item.getTerm().getIntValue();
            final int weekNumber = item.getWeek();
            int partNumber = 0;
            while ((partNumber < gridDetail.length) && ((null == gridDetail[partNumber]) || (term != gridDetail[partNumber]))) {
                partNumber++;
            }
            partNumber++;

            // сохраняем в мапе
            Map<Integer, int[]> partMap = map.get(rowId);
            if (partMap == null)
            {
                map.put(rowId, partMap = new TreeMap<>());
            }

            final int[] points = partMap.get(partNumber);
            if (points == null)
            {
                partMap.put(partNumber, new int[]{weekNumber, weekNumber});
            }
            else if (weekNumber < points[0])
            {
                points[0] = weekNumber;
            }
            else if (weekNumber > points[1])
            {
                points[1] = weekNumber;
            }
        }

        for (final Map.Entry<Long, Map<Integer, int[]>> entry : map.entrySet())
        {
            final Map<Integer, int[]> partMap = entry.getValue();
            final int[] points = new int[partMap.size() * 2];
            int i = 0;
            for (final int[] pair : partMap.values())
            {
                points[i++] = pair[0] - 1;
                points[i++] = pair[1] - 1;
            }

            row2points.put(entry.getKey(), points);
            row2data.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, points).getData());
        }
    }

    @Override
    public void saveWorkPlanByWorkGraphs(UsmaWorkGraph workGraph, boolean lookup4WorkPlan, boolean check4ExistWorkPlan, String comment)
    {
        if (null == workGraph) { throw new ApplicationException("Необходимо указать график учебного процесса."); }

        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(workGraph.getId()));

        comment = DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + "\n" + comment;

        EppState state = this.getByNaturalId(new EppState.NaturalId(EppState.STATE_FORMATIVE));
        IEppSettingsDAO.instance.get().getELoadWeekTypeMap();

        Map<Long, EppYearEducationProcess> yearMap = SafeMap.get(id -> (EppYearEducationProcess)session.get(EppYearEducationProcess.class, id));
        Map<Long, DevelopGridTerm> termMap = SafeMap.get(id -> (DevelopGridTerm)session.get(DevelopGridTerm.class, id));


        DQLSelectBuilder dqlCore = new DQLSelectBuilder();
        dqlCore.fromEntity(UsmaWorkGraphRow2EduPlan.class, "wgr2ep");
        dqlCore.where(eq(
            property(UsmaWorkGraphRow2EduPlanGen.row().graph().fromAlias("wgr2ep")),
            value(workGraph)
        ));

        dqlCore.fromEntity(DevelopGridTerm.class, "gterm");
        dqlCore.where(eq(
            property(DevelopGridTermGen.developGrid().fromAlias("gterm")),
            property(UsmaWorkGraphRow2EduPlanGen.eduPlanVersion().eduPlan().developGrid().fromAlias("wgr2ep"))
        ));
        dqlCore.where(eq(
            property(DevelopGridTermGen.course().fromAlias("gterm")),
            property(UsmaWorkGraphRow2EduPlanGen.row().course().fromAlias("wgr2ep"))
        ));

        dqlCore.fromEntity(EppEduPlanVersionBlock.class, "epvBlock");
        dqlCore.where(eq(
            property(EppEduPlanVersionBlockGen.eduPlanVersion().fromAlias("epvBlock")),
            property(UsmaWorkGraphRow2EduPlanGen.eduPlanVersion().fromAlias("wgr2ep"))
        ));

        dqlCore.column(property(UsmaWorkGraphRow2EduPlanGen.row().graph().year().id().fromAlias("wgr2ep")), "eppYear_id");
        dqlCore.column(property("gterm.id"), "gridTerm_id");
        dqlCore.column(property("epvBlock.id"), "eppEpvBlock_id");

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromDataSource(dqlCore.buildQuery(), "src");
        dql.column(property("src.eppYear_id"), "eppYear_id");
        dql.column(property("src.gridTerm_id"), "gridTerm_id");
        dql.column(property("src.eppEpvBlock_id"), "eppEpvBlock_id");

        if (check4ExistWorkPlan)
        {
            // не должно быть РУП(основной РУП) на тот же блок в том же году и семестре
            dql.where(notExists(
                new DQLSelectBuilder()
                .fromEntity(EppWorkPlan.class, "wpx").column(property("wpx.id"))
                .where(eq(property(EppWorkPlanGen.parent().id().fromAlias("wpx")), property("src.eppEpvBlock_id")))
                .where(eq(property(EppWorkPlanGen.year().id().fromAlias("wpx")), property("src.eppYear_id")))
                .where(eq(property(EppWorkPlanGen.cachedGridTerm().id().fromAlias("wpx")), property("src.gridTerm_id")))
                .buildQuery()
            ));
        }

        int i = 0;
        for (Object[] row: CommonDAO.scrollRows(dql.createStatement(this.getSession()))) {
            EppYearEducationProcess year = yearMap.get(((Number)row[0]).longValue());
            DevelopGridTerm term = termMap.get(((Number)row[1]).longValue());
            EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)session.load(EppEduPlanVersionBlock.class, ((Number)row[2]).longValue());

            // сохраняем РП
            EppWorkPlan wp = new EppWorkPlan(year, block, term);
            wp.setState(state);
            wp.setNumber(INumberQueueDAO.instance.get().getNextNumber(wp));
            wp.setRegistrationNumber(wp.getNumber());
            wp.setComment(comment);
            session.save(wp);

            // Создаем часть года
            IEppWorkPlanDAO.instance.get().doCreateEppYearPartToWP(wp);

            // заполняем его данными (либо из УП, либо из РП прошлых лет)
            if (IEppWorkPlanDAO.instance.get().doGenerateWorkPlanRows(wp.getId(), lookup4WorkPlan)) {
                i++;
            }
        }

        ContextLocal.getInfoCollector().add("Скопировано "+i+" РУП");
    }
}