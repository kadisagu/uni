/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2014
 */
public interface IUsmaMedCardDao extends INeedPersistenceSupport
{
	public ITemplateDocument getTemplateDocument();
}
