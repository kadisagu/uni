/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public interface IDAO<T extends IModel> extends IUniDao<T> {

}
