/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.VaccineAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccinationSign;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author DMITRY KNYAZEV
 * @since 24.06.2014
 */
@Configuration
public class UsmaMedCardVaccineAddEdit extends BusinessComponentManager
{
	//data source
	public static final String VACCINE_KIND_DS = "vaccineKindDS";
	public static final String VACCINATION_SIGN_DS = "vaccinationSignDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(VACCINE_KIND_DS, vaccineKindDSHandler()))
				.addDataSource(selectDS(VACCINATION_SIGN_DS, vaccinationSignDSHandler()))
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> vaccineKindDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), UsmaVaccineKind.class)
				.order(UsmaVaccineKind.title());
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> vaccinationSignDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), UsmaVaccinationSign.class)
				.order(UsmaVaccinationSign.title());
	}
}
