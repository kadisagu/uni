/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.row.UsmaCompetenceAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<UsmaCompetence> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Код компетенции", UsmaCompetence.eppSkillGroup().shortTitle().s()).setWidth(8).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Название", UsmaCompetence.P_TITLE).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteCompetence"));

        model.setDataSource(dataSource);
    }


    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickDeleteCompetence(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long competenceId = component.getListenerParameter();

        UsmaCompetence target = null;
        for(UsmaCompetence competence: model.getValues())
        {
            if (competence.getId().equals(competenceId))
            {
                target = competence;
                break;
            }
        }

        if (target == null)
            throw new IllegalStateException();

        model.getValues().remove(target);
    }

    public void onClickUpdateCompetence(IBusinessComponent component)
    {
        getDao().updateCompetence(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}