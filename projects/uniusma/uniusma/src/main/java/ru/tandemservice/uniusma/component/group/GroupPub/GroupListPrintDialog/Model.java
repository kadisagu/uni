// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.group.GroupPub.GroupListPrintDialog;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

/**
 * @author vnekrasov
 * @since 24/1/14
 */
//@Input(keys = "groupId", bindings = "groupId")
@Input({@Bind(key="groupId", binding="groupId")})

public class Model
{

    private boolean _bookNumberReq = true;
    private Long _groupId;

    public Long getGroupId() {
        return _groupId;
    }

    public void setGroupId(Long groupId) {
        _groupId = groupId;
    }

    public boolean isBookNumberReq() {
           return _bookNumberReq;
       }

    public void setBookNumberReq(boolean bookNumberReq) {
           _bookNumberReq = bookNumberReq;
       }

}
