/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e26.Pub;

import ru.tandemservice.uniusma.entity.catalog.codes.StudentExtractTypeCodes;

/**
 * @author Denis Perminov
 * @since 13.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e26.Pub.Model
{
    public boolean isPreambulaVisible()
    {
        String code = getExtract().getType().getCode();
        return code.equals(StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER);
    }
}
