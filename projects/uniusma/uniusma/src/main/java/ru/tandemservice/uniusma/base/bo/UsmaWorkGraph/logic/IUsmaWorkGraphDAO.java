/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic;

import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow2EduPlan;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
public interface IUsmaWorkGraphDAO extends ICommonDAO
{
    /**
     * Устанавливает значения по умолчанию в фильтры в списке графиков учебного процесса ДВФУ.
     * @param settings настройки компонента
     */
    public void setWorkGraphListFilterDefaultValues(IUISettings settings);

    /**
     * Устанавливает значения по умолчанию в фильтры на вкладке Учебные недели карточки графика учебного процесса.
     * @param settings настройки компонента
     * @param workGraphId id графика учебного процесса
     */
    public void setWorkGraphWeeksFilterDefaultValues(IUISettings settings, Long workGraphId);

    /**
     * Возвращает связи графика учебного процесса с курсами и версия УП.
     * @param workGraphId id графика учебного процесса
     * @return справочник наборов УПв и курсов на признак формирования ГУПом
     */
    public Map<PairKey<Long, String>, Boolean> getWorkGraphEduPlanVersionCourses(Long workGraphId);

    /**
     * Сохоаняет связи ГУПа с УПв и курсом.
     * @param workGraphId id графика учебного процесса
     * @param versionCourseIdsMap справочник наборов УПв и курсов на признак формирования ГУПом
     */
    public void saveWorkGraphEduPlanVersionCourses(Long workGraphId, Map<Long, Set<String>> versionCourseIdsMap);


    /**
     * Возвращает список курсов учебной сетки графика учебного процесса
     * @param workGraphId id графика
     * @return список курсов
     */
    public List<Course> getWorkGraphCourses(Long workGraphId);


    /**
     * Возвращает мапу направления подготовки на датасорс учебных недель.
     * @param workGraph график учебного процесса
     * @param courses список курсов
     * @param programSubject направление подготовки
     * @return мапу направления подготовки на датасорс учебных недель
     */
    public Map<EduProgramSubject, UsmaRangeSelectionWeekTypeListDataSource> getWorkGraphEduLevelDataSourceMap(UsmaWorkGraph workGraph, Collection<Course> courses, EduProgramSubject programSubject);

    /**
     * Подготавливает данные для списка строк графика учебного процесса.
     * @param graphDataSource обрабатываемый датасорс
     * @param workGraph ГУП
     * @param course курс
     * @param row2epvs отображение строки ГУПа на набор привязанных УПв
     * @param filteredIds id строк УПв ГУПа
     * @param weekData данные поучебным неделям
     */
    public void prepareGraphDataSource(UsmaRangeSelectionWeekTypeListDataSource<UsmaWorkGraphRow> graphDataSource, UsmaWorkGraph workGraph, Course course, Map<UsmaWorkGraphRow, Collection<UsmaWorkGraphRow2EduPlan>> row2epvs, Set<Long> filteredIds, EppYearEducationWeek[] weekData);

    /**
     * Возвращает список связей УПв с ГУПом.
     * @param workGraph ГУП
     * @param programSubject направление подготовки, null - игнорировать
     * @return список связей УПв с ГУПом
     */
    public Set<Long> getFilteredEduPlanRowIds(UsmaWorkGraph workGraph, EduProgramSubject programSubject);

    /**
     * Создать РУПы на основании графика учебного процесса.
     * @param workGraph ГУП
     * @param lookup4WorkPlan создавать на основе РУПов прошлых лет
     * @param check4ExistWorkPlan копировать только новые РУПы
     * @param comment комментарий
     */
    public void saveWorkPlanByWorkGraphs(UsmaWorkGraph workGraph, boolean lookup4WorkPlan, boolean check4ExistWorkPlan, String comment);
}