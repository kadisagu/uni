package ru.tandemservice.uniusma.component.group.GroupStudentListPrint;

import org.springframework.util.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 23.01.14
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class Controller  extends AbstractBusinessController<IDAO, Model> {

    @Override
        public void onRefreshComponent(IBusinessComponent component)
        {
            getDao().prepare(getModel(component));

            try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
            catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
        }

        public RtfDocument createRtfDocument()
        {
//            InputStream in = getClass().getClassLoader().getResourceAsStream("uniusma/templates/GroupStudentList.rtf");
//            byte[] template = IOUtils.toByteArray(in);
            byte[] template = DataAccessServices.dao().getByCode(UniScriptItem.class, UniDefines.TEMPLATE_GROUP_STUDENT_LIST).getTemplate();
            return new RtfReader().read(template);
        }

        public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
        {
            Model model = getModel(component);
            final RtfDocument document = createRtfDocument();

            new RtfInjectModifier()
                    .put("groupTitle", model.getGroup().getTitle())
                    .put("currentDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                    .put("educationLevel", model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle())
                    .put("developForm", model.getGroup().getEducationOrgUnit().getDevelopForm().getTitle())
                    .put("developPeriod", model.getGroup().getEducationOrgUnit().getDevelopPeriod().getTitle())
                    .modify(document);

            prepareTableModifier(model).modify(document);

            deactivate(component);
            return new CommonBaseRenderer().rtf().fileName("GroupList.rtf").document(document);
        }

    public RtfTableModifier prepareTableModifier(final Model model)
        {
            final boolean addBookNumber = model.isAddBookNum();
            RtfTableModifier tableModifier = new RtfTableModifier();

            tableModifier.put("T", getDao().getStudentData(model, addBookNumber));
            final int fioColumnIndex = 1;
            final int bookNumColumnIndex = 2;

            tableModifier.put("T", new RtfRowIntercepterBase()
            {
                @Override
               public void beforeModify(final RtfTable table, final int currentRowIndex)
               {
                   if (!addBookNumber)
                       {
                           deleteCellFioIncrease(table.getRowList().get(currentRowIndex), bookNumColumnIndex); //ячейка в строке с меткой T
                           deleteCellFioIncrease(table.getRowList().get(currentRowIndex - 1), bookNumColumnIndex); //ячейка в шапке
                       }
               }

               private void deleteCellFioIncrease(final RtfRow row, final int cellIndex)
               {
                   final RtfCell cell = row.getCellList().get(cellIndex);
                   final RtfCell destCell = row.getCellList().get(fioColumnIndex);
                   destCell.setWidth(destCell.getWidth() + cell.getWidth());
                   row.getCellList().remove(cellIndex);
               }

                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    if (model.getCaptainRowList().contains(rowIndex))
                        return new RtfString().boldBegin().append(StringUtils.capitalize(value)).boldEnd().toList();

                    return super.beforeInject(table, row, cell, rowIndex, colIndex, value);
                }
            });

            return tableModifier;
        }
}
