/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.IUsmaMedCardReportDao;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.UsmaMedCardReportDao;

/**
 * @author DMITRY KNYAZEV
 * @since 26.06.2014
 */
@Configuration
public class UsmaMedCardReportManager extends BusinessObjectManager
{
	public static UsmaMedCardReportManager instance()
	{
		return instance(UsmaMedCardReportManager.class);
	}

	@Bean
	public IUsmaMedCardReportDao dao()
	{
		return new UsmaMedCardReportDao();
	}
}
