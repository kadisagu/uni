/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.logic;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.UsmaMedCardManager;
import ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 25.06.2014
 */
public class UsmaMedCardPrinter extends UniBaseDao
{
	public void printMedCard(Long groupId)
	{
		DQLSelectBuilder studentDql = new DQLSelectBuilder().fromEntity(Student.class, "s")
				.column("s")
				.where(eq(property("s", Student.group()),value(groupId)))
				.where(eq(property("s", Student.status().active()), value(Boolean.TRUE)));
		this.printMedCard(DataAccessServices.dao().<Student>getList(studentDql));
	}

	public void printMedCard(List<Student> studentsList)
	{
		try
		{
			Collections.sort(studentsList, Student.FULL_FIO_AND_ID_COMPARATOR);
			BusinessComponentUtils.downloadDocument(buildDocumentRenderer(studentsList), false);
		}
		catch (Throwable t)
		{
			throw CoreExceptionUtils.getRuntimeException(t);
		}
	}

	private RtfDocument createRtfDocument()
	{
//		InputStream in = getClass().getClassLoader().getResourceAsStream("uniusma/templates/MedCard.rtf");
		ITemplateDocument templateDocument = UsmaMedCardManager.instance().dao().getTemplateDocument();
		return new RtfReader().read(templateDocument.getContent());
	}

	private IDocumentRenderer buildDocumentRenderer(List<Student> students)
	{
		final RtfDocument document = createRtfDocument();
		RtfDocument resultDoc = document.getClone();
		resultDoc.getElementList().clear();
		Iterator<Student> iterator = students.iterator();
		IRtfControl control = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
		while (iterator.hasNext())
		{
			RtfDocument doc = document.getClone();
			modifyByStudent(doc, iterator.next());
			resultDoc.getElementList().addAll(doc.getElementList());
			if (iterator.hasNext())
				resultDoc.getElementList().add(control);
		}
		resultDoc.setSettings(document.getSettings());
		resultDoc.setHeader(document.getHeader());

		return new CommonBaseRenderer().rtf().fileName("MedCard.rtf").document(resultDoc);
	}

	private void modifyByStudent(RtfDocument document, Student student)
	{
		new RtfInjectModifier()
				.put("fullName", student.getPerson().getFullFio())
				.put("groupTitle", student.getGroup() != null ? student.getGroup().getTitle() : null)
				.put("birthDate", student.getPerson().getBirthDateStr())
				.modify(document);
		prepareTableModifier(student).modify(document);
	}
	private RtfTableModifier prepareTableModifier(Student student)
	{
		List<UsmaStudentVaccineInfo> VT = DataAccessServices.dao().getList(UsmaStudentVaccineInfo.class, UsmaStudentVaccineInfo.L_STUDENT, student);
		Collections.sort(VT, (o1, o2) -> o1.getVaccineKind().getTitle().compareToIgnoreCase(o2.getVaccineKind().getTitle()));
		List<UsmaStudentFluorographyInfo> FT = DataAccessServices.dao().getList(UsmaStudentFluorographyInfo.class, UsmaStudentFluorographyInfo.L_STUDENT, student, UsmaStudentFluorographyInfo.P_DATE);
		final List<String[]> VTDataLines = new ArrayList<>();
		final List<String[]> FTDataLines = new ArrayList<>();

		for (UsmaStudentVaccineInfo info : VT)
		{
			String vaccineKind = info.getVaccineKind().getTitle();
			String sign = info.getVaccinationSign() == null ? "" : info.getVaccinationSign().getTitle();
			String date = info.getDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(info.getDate());
			String part = info.getPart() == null ? "" : info.getPart();
			String periodical = info.getPeriodical() == null ? "" : info.getPeriodical();
			VTDataLines.add(new String[]{vaccineKind, sign, date, part, periodical});
		}

		for (UsmaStudentFluorographyInfo info : FT)
		{
			FTDataLines.add(new String[]{DateFormatter.DEFAULT_DATE_FORMATTER.format(info.getDate()),info.getPictureNumber(), info.getResult()});
		}

		final RtfTableModifier tableModifier = new RtfTableModifier();
		tableModifier.put("VT", VTDataLines.toArray(new String[VTDataLines.size()][]));
		tableModifier.put("FT", FTDataLines.toArray(new String[FTDataLines.size()][]));

		return tableModifier;
	}
}
