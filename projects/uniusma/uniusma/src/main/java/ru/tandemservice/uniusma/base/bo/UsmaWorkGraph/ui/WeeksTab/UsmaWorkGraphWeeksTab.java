/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.WeeksTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Configuration
public class UsmaWorkGraphWeeksTab extends BusinessComponentManager
{
    public static final String DS_COURSE = "courseDS";
    public static final String DS_PROGRAM_SUBJECT = "programSubjectDS";

    public static final String COURSE = "course";

     @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(selectDS(DS_COURSE, UsmaWorkGraphManager.instance().courseDSHandler()))
            .addDataSource(selectDS(DS_PROGRAM_SUBJECT, UsmaWorkGraphManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .create();
    }
}