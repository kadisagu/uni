/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node;

import java.util.List;

/**
 * Представление нода: название, представление структурой атрибутов.
 * @author Alexander Zhebko
 * @since 29.07.2013
 */

public interface INodeView
{
    /**
     * Возвращает имя нода.
     * @return имя нода
     */
    public String getNodeName();

    /**
     * Представление нода как содержащего атрибуты.
     * @return представление атрибутами
     */
    public IAttributeView getAttributeView();

    /**
     * Возвращает список уникальных атрибутов.
     * @return список уникальных атрибутов
     */
    public List<String> getUniqueAttributes();
}