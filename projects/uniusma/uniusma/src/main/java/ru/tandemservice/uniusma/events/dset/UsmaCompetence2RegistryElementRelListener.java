/**
 *$Id$
 */
package ru.tandemservice.uniusma.events.dset;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 28.02.2013
 */
public class UsmaCompetence2RegistryElementRelListener extends AbstractUsmaCompetenceRelListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, UsmaCompetence2RegistryElementRel.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, UsmaCompetence2RegistryElementRel.class, this);
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "row")
                .joinEntity("row", DQLJoinType.inner, EppRegistryElement.class, "regel", eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("row")), property(EppRegistryElement.id().fromAlias("regel"))))
                .joinEntity("regel", DQLJoinType.inner, UsmaCompetence2RegistryElementRel.class, "comp2regel", eq(property(UsmaCompetence2RegistryElementRel.registryElement().id().fromAlias("comp2regel")), property(EppRegistryElement.id().fromAlias("regel"))))
                .where(in(property(UsmaCompetence2RegistryElementRel.id().fromAlias("comp2regel")), event.getMultitude().getInExpression()))
                .column(property(EppEpvRegistryRow.owner().id().fromAlias("row")))
                .createStatement(event.getContext()).list();
    }
}