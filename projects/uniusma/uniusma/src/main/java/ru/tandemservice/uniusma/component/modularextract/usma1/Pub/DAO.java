/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class DAO extends ModularStudentExtractPubDAO<UsmaExcludeAfterAcadWeekendStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
    }

}
