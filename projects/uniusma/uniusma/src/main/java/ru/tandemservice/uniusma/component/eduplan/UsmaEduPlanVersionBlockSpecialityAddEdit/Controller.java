/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockSpecialityAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        if (model.getSpecialityId() != null){ model.setNumber(model.getSpeciality().getNumber()); }
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSpeciality().setNumber(model.getNumber());
        getDao().saveOrUpdate(model.getSpeciality());
        deactivate(component);
    }
}