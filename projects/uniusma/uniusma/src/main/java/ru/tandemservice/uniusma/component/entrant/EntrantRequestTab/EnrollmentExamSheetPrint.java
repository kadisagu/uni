/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.entrant.EntrantRequestTab;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.dao.examgroup.IExamGroupSetDao;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.util.*;

/**
 * @author oleyba
 * @since 09.06.2009
 */
public class EnrollmentExamSheetPrint extends UniBaseDao implements IEnrollmentExamSheetPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long object)
    {
        RtfDocument document = new RtfReader().read(template);

        EntrantRequest entrantRequest = get(object);
        Entrant entrant = entrantRequest.getEntrant();
        IdentityCard identityCard = entrant.getPerson().getIdentityCard();

        // получаем выбранные направления приема
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().s(), entrant));

        EntrantDataUtil util = new EntrantDataUtil(getSession(), entrant.getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        getEntrantRequestList(util);

        // вычисляем список первых направлений из заявлений отсортированных по номеру заявления
        List<RequestedEnrollmentDirection> firstDirectionList = getRequestedEnrollmentDirectionList(util);

        // вычисляем метку orgUnit
        String orgUnit = getOrgUnitTitle(firstDirectionList);

        // вычисляем метку regNum
        String regNum = getRegNumTitle(firstDirectionList);

        // уникальные группы вступительных испытаний
        List<List<ChosenEntranceDiscipline>> uniqueList = getUniqueChosenList(util);

        // заполняем данные на первой странице
        RtfInjectModifier firstPageInjectModifier = new RtfInjectModifier();
        firstPageInjectModifier.put("orgUnit", orgUnit);
        firstPageInjectModifier.put("grNumRegNum", regNum);
        firstPageInjectModifier.put("lastName", identityCard.getLastName());
        firstPageInjectModifier.put("firstName", identityCard.getFirstName());
        firstPageInjectModifier.put("middleName", identityCard.getMiddleName());
        firstPageInjectModifier.put("entrantNumber", entrant.getPersonalNumber());
        firstPageInjectModifier.modify(document);

        // если групп нет - печатаем таблицу с оценками с одной пустой строкой
        if (uniqueList.isEmpty())
        {
            new RtfTableModifier().put("T", new String[1][]).modify(document);
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("pointsTotal", "");
            injectModifier.modify(document);
            return document;
        }

        // ищем в шаблоне элемент, соответствующий таблице с оценками
        RtfTable emptyMarkTable = null;
        // содержащий его список элементов
        List<IRtfElement> tableElementList = null;
        // и позицию элемента в этом списке
        int index = 0;
        boolean found = false;
        List<List<IRtfElement>> sourceList = new ArrayList<>();
        sourceList.add(document.getElementList());
        List<List<IRtfElement>> newSourceList = new ArrayList<>();
        while (!sourceList.isEmpty() && !found)
        {
            for (List<IRtfElement> elementList : sourceList)
            {
                for (int i = 0; i < elementList.size(); i++)
                {
                    IRtfElement element = elementList.get(i);
                    if (element instanceof IRtfGroup)
                        newSourceList.add(((IRtfGroup) element).getElementList());
                    if (element instanceof RtfTable)
                    {
                        RtfTable table = (RtfTable) element;
                        for (int currentRowIndex = 0; currentRowIndex < table.getRowList().size(); currentRowIndex++)
                        {
                            RtfRow currentRow = table.getRowList().get(currentRowIndex);
                            String rowName = getRowName(currentRow);
                            if ("T".equals(rowName))
                            {
                                emptyMarkTable = table.getClone();
                                tableElementList = elementList;
                                index = i;
                                found = true;
                            }
                        }
                    }
                }
            }
            if (!found)
            {
                sourceList.clear();
                sourceList.addAll(newSourceList);
                newSourceList.clear();
            }
        }

        // не нашли
        if (!found) return document;

        // для каждого блока вступительных испытаний печатаем свою таблицу с оценками
        Iterator<List<ChosenEntranceDiscipline>> iterator = uniqueList.iterator();
        while (iterator.hasNext())
        {
            List<ChosenEntranceDiscipline> chosenEntranceDisciplineList = iterator.next();
            List<String[]> tableData = new ArrayList<>();
            int num = 1;
            // общее число баллов показываем только в том случае, если есть оценки по всем предметам
            boolean showPointsTotal = true;
            Long pointsTotal = 0L;
            for (ChosenEntranceDiscipline chosen : chosenEntranceDisciplineList)
            {
                String finalMarkStr = "";
                if (chosen.getFinalMark() != null)
                {
                    // округляем, чтобы написать число прописью
                    // предполагается, что у УГМА всегда целые оценки - так что округление и его способ ни на что не влияют
                    final Long finalMark = Math.round(chosen.getFinalMark());
                    finalMarkStr = String.valueOf(finalMark) + " (" + NumberSpellingUtil.spellNumberMasculineGender(finalMark) + ")";
                    pointsTotal = pointsTotal + finalMark;
                } else
                    showPointsTotal = false;
                String source;
                if (chosen.getFinalMarkSource() != null && (UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 == chosen.getFinalMarkSource() || UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2 == chosen.getFinalMarkSource()))
                {
                    MarkStatistic statistic = util.getMarkStatistic(chosen);
                    source = statistic.getStateExamMark() != null ? statistic.getStateExamMark().getCertificate().getTitle() : "";
                } else
                    source = "";
                tableData.add(new String[]{String.valueOf(num++), chosen.getEnrollmentCampaignDiscipline().getTitle(), "", finalMarkStr, source, ""});
            }
            RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.toArray(new String[tableData.size()][]));
            tableModifier.modify(document);
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            String pointsTotalStr = "";
            if (showPointsTotal)
                pointsTotalStr = String.valueOf(pointsTotal) + " (" + NumberSpellingUtil.spellNumberMasculineGender(pointsTotal) + ")";
            injectModifier.put("pointsTotal", pointsTotalStr);
            injectModifier.modify(document);
            // клонируем таблицу с метками, если она еще будет нужна на следующей итерации
            if (iterator.hasNext()) tableElementList.add(++index, emptyMarkTable.getClone());
        }

        return document;
    }

    /**
     * Ищет "имя" строки - метку в первой ячейке
     *
     * @param row строка таблицы
     * @return метка
     */
    private String getRowName(RtfRow row)
    {
        List<RtfCell> cellList = row.getCellList();

        if (cellList.size() == 0)
            throw new RuntimeException("Invalid document: Row has no cells!");

        for (IRtfElement element : cellList.get(0).getElementList())
            if (element instanceof RtfField)
            {
                RtfField field = (RtfField) element;
                if (field.isMark()) return field.getFieldName();
            }
        return null;
    }

    // вычисляется метка orgUnit

    private String getOrgUnitTitle(List<RequestedEnrollmentDirection> firstDirectionList)
    {
        Set<String> firstDirectionTitleSet = new TreeSet<>();
        for (RequestedEnrollmentDirection direction : firstDirectionList)
            firstDirectionTitleSet.add(direction.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        return StringUtils.join(firstDirectionTitleSet, ", ");
    }

    // вычисляется метка regNum

    private String getRegNumTitle(List<RequestedEnrollmentDirection> firstDirectionList)
    {
        IExamGroupSetDao dao = UniecDAOFacade.getExamGroupSetDao();
        List<String> title = new ArrayList<>();
        for (RequestedEnrollmentDirection direction : firstDirectionList)
            for (ExamGroup examGroup : dao.getEntrantRequestGroups(direction.getEntrantRequest()))
                title.add(examGroup.getShortTitle() + " (" + direction.getEntrantRequest().getStringNumber() + ")");
        return StringUtils.join(title, ", ");
    }

    // вычисляются уникальные наборы выбранных дисциплин

    private List<List<ChosenEntranceDiscipline>> getUniqueChosenList(EntrantDataUtil util)
    {
        Set<MultiKey> used = new HashSet<>();
        List<List<ChosenEntranceDiscipline>> result = new ArrayList<>();
        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
        {
            List<ChosenEntranceDiscipline> chosenList = new ArrayList<>(util.getChosenEntranceDisciplineSet(direction));
            Collections.sort(chosenList, (o1, o2) -> o1.getEnrollmentCampaignDiscipline().getTitle().compareTo(o2.getEnrollmentCampaignDiscipline().getTitle()));
            List<Long> idList = new ArrayList<>();
            for (ChosenEntranceDiscipline chosen : chosenList)
                idList.add(chosen.getEnrollmentCampaignDiscipline().getId());
            if (!idList.isEmpty())
            {
                MultiKey id = new MultiKey(idList.toArray(new Object[idList.size()]));
                if (!used.contains(id))
                {
                    used.add(id);
                    result.add(chosenList);
                }
            }
        }
        return result;
    }

    // вычисляется список заявлений отсортированных по номеру

    private List<EntrantRequest> getEntrantRequestList(EntrantDataUtil util)
    {
        Set<EntrantRequest> requestSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
            requestSet.add(direction.getEntrantRequest());
        List<EntrantRequest> requestList = new ArrayList<>(requestSet);
        Collections.sort(requestList, (o1, o2) -> Integer.compare(o1.getRegNumber(), o2.getRegNumber()));
        return requestList;
    }

    // вычисляется список первых направлений из заявлений отсортированных по номеру заявления

    private List<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionList(EntrantDataUtil util)
    {
        Map<EntrantRequest, RequestedEnrollmentDirection> requestId2firstDirection = new TreeMap<>((o1, o2) -> Integer.compare(o1.getRegNumber(), o2.getRegNumber()));

        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
        {
            EntrantRequest request = direction.getEntrantRequest();
            RequestedEnrollmentDirection firstDirection = requestId2firstDirection.get(request);
            if (firstDirection == null || firstDirection.getPriority() > direction.getPriority())
                requestId2firstDirection.put(request, direction);
        }

        return new ArrayList<>(requestId2firstDirection.values());
    }
}
