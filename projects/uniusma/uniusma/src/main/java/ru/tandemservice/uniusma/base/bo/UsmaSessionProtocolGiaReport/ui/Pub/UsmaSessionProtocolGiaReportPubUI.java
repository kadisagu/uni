/*$Id:$*/
package ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;
import ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.UsmaSessionProtocolGiaReportManager;
import ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.logic.ISessionProtocolGiaReportDao;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 17.03.2016
 */
@Input({@Bind(key = "orgUnitId", binding = UsmaSessionProtocolGiaReportPubUI.ORG_UNIT_ID),})
public class UsmaSessionProtocolGiaReportPubUI extends UIPresenter
{

    public static final String ORG_UNIT_ID = "orgUnitId";

    private static final IdentifiableWrapper<IEntity> SOURCE_OWNER_ORGUNIT = new IdentifiableWrapper<>(1L, "С читающих подразделений");
    private static final IdentifiableWrapper<IEntity> SOURCE_ALL_ORGUNIT = new IdentifiableWrapper<>(2L, "Со всех подразделений");
    private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(
            SOURCE_OWNER_ORGUNIT,
            SOURCE_ALL_ORGUNIT
    );
    public static String PARAM_COURSE = "course";

    private ISelectModel ppsModel;
    private IdentifiableWrapper<IEntity> sourceFilter = SOURCE_OWNER_ORGUNIT;
    private List<PpsEntry> chairmanPps;
    private List<PpsEntry> membersPps;
    private List<PpsEntry> clerkPps;

    private final ISessionProtocolGiaReportDao dao = UsmaSessionProtocolGiaReportManager.instance().dao();

    private Long orgUnitId;
    private Course course;
    private List<Group> group;

    @Override
    public void onComponentRefresh()
    {
        setPpsModel(new PpsEntryModel()
        {
            @Override
            protected DQLSelectBuilder filter(String alias, DQLSelectBuilder ppsDQL)
            {
                ppsDQL.where(isNull(property(PpsEntry.removalDate().fromAlias(alias))));
                if (SOURCE_OWNER_ORGUNIT.equals(getSourceFilter()))
                {
                    ppsDQL.where(eq(property(PpsEntry.orgUnit().id().fromAlias(alias)), value(getOrgUnitId())));
                }
                return ppsDQL;
            }
        });
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UsmaSessionProtocolGiaReportPub.GROUP_DS.equals(dataSource.getName()))
            dataSource.put(PARAM_COURSE, course);
    }

    //Handlers
    public void onClickApply()
    {
        final RtfDocument document = dao.getDocument(group, chairmanPps, membersPps, clerkPps);
        final IDocumentRenderer documentRenderer = new CommonBaseRenderer()
                .rtf()
                .fileName("Протокол ГИА.rtf")
                .document(document);
        BusinessComponentUtils.downloadDocument(documentRenderer, true);
    }

    //Getters/Setters
    @Override
    public ISecured getSecuredObject()
    {
        return getOrgUnitId() != null ? DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()) : super.getSecuredObject();
    }

    public String getPermissionKey()
    {
        return getOrgUnitId() == null ? "usmaSessionProtocolGiaReportViewPermissionKey" : new OrgUnitSecModel(getOrgUnit()).getPermission("orgUnit_usmaSessionProtocolGiaReportViewPermissionKey");
    }

    private OrgUnit getOrgUnit()
    {
        return dao.getNotNull(OrgUnit.class, getOrgUnitId());
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public Course getCourse()
    {
        return course;
    }

    public void setCourse(Course course)
    {
        this.course = course;
    }

    public List<Group> getGroup()
    {
        return group;
    }

    public void setGroup(List<Group> group)
    {
        this.group = group;
    }

    public List<IdentifiableWrapper<IEntity>> getSourceFilterList()
    {
        return SOURCE_FILTER_LIST;
    }

    public IdentifiableWrapper<IEntity> getSourceFilter()
    {
        return sourceFilter;
    }

    public void setSourceFilter(IdentifiableWrapper<IEntity> sourceFilter)
    {
        this.sourceFilter = sourceFilter;
    }

    public ISelectModel getPpsModel()
    {
        return ppsModel;
    }

    public void setPpsModel(ISelectModel ppsModel)
    {
        this.ppsModel = ppsModel;
    }

    public List<PpsEntry> getChairmanPps()
    {
        return chairmanPps;
    }

    public void setChairmanPps(List<PpsEntry> chairmanPps)
    {
        this.chairmanPps = chairmanPps;
    }

    public List<PpsEntry> getClerkPps()
    {
        return clerkPps;
    }

    public void setClerkPps(List<PpsEntry> clerkPps)
    {
        this.clerkPps = clerkPps;
    }

    public List<PpsEntry> getMembersPps()
    {
        return membersPps;
    }

    public void setMembersPps(List<PpsEntry> membersPps)
    {
        this.membersPps = membersPps;
    }
}
