package ru.tandemservice.uniusma.entity.studentmodularorder;

import ru.tandemservice.uniusma.entity.studentmodularorder.gen.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении не приступившего к занятиям после академического отпуска
 */
public class UsmaExcludeAfterAcadWeekendStuExtract extends UsmaExcludeAfterAcadWeekendStuExtractGen
{
}