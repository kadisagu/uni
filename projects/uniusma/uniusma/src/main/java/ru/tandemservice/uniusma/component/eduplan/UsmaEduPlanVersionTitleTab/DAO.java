/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionTitleTab;

import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaDeveloper;
import ru.tandemservice.uniusma.entity.UsmaEpvBlockTitle;
import ru.tandemservice.uniusma.entity.UsmaQualification;
import ru.tandemservice.uniusma.entity.UsmaSpeciality;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private final static Map<String, String> LEVELS = new HashMap<>();
    static
    {
        LEVELS.put("B", "Бакалавриат");
        LEVELS.put("M", "Магистратура");
        LEVELS.put("S", "Специалитет");
    }

    @Override
    public void prepare(Model model)
    {
        final Long epvId = model.getId();

        model.setEpvBlockModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, "b")
                        .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("b")), value(epvId)));

                return new ListResult<>(builder.createStatement(getSession()).<EppEduPlanVersionBlock>list());
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EppEduPlanVersionBlock block = (EppEduPlanVersionBlock) value;
                return block.getTitle();
            }
        });

        Map<Long, Model.Block> blockMap = new HashMap<>();
        List<EppEduPlanVersionBlock> eduPlanVersionBlocks = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), epvId);
        for (EppEduPlanVersionBlock eduPlanVersionBlock: eduPlanVersionBlocks)
        {
            Long blockId = eduPlanVersionBlock.getId();
            String caption = eduPlanVersionBlock.getTitle();

            UsmaEpvBlockTitle blockTitle = get(UsmaEpvBlockTitle.class, UsmaEpvBlockTitle.block(), eduPlanVersionBlock);
            if (blockTitle == null)
            {
                blockTitle = new UsmaEpvBlockTitle();
                blockTitle.setBlock(eduPlanVersionBlock);
            }

            String level = LEVELS.get(blockTitle.getLevelCode());

            StaticListDataSource<UsmaDeveloper> developerDataSource = new StaticListDataSource<>();
            developerDataSource.addColumn(new SimpleColumn("Номер", UsmaDeveloper.P_NUMBER).setWidth(5).setOrderable(false));
            developerDataSource.addColumn(new SimpleColumn("ФИО", UsmaDeveloper.P_FIO).setWidth(50).setOrderable(false));
            developerDataSource.addColumn(new SimpleColumn("Должность", UsmaDeveloper.P_POST).setOrderable(false));
            developerDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditDeveloper").setPermissionKey("edit_epvBlockDeveloper"));
            developerDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDeveloper").setPermissionKey("delete_epvBlockDeveloper"));
            List<UsmaDeveloper> developers = getList(UsmaDeveloper.class, UsmaDeveloper.block(), eduPlanVersionBlock, UsmaDeveloper.P_NUMBER);
            developerDataSource.setRowList(developers);

            StaticListDataSource<UsmaQualification> qualificationDataSource = new StaticListDataSource<>();
            qualificationDataSource.addColumn(new SimpleColumn("Номер", UsmaQualification.P_NUMBER).setWidth(5).setOrderable(false));
            qualificationDataSource.addColumn(new SimpleColumn("Название", UsmaQualification.P_TITLE).setOrderable(false));
            qualificationDataSource.addColumn(new SimpleColumn("Срок обучения", UsmaQualification.P_DEVELOP_PERIOD).setWidth(20).setOrderable(false));
            qualificationDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditQualification").setPermissionKey("edit_epvBlockQualification"));
            qualificationDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteQualification").setPermissionKey("delete_epvBlockQualification"));
            List<UsmaQualification> qualifications = getList(UsmaQualification.class, UsmaQualification.block(), eduPlanVersionBlock, UsmaQualification.P_NUMBER);
            qualificationDataSource.setRowList(qualifications);

            StaticListDataSource<UsmaSpeciality> specialityDataSource = new StaticListDataSource<>();
            specialityDataSource.addColumn(new SimpleColumn("Номер", UsmaSpeciality.P_NUMBER).setWidth(5).setOrderable(false));
            specialityDataSource.addColumn(new SimpleColumn("Название", UsmaSpeciality.P_TITLE).setOrderable(false));
            specialityDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditSpeciality").setPermissionKey("edit_epvBlockSpeciality"));
            specialityDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteSpeciality").setPermissionKey("delete_epvBlockSpeciality"));
            List<UsmaSpeciality> specialities = getList(UsmaSpeciality.class, UsmaSpeciality.block(), eduPlanVersionBlock, UsmaSpeciality.P_NUMBER);
            specialityDataSource.setRowList(specialities);

            Model.Block block = new Model.Block(blockId, caption, blockTitle, level, developerDataSource, qualificationDataSource, specialityDataSource);

            blockMap.put(blockId, block);
        }

        model.setBlockMap(Collections.unmodifiableMap(blockMap));
    }
}