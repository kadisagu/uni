package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeek;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент разбиения учебного графика (УГМА)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaEduPlanVersionWeekPartGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart";
    public static final String ENTITY_NAME = "usmaEduPlanVersionWeekPart";
    public static final int VERSION_HASH = -1582170700;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION_WEEK = "eduPlanVersionWeek";
    public static final String P_PARTITION_ELEMENT_NUMBER = "partitionElementNumber";
    public static final String L_WEEK_TYPE = "weekType";

    private UsmaEduPlanVersionWeek _eduPlanVersionWeek;     // Неделя строки курса в учебном графике (УГМА)
    private int _partitionElementNumber;     // Номер элемента в разбиении
    private EppWeekType _weekType;     // Тип недели

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Неделя строки курса в учебном графике (УГМА). Свойство не может быть null.
     */
    @NotNull
    public UsmaEduPlanVersionWeek getEduPlanVersionWeek()
    {
        return _eduPlanVersionWeek;
    }

    /**
     * @param eduPlanVersionWeek Неделя строки курса в учебном графике (УГМА). Свойство не может быть null.
     */
    public void setEduPlanVersionWeek(UsmaEduPlanVersionWeek eduPlanVersionWeek)
    {
        dirty(_eduPlanVersionWeek, eduPlanVersionWeek);
        _eduPlanVersionWeek = eduPlanVersionWeek;
    }

    /**
     * @return Номер элемента в разбиении. Свойство не может быть null.
     */
    @NotNull
    public int getPartitionElementNumber()
    {
        return _partitionElementNumber;
    }

    /**
     * @param partitionElementNumber Номер элемента в разбиении. Свойство не может быть null.
     */
    public void setPartitionElementNumber(int partitionElementNumber)
    {
        dirty(_partitionElementNumber, partitionElementNumber);
        _partitionElementNumber = partitionElementNumber;
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип недели. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaEduPlanVersionWeekPartGen)
        {
            setEduPlanVersionWeek(((UsmaEduPlanVersionWeekPart)another).getEduPlanVersionWeek());
            setPartitionElementNumber(((UsmaEduPlanVersionWeekPart)another).getPartitionElementNumber());
            setWeekType(((UsmaEduPlanVersionWeekPart)another).getWeekType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaEduPlanVersionWeekPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaEduPlanVersionWeekPart.class;
        }

        public T newInstance()
        {
            return (T) new UsmaEduPlanVersionWeekPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersionWeek":
                    return obj.getEduPlanVersionWeek();
                case "partitionElementNumber":
                    return obj.getPartitionElementNumber();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersionWeek":
                    obj.setEduPlanVersionWeek((UsmaEduPlanVersionWeek) value);
                    return;
                case "partitionElementNumber":
                    obj.setPartitionElementNumber((Integer) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersionWeek":
                        return true;
                case "partitionElementNumber":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersionWeek":
                    return true;
                case "partitionElementNumber":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersionWeek":
                    return UsmaEduPlanVersionWeek.class;
                case "partitionElementNumber":
                    return Integer.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaEduPlanVersionWeekPart> _dslPath = new Path<UsmaEduPlanVersionWeekPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaEduPlanVersionWeekPart");
    }
            

    /**
     * @return Неделя строки курса в учебном графике (УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart#getEduPlanVersionWeek()
     */
    public static UsmaEduPlanVersionWeek.Path<UsmaEduPlanVersionWeek> eduPlanVersionWeek()
    {
        return _dslPath.eduPlanVersionWeek();
    }

    /**
     * @return Номер элемента в разбиении. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart#getPartitionElementNumber()
     */
    public static PropertyPath<Integer> partitionElementNumber()
    {
        return _dslPath.partitionElementNumber();
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends UsmaEduPlanVersionWeekPart> extends EntityPath<E>
    {
        private UsmaEduPlanVersionWeek.Path<UsmaEduPlanVersionWeek> _eduPlanVersionWeek;
        private PropertyPath<Integer> _partitionElementNumber;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Неделя строки курса в учебном графике (УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart#getEduPlanVersionWeek()
     */
        public UsmaEduPlanVersionWeek.Path<UsmaEduPlanVersionWeek> eduPlanVersionWeek()
        {
            if(_eduPlanVersionWeek == null )
                _eduPlanVersionWeek = new UsmaEduPlanVersionWeek.Path<UsmaEduPlanVersionWeek>(L_EDU_PLAN_VERSION_WEEK, this);
            return _eduPlanVersionWeek;
        }

    /**
     * @return Номер элемента в разбиении. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart#getPartitionElementNumber()
     */
        public PropertyPath<Integer> partitionElementNumber()
        {
            if(_partitionElementNumber == null )
                _partitionElementNumber = new PropertyPath<Integer>(UsmaEduPlanVersionWeekPartGen.P_PARTITION_ELEMENT_NUMBER, this);
            return _partitionElementNumber;
        }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return UsmaEduPlanVersionWeekPart.class;
        }

        public String getEntityName()
        {
            return "usmaEduPlanVersionWeekPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
