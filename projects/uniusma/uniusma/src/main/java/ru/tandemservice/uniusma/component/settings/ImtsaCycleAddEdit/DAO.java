/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.settings.ImtsaCycleAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setCyclePlanStructureRel(model.getId() == null ? new UsmaImtsaCyclePlanStructureRel() : get(UsmaImtsaCyclePlanStructureRel.class, model.getId()));
        if (model.isEditForm())
        {
            model.setGos2(model.getCyclePlanStructureRel().isGos2());
        }

        final String cyclesCode = model.isGos2() ? EppPlanStructureCodes.GOS_CYCLES : EppPlanStructureCodes.FGOS_CYCLES;
        model.setCycleModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EppPlanStructure> planStructureList = new DQLSelectBuilder()
                        .fromEntity(EppPlanStructure.class, "ps")
                        .column(property("ps"))
                        .joinEntity("ps", DQLJoinType.inner, EppPlanStructure.class, "parent", eq(property("ps", EppPlanStructure.parent()), property("parent")))
                        .where(eq(property("parent", EppPlanStructure.code()), value(cyclesCode)))
                        .where(likeUpper(property("ps", EppPlanStructure.title()), value(CoreStringUtils.escapeLike(filter))))
                        .createStatement(getSession())
                        .list();

                return new ListResult<>(planStructureList);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EppPlanStructure planStructure = (EppPlanStructure) value;
                return planStructure.getTitle();
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (!model.isEditForm())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(UsmaImtsaCyclePlanStructureRel.class, "c")
                    .where(eq(DQLFunctions.upper(property("c", UsmaImtsaCyclePlanStructureRel.imtsaCycle())), value(model.getCyclePlanStructureRel().getImtsaCycle().toUpperCase())))
                    .where(eq(property("c", UsmaImtsaCyclePlanStructureRel.gos2()), value(model.isGos2())));

            if (existsEntity(builder.buildQuery()))
            {
                errors.add("Цикл ИМЦА должен быть уникален среди циклов " + (model.isGos2() ? "ГОС2" : "ФГОС") + ".", "imtsaCycle");
            }
        }
    }
}