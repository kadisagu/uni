/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public class UsmaExcludeDebtStuExtractPrint implements IPrintFormCreator<UsmaExcludeDebtStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, UsmaExcludeDebtStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        short i = 1;
        modifier.put("debtDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDebtDate()));
        modifier.put("contractNumber", extract.getContractNumber());
        modifier.put("contractDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getContractDate()));
        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getExcludeDate()));
        modifier.put("stopPayments", (extract.isStopGrantsPaying() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") + " отменить выплату академической стипендии с " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingDate()) + " г." ) : ""));
        modifier.put("giveDiploma", (extract.isGiveDiploma() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") + " выдать диплом о незаконченном высшем образовании.") : ""));

        boolean budget = extract.getEntity().getCompensationType().isBudget();
        modifier.put("compensationTypeStr_G", budget ? "бюджетной основы" : "внебюджетной основы");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
