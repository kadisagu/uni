/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.validator;

/**
 * Валидатор целого числа: проверяет, что число меньше или равно заданному.
 * @author Alexander Zhebko
 * @since 18.07.2013
 */
public class LEIntegerValidator extends Validator<Integer>
{
    private int maxValue;

    public LEIntegerValidator(int maxValue)
    {
        this.maxValue = maxValue;
    }

    @Override
    protected boolean validateValue(Integer value)
    {
        return value <= maxValue;
    }

    @Override
    public String getInvalidMessage()
    {
        return "Значение больше " + maxValue + ".";
    }
}