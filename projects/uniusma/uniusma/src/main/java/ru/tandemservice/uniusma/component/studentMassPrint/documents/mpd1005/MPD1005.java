/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1005;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1005.Add.DAO;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1005.Add.Model;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.AbstractStudentMassPrint;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class MPD1005 extends AbstractStudentMassPrint<Model>
{
    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void initModel4Student(Student student, int documentNumber, Model model)
    {
        super.initModel4Student(student, documentNumber, model);

        DAO dao = new DAO();
        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        IdentityCard identityCard = student.getPerson().getIdentityCard();

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.DATIVE).toUpperCase());
        model.setStudentTitleStrIminit(PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.NOMINATIVE).toUpperCase());
        model.setCourse(student.getCourse());
    }

}
