/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e26.AddEdit;

import ru.tandemservice.uniusma.entity.catalog.codes.StudentExtractTypeCodes;

/**
 * @author Denis Perminov
 * @since 13.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e26.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e26.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;

        boolean is126 = m.getExtractType().getCode().equals(StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER);

        m.getExtract().setPrintPreambula(is126);
        m.setPreambulaVisible(is126);
    }
}
