{orderType}\par\par
\keep\keepn\fi709\qj
{reason} \par\fi0\par
приказываю:\par
\fi709\qj
1. {graduateStudent_D} {year} года {orgUnit_G} (группа {group}) {developForm_GF} формы обучения {compensationTypeStr_G_Alt} {fio} выдать дубликат приложения к диплому {excellentDiploma}№ {diplomaNumber} взамен утерянного.\par\par
Основание: {listBasics}\par\fi0