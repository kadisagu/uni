/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e13.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 22.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e13.Pub.Model
{
    boolean _freeAttendance;
    WeekendChildStuExtractUsmaExt _extUsmaExt;

    public boolean isFreeAttendance()
    {
        return _freeAttendance;
    }

    public void setFreeAttendance(boolean freeAttendance)
    {
        _freeAttendance = freeAttendance;
    }

    public WeekendChildStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(WeekendChildStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
