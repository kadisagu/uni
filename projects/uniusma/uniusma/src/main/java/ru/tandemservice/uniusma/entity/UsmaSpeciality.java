package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Специальность
 */
public class UsmaSpeciality extends UsmaSpecialityGen
{
    public UsmaSpeciality()
    {

    }

    public UsmaSpeciality(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}