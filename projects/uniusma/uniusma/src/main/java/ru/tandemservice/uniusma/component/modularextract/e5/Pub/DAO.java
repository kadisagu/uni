/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e5.Pub;

import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.TransferDevFormStuExtractUsmaExtGen;

import java.util.List;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e5.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e5.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к задолженностям
        List<StuExtractToDebtRelation> debtsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(m.getExtract());

        StringBuilder builder = new StringBuilder();
        for (StuExtractToDebtRelation rel : debtsList)
        {
            builder.append(builder.length() > 0 ? ",</li>" : "<ol> ").append("<li type=\"1\">");
            builder.append(rel.getDiscipline()).append(" (").append(rel.getHours()).append(" ч.) - ");
            builder.append(rel.getControlAction());
        }
        m.setDebtsList(builder.append(builder.length() > 0 ? "</li> </ol>" : "").toString());
        // обращаемся к расширению выписки
        TransferDevFormStuExtractUsmaExt extractExt = getByNaturalId(new TransferDevFormStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        m.setExtUsmaExt(extractExt);
    }
}
