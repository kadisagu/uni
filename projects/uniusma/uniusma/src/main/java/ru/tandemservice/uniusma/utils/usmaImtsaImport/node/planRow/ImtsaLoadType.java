/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniusma.entity.catalog.UsmaLoadType;
import ru.tandemservice.uniusma.entity.catalog.codes.UsmaLoadTypeCodes;


/**
 * @author Alexander Zhebko
 * @since 05.11.2013
 */
public enum ImtsaLoadType
{
    LECTURE(EppALoadType.class, EppALoadTypeCodes.TYPE_LECTURES),
    PRACTICE(EppALoadType.class, EppALoadTypeCodes.TYPE_PRACTICE),
    LABS(EppALoadType.class, EppALoadTypeCodes.TYPE_LABS),
    LECTURE_INTER(UsmaLoadType.class, UsmaLoadTypeCodes.TYPE_LECTURES_INTER),
    PRACTICE_INTER(UsmaLoadType.class, UsmaLoadTypeCodes.TYPE_PRACTICE_INTER),
    LABS_INTER(UsmaLoadType.class, UsmaLoadTypeCodes.TYPE_LABS_INTER),
    SELF_WORK(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK),
    EXAM_HOURS(UsmaLoadType.class, UsmaLoadTypeCodes.TYPE_EXAM_HOURS);

    private Class<? extends ICatalogItem> _typeClass;
    private String _code;
    private boolean _inter;
    private boolean _control;


    private ImtsaLoadType(Class<? extends ICatalogItem> typeClass, String code)
    {
        _typeClass = typeClass;
        _code = code;
        _inter = false;
        _control = code.equals(EppLoadType.FULL_CODE_CONTROL);
    }

    private ImtsaLoadType(Class<? extends ICatalogItem> typeClass, String code, boolean inter)
    {
        _typeClass = typeClass;
        _code = code;
        _inter = inter;
    }

    public Class<? extends ICatalogItem> getTypeClass() { return _typeClass; }
    public String getCode() { return _code; }
    public boolean isInter() { return _inter; }
    public boolean isControl() { return _control; }
}
