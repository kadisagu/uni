/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.SystemAction.ui.Pub;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.w3c.dom.Document;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ImtsaReImport.UsmaEduPlanImtsaReImport;
import ru.tandemservice.uniusma.base.bo.UsmaSystemAction.UsmaSystemActionManager;
import ru.tandemservice.uniusma.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;
import ru.tandemservice.uniusma.utils.imtsa.ImtsaLoader;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.ImtsaFile;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaPlanRow;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.parser.ImtsaParser;

import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public class UsmaSystemActionPubAddon extends UIAddon
{
    public UsmaSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
    
    public void onClickImportStudents() throws Exception
    {
        Debug.stopLogging();
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            UsmaSystemActionManager.instance().dao().saveStudents();
        }
        finally
        {
            eventLock.release();
            Debug.resumeLogging();
        }
    }

    public void onClickCreateEmpLabourContract() throws Exception
    {
        Debug.stopLogging();
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            UsmaSystemActionManager.instance().dao().doCreateEmpLabourContract();
        }
        finally
        {
            eventLock.release();
            Debug.resumeLogging();
        }
    }

    public void onClickLoadDataFromImtsa()
    {
        ImtsaLoader.load();
    }
    public void onClickReImportImtsa()
    {
        getActivationBuilder().asDesktopRoot(UsmaEduPlanImtsaReImport.class).activate();
    }



    private static void processCurrentVersionBlocks(List<Long> blockIds, boolean global, Map<Long, UsmaImtsaXml> programSpecImtsaXmlMap, List<String> errorList)
    {
        // выключаем системное логирование, чтобы не тормозило
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging();

        try
        {
            EppEduPlanVersionBlock root;
            List<EppEduPlanVersionBlock> blocks = DataAccessServices.dao().getList(EppEduPlanVersionBlock.class, blockIds);
            List<EppEduPlanVersionBlock> newBlocks = blocks;

            if (global)
            {
                root = CollectionUtils.find(blocks, block -> block instanceof EppEduPlanVersionRootBlock);

                blocks.remove(root);
                boolean rootFromImtsa = programSpecImtsaXmlMap.containsKey(rootBlockSpecializationId);
                if (!rootFromImtsa)
                {
                    // если основной блок не был импортирован из ИМЦА, не трогаем не импортированные
                    for (EppEduPlanVersionBlock block : new ArrayList<>(blocks))
                        if (!programSpecImtsaXmlMap.containsKey(((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization().getId()))
                            blocks.remove(block);
                }

                newBlocks = new ArrayList<>();
                if (rootFromImtsa)
                {
                    final Long rootId = root.getId();
                    EppEduPlanVersionRootBlock newRoot = new EppEduPlanVersionRootBlock();
                    newRoot.setEduPlanVersion(root.getEduPlanVersion());
                    newBlocks.add(newRoot);

                    DataAccessServices.dao().doInTransaction(
                            session -> new DQLDeleteBuilder(EppEduPlanVersionRootBlock.class)
                                    .where(eq(property(EppEduPlanVersionRootBlock.id()), value(rootId)))
                                    .createStatement(session).execute()
                    );
                }

                for (final EppEduPlanVersionBlock block : blocks)
                {
                    newBlocks.add(new EppEduPlanVersionSpecializationBlock(block.getEduPlanVersion(), ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization()));
                    DataAccessServices.dao().doInTransaction(
                            session -> new DQLDeleteBuilder(EppEduPlanVersionBlock.class)
                                    .where(eq(property(EppEduPlanVersionBlock.id()), value(block.getId())))
                                    .createStatement(session).execute()
                    );
                }
            }

            for (final EppEduPlanVersionBlock block : newBlocks)
            {
                if (global)
                    DataAccessServices.dao().doInTransaction(session -> session.save(block));

                UsmaImtsaXml xml = programSpecImtsaXmlMap.get(block instanceof EppEduPlanVersionRootBlock ? rootBlockSpecializationId : ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization().getId());
                if (xml == null)
                {
                    continue;
                }

                if (global)
                    DataAccessServices.dao().save(new UsmaImtsaXml(block, xml.getFileName(), xml.getEncoding(), xml.getXml(), xml.getNumber()));

                String xmlFileName = xml.getFileName();
                File file = null;
                boolean secondGosGeneration = block.getEduPlanVersion().getEduPlan().getGeneration().getNumber() == 2;
                try
                {
                    Document document = null;
                    if (xml.getEncoding() != null)
                    {
                        try
                        {
                            file = File.createTempFile("usmaTemp-", "xml");
                            FileUtils.writeStringToFile(file, new String(xml.getXml()), xml.getEncoding());
                            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                            DocumentBuilder db = dbf.newDocumentBuilder();
                            document = db.parse(file);
                            document.getDocumentElement().normalize();

                        }
                        catch (Exception e)
                        {
                            throw new ApplicationException("Неверная структура xml в файле».");
                        }

                    }
                    else
                    {
                        // костыль, нужно для повторной загрузки файлов ИМЦА
                        boolean successXmlParse = false;
                        for (String encoding : Arrays.asList("UTF-8", "windows-1251"))
                        {
                            if (successXmlParse)
                            {
                                break;
                            }
                            try
                            {
                                file = File.createTempFile("usmaTemp-", "xml");
                                FileUtils.writeStringToFile(file, new String(xml.getXml()), encoding);
                                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                                DocumentBuilder db = dbf.newDocumentBuilder();
                                document = db.parse(file);
                                document.getDocumentElement().normalize();
                                successXmlParse = true;

                            }
                            catch (Exception e)
                            {
                                // continue;
                            }
                        }

                        if (!successXmlParse)
                        {
                            throw new ApplicationException("Неверная структура xml в файле».");
                        }
                    }

                    ImtsaParser parser = ImtsaParser.get(document, secondGosGeneration);
                    parser.parse();


                    String blockError = null;
                    if (parser.hasErrors())
                    {
                        StringBuilder errorBuilder = new StringBuilder();
                        for (String error : parser.getErrors())
                        {
                            errorBuilder.append(error).append("\n");
                        }

                        blockError = errorBuilder.toString();
                    }

                    ImtsaFile imtsa = parser.getResult();
                    Map<String, EppPlanStructure> defaultPartsMap = new HashMap<>();
                    for (EppPlanStructure planStructure : UsmaSystemActionManager.instance().dao().getParts(secondGosGeneration))
                    {
                        defaultPartsMap.put(planStructure.getShortTitle(), planStructure);
                    }

                    Map<String, EppPlanStructure> defaultCyclesMap = new HashMap<>();
                    for (UsmaImtsaCyclePlanStructureRel rel : UsmaSystemActionManager.instance().dao().getImtsaCycles(secondGosGeneration))
                    {
                        defaultCyclesMap.put(rel.getImtsaCycle(), rel.getPlanStructure());
                    }

                    Map<String, Set<String>> cycle2PartsMap = new TreeMap<>();
                    for (Map.Entry<String, ImtsaPlanRow> cycleEntry : imtsa.getRootPlanRow().getChildMap().entrySet())
                    {
                        String cycleId = cycleEntry.getKey();
                        Set<String> parts = new TreeSet<>();
                        ru.tandemservice.uniusma.component.eduplan.UsmaImtsaImport.Controller.addParts(cycleEntry.getValue().getChildMap(), cycleId, parts);
                        cycle2PartsMap.put(cycleId, parts);
                    }


                    Map<String, String> abbreviation2TitleMap = imtsa.getTitle().getCycles().getAbbreviation2TitleMap();
                    Map<String, EppPlanStructure> cyclesMap = new LinkedHashMap<>();
                    Map<String, EppPlanStructure> partsMap = new LinkedHashMap<>();
                    for (Map.Entry<String, Set<String>> cycleEntry : cycle2PartsMap.entrySet())
                    {
                        String cycleAbbr = cycleEntry.getKey();
                        String cycleTitle = abbreviation2TitleMap.get(cycleAbbr);
                        EppPlanStructure cycleValue = defaultCyclesMap.get(cycleTitle);
                        if (cycleValue == null)
                        {
                            cycleValue = defaultCyclesMap.get(cycleAbbr);
                        }
                        cyclesMap.put(cycleAbbr, cycleValue);

                        for (String part : cycleEntry.getValue())
                        {
                            partsMap.put(part, defaultPartsMap.get(part.substring(part.lastIndexOf(".") + 1)));
                        }
                    }

                    List<String> errors = new ArrayList<>();
                    IImtsaImportDAO.instance.get().saveImtsa(block, imtsa, cyclesMap, partsMap, errors);
                    if (!errors.isEmpty())
                    {
                        String error = StringUtils.join(new TreeSet<>(errors), "\n");
                        blockError = blockError == null ? error : blockError + "\n" + error;

                        UsmaSystemActionManager.instance().dao().saveOrRewriteImportLog(block, blockError);
                    }

                }
                catch (Exception e)
                {
                    errorList.add("Произошла ошибка при импорте файла " + xmlFileName + ".");

                }
                finally
                {
                    file.delete();
                }
            }
        }
        finally
        {
            // включаем системное логирование
            eventLock.release();
            Debug.resumeLogging();
        }
    }

    private static final long rootBlockSpecializationId = -1L; // для совместимости
    public static void doReImportImtsa(@Nullable Collection<Long> blockIds, final boolean global)
    {
        final List<String> errorList = new ArrayList<>();
        //  version -> specialization -> xml
        final Map<Long, Map<Long, UsmaImtsaXml>> versionEduHSImtsaXmlMap = SafeMap.get(HashMap.class);
        for (UsmaImtsaXml imtsaXml : UsmaSystemActionManager.instance().dao().getXmlList(blockIds))
        {
            EppEduPlanVersionBlock block = imtsaXml.getBlock();
            versionEduHSImtsaXmlMap.get(block.getEduPlanVersion().getId()).put(block instanceof EppEduPlanVersionRootBlock ? rootBlockSpecializationId : ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization().getId(), imtsaXml);
        }

        final Map<Long, String> versionStateCodeMap = new HashMap<>();
        DQLSelectBuilder versionStateCodeBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, "b")
                .column(property("b", EppEduPlanVersionBlock.eduPlanVersion().id()))
                .column(property("b", EppEduPlanVersionBlock.eduPlanVersion().state().code()))
                .where(in(property("b", EppEduPlanVersionBlock.id()), blockIds));

        for (Object[] row : DataAccessServices.dao().<Object[]>getList(versionStateCodeBuilder))
        {
            versionStateCodeMap.put((Long) row[0], (String) row[1]);
        }

        final Map<EppEduPlanVersion, List<EppEduPlanVersionBlock>> versionBlockMap = SafeMap.get(ArrayList.class);
        for (EppEduPlanVersionBlock block : DataAccessServices.dao().getList(EppEduPlanVersionBlock.class, blockIds))
        {
            versionBlockMap.get(block.getEduPlanVersion()).add(block);
        }

        final int size = versionBlockMap.size();
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                int i = 1;

                for (Map.Entry<EppEduPlanVersion, List<EppEduPlanVersionBlock>> versionEntry : versionBlockMap.entrySet())
                {
                    try
                    {
                        if (!versionStateCodeMap.get(versionEntry.getKey().getId()).equals(EppState.STATE_FORMATIVE))
                        {
                            errorList.add("Состояние версии УП " + versionEntry.getKey().getTitle() + " запрещает редактирование.");
                            state.setCurrentValue(Math.round(100 * (i++) / size));
                            continue;
                        }

                        List<Long> ids = new ArrayList<>(UniBaseDao.ids(versionEntry.getValue()));
                        processCurrentVersionBlocks(ids, global, versionEduHSImtsaXmlMap.get(versionEntry.getKey().getId()), errorList);

                    }
                    catch (RuntimeException e)
                    {
                        errorList.add("Произошла ошибка при импорте версии УП " + versionEntry.getKey().getTitle() + ".");

                    }
                    finally
                    {
                        state.setCurrentValue(Math.round(100 * (i++) / size));
                    }
                }

                for (String error : errorList)
                {
                    ContextLocal.getErrorCollector().add(error);
                }

                return new ProcessResult("Повторная загрузка файлов ИМЦА успешно проведена.");
            }
        };

        new BackgroundProcessHolder().start("Повторная загрузка файлов ИМЦА", process, ProcessDisplayMode.percent);
    }

    public void onClickChangeStudentLogins()
    {
        UsmaSystemActionManager.instance().dao().changeStudentLogins();
    }
}
