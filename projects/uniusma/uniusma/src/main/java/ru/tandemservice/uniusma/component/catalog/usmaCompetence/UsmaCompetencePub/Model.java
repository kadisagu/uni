/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.catalog.usmaCompetence.UsmaCompetencePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Model extends DefaultCatalogPubModel<UsmaCompetence>
{
}