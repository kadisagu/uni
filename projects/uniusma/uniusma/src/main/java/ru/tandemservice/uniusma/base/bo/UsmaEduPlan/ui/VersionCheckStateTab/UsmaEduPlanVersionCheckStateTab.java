/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.VersionCheckStateTab;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Alexander Zhebko
 * @since 06.12.2013
 */
@Configuration
public class UsmaEduPlanVersionCheckStateTab extends BusinessComponentManager
{
}