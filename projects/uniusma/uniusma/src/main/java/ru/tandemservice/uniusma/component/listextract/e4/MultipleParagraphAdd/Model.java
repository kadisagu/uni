/* $Id$ */
package ru.tandemservice.uniusma.component.listextract.e4.MultipleParagraphAdd;

import java.util.Date;

/**
 * @author Andrey Avetisov
 * @since 26.12.2014
 */
public class Model extends ru.tandemservice.movestudent.component.listextract.e4.MultipleParagraphAdd.Model
{
    private Date _excludeDate;

    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    public void setExcludeDate(Date excludeDate)
    {
        _excludeDate = excludeDate;
    }
}
