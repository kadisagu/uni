/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd3.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.component.studentMassPrint.MassPrintUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class DAO extends ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.DAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        List<Student> studentList = model.getStudentList();

        Student firstStudent = studentList.get(0);
        model.setFirstStudent(firstStudent);
        model.setWarningMessage("");
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");

        model.setDepartmentList(Arrays.asList(
                new IdentifiableWrapper(0L, "дневном"),
                new IdentifiableWrapper(1L, "вечернем")
        ));

        List<String> postList = new ArrayList<>();
        Set<OrgUnit> formativeOrgUnits = studentList.stream().map(student -> student.getEducationOrgUnit().getFormativeOrgUnit()).collect(Collectors.toSet());
        MassPrintUtil.findManagers(
                formativeOrgUnits,
                (title, first) -> {
                    postList.add(title);
                    if (first) model.setManagerPostTitle(title);
                },
                (fio, first) -> {
                    if (first) model.setManagerFio(fio);
                });
        model.setManagerPostList(postList);
        if (formativeOrgUnits.size() > 1)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "Студенты принадлежат к разным формирующим подразделениям.");
        model.setDisplayWarning(model.getWarningBuilder().length() > 0);
    }
}
