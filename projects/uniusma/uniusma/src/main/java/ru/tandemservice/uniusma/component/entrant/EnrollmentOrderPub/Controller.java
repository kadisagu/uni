/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.entrant.EnrollmentOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uniec.IUniecComponents;

/**
 * @author vip_delete
 * @since 23.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.order.EnrollmentOrderPub.Model baseModel = (ru.tandemservice.uniec.component.order.EnrollmentOrderPub.Model) component.getModel(IUniecComponents.ENROLLMENT_ORDER_PUB);
        Model model = getModel(component);

        getDao().prepare(model, baseModel.getOrder());
    }
}
