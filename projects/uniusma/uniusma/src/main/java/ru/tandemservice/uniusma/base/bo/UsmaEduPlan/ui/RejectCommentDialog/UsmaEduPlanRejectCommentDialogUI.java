package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.RejectCommentDialog;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.UsmaEduPlanManager;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;


/**
 * @author ilunin
 * @since 13.10.2014
 */
@Input({
    @Bind(key = "eduPlanVersionId", binding = "eduPlanVersionId", required = true)
})
public class UsmaEduPlanRejectCommentDialogUI extends UIPresenter
{
    private Long _eduPlanVersionId;
    private String _comment;
    private UsmaEpvCheckState _checkState;

    public Long getEduPlanVersionId()
    {
        return _eduPlanVersionId;
    }

    public void setEduPlanVersionId(Long eduPlanVersionId)
    {
        this._eduPlanVersionId = eduPlanVersionId;
    }

    public String getComment()
    {
        return _comment;
    }

    public void setComment(String comment)
    {
        this._comment = comment;
    }

    @Override
    public void onComponentRefresh(){
        _checkState = UsmaEduPlanManager.instance().dao().getEpvCheckState(_eduPlanVersionId);
    }

    public void onClickApply()
    {
        _checkState.setRejectedComment(_comment);
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
        deactivate();
    }

}
