package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.4")
		};
    }

	private static Long getLong(ResultSet rs, int column) throws SQLException
	{
		Number value = (Number) rs.getObject(column);
		return null == value ? null : Long.valueOf(value.longValue());
	}

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaStudentOtherInfo

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmastudentotherinfo_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("healthgroup_id", DBType.LONG), 
				new DBColumn("dispensary_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaStudentOtherInfo");

			Statement statement = tool.getConnection().createStatement();
			statement.execute("SELECT student_id, healthgroup_id, dispensary_p FROM dbo.usmastudentvaccineinfo_t WHERE healthgroup_id IS NOT NULL OR dispensary_p = 1");

			PreparedStatement insertOther = tool.prepareStatement("insert into usmastudentotherinfo_t (id, discriminator, student_id, healthgroup_id, dispensary_p) values (?, ?, ?, ?, ?)");
			ResultSet selectRow = statement.getResultSet();
			int counter = 0;
			while (selectRow.next())
			{
				Long id = EntityIDGenerator.generateNewId(entityCode);
				Long studentId = selectRow.getLong(1);
				Long healthgroupId = getLong(selectRow, 2);
				Boolean dispensary = selectRow.getBoolean(3);

				insertOther.setLong(1, id);
				insertOther.setShort(2, entityCode);
				insertOther.setLong(3, studentId);
				insertOther.setObject(4, healthgroupId);
				insertOther.setBoolean(5, dispensary);
				insertOther.addBatch();
				if(++counter % 1000 == 0)
					insertOther.executeBatch();
			}
			insertOther.executeBatch();
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaStudentVaccineInfo

		// удалено свойство healthGroup
		{
			// удалить колонку
			tool.dropColumn("usmastudentvaccineinfo_t", "healthgroup_id");
		}

		// удалено свойство dispensary
		{
			// удалить колонку
			tool.dropColumn("usmastudentvaccineinfo_t", "dispensary_p");
		}


    }
}