package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendChildOutStuExtractUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<WeekendChildOutStuExtractUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt";
    public static final String ENTITY_NAME = "weekendChildOutStuExtractUsmaExt";
    public static final int VERSION_HASH = -2140634978;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_STOP_COMPENSATION_PAYMENT = "stopCompensationPayment";
    public static final String P_RESTART_GRANT_PAYING = "restartGrantPaying";
    public static final String P_RESTART_GRANT_PAYING_DATE = "restartGrantPayingDate";

    private WeekendChildOutStuExtract _extractExt;     // Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
    private Boolean _stopCompensationPayment = false;     // Прекратить выплату компенсации
    private Boolean _restartGrantPaying = false;     // Возобновить выплату стипендии
    private Date _restartGrantPayingDate;     // Дата возобновления выплаты стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public WeekendChildOutStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(WeekendChildOutStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Прекратить выплату компенсации.
     */
    public Boolean getStopCompensationPayment()
    {
        return _stopCompensationPayment;
    }

    /**
     * @param stopCompensationPayment Прекратить выплату компенсации.
     */
    public void setStopCompensationPayment(Boolean stopCompensationPayment)
    {
        dirty(_stopCompensationPayment, stopCompensationPayment);
        _stopCompensationPayment = stopCompensationPayment;
    }

    /**
     * @return Возобновить выплату стипендии.
     */
    public Boolean getRestartGrantPaying()
    {
        return _restartGrantPaying;
    }

    /**
     * @param restartGrantPaying Возобновить выплату стипендии.
     */
    public void setRestartGrantPaying(Boolean restartGrantPaying)
    {
        dirty(_restartGrantPaying, restartGrantPaying);
        _restartGrantPaying = restartGrantPaying;
    }

    /**
     * @return Дата возобновления выплаты стипендии.
     */
    public Date getRestartGrantPayingDate()
    {
        return _restartGrantPayingDate;
    }

    /**
     * @param restartGrantPayingDate Дата возобновления выплаты стипендии.
     */
    public void setRestartGrantPayingDate(Date restartGrantPayingDate)
    {
        dirty(_restartGrantPayingDate, restartGrantPayingDate);
        _restartGrantPayingDate = restartGrantPayingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof WeekendChildOutStuExtractUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((WeekendChildOutStuExtractUsmaExt)another).getExtractExt());
            }
            setStopCompensationPayment(((WeekendChildOutStuExtractUsmaExt)another).getStopCompensationPayment());
            setRestartGrantPaying(((WeekendChildOutStuExtractUsmaExt)another).getRestartGrantPaying());
            setRestartGrantPayingDate(((WeekendChildOutStuExtractUsmaExt)another).getRestartGrantPayingDate());
        }
    }

    public INaturalId<WeekendChildOutStuExtractUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<WeekendChildOutStuExtractUsmaExtGen>
    {
        private static final String PROXY_NAME = "WeekendChildOutStuExtractUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(WeekendChildOutStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof WeekendChildOutStuExtractUsmaExtGen.NaturalId) ) return false;

            WeekendChildOutStuExtractUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendChildOutStuExtractUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendChildOutStuExtractUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new WeekendChildOutStuExtractUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "stopCompensationPayment":
                    return obj.getStopCompensationPayment();
                case "restartGrantPaying":
                    return obj.getRestartGrantPaying();
                case "restartGrantPayingDate":
                    return obj.getRestartGrantPayingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((WeekendChildOutStuExtract) value);
                    return;
                case "stopCompensationPayment":
                    obj.setStopCompensationPayment((Boolean) value);
                    return;
                case "restartGrantPaying":
                    obj.setRestartGrantPaying((Boolean) value);
                    return;
                case "restartGrantPayingDate":
                    obj.setRestartGrantPayingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "stopCompensationPayment":
                        return true;
                case "restartGrantPaying":
                        return true;
                case "restartGrantPayingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "stopCompensationPayment":
                    return true;
                case "restartGrantPaying":
                    return true;
                case "restartGrantPayingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return WeekendChildOutStuExtract.class;
                case "stopCompensationPayment":
                    return Boolean.class;
                case "restartGrantPaying":
                    return Boolean.class;
                case "restartGrantPayingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendChildOutStuExtractUsmaExt> _dslPath = new Path<WeekendChildOutStuExtractUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendChildOutStuExtractUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getExtractExt()
     */
    public static WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Прекратить выплату компенсации.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getStopCompensationPayment()
     */
    public static PropertyPath<Boolean> stopCompensationPayment()
    {
        return _dslPath.stopCompensationPayment();
    }

    /**
     * @return Возобновить выплату стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getRestartGrantPaying()
     */
    public static PropertyPath<Boolean> restartGrantPaying()
    {
        return _dslPath.restartGrantPaying();
    }

    /**
     * @return Дата возобновления выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getRestartGrantPayingDate()
     */
    public static PropertyPath<Date> restartGrantPayingDate()
    {
        return _dslPath.restartGrantPayingDate();
    }

    public static class Path<E extends WeekendChildOutStuExtractUsmaExt> extends EntityPath<E>
    {
        private WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract> _extractExt;
        private PropertyPath<Boolean> _stopCompensationPayment;
        private PropertyPath<Boolean> _restartGrantPaying;
        private PropertyPath<Date> _restartGrantPayingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getExtractExt()
     */
        public WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Прекратить выплату компенсации.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getStopCompensationPayment()
     */
        public PropertyPath<Boolean> stopCompensationPayment()
        {
            if(_stopCompensationPayment == null )
                _stopCompensationPayment = new PropertyPath<Boolean>(WeekendChildOutStuExtractUsmaExtGen.P_STOP_COMPENSATION_PAYMENT, this);
            return _stopCompensationPayment;
        }

    /**
     * @return Возобновить выплату стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getRestartGrantPaying()
     */
        public PropertyPath<Boolean> restartGrantPaying()
        {
            if(_restartGrantPaying == null )
                _restartGrantPaying = new PropertyPath<Boolean>(WeekendChildOutStuExtractUsmaExtGen.P_RESTART_GRANT_PAYING, this);
            return _restartGrantPaying;
        }

    /**
     * @return Дата возобновления выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt#getRestartGrantPayingDate()
     */
        public PropertyPath<Date> restartGrantPayingDate()
        {
            if(_restartGrantPayingDate == null )
                _restartGrantPayingDate = new PropertyPath<Date>(WeekendChildOutStuExtractUsmaExtGen.P_RESTART_GRANT_PAYING_DATE, this);
            return _restartGrantPayingDate;
        }

        public Class getEntityClass()
        {
            return WeekendChildOutStuExtractUsmaExt.class;
        }

        public String getEntityName()
        {
            return "weekendChildOutStuExtractUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
