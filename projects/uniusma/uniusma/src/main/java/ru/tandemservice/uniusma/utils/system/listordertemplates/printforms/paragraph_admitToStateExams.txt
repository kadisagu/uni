\qc{parNumberRoman}. Допустить к Государственной итоговой аттестации следующих студентов {course} курса 
{orgUnit_G} {educationType} \'ab{educationOrgUnit}\'bb 
форма обучения \'ab{developForm}\'bb группа \'ab{group}\'bb согласно списку:
\par \fi350\qj {STUDENT_LIST}
\par \fi0\ql Всего {extractCount} ({extractCountStr}) {people}.\par\par