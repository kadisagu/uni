/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaImtsaXmlTab;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.09.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        List<Model.Block> blocks = new ArrayList<>();
        List<UsmaImtsaXml> xmlList = getList(UsmaImtsaXml.class, UsmaImtsaXml.block().eduPlanVersion().id(), model.getId());
        for (UsmaImtsaXml xml: xmlList)
        {
            Model.Block block = new Model.Block(xml.getBlock().getTitle(), xml.getFileName(), new String(xml.getXml()));
            blocks.add(block);
        }

        model.setBlocks(blocks);
    }
}