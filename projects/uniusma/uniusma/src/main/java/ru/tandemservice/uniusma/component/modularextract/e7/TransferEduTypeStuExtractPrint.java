/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e7;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.movestudent.entity.TransferEduTypeStuExtract;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferEduTypeStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.TransferEduTypeStuExtractUsmaExtGen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 29.05.2014
 */
public class TransferEduTypeStuExtractPrint implements IPrintFormCreator<TransferEduTypeStuExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, TransferEduTypeStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();
        RtfTableModifier table = new RtfTableModifier();

        TransferEduTypeStuExtractUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new TransferEduTypeStuExtractUsmaExtGen.NaturalId(extract));
        if (null != extractExt && null != extractExt.getAdmissionDate())
        {
            modifier.put("entryIntoForceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getAdmissionDate()));
        }
        else
            tagsToDel.add("entryIntoForceDate");

        short number = 1;

        if (null != extractExt && extractExt.isStopGrantsPaying())
        {
            modifier.put("stopGrantPayingStr", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " отменить выплату академической стипендии с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getStopGrantsPayingDate()) + " г.");
        }
        else
            tagsToDel.add("stopGrantPayingStr");

        if (null != extractExt && extractExt.isHasDebts())
        {
            modifier.put("debts", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getDeadline()) + " г.");

            short i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for (StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[]{String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        }
        else
        {
            tagsToDel.add("debts");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        table.modify(document);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}
