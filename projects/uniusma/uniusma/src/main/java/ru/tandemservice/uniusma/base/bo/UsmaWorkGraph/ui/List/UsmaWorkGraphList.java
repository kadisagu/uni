/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.UsmaWorkGraphDSHandler;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.Pub.UsmaWorkGraphPub;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
@Configuration
public class UsmaWorkGraphList extends BusinessComponentManager
{
    public static final String WORK_GRAPH_DS = "workGraphDS";

    public static final String PUPNAG_DS = "pupnagDS";

    public static final String PUPNAG = "pupnag";
    public static final String STATE = "state";
    public static final String DEVELOP_FORM = "developForm";
    public static final String DEVELOP_CONDITION = "developCondition";
    public static final String DEVELOP_TECH = "developTech";
    public static final String DEVELOP_GRID = "developGrid";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(WORK_GRAPH_DS, workGraphsCL(), usmaWorkGraphsDSHandler()))
                .addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
                .create();
    }

    @Bean
    ColumnListExtPoint workGraphsCL()
    {
        return columnListExtPointBuilder(WORK_GRAPH_DS)
                .addColumn(UniEppUtils.stateColumn(UsmaWorkGraph.state().code()))
                .addColumn(publisherColumn("title", UsmaWorkGraph.title()).businessComponent(UsmaWorkGraphPub.class))
                .addColumn(textColumn("stateTitle", UsmaWorkGraph.state().title()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("workGraphDS.delete.alert", UsmaWorkGraph.title())).permissionKey("deleteUsmaWorkGraph_list"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler usmaWorkGraphsDSHandler()
    {
        return new UsmaWorkGraphDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler pupnagDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class).order(EppYearEducationProcess.educationYear().intValue());
    }
}