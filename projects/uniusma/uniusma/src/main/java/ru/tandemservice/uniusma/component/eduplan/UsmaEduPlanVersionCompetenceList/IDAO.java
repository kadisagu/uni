/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionCompetenceList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     *  Увеличить номер компетенции для блока УПв
     * @param id идентификатор компетенции в блоке
     */
    public void doNumberUp(Long id);

    /**
     *  Уменьшить номер компетенции для блока УПв
     * @param id идентификатор компетенции в блоке
     */
    public void doNumberDown(Long id);
}