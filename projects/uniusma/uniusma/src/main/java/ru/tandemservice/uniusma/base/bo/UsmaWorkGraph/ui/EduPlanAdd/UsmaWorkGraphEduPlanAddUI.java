/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.EduPlanAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.IUsmaWorkGraphDAO;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraphId", required = true))
public class UsmaWorkGraphEduPlanAddUI extends UIPresenter
{
    private Long _workGraphId;
    private EduProgramSubject _programSubject;

    public Long getWorkGraphId(){ return _workGraphId; }
    public void setWorkGraphId(Long workGraphId){ _workGraphId = workGraphId; }

    public EduProgramSubject getProgramSubject() { return _programSubject; }
    public void setProgramSubject(EduProgramSubject programSubject) { _programSubject = programSubject; }

    private Map<PairKey<Long, String>, Boolean> _courseInUseMap;
    public boolean isCourseInUse()
    {
        BaseSearchListDataSource eduPlanVersionDS = getConfig().getDataSource(UsmaWorkGraphEduPlanAdd.EDU_PLAN_VERSION_DS);
        Long versionId = ((IEntity) eduPlanVersionDS.getCurrent()).getId();
        String courseCode = eduPlanVersionDS.getCurrentColumn().getName();

        return _courseInUseMap.get(PairKey.create(versionId, courseCode));
    }

    public void setCourseInUse(boolean courseInUseUp)
    {
        BaseSearchListDataSource eduPlanVersionDS = getConfig().getDataSource(UsmaWorkGraphEduPlanAdd.EDU_PLAN_VERSION_DS);
        Long versionId = ((IEntity) eduPlanVersionDS.getCurrent()).getId();
        String courseCode = eduPlanVersionDS.getCurrentColumn().getName();

        _courseInUseMap.put(PairKey.create(versionId, courseCode), courseInUseUp);
    }


    @Override
    public void onComponentRefresh()
    {
        IUsmaWorkGraphDAO dao = UsmaWorkGraphManager.instance().dao();

        _courseInUseMap = dao.getWorkGraphEduPlanVersionCourses(_workGraphId);

        BaseSearchListDataSource eduPlanVersionDS = getConfig().getDataSource(UsmaWorkGraphEduPlanAdd.EDU_PLAN_VERSION_DS);
        eduPlanVersionDS.doCleanupDataSource();

        IHeaderColumnBuilder courses = UsmaWorkGraphManager.headerColumn("courses");
        eduPlanVersionDS.addColumn(courses);
        for (Course course: dao.getWorkGraphCourses(_workGraphId))
        {
            courses.addSubColumn(UsmaWorkGraphManager.blockColumn(course.getCode(), "courseBlockColumn").headerAlign("center").align("center").create());
        }
    }

    public void onClickApply()
    {
        Map<Long, Set<String>> versionCourseIdsMap = SafeMap.get(HashSet.class);
        for (Map.Entry<PairKey<Long, String>, Boolean> entry: _courseInUseMap.entrySet())
        {
            Long versionId = entry.getKey().getFirst();
            String courseCode = entry.getKey().getSecond();
            if (Boolean.TRUE.equals(entry.getValue()))
            {
                versionCourseIdsMap.get(versionId).add(courseCode);
            }
        }

        UsmaWorkGraphManager.instance().dao().saveWorkGraphEduPlanVersionCourses(_workGraphId, versionCourseIdsMap);
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UsmaWorkGraphManager.BIND_WORK_GRAPH, getWorkGraphId());
        dataSource.put("programSubject", getProgramSubject() == null ? null : getProgramSubject().getId());
    }
}
