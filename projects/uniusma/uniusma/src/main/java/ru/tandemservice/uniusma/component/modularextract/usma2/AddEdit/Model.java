/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2.AddEdit;

import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public class Model extends CommonModularStudentExtractAddEditModel<UsmaExcludeDebtStuExtract>
{
    private StudentStatus _studentStatusNew;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }
}
