package ru.tandemservice.uniusma.entity.eduPlan;

import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * ГУП (Неделя строки курса)
 */
public class UsmaWorkGraphRowWeek extends UsmaWorkGraphRowWeekGen
{
    public UsmaWorkGraphRowWeek()
    {

    }

    public UsmaWorkGraphRowWeek(UsmaWorkGraphRow row, int week)
    {
        this.setRow(row);
        this.setWeek(week);
    }
}