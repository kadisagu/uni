/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusma.runtime.student;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.dbsupport.ddl.connect.DBConnect;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.PensionType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.PensionTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author agolubenko
 * @since 09.10.2008
 */
public class StudentImportDAO extends UniBaseDao implements IStudentImportDAO
{
    public static final Logger _logger = Logger.getLogger(StudentImportDAO.class);

    @SuppressWarnings("unchecked")
    @Override
    public void importStudents(File file) throws Exception
    {
        Session session = getSession();

        SAXReader reader = new SAXReader();
        Document document = reader.read(file);

        DBConnect tool = UniBaseUtils.createDDLTool(session);

        long russiaId = ((Number) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE)).setProjection(Projections.property(AddressCountry.P_ID)).uniqueResult()).longValue();
        AddressCountry russia = get(AddressCountry.class, russiaId);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Pattern adressPattern = Pattern.compile(".*?г. (.*?), (.*?). (.*?), ([^-а-я/]*)([а-я]|/.?)-?(.*)");

        Sex male = getCatalogItem(Sex.class, SexCodes.MALE);
        Sex female = getCatalogItem(Sex.class, SexCodes.FEMALE);

        IdentityCardType passport = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT);
        IdentityCardType passportNotRF = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT_NOT_RF);

        CompensationType budget = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);

        PensionType pensionType = getCatalogItem(PensionType.class, PensionTypeCodes.NONE);

        int importedCount = 0;
        List<Element> elements = document.getRootElement().elements();
        _logger.info("Start import students");
        for (Element element : elements)
        {
            String sFIO = getValue(element, "FIO");
            if (sFIO == null)
            {
                continue;
            }

            Group group = (Group) session.createCriteria(Group.class).add(Restrictions.eq(Group.P_TITLE, getValue(element, "group"))).uniqueResult();
            if (group == null)
            {
                _logger.info(String.format("Group '%s' not found for student '%s'", group, sFIO));
                continue;
            }

            String[] fio = sFIO.split(" ");
            IdentityCard identityCard = new IdentityCard();
            identityCard.setLastName(fio[0]);
            if (fio.length == 1)
            {
                _logger.info("Student has only lastName: " + sFIO);
                continue;
            }
            identityCard.setFirstName(fio[1]);
            identityCard.setMiddleName(fio.length > 2 ? fio[2] : null);
            identityCard.setSex(getValue(element, "sex").equals("М") ? male : female);

            String birthDate = getValue(element, "birthDate");
            identityCard.setBirthDate(birthDate != null ? dateFormat.parse(birthDate) : null);
            identityCard.setBirthPlace(getValue(element, "birthPlace"));

            AddressCountry country = (AddressCountry) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_TITLE, getValue(element, "citizenship"))).uniqueResult();
            identityCard.setCitizenship(country != null ? country : russia);
            identityCard.setSeria(getValue(element, "seria"));
            identityCard.setNumber(getValue(element, "number"));

            String issuanceDate = getValue(element, "issuanceDate");
            identityCard.setIssuanceDate(issuanceDate != null ? dateFormat.parse(issuanceDate) : null);
            identityCard.setIssuancePlace(getValue(element, "issuancePlace"));
            identityCard.setCardType((country != null && country.getCode() == 0) ? passport : passportNotRF);

            String identityCardAdress = getValue(element, "identityCardAddress");
            if (identityCardAdress != null)
            {
                Matcher matcher = adressPattern.matcher(identityCardAdress);
                if (matcher.find())
                {
                    String settlement = matcher.group(1);
                    String street = matcher.group(3) + " " + matcher.group(2);
                    String home = matcher.group(4);
                    String part = "";
                    String flat = matcher.group(5);
                    Long addressId = AddressBaseManager.instance().dao().createAddress(russiaId, "Свердловская обл", "", settlement, "", street, home, part, flat);

                    if (addressId != null)
                    {
                        identityCard.setAddress(get(AddressBase.class, addressId));
                    }
                    else
                    {
                        _logger.info("Cannot create identityCardAddress for student: " + sFIO);
                    }
                }
            }
            session.save(identityCard);
            
            // Персона

            Person person = new Person();
            person.setPensionType(pensionType);
            person.setIdentityCard(identityCard);

            person.setContactData(new PersonContactData());
            save(person.getContactData());

            session.save(person);

            identityCard.setPerson(person);
            session.update(identityCard);

            // Студент
            
            Student student = new Student();
            student.setGroup(group);
            student.setCourse(group.getCourse());
            student.setEducationOrgUnit(group.getEducationOrgUnit());
            student.setDevelopPeriodAuto(group.getEducationOrgUnit().getDevelopPeriod());

            int entranceYear = group.getStartEducationYear().getIntValue();
            student.setEntranceYear(entranceYear);
            student.setBookNumber(getValue(element, "bookNumber"));
            student.setStatus((StudentStatus) session.createCriteria(StudentStatus.class).add(Restrictions.eq(StudentStatus.P_TITLE, StringUtils.capitalize(getValue(element, "status")))).uniqueResult());
            
            student.setCompensationType(getValue(element, "compensationType").equals(0) ? budget : contract);
            student.setComment(getValue(element, "comment"));
            student.setPerson(person);
//            int personalNumber = DAO.generateUniqueStudentNumber(entranceYear, session);
//            student.setPersonalNumber(personalNumber);
            session.save(student);
            importedCount++;
            if (importedCount % 100 == 0)
            {
                _logger.info("+100");
            }
        }

        _logger.info("Imported " + importedCount + " students from " + elements.size());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void importBadDates(File file) throws Exception
    {
        Session session = getSession();

        SAXReader reader = new SAXReader();
        Document document = reader.read(file);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        List<Element> elements = document.getRootElement().elements();
        for (Element element : elements)
        {
            String sFIO = getValue(element, "FIO");
            if (sFIO == null)
            {
                continue;
            }

            String[] fio = sFIO.split(" ");
            if (fio.length > 1)
            {
                Criteria criteria = session.createCriteria(IdentityCard.class);
                criteria.add(Restrictions.eq(IdentityCard.P_LAST_NAME, fio[0]));
                criteria.add(Restrictions.eq(IdentityCard.P_FIRST_NAME, fio[1]));
                if (fio.length > 2)
                {
                    criteria.add(Restrictions.eq(IdentityCard.P_MIDDLE_NAME, fio[2]));
                }
                else
                {
                    criteria.add(Restrictions.isNull(IdentityCard.P_MIDDLE_NAME));
                }
                List<IdentityCard> identityCards = criteria.list();
                if (identityCards.isEmpty() || identityCards.size() > 1)
                {
                    _logger.error(sFIO);
                }
                else
                {
                    IdentityCard identityCard = identityCards.get(0);

                    String birthDate = getValue(element, "birthDate");
                    identityCard.setBirthDate(birthDate != null ? dateFormat.parse(birthDate) : null);

                    String issuanceDate = getValue(element, "issuanceDate");
                    identityCard.setIssuanceDate(issuanceDate != null ? dateFormat.parse(issuanceDate) : null);

                    session.update(identityCard);
                }
            }
        }
    }

    private String getValue(Element element, String attributeName)
    {
        return element.elementText(attributeName);
    }
}
