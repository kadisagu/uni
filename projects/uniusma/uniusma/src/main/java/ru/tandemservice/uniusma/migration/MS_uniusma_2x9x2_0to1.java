/* $Id$ */
package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Ekaterina Zvereva
 * @since 28.12.2015
 */
public class MS_uniusma_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // модуль unimail отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("unimail"))
                throw new RuntimeException("Module 'unimail' is not deleted");
        }

        // удалить сущность mailServerSetting
        if (tool.tableExists("mailserversetting_t"))
        {
            // удалить таблицу
            tool.dropTable("mailserversetting_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("mailServerSetting");

        }

        // удалить сущность mailTemplate
        if (tool.tableExists("mailtemplate_t"))
        {
            // удалить таблицу
            tool.dropTable("mailtemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("mailTemplate");

        }


        // удалить сущность mailState
        if (tool.tableExists("mailstate_t"))
        {
            // удалить таблицу
            tool.dropTable("mailstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("mailState");

        }

        // удалить сущность mailContentType
        if (tool.tableExists("mailcontenttype_t"))
        {
            // удалить таблицу
            tool.dropTable("mailcontenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("mailContentType");

        }

        // удалить сущность messageType
        if (tool.tableExists("messagetype_t"))
        {
            // удалить таблицу
            tool.dropTable("messagetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("messageType");

        }



        // удалить сущность mailLog
        if (tool.tableExists("maillog_t"))
        {
            // удалить таблицу
            tool.dropTable("maillog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("mailLog");

        }

        // удалить сущность mailMessage
        if (tool.tableExists("mailmessage_t"))
        {
            // удалить таблицу
            tool.dropTable("mailmessage_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("mailMessage");

        }

        MigrationUtils.removeModuleFromVersion_s(tool, "unimail");

        ////////////////////////////////////////////////////////////////////////////////
        // модуль unisakai отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("unisakai"))
                throw new RuntimeException("Module 'unisakai' is not deleted");
        }

        // удалить сущность sakaiEntityState
        if (tool.tableExists("sakaientitystate_t"))
        {
            // удалить таблицу
            tool.dropTable("sakaientitystate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("sakaiEntityState");

        }


        // удалить сущность uniSakaiTemplateDocument
        if (tool.tableExists("unisakaitemplatedocument_t"))
        {
            // удалить таблицу
            tool.dropTable("unisakaitemplatedocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("uniSakaiTemplateDocument");

        }

        // удалить сущность uniSakaiSettings
        if (tool.tableExists("unisakaisettings_t"))
        {
            // удалить таблицу
            tool.dropTable("unisakaisettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("uniSakaiSettings");

        }

        // удалить сущность uniSakaiSettingsType
        if (tool.tableExists("unisakaisettingstype_t"))
        {
            // удалить таблицу
            tool.dropTable("unisakaisettingstype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("uniSakaiSettingsType");

        }


        // удалить сущность uniSakaiMarkImport
        if (tool.tableExists("unisakaimarkimport_t"))
        {
            // удалить таблицу
            tool.dropTable("unisakaimarkimport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("uniSakaiMarkImport");

        }

        // удалить сущность uniSakaiSiteEtalon
        if (tool.tableExists("unisakaisiteetalon_t"))
        {
            // удалить таблицу
            tool.dropTable("unisakaisiteetalon_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("uniSakaiSiteEtalon");

        }

        MigrationUtils.removeModuleFromVersion_s(tool, "unisakai");
    }

}