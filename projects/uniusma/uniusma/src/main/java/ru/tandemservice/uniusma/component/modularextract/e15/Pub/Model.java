/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e15.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 27.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e15.Pub.Model
{
    EduEnrAsTransferStuExtractUsmaExt _extUsmaExt;

    public EduEnrAsTransferStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(EduEnrAsTransferStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
