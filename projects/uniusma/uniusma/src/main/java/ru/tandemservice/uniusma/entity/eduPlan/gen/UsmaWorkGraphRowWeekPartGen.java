package ru.tandemservice.uniusma.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Часть недели строки курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaWorkGraphRowWeekPartGen extends EntityBase
 implements INaturalIdentifiable<UsmaWorkGraphRowWeekPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart";
    public static final String ENTITY_NAME = "usmaWorkGraphRowWeekPart";
    public static final int VERSION_HASH = 2086428997;
    private static IEntityMeta ENTITY_META;

    public static final String L_WEEK = "week";
    public static final String P_PART = "part";
    public static final String L_TYPE = "type";

    private UsmaWorkGraphRowWeek _week;     // Неделя строки курса
    private int _part;     // Номер части недели
    private EppWeekType _type;     // Тип недели

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Неделя строки курса. Свойство не может быть null.
     */
    @NotNull
    public UsmaWorkGraphRowWeek getWeek()
    {
        return _week;
    }

    /**
     * @param week Неделя строки курса. Свойство не может быть null.
     */
    public void setWeek(UsmaWorkGraphRowWeek week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Номер части недели. Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part Номер части недели. Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getType()
    {
        return _type;
    }

    /**
     * @param type Тип недели. Свойство не может быть null.
     */
    public void setType(EppWeekType type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaWorkGraphRowWeekPartGen)
        {
            if (withNaturalIdProperties)
            {
                setWeek(((UsmaWorkGraphRowWeekPart)another).getWeek());
                setPart(((UsmaWorkGraphRowWeekPart)another).getPart());
            }
            setType(((UsmaWorkGraphRowWeekPart)another).getType());
        }
    }

    public INaturalId<UsmaWorkGraphRowWeekPartGen> getNaturalId()
    {
        return new NaturalId(getWeek(), getPart());
    }

    public static class NaturalId extends NaturalIdBase<UsmaWorkGraphRowWeekPartGen>
    {
        private static final String PROXY_NAME = "UsmaWorkGraphRowWeekPartNaturalProxy";

        private Long _week;
        private int _part;

        public NaturalId()
        {}

        public NaturalId(UsmaWorkGraphRowWeek week, int part)
        {
            _week = ((IEntity) week).getId();
            _part = part;
        }

        public Long getWeek()
        {
            return _week;
        }

        public void setWeek(Long week)
        {
            _week = week;
        }

        public int getPart()
        {
            return _part;
        }

        public void setPart(int part)
        {
            _part = part;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsmaWorkGraphRowWeekPartGen.NaturalId) ) return false;

            UsmaWorkGraphRowWeekPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getWeek(), that.getWeek()) ) return false;
            if( !equals(getPart(), that.getPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getWeek());
            result = hashCode(result, getPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getWeek());
            sb.append("/");
            sb.append(getPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaWorkGraphRowWeekPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaWorkGraphRowWeekPart.class;
        }

        public T newInstance()
        {
            return (T) new UsmaWorkGraphRowWeekPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "week":
                    return obj.getWeek();
                case "part":
                    return obj.getPart();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "week":
                    obj.setWeek((UsmaWorkGraphRowWeek) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "type":
                    obj.setType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "week":
                        return true;
                case "part":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "week":
                    return true;
                case "part":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "week":
                    return UsmaWorkGraphRowWeek.class;
                case "part":
                    return Integer.class;
                case "type":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaWorkGraphRowWeekPart> _dslPath = new Path<UsmaWorkGraphRowWeekPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaWorkGraphRowWeekPart");
    }
            

    /**
     * @return Неделя строки курса. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart#getWeek()
     */
    public static UsmaWorkGraphRowWeek.Path<UsmaWorkGraphRowWeek> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Номер части недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart#getType()
     */
    public static EppWeekType.Path<EppWeekType> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends UsmaWorkGraphRowWeekPart> extends EntityPath<E>
    {
        private UsmaWorkGraphRowWeek.Path<UsmaWorkGraphRowWeek> _week;
        private PropertyPath<Integer> _part;
        private EppWeekType.Path<EppWeekType> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Неделя строки курса. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart#getWeek()
     */
        public UsmaWorkGraphRowWeek.Path<UsmaWorkGraphRowWeek> week()
        {
            if(_week == null )
                _week = new UsmaWorkGraphRowWeek.Path<UsmaWorkGraphRowWeek>(L_WEEK, this);
            return _week;
        }

    /**
     * @return Номер части недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(UsmaWorkGraphRowWeekPartGen.P_PART, this);
            return _part;
        }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeekPart#getType()
     */
        public EppWeekType.Path<EppWeekType> type()
        {
            if(_type == null )
                _type = new EppWeekType.Path<EppWeekType>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return UsmaWorkGraphRowWeekPart.class;
        }

        public String getEntityName()
        {
            return "usmaWorkGraphRowWeekPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
