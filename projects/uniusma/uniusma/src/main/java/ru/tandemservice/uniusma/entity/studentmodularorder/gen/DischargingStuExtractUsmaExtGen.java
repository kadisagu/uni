package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.DischargingStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. Об отстранении от занятий в связи с задолженностью по оплате за обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DischargingStuExtractUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<DischargingStuExtractUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt";
    public static final String ENTITY_NAME = "dischargingStuExtractUsmaExt";
    public static final int VERSION_HASH = 1728769824;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_CONTRACT_TYPE = "contractType";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_CONTRACT_PARAGRAPH = "contractParagraph";
    public static final String P_DEBT_ELIMINATE_DATE = "debtEliminateDate";

    private DischargingStuExtract _extractExt;     // Выписка из сборного приказа по студенту. Об отстранении от занятий
    private String _contractType;     // Название договора
    private String _contractNumber;     // Номер договора
    private Date _contractDate;     // Дата договора
    private String _contractParagraph;     // Пункт договора
    private Date _debtEliminateDate;     // Ликвидировать задолженность до

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. Об отстранении от занятий. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public DischargingStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. Об отстранении от занятий. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(DischargingStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Название договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractType()
    {
        return _contractType;
    }

    /**
     * @param contractType Название договора. Свойство не может быть null.
     */
    public void setContractType(String contractType)
    {
        dirty(_contractType, contractType);
        _contractType = contractType;
    }

    /**
     * @return Номер договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора. Свойство не может быть null.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата договора. Свойство не может быть null.
     */
    @NotNull
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата договора. Свойство не может быть null.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Пункт договора.
     */
    @Length(max=255)
    public String getContractParagraph()
    {
        return _contractParagraph;
    }

    /**
     * @param contractParagraph Пункт договора.
     */
    public void setContractParagraph(String contractParagraph)
    {
        dirty(_contractParagraph, contractParagraph);
        _contractParagraph = contractParagraph;
    }

    /**
     * @return Ликвидировать задолженность до. Свойство не может быть null.
     */
    @NotNull
    public Date getDebtEliminateDate()
    {
        return _debtEliminateDate;
    }

    /**
     * @param debtEliminateDate Ликвидировать задолженность до. Свойство не может быть null.
     */
    public void setDebtEliminateDate(Date debtEliminateDate)
    {
        dirty(_debtEliminateDate, debtEliminateDate);
        _debtEliminateDate = debtEliminateDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DischargingStuExtractUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((DischargingStuExtractUsmaExt)another).getExtractExt());
            }
            setContractType(((DischargingStuExtractUsmaExt)another).getContractType());
            setContractNumber(((DischargingStuExtractUsmaExt)another).getContractNumber());
            setContractDate(((DischargingStuExtractUsmaExt)another).getContractDate());
            setContractParagraph(((DischargingStuExtractUsmaExt)another).getContractParagraph());
            setDebtEliminateDate(((DischargingStuExtractUsmaExt)another).getDebtEliminateDate());
        }
    }

    public INaturalId<DischargingStuExtractUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<DischargingStuExtractUsmaExtGen>
    {
        private static final String PROXY_NAME = "DischargingStuExtractUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(DischargingStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DischargingStuExtractUsmaExtGen.NaturalId) ) return false;

            DischargingStuExtractUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DischargingStuExtractUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DischargingStuExtractUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new DischargingStuExtractUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "contractType":
                    return obj.getContractType();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "contractParagraph":
                    return obj.getContractParagraph();
                case "debtEliminateDate":
                    return obj.getDebtEliminateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((DischargingStuExtract) value);
                    return;
                case "contractType":
                    obj.setContractType((String) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "contractParagraph":
                    obj.setContractParagraph((String) value);
                    return;
                case "debtEliminateDate":
                    obj.setDebtEliminateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "contractType":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
                case "contractParagraph":
                        return true;
                case "debtEliminateDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "contractType":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
                case "contractParagraph":
                    return true;
                case "debtEliminateDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return DischargingStuExtract.class;
                case "contractType":
                    return String.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "contractParagraph":
                    return String.class;
                case "debtEliminateDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DischargingStuExtractUsmaExt> _dslPath = new Path<DischargingStuExtractUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DischargingStuExtractUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. Об отстранении от занятий. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getExtractExt()
     */
    public static DischargingStuExtract.Path<DischargingStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Название договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractType()
     */
    public static PropertyPath<String> contractType()
    {
        return _dslPath.contractType();
    }

    /**
     * @return Номер договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Пункт договора.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractParagraph()
     */
    public static PropertyPath<String> contractParagraph()
    {
        return _dslPath.contractParagraph();
    }

    /**
     * @return Ликвидировать задолженность до. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getDebtEliminateDate()
     */
    public static PropertyPath<Date> debtEliminateDate()
    {
        return _dslPath.debtEliminateDate();
    }

    public static class Path<E extends DischargingStuExtractUsmaExt> extends EntityPath<E>
    {
        private DischargingStuExtract.Path<DischargingStuExtract> _extractExt;
        private PropertyPath<String> _contractType;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<String> _contractParagraph;
        private PropertyPath<Date> _debtEliminateDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. Об отстранении от занятий. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getExtractExt()
     */
        public DischargingStuExtract.Path<DischargingStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new DischargingStuExtract.Path<DischargingStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Название договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractType()
     */
        public PropertyPath<String> contractType()
        {
            if(_contractType == null )
                _contractType = new PropertyPath<String>(DischargingStuExtractUsmaExtGen.P_CONTRACT_TYPE, this);
            return _contractType;
        }

    /**
     * @return Номер договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(DischargingStuExtractUsmaExtGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(DischargingStuExtractUsmaExtGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Пункт договора.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getContractParagraph()
     */
        public PropertyPath<String> contractParagraph()
        {
            if(_contractParagraph == null )
                _contractParagraph = new PropertyPath<String>(DischargingStuExtractUsmaExtGen.P_CONTRACT_PARAGRAPH, this);
            return _contractParagraph;
        }

    /**
     * @return Ликвидировать задолженность до. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt#getDebtEliminateDate()
     */
        public PropertyPath<Date> debtEliminateDate()
        {
            if(_debtEliminateDate == null )
                _debtEliminateDate = new PropertyPath<Date>(DischargingStuExtractUsmaExtGen.P_DEBT_ELIMINATE_DATE, this);
            return _debtEliminateDate;
        }

        public Class getEntityClass()
        {
            return DischargingStuExtractUsmaExt.class;
        }

        public String getEntityName()
        {
            return "dischargingStuExtractUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
