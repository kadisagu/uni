/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.commons.gradation.IAcadWeekendExtract;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class Controller extends CommonModularStudentExtractAddEditController<UsmaExcludeAfterAcadWeekendStuExtract, IDAO, Model>
{
    public void onChangeAcadWeekendOrder(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (null != model.getAcadWeekendOrderSelected())
        {
            IAcadWeekendExtract extract = (IAcadWeekendExtract) model.getAcadWeekendOrderSelected();
            model.getExtract().setBeginAcadWeekendDate(extract.getBeginAcadWeekendDate());
            model.getExtract().setEndAcadWeekendDate(extract.getEndAcadWeekendDate());

            AbstractStudentOrder order = model.getAcadWeekendOrderSelected().getParagraph().getOrder();
            model.getExtract().setAcadWeekendOrderNumber(order.getNumber());
            model.getExtract().setAcadWeekendOrderDate(order.getCommitDate());
        }
        else
        {
            model.getExtract().setBeginAcadWeekendDate(null);
            model.getExtract().setEndAcadWeekendDate(null);
            model.getExtract().setAcadWeekendOrderNumber(null);
            model.getExtract().setAcadWeekendOrderDate(null);
        }
    }

}
