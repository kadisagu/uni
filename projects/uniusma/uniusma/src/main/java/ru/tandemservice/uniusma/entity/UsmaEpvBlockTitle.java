package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Титул блока УПв
 */
public class UsmaEpvBlockTitle extends UsmaEpvBlockTitleGen
{
    public UsmaEpvBlockTitle()
    {

    }

    public UsmaEpvBlockTitle(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}