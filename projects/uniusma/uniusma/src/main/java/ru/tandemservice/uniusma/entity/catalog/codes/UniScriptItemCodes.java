package ru.tandemservice.uniusma.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Юни»"
 * Имя сущности : uniScriptItem
 * Файл data.xml : uniusma-catalog.data.xml
 */
public interface UniScriptItemCodes
{
    /** Константа кода (code) элемента : Личная карточка студента (title) */
    String STUDENT_PERSONAL_CARD_PRINT_SCRIPT = "studentPersonalCardPrintScript";
    /** Константа кода (code) элемента : Список студентов группы (title) */
    String GROUP_STUDENT_LIST = "groupStudentList";
    /** Константа кода (code) элемента : Распределение студентов по курсам и направлениям подготовки (специальностям) (ВО) (title) */
    String STUDENTS_COURSES_SPECIALITIES_ALLOCATION_REPORT = "studentsCoursesSpecialitiesAllocationReport";
    /** Константа кода (code) элемента : Состав студентов по возрасту и полу (title) */
    String STUDENTS_AGE_SEX_DISTRIBUTION_REPORT = "studentsAgeSexDistributionReport";
    /** Константа кода (code) элемента : Списочный состав студентов групп (title) */
    String STUDENTS_GROUPS_LIST = "studentsGroupsList";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (title) */
    String STUDENT_INDEED_REFERENCE = "sd.uni.0";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (с визой ректора) (title) */
    String STUDENT_INDEED_RECTOR_REFERENCE = "sd.uni.1";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (в пенсионный фонд) (title) */
    String STUDENT_INDEED_PF_REFERENCE = "sd.uni.2";
    /** Константа кода (code) элемента : Справка «В военкомат» (title) */
    String MILITARY_REGISTRATION_REFERENCE = "sd.uni.3";
    /** Константа кода (code) элемента : Личная карточка студента (приложение) (title) */
    String STUDENT_PERSONAL_CARD_ATTACHMENT_PRINT_SCRIPT = "studentPersonalCardAttachmentPrintScript";
    /** Константа кода (code) элемента : 'Вкладыш' для учебной карточки студента (title) */
    String STUDENT_PERSONAL_CARD_ADD_PRINT_SCRIPT = "studentPersonalCardAddPrintScript";
    /** Константа кода (code) элемента : Печатать титул ЛК студента (title) */
    String STUDENT_PERSONAL_CARD_TITLE_PRINT_SCRIPT = "studentPersonalCardTitlePrintScript";
    /** Константа кода (code) элемента : Печатать сведения о ГИА ЛК студента (title) */
    String STUDENT_PERSONAL_CARD_DATA_PRINT_SCRIPT = "studentPersonalCardDataPrintScript";

    Set<String> CODES = ImmutableSet.of(STUDENT_PERSONAL_CARD_PRINT_SCRIPT, GROUP_STUDENT_LIST, STUDENTS_COURSES_SPECIALITIES_ALLOCATION_REPORT, STUDENTS_AGE_SEX_DISTRIBUTION_REPORT, STUDENTS_GROUPS_LIST, STUDENT_INDEED_REFERENCE, STUDENT_INDEED_RECTOR_REFERENCE, STUDENT_INDEED_PF_REFERENCE, MILITARY_REGISTRATION_REFERENCE, STUDENT_PERSONAL_CARD_ATTACHMENT_PRINT_SCRIPT, STUDENT_PERSONAL_CARD_ADD_PRINT_SCRIPT, STUDENT_PERSONAL_CARD_TITLE_PRINT_SCRIPT, STUDENT_PERSONAL_CARD_DATA_PRINT_SCRIPT);
}
