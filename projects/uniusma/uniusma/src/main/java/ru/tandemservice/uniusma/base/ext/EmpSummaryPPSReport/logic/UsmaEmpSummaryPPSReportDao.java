/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.EmpSummaryPPSReport.logic;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.EmpSummaryPPSReportManager;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.EmpSummaryPPSReportDao;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.EmployeeWrapper;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 21.11.12
 */
public class UsmaEmpSummaryPPSReportDao extends EmpSummaryPPSReportDao
{
    @Override
    public RtfDocument getReportDocument(Date reportDate, List<PostType> postTypeList)
    {
        List<EmployeeWrapper> employeeWrapperList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getEmployeeWrapperList(reportDate);

        List<SummaryDataWrapper<EmployeeWrapper>> summaryPPSList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSList(reportDate, postTypeList, employeeWrapperList);
        List<SummaryDataWrapper<EmployeeWrapper>> summaryPPSWithScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithScienceList(reportDate, postTypeList, employeeWrapperList);
        List<SummaryDataWrapper<EmployeeWrapper>> summaryPPSWithHighScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithHighScienceList(reportDate, postTypeList, employeeWrapperList);

        byte[] content = DataAccessServices.dao().get(EmployeeTemplateDocument.class, EmployeeTemplateDocument.code(), "empSummaryPPSReport").getContent();
        RtfDocument document = new RtfReader().read(content);

        final List<String[]> postTypes = new ArrayList<>();
        postTypes.add(new String[]{"Штатные", PostTypeCodes.MAIN_JOB});
        postTypes.add(new String[]{"Внутренние совместители", PostTypeCodes.SECOND_JOB_INNER});
        postTypes.add(new String[]{"Внешние совместители", PostTypeCodes.SECOND_JOB_OUTER});

        int columnNumber = 3 + postTypes.size();

        List<String[]> summaryPPSLineList = new ArrayList<>();
        List<String[]> summaryPPSWithScienceLineList = new ArrayList<>();
        List<String[]> summaryPPSWithHighScienceLineList = new ArrayList<>();

        for (SummaryDataWrapper wrapper : summaryPPSList)
        {
            int index = 0;

            String[] line = new String[columnNumber];

            line[index++] = wrapper.getTitle();
            for (String[] postType : postTypes)
            {
                String value = wrapper.getProperty(postType[1]) == null ? "" : wrapper.getProperty(postType[1]).toString();
                line[index++] = value;
            }
            line[index++] = wrapper.getProperty("hourlyPaid").toString();
            line[index] = "";

            summaryPPSLineList.add(line);
        }

        for (SummaryDataWrapper wrapper : summaryPPSWithScienceList)
        {
            int index = 0;

            String[] line = new String[columnNumber];

            line[index++] = wrapper.getTitle();
            for (String[] postType : postTypes)
            {
                String value = wrapper.getProperty(postType[1]) == null ? "" : wrapper.getProperty(postType[1]).toString();
                line[index++] = value;
            }
            line[index++] = wrapper.getProperty("hourlyPaid").toString();
            line[index] = "";

            summaryPPSWithScienceLineList.add(line);
        }

        for (SummaryDataWrapper wrapper : summaryPPSWithHighScienceList)
        {
            int index = 0;

            String[] line = new String[columnNumber];

            line[index++] = wrapper.getTitle();
            for (String[] postType : postTypes)
            {
                String value = wrapper.getProperty(postType[1]) == null ? "" : wrapper.getProperty(postType[1]).toString();
                line[index++] = value;
            }
            line[index++] = wrapper.getProperty("hourlyPaid").toString();
            line[index] = "";

            summaryPPSWithHighScienceLineList.add(line);
        }

        new RtfTableModifier()
                .put("T1", summaryPPSLineList.toArray(new String[summaryPPSLineList.size()][columnNumber]))
                .put("T2", summaryPPSWithScienceLineList.toArray(new String[summaryPPSWithScienceLineList.size()][columnNumber]))
                .put("T3", summaryPPSWithHighScienceLineList.toArray(new String[summaryPPSWithHighScienceLineList.size()][columnNumber]))
                .modify(document);

        new RtfInjectModifier()
                .put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(reportDate))
                .modify(document);

        return document;
    }
}
