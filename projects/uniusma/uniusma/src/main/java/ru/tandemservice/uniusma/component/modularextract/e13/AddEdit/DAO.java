/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e13.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.WeekendChildStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 22.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e13.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e13.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        WeekendChildStuExtractUsmaExt extractExt = getByNaturalId(new WeekendChildStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            // если расширения нет - создадим его
            extractExt = new WeekendChildStuExtractUsmaExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setExtUsmaExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e13.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);
        saveOrUpdate(m.getExtUsmaExt());
    }
}
