package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Неделя учебного графика (УГМА)
 */
public class UsmaEduPlanVersionWeek extends UsmaEduPlanVersionWeekGen
{
    public UsmaEduPlanVersionWeek()
    {
    }

    public UsmaEduPlanVersionWeek(EppEduPlanVersion version, Course course, EppWeek week)
    {
        this.setVersion(version);
        this.setCourse(course);
        this.setWeek(week);
    }
}