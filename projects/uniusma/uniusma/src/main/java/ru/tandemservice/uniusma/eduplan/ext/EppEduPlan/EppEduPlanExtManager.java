/* $Id$ */
package ru.tandemservice.uniusma.eduplan.ext.EppEduPlan;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 01.10.2014
 */
@Configuration
public class EppEduPlanExtManager extends BusinessObjectExtensionManager
{
    public static final String BIND_CHECKED_BY_UMU = "checkedByUMU";
    public static final String BIND_CHECKED_BY_CRK = "checkedByCRK";
    public static final String BIND_CHECKED_BY_OOP = "checkedByOOP";

    public static final String BIND_CHECKED_BY_UMU_DATE_FROM = "checkedByUMUDateFrom";
    public static final String BIND_CHECKED_BY_CRK_DATE_FROM = "checkedByCRKDateFrom";
    public static final String BIND_CHECKED_BY_OOP_DATE_FROM = "checkedByOOPDateFrom";

    public static final String BIND_CHECKED_BY_UMU_DATE_TO = "checkedByUMUDateTo";
    public static final String BIND_CHECKED_BY_CRK_DATE_TO = "checkedByCRKDateTo";
    public static final String BIND_CHECKED_BY_OOP_DATE_TO = "checkedByOOPDateTo";

    public static void filterEduPlanListDSHandler(DQLSelectBuilder builder, String alias, ExecutionContext context)
    {
        Date checkedByUMUDateFrom = context.get(BIND_CHECKED_BY_UMU_DATE_FROM);
        Date checkedByUMUDateTo = context.get(BIND_CHECKED_BY_UMU_DATE_TO);

        Date checkedByCRKDateFrom = context.get(BIND_CHECKED_BY_CRK_DATE_FROM);
        Date checkedByCRKDateTo = context.get(BIND_CHECKED_BY_CRK_DATE_TO);

        Date checkedByOOPDateFrom = context.get(BIND_CHECKED_BY_OOP_DATE_FROM);
        Date checkedByOOPDateTo = context.get(BIND_CHECKED_BY_OOP_DATE_TO);

        boolean checkedByUMU = context.get(BIND_CHECKED_BY_UMU) != null || checkedByUMUDateFrom != null || checkedByUMUDateTo != null;
        boolean checkedByCRK = context.get(BIND_CHECKED_BY_CRK) != null || checkedByCRKDateFrom != null || checkedByCRKDateTo != null;
        boolean checkedByOOP = context.get(BIND_CHECKED_BY_OOP) != null || checkedByOOPDateFrom != null || checkedByOOPDateTo != null;

        Collection<EduProgramSubjectIndex> subjectIndex = context.get("subjectIndex");
        if (subjectIndex == null) subjectIndex = Collections.emptyList();
        EduProgramSubject programSubject = context.get("programSubject");
        OrgUnit formativeOrgUnit = context.get("formativeOrgUnit");

        if (checkedByUMU || checkedByCRK || checkedByOOP)
        {
            DQLSelectBuilder checkBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaEpvCheckState.class, "s")
                .where(eq(property("s", UsmaEpvCheckState.version().eduPlan()), property(alias)));

            if (checkedByUMU) checkBuilder.where(eq(property("s", UsmaEpvCheckState.checkedByUMU()), value(Boolean.TRUE)));
            if (checkedByCRK) checkBuilder.where(eq(property("s", UsmaEpvCheckState.checkedByCRK()), value(Boolean.TRUE)));
            if (checkedByOOP) checkBuilder.where(eq(property("s", UsmaEpvCheckState.checkedByOOP()), value(Boolean.TRUE)));

            checkBuilder.where(betweenDays(UsmaEpvCheckState.checkedByUMUDate().fromAlias("s"), checkedByUMUDateFrom, checkedByUMUDateTo));
            checkBuilder.where(betweenDays(UsmaEpvCheckState.checkedByCRKDate().fromAlias("s"), checkedByCRKDateFrom, checkedByCRKDateTo));
            checkBuilder.where(betweenDays(UsmaEpvCheckState.checkedByOOPDate().fromAlias("s"), checkedByOOPDateFrom, checkedByOOPDateTo));

            builder.where(exists(checkBuilder.buildQuery()));
        }

        if ((!subjectIndex.isEmpty() && programSubject == null) || formativeOrgUnit != null)
        {
            if (formativeOrgUnit != null)
            {
                builder
                    .where(exists(
                        new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                            .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                            .fromEntity(EducationOrgUnit.class, "ou")
                            .where(eq(property("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization()), property("b", EppEduPlanVersionSpecializationBlock.programSpecialization())))
                            .where(eq(property("ou", EducationOrgUnit.formativeOrgUnit()), value(formativeOrgUnit)))
                            .buildQuery()));
            }
        }
    }
}