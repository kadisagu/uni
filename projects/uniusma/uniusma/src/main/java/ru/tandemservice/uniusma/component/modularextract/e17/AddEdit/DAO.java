/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e17.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.RestorationStuExtractExtUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 19.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e17.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e17.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        RestorationStuExtractExtUsmaExt extractExt = getByNaturalId(new RestorationStuExtractExtUsmaExtGen.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            // если расширения нет - создадим его
            extractExt = new RestorationStuExtractExtUsmaExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setExtUsmaExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e17.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);
        saveOrUpdate(m.getExtUsmaExt());
    }
}
