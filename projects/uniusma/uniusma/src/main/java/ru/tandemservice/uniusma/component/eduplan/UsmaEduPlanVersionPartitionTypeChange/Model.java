/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionPartitionTypeChange;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.06.2013
 */
@Input
(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private UsmaEduPlanVersionPartitionType _versionPartitionType;
    private List<UsmaSchedulePartitionType> _partitionTypes;
    private UsmaSchedulePartitionType _partitionTypeOld;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public UsmaEduPlanVersionPartitionType getVersionPartitionType()
    {
        return _versionPartitionType;
    }

    public void setVersionPartitionType(UsmaEduPlanVersionPartitionType versionPartitionType)
    {
        _versionPartitionType = versionPartitionType;
    }

    public List<UsmaSchedulePartitionType> getPartitionTypes()
    {
        return _partitionTypes;
    }

    public void setPartitionTypes(List<UsmaSchedulePartitionType> partitionTypes)
    {
        _partitionTypes = partitionTypes;
    }

    public UsmaSchedulePartitionType getPartitionTypeOld()
    {
        return _partitionTypeOld;
    }

    public void setPartitionTypeOld(UsmaSchedulePartitionType partitionTypeOld)
    {
        _partitionTypeOld = partitionTypeOld;
    }
}