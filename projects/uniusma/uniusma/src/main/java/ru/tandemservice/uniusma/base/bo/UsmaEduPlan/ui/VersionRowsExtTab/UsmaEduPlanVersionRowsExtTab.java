/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.VersionRowsExtTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
@Configuration
public class UsmaEduPlanVersionRowsExtTab extends BusinessComponentManager
{
    public static final String EDU_PLAN_VERSION_BLOCK_DS = "eduPlanVersionBlockDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PLAN_VERSION_BLOCK_DS, eduPlanVersionBlockDSHandler()).addColumn(EppEduPlanVersionBlock.title().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduPlanVersionBlockDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersionBlock.class)
            .where(EppEduPlanVersionBlock.eduPlanVersion(), "versionId");
    }
}