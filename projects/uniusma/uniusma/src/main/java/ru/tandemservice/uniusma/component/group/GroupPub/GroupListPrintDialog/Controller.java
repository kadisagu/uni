// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.group.GroupPub.GroupListPrintDialog;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author vnekrasov
 * @since 24/1/14
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public void onClickApply(final IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniusma.component.group.GroupStudentListPrint", new ParametersMap()
                        .add("groupId", getModel(component).getGroupId())
                        .add("addBookNum", getModel(component).isBookNumberReq())));
        this.deactivate(component);
    }


}