/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.EduPlansTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.EduPlanAdd.UsmaWorkGraphEduPlanAdd;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id", required = true))
public class UsmaWorkGraphEduPlansTabUI extends UIPresenter
{
    private UsmaWorkGraph _workGraph = new UsmaWorkGraph();

    public UsmaWorkGraph getWorkGraph(){ return _workGraph; }
    public void setWorkGraph(UsmaWorkGraph workGraph){ _workGraph = workGraph; }

    public void onClickSelectEduPlanVersion()
    {
        _uiActivation.asRegionDialog(UsmaWorkGraphEduPlanAdd.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, _workGraph.getId()).activate();
    }

    @Override
    public void onComponentRefresh()
    {
        _workGraph = UsmaWorkGraphManager.instance().dao().getNotNull(UsmaWorkGraph.class, _workGraph.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(UsmaWorkGraphEduPlansTab.EDU_PLAN_DS))
        {
            dataSource.put(UsmaWorkGraphManager.BIND_WORK_GRAPH, _workGraph.getId());
        }
    }
}