package ru.tandemservice.uniusma.entity.eduPlan;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * График учебного процесса (УГМА)
 */
public class UsmaWorkGraph extends UsmaWorkGraphGen implements IDevelopCombination, IEppStateObject
{
    private String _title_cache;

    @EntityDSLSupport
    @Override
    public String getTitle()
    {
        if (null != this._title_cache) { return this._title_cache; }
        return (this._title_cache = DevelopUtil.getTitle(this));
    }
}