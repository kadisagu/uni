/* $Id$ */
package ru.tandemservice.uniusma.eduplan.ext.EppEduPlanVersion;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author azhebko
 * @since 03.10.2014
 */
@Configuration
public class EppEduPlanVersionExtManager extends BusinessObjectExtensionManager
{
}