/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.VersionRowsExtTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.UsmaEduPlanManager;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic.IUsmaEduPlanDAO;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.VersionRowExtEdit.UsmaEduPlanVersionRowExtEdit;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.util.UsmaEpvRowWrapper;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "versionId", required = true))
public class UsmaEduPlanVersionRowsExtTabUI extends UIPresenter
{
    private Long _versionId;
    private EppEduPlanVersionBlock _block;
    private Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<UsmaEpvRowWrapper>, StaticListDataSource<UsmaEpvRowWrapper>>> _dataSourceMap;

    public Long getVersionId(){ return _versionId; }
    public void setVersionId(Long versionId){ _versionId = versionId; }

    public EppEduPlanVersionBlock getBlock(){ return _block; }
    public void setBlock(EppEduPlanVersionBlock block){ _block = block; }

    public StaticListDataSource<UsmaEpvRowWrapper> getDataSource(){ return _dataSourceMap.get(_block).getFirst(); }
    public StaticListDataSource<UsmaEpvRowWrapper> getActionDataSource(){ return _dataSourceMap.get(_block).getSecond(); }


    @Override
    public void onComponentRefresh()
    {
        IUsmaEduPlanDAO dao = UsmaEduPlanManager.instance().dao();
        EppEduPlanVersion version = dao.<EppEduPlanVersion>get(_versionId);

        // по умолчанию указывается общий блок
        _block = getBlock()!=null?getBlock():dao.get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), version);

        _dataSourceMap = dao.getEduPlanVersionBlockDataSourceMap(_versionId);

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(UsmaEduPlanVersionRowsExtTab.EDU_PLAN_VERSION_BLOCK_DS))
        {
            dataSource.put("versionId", _versionId);
        }
    }

    public void onClickEditRow()
    {
        _uiActivation.asRegionDialog(UsmaEduPlanVersionRowExtEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong()).activate();
    }

    public void onClickEditCompetences()
    {
        this.getActivationBuilder().asCurrent("ru.tandemservice.uniusma.component.eduplan.row.UsmaCompetenceAddEdit").parameter(PublisherActivator.PUBLISHER_ID_KEY, this.getListenerParameterAsLong()).activate();
    }
}