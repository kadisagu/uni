package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaQualification;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Квалификация
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaQualificationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaQualification";
    public static final String ENTITY_NAME = "usmaQualification";
    public static final int VERSION_HASH = 1500757813;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_DEVELOP_PERIOD = "developPeriod";

    private EppEduPlanVersionBlock _block;     // Блок УПв
    private int _number;     // Номер
    private String _title;     // Название
    private String _developPeriod;     // Срок обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Срок обучения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок обучения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaQualificationGen)
        {
            setBlock(((UsmaQualification)another).getBlock());
            setNumber(((UsmaQualification)another).getNumber());
            setTitle(((UsmaQualification)another).getTitle());
            setDevelopPeriod(((UsmaQualification)another).getDevelopPeriod());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaQualificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaQualification.class;
        }

        public T newInstance()
        {
            return (T) new UsmaQualification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "developPeriod":
                    return obj.getDevelopPeriod();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "developPeriod":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "developPeriod":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "number":
                    return Integer.class;
                case "title":
                    return String.class;
                case "developPeriod":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaQualification> _dslPath = new Path<UsmaQualification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaQualification");
    }
            

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Срок обучения.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    public static class Path<E extends UsmaQualification> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _title;
        private PropertyPath<String> _developPeriod;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UsmaQualificationGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UsmaQualificationGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Срок обучения.
     * @see ru.tandemservice.uniusma.entity.UsmaQualification#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(UsmaQualificationGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

        public Class getEntityClass()
        {
            return UsmaQualification.class;
        }

        public String getEntityName()
        {
            return "usmaQualification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
