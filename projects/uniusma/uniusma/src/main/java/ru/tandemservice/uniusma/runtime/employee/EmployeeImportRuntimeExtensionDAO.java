/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.runtime.employee;

import com.linuxense.javadbf.DBFReader;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.Fias.FiasManager;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

/**
 * @author vip_delete
 */
public class EmployeeImportRuntimeExtensionDAO extends UniBaseDao implements IEmployeeImportRuntimeExtensionDAO
{
    private static final Logger _logger = Logger.getLogger(EmployeeImportRuntimeExtensionDAO.class);

    @Override
    public void createEmployees() throws Exception
    {
        Sex male = get(Sex.class, Sex.P_CODE, SexCodes.MALE);
        Sex female = get(Sex.class, Sex.P_CODE, SexCodes.FEMALE);

        Object[] name2sexDATA = new Object[]{"А", 1, "АВГУСТА", 2, "АВГУСТИНА", 2, "АДЕЛАНДА", 2, "АЖЕМБИКА", 2, "АЗА", 2, "АЗАРИЙ", 1, "АЙРАТ", 1, "АЛЕБАЙ", 1, "АЛЕВТИНА", 2, "АЛЕКАСАНДР", 1, "АЛЕКСАНДР", 1, "АЛЕКСАНДРА", 2, "АЛЕКСЕЙ", 1, "АЛЕНА", 2, "АЛЕНГИНА", 2, "АЛЕФТИНА", 2, "АЛИНА", 2, "АЛЛА", 2, "АЛЬБЕРТ", 1, "АЛЬБИНА", 2, "АЛЬВИНА", 2, "АЛЬФИЯ", 2, "АЛЬФРЕД", 1, "АНАСТАСИЯ", 2, "АНАТОЛИЙ", 1, "АНАТОЛИЯ", 2, "АНГЕЛИНА", 2, "АНДРЕЙ", 1, "АНЖЕЛА", 2, "АНЖЕЛИКА", 2, "АНЖЕЛЛА", 2, "АНИСА", 2, "АННА", 2, "АНТОН", 1, "АНТОНИДА", 2, "АНТОНИНА", 2, "АНФИСА", 2, "АРИФЖАН", 1, "АРКАДИЙ", 1, "АРМЕН", 1, "АРСЕН", 1, "АРСЕНИЙ", 1, "АРТАК", 1, "АРТЕМ", 1, "АРТУР", 1, "АРШАВИР", 1, "АСКОЛЬД", 1, "АСЯ", 2, "АЭЛИТА", 2, "БЕСЛАН", 1, "БОГДАН", 1, "БОРИС", 1, "ВАДИМ", 1, "ВАКИЛЬ", 1, "ВАЛЕНТИН", 1, "ВАЛЕНТИНА", 2, "ВАЛЕРИЙ", 1, "ВАЛЕРИЯ", 2, "ВАСИЛИЙ", 1, "ВАСИЛИЯ", 2, "ВАССА", 1, "ВЕНЕДИКТ", 1, "ВЕНЕРА", 1, "ВЕНИАМИН", 1, "ВЕРА", 2, "ВЕРОНИКА", 2, "ВИКТОР", 1, "ВИКТОРИЯ", 2, "ВИЛЕН", 1, "ВИОЛА", 2, "ВИОЛЕТТА", 2, "ВИТАЛИЙ", 1, "ВЛАДИМИР", 1, "ВЛАДИСЛАВ", 1, "ВЯЧЕСЛАВ", 1, "ГАЙНУЛЛА", 2, "ГАЛИНА", 2, "ГАЛИЯ", 2, "ГАРРИ", 1, "ГЕННАДИЙ", 1, "ГЕНРИЕТА", 2, "ГЕОРГИЙ", 1, "ГЕРМАН", 1, "ГЛЕБ", 1, "ГРАНУШ", 1, "ГРИГОРИЙ", 1, "ГУЛЬЗАНА", 2, "ГУЛЬМАРЬЯМ", 1, "ГУЛЬНАРА", 2, "ГУЛЬСЕРА", 2, "ГУЛЬСИНА", 2, "ГУМАР", 1, "ГЮЗЕЛЬ", 2, "ДАМИР", 1, "ДАНИЕР", 1, "ДАНИЛ", 1, "ДАНИЯ", 2, "ДАРИНА", 2, "ДАРЬЯ", 2, "ДЕЛГИНА", 2, "ДЕНИС", 1, "ДИАНА", 2, "ДИЛАРИ", 2, "ДИЛЯ", 2, "ДИЛЯРА", 2, "ДИНА", 2, "ДМИТРИЙ", 1, "ЕВГЕНИЙ", 1, "ЕВГЕНИЯ", 2, "ЕГОР", 1, "ЕКАТЕРИНА", 2, "ЕЛЕНА", 2, "ЕЛИЗАВЕТА", 2, "ЖАННА", 2, "ЖАННЕТА", 2, "ЗАКИЯ", 2, "ЗЕМФИРА", 2, "ЗИНАИДА", 2, "ЗИНОВИЙ", 1, "ЗОЯ", 2, "ЗУБЕЙДА", 2, "ЗУГРЯ", 2, "ЗУЛЬФИЯ", 2, "ЗУХРА", 2, "ИВАН", 1, "ИГНАТ", 1, "ИГОРЬ", 1, "ИДА", 2, "ИЗОЛЬД", 1, "ИЛДАРЬ", 2, "ИЛОНА", 2, "ИЛСУР", 1, "ИЛЬГИЗА", 2, "ИЛЬДАР", 1, "ИЛЬМИРА", 2, "ИЛЬШАТ", 2, "ИЛЬЯ", 1, "ИЛЬЯС", 1, "ИНГА", 2, "ИНЕССА", 2, "ИННА", 2, "ИРАИДА", 2, "ИРЕК", 1, "ИРИНА", 2, "ИСЛАМ", 1, "КАЛТАЙ", 1, "КАМАЛИТДИН", 1, "КАРОЛИНА", 2, "КИРИЛЛ", 1, "КЛАВДИЯ", 2, "КЛАРА", 2, "КОВЕН", 1, "КОНСТАНТИН", 1, "КРЕМВИЛЬДА", 2, "КРИСТИНА", 2, "КСЕНИЯ", 2, "ЛАДА", 2, "ЛАРИСА", 2, "ЛЕВ", 1, "ЛЕЙЛА", 2, "ЛЕЛЯ", 2, "ЛЕОНИД", 1, "ЛИДИЯ", 2, "ЛИЛИАНА", 2, "ЛИЛИЯ", 2, "ЛИНА", 2, "ЛИНИЗА", 2, "ЛОЛИТА", 2, "ЛУИЗА", 2, "ЛЮБОВЬ", 2, "ЛЮДМИЛА", 2, "ЛЮЗЯ", 2, "ЛЮЦИЯ", 2, "ЛЯЛЯ", 2, "МАГОМЕД", 1, "МАЙЯ", 2, "МАКАР", 1, "МАКСИМ", 1, "МАРАТ", 1, "МАРГАРИТА", 2, "МАРИАННА", 2, "МАРИНА", 2, "МАРИОНЕЛЛА", 2, "МАРИЯ", 2, "МАРК", 1, "МИНОРА", 2, "МИРОН", 1, "МИРОСЛАВ", 2, "МИХАИЛ", 1, "МИША", 1, "МОИСЕЙ", 1, "МУБАРИЗ", 1, "МУЗА", 2, "МУНИРА", 2, "Н", 2, "НАДЕЖДА", 2, "НАЖИЯ", 2, "НАЗИП", 1, "НАЗИФ", 1, "НАИЛА", 2, "НАЙЛЯ", 2, "НАРГИЗА", 2, "НАСТАСИЯ", 2, "НАТАЛИЯ", 2, "НАТАЛЬЯ", 2, "НАТАН", 1, "НЕЛЛИ", 2, "НЕЛЯ", 2, "НИКИТА", 1, "НИКОЛАЙ", 1, "НИНА", 2, "НИНЕЛЬ", 2, "НОННА", 2, "НЭЛЛИ", 2, "НЭЛЛЯ", 2, "ОКСАНА", 2, "ОКТЯБРИНА", 2, "ОЛЕГ", 1, "ОЛЕСЯ", 2, "ОЛЖАС", 1, "ОЛЬГА", 2, "ОРЕСТ", 1, "ПАВЕЛ", 1, "ПЕЛАГЕЯ", 2, "ПЕТР", 1, "ПОЛИНА", 2, "ПРОКОПИЙ", 1, "РАВИЛЯ", 2, "РАДИНА", 2, "РАДИОН", 1, "РАИСА", 2, "РАКИЯ", 2, "РАЛИФ", 1, "РАМЗИЯ", 2, "РАСИК", 1, "РАУШАНИЯ", 2, "РАФАИЛ", 1, "РАФИК", 1, "РАФИС", 1, "РАХИЛЬ", 1, "РЕГИНА", 2, "РЕЗЕДА", 2, "РИМА", 2, "РИММА", 2, "РИНАТ", 1, "РИФА", 2, "РОВЗА", 2, "РОЗА", 2, "РОЗАЛИЯ", 2, "РОМАН", 1, "РОСТИСЛАВ", 1, "РУСЛАН", 1, "РУСЛАНА", 2, "РУСТАМ", 1, "РУФИНА", 2, "САЗИДА", 2, "САКИНА", 2, "САНФИРА", 2, "САУБАН", 1, "СВЕТЛАНА", 2, "СЕВИНДЖ", 1, "СЕРГЕЙ", 1, "СНЕЖАНА", 2, "СОЛОМОН", 1, "СОФЬЯ", 2, "СТАНИСЛАВ", 1, "СТАНИСЛАВА", 2, "СТЕЛЛА", 2, "СТЕПАН", 1, "ТАИСА", 2, "ТАЛИЯ", 2, "ТАМАРА", 2, "ТАРАС", 1, "ТАТЬЯНА", 2, "ТИМОФЕЙ", 1, "ТИМУР", 1, "УЛЬЯНА", 2, "ФАИНА", 2, "ФАЙРУЗА", 2, "ФАНЗЕЛЬ", 1, "ФАНЗИЛЯ", 2, "ФАРИД", 1, "ФАРИДА", 2, "ФЕДОР", 1, "ФЕЛИКС", 1, "ФЛОРА", 2, "ХАЙАЛ", 1, "ХИЙИР", 1, "ШАХИДА", 2, "ШЛЕМА", 1, "ЭДГАР", 1, "ЭДУАРД", 1, "ЭЛЕОНОРА", 2, "ЭЛИНА", 2, "ЭЛЬВИНА", 2, "ЭЛЬВИРА", 2, "ЭЛЬДАР", 1, "ЭЛЬМИРА", 2, "ЭЛЬШАН", 1, "ЭМИЛИЯ", 2, "ЭММА", 2, "ЭННОЛИЯ", 2, "ЭРИК", 1, "ЭРНСТ", 1, "ЮЛИЯ", 2, "ЮРИЙ", 1, "ЯКОВ", 1, "ЯН", 1, "ЯНА", 2, "ЯНИНА", 2, "ЯНИС", 1, "ЯНУЛА", 2, "ЯРОСЛАВ", 1, "ЯРОСЛАВА", 2};

        Map<String, Sex> name2sex = new HashMap<>();
        for (int i = 0; i < name2sexDATA.length; i += 2)
            name2sex.put((String) name2sexDATA[i], 1 == ((Integer) name2sexDATA[i + 1]) ? male : female);

        HashSet<IdentityCardItem> hashSet = new HashSet<>();
        IdentityCardNumber identityCardNumber = new IdentityCardNumber();

        String employeePath = ApplicationRuntime.getProperty("uniusma.employee-data");
        if (employeePath == null || getCount(Employee.class) > 500) return;

        importData(employeePath + "EMPL_1.DBF", hashSet, identityCardNumber, name2sex);

        importData(employeePath + "EMPL_2.DBF", hashSet, identityCardNumber, name2sex);

        //        FileOutputStream fo = new FileOutputStream("c:/1.txt");
        //        fo.write("Object[] name2sex = new Object[]{".getBytes());
        //        for (Map.Entry<String, Integer> entry : name2sex.entrySet())
        //            fo.write(("\"" + entry.getKey() + "\", ,").getBytes());
        //        fo.write("};".getBytes());
        //        throw new RuntimeException();
    }

    private void importData(String fileName, HashSet<IdentityCardItem> hashSet, IdentityCardNumber identityCardNumber, Map<String, Sex> name2sex) throws Exception
    {
        System.out.println("Start import from " + fileName);

        InputStream is = new FileInputStream(fileName);
        DBFReader reader = new DBFReader(is);
        reader.setCharactersetName("CP866");

        Object[] row;
        IdentityCardType passport = get(IdentityCardType.class, IdentityCardType.P_CODE, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT);
        AddressCountry russia = get(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE);

        while ((row = reader.nextRecord()) != null)
        {
            trimRow(row);
            String name = (String) row[0];
            String birthday = (String) row[1];
            String addressString = (String) row[2];
            String number = (String) row[3];

            IdentityCard identityCard = createIdentityCard(name, birthday, hashSet, identityCardNumber);
            if (identityCard == null) continue;

            identityCard.setSex(name2sex.get(identityCard.getFirstName().toUpperCase()));
            identityCard.setCardType(passport);
            identityCard.setCitizenship(russia);
            save(identityCard);

            AddressBase address = null;

            if (addressString.length() > 0)
            {
                //предопределенные значения
                try
                {
                    address = createAddress(addressString);
                } catch (Exception e)
                {
                    System.out.println(addressString);
                }
            }
            if (address != null)
                save(address);
            Person person = new Person();

            if (number.length() > 0)
            {
                person.setSnilsNumber(number);

                if (person.getSnilsNumber().length() == 1)
                    person.setSnilsNumber(null);
            }

            person.setContactData(new PersonContactData());
            save(person.getContactData());

            person.setIdentityCard(identityCard);
            save(person);

            identityCard.setAddress(address);
            identityCard.setPerson(person);
            update(identityCard);

            Employee employee = new Employee();
            employee.setPerson(person);

            save(employee);

            getSession().flush();
            getSession().clear();
        }
    }

    private void trimRow(Object[] row)
    {
        for (int i = 0; i < row.length; i++)
            if (row[i] instanceof String)
                row[i] = ((String) row[i]).trim();
    }

    private IdentityCard createIdentityCard(String name, String birthday, HashSet<IdentityCardItem> hashSet, IdentityCardNumber identityCardNumber)
    {
        String[] nameData = StringUtils.split(name, " ");

        IdentityCard identityCard = new IdentityCard();
        identityCard.setLastName(StringUtils.capitalize(nameData[0].toLowerCase().trim()));
        identityCard.setFirstName(StringUtils.capitalize(nameData[1].toLowerCase().trim()));
        if (nameData.length > 2)
            identityCard.setMiddleName(StringUtils.capitalize(nameData[2].toLowerCase().trim()));
        try
        {
            identityCard.setBirthDate(DateUtils.parseDate(birthday, new String[]{"dd.MM.yyyy"}));
        } catch (ParseException e)
        {
            //_logger.error("Invalid birthday '" + birthday + "', set null birthday!");
            identityCard.setBirthDate(null);
        }

        IdentityCardItem item = new IdentityCardItem(identityCard);
        if (hashSet.contains(item))
        {
            //_logger.error("Employee '" + identityCard.getLastName() + " " + identityCard.getFirstName() + " " + identityCard.getMiddleName() + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()) + " already imported, this employee has been ignored!'");
            return null;
        } else
        {
            hashSet.add(item);
        }
        identityCard.setSeria("0000");
        identityCard.setNumber(String.format("%06d", identityCardNumber.getNumber()));
        identityCard.setIssuanceDate(new Date());
        return identityCard;
    }

    private AddressBase createAddress(String addressString)
    {
        String[] data = getSplitted(addressString);

        // String commaRaw = data[0].trim();
        // String postcode = data[1].trim();
        String regionRaw = data[2].trim();
        String regionNextRaw = data[3].trim();
        String settlementRaw = data[4].trim();
        String settlementRawRegion = data[5].trim();
        String streetRaw = data[6].trim();
        String homeRaw = data[7].trim();
        String partRaw = data[8].trim();
        String flatRaw = data[9].trim();

        String settlementParent;
        String streetParent;

        String[] levels = new String[]{settlementRawRegion, settlementRaw, regionNextRaw, regionRaw};

        int i = 0;
        while (i < levels.length && levels[i].length() == 0) i++;
        streetParent = i < levels.length ? levels[i] : "";

        i++;
        while (i < levels.length && levels[i].length() == 0) i++;
        settlementParent = i < levels.length ? levels[i] : "";

        String region = settlementParent.length() == 0 ? null : settlementParent.substring(0, settlementParent.lastIndexOf(" "));
        String regionType = settlementParent.length() == 0 ? null : settlementParent.substring(settlementParent.lastIndexOf(" ") + 1);
        String settlement;
        String settlementType;
        int index = streetParent.lastIndexOf(" ");
        if (index == -1)
        {
            //незадан тип населенного пункта
            settlement = streetParent;
            settlementType = "г";
        } else
        {
            settlement = streetParent.substring(0, streetParent.lastIndexOf(" "));
            settlementType = streetParent.substring(streetParent.lastIndexOf(" ") + 1);
        }
        index = streetRaw.lastIndexOf(" ");
        String street;
        String streetType;
        if (index == -1)
        {
            //незадан тип улицы
            street = streetRaw;
            streetType = "ул";
        } else
        {
            street = streetRaw.length() == 0 ? "" : streetRaw.substring(0, streetRaw.lastIndexOf(" "));
            streetType = streetRaw.length() == 0 ? "" : streetRaw.substring(streetRaw.lastIndexOf(" "));
        }

        String home;
        String part = null;
        if (homeRaw.length() == 0)
            home = null;
        else
        {
            try
            {
                home = homeRaw;
            } catch (NumberFormatException e)
            {
                //скорее всего в номере еще есть корпус
                //возможно корпус написан через /
                index = homeRaw.indexOf('/');
                if (index == -1)
                {
                    home = homeRaw.substring(0, homeRaw.length() - 1);
                    part = homeRaw.substring(homeRaw.length() - 1);
                } else
                {
                    home = homeRaw.substring(0, index);
                    part = homeRaw.substring(index + 1);
                }
            }
        }
        if (part == null)
            part = partRaw.length() == 0 ? null : partRaw;
        String flat = flatRaw.length() == 0 ? null : "ОБЩ".equalsIgnoreCase(flatRaw) ? "общ." : flatRaw;

        AddressItem addressItem = getAddressItem(addressString, region, regionType, settlement, settlementType);
        if (addressItem == null)
            return null;


        AddressStreet streetItem = null;
        if (street.length() > 0)
            streetItem = getAddressStreet(addressString, street, streetType, addressItem);

        AddressRu address = new AddressRu();
        address.setCountry(FiasManager.instance().kladrDao().getCountry(IKladrDefines.RUSSIA_COUNTRY_CODE));
        address.setSettlement(addressItem);
        address.setStreet(streetItem);
        address.setHouseNumber(home);
        address.setHouseUnitNumber(part);
        address.setFlatNumber(flat);

        return address;
    }

    private static class IdentityCardItem
    {
        private IdentityCard _identityCard;

        private IdentityCardItem(IdentityCard identityCard)
        {
            _identityCard = identityCard;
        }

        public IdentityCard getIdentityCard()
        {
            return _identityCard;
        }

        @Override
        public int hashCode()
        {
            String hash = _identityCard.getFirstName() + _identityCard.getLastName() + _identityCard.getMiddleName() + _identityCard.getBirthDate();
            return hash.hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            if (!(obj instanceof IdentityCardItem)) return false;

            IdentityCard other = ((IdentityCardItem) obj).getIdentityCard();
            return other.getFirstName().equals(_identityCard.getFirstName()) &&
            other.getLastName().equals(_identityCard.getLastName()) &&
            other.getMiddleName().equals(_identityCard.getMiddleName()) &&
            ((other.getBirthDate() == null && _identityCard.getBirthDate() == null) || other.getBirthDate().equals(_identityCard.getBirthDate()));
        }
    }

    @SuppressWarnings("unchecked")
    private AddressItem getAddressItem(String addressString, String region, String regionType, String settlement, String settlementType)
    {
        //получаем возможные типы населенного пункта
        Criteria c = getSession().createCriteria(AddressType.class);
        c.add(Restrictions.like(AddressType.P_SHORT_TITLE, settlementType).ignoreCase());
        c.setProjection(Projections.property(AddressType.P_ID));
        List<Long> settlementTypeIds = c.list();

        if (settlementTypeIds.isEmpty())
        {
            _logger.error("Нет подходящего типа населенного пункта: " + settlementType + " str:" + addressString);
            return null;
        }

        //получаем возможные типы области пункта
        List<Long> regionTypeIds = null;
        if (regionType != null)
        {
            c = getSession().createCriteria(AddressType.class);
            c.add(Restrictions.like(AddressType.P_SHORT_TITLE, regionType).ignoreCase());
            c.setProjection(Projections.property(AddressType.P_ID));
            regionTypeIds = c.list();
        }

        if (settlementTypeIds.isEmpty())
        {
            _logger.error("Нет подходящего типа региона: " + regionType + " str:" + addressString);
            return null;
        }

        Query q;

        //1. получаем регион
        List<Long> regionIds = null;
        if (region != null)
        {
            q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title and " + AddressItem.L_ADDRESS_TYPE + ".id in (:typeIds)");
            q.setParameter("title", region.toUpperCase());
            q.setParameterList("typeIds", regionTypeIds);
            regionIds = q.list();
        }

        //2. получаем населенный пункт
        List<Long> settlementIds;
        if (regionIds != null && !regionIds.isEmpty())
        {
            q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title and " + AddressItem.L_ADDRESS_TYPE + ".id in (:typeIds) and " + AddressItem.L_PARENT + ".id in (:regionIds)");
            q.setParameter("title", settlement.toUpperCase());
            q.setParameterList("typeIds", settlementTypeIds);
            q.setParameterList("regionIds", regionIds);
            settlementIds = q.list();
        } else
        {
            q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title and " + AddressItem.L_ADDRESS_TYPE + ".id in (:typeIds) and " + AddressItem.L_PARENT + " is null");
            q.setParameter("title", settlement.toUpperCase());
            q.setParameterList("typeIds", settlementTypeIds);
            settlementIds = q.list();
        }

        if (settlementIds.isEmpty())
        {
            //Нет подходящего населенного пункта
            //пытаемся взять населенный пункт без региона
            q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title and " + AddressItem.L_ADDRESS_TYPE + ".id in (:typeIds)");
            q.setParameter("title", settlement.toUpperCase());
            q.setParameterList("typeIds", settlementTypeIds);
            settlementIds = q.list();
            if (settlementIds.isEmpty())
            {
                //СОВСЕМ НЕТ населенного пункта
                //пытаемся взять его без типа
                if (regionIds != null && !regionIds.isEmpty())
                {
                    q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title and " + AddressItem.L_PARENT + ".id in (:regionIds)");
                    q.setParameter("title", settlement.toUpperCase());
                    q.setParameterList("regionIds", regionIds);
                    settlementIds = q.list();
                } else
                {
                    q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title and " + AddressItem.L_PARENT + " is null");
                    q.setParameter("title", settlement.toUpperCase());
                    settlementIds = q.list();
                }

                if (settlementIds.isEmpty())
                {
                    //СОВСЕМ НЕТ населенного пункта
                    //пытаемся взять его без типа и без региона
                    q = getSession().createQuery("select id from " + AddressItem.ENTITY_CLASS + " where upper(" + AddressItem.P_TITLE + ")=:title");
                    q.setParameter("title", settlement.toUpperCase());
                    settlementIds = q.list();

                    if (settlementIds.isEmpty())
                    {
                        _logger.error("СОВСЕМ НЕТ населенного пункта: " + settlementType + " " + settlement + " str:" + addressString);
                        return null;
                    }
                }
            }
            if (settlementIds.size() > 1)
            {
                List<Long> newSettlemetIds = new ArrayList<>();
                for (Long settlementId : settlementIds)
                {
                    AddressItem temp = get(AddressItem.class, settlementId);

                    if (region != null && StringUtils.containsIgnoreCase(temp.getFullTitle(), region))
                        newSettlemetIds.add(temp.getId());
                }
                settlementIds = newSettlemetIds;
            }
            if (settlementIds.isEmpty())
            {
                _logger.error("Нет населенного пункта в указанном регионе во всей иерархии: " + settlementType + " " + settlement + " в " + regionType + " " + region + ", " + " str:" + addressString);
                return null;
            }
        }

        if (settlementIds.size() > 1)
        {
            _logger.error("Много (" + settlementIds.size() + ") подходящих населенных пунктов: " + settlementType + " " + settlement + " в " + regionType + " " + region + ", " + " str:" + addressString);
            return null;
        }

        return get(AddressItem.class, settlementIds.get(0));
    }

    @SuppressWarnings("unchecked")
    private AddressStreet getAddressStreet(String addressString, String street, String streetType, AddressItem addressItem)
    {
        //получаем возможные типы улицы
        Criteria c = getSession().createCriteria(AddressType.class);
        c.add(Restrictions.like(AddressType.P_SHORT_TITLE, streetType).ignoreCase());
        c.setProjection(Projections.property(AddressType.P_ID));
        List<Long> streetTypeIds = c.list();

        if (addressItem == null) return null;
        Query q = getSession().createQuery("from " + AddressStreet.ENTITY_CLASS + " where upper(" + AddressStreet.P_TITLE + ")=:title and " + AddressStreet.L_PARENT + "=:addressItem and " + AddressStreet.L_ADDRESS_TYPE + ".id in (:streetTypeIds)");
        q.setParameter("title", street.toUpperCase());
        q.setParameter("addressItem", addressItem);
        q.setParameterList("streetTypeIds", streetTypeIds);
        List<AddressStreet> list = q.list();

        if (list.isEmpty())
        {
            //Нет подходящей улицы
            //возможно указана только часть улицы
            q = getSession().createQuery("from " + AddressStreet.ENTITY_CLASS + " where upper(" + AddressStreet.P_TITLE + ") like :title and " + AddressStreet.L_PARENT + "=:addressItem and " + AddressStreet.L_ADDRESS_TYPE + ".id in (:streetTypeIds)");
            q.setParameter("title", "%" + street.toUpperCase() + "%");
            q.setParameter("addressItem", addressItem);
            q.setParameterList("streetTypeIds", streetTypeIds);
            list = q.list();
            if (list.isEmpty())
            {
                //не вышло :(

                //Нет подходящей улицы
                //пытаемся взять улицу без региона
                q = getSession().createQuery("from " + AddressStreet.ENTITY_CLASS + " where upper(" + AddressStreet.P_TITLE + ") like :title and " + AddressStreet.L_ADDRESS_TYPE + ".id in (:streetTypeIds)");
                q.setParameter("title", street.toUpperCase());
                q.setParameterList("streetTypeIds", streetTypeIds);
                list = q.list();
                if (list.isEmpty())
                {
                    _logger.error("СОВСЕМ НЕТ подходящей улицы: " + street + " str: " + addressString);
                    return null;
                }
                if (list.size() > 1)
                {
                    List<AddressStreet> newList = new ArrayList<>();
                    for (AddressStreet item : list)
                    {
                        if (StringUtils.containsIgnoreCase(item.getParent().getFullTitle(), addressItem.getTitle()))
                            newList.add(item);
                    }
                    list = newList;
                }
                if (list.isEmpty())
                {
                    _logger.error("Нет подходящей улицы: " + street + " в " + addressItem.getFullTitle() + " str: " + addressString);
                    return null;
                }
            }
        }

        if (list.size() > 2)
        {
            _logger.error("Много (" + list.size() + ") подходящих улиц: " + street + " в " + addressItem.getFullTitle() + " str: " + addressString);
            return null;
        }

        return list.get(0);
    }

    private String[] getSplitted(String string)
    {
        List<String> list = new ArrayList<>();
        int i = 0;
        while (i <= string.length())
        {
            int begin = i;
            while (i < string.length() && string.charAt(i) != ',') i++;
            list.add(string.substring(begin, i));
            i++;
        }
        return list.toArray(new String[list.size()]);
    }

    private static class IdentityCardNumber
    {
        private int _number = 1;

        public int getNumber()
        {
            return _number++;
        }
    }
}
