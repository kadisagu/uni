/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.parser;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.*;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaDiscipline;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaPlanRow;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaVariant;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.nodeItem.NodeItem;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.nodeItem.NodeItemAdvanced;

import java.io.InputStream;
import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 03.09.2013
 */
public class ThirdGosGenerationImtsaParser extends ImtsaParser
{
    public ThirdGosGenerationImtsaParser(Document document)
    {
        super(document);
    }

    public ThirdGosGenerationImtsaParser(InputStream inputStream)
    {
        super(inputStream);
    }


    protected ImtsaCycles parseCycles()
    {
        setStage(ParseStage.CYCLES);
        Node cyclesNode;
        NodeItem cyclesNodeItem;
        if ((cyclesNode = getUniqueNodeByTitleAndParent(ImtsaTitle.getCyclesNewNodeName(), ImtsaFile.getTitleNodeName())) == null && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        Map<String, String> abbreviation2TitleMap = new HashMap<>();
        if (cyclesNode != null)
        {
            if ((cyclesNodeItem = getNodeItem(cyclesNode, Collections.singletonList(ImtsaCycles.CYCLES_NEW_NODE_VIEW))) == null && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
            {
                return null;
            }

            if (cyclesNodeItem != null)
            {
                for (NodeItem cycleNodeItem: cyclesNodeItem.getChildNodes())
                {
                    Map<String, Object> attributes = cycleNodeItem.getAttributes();
                    String cycleAbbreviation = (String) attributes.get(ImtsaCycles.getCycleNewAbbreviationAttribute());
                    String cycleTitle = (String) attributes.get(ImtsaCycles.getCycleNewTitleAttribute());
                    if ((USMA_IMTSA_IMPORT_SIMPLE_MODE && cycleAbbreviation != null && cycleTitle != null) || !USMA_IMTSA_IMPORT_SIMPLE_MODE)
                        abbreviation2TitleMap.put(cycleAbbreviation, cycleTitle);
                }
            }
        }

        ImtsaCycles result = new ImtsaCycles();
        result.setAbbreviation2TitleMap(abbreviation2TitleMap);

        return result;
    }

    @Override
    protected String getDisciplineIdAttribute()
    {
        return ImtsaDiscipline.getNewIdAttribute();
    }


    @Override
    protected String getDisciplineCycleAttribute()
    {
        return ImtsaDiscipline.getNewCycleAttribute();
    }

    @Override
    protected String getModifiedId(String id)
    {
        return id
                .replaceAll(ImtsaVariant.getVariantRegExpVariantNumberFrom(), ImtsaVariant.getVariantRegExpVariantNumberTo())
                .replaceAll(ImtsaVariant.getVariantRegExpVariablePartReqDiscFrom(), ImtsaVariant.getVariantRegExpVariablePartReqDiscTo());
    }


    @Override
    protected Map<String, Collection<ImtsaPractice>> parsePractices()
    {
        setStage(ParseStage.PRACTICES);
        Map<String, INodeViewAdvanced> nodeViewMap = new HashMap<>();

        nodeViewMap.put(ImtsaFile.getGos3TutoringPracticeGroupNodeName(), ImtsaPractice.GOS3_PRACTICE_GROUP_NODE_VIEW);
        nodeViewMap.put(ImtsaFile.getGos3NirPracticeGroupNodeName(), ImtsaPractice.GOS3_PRACTICE_GROUP_NODE_VIEW);
        nodeViewMap.put(ImtsaFile.getGos3OtherPracticeGroupNodeName(), ImtsaPractice.GOS3_PRACTICE_GROUP_NODE_VIEW);

        Node practicesNode;
        NodeItemAdvanced practicesNodeItem;
        if ((practicesNode = getUniqueNodeByTitleAndParent(ImtsaFile.getGos3SpecialWorkKindsNodeName(), ImtsaFile.getPlanNodeName())) == null || (practicesNodeItem = getNodeItemAdvanced(practicesNode, nodeViewMap)) == null)
        {
            return null;
        }

        Map<String, Collection<ImtsaPractice>> result = new HashMap<>();
        Map<String, List<NodeItemAdvanced>> childMap = practicesNodeItem.getChildMap();
        result.put(ImtsaFile.getGos3TutoringPracticeGroupNodeName(), getPractices(childMap.get(ImtsaFile.getGos2TutoringPracticeGroupNodeName())));
        result.put(ImtsaFile.getGos3NirPracticeGroupNodeName(), getPractices(childMap.get(ImtsaFile.getGos3NirPracticeGroupNodeName())));
        result.put(ImtsaFile.getGos3OtherPracticeGroupNodeName(), getPractices(childMap.get(ImtsaFile.getGos3OtherPracticeGroupNodeName())));

        return result;
    }


    private Collection<ImtsaPractice> getPractices(List<NodeItemAdvanced> practiceGroups)
    {
        if (practiceGroups == null)
        {
            return Collections.emptyList();
        }

//        одна строка - одна практика
//        Map<String, ImtsaPractice> title2PracticeMap = SafeMap.get(new SafeMap.Callback<String, ImtsaPractice>(){ @Override public ImtsaPractice resolve(String key){ return new ImtsaPractice(key); }});

        String examCode = ImtsaPractice.getControlActionCodeMap().get(ImtsaPractice.getGos3TermExamAttribute());
        String setOffCode = ImtsaPractice.getControlActionCodeMap().get(ImtsaPractice.getGos3TermSetOffAttribute());
        String setOffDiffCode = ImtsaPractice.getControlActionCodeMap().get(ImtsaPractice.getGos3TermSetOffDiffAttribute());

        List<ImtsaPractice> result = new ArrayList<>();
        for (NodeItemAdvanced practiceGroupNodeItem: practiceGroups)
        {
            for (Map.Entry<String, List<NodeItemAdvanced>> practiceEntry: practiceGroupNodeItem.getChildMap().entrySet())
            {
                for (NodeItemAdvanced practiceNodeItem: practiceEntry.getValue())
                {
                    Map<String, Object> practiceAttributes = practiceNodeItem.getAttributes();
                    String title = (String) practiceAttributes.get(ImtsaPractice.getGos3PracticeTitleAttribute());
                    ImtsaPractice practice = new ImtsaPractice(title);

                    String competenceCodesStr = (String) practiceAttributes.get(ImtsaPractice.getGos3PracticeCompetenceCodesAttribute());
                    if (competenceCodesStr != null)
                    {
                        Set<String> competenceCodes = new HashSet<>(Arrays.asList(StringUtils.split(competenceCodesStr, ImtsaPlanRow.getCompetenceCodesSeparator())));
                        practice.setCompetenceCodes(competenceCodes);
                    }

                    Double hoursAmountInZet = (Double) practiceAttributes.get(ImtsaPractice.getGos3PracticeHoursAmountInZetAttribute());
                    String type = (String) practiceAttributes.get(ImtsaPractice.getGos3PracticeTypeAttribute());
                    boolean distributed = ImtsaPractice.getDistributedPracticeCode().equals(type);
                    practice.setDispersed(distributed);

                    Collection<NodeItemAdvanced> termNodeItems = practiceNodeItem.getChildMap().get(ImtsaPractice.getGos3TermNodeName());
                    if (termNodeItems == null) { continue; }

                    Map<Integer, Map<String, Double>> term2LoadSizeMap = SafeMap.get(HashMap.class);
                    Map<String, Set<Integer>> controlAction2TermSetMap = SafeMap.get(HashSet.class);
                    for (NodeItemAdvanced termNodeItem: termNodeItems)
                    {
                        Map<String, Object> termAttributes = termNodeItem.getAttributes();
                        Integer number = (Integer) termAttributes.get(ImtsaPractice.getGos3TermNumberAttribute());
                        Double planZet = (Double) termAttributes.get(ImtsaPractice.getGos3TermPlanZetAttribute());
                        Double planHours = (Double) termAttributes.get(ImtsaPractice.getGos3TermPlanHoursAttribute());
                        Double planHoursAudit = (Double) termAttributes.get(ImtsaPractice.getGos3TermPlanHoursAuditAttribute());
                        Double planHoursSelfWork = (Double) termAttributes.get(ImtsaPractice.getGos3TermPlanHoursSelfWorkAttribute());
                        Double planWeeks = (Double) termAttributes.get(ImtsaPractice.getGos3TermPlanWeeksAttribute());
                        Integer planDays = (Integer) termAttributes.get(ImtsaPractice.getGos3TermPlanDaysAttribute());
                        /*todo практики в строках уп */

                        if (planWeeks != null || planDays != null)
                        {
                            practice.getTerm2WeeksMap().put(number, (planWeeks == null ? 0.0d : planWeeks) + (planDays == null ? 0.0d : (double) planDays / 6));
                        }

                        if (hoursAmountInZet != null && planZet != null)
                        {
                            Double planLabor = planZet * hoursAmountInZet;
                            planHours = planLabor;
                            if (planHoursAudit == null)
                            {
                                if (planHoursSelfWork != null)
                                {
                                    planHoursSelfWork = planHoursSelfWork > planHours ? planHours : planHoursSelfWork;
                                    planHoursAudit = planHours - planHoursSelfWork;
                                }

                            } else
                            {
                                planHoursAudit = planHoursAudit > planHours ? planHours : planHoursAudit;
                                planHoursSelfWork = planHours - planHoursAudit;
                            }

                        } else if (planHours != null)
                        {
                            if (planHoursAudit == null)
                            {
                                if (planHoursSelfWork != null)
                                {
                                    planHoursSelfWork = planHoursSelfWork > planHours ? planHours : planHoursSelfWork;
                                    planHoursAudit = planHours - planHoursSelfWork;
                                }

                            } else
                            {
                                planHoursAudit = planHoursAudit > planHours ? planHours : planHoursAudit;
                                planHoursSelfWork = planHours - planHoursAudit;
                            }
                        }

                        Map<String, Double> termLoadSizeMap = term2LoadSizeMap.get(number);
                        String aCode = ImtsaPractice.getLoadTypeCodeMap().get(ImtsaPractice.getGos3TermPlanHoursAuditAttribute());
                        String sCode = ImtsaPractice.getLoadTypeCodeMap().get(ImtsaPractice.getGos3TermPlanHoursSelfWorkAttribute());

                        if (planHours != null)
                        {
                            termLoadSizeMap.put(ImtsaPractice.getTotalLoadSizeCode(), planHours);
                        }

                        if (planZet != null)
                        {
                            termLoadSizeMap.put(ImtsaPractice.getTotalLaborCode(), planZet);
                        }

                        if(planHoursAudit != null && distributed)
                        {
                            termLoadSizeMap.put(aCode, planHoursAudit);
                        }

                        if (planHoursSelfWork != null && distributed)
                        {
                            termLoadSizeMap.put(sCode, planHoursSelfWork);
                        }

                        Integer exam = (Integer) termAttributes.get(ImtsaPractice.getGos3TermExamAttribute());
                        Integer setOff = (Integer) termAttributes.get(ImtsaPractice.getGos3TermSetOffAttribute());
                        Integer setOffDiff = (Integer) termAttributes.get(ImtsaPractice.getGos3TermSetOffDiffAttribute());

                        if (exam != null){ controlAction2TermSetMap.get(examCode).add(number); }
                        if (setOff != null){ controlAction2TermSetMap.get(setOffCode).add(number); }
                        if (setOffDiff != null){ controlAction2TermSetMap.get(setOffDiffCode).add(number); }

                        List<NodeItemAdvanced> chairs = termNodeItem.getChildMap().get(ImtsaPractice.getGos3ChairNodeName());
                        if (chairs != null && !chairs.isEmpty())
                        {
                            NodeItemAdvanced chairNodeItem = (NodeItemAdvanced) chairs.toArray()[0];
                            String chairCode = (String) chairNodeItem.getAttributes().get(ImtsaPractice.getGos3ChairCodeAttribute());

                            practice.setOwnerCode(chairCode);
                        }
                    }

                    term2LoadSizeMap.get(0).put(ImtsaPractice.getTotalLaborCode(), (Double) practiceAttributes.get(ImtsaPractice.getGos3PracticeExpertZetAttribute()));

                    practice.setControlAction2TermSetMap(controlAction2TermSetMap);
                    practice.setTerm2LoadSizeMap(term2LoadSizeMap);

                    result.add(practice);
                }
            }
        }

        return result;
    }
}