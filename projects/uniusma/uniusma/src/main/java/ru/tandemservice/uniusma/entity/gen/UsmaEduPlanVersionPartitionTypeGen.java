package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип разбиения для УПв
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaEduPlanVersionPartitionTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType";
    public static final String ENTITY_NAME = "usmaEduPlanVersionPartitionType";
    public static final int VERSION_HASH = -1263993674;
    private static IEntityMeta ENTITY_META;

    public static final String L_VERSION = "version";
    public static final String L_PARTITION_TYPE = "partitionType";

    private EppEduPlanVersion _version;     // УПв
    private UsmaSchedulePartitionType _partitionType;     // Тип разбиения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    /**
     * @param version УПв. Свойство не может быть null и должно быть уникальным.
     */
    public void setVersion(EppEduPlanVersion version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Тип разбиения. Свойство не может быть null.
     */
    @NotNull
    public UsmaSchedulePartitionType getPartitionType()
    {
        return _partitionType;
    }

    /**
     * @param partitionType Тип разбиения. Свойство не может быть null.
     */
    public void setPartitionType(UsmaSchedulePartitionType partitionType)
    {
        dirty(_partitionType, partitionType);
        _partitionType = partitionType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaEduPlanVersionPartitionTypeGen)
        {
            setVersion(((UsmaEduPlanVersionPartitionType)another).getVersion());
            setPartitionType(((UsmaEduPlanVersionPartitionType)another).getPartitionType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaEduPlanVersionPartitionTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaEduPlanVersionPartitionType.class;
        }

        public T newInstance()
        {
            return (T) new UsmaEduPlanVersionPartitionType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "partitionType":
                    return obj.getPartitionType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((EppEduPlanVersion) value);
                    return;
                case "partitionType":
                    obj.setPartitionType((UsmaSchedulePartitionType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "partitionType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "partitionType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return EppEduPlanVersion.class;
                case "partitionType":
                    return UsmaSchedulePartitionType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaEduPlanVersionPartitionType> _dslPath = new Path<UsmaEduPlanVersionPartitionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaEduPlanVersionPartitionType");
    }
            

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType#getVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Тип разбиения. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType#getPartitionType()
     */
    public static UsmaSchedulePartitionType.Path<UsmaSchedulePartitionType> partitionType()
    {
        return _dslPath.partitionType();
    }

    public static class Path<E extends UsmaEduPlanVersionPartitionType> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _version;
        private UsmaSchedulePartitionType.Path<UsmaSchedulePartitionType> _partitionType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType#getVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> version()
        {
            if(_version == null )
                _version = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_VERSION, this);
            return _version;
        }

    /**
     * @return Тип разбиения. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType#getPartitionType()
     */
        public UsmaSchedulePartitionType.Path<UsmaSchedulePartitionType> partitionType()
        {
            if(_partitionType == null )
                _partitionType = new UsmaSchedulePartitionType.Path<UsmaSchedulePartitionType>(L_PARTITION_TYPE, this);
            return _partitionType;
        }

        public Class getEntityClass()
        {
            return UsmaEduPlanVersionPartitionType.class;
        }

        public String getEntityName()
        {
            return "usmaEduPlanVersionPartitionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
