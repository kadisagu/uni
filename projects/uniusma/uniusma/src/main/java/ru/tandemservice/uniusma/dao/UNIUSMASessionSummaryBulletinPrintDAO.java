package ru.tandemservice.uniusma.dao;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionSummaryBulletinPrintDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.Collection;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 10.01.12
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
public class UNIUSMASessionSummaryBulletinPrintDAO extends SessionSummaryBulletinPrintDAO
{
    @Override
    protected String[] printStudentData(int studentNumber, ISessionSummaryBulletinStudent student)
    {
        String number = Integer.toString(studentNumber);
        StringBuilder fio = new StringBuilder();
        fio.append(student.getStudent().getPerson().getFullFio());

        Group group = student.getStudent().getGroup();
        if (null != group)
        {
            List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
            fio.append(studentList.contains(student.getStudent()) ? " - староста" : "");
        }

        String compType = student.getStudent().getCompensationType().isBudget() ?
                (student.getStudent().isTargetAdmission() ? "цб" : "б") :
                (student.getStudent().isTargetAdmission() ? "цк" : "к");
        return new String[]{number, fio.toString(), compType};
    }

    @Override
    protected String formatBulletinsNumber(Collection<SessionBulletinDocument> bulletins)
    {
        return "№ "+UniStringUtils.join(bulletins, SessionBulletinDocument.number().s(), ", № ");
    }
}