/**
 *$Id:$
 */
package ru.tandemservice.uniusma.base.ext.Employee.ui.PostAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEdit;

/**
 * @author Alexander Shaburov
 * @since 09.10.12
 */
@Configuration
public class EmployeePostAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniusma" + EmployeePostAddEditExtUI.class.getSimpleName();

    @Autowired
    private EmployeePostAddEdit _employeePostAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostAddEditExtUI.class))
                .create();
    }
}
