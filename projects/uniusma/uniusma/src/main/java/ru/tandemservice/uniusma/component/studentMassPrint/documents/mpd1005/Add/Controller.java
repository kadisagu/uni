/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1005.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class Controller extends ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.Controller<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
        onChangePost(component);
        onChangeExecutor(component);
    }

    public void onChangeDate(IBusinessComponent component)
    {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null))
        {
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        }
        else
            model.setContinuance(0);
    }

    public void onChangePost(IBusinessComponent component)
    {
        Model model = component.getModel();

        int i = model.getManagerPostList().indexOf(model.getManagerPostTitle());
        if (i >= 0)
            model.setManagerFio(model.getManagerFioList().get(i));
    }

    public void onChangeExecutor(IBusinessComponent component)
    {
        Model model = component.getModel();

        String executant;
        if (model.getExecutant() == null)
        {
            UserContext userContext = ContextLocal.getUserContext();
            Person currentUser = userContext != null ? PersonManager.instance().dao().getPerson(userContext.getPrincipalContext()) : null;
            executant = (currentUser == null ? null : (currentUser.getIdentityCard().getFullFio() + " "));
        }
        else executant = model.getEmployeePost().getPerson().getFullFio();

        if (executant == null)
        {
            model.setExecutant("");
            model.setTelephone("");
        }
        else
        {
            ListResult listPost = model.getEmployeePostListModel().findValues(executant);
            EmployeePost ep = (EmployeePost) listPost.getObjects().get(0);
            model.setEmployeePost(ep);
            model.setExecutant(executant);
            model.setTelephone(ep.getOrgUnit().getPhone());
        }
    }
}
