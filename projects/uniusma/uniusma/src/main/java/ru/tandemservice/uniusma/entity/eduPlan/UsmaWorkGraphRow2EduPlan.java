package ru.tandemservice.uniusma.entity.eduPlan;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * ГУП (Связь строки с учебным планом)
 */
public class UsmaWorkGraphRow2EduPlan extends UsmaWorkGraphRow2EduPlanGen
{
    public UsmaWorkGraphRow2EduPlan()
    {

    }

    public UsmaWorkGraphRow2EduPlan(UsmaWorkGraphRow workGraphRow, EppEduPlanVersion version)
    {
        this.setRow(workGraphRow);
        this.setEduPlanVersion(version);
    }
}