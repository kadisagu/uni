/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.Base;

import org.apache.commons.lang.ArrayUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public abstract class DAO<T extends Model> extends UniDao<T> implements IDAO<T>
{
    private static final DQLOrderDescriptionRegistry _orderSettings = new DQLOrderDescriptionRegistry(Student.class, "s");
    private static OrderDescription[] FIO_KEY = {new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME)};

    static
    {
        _orderSettings.setOrders(Student.PASSPORT_KEY, new OrderDescription("idCard", IdentityCard.P_SERIA), new OrderDescription("idCard", IdentityCard.P_NUMBER));
        _orderSettings.setOrders(Student.FORMATIVE_ORGUNIT_KEY, new OrderDescription("s", Student.educationOrgUnit().formativeOrgUnit().title().s()), new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(Student.TERRITORIAL_ORGUNIT_KEY, new OrderDescription("s", Student.educationOrgUnit().territorialOrgUnit().territorialTitle().s()), new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(Student.PRODUCTIVE_ORGUNIT_KEY, new OrderDescription("s", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
        _orderSettings.setOrders(Student.STATUS_KEY, (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("studentStatus", StudentStatus.P_PRIORITY)}, FIO_KEY));
        _orderSettings.setOrders(new String[]{Student.L_GROUP, Group.P_TITLE}, new OrderDescription("g", Group.P_TITLE));
    }

    @Override
    public void prepare(final T model)
    {
        if (model.getDevelopFormListModel() == null)
            model.setDevelopFormListModel(new LazySimpleSelectModel<>(DevelopForm.class).setSortProperty(DevelopForm.code().s()));

        if (model.getDevelopTechListModel() == null)
            model.setDevelopTechListModel(new LazySimpleSelectModel<>(DevelopTech.class).setSortProperty(DevelopTech.code().s()));

        if (model.getFormativeOrgUnitListModel() == null)
            model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)
            {
                @Override
                public ListResult<OrgUnit> findValues(String filter)
                {
                    if (model.getOrgUnitId() != null)
                        return new ListResult<>(DAO.this.<OrgUnit>get(model.getOrgUnitId()));

                    return super.findValues(filter);
                }
            });

        if (model.getProducingOrgUnitListModel() == null)
            model.setProducingOrgUnitListModel(new LazySimpleSelectModel<>(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)));

        if (model.getStudentStatusListModel() == null)
            model.setStudentStatusListModel(new LazySimpleSelectModel<>(getList(StudentStatus.class, StudentStatus.usedInSystem(), Boolean.TRUE, StudentStatus.priority().s())));

        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));

        model.setSexListModel(new LazySimpleSelectModel<>(Sex.class));

        model.setGroupListModel(new LazySimpleSelectModel<>(Group.class));

        model.setAdmissionYearListModel(new FullCheckSelectModel("entranceYear")
        {
            @Override
            public ListResult<Wrapper> findValues(String filter)
            {
                List<Wrapper> resultList = new ArrayList<>();

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, "s")
                        .column(DQLExpressions.property("s", Student.entranceYear()))
                        .distinct()
                        .order(DQLExpressions.property("s", Student.entranceYear()));

                List<Integer> list = getList(builder);

                long i = 1;
                for (Integer entranceYear : list)
                    resultList.add(new Wrapper(i++, entranceYear));

                return new ListResult<>(resultList);
            }
        });
    }

    @Override
    public void prepareListDataSource(T model)
    {
        DQLSelectBuilder builder = getDQLSelectBuilder(model);
        addAdditionalRestrictions(model, builder, "s");
        final DynamicListDataSource<Student> dataSource = model.getStudentDataSource();
        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());

        builder.column(property("s"));
        UniBaseUtils.createPage(dataSource, builder, getComponentSession());

        // если выводят много студентов - нужно делать кеш
        List<ViewWrapper<Student>> lst = ViewWrapper.getPatchedList(dataSource);
        wrapPatchedList(lst);
    }

    protected abstract void addAdditionalRestrictions(T model, DQLSelectBuilder builder, String alias);

    public DQLSelectBuilder getDQLSelectBuilder(T model)
    {
        IDataSettings settings = model.getSettings();

        List<Wrapper> admissionYearList = settings.get("admissionYearList");
        List<Integer> entranceYearList = CommonBaseUtil.getPropertiesList(admissionYearList, "entranceYear");

        DQLSelectBuilder builder = _orderSettings.buildDQLSelectBuilder()

                .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
                .joinPath(DQLJoinType.left, Student.status().fromAlias("s"), "studentStatus")
                .fetchPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("s"), "ou")
                .fetchPath(DQLJoinType.inner, Student.person().fromAlias("s"), "p")
                .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "idCard");

        FilterUtils.applySimpleLikeFilter(builder, "s", Student.person().identityCard().lastName(), settings.get("personLastName"));
        FilterUtils.applySimpleLikeFilter(builder, "s", Student.person().identityCard().firstName(), settings.get("personFirstName"));

        FilterUtils.applySelectFilter(builder, "s", Student.status(), settings.get("studentStatusList"));
        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().formativeOrgUnit(), settings.get("formativeOrgUnitList"));
        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool().orgUnit(), settings.get("producingOrgUnitList"));
        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().developForm(), settings.get("developFormList"));
        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().developTech(), settings.get("developTechList"));
        FilterUtils.applySelectFilter(builder, "s", Student.course(), settings.get("courseList"));
        FilterUtils.applySelectFilter(builder, "s", Student.person().identityCard().sex(), settings.get("studentSexList"));
        FilterUtils.applySelectFilter(builder, "s", Student.group(), settings.get("groupList"));
        FilterUtils.applySelectFilter(builder, "s", Student.entranceYear(), entranceYearList);


        if (!withArchivalStudents())
            builder.where(eq(property("s", Student.P_ARCHIVAL), value(Boolean.FALSE)));

        //Для табов на конкретноое форм. подразделение
        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().formativeOrgUnit().id(), model.getOrgUnitId());

        return builder;
    }

    /**
     * Тут можно внуть враппера добавить новые поля
     */
    protected abstract void wrapPatchedList(List<ViewWrapper<Student>> lst);

    public static class Wrapper extends EntityBase implements Serializable
    {
        private Long id;
        private Integer entranceYear;

        public Wrapper(Long id, Integer entranceYear)
        {
            this.id = id;
            this.entranceYear = entranceYear;
        }

        @Override
        public Long getId()
        {
            return id;
        }

        public void setId(Long id)
        {
            this.id = id;
        }

        public Integer getEntranceYear()
        {
            return entranceYear;
        }

        public void setEntranceYear(Integer entranceYear)
        {
            this.entranceYear = entranceYear;
        }
    }

    protected abstract boolean withArchivalStudents();
}
