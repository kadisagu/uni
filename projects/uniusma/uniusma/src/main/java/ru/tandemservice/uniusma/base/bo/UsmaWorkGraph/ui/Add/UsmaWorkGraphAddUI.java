/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 04.10.2013
 */
public class UsmaWorkGraphAddUI extends UIPresenter
{
    private UsmaWorkGraph _workGraph = new UsmaWorkGraph();
    private DevelopCombination _combination;

    public UsmaWorkGraph getWorkGraph(){ return _workGraph; }

    public boolean isDevelopAttributesDisabled(){ return _combination != null; }

    public DevelopCombination getCombination(){ return _combination; }
    public void setCombination(DevelopCombination combination){ _combination = combination; }

    public void onChangeDevelopCombination()
    {
        if (_combination != null)
        {
            _workGraph.setDevelopForm(_combination.getDevelopForm());
            _workGraph.setDevelopCondition(_combination.getDevelopCondition());
            _workGraph.setDevelopTech(_combination.getDevelopTech());
            _workGraph.setDevelopGrid(_combination.getDevelopGrid());
        }
    }

    public void onClickApply()
    {
        EppState state = UsmaWorkGraphManager.instance().dao().getByNaturalId(new EppState.NaturalId(EppState.STATE_FORMATIVE));
        _workGraph.setState(state);
        UsmaWorkGraphManager.instance().dao().save(_workGraph);
        deactivate();
    }
}