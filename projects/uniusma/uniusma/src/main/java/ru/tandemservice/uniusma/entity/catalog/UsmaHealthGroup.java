package ru.tandemservice.uniusma.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniusma.entity.catalog.gen.UsmaHealthGroupGen;

/**
 * Группы здоровья
 */
public class UsmaHealthGroup extends UsmaHealthGroupGen implements IDynamicCatalogItem
{
}