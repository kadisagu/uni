package ru.tandemservice.uniusma.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniusma.entity.catalog.gen.UsmaDiseasesKindGen;

/**
 * Виды заболеваний
 */
public class UsmaDiseasesKind extends UsmaDiseasesKindGen implements IDynamicCatalogItem
{
}