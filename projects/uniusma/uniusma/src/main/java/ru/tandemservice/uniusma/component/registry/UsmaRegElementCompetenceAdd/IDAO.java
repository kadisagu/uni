/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.registry.UsmaRegElementCompetenceAdd;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public interface IDAO extends IUniDao<Model>
{
}