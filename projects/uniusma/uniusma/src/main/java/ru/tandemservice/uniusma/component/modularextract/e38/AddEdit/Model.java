/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e38.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 26.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.Model
{
    WeekendChildOutStuExtractUsmaExt _extUsmaExt;

    public WeekendChildOutStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(WeekendChildOutStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
