/**
 *$Id$
 */
package ru.tandemservice.uniusma.tapestry.richTableList;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichTableList;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.callback.ParamCallback;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.SimpleColumnRender;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public class UsmaWeekTypeBlockColumnRender extends SimpleColumnRender
{
    private static final Map<Integer, String> PARTS_NUMBER_TO_MERGED_WEEK_KLASS = new HashMap<>();
    static
    {
        PARTS_NUMBER_TO_MERGED_WEEK_KLASS.put(1, "usma-input-single");
        PARTS_NUMBER_TO_MERGED_WEEK_KLASS.put(2, "usma-input-double");
        PARTS_NUMBER_TO_MERGED_WEEK_KLASS.put(3, "usma-input-triple");
        PARTS_NUMBER_TO_MERGED_WEEK_KLASS.put(6, "usma-input-six");
    }

    @Override
    public void render(final IMarkupWriter writer, final IRequestCycle cycle, final RichTableList tableList)
    {
        final UsmaWeekTypeBlockColumn column = (UsmaWeekTypeBlockColumn) tableList.getDataSource().getCurrentColumn();

        final Long rowId = tableList.getDataSource().getCurrentEntity().getId();
        final Long colId = column.getColumnId();
        final String prefix = colId + "_" + rowId;

        final UsmaRangeSelectionWeekTypeListDataSource<?> dataSource = (UsmaRangeSelectionWeekTypeListDataSource<?>) tableList.getModel();

        Map<Long, Map<Long, Map<Integer, EppWeekType>>> fullDataMap = dataSource.getFullDataMap();

        Map<Long, Map<Integer, EppWeekType>> rowMap = fullDataMap.get(rowId);
        int rowSize = Collections.max(rowMap.values(), new Comparator<Map<Integer, EppWeekType>>()
        {
            @Override
            public int compare(Map<Integer, EppWeekType> o1, Map<Integer, EppWeekType> o2)
            {
                return o1.size() - o2.size();
            }
        }).size();

        Map<Integer, EppWeekType> weekPartMap = rowMap.get(colId);

        int weekPartSize = weekPartMap.size();
        if (rowId.equals(dataSource.getEditId()))
        {
            // активная строка
            final String clientId = tableList.getClientId();

            // проверяем, что строка доступна для редкатирования содержимого (границы двигать можно)
            final boolean editDisabled = column.isDisabled(tableList.getDataSource().getCurrentEntity());

            final MutableInt number = new MutableInt(weekPartSize == 1 ? 0 : 1);

            final ParamCallback.IParamBinding paramListener = editDisabled ? null : new ParamCallback.IParamBinding()
            {
                @Override
                public void rewind(final IRequestCycle cycle, final RichTableList tableList, final String value)
                {
                    final UsmaRangeSelectionWeekTypeListDataSource<?> model = (UsmaRangeSelectionWeekTypeListDataSource<?>) tableList.getModel();

                    Map<Integer, EppWeekType> localWeekPartMap = model.getFullDataMap().get(rowId).get(colId);
                    int partNumber = number.intValue();
                    number.increment();

                    try
                    {
                        final EppWeekType weekType = model.getWeekType(value);

                        if (null == weekType)
                        {
                            localWeekPartMap.put(partNumber, null);

                        } else
                        {
                            localWeekPartMap.put(partNumber, weekType);
                        }

                    } catch (ValidatorException e)
                    {
                        tableList.getForm().getDelegate().record(e);
                        EppWeekType tmp = new EppWeekType();
                        tmp.setAbbreviationEdit(value);
                        localWeekPartMap.put(partNumber, tmp);
                    }
                }
            };

            final RichRangeSelection selection = dataSource.getSelection();

            final StringBuilder sb = new StringBuilder("rr-cell rr-edit");
            final int type = selection.getPointType(column.getColumnIdx());
            switch (type)
            {
                case RichRangeSelection.LEFT:
                    sb.append(" rr-left");
                    break;
                case RichRangeSelection.RIGHT:
                    sb.append(" rr-right");
                    break;
                case RichRangeSelection.BOTH:
                    sb.append(" rr-both");
                    break;
            }

            writer.begin("td");
            writer.attribute("class", sb.toString());

            {
                for (Integer k : new TreeSet<>(weekPartMap.keySet()))
                {
                    EppWeekType value = weekPartMap.get(k);

                    writer.beginEmpty("input");

                    if (null != paramListener)
                    {
                        writer.attribute("name", tableList.registerParamBinding(prefix + "_" + k + "_input", paramListener));
                        writer.attribute("onclick", "event.cancelBubble=true;return false;");

                    } else
                    {
                        writer.attribute("disabled", "true");
                    }

                    writer.attribute("type", "text");

                    if (null != value)
                    {
                        try
                        {
                            final UsmaRangeSelectionWeekTypeListDataSource<?> model = (UsmaRangeSelectionWeekTypeListDataSource<?>) tableList.getModel();
                            model.getWeekType(value.getAbbreviationEdit());

                        } catch (ValidatorException validatorException)
                        {
                            writer.attribute("style", "background-color:#ffe8f3");
                            writer.attribute("title", validatorException.getMessage());
                        }
                    }

                    String klass = weekPartSize == 1 ? PARTS_NUMBER_TO_MERGED_WEEK_KLASS.get(rowSize) : PARTS_NUMBER_TO_MERGED_WEEK_KLASS.get(1);

                    writer.attribute("class", klass);
                    writer.attribute("maxlength", "2");
                    writer.attribute("value", (null == value ? "" : value.getAbbreviationEdit()));
                    writer.attribute("onkeydown", "saveonenter(event)");
                    writer.closeTag();
                }
            }

            {
                writer.begin("div");
                StringBuilder klassBuilder = new StringBuilder("cell-link");
                if (dataSource.isSplittable())
                {
                    klassBuilder.append(" usma-pointer");
                    writer.attribute("onclick", "buttonClick(event,this);return false;");
                }

                writer.attribute("class", klassBuilder.toString());
                writer.attribute("id", tableList.registerSubmitListener(prefix + "_splitweek_submit", "onClickSplitWeek", column.getColumnId(), clientId));

                {
                    writer.begin("img");
                    String src;
                    String title;
                    if (weekPartSize == 1)
                    {
                        src = dataSource.isSplittable() ? "img/general/toggle_off.png" :  "img/general/toggle_off_disabled.png";
                        title = "Неделя не разбита";

                    } else
                    {
                        src = "img/general/toggle_on.png";
                        title = "Неделя разбита";
                    }

                    writer.attribute("src", src);
                    writer.attribute("title", title);
                    writer.end("img");
                }

                writer.end("div");
            }

            writer.begin("div");
            if (dataSource.getSelection().isCanClick(column.getColumnIdx()))
            {
                writer.attribute("id", tableList.registerSubmitListener(prefix + "_submit", "onClickPoint", column.getColumnIdx(), clientId));
                writer.attribute("onclick", "buttonClick(event,this);return false;");
                writer.attribute("class", "usma-click-cell usma-pointer");

            }

            {
                writer.begin("table");
                writer.attribute("class", "usma-simple-table");
                {
                    writer.begin("tr");
                    {
                        writer.begin("td");
                        writer.attribute("class", "usma-simple-td");
                        writer.end("td");
                    }
                    writer.end("tr");
                }
                writer.end("table");
            }

            writer.end("div");

            writer.end("td");

        } else
        {
            // пассивная строка
            final int[] data = dataSource.getRow2data().get(rowId);
            final int type = data == null ? RichRangeSelection.EMPTY : data[column.getColumnIdx()];

            writer.begin("td");
            switch (type)
            {
                case RichRangeSelection.EMPTY:
                    writer.attribute("class", "rr-cell");
                    break;

                case RichRangeSelection.LEFT:
                    writer.attribute("class", "rr-cell rr-left");
                    break;

                case RichRangeSelection.RIGHT:
                    writer.attribute("class", "rr-cell rr-right");
                    break;

                case RichRangeSelection.BOTH:
                    writer.attribute("class", "rr-cell rr-both");
                    break;
            }

            renderCell(writer, weekPartMap);

            writer.end("td");
        }
    }

    private void renderCell(IMarkupWriter writer, Map<Integer, EppWeekType> weekPartMap)
    {
        if (weekPartMap.size() > 1)
        {
            writer.begin("table");
            writer.attribute("class", "usma-simple-table");

            for (Integer key: new TreeSet<>(weekPartMap.keySet()))
            {
                writer.begin("tr");
                {
                    writer.begin("td");
                    writer.attribute("class", "rr-cell");
                    writer.print(weekPartMap.get(key).getAbbreviationView());
                    writer.end("td");
                }
                writer.end("tr");
            }
            writer.end("table");

        } else
        {
            EppWeekType weekType = weekPartMap.get(0);
            writer.print(weekType == null ? "" : weekType.getAbbreviationView());
        }
    }
}