/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.VersionCheckStateTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.UsmaEduPlanManager;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;

import java.util.Date;

/**
 * @author Alexander Zhebko
 * @since 06.12.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "versionId"))
public class UsmaEduPlanVersionCheckStateTabUI extends UIPresenter
{
    private Long _versionId;
    private UsmaEpvCheckState _checkState;

    public Long getVersionId(){ return _versionId; }
    public void setVersionId(Long versionId){ _versionId = versionId; }

    public UsmaEpvCheckState getCheckState(){ return _checkState; }
    public void setCheckState(UsmaEpvCheckState checkState){ _checkState = checkState; }

    // calculated
    public boolean isCheckByUMUVisible(){ return !_checkState.isCheckedByUMU(); }
    public boolean isUncheckByUMUVisible(){ return _checkState.isCheckedByUMU(); }

    public boolean isCheckByCRKVisible(){ return !_checkState.isCheckedByCRK(); }
    public boolean isUncheckByCRKVisible(){ return _checkState.isCheckedByCRK(); }

    public boolean isCheckByOOPVisible(){ return !_checkState.isCheckedByOOP(); }
    public boolean isUncheckByOOPVisible(){ return _checkState.isCheckedByOOP(); }

    public boolean isAccepted(){return _checkState.getVersion().getState().getCode().equals(EppState.STATE_ACCEPTED);}
    public boolean isRejected(){return _checkState.getVersion().getState().getCode().equals(EppState.STATE_REJECTED);}

    public boolean isEduPlanAccepted(){return _checkState.getVersion().getEduPlan().getState().getCode().equals(EppState.STATE_ACCEPTED);}

    @Override
    public void onComponentRefresh()
    {
        _checkState = UsmaEduPlanManager.instance().dao().getEpvCheckState(_versionId);
    }


    // listeners
    public void onClickCheckByUMU()
    {
        _checkState.setCheckedByUMU(true);
        _checkState.setCheckedByUMUDate(new Date());
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickUncheckByUMU()
    {
        _checkState.setCheckedByUMU(false);
        _checkState.setCheckedByUMUDate(null);
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickCheckByCRK()
    {
        _checkState.setCheckedByCRK(true);
        _checkState.setCheckedByCRKDate(new Date());
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickUncheckByCRK()
    {
        _checkState.setCheckedByCRK(false);
        _checkState.setCheckedByCRKDate(null);
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickCheckByOOP()
    {
        _checkState.setCheckedByOOP(true);
        _checkState.setCheckedByOOPDate(new Date());
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickUncheckByOOP()
    {
        _checkState.setCheckedByOOP(false);
        _checkState.setCheckedByOOPDate(null);
        UsmaEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }
}