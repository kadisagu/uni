package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaHealthGroup

		// создана новая сущность
	    if(!tool.tableExists("usmahealthgroup_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmahealthgroup_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaHealthGroup");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usmaVaccinationSign

		// создана новая сущность
	    if(!tool.tableExists("usmavaccinationsign_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("usmavaccinationsign_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usmaVaccinationSign");
		}

	    //---------------------------------------------------------------------
	    ////////////////////////////////////////////////////////////////////////////////
	    // сущность usmaStudentVaccineInfo

	    // создано свойство vaccinationSign
	    if(!tool.columnExists("usmastudentvaccineinfo_t", "vaccinationsign_id"))
	    {
		    // создать колонку
		    tool.createColumn("usmastudentvaccineinfo_t", new DBColumn("vaccinationsign_id", DBType.LONG));

		    Statement statement = tool.getConnection().createStatement();
		    statement.execute("SELECT DISTINCT sign_p from usmastudentvaccineinfo_t  WHERE sign_p IS NOT NULL ORDER BY sign_p ASC");

		    PreparedStatement insertSign = tool.prepareStatement("insert into usmavaccinationsign_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)");

		    ResultSet insertTab = statement.getResultSet();

		    Integer code = 0;
		    short entitySignCode = tool.entityCodes().ensure("usmaVaccinationSign");
		    // Заполняем справочник "usmavaccinationsign_t" записями из таблицы
		    while (insertTab.next())
		    {
			    String signTitle = insertTab.getString(1);

			    Long id = EntityIDGenerator.generateNewId(entitySignCode);
			    insertSign.setLong(1, id);
			    insertSign.setShort(2, entitySignCode);
			    insertSign.setString(3, String.valueOf(code++));
			    insertSign.setString(4, signTitle);
			    insertSign.executeUpdate();
		    }

		    // Заполняем поле vaccinationsign_id значениями из кароточек здравпункта
		    tool.executeUpdate("update usmastudentvaccineinfo_t set vaccinationsign_id = (select c.id from usmavaccinationsign_t c where c.title_p=sign_p)");
	    }

	    if(tool.columnExists("usmastudentvaccineinfo_t", "sign_p"))
	    {
		    tool.dropColumn("usmastudentvaccineinfo_t", "sign_p");
	    }

	    // создано свойство healthGroup
	    if(!tool.columnExists("usmastudentvaccineinfo_t", "healthgroup_id"))
	    {
		    // создать колонку
		    tool.createColumn("usmastudentvaccineinfo_t", new DBColumn("healthgroup_id", DBType.LONG));

	    }

	    // создано обязательное свойство dispensary
	    if(!tool.columnExists("usmastudentvaccineinfo_t", "dispensary_p"))
	    {
		    // создать колонку
		    tool.createColumn("usmastudentvaccineinfo_t", new DBColumn("dispensary_p", DBType.BOOLEAN));

		    // задать значение по умолчанию
		    Boolean defaultDispensary = false;
		    tool.executeUpdate("update usmastudentvaccineinfo_t set dispensary_p=? where dispensary_p is null", defaultDispensary);

		    // сделать колонку NOT NULL
		    tool.setColumnNullable("usmastudentvaccineinfo_t", "dispensary_p", false);

	    }

    }
}