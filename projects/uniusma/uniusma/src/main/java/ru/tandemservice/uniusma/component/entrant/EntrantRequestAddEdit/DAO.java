/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.entrant.EntrantRequestAddEdit;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model;

/**
 * @author oleyba
 * @since 18.06.2009
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.getRequestedEnrollmentDirection().setStudentCategory(getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT));
    }
}
