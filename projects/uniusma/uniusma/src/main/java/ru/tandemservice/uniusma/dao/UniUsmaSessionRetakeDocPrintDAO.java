/* $Id$ */
package ru.tandemservice.uniusma.dao;

import ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 29.12.2014
 */
public class UniUsmaSessionRetakeDocPrintDAO extends SessionRetakeDocPrintDAO
{
    @Override
    protected List<ColumnType> prepareColumnList(BulletinPrintInfo printData)
    {
        List<ColumnType> columnTypeList = super.prepareColumnList(printData);
        columnTypeList.remove(ColumnType.GROUP);
        return columnTypeList;
    }
}
