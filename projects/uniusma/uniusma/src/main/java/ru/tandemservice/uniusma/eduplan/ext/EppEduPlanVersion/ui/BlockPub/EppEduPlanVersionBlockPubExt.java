/* $Id$ */
package ru.tandemservice.uniusma.eduplan.ext.EppEduPlanVersion.ui.BlockPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub.EppEduPlanVersionBlockPub;

/**
 * @author azhebko
 * @since 03.10.2014
 */
@Configuration
public class EppEduPlanVersionBlockPubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_USMA_EDU_PLAN_VERSION_BLOCK_PUB = "usmaEduPlanVersionBlockPubAddon";

    @Autowired
    private EppEduPlanVersionBlockPub _eppEduPlanVersionBlockPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlanVersionBlockPub.presenterExtPoint())
//            .addAddon(uiAddon(ADDON_USMA_EDU_PLAN_VERSION_BLOCK_PUB, UsmaEduPlanVersionBlockPubAddon.class))
            .create();
    }

    @Bean
    public ButtonListExtension blockButtonListExtension()
    {
        return buttonListExtensionBuilder(_eppEduPlanVersionBlockPub.blockActionButtonListExtPoint())
            .create();
    }
}