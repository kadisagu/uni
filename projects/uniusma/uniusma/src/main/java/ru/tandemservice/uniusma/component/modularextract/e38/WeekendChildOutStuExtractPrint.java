/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.modularextract.e38;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.WeekendChildOutStuExtractUsmaExtGen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 15.03.2010
 */
public class WeekendChildOutStuExtractPrint implements IPrintFormCreator<WeekendChildOutStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildOutStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();

        short number = 1;

        if (extract.isAllowIndependentSchedule())
        {
            modifier.put("independentScheduleLine", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " предоставить свободное посещение занятий по согласованию с заведующими кафедр, в связи с рождением ребенка с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getIndepSchedBeginDate()) + " г. по " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getIndepSchedEndDate()) + " г.");
        }
        else
            tagsToDel.add("independentScheduleLine");

        WeekendChildOutStuExtractUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new WeekendChildOutStuExtractUsmaExtGen.NaturalId(extract));

        if (null != extractExt)
        {
            if (extractExt.getStopCompensationPayment())
            {
                modifier.put("stopCompensationPayment", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                " прекратить выплату ежемесячной компенсационной выплаты в соответствии с Постановлением Правительства РФ от 03.11.1994 г. № 1206 «Об утверждении порядка назначения и выплаты ежемесячных компенсационных выплат отдельным категориям граждан».");
            }
            else
                tagsToDel.add("stopCompensationPayment");

            if (extractExt.getRestartGrantPaying())
            {
                modifier.put("restartGrantPaying", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                " назначить выплату академической стипендии с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getRestartGrantPayingDate()) + " г.");
            }
            else
                tagsToDel.add("restartGrantPaying");
        }
        else
        {
            tagsToDel.add("stopCompensationPayment");
            tagsToDel.add("restartGrantPaying");
        }

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}