/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic;

import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2014
 */
public interface IUsmaMedCardReportDao extends ISharedBaseDao
{
    /**
     * Возвращает шаблон печатного выписки для здравпункта
     *
     * @return шаблон печатной формы
     */
    public ITemplateDocument getTemplateDocument();
}
