package ru.tandemservice.uniusma.entity.eduPlan;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * ГУП (Строка для курса)
 */
public class UsmaWorkGraphRow extends UsmaWorkGraphRowGen
{
    public UsmaWorkGraphRow()
    {

    }

    public UsmaWorkGraphRow(UsmaWorkGraph workGraph, Course course)
    {
        this.setGraph(workGraph);
        this.setCourse(course);
    }
}