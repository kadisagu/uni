/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic;

import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uniusma.UniusmaDefines;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2014
 */
public class UsmaMedCardReportDao extends SharedBaseDao implements IUsmaMedCardReportDao
{
	public ITemplateDocument getTemplateDocument()
	{
		return getCatalogItem(TemplateDocument.class, UniusmaDefines.TEMPLATE_MED_CARD_REPORT);
	}
}
