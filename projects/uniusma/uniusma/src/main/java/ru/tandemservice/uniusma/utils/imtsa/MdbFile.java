/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.imtsa;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Zhebko
 * @since 17.05.2013
 */
public class MdbFile extends ImtsaLogable
{
    private static final boolean forceRootStructureElement = true; /* все структурные элементы автоматом перемещать в основной блок, даже если их там не было */

    private static final Pattern OKSO = Pattern.compile("^([0-9]+([.][0-9])?)[ ].*");

    private static final String getOkso(final String title)
    {
        final Matcher matcher = OKSO.matcher(title);
        if (matcher.find())
        {
            return matcher.group(1);
        }
        return "";
    }

    private final Database mdb;

    private final Set<String> epvStructureCodes = new HashSet<>();
    private final Map<String, String[]> eduHs2DataMap = new HashMap<>();
    private final Map<String, String> epvId2RootEduHsIdMap = new HashMap<>();
    private final Map<String, String> orgUnitCode2IdMap = new HashMap<>();
    private final Map<String, Set<String>> epvId2BlockEduHsIdsMap = new HashMap<>();

    private final Set<String> ignoredScheduleEpvIds;
    public Set<String> getIgnoredScheduleEpvIds(){ return ignoredScheduleEpvIds; }

    private final Map<String, Map<Integer, Integer>> developGridMetaMap;
    public Map<String, Map<Integer, Integer>> getDevelopGridMetaMap(){ return developGridMetaMap; }

    private final Table t_epv_row;
    private final Table t_epv_row_term;
    private final Table t_competence;
    private final Table t_epv_row_competence;
    private final Table t_epv_schedule;
    private final Table t_develop_grid_meta;
    private final Table t_epp_epv_block_title;
    private final Table t_epp_developer;
    private final Table t_epp_qualification;
    private final Table t_epp_speciality;

    public MdbFile(final File mdbFile)
    {
        try
        {
            this.mdb = Database.open(mdbFile, false, false);
        } catch (final Throwable t)
        {
            throw new RuntimeException("Не удается открыть файл базы данных:\n" + t.getMessage(), t);
        }

        try
        {
            // таблицы в которые будет идти запись
            this.t_epv_row = this.mdb.getTable("epp_eduplanversion_row_t");
            this.t_epv_row_term = this.mdb.getTable("epp_eduplanversion_row_term_t");
            this.t_competence = this.mdb.getTable("competence_t");
            this.t_epv_row_competence = this.mdb.getTable("epp_eduplanversion_row_competence_t");
            this.t_epv_schedule = this.mdb.getTable("epp_epv_schedule_t");
            this.t_develop_grid_meta = this.mdb.getTable("epp_develop_grid_meta_t");
            this.t_epp_epv_block_title = this.mdb.getTable("epp_epv_block_title_t");
            this.t_epp_developer = this.mdb.getTable("epp_developer_t");
            this.t_epp_qualification = this.mdb.getTable("epp_qualification_t");
            this.t_epp_speciality = this.mdb.getTable("epp_speciality_t");


            // заполняем коды справочника структуры УП
            for (final Map<String, Object> row : this.mdb.getTable("catalog_eppPlanStructure"))
            {
                final String code = StringUtils.substringBefore((String) row.get("text"), " ");
                this.epvStructureCodes.add(code);
            }

            // заполняем данные по НПВ
            for (final Map<String, Object> row : this.mdb.getTable("edu_lvlhs_t"))
            {
                final String id = (String) row.get("id");
                final String[] data = new String[]{
                        (String) row.get("orgunit_id"),
                        getOkso((String) row.get("lvl_title"))
                };
                if (null != this.eduHs2DataMap.put(id, data))
                {
                    throw new IllegalStateException("Повторяющаяся строка таблицы «edu_lvlhs_t» для НПВ «" + id + "»");
                }
            }

            // заполняем список основных НПВ для УП(в)
            for (final Map<String, Object> row : this.mdb.getTable("epp_eduplanversion_t"))
            {
                final String id = (String) row.get("id");
                if (null != this.epvId2RootEduHsIdMap.put(id, (String) row.get("plan_eduhs_id")))
                {
                    throw new IllegalStateException("Повторяющаяся строка таблицы «epp_eduplanversion_t» для версии «" + id + "»");
                }
            }

            // заполняем список доступных НПВ для УП(в)
            for (final Map<String, Object> row : this.mdb.getTable("epp_eduplanversion_block_t"))
            {
                final String versionId = (String) row.get("version_id");
                Set<String> eduHsIds = this.epvId2BlockEduHsIdsMap.get(versionId);
                if (null == eduHsIds)
                {
                    this.epvId2BlockEduHsIdsMap.put(versionId, eduHsIds = new HashSet<>(4));
                }
                eduHsIds.add((String) row.get("eduhs_id"));
            }

            // заполняем коды читающих подразделений
            for (final Map<String, Object> row : this.mdb.getTable("epp_tutor_orgunit_t"))
            {
                final String orgUnitId = (String) row.get("orgunit_id");
                final String imtsaCode = (String) row.get("imtsa_code");
                if (null != this.orgUnitCode2IdMap.put(imtsaCode, orgUnitId))
                {
                    throw new IllegalStateException("Повторяющийся код в таблицы «epp_tutor_orgunit_t» для подразделения «" + orgUnitId + "»");
                }
            }


            // заполняем идентификаторы УПв, учебные графики которых не импортируются
            Set<String> ids = new HashSet<>();
            for (final Map<String, Object> row : this.t_epv_schedule)
            {
                 ids.add((String) row.get("id"));
            }
            ignoredScheduleEpvIds = Collections.unmodifiableSet(ids);


            // заполняем мета-информацию учебной сетки выгружаемых УПв
            Map<String, Map<Integer, Integer>> metaMap = new HashMap<>();
            Map<Character, Integer> courseMap = SafeMap.get(key -> Integer.parseInt(String.valueOf(key)));
            for (final Map<String, Object> row : this.t_develop_grid_meta)
            {
                String versionId = (String) row.get("version_id");
                String developGridMeta = (String) row.get("develop_grid_meta_p");

                Map<Character, MutableInt> charMap = SafeMap.get(MutableInt.class);
                Map<Integer, Integer> map = new HashMap<>();
                for (int i = 0; i < developGridMeta.length(); i++)
                {
                    charMap.get(developGridMeta.charAt(i)).increment();
                }

                for (Map.Entry<Character, MutableInt> entry: charMap.entrySet())
                {
                    map.put(courseMap.get(entry.getKey()), entry.getValue().intValue());
                }

                metaMap.put(versionId, map);
            }
            developGridMetaMap = Collections.unmodifiableMap(metaMap);

        } catch (final Throwable t)
        {
            throw new RuntimeException("Неверная структура базы данных:\n" + t.getMessage(), t);
        }
    }

    public static class MdbCompetence
    {
        private String eduHsId;
        public String getEduHsId() { return eduHsId; }

        private String skillGroup;
        public String getSkillGroup(){ return skillGroup; }

        private int number;
        public int getNumber(){ return number; }

        private String content;
        public String getContent(){ return content; }

        private MdbCompetence(String eduHsId, String skillGroup, int number, String content)
        {
            this.eduHsId = eduHsId;
            this.skillGroup = skillGroup;
            this.number = number;
            this.content = content;
        }
    }

    public static class MdbSchedule
    {
        private int course;
        public int getCourse(){ return course; }

        private int term;
        public int getTerm(){ return term; }

        private int part;
        public int getPart(){ return part; }

        private String schedule;
        public String getSchedule(){ return schedule; }

        private MdbSchedule(int course, int term , int part, String schedule)
        {
            this.course = course;
            this.term = term;
            this.part = part;
            this.schedule = schedule;
        }

        public static MdbSchedule get(int course, int term, int part, String schedule)
        {
            return new MdbSchedule(course, term, part, schedule);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MdbSchedule that = (MdbSchedule) o;

            return this.course == that.course &&
                    this.term == that.term &&
                    this.part == that.part &&
                    this.schedule.equals(that.schedule);
        }

        @Override
        public int hashCode()
        {
            return course & term & part & schedule.hashCode();
        }
    }

    public static class MdbTitle
    {
        private String eduHsId;
        private String planFullTitle;
        private String planTitle;
        private String userNumber;
        private String academy;
        private String orgUnit;
        private String higherEchelon;
        private String producingOrgUnit;
        private String formativeOrgUnit;
        private String educationDirectionCode;
        private Integer startYear;
        private Boolean includeExamsInHoursAmount;
        private Boolean dissertationAsAttestation;
        private Boolean stateExamAsAttestation;
        private String ksrOrIndividualLessons;
        private Double zetInWeekAtt;
        private Integer hoursAmountInZetAtt;
        private Double programLabourUnits;
        private Double interactiveLessonsPercent;
        private Double lecturesPercent;
        private Double choiceDisciplinePercent;
        private Double maxSize;
        private String planKind;
        private String levelCode;
        private Integer termsInCourse;
        private Integer elementsInWeek;
        private Date stateExamDate;
        private String stateExamDoc;
        private String stateExamType;
        private String application;
        private Date applicationDate;
        private String applicationVersion;
        private Double zetInYear;
        private Double zetInWeek;
        private Integer hoursAmountInZET;
        private Double totalZET;
        private Date certificateDate;

        public String getEduHsId()
        {
            return eduHsId;
        }

        public void setEduHsId(String eduHsId)
        {
            this.eduHsId = eduHsId;
        }

        public String getPlanFullTitle()
        {
            return planFullTitle;
        }

        public void setPlanFullTitle(String planFullTitle)
        {
            this.planFullTitle = planFullTitle;
        }

        public String getPlanTitle()
        {
            return planTitle;
        }

        public void setPlanTitle(String planTitle)
        {
            this.planTitle = planTitle;
        }

        public String getUserNumber()
        {
            return userNumber;
        }

        public void setUserNumber(String userNumber)
        {
            this.userNumber = userNumber;
        }

        public String getAcademy()
        {
            return academy;
        }

        public void setAcademy(String academy)
        {
            this.academy = academy;
        }

        public String getOrgUnit()
        {
            return orgUnit;
        }

        public void setOrgUnit(String orgUnit)
        {
            this.orgUnit = orgUnit;
        }

        public String getHigherEchelon()
        {
            return higherEchelon;
        }

        public void setHigherEchelon(String higherEchelon)
        {
            this.higherEchelon = higherEchelon;
        }

        public String getProducingOrgUnit()
        {
            return producingOrgUnit;
        }

        public void setProducingOrgUnit(String producingOrgUnit)
        {
            this.producingOrgUnit = producingOrgUnit;
        }

        public String getFormativeOrgUnit()
        {
            return formativeOrgUnit;
        }

        public void setFormativeOrgUnit(String formativeOrgUnit)
        {
            this.formativeOrgUnit = formativeOrgUnit;
        }

        public String getEducationDirectionCode()
        {
            return educationDirectionCode;
        }

        public void setEducationDirectionCode(String educationDirectionCode)
        {
            this.educationDirectionCode = educationDirectionCode;
        }

        public Integer getStartYear()
        {
            return startYear;
        }

        public void setStartYear(Integer startYear)
        {
            this.startYear = startYear;
        }

        public Boolean isIncludeExamsInHoursAmount()
        {
            return includeExamsInHoursAmount;
        }

        public void setIncludeExamsInHoursAmount(Boolean includeExamsInHoursAmount)
        {
            this.includeExamsInHoursAmount = includeExamsInHoursAmount;
        }

        public Boolean isDissertationAsAttestation()
        {
            return dissertationAsAttestation;
        }

        public void setDissertationAsAttestation(Boolean dissertationAsAttestation)
        {
            this.dissertationAsAttestation = dissertationAsAttestation;
        }

        public Boolean isStateExamAsAttestation()
        {
            return stateExamAsAttestation;
        }

        public void setStateExamAsAttestation(Boolean stateExamAsAttestation)
        {
            this.stateExamAsAttestation = stateExamAsAttestation;
        }

        public String getKsrOrIndividualLessons()
        {
            return ksrOrIndividualLessons;
        }

        public void setKsrOrIndividualLessons(String ksrOrIndividualLessons)
        {
            this.ksrOrIndividualLessons = ksrOrIndividualLessons;
        }

        public Double getZetInWeekAtt()
        {
            return zetInWeekAtt;
        }

        public void setZetInWeekAtt(Double zetInWeekAtt)
        {
            this.zetInWeekAtt = zetInWeekAtt;
        }

        public Integer getHoursAmountInZetAtt()
        {
            return hoursAmountInZetAtt;
        }

        public void setHoursAmountInZetAtt(Integer hoursAmountInZetAtt)
        {
            this.hoursAmountInZetAtt = hoursAmountInZetAtt;
        }

        public Double getProgramLabourUnits()
        {
            return programLabourUnits;
        }

        public void setProgramLabourUnits(Double programLabourUnits)
        {
            this.programLabourUnits = programLabourUnits;
        }

        public Double getInteractiveLessonsPercent()
        {
            return interactiveLessonsPercent;
        }

        public void setInteractiveLessonsPercent(Double interactiveLessonsPercent)
        {
            this.interactiveLessonsPercent = interactiveLessonsPercent;
        }

        public Double getLecturesPercent()
        {
            return lecturesPercent;
        }

        public void setLecturesPercent(Double lecturesPercent)
        {
            this.lecturesPercent = lecturesPercent;
        }

        public Double getChoiceDisciplinePercent()
        {
            return choiceDisciplinePercent;
        }

        public void setChoiceDisciplinePercent(Double choiceDisciplinePercent)
        {
            this.choiceDisciplinePercent = choiceDisciplinePercent;
        }

        public Double getMaxSize()
        {
            return maxSize;
        }

        public void setMaxSize(Double maxSize)
        {
            this.maxSize = maxSize;
        }

        public String getPlanKind()
        {
            return planKind;
        }

        public void setPlanKind(String planKind)
        {
            this.planKind = planKind;
        }

        public String getLevelCode()
        {
            return levelCode;
        }

        public void setLevelCode(String levelCode)
        {
            this.levelCode = levelCode;
        }

        public Integer getTermsInCourse()
        {
            return termsInCourse;
        }

        public void setTermsInCourse(Integer termsInCourse)
        {
            this.termsInCourse = termsInCourse;
        }

        public Integer getElementsInWeek()
        {
            return elementsInWeek;
        }

        public void setElementsInWeek(Integer elementsInWeek)
        {
            this.elementsInWeek = elementsInWeek;
        }

        public Date getStateExamDate()
        {
            return stateExamDate;
        }

        public void setStateExamDate(Date stateExamDate)
        {
            this.stateExamDate = stateExamDate;
        }

        public String getStateExamDoc()
        {
            return stateExamDoc;
        }

        public void setStateExamDoc(String stateExamDoc)
        {
            this.stateExamDoc = stateExamDoc;
        }

        public String getStateExamType()
        {
            return stateExamType;
        }

        public void setStateExamType(String stateExamType)
        {
            this.stateExamType = stateExamType;
        }

        public String getApplication()
        {
            return application;
        }

        public void setApplication(String application)
        {
            this.application = application;
        }

        public Date getApplicationDate()
        {
            return applicationDate;
        }

        public void setApplicationDate(Date applicationDate)
        {
            this.applicationDate = applicationDate;
        }

        public String getApplicationVersion()
        {
            return applicationVersion;
        }

        public void setApplicationVersion(String applicationVersion)
        {
            this.applicationVersion = applicationVersion;
        }

        public Double getZetInYear()
        {
            return zetInYear;
        }

        public void setZetInYear(Double zetInYear)
        {
            this.zetInYear = zetInYear;
        }

        public Double getZetInWeek()
        {
            return zetInWeek;
        }

        public void setZetInWeek(Double zetInWeek)
        {
            this.zetInWeek = zetInWeek;
        }

        public Integer getHoursAmountInZET()
        {
            return hoursAmountInZET;
        }

        public void setHoursAmountInZET(Integer hoursAmountInZET)
        {
            this.hoursAmountInZET = hoursAmountInZET;
        }

        public Double getTotalZET()
        {
            return totalZET;
        }

        public void setTotalZET(Double totalZET)
        {
            this.totalZET = totalZET;
        }

        public Date getCertificateDate()
        {
            return certificateDate;
        }

        public void setCertificateDate(Date certificateDate)
        {
            this.certificateDate = certificateDate;
        }
    }

    public static class MdbDeveloper
    {
        private String eduHsId;
        private int number;
        private String fio;
        private String post;

        public MdbDeveloper(String eduHsId, int number, String fio, String post)
        {
            this.eduHsId = eduHsId;
            this.number= number;
            this.fio = fio;
            this.post = post;
        }

        public String getEduHsId()
        {
            return eduHsId;
        }

        public int getNumber()
        {
            return number;
        }

        public String getFio()
        {
            return fio;
        }

        public String getPost()
        {
            return post;
        }
    }

    public static class MdbQualification
    {
        private String eduHsId;
        private int number;
        private String title;
        private String developPeriod;

        public MdbQualification(String eduHsId, int number, String title, String developPeriod)
        {
            this.eduHsId = eduHsId;
            this.number= number;
            this.title = title;
            this.developPeriod = developPeriod;
        }

        public String getEduHsId()
        {
            return eduHsId;
        }

        public int getNumber()
        {
            return number;
        }

        public String getTitle()
        {
            return title;
        }

        public String getDevelopPeriod()
        {
            return developPeriod;
        }
    }

    public static class MdbSpeciality
    {
        private String eduHsId;
        private int number;
        private String title;

        public MdbSpeciality(String eduHsId, int number, String title)
        {
            this.eduHsId = eduHsId;
            this.number= number;
            this.title = title;
        }

        public String getEduHsId()
        {
            return eduHsId;
        }

        public int getNumber()
        {
            return number;
        }

        public String getTitle()
        {
            return title;
        }
    }

    public static class MdbRow
    {
        private final Map<String, MdbRow> childMap = new LinkedHashMap<>();
        public Collection<MdbRow> getChildRows() { return this.childMap.values(); }
        public MdbRow get(final String indexPart) { return this.childMap.get(indexPart); }

        private final Map<Integer, Map<String, Object>> term2LoadMap = new TreeMap<>();

        public Map<String, Object> getTermLoadMap(final Integer term)
        {
            Map<String, Object> map = this.term2LoadMap.get(term);
            if (null == map)
            {
                this.term2LoadMap.put(term, map = new LinkedHashMap<>());
            }
            return map;
        }

        private final String eduHsId;
        public String getEduHsId() { return this.eduHsId; }

        private final String number;
        public String getNumber() { return this.number; }

        private final String indexPart;
        public String getIndexPart() { return this.indexPart; }

        private final String hierarchyIndex;
        public String getHierarchyIndex() { return this.hierarchyIndex; }

        private final List<PairKey<String, Integer>> competences = new ArrayList<>();
        public List<PairKey<String, Integer>> getCompetences(){ return competences; }


        private MdbRow(final String rootEduHsId)
        {
            this.eduHsId = rootEduHsId;
            this.number = "";
            this.indexPart = "";
            this.hierarchyIndex = "";
        }

        protected MdbRow(final MdbRow parent, final String eduHsId, final String indexPart)
        {
            if (null != parent.childMap.put(indexPart, this))
            {
                throw new IllegalStateException();
            }

            this.eduHsId = eduHsId;
            this.indexPart = indexPart;
            this.number = StringUtils.leftPad(String.valueOf(parent.childMap.size()), 2, '0');
            this.hierarchyIndex = buildIndex(parent.getHierarchyIndex(), this.number);
        }

        protected static String buildIndex(String parentIndex, final String selfIndexPart)
        {
            parentIndex = StringUtils.trimToNull(parentIndex);
            return (null == parentIndex ? selfIndexPart : (parentIndex + "." + selfIndexPart));
        }

        public String getType(){ throw new UnsupportedOperationException(); }
        public String getDsc(){ throw new UnsupportedOperationException(); }
        public String getTitle(){ throw new UnsupportedOperationException(); }
    }

    public static class MdbRowStructure extends MdbRow
    {
        public MdbRowStructure(final MdbRow parent, final String eduHsId, final String indexPart)
        {
            super(parent, eduHsId, indexPart);
        }

        private String code;

        public String getCode(){ return this.code; }
        public void setCode(final String code){ this.code = code; }

        @Override public String getType(){ return "s"; }
        @Override public String getDsc(){ return this.getCode();}
        @Override public String getTitle(){ return this.getCode();}
    }

    public static class MdbRowDiscipline extends MdbRow
    {
        public MdbRowDiscipline(final MdbRow parent, final String eduHsId, final String indexPart)
        {
            super(parent, eduHsId, indexPart);
        }

        private String title;
        private String kind;
        private String ownerId;

        public String getKind(){ return this.kind; }
        public void setKind(final String kind){ this.kind = kind; }

        public String getOwnerId(){ return this.ownerId; }
        public void setOwnerId(final String ownerId){ this.ownerId = ownerId; }

        @Override public String getTitle(){ return this.title; }
        public void setTitle(final String title){ this.title = title; }

        @Override public String getType(){ return "r"; }
        @Override public String getDsc(){ return this.getKind(); }


    }

    public static class MdbRowVariant extends MdbRow
    {
        public MdbRowVariant(final MdbRow parent, final String eduHsId, final String indexPart)
        {
            super(parent, eduHsId, indexPart);
        }

        private String title;

        @Override
        public String getTitle(){ return this.title; }
        public void setTitle(final String title){ this.title = title; }

        @Override public String getType(){ return "v"; }
        @Override public String getDsc(){ return "1"; /* одна */ }
    }

    public static class MdbRowGroup extends MdbRow
    {
        public MdbRowGroup(final MdbRow parent, final String eduHsId, final String indexPart)
        {
            super(parent, eduHsId, indexPart);
        }

        private String title;

        @Override
        public String getTitle(){ return this.title; }
        public void setTitle(final String title){ this.title = title;}

        @Override public String getType(){ return "g"; }
        @Override public String getDsc(){ return ""; }
    }


    public static class MdbEpv
    {
        private final MdbFile mdb;
        private final MdbRow root;

        private final List<MdbCompetence> competences = new ArrayList<>();

        private final Set<MdbSchedule> schedules = new HashSet<>();

        private final List<MdbTitle> blockTitles = new ArrayList<>();
        private final List<MdbDeveloper> developers = new ArrayList<>();
        private final List<MdbQualification> qualifications = new ArrayList<>();
        private final List<MdbSpeciality> specialities = new ArrayList<>();

        private final String epvId;
        public String getEpvId(){ return this.epvId; }

        private final String eduHsRoot;
        public String getEduHsRoot(){ return this.eduHsRoot; }

        private final String rowOwnerId;
        public String getRowOwnerId(){ return this.rowOwnerId; }

        private final Set<String> eduHsSet;
        public Set<String> getEduHsSet(){ return this.eduHsSet; }

        private String title;
        public String getTitle(){ return this.title; }
        public void setTitle(final String title){ this.title = title; }


        private MdbEpv(final MdbFile mdb, final String epvId)
        {
            this.mdb = mdb;
            this.epvId = epvId;

            // НПВ
            if (null == (this.eduHsRoot = this.mdb.epvId2RootEduHsIdMap.get(epvId)))
            {
                throw new IllegalStateException("Отсутствует запись о основном НПВ для версии «" + epvId + "»");
            }
            if (null == (this.eduHsSet = this.mdb.epvId2BlockEduHsIdsMap.get(epvId)))
            {
                throw new IllegalStateException("Отсутствует запись о составе НПВ блоков для версии «" + epvId + "»");
            }
            if (!this.eduHsSet.contains(this.eduHsRoot))
            {
                throw new IllegalStateException("Отсутствует запись о основном НПВ в записях о НПВ блоков для версии «" + epvId + "»");
            }

            // ответственное подразделение
            if (null == (this.rowOwnerId = this.mdb.eduHs2DataMap.get(this.eduHsRoot)[0]))
            {
                throw new IllegalStateException("Отсутствует запись о подразделении версии «" + epvId + "»");
            }

            // строки
            this.root = new MdbRow(this.eduHsRoot);
        }

        @SuppressWarnings("unchecked")
        public <T extends MdbRow> T add(final String eduHsId, final String[] path, final Class<T> klass)
        {
            if (!this.eduHsSet.contains(eduHsId))
            {
                throw new IllegalStateException("В версии «" + this.epvId + "» нет блока для НПВ «" + eduHsId + "»");
            }

            final Iterator<String> pathIterator = Arrays.asList(path).iterator();
            MdbRow row = this.root;
            while (pathIterator.hasNext())
            {
                // проверяем, что блоки корректно вложены друг в друга
                // (как только блок становится не основным, то дочерние строки должны иметь в точности тот же блок)
                if (!this.eduHsRoot.equals(row.getEduHsId()))
                {
                    if (!row.getEduHsId().equals(eduHsId))
                    {
                        throw new IllegalStateException("Нарушена последовательность блоков в цепочке:" + Arrays.asList(path).toString() + "; блок строки=" + row.getEduHsId() + ", блок файла=" + eduHsId + ", основной блок=" + this.eduHsRoot);
                    }
                }

                // переходим к следующему элементу
                final String pathItem = StringUtils.trimToEmpty(pathIterator.next());
                final MdbRow tmp = row.get(pathItem);

                if (null != tmp)
                {
                    // если элемент нашли - то сохраняем его
                    row = tmp;
                } else
                {
                    // если это не последний элемент - то кричим, что он еще не добавлен
                    if (pathIterator.hasNext())
                    {
                        throw new IllegalStateException("Нарушен порядок заполнения строк: обнаружен не добавленный ранее элемент «" + pathItem + "» в цепочке " + Arrays.asList(path).toString());
                    }

                    // если последний - создаем
                    try
                    {
                        final Constructor<T> constructor = klass.getConstructor(MdbRow.class, String.class, String.class);

                        if (forceRootStructureElement && MdbRowStructure.class.isAssignableFrom(klass))
                        {
                            // все структурные элементы суем автоматически в основной блок
                            row = constructor.newInstance(row, this.eduHsRoot, pathItem);
                        } else
                        {
                            row = constructor.newInstance(row, eduHsId, pathItem);
                        }

                    } catch (final Throwable t)
                    {
                        throw new RuntimeException(t.getMessage(), t);
                    }
                }

            }
            return (T) row;
        }

        public void addCompetence(String eduHsId, String index, int number, String content)
        {
            this.competences.add(new MdbCompetence(eduHsId, index, number, content));
        }

        public boolean checkScheduleAndFillIfEmpty(Set<MdbSchedule> schedules)
        {
            return this.schedules.isEmpty() ? this.schedules.addAll(schedules) : this.schedules.equals(schedules);
        }

        public void addTitle(String eduHsId, ImtsaFile.ImtsaTitle title)
        {
            MdbTitle blockTitle = new MdbTitle();
            blockTitle.setEduHsId(eduHsId);
            blockTitle.setPlanFullTitle(title.getPlanFullTitle());
            blockTitle.setPlanTitle(title.getPlanTitle());
            blockTitle.setUserNumber(title.getUserNumber());
            blockTitle.setAcademy(title.getAcademy());
            blockTitle.setOrgUnit(title.getOrgUnit());
            blockTitle.setHigherEchelon(title.getHigherEchelon());
            blockTitle.setProducingOrgUnit(title.getProducingOrgUnit());
            blockTitle.setFormativeOrgUnit(title.getFormativeOrgUnit());
            blockTitle.setEducationDirectionCode(title.getEducationDirectionCode());
            blockTitle.setStartYear(title.getStartYear());
            blockTitle.setIncludeExamsInHoursAmount(title.isIncludeExamsInHoursAmount());
            blockTitle.setDissertationAsAttestation(title.isDissertationAsAttestation());
            blockTitle.setStateExamAsAttestation(title.isStateExamAsAttestation());
            blockTitle.setKsrOrIndividualLessons(title.getKsrOrIndividualLessons());
            blockTitle.setZetInWeekAtt(title.getZetInWeekAtt());
            blockTitle.setHoursAmountInZetAtt(title.getHoursAmountInZetAtt());
            blockTitle.setProgramLabourUnits(title.getProgramLabourUnits());
            blockTitle.setInteractiveLessonsPercent(title.getInteractiveLessonsPercent());
            blockTitle.setLecturesPercent(title.getLecturesPercent());
            blockTitle.setChoiceDisciplinePercent(title.getChoiceDisciplinePercent());
            blockTitle.setMaxSize(title.getMaxSize());
            blockTitle.setPlanKind(title.getPlanKind());
            blockTitle.setLevelCode(title.getLevelCode());
            blockTitle.setTermsInCourse(title.getTermsInCourse());
            blockTitle.setElementsInWeek(title.getElementsInWeek());
            blockTitle.setStateExamDate(title.getStateExamDate());
            blockTitle.setStateExamDoc(title.getStateExamDoc());
            blockTitle.setStateExamType(title.getStateExamType());
            blockTitle.setApplication(title.getApplication());
            blockTitle.setApplicationDate(title.getApplicationDate());
            blockTitle.setApplicationVersion(title.getApplicationVersion());

            blockTitle.setZetInYear(title.getZetInYear());
            blockTitle.setZetInWeek(title.getZetInWeek());
            blockTitle.setHoursAmountInZET(title.getHoursAmountInZET());
            blockTitle.setTotalZET(title.getTotalZET());
            blockTitle.setCertificateDate(title.getCertificateDate());

            blockTitles.add(blockTitle);
        }

        public void addDeveloper(String eduHsId, int number, String fio, String post)
        {
            this.developers.add(new MdbDeveloper(eduHsId, number, fio, post));
        }

        public void addQualification(String eduHsId, int number, String title, String developPeriod)
        {
            this.qualifications.add(new MdbQualification(eduHsId, number, title, developPeriod));
        }

        public void addSpeciality(String eduHsId, int number, String title)
        {
            this.specialities.add(new MdbSpeciality(eduHsId, number, title));
        }

        protected void flush()
        {
            try
            {
                try
                {

                    // удаляем все строки, которые относятся к версии
                    {
                        this.mdb.t_epv_row.reset();
                        Map<String, Object> row;
                        while (null != (row = this.mdb.t_epv_row.getNextRow()))
                        {
                            if (this.epvId.equals(StringUtils.trimToEmpty((String) row.get("row_version_id"))))
                            {
                                this.mdb.t_epv_row.deleteCurrentRow();
                            }
                        }

                        // отправляем в файл
                        this.mdb.mdb.flush();
                    }

                    // удаляем все данные по семестрам, которые относятся к версии
                    {
                        this.mdb.t_epv_row_term.reset();
                        Map<String, Object> row;
                        while (null != (row = this.mdb.t_epv_row_term.getNextRow()))
                        {
                            if (this.epvId.equals(StringUtils.trimToEmpty((String) row.get("row_version_id"))))
                            {
                                this.mdb.t_epv_row_term.deleteCurrentRow();
                            }
                        }

                        // отправляем в файл
                        this.mdb.mdb.flush();
                    }

                    deleteCompetences();
                    saveCompetences();

                    deleteSchedules();
                    saveSchedules();

                    deleteBlockTitles();
                    saveBlockTitles();

                    deleteDevelopers();
                    saveDevelopers();

                    deleteQualifications();
                    saveQualifications();

                    deleteSpeciality();
                    saveSpeciality();

                    // сохраняем строки
                    this.save(this.root.getChildRows());

                } finally
                {
                    // отправляем в файл
                    this.mdb.mdb.flush();
                }
            } catch (final Throwable t)
            {
                throw new RuntimeException(t.getMessage(), t);
            }

        }

        private void deleteCompetences() throws IOException
        {
            Table table = this.mdb.t_competence;
            table.reset();

            Map<String, Object> row;
            while ((row = table.getNextRow()) != null)
            {
                if (this.epvId.equals(row.get("version_id")))
                {
                    table.deleteCurrentRow();
                }
            }

            this.mdb.mdb.flush();
        }

        private void deleteSchedules() throws IOException
        {
            Table table = this.mdb.t_epv_schedule;
            table.reset();

            Map<String, Object> row;
            while ((row = table.getNextRow()) != null)
            {
                if (this.epvId.equals(row.get("version_id")))
                {
                    table.deleteCurrentRow();
                }
            }

            this.mdb.mdb.flush();
        }

        private void deleteBlockTitles() throws IOException
        {
            Table table = this.mdb.t_epp_epv_block_title;
            table.reset();
            Map<String, Object> row;
            while ((row = table.getNextRow()) != null)
            {
                if (this.epvId.equals(row.get("version_id")))
                {
                    table.deleteCurrentRow();
                }
            }
        }

        private void deleteDevelopers() throws IOException
        {
            Table table = this.mdb.t_epp_developer;
            table.reset();
            Map<String, Object> row;
            while ((row = table.getNextRow()) != null)
            {
                if (this.epvId.equals(row.get("version_id")))
                {
                    table.deleteCurrentRow();
                }
            }
        }

        private void deleteQualifications() throws IOException
        {
            Table table = this.mdb.t_epp_qualification;
            table.reset();
            Map<String, Object> row;
            while ((row = table.getNextRow()) != null)
            {
                if (this.epvId.equals(row.get("version_id")))
                {
                    table.deleteCurrentRow();
                }
            }
        }

        private void deleteSpeciality() throws IOException
        {
            Table table = this.mdb.t_epp_speciality;
            table.reset();
            Map<String, Object> row;
            while ((row = table.getNextRow()) != null)
            {
                if (this.epvId.equals(row.get("version_id")))
                {
                    table.deleteCurrentRow();
                }
            }
        }

        private void save(final Collection<MdbRow> rows) throws IOException
        {
            for (final MdbRow row : rows)
            {
                final String rowPath = row.getHierarchyIndex();

                // сама строка
                {
                    final Map<String, Object> rowMap = new HashMap<>();
                    rowMap.put("row_version_id", this.epvId);
                    rowMap.put("row_path", rowPath);
                    rowMap.put("row_eduhs_id", row.getEduHsId());
                    rowMap.put("row_owner_id", this.rowOwnerId);
                    rowMap.put("row_dsc_type_p", row.getType());
                    rowMap.put("row_dsc_rel", row.getDsc());
                    rowMap.put("row_index_p", row.getNumber());
                    rowMap.put("row_title_p", row.getTitle());
                    rowMap.put("row_terms_p", "");

                    if (row instanceof MdbRowDiscipline)
                    {
                        final String ownerId = StringUtils.trimToNull(((MdbRowDiscipline) row).getOwnerId());
                        if (null != ownerId) { rowMap.put("row_owner_id", ownerId); }
                    }


                    this.mdb.t_epv_row.addRow(this.mdb.t_epv_row.asRow(rowMap));
                }

                // нагрузка по семестрам
                for (final Map.Entry<Integer, Map<String, Object>> e : row.term2LoadMap.entrySet())
                {
                    final Map<String, Object> rowMap = new HashMap<>();
                    rowMap.put("row_version_id", this.epvId);
                    rowMap.put("row_path", rowPath);
                    rowMap.put("term", e.getKey());

                    // предзаполняем нуликами, иначе упадет код в uni
                    {
                        rowMap.put("weeks_p", "0");
                        rowMap.put("size_p", "0");
                        rowMap.put("labor_p", "0");
                        rowMap.put("e_audit_p", "0");
                        rowMap.put("a_lect_p", "0");
                        rowMap.put("a_pract_p", "0");
                        rowMap.put("a_lab_p", "0");
                        rowMap.put("e_self_p", "0");
                    }

                    for (final Map.Entry<String, Object> te : e.getValue().entrySet())
                    {
                        rowMap.put(te.getKey(), String.valueOf(te.getValue()));
                    }
                    this.mdb.t_epv_row_term.addRow(this.mdb.t_epv_row_term.asRow(rowMap));
                }

                // компетенции
                for (final PairKey<String, Integer> key : row.getCompetences())
                {
                    final Map<String, Object> rowMap = new HashMap<>();
                    rowMap.put("version_id", this.epvId);
                    rowMap.put("row_path", rowPath);
                    rowMap.put("comp_skill_group_p", key.getFirst());
                    rowMap.put("comp_number_p", key.getSecond());

                    this.mdb.t_epv_row_competence.addRow(this.mdb.t_epv_row_competence.asRow(rowMap));
                }

                // созраняем все дочерние строки
                this.save(row.getChildRows());
            }
        }

        private void saveCompetences() throws IOException
        {
            for (MdbCompetence competence : this.competences)
            {
                String content = competence.getContent();
                for (int i = 0, part = 0, len = content.length(), beginIndex, endIndex; i < len; i += 255, part++)
                {
                    beginIndex = i;
                    endIndex = Math.min(i + 255, len);

                    final Map<String, Object> rowMap = new HashMap<>();
                    rowMap.put("version_id", this.epvId);
                    rowMap.put("eduhs_id", competence.getEduHsId());
                    rowMap.put("skill_group_p", competence.getSkillGroup());
                    rowMap.put("number_p", competence.getNumber());
                    rowMap.put("content_p", content.substring(beginIndex, endIndex));
                    rowMap.put("part_p", part);

                    this.mdb.t_competence.addRow(this.mdb.t_competence.asRow(rowMap));
                }
            }

            this.mdb.mdb.flush();
        }

        private void saveSchedules() throws IOException
        {
            for (MdbSchedule schedule: this.schedules)
            {
                Map<String, Object> rowMap = new HashMap<>();
                rowMap.put("version_id", this.epvId);
                rowMap.put("course_p", schedule.getCourse());
                rowMap.put("term_p", schedule.getTerm());
                rowMap.put("part_p", schedule.getPart());
                rowMap.put("schedule_p", schedule.getSchedule());

                this.mdb.t_epv_schedule.addRow(this.mdb.t_epv_schedule.asRow(rowMap));
            }

            this.mdb.mdb.flush();
        }

        private void saveBlockTitles() throws IOException
        {
            for (MdbTitle title: blockTitles)
            {
                Map<String, Object> rowMap = new HashMap<>();
                rowMap.put("version_id", epvId);
                rowMap.put("eduhs_id", title.getEduHsId());
                rowMap.put("plan_full_title_p", title.getPlanFullTitle());
                rowMap.put("plan_title_p", title.getPlanTitle());
                rowMap.put("user_number_p", title.getUserNumber());
                rowMap.put("academy_p", title.getAcademy());
                rowMap.put("org_unit_p", title.getOrgUnit());
                rowMap.put("higher_echelon_p", title.getHigherEchelon());
                rowMap.put("producing_org_unit_p", title.getProducingOrgUnit());
                rowMap.put("formative_org_unit_p", title.getFormativeOrgUnit());
                rowMap.put("edu_direction_code_p", title.getEducationDirectionCode());
                rowMap.put("start_year_p", title.getStartYear());
                rowMap.put("ex_in_hours_amount_p", title.isIncludeExamsInHoursAmount());
                rowMap.put("dis_as_attestation_p", title.isDissertationAsAttestation());
                rowMap.put("st_ex_as_attestation_p", title.isStateExamAsAttestation());
                rowMap.put("ksr_ind_les_p", title.getKsrOrIndividualLessons());
                rowMap.put("zet_in_week_att_p", title.getZetInWeekAtt());
                rowMap.put("hours_amount_in_zet_att_p", title.getHoursAmountInZetAtt());
                rowMap.put("program_labour_units_p", title.getProgramLabourUnits());
                rowMap.put("inter_les_per_p", title.getInteractiveLessonsPercent());
                rowMap.put("lec_per_p", title.getLecturesPercent());
                rowMap.put("ch_disc_per_p", title.getChoiceDisciplinePercent());
                rowMap.put("max_size_p", title.getMaxSize());
                rowMap.put("plan_kind_p", title.getPlanKind());
                rowMap.put("level_code_p", title.getLevelCode());
                rowMap.put("terms_in_course_p", title.getTermsInCourse());
                rowMap.put("el_in_week_p", title.getElementsInWeek());
                rowMap.put("st_ex_date_p", title.getStateExamDate());
                rowMap.put("st_ex_doc_p", title.getStateExamDoc());
                rowMap.put("st_ex_type_p", title.getStateExamType());
                rowMap.put("application_p", title.getApplication());
                rowMap.put("application_date_p", title.getApplicationDate());
                rowMap.put("application_ver_p", title.getApplicationVersion());

                rowMap.put("", title.getZetInYear());
                rowMap.put("", title.getZetInWeek());
                rowMap.put("", title.getHoursAmountInZET());
                rowMap.put("", title.getTotalZET());
                rowMap.put("", title.getCertificateDate());

                this.mdb.t_epp_epv_block_title.addRow(this.mdb.t_epp_epv_block_title.asRow(rowMap));
            }
        }

        private void saveDevelopers() throws IOException
        {
            for (MdbDeveloper developer:  developers)
            {
                Map<String, Object> rowMap = new HashMap<>();
                rowMap.put("version_id", epvId);
                rowMap.put("eduhs_id", developer.getEduHsId());
                rowMap.put("number_p", developer.getNumber());
                rowMap.put("fio_p", developer.getFio());
                rowMap.put("post_p", developer.getPost());

                this.mdb.t_epp_developer.addRow(this.mdb.t_epp_developer.asRow(rowMap));
            }
        }

        private void saveQualifications() throws IOException
        {
            for (MdbQualification qualification:  qualifications)
            {
                Map<String, Object> rowMap = new HashMap<>();
                rowMap.put("version_id", epvId);
                rowMap.put("eduhs_id", qualification.getEduHsId());
                rowMap.put("number_p", qualification.getNumber());
                rowMap.put("title_p", qualification.getTitle());
                rowMap.put("develop_period_p", qualification.getDevelopPeriod());

                this.mdb.t_epp_qualification.addRow(this.mdb.t_epp_qualification.asRow(rowMap));
            }
        }

        private void saveSpeciality() throws IOException
        {
            for (MdbSpeciality speciality:  specialities)
            {
                Map<String, Object> rowMap = new HashMap<>();
                rowMap.put("version_id", epvId);
                rowMap.put("eduhs_id", speciality.getEduHsId());
                rowMap.put("number_p", speciality.getNumber());
                rowMap.put("title_p", speciality.getTitle());

                this.mdb.t_epp_speciality.addRow(this.mdb.t_epp_speciality.asRow(rowMap));
            }
        }
    }

    private final Map<String, MdbEpv> epvMap = new HashMap<>();
    public Map<String, MdbEpv> getEpvMap() { return this.epvMap; }

    public MdbEpv get(final String epvId) {
        MdbEpv epv = this.epvMap.get(epvId);
        if (null == epv)
        {
            this.epvMap.put(epvId, epv = new MdbEpv(this, epvId));
        }
        return epv;
    }

    public String checkStructureCode(final String code)
    {
        if (!this.epvStructureCodes.contains(code))
        {
            throw new IllegalStateException("Неизвестный код справочника «" + code + "».");
        }
        return code;
    }

    public String getOwnerId(final String orgUnitCode)
    {
        if (null == orgUnitCode)
        {
            return null;
        }
        return this.orgUnitCode2IdMap.get(orgUnitCode);
    }
}