/**
 *$Id:$
 */
package ru.tandemservice.uniusma.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;

/**
 * @author Alexander Shaburov
 * @since 09.10.12
 */
public class EmployeePostAddEditExtUI extends UIAddon
{
    public EmployeePostAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onChangeHourlyPaid()
    {
        EmployeePostAddEditUI presenter = (EmployeePostAddEditUI) getPresenter();
        if (presenter.isAddForm() && presenter.getEmployeePost().isHourlyPaid())
        {
            presenter.getEmployeePost().setWeekWorkLoad(null);
            presenter.getEmployeePost().setWorkWeekDuration(null);
        }
    }
}
