/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.IStudentCardAttachmentReportDao;
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.StudentCardAttachmentReportDao;

/**
 * @author DMITRY KNYAZEV
 * @since 23.12.2015
 */
@Configuration
public class UsmaStudentCardAttachmentReportManager extends BusinessObjectManager
{
    public static UsmaStudentCardAttachmentReportManager instance()
    {
        return instance(UsmaStudentCardAttachmentReportManager.class);
    }

    @Bean
    public IStudentCardAttachmentReportDao dao()
    {
        return new StudentCardAttachmentReportDao();
    }
}
