/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockDeveloperAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        if (model.getDeveloperId() != null){ model.setNumber(model.getDeveloper().getNumber()); }
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDeveloper().setNumber(model.getNumber());
        getDao().saveOrUpdate(model.getDeveloper());
        deactivate(component);
    }
}