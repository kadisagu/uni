/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.catalog.usmaCompetence.UsmaCompetenceItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Model extends DefaultCatalogItemPubModel<UsmaCompetence>
{
}