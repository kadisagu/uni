/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.catalog.usmaCompetence.UsmaCompetenceAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Model extends DefaultCatalogAddEditModel<UsmaCompetence>
{
    private ISelectModel _eppSkillGroupModel;

    public ISelectModel getEppSkillGroupModel()
    {
        return _eppSkillGroupModel;
    }

    public void setEppSkillGroupModel(ISelectModel eppSkillGroupModel)
    {
        _eppSkillGroupModel = eppSkillGroupModel;
    }
}