/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.Add.UsmaWorkGraphAdd;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
public class UsmaWorkGraphListUI extends UIPresenter
{
    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        UsmaWorkGraphManager.instance().dao().setWorkGraphListFilterDefaultValues(getSettings());
        saveSettings();
    }

    public void onClickAddWorkGraph()
    {
        _uiActivation.asRegionDialog(UsmaWorkGraphAdd.class).activate();
    }

    public void onDeleteEntityFromList()
    {
        UsmaWorkGraphManager.instance().dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(UsmaWorkGraphList.WORK_GRAPH_DS))
        {
            dataSource.putAll(getSettings().getAsMap(
                    false,
                    UsmaWorkGraphList.PUPNAG,
                    UsmaWorkGraphList.STATE,
                    UsmaWorkGraphList.DEVELOP_FORM,
                    UsmaWorkGraphList.DEVELOP_CONDITION,
                    UsmaWorkGraphList.DEVELOP_TECH,
                    UsmaWorkGraphList.DEVELOP_GRID));
        }
    }
}