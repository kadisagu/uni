/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public interface IUsmaSystemActionDao extends INeedPersistenceSupport
{
    void saveStudents() throws Exception;

    void doCreateEmpLabourContract() throws Exception;

    /**
     * Возвращает список привязанных к учебным планам xml.
     *
     * @param blockIds id блоков
     * @return список xml
     */
    public List<UsmaImtsaXml> getXmlList(Collection<Long> blockIds);

    /**
     * Возвращает части ГОС треебуемого поколения.
     *
     * @param secondGosGeneration ГОС2/ФГОС
     * @return список частей ГОС
     */
    public List<EppPlanStructure> getParts(boolean secondGosGeneration);

    /**
     * Возвращает список соспоставлений циклов ИМЦА циклам Uni.
     *
     * @param secondGosGeneration ГОС2/ФГОС
     * @return список сопоставлений циклов ИМЦА и циклов Uni
     */
    public List<UsmaImtsaCyclePlanStructureRel> getImtsaCycles(boolean secondGosGeneration);

    /**
     * Сохраняет лог ошибки импорта ИМЦА. Стирает предшествующий (если был).
     *
     * @param block блок
     * @param error текст ошибки
     */
    public void saveOrRewriteImportLog(EppEduPlanVersionBlock block, String error);

    /**
     * Обновляет логиины студентов, удаляя из логинов все апострофы, пробелы по краям и если логин начинается с точки, то убирает и эту точку.
     */
    void changeStudentLogins();
}
