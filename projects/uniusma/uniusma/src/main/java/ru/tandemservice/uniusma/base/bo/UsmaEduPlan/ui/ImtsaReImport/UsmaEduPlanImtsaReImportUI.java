/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ImtsaReImport;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.UsmaEduPlanManager;
import ru.tandemservice.uniusma.base.ext.SystemAction.ui.Pub.UsmaSystemActionPubAddon;

import java.util.Collection;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public class UsmaEduPlanImtsaReImportUI extends UIPresenter
{
    public void onClickReImportImtsa()
    {
        Collection<IEntity> selected = ((BaseSearchListDataSource) getConfig().getDataSource(UsmaEduPlanImtsaReImport.DS_EDU_PLAN_VERSION)).getOptionColumnSelectedObjects("selected");
        if (!"ok".equals(_uiSupport.getClientParameter()))
        {
            if (selected.isEmpty())
                throw new ApplicationException("Нужно отметить не меньше одной версии УП.");

            String confirmStr = getConfig().getProperty("ui.reImportImtsaMessage");
            ConfirmInfo confirm = new ConfirmInfo(confirmStr,
                    new ClickButtonAction("reImportImtsa", "ok", false));
            TapSupportUtils.displayConfirm(confirm);

            return;

        }

        Collection<Long> versionIds = CommonDAO.ids(selected);
        Collection<Long> blockIds = CommonDAO.ids(UsmaEduPlanManager.instance().dao().getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), versionIds));

        UsmaSystemActionPubAddon.doReImportImtsa(blockIds, true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(UsmaEduPlanImtsaReImport.DS_EDU_PLAN_VERSION))
        {
            dataSource.putAll(getSettings().getAsMap(
                    UsmaEduPlanImtsaReImport.BIND_NUMBER,
                    UsmaEduPlanImtsaReImport.BIND_PROGRAM_SUBJECT,
                    UsmaEduPlanImtsaReImport.BIND_PROGRAM_FORM,
                    UsmaEduPlanImtsaReImport.BIND_DEVELOP_CONDITION,
                    UsmaEduPlanImtsaReImport.BIND_PROGRAM_TRAIT,
                    UsmaEduPlanImtsaReImport.BIND_DEVELOP_GRID
            ));
        }
    }
}