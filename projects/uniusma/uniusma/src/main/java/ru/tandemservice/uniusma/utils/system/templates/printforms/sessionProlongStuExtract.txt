{orderType}\par\par
\keep\keepn\fi709\qj 
{Student} {fio_N} обучается на {course} курсе {orgUnit_G} (группа {group}) {developForm_GF} формы обучения {compensationTypeStr_G_Alt} {applyed} с заявлением о продлении {sessionType_I} экзаменационной сессии, {textReason}.\par\fi0\par
приказываю:\par
\fi709\qj
1. {Student_D} {fio} продлить {sessionType_A} экзаменационную сессию с {dateStart} г. по {dateFinish} г.\par\par
Основание: {listBasics}\par\fi0