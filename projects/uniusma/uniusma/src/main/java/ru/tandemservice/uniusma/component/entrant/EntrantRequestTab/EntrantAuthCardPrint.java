// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.entrant.EntrantRequestTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.List;

/**
 * @author oleyba
 * @since 22.04.2010
 */
public class EntrantAuthCardPrint extends UniBaseDao implements IDocumentListAndReceiptPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long object)
    {
        RtfDocument document = new RtfReader().read(template);

        Entrant entrant = get(EntrantRequest.class, object).getEntrant();
        List<EntrantRequest> requests = getList(EntrantRequest.class, EntrantRequest.entrant().s(), entrant, EntrantRequest.regNumber().s());
        EnrollmentCampaignPeriod period = entrant.getEnrollmentCampaign().getPeriodList().get(0);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("entrantNumber", entrant.getPersonalNumber());
        modifier.put("statements", StringUtils.join(CommonBaseUtil.getPropertiesList(requests, EntrantRequest.P_STRING_NUMBER), ", "));
        modifier.put("lastName", entrant.getPerson().getIdentityCard().getLastName());
        modifier.put("firstName", entrant.getPerson().getIdentityCard().getFirstName());
        modifier.put("middleName", StringUtils.trimToEmpty(entrant.getPerson().getIdentityCard().getMiddleName()));
        modifier.put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(period.getDateFrom()));
        modifier.put("dateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(period.getDateTo()));
        modifier.modify(document);
        return document;
    }
}
