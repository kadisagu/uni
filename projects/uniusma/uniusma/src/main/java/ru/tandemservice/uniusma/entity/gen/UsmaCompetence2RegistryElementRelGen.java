package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь компетенции с элементом реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaCompetence2RegistryElementRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel";
    public static final String ENTITY_NAME = "usmaCompetence2RegistryElementRel";
    public static final int VERSION_HASH = 922071070;
    private static IEntityMeta ENTITY_META;

    public static final String L_USMA_COMPETENCE = "usmaCompetence";
    public static final String L_REGISTRY_ELEMENT = "registryElement";

    private UsmaCompetence _usmaCompetence;     // Компетенция(УГМА)
    private EppRegistryElement _registryElement;     // Элемент реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     */
    @NotNull
    public UsmaCompetence getUsmaCompetence()
    {
        return _usmaCompetence;
    }

    /**
     * @param usmaCompetence Компетенция(УГМА). Свойство не может быть null.
     */
    public void setUsmaCompetence(UsmaCompetence usmaCompetence)
    {
        dirty(_usmaCompetence, usmaCompetence);
        _usmaCompetence = usmaCompetence;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaCompetence2RegistryElementRelGen)
        {
            setUsmaCompetence(((UsmaCompetence2RegistryElementRel)another).getUsmaCompetence());
            setRegistryElement(((UsmaCompetence2RegistryElementRel)another).getRegistryElement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaCompetence2RegistryElementRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaCompetence2RegistryElementRel.class;
        }

        public T newInstance()
        {
            return (T) new UsmaCompetence2RegistryElementRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "usmaCompetence":
                    return obj.getUsmaCompetence();
                case "registryElement":
                    return obj.getRegistryElement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "usmaCompetence":
                    obj.setUsmaCompetence((UsmaCompetence) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "usmaCompetence":
                        return true;
                case "registryElement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "usmaCompetence":
                    return true;
                case "registryElement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "usmaCompetence":
                    return UsmaCompetence.class;
                case "registryElement":
                    return EppRegistryElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaCompetence2RegistryElementRel> _dslPath = new Path<UsmaCompetence2RegistryElementRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaCompetence2RegistryElementRel");
    }
            

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel#getUsmaCompetence()
     */
    public static UsmaCompetence.Path<UsmaCompetence> usmaCompetence()
    {
        return _dslPath.usmaCompetence();
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    public static class Path<E extends UsmaCompetence2RegistryElementRel> extends EntityPath<E>
    {
        private UsmaCompetence.Path<UsmaCompetence> _usmaCompetence;
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel#getUsmaCompetence()
     */
        public UsmaCompetence.Path<UsmaCompetence> usmaCompetence()
        {
            if(_usmaCompetence == null )
                _usmaCompetence = new UsmaCompetence.Path<UsmaCompetence>(L_USMA_COMPETENCE, this);
            return _usmaCompetence;
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

        public Class getEntityClass()
        {
            return UsmaCompetence2RegistryElementRel.class;
        }

        public String getEntityName()
        {
            return "usmaCompetence2RegistryElementRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
