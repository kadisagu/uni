package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x5x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность restorationStuExtractExtUsmaExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("rstrtnstextrctextusmext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("extractext_id", DBType.LONG).setNullable(false), 
				new DBColumn("startgrantspaying_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("startgrantspayingdate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("restorationStuExtractExtUsmaExt");

		}


    }
}