/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e19;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.DischargingStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.DischargingStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 26.05.2014
 */
public class DischargingStuExtractPrint implements IPrintFormCreator<DischargingStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, DischargingStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        DischargingStuExtractUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new DischargingStuExtractUsmaExtGen.NaturalId(extract));

        modifier.put("paragraph", (null != extractExt ? (null != extractExt.getContractParagraph() ? extractExt.getContractParagraph() : "б/н") : ""));
        modifier.put("contractType", (null != extractExt ? extractExt.getContractType() : ""));
        modifier.put("contractDate", (null != extractExt ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getContractDate()) : ""));
        modifier.put("contractNumber", (null != extractExt ? extractExt.getContractNumber() : ""));
        modifier.put("dischargingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDischargingDate()));
        modifier.put("debtEliminateDate", (null != extractExt ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getDebtEliminateDate()) : ""));

        modifier.modify(document);
        return document;
    }
}
