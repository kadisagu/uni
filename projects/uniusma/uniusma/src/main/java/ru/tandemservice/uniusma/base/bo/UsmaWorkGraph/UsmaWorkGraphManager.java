/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.UsmaWorkGraphDAO;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.UsmaWorkGraphRowDAO;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.IUsmaWorkGraphDAO;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic.IUsmaWorkGraphRowDAO;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow2EduPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
@Configuration
public class UsmaWorkGraphManager extends BusinessObjectManager
{
    public static UsmaWorkGraphManager instance()
    {
        return instance(UsmaWorkGraphManager.class);
    }

    @Bean
    public IUsmaWorkGraphDAO dao()
    {
        return new UsmaWorkGraphDAO();
    }

    @Bean
    public IUsmaWorkGraphRowDAO rowDao()
    {
        return new UsmaWorkGraphRowDAO();
    }

    public static final String BIND_WORK_GRAPH = "workGraph";

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(UsmaWorkGraphRow2EduPlan.class, "r2p")
                        .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                        .where(eq(property("r2p", UsmaWorkGraphRow2EduPlan.eduPlanVersion()), property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion())))
                        .where(eq(property("r2p", UsmaWorkGraphRow2EduPlan.row().graph()), commonValue(context.get(BIND_WORK_GRAPH))))
                        .where(eq(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject()), property(alias)))
                        .buildQuery()
                ));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), Course.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(UsmaWorkGraphRow.class, "r")
                        .where(eq(property("r", UsmaWorkGraphRow.course()), property(alias)))
                        .where(eq(property("r", UsmaWorkGraphRow.graph()), commonValue(context.get(BIND_WORK_GRAPH))))
                        .buildQuery()
                ));
            }
        }
            .filter(Course.title())
            .order(Course.title());
    }
}