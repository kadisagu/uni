// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.documents.d1.Add;

import java.util.Date;

/**
 * @author oleyba
 * @since 17.02.2010
 */
public class Model extends ru.tandemservice.uni.component.documents.d1.Add.Model
{
    private Date enrollmentDate;
    private Date enrollmentOrderDate;
    private String enrollmentOrderNumber;
    private Date eduEndDate;

    public Date getEnrollmentDate()
    {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate)
    {
        this.enrollmentDate = enrollmentDate;
    }

    public Date getEnrollmentOrderDate()
    {
        return enrollmentOrderDate;
    }

    public void setEnrollmentOrderDate(Date enrollmentOrderDate)
    {
        this.enrollmentOrderDate = enrollmentOrderDate;
    }

    public String getEnrollmentOrderNumber()
    {
        return enrollmentOrderNumber;
    }

    public void setEnrollmentOrderNumber(String enrollmentOrderNumber)
    {
        this.enrollmentOrderNumber = enrollmentOrderNumber;
    }

    public Date getEduEndDate()
    {
        return eduEndDate;
    }

    public void setEduEndDate(Date eduEndDate)
    {
        this.eduEndDate = eduEndDate;
    }
}
