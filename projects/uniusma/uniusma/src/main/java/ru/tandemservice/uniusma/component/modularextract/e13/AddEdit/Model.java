/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e13.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 22.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e13.AddEdit.Model
{
    WeekendChildStuExtractUsmaExt _extUsmaExt;

    public WeekendChildStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(WeekendChildStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
