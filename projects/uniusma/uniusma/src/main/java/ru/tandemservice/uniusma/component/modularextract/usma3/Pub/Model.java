/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public class Model extends ModularStudentExtractPubModel<UsmaExcludeEduPlanStuExtract>
{
    private StudentStatus _studentStatusNew;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }
}
