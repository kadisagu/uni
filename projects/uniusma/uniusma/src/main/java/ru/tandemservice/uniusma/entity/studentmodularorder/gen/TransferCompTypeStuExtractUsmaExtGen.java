package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О переводе на другую основу оплаты обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferCompTypeStuExtractUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<TransferCompTypeStuExtractUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt";
    public static final String ENTITY_NAME = "transferCompTypeStuExtractUsmaExt";
    public static final int VERSION_HASH = 79228086;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_START_GRANTS_PAYING = "startGrantsPaying";
    public static final String P_START_GRANTS_PAYING_DATE = "startGrantsPayingDate";

    private TransferCompTypeStuExtract _extractExt;     // Выписка из сборного приказа по студенту. О переводе на другую основу оплаты обучения
    private boolean _startGrantsPaying = false;     // Назначить выплату академической стипендии
    private Date _startGrantsPayingDate;     // Дата начала выплаты стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другую основу оплаты обучения. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public TransferCompTypeStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О переводе на другую основу оплаты обучения. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(TransferCompTypeStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Назначить выплату академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStartGrantsPaying()
    {
        return _startGrantsPaying;
    }

    /**
     * @param startGrantsPaying Назначить выплату академической стипендии. Свойство не может быть null.
     */
    public void setStartGrantsPaying(boolean startGrantsPaying)
    {
        dirty(_startGrantsPaying, startGrantsPaying);
        _startGrantsPaying = startGrantsPaying;
    }

    /**
     * @return Дата начала выплаты стипендии.
     */
    public Date getStartGrantsPayingDate()
    {
        return _startGrantsPayingDate;
    }

    /**
     * @param startGrantsPayingDate Дата начала выплаты стипендии.
     */
    public void setStartGrantsPayingDate(Date startGrantsPayingDate)
    {
        dirty(_startGrantsPayingDate, startGrantsPayingDate);
        _startGrantsPayingDate = startGrantsPayingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TransferCompTypeStuExtractUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((TransferCompTypeStuExtractUsmaExt)another).getExtractExt());
            }
            setStartGrantsPaying(((TransferCompTypeStuExtractUsmaExt)another).isStartGrantsPaying());
            setStartGrantsPayingDate(((TransferCompTypeStuExtractUsmaExt)another).getStartGrantsPayingDate());
        }
    }

    public INaturalId<TransferCompTypeStuExtractUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<TransferCompTypeStuExtractUsmaExtGen>
    {
        private static final String PROXY_NAME = "TransferCompTypeStuExtractUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(TransferCompTypeStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TransferCompTypeStuExtractUsmaExtGen.NaturalId) ) return false;

            TransferCompTypeStuExtractUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferCompTypeStuExtractUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferCompTypeStuExtractUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new TransferCompTypeStuExtractUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "startGrantsPaying":
                    return obj.isStartGrantsPaying();
                case "startGrantsPayingDate":
                    return obj.getStartGrantsPayingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((TransferCompTypeStuExtract) value);
                    return;
                case "startGrantsPaying":
                    obj.setStartGrantsPaying((Boolean) value);
                    return;
                case "startGrantsPayingDate":
                    obj.setStartGrantsPayingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "startGrantsPaying":
                        return true;
                case "startGrantsPayingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "startGrantsPaying":
                    return true;
                case "startGrantsPayingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return TransferCompTypeStuExtract.class;
                case "startGrantsPaying":
                    return Boolean.class;
                case "startGrantsPayingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferCompTypeStuExtractUsmaExt> _dslPath = new Path<TransferCompTypeStuExtractUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferCompTypeStuExtractUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другую основу оплаты обучения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt#getExtractExt()
     */
    public static TransferCompTypeStuExtract.Path<TransferCompTypeStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Назначить выплату академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt#isStartGrantsPaying()
     */
    public static PropertyPath<Boolean> startGrantsPaying()
    {
        return _dslPath.startGrantsPaying();
    }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt#getStartGrantsPayingDate()
     */
    public static PropertyPath<Date> startGrantsPayingDate()
    {
        return _dslPath.startGrantsPayingDate();
    }

    public static class Path<E extends TransferCompTypeStuExtractUsmaExt> extends EntityPath<E>
    {
        private TransferCompTypeStuExtract.Path<TransferCompTypeStuExtract> _extractExt;
        private PropertyPath<Boolean> _startGrantsPaying;
        private PropertyPath<Date> _startGrantsPayingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другую основу оплаты обучения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt#getExtractExt()
     */
        public TransferCompTypeStuExtract.Path<TransferCompTypeStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new TransferCompTypeStuExtract.Path<TransferCompTypeStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Назначить выплату академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt#isStartGrantsPaying()
     */
        public PropertyPath<Boolean> startGrantsPaying()
        {
            if(_startGrantsPaying == null )
                _startGrantsPaying = new PropertyPath<Boolean>(TransferCompTypeStuExtractUsmaExtGen.P_START_GRANTS_PAYING, this);
            return _startGrantsPaying;
        }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt#getStartGrantsPayingDate()
     */
        public PropertyPath<Date> startGrantsPayingDate()
        {
            if(_startGrantsPayingDate == null )
                _startGrantsPayingDate = new PropertyPath<Date>(TransferCompTypeStuExtractUsmaExtGen.P_START_GRANTS_PAYING_DATE, this);
            return _startGrantsPayingDate;
        }

        public Class getEntityClass()
        {
            return TransferCompTypeStuExtractUsmaExt.class;
        }

        public String getEntityName()
        {
            return "transferCompTypeStuExtractUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
