/**
 *$Id$
 */
package ru.tandemservice.uniusma.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public abstract class AbstractUsmaCompetenceRelListener extends ParamTransactionCompleteListener<Boolean>
{
    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        final Set<Long> childBlocks = new HashSet<>();

        // нужны уникальные
        BatchUtils.execute(new HashSet<>(params), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                List<EppEduPlanVersionBlock> blocks = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, "b")
                        .where(in(EppEduPlanVersionBlock.id().fromAlias("b"), elements))
                        .createStatement(session)
                        .<EppEduPlanVersionBlock>list();

                for (EppEduPlanVersionBlock block : blocks)
                {
                    Long blockId = block.getId();

                    if (block.isRootBlock())
                    {

                        IDQLSelectableQuery competenceToAddQuery = createCompetenceToAddQuery(blockId);
                        IDQLSelectableQuery competenceToRemoveQuery = createCompetenceToRemoveQuery(blockId);

                        createCompetences(block, session, competenceToAddQuery);
                        deleteCompetences(blockId, session, competenceToRemoveQuery);
                        numerateCompetences(blockId, session);

                        childBlocks.addAll(new DQLSelectBuilder()
                                .fromEntity(EppEduPlanVersionBlock.class, "b")
                                .column(property(EppEduPlanVersionBlock.id().fromAlias("b")))
                                .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().fromAlias("b")), value(block.getEduPlanVersion())))
                                .where(ne(property(EppEduPlanVersionBlock.id().fromAlias("b")), value(blockId)))
                                .createStatement(session)
                                .<Long>list());

                    } else
                    {
                        childBlocks.add(blockId);
                    }
                }
            }
        });

        BatchUtils.execute(childBlocks, 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                List<EppEduPlanVersionBlock> blocks = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, "b")
                        .where(in(EppEduPlanVersionBlock.id().fromAlias("b"), elements))
                        .createStatement(session)
                        .<EppEduPlanVersionBlock>list();

                for (EppEduPlanVersionBlock block : blocks)
                {
                    Long blockId = block.getId();

                    Long rootBlockId = new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionRootBlock.class, "b")
                            .column(property(EppEduPlanVersionBlock.id().fromAlias("b")))
                            .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().fromAlias("b")), value(block.getEduPlanVersion())))
                            .createStatement(session)
                            .uniqueResult();

                    IDQLSelectableQuery blockCompetenceToAddQuery = createCompetenceToAddQuery(rootBlockId, blockId);
                    IDQLSelectableQuery blockCompetenceToRemoveQuery = createCompetenceToRemoveQuery(rootBlockId, blockId);

                    createCompetences(block, session, blockCompetenceToAddQuery);
                    deleteCompetences(blockId, session, blockCompetenceToRemoveQuery);
                    numerateCompetences(blockId, session);
                }
            }
        });

        return super.beforeCompletion(session, params);
    }

    private IDQLSelectableQuery createRowCompetenceQuery(Long blockId)
    {
        // компетенции, фомируемые строками и дисциплинами данного блока

        // димциплины
        IDQLSelectableQuery regElementQuery = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "comp")
                .joinEntity("comp", DQLJoinType.left, UsmaCompetence2RegistryElementRel.class, "comp2regel", eq(property(UsmaCompetence.id().fromAlias("comp")), property(UsmaCompetence2RegistryElementRel.usmaCompetence().id().fromAlias("comp2regel"))))
                .joinEntity("comp2regel", DQLJoinType.inner, EppRegistryElement.class, "regel", eq(property(EppRegistryElement.id().fromAlias("regel")), property(UsmaCompetence2RegistryElementRel.registryElement().id().fromAlias("comp2regel"))))
                .joinEntity("regel", DQLJoinType.inner, EppEpvRegistryRow.class, "regrow", eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("regrow")), property(EppRegistryElement.id().fromAlias("regel"))))
                .where(eq(property(EppEpvRegistryRow.owner().id().fromAlias("regrow")), value(blockId)))
                .column(property("comp"))
                .buildQuery();

        // строки учебного плана
        IDQLSelectableQuery rowQuery = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "comp")
                .joinEntity("comp", DQLJoinType.left, UsmaCompetence2EpvRegistryRowRel.class, "comp2row", eq(property(UsmaCompetence.id().fromAlias("comp")), property(UsmaCompetence2EpvRegistryRowRel.usmaCompetence().id().fromAlias("comp2row"))))
                .where(eq(property(UsmaCompetence2EpvRegistryRowRel.registryRow().owner().id().fromAlias("comp2row")), value(blockId)))
                .column(property("comp"))
                .buildQuery();

        DQLSelectBuilder resultBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "comp")
                .where(or(
                        in(property("comp"), regElementQuery),
                        in(property("comp"), rowQuery)))

                .predicate(DQLPredicateType.distinct);

        return resultBuilder.buildQuery();
    }

    private IDQLSelectableQuery createBlockCompetenceQuery(Long blockId)
    {
        // компетенции, привязанные к блоку
        return new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "cb")
                .column(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().fromAlias("cb")))
                .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block().id().fromAlias("cb")), value(blockId)))
                .buildQuery();
    }

    private IDQLSelectableQuery createCompetenceToAddQuery(Long blockId)
    {
        return createCompetenceToAddQuery(null, blockId);
    }

    private IDQLSelectableQuery createCompetenceToAddQuery(Long rootBlockId, Long blockId)
    {
        // компетенции, которые появились у блока (или у базового)
        return new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "c")
                .where(or(
                        in(property("c"), createRowCompetenceQuery(blockId)),
                        in(property("c"), createRowCompetenceQuery(rootBlockId))))
                .where(notIn(property("c"), createBlockCompetenceQuery(blockId)))
                .buildQuery();
    }

    private IDQLSelectableQuery createCompetenceToRemoveQuery(Long blockId)
    {
        return createCompetenceToRemoveQuery(null, blockId);
    }

    private IDQLSelectableQuery createCompetenceToRemoveQuery(Long rootBlockId, Long blockId)
    {
        // компетенции, которых уже нет у блока (или у базвого)
        return new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "c")
                .where(notIn(property("c"), createRowCompetenceQuery(blockId)))
                .where(notIn(property("c"), createRowCompetenceQuery(rootBlockId)))
                .where(or(
                        in(property("c"), createBlockCompetenceQuery(blockId)),
                        in(property("c"), createBlockCompetenceQuery(rootBlockId))))
                .buildQuery();
    }

    private void deleteCompetences(Long blockId, Session session, IDQLSelectableQuery blockCompetenceToRemoveQuery)
    {
        // удаление компетенций, которых нет у блока
        new DQLDeleteBuilder(UsmaCompetence2EduPlanVersionBlockRel.class)
                .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block().id()), value(blockId)))
                .where(in(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence()), blockCompetenceToRemoveQuery))
                .createStatement(session)
                .execute();
    }

    private void createCompetences(EppEduPlanVersionBlock block, Session session, IDQLSelectableQuery blockCompetenceToAddQuery)
    {
        // самый большой номер компетенции блока (из всех групп - позже пронумеруем)
        Integer competencesNumber = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "a")
                .column(DQLFunctions.max(property(UsmaCompetence2EduPlanVersionBlockRel.number().fromAlias("a"))))
                .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.id().fromAlias("a")), value(block.getId())))
                .createStatement(session)
                .uniqueResult();

        if (competencesNumber == null)
        {
            competencesNumber = 0;
        }

        DQLSelectBuilder competenceToAddBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence.class, "c")
                .where(in(property("c"), blockCompetenceToAddQuery));

        // привязывание компетенций, которые появились у блока
        for (UsmaCompetence competence : competenceToAddBuilder.createStatement(session).<UsmaCompetence>list())
        {
            session.save(new UsmaCompetence2EduPlanVersionBlockRel(block, competence, ++competencesNumber));
        }

        session.flush();
    }

    private void numerateCompetences(Long blockId, Session session)
    {
        // нумерация
        List<Long> eppSkilGroupIds = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "comp2block")
                .column(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup().id().fromAlias("comp2block")))
                .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block().id().fromAlias("comp2block")), value(blockId)))
                .createStatement(session)
                .<Long>list();

        for (Long eppSkilGroupId : eppSkilGroupIds)
        {
            int number = 0;
            List<UsmaCompetence2EduPlanVersionBlockRel> relList = new DQLSelectBuilder()
                    .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "comp2block")
                    .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block().id().fromAlias("comp2block")), value(blockId)))
                    .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup().id().fromAlias("comp2block")), value(eppSkilGroupId)))
                    .order(property(UsmaCompetence2EduPlanVersionBlockRel.number().fromAlias("comp2block")))
                    .createStatement(session)
                    .<UsmaCompetence2EduPlanVersionBlockRel>list();

            for (UsmaCompetence2EduPlanVersionBlockRel rel : relList)
            {
                rel.setNumber(++number);
                session.update(rel);
            }
        }

        session.flush();
    }
}