/* $Id$ */
package ru.tandemservice.uniusma.component.listextract.e4;


import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ExcludeStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;


/**
 * @author Andrey Avetisov
 * @since 26.12.2014
 */
public class ExcludeStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        final RtfDocument document = new RtfReader().read(template);

        ExcludeStuListExtract firstExtract = (ExcludeStuListExtract) MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, firstExtract);
        IListParagraphPrintFormCreator<AbstractStudentExtract> printForm = CommonListOrderPrint.getListParagraphPrintForm(EntityRuntime.getMeta(firstExtract).getName() + "_extractPrint");
        injectModifier.put("entryIntoForceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((firstExtract).getExcludeDate()));
        printForm.modifyOrderTemplate(injectModifier, order, firstExtract);
        injectModifier.modify(document);
        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        CommonListOrderPrint.injectParagraphs(document, order, null, firstExtract);

        return document;
    }
}