package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.RestorationStuExtractExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О восстановлении в число студентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RestorationStuExtractExtUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<RestorationStuExtractExtUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt";
    public static final String ENTITY_NAME = "restorationStuExtractExtUsmaExt";
    public static final int VERSION_HASH = -3139927;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_START_GRANTS_PAYING = "startGrantsPaying";
    public static final String P_START_GRANTS_PAYING_DATE = "startGrantsPayingDate";

    private RestorationStuExtractExt _extractExt;     // Выписка из сборного приказа по студенту. О восстановлении (расш. вариант)
    private boolean _startGrantsPaying = false;     // Назначить выплату академической стипендии
    private Date _startGrantsPayingDate;     // Дата начала выплаты стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О восстановлении (расш. вариант). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RestorationStuExtractExt getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О восстановлении (расш. вариант). Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(RestorationStuExtractExt extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Назначить выплату академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStartGrantsPaying()
    {
        return _startGrantsPaying;
    }

    /**
     * @param startGrantsPaying Назначить выплату академической стипендии. Свойство не может быть null.
     */
    public void setStartGrantsPaying(boolean startGrantsPaying)
    {
        dirty(_startGrantsPaying, startGrantsPaying);
        _startGrantsPaying = startGrantsPaying;
    }

    /**
     * @return Дата начала выплаты стипендии.
     */
    public Date getStartGrantsPayingDate()
    {
        return _startGrantsPayingDate;
    }

    /**
     * @param startGrantsPayingDate Дата начала выплаты стипендии.
     */
    public void setStartGrantsPayingDate(Date startGrantsPayingDate)
    {
        dirty(_startGrantsPayingDate, startGrantsPayingDate);
        _startGrantsPayingDate = startGrantsPayingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RestorationStuExtractExtUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((RestorationStuExtractExtUsmaExt)another).getExtractExt());
            }
            setStartGrantsPaying(((RestorationStuExtractExtUsmaExt)another).isStartGrantsPaying());
            setStartGrantsPayingDate(((RestorationStuExtractExtUsmaExt)another).getStartGrantsPayingDate());
        }
    }

    public INaturalId<RestorationStuExtractExtUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<RestorationStuExtractExtUsmaExtGen>
    {
        private static final String PROXY_NAME = "RestorationStuExtractExtUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(RestorationStuExtractExt extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof RestorationStuExtractExtUsmaExtGen.NaturalId) ) return false;

            RestorationStuExtractExtUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RestorationStuExtractExtUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RestorationStuExtractExtUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new RestorationStuExtractExtUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "startGrantsPaying":
                    return obj.isStartGrantsPaying();
                case "startGrantsPayingDate":
                    return obj.getStartGrantsPayingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((RestorationStuExtractExt) value);
                    return;
                case "startGrantsPaying":
                    obj.setStartGrantsPaying((Boolean) value);
                    return;
                case "startGrantsPayingDate":
                    obj.setStartGrantsPayingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "startGrantsPaying":
                        return true;
                case "startGrantsPayingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "startGrantsPaying":
                    return true;
                case "startGrantsPayingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return RestorationStuExtractExt.class;
                case "startGrantsPaying":
                    return Boolean.class;
                case "startGrantsPayingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RestorationStuExtractExtUsmaExt> _dslPath = new Path<RestorationStuExtractExtUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RestorationStuExtractExtUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О восстановлении (расш. вариант). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt#getExtractExt()
     */
    public static RestorationStuExtractExt.Path<RestorationStuExtractExt> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Назначить выплату академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt#isStartGrantsPaying()
     */
    public static PropertyPath<Boolean> startGrantsPaying()
    {
        return _dslPath.startGrantsPaying();
    }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt#getStartGrantsPayingDate()
     */
    public static PropertyPath<Date> startGrantsPayingDate()
    {
        return _dslPath.startGrantsPayingDate();
    }

    public static class Path<E extends RestorationStuExtractExtUsmaExt> extends EntityPath<E>
    {
        private RestorationStuExtractExt.Path<RestorationStuExtractExt> _extractExt;
        private PropertyPath<Boolean> _startGrantsPaying;
        private PropertyPath<Date> _startGrantsPayingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О восстановлении (расш. вариант). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt#getExtractExt()
     */
        public RestorationStuExtractExt.Path<RestorationStuExtractExt> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new RestorationStuExtractExt.Path<RestorationStuExtractExt>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Назначить выплату академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt#isStartGrantsPaying()
     */
        public PropertyPath<Boolean> startGrantsPaying()
        {
            if(_startGrantsPaying == null )
                _startGrantsPaying = new PropertyPath<Boolean>(RestorationStuExtractExtUsmaExtGen.P_START_GRANTS_PAYING, this);
            return _startGrantsPaying;
        }

    /**
     * @return Дата начала выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt#getStartGrantsPayingDate()
     */
        public PropertyPath<Date> startGrantsPayingDate()
        {
            if(_startGrantsPayingDate == null )
                _startGrantsPayingDate = new PropertyPath<Date>(RestorationStuExtractExtUsmaExtGen.P_START_GRANTS_PAYING_DATE, this);
            return _startGrantsPayingDate;
        }

        public Class getEntityClass()
        {
            return RestorationStuExtractExtUsmaExt.class;
        }

        public String getEntityName()
        {
            return "restorationStuExtractExtUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
