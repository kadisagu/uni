package ru.tandemservice.uniusma.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusma_2x6x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dischargingStuExtractUsmaExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("dischargingstuextractusmaext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("extractext_id", DBType.LONG).setNullable(false), 
				new DBColumn("contracttype_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("contractnumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("contractdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("contractparagraph_p", DBType.createVarchar(255)), 
				new DBColumn("debteliminatedate_p", DBType.DATE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("dischargingStuExtractUsmaExt");

		}


    }
}