/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public class UsmaExcludeEduPlanStuExtractPrint implements IPrintFormCreator<UsmaExcludeEduPlanStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, UsmaExcludeEduPlanStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Person person = extract.getEntity().getPerson();

        boolean isMale = person.isMale();
        short i = 1;

        modifier.put("session", extract.getSessionAcadDebt());
        modifier.put("disciplines", extract.getDisciplinesAcadDebt());
        modifier.put("notFulfilled_A", "не выполнивш" + (isMale ? "его" : "ую"));
        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getExcludeDate()));
        modifier.put("stopPayments", (extract.isStopGrantsPaying() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") + " отменить выплату академической стипендии с " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingDate()) + " г." ) : ""));
        modifier.put("giveDiploma", (extract.isGiveDiploma() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") + " выдать диплом о незаконченном высшем образовании.") : ""));

        boolean budget = extract.getEntity().getCompensationType().isBudget();
        modifier.put("compensationTypeStr_G", budget ? "бюджетной основы" : "внебюджетной основы");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

}
