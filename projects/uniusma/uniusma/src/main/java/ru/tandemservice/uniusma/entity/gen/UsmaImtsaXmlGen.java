package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * XML файл ИМЦА
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaImtsaXmlGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaImtsaXml";
    public static final String ENTITY_NAME = "usmaImtsaXml";
    public static final int VERSION_HASH = 1074780790;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String P_FILE_NAME = "fileName";
    public static final String P_ENCODING = "encoding";
    public static final String P_XML = "xml";
    public static final String P_NUMBER = "number";

    private EppEduPlanVersionBlock _block;     // Блок УПв
    private String _fileName;     // Имя файла
    private String _encoding;     // Кодировка файла
    private byte[] _xml;     // XML файл
    private int _number;     // Номер версии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null и должно быть уникальным.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Имя файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileName()
    {
        return _fileName;
    }

    /**
     * @param fileName Имя файла. Свойство не может быть null.
     */
    public void setFileName(String fileName)
    {
        dirty(_fileName, fileName);
        _fileName = fileName;
    }

    /**
     * @return Кодировка файла.
     */
    @Length(max=255)
    public String getEncoding()
    {
        return _encoding;
    }

    /**
     * @param encoding Кодировка файла.
     */
    public void setEncoding(String encoding)
    {
        dirty(_encoding, encoding);
        _encoding = encoding;
    }

    /**
     * @return XML файл. Свойство не может быть null.
     */
    @NotNull
    public byte[] getXml()
    {
        return _xml;
    }

    /**
     * @param xml XML файл. Свойство не может быть null.
     */
    public void setXml(byte[] xml)
    {
        dirty(_xml, xml);
        _xml = xml;
    }

    /**
     * @return Номер версии. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер версии. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaImtsaXmlGen)
        {
            setBlock(((UsmaImtsaXml)another).getBlock());
            setFileName(((UsmaImtsaXml)another).getFileName());
            setEncoding(((UsmaImtsaXml)another).getEncoding());
            setXml(((UsmaImtsaXml)another).getXml());
            setNumber(((UsmaImtsaXml)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaImtsaXmlGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaImtsaXml.class;
        }

        public T newInstance()
        {
            return (T) new UsmaImtsaXml();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "fileName":
                    return obj.getFileName();
                case "encoding":
                    return obj.getEncoding();
                case "xml":
                    return obj.getXml();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "fileName":
                    obj.setFileName((String) value);
                    return;
                case "encoding":
                    obj.setEncoding((String) value);
                    return;
                case "xml":
                    obj.setXml((byte[]) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "fileName":
                        return true;
                case "encoding":
                        return true;
                case "xml":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "fileName":
                    return true;
                case "encoding":
                    return true;
                case "xml":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "fileName":
                    return String.class;
                case "encoding":
                    return String.class;
                case "xml":
                    return byte[].class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaImtsaXml> _dslPath = new Path<UsmaImtsaXml>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaImtsaXml");
    }
            

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Имя файла. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getFileName()
     */
    public static PropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    /**
     * @return Кодировка файла.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getEncoding()
     */
    public static PropertyPath<String> encoding()
    {
        return _dslPath.encoding();
    }

    /**
     * @return XML файл. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getXml()
     */
    public static PropertyPath<byte[]> xml()
    {
        return _dslPath.xml();
    }

    /**
     * @return Номер версии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends UsmaImtsaXml> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<String> _fileName;
        private PropertyPath<String> _encoding;
        private PropertyPath<byte[]> _xml;
        private PropertyPath<Integer> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Имя файла. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getFileName()
     */
        public PropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new PropertyPath<String>(UsmaImtsaXmlGen.P_FILE_NAME, this);
            return _fileName;
        }

    /**
     * @return Кодировка файла.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getEncoding()
     */
        public PropertyPath<String> encoding()
        {
            if(_encoding == null )
                _encoding = new PropertyPath<String>(UsmaImtsaXmlGen.P_ENCODING, this);
            return _encoding;
        }

    /**
     * @return XML файл. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getXml()
     */
        public PropertyPath<byte[]> xml()
        {
            if(_xml == null )
                _xml = new PropertyPath<byte[]>(UsmaImtsaXmlGen.P_XML, this);
            return _xml;
        }

    /**
     * @return Номер версии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaImtsaXml#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UsmaImtsaXmlGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return UsmaImtsaXml.class;
        }

        public String getEntityName()
        {
            return "usmaImtsaXml";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
