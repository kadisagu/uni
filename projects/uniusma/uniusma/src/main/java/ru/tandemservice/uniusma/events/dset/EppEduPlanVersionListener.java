/**
 *$Id$
 */
package ru.tandemservice.uniusma.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.12.2013
 */
public class EppEduPlanVersionListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppEduPlanVersion.class, this, Collections.singleton(EppEduPlanVersion.L_STATE));
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "a").column("a.id").createStatement(event.getContext()).list();
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        BatchUtils.execute(new HashSet<>(params), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                IDQLSelectableQuery checkStateIdsQuery = new DQLSelectBuilder()
                        .fromEntity(UsmaEpvCheckState.class, "cs")
                        .column(property("cs", UsmaEpvCheckState.id()))
                        .where(in(property("cs", UsmaEpvCheckState.version().id()), elements))
                        .where(eq(property("cs", UsmaEpvCheckState.version().state().code()), value(EppState.STATE_REJECTED)))
                        .buildQuery();

                new DQLUpdateBuilder(UsmaEpvCheckState.class)
                        .set(UsmaEpvCheckState.checkedByUMU().s(), value(false))
                        .set(UsmaEpvCheckState.checkedByCRK().s(), value(false))
                        .set(UsmaEpvCheckState.checkedByOOP().s(), value(false))
                        .set(UsmaEpvCheckState.checkedByUMUDate().s(), nul())
                        .set(UsmaEpvCheckState.checkedByCRKDate().s(), nul())
                        .set(UsmaEpvCheckState.checkedByOOPDate().s(), nul())
                        .where(in(property(UsmaEpvCheckState.id()), checkStateIdsQuery))
                        .createStatement(session)
                        .execute();
            }
        });

        return super.beforeCompletion(session, params);
    }
}