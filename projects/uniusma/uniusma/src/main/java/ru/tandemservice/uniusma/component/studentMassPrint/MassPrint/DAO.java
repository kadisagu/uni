/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.MassPrint;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class DAO extends ru.tandemservice.uniusma.component.studentMassPrint.Base.DAO<Model>
        implements ru.tandemservice.uniusma.component.studentMassPrint.Base.IDAO<Model>
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getStudentTemplateDocumentListModel() == null)
        {
            List<WrapStudentDocument> wrappedDocuments = getCatalogItemList(StudentDocumentType.class).stream()
                    .filter(std -> ApplicationRuntime.containsBean("mpd" + std.getIndex()))
                    .map(WrapStudentDocument::new)
                    .collect(Collectors.toList());
            model.setStudentTemplateDocumentListModel(new LazySimpleSelectModel<>(wrappedDocuments));
        }

        if (model.getOrgUnitId() != null)
            model.setSecModel(new OrgUnitSecModel(get(OrgUnit.class, model.getOrgUnitId())));
    }

    @Override
    protected void addAdditionalRestrictions(Model model, DQLSelectBuilder builder, String alias)
    {
        Object studentCategory = model.getSettings().get("studentCategory");
        FilterUtils.applySelectFilter(builder, alias, Student.studentCategory(), studentCategory);
    }

    @Override
    protected void wrapPatchedList(List<ViewWrapper<Student>> lst)
    {
    }

    @Override
    protected boolean withArchivalStudents()
    {
        return false;
    }
}
