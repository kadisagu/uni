package ru.tandemservice.uniusma.entity.eduPlan;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * Рассредоточенность практики (УГМА)
 */
public class UsmaPracticeDispersion extends UsmaPracticeDispersionGen
{
    public UsmaPracticeDispersion()
    {

    }

    public UsmaPracticeDispersion(EppEpvRegistryRow practice, boolean dispersed)
    {
        this.setPractice(practice);
        this.setDispersed(dispersed);
    }
}