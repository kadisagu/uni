/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public interface IDAO extends IModularStudentExtractPubDAO<UsmaExcludeDebtStuExtract, Model>
{
}