/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.UsmaStudentCardAttachmentReportManager;
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.IStudentCardAttachmentReportDao;
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.StudentWpeActionContainer;
import ru.tandemservice.uniusma.entity.catalog.codes.UniScriptItemCodes;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 23.12.2015
 */
@Input({@Bind(key = "orgUnitId", binding = UsmaStudentCardAttachmentReportPubUI.ORG_UNIT_ID),})
public class UsmaStudentCardAttachmentReportPubUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";

    private final IStudentCardAttachmentReportDao dao = UsmaStudentCardAttachmentReportManager.instance().dao();

    private Long orgUnitId;
    private EppYearEducationProcess eduYear;
    private List<YearDistributionPart> yearPart;
    private List<Course> course;
    private List<Group> group;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UsmaStudentCardAttachmentReportPub.GROUP_DS.equals(dataSource.getName()))
            dataSource.put(ORG_UNIT_ID, getOrgUnitId());
    }

    //Handlers
    public void onClickApply()
    {
        final IStudentCardAttachmentReportDao dao = UsmaStudentCardAttachmentReportManager.instance().dao();

        final List<StudentWpeActionContainer> studentWpeActionList = dao.getStudentWpeActions(getOrgUnit(), getEduYear(), getYearPart(), getCourse(), getGroup());
        final RtfDocument resultDoc = createDocument(studentWpeActionList);
        final IDocumentRenderer documentRenderer = new CommonBaseRenderer().rtf().fileName("Учебная карточка УГМУ - вложение.rtf").document(resultDoc);
        BusinessComponentUtils.downloadDocument(documentRenderer, true);
    }

    private RtfDocument createDocument(Collection<StudentWpeActionContainer> studentWpeActionList)
    {
        if (studentWpeActionList == null || studentWpeActionList.isEmpty())
            throw new ApplicationException("Нет данных для печати");

        final IStudentCardAttachmentReportDao dao = UsmaStudentCardAttachmentReportManager.instance().dao();
        final UniScriptItem script = dao.getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_ADD_PRINT_SCRIPT);
        final RtfDocument rtfDocument = new RtfDocument();

        final Iterator<StudentWpeActionContainer> iterator = studentWpeActionList.iterator();
        while (iterator.hasNext())
        {
            final StudentWpeActionContainer studentWpeActionContainer = iterator.next();
            final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script,
                    "template", script.getCurrentTemplate(),
                    "data", studentWpeActionContainer);
            final RtfDocument resultDoc = (RtfDocument) scriptResult.get("rtf");
            rtfDocument.addElement(resultDoc);
            if (!iterator.hasNext())
            {
                rtfDocument.setSettings(resultDoc.getSettings());
                rtfDocument.setHeader(resultDoc.getHeader());
            }
        }
        return rtfDocument;
    }

    //Getters/Setters
    @Override
    public ISecured getSecuredObject()
    {
        return getOrgUnitId() != null ? DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()) : super.getSecuredObject();
    }

    public String getPermissionKey()
    {
        return getOrgUnitId() == null ? "usmaStudentCardLinerReportViewPermissionKey" : new OrgUnitSecModel(getOrgUnit()).getPermission("orgUnit_usmaStudentCardLinerReportViewPermissionKey");
    }

    private OrgUnit getOrgUnit()
    {
        return dao.getNotNull(OrgUnit.class, getOrgUnitId());
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public EppYearEducationProcess getEduYear()
    {
        return eduYear;
    }

    public void setEduYear(EppYearEducationProcess eduYear)
    {
        this.eduYear = eduYear;
    }

    public List<YearDistributionPart> getYearPart()
    {
        return yearPart;
    }

    public void setYearPart(List<YearDistributionPart> yearPart)
    {
        this.yearPart = yearPart;
    }

    public List<Course> getCourse()
    {
        return course;
    }

    public void setCourse(List<Course> course)
    {
        this.course = course;
    }

    public List<Group> getGroup()
    {
        return group;
    }

    public void setGroup(List<Group> group)
    {
        this.group = group;
    }
}
