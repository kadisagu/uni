/* $Id: IDAO.java 30889 2013-11-01 08:57:13Z alopatin $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.group.GroupStudentListPrint;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author vip_delete
 * @since 12.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
    String[][] getStudentData(Model model, boolean addBookNum);
}
