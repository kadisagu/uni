/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionScheduleTab;

import com.google.common.collect.ImmutableMap;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.RichCancelColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.RichSaveEditColumn;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniusma.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeek;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;
import ru.tandemservice.uniusma.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniusma.entity.catalog.codes.UsmaSchedulePartitionTypeCodes;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniusma.utils.UsmaEduPlanVersionScheduleUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));

        final EppEduPlanVersion version = get(EppEduPlanVersion.class, model.getId());
        model.setVersion(version);

        final UsmaEduPlanVersionScheduleUtils schedule = new UsmaEduPlanVersionScheduleUtils()
        {
            @Override
            protected boolean isTotalCourseRowPresent()
            {
                return false;
            }

            @Override
            protected EppEduPlanVersion getEduPlanVersion()
            {
                return version;
            }
        };

        model.setScheduleDataSource(schedule.getUsmaRangeSelectionWeekTypeListDataSource());
        final UsmaRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> rangeModel = model.getScheduleDataSource();

        {
            final AbstractListDataSource<ViewWrapper<Course>> dataSource = rangeModel.getDataSource();

            if (!model.getVersion().getState().isReadOnlyState())
            {
                dataSource.addColumn(new RichSaveEditColumn(rangeModel, "onClickRichRowSave", "onClickRichRowEdit").setPermissionKey("editUsmaSchedule_eppEduPlanVersion"));
                dataSource.addColumn(new RichCancelColumn(rangeModel, "onClickRichRowCancel").setPermissionKey("editUsmaSchedule_eppEduPlanVersion"));
            }
            model.setScheduleDataSource(rangeModel);
        }

        /******************************************************************/
        /***** Сводные данные по бюджету времени **************************/

        class DoubleWrapper
        {
            private int _ratio;

            public DoubleWrapper(int ratio){ _ratio = ratio; }

            public Double wrap(Long value){ return ((double) value/_ratio); }
            public Long unwrap(Double value){ return Math.round(value * _ratio); }

            public Double updateValue(Double value){ return wrap(unwrap(value)); }
            public Double sum(Double d1, Double d2){ return mathSum(d1, d2, true); }
            public Double diff(Double d1, Double d2){ return mathSum(d1, d2, false); }

            private Double mathSum(Double d1, Double d2, boolean sign){ return wrap(unwrap(d1) + (sign ? 1 : -1) * unwrap(d2)); }
        }

        DoubleWrapper vw = new DoubleWrapper(6);
        Map<String, String> typeFromTypeToMap = SafeMap.get(key -> key);

        typeFromTypeToMap.put(EppWeekTypeCodes.TEACH_PRACTICE_WITH_AUDIT_LOAD, EppWeekTypeCodes.THEORY);
        typeFromTypeToMap.put(EppWeekTypeCodes.RESEARCH_WORK_WITH_AUDIT_LOAD, EppWeekTypeCodes.THEORY);
        typeFromTypeToMap.put(EppWeekTypeCodes.WORK_PRACTICE_WITH_AUDIT_LOAD, EppWeekTypeCodes.THEORY);

        Map<Long, PairKey<Integer, Integer>> term2CoursePartMap = new HashMap<>();
        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm: getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), version.getEduPlan().getDevelopGrid()))
        {
            term2CoursePartMap.put(developGridTerm.getTerm().getId(), PairKey.create(developGridTerm.getCourse().getIntValue(), developGridTerm.getPartNumber()));
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());
        }


        //  course       part     row code    value
        final Map<Integer, Map<Integer, Map<String, Double>>> dataMap = SafeMap.get(key -> SafeMap.get(key1 -> SafeMap.get(key2 -> 0.0d)));

        DQLSelectBuilder termWeekTypeBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeek.class, "w")
                .column(property("w", UsmaEduPlanVersionWeek.term().id()))
                .column(property("w", UsmaEduPlanVersionWeek.weekType().code()))
                .column(DQLFunctions.count(property("w", UsmaEduPlanVersionWeek.id())))
                .where(eq(property("w", UsmaEduPlanVersionWeek.version()), value(version)))
                .where(isNotNull(property("w", UsmaEduPlanVersionWeek.weekType())))
                .group(property("w", UsmaEduPlanVersionWeek.term().id()))
                .group(property("w", UsmaEduPlanVersionWeek.weekType().code()));

        int parts = get(UsmaEduPlanVersionPartitionType.class, UsmaEduPlanVersionPartitionType.version(), version).getPartitionType().getPartsNumber();
        DQLSelectBuilder termWeekPartTypeBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeekPart.class, "wp")
                .column(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().term().id()))
                .column(property("wp", UsmaEduPlanVersionWeekPart.weekType().code()))
                .column(DQLFunctions.count(property("wp", UsmaEduPlanVersionWeekPart.id())))
                .where(eq(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(version)))
                .group(property("wp", UsmaEduPlanVersionWeekPart.eduPlanVersionWeek().term().id()))
                .group(property("wp", UsmaEduPlanVersionWeekPart.weekType().code()));

        for (boolean part: new boolean[]{false, true})
        {
            DQLSelectBuilder builder = part ? termWeekPartTypeBuilder : termWeekTypeBuilder;
            Double coeff = part ? 1.0d/parts : 1.0d;
            List<Object[]> rows = builder.createStatement(getSession()).<Object[]>list();
            for (Object[] row: rows)
            {
                Long termId = (Long) row[0];
                String typeCode = typeFromTypeToMap.get((String) row[1]);
                Long count = (Long) row[2];

                PairKey<Integer, Integer> key = term2CoursePartMap.get(termId);
                Integer courseNumber = key.getFirst();
                Integer partNumber = key.getSecond();

                Map<Integer, Map<String, Double>> partDataMap = dataMap.get(courseNumber);

                Map<String, Double> typeDataMap = partDataMap.get(partNumber);
                double value = coeff * count + typeDataMap.get(typeCode);
                value = vw.updateValue(value);
                typeDataMap.put(typeCode, value);

                partDataMap.get(0).put(typeCode, vw.sum(value, partDataMap.get(0).get(typeCode)));
            }
        }

        Map<String, String> typeMap = ImmutableMap.of(
                EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, UsmaSummaryBudgetRowWrapper.TEACH_DISPERSED,
                EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR, UsmaSummaryBudgetRowWrapper.NIR_DISPERSED,
                EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION, UsmaSummaryBudgetRowWrapper.WORK);

        DQLSelectBuilder practiceBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaPracticeDispersion.class, "d")
                .joinEntity("d", DQLJoinType.inner, EppEpvRegistryRow.class, "rr", eq(property("rr", EppEpvRegistryRow.id()), property("d", UsmaPracticeDispersion.practice())))
                .joinEntity("rr", DQLJoinType.inner, EppEpvRowTerm.class, "rt", eq(property("rt", EppEpvRowTerm.row().id()), property("rr", EppEpvTermDistributedRow.id())))
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.owner().eduPlanVersion().fromAlias("rr"), "v")

                .column(property("rr", EppEpvRegistryRow.registryElementType().code()))
                .column(property("rt", EppEpvRowTerm.term().id()))
                .column(property("rt", EppEpvRowTerm.weeks()))

                .where(and(
                    eq(property("v", EppEduPlanVersion.id()), value(version.getId())),
                    instanceOf(EppEpvRegistryRow.owner().fromAlias("rr").s(), EppEduPlanVersionRootBlock.class)))
                .where(in(property("rr", EppEpvRegistryRow.registryElementType().code()), typeMap.keySet()))
                .where(eq(property("d", UsmaPracticeDispersion.dispersed()), value(Boolean.TRUE)))
                .where(gt(property("rt", EppEpvRowTerm.weeks()), value(0L)));

        for (Object[] row: practiceBuilder.createStatement(getSession()).<Object[]>list())
        {
            String typeCode = typeMap.get((String) row[0]) ;
            Long termId = (Long) row[1];
            Double weesSize = UniEppUtils.wrap((Long) row[2]);

            PairKey<Integer, Integer> key = term2CoursePartMap.get(termId);
            Integer courseNumber = key.getFirst();
            Integer partNumber = key.getSecond();

            Map<Integer, Map<String, Double>> partDataMap = dataMap.get(courseNumber);

            Map<String, Double> typeDataMap = partDataMap.get(partNumber);
            double value = typeDataMap.get(typeCode) + weesSize;
            value = vw.updateValue(value);
            typeDataMap.put(typeCode, value);
            typeDataMap.put(EppWeekTypeCodes.THEORY, vw.diff(typeDataMap.get(EppWeekTypeCodes.THEORY), weesSize));
            partDataMap.get(0).put(typeCode, vw.sum(weesSize,partDataMap.get(0).get(typeCode)));
            partDataMap.get(0).put(EppWeekTypeCodes.THEORY, vw.diff(partDataMap.get(0).get(EppWeekTypeCodes.THEORY), weesSize));
        }


        Double total = 0.0d;
        for (Map.Entry<Integer, Map<Integer, Map<String, Double>>> courseEntry: new HashMap<>(dataMap).entrySet())
        {
            Integer courseNumber = courseEntry.getKey();
            for (Map.Entry<Integer, Map<String, Double>> partEntry: courseEntry.getValue().entrySet())
            {
                Integer partNumber = partEntry.getKey();
                Double totalPartValue = 0.0d;
                for (Map.Entry<String, Double> typeEntry: partEntry.getValue().entrySet())
                {
                    String typeCode = typeEntry.getKey();
                    Double value = typeEntry.getValue();

                    if (partNumber != 0)
                        dataMap.get(0).get(0).put(typeCode, value + dataMap.get(0).get(0).get(typeCode));

                    totalPartValue += value;
                }

                dataMap.get(courseNumber).get(partNumber).put(UsmaSummaryBudgetRowWrapper.TOTAL, totalPartValue);
                if (partNumber != 0)
                    total += totalPartValue;
            }
        }

        dataMap.get(0).get(0).put(UsmaSummaryBudgetRowWrapper.TOTAL, total);

        final Map<String, Long> typeIdMap = SafeMap.get(new SafeMap.Callback<String, Long>()
        {
            private long id = -1L;
            @Override public Long resolve(String key){ return --id; }
        });

        IStyleResolver boldStyle = rowEntity -> rowEntity.getId().equals(typeIdMap.get(UsmaSummaryBudgetRowWrapper.TOTAL) ) ? "font-weight: bold;" : "";

        StaticListDataSource<UsmaSummaryBudgetRowWrapper> dataSource = new StaticListDataSource<>();
        SimpleColumn titleColumn = new SimpleColumn("Тип учебной недели", UsmaSummaryBudgetRowWrapper.TITLE);
        titleColumn.setOrderable(false).setStyleResolver(boldStyle);
        dataSource.addColumn(titleColumn);

        final Map<Long, String> typeCodeMap = new HashMap<>();
        for (Map.Entry<Integer, Set<Integer>> courseEntry: coursePartsMap.entrySet())
        {
            final Integer courseNumber = courseEntry.getKey();
            HeadColumn courseColumn = new HeadColumn("course" + courseNumber, "Курс " + courseNumber);
            courseColumn.setHeaderAlign("center");

            for (final Integer partNumber: courseEntry.getValue())
            {
                SimpleColumn termColumn = new SimpleColumn("Семестр " + partNumber, partNumber )
                {
                    @Override
                    public String getContent(IEntity entity)
                    {
                        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(dataMap.get(courseNumber).get(partNumber).get(typeCodeMap.get(entity.getId())));
                    }
                };
                termColumn.setVerticalHeader(true).setOrderable(false).setAlign("center").setHeaderAlign("center").setStyleResolver(boldStyle);
                courseColumn.addColumn(termColumn);
            }

            SimpleColumn courseTotalColumn = new SimpleColumn("Всего", courseNumber)
            {
                @Override
                public String getContent(IEntity entity)
                {
                    return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(dataMap.get(courseNumber).get(0).get(typeCodeMap.get(entity.getId())));
                }
            };
            courseTotalColumn.setVerticalHeader(true).setOrderable(false).setAlign("center").setHeaderAlign("center").setStyleResolver(boldStyle);
            courseColumn.addColumn(courseTotalColumn);

            dataSource.addColumn(courseColumn);
        }

        SimpleColumn totalColumn = new SimpleColumn("Всего", "total")
        {
            @Override
            public String getContent(IEntity entity)
            {
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(dataMap.get(0).get(0).get(typeCodeMap.get(entity.getId())));
            }
        };
        totalColumn.setOrderable(false).setAlign("center").setStyleResolver(boldStyle);
        dataSource.addColumn(totalColumn);


        List<UsmaSummaryBudgetRowWrapper> wrappers = new ArrayList<>();

        for (String code: UsmaSummaryBudgetRowWrapper.CODES)
        {
            wrappers.add(new UsmaSummaryBudgetRowWrapper(typeIdMap.get(code), code));
        }

        for (Map.Entry<String, Long> typeEntry: typeIdMap.entrySet())
        {
            typeCodeMap.put(typeEntry.getValue(), typeEntry.getKey());
        }


        Collections.sort(wrappers, UsmaSummaryBudgetRowWrapper.COMPARATOR);

        dataSource.setRowList(wrappers);
        model.setSummaryBudgetDataSource(dataSource);
    }

    @Override
    public void prepareEditRow(final Model model, final Long rowId)
    {
        final Course course = this.getNotNull(Course.class, rowId);
        final UsmaRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleDataSource = model.getScheduleDataSource();

        List<UsmaEduPlanVersionWeek> weekList = new DQLSelectBuilder()
                .fromEntity(UsmaEduPlanVersionWeek.class, "w")
                .where(eq(property(UsmaEduPlanVersionWeek.version().fromAlias("w")), value(model.getVersion())))
                .where(eq(property(UsmaEduPlanVersionWeek.course().fromAlias("w")), value(course)))
                .createStatement(getSession())
                .list();

        if (weekList.isEmpty())
        {
            final DevelopGrid developGrid = model.getVersion().getEduPlan().getDevelopGrid();

            scheduleDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, UsmaEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(developGrid, course)));

        } else
        {
            // уже есть связи => можно установить границы семестров
            scheduleDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, scheduleDataSource.getRow2points().get(rowId)));
        }
    }


    @Override
    public void updateScheduleRow(final Long courseId, final Model model)
    {
        final Session session = this.getSession();
        final Course course = this.getNotNull(Course.class, courseId);
        final List<EppWeek> weekList = this.getCatalogItemListOrderByCode(EppWeek.class);

        Map<Long, Map<Long, Map<Integer, EppWeekType>>> fullDataMap = model.getScheduleDataSource().getFullDataMap();

        // U P D A T E

        final Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        final List<Integer> terms = UsmaEduPlanVersionScheduleUtils.getDevelopGridDetailTerms(model.getVersion().getEduPlan().getDevelopGrid(), course);

        // существующие в базе связи для данной строки

        Map<PairKey<Long, Long>, UsmaEduPlanVersionWeek> fullMap = new HashMap<>();
        final EppWeekType theory = getCatalogItem(EppWeekType.class, EppWeekTypeCodes.THEORY);

        for (UsmaEduPlanVersionWeek week : getList(UsmaEduPlanVersionWeek.class, UsmaEduPlanVersionWeek.version(), model.getVersion()))
        {
            fullMap.put(PairKey.create(week.getCourse().getId(), week.getWeek().getId()), week);
        }

        final int[] ranges = model.getScheduleDataSource().getSelection().getRanges();
        for (final EppWeek week : weekList)
        {
            final PairKey<Long, Long> key = PairKey.create(courseId, week.getId());
            UsmaEduPlanVersionWeek item = fullMap.get(key);

            final int partNumber = ranges[week.getNumber() - 1];
            if (partNumber == -1)
            {
                // неделя не попала ни в один диапазон. надо удалить связь
                if (item != null)
                {
                    session.delete(item);
                }

            } else
            {
                final Term term = termMap.get(terms.get(partNumber));
                Map<Integer, EppWeekType> weekTypeMap = fullDataMap.get(courseId).get(week.getId());

                if (null == term)
                {
                    if (null != item)
                    {
                        session.delete(item);
                    }

                } else
                {
                    if (item == null)
                    {
                        item = new UsmaEduPlanVersionWeek(model.getVersion(), course, week);

                    } else
                    {
                        new DQLDeleteBuilder(UsmaEduPlanVersionWeekPart.class).where(eq(property(UsmaEduPlanVersionWeekPart.eduPlanVersionWeek()), value(item))).createStatement(getSession()).execute();
                    }

                    // надо обновить
                    item.setTerm(term);

                    if (weekTypeMap.size() == 1)
                    {
                        EppWeekType weekType = weekTypeMap.get(0);
                        item.setWeekType(weekType == null ? theory : weekType);
                    }

                    session.saveOrUpdate(item);

                    if (weekTypeMap.size() > 1)
                    {
                        item.setWeekType(null);
                        update(item);

                        for (Map.Entry<Integer, EppWeekType> weekTypeEntry : weekTypeMap.entrySet())
                        {
                            EppWeekType weekType = weekTypeEntry.getValue();
                            UsmaEduPlanVersionWeekPart part = new UsmaEduPlanVersionWeekPart(item, weekTypeEntry.getKey(), weekType == null ? theory : weekType);

                            save(part);
                        }
                    }
                }
            }
        }

        IImtsaImportDAO.instance.get().updateEduPlanVersionWeekTypes(model.getVersion().getId(), courseId);
    }

    @Override
    public void updateEduPlanVersionPartitionType(Model model)
    {
        EppEduPlanVersion version = get(EppEduPlanVersion.class, model.getId());
        UsmaEduPlanVersionPartitionType versionPartitionType = get(UsmaEduPlanVersionPartitionType.class, UsmaEduPlanVersionPartitionType.version(), version);
        if (versionPartitionType == null)
        {
            save(new UsmaEduPlanVersionPartitionType(version, getCatalogItem(UsmaSchedulePartitionType.class, UsmaSchedulePartitionTypeCodes.WEEK)));
        }

        model.setVersionPartitionType(versionPartitionType);
    }
}