/* $Id$ */
package ru.tandemservice.uniusma.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

/**
 * @author Andrey Avetisov
 * @since 30.10.2014
 */
public interface IUniUsmaDao
{
    String UNIUSMA_DAO_BEAN_NAME = "uniUsmaDao";

    final SpringBeanCache<IUniUsmaDao> instance = new SpringBeanCache<>(UNIUSMA_DAO_BEAN_NAME);
    UsmaImtsaXml getXml(Long blockId);
}
