package ru.tandemservice.uniusma.dao;

import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class UNIUSMASessionBulletinPrintDAO extends SessionBulletinPrintDAO
{
    @Override
    protected List<ColumnType> prepareColumnList(final BulletinPrintInfo printInfo)
    {
        if (printInfo.getBulletin().getGroup().getActivityPart().getRegistryElement() instanceof EppRegistryPractice)
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.GROUP,
                    ColumnType.MARK);
        }
        List<ColumnType> columnTypeList = super.prepareColumnList(printInfo);
        columnTypeList.remove(ColumnType.GROUP);
        return columnTypeList;
    }

    @Override
    protected List<ColumnType> getTemplateColumnList(final BulletinPrintInfo printInfo)
    {
        if (printInfo.getBulletin().getGroup().getActivityPart().getRegistryElement() instanceof EppRegistryPractice)
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.GROUP,
                    ColumnType.MARK
            );
        }
        return super.getTemplateColumnList(printInfo);
    }

    @Override
    protected void initTemplates(final Map<SessionBulletinDocument, BulletinPrintInfo> printInfoMap)
    {
        super.initTemplates(printInfoMap);

        final RtfReader reader = new RtfReader();
        // шаблон ведомости на практику
        UnisessionBulletinTemplate practiceTemplateCI = getCatalogItem(UnisessionBulletinTemplate.class, "uniusma_practice");
        RtfDocument practiceTemplate = practiceTemplateCI == null ? null : reader.read(practiceTemplateCI.getContent());

        for (final BulletinPrintInfo printInfo : printInfoMap.values())
        {
            // если есть шаблон на практику и это практика - печатаем по шаблону на практику
            if (practiceTemplate != null && printInfo.getBulletin().getGroup().getActivityPart().getHierarhyParent().getParent().isPracticeElement())
            {
                printInfo.setDocument(practiceTemplate.getClone());
            }
        }
    }

    @Override
    protected void printAdditionalInfo(final BulletinPrintInfo printInfo, SessionBulletinPrintDAO.MarkAmountData markAmountData)
    {
        RtfDocument document = printInfo.getDocument();
        RtfInjectModifier modifier = new RtfInjectModifier();
//        начинка для метки visas
        List<PpsEntry> commission = printInfo.getCommission();
        RtfString tutorString = new RtfString();
        RtfString emptyString = new RtfString();
        int counter = 0;
        for (PpsEntry tutor : commission)
        {
            counter++;
            tutorString.append(String.valueOf(counter));
            tutorString.append(". ");
            tutorString.append(tutor.getPerson().getIdentityCard().getFio());
            tutorString.append(IRtfData.LINE);
            emptyString.append("____________________________");
            emptyString.append(IRtfData.LINE);
        }
        modifier.put("visas", tutorString);
        modifier.put("line", emptyString);
        modifier.modify(document);
    }
}
