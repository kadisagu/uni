/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduplanVersionImtsaImportLogTab;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaImtsaImportLog;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 05.09.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        List<EppEduPlanVersionBlock> blocks = new ArrayList<>();
        Map<EppEduPlanVersionBlock, String[]> logMap = new HashMap<>();
        List<UsmaImtsaImportLog> logList = getList(UsmaImtsaImportLog.class, UsmaImtsaImportLog.block().eduPlanVersion().id(), model.getId());
        for (UsmaImtsaImportLog log: logList)
        {
            blocks.add(log.getBlock());
            logMap.put(log.getBlock(), StringUtils.split(log.getMessage(), "\n"));
        }

        Collections.sort(blocks, EppEduPlanVersionBlock.COMPARATOR);

        model.setLogMap(logMap);
        model.setBlocks(blocks);
    }
}