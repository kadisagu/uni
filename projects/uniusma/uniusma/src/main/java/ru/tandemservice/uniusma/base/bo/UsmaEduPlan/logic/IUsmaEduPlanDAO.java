/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.util.UsmaEpvRowWrapper;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvCheckState;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public interface IUsmaEduPlanDAO extends ICommonDAO, INeedPersistenceSupport
{
    /**
     * Возвращает отображение семестров на часы нагрузки по типам.
     * @param rowId id строки
     * @return отображение семестров на часы нагрузки по типам
     */
    public Map<Term, Map<ICatalogItem, Double>> getEpvRowExtLoadDataMap(Long rowId);

    /**
     * Сохраняет нагрузку строки плана.
     * @param rowId id строки
     * @param dataMap отображение семестров на часы нагрузки по типам
     * @param errorCollector коллектор ошибок
     */
    public void updateEpvRowExtLoad(Long rowId, Map<Term, Map<ICatalogItem, Double>> dataMap, ErrorCollector errorCollector);

    /**
     * Возвращает справочник датасорсов дисциплин и мероприятий блоков УПв.
     * @param versionId id УПв
     * @return справочник датасорсов дисциплин и мероприятий блоков УПв
     */
    public Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<UsmaEpvRowWrapper>, StaticListDataSource<UsmaEpvRowWrapper>>> getEduPlanVersionBlockDataSourceMap(Long versionId);

    /**
     * Сохраняет оригинальный файл ИМЦА.
     * @param block блок УПв
     * @param source файл
     * @param fileName имя файла
     * @param encoding кодировка
     * @param number номер версии
     */
    public void saveImtsaXml(EppEduPlanVersionBlock block, IUploadFile source, String fileName, String encoding, int number);

    /**
     * Состояние проверки УПв. Возвращает новое, если не было.
     * @param versionId id УПв
     * @return состояние проверки УПв
     */
    public UsmaEpvCheckState getEpvCheckState(Long versionId);

    /**
     * Копирует учебный график из другой УПв.
     * @param source УПв, откуда копируется график
     * @param target УПв, куда копируется график
     * @param courses курсы, которые будут заменены в учебном графике; должен быть указан хотя бы один
     */
    void doCopyEduPlanVersionSchedule(@NotNull EppEduPlanVersion source, @NotNull EppEduPlanVersion target, @NotNull Collection<Course> courses);
}