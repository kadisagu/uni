package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Сообщения об ошибках импорта ИМЦА
 */
public class UsmaImtsaImportLog extends UsmaImtsaImportLogGen
{
    public UsmaImtsaImportLog()
    {

    }

    public UsmaImtsaImportLog(EppEduPlanVersionBlock block, String message)
    {
        this.setBlock(block);
        this.setMessage(message);
    }
}