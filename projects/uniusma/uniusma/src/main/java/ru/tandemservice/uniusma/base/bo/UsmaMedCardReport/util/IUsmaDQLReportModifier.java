/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.util;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 03.02.2015
 */
public interface IUsmaDQLReportModifier
{
    /**
     * Применяет фильтры блока к исходному DQL запросу. Базовый запрос строится для сущности {@link ru.tandemservice.uni.entity.employee.Student}
     *
     * @param alias псевдоним сущности
     * @param dql   основной запрос
     */
    public void modify(String alias, DQLSelectBuilder dql);

    /**
     * Добавляет строку использованными фильтрами.
     * Строка должна быть в формате CSV.<br/>
     * В качестве разделителя используется {@link ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.UsmaMedCardReportPrinter#CSV_SEPARATOR}<br/>
     * Строка всегда делится на две части по первому встреченному символу разделителя. Следующие символы разделителя игнорируются.<br/>
     * Первая часть - название фильтра; Вторая часть - значения фильтра<br/>
     * Пример:<br/>
     * Исходная строка:"Адрес;Дом-23;Кв.-47;"
     * Строки после разделения: "Адрес" "Дом-23;Кв.-47;"
     *
     * @param parameters строка для добавления применённых фильтров
     */
    public void addParameters(List<String> parameters);
}
