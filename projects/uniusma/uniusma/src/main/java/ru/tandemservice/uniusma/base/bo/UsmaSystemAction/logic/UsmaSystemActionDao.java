/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaSystemAction.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.dbsupport.ddl.connect.DBConnect;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;
import ru.tandemservice.uniusma.entity.UsmaImtsaImportLog;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.like;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public class UsmaSystemActionDao extends UniBaseDao implements IUsmaSystemActionDao
{
    private static final Logger _logger = Logger.getLogger(UsmaSystemActionDao.class);

    /**
     * Импортирует студентов трех факультетов
     */
    @Override
    public void saveStudents() throws Exception
    {
        if (getCount(Student.class) > 4000)
            throw new ApplicationException("Студенты уже импортированы.");

        importStudents(new File(ApplicationRuntime.getAppInstallPath(), "/data/edudata/zaoch.xml"));
    }

    /**
     * Импортирует студентов в группы из xml-файла
     * @param file файл с данными
     */
    @SuppressWarnings("unchecked")
    private void importStudents(File file) throws Exception
    {
        Session session = getSession();
        DBConnect tool = UniBaseUtils.createDDLTool(session);

        SAXReader reader = new SAXReader();
        Document document = reader.read(file);

        long russiaId = ((Number) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE)).setProjection(Projections.property(AddressCountry.P_ID)).uniqueResult()).longValue();
        AddressCountry russia = get(AddressCountry.class, russiaId);

        Sex male = getCatalogItem(Sex.class, SexCodes.MALE);
        Sex female = getCatalogItem(Sex.class, SexCodes.FEMALE);

        IdentityCardType passport = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT);
        IdentityCardType passportNotRF = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT_NOT_RF);

        CompensationType budget = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);

        StudentCategory studentCategory = getCatalogItem(StudentCategory.class, "1");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Pattern adressPattern = Pattern.compile(".*?г. (.*?), (.*?). (.*?), ([^-а-я/]*)([а-я]|/.?)-?(.*)");

        int importedCount = 0;
        List<Element> elements = document.getRootElement().elements();
        _logger.info("Start import students");
        for (Element element : elements)
        {
            String sFIO = getValue(element, "FIO");
            if (sFIO == null)
            {
                continue;
            }

            String groupTitle = getValue(element, "group");

            Criteria criteria = session.createCriteria(Group.class);
            criteria.add(Restrictions.eq(Group.P_TITLE, groupTitle.trim()));
            criteria.setFetchMode(Group.L_START_EDUCATION_YEAR, FetchMode.JOIN);
            Group group = (Group) criteria.uniqueResult();
            if (group == null)
            {
                _logger.error(String.format("Group '%s' not found for student '%s'", groupTitle, sFIO));
                continue;
            }

            sFIO = WordUtils.capitalizeFully(sFIO);
            String[] fio = sFIO.split(" ");
            if (fio.length == 1)
            {
                _logger.error("Student has only lastName: " + sFIO);
                continue;
            }

            // Удостоверение личности

            IdentityCard identityCard = new IdentityCard();

            identityCard.setLastName(fio[0]);
            identityCard.setFirstName(fio[1]);
            identityCard.setMiddleName(fio.length > 2 ? fio[2] : null);

            String birthDate = getValue(element, "birthDate");
            try
            {
                identityCard.setBirthDate(birthDate != null ? dateFormat.parse(birthDate) : null);
            }
            catch (Exception e)
            {
            }
            identityCard.setBirthPlace(getValue(element, "birthPlace"));

            String identityCardAdress = getValue(element, "identityCardAddress");
            if (identityCardAdress != null)
            {
                Matcher matcher = adressPattern.matcher(identityCardAdress);
                if (matcher.find())
                {
                    String settlement = matcher.group(1);
                    String street = matcher.group(3) + " " + matcher.group(2);
                    String home = matcher.group(4);
                    String part = "";
                    String flat = matcher.group(5);
                    Long addressId = AddressBaseManager.instance().dao().createAddress(russiaId, "Свердловская обл", "", settlement, "", street, home, part, flat);

                    if (addressId != null)
                    {
                        identityCard.setAddress(get(AddressBase.class, addressId));
                    }
                    else
                    {
                        _logger.info("Cannot create identityCardAddress for student: " + sFIO);
                    }
                }
            }

            String sex = getValue(element, "sex");
            identityCard.setSex((sex == null || sex.equals("М")) ? male : female);

            identityCard.setSeria(getValue(element, "seria"));
            identityCard.setNumber(getValue(element, "number"));

            String issuanceDate = getValue(element, "issuanceDate");
            try
            {
                identityCard.setIssuanceDate(issuanceDate != null ? dateFormat.parse(issuanceDate) : null);
            }
            catch (Exception e)
            {
            }
            identityCard.setIssuancePlace(getValue(element, "issuancePlace"));

            AddressCountry country = (AddressCountry) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_TITLE, getValue(element, "citizenship"))).uniqueResult();
            identityCard.setCitizenship(country != null ? country : russia);
            identityCard.setCardType((country == null || country.getCode() == 0) ? passport : passportNotRF);

            session.save(identityCard);

            // Персона

            Person person = new Person();
            person.setIdentityCard(identityCard);
            person.setContactData(new PersonContactData());

            String phone = getValue(element, "phone");
            person.getContactData().setPhoneMobile(phone);

            session.save(person.getContactData());

            session.save(person);

            identityCard.setPerson(person);
            session.update(identityCard);

            // Студент

            Student student = new Student();
            student.setGroup(group);
            student.setCourse(group.getCourse());
            student.setEducationOrgUnit(group.getEducationOrgUnit());
            student.setDevelopPeriodAuto(group.getEducationOrgUnit().getDevelopPeriod());
            student.setStudentCategory(studentCategory);

            int entranceYear = group.getStartEducationYear().getIntValue();
            student.setEntranceYear(entranceYear);
            student.setBookNumber(getValue(element, "bookNumber"));
            student.setStatus((StudentStatus) session.createCriteria(StudentStatus.class).add(Restrictions.eq(StudentStatus.P_TITLE, StringUtils.capitalize(getValue(element, "status")))).uniqueResult());

            String compensationType = getValue(element, "compensationType");
            student.setCompensationType((compensationType == null || compensationType.equals("0")) ? budget : contract);

            String specialPurposeRecruit = getValue(element, "specialPurposeRecruit");
            student.setTargetAdmission((specialPurposeRecruit == null || specialPurposeRecruit.equals("0")) ? false : true);
            student.setPerson(person);

            session.save(student);

            PersonManager.instance().dao().createPrincipalContext(student);

            importedCount++;
            if (importedCount % 100 == 0)
            {
                _logger.info("+100");
            }
        }

        _logger.info("Imported " + importedCount + " students from " + elements.size());
    }

    private String getValue(Element element, String attributeName)
    {
        return element.elementText(attributeName);
    }

    @Override
    public void doCreateEmpLabourContract() throws Exception
    {
        //поднимаем Сотрудников в Состоянии Актуальный и без ТД
        List<EmployeePost> employeePostList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column("ep")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePost.postStatus().actual().fromAlias("ep")), true))
                .where(DQLExpressions.notExists(new DQLSelectBuilder().fromEntity(EmployeeLabourContract.class, "ec").column("ec")
                        .where(DQLExpressions.eq(DQLExpressions.property(EmployeeLabourContract.employeePost().id().fromAlias("ec")), DQLExpressions.property(EmployeePost.id().fromAlias("ep"))))
                        .buildQuery()))
                .createStatement(getSession()).list();

        //поднимаем Типы ТД
        List<LabourContractType> contractTypeList = new DQLSelectBuilder().fromEntity(LabourContractType.class, "ct").column("ct")
                .order(DQLExpressions.property(LabourContractType.title().fromAlias("ct")), OrderDirection.asc)
                .createStatement(getSession()).list();

        //создаем ТД для Сотрудников
        for (EmployeePost employeePost : employeePostList)
        {
            //метка определяет с каким типом добавляем договор: срочный, бессрочный
            boolean second = employeePost.getPostType().getCode().equals(PostTypeCodes.INSTEAD_ABSENT_EMPLOYEE) || employeePost.getDismissalDate() != null;

            EmployeeLabourContract contract = new EmployeeLabourContract();
            contract.setEmployeePost(employeePost);
            contract.setNumber("000");
            contract.setDate(employeePost.getPostDate());
            contract.setBeginDate(employeePost.getPostDate());
            contract.setEndDate(employeePost.getDismissalDate());
            contract.setType(second ? contractTypeList.get(1) : contractTypeList.get(0));

            save(contract);
        }
    }

    @Override
    public List<UsmaImtsaXml> getXmlList(Collection<Long> blockIds)
    {
        List<UsmaImtsaXml> xmlList = blockIds == null ? getList(UsmaImtsaXml.class) : new DQLSelectBuilder().fromEntity(UsmaImtsaXml.class, "x").where(in(property("x", UsmaImtsaXml.block().id()), blockIds)).createStatement(getSession()).<UsmaImtsaXml>list();
        Collections.sort(xmlList, new Comparator<UsmaImtsaXml>()
        {
            @Override
            public int compare(UsmaImtsaXml o1, UsmaImtsaXml o2)
            {
                EppEduPlanVersionBlock block1 = o1.getBlock();
                EppEduPlanVersionBlock block2 = o2.getBlock();

                Long versionId1 = block1.getEduPlanVersion().getId();
                Long versionId2 = block2.getEduPlanVersion().getId();
                if (versionId1.equals(versionId2))
                {
                    if (block1.isRootBlock() || block2.isRootBlock())
                    {
                        return block1.isRootBlock() ? -1 : 1;
                    }

                    return block1.getId().compareTo(block2.getId());
                }

                return versionId1.compareTo(versionId2);
            }
        });

        return xmlList;
    }

    @Override
    public List<EppPlanStructure> getParts(boolean secondGosGeneration)
    {
        return getList(EppPlanStructure.class, EppPlanStructure.parent().code(), secondGosGeneration ? EppPlanStructureCodes.GOS_COMPONENTS : EppPlanStructureCodes.FGOS_PARTS);
    }

    @Override
    public List<UsmaImtsaCyclePlanStructureRel> getImtsaCycles(boolean secondGosGeneration)
    {
        return getList(UsmaImtsaCyclePlanStructureRel.class, UsmaImtsaCyclePlanStructureRel.gos2(), secondGosGeneration);
    }

    @Override
    public void saveOrRewriteImportLog(EppEduPlanVersionBlock block, String error)
    {
        UsmaImtsaImportLog log = get(UsmaImtsaImportLog.class, UsmaImtsaImportLog.block(), block);
        if (log != null)
        {
            delete(log);
            getSession().flush();
            getSession().clear();
        }

        log = new UsmaImtsaImportLog(block, error);
        save(log);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void changeStudentLogins()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Principal.class, "p").column(property("p"))
                .where(or(
                        like(property("p", Principal.login()), value(CoreStringUtils.escapeLike("'"))),
                        like(property("p", Principal.login()), value(".%")),
                        like(property("p", Principal.login()), value(" %")),
                        like(property("p", Principal.login()), value("% "))
                ));

        List<Principal> list = getList(builder);

        if (list.size() > 0)
        {
            BatchUtils.execute(list, DQL.MAX_VALUES_ROW_NUMBER, elements -> updateStudentLogins(elements));
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    private void updateStudentLogins(Collection<Principal> principalList)
    {

        for (Principal principal : principalList)
        {
            int postfix = 1;
            String newLogin = principal.getLogin().replaceAll("[^A-Za-z0-9.\\-_]", "").trim();
            if(newLogin.startsWith(".")) newLogin = newLogin.substring(1);
            String result = newLogin.trim();
            while (existsEntity(Principal.class, Principal.login().s(), result))
            {
                result = newLogin + postfix++;
            }
            principal.setLogin(result);
            update(principal);
            getSession().flush();
        }
    }
}