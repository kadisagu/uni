/* $Id$ */
package ru.tandemservice.uniusma.dao;

/**
 * @author Andrey Avetisov
 * @since 30.10.2014
 */
public class UniUsmaDaoFacade
{
    public static IUniUsmaDao getUniUsmaDAO()
    {
        return IUniUsmaDao.instance.get();
    }
}
