/* $Id: Model.java 31698 2013-12-19 09:05:40Z alopatin $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.group.GroupStudentListPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vnekrasov
 * @since 26.01.2014
 */
@State({
        @Bind(key = "groupId", binding = "group.id"),
        @Bind(key = "addBookNum", binding = "addBookNum"),
})
public class Model
{
    private Group _group = new Group();
    private List<Integer> _captainRowList = new ArrayList<>();
    private boolean _addBookNum;

    public boolean isAddBookNum() {
        return _addBookNum;
    }

    public void setAddBookNum(boolean addBookNum) {
        _addBookNum = addBookNum;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public List<Integer> getCaptainRowList()
    {
        return _captainRowList;
    }

    public void setCaptainRowList(List<Integer> captainRowList)
    {
        _captainRowList = captainRowList;
    }
}
