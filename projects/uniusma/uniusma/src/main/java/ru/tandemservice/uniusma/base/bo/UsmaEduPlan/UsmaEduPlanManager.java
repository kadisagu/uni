/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList.EppEduPlanHigherProfDSHandler;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList.EppEduPlanHigherProfList;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfList.EppEduPlanSecondaryProfDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic.IUsmaEduPlanDAO;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic.UsmaEduPlanDAO;
import ru.tandemservice.uniusma.eduplan.ext.EppEduPlan.EppEduPlanExtManager;
import ru.tandemservice.uniusma.eduplan.ext.EppEduPlan.UsmaEduPlanProfListAddon;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
@Configuration
public class UsmaEduPlanManager extends BusinessObjectManager
{
    public static UsmaEduPlanManager instance()
    {
        return instance(UsmaEduPlanManager.class);
    }

    @Bean
    public IUsmaEduPlanDAO dao()
    {
        return new UsmaEduPlanDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler higherEduPlanDSHandler()
    {
        return new EppEduPlanHigherProfDSHandler(this.getName())
        {
            @Override
            protected void filter(DQLSelectBuilder builder, String alias, ExecutionContext context)
            {
                super.filter(builder, alias, context);
                EppEduPlanExtManager.filterEduPlanListDSHandler(builder, alias, context);
            }
        };
    }

    @Bean
    public IDefaultSearchDataSourceHandler secondaryEduPlanDSHandler()
    {
        return new EppEduPlanSecondaryProfDSHandler(this.getName())
        {
            @Override
            protected void filter(DQLSelectBuilder builder, String alias, ExecutionContext context)
            {
                super.filter(builder, alias, context);
                EppEduPlanExtManager.filterEduPlanListDSHandler(builder, alias, context);
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler yesDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(this.getName())
            .addAll(Collections.singleton(UsmaEduPlanProfListAddon.OPTION_YES))
            .filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EducationOrgUnit.class, "ou")
                        .where(eq(property("ou", EducationOrgUnit.formativeOrgUnit()), property(alias)))
                        .buildQuery()));
            }
        }
            .filter(OrgUnit.fullTitle())
            .order(OrgUnit.fullTitle());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programSubject()), property(alias)))
                    .where(eq(property("p", EppEduPlanHigherProf.programKind()), commonValue(context.get(EppEduPlanHigherProfList.PARAM_PROGRAM_KIND))))
                    .buildQuery()));

                Collection<EduProgramSubjectIndex> subjectIndex = context.get("subjectIndex");
                if (subjectIndex != null && !subjectIndex.isEmpty())
                    dql.where(in(property(alias, EduProgramSubject.subjectIndex()), subjectIndex));
            }
        }
            .pageable(true)
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramKind.title());
    }
}