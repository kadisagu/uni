/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e15.AddEdit;

import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.EduEnrAsTransferStuExtractUsmaExtGen;

import java.util.Date;

/**
 * @author Denis Perminov
 * @since 27.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.Model model)
    {
        super.prepare(model);

	    EduEnrAsTransferStuExtract extract = model.getExtract();
	    extract.setCourseFrom("-");
	    extract.setFacultyFrom("-");
	    extract.setDevelopFormFrom("-");
	    extract.setCompensTypeFrom("-");
	    extract.setCompensTypeFrom("-");
	    extract.setApplyDate(new Date());

        Model m = (Model) model;
        // обращаемся к расширению выписки
        EduEnrAsTransferStuExtractUsmaExt extractExt = getByNaturalId(new EduEnrAsTransferStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            extractExt = new EduEnrAsTransferStuExtractUsmaExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setExtUsmaExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);
        saveOrUpdate(m.getExtUsmaExt());
    }
}
