package ru.tandemservice.nsiclient.datagram;

import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Корневой элемент датаграммы
 *
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="AcademicDegree" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AcademicDegreeType" minOccurs="0"/>
 *         &lt;element name="AcademicGroup" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AcademicGroupType" minOccurs="0"/>
 *         &lt;element name="AcademicStatus" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AcademicStatusType" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AddressType" minOccurs="0"/>
 *         &lt;element name="AddressLevel" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AddressLevelType" minOccurs="0"/>
 *         &lt;element name="AddressRU" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AddressRUType" minOccurs="0"/>
 *         &lt;element name="AddressType" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}AddressTypeType" minOccurs="0"/>
 *         &lt;element name="CompensationType" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CompensationTypeType" minOccurs="0"/>
 *         &lt;element name="ContactPerson" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContactPersonType" minOccurs="0"/>
 *         &lt;element name="ContractObject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContractObjectType" minOccurs="0"/>
 *         &lt;element name="ContractPayment" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContractPaymentType" minOccurs="0"/>
 *         &lt;element name="ContractVersion" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContractVersionType" minOccurs="0"/>
 *         &lt;element name="ContractVersionContractor" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContractVersionContractorType" minOccurs="0"/>
 *         &lt;element name="Course" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CourseType" minOccurs="0"/>
 *         &lt;element name="Department" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DepartmentType" minOccurs="0"/>
 *         &lt;element name="DevelopCondition" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DevelopConditionType" minOccurs="0"/>
 *         &lt;element name="DevelopForm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DevelopFormType" minOccurs="0"/>
 *         &lt;element name="DevelopPeriod" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DevelopPeriodType" minOccurs="0"/>
 *         &lt;element name="DevelopTech" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DevelopTechType" minOccurs="0"/>
 *         &lt;element name="EduCtrEducationPromise" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduCtrEducationPromiseType" minOccurs="0"/>
 *         &lt;element name="EduDocumentKind" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduDocumentKindType" minOccurs="0"/>
 *         &lt;element name="EduLevel" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduLevelType" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineInformation" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramDisciplineInformationType" minOccurs="0"/>
 *         &lt;element name="EduProgramDuration" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramDurationType" minOccurs="0"/>
 *         &lt;element name="EduProgramForm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramFormType" minOccurs="0"/>
 *         &lt;element name="EduProgramHigherProf" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramHigherProfType" minOccurs="0"/>
 *         &lt;element name="EduProgramQualification" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramQualificationType" minOccurs="0"/>
 *         &lt;element name="EduProgramSubject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramSubjectType" minOccurs="0"/>
 *         &lt;element name="EduProgramTrait" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramTraitType" minOccurs="0"/>
 *         &lt;element name="EduSpecializationBase" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduSpecializationBaseType" minOccurs="0"/>
 *         &lt;element name="EducationalProgram" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EducationalProgramType" minOccurs="0"/>
 *         &lt;element name="Employee" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EmployeeType" minOccurs="0"/>
 *         &lt;element name="EmployeeDepartmentHead" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EmployeeDepartmentHeadType" minOccurs="0"/>
 *         &lt;element name="EmployeeStatus" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EmployeeStatusType" minOccurs="0"/>
 *         &lt;element name="EmploymentType" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EmploymentTypeType" minOccurs="0"/>
 *         &lt;element name="FamilyStructure" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}FamilyStructureType" minOccurs="0"/>
 *         &lt;element name="ForeignLanguage" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ForeignLanguageType" minOccurs="0"/>
 *         &lt;element name="ForeignLanguageSkill" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ForeignLanguageSkillType" minOccurs="0"/>
 *         &lt;element name="Grade" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}GradeType" minOccurs="0"/>
 *         &lt;element name="GraduationHonour" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}GraduationHonourType" minOccurs="0"/>
 *         &lt;element name="Principal" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}PrincipalType" minOccurs="0"/>
 *         &lt;element name="Human" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
 *         &lt;element name="HumanAcademicDegree" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanAcademicDegreeType" minOccurs="0"/>
 *         &lt;element name="HumanAcademicStatus" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanAcademicStatusType" minOccurs="0"/>
 *         &lt;element name="HumanContactData" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanContactDataType" minOccurs="0"/>
 *         &lt;element name="IdentityCard" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}IdentityCardType" minOccurs="0"/>
 *         &lt;element name="IdentityCardKind" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}IdentityCardKindType" minOccurs="0"/>
 *         &lt;element name="Oksm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}OksmType" minOccurs="0"/>
 *         &lt;element name="Organization" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}OrganizationType" minOccurs="0"/>
 *         &lt;element name="PaymentPromice" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}PaymentPromiceType" minOccurs="0"/>
 *         &lt;element name="PersonEduDocument" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}PersonEduDocumentType" minOccurs="0"/>
 *         &lt;element name="PersonForeignLanguage" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}PersonForeignLanguageType" minOccurs="0"/>
 *         &lt;element name="Post" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}PostType" minOccurs="0"/>
 *         &lt;element name="Qualification" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}QualificationType" minOccurs="0"/>
 *         &lt;element name="RegistryDiscipline" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}RegistryDisciplineType" minOccurs="0"/>
 *         &lt;element name="RelDegree" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}RelDegreeType" minOccurs="0"/>
 *         &lt;element name="Student" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}StudentType" minOccurs="0"/>
 *         &lt;element name="StudentCategory" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}StudentCategoryType" minOccurs="0"/>
 *         &lt;element name="StudentStatus" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}StudentStatusType" minOccurs="0"/>
 *       &lt;/choice>
 *       &lt;anyAttribute/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
})
@XmlRootElement(name = "x-datagram")
public class XDatagram implements IXDatagram {

    @XmlElements({
            @XmlElement(name = "AcademicDegree", type = AcademicDegreeType.class),
            @XmlElement(name = "AcademicGroup", type = AcademicGroupType.class),
            @XmlElement(name = "AcademicStatus", type = AcademicStatusType.class),
            @XmlElement(name = "Address", type = AddressType.class),
            @XmlElement(name = "AddressLevel", type = AddressLevelType.class),
            @XmlElement(name = "AddressRU", type = AddressRUType.class),
            @XmlElement(name = "AddressString", type = AddressStringType.class),
            @XmlElement(name = "AddressType", type = AddressTypeType.class),
            @XmlElement(name = "CompensationType", type = CompensationTypeType.class),
            @XmlElement(name = "ContactPerson", type = ContactPersonType.class),
            @XmlElement(name = "ContractObject", type = ContractObjectType.class),
            @XmlElement(name = "ContractPayment", type = ContractPaymentType.class),
            @XmlElement(name = "ContractVersion", type = ContractVersionType.class),
            @XmlElement(name = "ContractVersionContractor", type = ContractVersionContractorType.class),
            @XmlElement(name = "Course", type = CourseType.class),
            @XmlElement(name = "Department", type = DepartmentType.class),
            @XmlElement(name = "DepartmentType", type = DepartmentTypeType.class),
            @XmlElement(name = "DevelopCondition", type = DevelopConditionType.class),
            @XmlElement(name = "DevelopForm", type = DevelopFormType.class),
            @XmlElement(name = "DevelopPeriod", type = DevelopPeriodType.class),
            @XmlElement(name = "DevelopTech", type = DevelopTechType.class),
            @XmlElement(name = "EduCtrEducationPromise", type = EduCtrEducationPromiseType.class),
            @XmlElement(name = "EduDocumentKind", type = EduDocumentKindType.class),
            @XmlElement(name = "EduLevel", type = EduLevelType.class),
            @XmlElement(name = "EduPlanVersion", type = EduPlanVersionType.class),
            @XmlElement(name = "EduPlanBlock", type = EduPlanBlockType.class),
            @XmlElement(name = "EduPlanRow", type = EduPlanRowType.class),
            @XmlElement(name = "EduPlanRowLoad", type = EduPlanRowLoadType.class),
            @XmlElement(name = "EduPlanRowTerm", type = EduPlanRowTermType.class),
            @XmlElement(name = "EduPlanRowTermAction", type = EduPlanRowTermActionType.class),
            @XmlElement(name = "EduPlanRowTermLoad", type = EduPlanRowTermLoadType.class),
            @XmlElement(name = "EduProgramDisciplineInformation", type = EduProgramDisciplineInformationType.class),
            @XmlElement(name = "EduProgramDuration", type = EduProgramDurationType.class),
            @XmlElement(name = "EduProgramForm", type = EduProgramFormType.class),
            @XmlElement(name = "EduProgramHigherProf", type = EduProgramHigherProfType.class),
            @XmlElement(name = "EduProgramQualification", type = EduProgramQualificationType.class),
            @XmlElement(name = "EduProgramSubject", type = EduProgramSubjectType.class),
            @XmlElement(name = "EduProgramTrait", type = EduProgramTraitType.class),
            @XmlElement(name = "EduSpecializationBase", type = EduSpecializationBaseType.class),
            @XmlElement(name = "EducationalProgram", type = EducationalProgramType.class),
            @XmlElement(name = "EducationYear", type = EducationYearType.class),
            @XmlElement(name = "Employee", type = EmployeeType.class),
            @XmlElement(name = "EmployeeDepartmentHead", type = EmployeeDepartmentHeadType.class),
            @XmlElement(name = "EmployeeStatus", type = EmployeeStatusType.class),
            @XmlElement(name = "EmploymentType", type = EmploymentTypeType.class),
            @XmlElement(name = "FamilyStructure", type = FamilyStructureType.class),
            @XmlElement(name = "ForeignLanguage", type = ForeignLanguageType.class),
            @XmlElement(name = "ForeignLanguageSkill", type = ForeignLanguageSkillType.class),
            @XmlElement(name = "Grade", type = GradeType.class),
            @XmlElement(name = "GraduationHonour", type = GraduationHonourType.class),
            @XmlElement(name = "Principal", type = PrincipalType.class),
            @XmlElement(name = "Human", type = HumanType.class),
            @XmlElement(name = "HumanAcademicDegree", type = HumanAcademicDegreeType.class),
            @XmlElement(name = "HumanAcademicStatus", type = HumanAcademicStatusType.class),
            @XmlElement(name = "HumanContactData", type = HumanContactDataType.class),
            @XmlElement(name = "HumanPhoto", type = HumanPhotoType.class),
            @XmlElement(name = "IdentityCard", type = IdentityCardType.class),
            @XmlElement(name = "IdentityCardKind", type = IdentityCardKindType.class),
            @XmlElement(name = "Oksm", type = OksmType.class),
            @XmlElement(name = "Organization", type = OrganizationType.class),
            @XmlElement(name = "PaymentPromice", type = PaymentPromiceType.class),
            @XmlElement(name = "PersonEduDocument", type = PersonEduDocumentType.class),
            @XmlElement(name = "PersonForeignLanguage", type = PersonForeignLanguageType.class),
            @XmlElement(name = "Post", type = PostType.class),
            @XmlElement(name = "Qualification", type = QualificationType.class),
            @XmlElement(name = "RegistryDiscipline", type = RegistryDisciplineType.class),
            @XmlElement(name = "RelDegree", type = RelDegreeType.class),
            @XmlElement(name = "Student", type = StudentType.class),
            @XmlElement(name = "Student2EduPlanBlock", type = Student2EduPlanBlockType.class),
            @XmlElement(name = "StudentCategory", type = StudentCategoryType.class),
            @XmlElement(name = "StudentStatus", type = StudentStatusType.class)
    })
    protected List<IDatagramObject> entityList;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the academicDegreeOrAcademicStatusOrAddress property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the academicDegreeOrAcademicStatusOrAddress property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcademicDegreeOrAcademicStatusOrAddress().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcademicDegreeType }
     * {@link AcademicGroupType }
     * {@link AcademicStatusType }
     * {@link AddressLevelType }
     * {@link AddressRUType }
     * {@link AddressStringType }
     * {@link AddressType }
     * {@link AddressTypeType }
     * {@link CompensationTypeType }
     * {@link ContactPersonType }
     * {@link ContractObjectType }
     * {@link ContractPaymentType }
     * {@link ContractVersionContractorType }
     * {@link ContractVersionType }
     * {@link CourseType }
     * {@link DepartmentType }
     * {@link DevelopConditionType }
     * {@link DevelopFormType }
     * {@link DevelopPeriodType }
     * {@link DevelopTechType }
     * {@link EduCtrEducationPromiseType }
     * {@link EduDocumentKindType }
     * {@link EduLevelType }
     * {@link EduProgramDisciplineInformationType }
     * {@link EduProgramDurationType }
     * {@link EduProgramFormType }
     * {@link EduProgramHigherProfType }
     * {@link EduProgramQualificationType }
     * {@link EduProgramSubjectType }
     * {@link EduProgramTraitType }
     * {@link EduSpecializationBaseType }
     * {@link EducationalProgramType }
     * {@link EmployeeDepartmentHeadType }
     * {@link EmployeeStatusType }
     * {@link EmployeeType }
     * {@link EmploymentTypeType }
     * {@link FamilyStructureType }
     * {@link ForeignLanguageSkillType }
     * {@link ForeignLanguageType }
     * {@link GradeType }
     * {@link GraduationHonourType }
     * {@link PrincipalType }
     * {@link HumanAcademicDegreeType }
     * {@link HumanAcademicStatusType }
     * {@link HumanContactDataType }
     * {@link HumanType }
     * {@link IdentityCardKindType }
     * {@link IdentityCardType }
     * {@link OksmType }
     * {@link OrganizationType }
     * {@link PaymentPromiceType }
     * {@link PersonEduDocumentType }
     * {@link PersonForeignLanguageType }
     * {@link PostType }
     * {@link QualificationType }
     * {@link RegistryDisciplineType }
     * {@link RelDegreeType }
     * {@link StudentCategoryType }
     * {@link StudentStatusType }
     * {@link StudentType }
     *
     *
     */
    public List<IDatagramObject> getEntityList() {
        if (entityList == null) {
            entityList = new ArrayList<IDatagramObject>();
        }
        return this.entityList;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     *
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     *
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     *
     *
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
