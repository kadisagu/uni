/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockTitleEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaEpvBlockTitle;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final Map<Long, String> LEVEL_CODES = new HashMap<>();
    static
    {
        LEVEL_CODES.put(1L, "B");
        LEVEL_CODES.put(2L, "M");
        LEVEL_CODES.put(3L, "S");
    }

    @Override
    public void prepare(Model model)
    {
        EppEduPlanVersionBlock block = get(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.id(), model.getId());
        UsmaEpvBlockTitle blockTitle = get(UsmaEpvBlockTitle.class, UsmaEpvBlockTitle.block(), block);
        if (blockTitle == null)
        {
            blockTitle = new UsmaEpvBlockTitle();
            blockTitle.setBlock(block);
        }

        model.setBlockTitle(blockTitle);

        Long defaultKey = 1L;

        Map<Long, IdentifiableWrapper> levels = new LinkedHashMap<>();
        levels.put(1L, new IdentifiableWrapper(1L, "Бакалавриат"));
        levels.put(2L, new IdentifiableWrapper(2L, "Магистратура"));
        levels.put(3L, new IdentifiableWrapper(3L, "Специалитет"));
        model.setLevels(levels.values());
        if (blockTitle.getLevelCode() !=null)
        {
            Long key = defaultKey;
            for (Map.Entry<Long, String> entry: LEVEL_CODES.entrySet())
            {
                if (entry.getValue().equals(blockTitle.getLevelCode()))
                {
                    key = entry.getKey();
                }
            }

            model.setLevel(levels.get(key));
        }

        Map<Long, IdentifiableWrapper> selfWork = new LinkedHashMap<>();
        selfWork.put(1L, new IdentifiableWrapper(1L, "КСР"));
        selfWork.put(2L, new IdentifiableWrapper(2L, "ИЗ"));
        model.setSelfWorks(selfWork.values());
        if (blockTitle.getKsrOrIndividualLessons() != null)
        {
            Long key = defaultKey;
            for (Map.Entry<Long, IdentifiableWrapper> entry: selfWork.entrySet())
            {
                if (entry.getValue().getTitle().equals(blockTitle.getKsrOrIndividualLessons()))
                {
                    key = entry.getKey();
                }
            }

            model.setSelfWork(selfWork.get(key));
        }
    }

    @Override
    public void update(Model model)
    {
        UsmaEpvBlockTitle blockTitle = model.getBlockTitle();
        String selfWorkTitle = model.getSelfWork()!=null?model.getSelfWork().getTitle():"";
        String levelCodeTitle = model.getLevel()!=null?LEVEL_CODES.get(model.getLevel().getId()):"";
        blockTitle.setLevelCode(levelCodeTitle);
        blockTitle.setKsrOrIndividualLessons(selfWorkTitle);

        saveOrUpdate(blockTitle);
    }
}