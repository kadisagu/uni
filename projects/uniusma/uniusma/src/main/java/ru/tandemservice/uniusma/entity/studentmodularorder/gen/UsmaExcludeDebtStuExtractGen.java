package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении за неуплату
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaExcludeDebtStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract";
    public static final String ENTITY_NAME = "usmaExcludeDebtStuExtract";
    public static final int VERSION_HASH = 880361465;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_PAYING_DATE = "stopPayingDate";
    public static final String P_GIVE_DIPLOMA = "giveDiploma";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_DEBT_DATE = "debtDate";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";

    private Date _excludeDate;     // Дата отчисления
    private boolean _stopGrantsPaying;     // Отменить выплату стипендии
    private Date _stopPayingDate;     // Дата прекращения выплаты стипендии
    private boolean _giveDiploma;     // Выдается диплом о незаконченном высшем образовании
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private Date _debtDate;     // Задолженность по оплате с
    private String _contractNumber;     // Номер договора об оказании платных образовательных услуг
    private Date _contractDate;     // Дата договора об оказании платных образовательных услуг

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Отменить выплату стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     */
    public Date getStopPayingDate()
    {
        return _stopPayingDate;
    }

    /**
     * @param stopPayingDate Дата прекращения выплаты стипендии.
     */
    public void setStopPayingDate(Date stopPayingDate)
    {
        dirty(_stopPayingDate, stopPayingDate);
        _stopPayingDate = stopPayingDate;
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isGiveDiploma()
    {
        return _giveDiploma;
    }

    /**
     * @param giveDiploma Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    public void setGiveDiploma(boolean giveDiploma)
    {
        dirty(_giveDiploma, giveDiploma);
        _giveDiploma = giveDiploma;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Задолженность по оплате с. Свойство не может быть null.
     */
    @NotNull
    public Date getDebtDate()
    {
        return _debtDate;
    }

    /**
     * @param debtDate Задолженность по оплате с. Свойство не может быть null.
     */
    public void setDebtDate(Date debtDate)
    {
        dirty(_debtDate, debtDate);
        _debtDate = debtDate;
    }

    /**
     * @return Номер договора об оказании платных образовательных услуг. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора об оказании платных образовательных услуг. Свойство не может быть null.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата договора об оказании платных образовательных услуг. Свойство не может быть null.
     */
    @NotNull
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата договора об оказании платных образовательных услуг. Свойство не может быть null.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsmaExcludeDebtStuExtractGen)
        {
            setExcludeDate(((UsmaExcludeDebtStuExtract)another).getExcludeDate());
            setStopGrantsPaying(((UsmaExcludeDebtStuExtract)another).isStopGrantsPaying());
            setStopPayingDate(((UsmaExcludeDebtStuExtract)another).getStopPayingDate());
            setGiveDiploma(((UsmaExcludeDebtStuExtract)another).isGiveDiploma());
            setStudentStatusOld(((UsmaExcludeDebtStuExtract)another).getStudentStatusOld());
            setDebtDate(((UsmaExcludeDebtStuExtract)another).getDebtDate());
            setContractNumber(((UsmaExcludeDebtStuExtract)another).getContractNumber());
            setContractDate(((UsmaExcludeDebtStuExtract)another).getContractDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaExcludeDebtStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaExcludeDebtStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsmaExcludeDebtStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return obj.getExcludeDate();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopPayingDate":
                    return obj.getStopPayingDate();
                case "giveDiploma":
                    return obj.isGiveDiploma();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "debtDate":
                    return obj.getDebtDate();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopPayingDate":
                    obj.setStopPayingDate((Date) value);
                    return;
                case "giveDiploma":
                    obj.setGiveDiploma((Boolean) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "debtDate":
                    obj.setDebtDate((Date) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopPayingDate":
                        return true;
                case "giveDiploma":
                        return true;
                case "studentStatusOld":
                        return true;
                case "debtDate":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopPayingDate":
                    return true;
                case "giveDiploma":
                    return true;
                case "studentStatusOld":
                    return true;
                case "debtDate":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return Date.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopPayingDate":
                    return Date.class;
                case "giveDiploma":
                    return Boolean.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "debtDate":
                    return Date.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaExcludeDebtStuExtract> _dslPath = new Path<UsmaExcludeDebtStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaExcludeDebtStuExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getStopPayingDate()
     */
    public static PropertyPath<Date> stopPayingDate()
    {
        return _dslPath.stopPayingDate();
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#isGiveDiploma()
     */
    public static PropertyPath<Boolean> giveDiploma()
    {
        return _dslPath.giveDiploma();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Задолженность по оплате с. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getDebtDate()
     */
    public static PropertyPath<Date> debtDate()
    {
        return _dslPath.debtDate();
    }

    /**
     * @return Номер договора об оказании платных образовательных услуг. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата договора об оказании платных образовательных услуг. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    public static class Path<E extends UsmaExcludeDebtStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopPayingDate;
        private PropertyPath<Boolean> _giveDiploma;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _debtDate;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(UsmaExcludeDebtStuExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(UsmaExcludeDebtStuExtractGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getStopPayingDate()
     */
        public PropertyPath<Date> stopPayingDate()
        {
            if(_stopPayingDate == null )
                _stopPayingDate = new PropertyPath<Date>(UsmaExcludeDebtStuExtractGen.P_STOP_PAYING_DATE, this);
            return _stopPayingDate;
        }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#isGiveDiploma()
     */
        public PropertyPath<Boolean> giveDiploma()
        {
            if(_giveDiploma == null )
                _giveDiploma = new PropertyPath<Boolean>(UsmaExcludeDebtStuExtractGen.P_GIVE_DIPLOMA, this);
            return _giveDiploma;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Задолженность по оплате с. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getDebtDate()
     */
        public PropertyPath<Date> debtDate()
        {
            if(_debtDate == null )
                _debtDate = new PropertyPath<Date>(UsmaExcludeDebtStuExtractGen.P_DEBT_DATE, this);
            return _debtDate;
        }

    /**
     * @return Номер договора об оказании платных образовательных услуг. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(UsmaExcludeDebtStuExtractGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата договора об оказании платных образовательных услуг. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(UsmaExcludeDebtStuExtractGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

        public Class getEntityClass()
        {
            return UsmaExcludeDebtStuExtract.class;
        }

        public String getEntityName()
        {
            return "usmaExcludeDebtStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
