/* $Id:$ */
package ru.tandemservice.uniusma.report.bo.UsmaEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniusma.report.bo.UsmaEnrReport.ui.EntrantStudentCardAdd.UsmaEnrReportEntrantStudentCardAddUI;

/**
 * @author Denis Perminov
 * @since 27.08.2014
 */
public interface IUsmaEnrReportDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Long createReportEntrantStudentCard(UsmaEnrReportEntrantStudentCardAddUI model);
}
