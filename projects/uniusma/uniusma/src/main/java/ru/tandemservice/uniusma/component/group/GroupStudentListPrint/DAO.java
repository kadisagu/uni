package ru.tandemservice.uniusma.component.group.GroupStudentListPrint;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author oleyba
 * @since 24.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
   public void prepare(Model model)
   {
       Group group = get(Group.class, model.getGroup().getId());
       model.setGroup(group);
   }

       @SuppressWarnings("unchecked")
       public String[][] getStudentData(Model model, boolean addBookNumber)
               {
                   Criteria criteria = getCriteria(model);
                   List<Student> studentList = criteria.list();
                   String[][] studentData = new String[studentList.size()][];
                   List<Student> captainStudentList = UniDaoFacade.getGroupDao().getCaptainStudentList(model.getGroup());
                   int i = 0;
                   for (Student student : studentList)
                   {
                       String number = Integer.toString(i + 1);
                       String fio = student.getPerson().getFullFio();
                       IdentityCard identityCard = student.getPerson().getIdentityCard();

                       String sex = identityCard.getSex().getShortTitle();
                       String compensation = (student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET)) ? "Б" : "К";
                       String status = student.getStatus().getTitle();
                       String birthDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate());

                       String passport = getIdentityCardTitle(identityCard);
                       AddressBase address = identityCard.getAddress();
                       AddressBase factAddress = student.getPerson().getAddress();

                       String addressTitle = address == null ? "" : address.getTitleWithFlat();

                       String factAddressTitle = "";

                       if (null != factAddress)
                           factAddressTitle = address != null && address.getTitleWithFlat().equals(factAddress.getTitleWithFlat()) ? "" : factAddress.getTitleWithFlat();

                       if (captainStudentList.contains(student))  model.getCaptainRowList().add(i);

                       if (addBookNumber)
                       {
                           String bookNum = student.getBookNumber();
                           studentData[i++] = new String[] {number, fio, bookNum, sex, compensation, status, birthDate, passport, addressTitle, factAddressTitle};
                       }
                       else
                           studentData[i++] = new String[] {number, fio, sex, compensation, status, birthDate, passport, addressTitle, factAddressTitle};
                   }
                   return studentData;
               }


       protected Criteria getCriteria(Model model)
       {
           Criteria criteria = getSession().createCriteria(Student.class);
           criteria.createAlias(Student.L_STATUS, "status");
           criteria.createAlias(Student.L_PERSON, "person");
           criteria.createAlias("person." + Person.L_IDENTITY_CARD, "identityCard");
           criteria.add(Restrictions.eq(Student.L_GROUP, model.getGroup()));
           criteria.add(Restrictions.eq("status." + StudentStatus.P_ACTIVE, true));
           criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));
           criteria.addOrder(Order.asc("status." + StudentStatus.P_PRIORITY));
           criteria.addOrder(Order.asc("identityCard." + IdentityCard.P_LAST_NAME));
           criteria.addOrder(Order.asc("identityCard." + IdentityCard.P_FIRST_NAME));
           criteria.addOrder(Order.asc("identityCard." + IdentityCard.P_MIDDLE_NAME));
           return criteria;
       }

       public String getIdentityCardTitle(IdentityCard identityCard)
       {
           StringBuilder str = new StringBuilder();
           if (identityCard.getCardType() != null)
           {
               str.append(identityCard.getCardType().getShortTitle()).append(": ");
           }
           if (identityCard.getSeria() != null)
           {
               str.append(identityCard.getSeria()).append(" ");
           }
           if (identityCard.getNumber() != null)
           {
               str.append(identityCard.getNumber()).append(" ");
           }
           if (StringUtils.isNotEmpty(identityCard.getIssuancePlace()))
           {
               str.append("Выдан: ").append(identityCard.getIssuancePlace()).append(" ");
           }
           if (identityCard.getIssuanceDate() != null)
           {
               str.append("Дата выдачи: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()));
           }
           return str.toString();
       }
   }


