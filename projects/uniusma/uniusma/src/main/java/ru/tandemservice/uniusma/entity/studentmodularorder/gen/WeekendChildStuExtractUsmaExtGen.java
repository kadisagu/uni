package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.WeekendChildStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком до достижения им возраста 3-х лет
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendChildStuExtractUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<WeekendChildStuExtractUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt";
    public static final String ENTITY_NAME = "weekendChildStuExtractUsmaExt";
    public static final int VERSION_HASH = 1557488709;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_FREE_ATTENDANCE = "freeAttendance";

    private WeekendChildStuExtract _extractExt;     // Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком
    private boolean _freeAttendance = false;     // Разрешить свободное посещение занятий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public WeekendChildStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(WeekendChildStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Разрешить свободное посещение занятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreeAttendance()
    {
        return _freeAttendance;
    }

    /**
     * @param freeAttendance Разрешить свободное посещение занятий. Свойство не может быть null.
     */
    public void setFreeAttendance(boolean freeAttendance)
    {
        dirty(_freeAttendance, freeAttendance);
        _freeAttendance = freeAttendance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof WeekendChildStuExtractUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((WeekendChildStuExtractUsmaExt)another).getExtractExt());
            }
            setFreeAttendance(((WeekendChildStuExtractUsmaExt)another).isFreeAttendance());
        }
    }

    public INaturalId<WeekendChildStuExtractUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<WeekendChildStuExtractUsmaExtGen>
    {
        private static final String PROXY_NAME = "WeekendChildStuExtractUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(WeekendChildStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof WeekendChildStuExtractUsmaExtGen.NaturalId) ) return false;

            WeekendChildStuExtractUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendChildStuExtractUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendChildStuExtractUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new WeekendChildStuExtractUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "freeAttendance":
                    return obj.isFreeAttendance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((WeekendChildStuExtract) value);
                    return;
                case "freeAttendance":
                    obj.setFreeAttendance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "freeAttendance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "freeAttendance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return WeekendChildStuExtract.class;
                case "freeAttendance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendChildStuExtractUsmaExt> _dslPath = new Path<WeekendChildStuExtractUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendChildStuExtractUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt#getExtractExt()
     */
    public static WeekendChildStuExtract.Path<WeekendChildStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Разрешить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt#isFreeAttendance()
     */
    public static PropertyPath<Boolean> freeAttendance()
    {
        return _dslPath.freeAttendance();
    }

    public static class Path<E extends WeekendChildStuExtractUsmaExt> extends EntityPath<E>
    {
        private WeekendChildStuExtract.Path<WeekendChildStuExtract> _extractExt;
        private PropertyPath<Boolean> _freeAttendance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt#getExtractExt()
     */
        public WeekendChildStuExtract.Path<WeekendChildStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new WeekendChildStuExtract.Path<WeekendChildStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Разрешить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt#isFreeAttendance()
     */
        public PropertyPath<Boolean> freeAttendance()
        {
            if(_freeAttendance == null )
                _freeAttendance = new PropertyPath<Boolean>(WeekendChildStuExtractUsmaExtGen.P_FREE_ATTENDANCE, this);
            return _freeAttendance;
        }

        public Class getEntityClass()
        {
            return WeekendChildStuExtractUsmaExt.class;
        }

        public String getEntityName()
        {
            return "weekendChildStuExtractUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
