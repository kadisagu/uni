/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e17.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt;

/**
 * @author Denis Perminov
 * @since 20.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e17.Pub.Model
{
    RestorationStuExtractExtUsmaExt _extUsmaExt;

    public RestorationStuExtractExtUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(RestorationStuExtractExtUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
