/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<UsmaExcludeEduPlanStuExtract, Model>
{
}
