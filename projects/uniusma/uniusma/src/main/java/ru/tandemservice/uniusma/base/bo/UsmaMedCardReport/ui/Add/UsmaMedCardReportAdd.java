/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressStreet;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address.FactAddressParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address.RegAddressParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.medCard.MedCardParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.personalData.PersonalDataParam;
import ru.tandemservice.uniusma.entity.catalog.UsmaDiseasesKind;
import ru.tandemservice.uniusma.entity.catalog.UsmaHealthGroup;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author DMITRY KNYAZEV
 * @since 26.06.2014
 */
@Configuration
public class UsmaMedCardReportAdd extends BusinessComponentManager
{
    //BLOCKS
    public static final String USMA_MED_CARD_REPORT_BLOCK_LIST = "usmaMedCardReportBlockList";
    public static final String MED_CARD = "medCard";
    public static final String PERSONAL_DATA = "personData";
    public static final String FACT_ADDRESS = "factAddress";
    public static final String REG_ADDRESS = "regAddress";
    //PARAMETERS
    public static final String PARAM_ORG_UNIT_ID = "orgUnit";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(PersonalDataParam.GROUP_DS, groupDSHandler()))
                .addDataSource(selectDS(PersonalDataParam.STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
                .addDataSource(selectDS(PersonalDataParam.STUDENT_STATUS_DS, studentStatusDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(selectDS(PersonalDataParam.SEX_DS, sexDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(FactAddressParam.FACT_COUNTRY_DS, countryDS()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(FactAddressParam.FACT_SETTLEMENT_DS, getName(), AddressItem.settlementComboDSHandler(getName())))
                .addDataSource(selectDS(FactAddressParam.FACT_STREET_DS, streetDS()).addColumn(AddressStreet.titleWithType().s()))
                .addDataSource(selectDS(RegAddressParam.REG_COUNTRY_DS, countryDS()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(RegAddressParam.REG_SETTLEMENT_DS, getName(), AddressItem.settlementComboDSHandler(getName())))
                .addDataSource(selectDS(RegAddressParam.REG_STREET_DS, streetDS()).addColumn(AddressStreet.titleWithType().s()))

                .addDataSource(selectDS(MedCardParam.VACCINE_KIND_DS, vaccineKindDSHandler()))
                .addDataSource(selectDS(MedCardParam.DISEASES_KIND_DS, diseasesKindDSHandler()))
                .addDataSource(selectDS(MedCardParam.HEALTH_GROUP_DS, healthGroupDSHandler()))
                .create();
    }

    @Bean
    public BlockListExtPoint usmaMedCardReportBlockListExtPoint()
    {
        return blockListExtPointBuilder(USMA_MED_CARD_REPORT_BLOCK_LIST)
                .addBlock(htmlBlock(MED_CARD, "block/medCard/medCard"))
                .addBlock(htmlBlock(PERSONAL_DATA, "block/personalData/personalData"))
                .addBlock(htmlBlock(FACT_ADDRESS, "block/address/factAddress"))
                .addBlock(htmlBlock(REG_ADDRESS, "block/address/regAddress"))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler countryDS()
    {
        return new DefaultComboDataSourceHandler(getName(), AddressCountry.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler streetDS()
    {
        return AddressBaseManager.instance().streetComboDSHandler();
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
                .where(Group.parent().id(), PARAM_ORG_UNIT_ID)
                .filter(Group.title())
                .order(Group.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StudentCategory.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler studentStatusDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StudentStatus.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler sexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Sex.class)
                .filter(Sex.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler diseasesKindDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UsmaDiseasesKind.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler vaccineKindDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UsmaVaccineKind.class)
                .filter(UsmaVaccineKind.title())
                .order(UsmaVaccineKind.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> healthGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UsmaHealthGroup.class)
                .filter(UsmaHealthGroup.title())
                .order(UsmaHealthGroup.title());
    }
}
