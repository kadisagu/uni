/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduplanVersionImtsaImportLogTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 05.09.2013
 */
public interface IDAO extends IUniDao<Model>
{
}