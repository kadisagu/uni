\fs28\qc\b{orderType}\b0\par
\par\fi709\qj\keep\keepn
В соответствии с п. 12 ч. 1 ст. 34 Федерального закона от 29.12.2012 г. № 273-ФЗ «Об образовании в РФ», Порядком и основаниями предоставления академического отпуска обучающимся (утв. Приказом Министерства образования и науки Российской Федерации от 13.06.2013 г. № 455),\par\par
\expnd8\expndtw40\b приказываю: \expndtw0\expnd0\b0\par\par
1. Предоставить {student_D} {fio} {birthDate} года рождения, группы {group}, {course} курса {orgUnit_G}, {developForm_GF} формы обучения {compensationTypeStr_G} академический отпуск ({reason}) с {beginDate} г. по {endDate} г.\par
{stopGrantPayingStr}\par
{assignPaymentStr}\par\par
Основание: {listBasics}