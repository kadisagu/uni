/* $Id$ */
package ru.tandemservice.uniusma.component.catalog.usmaVaccineKind.UsmaVaccineKindAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<UsmaVaccineKind, Model>
{
}
