package ru.tandemservice.uniusma.entity.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.entity.catalog.UsmaDiseasesKind;
import ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сведения о заболеваниях
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaStudentDiseaseInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo";
    public static final String ENTITY_NAME = "usmaStudentDiseaseInfo";
    public static final int VERSION_HASH = 1313122195;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_DISEASES_KIND = "diseasesKind";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";

    private Student _student;     // Студент
    private UsmaDiseasesKind _diseasesKind;     // Наименование заболеваний
    private Date _beginDate;     // Дата начала заболевания
    private Date _endDate;     // Дата окончания заболевания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Наименование заболеваний. Свойство не может быть null.
     */
    @NotNull
    public UsmaDiseasesKind getDiseasesKind()
    {
        return _diseasesKind;
    }

    /**
     * @param diseasesKind Наименование заболеваний. Свойство не может быть null.
     */
    public void setDiseasesKind(UsmaDiseasesKind diseasesKind)
    {
        dirty(_diseasesKind, diseasesKind);
        _diseasesKind = diseasesKind;
    }

    /**
     * @return Дата начала заболевания.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала заболевания.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания заболевания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания заболевания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaStudentDiseaseInfoGen)
        {
            setStudent(((UsmaStudentDiseaseInfo)another).getStudent());
            setDiseasesKind(((UsmaStudentDiseaseInfo)another).getDiseasesKind());
            setBeginDate(((UsmaStudentDiseaseInfo)another).getBeginDate());
            setEndDate(((UsmaStudentDiseaseInfo)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaStudentDiseaseInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaStudentDiseaseInfo.class;
        }

        public T newInstance()
        {
            return (T) new UsmaStudentDiseaseInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "diseasesKind":
                    return obj.getDiseasesKind();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "diseasesKind":
                    obj.setDiseasesKind((UsmaDiseasesKind) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "diseasesKind":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "diseasesKind":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "diseasesKind":
                    return UsmaDiseasesKind.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaStudentDiseaseInfo> _dslPath = new Path<UsmaStudentDiseaseInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaStudentDiseaseInfo");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Наименование заболеваний. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getDiseasesKind()
     */
    public static UsmaDiseasesKind.Path<UsmaDiseasesKind> diseasesKind()
    {
        return _dslPath.diseasesKind();
    }

    /**
     * @return Дата начала заболевания.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания заболевания.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends UsmaStudentDiseaseInfo> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private UsmaDiseasesKind.Path<UsmaDiseasesKind> _diseasesKind;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Наименование заболеваний. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getDiseasesKind()
     */
        public UsmaDiseasesKind.Path<UsmaDiseasesKind> diseasesKind()
        {
            if(_diseasesKind == null )
                _diseasesKind = new UsmaDiseasesKind.Path<UsmaDiseasesKind>(L_DISEASES_KIND, this);
            return _diseasesKind;
        }

    /**
     * @return Дата начала заболевания.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsmaStudentDiseaseInfoGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания заболевания.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsmaStudentDiseaseInfoGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return UsmaStudentDiseaseInfo.class;
        }

        public String getEntityName()
        {
            return "usmaStudentDiseaseInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
