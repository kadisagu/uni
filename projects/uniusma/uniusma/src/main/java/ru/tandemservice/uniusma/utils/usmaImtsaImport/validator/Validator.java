/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.validator;

import java.util.Arrays;
import java.util.List;

/**
 * Валидатор. Проверяет, что значение соответствует некоторым требованиям.
 * @author Alexander Zhebko
 * @since 18.07.2013
 */
public abstract class Validator<T>
{
    public static final Validator<Integer> INTEGER_GE_ZERO = new GEIntegerValidator(0);
    public static final Validator<Integer> INTEGER_GE_ONE = new GEIntegerValidator(1);
    public static final Validator<Double> DOUBLE_GE_ZERO = new GEDoubleValidator(0.0d);
    public static final List<Validator<?>> DOUBLE_PERCENT = Arrays.<Validator<?>>asList(DOUBLE_GE_ZERO, new LEDoubleValidator(100.0d));
    public static final Validator<String> UNI_MAX_LENGTH_VALIDATOR = new StringLengthValidator(4000);

    /**
     * Проверяет значение на соотвествие заданным требованиям.
     * @param value значение, которое нужно проверить
     * @return true, если значение соответствует требованиям, false если нет
     * @throws ClassCastException если объект для проверки неправильного типа
     */
    public boolean validate(Object value) throws ClassCastException
    {
        @SuppressWarnings("unchecked")
        T castValue = (T) value;
        return validateValue(castValue);
    }

    /**
     * Текст несоответствия требованиям.
     * @return текст несооттветствия требованиям
     */
    public abstract String getInvalidMessage();

    /**
     * Проверяет значения данного типа на соответствие требованиям.
     * @param value значение, которое нужно проверить
     * @return true, если значение соответствует требованиям, false если нет
     */
    protected abstract boolean validateValue(T value);
}