/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e5.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.TransferDevFormStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e5.Pub.Model
{
    TransferDevFormStuExtractUsmaExt _extUsmaExt;
    private String _debtsList;

    public TransferDevFormStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(TransferDevFormStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }

    public String getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(String debtsList)
    {
        _debtsList = debtsList;
    }
}
