/*$Id:$*/
package ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 17.03.2016
 */
@Configuration
public class UsmaSessionProtocolGiaReportPub extends BusinessComponentManager
{

    public static final String COURSE_DS = "courseDS";
    public static final String GROUP_DS = "groupDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class)
                .filter(Course.title())
                .order(Course.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
                .customize((alias, dql, context, filter) -> {
                    final Long orgUnitId = context.get(UsmaSessionProtocolGiaReportPubUI.ORG_UNIT_ID);
                    if (orgUnitId != null)
                        dql.where(eq(property(Group.parent().id().fromAlias(alias)), value(orgUnitId)));

                    FilterUtils.applySelectFilter(dql, alias, Group.course().s(), context.get(UsmaSessionProtocolGiaReportPubUI.PARAM_COURSE));

                    return dql.where(eq(property(alias, Group.archival()), value(false)));
                })
                .filter(Group.title())
                .order(Group.title());
    }

}
