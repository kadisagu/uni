/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleUtils;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionPartitionType;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeek;
import ru.tandemservice.uniusma.entity.UsmaEduPlanVersionWeekPart;
import ru.tandemservice.uniusma.entity.catalog.codes.UsmaSchedulePartitionTypeCodes;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaWeekTypeBlockColumn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public abstract class UsmaEduPlanVersionScheduleUtils extends EppEduPlanVersionScheduleUtils
{
    private final List<UsmaEduPlanVersionWeek> usmaWeekList = UniDaoFacade.getCoreDao().getList(UsmaEduPlanVersionWeek.class, UsmaEduPlanVersionWeek.version(), getEduPlanVersion());
    public List<UsmaEduPlanVersionWeek> getUsmaWeekList(){ return usmaWeekList;}

    private final UsmaRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> usmaRangeSelectionListDataSource = buildUsmaRangeSelectionListDataSource();
    public UsmaRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> getUsmaRangeSelectionWeekTypeListDataSource()
    {
        return usmaRangeSelectionListDataSource;
    }


    protected UsmaRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> buildUsmaRangeSelectionListDataSource()
    {
        final StaticListDataSource<ViewWrapper<Course>> scheduleDataSource = this.buildUsmaScheduleDataSource();
        final UsmaRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleRangeModel = new UsmaRangeSelectionWeekTypeListDataSource<>(scheduleDataSource);

        Map<Long, Map<Long, Map<Integer, EppWeekType>>> fullDataMap = SafeMap.get(HashMap.class);
        scheduleRangeModel.setFullDataMap(fullDataMap);

        final Map<Long, int[]> row2points = new HashMap<>();
        final Map<Long, int[]> row2data = new HashMap<>();
        scheduleRangeModel.setRow2points(row2points);
        scheduleRangeModel.setRow2data(row2data);

        this.prepareRowDataPoints(row2points, row2data);

        final Map<Long, int[]> row2ranges = new HashMap<>();
        for (final Map.Entry<Long, int[]> entry : row2points.entrySet())
        {
            row2ranges.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, entry.getValue()).getRanges());
        }

        Map<UsmaEduPlanVersionWeek, Map<Integer, EppWeekType>> weekPartTypes = SafeMap.get(HashMap.class);
        List<UsmaEduPlanVersionWeekPart> weekPartList = UniDaoFacade.getCoreDao().getList(UsmaEduPlanVersionWeekPart.class, UsmaEduPlanVersionWeekPart.eduPlanVersionWeek(), usmaWeekList);
        for (UsmaEduPlanVersionWeekPart weekPart : weekPartList)
        {
            weekPartTypes.get(weekPart.getEduPlanVersionWeek()).put(weekPart.getPartitionElementNumber(), weekPart.getWeekType());
        }

        Map<Long, Map<Long, UsmaEduPlanVersionWeek>> weekMap = SafeMap.get(HashMap.class);

        for (UsmaEduPlanVersionWeek week : usmaWeekList)
        {
            weekMap.get(week.getCourse().getId()).put(week.getWeek().getId(), week);
        }

        for (IEntity course : getCourseList())
        {
            Long courseId = course.getId();
            for (EppWeek eppWeek : this.getGlobalWeekList())
            {
                Long weekId = eppWeek.getId();
                UsmaEduPlanVersionWeek week = weekMap.get(courseId).get(weekId);

                if (week != null && week.getWeekType() == null)
                {
                    fullDataMap.get(courseId).put(weekId, weekPartTypes.get(week));

                } else
                {
                    Map<Integer, EppWeekType> singletonMap = new HashMap<>();
                    singletonMap.put(0, week == null ? null : week.getWeekType());
                    fullDataMap.get(courseId).put(weekId, singletonMap);
                }
            }
        }

        UsmaEduPlanVersionPartitionType versionPartitionType = UniDaoFacade.getCoreDao().get(UsmaEduPlanVersionPartitionType.class, UsmaEduPlanVersionPartitionType.version(), getEduPlanVersion());
        scheduleRangeModel.setSplittable(!versionPartitionType.getPartitionType().getCode().equals(UsmaSchedulePartitionTypeCodes.WEEK));

        return scheduleRangeModel;
    }

    protected StaticListDataSource<ViewWrapper<Course>> buildUsmaScheduleDataSource()
    {
        final StaticListDataSource<ViewWrapper<Course>> scheduleDataSource = new StaticListDataSource<>(this.getCourseList());
        final AbstractColumn courseColumn = new SimpleColumn("Курс", CourseGen.P_INT_VALUE, EppEduPlanVersionScheduleUtils.COURSE_FORMATTER).setClickable(false).setOrderable(false);
        courseColumn.setHeaderStyle("text-align:center");
        scheduleDataSource.addColumn(courseColumn);

        HeadColumn head = null;

        for (final EppWeek week : this.getGlobalWeekList())
        {
            final String headName = "month." + week.getMonth();
            if ((null == head) || (!head.getName().equals(headName)))
            {
                scheduleDataSource.addColumn(head = new HeadColumn(headName, RussianDateFormatUtils.getMonthName(week.getMonth(), true)));
                head.setHeaderStyle("text-align:center");
            }

            final HeadColumn weekColumn = new HeadColumn("week." + week.getCode(), week.getTitle());
            weekColumn.setVerticalHeader(true);

            final UsmaWeekTypeBlockColumn numColumn = new UsmaWeekTypeBlockColumn(week.getId(), week.getNumber() - 1, Integer.toString(week.getNumber()));
            numColumn.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;min-width:17px;font-size:11px;");

            weekColumn.addColumn(numColumn);
            head.addColumn(weekColumn);
        }

        return scheduleDataSource;
    }


    private void prepareRowDataPoints(final Map<Long, int[]> row2points, final Map<Long, int[]> row2data)
    {
        // row -> номер части в году -> [минимальный номер недели,максимальный номер недели]
        final Map<Long, Map<Integer, int[]>> map = new HashMap<>();
        for (UsmaEduPlanVersionWeek item : usmaWeekList)
        {
            // получаем данные
            final Long rowId = item.getCourse().getId();
            final int term = item.getTerm().getIntValue();
            final int weekNumber = item.getWeek().getNumber();
            int partNumber = 0;
            final Integer[] gridDetail = getDevelopGridDetail().get(item.getCourse());
            while ((partNumber < gridDetail.length) && ((null == gridDetail[partNumber]) || (gridDetail[partNumber] != term)))
            {
                partNumber++;
            }
            partNumber++;

            // сохраняем в мапе
            Map<Integer, int[]> partMap = map.get(rowId);
            if (partMap == null)
            {
                map.put(rowId, partMap = new TreeMap<>());
            }

            final int[] points = partMap.get(partNumber);
            if (points == null)
            {
                partMap.put(partNumber, new int[]{weekNumber, weekNumber});
            } else if (weekNumber < points[0])
            {
                points[0] = weekNumber;
            } else if (weekNumber > points[1])
            {
                points[1] = weekNumber;
            }
        }

        for (final Map.Entry<Long, Map<Integer, int[]>> entry : map.entrySet())
        {
            final Map<Integer, int[]> partMap = entry.getValue();
            final int[] points = new int[partMap.size() * 2];
            int i = 0;
            for (final int[] pair : partMap.values())
            {
                points[i++] = pair[0] - 1;
                points[i++] = pair[1] - 1;
            }

            row2points.put(entry.getKey(), points);
            row2data.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, points).getData());
        }
    }
}