/* $Id$ */
package ru.tandemservice.uniusma.component.listextract.e2;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.e2.CourseTransferStuListExtractPrint;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.uniusma.utils.UsmaReportUtils;

/**
 * @author Stanislav Shibarshin
 * @since 01.09.2016
 */
public class UsmaCourseTransferStuListExtractPrint extends CourseTransferStuListExtractPrint
{

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = super.createParagraphInjectModifier(paragraph, firstExtract);
        UsmaReportUtils.removeEduOrgUnitShortFormExtract(injectModifier);
        return injectModifier;
    }

}