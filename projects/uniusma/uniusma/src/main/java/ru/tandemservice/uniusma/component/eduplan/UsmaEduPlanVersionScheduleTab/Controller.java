/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionScheduleTab;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ScheduleCopy.UsmaEduPlanScheduleCopy;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;
import ru.tandemservice.uniusma.tapestry.richTableList.UsmaRangeSelectionWeekTypeListDataSource;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        getDao().updateEduPlanVersionPartitionType(model);
        this.getDao().prepare(model);
    }

    public void onClickRichRowSave(final IBusinessComponent component)
    {
        try {
            final Model model = this.getModel(component);
            final Long rowId = component.getListenerParameter();
            this.getDao().updateScheduleRow(rowId, model);
            model.getScheduleDataSource().setEditId(null);
            this.resetLastVisitedEntityId(model);
        } finally {
            this.onRefreshComponent(component);
        }
    }

    public void onClickRichRowEdit(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Long rowId = component.getListenerParameter();
        this.getDao().prepareEditRow(model, rowId);
        model.getScheduleDataSource().setEditId(rowId);
        this.resetLastVisitedEntityId(model);
    }

    public void onClickRichRowCancel(final IBusinessComponent component)
    {
        try {
            final Model model = this.getModel(component);
            model.getScheduleDataSource().setEditId(null);
            this.resetLastVisitedEntityId(model);
        } finally {
            this.onRefreshComponent(component);
        }
    }

    public void onClickPoint(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Integer index = (Integer) component.getListenerParameter();
        model.getScheduleDataSource().getSelection().doPointClick(index);
        this.resetLastVisitedEntityId(model);
    }

    private void resetLastVisitedEntityId(final Model model)
    {
        model.getScheduleDataSource().getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickChangePartitionType(IBusinessComponent component)
    {
        Model model = getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                "ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionPartitionTypeChange",
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())));
    }

    public void onClickSplitWeek(IBusinessComponent component)
    {
        Model model = getModel(component);

        UsmaSchedulePartitionType partitionType = model.getVersionPartitionType().getPartitionType();
        UsmaRangeSelectionWeekTypeListDataSource<?> dataSource = model.getScheduleDataSource();
        Long courseId = dataSource.getEditId();
        Long weekId = component.getListenerParameter();

        Map<Integer, EppWeekType> weekTypeMap = dataSource.getFullDataMap().get(courseId).get(weekId);

        if (weekTypeMap.size() == 1)
        {
            // неделя была объединена - разбиваем
            EppWeekType weekType = weekTypeMap.get(0);
            weekTypeMap.clear();

            for (int i = 1, size = partitionType.getPartsNumber(); i <= size; i++)
            {
                weekTypeMap.put(i, weekType);
            }

        } else
        {
            // неделя разбита - объединяем
            Map<EppWeekType, Integer> weekTypeCountMap = CollectionUtils.getCardinalityMap(weekTypeMap.values());
            int maxCount = 0;
            EppWeekType weekType = null;

            for (Map.Entry<EppWeekType, Integer> countEntry: weekTypeCountMap.entrySet())
            {
                if (countEntry.getValue() > maxCount)
                {
                    weekType = countEntry.getKey();
                    maxCount = countEntry.getValue();
                }
            }

            weekTypeMap.clear();
            weekTypeMap.put(0, weekType);
        }

        resetLastVisitedEntityId(model);
    }

    public void onClickCopySchedule(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(UsmaEduPlanScheduleCopy.class.getSimpleName(), ParametersMap.createWith("version", this.getModel(component).getVersion().getId())));
    }
}