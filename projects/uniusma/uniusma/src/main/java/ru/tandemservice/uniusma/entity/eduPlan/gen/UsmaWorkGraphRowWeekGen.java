package ru.tandemservice.uniusma.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Неделя строки курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaWorkGraphRowWeekGen extends EntityBase
 implements INaturalIdentifiable<UsmaWorkGraphRowWeekGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek";
    public static final String ENTITY_NAME = "usmaWorkGraphRowWeek";
    public static final int VERSION_HASH = -568077551;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String P_WEEK = "week";
    public static final String L_TYPE = "type";
    public static final String L_TERM = "term";

    private UsmaWorkGraphRow _row;     // Строка ГУП
    private int _week;     // Номер недели
    private EppWeekType _type;     // Тип недели
    private Term _term;     // Семестр

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка ГУП. Свойство не может быть null.
     */
    @NotNull
    public UsmaWorkGraphRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка ГУП. Свойство не может быть null.
     */
    public void setRow(UsmaWorkGraphRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Номер недели. Свойство не может быть null.
     */
    @NotNull
    public int getWeek()
    {
        return _week;
    }

    /**
     * @param week Номер недели. Свойство не может быть null.
     */
    public void setWeek(int week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Тип недели.
     */
    public EppWeekType getType()
    {
        return _type;
    }

    /**
     * @param type Тип недели.
     */
    public void setType(EppWeekType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaWorkGraphRowWeekGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((UsmaWorkGraphRowWeek)another).getRow());
                setWeek(((UsmaWorkGraphRowWeek)another).getWeek());
            }
            setType(((UsmaWorkGraphRowWeek)another).getType());
            setTerm(((UsmaWorkGraphRowWeek)another).getTerm());
        }
    }

    public INaturalId<UsmaWorkGraphRowWeekGen> getNaturalId()
    {
        return new NaturalId(getRow(), getWeek());
    }

    public static class NaturalId extends NaturalIdBase<UsmaWorkGraphRowWeekGen>
    {
        private static final String PROXY_NAME = "UsmaWorkGraphRowWeekNaturalProxy";

        private Long _row;
        private int _week;

        public NaturalId()
        {}

        public NaturalId(UsmaWorkGraphRow row, int week)
        {
            _row = ((IEntity) row).getId();
            _week = week;
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public int getWeek()
        {
            return _week;
        }

        public void setWeek(int week)
        {
            _week = week;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsmaWorkGraphRowWeekGen.NaturalId) ) return false;

            UsmaWorkGraphRowWeekGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getWeek(), that.getWeek()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getWeek());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getWeek());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaWorkGraphRowWeekGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaWorkGraphRowWeek.class;
        }

        public T newInstance()
        {
            return (T) new UsmaWorkGraphRowWeek();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "week":
                    return obj.getWeek();
                case "type":
                    return obj.getType();
                case "term":
                    return obj.getTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((UsmaWorkGraphRow) value);
                    return;
                case "week":
                    obj.setWeek((Integer) value);
                    return;
                case "type":
                    obj.setType((EppWeekType) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "week":
                        return true;
                case "type":
                        return true;
                case "term":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "week":
                    return true;
                case "type":
                    return true;
                case "term":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return UsmaWorkGraphRow.class;
                case "week":
                    return Integer.class;
                case "type":
                    return EppWeekType.class;
                case "term":
                    return Term.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaWorkGraphRowWeek> _dslPath = new Path<UsmaWorkGraphRowWeek>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaWorkGraphRowWeek");
    }
            

    /**
     * @return Строка ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getRow()
     */
    public static UsmaWorkGraphRow.Path<UsmaWorkGraphRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Номер недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getWeek()
     */
    public static PropertyPath<Integer> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Тип недели.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getType()
     */
    public static EppWeekType.Path<EppWeekType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    public static class Path<E extends UsmaWorkGraphRowWeek> extends EntityPath<E>
    {
        private UsmaWorkGraphRow.Path<UsmaWorkGraphRow> _row;
        private PropertyPath<Integer> _week;
        private EppWeekType.Path<EppWeekType> _type;
        private Term.Path<Term> _term;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getRow()
     */
        public UsmaWorkGraphRow.Path<UsmaWorkGraphRow> row()
        {
            if(_row == null )
                _row = new UsmaWorkGraphRow.Path<UsmaWorkGraphRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Номер недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getWeek()
     */
        public PropertyPath<Integer> week()
        {
            if(_week == null )
                _week = new PropertyPath<Integer>(UsmaWorkGraphRowWeekGen.P_WEEK, this);
            return _week;
        }

    /**
     * @return Тип недели.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getType()
     */
        public EppWeekType.Path<EppWeekType> type()
        {
            if(_type == null )
                _type = new EppWeekType.Path<EppWeekType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRowWeek#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

        public Class getEntityClass()
        {
            return UsmaWorkGraphRowWeek.class;
        }

        public String getEntityName()
        {
            return "usmaWorkGraphRowWeek";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
