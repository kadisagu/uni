package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Связь компетенции с элементом реестра
 */
public class UsmaCompetence2RegistryElementRel extends UsmaCompetence2RegistryElementRelGen
{
    public UsmaCompetence2RegistryElementRel()
    {
    }

    public UsmaCompetence2RegistryElementRel(EppRegistryElement registryElement, UsmaCompetence competence)
    {
        setRegistryElement(registryElement);
        setUsmaCompetence(competence);
    }
}