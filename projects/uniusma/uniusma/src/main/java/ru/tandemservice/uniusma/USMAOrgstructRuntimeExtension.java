/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusma;

import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.uniusma.runtime.orgstruct.IUSMAOrgstructDao;

/**
 * @author dseleznev
 * Created on: 16.06.2008
 */
public class USMAOrgstructRuntimeExtension implements IRuntimeExtension
{
    private IUSMAOrgstructDao _usmaOrgstructDao;

    public void setUsmaOrgstructDao(IUSMAOrgstructDao usmaOrgstructDao)
    {
        this._usmaOrgstructDao = usmaOrgstructDao;
    }

    @Override
    public void init(Object object)
    {
        _usmaOrgstructDao.createOrgstructHierarhy();
        _usmaOrgstructDao.createOrgUnitToKindRelations();
        _usmaOrgstructDao.createOrgUnitsStructure();
    }

    @Override
    public void destroy()
    {
    }
}