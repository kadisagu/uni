/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.base.bo.UsmaSystemAction.logic.IUsmaSystemActionDao;
import ru.tandemservice.uniusma.base.bo.UsmaSystemAction.logic.UsmaSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class UsmaSystemActionManager extends BusinessObjectManager
{
    public static UsmaSystemActionManager instance()
    {
        return instance(UsmaSystemActionManager.class);
    }

    @Bean
    public IUsmaSystemActionDao dao()
    {
        return new UsmaSystemActionDao();
    }
}
