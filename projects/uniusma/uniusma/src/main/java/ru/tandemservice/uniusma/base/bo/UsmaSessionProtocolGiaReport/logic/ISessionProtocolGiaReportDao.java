/*$Id:$*/
package ru.tandemservice.uniusma.base.bo.UsmaSessionProtocolGiaReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 17.03.2016
 */
public interface ISessionProtocolGiaReportDao extends ICommonDAO, INeedPersistenceSupport
{

    RtfDocument getDocument(
            final List<Group> groupList,
            final List<PpsEntry> chairmanPpsList,
            final List<PpsEntry> membersPpsList,
            final List<PpsEntry> clerkPpsList);
}
