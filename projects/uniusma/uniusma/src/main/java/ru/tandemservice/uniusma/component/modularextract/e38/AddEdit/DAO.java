/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e38.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.WeekendChildOutStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 26.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        WeekendChildOutStuExtractUsmaExt extractExt = getByNaturalId(new WeekendChildOutStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            // если расширения нет - создадим его
            extractExt = new WeekendChildOutStuExtractUsmaExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setExtUsmaExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);
        saveOrUpdate(m.getExtUsmaExt());
    }
}
