\fs28\qc\bО переводе студентов с одной специальности на другую\b0\par
\par\fi709\qj\keep\keepn
В соответствии с ч. 2 ст. 30, п. 13 ч. 1 ст. 34 Федерального закона от 29.12.2012 г. № 273-ФЗ «Об образовании в РФ», п. 9 Порядка перевода студентов из одного высшего учебного заведения РФ в другое (утв. Приказом Министерства общего и профессионального образования РФ от 24.02.1998 г. № 501), Положением о порядке перевода и восстановления студентов ГБОУ ВПО УГМУ Минздрава России,\par\par
\expnd8\expndtw40\b приказываю: \expndtw0\expnd0\b0\par\par
1. Перевести {fio} {birthDate} года рождения, {student_A} {courseOld} курса {developFormOld_GF} формы обучения группы {groupOld} {orgUnitOld_G}, {learned_A} {compensationTypeStrOld}, на {courseNew} курс {developFormNew_GF} формы обучения в группу {groupNew} {orgUnitNew_G} {compensationTypeStrNew}. К занятиям допустить с {entryIntoForceDate} г.\par
{stopGrantPayingStr}\par
{debts}\par\par
\fi0\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx376
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6330
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8031
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10299
\pard\intbl\qc\'b9\cell
\'c4\'e8\'f1\'f6\'e8\'ef\'eb\'e8\'ed\'e0\cell
\'ca\'ee\'eb\'e8\'f7\'e5\'f1\'f2\'e2\'ee \'f7\'e0\'f1\'ee\'e2\cell\'c2\'e8\'e4 \'ee\'f2\'f7\'e5\'f2\'ed\'ee\'f1\'f2\'e8\cell\row
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx376
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6330
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8031
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10299
\pard\intbl\qj {T}\cell
\cell
\pard\intbl\qc\cell
\cell\row
\pard
\par\fi709\qj\keep\keepn
Основание: {listBasics}