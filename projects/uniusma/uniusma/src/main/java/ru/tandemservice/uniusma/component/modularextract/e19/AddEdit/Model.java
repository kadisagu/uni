/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e19.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 26.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e19.AddEdit.Model
{
    DischargingStuExtractUsmaExt _extUsmaExt;

    public DischargingStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(DischargingStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
