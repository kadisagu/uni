/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockSpecialityAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaSpeciality;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getSpecialityId() == null)
        {
            UsmaSpeciality speciality = new UsmaSpeciality();
            speciality.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
            model.setSpeciality(speciality);

        } else
        {
            model.setSpeciality(get(UsmaSpeciality.class, model.getSpecialityId()));
        }
    }
}