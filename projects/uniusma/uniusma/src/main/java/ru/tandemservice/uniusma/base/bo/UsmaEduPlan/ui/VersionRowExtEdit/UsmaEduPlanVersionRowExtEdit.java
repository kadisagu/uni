/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.VersionRowExtEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Alexander Zhebko
 * @since 29.10.2013
 */
@Configuration
public class UsmaEduPlanVersionRowExtEdit extends BusinessComponentManager
{
}