package ru.tandemservice.uniusma.entity.orders;

import java.util.Date;

import ru.tandemservice.uniusma.entity.orders.gen.UsmaEnrollmentOrderCardGen;

/**
 * Карточка приказа о зачислении абитуриентов
 */
public class UsmaEnrollmentOrderCard extends UsmaEnrollmentOrderCardGen
{
    public Date getEnrollmentDate()
    {
        return getOrder().getEnrollmentDate();
    }

    public void setEnrollmentDate(Date enrollmentDate)
    {
        getOrder().setEnrollmentDate(enrollmentDate);
    }    
}