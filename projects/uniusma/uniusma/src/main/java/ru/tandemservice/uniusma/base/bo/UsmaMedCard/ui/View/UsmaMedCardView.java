/* $Id$ */
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 24.06.2014
 */
@Configuration
public class UsmaMedCardView extends BusinessComponentManager
{
	//constants
	public static final String EDIT_LISTENER = "onEditEntityFromList";
	public static final String DELETE_LISTENER = "onDeleteEntityFromList";
	public static final String ALERT_FORMATTER = "ui:currentItemAlert";
	//data source
	public static final String STU_VACCINE_INFO_DS = "stuVaccineInfoDS";
	public static final String STU_DISEASE_INFO_DS = "stuDiseaseInfoDS";
	public static final String STU_FLUOROGRAPHY_INFO_DS = "stuFluorographyInfoDS";
	public static final String STU_OTHER_INFO_DS = "stuOtherInfoDS";
	//student data
	public static final String STUDENT_ID = "studentId";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(STU_VACCINE_INFO_DS, getStuVaccineInfoDS(), stuVaccineInfoDSHandler()))
				.addDataSource(searchListDS(STU_FLUOROGRAPHY_INFO_DS, getStuFluorographyInfoDS(), stuFluorographyInfoDSHandler()))
				.addDataSource(searchListDS(STU_DISEASE_INFO_DS, getStuDiseaseInfoDS(), stuDiseaseInfoDSHandler()))
				.addDataSource(searchListDS(STU_OTHER_INFO_DS, getStuOtherInfoDS(), stuOtherInfoDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint getStuVaccineInfoDS()
	{
		return columnListExtPointBuilder(STU_VACCINE_INFO_DS)
				.addColumn(textColumn("vaccineKind", UsmaStudentVaccineInfo.vaccineKind().title()).order())
				.addColumn(textColumn("vaccinationSign", UsmaStudentVaccineInfo.vaccinationSign().title()))
				.addColumn(dateColumn("date", UsmaStudentVaccineInfo.date()).order())
				.addColumn(textColumn("vaccinePart", UsmaStudentVaccineInfo.part()))
				.addColumn(textColumn("periodical", UsmaStudentVaccineInfo.periodical()))
				.addColumn(actionColumn("edit").icon(ActionColumn.EDIT).listener(EDIT_LISTENER).displayHeader(false).permissionKey("editVaccineInfo"))
				.addColumn(actionColumn("delete").icon(ActionColumn.DELETE).listener(DELETE_LISTENER).alert(ALERT_FORMATTER).displayHeader(false).permissionKey("deleteVaccineInfo"))
				.create();
	}

	@Bean
	public ColumnListExtPoint getStuFluorographyInfoDS()
	{
		return columnListExtPointBuilder(STU_FLUOROGRAPHY_INFO_DS)
				.addColumn(dateColumn("date", UsmaStudentFluorographyInfo.date()).order())
				.addColumn(textColumn("result", UsmaStudentFluorographyInfo.result()))
				.addColumn(textColumn("pictureNumber", UsmaStudentFluorographyInfo.pictureNumber()))
				.addColumn(actionColumn("edit").icon(ActionColumn.EDIT).listener(EDIT_LISTENER).displayHeader(false).permissionKey("editFluorographyInfo"))
				.addColumn(actionColumn("delete").icon(ActionColumn.DELETE).listener(DELETE_LISTENER).alert(ALERT_FORMATTER).displayHeader(false).permissionKey("deleteFluorographyInfo"))
				.create();
	}

	@Bean
	public ColumnListExtPoint getStuDiseaseInfoDS()
	{
		return columnListExtPointBuilder(STU_DISEASE_INFO_DS)
				.addColumn(textColumn("diseasesName", UsmaStudentDiseaseInfo.diseasesKind().title()))
				.addColumn(dateColumn("beginDate", UsmaStudentDiseaseInfo.beginDate()).order())
				.addColumn(dateColumn("endDate", UsmaStudentDiseaseInfo.endDate()))
				.addColumn(actionColumn("edit").icon(ActionColumn.EDIT).listener(EDIT_LISTENER).displayHeader(false).permissionKey("editDiseaseInfo"))
				.addColumn(actionColumn("delete").icon(ActionColumn.DELETE).listener(DELETE_LISTENER).alert(ALERT_FORMATTER).displayHeader(false).permissionKey("deleteDiseaseInfo"))
				.create();
	}

	@Bean
	public ColumnListExtPoint getStuOtherInfoDS()
	{
		return columnListExtPointBuilder(STU_OTHER_INFO_DS)
				.addColumn(textColumn("healthGroup", UsmaStudentOtherInfo.healthGroup().title()))
				.addColumn(booleanColumn("dispensary", UsmaStudentOtherInfo.dispensary()))
				.addColumn(actionColumn("edit").icon(ActionColumn.EDIT).listener(EDIT_LISTENER).displayHeader(false).permissionKey("editOtherInfo"))
				.addColumn(actionColumn("delete").icon(ActionColumn.DELETE).listener(DELETE_LISTENER).alert(ALERT_FORMATTER).displayHeader(false).permissionKey("deleteOtherInfo"))
				.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> stuVaccineInfoDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				Long studentId = context.get(STUDENT_ID);
				DQLSelectBuilder builder = new DQLSelectBuilder()
						.fromEntity(UsmaStudentVaccineInfo.class, "vac")
						.column("vac")
						.where(eq(property("vac", UsmaStudentVaccineInfo.student().id()), value(studentId)))
						.order(property("vac", (String) input.getEntityOrder().getKey()), input.getEntityOrder().getDirection());

				List<UsmaStudentVaccineInfo> list = builder.createStatement(context.getSession()).list();

				input.setCountRecord(Math.max(3, list.size()));

				return ListOutputBuilder.get(input, list).preserveCountRecord().pageable(false).build();
			}
		};
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> stuFluorographyInfoDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				Long studentId = context.get(STUDENT_ID);
				DQLSelectBuilder builder = new DQLSelectBuilder()
						.fromEntity(UsmaStudentFluorographyInfo.class, "flg")
						.column("flg")
						.where(eq(property("flg", UsmaStudentFluorographyInfo.student().id()), value(studentId)))
						.order(property("flg", (String) input.getEntityOrder().getKey()), input.getEntityOrder().getDirection());

				List<UsmaStudentFluorographyInfo> list = builder.createStatement(context.getSession()).list();

				input.setCountRecord(Math.max(3, list.size()));

				return ListOutputBuilder.get(input, list).preserveCountRecord().pageable(false).build();
			}
		};
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> stuDiseaseInfoDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				Long studentId = context.get(STUDENT_ID);
				DQLSelectBuilder builder = new DQLSelectBuilder()
						.fromEntity(UsmaStudentDiseaseInfo.class, "dis")
						.column("dis")
						.where(eq(property("dis", UsmaStudentDiseaseInfo.student().id()), value(studentId)))
						.order(property("dis", (String) input.getEntityOrder().getKey()), input.getEntityOrder().getDirection());

				List<UsmaStudentDiseaseInfo> list = builder.createStatement(context.getSession()).list();

				input.setCountRecord(Math.max(3, list.size()));

				return ListOutputBuilder.get(input, list).preserveCountRecord().pageable(false).build();
			}
		};
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> stuOtherInfoDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				Long studentId = context.get(STUDENT_ID);
				DQLSelectBuilder builder = new DQLSelectBuilder()
						.fromEntity(UsmaStudentOtherInfo.class, "oth")
						.column("oth")
						.where(eq(property("oth", UsmaStudentOtherInfo.student().id()), value(studentId)))
						.order(property("oth", UsmaStudentOtherInfo.L_HEALTH_GROUP), OrderDirection.asc);

				List<UsmaStudentOtherInfo> list = builder.createStatement(context.getSession()).list();

				input.setCountRecord(Math.max(3, list.size()));

				return ListOutputBuilder.get(input, list).preserveCountRecord().pageable(false).build();
			}
		};
	}
}