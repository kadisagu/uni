package ru.tandemservice.uniusma.entity.eduPlan;

import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * ГУП (Часть недели строки курса)
 */
public class UsmaWorkGraphRowWeekPart extends UsmaWorkGraphRowWeekPartGen
{
    public UsmaWorkGraphRowWeekPart()
    {

    }

    public UsmaWorkGraphRowWeekPart(UsmaWorkGraphRowWeek week, int part)
    {
        this.setWeek(week);
        this.setPart(part);
    }
}