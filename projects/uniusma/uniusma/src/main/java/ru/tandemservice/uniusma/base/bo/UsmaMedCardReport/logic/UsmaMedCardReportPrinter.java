/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.UsmaMedCardReportManager;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.util.IUsmaDQLReportModifier;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.vo.UsmaReportDataWrapper;
import ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo;

import javax.validation.constraints.NotNull;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 25.06.2014
 */
public class UsmaMedCardReportPrinter
{
    public static final String CSV_SEPARATOR = ";";
    private final long _orgUnitId;

    public UsmaMedCardReportPrinter(long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public IDocumentRenderer printReport(@NotNull Collection<IUsmaDQLReportModifier> modifiers)
    {
        List<String> parameters = new ArrayList<>();
        for (IUsmaDQLReportModifier modifier : modifiers)
        {
            modifier.addParameters(parameters);
        }

        List<UsmaReportDataWrapper> students = getStudentList(modifiers);
        return buildDocumentRenderer(parameters, students);
    }

    private RtfDocument createRtfDocument()
    {
//      InputStream in = getClass().getClassLoader().getResourceAsStream("uniusma/templates/MedCardReport.rtf");
        ITemplateDocument templateDocument = UsmaMedCardReportManager.instance().dao().getTemplateDocument();
        return new RtfReader().read(templateDocument.getContent());
    }

    private IDocumentRenderer buildDocumentRenderer(Collection<String> parameters, Collection<UsmaReportDataWrapper> students)
    {
        final RtfDocument document = createRtfDocument();
        OrgUnit formativeOrgUnit = DataAccessServices.dao().getNotNull(_orgUnitId);
        new RtfInjectModifier()
                .put("formativeOrgUnit", formativeOrgUnit.getPrintTitle())
                .modify(document);
        prepareHeaderTableModifier(parameters).modify(document);
        prepareStudentsTableModifier(students).modify(document);
        return new CommonBaseRenderer().rtf().fileName("MedCardReport.rtf").document(document);
    }

    private RtfTableModifier prepareHeaderTableModifier(Collection<String> parameters)
    {
        List<String[]> parametersTable = new ArrayList<>();
        for (String parameter : parameters)
        {
            parametersTable.add(parameter.split(CSV_SEPARATOR, 2));
        }
        final RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("H", parametersTable.toArray(new String[parametersTable.size()][]));

        return tableModifier;
    }

    private RtfTableModifier prepareStudentsTableModifier(Collection<UsmaReportDataWrapper> students)
    {
        final List<String[]> studentsTable = new ArrayList<>();
        int i = 1;
        for (UsmaReportDataWrapper item : students)
        {
            studentsTable.add(new String[]{String.valueOf(i++), item.getFullFio(), item.getBirthDate(), item.getGroupTitle(), item.getFlgDate()});
        }

        final RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("SL", studentsTable.toArray(new String[studentsTable.size()][]));

        return tableModifier;
    }

    private List<UsmaReportDataWrapper> getStudentList(@NotNull Collection<IUsmaDQLReportModifier> modifiersList)
    {
        final String studentAlias = "stu";
        DQLSelectBuilder studentBuilder = new DQLSelectBuilder()
                .fromEntity(Student.class, studentAlias)
                .where(eq(property(studentAlias, Student.educationOrgUnit().formativeOrgUnit()), value(_orgUnitId)));

        //join fio and birth date from IdentityCard
        final String identityCardAlias = "idc";
        final String personAlias = "p";
        DQLSelectBuilder idcDql = new DQLSelectBuilder().fromEntity(Person.class, personAlias)
                .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias(personAlias), identityCardAlias)
                .column(property(personAlias, Person.P_ID), "person_id")
                .column(property(identityCardAlias, IdentityCard.P_FULL_FIO), "full_fio")
                .column(property(identityCardAlias, IdentityCard.P_BIRTH_DATE), "birth_date");
        studentBuilder.joinDataSource(studentAlias, DQLJoinType.left, idcDql.buildQuery(), "stu_p", eq(property("stu_p.person_id"), property(Student.person().fromAlias(studentAlias))));

        //join group
        DQLSelectBuilder groupDql = new DQLSelectBuilder().fromEntity(Group.class, "g")
                .column(property("g", Group.P_ID), "group_id")
                .column(property("g", Group.P_TITLE), "group_title");
        studentBuilder.joinDataSource(studentAlias, DQLJoinType.left, groupDql.buildQuery(), "stu_gr", eq(property("stu_gr.group_id"), property(studentAlias, Student.group().id())));

        // join last fluorography date from UsmaStudentFluorographyInfo
        DQLSelectBuilder flgDql = new DQLSelectBuilder().fromEntity(UsmaStudentFluorographyInfo.class, "f")
                .column(property("f", UsmaStudentFluorographyInfo.student().id()), "student_id")
                .column(DQLFunctions.max(property("f", UsmaStudentFluorographyInfo.P_DATE)), "fl_date")
                .group(property("f", UsmaStudentFluorographyInfo.student().id()));

        studentBuilder.joinDataSource(studentAlias, DQLJoinType.left, flgDql.buildQuery(), "stu_fl", eq(property("stu_fl.student_id"), property(studentAlias)));

        //apply filters
        for (IUsmaDQLReportModifier modifier : modifiersList)
        {
            modifier.modify(studentAlias, studentBuilder);
        }

        //add columns
        studentBuilder.column("stu_p.full_fio");    //0
        studentBuilder.column("stu_p.birth_date");  //1
        studentBuilder.column("stu_gr.group_title");//2
        studentBuilder.column("stu_fl.fl_date");    //3

        List<Object[]> result = UsmaMedCardReportManager.instance().dao().getList(studentBuilder);
        List<UsmaReportDataWrapper> rows = new ArrayList<>(result.size());
        for (Object[] row : result)
        {
            String fullFio = (String) row[0];
            Date birthDate = (Date) row[1];
            String groupTitle = (String) row[2];
            Date flgDate = (Date) row[3];
            rows.add(new UsmaReportDataWrapper(fullFio, groupTitle, birthDate, flgDate));
        }

        Collections.sort(rows);
        return rows;
    }
}
