/* $Id$ */
package ru.tandemservice.uniusma.component.eduplan.EduPlanVersionAddRowFromOther;

import com.google.common.base.Preconditions;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaEpvRowTermLoad;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 24.10.2014
 */
public class DAO extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther.DAO
{
    @Override
    protected void updateCopiedRows(Map<Long, Long> rowMap)
    {
        super.updateCopiedRows(rowMap);

        if (rowMap.isEmpty())
            return;

        // при сохранении строк необходимо также скопировать
        //  компетенции
        //  рассредоточенность практик
        //  нагрузку (ДВФУ)
        updateEpvPracticeDispersion(rowMap);
        updateEpvRowCompetences(rowMap);
        updateEpvLoad(rowMap);
    }

    /** Сохраняет данные о рассредоточенности практик УП. */
    private void updateEpvPracticeDispersion(@NotNull Map<Long, Long> rowMap)
    {
        Preconditions.checkNotNull(rowMap);
        new DQLDeleteBuilder(UsmaPracticeDispersion.class).where(in(property(UsmaPracticeDispersion.practice()), rowMap.values())).createStatement(this.getSession()).execute();

        Collection<Long> sourceRowDispersedPractices = this.getList(
            new DQLSelectBuilder()
                .fromEntity(UsmaPracticeDispersion.class, "d")
                .column(property("d", UsmaPracticeDispersion.practice().id()))
                .where(eq(property("d", UsmaPracticeDispersion.dispersed()), value(Boolean.TRUE)))
                .where(in(property("d", UsmaPracticeDispersion.practice()), rowMap.keySet())));

        if (sourceRowDispersedPractices.isEmpty())
            return;

        short entityCode = EntityRuntime.getMeta(UsmaPracticeDispersion.class).getEntityCode();
        DQLInsertValuesBuilder builder = new DQLInsertValuesBuilder(UsmaPracticeDispersion.class);

        for (Long sourceRow: sourceRowDispersedPractices)
        {
            builder.value(UsmaPracticeDispersion.P_ID, EntityIDGenerator.generateNewId(entityCode));
            builder.value(UsmaPracticeDispersion.L_PRACTICE, rowMap.get(sourceRow));
            builder.value(UsmaPracticeDispersion.P_DISPERSED, Boolean.TRUE);
            builder.addBatch();
        }

        builder.createStatement(this.getSession()).execute();
    }

    /** Сохраняет компетенции строк УП. */
    private void updateEpvRowCompetences(@NotNull Map<Long, Long> rowMap)
    {
        Preconditions.checkNotNull(rowMap);
        new DQLDeleteBuilder(UsmaCompetence2EpvRegistryRowRel.class).where(in(property(UsmaCompetence2EpvRegistryRowRel.registryRow()), rowMap.values())).createStatement(this.getSession()).execute();

        Collection<Object[]> rows = new DQLSelectBuilder()
            .fromEntity(UsmaCompetence2EpvRegistryRowRel.class, "rel")
            .column(property("rel", UsmaCompetence2EpvRegistryRowRel.registryRow().id()))
            .column(property("rel", UsmaCompetence2EpvRegistryRowRel.usmaCompetence().id()))
            .where(in(property("rel", UsmaCompetence2EpvRegistryRowRel.registryRow()), rowMap.keySet()))
            .createStatement(this.getSession()).list();

        if (rows.isEmpty())
            return;

        int rowCount = 0;
        short entityCode = EntityRuntime.getMeta(UsmaCompetence2EpvRegistryRowRel.class).getEntityCode();
        DQLInsertValuesBuilder builder = new DQLInsertValuesBuilder(UsmaCompetence2EpvRegistryRowRel.class);

        for (Object[] row: rows)
        {
            rowCount++;
            builder.value(UsmaCompetence2EpvRegistryRowRel.P_ID, EntityIDGenerator.generateNewId(entityCode));
            builder.value(UsmaCompetence2EpvRegistryRowRel.L_REGISTRY_ROW, rowMap.get((Long) row[0]));
            builder.value(UsmaCompetence2EpvRegistryRowRel.L_USMA_COMPETENCE, (Long) row[1]);
            builder.addBatch();

            if (rowCount == DQL.MAX_VALUES_ROW_NUMBER)
            {
                builder.createStatement(this.getSession()).execute();
                builder = new DQLInsertValuesBuilder(UsmaCompetence2EpvRegistryRowRel.class);
                rowCount = 0;
            }
        }

        if (rowCount > 0) builder.createStatement(this.getSession()).execute();
    }

    /** Сохраняет интерактивную нагрузку и часы за экзамен. */
    private void updateEpvLoad(@NotNull Map<Long, Long> rowMap)
    {
        Preconditions.checkNotNull(rowMap);
        new DQLDeleteBuilder(UsmaEpvRowTermLoad.class).where(in(property(UsmaEpvRowTermLoad.rowTerm().row()), rowMap.values())).createStatement(this.getSession()).execute();

        Collection<UsmaEpvRowTermLoad> usmaRowTermLoads = new DQLSelectBuilder()
            .fromEntity(UsmaEpvRowTermLoad.class, "l")
            .fetchPath(DQLJoinType.inner, UsmaEpvRowTermLoad.rowTerm().fromAlias("l"), "t")
            .column(property("l"))
            .where(in(property("t", EppEpvRowTerm.row()), rowMap.keySet()))
            .createStatement(this.getSession()).list();

        if (usmaRowTermLoads.isEmpty())
            return;

        //   row ->  term -> rowTerm
        Map<Long, Map<Long, Long>> rowTermLoadMap = SafeMap.get(HashMap.class);
        for (EppEpvRowTerm rowTerm: this.getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id(), rowMap.values()))
            rowTermLoadMap.get(rowTerm.getRow().getId()).put(rowTerm.getTerm().getId(), rowTerm.getId());

        short rowTermEntityCode = EntityRuntime.getMeta(EppEpvRowTerm.class).getEntityCode();
        DQLInsertValuesBuilder rowTermInsertBuilder = new DQLInsertValuesBuilder(EppEpvRowTerm.class);
        boolean insertRowTerms = false;

        short usmaEpvRowTermEntityCode = EntityRuntime.getMeta(UsmaEpvRowTermLoad.class).getEntityCode();
        DQLInsertValuesBuilder usmaRowTermInsertBuilder = new DQLInsertValuesBuilder(UsmaEpvRowTermLoad.class);

        for (UsmaEpvRowTermLoad usmaRowTermLoad: usmaRowTermLoads)
        {
            EppEpvRowTerm sourceRowTerm = usmaRowTermLoad.getRowTerm();
            Long term = sourceRowTerm.getTerm().getId();
            Long row = rowMap.get(sourceRowTerm.getRow().getId());
            Long rowTerm = rowTermLoadMap.get(row).get(term);

            if (rowTerm == null)
            {
                rowTerm = EntityIDGenerator.generateNewId(rowTermEntityCode);
                rowTermLoadMap.get(row).put(term, rowTerm);

                rowTermInsertBuilder.value(EppEpvRowTerm.P_ID, rowTerm);
                rowTermInsertBuilder.value(EppEpvRowTerm.L_ROW, row);
                rowTermInsertBuilder.value(EppEpvRowTerm.L_TERM, term);
                rowTermInsertBuilder.value(EppEpvRowTerm.P_HOURS_TOTAL, sourceRowTerm.getHoursTotal());
                rowTermInsertBuilder.value(EppEpvRowTerm.P_LABOR, sourceRowTerm.getLabor());
                rowTermInsertBuilder.value(EppEpvRowTerm.P_WEEKS, sourceRowTerm.getWeeks());
                rowTermInsertBuilder.value(EppEpvRowTerm.P_HOURS_AUDIT, sourceRowTerm.getHoursAudit());
                rowTermInsertBuilder.value(EppEpvRowTerm.P_HOURS_SELFWORK, sourceRowTerm.getHoursSelfwork());
                rowTermInsertBuilder.value(EppEpvRowTerm.P_HOURS_CONTROL, sourceRowTerm.getHoursControl());
                rowTermInsertBuilder.value(EppEpvRowTerm.P_HOURS_CONTROL_E, sourceRowTerm.getHoursControlE());

                rowTermInsertBuilder.addBatch();
                insertRowTerms = true;
            }

            usmaRowTermInsertBuilder.value(UsmaEpvRowTermLoad.P_ID, EntityIDGenerator.generateNewId(usmaEpvRowTermEntityCode));
            usmaRowTermInsertBuilder.value(UsmaEpvRowTermLoad.L_ROW_TERM, rowTerm);
            usmaRowTermInsertBuilder.value(UsmaEpvRowTermLoad.L_LOAD_TYPE, usmaRowTermLoad.getLoadType());
            usmaRowTermInsertBuilder.value(UsmaEpvRowTermLoad.P_LOAD, usmaRowTermLoad.getLoad());

            usmaRowTermInsertBuilder.addBatch();
        }

        if (insertRowTerms)
            rowTermInsertBuilder.createStatement(this.getSession()).execute();

        usmaRowTermInsertBuilder.createStatement(this.getSession()).execute();
    }
}