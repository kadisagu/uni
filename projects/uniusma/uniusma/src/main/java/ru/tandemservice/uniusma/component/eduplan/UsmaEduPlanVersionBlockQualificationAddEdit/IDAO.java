/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockQualificationAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public interface IDAO extends IUniDao<Model>
{
}