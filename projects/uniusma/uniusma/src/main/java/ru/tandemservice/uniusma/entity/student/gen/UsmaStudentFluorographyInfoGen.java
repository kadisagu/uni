package ru.tandemservice.uniusma.entity.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сведения о флюрографии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaStudentFluorographyInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo";
    public static final String ENTITY_NAME = "usmaStudentFluorographyInfo";
    public static final int VERSION_HASH = 1599497217;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_DATE = "date";
    public static final String P_RESULT = "result";
    public static final String P_PICTURE_NUMBER = "pictureNumber";

    private Student _student;     // Студент
    private Date _date;     // Дата флюорографии
    private String _result;     // Результаты флюорографии
    private String _pictureNumber;     // Номер снимка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Дата флюорографии. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата флюорографии. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Результаты флюорографии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getResult()
    {
        return _result;
    }

    /**
     * @param result Результаты флюорографии. Свойство не может быть null.
     */
    public void setResult(String result)
    {
        dirty(_result, result);
        _result = result;
    }

    /**
     * @return Номер снимка.
     */
    @Length(max=255)
    public String getPictureNumber()
    {
        return _pictureNumber;
    }

    /**
     * @param pictureNumber Номер снимка.
     */
    public void setPictureNumber(String pictureNumber)
    {
        dirty(_pictureNumber, pictureNumber);
        _pictureNumber = pictureNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaStudentFluorographyInfoGen)
        {
            setStudent(((UsmaStudentFluorographyInfo)another).getStudent());
            setDate(((UsmaStudentFluorographyInfo)another).getDate());
            setResult(((UsmaStudentFluorographyInfo)another).getResult());
            setPictureNumber(((UsmaStudentFluorographyInfo)another).getPictureNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaStudentFluorographyInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaStudentFluorographyInfo.class;
        }

        public T newInstance()
        {
            return (T) new UsmaStudentFluorographyInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "date":
                    return obj.getDate();
                case "result":
                    return obj.getResult();
                case "pictureNumber":
                    return obj.getPictureNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "result":
                    obj.setResult((String) value);
                    return;
                case "pictureNumber":
                    obj.setPictureNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "date":
                        return true;
                case "result":
                        return true;
                case "pictureNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "date":
                    return true;
                case "result":
                    return true;
                case "pictureNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "date":
                    return Date.class;
                case "result":
                    return String.class;
                case "pictureNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaStudentFluorographyInfo> _dslPath = new Path<UsmaStudentFluorographyInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaStudentFluorographyInfo");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дата флюорографии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Результаты флюорографии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getResult()
     */
    public static PropertyPath<String> result()
    {
        return _dslPath.result();
    }

    /**
     * @return Номер снимка.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getPictureNumber()
     */
    public static PropertyPath<String> pictureNumber()
    {
        return _dslPath.pictureNumber();
    }

    public static class Path<E extends UsmaStudentFluorographyInfo> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _result;
        private PropertyPath<String> _pictureNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дата флюорографии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UsmaStudentFluorographyInfoGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Результаты флюорографии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getResult()
     */
        public PropertyPath<String> result()
        {
            if(_result == null )
                _result = new PropertyPath<String>(UsmaStudentFluorographyInfoGen.P_RESULT, this);
            return _result;
        }

    /**
     * @return Номер снимка.
     * @see ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo#getPictureNumber()
     */
        public PropertyPath<String> pictureNumber()
        {
            if(_pictureNumber == null )
                _pictureNumber = new PropertyPath<String>(UsmaStudentFluorographyInfoGen.P_PICTURE_NUMBER, this);
            return _pictureNumber;
        }

        public Class getEntityClass()
        {
            return UsmaStudentFluorographyInfo.class;
        }

        public String getEntityName()
        {
            return "usmaStudentFluorographyInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
