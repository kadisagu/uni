/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusma.runtime.student;

import org.tandemframework.core.runtime.IRuntimeExtension;

/**
 * @author agolubenko
 * @since 09.10.2008
 */
public class StudentImportRuntimeExtension implements IRuntimeExtension
{
    @SuppressWarnings("unused")
    private IStudentImportDAO _dao;

    public void setDao(IStudentImportDAO dao)
    {
        _dao = dao;
    }

    @Override
    public void init(Object object)
    {
        try
        {
            /*
            File file = new File(ApplicationRuntime.getAppInstallPath(), "/data/edudata/farm.xml");
            if (file.exists())
            {
                _dao.importStudentsIntoGroups(file);
            }*/
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void destroy()
    {
        
    }
}
