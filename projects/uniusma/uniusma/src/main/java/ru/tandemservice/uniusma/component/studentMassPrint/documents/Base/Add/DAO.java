/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.IStudentMassPrint;
import ru.tandemservice.uniusma.component.studentMassPrint.MassPrintUtil;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public abstract class DAO<T extends IModel> extends UniDao<T> implements IDAO<T> {

    @Override
    public void update(T model)
    {
        IStudentMassPrint<IModel> bean = MassPrintUtil.getBean(model);
        byte[] bytes = bean.getDocument(model.getStudentList(), model, model.getDocIndex(), model.getStudentDocumentType().getId());

        String fileName = "массовая печать документов по студентам";
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(bytes), false);
    }
}
