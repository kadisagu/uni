/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.ui;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author vip_delete
 * @since 24.06.2009
 */
public class USMAEntrantRequestNumberFormatter implements IFormatter<Integer>
{
    @Override
    public String format(Integer source)
    {
        if (null == source) return "";
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
        formatter.setMinimumIntegerDigits(4);//4 - число цифр в номере заявдения
        formatter.setGroupingUsed(false);
        return formatter.format(source);
    }
}
