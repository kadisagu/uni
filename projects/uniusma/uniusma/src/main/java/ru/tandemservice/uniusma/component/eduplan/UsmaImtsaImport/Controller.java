/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaImtsaImport;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniusma.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;
import ru.tandemservice.uniusma.entity.UsmaImtsaImportLog;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.ImtsaFile;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaPlanRow;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.ImtsaStructure;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.parser.ImtsaParser;

import java.io.InputStream;
import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        model.setStage(Model.ImportStage.PICK_UP);
    }

    public void onClickParseImtsa(IBusinessComponent component)
    {
        Model model = getModel(component);
        InputStream inputStream = model.getSource().getStream();
        ImtsaParser parser = ImtsaParser.get(inputStream, model.isSecondGosGeneration());

        model.setEncoding(parser.getEncoding());
        getDao().saveImtsaXml(model);
        parser.parse();
        getDao().deleteImtsaImportLog(model.getId());
        if (parser.hasErrors())
        {
            StringBuilder errorBuilder = new StringBuilder();
            for (String error: parser.getErrors())
            {
                errorBuilder.append(error).append("\n");
            }


            UsmaImtsaImportLog log = new UsmaImtsaImportLog(model.getBlock(), errorBuilder.toString());
            getDao().save(log);
            model.setLog(log);

            if (!ImtsaParser.USMA_IMTSA_IMPORT_SIMPLE_MODE)
            {
                model.setErrorMessage(errorBuilder.toString());
                model.setStage(Model.ImportStage.ERROR);

                return;
            }
        }

        ImtsaFile imtsa = parser.getResult();
        model.setResult(imtsa);

        Map<String, String> abbreviation2TitleMap = imtsa.getTitle().getCycles().getAbbreviation2TitleMap();
        Map<String, Set<String>> cycle2PartsMap = new TreeMap<>();
        for (Map.Entry<String, ImtsaPlanRow> cycleEntry: imtsa.getRootPlanRow().getChildMap().entrySet())
        {
            String cycleId = cycleEntry.getKey();
            Set<String> parts = new TreeSet<>();
            addParts(cycleEntry.getValue().getChildMap(), cycleId, parts);
            cycle2PartsMap.put(cycleId, parts);
        }

        model.setCycles2PartsMap(cycle2PartsMap);
        model.setCycleTitlesMap(abbreviation2TitleMap);

        Map<String, EppPlanStructure> defaultPartsMap = new HashMap<>();
        for (EppPlanStructure planStructure: getDao().getParts(model))
        {
            defaultPartsMap.put(planStructure.getShortTitle(), planStructure);
        }

        Map<String, EppPlanStructure> defaultCyclesMap = new HashMap<>();
        for (UsmaImtsaCyclePlanStructureRel rel: getDao().getImtsaCycles(model))
        {
            defaultCyclesMap.put(rel.getImtsaCycle(), rel.getPlanStructure());
        }

        Map<String, EppPlanStructure> cyclesMap = new LinkedHashMap<>();
        Map<String, EppPlanStructure> partsMap = new LinkedHashMap<>();
        Map<String, String> cycleAbbreviation2TitleMap = model.getCycleTitlesMap();
        for (Map.Entry<String, Set<String>> cycleEntry: cycle2PartsMap.entrySet())
        {
            String cycleAbbr = cycleEntry.getKey();
            String cycleTitle = cycleAbbreviation2TitleMap.get(cycleAbbr);
            EppPlanStructure cycleValue = defaultCyclesMap.get(cycleTitle);
            if (cycleValue == null){ cycleValue = defaultCyclesMap.get(cycleAbbr); }
            cyclesMap.put(cycleAbbr, cycleValue);

            for (String part: cycleEntry.getValue())
            {
                partsMap.put(part, defaultPartsMap.get(part.substring(part.lastIndexOf(".") + 1)));
            }
        }

        model.setCyclesMap(cyclesMap);
        model.setPartsMap(partsMap);
        model.setStage(Model.ImportStage.CONFIG);
    }

    public static void addParts(Map<String, ImtsaPlanRow> rowMap, String parentId, Set<String> parts)
    {
        for (Map.Entry<String, ImtsaPlanRow> rowEntry: rowMap.entrySet())
        {
            ImtsaPlanRow row = rowEntry.getValue();
            if (row instanceof ImtsaStructure)
            {
                String rowId = parentId + ImtsaPlanRow.getIdSeparator() + rowEntry.getKey();
                parts.add(rowId);

                addParts(row.getChildMap(), rowId, parts);
            }
        }
    }

    public void onClickSaveImtsa(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.isSaveConfig())
        {
            getDao().updateCycleConfiguration(model);
        }

        List<String> errors = new ArrayList<>();
        IImtsaImportDAO.instance.get().saveImtsa(model.getBlock(), model.getResult(), model.getCyclesMap(), model.getPartsMap(), errors);
        if (!errors.isEmpty())
        {
            String error = StringUtils.join(new TreeSet<>(errors), "\n");
            UsmaImtsaImportLog log = model.getLog();
            if (log == null)
            {
                log = new UsmaImtsaImportLog(model.getBlock(), error);
                getDao().save(log);
                model.setLog(log);

            } else
            {
                String logMessage = StringUtils.trimToNull(log.getMessage());
                logMessage = logMessage == null ? error : logMessage + "\n" + error;
                log.setMessage(logMessage);

                getDao().update(log);
            }
        }

        deactivate(component);
    }
}