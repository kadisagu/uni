/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.nodeItem;

import java.util.List;
import java.util.Map;

/**
 * Данные xml-нода: атрибуты и данные дочерних нодов.
 * @author Alexander Zhebko
 * @since 26.07.2013
 */
public class NodeItem
{
    private Map<String, Object> attributes;
    private List<NodeItem> childNodes;

    public Map<String, Object> getAttributes(){ return attributes; }
    public void setAttributes(Map<String, Object> attributes){ this.attributes = attributes; }

    public List<NodeItem> getChildNodes(){ return childNodes; }
    public void setChildNodes(List<NodeItem> childNodes){ this.childNodes = childNodes; }
}