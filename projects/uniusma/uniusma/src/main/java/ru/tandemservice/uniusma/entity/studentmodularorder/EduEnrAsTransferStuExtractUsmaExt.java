package ru.tandemservice.uniusma.entity.studentmodularorder;

import ru.tandemservice.uniusma.entity.studentmodularorder.gen.*;

/**
 * Расширение выписки из сборного приказа по студенту. О зачислении на второй и последующие курсы в порядке перевода
 */
public class EduEnrAsTransferStuExtractUsmaExt extends EduEnrAsTransferStuExtractUsmaExtGen
{
}