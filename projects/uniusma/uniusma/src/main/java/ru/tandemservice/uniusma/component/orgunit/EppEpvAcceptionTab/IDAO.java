/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.orgunit.EppEpvAcceptionTab;

import org.tandemframework.core.settings.IDataSettings;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 27.01.2014
 */
public interface IDAO extends ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.IDAO
{
    /**
     * Возвращает список id строк УП, подходящих под критерии отбора.
     * @param orgUnitId id читающего подразделения.
     * @param settings настрйоки пользователя
     * @return список id строк УП
     */
    public List<Long> getRegistryRowIds(Long orgUnitId, IDataSettings settings);
}
