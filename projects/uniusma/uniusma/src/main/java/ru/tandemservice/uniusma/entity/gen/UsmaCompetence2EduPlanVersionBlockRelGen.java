package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь компетенции с блоком УП(в)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaCompetence2EduPlanVersionBlockRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel";
    public static final String ENTITY_NAME = "usmaCompetence2EduPlanVersionBlockRel";
    public static final int VERSION_HASH = -2052949418;
    private static IEntityMeta ENTITY_META;

    public static final String L_USMA_COMPETENCE = "usmaCompetence";
    public static final String L_BLOCK = "block";
    public static final String P_NUMBER = "number";

    private UsmaCompetence _usmaCompetence;     // Компетенция(УГМА)
    private EppEduPlanVersionBlock _block;     // Блок УП(в)
    private int _number;     // Номер компетенции

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     */
    @NotNull
    public UsmaCompetence getUsmaCompetence()
    {
        return _usmaCompetence;
    }

    /**
     * @param usmaCompetence Компетенция(УГМА). Свойство не может быть null.
     */
    public void setUsmaCompetence(UsmaCompetence usmaCompetence)
    {
        dirty(_usmaCompetence, usmaCompetence);
        _usmaCompetence = usmaCompetence;
    }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УП(в). Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Номер компетенции. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер компетенции. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaCompetence2EduPlanVersionBlockRelGen)
        {
            setUsmaCompetence(((UsmaCompetence2EduPlanVersionBlockRel)another).getUsmaCompetence());
            setBlock(((UsmaCompetence2EduPlanVersionBlockRel)another).getBlock());
            setNumber(((UsmaCompetence2EduPlanVersionBlockRel)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaCompetence2EduPlanVersionBlockRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaCompetence2EduPlanVersionBlockRel.class;
        }

        public T newInstance()
        {
            return (T) new UsmaCompetence2EduPlanVersionBlockRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "usmaCompetence":
                    return obj.getUsmaCompetence();
                case "block":
                    return obj.getBlock();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "usmaCompetence":
                    obj.setUsmaCompetence((UsmaCompetence) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "usmaCompetence":
                        return true;
                case "block":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "usmaCompetence":
                    return true;
                case "block":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "usmaCompetence":
                    return UsmaCompetence.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaCompetence2EduPlanVersionBlockRel> _dslPath = new Path<UsmaCompetence2EduPlanVersionBlockRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaCompetence2EduPlanVersionBlockRel");
    }
            

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel#getUsmaCompetence()
     */
    public static UsmaCompetence.Path<UsmaCompetence> usmaCompetence()
    {
        return _dslPath.usmaCompetence();
    }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Номер компетенции. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends UsmaCompetence2EduPlanVersionBlockRel> extends EntityPath<E>
    {
        private UsmaCompetence.Path<UsmaCompetence> _usmaCompetence;
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<Integer> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel#getUsmaCompetence()
     */
        public UsmaCompetence.Path<UsmaCompetence> usmaCompetence()
        {
            if(_usmaCompetence == null )
                _usmaCompetence = new UsmaCompetence.Path<UsmaCompetence>(L_USMA_COMPETENCE, this);
            return _usmaCompetence;
        }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Номер компетенции. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UsmaCompetence2EduPlanVersionBlockRelGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return UsmaCompetence2EduPlanVersionBlockRel.class;
        }

        public String getEntityName()
        {
            return "usmaCompetence2EduPlanVersionBlockRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
