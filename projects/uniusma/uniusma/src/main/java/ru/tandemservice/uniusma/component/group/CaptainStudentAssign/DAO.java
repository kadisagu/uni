/* $Id:$ */
package ru.tandemservice.uniusma.component.group.CaptainStudentAssign;

import ru.tandemservice.uni.component.group.CaptainStudentAssign.Model;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.exception.ApplicationException;

import java.util.List;

/**
 * @author Denis Perminov
 * @since 25.03.2014
 */
public class DAO extends ru.tandemservice.uni.component.group.CaptainStudentAssign.DAO
{
    @Override
    public void countCaptainStudentValidate(Model model)
    {
        // DEV-4545
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(model.getGroup());
        if (model.getCaptainStudentOldList().size() != captainList.size())
            throw new ApplicationException("Староста уже назначен другим пользователем.");
    }
}
