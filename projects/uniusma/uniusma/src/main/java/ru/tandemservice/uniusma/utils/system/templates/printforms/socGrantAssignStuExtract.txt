{orderType}\par\par
\fi709\qj {reason} {orgUnit_G}\par\fi0\par
приказываю:\par
\keep\keepn\fi709\qj 
1. Назначить {student_D} {course} курса {orgUnit_G} (группа {group}) {developForm_GF} формы обучения {fio} {birthDate} года рождения, социальную стипендию с {beginDate} г. по {endDate} г.\par\par 
\fi709\qj Основание: {listBasics}\par\fi0