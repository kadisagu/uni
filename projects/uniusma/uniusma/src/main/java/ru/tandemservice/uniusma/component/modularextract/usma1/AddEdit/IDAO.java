/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public interface IDAO  extends ICommonModularStudentExtractAddEditDAO<UsmaExcludeAfterAcadWeekendStuExtract, Model>
{
}
