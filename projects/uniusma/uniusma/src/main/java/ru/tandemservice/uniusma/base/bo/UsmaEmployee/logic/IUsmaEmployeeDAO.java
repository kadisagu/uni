/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEmployee.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 16.07.12
 */
public interface IUsmaEmployeeDAO extends INeedPersistenceSupport
{
    /**
     * Вычесляет дату начала непрерывной работы Кадровго ресурса.
     * @param employeePost Сотрудник
     * @return дату начала работы
     */
    Date getStartOrgUnitDate(EmployeePost employeePost);

    /**
     * Вычесляет дату и номер приказа о назначении Кадровго ресурса на Должность, начиная с которой работа не прерывалась.
     * @param employeePost Сотрудник
     * @return пара с датой и номером приказа о назначении
     */
    CoreCollectionUtils.Pair<Date, String> getStartOrgUnitOrderDateNumber(EmployeePost employeePost);

    /**
     * Вычесляет Должность, начиная с которой работа не прерывалась у указанного Сотрудника.
     * @param employeePost Сотрудник
     * @return Сотрудник
     */
    EmployeePost getStartOrgUnitEmployeePost(EmployeePost employeePost);
}
