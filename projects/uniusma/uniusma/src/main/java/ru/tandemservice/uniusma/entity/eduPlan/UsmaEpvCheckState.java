package ru.tandemservice.uniusma.entity.eduPlan;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * Состояние проверки УПв
 */
public class UsmaEpvCheckState extends UsmaEpvCheckStateGen
{
    public UsmaEpvCheckState()
    {

    }

    public UsmaEpvCheckState(EppEduPlanVersion version)
    {
        this.setVersion(version);
    }
}