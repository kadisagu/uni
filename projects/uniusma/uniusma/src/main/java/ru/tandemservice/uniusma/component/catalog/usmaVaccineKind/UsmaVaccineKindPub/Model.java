/*$Id$*/
package ru.tandemservice.uniusma.component.catalog.usmaVaccineKind.UsmaVaccineKindPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author DMITRY KNYAZEV
 * @since 21.06.2014
 */
public class Model extends DefaultCatalogPubModel<UsmaVaccineKind>
{
}
