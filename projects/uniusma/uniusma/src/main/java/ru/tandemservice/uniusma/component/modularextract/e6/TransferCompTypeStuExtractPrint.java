/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e6;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferCompTypeStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.TransferCompTypeStuExtractUsmaExtGen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class TransferCompTypeStuExtractPrint implements IPrintFormCreator<TransferCompTypeStuExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, TransferCompTypeStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();
        short i = 1;

        modifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTransferDate()));

        TransferCompTypeStuExtractUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new TransferCompTypeStuExtractUsmaExtGen.NaturalId(extract));
        if (null != extractExt && extractExt.isStartGrantsPaying())
        {
            modifier.put("startGrantPayingStr", String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " назначить выплату академической стипендии с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getStartGrantsPayingDate()) + " г.");
        }
        else
            tagsToDel.add("startGrantPayingStr");

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}
