package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaDeveloper;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Разработчик
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaDeveloperGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaDeveloper";
    public static final String ENTITY_NAME = "usmaDeveloper";
    public static final int VERSION_HASH = 776615186;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String P_NUMBER = "number";
    public static final String P_FIO = "fio";
    public static final String P_POST = "post";

    private EppEduPlanVersionBlock _block;     // Блок УПв
    private int _number;     // Номер
    private String _fio;     // ФИО
    private String _post;     // Должность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return ФИО. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio ФИО. Свойство не может быть null.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность. Свойство не может быть null.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaDeveloperGen)
        {
            setBlock(((UsmaDeveloper)another).getBlock());
            setNumber(((UsmaDeveloper)another).getNumber());
            setFio(((UsmaDeveloper)another).getFio());
            setPost(((UsmaDeveloper)another).getPost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaDeveloperGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaDeveloper.class;
        }

        public T newInstance()
        {
            return (T) new UsmaDeveloper();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "number":
                    return obj.getNumber();
                case "fio":
                    return obj.getFio();
                case "post":
                    return obj.getPost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "number":
                        return true;
                case "fio":
                        return true;
                case "post":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "number":
                    return true;
                case "fio":
                    return true;
                case "post":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "number":
                    return Integer.class;
                case "fio":
                    return String.class;
                case "post":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaDeveloper> _dslPath = new Path<UsmaDeveloper>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaDeveloper");
    }
            

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return ФИО. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    public static class Path<E extends UsmaDeveloper> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _fio;
        private PropertyPath<String> _post;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UsmaDeveloperGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return ФИО. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(UsmaDeveloperGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaDeveloper#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(UsmaDeveloperGen.P_POST, this);
            return _post;
        }

        public Class getEntityClass()
        {
            return UsmaDeveloper.class;
        }

        public String getEntityName()
        {
            return "usmaDeveloper";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
