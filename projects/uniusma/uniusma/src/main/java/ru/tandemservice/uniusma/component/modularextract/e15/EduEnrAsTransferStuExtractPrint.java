/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e15;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.EduEnrAsTransferStuExtractUsmaExtGen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 27.05.2014
 */
public class EduEnrAsTransferStuExtractPrint implements IPrintFormCreator<EduEnrAsTransferStuExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, EduEnrAsTransferStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();
        short number = 1;

        EduEnrAsTransferStuExtractUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new EduEnrAsTransferStuExtractUsmaExtGen.NaturalId(extract));
        modifier.put("protocolNumber", (null != extractExt) ? extractExt.getProtocolNumber() : "");
        modifier.put("protocolDate", (null != extractExt) ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getProtocolDate()) : "");

        modifier.put("entryIntoForceDate", (null != extract.entryDate()) ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEntryDate()) : "");

        RtfTableModifier table = new RtfTableModifier();
        if (extract.isHasDebts())
        {
            modifier.put("debts", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline()) + " г.");

            short i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for (StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[]{String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        }
        else
        {
            tagsToDel.add("debts");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        table.modify(document);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}
