/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.Validator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 11.09.2013
 */
public class ImtsaAttestation
{
    // представления
    public static final IAttributeView ATTESTATION_ATTRIBUTE_VIEW = new AttestationAttributeView();

    // загружаемые данные
    private Double _dissertationZetInWeek;
    private Double _stateExamZetInWeek;

    public Double getDissertationZetInWeek(){ return _dissertationZetInWeek; }
    public void setDissertationZetInWeek(Double dissertationZetInWeek){ _dissertationZetInWeek = dissertationZetInWeek; }

    public Double getStateExamZetInWeek(){ return _stateExamZetInWeek; }
    public void setStateExamZetInWeek(Double stateExamZetInWeek){ _stateExamZetInWeek = stateExamZetInWeek; }


    // вспомогательные данные
    public static String getAttestationZetInWeekAttribute(){ return AttestationAttributeView.ZET_IN_WEEK; }
    public static String getAttestationHoursInZetAttribute(){ return AttestationAttributeView.HOURS_AMOUNT_IN_ZET; }

    public static String getDissertationNodeName(){ return DISSERTATION_NODE_NAME; }
    public static String getStateExamNodeName(){ return STATE_EXAM_NODE_NAME; }


    /**
     * Представление атрибутов нода "ДиссертацияМагистров"/"ГосЭкзаменыМагистров".
     */
    private static class AttestationAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String HOURS_AMOUNT_IN_ZET = "ЧасовВЗЕТ";
        private static final String ZET_IN_WEEK = "ЗЕТвНеделе";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(HOURS_AMOUNT_IN_ZET, Double.class);
            ATTRIBUTE_TYPE_MAP.put(ZET_IN_WEEK, Double.class);
        }

        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Collections.emptyList();


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> { return Collections.<Validator<?>>emptyList(); });
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(HOURS_AMOUNT_IN_ZET, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(ZET_IN_WEEK, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Collections.emptyList();
    }


    // ноды
    private static final String DISSERTATION_NODE_NAME = "ДиссертацияМагистров";
    private static final String STATE_EXAM_NODE_NAME = "ГосЭкзаменыМагистров";
}