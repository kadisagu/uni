package ru.tandemservice.uniusma;

/**
 * @author oleyba
 * @since 24.11.2009
 */
public interface UniusmaDefines
{
    String TEMPLATE_BULLETIN_EXAM_TEST_CODE = "examAndTestBulletin"; // шаблон ведомости на экзамен и зачет одновременно
    String TEMPLATE_MED_CARD = "studentMedCard"; // шаблон выписки для здравпункта
    String TEMPLATE_MED_CARD_REPORT = "studentMedCardReport"; // шаблон выборки для здравпункта

    /* Формы текущего контроля */
    public static final String CONTROL_ACTION_GRAPH_WORK = "22";

    /* Типы учебных недель ИМЦА */
    char IMTSA_EXAMINATION_SESSION = 'Э';
    char IMTSA_TEACH_PRACTICE = 'У';
    char IMTSA_RESEARCH_WORK = 'Н';
    char IMTSA_WORK_PRACTICE = 'П';
    char IMTSA_QUALIFICATION_WORK = 'Д';
    char IMTSA_STATE_EXAMINATION = 'Г';
    char IMTSA_HOLIDAYS = 'К';
    char IMTSA_UNUSED = '=';
    char IMTSA_THEORY = 'Т';
    char IMTSA_ATTESTATION = 'А';
    char IMTSA_IGNORE = '-';
}
