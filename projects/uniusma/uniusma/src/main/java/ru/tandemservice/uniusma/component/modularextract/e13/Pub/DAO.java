/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e13.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.WeekendChildStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 22.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e13.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e13.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        WeekendChildStuExtractUsmaExt extractExt = getByNaturalId(new WeekendChildStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        m.setFreeAttendance(null != extractExt && extractExt.isFreeAttendance());
    }
}
