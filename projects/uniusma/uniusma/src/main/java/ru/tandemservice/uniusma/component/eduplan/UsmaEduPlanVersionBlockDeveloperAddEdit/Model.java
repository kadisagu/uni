/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockDeveloperAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniusma.entity.UsmaDeveloper;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input({
    @Bind(key = "developerId", binding = "developerId"),
    @Bind(key = "blockId", binding = "blockId")
})
public class Model
{
    private Long _developerId;
    private Long _blockId;
    private UsmaDeveloper _developer;
    private Integer _number;

    public Long getDeveloperId()
    {
        return _developerId;
    }

    public void setDeveloperId(Long developerId)
    {
        _developerId = developerId;
    }

    public Long getBlockId()
    {
        return _blockId;
    }

    public void setBlockId(Long blockId)
    {
        _blockId = blockId;
    }

    public UsmaDeveloper getDeveloper()
    {
        return _developer;
    }

    public void setDeveloper(UsmaDeveloper developer)
    {
        _developer = developer;
    }

    public Integer getNumber()
    {
        return _number;
    }

    public void setNumber(Integer number)
    {
        _number = number;
    }
}