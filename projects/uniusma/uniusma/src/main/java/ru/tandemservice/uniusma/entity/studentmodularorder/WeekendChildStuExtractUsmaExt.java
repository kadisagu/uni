package ru.tandemservice.uniusma.entity.studentmodularorder;

import ru.tandemservice.uniusma.entity.studentmodularorder.gen.*;

/**
 * Расширение выписки из сборного приказа по студенту. О предоставлении отпуска по уходу за ребенком до достижения им возраста 3-х лет
 */
public class WeekendChildStuExtractUsmaExt extends WeekendChildStuExtractUsmaExtGen
{
}