/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public interface IDAO extends IModularStudentExtractPubDAO<UsmaExcludeEduPlanStuExtract, Model>
{
}
