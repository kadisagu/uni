/*$Id$*/
package ru.tandemservice.uniusma.base.ext.UniStudent.ui.OrgUnitList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitList;

/**
 * @author DMITRY KNYAZEV
 * @since 08.02.2016
 */
@Configuration
public class UniStudentOrgUnitListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniusma" + UniStudentOrgUnitListExtUI.class.getSimpleName();

    @Autowired
    private UniStudentOrgUnitList uniStudentOrgUnitList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(uniStudentOrgUnitList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, UniStudentOrgUnitListExtUI.class))
                .create();
    }
}
