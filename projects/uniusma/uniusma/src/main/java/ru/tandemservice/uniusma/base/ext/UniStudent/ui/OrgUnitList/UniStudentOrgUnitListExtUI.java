/*$Id$*/
package ru.tandemservice.uniusma.base.ext.UniStudent.ui.OrgUnitList;

import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitListUI;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;
import ru.tandemservice.uniusma.entity.catalog.codes.UniScriptItemCodes;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author DMITRY KNYAZEV
 * @since 08.02.2016
 */
public class UniStudentOrgUnitListExtUI extends UIAddon
{
    private String printStudentPersonCardTitlesKey;
    private String printStudentPersonCardDatasKey;

    public UniStudentOrgUnitListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        printStudentPersonCardTitlesKey = "orgUnit_printStudentPersonCardTitles_" + ((isArchival()) ? "archival_" : "") + getOrgUnit().getOrgUnitType().getCode();
        printStudentPersonCardDatasKey = "orgUnit_printStudentPersonCardDatas_" + ((isArchival()) ? "archival_" : "") + getOrgUnit().getOrgUnitType().getCode();
    }

    public void onClickPrintStudentPersonCardTitles()
    {
        List<Student> studentList = getParent().getConfig().<BaseSearchListDataSource>getDataSource(UniStudentOrgUnitList.STUDENT_SEARCH_LIST_DS).getRecords();
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final IDocumentRenderer documentRenderer = this.getDocumentRenderer(UniBaseDao.ids(studentList), UniScriptItemCodes.STUDENT_PERSONAL_CARD_TITLE_PRINT_SCRIPT);
            BusinessComponentUtils.downloadDocument(documentRenderer, true);
        } else
        {
            getActivationBuilder().asDesktopRoot(IUniComponents.STUDENT_PERSON_CARDS_ORG_UNIT_LIST)
                    .parameter("studentIds", UniBaseDao.ids(studentList))
                    .activate();
        }

    }

    public void onClickPrintStudentPersonCardDatas()
    {
        List<Student> studentList = getParent().getConfig().<BaseSearchListDataSource>getDataSource(UniStudentOrgUnitList.STUDENT_SEARCH_LIST_DS).getRecords();
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled())
        {
            final IDocumentRenderer documentRenderer = this.getDocumentRenderer(UniBaseDao.ids(studentList), UniScriptItemCodes.STUDENT_PERSONAL_CARD_DATA_PRINT_SCRIPT);
            BusinessComponentUtils.downloadDocument(documentRenderer, true);
        } else
        {
            getActivationBuilder().asDesktopRoot(IUniComponents.STUDENT_PERSON_CARDS_ORG_UNIT_LIST)
                    .parameter("studentIds", UniBaseDao.ids(studentList))
                    .activate();
        }

    }

    //Getters/Setters
    private UniStudentOrgUnitListUI getParent()
    {
        return getPresenter();
    }

    private OrgUnit getOrgUnit()
    {
        return getParent().getOrgUnit();
    }

    private boolean isArchival()
    {
        return getParent().isArchival();
    }

    public String getPrintStudentPersonCardTitlesKey()
    {
        return printStudentPersonCardTitlesKey;
    }

    public String getPrintStudentPersonCardDatasKey()
    {
        return printStudentPersonCardDatasKey;
    }

    //Document printer
    private IDocumentRenderer getDocumentRenderer(final Collection<Long> ids, final String scriptType)
    {
        final Session session = getSession();
        final Long NULL = (long) 0;

        final Map<Long, Collection<Long>> group2studentIdsMap = new HashMap<>();
        BatchUtils.execute(ids, 200, ids1 -> {
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(Student.class, "s");
            dql.column(DQLExpressions.property(Student.id().fromAlias("s")));
            dql.column(DQLExpressions.property(Student.group().id().fromAlias("s")));
            dql.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), ids1));
            for (Object[] row : dql.createStatement(session).<Object[]>list())
            {
                SafeMap.safeGet(group2studentIdsMap, (null == row[1] ? NULL : (Long) row[1]), ArrayList.class).add((Long) row[0]);
            }
        });

        if (group2studentIdsMap.isEmpty())
        {
            throw new ApplicationException("Список студентов пуст.");
        }

        final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, scriptType);

        //final RtfDocument template = new RtfReader().read(script.getTemplate());

        try
        {
            int i = 1;
            ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
            BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

            ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
            zipOut.setLevel(Deflater.BEST_COMPRESSION);

            for (Map.Entry<Long, Collection<Long>> entry : group2studentIdsMap.entrySet())
            {

                final Group group = (NULL.equals(entry.getKey()) ? null : (Group) session.get(Group.class, entry.getKey()));

                final RtfDocument resultDoc = new RtfDocument();

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
                builder.add(MQExpression.in("s", "id", entry.getValue()));
                builder.addLeftJoinFetch("s", Student.L_GROUP, "g");
                builder.addJoinFetch("s", Student.L_PERSON, "p");
                builder.addJoinFetch("p", Person.L_IDENTITY_CARD, "idCard");
                builder.addJoinFetch("s", Student.L_STATUS, "studentStatus");
                builder.addJoinFetch("s", Student.L_EDUCATION_ORG_UNIT, "ou");
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME);

                final List<Student> studentList = builder.getResultList(session);

                final Map<Long, List<org.tandemframework.shared.person.base.entity.PersonEduInstitution>> personEduInstitutionMap = new HashMap<>();
                final Map<Long, List<PersonNextOfKin>> personNextOfKinMap = new HashMap<>();
                {
                    MQBuilder personIdsBuilder = new MQBuilder(Student.ENTITY_CLASS, "s", new String[]{Student.person().id().s()});
                    personIdsBuilder.add(MQExpression.in("s", "id", entry.getValue()));
                    {
                        MQBuilder eduInstitutionsBuilder = new MQBuilder(org.tandemframework.shared.person.base.entity.PersonEduInstitution.ENTITY_CLASS, "ei");
                        eduInstitutionsBuilder.add(MQExpression.in("ei", org.tandemframework.shared.person.base.entity.PersonEduInstitution.person().id().s(), personIdsBuilder));
                        for (org.tandemframework.shared.person.base.entity.PersonEduInstitution eduInstitution : eduInstitutionsBuilder.<org.tandemframework.shared.person.base.entity.PersonEduInstitution>getResultList(getSession()))
                        {
                            SafeMap.safeGet(personEduInstitutionMap, eduInstitution.getPerson().getId(), ArrayList.class).add(eduInstitution);
                        }
                    }
                    {
                        MQBuilder nextOfKinBuilder = new MQBuilder(PersonNextOfKin.ENTITY_CLASS, "nok");
                        nextOfKinBuilder.add(MQExpression.in("nok", PersonNextOfKin.person().id().s(), personIdsBuilder));
                        for (PersonNextOfKin kextOfKin : nextOfKinBuilder.<PersonNextOfKin>getResultList(getSession()))
                        {
                            SafeMap.safeGet(personNextOfKinMap, kextOfKin.getPerson().getId(), ArrayList.class).add(kextOfKin);
                        }
                    }
                }


                for (Student student : studentList)
                {

                    final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, student.getId());
                    final RtfDocument document = (RtfDocument) scriptResult.get("rtf");
                    resultDoc.addElement(document);
                    if (studentList.indexOf(student) < studentList.size() - 1)
                        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                    else
                    {
                        resultDoc.setSettings(document.getSettings());
                        resultDoc.setHeader(document.getHeader());
                    }
                }

                final String fileName = "student-cards-" + StudentNumberFormatter.INSTANCE.format(i++) + "-" + CoreStringUtils.transliterate((null == group ? "no-group" : group.getTitle()) + ".rtf");
                if (1 == group2studentIdsMap.size())
                {
                    // если группа одна - то возвращаем rtf
                    return new CommonBaseRenderer().rtf().fileName(fileName).document(resultDoc);
                }

                final ByteArrayOutputStream groupStream = new ByteArrayOutputStream(1024);
                final BufferedOutputStream groupBuffer = new BufferedOutputStream(groupStream, 1024);
                new CommonBaseRenderer().rtf().fileName(fileName).document(resultDoc).render(groupBuffer);
                groupBuffer.close();
                groupStream.close();

                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.write(groupStream.toByteArray());
                zipOut.closeEntry();
                session.clear();
            }

            zipOut.close();
            zipBuffer.close();

            String fileName = "student-cards.zip";
            switch (scriptType)
            {
                case UniScriptItemCodes.STUDENT_PERSONAL_CARD_TITLE_PRINT_SCRIPT:
                {
                    fileName = "student-cards-title.zip";
                    break;
                }
                case UniScriptItemCodes.STUDENT_PERSONAL_CARD_DATA_PRINT_SCRIPT:
                {
                    fileName = "student-cards-data.zip";
                }
            }
            return new CommonBaseRenderer().zip().fileName(fileName).document(zipStream);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
