/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.parser;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniusma.UniusmaDefines;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.*;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow.*;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.nodeItem.NodeItem;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.nodeItem.NodeItemAdvanced;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.translator.Translators;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.Validator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Парсер xml-документа ИМЦА.
 *
 * @author Alexander Zhebko
 * @since 08.07.2013
 */
public abstract class ImtsaParser
{
    /* "простой режим ДВФУ" - импортируем как можно все, остальное добавляют руками */ /* на основании "простого режима" будет со временем дорабатываться основной */
    public static final boolean USMA_IMTSA_IMPORT_SIMPLE_MODE = Boolean.valueOf(ApplicationRuntime.getProperty("usmaImtsaImportSimpleMode"));

    private final Document _document;
    private ImtsaFile _result;
    private boolean _completed = false;

    /**
     * Создает парсер, используя xml-документ.
     * @param document xml-документ
     */
    protected ImtsaParser(Document document)
    {
        _document = document;
    }

    /**
     * Создает парсер, используя входящий поток байтов.
     * @param inputStream входящий поток данных
     */
    protected ImtsaParser(InputStream inputStream)
    {
        Document document;
        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(inputStream);
            document.getDocumentElement().normalize();

            _document = document;
        }
        catch (Exception e)
        {
            throw new ApplicationException("Неверная структура xml в файле».");
        }
    }


    /**
     * Возвращает файл ИМЦА - результат парсинга.
     * @return результат парсинга.
     * @throws IllegalStateException если парсинг не отработал, или отработал с ошибкой
     */
    public ImtsaFile getResult() throws IllegalStateException
    {
        if (_result == null)
        {
            throw new IllegalStateException("Парсинг не был завершен.");
        }

        return _result;
    }


    /**
     * Показывает, произошли ли ошибки во время парсинга.
     * @return true если ошибки были, false - иначе
     */
    public boolean hasErrors()
    {
        if (!_completed)
        {
            throw new IllegalStateException("Парсинг не был завершен.");
        }

        return _localErrors.size() > 0;
    }

    private List<String> _errors;

    /**
     * Возвращает список всех ошибок, произошедших во впемя парсинга файла ИМЦА.
     * @return список ошибок.
     */
    public List<String> getErrors()
    {
        if (!_completed)
        {
            throw new IllegalStateException("Парсинг не был завершен.");
        }

        if (_errors == null)
        {
            _errors = Collections.unmodifiableList(_localErrors);
        }

        return _errors;
    }

    /**
     * Кодировка источника.
     * @return кодировку
     */
    public String getEncoding()
    {
        if (_document == null)
        {
            throw new IllegalStateException("Нет источника.");
        }

        return _document.getXmlEncoding();
    }


    /**
     * Парсит xml документ, выделяет необходимые для импорта данные
     */
    public void parse()
    {
        ImtsaTitle title = parseTitle();
        ImtsaCompetences competences = parseCompetences();

        Collection<String> cycles = title == null ? null : title.getCycles().getAbbreviation2TitleMap().keySet();

        Collection<String> undistributedCompetenceIndexes = competences == null ? null : competences.getUndistributedCompetenceIndexes();
        Map<String, String> compCode2IndexMap = competences == null ? null : competences.getCompetenceCode2IndexMap();

        Map<Integer, Integer> minCoursePartNumberMap = new HashMap<>();
        Map<Integer, Integer> maxCoursePartNumberMap = new HashMap<>();
        ImtsaPlanRow rootPlanRow = parsePlanRows(cycles, compCode2IndexMap, minCoursePartNumberMap, maxCoursePartNumberMap, undistributedCompetenceIndexes);
        Map<String, Collection<ImtsaPractice>> practiceMap = parsePractices();

        setStage(ParseStage.FINISH);
        Double dissertationLaborSize = 0.0d;
        Double stateExamLaborSize = 0.0d;
        Map<Integer, Map<Integer, PairKey<Double, Double>>> dissertationWeekTypeMap = SafeMap.get(HashMap.class);
        Map<Integer, Map<Integer, PairKey<Double, Double>>> stateExamWeekTypeMap = SafeMap.get(HashMap.class);

        Double commonZetInWeek = title == null || title.getAttributes() == null ? null : (Double) title.getAttributes().get(ImtsaTitle.getZetInWeeksAttAttribute());
        /* todo перенести в парсер титула */
        if (commonZetInWeek == null)
        {
            commonZetInWeek = ImtsaTitle.getDefaultZetInWeeksAtt();
        }

        Double commonHoursInZet = title == null || title.getAttributes() == null ? null : (Double) title.getAttributes().get(ImtsaTitle.getHoursInZetAttAttribute());
        if (commonHoursInZet == null)
        {
            commonHoursInZet = ImtsaTitle.getDefaultHoursInZetAtt();
        }

        Map<String, Map<String, Double>> attestationMap = parseAttestation();
        Double dissertationZetInWeek = attestationMap.get(ImtsaAttestation.getDissertationNodeName()).get(ImtsaAttestation.getAttestationZetInWeekAttribute());
        Double stateExamZetInWeek = attestationMap.get(ImtsaAttestation.getStateExamNodeName()).get(ImtsaAttestation.getAttestationZetInWeekAttribute());

        if (dissertationZetInWeek == null){ dissertationZetInWeek = commonZetInWeek; }
        if (stateExamZetInWeek == null){ stateExamZetInWeek = commonZetInWeek; }

        Double dissertationHoursInZet = attestationMap.get(ImtsaAttestation.getDissertationNodeName()).get(ImtsaAttestation.getAttestationHoursInZetAttribute());
        Double stateExamHoursInZet = attestationMap.get(ImtsaAttestation.getStateExamNodeName()).get(ImtsaAttestation.getAttestationHoursInZetAttribute());

        if (dissertationHoursInZet == null){ dissertationHoursInZet = commonHoursInZet; }
        if (stateExamHoursInZet == null){ stateExamHoursInZet = commonHoursInZet; }

        Map<Integer, Map<Integer, Map<Integer, String>>> scheduleMap = title == null || title.getSchedule() == null ? null : title.getSchedule().getCourseTermsScheduleMap();
        if (scheduleMap != null)
        {
            int parts = 1;
            for (Map.Entry<Integer, Map<Integer, Map<Integer, String>>> courseEntry: scheduleMap.entrySet())
            {
                for (Map.Entry<Integer, Map<Integer, String>> termEntry: courseEntry.getValue().entrySet())
                {
                    boolean split = termEntry.getValue().size() > 1;
                    if (split){ parts = termEntry.getValue().size(); }

                    Integer termDissertationWeeks = 0;
                    Integer termStateExamWeeks = 0;
                    for (Map.Entry<Integer, String> partEntry: termEntry.getValue().entrySet())
                    {
                        String schedule = partEntry.getValue();
                        Map<Character, Integer> countWeekTypeMap = getCountWeekTypeMap(schedule);

                        Integer dissertationWeeks = countWeekTypeMap.get(ImtsaFile.getDissertationWeekType());
                        Integer stateExamWeeks = countWeekTypeMap.get(ImtsaFile.getStateExamWeekType());

                        if (dissertationWeeks == null){ dissertationWeeks = 0; }
                        if (stateExamWeeks == null){ stateExamWeeks = 0; }

                        termDissertationWeeks += dissertationWeeks;
                        termStateExamWeeks += stateExamWeeks;
                    }

                    double dissertationZet = dissertationZetInWeek * (split ? ((double) termDissertationWeeks / parts) : (double) termDissertationWeeks);
                    double dissertationHours = dissertationZet * dissertationHoursInZet;
                    double stateExamZet = stateExamZetInWeek * (split ? ((double) termStateExamWeeks / parts) : (double) termStateExamWeeks);
                    double stateExamHours = stateExamZet * stateExamHoursInZet;

                    dissertationLaborSize += dissertationZet;
                    stateExamLaborSize += stateExamZet;

                    dissertationWeekTypeMap.get(courseEntry.getKey()).put(termEntry.getKey(), PairKey.create(dissertationZet, dissertationHours));
                    stateExamWeekTypeMap.get(courseEntry.getKey()).put(termEntry.getKey(), PairKey.create(stateExamZet, stateExamHours));
                }
            }

            dissertationWeekTypeMap.get(0).put(0, PairKey.create(dissertationLaborSize, dissertationLaborSize * dissertationHoursInZet));
            stateExamWeekTypeMap.get(0).put(0, PairKey.create(stateExamLaborSize, stateExamLaborSize * stateExamHoursInZet));
        }

        Set<String> additionCompetenceCodes = null;
        Node additionalCompetenceNode = getUniqueNodeByTitleAndParent(ImtsaFile.getAdditionalCompetenceNodeName(), ImtsaFile.getPlanNodeName(), false);
        if (additionalCompetenceNode != null)
        {
            Map<String, Object> attributes = getNodeAttributes(additionalCompetenceNode, IAttributeView.EMPTY);
            String competenceCodeStr;
            if (attributes != null && (competenceCodeStr = (String) attributes.get(ImtsaFile.getAttestationCompetenceAttribute())) != null)
            {
                additionCompetenceCodes = new HashSet<>(Arrays.asList(StringUtils.split(competenceCodeStr, ImtsaDiscipline.getCompetenceCodesSeparator())));
            }
        }

        if (compCode2IndexMap != null && undistributedCompetenceIndexes != null && additionCompetenceCodes != null)
        {
            for (String code: additionCompetenceCodes)
            {
                String index = compCode2IndexMap.get(code);
                if (index != null || !USMA_IMTSA_IMPORT_SIMPLE_MODE)
                    undistributedCompetenceIndexes.remove(index);
            }
        }

        if (competences != null && rootPlanRow != null && !undistributedCompetenceIndexes.isEmpty())
        {
            Map<String, PairKey<String, Integer>> compIndex2GroupAndNumberMap = competences.getCompetenceIndex2GroupCodeAndNumberMap();
            Map<String, Set<Integer>> undistributedCompMap = SafeMap.get(TreeSet.class);
            for (String index: undistributedCompetenceIndexes)
            {
                PairKey<String, Integer> key = compIndex2GroupAndNumberMap.get(index);
                if (USMA_IMTSA_IMPORT_SIMPLE_MODE && key == null)
                {
                    continue;
                }

                undistributedCompMap.get(key.getFirst()).add(key.getSecond());
            }

            for (Map.Entry<String, Set<Integer>> entry: undistributedCompMap.entrySet())
            {
                int size = entry.getValue().size();
                Integer[] numbers = entry.getValue().toArray(new Integer[size]);
                int maxNumber = competences.getCompGroupSize().get(entry.getKey());
                int minNumber = maxNumber - size + 1;
                if (numbers[0] != minNumber || numbers[size - 1] != maxNumber)
                {
                    registerError("Нераспределены компетенции в группе " + entry.getKey() + ".");
                }
            }
        }

        ImtsaFile result = new ImtsaFile();
        result.setTitle(title);
        result.setCompetences(competences);
        result.setRootPlanRow(rootPlanRow);
        result.setPracticeMap(practiceMap);

        result.setDissertationLaborMap(dissertationWeekTypeMap);
        result.setStateExamLaborMap(stateExamWeekTypeMap);

        result.setAttestationCompetenceCodes(additionCompetenceCodes);
        result.setMinCoursePartNumberMap(minCoursePartNumberMap);
        result.setMaxCoursePartNumberMap(maxCoursePartNumberMap);

        _result = _localErrors.isEmpty() || USMA_IMTSA_IMPORT_SIMPLE_MODE ? result : null;
        _completed = true;
    }

    /**
     * Считывает титул: атрибуты титула, разработчики, квалификации, специальности учебного плана, циклы, учебный график.
     * @return титул
     */
    private ImtsaTitle parseTitle()
    {
        setStage(ParseStage.TITLE);
        boolean errors = false;
        Node titleNode = getUniqueNodeByTitleAndParent(ImtsaFile.getTitleNodeName(), ImtsaFile.getPlanNodeName());
        if (titleNode == null && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        // атрибуты титула
        Map<String, Object> attributes = null;

        // разработчики, квалификации, специальности
        List<ImtsaDeveloper> developerList = new ArrayList<>();
        List<ImtsaQualification> qualificationList = new ArrayList<>();
        List<ImtsaSpeciality> specialityList = new ArrayList<>();

        ImtsaCycles cycles = null;
        ImtsaSchedule schedule = null;

        if (titleNode != null)
        {
            attributes = getNodeAttributes(titleNode, ImtsaTitle.ATTRIBUTE_VIEW);
            if (attributes == null)
            {
                errors = true;
            }

            Node developersNode  = getUniqueNodeByTitleAndParent(ImtsaTitle.getDevelopersNodeName(), ImtsaFile.getTitleNodeName(), false);
            if (developersNode != null)
            {
                NodeItem developersNodeItem = getNodeItem(developersNode, Collections.singletonList(ImtsaDeveloper.NODE_VIEW));
                if (developersNodeItem == null){ errors = true; }
                else
                {
                    for (NodeItem developerNodeItem: developersNodeItem.getChildNodes())
                    {
                        ImtsaDeveloper developer = ImtsaDeveloper.createDeveloper(developerNodeItem.getAttributes());
                        if (!(USMA_IMTSA_IMPORT_SIMPLE_MODE && developer.getAttributes().get(ImtsaDeveloper.getDeveloperFioAttribute()) == null))
                            developerList.add(developer);
                    }
                }
            }

            Node qualificationsNode = getUniqueNodeByTitleAndParent(ImtsaTitle.getQualificationsNodeName(), ImtsaFile.getTitleNodeName(), false);
            if (qualificationsNode != null)
            {
                NodeItem qualificationsNodeItem = getNodeItem(qualificationsNode, Collections.singletonList(ImtsaQualification.NODE_VIEW));
                if (qualificationsNodeItem == null){ errors = true; }
                else
                {
                    for (NodeItem qualificationNodeItem: qualificationsNodeItem.getChildNodes())
                    {
                        qualificationList.add(ImtsaQualification.createQualification(qualificationNodeItem.getAttributes()));
                    }
                }
            }

            Node specialitiesNode = getUniqueNodeByTitleAndParent(ImtsaTitle.getSpecialitiesNodeName(), ImtsaFile.getTitleNodeName(), false);
            if (specialitiesNode != null)
            {
                NodeItem specialtyNodeItem = getNodeItem(specialitiesNode, Collections.singletonList(ImtsaSpeciality.NODE_VIEW));
                if (specialtyNodeItem == null){ errors = true;}
                else
                {
                    for (NodeItem specialityNodeItem: specialtyNodeItem.getChildNodes())
                    {
                        specialityList.add(ImtsaSpeciality.createSpeciality(specialityNodeItem.getAttributes()));
                    }
                }
            }

            cycles = parseCycles();
            if (cycles == null){ errors = true; }

            schedule = parseSchedule();
            if (schedule == null){ errors = true; }
        }

        if (errors && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        if (schedule != null && attributes != null && ImtsaTitle.getLevelCodeM().equals(attributes.get(ImtsaTitle.getLevelCodeAttribute())))
        {
            // для магистров всегда сдвигаем график - первый заполненный курс становится первым, остальные сдвигаются так же
            Map<Integer, Map<Integer, Map<Integer, String>>> courseTermPartScheduleMap = schedule.getCourseTermsScheduleMap();
            if (courseTermPartScheduleMap.isEmpty()){ return null; }
            Map<Integer, Map<Integer, Map<Integer, String>>> resultsCourseTermPartScheduleMap = new HashMap<>();
            Integer minCourse = Collections.min(courseTermPartScheduleMap.keySet());
            if (minCourse != 1)
            {
                for (Map.Entry<Integer, Map<Integer, Map<Integer, String>>> courseEntry: courseTermPartScheduleMap.entrySet())
                {
                    resultsCourseTermPartScheduleMap.put(courseEntry.getKey() - minCourse + 1, courseEntry.getValue());

                }

                schedule.setCourseTermsScheduleMap(resultsCourseTermPartScheduleMap);
            }
        }

        ImtsaTitle result = new ImtsaTitle();
        result.setAttributes(attributes);
        result.setDeveloperList(developerList);
        result.setQualificationList(qualificationList);
        result.setSpecialityList(specialityList);
        result.setCycles(cycles);
        result.setSchedule(schedule);

        return result;
    }

    /**
     * Считывает циклы.
     * @return циклы ИМЦА
     */
    protected abstract ImtsaCycles parseCycles();


    /**
     * Считывает график учебного процесса.
     * @return график учебного процесса ИМЦЫ
     */
    private ImtsaSchedule parseSchedule()
    {
        setStage(ParseStage.SCHEDULE);
        boolean errors = false;
        Node scheduleNode;
        NodeItem scheduleNodeItem;
        if ((scheduleNode = getUniqueNodeByTitleAndParent(ImtsaTitle.getScheduleNodeName(), ImtsaFile.getTitleNodeName(), false)) == null || (scheduleNodeItem = getNodeItem(scheduleNode, Arrays.asList(ImtsaSchedule.COURSE_NODE_VIEW, ImtsaSchedule.TERM_NODE_VIEW))) == null)
        {
            return null;
        }

        int partitionNumber = 1;
        String scheduleMask = null;
        Map<Integer, Map<Integer, String>> coursePartMaskMap = new HashMap<>();
        Map<Integer, Map<Integer, Map<Integer, String>>> courseTermPartScheduleMap = new HashMap<>();
        for (NodeItem courseNodeItem: scheduleNodeItem.getChildNodes())
        {
            Map<String, Object> courseAttributes = courseNodeItem.getAttributes();
            Integer courseNumber = (Integer) courseAttributes.get(ImtsaSchedule.getCourseNumberAttribute());

            if (courseNumber == null)
            {
                scheduleMask = (String) courseAttributes.get(ImtsaSchedule.getCourseScheduleAttribute());

            } else
            {
                String courseSchedule = (String) courseAttributes.get(ImtsaSchedule.getCourseScheduleAttribute());
                Map<Integer, String> partMaskMap = new HashMap<>();
                coursePartMaskMap.put(courseNumber, partMaskMap);
                partMaskMap.put(1, courseSchedule);
                boolean loadSchedule = false;
                for (char c: courseSchedule.toCharArray())
                {
                    if (c != '='){ loadSchedule = true; break; }
                }

                if (!loadSchedule){ continue; }

                List<NodeItem> termNodeItemList = courseNodeItem.getChildNodes();
                if (termNodeItemList.isEmpty())
                {
                    Map<Integer, Map<Integer, String>> termScheduleMap = new HashMap<>();
                    int scheduleLength = courseSchedule.length();
                    int i = 0;
                    int termNumber = 1;
                    while (i < scheduleLength)
                    {
                        int start = i;
                        while (i < scheduleLength && courseSchedule.charAt(i) == UniusmaDefines.IMTSA_HOLIDAYS){ i++; }
                        while (i < scheduleLength && courseSchedule.charAt(i) != UniusmaDefines.IMTSA_HOLIDAYS){ i++; }
                        while (i < scheduleLength
                                && ((USMA_IMTSA_IMPORT_SIMPLE_MODE && termNumber == 2)
                                || (i == scheduleLength - 1
                                || (courseSchedule.charAt(i) != UniusmaDefines.IMTSA_THEORY
                                && courseSchedule.charAt(i) != UniusmaDefines.IMTSA_WORK_PRACTICE
                                && courseSchedule.charAt(i) != UniusmaDefines.IMTSA_QUALIFICATION_WORK))))
                        { i++; }

                        String termSchedule = courseSchedule.substring(start, i);
                        Map<Integer, String> partScheduleMap = new HashMap<>();
                        partScheduleMap.put(1, termSchedule);
                        termScheduleMap.put(termNumber++, partScheduleMap);
                    }

                    courseTermPartScheduleMap.put(courseNumber, termScheduleMap);

                } else
                {
                    for (int i = 0, maskNumber = 1; i < ImtsaSchedule.getCourseScheduleAttributes().size(); maskNumber = ++i + 1)
                    {
                        String scheduleAttribute = ImtsaSchedule.getCourseScheduleAttributes().get(i);
                        String partMask = (String) courseAttributes.get(scheduleAttribute);
                        if (partMask != null)
                        {
                            partMaskMap.put(maskNumber, partMask);
                        }
                    }

                    coursePartMaskMap.put(courseNumber, partMaskMap);

                    Map<Integer, Map<Integer, String>> termPartScheduleMap = new TreeMap<>();
                    Map<Integer, Integer> termFirstWeekNumberMap = new HashMap<>();
                    Map<Integer, Integer> termLengthByScheduleMap = new HashMap<>();
                    boolean scheduleMissError = false;
                    for (NodeItem termNodeItem: termNodeItemList)
                    {
                        Map<String, Object> termAttributes = termNodeItem.getAttributes();
                        Integer termNumber = (Integer) termAttributes.get(ImtsaSchedule.getTermNumberAttribute());

                        Integer termFirstWeekNumber = (Integer) termAttributes.get(ImtsaSchedule.getTermFirstWeekAttribute());
                        termFirstWeekNumberMap.put(termNumber, termFirstWeekNumber);

                        Map<Integer, String> partScheduleMap = new HashMap<>();

                        List<Integer> scheduleCopyNumberCandidates = new ArrayList<>();
                        String termScheduleSample = (String) termAttributes.get(ImtsaSchedule.getTermScheduleAttribute());
                        partScheduleMap.put(1, termScheduleSample);
                        int termScheduleLength = termScheduleSample.length();
                        termLengthByScheduleMap.put(termNumber, termScheduleLength);
                        int lastNotNullNumber = 1;

                        for (int i = 1, scheduleNumber = 2; i < ImtsaSchedule.getTermScheduleAttributes().size(); scheduleNumber = ++i + 1)
                        {
                            String scheduleAttribute = ImtsaSchedule.getTermScheduleAttributes().get(i);
                            String partSchedule = (String) termAttributes.get(scheduleAttribute);

                            if (partSchedule != null)
                            {
                                if (USMA_IMTSA_IMPORT_SIMPLE_MODE)
                                {
                                    for (Integer scheduleCopyNumberCandidate: scheduleCopyNumberCandidates)
                                    {
                                        partScheduleMap.put(scheduleCopyNumberCandidate, termScheduleSample);
                                    }

                                    scheduleCopyNumberCandidates.clear();
                                }

                                if (!USMA_IMTSA_IMPORT_SIMPLE_MODE && lastNotNullNumber != scheduleNumber - 1)
                                {
                                    registerError("В " + termNumber + " семестре " + courseNumber + " курса учебного графика пропущен атрибут «" + ImtsaSchedule.getTermScheduleAttributes().get(i - 1) + "».");
                                    scheduleMissError = true;
                                    break;
                                }

                                lastNotNullNumber = scheduleNumber;

                                int partScheduleLength = partSchedule.length();
                                if (termScheduleLength != partScheduleLength)
                                {
                                    registerError("Учебные графики в " + termNumber + " семестре " + courseNumber + " курса имеют различную длину.");
                                    errors = true;
                                }

                                partScheduleMap.put(scheduleNumber, partSchedule);

                            } else if (USMA_IMTSA_IMPORT_SIMPLE_MODE)
                            {
                                scheduleCopyNumberCandidates.add(scheduleNumber);
                            }
                        }

                        if (lastNotNullNumber != 1 && lastNotNullNumber != partitionNumber)
                        {
                            if (partitionNumber == 1)
                            {
                                partitionNumber = lastNotNullNumber;

                            } else if (!scheduleMissError)
                            {
                                // ошибка уже зафиксирована
                                registerError("В учебном графике присутствуют различные типы разбиения.");
                                errors = true;
                            }
                        }

                        termPartScheduleMap.put(termNumber, partScheduleMap);
                    }

                    errors |= scheduleMissError;

                    if (!scheduleMissError)
                    {
                        int termSize = termPartScheduleMap.size();
                        Integer[] terms = termPartScheduleMap.keySet().toArray(new Integer[termSize]);
                        if (terms[0] != 1 || terms[termSize - 1] != termSize)
                        {
                            registerError("В " + courseNumber + " курсе учебного графика пропущен семестр.");
                            errors = true;
                            continue;
                        }

                        checkTermsLength(termFirstWeekNumberMap, termLengthByScheduleMap, courseNumber, termSize);
                        if (!USMA_IMTSA_IMPORT_SIMPLE_MODE)
                            checkSchedule(partMaskMap, termPartScheduleMap, courseNumber, partitionNumber);
                    }

                    courseTermPartScheduleMap.put(courseNumber, termPartScheduleMap);
                }
            }
        }


        int scheduleLength = scheduleMask == null ? -1 : scheduleMask.length();
        for (Map.Entry<Integer, Map<Integer, String>> courseEntry: coursePartMaskMap.entrySet())
        {
            Integer courseNumber = courseEntry.getKey();
            for (Map.Entry<Integer, String> partEntry: courseEntry.getValue().entrySet())
            {
                Integer part = partEntry.getKey();
                String mask = partEntry.getValue();

                if (scheduleLength == -1)
                {
                    scheduleLength = mask.length();

                } else if (scheduleLength != mask.length())
                {
                    registerError("«" + ImtsaSchedule.getCourseScheduleAttributes().get(part - 1) + "» " + courseNumber + " курса не подходит под шаблон учебного графика.");
                    errors = true;
                }
            }
        }

        if (errors)
        {
            return null;
        }

        ImtsaSchedule result = new ImtsaSchedule();
        result.setCourseTermsScheduleMap(courseTermPartScheduleMap);
        result.setPartitionNumber(partitionNumber);
        return result;
    }


    /**
     * Возвращает отображения символа типа недели на его частоту в строке учебного плана.
     * @param schedule строка учебного графика
     * @return отображение типа недели на кол-во недель
     */
    private Map<Character, Integer> getCountWeekTypeMap(String schedule)
    {
        Map<Character, MutableInt> countMap = SafeMap.get(MutableInt.class);
        for (char ch: schedule.toCharArray())
        {
            countMap.get(ch).increment();
        }

        Map<Character, Integer> result = new HashMap<>();
        for (Map.Entry<Character, MutableInt> countEntry: countMap.entrySet())
        {
            result.put(countEntry.getKey(), countEntry.getValue().intValue());
        }

        return result;
    }

    /**
     * Сравнивает длины семестров, полученные по длине учебных графиков и номеру первой недели.
     * @param termFirstWeekNumberMap номера первых недель семестров
     * @param termLengthByScheduleMap длины семестров согласно учебному графику
     * @param courseNumber номер курса
     * @param termsSize количество семестров
     */
    private void checkTermsLength(Map<Integer, Integer> termFirstWeekNumberMap, Map<Integer, Integer> termLengthByScheduleMap, Integer courseNumber, Integer termsSize)
    {
        Map<Integer, Integer> termLengthByFirstWeekNumberMap = new HashMap<>();
        for (int i = 1; i < termsSize - 1; i++)
        {
            Integer nextTermFirstWeekNumber = termFirstWeekNumberMap.get(i + 1);
            Integer termFirstWeekNumber = termFirstWeekNumberMap.get(i);

            if (termFirstWeekNumber != null && nextTermFirstWeekNumber != null)
            {
                termLengthByFirstWeekNumberMap.put(i, nextTermFirstWeekNumber - termFirstWeekNumber);
            }
        }

        for (Map.Entry<Integer, Integer> termLengthByFirstWeekNumberEntry: termLengthByFirstWeekNumberMap.entrySet())
        {
            Integer termNumber = termLengthByFirstWeekNumberEntry.getKey();
            Integer termLengthByFirstWeekNumber = termLengthByFirstWeekNumberEntry.getValue();
            Integer termLengthBySchedule = termLengthByScheduleMap.get(termNumber);

            if (!termLengthByFirstWeekNumber.equals(termLengthBySchedule))
            {
                registerError("Количество недель в " + termNumber + " семестре " + courseNumber + " курса, соответствующее учебному графику не совпадает с количеством недель, согласно номерам первых недель семестров.");
            }
        }
    }


    /**
     * Проверяет, что учебные графики, указанные в семестрах, соответствуют курсовым шаблонам.
     * @param partMaskMap курсовые шаблоны учебных графиков
     * @param termPartScheduleMap учебные графики по семестрам
     * @param courseNumber номер семестра
     * @param partitionNumber количество элементов разбиения
     */
    private void checkSchedule(Map<Integer, String> partMaskMap, Map<Integer, Map<Integer, String>> termPartScheduleMap, Integer courseNumber, Integer partitionNumber)
    {
        for (int i = 0, partNumber = 1; i < partitionNumber; partNumber = ++i + 1)
        {
            if (partMaskMap.get(partNumber) != null)
            {
                String scheduleSample = partMaskMap.get(1);
                String partMask = partNumber == 1 ? null : partMaskMap.get(partNumber);

                StringBuilder scheduleBuilder = new StringBuilder();
                for (Map.Entry<Integer, Map<Integer, String>> termPartScheduleEntry: termPartScheduleMap.entrySet())
                {
                    Map<Integer, String> partScheduleMap = termPartScheduleEntry.getValue();
                    String schedulePart = partScheduleMap.size() == 1 ? partScheduleMap.get(1) : partScheduleMap.get(partNumber);
                    scheduleBuilder.append(schedulePart);
                }

                String schedule = scheduleBuilder.toString();
                int scheduleLength = schedule.length();
                int sampleLength = scheduleSample.length();

                if (scheduleLength != sampleLength)
                {
                    registerError("«" + ImtsaSchedule.getTermScheduleAttributes().get(i) + "», заданный семестрами " + courseNumber + " курса, не подходит под шаблон курса.");
                    break;
                }

                for (int j = 0; j < scheduleLength; j++)
                {
                    char sch = schedule.charAt(j);
                    char smp = scheduleSample.charAt(j);
                    if (partMask != null && partMask.charAt(j) != '0')
                    {
                        smp = partMask.charAt(j);
                    }

                    if (sch != smp)
                    {
                        registerError("«" + ImtsaSchedule.getTermScheduleAttributes().get(i) + "», заданный семестрами " + courseNumber + " курса, не подходит под шаблон курса.");
                        break;
                    }
                }
            }
        }
    }


    /**
     * Считывает компетенции учебного плана.
     * @return считанные компетенции ИМЦА
     */
    private ImtsaCompetences parseCompetences()
    {
        setStage(ParseStage.COMPETENCES);
        boolean errors = false;
        Node competencesNode;
        NodeItem competencesNodeItem;
        if ((competencesNode = getUniqueNodeByTitleAndParent(ImtsaFile.getCompetencesNodeName(), ImtsaFile.getPlanNodeName(), false)) == null || (competencesNodeItem = getNodeItem(competencesNode, Collections.singletonList(ImtsaCompetences.NODE_VIEW))) == null)
        {
            return null;
        }

        Map<String, String> compIndex2ContentMap = new HashMap<>();
        Map<String, String> compCode2IndexMap = new HashMap<>();
        for (NodeItem competenceNodeItem: competencesNodeItem.getChildNodes())
        {
            Map<String, Object> attributes = competenceNodeItem.getAttributes();

            String competenceCode = (String) attributes.get(ImtsaCompetences.getCompetencesCodeAttribute());
            String competenceIndex = (String) attributes.get(ImtsaCompetences.getCompetenceIndexAttribute());
            String competenceContent = (String) attributes.get(ImtsaCompetences.getCompetencesContentAttribute());

            compIndex2ContentMap.put(competenceIndex, competenceContent);
            compCode2IndexMap.put(competenceCode, competenceIndex);
        }

        Set<String> competenceIndexes = new TreeSet<>(compIndex2ContentMap.keySet());
        String compIndexRegExp = ImtsaCompetences.getCompetenceIndexRegExp();
        Pattern pattern = Pattern.compile(compIndexRegExp);
        Map<String, PairKey<String, Integer>> compIndex2GroupCodeAndNumberMap = new HashMap<>();
        for (String index: competenceIndexes)
        {
            Matcher matcher = pattern.matcher(index);
            if (!matcher.find())
            {
                registerError("Индекс компетенции «" + index + "» имеет неизвестный формат.");
                errors = true;
                continue;
            }

            String groupCode = matcher.group(1);
            String numberStr = matcher.group(2);

            int number = Integer.parseInt(numberStr);

            compIndex2GroupCodeAndNumberMap.put(index, PairKey.create(groupCode, number));
        }

        Map<String, Set<Integer>> compGroupCode2NumberSet = SafeMap.get(TreeSet.class);
        for (Map.Entry<String, PairKey<String, Integer>> indexEntry: compIndex2GroupCodeAndNumberMap.entrySet())
        {
            PairKey<String, Integer> groupCodeAndNumberKey = indexEntry.getValue();
            String groupCode = groupCodeAndNumberKey.getFirst();
            Integer number = groupCodeAndNumberKey.getSecond();

            compGroupCode2NumberSet.get(groupCode).add(number);
        }

        Map<String, Integer> compGroupSize = new HashMap<>();
        for (Map.Entry<String, Set<Integer>> compGroupEntry: compGroupCode2NumberSet.entrySet())
        {
            String compGroupCode = compGroupEntry.getKey();
            Set<Integer> numberSet = compGroupEntry.getValue();
            int size = numberSet.size();
            Integer[] numbers = numberSet.toArray(new Integer[size]);
            if (numbers[0] != 1 || numbers[size - 1] != size)
            {
                registerError("Пропущен номер компетенции в группе «" + compGroupCode + "».");
                errors = true;
            }

            compGroupSize.put(compGroupCode, size);
        }

        if (errors && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        ImtsaCompetences result = new ImtsaCompetences();
        result.setCompetencesIndex2ContentMap(compIndex2ContentMap);
        result.setCompetenceCode2IndexMap(compCode2IndexMap);
        result.setUndistributedCompetenceIndexes(competenceIndexes);
        result.setCompetenceIndex2GroupCodeAndNumberMap(compIndex2GroupCodeAndNumberMap);
        result.setCompGroupSize(compGroupSize);

        return result;
    }


    /**
     * Считывает строки плана.
     * @param cycles циклы
     * @param compCode2IndexMap мапа кодов и индексов компетенций
     * @param minCoursePartNumberMap мапа наименьших номеров частей курса
     * @param maxCoursePartNumberMap мапа наибольших номеров частей курса
     * @param undistributedCompetenceIndexes оставшиеся (нераспределенные индксы компетенций)
     * @return корневой нод, которому подчинены оставшиеся
     */
    private ImtsaPlanRow parsePlanRows(Collection<String> cycles, Map<String, String> compCode2IndexMap, Map<Integer, Integer> minCoursePartNumberMap, Map<Integer, Integer> maxCoursePartNumberMap, Collection<String> undistributedCompetenceIndexes)
    {
        setStage(ParseStage.PLAN_ROWS);
        boolean errors = false;
        Node rowsNode;
        NodeItemAdvanced rowsNodeItem;
        if ((rowsNode = getUniqueNodeByTitleAndParent(ImtsaFile.getPlanRowsNodeName(), ImtsaFile.getPlanNodeName())) == null || (rowsNodeItem = getNodeItemAdvanced(rowsNode, Collections.singletonMap(ImtsaDiscipline.getRowNodeName(), ImtsaDiscipline.DISCIPLINE_ROW_NODE_VIEW))) == null)
        {
            return null;
        }

        Collection<NodeItemAdvanced> rowNodeItems = rowsNodeItem.getChildMap().get(ImtsaDiscipline.getRowNodeName());
        Map<String, ImtsaStructure> structureMap = new HashMap<>();
        ImtsaStructure result = new ImtsaStructure();
        structureMap.put(null, result);
        Map<String, ImtsaGroup> groupDiscId2NodeItemMap = new HashMap<>();

        /* если не удается определить группу раздела по id, указывается идущая перед ним */
        Map<String, String> part2DefaultGroupMap = new HashMap<>();
        String prevGroupId = null;
        for (NodeItemAdvanced rowNodeItem: rowNodeItems)
        {
            Map<String, Object> attributes = rowNodeItem.getAttributes();
            if (attributes.get(ImtsaDiscipline.getPracticeType()) != null)
            {
                continue;
            }

            String originalId = (String) attributes.get(getDisciplineIdAttribute());
            String cycle = (String) attributes.get(getDisciplineCycleAttribute());
            if (originalId == null || cycle == null)
            {
                continue;
            }

            Boolean groupAttribute = (Boolean) attributes.get(ImtsaDiscipline.getGroupAttribute());
            if  (groupAttribute != null && groupAttribute)
            {
                String id = getModifiedId(originalId);
                int lastDotIndex = id.lastIndexOf(ImtsaPlanRow.getIdSeparator());
                String localId = id.substring(lastDotIndex + 1);
                String parentId = id.substring(0, lastDotIndex);
                String title = (String) attributes.get(ImtsaDiscipline.getTitleAttribute());
                if (title == null)
                {
                    if (USMA_IMTSA_IMPORT_SIMPLE_MODE)
                    {
                        registerError("Пропущена дисциплина " + originalId + " - не указано название.");

                    } else
                    {
                        errors = true;
                    }

                    continue;
                }

                if (cycle.startsWith(ImtsaPlanRow.getIdSeparator()))
                {
                    registerError("У дисциплины «" + title + "» указан неизвестный цикл.");
                    continue;
                }

                String cycleValue = StringUtils.split(cycle, ImtsaPlanRow.getIdSeparator())[0];

                if (!USMA_IMTSA_IMPORT_SIMPLE_MODE && cycles != null && !cycles.contains(cycleValue))
                {
                    registerError("У дисциплины «" + title + "» указан неизвестный цикл.");
                    errors = true;
                }

                if (!id.substring(0, id.indexOf(ImtsaPlanRow.getIdSeparator())).equals(cycleValue))
                {
                    registerError("Идентификатор дисциплины «" + title + "» не соответствует ее циклу.");
                    errors = true;
                }

                ImtsaGroup group = new ImtsaGroup();
                group.setTitle(title);
                group.setId(id);

                ImtsaPlanRow parent = getStructureParent(parentId, structureMap);
                group.setParent(parent);
                parent.getChildMap().put(localId, group);

                groupDiscId2NodeItemMap.put(id, group);

                prevGroupId = id;

            } else
            {
                Boolean part = (Boolean) attributes.get(ImtsaDiscipline.getPartAttribute());
                if (part != null && part)
                {
                    part2DefaultGroupMap.put(getModifiedId(originalId), prevGroupId);
                }
            }
        }

        final String termsString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Map<Character, Integer> termCharMap = SafeMap.get(termsString::indexOf);

        Map<String, ImtsaGroup> groupMap = Collections.unmodifiableMap(groupDiscId2NodeItemMap);
        Map<String, ImtsaVariant> variantMap = new HashMap<>();

        int defaultDiscId = 0;
        Boolean correspondence = null;
        for (NodeItemAdvanced rowNodeItem: rowNodeItems)
        {
            Map<String, Object> attributes = rowNodeItem.getAttributes();
            if (attributes.get(ImtsaDiscipline.getPracticeType()) != null)
            {
                continue;
            }

            String originalId = (String) attributes.get(getDisciplineIdAttribute());
            String cycle = (String) attributes.get(getDisciplineCycleAttribute());

            if (originalId == null || cycle == null)
            {
                continue;
            }

            String id = getModifiedId(originalId);
            if (!groupMap.containsKey(id))
            {
                String title = (String) attributes.get(ImtsaDiscipline.getTitleAttribute());
                if (title == null)
                {
                    if (USMA_IMTSA_IMPORT_SIMPLE_MODE)
                    {
                        registerError("Пропущена дисциплина " + originalId + " - не указано название.");

                    } else
                    {
                        errors = true;
                    }

                    continue;
                }

                if (cycle.startsWith(ImtsaPlanRow.getIdSeparator()))
                {
                    registerError("У дисциплины «" + title + "» указан неизвестный цикл.");
                    continue;
                }

                String cycleValue = StringUtils.split(cycle, ImtsaPlanRow.getIdSeparator())[0];

                int lastDotIndex = id.lastIndexOf(ImtsaPlanRow.getIdSeparator());
                if (!USMA_IMTSA_IMPORT_SIMPLE_MODE && lastDotIndex == -1)
                {
                    registerError("Некорректный идентификатор дисциплины «" + originalId + "».");
                }

                String localId = lastDotIndex == -1 ? String.valueOf(defaultDiscId++) : id.substring(lastDotIndex + 1);
                String parentId = lastDotIndex == -1 ? id : id.substring(0, lastDotIndex);

                ImtsaDiscipline discipline = new ImtsaDiscipline();
                ImtsaPlanRow parent;
                if (part2DefaultGroupMap.containsKey(id))
                {
                    /*парент группа дисциплин*/
                    parent = groupMap.get(parentId);
                    if (parent == null)
                    {
                        parent = groupMap.get(part2DefaultGroupMap.get(id));
                        if (parent == null)
                        {
                            registerError("Невозможно подобрать группу для дисциплины «" + originalId + "».");
                        }
                    }

                } else
                {
                    parent = getVariantParent(parentId, variantMap, structureMap);
                    if (parent == null)
                    {
                        /* парент не группа дисциплина по выбору, остается только структурная единица*/
                        parent = getStructureParent(parentId, structureMap);
                    }
                }

                if (parent == null && USMA_IMTSA_IMPORT_SIMPLE_MODE)
                {
                    continue;
                }

                parent.getChildMap().put(localId, discipline);

                if (!USMA_IMTSA_IMPORT_SIMPLE_MODE && cycles != null && !cycles.contains(cycleValue))
                {
                    registerError("У дисциплины «" + title + "» указан неизвестный цикл.");
                    errors = true;
                }

                if (!(StringUtils.split(id, ImtsaPlanRow.getIdSeparator())[0]).equals(cycleValue))
                {
                    registerError("Идентификатор дисциплины «" + title + "» не соответствует ее циклу.");
                    errors = true;
                }

                discipline.setTitle(title);
                discipline.setOwnerCode((String) attributes.get(ImtsaDiscipline.getOwnerCodeAttribute()));
                discipline.setTotalSize((Double) attributes.get(ImtsaDiscipline.getTotalSizeAttribute()));
                discipline.setTotalLabor((Double) attributes.get(ImtsaDiscipline.getTotalLaborAttribute()));
                discipline.setSelfWork((Double) attributes.get(ImtsaDiscipline.getSelfWorkAttribute()));
                discipline.setId(id);

                String competenceCodesStr = (String) attributes.get(ImtsaDiscipline.getCompetenceCodesAttribute());
                String competenceIndexesStr = (String) attributes.get(ImtsaDiscipline.getCompetenceIndexesAttribute());

                Set<String> competenceCodes = competenceCodesStr == null ? null : new HashSet<>(Arrays.asList(StringUtils.split(competenceCodesStr.replaceAll("[ ]+", ""), ImtsaPlanRow.getCompetenceCodesSeparator())));
                Set<String> competenceIndexes = competenceIndexesStr == null ? null : new HashSet<>(Arrays.asList(StringUtils.split(competenceIndexesStr.replaceAll("[ ]+", ""), ImtsaPlanRow.getCompetencesIndexesSeparator())));

                if (undistributedCompetenceIndexes != null && competenceIndexes != null)
                {
                    if (competenceCodes != null)
                    {
                        Set<String> competenceIndexesByCodes = new HashSet<>();
                        for (String competenceCode: competenceCodes)
                        {
                            competenceIndexesByCodes.add(compCode2IndexMap.get(competenceCode));
                        }

                        if (!(competenceIndexesByCodes.size() == 1 && competenceIndexesByCodes.iterator().next() == null) && !competenceIndexes.equals(competenceIndexesByCodes))
                        {
                            registerError("Коды компетенций дисциплины «" + title + "» не соответствуют индексам компетенций");
                            errors = true;
                        }
                    }

                    discipline.setCompetenceIndexes(competenceIndexes);
                    undistributedCompetenceIndexes.removeAll(competenceIndexes);
                }

                Map<ImtsaLoadType, Double> totalLoadMap = new HashMap<>();

                Map<Integer, Map<ImtsaLoadType, Double>> term2LoadSizeMap = new HashMap<>();
                Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> term2ControlSizeMap = new HashMap<>();

                List<NodeItemAdvanced> termNodeItems = rowNodeItem.getChildMap().get(ImtsaDiscipline.getTermNodeName());
                List<NodeItemAdvanced> courseNodeItems = rowNodeItem.getChildMap().get(ImtsaDiscipline.getCourseNodeName());
                if (correspondence == null && (termNodeItems !=null || courseNodeItems != null))
                {
                    correspondence = termNodeItems == null;
                }

                if (correspondence == null){ continue; }

                if (!correspondence && termNodeItems != null)
                {
                    for (NodeItemAdvanced termNodeItem: termNodeItems)
                    {
                        Map<String, Object> termAttributes = termNodeItem.getAttributes();
                        Integer termNumber = (Integer) termAttributes.get(ImtsaDiscipline.getTermNumberAttribute());

                        Map<ImtsaLoadType, Double> loadSizeMap = new HashMap<>();
                        Map<Class<? extends EppControlActionType>, Map<String, Integer>> controlSizeMap = SafeMap.get(HashMap.class);
                        for (Map.Entry<String, ImtsaLoadType> loadTypeEntry: ImtsaDiscipline.getTermLoadTypeCodeMap().entrySet())
                        {
                            String loadTypeAttribute = loadTypeEntry.getKey();
                            Double load = (Double) termAttributes.get(loadTypeAttribute);
                            if (load != null)
                            {
                                ImtsaLoadType loadType = loadTypeEntry.getValue();
                                Double oldLoad = loadSizeMap.get(loadType);
                                loadSizeMap.put(loadType, load + (oldLoad == null ? 0.0d : oldLoad));

                                Double oldTotalLoad = totalLoadMap.get(loadType);
                                totalLoadMap.put(loadType, load + (oldTotalLoad == null ? 0.0d : oldTotalLoad));
                            }
                        }

                        for (Map.Entry<String, PairKey<Class<? extends EppControlActionType>, String>> controlTypeEntry: ImtsaDiscipline.getControlActionCodeMap().entrySet())
                        {
                            String controlAttribute = controlTypeEntry.getKey();
                            PairKey<Class<? extends EppControlActionType>, String> key = controlTypeEntry.getValue();
                            Class<? extends EppControlActionType> clazz = key.getFirst();
                            String controlActionCode = key.getSecond();

                            Integer controlSize = (Integer) termAttributes.get(controlAttribute);
                            if (controlSize != null)
                            {
                                if (ImtsaDiscipline.getMaxControlSizeMap().containsKey(controlAttribute))
                                {
                                    controlSize = Math.min(controlSize, ImtsaDiscipline.getMaxControlSizeMap().get(controlAttribute));
                                }

                                controlSizeMap.get(clazz).put(controlActionCode, controlSize);
                            }
                        }

                        term2LoadSizeMap.put(termNumber, loadSizeMap);
                        term2ControlSizeMap.put(termNumber, controlSizeMap);

                        if (termAttributes.get(ImtsaDiscipline.getTermZetAttribute()) != null)
                        {
                            Double zet = (Double) termAttributes.get(ImtsaDiscipline.getTermZetAttribute());
                            SafeMap.safeGet(term2LoadSizeMap, termNumber, HashMap.class).put(null, zet);
                        }
                    }

                    if (!totalLoadMap.isEmpty())
                    {
                        term2LoadSizeMap.put(0, new HashMap<>(totalLoadMap));
                        totalLoadMap.clear();
                    }
                }


                Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> courseSessionLoadSizeMap = new HashMap<>();
                Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> courseSessionControlSizeMap = new HashMap<>();

                if (correspondence && courseNodeItems != null)
                {
                    Double courseSelfWork = 0.0d;
                    for (NodeItemAdvanced courseNodeItem: courseNodeItems)
                    {
                        Map<String, Object> courseAttributes = courseNodeItem.getAttributes();
                        Integer courseNumber = (Integer) courseAttributes.get(ImtsaDiscipline.getCourseNumberAttribute());
                        Double selfWork = (Double) courseAttributes.get(ImtsaDiscipline.getCourseSelfWorkAttribute());
                        courseSelfWork += selfWork == null ? 0.0d : selfWork;

                        Map<Integer, Map<ImtsaLoadType, Double>> sessionLoadSizeMap = new HashMap<>();
                        Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> sessionControlSizeMap = new HashMap<>();
                        List<NodeItemAdvanced> sessionNodeItems = courseNodeItem.getChildMap().get(ImtsaDiscipline.getSessionNodeName());
                        if (sessionNodeItems == null){ continue; }
                        for (NodeItemAdvanced sessionNodeItem: sessionNodeItems)
                        {
                            Map<String, Object> sessionAttributes = sessionNodeItem.getAttributes();
                            Integer sessionNumber = (Integer) sessionAttributes.get(ImtsaDiscipline.getTermNumberAttribute());

                            boolean hasMeaningAttribute = false;
                            for (String meaningAttribute: ImtsaDiscipline.getSessionMeaningAttributes())
                            {
                                if (sessionAttributes.get(meaningAttribute) != null)
                                {
                                    hasMeaningAttribute = true;
                                    break;
                                }
                            }


                            if (maxCoursePartNumberMap.get(courseNumber) == null || maxCoursePartNumberMap.get(courseNumber) < sessionNumber)
                            {
                                maxCoursePartNumberMap.put(courseNumber, sessionNumber);
                            }

                            if (!hasMeaningAttribute)
                            {
                                continue;
                            }

                            if (minCoursePartNumberMap.get(courseNumber) == null || minCoursePartNumberMap.get(courseNumber) > sessionNumber)
                            {
                                minCoursePartNumberMap.put(courseNumber, sessionNumber);
                            }

                            Map<ImtsaLoadType, Double> loadSizeMap = new HashMap<>();
                            Map<Class<? extends EppControlActionType>, Map<String, Integer>> controlSizeMap = SafeMap.get(HashMap.class);

                            for (Map.Entry<String, ImtsaLoadType> loadTypeEntry: ImtsaDiscipline.getTermLoadTypeCodeMap().entrySet())
                            {
                                String loadTypeAttribute = loadTypeEntry.getKey();
                                Double load = (Double) sessionAttributes.get(loadTypeAttribute);
                                if (load != null)
                                {
                                    ImtsaLoadType loadType = loadTypeEntry.getValue();
                                    Double oldLoad = loadSizeMap.get(loadType);
                                    loadSizeMap.put(loadType, load + (oldLoad == null ? 0.0d : oldLoad));

                                    Double oldTotalLoad = totalLoadMap.get(loadType);
                                    totalLoadMap.put(loadType, load + (oldTotalLoad == null ? 0.0d : oldTotalLoad));
                                }
                            }

                            for (Map.Entry<String, PairKey<Class<? extends EppControlActionType>, String>> controlTypeEntry: ImtsaDiscipline.getControlActionCodeMap().entrySet())
                            {
                                String controlAttribute = controlTypeEntry.getKey();
                                PairKey<Class<? extends EppControlActionType>, String> key = controlTypeEntry.getValue();
                                Class<? extends EppControlActionType> clazz = key.getFirst();
                                String controlActionCode = key.getSecond();

                                Integer controlSize = (Integer) sessionAttributes.get(controlAttribute);
                                if (controlSize != null)
                                {
                                    if (ImtsaDiscipline.getMaxControlSizeMap().containsKey(controlAttribute))
                                    {
                                        controlSize = Math.min(controlSize, ImtsaDiscipline.getMaxControlSizeMap().get(controlAttribute));
                                    }

                                    controlSizeMap.get(clazz).put(controlActionCode, controlSize);
                                }
                            }

                            sessionLoadSizeMap.put(sessionNumber, loadSizeMap);
                            sessionControlSizeMap.put(sessionNumber, controlSizeMap);
                        }

                        courseSessionLoadSizeMap.put(courseNumber, sessionLoadSizeMap);
                        courseSessionControlSizeMap.put(courseNumber, sessionControlSizeMap);
                    }

                    if (discipline.getSelfWork() == null)
                    {
                        discipline.setSelfWork(courseSelfWork);
                    }

                    if (!totalLoadMap.isEmpty())
                    {
                        courseSessionLoadSizeMap.put(0, Collections.<Integer, Map<ImtsaLoadType, Double>>singletonMap(0, new HashMap<>(totalLoadMap)));
                    }
                }

                if (correspondence != null && !correspondence)
                {
                    discipline.setTerm2LoadSizeMap(term2LoadSizeMap);
                    discipline.setTerm2ControlSizeMap(term2ControlSizeMap);
                }

                if (correspondence != null && correspondence)
                {
                    discipline.setCourseTermLoadSizeMap(courseSessionLoadSizeMap);
                    discipline.setCourseTermControlSizeMap(courseSessionControlSizeMap);
                }
            }
        }

        if (correspondence == null) correspondence = false;

        if (!correspondence)
        {
            for (ImtsaVariant variant: variantMap.values())
            {
                Double totalSize = 0.0d;
                Double totalLabor = 0.0d;
                Double selfWork = 0.0d;
                Map<Integer, Map<ImtsaLoadType, Double>> variantTerm2LoadSizeMap = SafeMap.get(HashMap.class);
                Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> variantTerm2ControlSizeMap = SafeMap.get(key -> SafeMap.get(HashMap.class));

//                for (ImtsaPlanRow child: variant.getChildMap().values())
                {
                    ImtsaDiscipline childDiscipline = (ImtsaDiscipline) variant.getChildMap().values().iterator().next(); /* берем первую */
                    if (childDiscipline.getTotalSize() != null && childDiscipline.getTotalSize() > totalSize){ totalSize = childDiscipline.getTotalSize(); }
                    if (childDiscipline.getTotalLabor() != null && childDiscipline.getTotalLabor() > totalLabor){ totalLabor = childDiscipline.getTotalLabor(); }
                    if (childDiscipline.getSelfWork() != null && childDiscipline.getSelfWork() > selfWork){ selfWork = childDiscipline.getSelfWork(); }

                    if (childDiscipline.getTerm2ControlSizeMap() == null){ continue; }
                    for (Map.Entry<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> termControlEntry: childDiscipline.getTerm2ControlSizeMap().entrySet())
                    {
                        Integer termNumber = termControlEntry.getKey();
                        for (Map.Entry<Class<? extends EppControlActionType>, Map<String, Integer>> actionTypeEntry: termControlEntry.getValue().entrySet())
                        {
                            Map<String, Integer> variantControlMap = SafeMap.safeGet(SafeMap.safeGet(variantTerm2ControlSizeMap, termNumber, HashMap.class), actionTypeEntry.getKey(), HashMap.class);
                            for (Map.Entry<String, Integer> controlEntry: actionTypeEntry.getValue().entrySet())
                            {
                                String controlCode = controlEntry.getKey();
                                Integer variantControlSize = variantControlMap.get(controlCode);

                                variantControlMap.put(controlCode, variantControlSize == null ? controlEntry.getValue() : Math.max(variantControlSize, controlEntry.getValue()));
                            }
                        }
                    }

                    for (Map.Entry<Integer, Map<ImtsaLoadType, Double>> termLoadEntry: childDiscipline.getTerm2LoadSizeMap().entrySet())
                    {
                        Integer termNumber = termLoadEntry.getKey();
                        Map<ImtsaLoadType, Double> loadMap = SafeMap.safeGet(variantTerm2LoadSizeMap, termNumber, HashMap.class);
                        for (Map.Entry<ImtsaLoadType, Double> loadEntry: termLoadEntry.getValue().entrySet())
                        {
                            ImtsaLoadType loadType = loadEntry.getKey();
                            Double loadSize = loadEntry.getValue();
                            Double oldLoadSize = loadMap.get(loadType);

                            if (oldLoadSize == null || oldLoadSize < loadSize)
                            {
                                loadMap.put(loadType, loadSize);
                            }
                        }
                    }
                }

                variant.setTotalSize(totalSize);
                variant.setTotalLabor(totalLabor);
                variant.setSelfWork(selfWork);
                variant.setTerm2LoadSizeMap(variantTerm2LoadSizeMap);
                variant.setTerm2ControlSizeMap(variantTerm2ControlSizeMap);

                for (ImtsaPlanRow child: variant.getChildMap().values())
                {
                    ImtsaDiscipline childDiscipline = (ImtsaDiscipline) child;
                    childDiscipline.setTotalSize(totalSize);
                    childDiscipline.setTotalLabor(totalLabor);
                    Map<Integer, Map<ImtsaLoadType, Double>> term2LoadSizeMap = new HashMap<>();
                    Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> term2ControlSizeMap = new HashMap<>();

                    for (Map.Entry<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> termControlEntry: variantTerm2ControlSizeMap.entrySet())
                    {
                        Integer termNumber = termControlEntry.getKey();
                        Map<Class<? extends EppControlActionType>, Map<String, Integer>> controlSizeMap = new HashMap<>();
                        controlSizeMap.putAll(termControlEntry.getValue());

                        term2ControlSizeMap.put(termNumber, controlSizeMap);
                    }

                    for (Map.Entry<Integer, Map<ImtsaLoadType, Double>> termLoadSizeEntry: variantTerm2LoadSizeMap.entrySet())
                    {
                        Integer termNumber = termLoadSizeEntry.getKey();
                        Map<ImtsaLoadType, Double> loadSizeMap = new HashMap<>();

                        loadSizeMap.putAll(termLoadSizeEntry.getValue());
                        term2LoadSizeMap.put(termNumber, loadSizeMap);
                    }

                    childDiscipline.setTerm2LoadSizeMap(term2LoadSizeMap);
                    childDiscipline.setTerm2ControlSizeMap(term2ControlSizeMap);
                }
            }
        }

        if (correspondence)
        {
            for (ImtsaVariant variant: variantMap.values())
            {
                Double totalSize = 0.0d;
                Double totalLabor = 0.0d;
                Double selfWork = 0.0d;

                Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> variantLoadMap = new HashMap<>();
                Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> variantControlMap = new HashMap<>();

//                for (ImtsaPlanRow child: variant.getChildMap().values())
                {
                    ImtsaDiscipline childDiscipline = (ImtsaDiscipline) variant.getChildMap().values().iterator().next();
                    if (childDiscipline.getTotalSize() != null && childDiscipline.getTotalSize() > totalSize){ totalSize = childDiscipline.getTotalSize(); }
                    if (childDiscipline.getTotalLabor() != null && childDiscipline.getTotalLabor() > totalLabor){ totalLabor = childDiscipline.getTotalLabor(); }
                    if (childDiscipline.getSelfWork() != null && childDiscipline.getSelfWork() > selfWork){ selfWork = childDiscipline.getSelfWork(); }

                    if (childDiscipline.getCourseTermLoadSizeMap() == null){ continue; }
                    for (Map.Entry<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> childLoadEntry: childDiscipline.getCourseTermLoadSizeMap().entrySet())
                    {
                        Integer courseNumber = childLoadEntry.getKey();
                        Map<Integer, Map<ImtsaLoadType, Double>> termLoadMap = childLoadEntry.getValue();
                        Map<Integer, Map<ImtsaLoadType, Double>> variantTerm2LoadSizeMap = SafeMap.get(HashMap.class);

                        for (Map.Entry<Integer, Map<ImtsaLoadType, Double>> termLoadEntry: termLoadMap.entrySet())
                        {
                            Integer termNumber = termLoadEntry.getKey();
                            Map<ImtsaLoadType, Double> loadMap = SafeMap.safeGet(variantTerm2LoadSizeMap, termNumber, HashMap.class);
                            for (Map.Entry<ImtsaLoadType, Double> loadEntry: termLoadEntry.getValue().entrySet())
                            {
                                ImtsaLoadType loadType = loadEntry.getKey();
                                Double loadSize = loadEntry.getValue();
                                Double oldLoadSize = loadMap.get(loadType);

                                if (oldLoadSize == null || oldLoadSize < loadSize)
                                {
                                    loadMap.put(loadType, loadSize);
                                }
                            }
                        }

                        variantLoadMap.put(courseNumber, variantTerm2LoadSizeMap);
                    }

                    for (Map.Entry<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> childControlEntry: childDiscipline.getCourseTermControlSizeMap().entrySet())
                    {
                        Integer courseNumber = childControlEntry.getKey();

                        Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> termControlMap = childControlEntry.getValue();
                        Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> variantTerm2ControlSizeMap = SafeMap.get(HashMap.class);

                        for (Map.Entry<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> termControlEntry: termControlMap.entrySet())
                        {
                            Integer termNumber = termControlEntry.getKey();
                            for (Map.Entry<Class<? extends EppControlActionType>, Map<String, Integer>> actionTypeEntry: termControlEntry.getValue().entrySet())
                            {
                                Map<String, Integer> controlMap = SafeMap.safeGet(SafeMap.safeGet(variantTerm2ControlSizeMap, termNumber, HashMap.class), actionTypeEntry.getKey(), HashMap.class);
                                for (Map.Entry<String, Integer> controlEntry: actionTypeEntry.getValue().entrySet())
                                {
                                    String controlCode = controlEntry.getKey();
                                    Integer variantControlSize = controlMap.get(controlCode);

                                    controlMap.put(controlCode, variantControlSize == null ? controlEntry.getValue() : Math.max(variantControlSize, controlEntry.getValue()));
                                }
                            }
                        }

                        variantControlMap.put(courseNumber, variantTerm2ControlSizeMap);
                    }
                }

                variant.setTotalSize(totalSize);
                variant.setTotalLabor(totalLabor);
                variant.setSelfWork(selfWork);
                variant.setCourseTermLoadSizeMap(variantLoadMap);
                variant.setCourseTermControlSizeMap(variantControlMap);

                for (ImtsaPlanRow child: variant.getChildMap().values())
                {
                    ImtsaDiscipline childDiscipline = (ImtsaDiscipline) child;
                    childDiscipline.setTotalSize(totalSize);
                    childDiscipline.setTotalLabor(totalLabor);
                    childDiscipline.setSelfWork(selfWork);

                    childDiscipline.setCourseTermLoadSizeMap(variantLoadMap);
                    childDiscipline.setCourseTermControlSizeMap(variantControlMap);
                }
            }
        }

        return errors && !USMA_IMTSA_IMPORT_SIMPLE_MODE ? null : result;
    }


    /**
     * Название атрибута, содержащего идентификатор дисциплины.
     * @return название атрибута
     */
    protected abstract String getDisciplineIdAttribute();

    /**
     * Название атрибута, содержащего цикл дисциплины.
     * @return название атрибута
     */
    protected abstract String getDisciplineCycleAttribute();

    /**
     * Модифицирует идентификатор дисциплины.
     * @param id идентификатор
     * @return модифицированный идентификатор
     */
    protected abstract String getModifiedId(String id);


    /**
     * Возвращает парента (группу дисциплин по выбору) по id.
     * @param id иденификатор
     * @param variantMap мапа групп дисциплин по выбору
     * @param structureMap мапа иерархичеких нодов
     * @return парент, null, если парент не дисциплина по выбору
     */
    private ImtsaVariant getVariantParent(String id, Map<String, ImtsaVariant> variantMap, Map<String, ImtsaStructure> structureMap)
    {
        ImtsaVariant result;
        if (null != (result = variantMap.get(id)))
        {
            return result;
        }

        int lastDotIndex = id.lastIndexOf(ImtsaPlanRow.getIdSeparator());
        if (lastDotIndex == -1)
        {
            return null;
        }

        result = new ImtsaVariant();

        String localId = id.substring(lastDotIndex + 1);
        String parentId = id.substring(0, lastDotIndex);

        Matcher matcher = ImtsaVariant.getVariantPattern().matcher(localId);
        if (matcher.matches())
        {
            result.setTitle(ImtsaVariant.getNewVariantPrefix() + matcher.replaceAll("$1"));
            variantMap.put(id, result);

            ImtsaPlanRow parent = getStructureParent(parentId, structureMap);
            result.setParent(parent);
            result.setId(id);
            parent.getChildMap().put(localId, result);

            return result;

        } else
        {
            return null;
        }
    }


    /**
     * Возвращает парента (иерархическую ноду) по id.
     * @param id идентификатор
     * @param structureMap мапа иерархических нодов
     * @return парент
     */
    private ImtsaStructure getStructureParent(String id, Map<String, ImtsaStructure> structureMap)
    {
        ImtsaStructure result;
        if (null != (result = structureMap.get(id)))
        {
            return result;
        }

        result = new ImtsaStructure();
        String localId;
        ImtsaStructure parent;

        int lastDotIndex = id.lastIndexOf(ImtsaPlanRow.getIdSeparator());
        if (lastDotIndex == -1)
        {
            // цикл
            parent = structureMap.get(null);
            localId = id;

        } else
        {
            // узел
            parent = getStructureParent(id.substring(0, lastDotIndex), structureMap);
            localId = id.substring(lastDotIndex + 1);
        }

        result.setTitle(localId);
        result.setParent(parent);
        result.setId(id);
        parent.getChildMap().put(localId, result);
        structureMap.put(id, result);

        return result;
    }


    /**
     * Считывает практики.
     * @return отображение типа практики на список практик
     */
    protected abstract Map<String, Collection<ImtsaPractice>> parsePractices();


    /**
     * Считывает ВКР и Гос. экзамены.
     * @return отображение названия ИГА на норму ЗЕТ в неделе
     */
    private Map<String, Map<String, Double>> parseAttestation()
    {
        Node dissertationNode = getUniqueNodeByTitleAndParent(ImtsaAttestation.getDissertationNodeName(), ImtsaFile.getGos3SpecialWorkKindsNodeName(), false);
        Node stateExamNode = getUniqueNodeByTitleAndParent(ImtsaAttestation.getStateExamNodeName(), ImtsaFile.getGos3SpecialWorkKindsNodeName(), false);

        Map<String, Double> dissertationAttrMap = new HashMap<>();
        Map<String, Double> stateExamAttrMap = new HashMap<>();

        if (dissertationNode != null)
        {
            Map<String, Object> dissertationAttributes = getNodeAttributes(dissertationNode, ImtsaAttestation.ATTESTATION_ATTRIBUTE_VIEW);
            if (dissertationAttributes != null)
            {
                dissertationAttrMap.put(ImtsaAttestation.getAttestationZetInWeekAttribute(),  (Double) dissertationAttributes.get(ImtsaAttestation.getAttestationZetInWeekAttribute()));
                dissertationAttrMap.put(ImtsaAttestation.getAttestationHoursInZetAttribute(), (Double) dissertationAttributes.get(ImtsaAttestation.getAttestationHoursInZetAttribute()));
            }
        }

        if (stateExamNode != null)
        {
            Map<String, Object> stateExamAttributes = getNodeAttributes(stateExamNode, ImtsaAttestation.ATTESTATION_ATTRIBUTE_VIEW);
            if (stateExamAttributes != null)
            {
                stateExamAttrMap.put(ImtsaAttestation.getAttestationZetInWeekAttribute(),  (Double) stateExamAttributes.get(ImtsaAttestation.getAttestationZetInWeekAttribute()));
                stateExamAttrMap.put(ImtsaAttestation.getAttestationHoursInZetAttribute(), (Double) stateExamAttributes.get(ImtsaAttestation.getAttestationHoursInZetAttribute()));
            }
        }

        Map<String, Map<String, Double>> result = new HashMap<>();

        result.put(ImtsaAttestation.getDissertationNodeName(), dissertationAttrMap);
        result.put(ImtsaAttestation.getStateExamNodeName(), stateExamAttrMap);

        return result;
    }

    /**
     * Выбирает нод с заданным названием, заданным родителем, проверяет его уникальность.
     * @param nodeName название требуемого нода
     * @param parentNodeName название родительского нода
     * @return требуемый нод, null если такого нода с таким родителем нет, или их больше одного
     */
    protected Node getUniqueNodeByTitleAndParent(String nodeName, String parentNodeName)
    {
        return getUniqueNodeByTitleAndParent(nodeName, parentNodeName, true);
    }

    /**
     * Выбирает нод с заданным названием, заданным родителем, проверяет его уникальность.
     * @param nodeName название требуемого нода
     * @param parentNodeName название родительского нода
     * @param required обязательно встречается в документе
     * @return требуемый нод, null если такого нода с таким родителем нет, или их больше одного
     */
    protected Node getUniqueNodeByTitleAndParent(String nodeName, String parentNodeName, boolean required)
    {
        NodeList nodeList = _document.getElementsByTagName(nodeName);
        if (nodeList == null || nodeList.getLength() == 0)
        {
            if (required)
            {
                registerError("В документе отсутствует элемент «" + nodeName + "».");
            }

            return null;
        }

        if (nodeList.getLength() > 1)
        {
            registerError("Элемент «" + nodeName + "» встречается в документе более одного раза.");
            return null;
        }

        Node node = nodeList.item(0);
        if (!node.getParentNode().getNodeName().equals(parentNodeName))
        {
            registerError("Элемент «" + nodeName + "» должен быть вложен в элемент «" + parentNodeName + "».");
            return null;
        }

        return node;
    }


    /**
     * Структура нода с атрибутами и всеми вложенными нодами, представленными по атрибутам с уникальными значениями
     * @param node xml-нод источник
     * @param nodeViews список представлений вложенных нодов
     * @return структуру нода и всех его вложенных нодов
     */
    protected NodeItem getNodeItem(Node node, List<INodeView> nodeViews)
    {
        boolean errors = false;
        int size = nodeViews.size();
        if (size == 0)
        {
            throw new IllegalStateException("Должен быть задан хотя бы один вложенный элемент.");
        }

        INodeView nodeView = nodeViews.get(0);

        String nodeName = nodeView.getNodeName();
        IAttributeView attributeView = nodeView.getAttributeView();
        List<String> uniqueAttributes = nodeView.getUniqueAttributes();

        Map<String, Set<Object>> attributeValues = SafeMap.get(HashSet.class);
        List<NodeItem> childNodes = new ArrayList<>();
        NodeList childNodeList = node.getChildNodes();
        for (int i = 0; i < childNodeList.getLength(); i++)
        {
            Node childNode = childNodeList.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals(nodeName))
            {
                Map<String, Object> childNodeAttributes = getNodeAttributes(childNode, attributeView);
                if (childNodeAttributes == null)
                {
                    errors = true;
                    continue;
                }

                for (String uniqueAttribute: uniqueAttributes)
                {
                    if (!attributeValues.get(uniqueAttribute).add(childNodeAttributes.get(uniqueAttribute)))
                    {
                        registerParseError("Значение атрибута «" + uniqueAttribute + "» элемента «" + childNode.getNodeName() + "» должно быть уникальным.");
                        errors = true;
                    }
                }

                NodeItem childNodeItem = size > 1 ? getNodeItem(childNode, nodeViews.subList(1, size)) : new NodeItem();
                if (childNodeItem == null)
                {
                    errors = true;
                    continue;
                }

                if (!errors)
                {
                    childNodeItem.setAttributes(childNodeAttributes);
                    childNodes.add(childNodeItem);
                }
            }
        }

        if (errors && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        NodeItem result = new NodeItem();
        result.setChildNodes(childNodes);
        return result;
    }


    /**
     * Структура нода с атрибутами и всеми вложенными нодами, представленными по атрибутам с уникальными значениями
     * @param parentNode xml-нод источник
     * @param nodeViewMap отображение представлений вложенных нодов
     * @return структуру нода и всех его вложенных нодов
     */
    protected NodeItemAdvanced getNodeItemAdvanced(Node parentNode, Map<String, INodeViewAdvanced> nodeViewMap)
    {
        boolean errors = false;
        Map<String, Map<String, Set<Object>>> attributeValues = SafeMap.get(key -> SafeMap.get(HashSet.class));

        Map<String, List<NodeItemAdvanced>> nodeItemMap = new HashMap<>();
        NodeList nodeList = parentNode.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);
            String nodeName = node.getNodeName();
            INodeViewAdvanced nodeView = nodeViewMap.get(nodeName);
            if (node.getNodeType() == Node.ELEMENT_NODE && nodeView != null)
            {
                IAttributeView attributeView = nodeView.getAttributeView();
                Map<String, Object> childNodeAttributes = getNodeAttributes(node, attributeView);
                if (childNodeAttributes == null)
                {
                    errors = true;
                    continue;
                }

                for (String uniqueAttribute: nodeView.getUniqueAttributes())
                {
                    if (!attributeValues.get(nodeName).get(uniqueAttribute).add(childNodeAttributes.get(uniqueAttribute)))
                    {
                        registerParseError("Значение атрибута «" + uniqueAttribute + "» элемента «" + nodeName + "» должно быть уникальным.");
                        errors = true;
                    }
                }

                NodeItemAdvanced childNodeItem = getNodeItemAdvanced(node, nodeView.getChildNodeViewMap());
                if (childNodeItem == null)
                {
                    errors = true;
                    continue;
                }

                childNodeItem.setAttributes(childNodeAttributes);
                SafeMap.safeGet(nodeItemMap, nodeName, ArrayList.class).add(childNodeItem);
            }
        }

        if (errors && !USMA_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        NodeItemAdvanced result = new NodeItemAdvanced();
        result.setChildMap(nodeItemMap);

        return result;
    }


    /**
     * Получает список атрибутов данного нода
     * @param node нод, атрибуты которого нужно получить
     * @param attributeView представление нода атрибутами
     * @return отображние названия атрибута на его значение
     */
    private Map<String, Object> getNodeAttributes(Node node, IAttributeView attributeView)
    {
        boolean errors = false;
        Map<String, Class<?>> attributeTypeMap = attributeView.getAttributeTypeMap();
        Map<String, List<Validator<?>>> validatorMap = attributeView.getAttributeValidatorMap();
        List<String> requiredAttributes = attributeView.getRequiredAttributes();
        List<String> upperCaseAttributes = attributeView.getUpperCaseAttributes();

        String nodeName = node.getNodeName();
        NamedNodeMap attributeNamedNodeMap = node.getAttributes();
        Map<String, Object> attributes = new HashMap<>();

        for (int k = 0; k < attributeNamedNodeMap.getLength(); k++)
        {
            Node attributeNode = attributeNamedNodeMap.item(k);

            String attributeName = attributeNode.getNodeName();
            String attributeValue = StringUtils.trimToNull(attributeNode.getNodeValue());

            Object result = null;

            if (attributeValue != null)
            {
                if (upperCaseAttributes.contains(attributeName))
                {
                    attributeValue = attributeValue.toUpperCase();
                }

                Class<?> clazz = attributeTypeMap.get(attributeName);
                try
                {
                    result = Translators.get(clazz).translate(attributeValue);

                } catch (IllegalArgumentException e)
                {
                    registerParseError("Неизвестный формат атрибута «" + attributeName + "» элемента «" + nodeName + "».");
                    errors = true;
                    continue;
                }

                for (Validator<?> validator: validatorMap.get(attributeName))
                {
                    try
                    {
                        if (!validator.validate(result))
                        {
                            registerParseError("Некорректное значение атрибута «" + attributeName + "» элемента «" + nodeName + "». " + validator.getInvalidMessage());
                            errors = true;
                        }

                    } catch (ClassCastException e)
                    {
                        throw new IllegalStateException("Для атрибута «" + attributeName + "» элемента «" + nodeName + "» задана некорректная проверка.");
                    }
                }
            }

            attributes.put(attributeName, result);
        }

        for (String attribute: requiredAttributes)
        {
            if (attributes.get(attribute) == null)
            {
                registerParseError("Атрибут «" + attribute + "» элемента «" + nodeName + "» должен быть заполнен.");
                errors = true;
            }
        }

        return errors && !USMA_IMTSA_IMPORT_SIMPLE_MODE ? null : attributes;
    }



    private final List<String> _localErrors = new ArrayList<>();

    private final List<String> _parseErrors = new ArrayList<>();
    private final List<String> _processErrors = new ArrayList<>();

    protected void registerParseError(String errorMessage)
    {
        _parseErrors.add(_currentStage.getStageErrorMessage() + " " + errorMessage);
    }

    private static final String WARN_PREFIX = "Предупреждение анализа плана.";
    protected void registerError(String errorMessage)
    {
        _processErrors.add(WARN_PREFIX + " " + errorMessage);
    }

    private ParseStage _currentStage;
    protected void setStage(ParseStage stage)
    {
        _localErrors.addAll(new TreeSet<>(_parseErrors));
        _localErrors.addAll(new TreeSet<>(_processErrors));

        _parseErrors.clear();
        _processErrors.clear();

        _currentStage = stage;
    }

    protected enum ParseStage
    {
        TITLE("Предупреждение анализа плана: титул."),
        SCHEDULE("Предупреждение анализа плана: учебный график."),
        CYCLES("Предупреждение анализа плана: атрибуты циклов."),
        COMPETENCES("Предупреждение анализа плана: компетенции."),
        PLAN_ROWS("Предупреждение анализа плана: строки плана."),
        PRACTICES("Предупреждение анализа плана: практики."),
        FINISH("");

        private String _stageErrorMessage;
        public String getStageErrorMessage(){ return _stageErrorMessage; }

        ParseStage(String stageErrorMessage)
        {
            _stageErrorMessage = stageErrorMessage;
        }
    }


    public static ImtsaParser get(Document document, boolean gos2)
    {
        return gos2 ? new SecondGosGenerationImtsaParser(document) : new ThirdGosGenerationImtsaParser(document);
    }

    public static ImtsaParser get(InputStream inputStream, boolean gos2)
    {
        return gos2 ? new SecondGosGenerationImtsaParser(inputStream) : new ThirdGosGenerationImtsaParser(inputStream);
    }
}
