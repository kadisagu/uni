/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e6.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.TransferCompTypeStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e6.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e6.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        TransferCompTypeStuExtractUsmaExt extractExt = getByNaturalId(new TransferCompTypeStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            // если расширения нет - создадим его
            extractExt = new TransferCompTypeStuExtractUsmaExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setExtUsmaExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e6.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);
        saveOrUpdate(m.getExtUsmaExt());
    }
}
