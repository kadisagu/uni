/*$Id$*/
package ru.tandemservice.uniusma.component.catalog.usmaVaccineKind.UsmaVaccineKindPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author DMITRY KNYAZEV
 * @since 21.06.2014
 */
public interface IDAO extends IDefaultCatalogPubDAO<UsmaVaccineKind, Model>
{
}
