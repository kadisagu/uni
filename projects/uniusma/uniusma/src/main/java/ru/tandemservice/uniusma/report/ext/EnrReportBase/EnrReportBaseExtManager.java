/* $Id:$ */
package ru.tandemservice.uniusma.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.uniusma.report.bo.UsmaEnrReport.ui.EntrantStudentCardAdd.UsmaEnrReportEntrantStudentCardAdd;

/**
 * @author Denis Perminov
 * @since 27.08.2014
 */
@Configuration
public class EnrReportBaseExtManager  extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
                .add(UsmaEnrReportEntrantStudentCardAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(UsmaEnrReportEntrantStudentCardAdd.REPORT_KEY, UsmaEnrReportEntrantStudentCardAdd.class))
                .create();
    }
}
