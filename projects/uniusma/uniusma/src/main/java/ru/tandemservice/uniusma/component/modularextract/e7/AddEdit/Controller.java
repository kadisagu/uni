/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e7.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;

/**
 * @author Denis Perminov
 * @since 02.06.2014
 */
public class Controller extends ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.Controller
{
    public void onClickAddDebt(IBusinessComponent component)
    {
        Model m = (Model) getModel(component);
        m.getDebtsList().add(new StuExtractToDebtRelation());
    }

    public void onClickDeleteDebt(IBusinessComponent component)
    {
        Model m = (Model) getModel(component);
        if (m.getDebtsList().size() == 1) return;

        short debtNumber = component.getListenerParameter();
        StuExtractToDebtRelation rel = m.getDebtsList().get(debtNumber);
        m.getDebtsList().remove(rel);
        if (!m.getDebtsToDel().contains(rel) && null != rel.getId())
            m.getDebtsToDel().add(rel);
    }
}
