/* $Id$ */
package ru.tandemservice.uniusma.dao.eppEduPlan;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;

import java.util.ArrayList;
import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 27.10.2014
 */
public class UsmaEduPlanVersionDataDAO extends EppEduPlanVersionDataDAO
{
    @Override
    public Collection<EppEpvTotalRow> getTotalRows(IEppEpvBlockWrapper versionWrapper, Collection<IEppEpvRowWrapper> versionRows)
    {
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(versionRows);

        Collection<EppEpvTotalRow> result = new ArrayList<>();

        result.add(EppEpvTotalRow.getTotalRowTotal(filteredRows));
        result.add(EppEpvTotalRow.getTotalRowAvgLoad(versionWrapper, filteredRows));
        result.add(getTotalRowWeeks(filteredRows));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего экзаменов", EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего зачетов", EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего курсовых работ", EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего курсовых проектов", EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));

        return result;
    }

    public EppEpvTotalRow getTotalRowWeeks(final Collection<IEppEpvRowWrapper> filteredRows)
    {
        return new EppEpvTotalRow("Всего недель")
        {
            private static final long serialVersionUID = 1L;

            @Override
            public String getLoadValue(final int term, final String loadFullCode)
            {
                if (term <= 0)
                    return "";

                Collection<IEppEpvRowWrapper> usedInLoad = CollectionUtils.select(filteredRows, IEppEpvRow::getUsedInLoad);

                return  UniEppUtils.formatLoad(UniEppUtils.wrap(
                        new DQLSelectBuilder()
                                .fromEntity(EppEpvRowTerm.class, "t")
                                .column(DQLFunctions.sum(DQLExpressions.property("t", EppEpvRowTerm.weeks())))
                                .where(gt(property("t", EppEpvRowTerm.weeks()), value(0L)))
                                .where(in(property("t", EppEpvRowTerm.row()), usedInLoad))
                                .where(eq(property("t", EppEpvRowTerm.term().intValue()), value(term)))
                                .createStatement(UsmaEduPlanVersionDataDAO.this.getSession())
                                .<Long>uniqueResult()), true);
            }

            @Override public boolean isMergeTermCells() { return true; }
        };
    }
}