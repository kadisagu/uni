/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e17.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.RestorationStuExtractExtUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 20.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e17.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e17.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        RestorationStuExtractExtUsmaExt extractExt = getByNaturalId(new RestorationStuExtractExtUsmaExtGen.NaturalId(m.getExtract()));
        m.setExtUsmaExt(extractExt);
    }
}
