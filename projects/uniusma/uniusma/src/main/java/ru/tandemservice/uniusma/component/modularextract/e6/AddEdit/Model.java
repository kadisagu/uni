/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e6.AddEdit;

import ru.tandemservice.uniusma.entity.studentmodularorder.TransferCompTypeStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e6.AddEdit.Model
{
    TransferCompTypeStuExtractUsmaExt _extUsmaExt;

    public TransferCompTypeStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(TransferCompTypeStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }
}
