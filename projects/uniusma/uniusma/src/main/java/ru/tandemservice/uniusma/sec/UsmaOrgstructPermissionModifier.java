/* $Id$ */
package ru.tandemservice.uniusma.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public class UsmaOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{

    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniusma");
        config.setName("uniusma-sec-config");
        config.setTitle("");

        ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        // для каждого типа подразделения добавляем права на вкладку «студенты / договоры» (вкладка непосредственно подразделения)
        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // Вкладка «Студенты» на подразделении
            PermissionGroupMeta permissionGroupMedCardTab = PermissionMetaUtil.createPermissionGroup(config, code + "MedCardPermissionGroup", "Вкладка «Сведения для здравпункта»");
            PermissionMetaUtil.createPermission(permissionGroupMedCardTab, "orgUnit_viewMedCardTab_" + code, "Просмотр");
            addTabPermission(config, globalClassGroup, localClassGroup, permissionGroupMedCardTab);

            // Вкладка «Отчеты» на подразделении
            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            // Блок "Отчеты модуля «Контингент студентов»" на табе «Отчеты»
            PermissionGroupMeta pgStudentReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "StudentReportPermissionGroup", "Отчеты модуля «Контингент студентов»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_usmaMedCardReportViewPermissionKey_" + code, "Добавление отчета «Выборка для здравпункта»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_usmaStudentCardAttachmentReportViewPermissionKey_" + code, "Добавление отчета «Печать \"вкладышей\" для учебной карточки студента»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_usmaStudentCardLinerReportViewPermissionKey_" + code, "Печать \"вкладышей\" для учебной карточки студента");
            // Блок "Отчеты модуля «Сессия»" на табе «Отчеты»
            PermissionGroupMeta pgSessionReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "SessionReportPG", "Отчеты модуля «Сессия»");
            PermissionMetaUtil.createPermission(pgSessionReports, "orgUnit_usmaSessionProtocolGiaReportViewPermissionKey_" + code, "Протокол ГИА (на академ. группу)");

            // Вкладка «Студенты» на подразделении
            PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");
            // Вкладка "Массовая печать" на подразделении
            PermissionGroupMeta permissionGroupMassPrintTab = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "MassPrintPermissionGroup", "Вкладка «Массовая печать»");
            PermissionMetaUtil.createPermission(permissionGroupMassPrintTab, "orgUnit_usmaViewMassPrintTabPermissionKey_" + code, "Просмотр");
            // Вкладка «Студенты» на табе «Студенты»
            PermissionGroupMeta permissionGroupStudent = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "StudentsTabPermissionGroup", "Вкладка «Студенты»");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_printStudentPersonCardTitles_" + code, "Печатать титул всех ЛК студента");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_printStudentPersonCardDatas_" + code, "Печатать все сведения о ГИА ЛК студента");
            // Вкладка «Архив студентов» на табе «Студенты»
            PermissionGroupMeta permissionGroupStudentArchival = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "ArchivalStudentsTabPermissionGroup", "Вкладка «Архив студентов»");
            PermissionMetaUtil.createPermission(permissionGroupStudentArchival, "orgUnit_printStudentPersonCardTitles_archival_" + code, "Печатать титул всех ЛК студента");
            PermissionMetaUtil.createPermission(permissionGroupStudentArchival, "orgUnit_printStudentPersonCardDatas_archival_" + code, "Печатать все сведения о ГИА ЛК студента");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }

    private void addTabPermission(SecurityConfigMeta config, ClassGroupMeta globalClassGroup, ClassGroupMeta localClassGroup, PermissionGroupMeta tab)
    {
        PermissionMetaUtil.createGroupRelation(config, tab.getName(), globalClassGroup.getName());
        PermissionMetaUtil.createGroupRelation(config, tab.getName(), localClassGroup.getName());
    }
}