package ru.tandemservice.uniusma.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь компетенции со строкой УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaCompetence2EpvRegistryRowRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel";
    public static final String ENTITY_NAME = "usmaCompetence2EpvRegistryRowRel";
    public static final int VERSION_HASH = 1856283453;
    private static IEntityMeta ENTITY_META;

    public static final String L_USMA_COMPETENCE = "usmaCompetence";
    public static final String L_REGISTRY_ROW = "registryRow";

    private UsmaCompetence _usmaCompetence;     // Компетенция(УГМА)
    private EppEpvRegistryRow _registryRow;     // Строка УП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     */
    @NotNull
    public UsmaCompetence getUsmaCompetence()
    {
        return _usmaCompetence;
    }

    /**
     * @param usmaCompetence Компетенция(УГМА). Свойство не может быть null.
     */
    public void setUsmaCompetence(UsmaCompetence usmaCompetence)
    {
        dirty(_usmaCompetence, usmaCompetence);
        _usmaCompetence = usmaCompetence;
    }

    /**
     * @return Строка УП. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRegistryRow getRegistryRow()
    {
        return _registryRow;
    }

    /**
     * @param registryRow Строка УП. Свойство не может быть null.
     */
    public void setRegistryRow(EppEpvRegistryRow registryRow)
    {
        dirty(_registryRow, registryRow);
        _registryRow = registryRow;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaCompetence2EpvRegistryRowRelGen)
        {
            setUsmaCompetence(((UsmaCompetence2EpvRegistryRowRel)another).getUsmaCompetence());
            setRegistryRow(((UsmaCompetence2EpvRegistryRowRel)another).getRegistryRow());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaCompetence2EpvRegistryRowRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaCompetence2EpvRegistryRowRel.class;
        }

        public T newInstance()
        {
            return (T) new UsmaCompetence2EpvRegistryRowRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "usmaCompetence":
                    return obj.getUsmaCompetence();
                case "registryRow":
                    return obj.getRegistryRow();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "usmaCompetence":
                    obj.setUsmaCompetence((UsmaCompetence) value);
                    return;
                case "registryRow":
                    obj.setRegistryRow((EppEpvRegistryRow) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "usmaCompetence":
                        return true;
                case "registryRow":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "usmaCompetence":
                    return true;
                case "registryRow":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "usmaCompetence":
                    return UsmaCompetence.class;
                case "registryRow":
                    return EppEpvRegistryRow.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaCompetence2EpvRegistryRowRel> _dslPath = new Path<UsmaCompetence2EpvRegistryRowRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaCompetence2EpvRegistryRowRel");
    }
            

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel#getUsmaCompetence()
     */
    public static UsmaCompetence.Path<UsmaCompetence> usmaCompetence()
    {
        return _dslPath.usmaCompetence();
    }

    /**
     * @return Строка УП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel#getRegistryRow()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> registryRow()
    {
        return _dslPath.registryRow();
    }

    public static class Path<E extends UsmaCompetence2EpvRegistryRowRel> extends EntityPath<E>
    {
        private UsmaCompetence.Path<UsmaCompetence> _usmaCompetence;
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _registryRow;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Компетенция(УГМА). Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel#getUsmaCompetence()
     */
        public UsmaCompetence.Path<UsmaCompetence> usmaCompetence()
        {
            if(_usmaCompetence == null )
                _usmaCompetence = new UsmaCompetence.Path<UsmaCompetence>(L_USMA_COMPETENCE, this);
            return _usmaCompetence;
        }

    /**
     * @return Строка УП. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel#getRegistryRow()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> registryRow()
        {
            if(_registryRow == null )
                _registryRow = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_REGISTRY_ROW, this);
            return _registryRow;
        }

        public Class getEntityClass()
        {
            return UsmaCompetence2EpvRegistryRowRel.class;
        }

        public String getEntityName()
        {
            return "usmaCompetence2EpvRegistryRowRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
