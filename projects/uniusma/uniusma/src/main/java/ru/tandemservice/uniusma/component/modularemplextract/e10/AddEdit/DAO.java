/**
 *:$
 */
package ru.tandemservice.uniusma.component.modularemplextract.e10.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

/**
 * Create by: ashaburov
 * Date: 15.04.11
 */
public class DAO extends ru.tandemservice.moveemployee.component.modularemplextract.e10.AddEdit.DAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }
}
