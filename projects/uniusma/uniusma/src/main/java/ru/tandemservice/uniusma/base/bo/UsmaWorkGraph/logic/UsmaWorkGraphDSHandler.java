/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.List.UsmaWorkGraphList;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
public class UsmaWorkGraphDSHandler extends DefaultSearchDataSourceHandler
{
    public UsmaWorkGraphDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        EppYearEducationProcess pupnag = context.get(UsmaWorkGraphList.PUPNAG);
        EppState state = context.get(UsmaWorkGraphList.STATE);
        List<DevelopForm> developForms = context.get(UsmaWorkGraphList.DEVELOP_FORM);
        List<DevelopCondition> developConditions = context.get(UsmaWorkGraphList.DEVELOP_CONDITION);
        List<DevelopTech> developTechs = context.get(UsmaWorkGraphList.DEVELOP_TECH);
        List<DevelopGrid> developGrids = context.get(UsmaWorkGraphList.DEVELOP_GRID);

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraph.class, "wg");

        if (pupnag != null){ dql.where(eq(property("wg", UsmaWorkGraph.year()), value(pupnag))); }
        if (state != null){ dql.where(eq(property("wg", UsmaWorkGraph.state()), value(state))); }
        if (developForms != null && !developForms.isEmpty()){ dql.where(in(property("wg", UsmaWorkGraph.developForm()), developForms)); }
        if (developConditions != null && !developConditions.isEmpty()){ dql.where(in(property("wg", UsmaWorkGraph.developCondition()), developConditions)); }
        if (developTechs != null && !developTechs.isEmpty()){ dql.where(in(property("wg", UsmaWorkGraph.developTech()), developTechs)); }
        if (developGrids != null && !developGrids.isEmpty()){ dql.where(in(property("wg", UsmaWorkGraph.developGrid()), developGrids)); }

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build().ordering(new Comparator<UsmaWorkGraph>()
        {
            @Override
            public int compare(UsmaWorkGraph w1, UsmaWorkGraph w2)
            {
                return w1.getTitle().compareTo(w2.getTitle());
            }
        });
    }
}