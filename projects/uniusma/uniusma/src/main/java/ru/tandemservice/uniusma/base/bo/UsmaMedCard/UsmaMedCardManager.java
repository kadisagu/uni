/* $Id$ */
package ru.tandemservice.uniusma.base.bo.UsmaMedCard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.logic.IUsmaMedCardDao;
import ru.tandemservice.uniusma.base.bo.UsmaMedCard.logic.UsmaMedCardDao;

/**
 * @author DMITRY KNYAZEV
 * @since 24.06.2014
 */
@Configuration
public class UsmaMedCardManager extends BusinessObjectManager
{
	public static UsmaMedCardManager instance()
	{
		return instance(UsmaMedCardManager.class);
	}

	@Bean
	public IUsmaMedCardDao dao()
	{
		return new UsmaMedCardDao();
	}
}