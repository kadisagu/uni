/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusma.component.documents.d1.Add;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.d1.Add.Model;
import ru.tandemservice.uni.entity.employee.OrderData;

/**
 * @author vip_delete
 * @since 15.12.2009
 */
public class DAO extends ru.tandemservice.uni.component.documents.d1.Add.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setDocumentForTitle("предъявления по месту требования.");
        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE))
            model.setManagerPostTitle("Директор института");
        else if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH))
            model.setManagerPostTitle("Директор филиала");
        else
            model.setManagerPostTitle("Декан факультета");
        Person2PrincipalRelation rel = get(Person2PrincipalRelation.class, Person2PrincipalRelation.principal().s(), ContextLocal.getUserContext().getPrincipal());
        if (null != rel)
            model.setSecretarTitle(rel.getPerson().getIdentityCard().getIof());
        OrderData orderData = get(OrderData.class, OrderData.student().s(), model.getStudent());
        if (null == orderData) return;
        ru.tandemservice.uniusma.component.documents.d1.Add.Model casted = (ru.tandemservice.uniusma.component.documents.d1.Add.Model) model;
        setEduEnrData(casted);
    }

    private void setEduEnrData(ru.tandemservice.uniusma.component.documents.d1.Add.Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getEduEnrollmentOrderDate() == null) return;
        model.setEnrollmentDate(data.getEduEnrollmentOrderEnrDate());
        model.setEnrollmentOrderDate(data.getEduEnrollmentOrderDate());
        model.setEnrollmentOrderNumber(data.getEduEnrollmentOrderNumber());
    }
}