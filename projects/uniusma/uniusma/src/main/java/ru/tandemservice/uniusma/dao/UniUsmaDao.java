/* $Id$ */
package ru.tandemservice.uniusma.dao;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniusma.dao.IUniUsmaDao;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

/**
 * @author Andrey Avetisov
 * @since 30.10.2014
 */
public class UniUsmaDao extends UniBaseDao implements IUniUsmaDao
{
    @Override
    public UsmaImtsaXml getXml(Long blockId)
    {
        return get(UsmaImtsaXml.class, UsmaImtsaXml.block().id(), blockId);
    }
}
