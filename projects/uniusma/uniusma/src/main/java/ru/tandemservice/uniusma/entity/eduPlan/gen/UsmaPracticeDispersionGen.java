package ru.tandemservice.uniusma.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рассредоточенность практики (УГМА)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaPracticeDispersionGen extends EntityBase
 implements INaturalIdentifiable<UsmaPracticeDispersionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion";
    public static final String ENTITY_NAME = "usmaPracticeDispersion";
    public static final int VERSION_HASH = 1708486;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRACTICE = "practice";
    public static final String P_DISPERSED = "dispersed";

    private EppEpvRegistryRow _practice;     // Практика
    private boolean _dispersed;     // Рассредоточенность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Практика. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEpvRegistryRow getPractice()
    {
        return _practice;
    }

    /**
     * @param practice Практика. Свойство не может быть null и должно быть уникальным.
     */
    public void setPractice(EppEpvRegistryRow practice)
    {
        dirty(_practice, practice);
        _practice = practice;
    }

    /**
     * @return Рассредоточенность. Свойство не может быть null.
     */
    @NotNull
    public boolean isDispersed()
    {
        return _dispersed;
    }

    /**
     * @param dispersed Рассредоточенность. Свойство не может быть null.
     */
    public void setDispersed(boolean dispersed)
    {
        dirty(_dispersed, dispersed);
        _dispersed = dispersed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaPracticeDispersionGen)
        {
            if (withNaturalIdProperties)
            {
                setPractice(((UsmaPracticeDispersion)another).getPractice());
            }
            setDispersed(((UsmaPracticeDispersion)another).isDispersed());
        }
    }

    public INaturalId<UsmaPracticeDispersionGen> getNaturalId()
    {
        return new NaturalId(getPractice());
    }

    public static class NaturalId extends NaturalIdBase<UsmaPracticeDispersionGen>
    {
        private static final String PROXY_NAME = "UsmaPracticeDispersionNaturalProxy";

        private Long _practice;

        public NaturalId()
        {}

        public NaturalId(EppEpvRegistryRow practice)
        {
            _practice = ((IEntity) practice).getId();
        }

        public Long getPractice()
        {
            return _practice;
        }

        public void setPractice(Long practice)
        {
            _practice = practice;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsmaPracticeDispersionGen.NaturalId) ) return false;

            UsmaPracticeDispersionGen.NaturalId that = (NaturalId) o;

            if( !equals(getPractice(), that.getPractice()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPractice());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPractice());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaPracticeDispersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaPracticeDispersion.class;
        }

        public T newInstance()
        {
            return (T) new UsmaPracticeDispersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "practice":
                    return obj.getPractice();
                case "dispersed":
                    return obj.isDispersed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "practice":
                    obj.setPractice((EppEpvRegistryRow) value);
                    return;
                case "dispersed":
                    obj.setDispersed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "practice":
                        return true;
                case "dispersed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "practice":
                    return true;
                case "dispersed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "practice":
                    return EppEpvRegistryRow.class;
                case "dispersed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaPracticeDispersion> _dslPath = new Path<UsmaPracticeDispersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaPracticeDispersion");
    }
            

    /**
     * @return Практика. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion#getPractice()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> practice()
    {
        return _dslPath.practice();
    }

    /**
     * @return Рассредоточенность. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion#isDispersed()
     */
    public static PropertyPath<Boolean> dispersed()
    {
        return _dslPath.dispersed();
    }

    public static class Path<E extends UsmaPracticeDispersion> extends EntityPath<E>
    {
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _practice;
        private PropertyPath<Boolean> _dispersed;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Практика. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion#getPractice()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> practice()
        {
            if(_practice == null )
                _practice = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_PRACTICE, this);
            return _practice;
        }

    /**
     * @return Рассредоточенность. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion#isDispersed()
     */
        public PropertyPath<Boolean> dispersed()
        {
            if(_dispersed == null )
                _dispersed = new PropertyPath<Boolean>(UsmaPracticeDispersionGen.P_DISPERSED, this);
            return _dispersed;
        }

        public Class getEntityClass()
        {
            return UsmaPracticeDispersion.class;
        }

        public String getEntityName()
        {
            return "usmaPracticeDispersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
