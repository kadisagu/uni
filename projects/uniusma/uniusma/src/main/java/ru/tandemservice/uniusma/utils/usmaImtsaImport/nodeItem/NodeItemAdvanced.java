/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.nodeItem;

import java.util.List;
import java.util.Map;

/**
 * Данные xml-нода: атрибуты и данные дочерних нодов.
 * @author Alexander Zhebko
 * @since 15.08.2013
 */
public class NodeItemAdvanced
{
    private Map<String, Object> _attributes;
    private Map<String, List<NodeItemAdvanced>> _childMap;

    public Map<String, Object> getAttributes(){ return _attributes; }
    public void setAttributes(Map<String, Object> attributes){ _attributes = attributes; }

    public Map<String, List<NodeItemAdvanced>> getChildMap(){ return _childMap; }
    public void setChildMap(Map<String, List<NodeItemAdvanced>> childMap){ _childMap = childMap; }
}