/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaImtsaImport;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Возвращает список циклов ИМЦА.
     * @param model модель
     * @return список циклов ИМЦА
     */
    public List<UsmaImtsaCyclePlanStructureRel> getImtsaCycles(Model model);

    /**
     * Возвращает список частей/компонентов требуемого поколения ГОС.
     * @param model модель
     * @return список частей/компонентов
     */
    public List<EppPlanStructure> getParts(Model model);

    /**
     * Сохраняет конфигурацию циклов.
     * @param model модель
     */
    public void updateCycleConfiguration(Model model);

    /**
     * Удаляет лог импорта ИМЦА.
     * @param blockId id блока УПВ
     */
    public void deleteImtsaImportLog(Long blockId);


    /**
     * Сохраняет xml-файл ИМЦА.
     * @param model модель
     */
    public void saveImtsaXml(Model model);
}