package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Квалификация
 */
public class UsmaQualification extends UsmaQualificationGen
{
    public UsmaQualification()
    {

    }

    public UsmaQualification(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}