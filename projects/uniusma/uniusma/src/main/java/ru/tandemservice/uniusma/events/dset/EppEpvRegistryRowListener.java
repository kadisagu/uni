/**
 *$Id$
 */
package ru.tandemservice.uniusma.events.dset;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Zhebko
 * @since 26.02.2013
 */
public class EppEpvRegistryRowListener extends AbstractUsmaCompetenceRelListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppEpvRegistryRow.class, this, Collections.singleton(EppEpvRegistryRow.L_REGISTRY_ELEMENT));
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppEpvRegistryRow.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EppEpvRegistryRow.class, this);
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "row")
                .where(in(property("row.id"), event.getMultitude().getInExpression()))
                .column(property(EppEpvRegistryRow.owner().id().fromAlias("row")))
                .createStatement(event.getContext()).<Long>list();
    }
}