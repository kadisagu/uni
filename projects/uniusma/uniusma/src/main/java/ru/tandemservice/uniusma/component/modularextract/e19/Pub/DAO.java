/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e19.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.DischargingStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.DischargingStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 26.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e19.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e19.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        DischargingStuExtractUsmaExt extractExt = getByNaturalId(new DischargingStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        m.setExtUsmaExt(extractExt);
    }
}
