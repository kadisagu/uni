/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class Model extends CommonModularStudentExtractAddEditModel<UsmaExcludeAfterAcadWeekendStuExtract>
{
    private StudentStatus _studentStatusNew;
    private ISingleSelectModel _acadWeekendOrderModel;
    private AbstractStudentExtract _acadWeekendOrderSelected;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public ISingleSelectModel getacadWeekendOrderModel()
    {
        return _acadWeekendOrderModel;
    }

    public void setAcadWeekendOrderModel(ISingleSelectModel acadWeekendOrderModel)
    {
        _acadWeekendOrderModel = acadWeekendOrderModel;
    }

    public AbstractStudentExtract getAcadWeekendOrderSelected()
    {
        return _acadWeekendOrderSelected;
    }

    public void setAcadWeekendOrderSelected(AbstractStudentExtract acadWeekendOrderSelected)
    {
        _acadWeekendOrderSelected = acadWeekendOrderSelected;
    }
}
