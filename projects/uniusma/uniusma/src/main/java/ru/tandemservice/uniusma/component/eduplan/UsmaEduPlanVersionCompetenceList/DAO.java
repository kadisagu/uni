/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionCompetenceList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EpvRegistryRowRel;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final String REGISTRY_ROW_ALIAS = "row";

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        prepareDataSourceList(model);
    }

    private void prepareDataSourceList(Model model)
    {
        EppEduPlanVersion version = get(EppEduPlanVersion.class, model.getId());
        List<EppEduPlanVersionBlock> blocks = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.L_EDU_PLAN_VERSION, version);

        EppEduPlanVersionBlock rootBlock = null;  // базовый блок для данного, null если сам базовый
        if (blocks.size() > 1)
        {
            for (EppEduPlanVersionBlock block : blocks)
            {
                if (block.isRootBlock())
                {
                    rootBlock = block;
                }
            }

            blocks.remove(rootBlock);
        }

        model.setBlocks(blocks);

        for (EppEduPlanVersionBlock block : blocks)
        {
            StaticListDataSource<UsmaCompetenceListWrapper> dataSource = new StaticListDataSource<>();

            dataSource.addColumn(new SimpleColumn("Код компетенции", UsmaCompetenceListWrapper.COMPETENCE_CODE).setWidth(8).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Название", UsmaCompetenceListWrapper.COMPETENCE_TITLE).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Строка УП", UsmaCompetenceListWrapper.DISCIPLINE_TITLES).setFormatter(UsmaCompetenceListWrapper.USMA_EPV_ROW_FORMATTER).setClickable(false).setOrderable(false));

            dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickNumberUp").setPermissionKey("edit_eppCompetenceNumber"));
            dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickNumberDown").setPermissionKey("edit_eppCompetenceNumber"));

            DQLSelectBuilder competenceBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "comp2block")

                            /*0*/.column(property(UsmaCompetence2EduPlanVersionBlockRel.id().fromAlias("comp2block")))
                            /*1*/.column(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup().shortTitle().fromAlias("comp2block")))
                            /*2*/.column(property(UsmaCompetence2EduPlanVersionBlockRel.number().fromAlias("comp2block")))
                            /*3*/.column(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().title().fromAlias("comp2block")))

                            /*4*/.column(property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().id().fromAlias("comp2block")))

                    .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block().id().fromAlias("comp2block")), value(block.getId())));

            List<UsmaCompetenceListWrapper> wrapperList = new ArrayList<>();

            // в каких блоках искать дисциплины - в самом
            IDQLExpression blockExpression = eq(property(EppEpvRegistryRow.owner().fromAlias(REGISTRY_ROW_ALIAS)), value(block.getId()));

            // и в базовом, если такой есть
            if (rootBlock != null)
            {
                blockExpression = or(
                        blockExpression,
                        eq(property(EppEpvRegistryRow.owner().fromAlias(REGISTRY_ROW_ALIAS)), value(rootBlock.getId())));
            }

            for (Object[] row : competenceBuilder.createStatement(getSession()).<Object[]>list())
            {
                UsmaCompetenceListWrapper wrapper = new UsmaCompetenceListWrapper((Long) row[0], (String) row[1], (Integer) row[2], (String) row[3]);

                IDQLSelectableQuery regElCompetenceRowIdQuery = regElCompetenceRowIdQuery(blockExpression, (Long) row[4]);
                IDQLSelectableQuery epvRowCompetenceRowIdQuery = epvRowCompetenceRowIdQuery(blockExpression, (Long) row[4]);

                // компетенции формируются и строкой уп и дисциплиной реестра
                for (String rowTitle : getEpvRowTitles(regElCompetenceRowIdQuery, true, epvRowCompetenceRowIdQuery, true))
                {
                    wrapper.addRow(rowTitle, CompetenceColor.getCommonCompetenceColor());
                }

                // компетенции формируются только строкой уп
                for (String rowTitle : getEpvRowTitles(regElCompetenceRowIdQuery, false, epvRowCompetenceRowIdQuery, true))
                {
                    wrapper.addRow(rowTitle, CompetenceColor.getEpvRowCompetenceColor());
                }

                // компетенции формируются только дисциплиной реестра
                for (String rowTitle : getEpvRowTitles(regElCompetenceRowIdQuery, true, epvRowCompetenceRowIdQuery, false))
                {
                    wrapper.addRow(rowTitle, CompetenceColor.getRegistryElementCompetenceColor());
                }

                wrapperList.add(wrapper);
            }

            Collections.sort(wrapperList, UsmaCompetenceListWrapper.COMPETENCE_COMPARATOR);

            dataSource.setRowList(wrapperList);

            model.getDataSourceMap().put(block, dataSource);
        }
    }

    /**
     * Возвращает запрос с id строк блоков УП, заданных в blockExpression, дисциплины которых формируют данную компетенцию
     *
     * @param blockExpression where-statement в каких блоках искать строки УП
     * @param competenceId    идентификатор компетенции
     * @return Запрос, содержащий id строк УП
     */
    private IDQLSelectableQuery regElCompetenceRowIdQuery(IDQLExpression blockExpression, Long competenceId)
    {
        return new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2RegistryElementRel.class, "comp2regel")
                .joinEntity("comp2regel", DQLJoinType.inner, EppRegistryElement.class, "regel", eq(property(EppRegistryElement.id().fromAlias("regel")), property(UsmaCompetence2RegistryElementRel.registryElement().id().fromAlias("comp2regel"))))
                .joinEntity("regel", DQLJoinType.inner, EppEpvRegistryRow.class, REGISTRY_ROW_ALIAS, eq(property(EppEpvRegistryRow.registryElement().id().fromAlias(REGISTRY_ROW_ALIAS)), property(EppRegistryElement.id().fromAlias("regel"))))
                .column(property(EppEpvRegistryRow.id().fromAlias(REGISTRY_ROW_ALIAS)))
                .where(eq(property(UsmaCompetence2RegistryElementRel.usmaCompetence().id().fromAlias("comp2regel")), value(competenceId)))
                .where(blockExpression)
                .buildQuery();
    }

    /**
     * Возвращает запрос с id строк блоков УП, заданных в blockExpression, которые формируют данную компетенцию
     *
     * @param blockExpression where-statement в каких блоках искать строки УП
     * @param competenceId    идентификатор компетенции
     * @return Запрос, содержащий id строк УП
     */
    private IDQLSelectableQuery epvRowCompetenceRowIdQuery(IDQLExpression blockExpression, Long competenceId)
    {
        return new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EpvRegistryRowRel.class, "comp2row")
                .joinEntity("comp2row", DQLJoinType.inner, EppEpvRegistryRow.class, REGISTRY_ROW_ALIAS, eq(property(EppEpvRegistryRow.id().fromAlias(REGISTRY_ROW_ALIAS)), property(UsmaCompetence2EpvRegistryRowRel.registryRow().id().fromAlias("comp2row"))))
                .column(property(EppEpvRegistryRow.id().fromAlias(REGISTRY_ROW_ALIAS)))
                .where(eq(property(UsmaCompetence2EpvRegistryRowRel.usmaCompetence().id().fromAlias("comp2row")), value(competenceId)))
                .where(blockExpression)
                .buildQuery();
    }

    /**
     * Возвращает список названий строк УП, id которых содержатся в первом (если указан признак) и втором (если указан признак) запросе. Оба признака не могут быть false.
     *
     * @param regElQuery запрос с id строк УП
     * @param inRegEls признак выбора строк предыдущего запроса
     * @param epvRowQuery запрос с id строк УП
     * @param inEpvRows признак выбора строк предыдущего запроса
     * @return Списоск названий строк УП.
     */
    private List<String> getEpvRowTitles(IDQLSelectableQuery regElQuery, boolean inRegEls, IDQLSelectableQuery epvRowQuery, boolean inEpvRows)
    {
        if (!inRegEls && !inEpvRows)
            // возвращаемые строки должны быть хотя бы в одном из запросов
            throw new IllegalArgumentException();

        return new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "row")
                .column(DQLFunctions.concat(
                        property(EppEpvRegistryRow.title().fromAlias("row")),
                        caseExpr(
                                new IDQLExpression[]{eq(property(EppEpvRegistryRow.registryElementType().code().fromAlias("row")), value(EppRegistryStructureCodes.REGISTRY_DISCIPLINE))},
                                new IDQLExpression[]{value("")},
                                DQLFunctions.concat(
                                        value(" ("),
                                        property(EppEpvRegistryRow.registryElementType().title().fromAlias("row")),
                                        value(")")))))
                .where(and(

                        // id среди id строк, дисциплины которых формируют компетенцию
                        inRegEls ? in(property(EppEpvRegistryRow.id().fromAlias("row")), regElQuery) : notIn(property(EppEpvRegistryRow.id().fromAlias("row")), regElQuery),

                        // id среди id строк, которые формируют компетенцию
                        inEpvRows ? in(property(EppEpvRegistryRow.id().fromAlias("row")), epvRowQuery) : notIn(property(EppEpvRegistryRow.id().fromAlias("row")), epvRowQuery)))

                .createStatement(getSession())
                .<String>list();
    }


    @Override
    public void doNumberUp(Long id)
    {
        UsmaCompetence2EduPlanVersionBlockRel rel = get(id);

        int minNumber = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "comp2block")
                .column(DQLFunctions.min(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.number().fromAlias("comp2block"))))
                .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.block().fromAlias("comp2block")), DQLExpressions.value(rel.getBlock())))
                .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup().fromAlias("comp2block")), DQLExpressions.value(rel.getUsmaCompetence().getEppSkillGroup())))
                .createStatement(getSession())
                .uniqueResult();

        if (rel.getNumber() > minNumber) // по факту единица
        {
            int newNumber = rel.getNumber() - 1;

            new DQLUpdateBuilder(UsmaCompetence2EduPlanVersionBlockRel.class)
                    .set(UsmaCompetence2EduPlanVersionBlockRel.P_NUMBER, DQLExpressions.value(rel.getNumber()))
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.number()),
                            DQLExpressions.value(newNumber)))
                    .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.block()), DQLExpressions.value(rel.getBlock())))
                    .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup()), DQLExpressions.value(rel.getUsmaCompetence().getEppSkillGroup())))
                    .createStatement(getSession())
                    .execute();

            rel.setNumber(newNumber);

            update(rel);
        }
    }

    @Override
    public void doNumberDown(Long id)
    {
        UsmaCompetence2EduPlanVersionBlockRel rel = get(id);

        int maxNumber = new DQLSelectBuilder()
                .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "comp2block")
                .column(DQLFunctions.max(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.number().fromAlias("comp2block"))))
                .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.block().fromAlias("comp2block")), DQLExpressions.value(rel.getBlock())))
                .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup().fromAlias("comp2block")), DQLExpressions.value(rel.getUsmaCompetence().getEppSkillGroup())))
                .createStatement(getSession())
                .uniqueResult();

        if (rel.getNumber() < maxNumber)
        {
            int newNumber = rel.getNumber() + 1;

            new DQLUpdateBuilder(UsmaCompetence2EduPlanVersionBlockRel.class)
                    .set(UsmaCompetence2EduPlanVersionBlockRel.P_NUMBER, DQLExpressions.value(rel.getNumber()))
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.number()),
                            DQLExpressions.value(newNumber)))
                    .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.block()), DQLExpressions.value(rel.getBlock())))
                    .where(DQLExpressions.eq(DQLExpressions.property(UsmaCompetence2EduPlanVersionBlockRel.usmaCompetence().eppSkillGroup()), DQLExpressions.value(rel.getUsmaCompetence().getEppSkillGroup())))
                    .createStatement(getSession())
                    .execute();

            rel.setNumber(newNumber);

            update(rel);
        }
    }

    private static class CompetenceColor
    {
        private static final String DEFAULT_EPV_ROW_COMPETENCE_COLOR = "#DB0000";
        private static String epvRowCompetenceColor;
        private static String getEpvRowCompetenceColor()
        {
            if (epvRowCompetenceColor == null)
            {
                epvRowCompetenceColor = getColor("epvRowCompetenceColor", DEFAULT_EPV_ROW_COMPETENCE_COLOR);
            }

            return epvRowCompetenceColor;
        }

        private static final String DEFAULT_REG_ELEMENT_COMPETENCE_COLOR = "#006600";
        private static String registryElementCompetenceColor;
        private static String getRegistryElementCompetenceColor()
        {
            if (registryElementCompetenceColor == null)
            {
                registryElementCompetenceColor = getColor("registryElementCompetenceColor", DEFAULT_REG_ELEMENT_COMPETENCE_COLOR);
            }

            return registryElementCompetenceColor;
        }

        private static final String DEFAULT_COMMON_COMPETENCE_COLOR = "#000000";
        private static String commonCompetenceColor;
        private static String getCommonCompetenceColor()
        {
            if (commonCompetenceColor == null)
            {
                commonCompetenceColor = getColor("commonCompetenceColor", DEFAULT_COMMON_COMPETENCE_COLOR);
            }

            return commonCompetenceColor;
        }

        private static String getColor(String propertyName, String defaultValue)
        {
            String color = StringUtils.trimToNull(ApplicationRuntime.getProperty(propertyName));
            return color == null ? defaultValue : color;
        }
    }
}