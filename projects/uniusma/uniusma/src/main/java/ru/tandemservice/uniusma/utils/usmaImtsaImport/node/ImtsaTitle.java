/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniusma.entity.UsmaEpvBlockTitle;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.GEIntegerValidator;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.InValuesValidator;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.LEIntegerValidator;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.Validator;

import java.util.*;

/**
 * Титул ИМЦА.
 * @author Alexander Zhebko
 * @since 22.07.2013
 */
public class ImtsaTitle
{
    // представления
    public static final IAttributeView ATTRIBUTE_VIEW = new TitleAttributeView();


    // импортируемые данные
    private Map<String, Object> _attributes;
    private List<ImtsaDeveloper> _developerList;
    private List<ImtsaQualification> _qualificationList;
    private List<ImtsaSpeciality> _specialityList;
    private ImtsaCycles _cycles;
    private ImtsaSchedule _schedule;

    public Map<String, Object> getAttributes(){ return _attributes; }
    public void setAttributes(Map<String, Object> attributes){ _attributes = attributes; }

    public List<ImtsaDeveloper> getDeveloperList(){ return _developerList; }
    public void setDeveloperList(List<ImtsaDeveloper> developerList){ _developerList = developerList; }

    public List<ImtsaQualification> getQualificationList(){ return _qualificationList; }
    public void setQualificationList(List<ImtsaQualification> qualificationList){ _qualificationList = qualificationList; }

    public List<ImtsaSpeciality> getSpecialityList(){ return _specialityList;}
    public void setSpecialityList(List<ImtsaSpeciality> specialityList){ _specialityList = specialityList; }

    public ImtsaCycles getCycles(){ return _cycles; }
    public void setCycles(ImtsaCycles cycles){ _cycles = cycles; }

    public ImtsaSchedule getSchedule(){ return _schedule; }
    public void setSchedule(ImtsaSchedule schedule){ _schedule = schedule; }


    // вспомогательные данные
    public static Map<String,String> getAttributePropertyMap(){ return TitleAttributeView.ATTRIBUTE_PROPERTY_MAP; }

    public static String getDevelopersNodeName(){ return DEVELOPERS_NODE_NAME; }
    public static String getQualificationsNodeName(){ return QUALIFICATIONS_NODE_NAME; }
    public static String getSpecialitiesNodeName(){ return SPECIALITIES_NODE_NAME; }
    public static String getCyclesNodeName(){ return CYCLES_NODE_NAME; }
    public static String getCyclesNewNodeName(){ return CYCLES_NEW_NODE_NAME; }
    public static String getScheduleNodeName(){ return SCHEDULE_NODE_NAME; }

    public static String getLevelCodeAttribute(){ return TitleAttributeView.A_LEVEL_CODE; }
    public static String getZetInWeeksAttAttribute(){ return TitleAttributeView.A_ZET_IN_WEEK_ATT; }
    public static String getHoursInZetAttAttribute(){ return TitleAttributeView.A_HOURS_AMOUNT_IN_Z_E_T_ATT; }
    public static String getDissertationAsAttestationAttribute(){ return TitleAttributeView.A_DISSERTATION_AS_ATTESTATION; }
    public static String getStateExamAsAttestationAttribute(){ return TitleAttributeView.A_STATE_EXAM_AS_ATTESTATION; }

    public static String getLevelCodeM(){ return LEVEL_CODE_M; }

    public static Double getDefaultZetInWeeksAtt(){ return DEFAULT_ZET_IN_WEEKS_ATT; }
    public static Double getDefaultHoursInZetAtt(){ return DEFAULT_HOURS_IN_ZET_ATT; }

    private static final String LEVEL_CODE_B = "B";
    private static final String LEVEL_CODE_M = "M";
    private static final String LEVEL_CODE_S = "S";

    private static final Double DEFAULT_ZET_IN_WEEKS_ATT = 1.5d;
    private static final Double DEFAULT_HOURS_IN_ZET_ATT = 36.0d;

    /*todo значения по умолчанию*/
    private static final boolean DEFAULT_DISSERTATION_AS_ATTESTATION = true;
    private static final boolean DEFAULT_STATE_EXAM_AS_ATTESTATION = true;

    /**
     * Представление атрибутов нода "Титул".
     */
    private static class TitleAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String A_PLAN_FULL_TITLE = "ПолноеИмяПлана";
        private static final String A_PLAN_TITLE = "ИмяПлана";
        private static final String A_USER_NUMBER = "НомерПользователя";
        private static final String A_ACADEMY = "ИмяВуза";
        private static final String A_ORG_UNIT = "ИмяВуза2";
        private static final String A_HIGHER_ECHELON = "Головная";
        private static final String A_PRODUCING_ORG_UNIT = "КодКафедры";
        private static final String A_FORMATIVE_ORG_UNIT = "Факультет";
        private static final String A_EDUCATION_DIRECTION_CODE = "ПоследнийШифр";
        private static final String A_START_YEAR = "ГодНачалаПодготовки";
        private static final String A_INCLUDE_EXAMS_IN_HOURS_AMOUNT = "ВключатьЭкВСуммуЧасов";
        private static final String A_DISSERTATION_AS_ATTESTATION = "ДвИГА";
        private static final String A_STATE_EXAM_AS_ATTESTATION = "ГвИГА";
        private static final String A_KSR_OR_INDIVIDUAL_LESSONS = "КСР_ИЗ";
        private static final String A_ZET_IN_WEEK_ATT = "ИГА_ЗЕТвНеделе";
        private static final String A_HOURS_AMOUNT_IN_Z_E_T_ATT = "ИГА_ЧасовВЗЕТ";
        private static final String A_PROGRAM_LABOUR_UNITS = "ООПет";
        private static final String A_INTERACTIVE_LESSONS_PERCENT = "Интер";
        private static final String A_LECTURES_PERCENT = "Лекц";
        private static final String A_CHOICE_DISCIPLINES_PERCENT = "ДВВ";
        private static final String A_MAX_SIZE = "МаксНагр";
        private static final String A_PLAN_KIND = "ВидПлана";
        private static final String A_LEVEL_CODE = "КодУровня";
        private static final String A_LEVEL = "Уровень";
        private static final String A_TERMS_IN_COURSE = "СеместровНаКурсе";
        private static final String A_ELEMENTS_IN_WEEK = "ЭлементовВНеделе";
        private static final String A_STATE_EXAM_DATE = "ДатаГОСа";
        private static final String A_STATE_EXAM_DOC = "ДокументГОСа";
        private static final String A_STATE_EXAM_TYPE = "ТипГОСа";
        private static final String A_APPLICATION = "Приложение";
        private static final String A_APPLICATION_DATE = "ДатаПриложения";
        private static final String A_APPLICATION_VERSION = "ВерсияПриложения";
        private static final String A_ZET_IN_YEAR = "ЗЕТнаГОД";
        private static final String A_ZET_IN_WEEK = "ЗЕТвНеделе";
        private static final String A_HOURS_AMOUNT_IN_Z_E_T = "ЧасовВЗЕТ";
        private static final String A_TOTAL_Z_E_T = "ЗЕТнаВСЕ";
        private static final String A_CERTIFICATE_DATE = "ДатаСертификатаИМЦА";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(A_START_YEAR, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(A_INCLUDE_EXAMS_IN_HOURS_AMOUNT, Boolean.class);
            ATTRIBUTE_TYPE_MAP.put(A_DISSERTATION_AS_ATTESTATION, Boolean.class);
            ATTRIBUTE_TYPE_MAP.put(A_STATE_EXAM_AS_ATTESTATION, Boolean.class);
            ATTRIBUTE_TYPE_MAP.put(A_ZET_IN_WEEK_ATT, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_HOURS_AMOUNT_IN_Z_E_T_ATT, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_PROGRAM_LABOUR_UNITS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_INTERACTIVE_LESSONS_PERCENT, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_LECTURES_PERCENT, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_CHOICE_DISCIPLINES_PERCENT, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_MAX_SIZE, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_TERMS_IN_COURSE, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(A_ELEMENTS_IN_WEEK, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(A_STATE_EXAM_DATE, Date.class);
            ATTRIBUTE_TYPE_MAP.put(A_APPLICATION_DATE, Date.class);
            ATTRIBUTE_TYPE_MAP.put(A_ZET_IN_YEAR, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_ZET_IN_WEEK, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_HOURS_AMOUNT_IN_Z_E_T, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_TOTAL_Z_E_T, Double.class);
            ATTRIBUTE_TYPE_MAP.put(A_CERTIFICATE_DATE, Date.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                A_PLAN_KIND,
                A_LEVEL_CODE
        );


        //4. отображение названия атрибута на список его валидаторов
        private static Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(A_START_YEAR, Arrays.<Validator<?>>asList(new GEIntegerValidator(1900), new LEIntegerValidator(2100)));
            ATTRIBUTE_VALIDATOR_MAP.put(A_KSR_OR_INDIVIDUAL_LESSONS, Arrays.<Validator<?>>asList(new InValuesValidator("КСР", "ИЗ")));
            ATTRIBUTE_VALIDATOR_MAP.put(A_INTERACTIVE_LESSONS_PERCENT, Validator.DOUBLE_PERCENT);
            ATTRIBUTE_VALIDATOR_MAP.put(A_LECTURES_PERCENT, Validator.DOUBLE_PERCENT);
            ATTRIBUTE_VALIDATOR_MAP.put(A_CHOICE_DISCIPLINES_PERCENT, Validator.DOUBLE_PERCENT);

            ATTRIBUTE_VALIDATOR_MAP.put(A_LEVEL_CODE, Arrays.<Validator<?>>asList(new InValuesValidator(LEVEL_CODE_B, LEVEL_CODE_M, LEVEL_CODE_S)));
            ATTRIBUTE_VALIDATOR_MAP.put(A_TERMS_IN_COURSE, Arrays.<Validator<?>>asList(new InValuesValidator(1, 2, 3, 4)));
            ATTRIBUTE_VALIDATOR_MAP.put(A_ELEMENTS_IN_WEEK, Arrays.<Validator<?>>asList(new InValuesValidator(1, 2, 3, 6)));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Arrays.asList(
                A_KSR_OR_INDIVIDUAL_LESSONS,
                A_LEVEL_CODE
        );

        // 6. поля сущности
        private static final String P_PLAN_FULL_TITLE = UsmaEpvBlockTitle.P_PLAN_FULL_TITLE;
        private static final String P_PLAN_TITLE = UsmaEpvBlockTitle.P_PLAN_TITLE;
        private static final String P_USER_NUMBER = UsmaEpvBlockTitle.P_USER_NUMBER;
        private static final String P_ACADEMY = UsmaEpvBlockTitle.P_ACADEMY;
        private static final String P_ORG_UNIT = UsmaEpvBlockTitle.P_ORG_UNIT;
        private static final String P_HIGHER_ECHELON = UsmaEpvBlockTitle.P_HIGHER_ECHELON;
        private static final String P_PRODUCING_ORG_UNIT = UsmaEpvBlockTitle.P_PRODUCING_ORG_UNIT;
        private static final String P_FORMATIVE_ORG_UNIT = UsmaEpvBlockTitle.P_FORMATIVE_ORG_UNIT;
        private static final String P_EDUCATION_DIRECTION_CODE = UsmaEpvBlockTitle.P_EDUCATION_DIRECTION_CODE;
        private static final String P_START_YEAR = UsmaEpvBlockTitle.P_START_YEAR;
        private static final String P_INCLUDE_EXAMS_IN_HOURS_AMOUNT = UsmaEpvBlockTitle.P_INCLUDE_EXAMS_IN_HOURS_AMOUNT;
        private static final String P_DISSERTATION_AS_ATTESTATION = UsmaEpvBlockTitle.P_DISSERTATION_AS_ATTESTATION;
        private static final String P_STATE_EXAM_AS_ATTESTATION = UsmaEpvBlockTitle.P_STATE_EXAM_AS_ATTESTATION;
        private static final String P_KSR_OR_INDIVIDUAL_LESSONS = UsmaEpvBlockTitle.P_KSR_OR_INDIVIDUAL_LESSONS;
        private static final String P_ZET_IN_WEEK_ATT = UsmaEpvBlockTitle.P_ZET_IN_WEEK_ATT;
        private static final String P_HOURS_AMOUNT_IN_Z_E_T_ATT = UsmaEpvBlockTitle.P_HOURS_AMOUNT_IN_Z_E_T_ATT;
        private static final String P_PROGRAM_LABOUR_UNITS = UsmaEpvBlockTitle.P_PROGRAM_LABOUR_UNITS;
        private static final String P_INTERACTIVE_LESSONS_PERCENT = UsmaEpvBlockTitle.P_INTERACTIVE_LESSONS_PERCENT;
        private static final String P_LECTURES_PERCENT = UsmaEpvBlockTitle.P_LECTURES_PERCENT;
        private static final String P_CHOICE_DISCIPLINES_PERCENT = UsmaEpvBlockTitle.P_CHOICE_DISCIPLINES_PERCENT;
        private static final String P_MAX_SIZE = UsmaEpvBlockTitle.P_MAX_SIZE;
        private static final String P_PLAN_KIND = UsmaEpvBlockTitle.P_PLAN_KIND;
        private static final String P_LEVEL_CODE = UsmaEpvBlockTitle.P_LEVEL_CODE;
        private static final String P_LEVEL = UsmaEpvBlockTitle.P_LEVEL;
        private static final String P_TERMS_IN_COURSE = UsmaEpvBlockTitle.P_TERMS_IN_COURSE;
        private static final String P_ELEMENTS_IN_WEEK = UsmaEpvBlockTitle.P_ELEMENTS_IN_WEEK;
        private static final String P_STATE_EXAM_DATE = UsmaEpvBlockTitle.P_STATE_EXAM_DATE;
        private static final String P_STATE_EXAM_DOC = UsmaEpvBlockTitle.P_STATE_EXAM_DOC;
        private static final String P_STATE_EXAM_TYPE = UsmaEpvBlockTitle.P_STATE_EXAM_TYPE;
        private static final String P_APPLICATION = UsmaEpvBlockTitle.P_APPLICATION;
        private static final String P_APPLICATION_DATE = UsmaEpvBlockTitle.P_APPLICATION_DATE;
        private static final String P_APPLICATION_VERSION = UsmaEpvBlockTitle.P_APPLICATION_VERSION;
        private static final String P_ZET_IN_YEAR = UsmaEpvBlockTitle.P_ZET_IN_YEAR;
        private static final String P_ZET_IN_WEEK = UsmaEpvBlockTitle.P_ZET_IN_WEEK;
        private static final String P_HOURS_AMOUNT_IN_Z_E_T = UsmaEpvBlockTitle.P_HOURS_AMOUNT_IN_Z_E_T;
        private static final String P_TOTAL_Z_E_T = UsmaEpvBlockTitle.P_TOTAL_Z_E_T;
        private static final String P_CERTIFICATE_DATE = UsmaEpvBlockTitle.P_CERTIFICATE_DATE;


        // 7. отображение названия атрибута на название поля сущности
        public static Map<String, String> ATTRIBUTE_PROPERTY_MAP = new HashMap<>();
        static
        {
            ATTRIBUTE_PROPERTY_MAP.put(A_PLAN_FULL_TITLE, P_PLAN_FULL_TITLE);
            ATTRIBUTE_PROPERTY_MAP.put(A_PLAN_TITLE, P_PLAN_TITLE);
            ATTRIBUTE_PROPERTY_MAP.put(A_USER_NUMBER, P_USER_NUMBER);
            ATTRIBUTE_PROPERTY_MAP.put(A_ACADEMY, P_ACADEMY);
            ATTRIBUTE_PROPERTY_MAP.put(A_ORG_UNIT, P_ORG_UNIT);
            ATTRIBUTE_PROPERTY_MAP.put(A_HIGHER_ECHELON, P_HIGHER_ECHELON);
            ATTRIBUTE_PROPERTY_MAP.put(A_PRODUCING_ORG_UNIT, P_PRODUCING_ORG_UNIT);
            ATTRIBUTE_PROPERTY_MAP.put(A_FORMATIVE_ORG_UNIT, P_FORMATIVE_ORG_UNIT);
            ATTRIBUTE_PROPERTY_MAP.put(A_EDUCATION_DIRECTION_CODE, P_EDUCATION_DIRECTION_CODE);
            ATTRIBUTE_PROPERTY_MAP.put(A_START_YEAR, P_START_YEAR);
            ATTRIBUTE_PROPERTY_MAP.put(A_INCLUDE_EXAMS_IN_HOURS_AMOUNT, P_INCLUDE_EXAMS_IN_HOURS_AMOUNT);
            ATTRIBUTE_PROPERTY_MAP.put(A_DISSERTATION_AS_ATTESTATION, P_DISSERTATION_AS_ATTESTATION);
            ATTRIBUTE_PROPERTY_MAP.put(A_STATE_EXAM_AS_ATTESTATION, P_STATE_EXAM_AS_ATTESTATION);
            ATTRIBUTE_PROPERTY_MAP.put(A_KSR_OR_INDIVIDUAL_LESSONS, P_KSR_OR_INDIVIDUAL_LESSONS);
            ATTRIBUTE_PROPERTY_MAP.put(A_ZET_IN_WEEK_ATT, P_ZET_IN_WEEK_ATT);
            ATTRIBUTE_PROPERTY_MAP.put(A_HOURS_AMOUNT_IN_Z_E_T_ATT, P_HOURS_AMOUNT_IN_Z_E_T_ATT);
            ATTRIBUTE_PROPERTY_MAP.put(A_PROGRAM_LABOUR_UNITS, P_PROGRAM_LABOUR_UNITS);
            ATTRIBUTE_PROPERTY_MAP.put(A_INTERACTIVE_LESSONS_PERCENT, P_INTERACTIVE_LESSONS_PERCENT);
            ATTRIBUTE_PROPERTY_MAP.put(A_LECTURES_PERCENT, P_LECTURES_PERCENT);
            ATTRIBUTE_PROPERTY_MAP.put(A_CHOICE_DISCIPLINES_PERCENT, P_CHOICE_DISCIPLINES_PERCENT);
            ATTRIBUTE_PROPERTY_MAP.put(A_MAX_SIZE, P_MAX_SIZE);
            ATTRIBUTE_PROPERTY_MAP.put(A_PLAN_KIND, P_PLAN_KIND);
            ATTRIBUTE_PROPERTY_MAP.put(A_LEVEL_CODE, P_LEVEL_CODE);
            ATTRIBUTE_PROPERTY_MAP.put(A_LEVEL, P_LEVEL);
            ATTRIBUTE_PROPERTY_MAP.put(A_TERMS_IN_COURSE, P_TERMS_IN_COURSE);
            ATTRIBUTE_PROPERTY_MAP.put(A_ELEMENTS_IN_WEEK, P_ELEMENTS_IN_WEEK);
            ATTRIBUTE_PROPERTY_MAP.put(A_STATE_EXAM_DATE, P_STATE_EXAM_DATE);
            ATTRIBUTE_PROPERTY_MAP.put(A_STATE_EXAM_DOC, P_STATE_EXAM_DOC);
            ATTRIBUTE_PROPERTY_MAP.put(A_STATE_EXAM_TYPE, P_STATE_EXAM_TYPE);
            ATTRIBUTE_PROPERTY_MAP.put(A_APPLICATION, P_APPLICATION);
            ATTRIBUTE_PROPERTY_MAP.put(A_APPLICATION_DATE, P_APPLICATION_DATE);
            ATTRIBUTE_PROPERTY_MAP.put(A_APPLICATION_VERSION, P_APPLICATION_VERSION);
            ATTRIBUTE_PROPERTY_MAP.put(A_ZET_IN_YEAR, P_ZET_IN_YEAR);
            ATTRIBUTE_PROPERTY_MAP.put(A_ZET_IN_WEEK, P_ZET_IN_WEEK);
            ATTRIBUTE_PROPERTY_MAP.put(A_HOURS_AMOUNT_IN_Z_E_T, P_HOURS_AMOUNT_IN_Z_E_T);
            ATTRIBUTE_PROPERTY_MAP.put(A_TOTAL_Z_E_T, P_TOTAL_Z_E_T);
            ATTRIBUTE_PROPERTY_MAP.put(A_CERTIFICATE_DATE, P_CERTIFICATE_DATE);
        }
    }


    // ноды
    private static final String DEVELOPERS_NODE_NAME = "Разработчики";
    private static final String QUALIFICATIONS_NODE_NAME = "Квалификации";
    private static final String SPECIALITIES_NODE_NAME = "Специальности";
    private static final String CYCLES_NODE_NAME = "АтрибутыЦиклов";
    private static final String CYCLES_NEW_NODE_NAME = "АтрибутыЦикловНов";
    private static final String SCHEDULE_NODE_NAME = "ГрафикУчПроцесса";
}