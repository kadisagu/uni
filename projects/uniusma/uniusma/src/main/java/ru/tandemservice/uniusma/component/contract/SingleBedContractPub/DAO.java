/* $Id$ */
package ru.tandemservice.uniusma.component.contract.SingleBedContractPub;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unisettle.component.contract.SingleBedContractPub.Model;
import ru.tandemservice.unisettle.entity.catalog.UnisettleTemplateDocument;
import ru.tandemservice.unisettle.entity.settlement.UnisettleContractAccomodation;
import ru.tandemservice.unisettle.entity.settlement.UnisettleSingleBedContract;

import java.text.SimpleDateFormat;

/**
 * @author Andrey Avetisov
 * @since 31.07.2015
 */
public class DAO extends ru.tandemservice.unisettle.component.contract.SingleBedContractPub.DAO
{
    @Override
    public byte[] printContract(Model model)
    {
        UnisettleTemplateDocument template = getCatalogItem(UnisettleTemplateDocument.class, "singleContract");
        RtfDocument document = new RtfReader().read(template.getContent());

        UnisettleSingleBedContract contract = model.getContract();
        UnisettleContractAccomodation accomodation = model.getAccomodation();
        IdentityCard card = contract.getResident().getPerson().getIdentityCard();

        TopOrgUnit academy = TopOrgUnit.getInstance();
        EmployeePost rector = EmployeeManager.instance().dao().getHead(academy);

        RtfInjectModifier modifier = new RtfInjectModifier();

        modifier.put("number", contract.getNumber());
        modifier.put("city", academy.getAddress() == null ? "" : academy.getAddress().getSettlement().getTitleWithType());
        modifier.put("regDate", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(contract.getRegDate()) + "г.");
        modifier.put("residentFio", card.getFullFio());
        modifier.put("residentBirthday", DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()));
        modifier.put("placeNumber", accomodation == null ? "-" : accomodation.getPlace().getPlace().getNumber());
        modifier.put("buildingNumber", accomodation == null ? "-" : accomodation.getPlace().getPlace().getFloor().getUnit().getBuilding().getIdentifier());
        modifier.put("address", accomodation == null ? "-" : accomodation.getPlace().getPlace().getFloor().getUnit().getBuilding().getAddress().getTitleWithFlat());
        modifier.put("academy", academy.getPrintTitle());
        modifier.put("academyAddress", academy.getAddress() == null ? "-" : academy.getAddress().getTitleWithFlat());
        modifier.put("rectorFio", rector == null ? "-" : rector.getEmployee().getPerson().getFullFio());
        modifier.put("residentPassport", card.getTitle());

        String flatArea = accomodation!=null&&accomodation.getPlace().getPlace().getFractionalArea()!=null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(accomodation.getPlace().getPlace().getFractionalArea()):"";
        modifier.put("flatArea", String.valueOf(flatArea));
        modifier.put("checkoutDate", contract.getCheckoutDate() != null ? new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(contract.getCheckoutDate()) + "г." : "");
        modifier.put("registrationAddress", card.getAddress() != null ? card.getAddress().getTitleWithFlat() : "");

        modifier.modify(document);

        return RtfUtil.toByteArray(document);
    }
}
