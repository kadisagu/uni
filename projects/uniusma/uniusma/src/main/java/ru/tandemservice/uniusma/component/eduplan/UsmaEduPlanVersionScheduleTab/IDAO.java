/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionScheduleTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public interface IDAO extends IUniDao<Model>
{
    // подготавливаем данные для редактирования строки учебного графика
    void prepareEditRow(Model model, Long rowId);

    // сохраняет изменения в таблице для строки
    void updateScheduleRow(Long rowId, Model model);

    /**
     * Проверяет тип разбиения учебного графика УПв. Если тип разбиения не указан - указывается "недельный"
     * @param model модель
     */
    public void updateEduPlanVersionPartitionType(Model model);
}