/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class UsmaExcludeAfterAcadWeekendStuExtractPrint implements IPrintFormCreator<UsmaExcludeAfterAcadWeekendStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, UsmaExcludeAfterAcadWeekendStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Person person = extract.getEntity().getPerson();

        boolean isMale = person.isMale();
        short i = 1;

        modifier.put("weekendOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getAcadWeekendOrderDate()));
        modifier.put("weekendOrderNumber", extract.getAcadWeekendOrderNumber());
        modifier.put("weekendStartDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginAcadWeekendDate()));
        modifier.put("weekendEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndAcadWeekendDate()));
        modifier.put("started", "приступил" + (isMale ? "" : "а"));
        modifier.put("notStarted_A", "не приступивш" + (isMale ? "его" : "ую"));
        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getExcludeDate()));
        modifier.put("stopPayments", (extract.isStopGrantsPaying() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") + " отменить выплату академической стипендии с " +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStopPayingDate()) + " г." ) : ""));
        modifier.put("giveDiploma", (extract.isGiveDiploma() ?
                (String.valueOf(++i) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") + " выдать диплом о незаконченном высшем образовании.") : ""));

        boolean budget = extract.getEntity().getCompensationType().isBudget();
        modifier.put("compensationTypeStr_G", budget ? "бюджетной основы" : "внебюджетной основы");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

}
