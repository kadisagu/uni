package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Тип разбиения для УПв
 */
public class UsmaEduPlanVersionPartitionType extends UsmaEduPlanVersionPartitionTypeGen
{
    public UsmaEduPlanVersionPartitionType()
    {
    }

    public UsmaEduPlanVersionPartitionType(EppEduPlanVersion version, UsmaSchedulePartitionType partitionType)
    {
        this.setVersion(version);
        this.setPartitionType(partitionType);
    }
}