/* $Id$ */
package ru.tandemservice.nsiclient;

import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.datagram.XDatagram;
import ru.tandemservice.nsiclient.utils.INsiBean;

/**
 * @author Andrey Avetisov
 * @since 04.12.2015
 */
public class NsiBean implements INsiBean
{
    public IXDatagram createXDatagram() {
        return new XDatagram();
    }
}
  