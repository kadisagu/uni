package ru.tandemservice.uniusma.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Карточка приказа о зачислении абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaEnrollmentOrderCardGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard";
    public static final String ENTITY_NAME = "usmaEnrollmentOrderCard";
    public static final int VERSION_HASH = 440987663;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_PROTOCOL_TITLE = "protocolTitle";
    public static final String P_CONTRACT_WITH = "contractWith";

    private EnrollmentOrder _order;     // Приказ о зачислении абитуриентов
    private String _protocolTitle;     // Протокол заседания приемной комиссии
    private String _contractWith;     // Договор на целевую подготовку с

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(EnrollmentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Протокол заседания приемной комиссии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolTitle()
    {
        return _protocolTitle;
    }

    /**
     * @param protocolTitle Протокол заседания приемной комиссии. Свойство не может быть null.
     */
    public void setProtocolTitle(String protocolTitle)
    {
        dirty(_protocolTitle, protocolTitle);
        _protocolTitle = protocolTitle;
    }

    /**
     * @return Договор на целевую подготовку с.
     */
    @Length(max=255)
    public String getContractWith()
    {
        return _contractWith;
    }

    /**
     * @param contractWith Договор на целевую подготовку с.
     */
    public void setContractWith(String contractWith)
    {
        dirty(_contractWith, contractWith);
        _contractWith = contractWith;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaEnrollmentOrderCardGen)
        {
            setOrder(((UsmaEnrollmentOrderCard)another).getOrder());
            setProtocolTitle(((UsmaEnrollmentOrderCard)another).getProtocolTitle());
            setContractWith(((UsmaEnrollmentOrderCard)another).getContractWith());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaEnrollmentOrderCardGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaEnrollmentOrderCard.class;
        }

        public T newInstance()
        {
            return (T) new UsmaEnrollmentOrderCard();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "protocolTitle":
                    return obj.getProtocolTitle();
                case "contractWith":
                    return obj.getContractWith();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrder) value);
                    return;
                case "protocolTitle":
                    obj.setProtocolTitle((String) value);
                    return;
                case "contractWith":
                    obj.setContractWith((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "protocolTitle":
                        return true;
                case "contractWith":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "protocolTitle":
                    return true;
                case "contractWith":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrder.class;
                case "protocolTitle":
                    return String.class;
                case "contractWith":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaEnrollmentOrderCard> _dslPath = new Path<UsmaEnrollmentOrderCard>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaEnrollmentOrderCard");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard#getOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Протокол заседания приемной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard#getProtocolTitle()
     */
    public static PropertyPath<String> protocolTitle()
    {
        return _dslPath.protocolTitle();
    }

    /**
     * @return Договор на целевую подготовку с.
     * @see ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard#getContractWith()
     */
    public static PropertyPath<String> contractWith()
    {
        return _dslPath.contractWith();
    }

    public static class Path<E extends UsmaEnrollmentOrderCard> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _order;
        private PropertyPath<String> _protocolTitle;
        private PropertyPath<String> _contractWith;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard#getOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> order()
        {
            if(_order == null )
                _order = new EnrollmentOrder.Path<EnrollmentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Протокол заседания приемной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard#getProtocolTitle()
     */
        public PropertyPath<String> protocolTitle()
        {
            if(_protocolTitle == null )
                _protocolTitle = new PropertyPath<String>(UsmaEnrollmentOrderCardGen.P_PROTOCOL_TITLE, this);
            return _protocolTitle;
        }

    /**
     * @return Договор на целевую подготовку с.
     * @see ru.tandemservice.uniusma.entity.orders.UsmaEnrollmentOrderCard#getContractWith()
     */
        public PropertyPath<String> contractWith()
        {
            if(_contractWith == null )
                _contractWith = new PropertyPath<String>(UsmaEnrollmentOrderCardGen.P_CONTRACT_WITH, this);
            return _contractWith;
        }

        public Class getEntityClass()
        {
            return UsmaEnrollmentOrderCard.class;
        }

        public String getEntityName()
        {
            return "usmaEnrollmentOrderCard";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
