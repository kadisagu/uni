/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.OtherAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniusma.entity.catalog.UsmaHealthGroup;

/**
 * @author DMITRY KNYAZEV
 * @since 06.08.2014
 */
@Configuration
public class UsmaMedCardOtherAddEdit extends BusinessComponentManager
{
	public static final String HEALTH_GROUP_DS = "healthGroupDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(HEALTH_GROUP_DS, healthGroupDSHandler()))
				.addDataSource(CommonManager.instance().yesNoDSConfig())
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> healthGroupDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), UsmaHealthGroup.class)
				.order(UsmaHealthGroup.title());
	}
}
