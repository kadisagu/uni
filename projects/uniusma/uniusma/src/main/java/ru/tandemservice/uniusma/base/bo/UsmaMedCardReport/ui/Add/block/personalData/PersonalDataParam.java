/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.personalData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.UsmaMedCardReportPrinter;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.medCard.MedCardParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.util.IUsmaDQLReportModifier;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 03.02.2015
 */
public class PersonalDataParam implements IUsmaDQLReportModifier
{
    //DATA SOURCE
    public static final String GROUP_DS = "groupDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";
    public static final String SEX_DS = "sexDS";

    //at Student entity
    private final IReportParam<Collection<Course>> _courseList = new ReportParam<>();
    private final IReportParam<Collection<Group>> _groupList = new ReportParam<>();
    private final IReportParam<StudentCategory> _studentCategory = new ReportParam<>();
    private final IReportParam<Collection<StudentStatus>> _studentStatusList = new ReportParam<>();
    private final IReportParam<DataWrapper> _archival = new ReportParam<>();
    private final IReportParam<Collection<DevelopForm>> _developFormList = new ReportParam<>();
    private final IReportParam<Long> _entranceYear = new ReportParam<>();
    private final IReportParam<Long> _age = new ReportParam<>();
    private final IReportParam<Sex> _sex = new ReportParam<>();

    @Override
    public void modify(String alias, DQLSelectBuilder dql)
    {
        if (getCourseList().isActive() && !getCourseList().getData().isEmpty())
        {
            dql.where(in(property(alias, Student.course()), getCourseList().getData()));
        }
        if (getGroupList().isActive() && !getGroupList().getData().isEmpty())
        {
            dql.where(in(property(alias, Student.group()), getGroupList().getData()));
        }
        if (getStudentCategory().isActive())
        {
            dql.where(eq(property(alias, Student.studentCategory()), value(getStudentCategory().getData())));
        }
        if (getStudentStatusList().isActive() && !getStudentStatusList().getData().isEmpty())
        {
            dql.where(in(property(alias, Student.status()), getStudentStatusList().getData()));
        }
        if (getArchival().isActive())
        {
            dql.where(eq(property(alias, Student.archival()), value(getBooleanFromWrapper(getArchival()))));
        }
        if (getDevelopFormList().isActive() && !getDevelopFormList().getData().isEmpty())
        {
            dql.where(in(property(alias, Student.educationOrgUnit().developForm()), getDevelopFormList().getData()));
        }
        if (getEntranceYear().isActive())
        {
            dql.where(eq(property(alias, Student.entranceYear()), value(getEntranceYear().getData())));
        }
        if (getAge().isActive())
        {
            dql.where(eq(DQLFunctions.diffyears(DQLExpressions.valueDate(new Date()), DQLExpressions.property(alias, Student.person().identityCard().birthDate())), value(getAge().getData())));
        }
        if (getSex().isActive())
        {
            dql.where(eq(DQLExpressions.property(alias, Student.person().identityCard().sex()), value(getSex().getData())));
        }

    }

    @Override
    public void addParameters(List<String> parameters)
    {
        final String separator = UsmaMedCardReportPrinter.CSV_SEPARATOR;
        if (getCourseList().isActive())
        {
            parameters.add("Курс" + separator + CommonBaseStringUtil.join(getCourseList().getData(), Course.P_TITLE, ", "));
        }
        if (getGroupList().isActive())
        {
            parameters.add("Группа" + separator + CommonBaseStringUtil.join(getGroupList().getData(), Group.P_TITLE, ", "));
        }
        if (getStudentCategory().isActive())
        {
            parameters.add("Является студентом" + separator + getStudentCategory().getData().getTitle());
        }
        if (getStudentStatusList().isActive())
        {
            parameters.add("Состояние студента" + separator + CommonBaseStringUtil.join(getStudentStatusList().getData(), StudentStatus.P_TITLE, ", "));
        }
        if (getArchival().isActive())
        {
            parameters.add("Архивный" + separator + getArchival().getData().getTitle());
        }
        if (getDevelopFormList().isActive())
        {
            parameters.add("Форма освоения" + separator + CommonBaseStringUtil.join(getDevelopFormList().getData(), DevelopForm.P_TITLE, ", "));
        }
        if (getEntranceYear().isActive())
        {
            parameters.add("Год приема" + separator + getEntranceYear().getData().toString());
        }
        if (getAge().isActive())
        {
            parameters.add("Возраст" + separator + getAge().getData().toString());
        }
        if (getSex().isActive())
        {
            parameters.add("Пол" + separator + getSex().getData().getTitle());
        }
    }

    private boolean getBooleanFromWrapper(IReportParam<DataWrapper> o)
    {
        return TwinComboDataSourceHandler.getSelectedValueNotNull(o.getData());
    }

    //GETTERS
    public IReportParam<Collection<Course>> getCourseList()
    {
        return _courseList;
    }

    public IReportParam<Collection<Group>> getGroupList()
    {
        return _groupList;
    }

    public IReportParam<StudentCategory> getStudentCategory()
    {
        return _studentCategory;
    }

    public IReportParam<Collection<StudentStatus>> getStudentStatusList()
    {
        return _studentStatusList;
    }

    public IReportParam<DataWrapper> getArchival()
    {
        return _archival;
    }

    public IReportParam<Collection<DevelopForm>> getDevelopFormList()
    {
        return _developFormList;
    }

    public IReportParam<Long> getEntranceYear()
    {
        return _entranceYear;
    }

    public IReportParam<Long> getAge()
    {
        return _age;
    }

    public IReportParam<Sex> getSex()
    {
        return _sex;
    }
}
