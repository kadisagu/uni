/**
 *$Id$
 */
package ru.tandemservice.uniusma.sec;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionModifier;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraph;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 13.10.2013
 */
public class UsmaStateChangePermissionModifier extends UnieppStateChangePermissionModifier
{
    private static final Map<Class, CoreCollectionUtils.Pair<String, String>> CLASS_PERMISSION_MAP =
            ImmutableMap.<Class, CoreCollectionUtils.Pair<String, String>>of(
                    UsmaWorkGraph.class, new CoreCollectionUtils.Pair<>("usmaWorkGraph", "Объект «График учебного процесса (УГМА)»"));


    @Override
    protected String getModuleName()
    {
        return "uniusma";
    }

    @Override
    protected String getConfigName()
    {
        return "uniusma-stateChange-config";
    }

    @Override
    protected Map<Class, CoreCollectionUtils.Pair<String, String>> getClassPermissionMap()
    {
        return CLASS_PERMISSION_MAP;
    }
}