/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaImtsaXmlTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.09.2013
 */
@Input(
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id", required = true)
)
public class Model
{
    private Long _id;
    private List<Block> _blocks;
    private Block _currentBlock;
    private String _currentLine;

    public Long getId(){ return _id; }
    public void setId(Long id){ _id = id; }

    public List<Block> getBlocks(){ return _blocks; }
    public void setBlocks(List<Block> blocks){ _blocks = blocks; }

    public Block getCurrentBlock(){ return _currentBlock; }
    public void setCurrentBlock(Block currentBlock){ _currentBlock = currentBlock; }

    public String getCurrentLine(){ return _currentLine; }
    public void setCurrentLine(String currentLine){ _currentLine = currentLine; }


    public static class Block
    {
        private String _directionName;
        private String _fileName;
        private String _xml;

        public String getDirectionName(){ return _directionName; }
        public String getFileName(){ return _fileName; }

        public String getXml(){ return _xml; }
        public void setXml(String xml){ _xml = xml; }

        public Block(String directionName, String fileName, String xml)
        {
            _directionName = directionName;
            _fileName = fileName;
            _xml = xml;
        }
    }
}