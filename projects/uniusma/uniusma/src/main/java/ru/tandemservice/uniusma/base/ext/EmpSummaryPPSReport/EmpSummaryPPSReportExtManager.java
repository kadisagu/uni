/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.EmpSummaryPPSReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.IEmpSummaryPPSReportDao;
import ru.tandemservice.uniusma.base.ext.EmpSummaryPPSReport.logic.UsmaEmpSummaryPPSReportDao;

/**
 * @author Alexander Shaburov
 * @since 21.11.12
 */
@Configuration
public class EmpSummaryPPSReportExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEmpSummaryPPSReportDao empSummaryPPSReportDao()
    {
        return new UsmaEmpSummaryPPSReportDao();
    }
}
