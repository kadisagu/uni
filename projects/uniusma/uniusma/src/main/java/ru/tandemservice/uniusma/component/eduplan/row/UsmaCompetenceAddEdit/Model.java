/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.row.UsmaCompetenceAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaPracticeDispersion;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
@Input(
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "row.id")
)
public class Model
{
    private EppEpvRegistryRow _row = new EppEpvRegistryRow();
    private Long _id;
    private ISelectModel _competenceModel;
    private List<UsmaCompetence> _selectedValues = new ArrayList<>();
    private Set<UsmaCompetence> _values = new TreeSet<>(UsmaCompetence.USAM_COMPETENCE_CODE_TITLE_COMPARATOR);
    private DynamicListDataSource<UsmaCompetence> _dataSource;
    private boolean _practice;
    private UsmaPracticeDispersion _dispersion; /*только для практик*/


    public EppEpvRegistryRow getRow()
    {
        return _row;
    }

    public void setRow(EppEpvRegistryRow row)
    {
        _row = row;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public ISelectModel getCompetenceModel()
    {
        return _competenceModel;
    }

    public void setCompetenceModel(ISelectModel competenceModel)
    {
        _competenceModel = competenceModel;
    }

    public List<UsmaCompetence> getSelectedValues()
    {
        return _selectedValues;
    }

    public void setSelectedValues(List<UsmaCompetence> selectedValues)
    {
        _selectedValues = selectedValues;
    }

    public Set<UsmaCompetence> getValues()
    {
        return _values;
    }

    public void setValues(Set<UsmaCompetence> values)
    {
        _values = values;
    }

    public DynamicListDataSource<UsmaCompetence> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UsmaCompetence> dataSource)
    {
        _dataSource = dataSource;
    }

    public boolean isPractice(){ return _practice; }
    public void setPractice(boolean practice){ _practice = practice; }

    public UsmaPracticeDispersion getDispersion(){ return _dispersion; }
    public void setDispersion(UsmaPracticeDispersion dispersion){ _dispersion = dispersion; }

    public boolean isButton()
    {
        return _row.getRegistryElement() != null;
    }
}