package ru.tandemservice.uniusma.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип разбиения учебного графика (УГМА)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaSchedulePartitionTypeGen extends EntityBase
 implements INaturalIdentifiable<UsmaSchedulePartitionTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType";
    public static final String ENTITY_NAME = "usmaSchedulePartitionType";
    public static final int VERSION_HASH = 610267486;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_PARTS_NUMBER = "partsNumber";

    private String _code;     // Системный код
    private String _title;     // Название
    private int _partsNumber;     // Количество элементов разбиения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Количество элементов разбиения. Свойство не может быть null.
     */
    @NotNull
    public int getPartsNumber()
    {
        return _partsNumber;
    }

    /**
     * @param partsNumber Количество элементов разбиения. Свойство не может быть null.
     */
    public void setPartsNumber(int partsNumber)
    {
        dirty(_partsNumber, partsNumber);
        _partsNumber = partsNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsmaSchedulePartitionTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((UsmaSchedulePartitionType)another).getCode());
            }
            setTitle(((UsmaSchedulePartitionType)another).getTitle());
            setPartsNumber(((UsmaSchedulePartitionType)another).getPartsNumber());
        }
    }

    public INaturalId<UsmaSchedulePartitionTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<UsmaSchedulePartitionTypeGen>
    {
        private static final String PROXY_NAME = "UsmaSchedulePartitionTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsmaSchedulePartitionTypeGen.NaturalId) ) return false;

            UsmaSchedulePartitionTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaSchedulePartitionTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaSchedulePartitionType.class;
        }

        public T newInstance()
        {
            return (T) new UsmaSchedulePartitionType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "partsNumber":
                    return obj.getPartsNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "partsNumber":
                    obj.setPartsNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "partsNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "partsNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "partsNumber":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaSchedulePartitionType> _dslPath = new Path<UsmaSchedulePartitionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaSchedulePartitionType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Количество элементов разбиения. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType#getPartsNumber()
     */
    public static PropertyPath<Integer> partsNumber()
    {
        return _dslPath.partsNumber();
    }

    public static class Path<E extends UsmaSchedulePartitionType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _partsNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(UsmaSchedulePartitionTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UsmaSchedulePartitionTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Количество элементов разбиения. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.catalog.UsmaSchedulePartitionType#getPartsNumber()
     */
        public PropertyPath<Integer> partsNumber()
        {
            if(_partsNumber == null )
                _partsNumber = new PropertyPath<Integer>(UsmaSchedulePartitionTypeGen.P_PARTS_NUMBER, this);
            return _partsNumber;
        }

        public Class getEntityClass()
        {
            return UsmaSchedulePartitionType.class;
        }

        public String getEntityName()
        {
            return "usmaSchedulePartitionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
