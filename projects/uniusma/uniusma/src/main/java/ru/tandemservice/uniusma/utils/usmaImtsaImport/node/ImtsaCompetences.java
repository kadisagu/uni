/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.RegularExpressionValidator;
import ru.tandemservice.uniusma.utils.usmaImtsaImport.validator.Validator;

import java.util.*;

/**
 * Компетенции ИМЦА.
 * @author Alexander Zhebko
 * @since 26.07.2013
 */
public class ImtsaCompetences
{
    // представления
    public static final IAttributeView ATTRIBUTE_VIEW = new CompetenceAttributeView();
    public static final INodeView NODE_VIEW = new CompetenceNodeView();


    // загружаемые данные
    private Map<String, String> _competencesIndex2ContentMap;
    private Map<String, String> _competenceCode2IndexMap;
    private Map<String, PairKey<String, Integer>> _competenceIndex2GroupCodeAndNumberMap;
    private Set<String> _undistributedCompetenceIndexes;
    private Map<String, Integer> _compGroupSize;

    public Map<String, String> getCompetencesIndex2ContentMap(){ return _competencesIndex2ContentMap; }
    public void setCompetencesIndex2ContentMap(Map<String, String> competencesIndex2ContentMap){ _competencesIndex2ContentMap = competencesIndex2ContentMap; }

    public Map<String, String> getCompetenceCode2IndexMap(){ return _competenceCode2IndexMap; }
    public void setCompetenceCode2IndexMap(Map<String, String> competenceCode2IndexMap){ _competenceCode2IndexMap = competenceCode2IndexMap; }

    public Map<String, PairKey<String, Integer>> getCompetenceIndex2GroupCodeAndNumberMap(){ return _competenceIndex2GroupCodeAndNumberMap; }
    public void setCompetenceIndex2GroupCodeAndNumberMap(Map<String, PairKey<String, Integer>> competenceIndex2GroupCodeAndNumberMap){ _competenceIndex2GroupCodeAndNumberMap = competenceIndex2GroupCodeAndNumberMap; }

    public Set<String> getUndistributedCompetenceIndexes(){ return _undistributedCompetenceIndexes; }
    public void setUndistributedCompetenceIndexes(Set<String> undistributedCompetenceIndexes){ _undistributedCompetenceIndexes = undistributedCompetenceIndexes; }

    public Map<String, Integer> getCompGroupSize(){ return _compGroupSize; }
    public void setCompGroupSize(Map<String, Integer> compGroupSize){ _compGroupSize = compGroupSize; }


    // вспомогательные данные
    public static String getCompetencesCodeAttribute(){ return CompetenceAttributeView.CODE; }
    public static String getCompetenceIndexAttribute(){ return CompetenceAttributeView.INDEX; }
    public static String getCompetencesContentAttribute(){ return CompetenceAttributeView.CONTENT; }
    public static String getCompetenceIndexRegExp(){ return CompetenceAttributeView.COMPETENCE_INDEX_REG_EXP; }


    /**
     * Представление атрибутов нода "Строка" компетенций.
     */
    private static class CompetenceAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // шаблон-регулярное выражение для индекса компетенции
        private static final String COMPETENCE_INDEX_REG_EXP = "^([А-Я]+)-([1-9][0-9]*)$";


        // 1. атрибуты
        private static final String CODE = "Код";
        private static final String INDEX = "Индекс";
        private static final String CONTENT = "Содержание";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                CODE,
                INDEX,
                CONTENT
        );


        // 4. отображение названия атрибута на список его вадидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(INDEX, Arrays.<Validator<?>>asList(new RegularExpressionValidator(COMPETENCE_INDEX_REG_EXP)));
            ATTRIBUTE_VALIDATOR_MAP.put(CONTENT, Arrays.<Validator<?>>asList(Validator.UNI_MAX_LENGTH_VALIDATOR));
        }


        // 5. список атрибутов, которые необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Arrays.asList(
                INDEX
        );


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                CODE,
                INDEX,
                CONTENT
        );
    }


    /**
     * Представление нода "Строка" компетениций.
     */
    private static class CompetenceNodeView implements INodeView
    {
        @Override
        public String getNodeName()
        {
            return ROW_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return CompetenceAttributeView.UNIQUE_ATTRIBUTES;
        }


        // ноды
        private static final String ROW_NODE_NAME = "Строка";
     }
}