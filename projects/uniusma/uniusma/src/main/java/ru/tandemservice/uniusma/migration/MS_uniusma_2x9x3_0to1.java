/* $Id$ */
package ru.tandemservice.uniusma.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_2to3;

/**
 * @author Nikolay Fedorovskih
 * @since 02.03.2016
 */
public class MS_uniusma_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_2to3.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        // Фиксим НПм на "Лечебное дело" и "Стоматология ортопедическая" для СПО ОКСО -
        // надо им направление проф. образования указать из нового перечня, иначе они будут считаться укрупненными группами
        int a = tool.executeUpdate("update educationlevels_t set eduprogramsubject_id=? where id=? and eduprogramsubject_id is null", 1464890425706101174L, 1319782421183429009L); // "Лечебное дело"
        int b = tool.executeUpdate("update educationlevels_t set eduprogramsubject_id=? where id=? and eduprogramsubject_id is null", 1464890425801521590L, 1319782421187623313L); // "Стоматология ортопедическая"
        tool.debug("fixed spo education levels: " + a + " + " + b);
    }
}