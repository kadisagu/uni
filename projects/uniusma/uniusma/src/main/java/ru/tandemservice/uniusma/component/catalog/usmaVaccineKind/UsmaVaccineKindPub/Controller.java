/*$Id$*/
package ru.tandemservice.uniusma.component.catalog.usmaVaccineKind.UsmaVaccineKindPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author DMITRY KNYAZEV
 * @since 21.06.2014
 */
public class Controller extends DefaultCatalogPubController<UsmaVaccineKind, Model, IDAO>
{
	@Override
	protected DynamicListDataSource<UsmaVaccineKind> createListDataSource(IBusinessComponent context)
	{
		Model model = getModel(context);

		DynamicListDataSource<UsmaVaccineKind> dataSource = new DynamicListDataSource<>(context, this);

		dataSource.addColumn(new SimpleColumn("Наименование", UsmaVaccineKind.P_TITLE).setOrderable(false).setClickable(false));
		dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
		dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", UsmaVaccineKind.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));

		return dataSource;
	}
}
