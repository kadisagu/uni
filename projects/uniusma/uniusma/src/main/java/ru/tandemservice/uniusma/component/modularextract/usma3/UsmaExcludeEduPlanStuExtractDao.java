/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

import java.util.Map;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public class UsmaExcludeEduPlanStuExtractDao extends UniBaseDao implements IExtractComponentDao<UsmaExcludeEduPlanStuExtract>
{
    @Override
    public void doCommit(UsmaExcludeEduPlanStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);

        // сохраняем старый статус студента
        extract.setStudentStatusOld(student.getStatus());
        // устанавливаем новый статус студенту
        student.setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
        // сохраняем печатную форму выписки
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getExcludeOrderDate());
            extract.setPrevOrderNumber(orderData.getExcludeOrderNumber());
        }
        orderData.setExcludeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setExcludeOrderNumber(extract.getParagraph().getOrder().getNumber());
        saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(UsmaExcludeEduPlanStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        // возвращаем статус студенту
        student.setStatus(extract.getStudentStatusOld());
        // возвращаем предыдущие номер и дату приказа
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setExcludeOrderDate(extract.getPrevOrderDate());
        orderData.setExcludeOrderNumber(extract.getPrevOrderNumber());
        saveOrUpdate(orderData);
    }

}
