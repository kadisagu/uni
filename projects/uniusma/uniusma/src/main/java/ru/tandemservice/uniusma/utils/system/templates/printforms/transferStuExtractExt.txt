{orderType}\par\par
\keep\keepn\fi709\qj 
{asReAttestationResult}\par\fi0\par
приказываю:\par
\fi709\qj
1. Перевести {fio} {birthDate} года рождения, {student_A} {courseOld} курса {orgUnitOld_G} (группа {groupOld}) {developFormOld_GF} формы обучения {compensationTypeStrOld_G_Alt} {shortCourse}на {courseNew} курс {orgUnitNew_G} {developFormNew_GF} формы обучения {compensationTypeStrNew_A_Alt} и определить в группу {groupNew}.\par
{stopGrantPayingStr}\par
{debts}\par\fi0\par
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx376\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6330\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8031\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10299\pard\intbl\qc\'b9\cell\'c4\'e8\'f1\'f6\'e8\'ef\'eb\'e8\'ed\'e0\cell\'ca\'ee\'eb\'e8\'f7\'e5\'f1\'f2\'e2\'ee \'f7\'e0\'f1\'ee\'e2\cell\'c2\'e8\'e4 \'ee\'f2\'f7\'e5\'f2\'ed\'ee\'f1\'f2\'e8\cell\row\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx376\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6330\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8031\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10299\pard\intbl\qj {T}\cell\cell\pard\intbl\qc\cell\cell\row\pard\qj\par
\fi709\qj Основание: {listBasics}\par\fi0