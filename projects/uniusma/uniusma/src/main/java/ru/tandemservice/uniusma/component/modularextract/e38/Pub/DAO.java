/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e38.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.WeekendChildOutStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.WeekendChildOutStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 26.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e38.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e38.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        WeekendChildOutStuExtractUsmaExt extractExt = getByNaturalId(new WeekendChildOutStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        m.setExtUsmaExt(extractExt);
    }
}
