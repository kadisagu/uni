/**
 *$Id$
 */
package ru.tandemservice.uniusma.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniusma.entity.UsmaCompetence2EduPlanVersionBlockRel;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 28.02.2013
 */
public class EppEduPlanVersionBlockListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppEduPlanVersionBlock.class, this);
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "a").column("a.id").createStatement(event.getContext()).list();
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        BatchUtils.execute(params, 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                List<EppEduPlanVersionBlock> blocks = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, "block")
                        .where(in(property(EppEduPlanVersionBlock.id().fromAlias("block")), elements))
                        .createStatement(session)
                        .list();

                for (EppEduPlanVersionBlock block : blocks)
                {
                    if (!block.isRootBlock())
                    {
                        EppEduPlanVersionBlock rootBlock = new DQLSelectBuilder()
                                .fromEntity(EppEduPlanVersionRootBlock.class, "block")
                                .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("block")), value(block.getEduPlanVersion())))
                                .createStatement(session)
                                .uniqueResult();

                        DQLSelectBuilder rootBlockCompetenceBuilder = new DQLSelectBuilder()
                                .fromEntity(UsmaCompetence2EduPlanVersionBlockRel.class, "a")
                                .where(eq(property(UsmaCompetence2EduPlanVersionBlockRel.block().id().fromAlias("a")), value(rootBlock.getId())));

                        for (UsmaCompetence2EduPlanVersionBlockRel rel: rootBlockCompetenceBuilder.createStatement(session).<UsmaCompetence2EduPlanVersionBlockRel>list())
                        {
                            session.save(new UsmaCompetence2EduPlanVersionBlockRel(block, rel.getUsmaCompetence(), rel.getNumber()));
                        }
                    }
                }

                session.flush();
            }
        });

        return super.beforeCompletion(session, params);
    }
}