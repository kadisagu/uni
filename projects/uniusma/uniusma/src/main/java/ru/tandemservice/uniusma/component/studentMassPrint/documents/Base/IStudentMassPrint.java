/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.Base;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.component.studentMassPrint.MassPrint.WrapStudentDocument;
import ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.IModel;

import java.util.List;

/**
 * Интерфейс массовой печати документов по студентам
 *
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public interface IStudentMassPrint<M extends IModel> {
    void doMassPrint(List<Student> students, WrapStudentDocument doc);

    void initModel(M model);

    void initModel4Student(Student student, int documentNumber, M model);

    byte[] getDocument(List<Student> students, M model, int docIndex, Long studentDocumentTypeId);
}
