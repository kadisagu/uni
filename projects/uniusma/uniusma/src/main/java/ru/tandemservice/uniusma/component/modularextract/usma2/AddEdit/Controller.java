/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public class Controller extends CommonModularStudentExtractAddEditController<UsmaExcludeDebtStuExtract, IDAO, Model>
{
}
