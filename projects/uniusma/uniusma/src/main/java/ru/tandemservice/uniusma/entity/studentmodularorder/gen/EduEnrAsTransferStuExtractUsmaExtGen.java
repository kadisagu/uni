package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О зачислении на второй и последующие курсы в порядке перевода
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduEnrAsTransferStuExtractUsmaExtGen extends EntityBase
 implements INaturalIdentifiable<EduEnrAsTransferStuExtractUsmaExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt";
    public static final String ENTITY_NAME = "eduEnrAsTransferStuExtractUsmaExt";
    public static final int VERSION_HASH = 1236989404;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_PROTOCOL_DATE = "protocolDate";

    private EduEnrAsTransferStuExtract _extractExt;     // Выписка из сборного приказа по студенту. О зачислении в порядке перевода
    private String _protocolNumber;     // Номер протокола
    private Date _protocolDate;     // Дата протокола

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О зачислении в порядке перевода. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EduEnrAsTransferStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О зачислении в порядке перевода. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(EduEnrAsTransferStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола. Свойство не может быть null.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Дата протокола. Свойство не может быть null.
     */
    @NotNull
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола. Свойство не может быть null.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduEnrAsTransferStuExtractUsmaExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((EduEnrAsTransferStuExtractUsmaExt)another).getExtractExt());
            }
            setProtocolNumber(((EduEnrAsTransferStuExtractUsmaExt)another).getProtocolNumber());
            setProtocolDate(((EduEnrAsTransferStuExtractUsmaExt)another).getProtocolDate());
        }
    }

    public INaturalId<EduEnrAsTransferStuExtractUsmaExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<EduEnrAsTransferStuExtractUsmaExtGen>
    {
        private static final String PROXY_NAME = "EduEnrAsTransferStuExtractUsmaExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(EduEnrAsTransferStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduEnrAsTransferStuExtractUsmaExtGen.NaturalId) ) return false;

            EduEnrAsTransferStuExtractUsmaExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduEnrAsTransferStuExtractUsmaExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduEnrAsTransferStuExtractUsmaExt.class;
        }

        public T newInstance()
        {
            return (T) new EduEnrAsTransferStuExtractUsmaExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "protocolDate":
                    return obj.getProtocolDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((EduEnrAsTransferStuExtract) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "protocolNumber":
                        return true;
                case "protocolDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "protocolNumber":
                    return true;
                case "protocolDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return EduEnrAsTransferStuExtract.class;
                case "protocolNumber":
                    return String.class;
                case "protocolDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduEnrAsTransferStuExtractUsmaExt> _dslPath = new Path<EduEnrAsTransferStuExtractUsmaExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduEnrAsTransferStuExtractUsmaExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О зачислении в порядке перевода. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt#getExtractExt()
     */
    public static EduEnrAsTransferStuExtract.Path<EduEnrAsTransferStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    public static class Path<E extends EduEnrAsTransferStuExtractUsmaExt> extends EntityPath<E>
    {
        private EduEnrAsTransferStuExtract.Path<EduEnrAsTransferStuExtract> _extractExt;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Date> _protocolDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О зачислении в порядке перевода. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt#getExtractExt()
     */
        public EduEnrAsTransferStuExtract.Path<EduEnrAsTransferStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new EduEnrAsTransferStuExtract.Path<EduEnrAsTransferStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(EduEnrAsTransferStuExtractUsmaExtGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(EduEnrAsTransferStuExtractUsmaExtGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

        public Class getEntityClass()
        {
            return EduEnrAsTransferStuExtractUsmaExt.class;
        }

        public String getEntityName()
        {
            return "eduEnrAsTransferStuExtractUsmaExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
