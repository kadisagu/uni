/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaEduPlanVersionBlockQualificationAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusma.entity.UsmaQualification;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getQualificationId() == null)
        {
            UsmaQualification qualification = new UsmaQualification();
            qualification.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
            model.setQualification(qualification);

        } else
        {
            model.setQualification(get(UsmaQualification.class, model.getQualificationId()));
        }
    }
}