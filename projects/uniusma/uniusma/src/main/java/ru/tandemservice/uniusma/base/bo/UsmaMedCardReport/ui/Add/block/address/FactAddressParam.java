/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address;

import org.tandemframework.core.entity.dsl.MetaDSLPath;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author DMITRY KNYAZEV
 * @since 03.02.2015
 */
public class FactAddressParam extends AbstractAddressParam
{

    //DATA SOURCE
    public static final String FACT_COUNTRY_DS = "factCountryDS";
    public static final String FACT_SETTLEMENT_DS = "factSettlementDS";
    public static final String FACT_STREET_DS = "factStreetDS";

    @Override
    protected MetaDSLPath getStudentAddressPath()
    {
        return Student.person().address();
    }

    @Override
    protected String getParameterName()
    {
        return "Фактический адрес";
    }
}
