/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.medCard;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.UsmaMedCardReportPrinter;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.util.IUsmaDQLReportModifier;
import ru.tandemservice.uniusma.entity.catalog.UsmaDiseasesKind;
import ru.tandemservice.uniusma.entity.catalog.UsmaHealthGroup;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;
import ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentFluorographyInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentOtherInfo;
import ru.tandemservice.uniusma.entity.student.UsmaStudentVaccineInfo;

import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 03.02.2015
 */
public class MedCardParam implements IUsmaDQLReportModifier
{
    //DATA SOURCE
    public static final String VACCINE_KIND_DS = "vaccineKindDS";
    public static final String DISEASES_KIND_DS = "diseasesKindDS";
    public static final String HEALTH_GROUP_DS = "healthGroupDS";
    private Date _fromDate = null;
    private Date _toDate = null;

    private final IReportParam<DataWrapper> _needFluorography = new ReportParam<>();
    private final IReportParam<DataWrapper> _needVaccine = new ReportParam<>();
    private final IReportParam<List<UsmaVaccineKind>> _vaccineList = new ReportParam<>();
    private final IReportParam<String> _vaccinationSign = new ReportParam<>();
    private final IReportParam<String> _periodical = new ReportParam<>();
    private final IReportParam<DataWrapper> _needDiseases = new ReportParam<>();
    private final IReportParam<Collection<UsmaDiseasesKind>> _diseasesList = new ReportParam<>();
    private final IReportParam<DataWrapper> _needHealthGroup = new ReportParam<>();
    private final IReportParam<List<UsmaHealthGroup>> _healthGroupList = new ReportParam<>();
    private final IReportParam<DataWrapper> _needDispensary = new ReportParam<>();

    @Override
    public void modify(String alias, DQLSelectBuilder dql)
    {
        // фильтруем  флюрографию
        if (getNeedFluorography().isActive())
        {
            String flgInfoAlias = "flg";
            DQLSelectBuilder flgBuilder = new DQLSelectBuilder().fromEntity(UsmaStudentFluorographyInfo.class, flgInfoAlias)
                    .where(eq(property(flgInfoAlias, UsmaStudentFluorographyInfo.L_STUDENT), property(alias, Student.P_ID)))
                    .where(betweenDays(UsmaStudentVaccineInfo.date().fromAlias(flgInfoAlias), getFromDate(), getToDate()));
            // по наличию
            if (getBooleanFromWrapper(getNeedFluorography()))
                dql.where(exists(flgBuilder.buildQuery()));
            else
                dql.where(notExists(flgBuilder.buildQuery()));
        }

        // фильтруем  результаты вакцинации
        if (getNeedVaccine().isActive())
        {
            String vacAlias = "vac";
            DQLSelectBuilder vacBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaStudentVaccineInfo.class, vacAlias)
                    .where(eq(property(vacAlias, UsmaStudentVaccineInfo.L_STUDENT), property(alias, Student.P_ID)))
                    .where(betweenDays(UsmaStudentVaccineInfo.date().fromAlias(vacAlias), getFromDate(), getToDate()));
            // по типам
            if (getVaccineList().isActive())
                vacBuilder.where(in(property(vacAlias, UsmaStudentVaccineInfo.L_VACCINE_KIND), getVaccineList().getData()));

            //по признаку вакцинации
            if (getVaccinationSign().isActive())
                vacBuilder.where(likeUpper(property(vacAlias, UsmaStudentVaccineInfo.L_VACCINATION_SIGN), value(CoreStringUtils.escapeLike(getVaccinationSign().getData()))));

            //по периодичности
            if (getPeriodical().isActive())
                vacBuilder.where(likeUpper(property(vacAlias, UsmaStudentVaccineInfo.P_PERIODICAL), value(CoreStringUtils.escapeLike(getPeriodical().getData()))));

            // по наличию
            if (getBooleanFromWrapper(getNeedVaccine()))
                dql.where(exists(vacBuilder.buildQuery()));
            else
                dql.where(notExists(vacBuilder.buildQuery()));
        }

        // фильтруем  заболевания
        if (getNeedDiseases().isActive())
        {
            String desAlias = "des";
            DQLSelectBuilder desBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaStudentDiseaseInfo.class, desAlias)
                    .where(eq(property(desAlias, UsmaStudentDiseaseInfo.L_STUDENT), property(alias, Student.P_ID)));

            //проверка по дате
            if (getFromDate() != null && getToDate() != null)
            {
                Date dateFrom = alignDate(getFromDate());
                Date dateTo = alignDate(getToDate());
                desBuilder.where(or(compareDiseaseDate(desAlias, dateFrom), compareDiseaseDate(desAlias, dateTo)));
            }
            else if (getFromDate() != null)
            {
                Date date = alignDate(getFromDate());
                desBuilder.where(compareDiseaseDate(desAlias, date));
            }
            else if (getToDate() != null)
            {
                Date date = alignDate(getFromDate());
                desBuilder.where(compareDiseaseDate(desAlias, date));
            }

            // по типам
            if (getVaccineList().isActive())
                desBuilder.where(in(property(desAlias, UsmaStudentDiseaseInfo.L_DISEASES_KIND), getDiseasesList().getData()));

            // по наличию
            if (getBooleanFromWrapper(getNeedDiseases()))
                dql.where(exists(desBuilder.buildQuery()));
            else
                dql.where(notExists(desBuilder.buildQuery()));
        }

        // фильтруем  группы здоровья
        if (getNeedHealthGroup().isActive())
        {
            String healthAlias = "health";
            DQLSelectBuilder healthBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaStudentOtherInfo.class, healthAlias)
                    .where(eq(property(healthAlias, UsmaStudentOtherInfo.L_STUDENT), property(alias, Student.P_ID)));
            //по типам
            if (getHealthGroupList().isActive())
                healthBuilder.where(in(property(healthAlias, UsmaStudentOtherInfo.L_HEALTH_GROUP), getHealthGroupList().getData()));
            else
                healthBuilder.where(isNotNull(property(healthAlias, UsmaStudentOtherInfo.L_HEALTH_GROUP)));
            // по наличию
            if (getBooleanFromWrapper(getNeedHealthGroup()))
                dql.where(exists(healthBuilder.buildQuery()));
            else
                dql.where(notExists(healthBuilder.buildQuery()));
        }

        // фильтруем деипансерный учет.
        if (getNeedDispensary().isActive())
        {
            String dispAlias = "disp";
            DQLSelectBuilder dispBuilder = new DQLSelectBuilder()
                    .fromEntity(UsmaStudentOtherInfo.class, dispAlias)
                    .where(eq(property(dispAlias, UsmaStudentOtherInfo.L_STUDENT), property(alias, Student.P_ID)))
                    .where(eq(property(dispAlias, UsmaStudentOtherInfo.P_DISPENSARY), value(Boolean.TRUE)));
            // Считаем что все студенты у кого нет признака "да" или они отсутствуют в таблице "UsmaStudentOtherInfo" не имеют диспансерного учета
            if (getBooleanFromWrapper(getNeedDispensary()))
                dql.where(exists(dispBuilder.buildQuery()));
            else
                dql.where(notExists(dispBuilder.buildQuery()));
        }
    }

    @Override
    public void addParameters(List<String> parameters)
    {
        final String separator = UsmaMedCardReportPrinter.CSV_SEPARATOR;

        if (getFromDate() != null)
            parameters.add("Дата с" + separator + DateFormatter.DEFAULT_DATE_FORMATTER.format(getFromDate()));

        if (getToDate() != null)
            parameters.add("Дата по" + separator + DateFormatter.DEFAULT_DATE_FORMATTER.format(getToDate()));

        if (getNeedFluorography().isActive())
            parameters.add("Есть результаты флюорографии" + separator + getNeedFluorography().getData().getTitle());

        if (getNeedVaccine().isActive())
            parameters.add("Есть результаты вакцинации" + separator + getNeedVaccine().getData().getTitle());

        if (getVaccineList().isActive())
            parameters.add("Сведения о вакцинации" + separator + CommonBaseStringUtil.join(getVaccineList().getData(), UsmaVaccineKind.P_TITLE, ", "));

        if (getVaccinationSign().isActive())
            parameters.add("Признак вакцинации/ревакцинации" + separator + getVaccinationSign().getData());

        if (getPeriodical().isActive())
            parameters.add("Периодичность" + separator + getPeriodical().getData());

        if (getNeedDiseases().isActive())
            parameters.add("Есть заболевания" + separator + getNeedDiseases().getData().getTitle());

        if (getDiseasesList().isActive())
            parameters.add("Виды заболеваний" + separator + CommonBaseStringUtil.join(getDiseasesList().getData(), UsmaDiseasesKind.P_TITLE, ", "));

        if (getNeedHealthGroup().isActive())
            parameters.add("Есть сведения о группе здоровья" + separator + getNeedHealthGroup().getData().getTitle());

        if (getHealthGroupList().isActive())
            parameters.add("Группы здоровья" + separator + CommonBaseStringUtil.join(getHealthGroupList().getData(), UsmaHealthGroup.P_TITLE, ", "));

        if (getNeedDispensary().isActive())
            parameters.add("Диспансерный учет" + separator + getNeedDispensary().getData().getTitle());
    }

    private boolean getBooleanFromWrapper(IReportParam<DataWrapper> o)
    {
        return TwinComboDataSourceHandler.getSelectedValueNotNull(o.getData());
    }

    private IDQLExpression compareDiseaseDate(String desAlias, Date date)
    {
        return and(le(property(UsmaStudentDiseaseInfo.beginDate().fromAlias(desAlias)), value(date, PropertyType.DATE)),
                   ge(property(UsmaStudentDiseaseInfo.endDate().fromAlias(desAlias)), value(date, PropertyType.DATE)));
    }

    private Date alignDate(@NotNull Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    //GETTERS
    public Date getFromDate()
    {
        return _fromDate;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setFromDate(Date fromDate)
    {
        _fromDate = fromDate;
    }

    public Date getToDate()
    {
        return _toDate;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setToDate(Date toDate)
    {
        _toDate = toDate;
    }

    public IReportParam<DataWrapper> getNeedFluorography()
    {
        return _needFluorography;
    }

    public IReportParam<DataWrapper> getNeedVaccine()
    {
        return _needVaccine;
    }

    public IReportParam<List<UsmaVaccineKind>> getVaccineList()
    {
        return _vaccineList;
    }

    public IReportParam<String> getVaccinationSign()
    {
        return _vaccinationSign;
    }

    public IReportParam<String> getPeriodical()
    {
        return _periodical;
    }

    public IReportParam<DataWrapper> getNeedDiseases()
    {
        return _needDiseases;
    }

    public IReportParam<Collection<UsmaDiseasesKind>> getDiseasesList()
    {
        return _diseasesList;
    }

    public IReportParam<DataWrapper> getNeedHealthGroup()
    {
        return _needHealthGroup;
    }

    public IReportParam<List<UsmaHealthGroup>> getHealthGroupList()
    {
        return _healthGroupList;
    }

    public IReportParam<DataWrapper> getNeedDispensary()
    {
        return _needDispensary;
    }
}
