/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma3.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;

/**
 * @author Denis Perminov
 * @since 08.05.2014
 */
public class Controller extends CommonModularStudentExtractAddEditController<UsmaExcludeEduPlanStuExtract, IDAO, Model>
{
}
