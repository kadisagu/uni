/* $Id$ */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ScheduleCopy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 23.10.2014
 */
@Configuration
public class UsmaEduPlanScheduleCopy extends BusinessComponentManager
{
    public static final String BIND_VERSION = "version";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(selectDS("eduPlanVersionDS", eduPlanVersionDSHandler())
                .addColumn(EppEduPlanVersion.title().s())
                .addColumn(EppEduPlanVersion.state().title().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduPlanVersionDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppEduPlanVersion.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                Long version = context.get(BIND_VERSION);
                dql.where(ne(property(alias), value(version)));

                dql
                    .fromEntity(EppEduPlanVersion.class, "v")
                    .where(eq(property("v", EppEduPlanVersion.eduPlan().developGrid()), property(alias, EppEduPlanVersion.eduPlan().developGrid())))
                    .where(eq(property("v"), value(version)));
            }
        }
            .filter(EppEduPlanVersion.eduPlan().number())
            .filter(EppEduPlanVersion.number())
            .filter(EppEduPlanVersion.titlePostfix())
            .filter(EppEduPlanVersion.state().title())
            .order(EppEduPlanVersion.eduPlan().number())
            .order(EppEduPlanVersion.number())
            .order(EppEduPlanVersion.titlePostfix())
            .order(EppEduPlanVersion.state().title())
            .pageable(true);
    }
}