/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 30.12.2015
 */
public interface IStudentCardAttachmentReportDao extends ICommonDAO, INeedPersistenceSupport
{
    AbstractStudentOrder getStudentTransferOrder(Student student, int courseOld, int courseNew);

    List<StudentWpeActionContainer> getStudentWpeActions(@NotNull Student student);

    List<StudentWpeActionContainer> getStudentWpeActions(
            final @NotNull OrgUnit orgUnit,
            final @NotNull EppYearEducationProcess eduYear,
            final Collection<YearDistributionPart> yearPartList,
            final Collection<Course> courseList,
            final Collection<Group> groupList

    );
}
