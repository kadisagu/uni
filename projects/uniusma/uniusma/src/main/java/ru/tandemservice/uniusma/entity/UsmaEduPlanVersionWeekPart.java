package ru.tandemservice.uniusma.entity;

import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniusma.entity.gen.*;

/**
 * Элемент разбиения учебного графика (УГМА)
 */
public class UsmaEduPlanVersionWeekPart extends UsmaEduPlanVersionWeekPartGen
{
    public UsmaEduPlanVersionWeekPart()
    {
    }

    public UsmaEduPlanVersionWeekPart(UsmaEduPlanVersionWeek eduPlanVersionWeek, int partitionElementNumber, EppWeekType weekType)
    {
        this.setEduPlanVersionWeek(eduPlanVersionWeek);
        this.setPartitionElementNumber(partitionElementNumber);
        this.setWeekType(weekType);
    }
}