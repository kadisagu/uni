\qc\fs24О продлении ежегодного отпуска\par\par
\fs28\fi709\qj {reasonPrint} \par\fi0\par
\ql\expnd12\expndtw60ПРИКАЗЫВАЮ:\expndtw0\expnd0\par
\par
\qlПРОДЛИТЬ:\par

\keep\keepn\fi709\qj

\pard\par\fi500\qj{extractNumber}.

 \b{Fio}\b0 – {post_D} {orgUnit_G} ежегодный оплачиваемый отпуск{period} на {amountDaysProlongation} с {prolongationFrom} по {prolongationTo}.

\par
\par
\'ce\'f1\'ed\'ee\'e2\'e0\'ed\'e8\'e5: {listBasics}.
\par
\par
С приказом ознакомлен (на) ______________ « ___ » _________20__ г.
\par