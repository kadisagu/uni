/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.translator;

import org.tandemframework.core.runtime.ApplicationRuntime;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Сборник трансляторов.
 * @author Alexander Zhebko
 * @since 23.07.2013
 */
public class Translators
{
    private static final TranslatorModel model = (TranslatorModel) ApplicationRuntime.getBean("imtsaImportTranslatorModel");
    private static final Map<Class<?>, Translator<?>> TRANSLATOR_MAP = new HashMap<>();
    static
    {
        TRANSLATOR_MAP.put(String.class, new Translator<String>()
        {
            @Override public String translate(String value){ return model.translateToString(value); }
        });

        TRANSLATOR_MAP.put(Integer.class, new Translator<Integer>()
        {
            @Override public Integer translate(String value){ return model.translateToInteger(value); }
        });

        TRANSLATOR_MAP.put(Double.class, new Translator<Double>()
        {
            @Override public Double translate(String value){ return model.translateToDouble(value); }
        });

        TRANSLATOR_MAP.put(Date.class, new Translator<Date>()
        {
            @Override public Date translate(String value){ return model.translateToDate(value); }
        });

        TRANSLATOR_MAP.put(Boolean.class, new Translator<Boolean>()
        {
            @Override public Boolean translate(String value){ return model.translateToBoolean(value); }
        });
    }

    /**
     * Выбирает соответствуеющий данному типу транслятор
     * @param clazz класс, по которому требуется подобрать транслятор
     * @return подобранный транслятор
     */
    public static Translator<?> get(Class<?> clazz)
    {
        return TRANSLATOR_MAP.get(clazz);
    }

    private Translators()
    {

    }
}