package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении не приступившего к занятиям после академического отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaExcludeAfterAcadWeekendStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract";
    public static final String ENTITY_NAME = "usmaExcludeAfterAcadWeekendStuExtract";
    public static final int VERSION_HASH = -1902500972;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_PAYING_DATE = "stopPayingDate";
    public static final String P_GIVE_DIPLOMA = "giveDiploma";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_BEGIN_ACAD_WEEKEND_DATE = "beginAcadWeekendDate";
    public static final String P_END_ACAD_WEEKEND_DATE = "endAcadWeekendDate";
    public static final String P_ACAD_WEEKEND_ORDER_NUMBER = "acadWeekendOrderNumber";
    public static final String P_ACAD_WEEKEND_ORDER_DATE = "acadWeekendOrderDate";

    private Date _excludeDate;     // Дата отчисления
    private boolean _stopGrantsPaying;     // Отменить выплату стипендии
    private Date _stopPayingDate;     // Дата прекращения выплаты стипендии
    private boolean _giveDiploma;     // Выдается диплом о незаконченном высшем образовании
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private Date _beginAcadWeekendDate;     // Дата начала академического отпуска
    private Date _endAcadWeekendDate;     // Дата окончания академического отпуска
    private String _acadWeekendOrderNumber;     // Номер приказа о предоставлении академического отпуска
    private Date _acadWeekendOrderDate;     // Дата приказа о предоставлении академического отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Отменить выплату стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     */
    public Date getStopPayingDate()
    {
        return _stopPayingDate;
    }

    /**
     * @param stopPayingDate Дата прекращения выплаты стипендии.
     */
    public void setStopPayingDate(Date stopPayingDate)
    {
        dirty(_stopPayingDate, stopPayingDate);
        _stopPayingDate = stopPayingDate;
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isGiveDiploma()
    {
        return _giveDiploma;
    }

    /**
     * @param giveDiploma Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    public void setGiveDiploma(boolean giveDiploma)
    {
        dirty(_giveDiploma, giveDiploma);
        _giveDiploma = giveDiploma;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Дата начала академического отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginAcadWeekendDate()
    {
        return _beginAcadWeekendDate;
    }

    /**
     * @param beginAcadWeekendDate Дата начала академического отпуска. Свойство не может быть null.
     */
    public void setBeginAcadWeekendDate(Date beginAcadWeekendDate)
    {
        dirty(_beginAcadWeekendDate, beginAcadWeekendDate);
        _beginAcadWeekendDate = beginAcadWeekendDate;
    }

    /**
     * @return Дата окончания академического отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getEndAcadWeekendDate()
    {
        return _endAcadWeekendDate;
    }

    /**
     * @param endAcadWeekendDate Дата окончания академического отпуска. Свойство не может быть null.
     */
    public void setEndAcadWeekendDate(Date endAcadWeekendDate)
    {
        dirty(_endAcadWeekendDate, endAcadWeekendDate);
        _endAcadWeekendDate = endAcadWeekendDate;
    }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAcadWeekendOrderNumber()
    {
        return _acadWeekendOrderNumber;
    }

    /**
     * @param acadWeekendOrderNumber Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    public void setAcadWeekendOrderNumber(String acadWeekendOrderNumber)
    {
        dirty(_acadWeekendOrderNumber, acadWeekendOrderNumber);
        _acadWeekendOrderNumber = acadWeekendOrderNumber;
    }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getAcadWeekendOrderDate()
    {
        return _acadWeekendOrderDate;
    }

    /**
     * @param acadWeekendOrderDate Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    public void setAcadWeekendOrderDate(Date acadWeekendOrderDate)
    {
        dirty(_acadWeekendOrderDate, acadWeekendOrderDate);
        _acadWeekendOrderDate = acadWeekendOrderDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsmaExcludeAfterAcadWeekendStuExtractGen)
        {
            setExcludeDate(((UsmaExcludeAfterAcadWeekendStuExtract)another).getExcludeDate());
            setStopGrantsPaying(((UsmaExcludeAfterAcadWeekendStuExtract)another).isStopGrantsPaying());
            setStopPayingDate(((UsmaExcludeAfterAcadWeekendStuExtract)another).getStopPayingDate());
            setGiveDiploma(((UsmaExcludeAfterAcadWeekendStuExtract)another).isGiveDiploma());
            setStudentStatusOld(((UsmaExcludeAfterAcadWeekendStuExtract)another).getStudentStatusOld());
            setBeginAcadWeekendDate(((UsmaExcludeAfterAcadWeekendStuExtract)another).getBeginAcadWeekendDate());
            setEndAcadWeekendDate(((UsmaExcludeAfterAcadWeekendStuExtract)another).getEndAcadWeekendDate());
            setAcadWeekendOrderNumber(((UsmaExcludeAfterAcadWeekendStuExtract)another).getAcadWeekendOrderNumber());
            setAcadWeekendOrderDate(((UsmaExcludeAfterAcadWeekendStuExtract)another).getAcadWeekendOrderDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaExcludeAfterAcadWeekendStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaExcludeAfterAcadWeekendStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsmaExcludeAfterAcadWeekendStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return obj.getExcludeDate();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopPayingDate":
                    return obj.getStopPayingDate();
                case "giveDiploma":
                    return obj.isGiveDiploma();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "beginAcadWeekendDate":
                    return obj.getBeginAcadWeekendDate();
                case "endAcadWeekendDate":
                    return obj.getEndAcadWeekendDate();
                case "acadWeekendOrderNumber":
                    return obj.getAcadWeekendOrderNumber();
                case "acadWeekendOrderDate":
                    return obj.getAcadWeekendOrderDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopPayingDate":
                    obj.setStopPayingDate((Date) value);
                    return;
                case "giveDiploma":
                    obj.setGiveDiploma((Boolean) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "beginAcadWeekendDate":
                    obj.setBeginAcadWeekendDate((Date) value);
                    return;
                case "endAcadWeekendDate":
                    obj.setEndAcadWeekendDate((Date) value);
                    return;
                case "acadWeekendOrderNumber":
                    obj.setAcadWeekendOrderNumber((String) value);
                    return;
                case "acadWeekendOrderDate":
                    obj.setAcadWeekendOrderDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopPayingDate":
                        return true;
                case "giveDiploma":
                        return true;
                case "studentStatusOld":
                        return true;
                case "beginAcadWeekendDate":
                        return true;
                case "endAcadWeekendDate":
                        return true;
                case "acadWeekendOrderNumber":
                        return true;
                case "acadWeekendOrderDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopPayingDate":
                    return true;
                case "giveDiploma":
                    return true;
                case "studentStatusOld":
                    return true;
                case "beginAcadWeekendDate":
                    return true;
                case "endAcadWeekendDate":
                    return true;
                case "acadWeekendOrderNumber":
                    return true;
                case "acadWeekendOrderDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return Date.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopPayingDate":
                    return Date.class;
                case "giveDiploma":
                    return Boolean.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "beginAcadWeekendDate":
                    return Date.class;
                case "endAcadWeekendDate":
                    return Date.class;
                case "acadWeekendOrderNumber":
                    return String.class;
                case "acadWeekendOrderDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaExcludeAfterAcadWeekendStuExtract> _dslPath = new Path<UsmaExcludeAfterAcadWeekendStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaExcludeAfterAcadWeekendStuExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getStopPayingDate()
     */
    public static PropertyPath<Date> stopPayingDate()
    {
        return _dslPath.stopPayingDate();
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#isGiveDiploma()
     */
    public static PropertyPath<Boolean> giveDiploma()
    {
        return _dslPath.giveDiploma();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Дата начала академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getBeginAcadWeekendDate()
     */
    public static PropertyPath<Date> beginAcadWeekendDate()
    {
        return _dslPath.beginAcadWeekendDate();
    }

    /**
     * @return Дата окончания академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getEndAcadWeekendDate()
     */
    public static PropertyPath<Date> endAcadWeekendDate()
    {
        return _dslPath.endAcadWeekendDate();
    }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getAcadWeekendOrderNumber()
     */
    public static PropertyPath<String> acadWeekendOrderNumber()
    {
        return _dslPath.acadWeekendOrderNumber();
    }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getAcadWeekendOrderDate()
     */
    public static PropertyPath<Date> acadWeekendOrderDate()
    {
        return _dslPath.acadWeekendOrderDate();
    }

    public static class Path<E extends UsmaExcludeAfterAcadWeekendStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopPayingDate;
        private PropertyPath<Boolean> _giveDiploma;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _beginAcadWeekendDate;
        private PropertyPath<Date> _endAcadWeekendDate;
        private PropertyPath<String> _acadWeekendOrderNumber;
        private PropertyPath<Date> _acadWeekendOrderDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getStopPayingDate()
     */
        public PropertyPath<Date> stopPayingDate()
        {
            if(_stopPayingDate == null )
                _stopPayingDate = new PropertyPath<Date>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_STOP_PAYING_DATE, this);
            return _stopPayingDate;
        }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#isGiveDiploma()
     */
        public PropertyPath<Boolean> giveDiploma()
        {
            if(_giveDiploma == null )
                _giveDiploma = new PropertyPath<Boolean>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_GIVE_DIPLOMA, this);
            return _giveDiploma;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Дата начала академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getBeginAcadWeekendDate()
     */
        public PropertyPath<Date> beginAcadWeekendDate()
        {
            if(_beginAcadWeekendDate == null )
                _beginAcadWeekendDate = new PropertyPath<Date>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_BEGIN_ACAD_WEEKEND_DATE, this);
            return _beginAcadWeekendDate;
        }

    /**
     * @return Дата окончания академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getEndAcadWeekendDate()
     */
        public PropertyPath<Date> endAcadWeekendDate()
        {
            if(_endAcadWeekendDate == null )
                _endAcadWeekendDate = new PropertyPath<Date>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_END_ACAD_WEEKEND_DATE, this);
            return _endAcadWeekendDate;
        }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getAcadWeekendOrderNumber()
     */
        public PropertyPath<String> acadWeekendOrderNumber()
        {
            if(_acadWeekendOrderNumber == null )
                _acadWeekendOrderNumber = new PropertyPath<String>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_ACAD_WEEKEND_ORDER_NUMBER, this);
            return _acadWeekendOrderNumber;
        }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract#getAcadWeekendOrderDate()
     */
        public PropertyPath<Date> acadWeekendOrderDate()
        {
            if(_acadWeekendOrderDate == null )
                _acadWeekendOrderDate = new PropertyPath<Date>(UsmaExcludeAfterAcadWeekendStuExtractGen.P_ACAD_WEEKEND_ORDER_DATE, this);
            return _acadWeekendOrderDate;
        }

        public Class getEntityClass()
        {
            return UsmaExcludeAfterAcadWeekendStuExtract.class;
        }

        public String getEntityName()
        {
            return "usmaExcludeAfterAcadWeekendStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
