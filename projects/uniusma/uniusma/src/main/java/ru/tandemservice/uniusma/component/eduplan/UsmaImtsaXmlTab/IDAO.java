/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.eduplan.UsmaImtsaXmlTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 07.09.2013
 */
public interface IDAO extends IUniDao<Model>
{
}