/* $Id$ */
package ru.tandemservice.uniusma.component.catalog.usmaVaccineKind.UsmaVaccineKindAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

/**
 * @author Denis Perminov
 * @since 03.06.2014
 */
public class DAO extends DefaultCatalogAddEditDAO<UsmaVaccineKind, Model> implements IDAO
{
}
