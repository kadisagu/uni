/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic;

import org.apache.commons.collections15.keyvalue.MultiKey;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 30.12.2015
 */
public class StudentCardAttachmentReportDao extends CommonDAO implements IStudentCardAttachmentReportDao
{

    private final String STUDENT_WPE_ALIAS = "wpe";

    @Override
    public AbstractStudentOrder getStudentTransferOrder(Student student, int courseOld, int courseNew)
    {
        String alias = "te";
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CourseTransferStuListExtract.class, alias)
                .column(property(alias, CourseTransferStuListExtract.paragraph().order()))
                .where(eq(property(alias, CourseTransferStuListExtract.entity()), value(student)))
                .where(eq(property(alias, CourseTransferStuListExtract.courseOld().intValue()), value(courseOld)))
                .where(eq(property(alias, CourseTransferStuListExtract.courseNew().intValue()), value(courseNew)))
                .order(property(alias, CourseTransferStuListExtract.paragraph().order().commitDate()), OrderDirection.desc);
        final List<AbstractStudentOrder> order = builder.createStatement(getSession()).list();
        return order.isEmpty() ? null : order.get(0);
    }

    @Override
    public List<StudentWpeActionContainer> getStudentWpeActions(@NotNull Student student)
    {
        final DQLSelectBuilder studentWpeActionBuilder = getStudentWpeActionBuilder(null, null, null);
        //МСРП только для данного студента
        studentWpeActionBuilder.where(eq(property(STUDENT_WPE_ALIAS, EppStudentWorkPlanElement.student()), value(student)));

        //получить значения
        final List<EppStudentWpeCAction> studentWpeActionList = studentWpeActionBuilder.createStatement(getSession()).list();
        return prepare(studentWpeActionList);
    }

    @Override
    public List<StudentWpeActionContainer> getStudentWpeActions(
            final @NotNull OrgUnit orgUnit,
            final @NotNull EppYearEducationProcess eduYear,
            final Collection<YearDistributionPart> yearPartList,
            final Collection<Course> courseList,
            final Collection<Group> groupList

    )
    {
        final DQLSelectBuilder studentWpeActionBuilder = getStudentWpeActionBuilder(eduYear, yearPartList, courseList);

        //филтр по студентам
        final String studentAlias = "s";
        final String educationOrgUnitAlias = "eou";
        final DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, studentAlias)
                .column(property(studentAlias))
                .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(studentAlias), educationOrgUnitAlias)
                .where(eq(property(studentAlias, Student.id()), property(STUDENT_WPE_ALIAS, EppStudentWorkPlanElement.student().id())));

        IDQLExpression expression = FilterUtils.getEducationOrgUnitFilter(educationOrgUnitAlias, orgUnit);
        studentBuilder.where(null == expression ? eq(property(studentAlias, Student.id()), value(-1L)) : expression);
        //фильтр по группе
        if (groupList != null && !groupList.isEmpty())
            studentBuilder.where(in(property(studentAlias, Student.group()), groupList));

        //МСРП только для найденных студентов
        studentWpeActionBuilder.where(exists(studentBuilder.buildQuery()));

        //получить значения
        final List<EppStudentWpeCAction> studentWpeActionList = studentWpeActionBuilder.createStatement(getSession()).list();
        return prepare(studentWpeActionList);
    }

    private DQLSelectBuilder getStudentWpeActionBuilder(
            final EppYearEducationProcess eduYear,
            final Collection<YearDistributionPart> yearPartList,
            final Collection<Course> courseList
    )
    {
        String studentWpeActionAlias = "swca";
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, studentWpeActionAlias)
                .column(studentWpeActionAlias)
                .where(isNull(EppStudentWpeCAction.removalDate().fromAlias(studentWpeActionAlias)))//get only actual WPE
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias(studentWpeActionAlias), STUDENT_WPE_ALIAS);

        //фильтр по учебному году
        if (eduYear != null)
            builder.where(eq(property(STUDENT_WPE_ALIAS, EppStudentWorkPlanElement.year()), value(eduYear)));

        //фильтр по части учебного года
        if (yearPartList != null && !yearPartList.isEmpty())
            builder.where(in(property(STUDENT_WPE_ALIAS, EppStudentWorkPlanElement.part()), yearPartList));

        //фильтр по курсу
        if (courseList != null && !courseList.isEmpty())
            builder.where(in(property(STUDENT_WPE_ALIAS, EppStudentWorkPlanElement.course()), courseList));

        return builder;
    }

    private List<StudentWpeActionContainer> prepare(Collection<EppStudentWpeCAction> studentWpeActionList)
    {
        //studentWpeActionId -> SessionMark
        final Map<Long, SessionMark> studentWpeAction2SessionMark = new HashMap<>();
        final Map<Long, SessionDocument> studentWpeAction2SessionBulletin = new HashMap<>();

        final List<SessionMark> markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column("mark")
                .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                .where(in(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), ids(studentWpeActionList)))
                .order(property(SessionMark.id().fromAlias("mark")))
                .createStatement(getSession())
                .list();

        for (final SessionMark mark : markDql)
        {
            final SessionDocumentSlot slot = mark.getSlot();
            final Long studentWpeActionId = slot.getStudentWpeCAction().getId();

            final SessionDocument document = slot.getDocument();
            if (document instanceof SessionStudentGradeBookDocument)
            {
                // итоговые оценки - в зачетке
                SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
                if (!mark.isInSession())
                {
                    studentWpeAction2SessionMark.put(studentWpeActionId, regularMark);
                }
            } else if (document instanceof SessionSheetDocument ||
                    document instanceof SessionTransferOutsideDocument)
            {
                if (studentWpeAction2SessionBulletin.containsKey(studentWpeActionId))
                {
                    final SessionDocument oldDoc = studentWpeAction2SessionBulletin.get(studentWpeActionId);
                    if (oldDoc.getFormingDate().before(document.getFormingDate()))
                    {
                        studentWpeAction2SessionBulletin.put(studentWpeActionId, document);
                    }
                } else
                {
                    studentWpeAction2SessionBulletin.put(studentWpeActionId, document);
                }
            }
        }

        Map<MultiKey<IEntity>, StudentWpeActionContainer> res = new HashMap<>();
        for (EppStudentWpeCAction studentWpeAction : studentWpeActionList)
        {
            final SessionDocument sessionDocument = studentWpeAction2SessionBulletin.getOrDefault(studentWpeAction.getId(), null);
            final SessionMark sessionMark = studentWpeAction2SessionMark.getOrDefault(studentWpeAction.getId(), null);

            final EppStudentWorkPlanElement studentWpe = studentWpeAction.getStudentWpe();
            final EppYearEducationProcess eduYear = studentWpe.getYear();
            final Course course = studentWpe.getCourse();
            final Term term = studentWpe.getTerm();
            final Student student = studentWpe.getStudent();
            final MultiKey<IEntity> key = new MultiKey<>(eduYear, course, student);
            final StudentWpeActionContainer studentWpeActionContainer = res.getOrDefault(key, new StudentWpeActionContainer(eduYear, course, student, term));
            if (sessionMark == null || !sessionMark.getCachedMarkPositiveStatus())
            {
                studentWpeActionContainer.add(studentWpeAction, sessionDocument, null);
            } else
            {
                studentWpeActionContainer.add(studentWpeAction, sessionDocument, sessionMark);
            }
            res.put(key, studentWpeActionContainer);
        }
        List<StudentWpeActionContainer> list = res.values().stream()
                .sorted((o1, o2) -> {
                    final int yearVal1 = o1.getEduYear().getEducationYear().getIntValue();
                    final int yearVal2 = o2.getEduYear().getEducationYear().getIntValue();
                    int cmp = Integer.compare(yearVal1, yearVal2);
                    if (cmp == 0)
                    {
                        cmp = o1.getStudent().getFullFio().compareToIgnoreCase(o2.getStudent().getFullFio());
                    }
                    return cmp;
                })
                .collect(Collectors.toList());

        return Collections.unmodifiableList(list);
    }
}