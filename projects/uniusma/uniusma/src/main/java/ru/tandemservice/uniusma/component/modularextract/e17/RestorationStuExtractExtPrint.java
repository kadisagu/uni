/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e17;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.RestorationStuExtractExt;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniusma.entity.studentmodularorder.RestorationStuExtractExtUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.RestorationStuExtractExtUsmaExtGen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 19.05.2014
 */
public class RestorationStuExtractExtPrint implements IPrintFormCreator<RestorationStuExtractExt>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationStuExtractExt extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<String> tagsToDel = new ArrayList<>();

        Person person = extract.getEntity().getPerson();

        boolean isMale = person.isMale();

        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getRestorationDate());
        modifier.put("disOrderNum", extract.getDismissOrder());
        modifier.put("disOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDismissOrderDate()));
        modifier.put("dateApp", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getApplyDate()));

        if (extract.isUnreasonableDismissExcuse())
            modifier.put("attestationCommissionDecision", "В связи с решением аттестационной комиссии");
        else
            tagsToDel.add("attestationCommissionDecision");

//        modifier.put("dismissed", "отчислен" + (isMale ? "" : "а"));
        modifier.put("applyed", "обратил" + (isMale ? "ся" : "ась"));

        short number = 1;

        RestorationStuExtractExtUsmaExt extractExt = DataAccessServices.dao().getByNaturalId(new RestorationStuExtractExtUsmaExtGen.NaturalId(extract));
        if (null != extractExt && extractExt.isStartGrantsPaying())
        {
            modifier.put("startGrantPayingStr", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " назначить выплату академической стипендии с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getStartGrantsPayingDate()) + " г.");
        } else
            tagsToDel.add("startGrantPayingStr");

        RtfTableModifier table = new RtfTableModifier();
        if (extract.isHasDebts())
        {
            modifier.put("debts", String.valueOf(++number) + ". " + modifier.getStringValue("Student_D") + " " + modifier.getStringValue("fio_D") +
                    " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline()) + " г.");

            short i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for (StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[]{String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        } else
        {
            tagsToDel.add("debts");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        table.modify(document);

        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, tagsToDel, false, false);
        modifier.modify(document);
        return document;
    }
}
