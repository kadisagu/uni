/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.registry.UsmaRegElementCompetenceList;

import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        // add_eppPracticeCompetence, delete_eppPracticeCompetence
        // add_eppAttestationCompetence, delete_eppAttestationCompetence
        // add_eppDisciplineCompetence, delete_eppDisciplineCompetence
        EppRegistryElement element = get(model.getId());
        StringBuilder addCompetencePermissionKey = new StringBuilder("add_epp");
        StringBuilder deleteComptencePermissionKey = new StringBuilder("delete_epp");

        if (element instanceof EppRegistryDiscipline)
        {
            addCompetencePermissionKey.append("Discipline");
            deleteComptencePermissionKey.append("Discipline");

        } else if (element instanceof EppRegistryPractice)
        {
            addCompetencePermissionKey.append("Practice");
            deleteComptencePermissionKey.append("Practice");

        } else
        {
            addCompetencePermissionKey.append("Attestation");
            deleteComptencePermissionKey.append("Attestation");
        }

        addCompetencePermissionKey.append("Competence");
        deleteComptencePermissionKey.append("Competence");

        StaticListDataSource<UsmaCompetence2RegistryElementRel> dataSource = model.getDataSource();
        dataSource.clearColumns();
        dataSource.addColumn(new SimpleColumn("Код компетенции", UsmaCompetence2RegistryElementRel.usmaCompetence().eppSkillGroup().shortTitle().s()).setWidth(10).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Название", UsmaCompetence2RegistryElementRel.usmaCompetence().title().s()).setClickable(false));

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить компетенцию «{0}» из списка?", UsmaCompetence2RegistryElementRel.usmaCompetence().title().s()).setPermissionKey(deleteComptencePermissionKey.toString()));

        EppRegistryElement registryElement = get(model.getId());

        dataSource.setRowList(getList(UsmaCompetence2RegistryElementRel.class, UsmaCompetence2RegistryElementRel.L_REGISTRY_ELEMENT, registryElement, UsmaCompetence2RegistryElementRel.usmaCompetence().eppSkillGroup().shortTitle().s(), UsmaCompetence2RegistryElementRel.usmaCompetence().title().s()));

        model.setAddCompetencePermissionKey(addCompetencePermissionKey.toString());
    }
}