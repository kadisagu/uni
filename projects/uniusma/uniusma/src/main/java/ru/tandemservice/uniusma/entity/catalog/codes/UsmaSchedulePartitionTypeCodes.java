package ru.tandemservice.uniusma.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип разбиения учебного графика (УГМА)"
 * Имя сущности : usmaSchedulePartitionType
 * Файл data.xml : uniusma-catalog.data.xml
 */
public interface UsmaSchedulePartitionTypeCodes
{
    /** Константа кода (code) элемента : Неделя (title) */
    String WEEK = "1";
    /** Константа кода (code) элемента : Полунеделя (title) */
    String HALF_WEEK = "2";
    /** Константа кода (code) элемента : 2 дня (title) */
    String TWO_DAY = "3";
    /** Константа кода (code) элемента : День (title) */
    String ONE_DAY = "4";

    Set<String> CODES = ImmutableSet.of(WEEK, HALF_WEEK, TWO_DAY, ONE_DAY);
}
