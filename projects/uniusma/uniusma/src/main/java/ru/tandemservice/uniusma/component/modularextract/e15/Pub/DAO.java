/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e15.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.EduEnrAsTransferStuExtractUsmaExt;
import ru.tandemservice.uniusma.entity.studentmodularorder.gen.EduEnrAsTransferStuExtractUsmaExtGen;

/**
 * @author Denis Perminov
 * @since 27.05.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e15.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e15.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        EduEnrAsTransferStuExtractUsmaExt extractExt = getByNaturalId(new EduEnrAsTransferStuExtractUsmaExtGen.NaturalId(m.getExtract()));
        m.setExtUsmaExt(extractExt);
    }
}
