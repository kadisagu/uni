/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.DiseaseAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniusma.entity.catalog.UsmaDiseasesKind;

/**
 * @author DMITRY KNYAZEV
 * @since 24.06.2014
 */
@Configuration
public class UsmaMedCardDiseaseAddEdit extends BusinessComponentManager
{
	//data source
	public static final String DISEASES_KIND_DS = "diseasesKindDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(DISEASES_KIND_DS, diseasesKindDSHandler()))
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> diseasesKindDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), UsmaDiseasesKind.class)
				.order(UsmaDiseasesKind.title());
	}
}
