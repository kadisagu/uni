/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.logic.UsmaMedCardReportPrinter;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address.FactAddressParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.address.RegAddressParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.medCard.MedCardParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.ui.Add.block.personalData.PersonalDataParam;
import ru.tandemservice.uniusma.base.bo.UsmaMedCardReport.util.IUsmaDQLReportModifier;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 26.06.2014
 */
@Input({@Bind(key = UsmaMedCardReportAddUI.ORG_UNIT_ID, binding = UsmaMedCardReportAddUI.ORG_UNIT_ID),})
public class UsmaMedCardReportAddUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";

    private final PersonalDataParam _personalData = new PersonalDataParam();
    private final MedCardParam _medCard = new MedCardParam();
    private final FactAddressParam _factAddress = new FactAddressParam();
    private final RegAddressParam _regAddress = new RegAddressParam();

    private Long _orgUnitId;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case PersonalDataParam.GROUP_DS:
            {
                dataSource.put(UsmaMedCardReportAdd.PARAM_ORG_UNIT_ID, getOrgUnitId());
                break;
            }
            case FactAddressParam.FACT_SETTLEMENT_DS:
            {
				getFactAddress().getCountry().putParamIfActive(dataSource, AddressItem.PARAM_COUNTRY);
                break;
            }
            case FactAddressParam.FACT_STREET_DS:
            {
                getFactAddress().getCountry().putParamIfActive(dataSource, AddressItem.PARAM_COUNTRY);
                getFactAddress().getSettlement().putParamIfActive(dataSource, AddressBaseManager.PARAM_SETTLEMENT);
                break;
            }
            case RegAddressParam.REG_SETTLEMENT_DS:
            {
                getRegAddress().getCountry().putParamIfActive(dataSource, AddressItem.PARAM_COUNTRY);
                break;
            }
            case RegAddressParam.REG_STREET_DS:
            {
                getRegAddress().getCountry().putParamIfActive(dataSource, AddressItem.PARAM_COUNTRY);
                getRegAddress().getSettlement().putParamIfActive(dataSource, AddressBaseManager.PARAM_SETTLEMENT);
                break;
            }
        }
    }

    // Handlers
    public void onClickApply()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        if (getMedCard().getFromDate() != null && getMedCard().getToDate() != null && getMedCard().getFromDate().compareTo(getMedCard().getToDate()) > 0)
            errorCollector.add("Начальная дата должна быть меньше конечной.");

        if (errorCollector.hasErrors()) return;

        try
        {
            List<IUsmaDQLReportModifier> modifierList = new ArrayList<IUsmaDQLReportModifier>()
            {{
                    add(getMedCard());
                    add(getPersonalData());
                    add(getFactAddress());
                    add(getRegAddress());
                }};
            BusinessComponentUtils.downloadDocument(new UsmaMedCardReportPrinter(getOrgUnitId()).printReport(modifierList), false);
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // disable methods
    @SuppressWarnings("UnusedDeclaration")
    public boolean isDisableVaccineData()
    {
        if (!getMedCard().getNeedVaccine().isActive() || null == getMedCard().getNeedVaccine().getData())
        {
            clearReportParam(getMedCard().getVaccineList());
            clearReportParam(getMedCard().getVaccinationSign());
            clearReportParam(getMedCard().getPeriodical());
            return true;
        }
        else
            return false;
    }

    @SuppressWarnings("UnusedDeclaration")
    public boolean isDisableDiseasesData()
    {
        if (!getMedCard().getNeedDiseases().isActive() || null == getMedCard().getNeedDiseases().getData())
        {
            clearReportParam(getMedCard().getDiseasesList());
            return true;
        }
        else
            return false;
    }

    @SuppressWarnings("UnusedDeclaration")
    public boolean isDisableHealthGroupData()
    {
        if (!getMedCard().getNeedHealthGroup().isActive() ||
                null == getMedCard().getNeedHealthGroup().getData() ||
                TwinComboDataSourceHandler.getSelectedValueNotNull(getMedCard().getNeedHealthGroup().getData()))
        {
            clearReportParam(getMedCard().getHealthGroupList());
            return true;
        }
        else
            return false;
    }

    private <T> void clearReportParam(IReportParam<T> param)
    {
        param.setActive(false);
        param.setData(null);
    }

    // Getters/Setters
    public PersonalDataParam getPersonalData()
    {
        return _personalData;
    }

    public MedCardParam getMedCard()
    {
        return _medCard;
    }

    public FactAddressParam getFactAddress()
    {
        return _factAddress;
    }

    public RegAddressParam getRegAddress()
    {
        return _regAddress;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOrgUnitId() != null ? DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()) : super.getSecuredObject();
    }

    public String getPermissionKey()
    {
        return getOrgUnitId() == null ? "usmaMedCardReportViewPermissionKey" : new OrgUnitSecModel(DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId())).getPermission("orgUnit_usmaMedCardReportViewPermissionKey");
    }
}
