/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEmployee.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;

import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 16.07.12
 */
public class UsmaEmployeeDAO extends CommonDAO implements IUsmaEmployeeDAO
{
    @Override
    public EmployeePost getStartOrgUnitEmployeePost(EmployeePost employeePost)
    {
        // поднимаем список Должностей КР
        List<EmployeePost> employeePostList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "p").column("p")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePost.employee().fromAlias("p")), employeePost.getEmployee()))
                .where(DQLExpressions.ne(DQLExpressions.property(EmployeePost.postStatus().code().fromAlias("p")), EmployeePostStatusCodes.STATUS_POSSIBLE))
                .where(DQLExpressions.le(DQLExpressions.property(EmployeePost.postDate().fromAlias("p")), DQLExpressions.valueDate(employeePost.getPostDate())))
                .order(DQLExpressions.property(EmployeePost.postDate().fromAlias("p")), OrderDirection.desc)
                .createStatement(getSession()).list();

        // искомой Должностью будет та Должность, работа по которой не прерывалась
        // строим граф Должностей: связь должности с предыдущими должностями, работа по которым не прерывалась
        Map<EmployeePost, Set<EmployeePost>> nodeMap = new HashMap<>();
        for (EmployeePost post : employeePostList)
        {
            Set<EmployeePost> followingList = getFollowPostList(post, employeePostList);
            nodeMap.put(post, followingList);
        }

        // идем по графу, сохраняем все даты назначения должностей и сами должности, по которым пройдем
        List<CoreCollectionUtils.Pair<Date, EmployeePost>> possibleDateList = new ArrayList<>();
        Set<EmployeePost> alreadyProcessedPostSet = new HashSet<>();
        Queue<EmployeePost> postQueue = new ArrayDeque<>();
        postQueue.add(employeePost);
        while (!postQueue.isEmpty())
        {
            EmployeePost post = postQueue.poll();
            Set<EmployeePost> postSet = nodeMap.get(post);
            if (postSet != null)
            {
                for(EmployeePost itemPost : postSet)
                if(!alreadyProcessedPostSet.contains(itemPost))
                {
                    postQueue.add(itemPost);
                    alreadyProcessedPostSet.add(itemPost);
                    possibleDateList.add(new CoreCollectionUtils.Pair<>(itemPost.getPostDate(), itemPost));
                }
            }
        }

        if (possibleDateList.isEmpty())
            return employeePost;

        // возьмем должность с наименьшей датой назначения, тем самым получим Должность, начиная с которой работа не прерывалась
        Collections.sort(possibleDateList, (o1, o2) -> o1.getX().compareTo(o2.getX()));
        return possibleDateList.get(0).getY();
    }

    /**
     * Вычисляем список предыдущих Должностей из указанного списка <code>employeePostList</code> для указанной должности <code>employeePost</code>.<p/>
     * Должность считается предыдущей, если после нее не было промежутка более 14 дней.
     * @param employeePost должность
     * @param employeePostList список должностей
     * @return список предыдущих должностей, всегда не <code>null</code>
     */
    private Set<EmployeePost> getFollowPostList(EmployeePost employeePost, List<EmployeePost> employeePostList)
    {
        Set<EmployeePost> resultList = new HashSet<>();
        for (EmployeePost post : employeePostList)
            if ((post.getDismissalDate() != null) && (((Math.abs(CommonBaseDateUtil.getBetweenPeriod(employeePost.getPostDate(), post.getDismissalDate(), Calendar.DAY_OF_YEAR))) <= 14d) ||
                    (employeePost.getPostDate().getTime() <= post.getDismissalDate().getTime() && employeePost.getPostDate().getTime() >= post.getPostDate().getTime())))
            {
                resultList.add(post);
            }

        resultList.remove(employeePost);

        return resultList;
    }

    @Override
    public Date getStartOrgUnitDate(EmployeePost employeePost)
    {
        return getStartOrgUnitEmployeePost(employeePost).getPostDate();
    }

    @Override
    public CoreCollectionUtils.Pair<Date, String> getStartOrgUnitOrderDateNumber(EmployeePost employeePost)
    {
        EmployeePost post = getStartOrgUnitEmployeePost(employeePost);
        return new CoreCollectionUtils.Pair<>(post.getOrderDate(), post.getOrderNumber());
    }


}
