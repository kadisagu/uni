package ru.tandemservice.uniusma.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид теоретической нагрузки УГМА"
 * Имя сущности : usmaLoadType
 * Файл data.xml : uniusma-catalog.data.xml
 */
public interface UsmaLoadTypeCodes
{
    /** Константа кода (code) элемента : Лекции (интер.) (title) */
    String TYPE_LECTURES_INTER = "1";
    /** Константа кода (code) элемента : Практические занятия (интер.) (title) */
    String TYPE_PRACTICE_INTER = "2";
    /** Константа кода (code) элемента : Лабораторные занятия (интер.) (title) */
    String TYPE_LABS_INTER = "3";
    /** Константа кода (code) элемента : Часы за экзамены (title) */
    String TYPE_EXAM_HOURS = "4";

    Set<String> CODES = ImmutableSet.of(TYPE_LECTURES_INTER, TYPE_PRACTICE_INTER, TYPE_LABS_INTER, TYPE_EXAM_HOURS);
}
