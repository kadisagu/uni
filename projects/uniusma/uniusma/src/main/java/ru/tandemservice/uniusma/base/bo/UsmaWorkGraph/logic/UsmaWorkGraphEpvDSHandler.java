/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.util.UsmaWorkGraphEpvWrapper;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow;
import ru.tandemservice.uniusma.entity.eduPlan.UsmaWorkGraphRow2EduPlan;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
public class UsmaWorkGraphEpvDSHandler extends DefaultSearchDataSourceHandler
{
    public UsmaWorkGraphEpvDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder versionCoursesBuilder = new DQLSelectBuilder()
                .fromEntity(UsmaWorkGraphRow2EduPlan.class, "wgrep")
                .joinPath(DQLJoinType.inner, UsmaWorkGraphRow2EduPlan.row().fromAlias("wgrep"), "r")
                .where(eq(property("r", UsmaWorkGraphRow.graph().id()), commonValue(context.get(UsmaWorkGraphManager.BIND_WORK_GRAPH))))
                .column(property("wgrep", UsmaWorkGraphRow2EduPlan.eduPlanVersion()))
                .column(property("r", UsmaWorkGraphRow.course().intValue()));

        Map<EppEduPlanVersion, Set<Integer>> versionCoursesMap = new TreeMap<>(new EntityComparator<>(new EntityOrder(EppEduPlanVersion.eduPlan().title()), new EntityOrder(EppEduPlanVersion.title())));
        for (Object[] row: versionCoursesBuilder.createStatement(context.getSession()).<Object[]>list())
        {
            EppEduPlanVersion version = (EppEduPlanVersion) row[0];
            Integer courseNumber = (Integer) row[1];

            SafeMap.safeGet(versionCoursesMap, version, TreeSet.class).add(courseNumber);
        }

        List<UsmaWorkGraphEpvWrapper> wrappers = new ArrayList<>();
        for (Map.Entry<EppEduPlanVersion, Set<Integer>> versionEntry: versionCoursesMap.entrySet())
        {
            EppEduPlanVersion version = versionEntry.getKey();
            Collection<Integer> courses = versionEntry.getValue();
            String coursesStr = StringUtils.join(courses.toArray(new Integer[courses.size()]), ", ");

            wrappers.add(new UsmaWorkGraphEpvWrapper(version, coursesStr));
        }

        return ListOutputBuilder.get(input, wrappers).pageable(true).build();
    }
}