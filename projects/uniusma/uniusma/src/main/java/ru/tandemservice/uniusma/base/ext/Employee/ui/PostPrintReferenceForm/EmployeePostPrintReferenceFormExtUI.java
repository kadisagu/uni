/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.ext.Employee.ui.PostPrintReferenceForm;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostPrintReferenceForm.EmployeePostPrintReferenceFormUI;
import ru.tandemservice.uniusma.base.bo.UsmaEmployee.UsmaEmployeeManager;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 16.07.12
 */
public class EmployeePostPrintReferenceFormExtUI extends UIAddon
{
    public EmployeePostPrintReferenceFormExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private RtfDocument initRtfDocument()
    {
        EmployeePostPrintReferenceFormUI presenter = (EmployeePostPrintReferenceFormUI) getPresenter();
        RtfDocument document = presenter.initRtfDocument();

        Date startOrgUnitDate = UsmaEmployeeManager.instance().usmaEmployeeDAO().getStartOrgUnitDate(presenter.getEmployeePost());
        CoreCollectionUtils.Pair<Date, String> startOrgUnitOrderDateNumber = UsmaEmployeeManager.instance().usmaEmployeeDAO().getStartOrgUnitOrderDateNumber(presenter.getEmployeePost());

        new RtfInjectModifier()
                .put("dateStartOrgMonthStr", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(startOrgUnitDate))
                .put("regOrderNumber", startOrgUnitOrderDateNumber.getY() != null ? startOrgUnitOrderDateNumber.getY() : "_______")
                .put("regOrderDate", startOrgUnitOrderDateNumber.getX() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(startOrgUnitOrderDateNumber.getX()) : "____________")
                .modify(document);

        return document;
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("EmployeeReference.rtf").document(initRtfDocument()), true);

        getPresenter().deactivate();
    }
}
