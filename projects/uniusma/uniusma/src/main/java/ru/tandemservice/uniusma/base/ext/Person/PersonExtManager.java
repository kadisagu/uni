/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniusma.base.ext.Person;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import ru.tandemservice.uniusma.base.ext.Person.logic.UsmaPersonDao;

/**
 * @author Vasily Zhukov
 * @since 03.04.2012
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IPersonDao dao()
    {
        return new UsmaPersonDao();
    }
}