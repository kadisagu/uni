/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.e7.Pub;

import ru.tandemservice.uniusma.entity.studentmodularorder.TransferEduTypeStuExtractUsmaExt;

/**
 * @author Denis Perminov
 * @since 29.05.2014
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e7.Pub.Model
{
    TransferEduTypeStuExtractUsmaExt _extUsmaExt;
    private String _debtsList;

    public TransferEduTypeStuExtractUsmaExt getExtUsmaExt()
    {
        return _extUsmaExt;
    }

    public void setExtUsmaExt(TransferEduTypeStuExtractUsmaExt extUsmaExt)
    {
        _extUsmaExt = extUsmaExt;
    }

    public String getDebtsList()
    {
        return _debtsList;
    }

    public void setDebtsList(String debtsList)
    {
        _debtsList = debtsList;
    }
}
