/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.settings.ImtsaCyclesConfiguration;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniusma.entity.UsmaImtsaCyclePlanStructureRel;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setBlockGos2(Model.GosGenerationBlock.create(true, getDataSource(component, true)));
        model.setBlockGos3(Model.GosGenerationBlock.create(false, getDataSource(component, false)));
    }

    private DynamicListDataSource<UsmaImtsaCyclePlanStructureRel> getDataSource(IBusinessComponent component, final boolean gos2)
    {
        final Model model = getModel(component);
        DynamicListDataSource<UsmaImtsaCyclePlanStructureRel> dataSourceGos = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model, gos2);
        }, 10);

        dataSourceGos.addColumn(new SimpleColumn("Цикл ИМЦА", UsmaImtsaCyclePlanStructureRel.P_IMTSA_CYCLE).setWidth(25).setOrderable(false));
        dataSourceGos.addColumn(new SimpleColumn("Элемент ГОС", UsmaImtsaCyclePlanStructureRel.planStructure().title()).setOrderable(false));
        dataSourceGos.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditImtsaCycleRel"));
        dataSourceGos.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteImtsaCycleRel"));

        return dataSourceGos;
    }


    public void onClickAddImtsaCycleRelGos3(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.settings.ImtsaCycleAddEdit", new ParametersMap().add("gos2", false)));
    }

    public void onClickAddImtsaCycleRelGos2(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.settings.ImtsaCycleAddEdit", new ParametersMap().add("gos2", true)));
    }


    public void onClickEditImtsaCycleRel(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniusma.component.settings.ImtsaCycleAddEdit", new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    public void onClickDeleteImtsaCycleRel(IBusinessComponent component)
    {
        Long imtsaCycleRelId = component.getListenerParameter();
        getDao().delete(imtsaCycleRelId);
        component.refresh();
    }


    public void onClickSearchGos3(IBusinessComponent component)
    {
        onClickSearch(getModel(component).getBlockGos3());
    }

    public void onClickSearchGos2(IBusinessComponent component)
    {
        onClickSearch(getModel(component).getBlockGos2());
    }

    public void onClickClearGos3(IBusinessComponent component)
    {
        onClickClear(getModel(component).getBlockGos3());
    }

    public void onClickClearGos2(IBusinessComponent component)
    {
        onClickClear(getModel(component).getBlockGos2());
    }


    private void onClickSearch(Model.GosGenerationBlock gosGenerationBlock)
    {
        IDataSettings dataSettings = gosGenerationBlock.getSettings();
        String cycleFilter = gosGenerationBlock.getCycleFilter();
        DynamicListDataSource<UsmaImtsaCyclePlanStructureRel> dataSource = gosGenerationBlock.getDataSource();

        dataSettings.set(Model.CYCLE_FILTER_NAME, cycleFilter);
        DataSettingsFacade.saveSettings(dataSettings);
        dataSource.refresh();
    }

    private void onClickClear(Model.GosGenerationBlock gosGenerationBlock)
    {
        gosGenerationBlock.setCycleFilter(null);
        IDataSettings dataSettings = gosGenerationBlock.getSettings();
        dataSettings.clear();
        onClickSearch(gosGenerationBlock);
    }
}