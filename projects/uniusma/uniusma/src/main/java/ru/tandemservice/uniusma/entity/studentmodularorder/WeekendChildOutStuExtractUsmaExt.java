package ru.tandemservice.uniusma.entity.studentmodularorder;

import ru.tandemservice.uniusma.entity.studentmodularorder.gen.*;

/**
 * Расширение выписки из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
 */
public class WeekendChildOutStuExtractUsmaExt extends WeekendChildOutStuExtractUsmaExtGen
{
}