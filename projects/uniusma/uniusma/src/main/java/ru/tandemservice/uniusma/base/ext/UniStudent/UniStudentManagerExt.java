/*$Id$*/
package ru.tandemservice.uniusma.base.ext.UniStudent;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 08.02.2016
 */
@Configuration
public class UniStudentManagerExt extends BusinessObjectExtensionManager
{
}
