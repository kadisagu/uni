/* $Id:$ */
package ru.tandemservice.uniusma.report.bo.UsmaEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusma.report.bo.UsmaEnrReport.logic.IUsmaEnrReportDao;
import ru.tandemservice.uniusma.report.bo.UsmaEnrReport.logic.UsmaEnrReportDao;

/**
 * @author Denis Perminov
 * @since 27.08.2014
 */
@Configuration
public class UsmaEnrReportManager  extends BusinessObjectManager
{
    public static UsmaEnrReportManager instance() { return instance(UsmaEnrReportManager.class); }

    @Bean
    public IUsmaEnrReportDao dao() { return new UsmaEnrReportDao(); }
}
