{orderType}\par\par
\keep\keepn\fi709\qj
{reason} \par\fi0\par
приказываю:\par
\fi709\qj
1. {Student_D} {fio} {birthDate} года рождения, {course} курса {orgUnit_G} (группа {group}) {developForm_GF} формы обучения {compensationTypeStr_G_Alt}, выдать дубликат студенческого билета № {bookNumber} взамен утерянного.\par\par
Основание: {listBasics}\par\fi0