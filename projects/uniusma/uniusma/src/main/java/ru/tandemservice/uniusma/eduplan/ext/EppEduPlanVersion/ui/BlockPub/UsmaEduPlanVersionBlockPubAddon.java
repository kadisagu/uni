/* $Id$ */
package ru.tandemservice.uniusma.eduplan.ext.EppEduPlanVersion.ui.BlockPub;

import org.apache.commons.io.FileUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.w3c.dom.Document;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub.EppEduPlanVersionBlockPubUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniusma.base.ext.SystemAction.ui.Pub.UsmaSystemActionPubAddon;
import ru.tandemservice.uniusma.dao.UniUsmaDaoFacade;
import ru.tandemservice.uniusma.entity.UsmaImtsaXml;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 03.10.2014
 */
public class UsmaEduPlanVersionBlockPubAddon extends UIAddon
{
    public UsmaEduPlanVersionBlockPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private EppEduPlanVersionBlock getBlock() { return ((EppEduPlanVersionBlockPubUI) this.getPresenter()).getBlock(); }

    public void onClickLoadImtsa()
    {
        this.getActivationBuilder().asRegionDialog("ru.tandemservice.uniusma.component.eduplan.UsmaImtsaImport").parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId()).activate();
    }


    public void onClickReImportImtsa()
    {
        UsmaSystemActionPubAddon.doReImportImtsa(Collections.singletonList(getBlock().getId()), false);
        this.getPresenter().getSupport().doRefresh();
    }

    public void onClickLoadFile()
    {
        EppEduPlanVersionBlock block = getBlock();
        if (!(block.getEduPlanVersion().getEduPlan() instanceof EppEduPlanHigherProf))
            throw new ApplicationException("Выгрузка учебных планов, отличных от учебных планов ВПО, запрещена.");

        EppEduPlanHigherProf eduPlan = ((EppEduPlanHigherProf) block.getEduPlanVersion().getEduPlan());
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "lhs")
                .column(property("lhs", EducationLevelsHighSchool.id()))
                .where(eq(property("lhs", EducationLevelsHighSchool.assignedQualification()), value(eduPlan.getProgramQualification())))
                .where(eq(property("lhs", EducationLevelsHighSchool.programOrientation()), value(eduPlan.getProgramOrientation())))
                .where(block instanceof EppEduPlanVersionRootBlock ?
                               and(
                                       isNull(property("lhs", EducationLevelsHighSchool.educationLevel().eduProgramSpecialization())),
                                       eq(property("lhs", EducationLevelsHighSchool.educationLevel().eduProgramSubject()), value(eduPlan.getProgramSubject()))) :
                               eq(property("lhs", EducationLevelsHighSchool.educationLevel().eduProgramSpecialization()), value(((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization())));

        Collection<Long> eduLevels = IUniBaseDao.instance.get().getList(builder);
        if (eduLevels.size() != 1)
            throw new ApplicationException("Невозможно найти учебный план. Проверьте корректность параметров обучения студентов.");

        Long eduLevelId = eduLevels.iterator().next();
        Long versionId = block .getEduPlanVersion().getId();

        UsmaImtsaXml xml = UniUsmaDaoFacade.getUniUsmaDAO().getXml(block.getId());
        if (xml != null)
        {
            String encoding = xml.getEncoding();
            if (encoding == null)
            {
                // костыль, нужно для повторной загрузки файлов ИМЦА
                Document document;
                File file;
                for (String pEncoding : Arrays.asList("UTF-8", "windows-1251"))
                {
                    if (encoding != null)
                    {
                        break;
                    }
                    try
                    {
                        file = File.createTempFile("usmaTemp-", "xml");
                        FileUtils.writeStringToFile(file, new String(xml.getXml()), pEncoding);
                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                        DocumentBuilder db = dbf.newDocumentBuilder();
                        document = db.parse(file);
                        document.getDocumentElement().normalize();
                        encoding = pEncoding;

                    } catch (Exception e)
                    {
                        // continue;
                    }
                }

                if (encoding == null)
                {
                    encoding = "UTF-8";
                }
            }
            String fileName = Long.toHexString(versionId) + "-" + Long.toHexString(eduLevelId) + "-" + String.valueOf(xml.getNumber()) + "-" + xml.getFileName();
            byte[] xmlContent = xml.getXml();
            try
            {
                xmlContent = new String(xmlContent, "UTF-8").getBytes(encoding);

            }catch (UnsupportedEncodingException e)
            {
                //

            } finally
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName).document(xmlContent), false);
            }
        }
    }

    public boolean isImtsaButtonsVisible() { return getBlock().getEduPlanVersion().getEduPlan() instanceof EppEduPlanHigherProf; }
}