/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.registry.UsmaRegElementCompetenceList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniusma.entity.UsmaCompetence2RegistryElementRel;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model
{
    private Long _id;
    private final StaticListDataSource<UsmaCompetence2RegistryElementRel> _dataSource = new StaticListDataSource<>();
    private String _addCompetencePermissionKey;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public StaticListDataSource<UsmaCompetence2RegistryElementRel> getDataSource()
    {
        return _dataSource;
    }

    public String getAddCompetencePermissionKey()
    {
        return _addCompetencePermissionKey;
    }

    public void setAddCompetencePermissionKey(String addCompetencePermissionKey)
    {
        _addCompetencePermissionKey = addCompetencePermissionKey;
    }
}