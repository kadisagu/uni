/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1005.Add;

import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public interface IDAO extends ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.IDAO<Model>
{
    Date getDate(Student student, String period, DevelopGridTerm term, boolean isStartDate);
}
