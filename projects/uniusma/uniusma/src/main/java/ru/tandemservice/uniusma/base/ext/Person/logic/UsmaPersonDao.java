/* $Id$ */
package ru.tandemservice.uniusma.base.ext.Person.logic;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import org.tandemframework.shared.person.base.bo.Person.logic.PersonDao;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;

/**
 * Created by dseleznev on 14.06.2016.
 */
public class UsmaPersonDao extends PersonDao implements IPersonDao
{
    @Override
    public String getUniqueLogin(Person person)
    {
        if (person == null) throw new RuntimeException("Person cannot be null!");

        final Session session = this.getSession();
        final FlushMode flushMode = session.getFlushMode();
        try
        {
            session.setFlushMode(FlushMode.NEVER);

            final String firstName = person.getIdentityCard().getFirstName();
            final String middleName = person.getIdentityCard().getMiddleName();
            final String lastName = person.getIdentityCard().getLastName();

            int postfix = 1;
            final String login = PersonSecurityUtil.generateLogin(firstName, lastName, middleName);
            String result = login.replaceAll("[^A-Za-z0-9.\\-_]", "").trim();
            if(result.startsWith(".")) result = result.substring(1);
            result = result.trim();

            while (existsEntity(Principal.class, Principal.login().s(), result.trim()))
            {
                result = login + postfix++;
            }

            return result;

        } finally
        {
            session.setFlushMode(flushMode);
        }
    }

    @Override
    public void updatePersonLogin(final Long personId, final String newLogin, final String newPassword, final AuthenticationType authenticationType)
    {
        String preProcessedLogin = newLogin.replaceAll("[^A-Za-z0-9.\\-_]", "").trim();
        if(preProcessedLogin.startsWith(".")) preProcessedLogin = preProcessedLogin.substring(1);
        preProcessedLogin = preProcessedLogin.trim();
        super.updatePersonLogin(personId, preProcessedLogin, newPassword, authenticationType);
    }
}