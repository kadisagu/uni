package ru.tandemservice.uniusma.entity.studentmodularorder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении за за невыполнение учебного плана
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsmaExcludeEduPlanStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract";
    public static final String ENTITY_NAME = "usmaExcludeEduPlanStuExtract";
    public static final int VERSION_HASH = 1794035368;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_STOP_GRANTS_PAYING = "stopGrantsPaying";
    public static final String P_STOP_PAYING_DATE = "stopPayingDate";
    public static final String P_GIVE_DIPLOMA = "giveDiploma";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_SESSION_ACAD_DEBT = "sessionAcadDebt";
    public static final String P_DISCIPLINES_ACAD_DEBT = "disciplinesAcadDebt";

    private Date _excludeDate;     // Дата отчисления
    private boolean _stopGrantsPaying;     // Отменить выплату стипендии
    private Date _stopPayingDate;     // Дата прекращения выплаты стипендии
    private boolean _giveDiploma;     // Выдается диплом о незаконченном высшем образовании
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private String _sessionAcadDebt;     // По результатам сессии
    private String _disciplinesAcadDebt;     // Задолженность по предметам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopGrantsPaying()
    {
        return _stopGrantsPaying;
    }

    /**
     * @param stopGrantsPaying Отменить выплату стипендии. Свойство не может быть null.
     */
    public void setStopGrantsPaying(boolean stopGrantsPaying)
    {
        dirty(_stopGrantsPaying, stopGrantsPaying);
        _stopGrantsPaying = stopGrantsPaying;
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     */
    public Date getStopPayingDate()
    {
        return _stopPayingDate;
    }

    /**
     * @param stopPayingDate Дата прекращения выплаты стипендии.
     */
    public void setStopPayingDate(Date stopPayingDate)
    {
        dirty(_stopPayingDate, stopPayingDate);
        _stopPayingDate = stopPayingDate;
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isGiveDiploma()
    {
        return _giveDiploma;
    }

    /**
     * @param giveDiploma Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     */
    public void setGiveDiploma(boolean giveDiploma)
    {
        dirty(_giveDiploma, giveDiploma);
        _giveDiploma = giveDiploma;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return По результатам сессии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSessionAcadDebt()
    {
        return _sessionAcadDebt;
    }

    /**
     * @param sessionAcadDebt По результатам сессии. Свойство не может быть null.
     */
    public void setSessionAcadDebt(String sessionAcadDebt)
    {
        dirty(_sessionAcadDebt, sessionAcadDebt);
        _sessionAcadDebt = sessionAcadDebt;
    }

    /**
     * @return Задолженность по предметам. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getDisciplinesAcadDebt()
    {
        return _disciplinesAcadDebt;
    }

    /**
     * @param disciplinesAcadDebt Задолженность по предметам. Свойство не может быть null.
     */
    public void setDisciplinesAcadDebt(String disciplinesAcadDebt)
    {
        dirty(_disciplinesAcadDebt, disciplinesAcadDebt);
        _disciplinesAcadDebt = disciplinesAcadDebt;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsmaExcludeEduPlanStuExtractGen)
        {
            setExcludeDate(((UsmaExcludeEduPlanStuExtract)another).getExcludeDate());
            setStopGrantsPaying(((UsmaExcludeEduPlanStuExtract)another).isStopGrantsPaying());
            setStopPayingDate(((UsmaExcludeEduPlanStuExtract)another).getStopPayingDate());
            setGiveDiploma(((UsmaExcludeEduPlanStuExtract)another).isGiveDiploma());
            setStudentStatusOld(((UsmaExcludeEduPlanStuExtract)another).getStudentStatusOld());
            setSessionAcadDebt(((UsmaExcludeEduPlanStuExtract)another).getSessionAcadDebt());
            setDisciplinesAcadDebt(((UsmaExcludeEduPlanStuExtract)another).getDisciplinesAcadDebt());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsmaExcludeEduPlanStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsmaExcludeEduPlanStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsmaExcludeEduPlanStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return obj.getExcludeDate();
                case "stopGrantsPaying":
                    return obj.isStopGrantsPaying();
                case "stopPayingDate":
                    return obj.getStopPayingDate();
                case "giveDiploma":
                    return obj.isGiveDiploma();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "sessionAcadDebt":
                    return obj.getSessionAcadDebt();
                case "disciplinesAcadDebt":
                    return obj.getDisciplinesAcadDebt();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "stopGrantsPaying":
                    obj.setStopGrantsPaying((Boolean) value);
                    return;
                case "stopPayingDate":
                    obj.setStopPayingDate((Date) value);
                    return;
                case "giveDiploma":
                    obj.setGiveDiploma((Boolean) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "sessionAcadDebt":
                    obj.setSessionAcadDebt((String) value);
                    return;
                case "disciplinesAcadDebt":
                    obj.setDisciplinesAcadDebt((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                        return true;
                case "stopGrantsPaying":
                        return true;
                case "stopPayingDate":
                        return true;
                case "giveDiploma":
                        return true;
                case "studentStatusOld":
                        return true;
                case "sessionAcadDebt":
                        return true;
                case "disciplinesAcadDebt":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return true;
                case "stopGrantsPaying":
                    return true;
                case "stopPayingDate":
                    return true;
                case "giveDiploma":
                    return true;
                case "studentStatusOld":
                    return true;
                case "sessionAcadDebt":
                    return true;
                case "disciplinesAcadDebt":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return Date.class;
                case "stopGrantsPaying":
                    return Boolean.class;
                case "stopPayingDate":
                    return Date.class;
                case "giveDiploma":
                    return Boolean.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "sessionAcadDebt":
                    return String.class;
                case "disciplinesAcadDebt":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsmaExcludeEduPlanStuExtract> _dslPath = new Path<UsmaExcludeEduPlanStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsmaExcludeEduPlanStuExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#isStopGrantsPaying()
     */
    public static PropertyPath<Boolean> stopGrantsPaying()
    {
        return _dslPath.stopGrantsPaying();
    }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getStopPayingDate()
     */
    public static PropertyPath<Date> stopPayingDate()
    {
        return _dslPath.stopPayingDate();
    }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#isGiveDiploma()
     */
    public static PropertyPath<Boolean> giveDiploma()
    {
        return _dslPath.giveDiploma();
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return По результатам сессии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getSessionAcadDebt()
     */
    public static PropertyPath<String> sessionAcadDebt()
    {
        return _dslPath.sessionAcadDebt();
    }

    /**
     * @return Задолженность по предметам. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getDisciplinesAcadDebt()
     */
    public static PropertyPath<String> disciplinesAcadDebt()
    {
        return _dslPath.disciplinesAcadDebt();
    }

    public static class Path<E extends UsmaExcludeEduPlanStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Boolean> _stopGrantsPaying;
        private PropertyPath<Date> _stopPayingDate;
        private PropertyPath<Boolean> _giveDiploma;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<String> _sessionAcadDebt;
        private PropertyPath<String> _disciplinesAcadDebt;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(UsmaExcludeEduPlanStuExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Отменить выплату стипендии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#isStopGrantsPaying()
     */
        public PropertyPath<Boolean> stopGrantsPaying()
        {
            if(_stopGrantsPaying == null )
                _stopGrantsPaying = new PropertyPath<Boolean>(UsmaExcludeEduPlanStuExtractGen.P_STOP_GRANTS_PAYING, this);
            return _stopGrantsPaying;
        }

    /**
     * @return Дата прекращения выплаты стипендии.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getStopPayingDate()
     */
        public PropertyPath<Date> stopPayingDate()
        {
            if(_stopPayingDate == null )
                _stopPayingDate = new PropertyPath<Date>(UsmaExcludeEduPlanStuExtractGen.P_STOP_PAYING_DATE, this);
            return _stopPayingDate;
        }

    /**
     * @return Выдается диплом о незаконченном высшем образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#isGiveDiploma()
     */
        public PropertyPath<Boolean> giveDiploma()
        {
            if(_giveDiploma == null )
                _giveDiploma = new PropertyPath<Boolean>(UsmaExcludeEduPlanStuExtractGen.P_GIVE_DIPLOMA, this);
            return _giveDiploma;
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return По результатам сессии. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getSessionAcadDebt()
     */
        public PropertyPath<String> sessionAcadDebt()
        {
            if(_sessionAcadDebt == null )
                _sessionAcadDebt = new PropertyPath<String>(UsmaExcludeEduPlanStuExtractGen.P_SESSION_ACAD_DEBT, this);
            return _sessionAcadDebt;
        }

    /**
     * @return Задолженность по предметам. Свойство не может быть null.
     * @see ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeEduPlanStuExtract#getDisciplinesAcadDebt()
     */
        public PropertyPath<String> disciplinesAcadDebt()
        {
            if(_disciplinesAcadDebt == null )
                _disciplinesAcadDebt = new PropertyPath<String>(UsmaExcludeEduPlanStuExtractGen.P_DISCIPLINES_ACAD_DEBT, this);
            return _disciplinesAcadDebt;
        }

        public Class getEntityClass()
        {
            return UsmaExcludeEduPlanStuExtract.class;
        }

        public String getEntityName()
        {
            return "usmaExcludeEduPlanStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
