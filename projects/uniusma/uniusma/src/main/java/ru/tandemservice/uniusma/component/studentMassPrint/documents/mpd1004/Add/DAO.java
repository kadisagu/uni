/* $Id$ */
package ru.tandemservice.uniusma.component.studentMassPrint.documents.mpd1004.Add;

import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniusma.component.studentMassPrint.MassPrintUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class DAO extends ru.tandemservice.uniusma.component.studentMassPrint.documents.Base.Add.DAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        Set<OrgUnit> formativeOrgUnits = new HashSet<>();
        Set<EppEduPlanVersion> versions = new HashSet<>();

        List<Student> studentList = model.getStudentList();

        Student firstStudent = studentList.get(0);
        model.setFirstStudent(firstStudent);

        boolean haventPlan = false;
        for (Student student : studentList)
        {
            EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
            if (rel == null)
                haventPlan = true;
            else
                versions.add(rel.getEduPlanVersion());

            formativeOrgUnits.add(student.getEducationOrgUnit().getFormativeOrgUnit());
        }
        if (formativeOrgUnits.size() == 1)
            model.setTelephone(firstStudent.getEducationOrgUnit().getFormativeOrgUnit().getPhone());

        if (haventPlan)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "Не у всех выбранных студентов есть УП.");
        if (versions.size() > 1)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "У выбранных студентов разные УП.");

        model.setPeriodList(Arrays.asList(Model.EXAMINATION_SESSION,
                                          Model.RESULT_QUALIFICATION_WORK,
                                          Model.STATE_EXAMINATION,
                                          Model.STATE_FINAL_EXAMINATION,
                                          Model.FINAL_EXAMINATION));
        model.setPeriod(Model.EXAMINATION_SESSION);

        List<String> postList = new ArrayList<>();
        List<String> fioList = new ArrayList<>();
        MassPrintUtil.findManagers(
                formativeOrgUnits,
                (title, first) -> {
                    postList.add(title);
                    if (first) model.setManagerPostTitle(title);
                },
                (fio, first) -> {
                    fioList.add(fio);
                    if (first) model.setManagerFio(fio);
                });
        model.setManagerPostList(postList);
        model.setManagerFioList(fioList);

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getFirstStudent().getId());
        if (rel != null)
        {
            final EppEduPlanVersion version = rel.getEduPlanVersion();
            model.setTermModel(new FullCheckSelectModel(DevelopGridTerm.part().title().s())
            {
                @Override
                public ListResult<DevelopGridTerm> findValues(String filter)
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(DevelopGridTerm.class, "dgt")
                            .where(eq(property("dgt", DevelopGridTerm.course()), value(model.getFirstStudent().getCourse())))
                            .where(eq(property("dgt", DevelopGridTerm.developGrid()), value(version.getEduPlan().getDevelopGrid())));

                    return new ListResult<>(DAO.this.<DevelopGridTerm>getList(builder));
                }
            });
        }

        model.setDateStartCertification(getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), false));

        model.setEmployeePostListModel(new BaseSingleSelectModel("fullTitle")
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if (primaryKey == null) return null;
                EmployeePost employeePost = DAO.this.get(EmployeePost.class, (Long) primaryKey);
                return employeePost == null ? null : employeePost;
            }

            @Override
            public ListResult<EmployeePost> findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EmployeePost.class, "e")
                        .column("e")
                        .where(eq(property("e", EmployeePost.postStatus().active().s()), value(Boolean.TRUE)))
                        .where(eq(property("e", EmployeePost.employee().archival().s()), value(Boolean.FALSE)))
                        .order(property("e", EmployeePost.employee().person().identityCard().fullFio()));

                FilterUtils.applyLikeFilter(builder, filter,
                                            EmployeePost.employee().person().identityCard().fullFio().fromAlias("e"),
                                            EmployeePost.postRelation().postBoundedWithQGandQL().post().title().fromAlias("e"),
                                            EmployeePost.orgUnit().shortTitle().fromAlias("e"),
                                            EmployeePost.orgUnit().orgUnitType().title().fromAlias("e"));

                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
                statement.setMaxResults(50);
                return new ListResult<>(statement.list(), count != null ? count.intValue() : 0L);
            }
        });

        if (formativeOrgUnits.size() > 1)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "Студенты принадлежат к разным формирующим подразделениям.");
        model.setDisplayWarning(model.getWarningBuilder().length() > 0);
    }


    @Override
    public Date getDate(Student student, String period, DevelopGridTerm term, boolean isStartDate)
    {
        return ru.tandemservice.uniusma.component.documents.d1004.Add.DAO.getDate(getSession(), student, period, term, isStartDate);
    }
}
