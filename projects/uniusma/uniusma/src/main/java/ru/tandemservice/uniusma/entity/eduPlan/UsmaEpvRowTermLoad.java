package ru.tandemservice.uniusma.entity.eduPlan;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniusma.entity.catalog.UsmaLoadType;
import ru.tandemservice.uniusma.entity.eduPlan.gen.*;

/**
 * Нагрузка строки УПв в семестре (УГМА)
 */
public class UsmaEpvRowTermLoad extends UsmaEpvRowTermLoadGen
{
    public UsmaEpvRowTermLoad()
    {

    }

    public UsmaEpvRowTermLoad(EppEpvRowTerm rowTerm, UsmaLoadType loadType)
    {
        this.setRowTerm(rowTerm);
        this.setLoadType(loadType);
    }

    @EntityDSLSupport
    @Override
    public Double getLoadAsDouble()
    {
        return UniEppUtils.wrap(this.getLoad());
    }

    public void setLoadAsDouble(Double loadAsDouble)
    {
        this.setLoad(UniEppUtils.unwrap(loadAsDouble));
    }
}