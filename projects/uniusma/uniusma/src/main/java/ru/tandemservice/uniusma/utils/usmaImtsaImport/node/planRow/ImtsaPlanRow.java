/**
 *$Id$
 */
package ru.tandemservice.uniusma.utils.usmaImtsaImport.node.planRow;

import org.tandemframework.core.common.ITitled;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alexander Zhebko
 * @since 05.08.2013
 */
public abstract class ImtsaPlanRow implements ITitled
{
    // загружаемые данные
    private String _title;
    private ImtsaPlanRow _parent;
    private Map<String, ImtsaPlanRow> _childMap = new TreeMap<>();

    private String _id; // индекс ИМЦА

    public String getTitle(){ return _title; }
    public void setTitle(String title){ _title = title; }

    public Map<String, ImtsaPlanRow> getChildMap(){ return _childMap; }
    public void setChildMap(Map<String, ImtsaPlanRow> childMap){ _childMap = childMap; }

    public ImtsaPlanRow getParent(){ return _parent; }
    public void setParent(ImtsaPlanRow parent){ _parent = parent; }

    public String getId() { return _id; }
    public void setId(String id) { _id = id; }


    // вспомогательные данные
    public static String getIdSeparator(){ return ID_SEPARATOR; }
    public static String getCompetenceCodesSeparator(){ return COMPETENCE_CODES_SEPARATOR; }
    public static String getCompetencesIndexesSeparator(){ return COMPETENCES_INDEXES_SEPARATOR; }

    private static final String ID_SEPARATOR = ".";
    private static final String COMPETENCE_CODES_SEPARATOR = "&";
    private static final String COMPETENCES_INDEXES_SEPARATOR = ",";
}