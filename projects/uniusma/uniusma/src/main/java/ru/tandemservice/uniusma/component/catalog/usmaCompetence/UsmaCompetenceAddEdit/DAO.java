/**
 *$Id$
 */
package ru.tandemservice.uniusma.component.catalog.usmaCompetence.UsmaCompetenceAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniusma.entity.catalog.UsmaCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class DAO extends DefaultCatalogAddEditDAO<UsmaCompetence, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEppSkillGroupModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(EppSkillGroup.class, "esg")
                        .where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EppSkillGroup.title().fromAlias("esg"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));

                return new ListResult<>(builder.createStatement(getSession()).<EppSkillGroup>list());
            }
        });
    }
}