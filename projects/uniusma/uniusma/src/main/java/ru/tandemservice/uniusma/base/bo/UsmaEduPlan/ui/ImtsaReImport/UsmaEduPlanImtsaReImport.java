/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaEduPlan.ui.ImtsaReImport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.logic.UsmaEpvBlockDSHandler;
import ru.tandemservice.uniusma.base.bo.UsmaEduPlan.util.UsmaEpvBlockWrapper;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
@Configuration
public class UsmaEduPlanImtsaReImport extends BusinessComponentManager
{
    public static final String DS_EDU_PLAN_VERSION = "eduPlanVersionDS";

    public static final String DS_PROGRAM_SUBJECT = "programSubjectDS";
    public static final String DS_PROGRAM_FORM = "programFormDS";
    public static final String DS_DEVELOP_CONDITION = "developConditionDS";
    public static final String DS_PROGRAM_TRAIT = "programTraitDS";
    public static final String DS_DEVELOP_GRID = "developGridDS";

    public static final String BIND_NUMBER = "number";
    public static final String BIND_PROGRAM_SUBJECT = "programSubject";
    public static final String BIND_PROGRAM_FORM = "programForm";
    public static final String BIND_DEVELOP_CONDITION = "developCondition";
    public static final String BIND_PROGRAM_TRAIT = "programTrait";
    public static final String BIND_DEVELOP_GRID = "developGrid";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_EDU_PLAN_VERSION, epvDSColumns(), epvDSHandler()))
            .addDataSource(selectDS(DS_PROGRAM_SUBJECT, programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(selectDS(DS_PROGRAM_FORM, programFormDSHandler()))
            .addDataSource(selectDS(DS_DEVELOP_CONDITION, developConditionDSHandler()))
            .addDataSource(selectDS(DS_PROGRAM_TRAIT, programTraitDSHandler()))
            .addDataSource(selectDS(DS_DEVELOP_GRID, developGridDSHandler()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programSubject()), property(alias)))
                    .buildQuery()));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramKind.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programForm()), property(alias)));

                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), context.get(BIND_PROGRAM_SUBJECT));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(EduProgramForm.title())
            .order(EduProgramForm.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.developCondition()), property(alias)));

                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), context.get(BIND_PROGRAM_SUBJECT));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programForm(), context.get(BIND_PROGRAM_FORM));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(DevelopCondition.title())
            .order(DevelopCondition.code());
    }


    @Bean
    public IDefaultComboDataSourceHandler programTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programTrait()), property(alias)));

                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), context.get(BIND_PROGRAM_SUBJECT));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programForm(), context.get(BIND_PROGRAM_FORM));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.developCondition(), context.get(BIND_DEVELOP_CONDITION));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(EduProgramTrait.title())
            .order(EduProgramTrait.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopGrid.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.developGrid()), property(alias)));

                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), context.get(BIND_PROGRAM_SUBJECT));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programForm(), context.get(BIND_PROGRAM_FORM));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.developCondition(), context.get(BIND_DEVELOP_CONDITION));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programTrait(), context.get(BIND_PROGRAM_TRAIT));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(DevelopGrid.title())
            .order(DevelopGrid.developPeriod().priority())
            .order(DevelopGrid.title());
    }

    @Bean
    public ColumnListExtPoint epvDSColumns()
    {
        return columnListExtPointBuilder(DS_EDU_PLAN_VERSION)
                .addColumn(checkboxColumn("selected").selectCaption("Выбрано"))
                .addColumn(publisherColumn("title", UsmaEpvBlockWrapper.TITLE).width("40%"))
                .addColumn(textColumn("blocks", UsmaEpvBlockWrapper.BLOCKS).formatter(RowCollectionFormatter.INSTANCE))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler epvDSHandler()
    {
        return new UsmaEpvBlockDSHandler(getName());
    }
}