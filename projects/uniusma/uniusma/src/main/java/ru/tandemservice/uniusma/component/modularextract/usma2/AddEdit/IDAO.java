/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma2.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeDebtStuExtract;

/**
 * @author Denis Perminov
 * @since 07.05.2014
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<UsmaExcludeDebtStuExtract, Model>
{

}
