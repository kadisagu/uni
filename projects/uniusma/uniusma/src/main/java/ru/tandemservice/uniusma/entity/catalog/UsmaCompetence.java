package ru.tandemservice.uniusma.entity.catalog;

import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniusma.entity.catalog.gen.*;

import java.util.Comparator;

/**
 * Компетенции (УГМА)
 */
public class UsmaCompetence extends UsmaCompetenceGen
{
    public static final Comparator<UsmaCompetence> USAM_COMPETENCE_CODE_TITLE_COMPARATOR = new Comparator<UsmaCompetence>()
    {
        @Override
        public int compare(UsmaCompetence o1, UsmaCompetence o2)
        {
            int result = o1.getEppSkillGroup().getShortTitle().compareTo(o2.getEppSkillGroup().getShortTitle());
            if (result == 0)
            {
                result = o1.getTitle().compareTo(o2.getTitle());
            }

            return result;
        }
    };

    public UsmaCompetence()
    {
    }

    public UsmaCompetence(EppSkillGroup eppSkillGroup, String code, String title)
    {
        this.setEppSkillGroup(eppSkillGroup);
        this.setCode(code);
        this.setTitle(title);
    }
}