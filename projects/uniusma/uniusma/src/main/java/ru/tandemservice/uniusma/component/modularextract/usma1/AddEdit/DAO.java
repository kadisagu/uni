/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.commons.gradation.IAcadWeekendExtract;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<UsmaExcludeAfterAcadWeekendStuExtract, Model> implements IDAO
{
    @Override
    protected UsmaExcludeAfterAcadWeekendStuExtract createNewInstance()
    {
        return new UsmaExcludeAfterAcadWeekendStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    public static String getOrderLabel(String orderType, String orderNumber, Date orderDate, int parNumber)
    {
        return "«" + orderType + "»" +
                " №" + orderNumber +
                " от " + new SimpleDateFormat("dd.MM.yyyy").format(orderDate) + " г." +
                " [п." + parNumber + "]";
    }

    private static DQLSelectBuilder getDQLSelectBuilder(Model model)
    {
        // Нам нужны все приказы об академических отпусках по студенту
        // с указанием номера приказа, даты проведения приказа
        // и номера параграфа
        return new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ase")    // select from ABSTRACTSTUDENTEXTRACT_T as ase

                .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().fromAlias("ase"), "asep")   // inner join ABSTRACTSTUDENTPARAGRAPH_T as asep on ABSTRACTSTUDENTPARAGRAPH_T.ID = ABSTRACTSTUDENTEXTRACT_T.PARAGRAPH_ID
                .joinPath(DQLJoinType.inner, AbstractStudentParagraph.order().fromAlias("asep"), "aso") // inner join ABSTRACTSTUDENTORDER_T as aso on ABSTRACTSTUDENTORDER_T.ID = ABSTRACTSTUDENTPARAGRAPH_T.ORDER_ID

                .joinEntity("ase", DQLJoinType.inner, StudentExtractTypeToGroup.class, "settg", // inner join select from STUDENTEXTRACTTYPETOGROUP_T as settg
                        eq(property("ase", AbstractStudentExtract.type()), property("settg", StudentExtractTypeToGroup.type())))    // on STUDENTEXTRACTTYPETOGROUP_T.TYPE_ID = (ABSTRACTSTUDENTEXTRACT_T.TYPE_ID ->> STUDENTEXTRACTTYPE_T.ID)
                .where(eq(property("settg", StudentExtractTypeToGroup.group().code()), value(StudentExtractGroupCodes.WEEKEND)))    // where STUDENTEXTRACTGROUP_T.CODE_P = 6 (STUDENTEXTRACTGROUP_T.ID = STUDENTEXTRACTTYPETOGROUP_T.GROUP_ID)

                .where(eq(property("ase", AbstractStudentExtract.entity()), value(model.getExtract().getEntity()))) // where ABSTRACTSTUDENTEXTRACT_T.ENTITY_ID = студент
                .where(eq(property("ase", AbstractStudentExtract.committed()), value(Boolean.TRUE)))  // and where приказ_проведен

                .order(property("aso", AbstractStudentOrder.commitDateSystem()), OrderDirection.desc);  // order by ABSTRACTSTUDENTORDER_T.COMMITDATESYSTEM_P desc
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));

        final DQLSelectBuilder dql = getDQLSelectBuilder(model);
        final DQLSelectColumnNumerator numerator = new DQLSelectColumnNumerator(dql);

        final int extractId = numerator.column(property("ase.id")); // ABSTRACTSTUDENTEXTRACT_T.ID
        final int orderNumber = numerator.column(property("aso", AbstractStudentOrder.number())); // ABSTRACTSTUDENTORDER_T.NUMBER_P
        final int orderDate = numerator.column(property("aso", AbstractStudentOrder.commitDate()));  // ABSTRACTSTUDENTORDER_T.COMMITDATE_P

        if (model.isAddForm())
        {
            List<Object[]> rows = dql.createStatement(getSession()).list();
            if (rows.size() == 1)
            {
                model.setAcadWeekendOrderSelected((AbstractStudentExtract) get((Long) rows.get(0)[extractId]));
                model.getExtract().setAcadWeekendOrderNumber((String) rows.get(0)[orderNumber]);
                model.getExtract().setAcadWeekendOrderDate((Date) rows.get(0)[orderDate]);

                IAcadWeekendExtract extract = (IAcadWeekendExtract) get((Long) rows.get(0)[extractId]);
                model.getExtract().setBeginAcadWeekendDate(extract.getBeginAcadWeekendDate());
                model.getExtract().setEndAcadWeekendDate(extract.getEndAcadWeekendDate());
            }
        }

        if (model.isEditForm())
        {
            dql.where(eq(property("aso", AbstractStudentOrder.number()), value(model.getExtract().getAcadWeekendOrderNumber())));  // and where ABSTRACTSTUDENTORDER_T.NUMBER_P == номер_приказа
            dql.where(eq(property("aso", AbstractStudentOrder.commitDate()), valueTimestamp(model.getExtract().getAcadWeekendOrderDate())));  // and where ABSTRACTSTUDENTORDER_T.COMMITDATE_P == дата_приказа
            List<Object[]> rows = dql.createStatement(getSession()).list();
            if (rows.size() > 0)
                model.setAcadWeekendOrderSelected((AbstractStudentExtract) get((Long) rows.get(0)[extractId]));
        }

        model.setAcadWeekendOrderModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                final DQLSelectBuilder dql = getDQLSelectBuilder(model);
                if (null != filter && !filter.isEmpty())
                    dql.where(likeUpper(property("aso", AbstractStudentOrder.number()), value(CoreStringUtils.escapeLike(filter, true))));  // and where ABSTRACTSTUDENTORDER_T.NUMBER_P like вводимый_номер_приказа

                final DQLSelectColumnNumerator numerator = new DQLSelectColumnNumerator(dql);

                final int extractId = numerator.column(property("ase.id"));
                final int orderNumber = numerator.column(property("aso", AbstractStudentOrder.number()));
                final int orderDate = numerator.column(property("aso", AbstractStudentOrder.commitDate()));
                final int orderType = numerator.column(property("ase", AbstractStudentExtract.type().title()));
                final int orderParagraph = numerator.column(property("asep", AbstractStudentParagraph.number()));

                List<Object[]> rows = dql.createStatement(getSession()).list();
                List<DataWrapper> resultList = new ArrayList<>(rows.size());

                for (Object[] col : rows)
                {
                    resultList.add(new DataWrapper(
                            (Long) col[extractId],
                            getOrderLabel(
                                    (String) col[orderType],
                                    (String) col[orderNumber],
                                    (Date) col[orderDate],
                                    (Integer) col[orderParagraph]
                            )
                    ));
                }
                return new ListResult<>(resultList);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (value instanceof AbstractStudentExtract)
                {
                    AbstractStudentExtract ase = (AbstractStudentExtract) value;
                    return getOrderLabel(ase.getType().getTitle(), ase.getParagraph().getOrder().getNumber(), ase.getParagraph().getOrder().getCommitDate(), ase.getParagraph().getNumber());
                }
                return super.getLabelFor(value, columnIndex);
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if ((null != model.getExtract().getBeginAcadWeekendDate()) && (null != model.getExtract().getEndAcadWeekendDate()))
        {
            if (model.getExtract().getBeginAcadWeekendDate().after(model.getExtract().getEndAcadWeekendDate()))
            {
                errors.add("Дата начала академического отпуска не может быть больше даты его окончания.", "beginDate", "endDate");
            }
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        super.update(model);
    }
}