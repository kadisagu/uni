\fs28\qc\b{orderType}\b0\par
\par\fi709\qj\keep\keepn
Приказом ректора от {weekendOrderDate} г. № {weekendOrderNumber} {student_D} {fio_D} {birthDate} года рождения, {course} курса {orgUnit_G} {developForm_GF} формы обучения, предоставлен академический отпуск на срок с {weekendStartDate} г. до {weekendEndDate} г., по истечении указанного периода {student} к занятиям не {started}.\par
Учитывая вышеизложенные обстоятельства, во исполнение ч. 12 ст. 43, ст. 58 Федерального закона от 29.12.2012 г. № 273-ФЗ «Об образовании в РФ», в соответствии с Порядком применения к обучающимся и снятии с обучающихся мер дисциплинарного взыскания (утв. Приказом Министерства образования РФ от 15.03.2013 г. № 185), Правилами внутреннего распорядка обучающихся ГБОУ ВПО УГМУ Минздрава России,\par\par
\expnd8\expndtw40\b приказываю: \expndtw0\expnd0\b0\par\par
1. {Student_A} {fio} {birthDate} года рождения, {course} курса {orgUnit_G} (группа {group}) {developForm_GF} формы обучения отчислить как {notStarted_A} к занятиям после академического отпуска c {excludeDate}.\par
{stopPayments}\par
{giveDiploma}\par\par
Основание: {listBasics}
