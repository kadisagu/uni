/**
 *$Id$
 */
package ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.UsmaWorkGraphManager;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.EduPlansTab.UsmaWorkGraphEduPlansTab;
import ru.tandemservice.uniusma.base.bo.UsmaWorkGraph.ui.WeeksTab.UsmaWorkGraphWeeksTab;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
@Configuration
public class UsmaWorkGraphPub extends BusinessComponentManager
{
    public static final String WORK_GRAPH_TAB_PANEL = "workGraphTabPanel";
    public static final String WORK_GRAPH_TAB = "workGraphTab";
    public static final String WORK_GRAPH_WEEKS_TAB = "workGraphWeeksTab";
    public static final String WORK_GRAPH_EDU_PLANS_TAB = "workGraphEduPlansTab";

    public static final String DS_COURSE = "courseDS";
    public static final String DS_PROGRAM_SUBJECT = "programSubjectDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(DS_COURSE, UsmaWorkGraphManager.instance().courseDSHandler()))
                .addDataSource(selectDS(DS_PROGRAM_SUBJECT, UsmaWorkGraphManager.instance().programSubjectDSHandler()))
                .create();
    }

    @Bean
    public TabPanelExtPoint workGraphTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(WORK_GRAPH_TAB_PANEL)
                .addTab(htmlTab(WORK_GRAPH_TAB, UsmaWorkGraphPub.class.getPackage() + ".Tab"))
                .addTab(componentTab(WORK_GRAPH_WEEKS_TAB, UsmaWorkGraphWeeksTab.class).permissionKey("viewWeeksTab_UsmaWorkGraph"))
                .addTab(componentTab(WORK_GRAPH_EDU_PLANS_TAB, UsmaWorkGraphEduPlansTab.class).permissionKey("viewEduPlansTab_usmaWorkGraph"))
                .create();
    }
}