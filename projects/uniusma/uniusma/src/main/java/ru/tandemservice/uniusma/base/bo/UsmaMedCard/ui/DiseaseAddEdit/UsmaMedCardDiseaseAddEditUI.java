/*$Id$*/
package ru.tandemservice.uniusma.base.bo.UsmaMedCard.ui.DiseaseAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusma.entity.student.UsmaStudentDiseaseInfo;

import java.util.Date;

import static org.tandemframework.hibsupport.dao.CommonDAO.proxy;

/**
 * @author DMITRY KNYAZEV
 * @since 24.06.2014
 */
@State({
		@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentId", required = true),
		@Bind(key = "entityId", binding = "entityId")
       })
public class UsmaMedCardDiseaseAddEditUI extends UIPresenter
{
	private Long _entityId;
	private Long _studentId;
	private UsmaStudentDiseaseInfo _entity;

	@Override
	public void onComponentRefresh()
	{
		if(getEntity() == null)
		{
			if (isEditForm())
				setEntity(DataAccessServices.dao().<UsmaStudentDiseaseInfo>getNotNull(getEntityId()));
			else
			{
				Student student = proxy(getStudentId());
				UsmaStudentDiseaseInfo entity = new UsmaStudentDiseaseInfo();
				entity.setStudent(student);
				setEntity(entity);
			}
		}
	}

	public void onClickSave()
	{
		Date fromDate = getEntity().getBeginDate();
		Date toDate = getEntity().getEndDate();
		if (fromDate != null && toDate != null && fromDate.after(toDate))
    		throw new ApplicationException("Начальная дата должна быть меньше конечной.");

		DataAccessServices.dao().saveOrUpdate(getEntity());
		deactivate();
	}

	public void onClickCancel()
	{
		deactivate();
	}

	public Long getStudentId()
	{
		return _studentId;
	}

	public void setStudentId(Long studentId)
	{
		this._studentId = studentId;
	}

	public Long getEntityId()
	{
		return _entityId;
	}

	public void setEntityId(Long entityId)
	{
		this._entityId = entityId;
	}

	public boolean isEditForm()
	{
		return _entityId != null;
	}

	public UsmaStudentDiseaseInfo getEntity()
	{
		return _entity;
	}

	public void setEntity(UsmaStudentDiseaseInfo entity)
	{
		_entity = entity;
	}
}
