/*$Id$*/
package ru.tandemservice.uniusma.component.catalog.usmaVaccineKind.UsmaVaccineKindPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uniusma.entity.catalog.UsmaVaccineKind;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 21.06.2014
 */
public class DAO extends DefaultCatalogPubDAO<UsmaVaccineKind, Model> implements IDAO
{
	@Override
	protected void prepareListItemDataSource(Model model)
	{
		DQLSelectBuilder builder = new DQLSelectBuilder()
				.fromEntity(UsmaVaccineKind.class, "vk")
				.order(property("vk", UsmaVaccineKind.P_TITLE), OrderDirection.asc);
		applyFilters(model, builder);
		List<UsmaVaccineKind> result = builder.createStatement(getSession()).list();
		final DynamicListDataSource<UsmaVaccineKind> dataSource = model.getDataSource();
		dataSource.setTotalSize(result.size());
		dataSource.setCountRow(Math.max(4, 1+result.size()));
		dataSource.createPage(result);
	}

	protected void applyFilters(Model model, DQLSelectBuilder builder)
	{
		String title = model.getSettings().get("title");
		if (StringUtils.isNotBlank(title))
		{
			builder.where(likeUpper(property("vk", UsmaVaccineKind.P_TITLE), value(CoreStringUtils.escapeLike(title))));
		}
	}
}
