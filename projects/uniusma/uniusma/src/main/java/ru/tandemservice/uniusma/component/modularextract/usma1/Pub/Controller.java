/* $Id:$ */
package ru.tandemservice.uniusma.component.modularextract.usma1.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.uniusma.entity.studentmodularorder.UsmaExcludeAfterAcadWeekendStuExtract;

/**
 * @author Denis Perminov
 * @since 23.04.2014
 */
public class Controller extends ModularStudentExtractPubController<UsmaExcludeAfterAcadWeekendStuExtract, IDAO, Model>
{
}
