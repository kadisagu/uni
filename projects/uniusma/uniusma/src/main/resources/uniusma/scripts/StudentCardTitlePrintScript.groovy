/* $Id:$ */
package uniusma.scripts

import org.hibernate.Session
import org.tandemframework.core.entity.IEntity
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.runtime.ApplicationRuntime
import org.tandemframework.core.util.SafeSimpleDateFormat
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLExpressions as DQLExp
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager
import org.tandemframework.shared.person.base.entity.PersonEduInstitution
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.movestudent.entity.AbstractStudentExtract
import ru.tandemservice.movestudent.entity.AbstractStudentOrder
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract
import ru.tandemservice.uni.component.student.StudentPersonCard.IPrintStudentEnrData
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.catalog.UniScriptItem
import ru.tandemservice.uni.entity.employee.OrderData
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.dao.IEntrantDAO
import ru.tandemservice.uniec.entity.catalog.CompetitionKind
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.uniepp.UniEppUtils
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unisession.entity.document.SessionDocument
import ru.tandemservice.unisession.entity.mark.SessionMark
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.UsmaStudentCardAttachmentReportManager
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.IStudentCardAttachmentReportDao
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.StudentWpeActionContainer
import ru.tandemservice.uniusma.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.uniusma.entity.catalog.codes.StudentExtractTypeCodes
import ru.tandemservice.uniusma.entity.catalog.codes.UniScriptItemCodes

/**
 * @author rsizonenko
 * @since 11.12.2014
 */

return new StudentCardTitlePrint(                   // стандартные входные параметры скрипта
        session: session,                           // сессия
        template: template,                         // шаблон
        student: session.get(Student.class, object) // объект печати
).print()


class StudentCardTitlePrint {
    Session session
    byte[] template
    Student student

    final static IStudentCardAttachmentReportDao dao = UsmaStudentCardAttachmentReportManager.instance().dao();

    final RtfInjectModifier im = new RtfInjectModifier()
    final RtfTableModifier tm = new RtfTableModifier()

    def print() {

        //PRODUCT MARKS
        final OrgUnit orgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();
        im.put("formativeOrgUnit", orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle())
        im.put("eduLevel", (student.getEducationOrgUnit().getEducationLevelHighSchool().isSpeciality() ? "Специальность " : "Направление ") + student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle())
        im.put("sex", student.getPerson().getIdentityCard().getSex().shortTitle)
        im.put("citizenship", student.getPerson().getIdentityCard().getCitizenship()?.title)
        im.put("birthdate", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getBirthDate()))
        im.put("birthplace", student.getPerson().getIdentityCard().birthPlace)
        String education;
        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            final List<PersonEduInstitution> personEduInstitutionList = DataAccessServices.dao().getList(PersonEduInstitution.class, PersonEduInstitution.L_PERSON, student.getPerson(), PersonEduInstitution.P_ISSUANCE_DATE);
            education = formatEducation(personEduInstitutionList.get(0))
        } else
            education = student.eduDocument?.titleExtended
        im.put("education", education)
        final RtfInjectModifier enrolmentModifier = injectEnrollmentExtractData(student.getId())// Enrollment data
        final OrderData orderData = DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, student)
        im.put("oldOrgUnit", getOriginOrgUnit(orderData, student, session))
        im.put("martialStatus", student.person.familyStatus != null ? student.person.familyStatus.title : "")
        im.put("children", student.getPerson().getChildCount() == null ? "" : String.valueOf(student.getPerson().getChildCount()))
        im.put("addressRegistration", student.getPerson().getIdentityCard().getAddress()?.titleWithFlat)
        final String phones = student.person.contactData.phoneDefault;
        im.put("phones", phones);
        im.put("addressTitle", student.getPerson().getAddress()?.titleWithFlat + (phones != null ? ", " : ""))
        PersonNextOfKin father = null
        PersonNextOfKin mother = null
        final List<PersonNextOfKin> relationList = DataAccessServices.dao().getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, student.person)
        for (PersonNextOfKin relation : relationList) {
            if (relation.getRelationDegree().code.equals(RelationDegreeCodes.FATHER)) {
                father = relation
            } else if (relation.getRelationDegree().code.equals(RelationDegreeCodes.MOTHER)) {
                mother = relation
            }
        }
        appendNextOfKinData(im, father, "father");
        appendNextOfKinData(im, mother, "mother");
        im.put("contacts", student.person.contactData.allPhones);
        im.put("seria", student.getPerson().getIdentityCard().seria)
        im.put("number", student.getPerson().getIdentityCard().number)
        im.put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getIssuanceDate()))
        im.put("issuance", student.getPerson().getIdentityCard().getIssuancePlace())

        //STUDENT DATA
        im.put("bookNum", student.bookNumber)
        im.put("lastName", student.getPerson().getIdentityCard().lastName)
        im.put("firstName", student.getPerson().getIdentityCard().firstName)
        im.put("middleName", student.getPerson().getIdentityCard().middleName)
        im.put("addressTitle", student.getPerson().getAddress()?.titleWithFlat)
        im.put("targetFlag", student.targetAdmission ? "да" : "нет")
        im.put("targetOrganization", student.targetAdmissionOrgUnit?.title)
        injectEnrollmentExtractDataNew(student, im)

        //ORDERS
        tm.put("ORDER1", getExcludeOrders(student))
        tm.put("ORDER2", getRestorationOrders(student))
        tm.put("ORDER3", getWeekendOrders(student))
        tm.put("ORDER4", getWeekendOutOrders(student))
        tm.put("ORDER5", getChangeFioOrders(student))

        final List<StudentWpeActionContainer> wpeActionContainers = dao.getStudentWpeActions(student)
        //ATTACHMENTS
        im.put("EP_ATTACHMENTS", getAttachments(wpeActionContainers))

        //MARKS
        DisciplineMarkData disciplineMarkData = getDisciplineMarkData(wpeActionContainers)
        im.put("numOfDisciplines", int2String(disciplineMarkData.numOfDisciplines))
        im.put("numOf5", int2String(disciplineMarkData.numOf5))
        im.put("numOf4", int2String(disciplineMarkData.numOf4))
        im.put("numOf3", int2String(disciplineMarkData.numOf3))

        im.put("admOrderNum", "")//Print nothing see DEV-6670
        im.put("admOrderDate", "")//Print nothing see DEV-6670

        //EXAMS
        tm.put("GOSEXAMS", getExams(wpeActionContainers))//Государственные экзамены
        //VKR
        im.put("vkrTitle", student.finalQualifyingWorkTheme)//Дипломная работа на тему
        im.put("vkrMark", getVkrMark(wpeActionContainers))//Дипломная работа защищена с оценкой

        im.put("qualification", student.educationOrgUnit.educationLevelHighSchool.educationLevel.programSubjectTitleWithCode)

        injectGraduateDiplomaOrder(student, im)

        RtfDocument document = new RtfReader().read(template)
        im.modify(document)
        tm.modify(document)
        enrolmentModifier.modify(document)

        return [document: RtfUtil.toByteArray(document),
                fileName: "Титул ЛК студента ${student.fullFio}.rtf",
                rtf     : document]
    }

    private static void injectGraduateDiplomaOrder(final Student student, final RtfInjectModifier im) {
        String orderNum = ""
        String orderDay = ""
        String orderMonthStr = ""
        String orderYr = ""

        final OrderData order = getOrderData(student)
        if (order != null) {
            if (order.graduateDiplomaOrderDate != null && order.graduateDiplomaOrderNumber != null) {
                //Приказ о выпуске (диплом)
                orderNum = order.graduateDiplomaOrderNumber
                final Date date = order.graduateDiplomaOrderDate
                orderYr = getYear(date)
                orderMonthStr = getMonth(date)
                orderDay = getDay(date)
            } else if (order.graduateSuccessDiplomaOrderDate != null && order.graduateSuccessDiplomaOrderNumber != null) {
                //Приказ о выпуске (диплом с отличием)
                orderNum = order.graduateSuccessDiplomaOrderNumber
                final Date date = order.graduateSuccessDiplomaOrderDate
                orderYr = getYear(date)
                orderMonthStr = getMonth(date)
                orderDay = getDay(date)
            }
        }

        im.put("orderNum", orderNum)
        im.put("orderDay", orderDay)
        im.put("orderMonthStr", orderMonthStr)
        im.put("orderYr", orderYr)
    }

    private static void injectEnrollmentExtractDataNew(final Student student, final RtfInjectModifier im) {
        //ENROLMENT ORDER
        String enrOrderNum = ""
        String enrOrderDate = ""
        String enrCourse = ""

        String formativeOrgUnit_G = ""
        String speciality = student.educationOrgUnit.educationLevelHighSchool.educationLevel.programSubjectTitleWithCode

        String totalMark = ""
        String noExamsFlag = "нет"//без вступительных испытаний
        String benefitFlag = "нет"//вне конкурса

        String compensationType = ""

        IEntity extract = getMaxOrder(student)

        if (extract instanceof EnrollmentExtract) {//Новый абитуриент
            final EnrollmentExtract extractNew = extract
            final def order = extractNew.order
            enrOrderNum = order.number
            enrOrderDate = date2String(order.commitDate)
            enrCourse = extractNew.course.title

            formativeOrgUnit_G = extractNew.entity.educationOrgUnit.formativeOrgUnit.genitiveCaseTitle
            final Double sumMark = IEntrantDAO.instance.get().getSumFinalMarks(extractNew.entity.requestedEnrollmentDirection);
            totalMark = double2String(sumMark)

            final CompetitionKind competitionKind = extractNew.entity.requestedEnrollmentDirection.competitionKind
            switch (competitionKind.code) {
                case UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES:
                    noExamsFlag = "да"
                    break
                case UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION:
                    benefitFlag = "да"
            }
            compensationType = extractNew.entity.compensationType.title
        } else if (extract instanceof EnrEnrollmentExtract) {//Старый абитуриент
            final EnrEnrollmentExtract extract14 = extract
            final def order = extract14.order
            enrOrderNum = order.number
            enrOrderDate = date2String(order.commitDate)
            enrCourse = "1"//always 1

            formativeOrgUnit_G = extract14.entity.competition.programSetOrgUnit.formativeOrgUnit.genitiveCaseTitle
            final EnrRatingItem ratingItem = DataAccessServices.dao().get(EnrRatingItem.class, EnrRatingItem.L_REQUESTED_COMPETITION, extract14.getRequestedCompetition());
            final Double sumMark = ratingItem != null ? ratingItem.getTotalMarkAsDouble() : null;
            totalMark = double2String(sumMark)

            final EnrCompetitionType competitionType = extract14.entity.competition.type
            switch (competitionType.code) {
                case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
                case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
                    noExamsFlag = "да"; break
                case EnrCompetitionTypeCodes.EXCLUSIVE:
                    benefitFlag = "да"
            }
            compensationType = extract14.competition.type.compensationType.title
        } else if (extract instanceof EduEnrAsTransferStuExtract) {
            final EduEnrAsTransferStuExtract extractStud = extract
            final def order = extractStud.paragraph.order
            enrOrderNum = order.number
            enrOrderDate = date2String(order.commitDate)
            enrCourse = extractStud.courseNew.title

            formativeOrgUnit_G = extractStud.entity.educationOrgUnit.formativeOrgUnit.genitiveCaseTitle

            compensationType = extractStud.compensationTypeNew.title
        }

        im.put("enrOrderNum", enrOrderNum)
        im.put("enrOrderDate", enrOrderDate)
        im.put("enrCourse", enrCourse)

        im.put("formativeOrgUnit_G", formativeOrgUnit_G)
        im.put("speciality", speciality)
        im.put("totalMark", totalMark)

        im.put("noExamsFlag", noExamsFlag)
        im.put("benefitFlag", benefitFlag)

        im.put("compensationType", compensationType)
    }

    private static IEntity getMaxOrder(Student student) {
        final EduEnrAsTransferStuExtract extractStud = getEnrolmentStudentOrder(student)
        final EnrollmentExtract extractNew = getEnrolmentOrder(student)
        final EnrEnrollmentExtract extract14 = getEnrolmentOrder2014(student)
        final List<OrderWrapper> list = new ArrayList<>(3)
        if (extractStud != null) {
            list.add(new OrderWrapper(extractStud, extractStud.paragraph.order.commitDate))
        }
        if (extractNew != null) {
            list.add(new OrderWrapper(extractNew, extractNew.paragraph.order.commitDate))
        }
        if (extract14 != null) {
            list.add(new OrderWrapper(extract14, extract14.paragraph.order.commitDate))
        }
        if (list.isEmpty()) {
            return null;
        }
        list.sort().get(0).extract
    }

    private static class OrderWrapper implements Comparable<OrderWrapper> {
        private final IEntity extract;
        private final Date orderDate;

        OrderWrapper(IEntity extract, Date orderDate) {
            this.extract = extract
            this.orderDate = orderDate
        }

        @Override
        int compareTo(OrderWrapper o) {
            return o.orderDate.compareTo(this.orderDate)
        }
    }

    private static EnrEnrollmentExtract getEnrolmentOrder2014(Student student) {
        def alias = 'ex'
        def builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, alias)
                .top(1)
                .column(DQLExp.property(alias))
                .where(DQLExp.eq(DQLExp.property(alias, EnrEnrollmentExtract.student()), DQLExp.value(student)))
                .where(DQLExp.isNotNull(DQLExp.property(alias, EnrEnrollmentExtract.paragraph().order())))
                .order(DQLExp.property(alias, EnrEnrollmentExtract.paragraph().order().commitDate()), OrderDirection.desc)

        List<EnrEnrollmentExtract> extracts = dao.getList(builder)
        if (extracts.isEmpty())
            return null
        return extracts.get(0)
    }

    private static EnrollmentExtract getEnrolmentOrder(Student student) {
        def alias = 'ex'
        def builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, alias)
                .top(1)
                .column(DQLExp.property(alias))
                .where(DQLExp.eq(DQLExp.property(alias, EnrollmentExtract.studentNew()), DQLExp.value(student)))
                .where(DQLExp.isNotNull(DQLExp.property(alias, EnrEnrollmentExtract.paragraph().order())))
                .order(DQLExp.property(alias, EnrollmentExtract.paragraph().order().commitDate()), OrderDirection.desc)

        List<EnrollmentExtract> extracts = dao.getList(builder)
        if (extracts.isEmpty())
            return null
        return extracts.get(0)
    }

    private static EduEnrAsTransferStuExtract getEnrolmentStudentOrder(Student student) {
        def alias = 'ex'
        def builder = new DQLSelectBuilder().fromEntity(EduEnrAsTransferStuExtract.class, alias)
                .top(1)
                .column(DQLExp.property(alias))
                .where(DQLExp.eq(DQLExp.property(alias, EduEnrAsTransferStuExtract.entity()), DQLExp.value(student)))
                .where(DQLExp.isNotNull(DQLExp.property(alias, EduEnrAsTransferStuExtract.paragraph().order())))
                .order(DQLExp.property(alias, EduEnrAsTransferStuExtract.paragraph().order().commitDate()), OrderDirection.desc)
        List<EduEnrAsTransferStuExtract> extracts = dao.getList(builder)

        if (extracts.isEmpty())
            return null
        return extracts.get(0)
    }

    private static String[][] getExcludeOrders(Student student) {
        Collection<AbstractStudentExtract> orders = getStudExtractOrders(student, excludeStudentExtractCodes)
        return prepareOrders(orders)
    }

    private static String[][] getRestorationOrders(Student student) {
        Collection<AbstractStudentExtract> orders = getStudExtractOrders(student, restorationStudentExtractCodes)
        return prepareOrders(orders)
    }

    private static String[][] getWeekendOrders(Student student) {
        Collection<AbstractStudentExtract> orders = getStudExtractOrders(student, weekendStudentExtractCodes)
        return prepareOrders(orders)
    }

    private static String[][] getWeekendOutOrders(Student student) {
        Collection<AbstractStudentExtract> orders = getStudExtractOrders(student, weekendOutStudentExtractCodes)
        return prepareOrders(orders)
    }

    private static String[][] getChangeFioOrders(Student student) {
        Collection<AbstractStudentExtract> orders = getStudExtractOrders(student, changeFioExtractCodes)
        return prepareOrders(orders)
    }

    private static String[][] prepareOrders(Collection<AbstractStudentExtract> extracts) {
        List<List<String>> res = new ArrayList<>(extracts.size());
        for (AbstractStudentExtract extract : extracts) {
            AbstractStudentOrder order = extract.paragraph.order
            res.add([
                    date2String(order.commitDate),
                    order.number,
                    extract.comment
            ])
        }
        return res as String[][]
    }

    private
    static Collection<AbstractStudentExtract> getStudExtractOrders(Student student, Collection<String> studentExtractTypeCodes) {
        def alias = "ase"
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, alias)
                .column(DQLExp.property(alias))
                .where(DQLExp.eq(DQLExp.property(alias, AbstractStudentExtract.entity()), DQLExp.value(student)))
                .where(DQLExp.in(DQLExp.property(alias, AbstractStudentExtract.type().code()), studentExtractTypeCodes))
                .order(DQLExp.property(alias, AbstractStudentExtract.paragraph().order().commitDate()))

        return dao.getList(builder);
    }

    private final static List<String> excludeStudentExtractCodes = [
            StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DUE_TRANSFER_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STATE_EXAM_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_UNACCEPTED_TO_GP_DEFENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_GP_DEFENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER,
            StudentExtractTypeCodes.USMA_EXCLUDE_DEBTOR_MODULAR_ORDER,
            StudentExtractTypeCodes.USMA_EXCLUDE_NOT_OUT_WEEKEND_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DEATH_MODULAR_ORDER,
            StudentExtractTypeCodes.USMA_EXCLUDE_DRAFT_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_OWN_FREE_WILL_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_EDU_PLAN_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_NOT_PASSED_STATE_ATTESTATION_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_VARIANT_1_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STUDENT_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_GENERAL_DIPLOMA_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_CATHEDRAL_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_CATHEDRAL_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_DIP_DOCUMENT_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DIP_DOCUMENT_LIST_EXTRACT,
    ]

    private final static List<String> restorationStudentExtractCodes = [
            StudentExtractTypeCodes.RESTORATION_VARIANT_1_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_VARIANT_2_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_COURSE_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_ADMIT_REPEAT_STATE_EXAMS_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_ADMIT_ABSCENCE_STATE_EXAMS_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_ADMIT_TO_DIPLOMA_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_ADMIT_TO_DIPLOMA_DEFEND_ABSCENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.RESTORATION_ADMIT_TO_REPEAT_DIPLOMA_DEFEND_MODULAR_ORDER
    ]
    private final static List<String> weekendStudentExtractCodes = [
            StudentExtractTypeCodes.WEEKEND_VARIANT_1_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_PREGNANCY_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_CHILD_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKENT_VARIANT_2_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_CHILD_THREE_MODULAR_ORDER

    ]
    private final static List<String> weekendOutStudentExtractCodes = [
            StudentExtractTypeCodes.WEEKEND_OUT_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_CHILD_OUT_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_PREGNANCY_OUT_MODULAR_ORDER,
            StudentExtractTypeCodes.WEEKEND_CHILD_OUT_VARIANT_2_MODULAR_ORDER
    ]
    private final static List<String> changeFioExtractCodes = [
            StudentExtractTypeCodes.CHANGE_FIO_MODULAR_ORDER,
    ]

    private static String[][] getExams(final Collection<StudentWpeActionContainer> wpeActionContainers) {
        if (wpeActionContainers == null || wpeActionContainers.isEmpty())
            return Collections.emptyList()

        List<List<String>> result = new ArrayList<>()

        for (StudentWpeActionContainer container : wpeActionContainers) {
            final Map<Long, SessionMark> sessionMarkMap = container.sessionMarkMap
            for (EppStudentWpeCAction wpeCAction : container.getAllWpe()) {
                final EppRegistryStructure registryStructure = wpeCAction.studentWpe.registryElementPart.registryElement.parent
                if (isGosExam(registryStructure)) {
                    final SessionMark sessionMark = sessionMarkMap.getOrDefault(wpeCAction.id, null)
                    String titleStr = wpeCAction.studentWpe.registryElementPart.titleWithNumber
                    String markStr = sessionMark == null ? "" : sessionMark.valueItem?.printTitle
                    String markDateStr = sessionMark == null ? "" : date2String(sessionMark.performDate)
                    result.add([
                            titleStr,
                            markStr,
                            markDateStr
                    ])
                }
            }
        }

        return result as String[][];
    }

    private static String getVkrMark(final Collection<StudentWpeActionContainer> wpeActionContainers) {
        if (wpeActionContainers == null || wpeActionContainers.isEmpty())
            return ""
        for (StudentWpeActionContainer container : wpeActionContainers) {
            final Map<Long, SessionMark> sessionMarkMap = container.sessionMarkMap
            for (EppStudentWpeCAction wpeCAction : container.getAllWpe()) {
                final EppRegistryStructure registryStructure = wpeCAction.studentWpe.registryElementPart.registryElement.parent
                if (isVkr(registryStructure)) {
                    final SessionMark sessionMark = sessionMarkMap.getOrDefault(wpeCAction.id, null)
                    if (sessionMark != null || sessionMark.valueItem != null)
                        return sessionMark.valueItem.printTitle
                }
            }
        }
        return ""
    }

    //Государственный экзамен
    private static isGosExam(EppRegistryStructure registryStructure) {
        return EppRegistryStructureCodes.REGISTRY_ATTESTATION_EXAM.equals(registryStructure.code)
    }

    //Выпускная квалификационная работа
    private static isVkr(EppRegistryStructure registryStructure) {
        return EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA.equals(registryStructure.code)
    }

    private static List<IRtfElement> getAttachments(Collection<StudentWpeActionContainer> wpeActionContainers) {
        if (wpeActionContainers == null || wpeActionContainers.isEmpty())
            return Collections.emptyList();

        final List<IRtfElement> result = new ArrayList<>();
        final UniScriptItem script = dao.getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_ATTACHMENT_PRINT_SCRIPT);

        final Iterator<StudentWpeActionContainer> iterator = wpeActionContainers.iterator();
        while (iterator.hasNext()) {
            final StudentWpeActionContainer data = iterator.next();
            final RtfDocument resultDoc = getAttachmentPart(data, script.getCurrentTemplate())
            result.addAll(resultDoc.elementList)
        }
        return result
    }

    private static RtfDocument getAttachmentPart(final StudentWpeActionContainer data, byte[] template) {
        final RtfInjectModifier imAttach = new RtfInjectModifier()
        final RtfTableModifier tmAttach = new RtfTableModifier()

        final Student student = data.student
        final EppYearEducationProcess eduYear = data.eduYear
        final Course course = data.course

        imAttach.put('course', numberToString(course.intValue))
        imAttach.put('eduYear', eduYear.getTitle())
        imAttach.put('firstTerm', numberToString(data.firstTerm))
        imAttach.put('secondTerm', numberToString(data.secondTerm))

        imAttach.put('nextCourse', numberToString(course.intValue + 1))
        imAttach.put('bookNum', student.bookNumber)

        final AbstractStudentOrder order = dao.getStudentTransferOrder(student, course.intValue, course.intValue + 1)
        imAttach.put('transferOrderNumber', order == null ? "-" : order.number)
        imAttach.put('transferOrderDate', order == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.commitDate))

        tmAttach.put('T1', getAttachmentTable(data.getFirstTermWpeSorted(getComparatorGroupTypeFCA()), data.sessionMarkMap, data.getSessionDocumentMap()))
        tmAttach.put('T2', getAttachmentTable(data.getSecondTermWpeSorted(getComparatorGroupTypeFCA()), data.sessionMarkMap, data.getSessionDocumentMap()))

        final RtfDocument document = new RtfReader().read(template)
        tmAttach.modify(document)
        imAttach.modify(document)

        return document
    }

    private static String[][] getAttachmentTable(
            Collection<EppStudentWpeCAction> wpeActionContainers,
            Map<Long, SessionMark> sessionMarkMap,
            Map<Long, SessionDocument> sessionDocumentMap
    ) {
        final List<List<String>> res = new ArrayList<>(wpeActionContainers.size())

        for (final EppStudentWpeCAction wpeAction : wpeActionContainers) {
            final EppRegistryElementPart registryElementPart = wpeAction.studentWpe.registryElementPart
            final EppRegistryElement registryElement = registryElementPart.registryElement
            final SessionMark sessionMark = sessionMarkMap.getOrDefault(wpeAction.id, null)
            final SessionDocument sessionDocument = sessionDocumentMap.getOrDefault(wpeAction.id, null)

            final String titleStr = registryElementPart.registryElement.title
            final String loadStr = UniEppUtils.formatLoad(registryElement.sizeAsDouble, false)
            final String zetStr = UniEppUtils.formatLoad(registryElement.laborAsDouble, false)
            final String examMarkStr
            final String offsetMarkStr
            final Date modificationDate
            if (sessionMark == null) {
                examMarkStr = ""
                offsetMarkStr = ""
                modificationDate = null;
            } else {
                examMarkStr = sessionMark == null ? "" : getExamMark(sessionMark.valueItem)
                offsetMarkStr = sessionMark == null ? "" : getOffsetMark(sessionMark.valueItem)
                modificationDate = sessionMark.modificationDate;
            }

            final String examListDateStr = sessionDocument == null ? "" : sessionDocument.getTypeTitle() + " " + date2String(modificationDate)
            res.add(
                    [titleStr,          //1 Наименование предмета
                     loadStr,           //2 Число часов по плану
                     zetStr,            //3 ЗЕТ (трудоёмкость)
                     examMarkStr,       //4 Экзаменационная оценка
                     offsetMarkStr,     //5 Отметка о зачете
                     examListDateStr]   //6 Дата и № экзам. листа
            )
        }

        return res as String[][];
    }

    private static String getExamMark(SessionMarkCatalogItem catalogItem) {
        if (catalogItem == null) return ""
        switch (catalogItem.code) {
            case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
            case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
            case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
            case SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO:
                return catalogItem.printTitle
            default:
                return ""
        }
    }

    private static String getOffsetMark(SessionMarkCatalogItem catalogItem) {
        if (catalogItem == null) return ""
        switch (catalogItem.code) {
            case SessionMarkGradeValueCatalogItemCodes.ZACHTENO:
            case SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO:
                return catalogItem.printTitle
            default:
                return ""
        }
    }


    private static DisciplineMarkData getDisciplineMarkData(
            final Collection<StudentWpeActionContainer> wpeActionContainers) {
        int numOfDisciplines = 0
        int numOf5 = 0
        int numOf4 = 0
        int numOf3 = 0

        for (StudentWpeActionContainer data : wpeActionContainers) {
            final DisciplineMarkData disciplineMarkDataMarkData = getDisciplineMarkData4Term(data.allWpe, data.sessionMarkMap)
            numOfDisciplines += disciplineMarkDataMarkData.numOfDisciplines
            numOf5 += disciplineMarkDataMarkData.numOf5
            numOf4 += disciplineMarkDataMarkData.numOf4
            numOf3 += disciplineMarkDataMarkData.numOf3
        }
        return new DisciplineMarkData(numOfDisciplines, numOf5, numOf4, numOf3)
    }

    private static DisciplineMarkData getDisciplineMarkData4Term(
            final Collection<EppStudentWpeCAction> actions,
            final Map<Long, SessionMark> sessionMarkMap
    ) {
        int numOf5 = 0
        int numOf4 = 0
        int numOf3 = 0
        for (EppStudentWpeCAction wpeCAction : actions) {
            final SessionMark mark = sessionMarkMap.getOrDefault(wpeCAction.id, null)
            if (mark == null || mark.cachedMarkValue == null)
                continue
            final String markCode = mark.cachedMarkValue.code

            switch (markCode) {
                case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
                    numOf5 += 1
                    break
                case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
                    numOf4 += 1
                    break
                case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
                    numOf3 += 1
                    break
            }
        }
        return new DisciplineMarkData(actions.size(), numOf5, numOf4, numOf3)
    }

    private static class DisciplineMarkData {
        private final int numOfDisciplines
        private final int numOf5
        private final int numOf4
        private final int numOf3

        DisciplineMarkData(int numOfDisciplines, int numOf5, int numOf4, int numOf3) {
            this.numOfDisciplines = numOfDisciplines
            this.numOf5 = numOf5
            this.numOf4 = numOf4
            this.numOf3 = numOf3
        }
    }

    private static String numberToString(Integer num) {
        if (NUM_TO_WORD.length < num)
            throw new ApplicationException("Невозможно перевести число \"{$num}\" в строку");
        return NUM_TO_WORD[num - 1]
    }

    private static OrderData getOrderData(Student student) {
        OrderData orderData = dao.getUnique(OrderData.class, OrderData.student().s(), student)
        return orderData
    }

    private final static String[] NUM_TO_WORD = [
            "Первый", "Второй", "Третий", "Четвёртый", "Пятый", "Шестой", "Седьмой", "Восьмой", "Девятый", "Десятый",
            "Одиннадцатый", "Двенадцатый", "Тринадцатый", "Четырнадцатый", "Пятнадцатый", "Шестнадцатый", "Семнадцатый",
            "Восемнадцатый", "Девятнадцатый", "Двадцатый"
    ];

    private static String date2String(final Date date) {
        return date == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(date)
    }

    private static String double2String(final Double value) {
        return value == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(value)
    }

    private static String int2String(final int value) {
        return String.valueOf(value)
    }

    private static String getDay(final Date date) {
        if (date == null)
            return ""
        return new SafeSimpleDateFormat("dd", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(date);
    }

    private static String getMonth(final Date date) {
        if (date == null)
            return ""
        return new SafeSimpleDateFormat("MMMMM", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(date);
    }

    private static String getYear(final Date date) {
        if (date == null)
            return ""
        return new SafeSimpleDateFormat("yy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(date);
    }

    private static String formatEducation(PersonEduInstitution personEduInstitution) {
        StringBuilder result = new StringBuilder();
        if (personEduInstitution.getEduInstitutionKind() != null)
            result.append(personEduInstitution.getEduInstitutionKind().getShortTitle()).append(" ");
        if (personEduInstitution.getEduInstitution() != null)
            result.append(" ").append(personEduInstitution.getEduInstitution().getTitle());
        result.append(", ");
        if (personEduInstitution.getAddressItem() != null)
            result.append(personEduInstitution.getAddressItem().getTitleWithType()).append(", ");
        result.append(personEduInstitution.getDocumentType().getTitle());
        if (personEduInstitution.getSeria() != null)
            result.append(": ").append(personEduInstitution.getSeria());
        if (personEduInstitution.getNumber() != null)
            result.append(" №").append(personEduInstitution.getNumber());
        result.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }

    private static String getOriginOrgUnit(OrderData orderData, Student student, Session session) {
        if (orderData == null) return "";
        EduEnrAsTransferStuExtract extract = new DQLSelectBuilder().fromEntity(EduEnrAsTransferStuExtract.class, "e")
                .column(DQLExp.property("e"))
                .where(DQLExp.eqValue(DQLExp.property("e", EduEnrAsTransferStuExtract.committed()), true))
                .where(DQLExp.eqValue(DQLExp.property("e", EduEnrAsTransferStuExtract.entity()), student))
                .where(DQLExp.eqValue(DQLExp.property("e", EduEnrAsTransferStuExtract.paragraph().order().commitDate()), orderData.eduEnrollmentOrderDate))
                .where(DQLExp.eqValue(DQLExp.property("e", EduEnrAsTransferStuExtract.paragraph().order().number()), orderData.eduEnrollmentOrderNumber))
                .createStatement(session).uniqueResult()

        return extract != null ? extract.university : "";
    }

    private static void appendNextOfKinData(RtfInjectModifier modifier, PersonNextOfKin relative, String prefix) {
        if (relative == null) {
            modifier.put(prefix + "Fio", "")
            modifier.put(prefix + "Birthdate", "")
            modifier.put(prefix + "Workplace", "")
            modifier.put(prefix + "Address", "");
            return;
        }

        modifier.put(prefix + "Fio", relative.birthDate != null ? relative.fio : relative.fio + ",");
        modifier.put(prefix + "Birthdate", relative.birthDate != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(relative.birthDate) : "");
        modifier.put(prefix + "Workplace", relative.getEmploymentPlace() != null ? relative.getEmploymentPlace() : "");
        modifier.put(prefix + "Address", relative.address != null ? relative.address.title : "");
    }

    private static RtfInjectModifier injectEnrollmentExtractData(Long studentId) {
        RtfInjectModifier modifier = null;
        // Студент может быть зачислен или старым модулем или новым (есть клиенты, у которых оба модуля подключены)
        if (ApplicationRuntime.containsBean(IPrintStudentEnrData.UNIENR14_SPRING_BEAN_NAME))
            modifier = ApplicationRuntime.getBean(IPrintStudentEnrData.UNIENR14_SPRING_BEAN_NAME, IPrintStudentEnrData.class).getEnrPrintDataModifier(studentId);

        if (modifier == null && ApplicationRuntime.containsBean(IPrintStudentEnrData.UNIEC_SPRING_BEAN_NAME))
            modifier = ApplicationRuntime.getBean(IPrintStudentEnrData.UNIEC_SPRING_BEAN_NAME, IPrintStudentEnrData.class).getEnrPrintDataModifier(studentId);

        if (modifier == null) {
            // Возможно, лучше будет передавать модифаер в бины и там затирать метки...
            modifier = new RtfInjectModifier()
                    .put("orderNumber", "")
                    .put("orderDate", "")
                    .put("course", "")
                    .put("ball", "");
        }
        modifier;
    }


    private static final Map<String, Integer> groupTypeFCAPriorityMap = getGroupTypeFCAPriority()

    private static Comparator<EppStudentWpeCAction> getComparatorGroupTypeFCA() {

        new Comparator<EppStudentWpeCAction>() {
            @Override
            int compare(EppStudentWpeCAction o1, EppStudentWpeCAction o2) {
                Integer priority1 = groupTypeFCAPriorityMap.get(o1.type.code)
                Integer priority2 = groupTypeFCAPriorityMap.get(o2.type.code)
                return Integer.compare(priority1, priority2)
            }
        }
    }

    // eppGroupTypeFCA.code -> eppFControlActionType.priority
    private static Map<String, Integer> getGroupTypeFCAPriority() {
        final String alias = 'cat'
        final List<EppFControlActionType> list = dao.getList(
                new DQLSelectBuilder().fromEntity(EppFControlActionType.class, alias)
                        .column(DQLExpressions.property(alias))
        )

        final Map<String, Integer> result = list.collectEntries { [it.eppGroupType.code, it.totalMarkPriority] }
        return result
    }
}