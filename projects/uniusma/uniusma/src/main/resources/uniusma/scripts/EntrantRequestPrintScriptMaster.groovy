package uniusma.scripts

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.YesNoFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintMaster(                                // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author oleyba
 * @since 5/13/13
 */
class EntrantRequestPrintMaster {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() // метки, строки с которыми необходимо удалить из таблицы

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()}=${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillCompetitionParallel(directions)
        fillCompetitionNoParallel(directions)
        fillInternalExams(directions)
        fillInjectParameters()
        fillNextOfKin()

        def fillAcceptedContract = fillAcceptedContract()

        RtfDocument document = new RtfReader().read(template)
        if (fillAcceptedContract) {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE))
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))

            byte[] template = IUniBaseDao.instance.get().getCatalogItem(ScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT).getCurrentTemplate()
            document.getElementList().addAll(new RtfReader().read(template).getElementList())
        }

        im.modify(document)
        tm.modify(document)
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.empty) {
            Set<String> titles = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.title }
            im.put("internalExams", StringUtils.join(titles, ", "))
        } else
            deleteLabels.add("internalExams")
    }

    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def parallelCompetitions = enrRequestedCompetitions.findAll() { e -> e.parallel }
        if (!parallelCompetitions.empty) {
            def dirMap = getDirMap(enrRequestedCompetitions)
            def rows = Lists.newArrayList()
            for (EnrRequestedCompetition requestedCompetition : parallelCompetitions) {
                // формируем строку из пяти столбцов
                def programSet = requestedCompetition.competition.programSetOrgUnit.programSet
                def subject = programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
//  продуктовая логика
//                def programSubject = subject.titleWithCode + ((specs != null && !specs.empty) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
//  проектная логика
                def programSubject = subject.subjectCode + " " + programSet.title + ((specs != null && !specs.empty) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

                def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

                rows.add([
                        String.valueOf(requestedCompetition.priority),
                        programSubject,
                        requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                        getPlaces(requestedCompetition.competition.type),
                        EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle] as String[])
            }
            tm.put("T2", rows as String[][])
        } else
            tm.remove("T2")
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        // пока нет признака параллельности считаем, что все не параллельные
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)
        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort { e -> e.priority }
        for (def requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            def programSet = requestedCompetition.competition.programSetOrgUnit.programSet
            def subject = programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
//  продуктовая логика
//                def programSubject = subject.titleWithCode + ((specs != null && !specs.empty) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
//  проектная логика
            def programSubject = subject.subjectCode + " " + programSet.title + ((specs != null && !specs.empty) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

            def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

            rows.add([
                    String.valueOf(requestedCompetition.priority),
                    programSubject,
                    requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                    getPlaces(requestedCompetition.competition.type),
                    EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle] as String[])
        }
        tm.put("T1", rows as String[][])
    }

    static def getPlaces(EnrCompetitionType type) {
        if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "по договорам об оказании платных образовательных услуг"
        else if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) ||
                EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code))
            return "финансируемые из федерального бюджета"
        else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    boolean fillAcceptedContract() {
        def acceptedContractCompetitions = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "rc")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().type().fromAlias("rc"), "ct")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().eduLevelRequirement().fromAlias("rc"), "elr")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().fromAlias("rc"), "ps")
                .column("rc")
                .where(eq(property("rc", EnrRequestedCompetition.request()), value(entrantRequest)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
                .order(property("rc", EnrRequestedCompetition.priority()))
                .createStatement(session).<EnrRequestedCompetition> list()

        if (acceptedContractCompetitions.empty)
            return false
        else {
            def dirMap = getDirMap(acceptedContractCompetitions)

            def rows = Lists.newArrayList()
            for (EnrRequestedCompetition requestedCompetition : acceptedContractCompetitions) {
                def programSet = requestedCompetition.competition.programSetOrgUnit.programSet
                def subject = programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
//  продуктовая логика
//                def programSubject = subject.titleWithCode + ((specs != null && !specs.empty) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
//  проектная логика
                def programSubject = subject.subjectCode +  " " + programSet.title + ((specs != null && !specs.empty) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

                def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

                rows.add([
                        requestedCompetition.getPriority(),
                        programSubject,
                        requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                        getPlaces(requestedCompetition.competition.type),
                        EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle,
                        YesNoFormatter.INSTANCE.format(requestedCompetition.isAcceptedContract())
                ] as String[])
            }
            tm.put("T6", rows as String[][])
            return true
        }
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "ch")
        builder.where(DQLExpressions.in(DQLExpressions.property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))

        def programs = builder.createStatement(session).<EnrRequestedProgram> list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id
            if (!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if (!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet())
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization)
                e.getValue().clear()

        return dirMap
    }

    void fillInjectParameters() {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def sex = card.sex
        def personAddress = person.address

        def headers = getHeaderEmployeePostList(TopOrgUnit.instance)
        IdentityCard headCard = (headers != null && !headers.empty) ? headers.get(0).person.identityCard : null

        im.put("regNumber", entrantRequest.stringNumber)
        im.put("highSchoolTitleShort", TopOrgUnit.instance.shortTitle)

        StringBuilder headIof = new StringBuilder();
        if (headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName))
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".")
            if (StringUtils.isNotEmpty(headCard.middleName))
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".")
            headIof.append(" ").append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }

        im.put("rector_G", headIof.toString())
        im.put("FIO", card.fullFio)
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("birthPlace", card.birthPlace)
        im.put("sex", sex.title)
        im.put("citizenship", card.citizenship.fullTitle)
        im.put("identityCardTitle", card.shortTitle)
        im.put("identityCardPlaceAndDate", [card.issuancePlace, card.issuanceDate?.format("dd.MM.yyyy")].grep().join(", "))
        im.put("adressTitleWithFlat", personAddress != null ? personAddress.titleWithFlat : "")
        im.put("adressPhonesTitle", person.contactData.mainPhones)
        im.put("email", person.contactData.email)
        im.put("age", card.age as String)

        im.put("education", entrantRequest.eduDocument.eduLevel?.title)
        im.put("certificate", entrantRequest.eduDocument.title)

        String foreignLanguage = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "pf")
                .column(property("pf", PersonForeignLanguage.language().title()))
                .where(eq(property("pf", PersonForeignLanguage.person()), value(person)))
                .where(eq(property("pf", PersonForeignLanguage.main()), value(Boolean.TRUE)))
                .createStatement(session).uniqueResult()

        im.put("foreignLanguages", StringUtils.trimToEmpty(foreignLanguage))
        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("additionalInfo", entrant.additionalInfo)

        im.put("wayOfProviding", entrantRequest.getOriginalSubmissionWay().getTitle())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))


        im.put("surName", PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put("name", PersonManager.instance().declinationDao().getDeclinationLastName(card.firstName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put("secondName", PersonManager.instance().declinationDao().getDeclinationLastName(card.middleName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put("idPlaceCode", StringUtils.trimToEmpty(card.issuanceCode))
        im.put("addressFact", StringUtils.trimToEmpty(entrantRequest.entrant.person.address?.titleWithFlat))

        im.put("idTitle", card.cardType.title)
        im.put("idSeria", StringUtils.trimToEmpty(card.seria))
        im.put("idNumber", StringUtils.trimToEmpty(card.number))
    }

    def fillNextOfKin() {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put("father", father)
        else
            im.put("father", "")

        if (mother != null)
            im.put("mother", mother)
        else
            im.put("mother", "")

        if (tutor != null)
            im.put("tutor", tutor)
        else
            deleteLabels.add("tutor")
    }

    def getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null;
    }

    def getHeaderEmployeePostList(OrgUnit orgUnit) {
        DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${orgUnit.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
                /).<EmployeePost> list()
    }
}
