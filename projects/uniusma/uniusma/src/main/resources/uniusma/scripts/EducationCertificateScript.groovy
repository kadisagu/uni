package uniusma.scripts

import org.apache.commons.collections.CollectionUtils
import org.hibernate.Session
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.entity.IEntity
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dao.ICommonDAO
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.movestudent.entity.AbstractStudentExtract
import ru.tandemservice.movestudent.entity.AbstractStudentOrder
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract
import ru.tandemservice.uni.entity.employee.OrderData
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.entity.orgstruct.AcademyRename
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEdit
import ru.tandemservice.unidip.base.entity.diploma.*
import ru.tandemservice.unidip.settings.entity.DipAssistantManager
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.uniusma.entity.catalog.codes.StudentExtractTypeCodes

import static ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes.*

/**
 * @author Andrey Avetisov
 * @since 02.10.2014
 */

return new EducationCertificatePrint(                                       // стандартные входные параметры скрипта
        session: session,                                                   // сессия
        template: template,                                                 // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   // Диплом
        diplomaIssuance: diplomaIssuance,                                   // Факт выдачи диплома
).print()

class EducationCertificatePrint {
    private static final ICommonDAO dao = DataAccessServices.dao()

    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaIssuance diplomaIssuance


    def print() {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()
        RtfDocument document = new RtfReader().read(template);


        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()

        def registrationNumber = diplomaIssuance != null ? diplomaIssuance.registrationNumber : ""
        def issuanceDate = diplomaIssuance != null ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate) + " года" : ""

        def student = diplomaObject.student
        def lastName = student.person.identityCard.lastName
        def firstName = student.person.identityCard.firstName
        def middleName = student.person.identityCard.middleName
        def birthDate = student.person.identityCard.birthDate != null ?
                RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(student.person.identityCard.birthDate) + " года" : "";
        def eduDocumentKind = student.eduDocument;
        def eduDocumentKindTitle = ""
        def yearEnd = ""
        if (eduDocumentKind != null) {
            eduDocumentKindTitle = eduDocumentKind.documentKindTitle.toLowerCase()
            if (!eduDocumentKind.eduOrganizationAddressItem.country.title.equals("Россия")) {
                eduDocumentKindTitle += ", " + eduDocumentKind.eduOrganizationAddressItem.country.title;
            }
            yearEnd = String.valueOf(eduDocumentKind.yearEnd) + " год"
        }

        def beginTraining = ""

        OrderData orderData = DataAccessServices.dao().get(OrderData.class, OrderData.student(), student);

        if (orderData != null && orderData.eduEnrollmentOrderEnrDate != null) {
            AcademyRename academyRename = new DQLSelectBuilder()
                    .fromEntity(AcademyRename.class, "r").column("r").top(1)
                    .where(DQLExpressions.betweenDays(AcademyRename.date().fromAlias("r"), orderData.eduEnrollmentOrderEnrDate, CoreDateUtils.getYearFirstTimeMoment(3000)))
                    .order(DQLExpressions.property("r", AcademyRename.P_DATE)).createStatement(session).uniqueResult()

            if (academyRename != null) {
                beginTraining = RussianDateFormatUtils.getYearString(orderData.eduEnrollmentOrderEnrDate, false) + " году в " + academyRename.previousFullTitle;
            } else {
                beginTraining = RussianDateFormatUtils.getYearString(orderData.eduEnrollmentOrderEnrDate, false) + " году в " + topOrgUnit.nominativeCaseTitle;
            }
        }

        def completedTraining

        DipAdditionalInformation dipAdditionalInformation = DataAccessServices.dao().get(DipAdditionalInformation.class, DipAdditionalInformation.L_DIPLOMA_OBJECT, diplomaObject)
        if (dipAdditionalInformation != null && dipAdditionalInformation.demand) {
            completedTraining = "продолжает обучение в " + topOrgUnit.getPrepositionalCaseTitle()
        } else {
            completedTraining = diplomaIssuance != null ? (RussianDateFormatUtils.getYearString(diplomaIssuance.issuanceDate, false) + " году в ") : ""
            completedTraining += topOrgUnit.getPrepositionalCaseTitle()
        }

        def programKind = ""
        def programSubjectTitle = ""
        final EduProgramSubject programSubject = student.educationOrgUnit.educationLevelHighSchool.subjectQualification.programSubject
        if (programSubject != null) {
            if (programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA)
                    || programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV)) {
                programKind = "бакалавриата/специалитета"
            } else if (programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_)
                    || programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA)) {
                programKind = "среднего профессионального образования"
            } else if (programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY)) {
                programKind = "магистратуры"
            }
            programSubjectTitle = programSubject.getTitleWithCodeOksoWithoutSpec()
        }

        def developPeriod = DipDocumentUtils.getDevelopPeriod(diplomaObject)
        def programQualification = DipDocumentUtils.getEduCertificateQualificationTitle(diplomaObject)

        List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList = DataAccessServices.dao().getList(DiplomaAcademyRenameData.class,
                DiplomaAcademyRenameData.L_DIPLOMA_CONTENT, diplomaObject.getContent(), DiplomaAcademyRenameData.academyRename().date().s())
        def academyRenameList = getAcademyRename(diplomaAcademyRenameDataList)
        def information = getEducationCertificateAdditionalInformation(dipAdditionalInformation, diplomaObject)
        def slash = ""
        def fioRector = ""
        DipAssistantManager dipAssistantManager = new DQLSelectBuilder().fromEntity(DipAssistantManager.class, "e")
                .createStatement(session).<DipAssistantManager> uniqueResult();
        if (dipAssistantManager != null) {
            slash = "/"
            fioRector = dipAssistantManager.employeePost.fio
        } else if (topOrgUnit.head != null) {
            fioRector = topOrgUnit.head.employee.person.identityCard.fio
        }

        injectEnrollmentExtractDataNew(student, im)
        injectStudentExtractOrders(student, im)


        List<DiplomaContentRow> diplomaContentRowList = DataAccessServices.dao().getList(DiplomaContentRow.class,
                DiplomaContentRow.owner(), diplomaObject.content, DiplomaContentRow.P_NUMBER)

        //var1 - EduPlan HighSchool 3d generation, var2 - other eduPlan //NOTE
        ArrayList<String[]> disciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> disciplineRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> stateExamRows = new ArrayList<String[]>()
        ArrayList<String[]> graduateWorkRows = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar2 = new ArrayList<String[]>()
        double allStateExamLaborLoad = 0
        double allStateExamWeeksLoad = 0
        double allPracticeLaborLoad = 0
        double allPracticeWeeksLoad = 0
        double audLoad = 0

        ArrayList<List<String>> courseTableVar1 = new ArrayList<>();
        ArrayList<List<String>> courseTableVar2 = new ArrayList<>();
        ArrayList<List<String>> practiceTableVar1 = new ArrayList<>();
        ArrayList<List<String>> practiceTableVar2 = new ArrayList<>();

        for (DiplomaContentRow contentRow : diplomaContentRowList) {
            double weeksAsDouble = contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
            if (weeksAsDouble == 0d)
                weeksAsDouble = contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;

            double loadAsDouble = contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0;
            if (loadAsDouble == 0d)
                loadAsDouble = contentRow.loadAsDouble != null ? contentRow.audLoadAsDouble : 0;

            if (contentRow instanceof DiplomaDisciplineRow) {
                disciplineRowsVar1.add([
                        contentRow.title,
                        double2String(contentRow.laborAsDouble) + " з.е.",
                        double2String(loadAsDouble),//NOTE
                        contentRow.mark
                ] as String[])
                disciplineRowsVar2.add([
                        contentRow.title,
                        double2String(loadAsDouble) + " час.",
                        double2String(loadAsDouble),//NOTE
                        contentRow.mark
                ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaPracticeRow) {
                allPracticeLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allPracticeWeeksLoad += weeksAsDouble;
                practiceRowsVar1.add([
                        contentRow.title,
                        double2String(contentRow.laborAsDouble) + " з.е.",
                        double2String(weeksAsDouble),//NOTE
                        contentRow.mark
                ] as String[])
                practiceRowsVar2.add([
                        contentRow.title,
                        DipDocumentUtils.getWeeksWithUnit(contentRow.weeksAsDouble) + ".",
                        double2String(weeksAsDouble),//NOTE
                        contentRow.mark
                ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaStateExamRow) {
                allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allStateExamWeeksLoad += weeksAsDouble;
                stateExamRows.add([
                        contentRow.title,
                        "x",
                        "x",//NOTE
                        contentRow.mark
                ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaQualifWorkRow) {
                allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allStateExamWeeksLoad += weeksAsDouble;
                String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                graduateWorkRows.add([
                        title,
                        "x",
                        "x",//NOTE
                        contentRow.mark
                ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaOptDisciplineRow) {
                optDisciplineRowsVar1.add([
                        contentRow.title,
                        double2String(contentRow.laborAsDouble) + " з.е.",
                        double2String(loadAsDouble),//NOTE
                        contentRow.mark
                ] as String[])
                optDisciplineRowsVar2.add([contentRow.title,
                                           double2String(loadAsDouble) + " час.",
                                           double2String(weeksAsDouble),//NOTE
                                           contentRow.mark
                                          ] as String[])
            }


            if (contentRow instanceof DiplomaCourseWorkRow) {
                courseTableVar1.add(getStringRow4Var1(contentRow));
                courseTableVar2.add(getStringRow4Var2(contentRow));
            }

            if (contentRow instanceof DiplomaPracticeRow) {
                practiceTableVar1.add(getStringRow4Var1(contentRow));
                practiceTableVar2.add(getStringRow4Var2(contentRow));
            }

        }

        im.put("fullNameOrganization", topOrgUnit.nominativeCaseTitle)
        im.put("cityOrganization", topOrgUnit.territorialTitle.replace(".", ""))
        im.put("registrationNumber", registrationNumber)
        im.put("issuanceDate", issuanceDate)
        im.put("lastName", lastName)
        im.put("firstName", firstName)
        im.put("middleName", middleName)
        im.put("birthDate", birthDate)
        im.put("eduDocumentKind", eduDocumentKindTitle)
        im.put("yearEnd", yearEnd)
        im.put("beginTraining", beginTraining)
        im.put("completedTraining", completedTraining)
        im.put("programKind", programKind)
        im.put("developPeriod", developPeriod)
        im.put("programSubject", programSubjectTitle)
        im.put("programQualification", programQualification)
        im.put("academyRename", academyRenameList)
        im.put("information", information)
        im.put("slash", slash)
        im.put("fioRector", fioRector)
        im.put("learned", student.status.active ? "Продолжает обучение в" : "Завершил(-а) обучение в")

        String practiceLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allPracticeLaborLoad) + " з.е."
        String practiceLoadVar2 = DipDocumentUtils.getWeeksWithUnit(allPracticeWeeksLoad)
        String examLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allStateExamLaborLoad) + " з.е."
        String examLoadVar2 = DipDocumentUtils.getWeeksWithUnit(allStateExamWeeksLoad)

        if (DipDocumentUtils.isShowLabor(diplomaObject)) {
            tm.put("discipline", disciplineRowsVar1 as String[][])
            fillEducationCertificatePractice(practiceLoadVar1, practiceRowsVar1, tm)
            fillTableLikeString("T", courseTableVar1, im, "не предусмотрены");
            fillTableLikeString("P", practiceTableVar1, im, "не проходил(а)");
            fillEducationCertificateStateExam(examLoadVar1, stateExamRows, graduateWorkRows, tm)
            def labor = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            def certLabor = diplomaObject.content.loadAsDouble ?: 0
            labor.add([
                    "Объем образовательной программы",
                    DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certLabor) + " з.е.",
                    "x",//NOTE
                    "x"
            ] as String[])
            audLoadRow.add([
                    "в том числе объем работы обучающихся во взаимодействии с преподавателем:",
                    DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.",
                    "x",//NOTE
                    "x"
            ] as String[])
            tm.put("labor", labor as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillEducationCertificateFacultDiscipline(optDisciplineRowsVar1, dipAdditionalInformation, tm)

        } else {
            tm.put("discipline", disciplineRowsVar2 as String[][])
            fillEducationCertificatePractice(practiceLoadVar2, practiceRowsVar2, tm)
            fillTableLikeString("T", courseTableVar2, im, "не предусмотрены");
            fillTableLikeString("P", practiceTableVar2, im, "не проходил(а)");
            fillEducationCertificateStateExam(examLoadVar2, stateExamRows, graduateWorkRows, tm)
            def labor = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            def certWeeks = diplomaObject.content.loadAsDouble == null ? 0 : diplomaObject.content.loadAsDouble;
            String weeksLoad = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certWeeks) + " " +
                    CommonBaseStringUtil.numberPostfixCase(
                            diplomaObject.content.isLoadInWeeks() ? diplomaObject.content.laborAsLong : diplomaObject.content.weeksAsLong,
                            "неделя", "недели", "недель")
            labor.add(["Срок освоения образовательной программы", weeksLoad, "x"] as String[])
            audLoadRow.add(["в том числе аудиторных часов:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
            tm.put("labor", labor as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillEducationCertificateFacultDiscipline(optDisciplineRowsVar2, dipAdditionalInformation, tm)
        }

        im.modify(document)
        tm.modify(document)

        return [document: document, fileName: 'document.rtf']
    }

    static List<String> getStringRow4Var1(DiplomaContentRow row)
    {
        double loadAsDouble = row.loadAsDouble != null ? row.loadAsDouble : 0;
        if (loadAsDouble == 0d)
            loadAsDouble = row.audLoadAsDouble != null ? row.audLoadAsDouble : 0;

        List<String> p1 = new ArrayList<>();
        p1.add(row.title);
        p1.add(double2String(row.laborAsDouble) + " ЗЕТ");
        p1.add(double2String(loadAsDouble) + " часов");
        p1.add(row.mark);

        return p1;
    }

    static List<String> getStringRow4Var2(DiplomaContentRow row)
    {
        double loadAsDouble = row.loadAsDouble != null ? row.loadAsDouble : 0;
        if (loadAsDouble == 0d)
            loadAsDouble = row.audLoadAsDouble != null ? row.audLoadAsDouble : 0;

        List<String> p1 = new ArrayList<>();
        p1.add(row.title);
        p1.add(DipDocumentUtils.getWeeksWithUnit(row.weeksAsDouble));
        p1.add(double2String(loadAsDouble) + " часов");
        p1.add(row.mark);

        return p1;
    }

    private static String double2String(Double value) {
        DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value != null ? value : 0)
    }

    private
    static void fillEducationCertificatePractice(String practiceRowsLoad, ArrayList<String[]> practiceRows, RtfTableModifier tm) {

        def allRows = new ArrayList<String[]>()

        if (!CollectionUtils.isEmpty(practiceRows)) {
            allRows.add([
                    "Практики",
                    practiceRowsLoad,
                    "x",//NOTE
                    "x"
            ] as String[])
            allRows.add(["в том числе:"] as String[])

            tm.put("allPractice", allRows as String[][])
            tm.put("practice", practiceRows as String[][])
        } else {
            tm.put("allPractice", "" as String[][])
            tm.put("practice", "" as String[][])
        }
    }

    static void fillTableLikeString(String label, List<List<String>> rows, RtfInjectModifier modifier, String ifEmpty) {

        if (!CollectionUtils.isEmpty(rows))
            modifier.put(label, getStringFromRows(rows));
        else
            modifier.put(label, ifEmpty);
    }

    static RtfString getStringFromRows(List<List<String>> rows)
    {
        def rtfString = new RtfString();

        def iterator = rows.iterator();
        int index = 1;
        while (iterator.hasNext())
        {
            rtfString
                    .append(String.valueOf(index++)).append(". ")
                    .append(CommonBaseStringUtil.joinNotEmpty(iterator.next(), ", "));
            if (iterator.hasNext())
                rtfString.append(";").append(IRtfData.PAR);
            else
                rtfString.append(".");
        }

        return rtfString;
    }

    private
    static void fillEducationCertificateFacultDiscipline(ArrayList<String[]> optDisciplineRows, DipAdditionalInformation additionalInformation, RtfTableModifier tm) {
        def allRows = new ArrayList<String[]>()
        allRows.add(["Факультативные дисциплины"] as String[])
        allRows.add(["в том числе:"] as String[])

        if (additionalInformation != null && additionalInformation.isShowAdditionalDiscipline()) {
            tm.put("facultative", allRows as String[][])
            tm.put("facultDiscipline", optDisciplineRows as String[][])
        } else {
            tm.put("facultative", new ArrayList<String[]>() as String[][])
            tm.put("facultDiscipline", new ArrayList<String[]>() as String[][])
        }
    }

    private
    static void fillEducationCertificateStateExam(String examRowsLoad, ArrayList<String[]> stateExamRows, ArrayList<String[]> graduateWorkRows, RtfTableModifier tm) {
        def allRows = new ArrayList<String[]>()
        if (!CollectionUtils.isEmpty(stateExamRows) || !CollectionUtils.isEmpty(graduateWorkRows)) {
            allRows.add([
                    "Государственная итоговая аттестация",
                    examRowsLoad,
                    "x",//NOTE
                    "x"
            ] as String[])
            allRows.add(["в том числе:"] as String[])

            tm.put("attestation", allRows as String[][])
            tm.put("exam", stateExamRows as String[][])
            tm.put("graduateWork", graduateWorkRows as String[][])
        } else {
            tm.put("attestation", "" as String[][])
            tm.put("exam", "" as String[][])
            tm.put("graduateWork", "" as String[][])
        }
    }

    private static RtfString getAcademyRename(List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList) {
        def academyRename = new RtfString();
        for (DiplomaAcademyRenameData renameData : diplomaAcademyRenameDataList) {
            if (diplomaAcademyRenameDataList.indexOf(renameData) > 0) {
                academyRename.par()
            }
            academyRename.append("Образовательная организация переименована в " + RussianDateFormatUtils.getYearString(renameData.getAcademyRename().getDate(), false) + " году.").par();
            academyRename.append("Старое полное официальное наименование образовательной организации - " + renameData.getAcademyRename().getPreviousFullTitle() + ".");
        }
        return academyRename
    }

    private
    static RtfString getEducationCertificateAdditionalInformation(DipAdditionalInformation additionalInformation, DiplomaObject diplomaObject) {
        def information = new RtfString();

        if (additionalInformation != null) {
            List<DipAddInfoEduForm> addInfoEduFormList = DataAccessServices.dao().getList(DipAddInfoEduForm.class, DipAddInfoEduForm.L_DIP_ADDITIONAL_INFORMATION, additionalInformation)

            if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null) {
                information.append("Форма обучения: " + addInfoEduFormList.get(0).getEduProgramForm().getTitle() + ".")
            } else if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1) {
                information.append("Сочетание форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList) {
                    if (addInfoEduFormList.indexOf(eduForm) > 0) {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            } else if (additionalInformation.isShowSelfEduForm() && CollectionUtils.isEmpty(addInfoEduFormList)) {
                information.append("Форма получения образования: самообразование.")
            } else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null) {
                String eduFormTitle = ""
                if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNAYA))
                    eduFormTitle = "очной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.ZAOCHNAYA))
                    eduFormTitle = "заочной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNO_ZAOCHNAYA))
                    eduFormTitle = "очно-заочной"
                information.append("Сочетание самообразования и " + eduFormTitle + " формы обучения.")
            } else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1) {
                information.append("Сочетание самообразования и форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList) {
                    if (addInfoEduFormList.indexOf(eduForm) > 0) {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            }

            String specialization = diplomaObject.getContent().getProgramSpecialization() != null ? diplomaObject.getContent().getProgramSpecialization().getTitle() : "";
            if (additionalInformation.showProgramSpecialization) {
                if (diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2005_62)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2005_68)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2009_62)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2009_68)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2013_03)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2013_04)) {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                }
            }
            if (diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2005_65)
                    || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2009_65)
                    || diplomaObject.content.programSubject.subjectIndex.code.equals(TITLE_2013_05)) {
                if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.ORIENTATION.title)) {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                } else if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.SPECIALIZATION.title)) {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Специализация:  " + specialization + ".")
                }
            }

            if (additionalInformation.passIntenssiveTraining) {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Пройдено ускоренное обучение по образовательной программе.")
            }

            if (additionalInformation.demand) {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Справка выдана по требованию.")
            }

            List<DipEduInOtherOrganization> otherOrganizationList = DataAccessServices.dao().getList(DipEduInOtherOrganization.class,
                    DipEduInOtherOrganization.dipAdditionalInformation(), additionalInformation);

            for (DipEduInOtherOrganization otherOrganization : otherOrganizationList) {
                if (information.toList().size() > 0)
                    information.par()

                if (otherOrganization.totalCreditsAsDouble != null && otherOrganization.totalCreditsAsDouble > 0) {
                    information.append("Часть образовательной программы в объеме "
                            + DipDocumentUtils.getLaborWithUnitInGenitive(otherOrganization.totalCreditsAsDouble)
                            + " освоена в "
                            + otherOrganization.row + ".")
                }

                if (otherOrganization.totalWeeksAsDouble != null && otherOrganization.totalWeeksAsDouble > 0) {
                    information.append("Часть образовательной программы в объеме "
                            + DipDocumentUtils.getWeeksWithUnitInGenitive(otherOrganization.totalWeeksAsDouble)
                            + " освоена в "
                            + otherOrganization.row + ".")
                }
            }
        }
        return information
    }

    private static void injectStudentExtractOrders(final Student student, final RtfInjectModifier im) {

        def alias = "ase"
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, alias)
                .column(DQLExpressions.property(alias))
                .top(1)
                .where(DQLExpressions.eq(DQLExpressions.property(alias, AbstractStudentExtract.entity()), DQLExpressions.value(student)))
                .where(DQLExpressions.in(DQLExpressions.property(alias, AbstractStudentExtract.type().code()), excludeStudentExtractCodes))
                .order(DQLExpressions.property(alias, AbstractStudentExtract.paragraph().order().commitDate()))

        List<AbstractStudentExtract> list = dao.getList(builder);
        if (!list.isEmpty()) {
            AbstractStudentExtract extract = list.get(0)
            AbstractStudentOrder order = extract.paragraph.order
            im.put("extractOrderNum", "Приказ об отчислении № " + order.number)
            im.put("extractOrderDate", date2String(order.commitDate))
            im.put("extractOrderAlt", "Приказ об отчислении" + (order.commitDate == null ? "" : " от " + date2String(order.commitDate) + " г.") + (order.number == null ? "" : " №" + order.number))
        } else {
            im.put("extractOrderNum", "")
            im.put("extractOrderDate", "")
            im.put("extractOrderAlt", "")
        }

    }

    private static void injectEnrollmentExtractDataNew(final Student student, final RtfInjectModifier im) {
        //ENROLMENT ORDER
        String enrOrderNum = ""
        String enrOrderDate = ""

        IEntity extract = getMaxOrder(student)

        if (extract != null)
        {
            if (extract instanceof EnrollmentExtract)
            {//Новый абитуриент
                final EnrollmentExtract extractNew = extract
                final def order = extractNew.order
                enrOrderNum = order.number
                enrOrderDate = date2String(order.commitDate)

            } else if (extract instanceof EnrEnrollmentExtract)
            {//Старый абитуриент
                final EnrEnrollmentExtract extract14 = extract
                final def order = extract14.order
                enrOrderNum = order.number
                enrOrderDate = date2String(order.commitDate)

            } else if (extract instanceof EduEnrAsTransferStuExtract)
            {
                final EduEnrAsTransferStuExtract extractStud = extract
                final def order = extractStud.paragraph.order
                enrOrderNum = order.number
                enrOrderDate = date2String(order.commitDate)
            }
        }

        im.put("enrOrderNum", enrOrderNum)
        im.put("enrOrderDate", enrOrderDate)
    }

    private static IEntity getMaxOrder(Student student) {
        final EduEnrAsTransferStuExtract extractStud = getEnrolmentStudentOrder(student)
        final EnrollmentExtract extractNew = getEnrolmentOrder(student)
        final EnrEnrollmentExtract extract14 = getEnrolmentOrder2014(student)
        final List<OrderWrapper> list = new ArrayList<>(3)
        if (extractStud != null) {
            list.add(new OrderWrapper(extractStud, extractStud.paragraph.order.commitDate))
        }
        if (extractNew != null) {
            list.add(new OrderWrapper(extractNew, extractNew.paragraph.order.commitDate))
        }
        if (extract14 != null) {
            list.add(new OrderWrapper(extract14, extract14.paragraph.order.commitDate))
        }

        if(list.size()>0)
            return list.sort().get(0).extract
        else
            return null
    }

    private static class OrderWrapper implements Comparable<OrderWrapper> {
        private final IEntity extract;
        private final Date orderDate;

        OrderWrapper(IEntity extract, Date orderDate) {
            this.extract = extract
            this.orderDate = orderDate
        }

        @Override
        int compareTo(OrderWrapper o) {
            return o.orderDate.compareTo(this.orderDate)
        }
    }

    private static EduEnrAsTransferStuExtract getEnrolmentStudentOrder(Student student) {
        def alias = 'ex'
        def builder = new DQLSelectBuilder().fromEntity(EduEnrAsTransferStuExtract.class, alias)
                .top(1)
                .column(DQLExpressions.property(alias))
                .where(DQLExpressions.eq(DQLExpressions.property(alias, EduEnrAsTransferStuExtract.entity()), DQLExpressions.value(student)))
                .where(DQLExpressions.isNotNull(DQLExpressions.property(alias, EduEnrAsTransferStuExtract.paragraph().order())))
                .order(DQLExpressions.property(alias, EduEnrAsTransferStuExtract.paragraph().order().commitDate()), OrderDirection.desc)
        List<EduEnrAsTransferStuExtract> extracts = dao.getList(builder)

        if (extracts.isEmpty())
            return null
        return extracts.get(0)
    }

    private static EnrollmentExtract getEnrolmentOrder(Student student) {
        def alias = 'ex'
        def builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, alias)
                .top(1)
                .column(DQLExpressions.property(alias))
                .where(DQLExpressions.eq(DQLExpressions.property(alias, EnrollmentExtract.studentNew()), DQLExpressions.value(student)))
                .where(DQLExpressions.isNotNull(DQLExpressions.property(alias, EnrEnrollmentExtract.paragraph().order())))
                .order(DQLExpressions.property(alias, EnrollmentExtract.paragraph().order().commitDate()), OrderDirection.desc)

        List<EnrollmentExtract> extracts = dao.getList(builder)
        if (extracts.isEmpty())
            return null
        return extracts.get(0)
    }

    private static EnrEnrollmentExtract getEnrolmentOrder2014(Student student) {
        def alias = 'ex'
        def builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, alias)
                .top(1)
                .column(DQLExpressions.property(alias))
                .where(DQLExpressions.eq(DQLExpressions.property(alias, EnrEnrollmentExtract.student()), DQLExpressions.value(student)))
                .where(DQLExpressions.isNotNull(DQLExpressions.property(alias, EnrEnrollmentExtract.paragraph().order())))
                .order(DQLExpressions.property(alias, EnrEnrollmentExtract.paragraph().order().commitDate()), OrderDirection.desc)

        List<EnrEnrollmentExtract> extracts = dao.getList(builder)
        if (extracts.isEmpty())
            return null
        return extracts.get(0)
    }

    private static String date2String(final Date date) {
        return date == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(date)
    }

    private final static List<String> excludeStudentExtractCodes = [
            StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DUE_TRANSFER_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STATE_EXAM_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_UNACCEPTED_TO_GP_DEFENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_GP_DEFENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER,
            StudentExtractTypeCodes.USMA_EXCLUDE_DEBTOR_MODULAR_ORDER,
            StudentExtractTypeCodes.USMA_EXCLUDE_NOT_OUT_WEEKEND_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DEATH_MODULAR_ORDER,
            StudentExtractTypeCodes.USMA_EXCLUDE_DRAFT_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_OWN_FREE_WILL_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_EDU_PLAN_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_NOT_PASSED_STATE_ATTESTATION_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_VARIANT_1_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STUDENT_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_GENERAL_DIPLOMA_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_CATHEDRAL_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_CATHEDRAL_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_DIP_DOCUMENT_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DIP_DOCUMENT_LIST_EXTRACT,
    ]
}