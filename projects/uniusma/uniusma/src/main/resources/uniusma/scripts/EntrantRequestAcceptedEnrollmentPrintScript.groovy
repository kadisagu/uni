package uniusma.scripts

import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.text.StrBuilder
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import org.tandemframework.shared.person.base.entity.gen.IdentityCardDeclinabilityGen
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram

import static org.tandemframework.core.view.formatter.DateFormatter.DEFAULT_DATE_FORMATTER
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq
import static org.tandemframework.hibsupport.dql.DQLExpressions.property
import static org.tandemframework.hibsupport.dql.DQLExpressions.value

/**
 * @author Igor Belanov
 * @since 07.07.2016
 * копипаста скрипта из продукта
 */

return new EntrantRequestAcceptEnrollmentPrint(
        session: session,
        template: template,
        entrantRequest: session.get(EnrEntrantRequest.class, object)
).print()


@SuppressWarnings("UnnecessaryQualifiedReference")
class EntrantRequestAcceptEnrollmentPrint
{
    def Session session
    def byte[] template
    def EnrEntrantRequest entrantRequest
    def RtfDocument mainDocument
    def List<EnrRequestedCompetition> requestedCompetitions // lazy initialized
    def Map<Long, List<EduProgramSpecialization>> specializationMap // lazy initialized
    final im = new RtfInjectModifier()
    final tm = new RtfTableModifier()

    def print()
    {
        mainDocument = new RtfReader().read(template)

        fillCompetitions() // Выбранные направления

        final entrant = entrantRequest.entrant
        final person = entrant.person
        final card = entrantRequest.identityCard


        im.put('highSchoolTitleShort', TopOrgUnit.instance.shortTitle) // Сокр. назв. головного подразделения (вуза)
        im.put('rector_D', getRectorIof(GrammaCase.DATIVE)) // [И.О. Фамилия] ректора в дательном падеже

        im.put('FIO_G', PersonManager.instance().declinationDao().getCalculatedFIODeclination(card, InflectorVariantCodes.RU_GENITIVE)) // ФИО абитуриента в родительном падеже
        im.put('birthDate', DEFAULT_DATE_FORMATTER.format(card.birthDate)) // дата рождения абитуриента
        im.put('citizenship', card.citizenship.fullTitle) // гражданство
        im.put('identityCardTitle', card.shortTitle) // сокр. название типа удостоверения личности
        im.put('identityCardPlaceAndDate', [card.issuancePlace, DEFAULT_DATE_FORMATTER.format(card.issuanceDate)].grep().join(', ')) // место и дата выдачи удостоверения личности
        im.put('addressTitleWithFlat', person.addressRegistration?.titleWithFlat) // адрес регистрации
        im.put('addressPhonesTitle', CommonBaseStringUtil.joinArrayUnique(', ', person.contactData.phoneDefault, person.contactData.phoneWork, person.contactData.phoneMobile)) // телефоны
        im.put('email', person.contactData.email) // email

        // Дата заявления
        Date date = new Date();
        im.put('day', RussianDateFormatUtils.getDayString(date, true)) // день
        im.put('monthStr', RussianDateFormatUtils.getMonthName(date, false)) // месяц строкой
        im.put('year', RussianDateFormatUtils.getYearString(date, false)) // год


        im.modify(mainDocument)

        tm.modify(mainDocument)

        return [document: RtfUtil.toByteArray(mainDocument), fileName: "Заявление о согласии на зачисление ${card.fullFio}.rtf"]
    }


    /** Таблицы с выбранными конкурсами */
    void fillCompetitions() {
        final simpleRows = new ArrayList<String[]>()
        int i=0;
        for (requestedCompetition in getRequestedCompetitions())
        {
            final row = [++i as String,
                         entrantRequest.stringNumber,
                         getProgramSubjectStr(requestedCompetition),
                         requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                         getPlaces(requestedCompetition)
            ]
            simpleRows.add(row as String[])

        }

        tm.put('TA1', simpleRows as String[][])

    }



    /** @return список выбранных конкурсов по зявлению, сортировка по приоритету */
    def List<EnrRequestedCompetition> getRequestedCompetitions() {

        // lazy!
        if (this.requestedCompetitions == null) {
            this.requestedCompetitions = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, 'e')
                    .column(property('e'))
                    .where(eq(property('e', EnrRequestedCompetition.request()), value(this.entrantRequest)))
                    .where(eq(property('e', EnrRequestedCompetition.accepted()), value(true)))
//                    .where(eq(property('e', EnrRequestedCompetition.parallel()), value(false)))
                    .order(property('e', EnrRequestedCompetition.priority()), OrderDirection.asc)
                    .createStatement(this.session).<EnrRequestedCompetition>list()
        }
        return requestedCompetitions
    }



    /** @return "на места ..." */
    static String getPlaces(EnrRequestedCompetition requestedCompetition) {
        def String str
        switch (requestedCompetition.competition.type.code)
        {
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
            case EnrCompetitionTypeCodes.CONTRACT:
                str = 'по договорам об оказании платных образовательных услуг'
                break
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
            case EnrCompetitionTypeCodes.MINISTERIAL:
                str = 'финансируемые из федерального бюджета'
                break
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                str = 'в пределах квоты целевого приема'
                break
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                str = 'в пределах квоты приема лиц, имеющих особое право'
                break
            default:
                return ''
        }
        if (requestedCompetition instanceof EnrRequestedCompetitionTA) {
            // Если конкурс по ЦП, дописываем название вида ЦП в скобках
            str += ' (' + (requestedCompetition as EnrRequestedCompetitionTA).targetAdmissionKind.targetAdmissionKind.title + ')'
        }
        return str
    }


    /**
     * @param grammaCase падеж
     * @return "И.О. Фамилия" ректора в нужном падеже
     */
    def String getRectorIof(GrammaCase grammaCase)
    {
        final IdentityCard card = (TopOrgUnit.instance.head?.employee?.person?.identityCard as IdentityCard) ?: getHeaderIdentityCard(TopOrgUnit.instance)
        if (card != null) {

            final headIof = new StrBuilder()
            headIof.append(card.firstName[0].toUpperCase()).append('.')
            if (card.middleName) {
                headIof.append(card.middleName[0].toUpperCase()).append('.')
            }
            headIof.append(' ')

            final customCase = DataAccessServices.dao().<IdentityCardDeclinability>getByNaturalId(new IdentityCardDeclinabilityGen.NaturalId(card))
            if (customCase != null) {
                if (customCase.isLastNameNonDeclinable()) {
                    return headIof.append(card.lastName).toString() // Фамилия не склоняется. Вставляем как есть
                } else {
                    // Фамилия склоняется, но, возможно, в базе правильное склонение указано пользователем
                    final variantMap = DeclinableManager.instance().dao().getPropertyValuesMap(customCase, IdentityCard.P_LAST_NAME)
                    final dativeLastName = variantMap?.get(DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase))
                    if (dativeLastName != null) {
                        return headIof.append(dativeLastName).toString()
                    }
                }
            }

            // Используем автосклонятор
            headIof.append(PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, grammaCase, card.sex.male))
            return headIof.toString()
        }
        return ''
    }

    /** @return Получаение удостоверения личности одного из руководителей подразделения */
    def IdentityCard getHeaderIdentityCard(final OrgUnit orgUnit)
    {
        return new DQLSelectBuilder().fromEntity(EmployeePost.class, 'p')
                .top(1)
                .column(property('p', EmployeePost.employee().person().identityCard()))
                .where(eq(property('p', EmployeePost.orgUnit()), value(orgUnit)))
                .where(eq(property('p', EmployeePost.postRelation().headerPost()), value(true)))
                .where(eq(property('p', EmployeePost.employee().archival()), value(false)))
                .where(eq(property('p', EmployeePost.postStatus().active()), value(true)))
                .createStatement(this.session).uniqueResult()
    }

    /** @return Название направления подготовки из выбранного конкурса с кодом и списком направленностей в скобках, если направленности есть */
    def String getProgramSubjectStr(EnrRequestedCompetition requestedCompetition)
    {
        final programSet = requestedCompetition.competition.programSetOrgUnit.programSet
        final specs = getRequestedProgramMap().get(requestedCompetition.id)
        return programSet.printTitle + ((specs != null && !specs.isEmpty()) ? ' (' + StringUtils.join(specs.collect {e -> e.title}, ', ') + ')' : '')
    }

    /** @return Направленности образовательных программ для выбранных конкурсов */
    def Map<Long, List<EduProgramSpecialization>> getRequestedProgramMap() {

        // lazy!
        if (this.specializationMap == null) {
            this.specializationMap = SafeMap.get(ArrayList.class)
            new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, 'p')
                    .column(property('p'))
                    .where(DQLExpressions.in(property('p', EnrRequestedProgram.requestedCompetition()), getRequestedCompetitions()))
                    .createStatement(this.session).<EnrRequestedProgram> list().each { this.specializationMap.get(it.requestedCompetition.id).add(it.programSetItem.program.programSpecialization) }

            for (e in this.specializationMap.entrySet()) {
                if (e.value.size() == 1 && e.value[0].rootSpecialization) {
                    e.value.clear()
                }
            }
        }
        return this.specializationMap
    }
}
