package uniusma.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.fias.base.entity.AddressDetailed
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA

return new EnrEntrantStudentCardPrint(
        session: session,
        template: template,
        reqComp: session.get(EnrRequestedCompetition.class, object)
).print()
/**
 * @author Denis Perminov
 * @since 27.08.2014
 */
class EnrEntrantStudentCardPrint {
    Session session
    byte[] template
    EnrRequestedCompetition reqComp

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print() {
        RtfDocument document = new RtfReader().read(template)

        EnrProgramSetOrgUnit psOU = reqComp.competition.programSetOrgUnit
        im.put("formativeOrgUnit", psOU.formativeOrgUnit.printTitle)
        im.put("formativeOrgUnit_G", psOU.formativeOrgUnit.getGenitiveCaseTitle().toLowerCase())

        EduProgramSubject programSubject = psOU.programSet.programSubject
        im.put("eduLevel", programSubject.code + " " + programSubject.title + ", " + programSubject.eduProgramKind.shortTitle)
        im.put("eduLevel_D", programSubject.eduProgramKind.getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_DATIVE).toLowerCase()+" " + programSubject.code + " " + programSubject.title)

        EnrEntrantRequest request = reqComp.request
        EnrEntrant entrant = request.entrant
        Person person = entrant.person
        IdentityCard idCard = person.identityCard

        //
        EnrCompetitionType competitionType = reqComp.competition.type
        im.put('compensationType', competitionType.compensationType.title)
        im.put('targetAdmission', EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competitionType.code) ? 'Да' : 'Нет')
        im.put('orgUnitTA', getTAOrgUnitTitle(reqComp))

        im.put("lastName", idCard.lastName)
        im.put("firstName", idCard.firstName)
        im.put("middleName", idCard.middleName)

        im.put("sex", idCard.sex.title)
        im.put("citizenship", idCard.citizenship?.title)
        im.put("birthdate", DateFormatter.DEFAULT_DATE_FORMATTER.format(idCard.birthDate))
        im.put("birthplace", idCard.birthPlace)

        def eduDocument = request.eduDocument
        im.put("education", [eduDocument.eduLevel?.title, eduDocument.eduOrganisationWithAddress, eduDocument.yearEnd as String].grep().join(", "))

        EnrEnrollmentExtract extract = DataAccessServices.dao().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.L_ENTITY, reqComp)
        def enrollOrder = (null != extract && extract.committed ? extract.paragraph?.order : null)
        im.put("orderNumber", (null != enrollOrder) ? enrollOrder?.number : "")
        im.put("orderDate", (null != enrollOrder) ? DateFormatter.DEFAULT_DATE_FORMATTER.format(enrollOrder?.commitDate) : "")
        def student = extract.student
        im.put("course", (null != student) ? student.course.title : "")

        EnrRatingItem ratingItem = DataAccessServices.dao().get(EnrRatingItem.class, EnrRatingItem.L_REQUESTED_COMPETITION, reqComp)
        im.put("mark", ratingItem.totalMarkAsString)

        im.put("martialStatus", person.familyStatus?.title)

        if (person.childCount > 0) {
            def nextOfKinList = (List<PersonNextOfKin>) getNextOfKin(person, RelationDegreeCodes.SON, true)
            def sonBirthYear = nextOfKinList.collect { e -> (DateFormatter.DATE_FORMATTER_JUST_YEAR.format(e.birthDate)) }.grep().join(", ")
            nextOfKinList = (List<PersonNextOfKin>) getNextOfKin(person, RelationDegreeCodes.DAUGHTER, true)
            def daughterBirthYear = nextOfKinList.collect { e -> (DateFormatter.DATE_FORMATTER_JUST_YEAR.format(e.birthDate)) }.grep().join(", ")
            im.put("children", person.childCount + ((null != sonBirthYear || null != daughterBirthYear) ? " (г.р." + [sonBirthYear, daughterBirthYear].join(", ") + ")" : ""))
        } else
            im.put("children", "")

        im.put("addressRegistration", idCard.address?.titleWithFlat)

        TopOrgUnit academy = TopOrgUnit.getInstance()
        if (null != academy.getAddress() && null != academy.getAddress().getSettlement() && null != person.getAddress() && person.getAddress() instanceof AddressDetailed && academy.getAddress().getSettlement().equals(((AddressDetailed) person.getAddress()).getSettlement())) {
            im.put("addressTitle", person.address?.titleWithFlat)
            im.put("phones", null != person.contactData.mainPhones ? " тел." + person.contactData.mainPhones : "")
        } else {
            im.put("addressTitle", "");
            im.put("phones", "");
        }

        def nextOfKin = (PersonNextOfKin) getNextOfKin(person, RelationDegreeCodes.FATHER, false)
        if (nextOfKin) {
            im.put("fatherFio", nextOfKin.fullFio)
            im.put("fatherBirthdate", null != nextOfKin?.birthDate ? " г.р." + DateFormatter.DATE_FORMATTER_JUST_YEAR.format(nextOfKin?.birthDate) : "")
            im.put("fatherWorkplace", [nextOfKin.employmentPlace, nextOfKin.post].grep().join(", "))
            im.put("fatherAddress", [nextOfKin.address?.titleWithFlat, (null != nextOfKin.phones ? " тел." + nextOfKin.phones : null)].grep().join(", "))
        } else {
            im.put("fatherFio", "")
            im.put("fatherBirthdate", "")
            im.put("fatherWorkplace", "")
            im.put("fatherAddress", "")
        }

        nextOfKin = (PersonNextOfKin) getNextOfKin(person, RelationDegreeCodes.MOTHER, false)
        if (nextOfKin) {
            im.put("motherFio", nextOfKin.fullFio)
            im.put("motherBirthdate", null != nextOfKin?.birthDate ? " г.р." + DateFormatter.DATE_FORMATTER_JUST_YEAR.format(nextOfKin?.birthDate) : "")
            im.put("motherWorkplace", [nextOfKin.employmentPlace, nextOfKin.post].grep().join(", "))
            im.put("motherAddress", [nextOfKin.address?.titleWithFlat, (null != nextOfKin.phones ? " тел." + nextOfKin.phones : null)].grep().join(", "))
        } else {
            im.put("motherFio", "")
            im.put("motherBirthdate", "")
            im.put("motherWorkplace", "")
            im.put("motherAddress", "")
        }

        def contacts = [(null != person.contactData.email ? " e-mail:" + person.contactData.email : null),
                        (null != person.contactData.phoneReg ? " дом.тел." + person.contactData.phoneReg : null),
                        (null != person.contactData.other ? " " + person.contactData.other : null)].grep().join(", ")
        im.put("contacts", contacts)

        im.put("seria", idCard.seria)
        im.put("number", idCard.number)
        im.put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(idCard.issuanceDate))
        im.put("issuance", idCard.issuancePlace)

        im.put("post", person.workPlacePosition)
        im.put("employmentPlace", person.workPlace)

        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: document,
                fileName: "Личная карточка для зачисленных абитуриентов ${idCard.fullFio}.rtf"]
    }

    def getNextOfKin(Person person, String code, boolean child) {
        def dql = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${code}'
                /)
        return (child ?
                dql.<PersonNextOfKin> list() :
                dql.setMaxResults(1).<PersonNextOfKin> uniqueResult()
        )
    }

    static def String getTAOrgUnitTitle(EnrRequestedCompetition requestedCompetition) {
        String competitionTypeCode = requestedCompetition.competition.type.code

        if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competitionTypeCode)) {
            String orgUnitTitle = ""
            if (requestedCompetition instanceof EnrRequestedCompetitionTA)
                orgUnitTitle = ((EnrRequestedCompetitionTA) requestedCompetition).targetAdmissionOrgUnit.legalFormWithTitle
            return orgUnitTitle
        }
        return ""
    }
}