package uniusma.scripts

import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.movestudent.entity.AbstractStudentOrder
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.UniEppUtils
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unisession.entity.document.SessionDocument
import ru.tandemservice.unisession.entity.mark.SessionMark
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.UsmaStudentCardAttachmentReportManager
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.IStudentCardAttachmentReportDao
import ru.tandemservice.uniusma.base.bo.UsmaStudentCardAttachmentReport.logic.StudentWpeActionContainer

return new StudentCardAddPrint(                                 // стандартные входные параметры скрипта
        template: template,                                     // шаблон
        data: data,                                             // данные
).print()

class StudentCardAddPrint {
    byte[] template
    StudentWpeActionContainer data

    private static final IStudentCardAttachmentReportDao dao = UsmaStudentCardAttachmentReportManager.instance().dao()
    private final RtfInjectModifier im = new RtfInjectModifier()
    private final RtfTableModifier tm = new RtfTableModifier()

    def print() {

        if (this.data == null)
            throw new ApplicationException("Нет данных для печати");

        final Student student = data.student
        final EppYearEducationProcess eduYear = data.eduYear
        final Course course = data.course

        im.put('FIO', student.fullFio)
        im.put('course', numberToString(course.intValue))
        im.put('eduYear', eduYear.getTitle())
        im.put('firstTerm', numberToString(data.firstTerm))
        im.put('secondTerm', numberToString(data.secondTerm))

        im.put('nextCourse', numberToString(course.intValue + 1))
        im.put('bookNum', student.bookNumber)

        final AbstractStudentOrder order = dao.getStudentTransferOrder(student, course.intValue, course.intValue + 1)
        im.put('transferOrderNumber', order == null ? "-" : order.number)
        im.put('transferOrderDate', order == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.commitDate))

        tm.put('T1', getTable(data.getFirstTermWpeSorted(getComparatorGroupTypeFCA()), data.getSessionMarkMap(), data.getSessionDocumentMap()))
        tm.put('T2', getTable(data.getSecondTermWpeSorted(getComparatorGroupTypeFCA()), data.getSessionMarkMap(), data.getSessionDocumentMap()))

        final RtfDocument document = new RtfReader().read(template)
        tm.modify(document)
        im.modify(document)


        return [document: RtfUtil.toByteArray(document),
                fileName: "Вкладышей студента УГМУ ${student.fullFio}.rtf",
                rtf     : document]
    }

    private static String numberToString(Integer num) {
        if (NUM_TO_WORD.length < num)
            throw new ApplicationException("Невозможно перевести число \"{$num}\" в строку");
        return NUM_TO_WORD[num - 1]
    }

    private static String[][] getTable(
            Collection<EppStudentWpeCAction> wpeActionList,
            Map<Long, SessionMark> sessionMarkMap,
            Map<Long, SessionDocument> sessionDocumentMap
    ) {
        final List<List<String>> res = new ArrayList<>(wpeActionList.size())

        for (final EppStudentWpeCAction wpeAction : wpeActionList) {
            final EppRegistryElementPart registryElementPart = wpeAction.studentWpe.registryElementPart
            final EppRegistryElement registryElement = registryElementPart.registryElement
            final SessionMark sessionMark = sessionMarkMap.getOrDefault(wpeAction.id, null)
            final SessionDocument sessionDocument = sessionDocumentMap.getOrDefault(wpeAction.id, null)

            final String titleStr = registryElementPart.registryElement.title
            final String loadStr = UniEppUtils.formatLoad(registryElement.sizeAsDouble, false)
            final String zetStr = UniEppUtils.formatLoad(registryElement.laborAsDouble, false)
            final String examMarkStr
            final String offsetMarkStr
            final Date modificationDate
            if (sessionMark == null) {
                examMarkStr = ""
                offsetMarkStr = ""
                modificationDate = null;
            } else {
                examMarkStr = sessionMark == null ? "" : getExamMark(sessionMark.valueItem)
                offsetMarkStr = sessionMark == null ? "" : getOffsetMark(sessionMark.valueItem)
                modificationDate = sessionMark.modificationDate;
            }

            final String examListDateStr = sessionDocument == null ? "" : sessionDocument.getTypeTitle() + " " + date2String(modificationDate)
            res.add(
                    [titleStr,          //1 Наименование предмета
                     loadStr,           //2 Число часов по плану
                     zetStr,            //3 ЗЕТ (трудоёмкость)
                     examMarkStr,       //4 Экзаменационная оценка
                     offsetMarkStr,     //5 Отметка о зачете
                     examListDateStr]   //6 Дата и № экзам. листа
            )
        }

        return res as String[][];
    }

    private static String getExamMark(SessionMarkCatalogItem catalogItem) {
        if (catalogItem == null) return ""
        switch (catalogItem.code) {
            case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
            case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
            case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
            case SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO:
                return catalogItem.printTitle
            default:
                return ""
        }
    }

    private static String getOffsetMark(SessionMarkCatalogItem catalogItem) {
        if (catalogItem == null) return ""
        switch (catalogItem.code) {
            case SessionMarkGradeValueCatalogItemCodes.ZACHTENO:
            case SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO:
                return catalogItem.printTitle
            default:
                return ""
        }
    }

    private final static String[] NUM_TO_WORD = [
            "Первый", "Второй", "Третий", "Четвёртый", "Пятый", "Шестой", "Седьмой", "Восьмой", "Девятый", "Десятый",
            "Одиннадцатый", "Двенадцатый", "Тринадцатый", "Четырнадцатый", "Пятнадцатый", "шестнадцатый", "Семнадцатый",
            "Восемнадцатый", "Девятнадцатый", "Двадцатый"
    ];

    private static final Map<String, Integer> groupTypeFCAPriorityMap = getGroupTypeFCAPriority()

    private static Comparator<EppStudentWpeCAction> getComparatorGroupTypeFCA() {

        new Comparator<EppStudentWpeCAction>() {
            @Override
            int compare(EppStudentWpeCAction o1, EppStudentWpeCAction o2) {
                Integer priority1 = groupTypeFCAPriorityMap.get(o1.type.code)
                Integer priority2 = groupTypeFCAPriorityMap.get(o2.type.code)
                return Integer.compare(priority1, priority2)
            }
        }
    }

    // eppGroupTypeFCA.code -> eppFControlActionType.priority
    private static Map<String, Integer> getGroupTypeFCAPriority() {
        final String alias = 'cat'
        final List<EppFControlActionType> list = dao.getList(
                new DQLSelectBuilder().fromEntity(EppFControlActionType.class, alias)
                        .column(DQLExpressions.property(alias))
        )

        final Map<String, Integer> result = list.collectEntries { [it.eppGroupType.code, it.totalMarkPriority] }
        return result
    }

    private static String date2String(final Date date) {
        return date == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(date)
    }
}
