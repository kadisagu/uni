package uniusma.scripts

import com.google.common.collect.Lists
import com.google.common.collect.Sets
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EnrollmentExamAllExamsSheetPrint(            // стандартные входные параметры скрипта
        session: session,                               // сессия
        template: template,                             // шаблон
        entrant: session.get(EnrEntrant.class, object)  // объект печати
).print()

/**
 * Алгоритм печати экзаменационного листа абитуриента
 * Копипаста скрипта EnrollmentExamAllExamsSheetPrint из unienr14 (03.06.2016)
 */
class EnrollmentExamAllExamsSheetPrint
{
    Session session
    byte[] template
    EnrEntrant entrant
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def card = entrant.person.identityCard

        im.put('highSchoolTitle', TopOrgUnit.instance.title);
        im.put('lastName', card.lastName);
        im.put('firstName', card.firstName);
        im.put('middleName', card.middleName);

        im.put('number', entrant.getPersonalNumber());
        im.put('date', new Date().format('dd.MM.yyyy'));

        fillEnrollmentResults();

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Экзаменационный лист абитуриента ${entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillEnrollmentResults()
    {
        // вычисляем внешние выбранные ВИ
        List<EnrChosenEntranceExamForm> chosenExamDisciplines = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExamForm.class, "e").column("e")
                .where(eq(property("e", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().id()), value(entrant.id)))
                .where(eq(property("e", EnrChosenEntranceExamForm.passForm().internal()), value(false)))
                .order(property("e", EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .order(property("e", EnrChosenEntranceExamForm.passForm().code()))
                .createStatement(session).list();

        // вычисляем внутренние дисциплины для сдачи
        List<EnrExamPassDiscipline> internalExamPassDisciplines = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "e").column("e")
                .where(eq(property("e", EnrExamPassDiscipline.entrant().id()), value(entrant.id)))
                .where(eq(property("e", EnrExamPassDiscipline.retake()), value(Boolean.FALSE)))
                .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
                .order(property("e", EnrExamPassDiscipline.passForm().code()))
                .createStatement(session).list();

        // список строк
        final List<String[]> rows = new ArrayList<String[]>();

        Map<EnrCampaignDiscipline, Integer> discCountMap = new HashMap<>();
        for (EnrExamPassDiscipline exam : internalExamPassDisciplines)
        {
            discCountMap.put(exam.getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline())) + 1);
        }
        for (EnrChosenEntranceExamForm exam : chosenExamDisciplines)
        {
            discCountMap.put(exam.getChosenEntranceExam().getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getChosenEntranceExam().getDiscipline())) + 1);
        }

        Map<String, String> places = new LinkedHashMap<>();

        Set<ExamRow> tableRowsSet = Sets.newHashSet();

        // для каждой дисциплины для сдачи
        for (def exam : internalExamPassDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline()));
            boolean printForm = discCount > 1;
            Long mark = exam.getMarkAsDouble() == null ? null : Math.round(exam.getMarkAsDouble());
            List<EnrExamGroupScheduleEvent> events = exam.getExamGroup() == null ? Collections.emptyList() :
                    IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class,
                            EnrExamGroupScheduleEvent.examGroup(), exam.getExamGroup(),
                            EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());

            for (EnrExamGroupScheduleEvent event : events)
                places.put(event.examScheduleEvent.examRoom.place.displayableTitle, event.examScheduleEvent.examRoom.place.fullLocationInfo)

            // сформировать строку
            String[] row = new String[8];
            // row[0] заполним после сортировки
            row[1] = exam.discipline.title + (printForm ? " (" + exam.passForm.title + ")" : "");
            row[2] = exam.examGroup?.title;
            row[3] = events.collect {
                it.timeTitle + ", " + it.examScheduleEvent.examRoom.place.displayableTitle
            }.grep().join("\n");

            // итоговый балл цифрами и прописью
            row[4] = mark == null ? "" : mark;
            row[5] = mark == null ? "" : NumberSpellingUtil.spellNumberMasculineGender(mark);

            row[6] = ""
            // events.collect { it.examScheduleEvent.commission }.grep().join("\n"); - можно подставить, если хочется вывести экзаменаторов

            tableRowsSet.add(new ExamRow(row: row, mark: mark, discipline: exam.discipline, passForm: exam.passForm));
        }

        // для каждой дисциплины для сдачи
        for (def exam : chosenExamDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getChosenEntranceExam().getDiscipline()));
            boolean printForm = discCount > 1;
            Long mark = exam.getMarkAsLong() == 0 ? null : Math.round(exam.getMarkAsDoubleNullSafe());

            // сформировать строку
            String[] row = new String[8];
            // row[0] заполним после сортировки
            row[1] = exam.chosenEntranceExam.discipline.title + (printForm ? " (" + exam.passForm.title + ")" : "");
            row[2] = "";
            row[3] = "";

            // итоговый балл цифрами и прописью
            row[4] = mark == null ? "" : mark;
            row[5] = mark == null ? "" : NumberSpellingUtil.spellNumberMasculineGender(mark);

            row[6] = "";

            tableRowsSet.add(new ExamRow(row: row, mark: mark, discipline: exam.chosenEntranceExam.discipline, passForm: exam.passForm));
        }

        List<Row> tableRowsList = Lists.newArrayList();
        for (Row row : tableRowsSet)
        {
            tableRowsList.add(row);
        }

        // ============================== изменения в копипасте ===============================
        // ================== (докидывание достижений абитуриента в список) ===================
        // возьмём из базы личные достижения абитуриента
        List<EnrEntrantAchievement> entrantAchievementList = new DQLSelectBuilder()
                .fromEntity(EnrEntrantAchievement, "ach")
                .where(eq(
                property(EnrEntrantAchievement.entrant().id().fromAlias("ach")),
                value(entrant.getId())
        ))
                .createStatement(session).list();

        // докинем список индивидуальных достижений к собираемой таблице
        for (EnrEntrantAchievement achievement : entrantAchievementList)
        {
            Long mark = achievement.getMark() != null ? Math.round(achievement.getMark()) : 0;
            String[] row = new String[7];
            // row[0] заполним после сортировки
            row[1] = "Индивидуальные достижения (" + achievement.getType().getTitle() + ")";
            row[2] = "";
            row[3] = "";
            row[4] = mark
            row[5] = NumberSpellingUtil.spellNumberMasculineGender(mark);
            row[6] = "";
            tableRowsList.add(new AchRow(row: row, mark: mark));
        }

        // если индивидуальные достижения отсутствуют нужно создать строку в таблице оповещающую об этом
        if (entrantAchievementList.isEmpty())
        {
            String[] row = new String[7];
            // row[0] заполним после сортировки
            row[1] = "Индивидуальные достижения";
            row[2] = "";
            row[3] = "";
            row[4] = "0";
            row[5] = NumberSpellingUtil.spellNumberMasculineGender(0);
            tableRowsList.add(new AchRow(row: row, mark: 0));
        }

        // ============================ (+ подправлена сортировка) ============================

        Collections.sort(tableRowsList, new Comparator<Row>() {
            @Override
            int compare(Row r1, Row r2)
            {
                // (немного изменён компаратор (достижения ставим в конце))
                if (r1 instanceof AchRow && r2 instanceof AchRow)
                {
                    return r2.mark - r1.mark;
                }
                if (r1 instanceof AchRow)
                {
                    return 1;
                }
                if (r2 instanceof AchRow)
                {
                    return -1;
                }
                int disciplineCompare = (r1 as ExamRow).discipline.getTitle().compareTo((r2 as ExamRow).discipline.getTitle());
                if (disciplineCompare != 0)
                {
                    return disciplineCompare;
                }
                return (r1 as ExamRow).passForm.getTitle().compareTo((r2 as ExamRow).passForm.getTitle());
            }
        })

        // после сортировки нумерация в первом столбце может сбиться, какой смысл нумеровать
        // перед сортировкой не понятно (так было в оригинальном скрипте), но надо это исправить хотя бы тут:
        int i = 1;
        for (Row row : tableRowsList)
        {
            row.row[0] = i++;
        }

        // сформируем строку с информацией об общей сумме баллов
        Long markSum = 0;
        for (Row row : tableRowsList)
        {
            markSum += row.mark != null ? row.mark : 0;
        }
        def markSumString = new RtfString();
        markSumString.append("Общее количество баллов: ");
        markSumString.append(markSum.toString());
        markSumString.append(" (" + NumberSpellingUtil.spellNumberMasculineGender(markSum) + ")")

        for (Row row : tableRowsList)
        {
            rows.add(row.row);
        }

        tm.put('T', rows as String[][]);

        im.put('markSumInfo', markSumString);

        // ====================================================================================

        def placeList = new ArrayList<String>()
        for (def place : places.entrySet())
            placeList.add(place.key + " - " + place.value)

        def resultPlace = new RtfString()
        for (def place : placeList)
        {
            if (placeList.indexOf(place) > 0)
                resultPlace.par()
            resultPlace.append(place)
        }

        im.put('place', resultPlace)
    }

    // ============================== изменения в копипасте ===============================
    // ======================= (для Row сделана небольшая иерархия) =======================

    abstract class Row
    {
        protected String[] row;
        protected Long mark;
    }

    class ExamRow extends Row
    {
        private EnrCampaignDiscipline discipline;
        private EnrExamPassForm passForm;

        @Override
        boolean equals(Object obj)
        {
            if (obj == null) return false;
            if (!(obj instanceof ExamRow)) return false;

            return discipline.equals(obj.discipline) && passForm.equals(obj.passForm)
        }

        @Override
        int hashCode()
        {
            return discipline.hashCode() + passForm.hashCode();
        }
    }

    class AchRow extends Row
    {}

    // ====================================================================================
}
