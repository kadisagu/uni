/* $Id$ */
package ru.tandemservice.nsiclient;

import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.datagram.XDatagram;
import ru.tandemservice.nsiclient.utils.INsiBean;

/**
 * @author Andrey Nikonov
 * @since 01.09.2015
 */
public class NsiBean implements INsiBean {

    public IXDatagram createXDatagram() {
        return new XDatagram();
    }
}