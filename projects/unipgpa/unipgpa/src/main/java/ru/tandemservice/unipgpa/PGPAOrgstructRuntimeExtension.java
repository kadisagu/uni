package ru.tandemservice.unipgpa;

import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.unipgpa.dao.IPGPAOrgstructDao;

/**
 * @author root
 */
public class PGPAOrgstructRuntimeExtension implements IRuntimeExtension {
    private IPGPAOrgstructDao dao;
    public void setDao(final IPGPAOrgstructDao dao) { this.dao = dao; }

    @Override
    public void init(final Object object) {
        dao.createOrgstructHierarhy();
    }

    @Override
    public void destroy() {
    }
}
