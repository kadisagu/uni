package ru.tandemservice.unipgpa.dao;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author root
 */
public class PGPAOrgstructDao extends UniBaseDao implements IPGPAOrgstructDao {
    @Override
    public void createOrgstructHierarhy() {
        TopOrgUnit academy = TopOrgUnit.getInstance();
        if (academy == null)
        {
            academy = new TopOrgUnit();
            academy.setTitle("Пермская государственная фармацевтическая академия Федерального агентства по здравоохранению и социальному развитию");
            academy.setShortTitle("ПГФА");
            academy.setOrgUnitType(UniDaoFacade.getCoreDao().get(OrgUnitType.class, OrgUnitType.P_CODE, "academy"));
            save(academy);
        }
    }
}
