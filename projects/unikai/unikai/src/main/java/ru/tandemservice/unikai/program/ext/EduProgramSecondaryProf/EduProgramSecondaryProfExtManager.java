/*$Id$*/
package ru.tandemservice.unikai.program.ext.EduProgramSecondaryProf;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 03.07.2014
 */
@Configuration
public class EduProgramSecondaryProfExtManager extends BusinessObjectExtensionManager
{
}
