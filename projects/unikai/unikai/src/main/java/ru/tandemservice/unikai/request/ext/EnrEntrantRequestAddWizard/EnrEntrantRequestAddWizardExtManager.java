/*$Id$*/
package ru.tandemservice.unikai.request.ext.EnrEntrantRequestAddWizard;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 30.06.2014
 */
@Configuration
public class EnrEntrantRequestAddWizardExtManager extends BusinessObjectExtensionManager
{
}
