/*$Id$*/
package ru.tandemservice.unikai.request.ext.EnrEntrantRequestAddWizard.ui.AddInfoStep;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AddInfoStep.EnrEntrantRequestAddWizardAddInfoStepUI;

/**
 * @author DMITRY KNYAZEV
 * @since 30.06.2014
 */
public class EnrEntrantRequestAddWizardAddInfoStepExtUI extends UIAddon
{
	public EnrEntrantRequestAddWizardAddInfoStepExtUI(IUIPresenter presenter, String name, String componentId)
	{
		super(presenter, name, componentId);
	}

	@Override
	public void onComponentRefresh()
	{
		((EnrEntrantRequestAddWizardAddInfoStepUI)getPresenter()).setWithForeignLanguage(false);
	}
}
