/*$Id$*/
package ru.tandemservice.unikai.request.ext.EnrEntrantRequestAddWizard.ui.AddInfoStep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AddInfoStep.EnrEntrantRequestAddWizardAddInfoStep;

/**
 * @author DMITRY KNYAZEV
 * @since 30.06.2014
 */
@Configuration
public class EnrEntrantRequestAddWizardAddInfoStepExt extends BusinessComponentExtensionManager
{
	public static final String ADDON_NAME = "kai" + EnrEntrantRequestAddWizardAddInfoStepExtUI.class.getSimpleName();

	@Autowired
	private EnrEntrantRequestAddWizardAddInfoStep _sessionTransferInsidePub;


	@Bean
	public PresenterExtension presenterExtension()
	{
		return presenterExtensionBuilder(_sessionTransferInsidePub.presenterExtPoint())
				.addAddon(uiAddon(ADDON_NAME, EnrEntrantRequestAddWizardAddInfoStepExtUI.class))
				.create();
	}
}
