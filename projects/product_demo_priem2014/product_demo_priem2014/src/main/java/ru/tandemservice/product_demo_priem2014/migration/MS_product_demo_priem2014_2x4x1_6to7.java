package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisettle отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unisettle") )
				throw new RuntimeException("Module 'unisettle' is not deleted");
		}

		// удалить сущность unisettleResident
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("settle_resident", "personrole_t");

			// удалить таблицу
			tool.dropTable("settle_resident", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleResident");

		}

		// удалить сущность unisettleTemplateDocument
		{
			// удалить таблицу
			tool.dropTable("unisettletemplatedocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleTemplateDocument");

		}

		// удалить сущность unisettleSettlementKind
		{
			// удалить таблицу
			tool.dropTable("settle_settlement_kind", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleSettlementKind");

		}

		// удалить сущность unisettleRequestState
		{
			// удалить таблицу
			tool.dropTable("settle_request_state", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleRequestState");

		}

		// удалить сущность unisettleRequest
		{
			// удалить таблицу
			tool.dropTable("settle_request", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleRequest");

		}

		// удалить сущность unisettleContractType
		{
			// удалить таблицу
			tool.dropTable("settle_contract_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleContractType");

		}

		// удалить сущность unisettleContractState
		{
			// удалить таблицу
			tool.dropTable("settle_contract_state", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleContractState");

		}

		// удалить сущность unisettleContractResident
		{
			// удалить таблицу
			tool.dropTable("settle_contract_resident", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleContractResident");

		}

		// удалить сущность unisettleContractAccomodation
		{
			// удалить таблицу
			tool.dropTable("settle_contract_accomodation", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleContractAccomodation");

		}

		// удалить сущность unisettleSumAreaContract
		{
			// удалить таблицу
			tool.dropTable("settle_area_contract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleSumAreaContract");

		}

		// удалить сущность unisettleSingleBedContract
		{
			// удалить таблицу
			tool.dropTable("settle_single_contract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleSingleBedContract");

		}

		// удалить сущность unisettleContract
		{
			// удалить таблицу
			tool.dropTable("settle_contract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisettleContract");

		}


    }
}