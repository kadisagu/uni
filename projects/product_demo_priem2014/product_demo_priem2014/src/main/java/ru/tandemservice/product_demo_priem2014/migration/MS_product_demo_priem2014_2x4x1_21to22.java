package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_21to22 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль ctr отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("ctr") )
				throw new RuntimeException("Module 'ctr' is not deleted");
		}

		// удалить персистентный интерфейс org.tandemframework.shared.ctr.base.entity.ICtrPriceElement
		{
			// удалить view
			tool.dropView("ictrpriceelement_v");

		}

		// удалить персистентный интерфейс org.tandemframework.shared.ctr.base.entity.ICtrActivityBaseOwner
		{
			// удалить view
			tool.dropView("ictractivitybaseowner_v");

		}

		// удалить сущность physicalContactor
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("physicalContactor");

		}

		// удалить сущность juridicalContactor
		{
			// удалить таблицу
			tool.dropTable("juridicalcontactor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("juridicalContactor");

		}

		// удалить сущность employeePostContactor
		{
			// удалить таблицу
			tool.dropTable("employeepostcontactor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePostContactor");

		}

		// удалить сущность contactorPerson
		{
			// удалить таблицу
			tool.dropTable("contactorperson_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("contactorPerson");

		}

		// удалить сущность scopeOfActivity
		{
			// удалить таблицу
			tool.dropTable("scopeofactivity_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("scopeOfActivity");

		}

		// удалить сущность organizationType
		{
			// удалить таблицу
			tool.dropTable("organizationtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("organizationType");

		}

		// удалить сущность legalFormGroup
		{
			// удалить таблицу
			tool.dropTable("legalformgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("legalFormGroup");

		}

		// удалить сущность legalForm
		{
			// удалить таблицу
			tool.dropTable("legalform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("legalForm");

		}

		// удалить сущность externalOrgUnit
		{
			// удалить таблицу
			tool.dropTable("externalorgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("externalOrgUnit");

		}

		// удалить сущность ctrTemplate
		{
			// удалить таблицу
			tool.dropTable("ctrtemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrTemplate");

		}

		// удалить сущность ctrPricePaymentGrid
		{
			// удалить таблицу
			tool.dropTable("ctrpr_c_paymentgrid", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPricePaymentGrid");

		}

		// удалить сущность ctrPriceElementPeriod
		{
			// удалить таблицу
			tool.dropTable("ctrpr_prel_period", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPriceElementPeriod");

		}

		// удалить сущность ctrPriceElementCostStage
		{
			// удалить таблицу
			tool.dropTable("ctrpr_prelcost_stage", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPriceElementCostStage");

		}

		// удалить сущность ctrPriceElementCost
		{
			// удалить таблицу
			tool.dropTable("ctrpr_prel_cost", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPriceElementCost");

		}

		// удалить сущность ctrPriceCategory
		{
			// удалить таблицу
			tool.dropTable("ctrpr_c_category", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPriceCategory");

		}

		// удалить сущность ctrPaymentOrderType
		{
			// удалить таблицу
			tool.dropTable("ctrpaymentordertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPaymentOrderType");

		}

		// удалить сущность ctrPaymentDocumentType
		{
			// удалить таблицу
			tool.dropTable("ctrpaymentdocumenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPaymentDocumentType");

		}

		// удалить сущность ctrIOPaymentSource
		{
			// удалить таблицу
			tool.dropTable("ctr_io_payment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrIOPaymentSource");

		}

		// удалить сущность ctrIOContractInfo
		{
			// удалить таблицу
			tool.dropTable("ctr_io_contract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrIOContractInfo");

		}

		// удалить сущность ctrContractVersionTemplateData
		{
			// удалить таблицу
			tool.dropTable("ctr_version_template_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractVersionTemplateData");

		}

		// удалить сущность ctrContractVersionContractorRole
		{
			// удалить таблицу
			tool.dropTable("ctr_contractor_role_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractVersionContractorRole");

		}

		// удалить сущность ctrContractVersionContractor
		{
			// удалить таблицу
			tool.dropTable("ctr_contractor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractVersionContractor");

		}

		// удалить сущность ctrContractVersionAttachment
		{
			// удалить таблицу
			tool.dropTable("ctr_contractver_print_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractVersionAttachment");

		}

		// удалить сущность ctrContractVersion
		{
			// удалить таблицу
			tool.dropTable("ctr_contractver_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractVersion");

		}

		// удалить сущность ctrContractTypePromice
		{
			// удалить таблицу
			tool.dropTable("ctr_contracttype_promice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractTypePromice");

		}

		// удалить сущность ctrContractType
		{
			// удалить таблицу
			tool.dropTable("ctrcontracttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractType");

		}

		// удалить сущность ctrContractRole
		{
			// удалить таблицу
			tool.dropTable("ctr_c_contract_role_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractRole");

		}

		// удалить сущность ctrPaymentResult
		{
			// удалить таблицу
			tool.dropTable("ctr_res_payment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPaymentResult");

		}

		// удалить сущность ctrPaymentOrder
		{
			// удалить таблицу
			tool.dropTable("ctr_res_payorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPaymentOrder");

		}

		// удалить сущность ctrContractResult
		{
			// удалить таблицу
			tool.dropTable("ctr_result_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractResult");

		}

		// удалить сущность ctrContractPromiceType
		{
			// удалить таблицу
			tool.dropTable("ctrcontractpromicetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractPromiceType");

		}

		// удалить сущность ctrPaymentPromice
		{
			// удалить таблицу
			tool.dropTable("ctr_pr_payment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPaymentPromice");

		}

		// удалить сущность ctrPaymentGarantorPromice
		{
			// удалить таблицу
			tool.dropTable("ctr_pr_garantor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrPaymentGarantorPromice");

		}

		// удалить сущность ctrContractPromice
		{
			// удалить таблицу
			tool.dropTable("ctr_promice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractPromice");

		}

		// удалить сущность ctrContractObject
		{
			// удалить таблицу
			tool.dropTable("ctr_contractobj_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrContractObject");

		}

		// удалить сущность ctrActivityBaseType
		{
			// удалить таблицу
			tool.dropTable("ctractivitybasetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrActivityBaseType");

		}

		// удалить сущность contactorPersonActivityBase
		{
			// удалить таблицу
			tool.dropTable("contactorpersonactivitybase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("contactorPersonActivityBase");

		}
    }
}