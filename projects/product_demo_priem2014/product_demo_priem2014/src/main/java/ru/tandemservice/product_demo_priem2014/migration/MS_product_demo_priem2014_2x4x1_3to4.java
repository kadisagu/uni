package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unienr13_fis отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unienr13_fis") )
				throw new RuntimeException("Module 'unienr13_fis' is not deleted");
		}

		// удалить сущность enrFisSyncSession
		{
			// удалить таблицу
			tool.dropTable("enrfis_session_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncSession");

		}

		// удалить сущность enrFisSyncPackageIOLog
		{
			// удалить таблицу
			tool.dropTable("enrfis_package_iolog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncPackageIOLog");

		}

		// удалить сущность enrFisSyncPackage4CampaignInfo
		{
			// удалить таблицу
			tool.dropTable("enrfis_package_1campaign_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncPackage4CampaignInfo");

		}

		// удалить сущность enrFisSyncPackage4ApplicationsInfo
		{
			// удалить таблицу
			tool.dropTable("enrfis_package_3applications_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncPackage4ApplicationsInfo");

		}

		// удалить сущность enrFisSyncPackage4AdmissionInfo
		{
			// удалить таблицу
			tool.dropTable("enrfis_package_2admission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncPackage4AdmissionInfo");

		}

		// удалить сущность enrFisSyncPackage4AOrdersInfo
		{
			// удалить таблицу
			tool.dropTable("enrfis_package_4orders_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncPackage4AOrdersInfo");

		}

		// удалить сущность enrFisSyncPackage
		{
			// удалить таблицу
			tool.dropTable("enrfis_package_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSyncPackage");

		}

		// удалить сущность enrFisSettingsECPeriodKey
		{
			// удалить таблицу
			tool.dropTable("enrfis_s_period_key_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSettingsECPeriodKey");

		}

		// удалить сущность enrFisSettingsECPeriodItem
		{
			// удалить таблицу
			tool.dropTable("enrfis_s_period_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisSettingsECPeriodItem");

		}

		// удалить сущность enrFisEntrantSyncData
		{
			// удалить таблицу
			tool.dropTable("enrfis_entrant_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisEntrantSyncData");

		}

		// удалить сущность enrFisConv4Olympiad
		{
			// удалить таблицу
			tool.dropTable("enrfis_conv_olympiad_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisConv4Olympiad");

		}

		// удалить сущность enrFisConv4ExamSetElement
		{
			// удалить таблицу
			tool.dropTable("enrfis_conv_examsetelement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisConv4ExamSetElement");

		}

		// удалить сущность enrFisConv4EduOu
		{
			// удалить таблицу
			tool.dropTable("enrfis_conv_eduou_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisConv4EduOu");

		}

		// удалить сущность enrFisConv4Country
		{
			// удалить таблицу
			tool.dropTable("enrfis_conv_country_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisConv4Country");

		}

		// удалить сущность enrFisCatalogItem
		{
			// удалить таблицу
			tool.dropTable("enrfis_catalog_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrFisCatalogItem");

		}
    }
}