package ru.tandemservice.product_demo_priem2014.migration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.IResultProcessor;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.14"),
            new ScriptDependency("org.tandemframework.shared", "1.4.2"),
        };
    }

    @Override
    public void run(final DBTool tool) throws Exception
    {
        final String[][] fields = {
            {"contactorPerson_t", "email_p"},
            {"externalOrgUnit_t", "emailList_p"},
            {"contractor_t", "email_p"},
            {"contactPerson_t", "email_p"},
            {"orgUnit_t", "email_p"},
            {"orgUnit_t", "webpage_p"},
            {"sc_ag_nperson", "homePhoneNumber_p"},
            {"sc_ag_nperson", "mobilePhoneNumber_p"},
            {"contactorPerson_t", "phone_p"},
            {"externalOrgUnit_t", "phone_p"},
            {"contractor_t", "phone_p"},
            {"contactPerson_t", "workPhone_p"},
            {"contactPerson_t", "mobilePhone_p"},
            {"contactPerson_t", "phone_p"},
            {"employeePost_t", "phone_p"},
            {"employeePost_t", "innerPhone_p"},
            {"employeePost_t", "mobilePhone_p"},
            {"onlineEntrant_t", "nextOfKinPhone_p"},
            {"orgUnit_t", "phone_p"},
            {"orgUnit_t", "internalPhone_p"},
            {"orgUnit_t", "fax_p"},
            {"personContactData_t", "phoneReg_p"},
            {"personContactData_t", "phoneRegTemp_p"},
            {"personContactData_t", "phoneFact_p"},
            {"personContactData_t", "phoneDefault_p"},
            {"personContactData_t", "phoneWork_p"},
            {"personContactData_t", "phoneMobile_p"},
            {"personContactData_t", "phoneRelatives_p"},
            {"personContactData_t", "email_p"},
        };

        // заменяем все буквы (на буквы) и цифры (на цифры), остальное - не трогаем
        for (final String[] field: fields) {
            System.out.println(field[0]+"."+field[1]);
            if (tool.tableExists(field[0])) {
                final IResultProcessor<Map<Long, String>> processor = new IResultProcessor<Map<Long, String>>() {
                    @Override public Map<Long, String> process(final IDBConnect ctool, final ResultSet rs) throws SQLException {
                        final Map<Long, String> result = new HashMap<Long, String>(1024);
                        while (rs.next()) {
                            String string = rs.getString(2);
                            if (null == StringUtils.trimToNull(string)) { continue; }
                            result.put(((Number)rs.getObject(1)).longValue(), string);
                        }
                        return result;
                    }
                };
                final Map<Long, String> result = tool.executeQuery(processor, "select id,"+field[1]+" from "+field[0]);
                for (final Map.Entry<Long, String> e: result.entrySet()) {
                    final String value = shuffle(e.getValue());
                    tool.executeUpdate("update "+field[0]+" set "+field[1]+"=? where id=?", value, e.getKey());
                }
            }
        }
    }

    private final Random random = new Random();
    private String shuffle(final String value) {
        if (null == StringUtils.trimToNull(value)) { return value; }

        final char[] array = value.toCharArray();
        final List<Integer> digits = new ArrayList<>(array.length);
        final List<Integer> alphas = new ArrayList<>(array.length);

        for (int i=0;i<array.length;i++) {
            final char ch = array[i];
            if (Character.isSpaceChar(ch)) { continue; }
            if (Character.isDigit(ch)) {
                digits.add(i);
            } else if (Character.isUpperCase(ch) || Character.isLowerCase(ch)) {
                alphas.add(i);
            }
        }

        if (alphas.size() > 1) {
            Collections.shuffle(alphas);
            for (int i=0;i<alphas.size();i++) {
                int i0 = alphas.get(i);
                int i1 = alphas.get((i+1)%alphas.size());
                final char ch = array[i0];
                array[i0] = array[i1];
                array[i1] = ch;
            }
        }

        if (digits.size() > 1) {
            Collections.shuffle(digits);
            for (int i=0;i<digits.size();i++) {
                int i0 = digits.get(i);
                int i1 = digits.get((i+1)%digits.size());
                final char ch = array[i0];
                array[i0] = array[i1];
                array[i1] = ch;
            }
        }

        return new String(array);
    }
}