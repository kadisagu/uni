package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль moveemployee отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("moveemployee") )
				throw new RuntimeException("Module 'moveemployee' is not deleted");
		}

		// удалить сущность vacationScheduleItemToEmployeeExtractRelation
		{
			// удалить таблицу
			tool.dropTable("vctnschdlitmtemplyextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("vacationScheduleItemToEmployeeExtractRelation");

		}

		// удалить сущность transferHolidayDateToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("trnsfrhldydttextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferHolidayDateToExtractRelation");

		}

		// удалить сущность staffRateToFinSrcChangeListExtractRelation
		{
			// удалить таблицу
			tool.dropTable("stffrttfnsrcchnglstextrctrlt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffRateToFinSrcChangeListExtractRelation");

		}

		// удалить сущность scheduleItemDataToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("schdlitmdttextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("scheduleItemDataToExtractRelation");

		}

		// удалить сущность removeEmployeePaymentToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("rmvemplypymnttextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("removeEmployeePaymentToExtractRelation");

		}

		// удалить сущность provideDateToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("providedatetoextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("provideDateToExtractRelation");

		}

		// удалить сущность postInListExtractRelation
		{
			// удалить таблицу
			tool.dropTable("postinlistextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("postInListExtractRelation");

		}

		// удалить сущность periodPartToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("periodparttoextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("periodPartToExtractRelation");

		}

		// удалить сущность paymentStringRepresentationFormat
		{
			// удалить таблицу
			tool.dropTable("pymntstrngrprsnttnfrmt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentStringRepresentationFormat");

		}

		// удалить сущность paymentStringFormatDocumentSettings
		{
			// удалить таблицу
			tool.dropTable("pymntstrngfrmtdcmntsttngs_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentStringFormatDocumentSettings");

		}

		// удалить сущность paymentPrintFormatLabel
		{
			// удалить таблицу
			tool.dropTable("paymentprintformatlabel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentPrintFormatLabel");

		}

		// удалить сущность orgUnitInListExtractRelation
		{
			// удалить таблицу
			tool.dropTable("orgunitinlistextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orgUnitInListExtractRelation");

		}

		// удалить сущность newVacationScheduleToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("nwvctnschdltextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("newVacationScheduleToExtractRelation");

		}

		// удалить сущность newPartInTransferAnnualHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("nwprtintrnsfrannlhldyextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("newPartInTransferAnnualHolidayExtract");

		}

		// удалить сущность moveemployeeTemplate
		{
			// удалить таблицу
			tool.dropTable("moveemployeetemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("moveemployeeTemplate");

		}

		// удалить сущность holidayToTransferAnnualHolidayExtRelation
		{
			// удалить таблицу
			tool.dropTable("hldyttrnsfrannlhldyextrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayToTransferAnnualHolidayExtRelation");

		}

		// удалить сущность holidayInEmployeeExtractRelation
		{
			// удалить таблицу
			tool.dropTable("hldyinemplyextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayInEmployeeExtractRelation");

		}

		// удалить сущность holidayInEmpHldyExtractRelation
		{
			// удалить таблицу
			tool.dropTable("hldyinemphldyextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayInEmpHldyExtractRelation");

		}

		// удалить сущность holidayDataToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("holidaydatatoextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayDataToExtractRelation");

		}

		// удалить сущность financingSourceDetailsToAllocItem
		{
			// удалить таблицу
			tool.dropTable("fnncngsrcdtlstallcitm_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("financingSourceDetailsToAllocItem");

		}

		// удалить сущность financingSourceDetails
		{
			// удалить таблицу
			tool.dropTable("financingsourcedetails_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("financingSourceDetails");

		}

		// удалить сущность encouragementToPrintFormatRelation
		{
			// удалить таблицу
			tool.dropTable("ncrgmnttprntfrmtrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("encouragementToPrintFormatRelation");

		}

		// удалить сущность encouragementToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("ncrgmnttextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("encouragementToExtractRelation");

		}

		// удалить сущность employeeTypeInListExtractRelation
		{
			// удалить таблицу
			tool.dropTable("mplytypinlstextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTypeInListExtractRelation");

		}

		// удалить сущность employeeReasonToTypeRel
		{
			// удалить таблицу
			tool.dropTable("employeereasontotyperel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeReasonToTypeRel");

		}

		// удалить сущность employeeReasonToTextParagraphRel
		{
			// удалить таблицу
			tool.dropTable("mplyrsnttxtprgrphrl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeReasonToTextParagraphRel");

		}

		// удалить сущность employeeReasonToDismissReasonRel
		{
			// удалить таблицу
			tool.dropTable("mplyrsntdsmssrsnrl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeReasonToDismissReasonRel");

		}

		// удалить сущность employeeReasonToBasicRel
		{
			// удалить таблицу
			tool.dropTable("employeereasontobasicrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeReasonToBasicRel");

		}

		// удалить сущность employeeOrderTextRelation
		{
			// удалить таблицу
			tool.dropTable("employeeordertextrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeOrderTextRelation");

		}

		// удалить сущность employeeOrderReasons
		{
			// удалить таблицу
			tool.dropTable("employeeorderreasons_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeOrderReasons");

		}

		// удалить сущность employeeOrderBasics
		{
			// удалить таблицу
			tool.dropTable("employeeorderbasics_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeOrderBasics");

		}

		// удалить сущность employeeModularOrderParagraphsSortRule
		{
			// удалить таблицу
			tool.dropTable("mplymdlrordrprgrphssrtrl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeModularOrderParagraphsSortRule");

		}

		// удалить сущность employeeHolidayToEmployeeExtractRelation
		{
			// удалить таблицу
			tool.dropTable("mplyhldytemplyextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeHolidayToEmployeeExtractRelation");

		}

		// удалить сущность employeeHolidayToEmpHldyExtractRelation
		{
			// удалить таблицу
			tool.dropTable("mplyhldytemphldyextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeHolidayToEmpHldyExtractRelation");

		}

		// удалить сущность employeeExtractType
		{
			// удалить таблицу
			tool.dropTable("employeeextracttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeExtractType");

		}

		// удалить сущность employeeExtractTextRelation
		{
			// удалить таблицу
			tool.dropTable("employeeextracttextrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeExtractTextRelation");

		}

		// удалить сущность employeeDismissReasons
		{
			// удалить таблицу
			tool.dropTable("employeedismissreasons_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeDismissReasons");

		}

		// удалить сущность employeeBonus
		{
			// удалить таблицу
			tool.dropTable("employeebonus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeBonus");

		}

		// удалить сущность emplListOrderToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("empllistordertobasicrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("emplListOrderToBasicRelation");

		}

		// удалить сущность empSingleExtractToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("mpsnglextrcttbscrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("empSingleExtractToBasicRelation");

		}

		// удалить сущность empListExtractToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("mplstextrcttbscrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("empListExtractToBasicRelation");

		}

		// удалить сущность empExtractToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("empextracttobasicrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("empExtractToBasicRelation");

		}

		// удалить сущность creationExtractAsOrderRule
		{
			// удалить таблицу
			tool.dropTable("creationextractasorderrule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("creationExtractAsOrderRule");

		}

		// удалить сущность createHolidayInRevFromHolidayExtRelation
		{
			// удалить таблицу
			tool.dropTable("crthldyinrvfrmhldyextrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("createHolidayInRevFromHolidayExtRelation");

		}

		// удалить сущность createEncouragementInExtractRelation
		{
			// удалить таблицу
			tool.dropTable("crtencrgmntinextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("createEncouragementInExtractRelation");

		}

		// удалить сущность combinationPostStaffRateExtractRelation
		{
			// удалить таблицу
			tool.dropTable("cmbntnpststffrtextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("combinationPostStaffRateExtractRelation");

		}

		// удалить сущность combinationPostStaffRateExtAllocItemRelation
		{
			// удалить таблицу
			tool.dropTable("cmbntnpststffrtextallcitmrlt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("combinationPostStaffRateExtAllocItemRelation");

		}

		// удалить сущность employeeSingleExtractParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("employeeSingleExtractParagraph");

		}

		// удалить сущность employeeModularParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("employeeModularParagraph");

		}

		// удалить сущность employeeListParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("employeeListParagraph");

		}

		// удалить сущность abstractEmployeeParagraph
		{
			// удалить таблицу
			tool.dropTable("abstractemployeeparagraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("abstractEmployeeParagraph");

		}

		// удалить сущность employeeSingleExtractOrder
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("employeeSingleExtractOrder");

		}

		// удалить сущность employeeModularOrder
		{
			// удалить таблицу
			tool.dropTable("employeemodularorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeModularOrder");

		}

		// удалить сущность paragraphEmployeeListOrder
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("paragraphEmployeeListOrder");

		}

		// удалить сущность employeeListOrder
		{
			// удалить таблицу
			tool.dropTable("employeelistorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeListOrder");

		}

		// удалить сущность abstractEmployeeOrder
		{
			// удалить таблицу
			tool.dropTable("abstractemployeeorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("abstractEmployeeOrder");

		}

		// удалить сущность voluntaryTerminationSExtract
		{
			// удалить таблицу
			tool.dropTable("voluntaryterminationsextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("voluntaryTerminationSExtract");

		}

		// удалить сущность employeeTransferSExtract
		{
			// удалить таблицу
			tool.dropTable("employeetransfersextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTransferSExtract");

		}

		// удалить сущность employeeTempAddSExtract
		{
			// удалить таблицу
			tool.dropTable("employeetempaddsextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTempAddSExtract");

		}

		// удалить сущность employeePostAddSExtract
		{
			// удалить таблицу
			tool.dropTable("employeepostaddsextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePostAddSExtract");

		}

		// удалить сущность employeeHolidaySExtract
		{
			// удалить таблицу
			tool.dropTable("employeeholidaysextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeHolidaySExtract");

		}

		// удалить сущность employeeEncouragementSExtract
		{
			// удалить таблицу
			tool.dropTable("mplyencrgmntsextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeEncouragementSExtract");

		}

		// удалить сущность employeeAddSExtract
		{
			// удалить таблицу
			tool.dropTable("employeeaddsextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeAddSExtract");

		}

		// удалить сущность employeeAddPaymentsSExtract
		{
			// удалить таблицу
			tool.dropTable("employeeaddpaymentssextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeAddPaymentsSExtract");

		}

		// удалить сущность singleEmployeeExtract
		{
			// удалить таблицу
			tool.dropTable("singleemployeeextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("singleEmployeeExtract");

		}

		// удалить сущность voluntaryTerminationOwnWishExtract
		{
			// удалить таблицу
			tool.dropTable("vlntrytrmntnownwshextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("voluntaryTerminationOwnWishExtract");

		}

		// удалить сущность transferHolidayDueDiseaseExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrhldyddssextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferHolidayDueDiseaseExtract");

		}

		// удалить сущность transferAnnualHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("transferannualholidayextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferAnnualHolidayExtract");

		}

		// удалить сущность scienceStatusPaymentExtract
		{
			// удалить таблицу
			tool.dropTable("sciencestatuspaymentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("scienceStatusPaymentExtract");

		}

		// удалить сущность scienceDegreePaymentExtract
		{
			// удалить таблицу
			tool.dropTable("sciencedegreepaymentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("scienceDegreePaymentExtract");

		}

		// удалить сущность revocationFromHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("revocationfromholidayextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("revocationFromHolidayExtract");

		}

		// удалить сущность returnFromChildCareHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("rtrnfrmchldcrhldyextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("returnFromChildCareHolidayExtract");

		}

		// удалить сущность repealExtract
		{
			// удалить таблицу
			tool.dropTable("repealextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("repealExtract");

		}

		// удалить сущность removalCombinationExtract
		{
			// удалить таблицу
			tool.dropTable("removalcombinationextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("removalCombinationExtract");

		}

		// удалить сущность prolongationAnnualHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("prlngtnannlhldyextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("prolongationAnnualHolidayExtract");

		}

		// удалить сущность paymentAssigningExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("paymentAssigningExtract");

		}

		// удалить сущность incDecWorkExtract
		{
			// удалить таблицу
			tool.dropTable("incdecworkextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("incDecWorkExtract");

		}

		// удалить сущность incDecCombinationExtract
		{
			// удалить таблицу
			tool.dropTable("incdeccombinationextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("incDecCombinationExtract");

		}

		// удалить сущность employeeTransferExtract
		{
			// удалить таблицу
			tool.dropTable("employeetransferextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTransferExtract");

		}

		// удалить сущность employeeTempAddExtract
		{
			// удалить таблицу
			tool.dropTable("employeetempaddextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTempAddExtract");

		}

		// удалить сущность employeeSurnameChangeExtract
		{
			// удалить таблицу
			tool.dropTable("employeesurnamechangeextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeSurnameChangeExtract");

		}

		// удалить сущность employeeRetiringExtract
		{
			// удалить таблицу
			tool.dropTable("employeeretiringextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeRetiringExtract");

		}

		// удалить сущность employeePostPPSAddExtract
		{
			// удалить таблицу
			tool.dropTable("employeepostppsaddextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePostPPSAddExtract");

		}

		// удалить сущность employeePostAddExtract
		{
			// удалить таблицу
			tool.dropTable("employeepostaddextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePostAddExtract");

		}

		// удалить сущность employeeHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("employeeholidayextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeHolidayExtract");

		}

		// удалить сущность employeeAddExtract
		{
			// удалить таблицу
			tool.dropTable("employeeaddextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeAddExtract");

		}

		// удалить сущность employeeActingExtract
		{
			// удалить таблицу
			tool.dropTable("employeeactingextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeActingExtract");

		}

		// удалить сущность dismissalWEndOfLabourContrExtract
		{
			// удалить таблицу
			tool.dropTable("dsmsslwendoflbrcntrextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dismissalWEndOfLabourContrExtract");

		}

		// удалить сущность dismissalExtract
		{
			// удалить таблицу
			tool.dropTable("dismissalextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dismissalExtract");

		}

		// удалить сущность contractTerminationExtract
		{
			// удалить таблицу
			tool.dropTable("contractterminationextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("contractTerminationExtract");

		}

		// удалить сущность combinationPostPaymentExtract
		{
			// удалить таблицу
			tool.dropTable("cmbntnpstpymntextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("combinationPostPaymentExtract");

		}

		// удалить сущность changeHolidayExtract
		{
			// удалить таблицу
			tool.dropTable("changeholidayextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeHolidayExtract");

		}

		// удалить сущность agreedDismissalExtract
		{
			// удалить таблицу
			tool.dropTable("agreeddismissalextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("agreedDismissalExtract");

		}

		// удалить сущность modularEmployeeExtract
		{
			// удалить таблицу
			tool.dropTable("modularemployeeextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("modularEmployeeExtract");

		}

		// удалить сущность payForHolidayDayProvideHoldayEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("pyfrhldydyprvdhldyempllstext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("payForHolidayDayProvideHoldayEmplListExtract");

		}

		// удалить сущность payForHolidayDayEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("pyfrhldydyempllstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("payForHolidayDayEmplListExtract");

		}

		// удалить сущность employmentDismissalEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("mplymntdsmsslempllstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employmentDismissalEmplListExtract");

		}

		// удалить сущность employeeTransferEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("mplytrnsfrempllstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTransferEmplListExtract");

		}

		// удалить сущность employeePaymentEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("mplypymntempllstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePaymentEmplListExtract");

		}

		// удалить сущность employeeHolidayEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("mplyhldyempllstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeHolidayEmplListExtract");

		}

		// удалить сущность employeeAddEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("employeeaddempllistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeAddEmplListExtract");

		}

		// удалить сущность changeFinancingSourceEmplListExtract
		{
			// удалить таблицу
			tool.dropTable("chngfnncngsrcempllstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeFinancingSourceEmplListExtract");

		}

		// удалить сущность listEmployeeExtract
		{
			// удалить таблицу
			tool.dropTable("listemployeeextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("listEmployeeExtract");

		}

		// удалить сущность abstractEmployeeExtract
		{
			// удалить таблицу
			tool.dropTable("abstractemployeeextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("abstractEmployeeExtract");

		}
    }
}