package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_19to20 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniplaces отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniplaces") )
				throw new RuntimeException("Module 'uniplaces' is not deleted");
		}

		// удалить сущность uniplacesUnit
		{
			// удалить таблицу
			tool.dropTable("places_unit", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesUnit");

		}

		// удалить сущность uniplacesPlace
		{
			// удалить таблицу
			tool.dropTable("places_place", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesPlace");

		}

		// удалить сущность uniplacesGround
		{
			// удалить таблицу
			tool.dropTable("places_ground", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesGround");

		}

		// удалить сущность uniplacesFloor
		{
			// удалить таблицу
			tool.dropTable("places_floor", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesFloor");

		}

		// удалить сущность uniplacesBuilding
		{
			// удалить таблицу
			tool.dropTable("places_building", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesBuilding");

		}

		// удалить сущность uniplacesRegistryRecord
		{
			// удалить таблицу
			tool.dropTable("places_registry_record", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesRegistryRecord");

		}

		// удалить сущность uniplacesPlacePurpose
		{
			// удалить таблицу
			tool.dropTable("places_place_purpose", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesPlacePurpose");

		}

		// удалить сущность uniplacesPlaceNumberingType
		{
			// удалить таблицу
			tool.dropTable("places_place_numbering_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesPlaceNumberingType");

		}

		// удалить сущность uniplacesPlaceCondition
		{
			// удалить таблицу
			tool.dropTable("places_place_condition", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesPlaceCondition");

		}

		// удалить сущность uniplacesHabitablePlace
		{
			// удалить таблицу
			tool.dropTable("place_habitable_place", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesHabitablePlace");

		}

		// удалить сущность uniplacesDocumentPurpose
		{
			// удалить таблицу
			tool.dropTable("places_document_purpose", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDocumentPurpose");

		}

		// удалить сущность uniplacesDocumentKind
		{
			// удалить таблицу
			tool.dropTable("places_document_kind", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDocumentKind");

		}

		// удалить сущность uniplacesDocumentRight
		{
			// удалить таблицу
			tool.dropTable("places_document_right", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDocumentRight");

		}

		// удалить сущность uniplacesDocumentPlan
		{
			// удалить таблицу
			tool.dropTable("places_document_plan", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDocumentPlan");

		}

		// удалить сущность uniplacesDocumentAct
		{
			// удалить таблицу
			tool.dropTable("places_document_act", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDocumentAct");

		}

		// удалить сущность uniplacesDocument
		{
			// удалить таблицу
			tool.dropTable("places_document", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDocument");

		}

		// удалить сущность uniplacesDisposalRight
		{
			// удалить таблицу
			tool.dropTable("places_disposal_right", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesDisposalRight");

		}

		// удалить сущность uniplacesBuildingPurpose
		{
			// удалить таблицу
			tool.dropTable("places_building_purpose", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesBuildingPurpose");

		}

		// удалить сущность uniplacesAssignedDocument
		{
			// удалить таблицу
			tool.dropTable("places_assigned_document", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniplacesAssignedDocument");

		}


    }
}