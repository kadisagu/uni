package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unitraining отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unitraining") )
				throw new RuntimeException("Module 'unitraining' is not deleted");
		}

		// удалить персистентный интерфейс ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner
		{
			// удалить view
			tool.dropView("ibrssettingsowner_v");

		}

		// удалить сущность trOrgUnitSettings
		{
			// удалить таблицу
			tool.dropTable("tr_ou_settings", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trOrgUnitSettings");

		}

		// удалить сущность trJournalModule
		{
			// удалить таблицу
			tool.dropTable("tr_journal_module", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalModule");

		}

		// удалить сущность trJournalGroupStudent
		{
			// удалить таблицу
			tool.dropTable("tr_journal_group_student", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalGroupStudent");

		}

		// удалить сущность trJournalGroup
		{
			// удалить таблицу
			tool.dropTable("tr_journal_group", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalGroup");

		}

		// удалить сущность trEventLoad
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event_load", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEventLoad");

		}

		// удалить сущность trEventAddon
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event_addon", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEventAddon");

		}

		// удалить сущность trEventAction
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event_action", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEventAction");

		}

		// удалить сущность trJournalEvent
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalEvent");

		}

		// удалить сущность trJournal
		{
			// удалить таблицу
			tool.dropTable("tr_journal", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournal");

		}

		// удалить сущность trEduGroupEventStudent
		{
			// удалить таблицу
			tool.dropTable("tr_edugrp_event_student", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEduGroupEventStudent");

		}

		// удалить сущность trEduGroupEvent
		{
			// удалить таблицу
			tool.dropTable("tr_edugrp_event", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEduGroupEvent");

		}

		// удалить сущность trBrsCoefficientValue
		{
			// удалить таблицу
			tool.dropTable("tr_brs_coef_val", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trBrsCoefficientValue");

		}

		// удалить сущность trBrsCoefficientOwnerType
		{
			// удалить таблицу
			tool.dropTable("tr_c_coef_owner_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trBrsCoefficientOwnerType");

		}

		// удалить сущность trBrsCoefficientDef
		{
			// удалить таблицу
			tool.dropTable("tr_c_coef_def", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trBrsCoefficientDef");

		}

		// удалить сущность trAddonType
		{
			// удалить таблицу
			tool.dropTable("tr_c_addon_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trAddonType");

		}

		// удалить сущность trAbsenceReason
		{
			// удалить таблицу
			tool.dropTable("tr_c_absence_reason", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trAbsenceReason");

		}
    }
}