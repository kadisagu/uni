package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unispp отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unispp") )
				throw new RuntimeException("Module 'unispp' is not deleted");
		}

		// удалить сущность sppBellScheduleEntry
		{
			// удалить таблицу
			tool.dropTable("spp_bell_schedule_entry", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppBellScheduleEntry");

		}

		// удалить сущность sppBellSchedule
		{
			// удалить таблицу
			tool.dropTable("spp_bell_schedule", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppBellSchedule");

		}


    }
}