package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniemp отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniemp") )
				throw new RuntimeException("Module 'uniemp' is not deleted");
		}

		// удалить персистентный интерфейс ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract
		{
			// удалить view
			tool.dropView("chsrwtrnsfrannlhldyextrct_v");

		}

		// удалить сущность staffListReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("stafflistreport_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("stafflistreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListReport");

		}

		// удалить сущность staffListAllocationReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("stafflistallocationreport_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("stafflistallocationreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListAllocationReport");

		}

		// удалить сущность quantityEmpByDepReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("quantityempbydepreport_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("quantityempbydepreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("quantityEmpByDepReport");

		}

		// удалить сущность employeeVPO1Report
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("employeevpo1report_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("employeevpo1report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeVPO1Report");

		}

		// удалить сущность vacationScheduleItem
		{
			// удалить таблицу
			tool.dropTable("vacationscheduleitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("vacationScheduleItem");

		}

		// удалить сущность vacationSchedule
		{
			// удалить таблицу
			tool.dropTable("vacationschedule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("vacationSchedule");

		}

		// удалить сущность staffListState
		{
			// удалить таблицу
			tool.dropTable("staffliststate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListState");

		}

		// удалить сущность staffListRepPaymentSettings
		{
			// удалить таблицу
			tool.dropTable("stafflistreppaymentsettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListRepPaymentSettings");

		}

		// удалить сущность staffListFakePayment
		{
			// удалить таблицу
			tool.dropTable("stafflistfakepayment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListFakePayment");

		}

		// удалить сущность staffListPostPayment
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("staffListPostPayment");

		}

		// удалить сущность staffListPaymentBase
		{
			// удалить таблицу
			tool.dropTable("stafflistpaymentbase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListPaymentBase");

		}

		// удалить сущность staffListItem
		{
			// удалить таблицу
			tool.dropTable("stafflistitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListItem");

		}

		// удалить сущность staffListAllocationItem
		{
			// удалить таблицу
			tool.dropTable("stafflistallocationitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffListAllocationItem");

		}

		// удалить сущность staffList
		{
			// удалить таблицу
			tool.dropTable("stafflist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("staffList");

		}

		// удалить сущность sickListBasics
		{
			// удалить таблицу
			tool.dropTable("sicklistbasics_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sickListBasics");

		}

		// удалить сущность serviceLengthType
		{
			// удалить таблицу
			tool.dropTable("servicelengthtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("serviceLengthType");

		}

		// удалить сущность rosobrReports
		{
			// удалить таблицу
			tool.dropTable("rosobrreports_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("rosobrReports");

		}

		// удалить сущность rosobrReportToPaymentRel
		{
			// удалить таблицу
			tool.dropTable("rosobrreporttopaymentrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("rosobrReportToPaymentRel");

		}

		// удалить сущность rosobrReportStimPaymentPriority
		{
			// удалить таблицу
			tool.dropTable("rsbrrprtstmpymntprrty_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("rosobrReportStimPaymentPriority");

		}

		// удалить сущность rosobrReportPaymentPriority
		{
			// удалить таблицу
			tool.dropTable("rosobrreportpaymentpriority_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("rosobrReportPaymentPriority");

		}

		// удалить сущность resolutionCertificationCommission
		{
			// удалить таблицу
			tool.dropTable("rsltncrtfctncmmssn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("resolutionCertificationCommission");

		}

		// удалить сущность raisingCoefficientType
		{
			// удалить таблицу
			tool.dropTable("raisingcoefficienttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("raisingCoefficientType");

		}

		// удалить сущность postToServiceLengthTypeRelation
		{
			// удалить таблицу
			tool.dropTable("psttsrvclngthtyprltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("postToServiceLengthTypeRelation");

		}

		// удалить сущность pensionQualificationAgeSettings
		{
			// удалить таблицу
			tool.dropTable("pnsnqlfctnagsttngs_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("pensionQualificationAgeSettings");

		}

		// удалить сущность paymentUnit
		{
			// удалить таблицу
			tool.dropTable("paymentunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentUnit");

		}

		// удалить сущность paymentType
		{
			// удалить таблицу
			tool.dropTable("paymenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentType");

		}

		// удалить сущность paymentToPostBaseValue
		{
			// удалить таблицу
			tool.dropTable("paymenttopostbasevalue_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentToPostBaseValue");

		}

		// удалить сущность paymentOnFOTToPaymentsRelation
		{
			// удалить таблицу
			tool.dropTable("pymntonfottpymntsrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentOnFOTToPaymentsRelation");

		}

		// удалить сущность payment
		{
			// удалить таблицу
			tool.dropTable("payment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("payment");

		}

		// удалить сущность orgUnitPostToTypeRelation
		{
			// удалить таблицу
			tool.dropTable("orgunitposttotyperelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orgUnitPostToTypeRelation");

		}

		// удалить сущность orgUnitPostToScStatusRelation
		{
			// удалить таблицу
			tool.dropTable("rguntpsttscsttsrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orgUnitPostToScStatusRelation");

		}

		// удалить сущность orgUnitPostToScDegreeRelation
		{
			// удалить таблицу
			tool.dropTable("rguntpsttscdgrrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orgUnitPostToScDegreeRelation");

		}

		// удалить сущность orgUnitPostRelation
		{
			// удалить таблицу
			tool.dropTable("orgunitpostrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orgUnitPostRelation");

		}

		// удалить сущность medicalQualificationCategory
		{
			// удалить таблицу
			tool.dropTable("medicalqualificationcategory_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("medicalQualificationCategory");

		}

		// удалить сущность medicalEducationLevel
		{
			// удалить таблицу
			tool.dropTable("medicaleducationlevel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("medicalEducationLevel");

		}

		// удалить сущность labourContractType
		{
			// удалить таблицу
			tool.dropTable("labourcontracttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("labourContractType");

		}

		// удалить сущность holidayType
		{
			// удалить таблицу
			tool.dropTable("holidaytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayType");

		}

		// удалить сущность financingSourceItem
		{
			// удалить таблицу
			tool.dropTable("financingsourceitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("financingSourceItem");

		}

		// удалить сущность financingSource
		{
			// удалить таблицу
			tool.dropTable("financingsource_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("financingSource");

		}

		// удалить сущность encouragementType
		{
			// удалить таблицу
			tool.dropTable("encouragementtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("encouragementType");

		}

		// удалить сущность employmentHistoryItemInner
		{
			// удалить таблицу
			tool.dropTable("employmenthistoryiteminner_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employmentHistoryItemInner");

		}

		// удалить сущность employmentHistoryItemFake
		{
			// удалить таблицу
			tool.dropTable("employmenthistoryitemfake_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employmentHistoryItemFake");

		}

		// удалить сущность employmentHistoryItemExt
		{
			// удалить таблицу
			tool.dropTable("employmenthistoryitemext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employmentHistoryItemExt");

		}

		// удалить сущность employmentHistoryItemArch
		{
			// удалить таблицу
			tool.dropTable("employmenthistoryitemarch_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employmentHistoryItemArch");

		}

		// удалить сущность employmentHistoryItemBase
		{
			// удалить таблицу
			tool.dropTable("employmenthistoryitembase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employmentHistoryItemBase");

		}

		// удалить сущность employeeVPO1ReportLines
		{
			// удалить таблицу
			tool.dropTable("employeevpo1reportlines_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeVPO1ReportLines");

		}

		// удалить сущность employeeVPO1RepLineToPostType
		{
			// удалить таблицу
			tool.dropTable("mplyvpo1rplntpsttyp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeVPO1RepLineToPostType");

		}

		// удалить сущность employeeVPO1RepLineToPost
		{
			// удалить таблицу
			tool.dropTable("employeevpo1replinetopost_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeVPO1RepLineToPost");

		}

		// удалить сущность employeeTrainingItem
		{
			// удалить таблицу
			tool.dropTable("employeetrainingitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTrainingItem");

		}

		// удалить сущность employeeTemplateDocument
		{
			// удалить таблицу
			tool.dropTable("employeetemplatedocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeTemplateDocument");

		}

		// удалить сущность employeeSickList
		{
			// удалить таблицу
			tool.dropTable("employeesicklist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeSickList");

		}

		// удалить сущность employeeRetrainingItem
		{
			// удалить таблицу
			tool.dropTable("employeeretrainingitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeRetrainingItem");

		}

		// удалить сущность employeePostStaffRateItem
		{
			// удалить таблицу
			tool.dropTable("employeepoststaffrateitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePostStaffRateItem");

		}

		// удалить сущность employeePayment
		{
			// удалить таблицу
			tool.dropTable("employeepayment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePayment");

		}

		// удалить сущность employeeMedicalSpeciality
		{
			// удалить таблицу
			tool.dropTable("employeemedicalspeciality_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeMedicalSpeciality");

		}

		// удалить сущность employeeLabourContract
		{
			// удалить таблицу
			tool.dropTable("employeelabourcontract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeLabourContract");

		}

		// удалить сущность employeeHoliday
		{
			// удалить таблицу
			tool.dropTable("employeeholiday_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeHoliday");

		}

		// удалить сущность employeeExtractGroup
		{
			// удалить таблицу
			tool.dropTable("employeeextractgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeExtractGroup");

		}

		// удалить сущность employeeEncouragement
		{
			// удалить таблицу
			tool.dropTable("employeeencouragement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeEncouragement");

		}

		// удалить сущность employeeCertificationItem
		{
			// удалить таблицу
			tool.dropTable("employeecertificationitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeCertificationItem");

		}

		// удалить сущность employeeCard
		{
			// удалить таблицу
			tool.dropTable("employeecard_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeCard");

		}

		// удалить сущность employeeActingItem
		{
			// удалить таблицу
			tool.dropTable("employeeactingitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeeActingItem");

		}

		// удалить сущность empHistoryToServiceLengthTypeRelation
		{
			// удалить таблицу
			tool.dropTable("mphstrytsrvclngthtyprltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("empHistoryToServiceLengthTypeRelation");

		}

		// удалить сущность contractTermination
		{
			// удалить таблицу
			tool.dropTable("contracttermination_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("contractTermination");

		}

		// удалить сущность contractCollateralAgreement
		{
			// удалить таблицу
			tool.dropTable("contractcollateralagreement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("contractCollateralAgreement");

		}

		// удалить сущность combinationPostType
		{
			// удалить таблицу
			tool.dropTable("emp_comb_post_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("combinationPostType");

		}

		// удалить сущность combinationPostStaffRateItem
		{
			// удалить таблицу
			tool.dropTable("combinationpoststaffrateitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("combinationPostStaffRateItem");

		}

		// удалить сущность combinationPost
		{
			// удалить таблицу
			tool.dropTable("combinationpost_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("combinationPost");

		}


    }
}