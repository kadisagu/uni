package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unidpp отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unidpp") )
				throw new RuntimeException("Module 'unidpp' is not deleted");
		}

		// удалить персистентный интерфейс ru.tandemservice.unidpp.base.entity.request.IDppDemandExtract
		{
			// удалить view
			tool.dropView("idppdemandextract_v");

		}

		// удалить сущность excludeListenersDpoListExtract
		{
			// удалить таблицу
			tool.dropTable("dpp_stu_exclude_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeListenersDpoListExtract");

		}

		// удалить сущность dppWorkPlanCycleRow
		{
            tool.dropConstraint("epp_wprow_regel_t", "fk_id_bd4b75da");
            tool.dropConstraint("epp_wprow_part_load_t", "fk_row_eppworkplanrowpartload");
            tool.dropConstraint("epp_rgrp_g4ca_t", "fk_workplanrow_40908878");
            tool.dropConstraint("epp_rgrp_g4ld_t", "fk_workplanrow_9d1c5128");

			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("dpp_cycle", "epp_wprow_t", "epp_wprow_regel_t");

			// удалить таблицу
			tool.dropTable("dpp_cycle", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppWorkPlanCycleRow");

		}

		// удалить сущность dppRequestStage
		{
			// удалить таблицу
			tool.dropTable("dpp_c_req_stage", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppRequestStage");

		}

		// удалить сущность dppRequestRecord
		{
			// удалить таблицу
			tool.dropTable("dpp_request_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppRequestRecord");

		}

		// удалить сущность dppRequestDBFile
		{
			// удалить таблицу
			tool.dropTable("dpp_request_attachment", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppRequestDBFile");

		}

		// удалить сущность dppRequest
		{
			// удалить таблицу
			tool.dropTable("dpp_request", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppRequest");

		}

		// удалить сущность dppOrgUnit
		{
			// удалить таблицу
			tool.dropTable("dpp_orgunit", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppOrgUnit");

		}

		// удалить сущность dppOrderReasonToTypeRel
		{
			// удалить таблицу
			tool.dropTable("dpp_order_reason2doctp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppOrderReasonToTypeRel");

		}

		// удалить сущность dppOrderReasonToBasicRel
		{
			// удалить таблицу
			tool.dropTable("dpp_order_reason_to_basic_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppOrderReasonToBasicRel");

		}

		// удалить сущность dppOrderReason
		{
			// удалить таблицу
			tool.dropTable("dpp_c_order_reason", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppOrderReason");

		}

		// удалить сущность dppOrderBasic
		{
			// удалить таблицу
			tool.dropTable("dpp_c_order_basic", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppOrderBasic");

		}

		// удалить сущность dppMoveDocumentType
		{
			// удалить таблицу
			tool.dropTable("dpp_c_move_doc_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppMoveDocumentType");

		}

		// удалить сущность dppMoveDocTemplate
		{
			// удалить таблицу
			tool.dropTable("dpp_c_move_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppMoveDocTemplate");

		}

		// удалить сущность dppEnrollmentOrderToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("dpp_enrlmnt_order_to_basic_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppEnrollmentOrderToBasicRelation");

		}

		// удалить сущность dppEnrollmentOrderTextRelation
		{
			// удалить таблицу
			tool.dropTable("dpp_order_to_text_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppEnrollmentOrderTextRelation");

		}

		// удалить сущность dppEducationType
		{
			// удалить таблицу
			tool.dropTable("dpp_c_edu_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppEducationType");

		}

		// удалить сущность dppEduFlow
		{
			// удалить таблицу
			tool.dropTable("dpp_c_edu_flow", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppEduFlow");

		}

		// удалить сущность dppDocTemplate
		{
			// удалить таблицу
			tool.dropTable("dpp_c_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDocTemplate");

		}

		// удалить сущность dppDemandStage
		{
			// удалить таблицу
			tool.dropTable("dpp_c_dem_stage", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandStage");

		}

		// удалить сущность dppDemandExtractRecordPart
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_extract__row_part", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandExtractRecordPart");

		}

		// удалить сущность dppDemandExtractRecord
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_extract__row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandExtractRecord");

		}

		// удалить сущность dppDemandDBFile
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_attachment", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandDBFile");

		}

		// удалить сущность dppDemandContract
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_contract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandContract");

		}

		// удалить сущность dppDemand
		{
			// удалить таблицу
			tool.dropTable("dpp_demand", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemand");

		}

		// удалить сущность dppCtrDocTemplate
		{
			// удалить таблицу
			tool.dropTable("dpp_c_ctr_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppCtrDocTemplate");

		}

		// удалить сущность dppEnrollmentParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("dppEnrollmentParagraph");

		}

		// удалить сущность dppAbstractParagraph
		{
			// удалить таблицу
			tool.dropTable("dpp_paragraph", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppAbstractParagraph");

		}

		// удалить сущность dppEnrollmentOrder
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("dppEnrollmentOrder");

		}

		// удалить сущность dppAbstractOrder
		{
			// удалить таблицу
			tool.dropTable("dpp_order", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppAbstractOrder");

		}

		// удалить сущность dppDemandExtractStart
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_extract_start", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandExtractStart");

		}

		// удалить сущность dppDemandExtractReserve
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_extract_reserve", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandExtractReserve");

		}

		// удалить сущность dppDemandExtractAppend
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_extract_append", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandExtractAppend");

		}

		// удалить сущность dppDemandExtract
		{
			// удалить таблицу
			tool.dropTable("dpp_demand_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppDemandExtract");

		}

		// удалить сущность dppAbstractExtract
		{
			// удалить таблицу
			tool.dropTable("dpp_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dppAbstractExtract");

		}
    }
}