package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_16to17 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль movestudent отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("movestudent") )
				throw new RuntimeException("Module 'movestudent' is not deleted");
		}

		// удалить сущность studentReasonToTypeRel
		{
			// удалить таблицу
			tool.dropTable("studentreasontotyperel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentReasonToTypeRel");

		}

		// удалить сущность studentReasonToBasicRel
		{
			// удалить таблицу
			tool.dropTable("studentreasontobasicrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentReasonToBasicRel");

		}

		// удалить сущность studentOtherOrderTextRelation
		{
			// удалить таблицу
			tool.dropTable("stdntothrordrtxtrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentOtherOrderTextRelation");

		}

		// удалить сущность studentOrderTextRelation
		{
			// удалить таблицу
			tool.dropTable("studentordertextrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentOrderTextRelation");

		}

		// удалить сущность studentOrderReasons
		{
			// удалить таблицу
			tool.dropTable("studentorderreasons_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentOrderReasons");

		}

		// удалить сущность studentOrderBasics
		{
			// удалить таблицу
			tool.dropTable("studentorderbasics_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentOrderBasics");

		}

		// удалить сущность studentExtractTypeToGroup
		{
			// удалить таблицу
			tool.dropTable("studentextracttypetogroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentExtractTypeToGroup");

		}

		// удалить сущность studentExtractType
		{
			// удалить таблицу
			tool.dropTable("studentextracttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentExtractType");

		}

		// удалить сущность studentExtractTextRelation
		{
			// удалить таблицу
			tool.dropTable("studentextracttextrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentExtractTextRelation");

		}

		// удалить сущность studentCustomStateToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("stdntcstmstttextrctrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentCustomStateToExtractRelation");

		}

		// удалить сущность stuOtherOrderToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("stuotherordertobasicrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuOtherOrderToBasicRelation");

		}

		// удалить сущность stuListOrderToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("stulistordertobasicrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuListOrderToBasicRelation");

		}

		// удалить сущность stuExtractToTermRelation
		{
			// удалить таблицу
			tool.dropTable("stuextracttotermrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuExtractToTermRelation");

		}

		// удалить сущность stuExtractToRePassDiscipline
		{
			// удалить таблицу
			tool.dropTable("stuextracttorepassdiscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuExtractToRePassDiscipline");

		}

		// удалить сущность stuExtractToDebtRelation
		{
			// удалить таблицу
			tool.dropTable("stuextracttodebtrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuExtractToDebtRelation");

		}

		// удалить сущность stuExtractToComissionMember
		{
			// удалить таблицу
			tool.dropTable("stuextracttocomissionmember_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuExtractToComissionMember");

		}

		// удалить сущность stuExtractToBasicRelation
		{
			// удалить таблицу
			tool.dropTable("stuextracttobasicrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stuExtractToBasicRelation");

		}

		// удалить сущность statusToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("statustoextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("statusToExtractRelation");

		}

		// удалить сущность otherStudentFieldsSettings
		{
			// удалить таблицу
			tool.dropTable("otherstudentfieldssettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("otherStudentFieldsSettings");

		}

		// удалить сущность otherOrgUnitExtractType
		{
			// удалить таблицу
			tool.dropTable("otherorgunitextracttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("otherOrgUnitExtractType");

		}

		// удалить сущность movestudentTemplate
		{
			// удалить таблицу
			tool.dropTable("movestudenttemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("movestudentTemplate");

		}

		// удалить сущность groupToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("grouptoextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("groupToExtractRelation");

		}

		// удалить сущность extractCreationRule
		{
			// удалить таблицу
			tool.dropTable("extractcreationrule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("extractCreationRule");

		}

		// удалить сущность degreeToExtractRelation
		{
			// удалить таблицу
			tool.dropTable("degreetoextractrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("degreeToExtractRelation");

		}

		// удалить сущность customParagraphPrintForm
		{
			// удалить таблицу
			tool.dropTable("customparagraphprintform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("customParagraphPrintForm");

		}

		// удалить сущность customOrderPrintForm
		{
			// удалить таблицу
			tool.dropTable("customorderprintform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("customOrderPrintForm");

		}

		// удалить сущность changeStudentStatusAction
		{
			// удалить таблицу
			tool.dropTable("changestudentstatusaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeStudentStatusAction");

		}

		// удалить сущность changeIdentityCardAction
		{
			// удалить таблицу
			tool.dropTable("changeidentitycardaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeIdentityCardAction");

		}

		// удалить сущность changeGroupCaptainAction
		{
			// удалить таблицу
			tool.dropTable("changegroupcaptainaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeGroupCaptainAction");

		}

		// удалить сущность changeGroupAction
		{
			// удалить таблицу
			tool.dropTable("changegroupaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeGroupAction");

		}

		// удалить сущность changeEducationOrgUnitAction
		{
			// удалить таблицу
			tool.dropTable("changeeducationorgunitaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeEducationOrgUnitAction");

		}

		// удалить сущность changeCustomStatesAction
		{
			// удалить таблицу
			tool.dropTable("changecustomstatesaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeCustomStatesAction");

		}

		// удалить сущность changeCustomStateActionRelation
		{
			// удалить таблицу
			tool.dropTable("chngcstmsttactnrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeCustomStateActionRelation");

		}

		// удалить сущность changeCourseAction
		{
			// удалить таблицу
			tool.dropTable("changecourseaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeCourseAction");

		}

		// удалить сущность changeCompensationTypeAction
		{
			// удалить таблицу
			tool.dropTable("changecompensationtypeaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeCompensationTypeAction");

		}

		// удалить сущность studentOtherParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("studentOtherParagraph");

		}

		// удалить сущность studentModularParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("studentModularParagraph");

		}

		// удалить сущность studentListParagraph
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("studentListParagraph");

		}

		// удалить сущность abstractStudentParagraph
		{
			// удалить таблицу
			tool.dropTable("abstractstudentparagraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("abstractStudentParagraph");

		}

		// удалить сущность studentOtherOrder
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("studentOtherOrder");

		}

		// удалить сущность studentModularOrder
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("studentModularOrder");

		}

		// удалить сущность studentListOrder
		{
			// удалить таблицу
			tool.dropTable("studentlistorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentListOrder");

		}

		// удалить сущность abstractStudentOrder
		{
			// удалить таблицу
			tool.dropTable("abstractstudentorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("abstractStudentOrder");

		}

		// удалить сущность otherStudentExtract
		{
			// удалить таблицу
			tool.dropTable("otherstudentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("otherStudentExtract");

		}

		// удалить сущность weekendStuExtractExt
		{
			// удалить таблицу
			tool.dropTable("weekendstuextractext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendStuExtractExt");

		}

		// удалить сущность weekendStuExtract
		{
			// удалить таблицу
			tool.dropTable("weekendstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendStuExtract");

		}

		// удалить сущность weekendPregnancyStuExtract
		{
			// удалить таблицу
			tool.dropTable("weekendpregnancystuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendPregnancyStuExtract");

		}

		// удалить сущность weekendPregnancyOutStuExtract
		{
			// удалить таблицу
			tool.dropTable("wkndprgnncyotstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendPregnancyOutStuExtract");

		}

		// удалить сущность weekendChildThreeStuExtract
		{
			// удалить таблицу
			tool.dropTable("weekendchildthreestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendChildThreeStuExtract");

		}

		// удалить сущность weekendChildStuExtract
		{
			// удалить таблицу
			tool.dropTable("weekendchildstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendChildStuExtract");

		}

		// удалить сущность weekendChildOneAndHalfStuExtract
		{
			// удалить таблицу
			tool.dropTable("wkndchldonandhlfstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendChildOneAndHalfStuExtract");

		}

		// удалить сущность weekendChildCareOutStuExtract
		{
			// удалить таблицу
			tool.dropTable("wkndchldcrotstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendChildCareOutStuExtract");

		}

		// удалить сущность transferIndPlanStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferindplanstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferIndPlanStuExtract");

		}

		// удалить сущность transferIndGraphStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferindgraphstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferIndGraphStuExtract");

		}

		// удалить сущность studentCardLossPenaltyStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("studentCardLossPenaltyStuExtract");

		}

		// удалить сущность socGrantStopStuExtract
		{
			// удалить таблицу
			tool.dropTable("socgrantstopstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("socGrantStopStuExtract");

		}

		// удалить сущность socGrantResumptionStuExtract
		{
			// удалить таблицу
			tool.dropTable("socgrantresumptionstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("socGrantResumptionStuExtract");

		}

		// удалить сущность socGrantPaymentStopStuExtract
		{
			// удалить таблицу
			tool.dropTable("scgrntpymntstpstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("socGrantPaymentStopStuExtract");

		}

		// удалить сущность socGrantAssignStuExtract
		{
			// удалить таблицу
			tool.dropTable("socgrantassignstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("socGrantAssignStuExtract");

		}

		// удалить сущность sessionProlongStuExtract
		{
			// удалить таблицу
			tool.dropTable("sessionprolongstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionProlongStuExtract");

		}

		// удалить сущность sessionIndividualStuExtract
		{
			// удалить таблицу
			tool.dropTable("sessionindividualstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionIndividualStuExtract");

		}

		// удалить сущность sessionIndividualAheadStuExtract
		{
			// удалить таблицу
			tool.dropTable("sssnindvdlahdstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionIndividualAheadStuExtract");

		}

		// удалить сущность revertOrderStuExtract
		{
			// удалить таблицу
			tool.dropTable("revertorderstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("revertOrderStuExtract");

		}

		// удалить сущность repeatProtectDiplomaWorkStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("repeatProtectDiplomaWorkStuExtract");

		}

		// удалить сущность repeatPassStateExamStuExtract
		{
			// удалить таблицу
			tool.dropTable("rptpsssttexmstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("repeatPassStateExamStuExtract");

		}

		// удалить сущность removeGroupManagerStuExtract
		{
			// удалить таблицу
			tool.dropTable("removegroupmanagerstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("removeGroupManagerStuExtract");

		}

		// удалить сущность recordBookLossPenaltyStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("recordBookLossPenaltyStuExtract");

		}

		// удалить сущность rePassDiscStuExtract
		{
			// удалить таблицу
			tool.dropTable("repassdiscstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("rePassDiscStuExtract");

		}

		// удалить сущность rePassDiscComissStuExtract
		{
			// удалить таблицу
			tool.dropTable("repassdisccomissstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("rePassDiscComissStuExtract");

		}

		// удалить сущность prolongWeekndChildStuExtract
		{
			// удалить таблицу
			tool.dropTable("prolongweekndchildstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("prolongWeekndChildStuExtract");

		}

		// удалить сущность prolongWeekendStuExtract
		{
			// удалить таблицу
			tool.dropTable("prolongweekendstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("prolongWeekendStuExtract");

		}

		// удалить сущность prescheduleSessionPassStuExtract
		{
			// удалить таблицу
			tool.dropTable("prschdlsssnpssstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("prescheduleSessionPassStuExtract");

		}

		// удалить сущность paymentDiscountStuExtract
		{
			// удалить таблицу
			tool.dropTable("paymentdiscountstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentDiscountStuExtract");

		}

		// удалить сущность modifyModularStuExtract
		{
			// удалить таблицу
			tool.dropTable("modifymodularstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("modifyModularStuExtract");

		}

		// удалить сущность matAidAssignStuExtract
		{
			// удалить таблицу
			tool.dropTable("mataidassignstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("matAidAssignStuExtract");

		}

		// удалить сущность holidayStuExtract
		{
			// удалить таблицу
			tool.dropTable("holidaystuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayStuExtract");

		}

		// удалить сущность groupAssignStuExtract
		{
			// удалить таблицу
			tool.dropTable("groupassignstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("groupAssignStuExtract");

		}

		// удалить сущность grantRiseStuExtract
		{
			// удалить таблицу
			tool.dropTable("grantrisestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("grantRiseStuExtract");

		}

		// удалить сущность giveDiplomaStuExtract
		{
			// удалить таблицу
			tool.dropTable("givediplomastuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplomaStuExtract");

		}

		// удалить сущность giveDiplomaDuplicateStuExtract
		{
			// удалить таблицу
			tool.dropTable("gvdplmdplctstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplomaDuplicateStuExtract");

		}

		// удалить сущность giveDiplIncompleteStuListExtract
		{
			// удалить таблицу
			tool.dropTable("gvdplincmpltstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplIncompleteStuListExtract");

		}

		// удалить сущность giveDiplAppDuplicateStuExtract
		{
			// удалить таблицу
			tool.dropTable("gvdplappdplctstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplAppDuplicateStuExtract");

		}

		// удалить сущность giveCardDuplicateStuExtract
		{
			// удалить таблицу
			tool.dropTable("givecardduplicatestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveCardDuplicateStuExtract");

		}

		// удалить сущность giveBookDuplicateStuExtract
		{
			// удалить таблицу
			tool.dropTable("givebookduplicatestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveBookDuplicateStuExtract");

		}

		// удалить сущность freeStudiesAttendanceStuExtract
		{
			// удалить таблицу
			tool.dropTable("frstdsattndncstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("freeStudiesAttendanceStuExtract");

		}

		// удалить сущность excludeUnacceptedToGPDefenceStuExtract
		{
			// удалить таблицу
			tool.dropTable("xcldunccptdtgpdfncstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeUnacceptedToGPDefenceStuExtract");

		}

		// удалить сущность excludeTransfStuExtract
		{
			// удалить таблицу
			tool.dropTable("excludetransfstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeTransfStuExtract");

		}

		// удалить сущность excludeStuExtractExt
		{
			// удалить таблицу
			tool.dropTable("excludestuextractext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeStuExtractExt");

		}

		// удалить сущность excludeStuExtract
		{
			// удалить таблицу
			tool.dropTable("excludestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeStuExtract");

		}

		// удалить сущность excludeStateExamStuExtract
		{
			// удалить таблицу
			tool.dropTable("excludestateexamstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeStateExamStuExtract");

		}

		// удалить сущность excludeGPDefenceFailStuExtract
		{
			// удалить таблицу
			tool.dropTable("xcldgpdfncflstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeGPDefenceFailStuExtract");

		}

		// удалить сущность disciplinaryPenaltyStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("disciplinaryPenaltyStuExtract");

		}

		// удалить сущность dischargingStuExtract
		{
			// удалить таблицу
			tool.dropTable("dischargingstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dischargingStuExtract");

		}

		// удалить сущность weekendOutStuExtract
		{
			// удалить таблицу
			tool.dropTable("weekendoutstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendOutStuExtract");

		}

		// удалить сущность weekendChildOutStuExtract
		{
			// удалить таблицу
			tool.dropTable("weekendchildoutstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("weekendChildOutStuExtract");

		}

		// удалить сущность transferTerritorialStuExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrtrrtrlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferTerritorialStuExtract");

		}

		// удалить сущность transferTerritorialExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrtrrtrlextstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferTerritorialExtStuExtract");

		}

		// удалить сущность transferStuExtractExt
		{
			// удалить таблицу
			tool.dropTable("transferstuextractext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferStuExtractExt");

		}

		// удалить сущность transferStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferStuExtract");

		}

		// удалить сущность transferProfileStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("transferProfileStuExtract");

		}

		// удалить сущность transferProfileGroupExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrprflgrpextstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferProfileGroupExtStuExtract");

		}

		// удалить сущность transferProfileExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferprofileextstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferProfileExtStuExtract");

		}

		// удалить сущность transferGroupStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("transferGroupStuExtract");

		}

		// удалить сущность transferFormativeStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferformativestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferFormativeStuExtract");

		}

		// удалить сущность transferEduTypeStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferedutypestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferEduTypeStuExtract");

		}

		// удалить сущность transferEduLevelStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferedulevelstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferEduLevelStuExtract");

		}

		// удалить сущность transferDevFormStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferdevformstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferDevFormStuExtract");

		}

		// удалить сущность transferDevCondStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferdevcondstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferDevCondStuExtract");

		}

		// удалить сущность transferDevCondExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("transferdevcondextstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferDevCondExtStuExtract");

		}

		// удалить сущность transferCourseWithChangeTerrOuStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("transferCourseWithChangeTerrOuStuExtract");

		}

		// удалить сущность transferCourseStuExtract
		{
			// удалить таблицу
			tool.dropTable("transfercoursestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferCourseStuExtract");

		}

		// удалить сущность transferCompTypeStuExtract
		{
			// удалить таблицу
			tool.dropTable("transfercomptypestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferCompTypeStuExtract");

		}

		// удалить сущность transferCompTypeExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrcmptypextstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferCompTypeExtStuExtract");

		}

		// удалить сущность transferBetweenTerritorialStuExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrbtwntrrtrlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferBetweenTerritorialStuExtract");

		}

		// удалить сущность restorationStuExtractExt
		{
			// удалить таблицу
			tool.dropTable("restorationstuextractext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationStuExtractExt");

		}

		// удалить сущность restorationStuExtract
		{
			// удалить таблицу
			tool.dropTable("restorationstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationStuExtract");

		}

		// удалить сущность restorationCourseStuExtract
		{
			// удалить таблицу
			tool.dropTable("restorationcoursestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationCourseStuExtract");

		}

		// удалить сущность restorationAdmitToDiplomaStuExtract
		{
			// удалить таблицу
			tool.dropTable("rstrtnadmttdplmstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationAdmitToDiplomaStuExtract");

		}

		// удалить сущность restorationAdmitToDiplomaDefStuExtract
		{
			// удалить таблицу
			tool.dropTable("rstrtnadmttdplmdfstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationAdmitToDiplomaDefStuExtract");

		}

		// удалить сущность restorationAdmitToDiplomaDefExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("rstrtnadmttdplmdfextstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationAdmitToDiplomaDefExtStuExtract");

		}

		// удалить сущность restorationAdmitStateExamsStuExtract
		{
			// удалить таблицу
			tool.dropTable("rstrtnadmtsttexmsstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationAdmitStateExamsStuExtract");

		}

		// удалить сущность restorationAdmitStateExamsExtStuExtract
		{
			// удалить таблицу
			tool.dropTable("rstrtnadmtsttexmsextstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("restorationAdmitStateExamsExtStuExtract");

		}

		// удалить сущность reEducationStuExtract
		{
			// удалить таблицу
			tool.dropTable("reeducationstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("reEducationStuExtract");

		}

		// удалить сущность reEducationFGOSStuExtract
		{
			// удалить таблицу
			tool.dropTable("reeducationfgosstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("reEducationFGOSStuExtract");

		}

		// удалить сущность profileAssignStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("profileAssignStuExtract");

		}

		// удалить сущность eduEnrolmentStuExtractExt
		{
			// удалить таблицу
			tool.dropTable("eduenrolmentstuextractext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduEnrolmentStuExtractExt");

		}

		// удалить сущность eduEnrolmentStuExtract
		{
			// удалить таблицу
			tool.dropTable("eduenrolmentstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduEnrolmentStuExtract");

		}

		// удалить сущность eduEnrAsTransferStuExtract
		{
			// удалить таблицу
			tool.dropTable("eduenrastransferstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduEnrAsTransferStuExtract");

		}

		// удалить сущность commonStuExtract
		{
			// удалить таблицу
			tool.dropTable("commonstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("commonStuExtract");

		}

		// удалить сущность changePassStateExamsStuExtract
		{
			// удалить таблицу
			tool.dropTable("chngpsssttexmsstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changePassStateExamsStuExtract");

		}

		// удалить сущность changePassDiplomaWorkPeriodStuExtract
		{
			// удалить таблицу
			tool.dropTable("chngpssdplmwrkprdstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changePassDiplomaWorkPeriodStuExtract");

		}

		// удалить сущность changeMiddleNameStuExtract
		{
			// удалить таблицу
			tool.dropTable("changemiddlenamestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeMiddleNameStuExtract");

		}

		// удалить сущность changeFirstNameStuExtract
		{
			// удалить таблицу
			tool.dropTable("changefirstnamestuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeFirstNameStuExtract");

		}

		// удалить сущность changeFioStuExtract
		{
			// удалить таблицу
			tool.dropTable("changefiostuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeFioStuExtract");

		}

		// удалить сущность changeDateStateExaminationStuExtract
		{
			// удалить таблицу
			tool.dropTable("chngdtsttexmntnstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeDateStateExaminationStuExtract");

		}

		// удалить сущность changeCategoryStuExtract
		{
			// удалить таблицу
			tool.dropTable("changecategorystuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeCategoryStuExtract");

		}

		// удалить сущность attestationPracticWithServiceRecStuExtract
		{
			// удалить таблицу
			tool.dropTable("ttsttnprctcwthsrvcrcstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("attestationPracticWithServiceRecStuExtract");

		}

		// удалить сущность attestationPracticWithProbationStuExtract
		{
			// удалить таблицу
			tool.dropTable("ttsttnprctcwthprbtnstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("attestationPracticWithProbationStuExtract");

		}

		// удалить сущность attestationPracticStuExtract
		{
			// удалить таблицу
			tool.dropTable("attestationpracticstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("attestationPracticStuExtract");

		}

		// удалить сущность assignBenefitStuExtract
		{
			// удалить таблицу
			tool.dropTable("assignbenefitstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("assignBenefitStuExtract");

		}

		// удалить сущность adoptRussianCitizenshipStuExtract
		{
			// удалить таблицу
			tool.dropTable("dptrssnctznshpstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("adoptRussianCitizenshipStuExtract");

		}

		// удалить сущность admitToStateExamsStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("admitToStateExamsStuExtract");

		}

		// удалить сущность admitToDiplomaWithoutExamStuExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("admitToDiplomaWithoutExamStuExtract");

		}

		// удалить сущность admitToDiplomaStuExtract
		{
			// удалить таблицу
			tool.dropTable("admittodiplomastuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("admitToDiplomaStuExtract");

		}

		// удалить сущность admissionStuExtract
		{
			// удалить таблицу
			tool.dropTable("admissionstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("admissionStuExtract");

		}

		// удалить сущность addGroupManagerStuExtract
		{
			// удалить таблицу
			tool.dropTable("addgroupmanagerstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("addGroupManagerStuExtract");

		}

		// удалить сущность acadGrantPaymentStopStuExtract
		{
			// удалить таблицу
			tool.dropTable("cdgrntpymntstpstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("acadGrantPaymentStopStuExtract");

		}

		// удалить сущность acadGrantBonusPaymentStopStuExtract
		{
			// удалить таблицу
			tool.dropTable("cdgrntbnspymntstpstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("acadGrantBonusPaymentStopStuExtract");

		}

		// удалить сущность acadGrantBonusAssignStuExtract
		{
			// удалить таблицу
			tool.dropTable("cdgrntbnsassgnstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("acadGrantBonusAssignStuExtract");

		}

		// удалить сущность acadGrantAssignStuExtract
		{
			// удалить таблицу
			tool.dropTable("acadgrantassignstuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("acadGrantAssignStuExtract");

		}

		// удалить сущность modularStudentExtract
		{
			// удалить таблицу
			tool.dropTable("modularstudentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("modularStudentExtract");

		}

		// удалить сущность transferDevConditionStuListExtract
		{
			// удалить таблицу
			tool.dropTable("trnsfrdvcndtnstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("transferDevConditionStuListExtract");

		}

		// удалить сущность territorialTransferStuListExtract
		{
			// удалить таблицу
			tool.dropTable("trrtrltrnsfrstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("territorialTransferStuListExtract");

		}

		// удалить сущность territorialTransferExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("trrtrltrnsfrextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("territorialTransferExtStuListExtract");

		}

		// удалить сущность territorialCourseTransferStuListExtract
		{
			// удалить таблицу
			tool.dropTable("trrtrlcrstrnsfrstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("territorialCourseTransferStuListExtract");

		}

		// удалить сущность splitStudentsStuListExtract
		{
			// удалить таблицу
			tool.dropTable("splitstudentsstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("splitStudentsStuListExtract");

		}

		// удалить сущность splitStudentsGroupStuListExtract
		{
			// удалить таблицу
			tool.dropTable("spltstdntsgrpstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("splitStudentsGroupStuListExtract");

		}

		// удалить сущность splitStudentsExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("spltstdntsextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("splitStudentsExtStuListExtract");

		}

		// удалить сущность splitFirstCourseStudentsGroupStuListExtract
		{
			// удалить таблицу
			tool.dropTable("spltfrstcrsstdntsgrpstlstext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("splitFirstCourseStudentsGroupStuListExtract");

		}

		// удалить сущность socGrantAssignStuListExtract
		{
			// удалить таблицу
			tool.dropTable("socgrantassignstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("socGrantAssignStuListExtract");

		}

		// удалить сущность setReviewerStuListExtract
		{
			// удалить таблицу
			tool.dropTable("setreviewerstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("setReviewerStuListExtract");

		}

		// удалить сущность setDiplomaWorkTopicAndScientificAdviserStuListExtract
		{
			// удалить таблицу
			tool.dropTable("stdplmwrktpcandscntfcadvsrst_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("setDiplomaWorkTopicAndScientificAdviserStuListExtract");

		}

		// удалить сущность sendPracticeInnerStuListExtract
		{
			// удалить таблицу
			tool.dropTable("sndprctcinnrstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sendPracticeInnerStuListExtract");

		}

		// удалить сущность modifyListStuExtract
		{
			// удалить таблицу
			tool.dropTable("modifyliststuextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("modifyListStuExtract");

		}

		// удалить сущность holidayStuListExtract
		{
			// удалить таблицу
			tool.dropTable("holidaystulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("holidayStuListExtract");

		}

		// удалить сущность groupTransferExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("grptrnsfrextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("groupTransferExtStuListExtract");

		}

		// удалить сущность grantRiseStuListExtract
		{
			// удалить таблицу
			tool.dropTable("grantrisestulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("grantRiseStuListExtract");

		}

		// удалить сущность graduateSuccessStuListExtract
		{
			// удалить таблицу
			tool.dropTable("grdtsccssstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("graduateSuccessStuListExtract");

		}

		// удалить сущность graduateStuListExtract
		{
			// удалить таблицу
			tool.dropTable("graduatestulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("graduateStuListExtract");

		}

		// удалить сущность gradExcludeStuListExtract
		{
			// удалить таблицу
			tool.dropTable("gradexcludestulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("gradExcludeStuListExtract");

		}

		// удалить сущность giveDiplSuccessStuListExtract
		{
			// удалить таблицу
			tool.dropTable("gvdplsccssstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplSuccessStuListExtract");

		}

		// удалить сущность giveDiplSuccessExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("gvdplsccssextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplSuccessExtStuListExtract");

		}

		// удалить сущность giveDiplStuListExtract
		{
			// удалить таблицу
			tool.dropTable("givediplstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplStuListExtract");

		}

		// удалить сущность giveDiplExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("givediplextstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("giveDiplExtStuListExtract");

		}

		// удалить сущность finAidAssignStuListExtract
		{
			// удалить таблицу
			tool.dropTable("finaidassignstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("finAidAssignStuListExtract");

		}

		// удалить сущность excludeStuListExtract
		{
			// удалить таблицу
			tool.dropTable("excludestulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeStuListExtract");

		}

		// удалить сущность excludeSingGrpSuccStuListExtract
		{
			// удалить таблицу
			tool.dropTable("xcldsnggrpsccstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeSingGrpSuccStuListExtract");

		}

		// удалить сущность excludeSingGrpStuListExtract
		{
			// удалить таблицу
			tool.dropTable("excludesinggrpstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeSingGrpStuListExtract");

		}

		// удалить сущность excludeSingGrpCathStuListExtract
		{
			// удалить таблицу
			tool.dropTable("xcldsnggrpcthstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("excludeSingGrpCathStuListExtract");

		}

		// удалить сущность courseTransferStuListExtract
		{
			// удалить таблицу
			tool.dropTable("coursetransferstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("courseTransferStuListExtract");

		}

		// удалить сущность courseTransferExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("crstrnsfrextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("courseTransferExtStuListExtract");

		}

		// удалить сущность courseTransferDebtorStuListExtract
		{
			// удалить таблицу
			tool.dropTable("crstrnsfrdbtrstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("courseTransferDebtorStuListExtract");

		}

		// удалить сущность courseNoChangeStuListExtract
		{
			// удалить таблицу
			tool.dropTable("coursenochangestulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("courseNoChangeStuListExtract");

		}

		// удалить сущность changeScientificAdviserExtract
		{
			// удалить таблицу
			tool.dropTable("chngscntfcadvsrextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeScientificAdviserExtract");

		}

		// удалить сущность changeReviewerStuListExtract
		{
			// удалить таблицу
			tool.dropTable("changereviewerstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeReviewerStuListExtract");

		}

		// удалить сущность changeDiplomaWorkTopicStuListExtract
		{
			// удалить таблицу
			tool.dropTable("chngdplmwrktpcstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeDiplomaWorkTopicStuListExtract");

		}

		// удалить сущность admitToStateExamsStuListExtract
		{
			// удалить таблицу
			tool.dropTable("dmttsttexmsstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("admitToStateExamsStuListExtract");

		}

		// удалить сущность admitToStateExamsExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("dmttsttexmsextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("admitToStateExamsExtStuListExtract");

		}

		// удалить сущность admitToPassDiplomaWorkStuListExtract
		{
			// удалить таблицу
			tool.dropTable("dmttpssdplmwrkstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("admitToPassDiplomaWorkStuListExtract");

		}

		// удалить сущность admitToPassDiplomaWorkExtStuListExtract
		{
			// удалить таблицу
			tool.dropTable("dmttpssdplmwrkextstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("admitToPassDiplomaWorkExtStuListExtract");

		}

		// удалить сущность addGroupManagerStuListExtract
		{
			// удалить таблицу
			tool.dropTable("ddgrpmngrstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("addGroupManagerStuListExtract");

		}

		// удалить сущность acadGrantBonusAssignStuListExtract
		{
			// удалить таблицу
			tool.dropTable("cdgrntbnsassgnstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("acadGrantBonusAssignStuListExtract");

		}

		// удалить сущность acadGrantAssignStuListExtract
		{
			// удалить таблицу
			tool.dropTable("cdgrntassgnstlstextrct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("acadGrantAssignStuListExtract");

		}

		// удалить сущность listStudentExtract
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("listStudentExtract");

		}

		// удалить сущность abstractStudentExtract
		{
			// удалить таблицу
			tool.dropTable("abstractstudentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("abstractStudentExtract");

		}
    }
}