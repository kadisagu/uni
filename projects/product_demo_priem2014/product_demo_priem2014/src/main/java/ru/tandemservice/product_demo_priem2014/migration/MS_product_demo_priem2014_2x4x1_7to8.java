package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unifrdo отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unifrdo") )
				throw new RuntimeException("Module 'unifrdo' is not deleted");
		}

		// удалить сущность frdoEducationLevel
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_lvl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEducationLevel");

		}

		// удалить сущность frdoEduDocumentsPackage
		{
			// удалить таблицу
			tool.dropTable("frdo_edu_doc_pck_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentsPackage");

		}

		// удалить сущность frdoEduDocumentType
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_doc_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentType");

		}

		// удалить сущность frdoEduDocumentState
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_doc_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentState");

		}

		// удалить сущность frdoEduDocumentDataType
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_doc_data_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentDataType");

		}

		// удалить сущность frdoEduDocumentBlankPropertySettings
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_doc_blank_p_set_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentBlankPropertySettings");

		}

		// удалить сущность frdoEduDocumentBlankProperty
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_doc_blank_prop_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentBlankProperty");

		}

		// удалить сущность frdoEduDocumentBlank
		{
			// удалить таблицу
			tool.dropTable("frdo_c_edu_doc_blank_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocumentBlank");

		}

		// удалить сущность frdoEduDocument
		{
			// удалить таблицу
			tool.dropTable("frdo_edu_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoEduDocument");

		}

		// удалить сущность frdoDevelopForm
		{
			// удалить таблицу
			tool.dropTable("frdo_c_dev_form_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("frdoDevelopForm");

		}


    }
}