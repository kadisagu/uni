package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniec_fis отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniec_fis") )
				throw new RuntimeException("Module 'uniec_fis' is not deleted");
		}

		// удалить сущность ecfSettingsECPeriodKey
		{
			// удалить таблицу
			tool.dropTable("ecf_s_period_key_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfSettingsECPeriodKey");

		}

		// удалить сущность ecfSettingsECPeriodItem
		{
			// удалить таблицу
			tool.dropTable("ecf_s_period_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfSettingsECPeriodItem");

		}

		// удалить сущность ecfOrgUnitPackageIOLog
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_log_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackageIOLog");

		}

		// удалить сущность ecfOrgUnitPackage4DelApplications
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_delapplications_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage4DelApplications");

		}

		// удалить сущность ecfOrgUnitPackage4DelAdmissionOrders
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_deladmissionorders_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage4DelAdmissionOrders");

		}

		// удалить сущность ecfOrgUnitPackage4CampaignInfo
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_compaign_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage4CampaignInfo");

		}

		// удалить сущность ecfOrgUnitPackage4ApplicationsInfo
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_applications_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage4ApplicationsInfo");

		}

		// удалить сущность ecfOrgUnitPackage4AdmissionOrders
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_admissionorders_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage4AdmissionOrders");

		}

		// удалить сущность ecfOrgUnitPackage4AdmissionInfo
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_admission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage4AdmissionInfo");

		}

		// удалить сущность ecfOrgUnitPackage
		{
			// удалить таблицу
			tool.dropTable("ecf_oupkg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnitPackage");

		}

		// удалить сущность ecfOrgUnit
		{
			// удалить таблицу
			tool.dropTable("ecf_orgunit_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfOrgUnit");

		}

		// удалить сущность ecfConv4SetDiscipline
		{
			// удалить таблицу
			tool.dropTable("ecf_conv_setdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfConv4SetDiscipline");

		}

		// удалить сущность ecfConv4Olympiad
		{
			// удалить таблицу
			tool.dropTable("ecf_conv_olympiad_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfConv4Olympiad");

		}

		// удалить сущность ecfConv4EduOu
		{
			// удалить таблицу
			tool.dropTable("ecf_conv_eduou_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfConv4EduOu");

		}

		// удалить сущность ecfCatalogItem
		{
			// удалить таблицу
			tool.dropTable("ecf_catalog_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecfCatalogItem");

		}
    }
}