package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisnpps отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unisnpps") )
				throw new RuntimeException("Module 'unisnpps' is not deleted");
		}

		// удалить сущность unisnppsSupernumeraryPps
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("snpps_pps", "personrole_t");

			// удалить таблицу
			tool.dropTable("snpps_pps", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisnppsSupernumeraryPps");

		}

		// удалить сущность ppsEntryByTimeworker
		{
            tool.dropConstraint("pps_entry_base_t", "fk_id_ppsentry");
            tool.dropConstraint("epp_pps_collection_t", "fk_pps_eppppscollectionitem");
            tool.dropConstraint("session_comission_pps_t", "fk_pps_sessioncomissionpps");
            tool.dropConstraint("chngdplmwrktpcstlstextrct_t", "fk_scientifi_a573d052_7798577b");
            tool.dropConstraint("stdplmwrktpcandscntfcadvsrst_t", "fk_scientifi_a573d052_1c3e3157");

			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("pps_entry_timeworker_t", "personrole_t", "pps_entry_base_t");

			// удалить таблицу
			tool.dropTable("pps_entry_timeworker_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsEntryByTimeworker");

		}

		// удалить сущность unisnppsTimeworker
		{
			// удалить таблицу
			tool.dropTable("snpps_timeworker", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisnppsTimeworker");

		}


    }
}