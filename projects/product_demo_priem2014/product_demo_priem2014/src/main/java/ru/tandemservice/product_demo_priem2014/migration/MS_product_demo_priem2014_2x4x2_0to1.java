package ru.tandemservice.product_demo_priem2014.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_priem2014_2x4x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniec14 отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniec14") )
				throw new RuntimeException("Module 'uniec14' is not deleted");
		}

		// удалить сущность ecEntrant
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("ec14_entrant_t", "personrole_t");

			// удалить таблицу
			tool.dropTable("ec14_entrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrant");

		}

		// удалить сущность ecTargetAdmissionKind
		{
			// удалить таблицу
			tool.dropTable("ec14_targetadmission_kind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecTargetAdmissionKind");

		}

		// удалить сущность ecStateExamSubjectMark
		{
			// удалить таблицу
			tool.dropTable("ec14_state_exam_mark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecStateExamSubjectMark");

		}

		// удалить сущность ecStateExamSubject
		{
			// удалить таблицу
			tool.dropTable("enc14_c_st_exam_subj_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecStateExamSubject");

		}

		// удалить сущность ecStateExamCertificate
		{
			// удалить таблицу
			tool.dropTable("ec14_state_exam_cert_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecStateExamCertificate");

		}

		// удалить сущность ecProgramSetItem
		{
			// удалить таблицу
			tool.dropTable("ec14_program_set_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecProgramSetItem");

		}

		// удалить сущность ecProgramSet
		{
			// удалить таблицу
			tool.dropTable("ec14_program_set_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecProgramSet");

		}

		// удалить сущность ecMockOrder
		{
			// удалить таблицу
			tool.dropTable("ec14_mock_order_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecMockOrder");

		}

		// удалить сущность ecMockExtract
		{
			// удалить таблицу
			tool.dropTable("ec14_mock_extract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecMockExtract");

		}

		// удалить сущность ecLanguage
		{
			// удалить таблицу
			tool.dropTable("enc14_c_language_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecLanguage");

		}

		// удалить сущность ecEntrantRequestCompetitionNoEntranceExams
		{
			// удалить таблицу
			tool.dropTable("ec14_request_noexam_prog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantRequestCompetitionNoEntranceExams");

		}

		// удалить сущность ecEntrantRequestCompetition
		{
			// удалить таблицу
			tool.dropTable("ec14_request_competition_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantRequestCompetition");

		}

		// удалить сущность ecEntrantRequest
		{
			// удалить таблицу
			tool.dropTable("ec14_request_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantRequest");

		}

		// удалить сущность ecEntrantOriginalDocumentStatus
		{
			// удалить таблицу
			tool.dropTable("ec14_entrant_original_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantOriginalDocumentStatus");

		}

		// удалить сущность ecEntrantMarkSourceDebug
		{
			// удалить таблицу
			tool.dropTable("ec14_entrant_marksrc_debug_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantMarkSourceDebug");

		}

		// удалить сущность ecEntrantMarkSource
		{
			// удалить таблицу
			tool.dropTable("ec14_entrant_marksrc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantMarkSource");

		}

		// удалить сущность ecEntrantCompetitionTestResult
		{
			// удалить таблицу
			tool.dropTable("ec14_entrant_test_res_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntrantCompetitionTestResult");

		}

		// удалить сущность ecEntranceStateTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceStateTestItem");

		}

		// удалить сущность ecEntranceLanguageTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_lang_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceLanguageTestItem");

		}

		// удалить сущность ecEntranceMainTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_main_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceMainTestItem");

		}

		// удалить сущность ecEntranceSpecializedTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_spec_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceSpecializedTestItem");

		}

		// удалить сущность ecEntranceProfessionalTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_prof_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceProfessionalTestItem");

		}

		// удалить сущность ecEntranceAdditionalTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_add_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceAdditionalTestItem");

		}

		// удалить сущность ecEntranceTestItem
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceTestItem");

		}

		// удалить сущность ecEntranceTestImpl
		{
			// удалить таблицу
			tool.dropTable("ec14_entrtest_impl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEntranceTestImpl");

		}

		// удалить сущность ecEnrollmentStepState
		{
			// удалить таблицу
			tool.dropTable("enc14_c_enr_step_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentStepState");

		}

		// удалить сущность ecEnrollmentStepItemOrderInfo
		{
			// удалить таблицу
			tool.dropTable("ec14_enr_step_item_orderinfo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentStepItemOrderInfo");

		}

		// удалить сущность ecEnrollmentStepItem
		{
			// удалить таблицу
			tool.dropTable("ec14_enr_step_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentStepItem");

		}

		// удалить сущность ecEnrollmentStep
		{
			// удалить таблицу
			tool.dropTable("ec14_enr_step_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentStep");

		}

		// удалить сущность ecEnrollmentOrgUnit
		{
			// удалить таблицу
			tool.dropTable("ec14_orgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentOrgUnit");

		}

		// удалить сущность ecEnrollmentConflictSolution
		{
			// удалить таблицу
			tool.dropTable("enc14_c_enr_confl_solution_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentConflictSolution");

		}

		// удалить сущность ecEnrollmentCampaign
		{
			// удалить таблицу
			tool.dropTable("ec14_campaign_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecEnrollmentCampaign");

		}

		// удалить сущность ecCompetitionTest
		{
			// удалить таблицу
			tool.dropTable("ec14_competition_test_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionTest");

		}

		// удалить сущность ecCompetitionRatingList
		{
			// удалить таблицу
			tool.dropTable("ec14_comprating_list_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionRatingList");

		}

		// удалить сущность ecCompetitionRatingItem
		{
			// удалить таблицу
			tool.dropTable("ec14_comprating_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionRatingItem");

		}

		// удалить сущность ecCompetitionTargetKind
		{
			// удалить таблицу
			tool.dropTable("ec14_compkind_target_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionTargetKind");

		}

		// удалить сущность ecCompetitionSelectiveKind
		{
			// удалить таблицу
			tool.dropTable("ec14_compkind_selective_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionSelectiveKind");

		}

		// удалить сущность ecCompetitionContractKind
		{
			// удалить таблицу
			tool.dropTable("ec14_compkind_contract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionContractKind");

		}

		// удалить сущность ecCompetitionCommoonKind
		{
			// удалить таблицу
			tool.dropTable("ec14_compkind_common_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionCommoonKind");

		}

		// удалить сущность ecCompetitionKind
		{
			// удалить таблицу
			tool.dropTable("ec14_compkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionKind");

		}

		// удалить сущность ecCompetitionAdd
		{
			// удалить таблицу
			tool.dropTable("ec14_competition_add_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetitionAdd");

		}

		// удалить сущность ecCompetition
		{
			// удалить таблицу
			tool.dropTable("ec14_competition_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecCompetition");

		}

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unimove") )
				throw new RuntimeException("Module 'unimove' is not deleted");
		}

		// удалить сущность orderStates
		{
			// удалить таблицу
			tool.dropTable("orderstates_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orderStates");

		}

		// удалить сущность extractStates
		{
			// удалить таблицу
			tool.dropTable("extractstates_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("extractStates");

		}

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unimv") )
				throw new RuntimeException("Module 'unimv' is not deleted");
		}

		// удалить персистентный интерфейс ru.tandemservice.unimv.entity.custom.ICustomOrderExtract
		{
			// удалить view
			tool.dropView("icustomorderextract_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.unimv.entity.custom.ICustomOrderAction
		{
			// удалить view
			tool.dropView("icustomorderaction_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.unimv.IPossibleVisa
		{
			// удалить view
			tool.dropView("ipossiblevisa_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.unimv.IAbstractDocument
		{
			// удалить view
			tool.dropView("iabstractdocument_v");

		}

		// удалить сущность visaTask
		{
			// удалить таблицу
			tool.dropTable("visatask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("visaTask");

		}

		// удалить сущность visaHistoryItem
		{
			// удалить таблицу
			tool.dropTable("visahistoryitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("visaHistoryItem");

		}

		// удалить сущность visa
		{
			// удалить таблицу
			tool.dropTable("visa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("visa");

		}

		// удалить сущность studentPossibleVisa
		{
			// удалить таблицу
			tool.dropTable("studentpossiblevisa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPossibleVisa");

		}

		// удалить сущность groupsMemberVising
		{
			// удалить таблицу
			tool.dropTable("groupsmembervising_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("groupsMemberVising");

		}

		// удалить сущность employeePostPossibleVisa
		{
			// удалить таблицу
			tool.dropTable("employeepostpossiblevisa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("employeePostPossibleVisa");

		}


    }
}