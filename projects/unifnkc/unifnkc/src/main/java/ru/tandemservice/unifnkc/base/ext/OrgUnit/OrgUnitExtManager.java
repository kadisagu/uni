/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unifnkc.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 16.09.2016
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
