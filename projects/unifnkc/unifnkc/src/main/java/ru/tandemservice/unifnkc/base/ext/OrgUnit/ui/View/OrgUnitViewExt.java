/* $Id$ */
package ru.tandemservice.unifnkc.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Ekaterina Zvereva
 * @since 16.09.2016
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .replaceTab(componentTab("uniSessionOrgUnitSessionTab").visible("false"))
                .create();
    }

}