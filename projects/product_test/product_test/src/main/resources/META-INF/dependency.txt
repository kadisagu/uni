org.tandemframework core 1.6.18-SNAPSHOT 
org.tandemframework db-support 1.6.18-SNAPSHOT 
org.tandemframework hib-support 1.6.18-SNAPSHOT 
org.tandemframework rtf 1.6.18-SNAPSHOT 
org.tandemframework caf 1.6.18-SNAPSHOT 
org.tandemframework __classreplace 1.6.18-SNAPSHOT 
org.tandemframework tap-support 1.6.18-SNAPSHOT 
org.tandemframework common 1.6.18-SNAPSHOT 
org.tandemframework sec 1.6.18-SNAPSHOT 
org.tandemframework.shared sandbox 1.11.4-SH-SNAPSHOT Sandbox
org.tandemframework.shared commonbase 1.11.4-SH-SNAPSHOT Общие прикладные функции
org.tandemframework.shared fias 1.11.4-SH-SNAPSHOT Реестр адресов
org.tandemframework.shared organization 1.11.4-SH-SNAPSHOT Оргструктура
org.tandemframework.shared person 1.11.4-SH-SNAPSHOT Персоны
org.tandemframework.shared cxfws 1.11.4-SH-SNAPSHOT Веб-сервисы CXF
org.tandemframework.shared image 1.11.4-SH-SNAPSHOT Работа с изображениями
org.tandemframework.shared archive 1.11.4-SH-SNAPSHOT Архив
org.tandemframework ldap 1.6.18-SNAPSHOT 
ru.tandemservice.uni.product unibase 2.11.4-UNI-SNAPSHOT Базовый модуль
ru.tandemservice.uni.product uniedu 2.11.4-UNI-SNAPSHOT Образовательные программы
org.tandemframework.shared employeebase 1.11.4-SH-SNAPSHOT Кадровый реестр
org.tandemframework.shared ctr 1.11.4-SH-SNAPSHOT Базовые договоры
org.tandemframework.shared survey 1.11.4-SH-SNAPSHOT Анкетирование
ru.tandemservice.uni.product uni 2.11.4-UNI-SNAPSHOT Студенты
ru.tandemservice.uni.product unimv 2.11.4-UNI-SNAPSHOT Визирование
ru.tandemservice.uni.product unimove 2.11.4-UNI-SNAPSHOT Базовое движение
ru.tandemservice.uni.product movestudent 2.11.4-UNI-SNAPSHOT Движение студентов
ru.tandemservice.uni.product unictr 2.11.4-UNI-SNAPSHOT Контрагенты (старый)
ru.tandemservice.uni.product unieductr 2.11.4-UNI-SNAPSHOT Договоры на обучение
ru.tandemservice.uni.product uniepp 2.11.4-UNI-SNAPSHOT Учебный процесс
ru.tandemservice.uni.product unisession 2.11.4-UNI-SNAPSHOT Сессия
ru.tandemservice.uni.product unisp 2.11.4-UNI-SNAPSHOT Выплаты студентам
ru.tandemservice.uni.product uniplaces 2.11.4-UNI-SNAPSHOT Здания и помещения (реестр)
ru.tandemservice.uni.product unisettle 2.11.4-UNI-SNAPSHOT Общежития и поселение
ru.tandemservice.uni.product uniemp 2.11.4-UNI-SNAPSHOT Кадры
ru.tandemservice.uni.product moveemployee 2.11.4-UNI-SNAPSHOT Движение кадрового состава
ru.tandemservice.uni.product unisnpps 2.11.4-UNI-SNAPSHOT Внештатные преподаватели
ru.tandemservice.uni.product unifrdo 2.11.4-UNI-SNAPSHOT ФРДО
ru.tandemservice.uni.product unidip 2.11.4-UNI-SNAPSHOT Дипломирование
org.tandemframework.shared adrenaline 1.11.4-SH-SNAPSHOT 
ru.tandemservice.uni.product unischedule 2.11.4-UNI-SNAPSHOT Базовое расписание
ru.tandemservice.uni.product unienr14 2.11.4-UNI-SNAPSHOT Абитуриенты
ru.tandemservice.uni.product unienr14_fis 2.11.4-UNI-SNAPSHOT Интеграция с ФИС
ru.tandemservice.uni.product unipractice 2.11.4-UNI-SNAPSHOT Практики студентов
ru.tandemservice.uni.product uniepp_load 2.11.4-UNI-SNAPSHOT Нагрузка
org.tandemframework.shared composer 1.11.4-SH-SNAPSHOT СЭД
ru.tandemservice.nsiclient nsiclient 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (базовые механизмы для обмена с НСИ)
ru.tandemservice.nsiclient nsiuni 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Базовые модули УНИ)
ru.tandemservice.nsiclient nsifias 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Реестр адресов)
ru.tandemservice.nsiclient nsiperson 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Персоны)
ru.tandemservice.nsiclient nsiorganization 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Оргструктура)
ru.tandemservice.nsiclient nsiemployeebase 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Кадровый реестр)
ru.tandemservice.nsiclient nsictr 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Базовые договоры)
ru.tandemservice.nsiclient nsiuniedu 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Образовательные программы)
ru.tandemservice.nsiclient nsiuniepp 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Учебный процесс)
ru.tandemservice.nsiclient nsimovestudent 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Движение контингента студентов)
ru.tandemservice.nsiclient nsiunieductr 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Договоры на обучение)
ru.tandemservice.nsiclient nsiunienr14 2.11.4-UNI-SNAPSHOT НСИ Абитуриенты
ru.tandemservice.uni.product unienr14_ctr 2.11.4-UNI-SNAPSHOT Договоры абитуриентов
ru.tandemservice.nsiclient nsiunienr14_ctr 2.11.4-UNI-SNAPSHOT НСИ Договоры на обучение
ru.tandemservice.nsiclient lksbase 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Личный кабинет студента. Базовые модули)
ru.tandemservice.nsiclient lksuniepp 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Личный кабинет студента. Учебный процесс)
ru.tandemservice.uni.product unispp 2.11.4-UNI-SNAPSHOT Расписание
ru.tandemservice.nsiclient lksunispp 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Личный кабинет студента. Учебное расписание)
ru.tandemservice.uni.product unitraining 2.11.4-UNI-SNAPSHOT БРС и журналы
ru.tandemservice.nsiclient lksunitraining 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Личный кабинет студента. Сессия и журналы преподавателей)
ru.tandemservice.uni.product product_test 2.11.4-UNI-SNAPSHOT 
