/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.product_test.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author dseleznev
 * Created on: 03.07.2008
 */
public class ProductUniDaoFacade
{
    private IProductUniOrgstructDao _orgstructDao;

    public IProductUniOrgstructDao getOrgstructDao()
    {
        if (_orgstructDao == null)
            _orgstructDao = (IProductUniOrgstructDao)ApplicationRuntime.getBean(IProductUniOrgstructDao.BEAN_NAME);
        return _orgstructDao;
    }
}