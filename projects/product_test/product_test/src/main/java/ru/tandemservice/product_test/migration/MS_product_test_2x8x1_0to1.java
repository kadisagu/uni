/* $Id$ */
package ru.tandemservice.product_test.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 16.06.2015
 */
public class MS_product_test_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Попытка удалить ошметки модуля uniec

        final String sql = "(select id from studentextracttype_t t where t.code_p in ('2.ent1', '2.ent1.1', '2.ent1.2', '2.ent2', '2.ent2.1', '2.ent2.2'))";

        tool.executeUpdate("delete from movestudenttemplate_t where type_id in " + sql);
        tool.executeUpdate("delete from stulistordertobasicrelation_t where order_id in (select o.id from studentlistorder_t o where o.type_id in " + sql + ")");
        tool.executeUpdate("delete from studentlistorder_t where type_id in " + sql);
        tool.executeUpdate("delete from studentextracttypetogroup_t where type_id in " + sql);
        tool.executeUpdate("delete from studentreasontotyperel_t where second_id in " + sql);
        tool.executeUpdate("delete from studentextracttype_t where id in " + sql);
    }
}