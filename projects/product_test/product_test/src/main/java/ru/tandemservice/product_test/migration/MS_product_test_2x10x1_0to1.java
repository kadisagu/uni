package ru.tandemservice.product_test.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_product_test_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unigrant отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unigrant") )
				throw new RuntimeException("Module 'unigrant' is not deleted");
		}

		// удалить сущность tmsRequest
		{
			// удалить таблицу
			tool.dropTable("tms_request_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsRequest");

		}

		// удалить сущность tmsOrgUnitResponsibleEmployee
		{
			// удалить таблицу
			tool.dropTable("tms_respemployee_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsOrgUnitResponsibleEmployee");

		}

		// удалить сущность tmsLotState
		{
			// удалить таблицу
			tool.dropTable("tmslotstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsLotState");

		}

		// удалить сущность tmsLot
		{
			// удалить таблицу
			tool.dropTable("tms_lot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsLot");

		}

		// удалить сущность tmsDemandSystemState
		{
			// удалить таблицу
			tool.dropTable("tms_demandsystemstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsDemandSystemState");

		}

		// удалить сущность tmsDemandState
		{
			// удалить таблицу
			tool.dropTable("tms_demandstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsDemandState");

		}

		// удалить сущность tmsDemand
		{
			// удалить таблицу
			tool.dropTable("tms_demand_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("tmsDemand");

		}

		// удалить сущность grantFinancingSource
		{
			// удалить таблицу
			tool.dropTable("grantfinancingsource_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("grantFinancingSource");

		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unigrant");
    }
}