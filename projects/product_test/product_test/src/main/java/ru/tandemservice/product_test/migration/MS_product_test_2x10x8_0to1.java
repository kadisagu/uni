package ru.tandemservice.product_test.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLDeleteQuery;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.PreparedStatement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_product_test_2x10x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // модуль unidpp отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if( ApplicationRuntime.hasModule("unidpp") )
                throw new RuntimeException("Module 'unidpp' is not deleted");
        }

        // удалить персистентный интерфейс ru.tandemservice.unidpp.base.entity.request.IDppDemandExtract
        {
            // удалить view
            tool.dropView("idppdemandextract_v");

        }

        // удалить сущность excludeListenersDpoListExtract
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("dpp_stu_exclude_extract", "dip_stu_exclude_extract_t", "abstractstudentextract_t");

            // удалить таблицу
            tool.dropTable("dpp_stu_exclude_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("excludeListenersDpoListExtract");

        }

        // удалить сущность dppWorkPlanCycleRow
        {
            tool.executeUpdate("delete from epp_pps_collection_t where list_id in (select id from epp_real_edu_group_t where workPlanRow_id in (select id from dpp_cycle))");

            tool.executeUpdate("delete from epp_wprow_part_load_t where row_id in (select id from dpp_cycle)");

            tool.executeUpdate("delete from epp_real_edu_group_t where workPlanRow_id in (select id from dpp_cycle)");

            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("dpp_cycle", "epp_wprow_regel_t", "epp_wprow_t");

            // удалить таблицу
            tool.dropTable("dpp_cycle", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppWorkPlanCycleRow");

        }

        // удалить сущность dppRequestStage
        {
            // удалить таблицу
            tool.dropTable("dpp_c_req_stage", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppRequestStage");

        }

        // удалить сущность dppRequestRecord
        {
            // удалить таблицу
            tool.dropTable("dpp_request_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppRequestRecord");

        }

        // удалить сущность dppRequestDBFile
        {
            // удалить таблицу
            tool.dropTable("dpp_request_attachment", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppRequestDBFile");

        }

        // удалить сущность dppRequest
        {
            // удалить таблицу
            tool.dropTable("dpp_request", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppRequest");

        }

        // удалить сущность dppOrgUnit
        {
            // удалить таблицу
            tool.dropTable("dpp_orgunit", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppOrgUnit");

        }

        // удалить сущность dppOrderReasonToTypeRel
        {
            // удалить таблицу
            tool.dropTable("dpp_order_reason2doctp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppOrderReasonToTypeRel");

        }

        // удалить сущность dppOrderReasonToBasicRel
        {
            // удалить таблицу
            tool.dropTable("dpp_order_reason_to_basic_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppOrderReasonToBasicRel");

        }

        // удалить сущность dppOrderReason
        {
            // удалить таблицу
            tool.dropTable("dpp_c_order_reason", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppOrderReason");

        }

        // удалить сущность dppOrderBasic
        {
            // удалить таблицу
            tool.dropTable("dpp_c_order_basic", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppOrderBasic");

        }

        // удалить сущность dppMoveDocumentType
        {
            // удалить таблицу
            tool.dropTable("dpp_c_move_doc_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppMoveDocumentType");

        }

        // удалить сущность dppMoveDocTemplate
        {
            // удалить таблицу
            tool.dropTable("dpp_c_move_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppMoveDocTemplate");

        }

        // удалить сущность dppEnrollmentOrderToBasicRelation
        {
            // удалить таблицу
            tool.dropTable("dpp_enrlmnt_order_to_basic_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppEnrollmentOrderToBasicRelation");

        }

        // удалить сущность dppEnrollmentOrderTextRelation
        {
            // удалить таблицу
            tool.dropTable("dpp_order_to_text_rel", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppEnrollmentOrderTextRelation");

        }

        // удалить сущность dppEducationType
        {
            // удалить таблицу
            tool.dropTable("dpp_c_edu_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppEducationType");

        }

        // удалить сущность dppEduFlow
        {
            // удалить таблицу
            tool.dropTable("dpp_c_edu_flow", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppEduFlow");

        }

        // удалить сущность dppDocTemplate
        {
            // удалить таблицу
            tool.dropTable("dpp_c_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDocTemplate");

        }

        // удалить сущность dppDemandStage
        {
            // удалить таблицу
            tool.dropTable("dpp_c_dem_stage", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandStage");

        }

        // удалить сущность dppDemandExtractRecordPart
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_extract__row_part", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandExtractRecordPart");

        }

        // удалить сущность dppDemandExtractRecord
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_extract__row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandExtractRecord");

        }

        // удалить сущность dppDemandDBFile
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_attachment", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandDBFile");

        }

        // удалить сущность dppDemand
        {
            // удалить таблицу
            tool.dropTable("dpp_demand", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemand");

        }

        // удалить сущность dppEnrollmentParagraph
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("dppEnrollmentParagraph");

        }

        // удалить сущность dppAbstractParagraph
        {
            // удалить таблицу
            tool.dropTable("dpp_paragraph", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppAbstractParagraph");

        }

        // удалить сущность dppEnrollmentOrder
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("dppEnrollmentOrder");

        }

        // удалить сущность dppAbstractOrder
        {
            // удалить таблицу
            tool.dropTable("dpp_order", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppAbstractOrder");

        }

        // удалить сущность dppDemandExtractStart
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_extract_start", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandExtractStart");

        }

        // удалить сущность dppDemandExtractReserve
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_extract_reserve", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandExtractReserve");

        }

        // удалить сущность dppDemandExtractAppend
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_extract_append", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandExtractAppend");

        }

        // удалить сущность dppDemandExtract
        {
            // удалить таблицу
            tool.dropTable("dpp_demand_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppDemandExtract");

        }

        // удалить сущность dppAbstractExtract
        {
            // удалить таблицу
            tool.dropTable("dpp_extract", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("dppAbstractExtract");

        }


        //добавлено для тех, кто стартует со старых баз
        ////////////////////////////////////////////////////////////////////////////////
        if (tool.tableExists("dpp_demand_contract"))
            tool.dropTable("dpp_demand_contract", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("dppDemandContract");

        if (tool.tableExists("dpp_c_ctr_doc_template"))
            tool.dropTable("dpp_c_ctr_doc_template", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("dppCtrDocTemplate");

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        // удалить сетки для ДПО договоров
        SQLDeleteQuery deleteGridQuery = new SQLDeleteQuery("ctrpr_c_paymentgrid", "pg");
        deleteGridQuery.getDeletedTableFrom().innerJoin(SQLFrom.table("ctrcontracttype_t", "ct"), "pg.contracttype_id=ct.id");
        deleteGridQuery.where("ct.code_p=?");
        tool.executeUpdate(translator.toSql(deleteGridQuery), "01.01.dpp");

        // удалить тип договора ДПО
        SQLDeleteQuery deleteTypeQuery = new SQLDeleteQuery("ctrcontracttype_t").where("code_p=?");
        tool.executeUpdate(translator.toSql(deleteTypeQuery), "01.01.dpp");

        /////////////////////////////////////////////////////////////////////////////////

        MigrationUtils.removeModuleFromVersion_s(tool, "unidpp");
    }
}