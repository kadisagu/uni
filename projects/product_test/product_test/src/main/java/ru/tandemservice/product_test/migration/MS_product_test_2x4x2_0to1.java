package ru.tandemservice.product_test.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrPricePaymentGridCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_test_2x4x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.2")
                };
    }

    @Override
    public ScriptDependency[] getAfterDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("uniepp_price", "2.4.2", 1)
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("CTRPR_PRELCOST_STAGE"))
        {
            // мероприятия реестра

            // этапы выплат мероприятий реестра на весь период
            Statement regElPeriod = tool.getConnection().createStatement();
            regElPeriod.execute("select element.ID as elementid, cost.id as costid, cost.period_id as costperiodid, cost.currency_id as costcurrenyid from CTRPR_PREL_COST cost \n" +
                    "inner join CTRPR_PREL_PERIOD period on period.ID=cost.PERIOD_ID\n" +
                    "inner join EPP_REG_BASE_T element on element.ID=period.ELEMENT_ID\n" +
                    "where cost.PAYMENTGRID_ID in (select id from ctrpr_c_paymentgrid where CODE_P='" + CtrPricePaymentGridCodes.EPP_PERIOD + "')");

            ResultSet regsElPeriodResult = regElPeriod.getResultSet();
            Map<Long, Map<Long, Map<Long, Long>>> regElMap = Maps.newHashMap();

            while (regsElPeriodResult.next())
            {
                Long costId = regsElPeriodResult.getLong("costid");
                Long regElId = regsElPeriodResult.getLong("elementid");
                Long periodId = regsElPeriodResult.getLong("costperiodid");
                Long currencyId = regsElPeriodResult.getLong("costcurrenyid");

                if (!regElMap.containsKey(regElId))
                    regElMap.put(regElId, Maps.<Long, Map<Long, Long>>newHashMap());
                if (!regElMap.get(regElId).containsKey(periodId))
                    regElMap.get(regElId).put(periodId, Maps.<Long, Long>newHashMap());
                if (!regElMap.get(regElId).get(periodId).containsKey(currencyId))
                    regElMap.get(regElId).get(periodId).put(currencyId, costId);
            }

            // этапы выплат за год
            Statement regElYear = tool.getConnection().createStatement();
            regElYear.execute("select stage.ID as stageid, cost.ID as costid, cost.period_id as costperiodid, cost.currency_id as costcurrenyid, period.ID as periodid, element.ID as elementid from CTRPR_PRELCOST_STAGE stage \n" +
                    "inner join CTRPR_PREL_COST cost on cost.ID=stage.COST_ID\n" +
                    "inner join CTRPR_PREL_PERIOD period on period.ID=cost.PERIOD_ID\n" +
                    "inner join EPP_REG_BASE_T element on element.ID=period.ELEMENT_ID\n" +
                    "where cost.PAYMENTGRID_ID in (select id from ctrpr_c_paymentgrid where CODE_P='" + CtrPricePaymentGridCodes.EPP_YEAR + "')");

            ResultSet regElYearResult = regElYear.getResultSet();

            PreparedStatement deleteCost = tool.prepareStatement("delete from CTRPR_PREL_COST where id = ?");

            while (regElYearResult.next())
            {
                Long regElId = regElYearResult.getLong("elementid");
                Long costPeriodId = regElYearResult.getLong("costperiodid");
                Long costCurrencyId = regElYearResult.getLong("costcurrenyid");

                // удаляем некорректные данные
                if (regElMap.containsKey(regElId) && regElMap.get(regElId).containsKey(costPeriodId) && regElMap.get(regElId).get(costPeriodId).containsKey(costCurrencyId))
                {
                    deleteCost.setLong(1, regElMap.get(regElId).get(costPeriodId).get(costCurrencyId));
                    deleteCost.executeUpdate();
                }
            }
        }
    }
}
