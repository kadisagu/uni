/* $Id:$ */
package ru.tandemservice.product_test.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author rsizonenko
 * @since 14.12.2015
 */
public class MS_product_test_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // удалить модуль unirecruit

        // убедиться, что модуль и в самом деле удален
        {
            if( ApplicationRuntime.hasModule("unirecruit") )
                throw new RuntimeException("Module 'unirecruit' is not deleted");
        }


        // удалить сущность studentRecruitCard
        {
            // удалить таблицу
            tool.dropTable("studentrecruitcard_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentRecruitCard");

        }

        // удалить сущность municipalControls
        {
            // удалить таблицу
            tool.dropTable("studentrecruitcard_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("municipalControls");

        }

        tool.executeUpdate("delete from accessmatrix_t where permissionkey_p in(?,?)", "viewStudentRecruitAttrs", "editStudentRecruit");

        MigrationUtils.removeModuleFromVersion_s(tool, "unirecruit");
        //studentrecruitcard_t
        //municipalcontrols_t
        //studentRecruitCard
        //municipalControls


    }
}