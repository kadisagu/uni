package ru.tandemservice.product_test.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_test_2x7x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unids отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unids") )
				throw new RuntimeException("Module 'unids' is not deleted");
		}

		// удалить сущность uniDSOrgUnitSessionResultRep
		{
			// удалить таблицу
			tool.dropTable("unidsorgunitsessionresultrep_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSOrgUnitSessionResultRep");

		}

		// удалить сущность uniDSSessionResultRep
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("unidssessionresultrep_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("unidssessionresultrep_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSSessionResultRep");

		}

		// удалить сущность uniDSSessionResultByDisciplinesRep
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("ndssssnrsltbydscplnsrp_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("ndssssnrsltbydscplnsrp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSSessionResultByDisciplinesRep");

		}

		// удалить сущность unidsMarkRatingScaleSettings
		{
			// удалить таблицу
			tool.dropTable("ds_mark_rating_scale_settings", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unidsMarkRatingScaleSettings");

		}

		// удалить сущность uniDSTermSummaryBulletinRep
		{
			// удалить таблицу
			tool.dropTable("unids_term_summ_bull_rep", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSTermSummaryBulletinRep");

		}

		// удалить сущность uniDSTemplate
		{
			// удалить таблицу
			tool.dropTable("unidstemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSTemplate");

		}

		// удалить сущность uniDSPracticeSlot
		{
			// удалить таблицу
			tool.dropTable("unidspracticeslot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSPracticeSlot");

		}

		// удалить сущность uniDSDisciplineSlot
		{
			// удалить таблицу
			tool.dropTable("unidsdisciplineslot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSDisciplineSlot");

		}

		// удалить сущность uniDSAttestationSlot
		{
			// удалить таблицу
			tool.dropTable("unidsattestationslot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSAttestationSlot");

		}

		// удалить сущность uniDSSlot
		{
			// удалить таблицу
			tool.dropTable("unidsslot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSSlot");

		}

		// удалить сущность uniDSMarkValue
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("uniDSMarkValue");

		}

		// удалить сущность uniDSGradeValue
		{
			// удалить таблицу
			tool.dropTable("unidsgradevalue_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSGradeValue");

		}

		// удалить сущность uniDSMarkValueBase
		{
			// удалить таблицу
			tool.dropTable("unidsmarkvaluebase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSMarkValueBase");

		}

		// удалить сущность uniDSMarkTransferReason
		{
			// удалить таблицу
			tool.dropTable("unidsmarktransferreason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSMarkTransferReason");

		}

		// удалить сущность uniDSMarkTransferDocument2Reason
		{
			// удалить таблицу
			tool.dropTable("ndsmrktrnsfrdcmnt2rsn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSMarkTransferDocument2Reason");

		}

		// удалить сущность uniDSOuterMarkTransfer
		{
			// удалить таблицу
			tool.dropTable("unidsoutermarktransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSOuterMarkTransfer");

		}

		// удалить сущность uniDSInnerMarkTransfer
		{
			// удалить таблицу
			tool.dropTable("unidsinnermarktransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSInnerMarkTransfer");

		}

		// удалить сущность uniDSMarkTransfer
		{
			// удалить таблицу
			tool.dropTable("unidsmarktransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSMarkTransfer");

		}

		// удалить сущность uniDSGradeScale
		{
			// удалить таблицу
			tool.dropTable("unidsgradescale_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSGradeScale");

		}

		// удалить сущность uniDSMarkTransferDocument
		{
			// удалить таблицу
			tool.dropTable("unidsmarktransferdocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSMarkTransferDocument");

		}

		// удалить сущность uniDSSheet
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("uniDSSheet");

		}

		// удалить сущность uniDSCard
		{
			// удалить таблицу
			tool.dropTable("unidscard_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSCard");

		}

		// удалить сущность uniDSELCDocument
		{
			// удалить таблицу
			tool.dropTable("unidselcdocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSELCDocument");

		}

		// удалить сущность uniDSBulletin
		{
			// удалить таблицу
			tool.dropTable("unidsbulletin_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSBulletin");

		}

		// удалить сущность uniDSDocument
		{
			// удалить таблицу
			tool.dropTable("unidsdocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSDocument");

		}

		// удалить сущность uniDSCrsTermSummaryBulletinRep
		{
			// удалить таблицу
			tool.dropTable("unids_crs_term_summ_bull_rep", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSCrsTermSummaryBulletinRep");

		}

		// удалить сущность uniDSCommission
		{
			// удалить таблицу
			tool.dropTable("unidscommission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSCommission");

		}

		// удалить сущность uniDSCardCreationReasons
		{
			// удалить таблицу
			tool.dropTable("unidscardcreationreasons_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSCardCreationReasons");

		}

		// удалить сущность uniDSBulletinType
		{
			// удалить таблицу
			tool.dropTable("unidsbulletintype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniDSBulletinType");

		}

		// удалить сущность studentStatus2BulletinType
		{
			// удалить таблицу
			tool.dropTable("studentstatus2bulletintype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentStatus2BulletinType");

		}

		// удалить сущность noCRSonCourse
		{
			// удалить таблицу
			tool.dropTable("nocrsoncourse_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("noCRSonCourse");

		}

		// удалить сущность examAllowancePolicy
		{
			// удалить таблицу
			tool.dropTable("examallowancepolicy_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("examAllowancePolicy");

		}

		// удалить сущность controlAction2GradeScale
		{
			// удалить таблицу
			tool.dropTable("controlaction2gradescale_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlAction2GradeScale");

		}

		// удалить сущность controlAction2BulletinType
		{
			// удалить таблицу
			tool.dropTable("controlaction2bulletintype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlAction2BulletinType");

		}

		// удалить сущность commission2EmployeePost
		{
			// удалить таблицу
			tool.dropTable("commission2employeepost_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("commission2EmployeePost");

		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unids");
    }
}