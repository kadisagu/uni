/* $Id$ */
package ru.tandemservice.nsiclient;

import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.datagram.XDatagram;
import ru.tandemservice.nsiclient.utils.INsiBean;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2016
 */
public class NsiBean implements INsiBean
{
    public IXDatagram createXDatagram()
    {
        return new XDatagram();
    }
}