package ru.tandemservice.uniorgma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniorgma_2x7x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisession отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unisession") )
				throw new RuntimeException("Module 'unisession' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unisession");

		// удалить персистентный интерфейс ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed
		{
			// удалить view
			tool.dropView("isessionstudentnotallowed_v");
		}

		// удалить сущность unisessionCommonTemplate
		{
			// удалить таблицу
			tool.dropTable("session_template_common_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionCommonTemplate");
		}

		// удалить сущность unisessionBulletinTemplate
		{
			// удалить таблицу
			tool.dropTable("session_template_bulletin_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionBulletinTemplate");
		}

		// удалить сущность unisessionTemplate
		{
			// удалить таблицу
			tool.dropTable("session_template_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionTemplate");
		}

		// удалить сущность unisessionSummaryBulletinReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_summary_bull_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionSummaryBulletinReport");
		}

		// удалить сущность unisessionResultsReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_results_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionResultsReport");
		}

		// удалить сущность unisessionResultsByDiscReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_results_d_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionResultsByDiscReport");
		}

		// удалить сущность unisessionGroupMarksReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_grp_marks_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionGroupMarksReport");
		}

		// удалить сущность unisessionGroupBulletinListReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_grp_bull_list_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionGroupBulletinListReport");
		}

		// удалить сущность unisessionDebtorsReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_debtors_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionDebtorsReport");
		}

		// удалить сущность sessionsSimpleDocumentReason
		{
			// удалить таблицу
			tool.dropTable("sessionssimpledocumentreason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionsSimpleDocumentReason");
		}

		// удалить сущность sessionTransferOutsideOperation
		{
			// удалить таблицу
			tool.dropTable("session_transfer_outop_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferOutsideOperation");
		}

		// удалить сущность sessionTransferInsideOperation
		{
			// удалить таблицу
			tool.dropTable("session_transfer_inop_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferInsideOperation");
		}

		// удалить сущность sessionTransferOperation
		{
			// удалить таблицу
			tool.dropTable("session_transfer_op_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferOperation");
		}

		// удалить сущность sessionStudentNotAllowedForBulletin
		{
			// удалить таблицу
			tool.dropTable("session_student_na4b_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionStudentNotAllowedForBulletin");
		}

		// удалить сущность sessionStudentNotAllowed
		{
			// удалить таблицу
			tool.dropTable("session_student_na_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionStudentNotAllowed");
		}

		// удалить сущность sessionSlotRatingData
		{
			// удалить таблицу
			tool.dropTable("session_slot_rating_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotRatingData");
		}

		// удалить сущность sessionProjectTheme
		{
			// удалить таблицу
			tool.dropTable("session_project_theme_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionProjectTheme");
		}

		// удалить сущность sessionObject
		{
			// удалить таблицу
			tool.dropTable("session_object_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionObject");
		}

		// удалить сущность sessionMarkRatingData
		{
			// удалить таблицу
			tool.dropTable("session_mark_rating_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkRatingData");
		}

		// удалить сущность sessionMarkStateCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_mark_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkStateCatalogItem");
		}

		// удалить сущность sessionMarkGradeValueCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_mark_grade_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkGradeValueCatalogItem");
		}

		// удалить сущность sessionCurrentMarkCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_cur_mark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionCurrentMarkCatalogItem");
		}

		// удалить сущность sessionMarkCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_mark_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkCatalogItem");
		}

		// удалить сущность sessionSlotMarkState
		{
			// удалить таблицу
			tool.dropTable("session_mark_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotMarkState");
		}

		// удалить сущность sessionSlotMarkGradeValue
		{
			// удалить таблицу
			tool.dropTable("session_mark_value_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotMarkGradeValue");
		}

		// удалить сущность sessionSlotRegularMark
		{
			// удалить таблицу
			tool.dropTable("session_mark_reg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotRegularMark");
		}

		// удалить сущность sessionSlotLinkMark
		{
			// удалить таблицу
			tool.dropTable("session_mark_link_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotLinkMark");
		}

		// удалить сущность sessionMark
		{
			// удалить таблицу
			tool.dropTable("session_mark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMark");
		}

		// удалить сущность sessionDocumentSlot
		{
			// удалить таблицу
			tool.dropTable("session_doc_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionDocumentSlot");
		}

		// удалить сущность sessionDocumentPrintVersion
		{
			// удалить таблицу
			tool.dropTable("session_doc_printform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionDocumentPrintVersion");
		}

		// удалить сущность sessionTransferOutsideDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_outside_transfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferOutsideDocument");
		}

		// удалить сущность sessionTransferInsideDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_inside_transfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferInsideDocument");
		}

		// удалить сущность sessionTransferDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_transfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferDocument");
		}

		// удалить сущность sessionStudentGradeBookDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_stgbook_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionStudentGradeBookDocument");
		}

		// удалить сущность sessionSheetDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_sheet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSheetDocument");
		}

		// удалить сущность sessionListDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_list_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionListDocument");
		}

		// удалить сущность sessionSimpleDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSimpleDocument");
		}

		// удалить сущность sessionRetakeDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_retake_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionRetakeDocument");
		}

		// удалить сущность sessionGlobalDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_global_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionGlobalDocument");
		}

		// удалить сущность sessionBulletinDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_bulletin_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionBulletinDocument");
		}

		// удалить сущность sessionDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionDocument");
		}

		// удалить сущность sessionCurrentMarkScale
		{
			// удалить таблицу
			tool.dropTable("session_c_cur_mark_scale_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionCurrentMarkScale");
		}

		// удалить сущность sessionComissionPps
		{
			// удалить таблицу
			tool.dropTable("session_comission_pps_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionComissionPps");
		}

		// удалить сущность sessionComission
		{
			// удалить таблицу
			tool.dropTable("session_comission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionComission");
		}

		// удалить сущность sessionAttestationTotalResultReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_att_total_result_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationTotalResultReport");
		}

		// удалить сущность sessionAttestationSlotAdditionalData
		{
			// удалить таблицу
			tool.dropTable("session_att_slot_adddata_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationSlotAdditionalData");
		}

		// удалить сущность sessionAttestationSlot
		{
			// удалить таблицу
			tool.dropTable("session_att_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationSlot");
		}

		// удалить сущность sessionAttestationResultReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_att_result_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationResultReport");
		}

		// удалить сущность sessionAttestationBulletinReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_att_bull_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationBulletinReport");
		}

		// удалить сущность sessionAttestationBulletin
		{
			// удалить таблицу
			tool.dropTable("session_att_bull_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationBulletin");
		}

		// удалить сущность sessionAttestation
		{
			// удалить таблицу
			tool.dropTable("session_attestation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestation");
		}

		// удалить сущность sessionAttBullletinPrintVersion
		{
			// удалить таблицу
			tool.dropTable("session_att_bull_printform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttBullletinPrintVersion");
		}
    }
}