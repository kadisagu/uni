package ru.tandemservice.uniorgma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniorgma_2x7x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisc отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unisc") )
				throw new RuntimeException("Module 'unisc' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unisc");

		// удалить сущность uniscTemplateDocument
		{
			// удалить таблицу
			tool.dropTable("unisctemplatedocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscTemplateDocument");
		}

		// удалить сущность uniscPaymentReport
		{
			// удалить таблицу
			tool.dropTable("uniscpaymentreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscPaymentReport");
		}

		// удалить сущность uniscPaymentDocumentType
		{
			// удалить таблицу
			tool.dropTable("uniscpaymentdocumenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscPaymentDocumentType");
		}

		// удалить сущность uniscPayPeriod
		{
			// удалить таблицу
			tool.dropTable("uniscpayperiod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscPayPeriod");
		}

		// удалить сущность uniscOrgUnitPresenter
		{
			// удалить таблицу
			tool.dropTable("sc_presenters", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscOrgUnitPresenter");
		}

		// удалить сущность uniscInflationHistory
		{
			// удалить таблицу
			tool.dropTable("sc_inflations", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscInflationHistory");
		}

		// удалить сущность uniscGroupIncomeReport
		{
			// удалить таблицу
			tool.dropTable("uniscgroupincomereport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscGroupIncomeReport");
		}

		// удалить сущность uniscFreq2DevForm
		{
			// удалить таблицу
			tool.dropTable("sc_freq2dform", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscFreq2DevForm");
		}

		// удалить сущность uniscFinesHistory
		{
			// удалить таблицу
			tool.dropTable("sc_fines", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscFinesHistory");
		}

		// удалить сущность uniscEduOrgUnitRelation
		{
			// удалить таблицу
			tool.dropTable("sc_eduou_config_relation", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduOrgUnitRelation");
		}

		// удалить сущность uniscEduOrgUnitPayPlanRow
		{
			// удалить таблицу
			tool.dropTable("sc_eduou_pay_plan_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduOrgUnitPayPlanRow");
		}

		// удалить сущность uniscEduOrgUnitPayPlan
		{
			// удалить таблицу
			tool.dropTable("sc_eduou_pay_plan", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduOrgUnitPayPlan");
		}

		// удалить сущность uniscEduOrgUnit
		{
			// удалить таблицу
			tool.dropTable("sc_eduou_config", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduOrgUnit");
		}

		// удалить сущность uniscEduAgreementPrintVersion
		{
			// удалить таблицу
			tool.dropTable("sc_ag_print", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementPrintVersion");
		}

		// удалить сущность uniscEduAgreementPayPlanRow
		{
			// удалить таблицу
			tool.dropTable("sc_ag_plan_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementPayPlanRow");
		}

		// удалить сущность uniscEduAgreementPayFactRow
		{
			// удалить таблицу
			tool.dropTable("sc_ag_fact_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementPayFactRow");
		}

		// удалить сущность uniscEduAgreementNaturalPerson
		{
			// удалить таблицу
			tool.dropTable("sc_ag_nperson", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementNaturalPerson");
		}

		// удалить сущность uniscEduAgreementFinesState
		{
			// удалить таблицу
			tool.dropTable("sc_ag_fines_row", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementFinesState");
		}

		// удалить сущность uniscEduAgreementFinesHistory
		{
			// удалить таблицу
			tool.dropTable("sc_ag_fines", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementFinesHistory");
		}

		// удалить сущность uniscEduMainAgreement
		{
			// удалить таблицу
			tool.dropTable("unisc_ag_agreement", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduMainAgreement");
		}

		// удалить сущность uniscEduAdditAgreement
		{
			// удалить таблицу
			tool.dropTable("sc_ag_additional", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAdditAgreement");
		}

		// удалить сущность uniscEduAgreementBase
		{
			// удалить таблицу
			tool.dropTable("sc_ag_base", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreementBase");
		}

		// удалить сущность uniscEduAgreement2Student
		{
			// удалить таблицу
			tool.dropTable("sc_eduag2student", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreement2Student");
		}

		// удалить сущность uniscCustomerType
		{
			// удалить таблицу
			tool.dropTable("unisccustomertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscCustomerType");
		}

		// удалить сущность uniscAgreementIncomeSummaryReport
		{
			// удалить таблицу
			tool.dropTable("nscagrmntincmsmmryrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscAgreementIncomeSummaryReport");
		}

		// удалить сущность uniscAgreementIncomeReport
		{
			// удалить таблицу
			tool.dropTable("uniscagreementincomereport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscAgreementIncomeReport");
		}
    }
}