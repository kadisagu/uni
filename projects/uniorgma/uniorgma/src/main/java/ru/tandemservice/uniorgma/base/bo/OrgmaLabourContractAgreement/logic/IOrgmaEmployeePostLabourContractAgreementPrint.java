/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * Интерфейс для печати дополнительных соглашений.
 * Они бывают разных типов => реализация для них будет разная.
 *
 * @author Alexander Shaburov
 * @since 27.11.12
 */
public interface IOrgmaEmployeePostLabourContractAgreementPrint extends INeedPersistenceSupport
{
    /**
     * @param template rtf шаблон
     * @param model ключи - это данные которые требуются для печати
     * @return сформированный документ
     */
    RtfDocument createPrintForm(byte[] template, MultiKey model);
}
