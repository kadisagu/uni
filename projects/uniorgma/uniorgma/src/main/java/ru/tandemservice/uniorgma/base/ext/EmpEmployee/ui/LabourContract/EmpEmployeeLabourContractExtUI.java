/**
 *$Id:$
 */
package ru.tandemservice.uniorgma.base.ext.EmpEmployee.ui.LabourContract;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.LabourContract.EmpEmployeeLabourContractUI;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.ui.Add.OrgmaLabourContractAgreementAdd;

/**
 * @author Alexander Shaburov
 * @since 23.10.12
 */
public class EmpEmployeeLabourContractExtUI extends UIAddon
{
    public EmpEmployeeLabourContractExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @SuppressWarnings("unchecked")
    public void onClickAddEmployeeLabourContract()
    {
        IPrintFormCreator<Long> print = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("employeePostLabourContract");

        EmployeeTemplateDocument templateDocument = DataAccessServices.dao().get(EmployeeTemplateDocument.class, EmployeeTemplateDocument.code().s(), "employeeLabourContractGen");
        EmpEmployeeLabourContractUI presenter = getPresenter();

        RtfDocument document = print.createPrintForm(templateDocument.getContent(), presenter.getContract().getId());

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("employeeLabourContract.rtf").document(document), false);
    }

    public void onClickGenContractAgreementPrintFile()
    {
        getActivationBuilder().asRegionDialog(OrgmaLabourContractAgreementAdd.class)
                .parameter("contractAgreementId", getListenerParameterAsLong())
                .activate();
    }

    @Override
    public void onComponentRefresh()
    {
        EmpEmployeeLabourContractUI presenter = getPresenter();
        presenter.getContractAgreementDataSource().addColumn(new ActionColumn("Сгенерировать файл доп. соглашения", "printer", EmpEmployeeLabourContractExt.ADDON_NAME + ":onClickGenContractAgreementPrintFile").setPermissionKey("genLabourContractAgreementPrintForm_employeePost"), 6);
    }
}
