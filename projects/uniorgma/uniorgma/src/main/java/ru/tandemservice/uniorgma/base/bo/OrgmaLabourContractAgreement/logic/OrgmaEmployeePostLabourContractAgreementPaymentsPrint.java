/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.OrgmaLabourContractAgreementManager;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Дополнительное соглашение к трудовому договору на установление доплат.
 *
 * @author Alexander Shaburov
 * @since 17.01.13
 */
public class OrgmaEmployeePostLabourContractAgreementPaymentsPrint extends UniBaseDao implements IOrgmaEmployeePostLabourContractAgreementPrint
{
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, MultiKey model)
    {
        final Long agreementId = (Long) model.getKey(0);
        final Date from = (Date) model.getKey(1);
        final Date to = (Date) model.getKey(2);

        if (to != null && to.getTime() < from.getTime())
        {
            UserContext.getInstance().getErrorCollector().add(OrgmaLabourContractAgreementManager.instance().getProperty("error.incorrect-date"), "agreementPeriodFrom", "agreementPeriodTo");
            return null;
        }

        final ContractCollateralAgreement agreement = get(ContractCollateralAgreement.class, agreementId);

        RtfDocument document = new RtfReader().read(template);

        injectModifier(document, agreement, from, to);
        injectTableModifier(document);

        return document;
    }

    protected void injectModifier(RtfDocument document, ContractCollateralAgreement agreement, Date from, Date to)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        BigDecimal staffRate = new BigDecimal(0d);
        for (EmployeePostStaffRateItem item : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(agreement.getContract().getEmployeePost()))
            staffRate = staffRate.add(new BigDecimal(item.getStaffRate()));

        final List<EmployeePayment> paymentList = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b")
                .where(eq(property(EmployeePayment.employeePost().fromAlias("b")), value(agreement.getContract().getEmployeePost())))
                .where(ne(property(EmployeePayment.payment().oneTimePayment().fromAlias("b")), value(true)))
                .createStatement(getSession()).list();

        for (int i = 0; i < paymentList.size(); i++)
        {
            EmployeePayment payment1 = paymentList.get(i);
            MultiKey key1 = new MultiKey(payment1.getPayment().getTitle(), payment1.getPaymentValue(), payment1.getFinancingSource() != null ? payment1.getFinancingSource().getId() : null, payment1.getFinancingSourceItem() != null ? payment1.getFinancingSourceItem().getId() : null);

            for (int j = i + 1; j < paymentList.size(); j++)
            {
                EmployeePayment payment2 = paymentList.get(j);
                MultiKey key2 = new MultiKey(payment2.getPayment().getTitle(), payment2.getPaymentValue(), payment2.getFinancingSource() != null ? payment2.getFinancingSource().getId() : null, payment2.getFinancingSourceItem() != null ? payment2.getFinancingSourceItem().getId() : null);

                if (key1.equals(key2))
                    paymentList.remove(payment1);
            }
        }

        EmployeePost head = (EmployeePost) TopOrgUnit.getInstance().getHead();

        modifier.put("collateralAgreementNumber", agreement.getNumber());
        modifier.put("lcNumber", agreement.getContract().getNumber());
        SimpleDateFormat df = new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU);
        modifier.put("lcDateStr", df.format(agreement.getContract().getDate()));
        modifier.put("collateralAgreementDateStr", df.format(agreement.getDate()));
        modifier.put("headPost", head != null ? (head.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? head.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : head.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()) : "");
        modifier.put("headPost_G", head != null ? (head.getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle() != null ? head.getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle() : head.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()) : "");
        modifier.put("headFio", head != null ? head.getPerson().getIdentityCard().getFirstName().substring(0, 1).toUpperCase() + ". " + (head.getPerson().getIdentityCard().getMiddleName() != null ? head.getPerson().getIdentityCard().getMiddleName().substring(0, 1).toUpperCase() : "") + ". " + head.getPerson().getIdentityCard().getLastName() : "");
        modifier.put("headFio_G", head != null ? head.getPerson().getIdentityCard().getFirstName().substring(0, 1).toUpperCase() + ". " + (head.getPerson().getIdentityCard().getMiddleName() != null ? head.getPerson().getIdentityCard().getMiddleName().substring(0, 1).toUpperCase() : "") + ". " + PersonManager.instance().declinationDao().getDeclinationLastName(head.getPerson().getIdentityCard().getLastName(), GrammaCase.GENITIVE, head.getPerson().isMale()) : "");
        modifier.put("employeeFio", agreement.getContract().getEmployeePost().getPerson().getFullFio());
        modifier.put("staffRate", staffRate.equals(new BigDecimal(1d)) ? "ставку" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate.doubleValue()) + " ставки");
        modifier.put("post", agreement.getContract().getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? agreement.getContract().getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : agreement.getContract().getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
        modifier.put("orgUnit", agreement.getContract().getEmployeePost().getOrgUnit().getNominativeCaseTitle() != null ? agreement.getContract().getEmployeePost().getOrgUnit().getNominativeCaseTitle() : agreement.getContract().getEmployeePost().getOrgUnit().getFullTitle());
        modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(agreement.getContract().getEmployeePost().getSalary()));
        modifier.put("collAgreementPeriod", "с " + df.format(from) + " г." + (to != null ? " на срок до " + RussianDateFormatUtils.getDateFormattedWithMonthName(to) + " г." : ""));
        modifier.put("bonusListFormatted", CommonExtractUtil.getFormattedBonusListStr(paymentList, "employeeLabourContractAgreement", false));
        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(from));
        modifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(to));

        modifier.modify(document);
    }

    protected void injectTableModifier(RtfDocument document)
    {

    }
}
