/**
 *$Id:$
 */
package ru.tandemservice.uniorgma.base.ext.EmpEmployee.ui.LabourContract;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.LabourContract.EmpEmployeeLabourContract;

/**
 * @author Alexander Shaburov
 * @since 23.10.12
 */
@Configuration
public class EmpEmployeeLabourContractExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "orgma" + EmpEmployeeLabourContract.class.getSimpleName();

    @Autowired
    private EmpEmployeeLabourContract _empEmployeeLabourContract;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_empEmployeeLabourContract.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmpEmployeeLabourContractExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension buttonListExtension()
    {
        return buttonListExtensionBuilder(_empEmployeeLabourContract.buttonListExtPoint())
                .addAllAfter("addContractFile")
                .addButton(submitButton("employeeLabourContract", ADDON_NAME + ":onClickAddEmployeeLabourContract").visible("ui:hasLabourContract").permissionKey("genLabourContractPrintForm_employeePost"))
                .create();
    }
}
