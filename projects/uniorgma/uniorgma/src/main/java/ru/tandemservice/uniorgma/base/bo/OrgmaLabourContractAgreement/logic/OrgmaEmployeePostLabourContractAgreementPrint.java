/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.ITransferEmployeePostExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Дополнительное соглашение к трудовому договору на перевод.
 *
 * @author Alexander Shaburov
 * @since 27.11.12
 */
public class OrgmaEmployeePostLabourContractAgreementPrint extends UniBaseDao implements IOrgmaEmployeePostLabourContractAgreementPrint
{
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, MultiKey model)
    {
        final Long agreementId = (Long) model.getKey(0);
        final Long extractId = (Long) model.getKey(1);

        ContractCollateralAgreement agreement = get(ContractCollateralAgreement.class, agreementId);

        RtfDocument document = new RtfReader().read(template);

        injectModifier(document, agreement, extractId);
        injectTableModifier(document, agreement);

        return document;
    }

    protected void injectModifier(RtfDocument document, ContractCollateralAgreement agreement, Long extractId)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        EmployeePost postOld = null;
        if (extractId != null)
        {
            AbstractEmployeeExtract extract = get(AbstractEmployeeExtract.class, extractId);

            if (extract instanceof ITransferEmployeePostExtract)
                postOld = ((ITransferEmployeePostExtract) extract).getEmployeePostOld();
        }

        List<EmployeePayment> paymentList = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b")
                .where(eq(property(EmployeePayment.employeePost().fromAlias("b")), value(agreement.getContract().getEmployeePost())))
                .where(ne(property(EmployeePayment.payment().oneTimePayment().fromAlias("b")), value(true)))
                .createStatement(getSession()).list();

        for (int i = 0; i < paymentList.size(); i++)
        {
            EmployeePayment payment1 = paymentList.get(i);
            MultiKey key1 = new MultiKey(payment1.getPayment().getTitle(), payment1.getPaymentValue(), payment1.getFinancingSource() != null ? payment1.getFinancingSource().getId() : null, payment1.getFinancingSourceItem() != null ? payment1.getFinancingSourceItem().getId() : null);

            for (int j = i + 1; j < paymentList.size(); j++)
            {
                EmployeePayment payment2 = paymentList.get(j);
                MultiKey key2 = new MultiKey(payment2.getPayment().getTitle(), payment2.getPaymentValue(), payment2.getFinancingSource() != null ? payment2.getFinancingSource().getId() : null, payment2.getFinancingSourceItem() != null ? payment2.getFinancingSourceItem().getId() : null);

                if (key1.equals(key2))
                    paymentList.remove(payment1);
            }
        }

        Double staffRate = 0d;
        for (EmployeePostStaffRateItem item : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(agreement.getContract().getEmployeePost()))
            staffRate += item.getStaffRate();

        BigDecimal decimal = new BigDecimal(staffRate);
        decimal = decimal.setScale(2, RoundingMode.HALF_UP);

        EmployeePost head = (EmployeePost) TopOrgUnit.getInstance().getHead();

        modifier.put("collateralAgreementNumber", agreement.getNumber());
        modifier.put("lcNumber", agreement.getContract().getNumber());
        SimpleDateFormat df = new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU);
        modifier.put("lcDateStr", df.format(agreement.getContract().getDate()));
        modifier.put("collateralAgreementDateStr", df.format(agreement.getDate()));
        modifier.put("headPost", head != null ? (head.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? head.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : head.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()) : "");
        modifier.put("headPost_G", head != null ? (head.getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle() != null ? head.getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle() : head.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()) : "");
        modifier.put("headFio", head != null ? head.getPerson().getIdentityCard().getFirstName().substring(0, 1).toUpperCase() + ". " + (head.getPerson().getIdentityCard().getMiddleName() != null ? head.getPerson().getIdentityCard().getMiddleName().substring(0, 1).toUpperCase() : "") + ". " + head.getPerson().getIdentityCard().getLastName() : "");
        modifier.put("headFio_G", head != null ? head.getPerson().getIdentityCard().getFirstName().substring(0, 1).toUpperCase() + ". " + (head.getPerson().getIdentityCard().getMiddleName() != null ? head.getPerson().getIdentityCard().getMiddleName().substring(0, 1).toUpperCase() : "") + ". " + PersonManager.instance().declinationDao().getDeclinationLastName(head.getPerson().getIdentityCard().getLastName(), GrammaCase.GENITIVE, head.getPerson().isMale()) : "");
        modifier.put("employeeFio", agreement.getContract().getEmployeePost().getPerson().getFullFio());
        modifier.put("staffRate", staffRate.equals(1d) ? "ставку" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(decimal.doubleValue()) + " ставки");
        modifier.put("post", agreement.getContract().getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? agreement.getContract().getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : agreement.getContract().getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
        modifier.put("orgUnit", agreement.getContract().getEmployeePost().getOrgUnit().getNominativeCaseTitle() != null ? agreement.getContract().getEmployeePost().getOrgUnit().getNominativeCaseTitle() : agreement.getContract().getEmployeePost().getOrgUnit().getFullTitle());
        modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(agreement.getContract().getEmployeePost().getSalary()));
        modifier.put("bonusListFormatted", CommonExtractUtil.getFormattedBonusListStr(paymentList, "employeeLabourContractAgreement", false));
        modifier.put("collAgreementPeriod", "с " + df.format(agreement.getContract().getEmployeePost().getPostDate()) + " г." + (agreement.getContract().getEmployeePost().getDismissalDate() != null ? " на срок до " + RussianDateFormatUtils.getDateFormattedWithMonthName(agreement.getContract().getEmployeePost().getDismissalDate()) + " г." : ""));

        if (postOld != null)
        {
            modifier.put("orgUnit_Old", postOld.getOrgUnit().getNominativeCaseTitle() != null ? postOld.getOrgUnit().getNominativeCaseTitle() : postOld.getOrgUnit().getFullTitle());
            modifier.put("post_Old", postOld.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? postOld.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : postOld.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
            modifier.put("comma", (String) null);
        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("orgUnit_Old", "post_Old"), false, false);
            modifier.put("comma", ",");
        }

        modifier.modify(document);
    }

    protected void injectTableModifier(RtfDocument document, ContractCollateralAgreement agreement)
    {

    }
}
