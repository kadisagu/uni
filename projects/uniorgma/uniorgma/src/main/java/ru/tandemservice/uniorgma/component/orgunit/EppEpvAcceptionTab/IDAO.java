package ru.tandemservice.uniorgma.component.orgunit.EppEpvAcceptionTab;

import org.tandemframework.core.settings.IDataSettings;

import java.util.List;

/**
 * @author Igor Lunin
 * @since 30.10.2014
 */
public interface IDAO
{
    /**
     * Возвращает список id строк УП, подходящих под критерии отбора.
     * @param orgUnitId id читающего подразделения.
     * @param settings настрйоки пользователя
     * @return список id строк УП
     */
    public List<Long> getRegistryRowIds(Long orgUnitId, IDataSettings settings);

    /**
     * Создает элементы реестра для указанных id строк УПв
     * @param registryRowIds id строк УПв
     */
    public void createRegistryElements(List<Long> registryRowIds);
}
