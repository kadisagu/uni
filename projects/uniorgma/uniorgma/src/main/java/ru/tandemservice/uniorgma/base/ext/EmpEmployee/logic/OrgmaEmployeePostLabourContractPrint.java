/**
 *$Id:$
 */
package ru.tandemservice.uniorgma.base.ext.EmpEmployee.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 22.10.12
 */
public class OrgmaEmployeePostLabourContractPrint extends UniBaseDao implements IPrintFormCreator<Long>
{
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, Long object)
    {
        EmployeeLabourContract contract = get(EmployeeLabourContract.class, object);

        RtfDocument document = new RtfReader().read(template);

        injectModifier(document, contract);
        injectTableModifier(document, contract);

        return document;
    }

    protected void injectModifier(RtfDocument document, EmployeeLabourContract contract)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance();
        EmployeePost employeePost = contract.getEmployeePost();

        Double staffRate = 0d;
        for (EmployeePostStaffRateItem item : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(employeePost))
            staffRate += item.getStaffRate();

        List<EmployeePayment> paymentList = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b")
                .where(eq(property(EmployeePayment.employeePost().fromAlias("b")), value(employeePost)))
                .where(ne(property(EmployeePayment.payment().oneTimePayment().fromAlias("b")), value(true)))
                .createStatement(getSession()).list();

        for (EmployeePayment payment1 : new ArrayList<EmployeePayment>(paymentList))
        {
            MultiKey key1 = new MultiKey(payment1.getPayment().getTitle(), payment1.getPaymentValue(), payment1.getFinancingSource() != null ? payment1.getFinancingSource().getId() : null, payment1.getFinancingSourceItem() != null ? payment1.getFinancingSourceItem().getId() : null);

            for (EmployeePayment payment2 : new ArrayList<EmployeePayment>(paymentList))
            {
                if (payment1.equals(payment2))
                    continue;

                MultiKey key2 = new MultiKey(payment2.getPayment().getTitle(), payment2.getPaymentValue(), payment2.getFinancingSource() != null ? payment2.getFinancingSource().getId() : null, payment2.getFinancingSourceItem() != null ? payment2.getFinancingSourceItem().getId() : null);

                if (key1.equals(key2))
                    paymentList.remove(payment1);
            }
        }

        modifier.put("lcDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()));
        modifier.put("lcNumber", contract.getNumber());
        modifier.put("organizationTitle", topOrgUnit.getPrintTitle() != null ? topOrgUnit.getPrintTitle() : topOrgUnit.getTitle());
        modifier.put("rector_fio", topOrgUnit.getHead() != null ? topOrgUnit.getHead().getEmployee().getPerson().getFullFio() : "");
        modifier.put("employee_fio", employeePost.getPerson().getFullFio());
        modifier.put("post", employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
        modifier.put("orgUnitPrep_in", Arrays.asList(OrgUnitTypeCodes.CATHEDRA, OrgUnitTypeCodes.FACULTY).contains(employeePost.getOrgUnit().getOrgUnitType().getCode()) ? "на" : "в");
        modifier.put("orgUnit_P", employeePost.getOrgUnit().getPrepositionalCaseTitle() != null ? employeePost.getOrgUnit().getPrepositionalCaseTitle() : employeePost.getOrgUnit().getTitleWithType());
        modifier.put("lcPeriodType", contract.getType().getTitle());
        modifier.put("contractBeginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getBeginDate()));
        modifier.put("contractEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getEndDate()));
        modifier.put("postType", Arrays.asList(PostTypeCodes.SECOND_JOB_OUTER, PostTypeCodes.SECOND_JOB_INNER).contains(employeePost.getPostType().getCode()) ? "по совместительству" : (employeePost.getPostType().getCode().equals(PostTypeCodes.MAIN_JOB) ? "по основной работе" : ""));
        modifier.put("conditions", employeePost.getWorkWeekDuration().getDaysAmount() + " дн. в нед. " + employeePost.getWeekWorkLoad().getHoursAmount() + " ч.");
        modifier.put("staffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(employeePost.getSalary()));
        modifier.put("payments", CommonExtractUtil.getFormattedBonusListStr(paymentList, "employeeLabourContract", false));
        modifier.put("employee_fioShort", employeePost.getFio());
        modifier.put("passportAddress", employeePost.getPerson().getIdentityCard().getAddress() != null ? employeePost.getPerson().getIdentityCard().getAddress().getTitleWithFlat() : "");
        modifier.put("phoneNumber", employeePost.getPerson().getContactData().getPhoneMobile());
        modifier.put("passNumber", (employeePost.getPerson().getIdentityCard().getSeria() != null ? employeePost.getPerson().getIdentityCard().getSeria() : "") + " " + (employeePost.getPerson().getIdentityCard().getNumber() != null ? employeePost.getPerson().getIdentityCard().getNumber() : ""));
        modifier.put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(employeePost.getPerson().getIdentityCard().getIssuanceDate()) + (employeePost.getPerson().getIdentityCard().getIssuanceDate() != null ? " г." : ""));
        modifier.put("issuancePlace", employeePost.getPerson().getIdentityCard().getIssuancePlace());
        modifier.put("rector_fioShort", topOrgUnit.getHead() != null ? topOrgUnit.getHead().getEmployee().getPerson().getIdentityCard().getFio() : "");

        modifier.modify(document);
    }

    protected void injectTableModifier(RtfDocument document, EmployeeLabourContract contract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
    }
}
