package ru.tandemservice.uniorgma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniorgma_2x7x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniecc отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniecc") )
				throw new RuntimeException("Module 'uniecc' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "uniecc");

		// удалить сущность uniscEduAgreement2Entrant
		{
			// удалить таблицу
			tool.dropTable("sc_eduag2entrant", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniscEduAgreement2Entrant");
		}

		// удалить сущность nextOfKinStatusRelation
		{
			// удалить таблицу
			tool.dropTable("ecc_next_of_kin", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("nextOfKinStatusRelation");
		}

		// удалить сущность eccEnrollmentCampaign
		{
			// удалить таблицу
			tool.dropTable("ecc_enrollment_campaign", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eccEnrollmentCampaign");
		}
   }
}