package ru.tandemservice.uniorgma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniorgma_2x7x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unitraining отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unitraining") )
				throw new RuntimeException("Module 'unitraining' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unitraining");

		// удалить персистентный интерфейс ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner
		{
			// удалить view
			tool.dropView("ibrssettingsowner_v");
		}

		// удалить сущность trOrgUnitSettings
		{
			// удалить таблицу
			tool.dropTable("tr_ou_settings", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trOrgUnitSettings");
		}

		// удалить сущность trJournalModule
		{
			// удалить таблицу
			tool.dropTable("tr_journal_module", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalModule");
		}

		// удалить сущность trJournalGroupStudent
		{
			// удалить таблицу
			tool.dropTable("tr_journal_group_student", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalGroupStudent");
		}

		// удалить сущность trJournalGroup
		{
			// удалить таблицу
			tool.dropTable("tr_journal_group", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalGroup");
		}

		// удалить сущность trEventLoad
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event_load", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEventLoad");
		}

		// удалить сущность trEventAddon
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event_addon", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEventAddon");
		}

		// удалить сущность trEventAction
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event_action", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEventAction");
		}

		// удалить сущность trJournalEvent
		{
			// удалить таблицу
			tool.dropTable("tr_journal_event", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournalEvent");
		}

		// удалить сущность trJournal
		{
			// удалить таблицу
			tool.dropTable("tr_journal", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trJournal");
		}

		// удалить сущность trEduGroupEventStudent
		{
			// удалить таблицу
			tool.dropTable("tr_edugrp_event_student", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEduGroupEventStudent");
		}

		// удалить сущность trEduGroupEvent
		{
			// удалить таблицу
			tool.dropTable("tr_edugrp_event", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trEduGroupEvent");
		}

		// удалить сущность trBrsCoefficientValue
		{
			// удалить таблицу
			tool.dropTable("tr_brs_coef_val", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trBrsCoefficientValue");
		}

		// удалить сущность trBrsCoefficientOwnerType
		{
			// удалить таблицу
			tool.dropTable("tr_c_coef_owner_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trBrsCoefficientOwnerType");
		}

		// удалить сущность trBrsCoefficientDef
		{
			// удалить таблицу
			tool.dropTable("tr_c_coef_def", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trBrsCoefficientDef");
		}

		// удалить сущность trAddonType
		{
			// удалить таблицу
			tool.dropTable("tr_c_addon_type", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trAddonType");
		}

		// удалить сущность trAbsenceReason
		{
			// удалить таблицу
			tool.dropTable("tr_c_absence_reason", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("trAbsenceReason");
		}

		////////////////////////////////////////////////////////////////////////////////
		// модуль unispp отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unispp") )
				throw new RuntimeException("Module 'unispp' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unispp");

		// удалить сущность sppTeacherSessionPreferenceList
		{
			// удалить таблицу
			tool.dropTable("spptchrsssnprfrnclst_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherSessionPreferenceList");
		}

		// удалить сущность sppTeacherSessionPreferenceElement
		{
			// удалить таблицу
			tool.dropTable("spptchrsssnprfrncelmnt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherSessionPreferenceElement");
		}

		// удалить сущность sppTeacherPreferenceWeekRow
		{
			// удалить таблицу
			tool.dropTable("sppteacherpreferenceweekrow_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherPreferenceWeekRow");
		}

		// удалить сущность sppTeacherPreferenceList
		{
			// удалить таблицу
			tool.dropTable("sppteacherpreferencelist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherPreferenceList");
		}

		// удалить сущность sppTeacherPreferenceElement
		{
			// удалить таблицу
			tool.dropTable("sppteacherpreferenceelement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherPreferenceElement");
		}

		// удалить сущность sppTeacherDailyPreferenceList
		{
			// удалить таблицу
			tool.dropTable("spptchrdlyprfrnclst_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherDailyPreferenceList");
		}

		// удалить сущность sppTeacherDailyPreferenceElement
		{
			// удалить таблицу
			tool.dropTable("spptchrdlyprfrncelmnt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppTeacherDailyPreferenceElement");
		}

		// удалить сущность sppScheduleWeekRow
		{
			// удалить таблицу
			tool.dropTable("sppscheduleweekrow_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleWeekRow");
		}

		// удалить сущность sppScheduleWarningLog
		{
			// удалить таблицу
			tool.dropTable("sppschedulewarninglog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleWarningLog");
		}

		// удалить сущность sppScheduleStatus
		{
			// удалить таблицу
			tool.dropTable("sppschedulestatus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleStatus");
		}

		// удалить сущность sppScheduleSessionWarningLog
		{
			// удалить таблицу
			tool.dropTable("sppschedulesessionwarninglog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleSessionWarningLog");
		}

		// удалить сущность sppScheduleSessionSeason
		{
			// удалить таблицу
			tool.dropTable("sppschedulesessionseason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleSessionSeason");
		}

		// удалить сущность sppScheduleSessionPrintForm
		{
			// удалить таблицу
			tool.dropTable("sppschedulesessionprintform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleSessionPrintForm");
		}

		// удалить сущность sppScheduleSessionEvent
		{
			// удалить таблицу
			tool.dropTable("sppschedulesessionevent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleSessionEvent");
		}

		// удалить сущность sppScheduleSession
		{
			// удалить таблицу
			tool.dropTable("sppschedulesession_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleSession");
		}

		// удалить сущность sppScheduleSeason
		{
			// удалить таблицу
			tool.dropTable("sppscheduleseason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleSeason");
		}

		// удалить сущность sppSchedulePrintForm
		{
			// удалить таблицу
			tool.dropTable("sppscheduleprintform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppSchedulePrintForm");
		}

		// удалить сущность sppSchedulePeriodFix
		{
			// удалить таблицу
			tool.dropTable("sppscheduleperiodfix_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppSchedulePeriodFix");
		}

		// удалить сущность sppSchedulePeriod
		{
			// удалить таблицу
			tool.dropTable("sppscheduleperiod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppSchedulePeriod");
		}

		// удалить сущность sppScheduleICal
		{
			// удалить таблицу
			tool.dropTable("sppscheduleical_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleICal");
		}

		// удалить сущность sppScheduleDailyWarningLog
		{
			// удалить таблицу
			tool.dropTable("sppscheduledailywarninglog_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleDailyWarningLog");
		}

		// удалить сущность sppScheduleDailySeason
		{
			// удалить таблицу
			tool.dropTable("sppscheduledailyseason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleDailySeason");
		}

		// удалить сущность sppScheduleDailyPrintForm
		{
			// удалить таблицу
			tool.dropTable("sppscheduledailyprintform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleDailyPrintForm");
		}

		// удалить сущность sppScheduleDailyEvent
		{
			// удалить таблицу
			tool.dropTable("sppscheduledailyevent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleDailyEvent");
		}

		// удалить сущность sppScheduleDaily
		{
			// удалить таблицу
			tool.dropTable("sppscheduledaily_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppScheduleDaily");
		}

		// удалить сущность sppSchedule
		{
			// удалить таблицу
			tool.dropTable("sppschedule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppSchedule");
		}

		// удалить сущность sppRegElementLoadCheck
		{
			// удалить таблицу
			tool.dropTable("spp_reg_element_load_check", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppRegElementLoadCheck");
		}

		// удалить сущность sppRegElementExt
		{
			// удалить таблицу
			tool.dropTable("spp_reg_element_ext", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppRegElementExt");
		}

		// удалить сущность sppDisciplinePreferenceList
		{
			// удалить таблицу
			tool.dropTable("sppdisciplinepreferencelist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppDisciplinePreferenceList");
		}

		// удалить сущность sppBellScheduleEntry
		{
			// удалить таблицу
			tool.dropTable("spp_bell_schedule_entry", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppBellScheduleEntry");
		}

		// удалить сущность sppBellSchedule
		{
			// удалить таблицу
			tool.dropTable("spp_bell_schedule", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sppBellSchedule");
		}
    }
}