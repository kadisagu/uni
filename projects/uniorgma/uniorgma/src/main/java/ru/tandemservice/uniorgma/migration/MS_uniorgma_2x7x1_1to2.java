package ru.tandemservice.uniorgma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniorgma_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unidip отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unidip") )
				throw new RuntimeException("Module 'unidip' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unidip");

		// удалить сущность dipStuExcludeExtract
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("dip_stu_exclude_extract_t", "abstractstudentextract_t");

			// удалить таблицу
			tool.dropTable("dip_stu_exclude_extract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipStuExcludeExtract");
		}

		// удалить сущность dipScriptItem
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("dip_c_script_t", "scriptitem_t");

			// удалить таблицу
			tool.dropTable("dip_c_script_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipScriptItem");
		}

		// удалить сущность dipDocTemplateCatalog
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("dip_c_doc_template_t", "scriptitem_t");

			// удалить таблицу
			tool.dropTable("dip_c_doc_template_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipDocTemplateCatalog");
		}

		// удалить сущность diplomaTemplate
		{
			// удалить таблицу
			tool.dropTable("dip_template_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaTemplate");
		}

		// удалить сущность diplomaObject
		{
			// удалить таблицу
			tool.dropTable("dip_object_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaObject");
		}

		// удалить сущность diplomaIssuance
		{
			// удалить таблицу
			tool.dropTable("dip_issuance_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaIssuance");
		}

		// удалить сущность diplomaEpvTemplateDefaultRel
		{
			// удалить таблицу
			tool.dropTable("dip_template_epv_def_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaEpvTemplateDefaultRel");
		}

		// удалить сущность diplomaStateExamRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_state_exam_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaStateExamRow");
		}

		// удалить сущность diplomaQualifWorkRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_qual_work_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaQualifWorkRow");
		}

		// удалить сущность diplomaPracticeRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_practice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaPracticeRow");
		}

		// удалить сущность diplomaOptDisciplineRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_opt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaOptDisciplineRow");
		}

		// удалить сущность diplomaDisciplineRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_disc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaDisciplineRow");
		}

		// удалить сущность diplomaCourseWorkRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_c_work_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaCourseWorkRow");
		}

		// удалить сущность diplomaContentRow
		{
			// удалить таблицу
			tool.dropTable("dip_row_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaContentRow");
		}

		// удалить сущность diplomaContentRegElPartFControlAction
		{
			// удалить таблицу
			tool.dropTable("dip_row_rfca_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaContentRegElPartFControlAction");
		}

		// удалить сущность diplomaContentIssuance
		{
			// удалить таблицу
			tool.dropTable("dip_content_issuance_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaContentIssuance");
		}

		// удалить сущность diplomaContent
		{
			// удалить таблицу
			tool.dropTable("dip_content_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaContent");
		}

		// удалить сущность diplomaAcademyRenameData
		{
			// удалить таблицу
			tool.dropTable("dip_academy_rename_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("diplomaAcademyRenameData");
		}

		// удалить сущность dipFormingRowAlgorithm
		{
			// удалить таблицу
			tool.dropTable("dip_form_row_algorithm_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipFormingRowAlgorithm");
		}

		// удалить сущность dipEduInOtherOrganization
		{
			// удалить таблицу
			tool.dropTable("dip_edu_other_organization_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipEduInOtherOrganization");
		}

		// удалить сущность dipDocumentType
		{
			// удалить таблицу
			tool.dropTable("dip_c_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipDocumentType");
		}

		// удалить сущность dipDiplomaBlankReport
		{
			// удалить таблицу
			tool.dropTable("dip_diploma_blank_report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipDiplomaBlankReport");
		}

		// удалить сущность dipDiplomaBlank
		{
			// удалить таблицу
			tool.dropTable("dip_diploma_blank_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipDiplomaBlank");
		}

		// удалить сущность dipContentIssuanceToDipExtractRelation
		{
			// удалить таблицу
			tool.dropTable("dip_iss_cont_to_extract_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipContentIssuanceToDipExtractRelation");
		}

		// удалить сущность dipBlankType
		{
			// удалить таблицу
			tool.dropTable("dip_c_blank_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipBlankType");
		}

		// удалить сущность dipBlankState
		{
			// удалить таблицу
			tool.dropTable("dip_c_blank_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipBlankState");
		}

		// удалить сущность dipBlankDisposalReason
		{
			// удалить таблицу
			tool.dropTable("dip_c_blank_disposal_reason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipBlankDisposalReason");
		}

		// удалить сущность dipAssistantManager
		{
			// удалить таблицу
			tool.dropTable("dip_assistant_manager_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipAssistantManager");
		}

		// удалить сущность dipAggregationMethod
		{
			// удалить таблицу
			tool.dropTable("dip_c_aggregation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipAggregationMethod");
		}

		// удалить сущность dipAdditionalInformation
		{
			// удалить таблицу
			tool.dropTable("dip_additional_information_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipAdditionalInformation");
		}

		// удалить сущность dipAddInfoEduForm
		{
			// удалить таблицу
			tool.dropTable("dip_add_info_edu_form_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("dipAddInfoEduForm");
		}
   }
}