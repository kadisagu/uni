/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.util;

/**
 * @author Alexander Shaburov
 * @since 27.11.12
 */
public interface ILabourContractAgreementTemplateDef
{
    Long getTemplateId();

    /**
     * Код для шаблона.
     * По этому коду достается шаблон EmployeeTemplateDocument.
     * По этому имени создается бин в OrgmaLabourContractAgreementManager.
     */
    String getTemplateCode();

    String getTemplateTitle();
}
