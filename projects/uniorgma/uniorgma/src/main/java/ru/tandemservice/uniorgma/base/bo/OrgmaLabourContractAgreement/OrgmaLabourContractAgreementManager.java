/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.logic.OrgmaEmployeePostLabourContractAgreementPaymentsPrint;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.util.ILabourContractAgreementTemplateDef;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.util.LabourContractAgreementTemplateDef;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.logic.IOrgmaEmployeePostLabourContractAgreementPrint;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.logic.OrgmaEmployeePostLabourContractAgreementPrint;

/**
 * @author Alexander Shaburov
 * @since 27.11.12
 */
@Configuration
public class OrgmaLabourContractAgreementManager extends BusinessObjectManager
{
    public static final Long CONTRACT_AGREEMENT_TEMPLATE_ID = 1L;
    public static final Long CONTRACT_AGREEMENT_PAYMENTS_TEMPLATE_ID = 2L;

    public static OrgmaLabourContractAgreementManager instance()
    {
        return instance(OrgmaLabourContractAgreementManager.class);
    }

    @Bean
    public ItemListExtPoint<ILabourContractAgreementTemplateDef> labourContractAgreementTemplateListExtPoint()
    {
        ILabourContractAgreementTemplateDef template1 = new LabourContractAgreementTemplateDef(CONTRACT_AGREEMENT_TEMPLATE_ID, "employeeLabourContractAgreementGen", "Дополнительное соглашение к трудовому договору на перевод");
        ILabourContractAgreementTemplateDef template2 = new LabourContractAgreementTemplateDef(CONTRACT_AGREEMENT_PAYMENTS_TEMPLATE_ID, "employeeLabourContractAgreementPaymentsGen", "Дополнительное соглашение к трудовому договору на установление доплат");

        return itemList(ILabourContractAgreementTemplateDef.class)
                .add(template1.getTemplateId().toString(), template1)
                .add(template2.getTemplateId().toString(), template2)
                .create();
    }

    @Bean
    public IOrgmaEmployeePostLabourContractAgreementPrint employeeLabourContractAgreementGen()
    {
        return new OrgmaEmployeePostLabourContractAgreementPrint();
    }

    @Bean
    public IOrgmaEmployeePostLabourContractAgreementPrint employeeLabourContractAgreementPaymentsGen()
    {
        return new OrgmaEmployeePostLabourContractAgreementPaymentsPrint();
    }

    public IOrgmaEmployeePostLabourContractAgreementPrint getPrintController(String code)
    {
        if (null == (code = StringUtils.trimToNull(code))) {
            throw new IllegalArgumentException("code is empty");
        }
        return instance().getMeta().getConfigContext().getBean("OrgmaLabourContractAgreement." + code, IOrgmaEmployeePostLabourContractAgreementPrint.class);
    }
}
