/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.ui.Add;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.OrgmaLabourContractAgreementManager;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.util.ILabourContractAgreementTemplateDef;

import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 27.11.12
 */
@Input
({
    @Bind(key = "contractAgreementId", binding = "contractAgreementId", required = true)
})
public class OrgmaLabourContractAgreementAddUI extends UIPresenter
{
    public static final String PROP_AGREEMENT = "agreement";

    private Long _contractAgreementId;
    private ContractCollateralAgreement _agreement;

    private DataWrapper _template;

    @Override
    public void onComponentRender()
    {
        ContextLocal.beginPageTitlePart(getConfig().getProperty("ui.sticker"));
    }

    @Override
    public void onComponentRefresh()
    {
        _agreement = DataAccessServices.dao().getNotNull(ContractCollateralAgreement.class, _contractAgreementId);

        // умолчания
        getSettings().set("agreementPeriodFrom", _agreement.getDate());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PROP_AGREEMENT, _agreement);
    }

    public void onClickApply()
    {
        final ILabourContractAgreementTemplateDef templateDef = OrgmaLabourContractAgreementManager.instance().labourContractAgreementTemplateListExtPoint().getItems().get(_template.getId().toString());

        final EmployeeTemplateDocument templateDocument = DataAccessServices.dao().get(EmployeeTemplateDocument.class, EmployeeTemplateDocument.code().s(), templateDef.getTemplateCode());

        RtfDocument document = null;
        if (templateDef.getTemplateId().equals(OrgmaLabourContractAgreementManager.CONTRACT_AGREEMENT_TEMPLATE_ID))
        {
            final AbstractEmployeeExtract  order = getSettings().get("order");
            document = OrgmaLabourContractAgreementManager.instance().getPrintController(templateDef.getTemplateCode()).createPrintForm(templateDocument.getContent(), new MultiKey(_contractAgreementId, order != null ? order.getId() : null));
        }
        else if (templateDef.getTemplateId().equals(OrgmaLabourContractAgreementManager.CONTRACT_AGREEMENT_PAYMENTS_TEMPLATE_ID))
        {
            final Date begin = getSettings().get("agreementPeriodFrom");
            final Date end = getSettings().get("agreementPeriodTo");
            document = OrgmaLabourContractAgreementManager.instance().getPrintController(templateDef.getTemplateCode()).createPrintForm(templateDocument.getContent(), new MultiKey(_contractAgreementId, begin, end));
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("employeePostLabourContractAgreement.rtf").document(document), false);

        deactivate();
    }

    public boolean isAgreementVisible()
    {
        return _template != null && _template.getId().equals(OrgmaLabourContractAgreementManager.CONTRACT_AGREEMENT_TEMPLATE_ID);
    }

    public boolean isAgreementPaymentsVisible()
    {
        return _template != null && _template.getId().equals(OrgmaLabourContractAgreementManager.CONTRACT_AGREEMENT_PAYMENTS_TEMPLATE_ID);
    }

    // Getters & Setters

    public ContractCollateralAgreement getAgreement()
    {
        return _agreement;
    }

    public void setAgreement(ContractCollateralAgreement agreement)
    {
        _agreement = agreement;
    }

    public Long getContractAgreementId()
    {
        return _contractAgreementId;
    }

    public void setContractAgreementId(Long contractAgreementId)
    {
        _contractAgreementId = contractAgreementId;
    }

    public DataWrapper getTemplate()
    {
        return _template;
    }

    public void setTemplate(DataWrapper template)
    {
        _template = template;
    }
}
