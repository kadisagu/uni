package ru.tandemservice.uniorgma.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniorgma_2x7x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisp отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unisp") )
				throw new RuntimeException("Module 'unisp' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unisp");

		// удалить сущность unispDocumentTemplate
		{
			// удалить таблицу
			tool.dropTable("unispdocumenttemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unispDocumentTemplate");
		}

		// удалить сущность studentPaymentsOrder
		{
			// удалить таблицу
			tool.dropTable("sp_order", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentsOrder");
		}

		// удалить сущность studentPaymentType
		{
			// удалить таблицу
			tool.dropTable("studentpaymenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentType");
		}

		// удалить сущность studentPaymentToExtractTypeRel
		{
			// удалить таблицу
			tool.dropTable("stdntpymnttextrcttyprl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentToExtractTypeRel");
		}

		// удалить сущность studentPaymentReasonToExtractTypeRel
		{
			// удалить таблицу
			tool.dropTable("stdntpymntrsntextrcttyprl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentReasonToExtractTypeRel");
		}

		// удалить сущность studentPaymentReasonToBasicsRel
		{
			// удалить таблицу
			tool.dropTable("stdntpymntrsntbscsrl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentReasonToBasicsRel");
		}

		// удалить сущность studentPaymentProtocolVisa
		{
			// удалить таблицу
			tool.dropTable("studentpaymentprotocolvisa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentProtocolVisa");
		}

		// удалить сущность studentPaymentParagraphToReason
		{
			// удалить таблицу
			tool.dropTable("sp_paragraph2reason", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentParagraphToReason");
		}

		// удалить сущность studentPaymentParagraphToBasic
		{
			// удалить таблицу
			tool.dropTable("sp_paragraph2basic", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentParagraphToBasic");
		}

		// удалить сущность studentPaymentParagraph
		{
			// удалить таблицу
			tool.dropTable("sp_paragraph", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentParagraph");
		}

		// удалить сущность studentPaymentOrderVisaGroup
		{
			// удалить таблицу
			tool.dropTable("studentpaymentordervisagroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentOrderVisaGroup");
		}

		// удалить сущность studentPaymentOrderVisa
		{
			// удалить таблицу
			tool.dropTable("studentpaymentordervisa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentOrderVisa");
		}

		// удалить сущность studentPaymentOrderTextRelation
		{
			// удалить таблицу
			tool.dropTable("sp_order_text", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentOrderTextRelation");
		}

		// удалить сущность studentPaymentOrderRelation
		{
			// удалить таблицу
			tool.dropTable("studentpaymentorderrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentOrderRelation");
		}

		// удалить сущность studentPaymentOrderProtocol
		{
			// удалить таблицу
			tool.dropTable("studentpaymentorderprotocol_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentOrderProtocol");
		}

		// удалить сущность studentPaymentExtractType
		{
			// удалить таблицу
			tool.dropTable("studentpaymentextracttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentExtractType");
		}

		// удалить сущность studentPaymentExtractTextRelation
		{
			// удалить таблицу
			tool.dropTable("sp_extract_text", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentExtractTextRelation");
		}

		// удалить сущность studentPaymentExtractReason
		{
			// удалить таблицу
			tool.dropTable("studentpaymentextractreason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentExtractReason");
		}

		// удалить сущность studentPaymentExtractBasic
		{
			// удалить таблицу
			tool.dropTable("studentpaymentextractbasic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentExtractBasic");
		}

		// удалить сущность studentPaymentAppointmentExtract
		{
			// удалить таблицу
			tool.dropTable("sp_extract_appoint", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentAppointmentExtract");
		}

		// удалить сущность studentPaymentExtract
		{
			// удалить таблицу
			tool.dropTable("sp_extract_base", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPaymentExtract");
		}

		// удалить сущность studentPayment
		{
			// удалить таблицу
			tool.dropTable("studentpayment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentPayment");
		}

		// удалить сущность paymentKind
		{
			// удалить таблицу
			tool.dropTable("paymentkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("paymentKind");
		}
    }
}