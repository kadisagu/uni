/**
 *$Id:$
 */
package ru.tandemservice.uniorgma.base.ext.EmpEmployee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Shaburov
 * @since 23.10.12
 */
@Configuration
public class EmpEmployeeExtManager extends BusinessObjectExtensionManager
{
}
