package ru.tandemservice.uniorgma.component.orgunit.EppEpvAcceptionTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.Model;

import java.util.List;

/**
 * @author Igor Lunin
 * @since 30.10.2014
 */
public class Controller extends ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.Controller
{
    public void onClickCreateRegistryElements(IBusinessComponent component)
    {
        if (!"ok".equals(component.getClientParameter()))
        {
            String confirmStr = "Данное действие создает и привязывает для всех отобранных фильтрами строк учебного плана дисциплины, если таковых не было. Вы уверены?";
            ConfirmInfo confirm = new ConfirmInfo(confirmStr,
                    new ClickButtonAction("createRegistryElements", "ok", false));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }

        Model model = getModel(component);

        IDAO dao = (IDAO) getDao();
        List<Long> registryRowIds = dao.getRegistryRowIds(model.getOrgUnitId(), model.getSettings());
        dao.createRegistryElements(registryRowIds);

        component.refresh();
    }
}
