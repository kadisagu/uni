/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.util;

/**
 * @author Alexander Shaburov
 * @since 27.11.12
 */
public class LabourContractAgreementTemplateDef implements ILabourContractAgreementTemplateDef
{
    private Long _id;
    private String _code;
    private String _title;

    public LabourContractAgreementTemplateDef(Long id, String code, String title)
    {
        _id = id;
        _code = code;
        _title = title;
    }

    @Override
    public String getTemplateTitle()
    {
        return _title;
    }

    @Override
    public String getTemplateCode()
    {
        return _code;
    }

    @Override
    public Long getTemplateId()
    {
        return _id;
    }
}
