/**
 *$Id$
 */
package ru.tandemservice.uniorgma.component.singleemplextract.e3;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.e3.EmployeePostAddSExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.UniempDefines;

/**
 * @author Alexander Shaburov
 * @since 22.01.13
 */
public class OrgmaEmployeePostAddSExtractPrint extends EmployeePostAddSExtractPrint
{
    @Override
    protected String getPaymentValueStr(EmployeeBonus bonus, Double staffRateSumm)
    {
        if (bonus.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_RUBLES))
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue() * staffRateSumm) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
        else
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
    }
}
