/**
 *$Id$
 */
package ru.tandemservice.uniorgma.component.listemplextract.e1;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import ru.tandemservice.moveemployee.component.listemplextract.e1.EmployeeAddEmplListExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddEmplListExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uniemp.UniempDefines;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 22.01.13
 */
public class OrgmaEmployeeAddEmplListExtractPrint extends EmployeeAddEmplListExtractPrint
{
    @Override
    protected String[][] getPreparedOrderData(EmployeeListOrder order)
    {
        List<EmployeeAddEmplListExtract> extractList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractList(order);
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        List<String[]> resultList = new ArrayList<String[]>();

        for(EmployeeAddEmplListExtract extract : extractList)
        {
            List<FinancingSourceDetails> sourceDetailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
            BigDecimal staffRate = new BigDecimal(0);
            for (FinancingSourceDetails details : sourceDetailsList)
                staffRate = staffRate.add(new BigDecimal(details.getStaffRate()));

            Iterator<EmployeeBonus> bonusIterator = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract).iterator();
            StringBuilder paymentBuilder = new StringBuilder();
            while (bonusIterator.hasNext())
            {
                EmployeeBonus bonus = bonusIterator.next();

                if (bonus.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_RUBLES))
                    paymentBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue() * staffRate.setScale(2, RoundingMode.HALF_UP).doubleValue()))
                            .append(" ")
                            .append(bonus.getPayment().getPaymentUnit().getShortTitle())
                            .append(" ")
                            .append(bonus.getPayment().getShortTitle());
                else
                    paymentBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()))
                            .append(" ")
                            .append(bonus.getPayment().getPaymentUnit().getShortTitle())
                            .append(" ")
                            .append(bonus.getPayment().getShortTitle());

                if (bonusIterator.hasNext())
                    paymentBuilder.append(",PAR");
            }

            String[] line = new String[12];
            Employee employee = extract.getEmployee();
            line[0] = employee.getPerson().getFullFio();
            line[1] = new EmployeeCodeFormatter().format(employee.getEmployeeCode());
            line[2] = null != extract.getOrgUnit().getNominativeCaseTitle() ? extract.getOrgUnit().getNominativeCaseTitle() : extract.getOrgUnit().getFullTitle();
            line[3] = extract.getPostBoundedWithQGandQL().getTitle();
            line[4] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extract.getSalary()); // TODO: payments
            line[5] = paymentBuilder.toString();
            line[6] = extract.getLabourContractNumber();
            line[7] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLabourContractDate());
            line[8] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate());
            line[9] = null == extract.getEndDate() ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate());
            line[10] = 0 != extract.getTrialPeriod() ? String.valueOf(extract.getTrialPeriod()) : "-";
            resultList.add(line);
        }

        return resultList.toArray(new String[extractList.size()][]);
    }

    @Override
    protected RtfRowIntercepterBase getRowIntercepterBase()
    {
        return new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 5 && value.contains("PAR"))
                {
                    Iterator<String> parts = Arrays.asList(value.split("PAR")).iterator();
                    RtfString rtfString = new RtfString();
                    while (parts.hasNext())
                    {
                        String part = parts.next();

                        rtfString.append(part);

                        if (parts.hasNext())
                            rtfString.par();
                    }

                    return rtfString.toList();
                }
                return null;
            }
        };
    }
}
