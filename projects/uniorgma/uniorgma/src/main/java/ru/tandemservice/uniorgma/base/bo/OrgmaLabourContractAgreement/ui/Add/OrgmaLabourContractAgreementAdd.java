/**
 *$Id$
 */
package ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.codes.EmployeeExtractGroupCodes;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.OrgmaLabourContractAgreementManager;
import ru.tandemservice.uniorgma.base.bo.OrgmaLabourContractAgreement.util.ILabourContractAgreementTemplateDef;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 27.11.12
 */
@Configuration
public class OrgmaLabourContractAgreementAdd extends BusinessComponentManager
{
    public static final String TEMPLATE_DS = "templateDS";
    public static final String ORDER_DS = "orderDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(TEMPLATE_DS, templateDSHandler()))
                .addDataSource(selectDS(ORDER_DS, orderDSHandler()).addColumn(AbstractEmployeeExtract.titleOrder().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> templateDSHandler()
    {
        SimpleTitledComboDataSourceHandler handler = new SimpleTitledComboDataSourceHandler(getName());
        for (ILabourContractAgreementTemplateDef templateDef : OrgmaLabourContractAgreementManager.instance().labourContractAgreementTemplateListExtPoint().getItems().values())
            handler.addRecord(templateDef.getTemplateId(), templateDef.getTemplateTitle());

        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), AbstractEmployeeExtract.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                ContractCollateralAgreement agreement = context.get(OrgmaLabourContractAgreementAddUI.PROP_AGREEMENT);

                Employee employee = agreement.getContract().getEmployeePost().getEmployee();

                DQLSelectBuilder extractBuilder = new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "e").column(property(AbstractEmployeeExtract.id().fromAlias("e")))
                        .where(eq(property(AbstractEmployeeExtract.entity().employee().fromAlias("e")), value(employee)))
                        .where(eq(property(AbstractEmployeeExtract.type().group().code().fromAlias("e")), value(EmployeeExtractGroupCodes.ADD_EMPLOYEE_POST)));

                dql.where(in(property(AbstractEmployeeExtract.id().fromAlias(alias)), extractBuilder.buildQuery()));
                dql.order(property(AbstractEmployeeExtract.createDate().fromAlias(alias)), OrderDirection.desc);
            }
        }
                .filter(AbstractEmployeeExtract.number())
                .filter(AbstractEmployeeExtract.createDate())
                .filter(AbstractEmployeeExtract.type().title());
    }
}
