/* $Id$ */
package ru.tandemservice.unimirea.component.place.BuildingAddEdit;

import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentKind;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDocumentPurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocumentRight;
import ru.tandemservice.uniplaces.component.place.BuildingAddEdit.Model;
import ru.tandemservice.uniplaces.util.PlacesUtil;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.BuildingAddEdit.DAO
{

    @Override
    public void prepare(Model model)
    {
         super.prepare(model);
        UniplacesDocumentPurpose documentPurpose = getCatalogItem(UniplacesDocumentPurpose.class, UniplacesDefines.CATALOG_DOCUMENT_PURPOSE_BUILDING_DISPOSAL_RIGHT);
        // фильтруем виды документов по назначению
        List<UniplacesDocumentKind> documentKinds = PlacesUtil.getFilteredDocumentKinds(getCatalogItemList(UniplacesDocumentKind.class), documentPurpose);
        ((ru.tandemservice.unimirea.component.place.BuildingAddEdit.Model)model).setDocumentKindList(HierarchyUtil.listHierarchyNodesWithParents(documentKinds, CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));

        MireaSecondDisposalRight secondDisposalRight = get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.L_REGISTRY_RECORD, model.getBuilding());
        if (secondDisposalRight == null)
            secondDisposalRight = new MireaSecondDisposalRight();
        ((ru.tandemservice.unimirea.component.place.BuildingAddEdit.Model)model).setSecondRight(secondDisposalRight);
        ((ru.tandemservice.unimirea.component.place.BuildingAddEdit.Model) model).setSecondRightDocument(secondDisposalRight.getDocument() == null ?
                                                                                                               new UniplacesDocumentRight() : secondDisposalRight.getDocument());
    }


    @Override
    public void update(Model model)
    {
        super.update(model);

        UniplacesDocument document = ((ru.tandemservice.unimirea.component.place.BuildingAddEdit.Model)model).getSecondRightDocument();
        MireaSecondDisposalRight secondRight = ((ru.tandemservice.unimirea.component.place.BuildingAddEdit.Model)model).getSecondRight();

        if (secondRight.getDisposalRight() == null)
        {
            if (secondRight.getId() != null)
                delete(secondRight);
            return;
        }

        UniplacesDocument oldDoc = secondRight.getDocument();
        if (document.getKind() != null)
        {
            saveOrUpdate(document);
            secondRight.setDocument(document);

        }
        else secondRight.setDocument(null);

        secondRight.setRegistryRecord(model.getBuilding());
        saveOrUpdate(secondRight);

        if (oldDoc != null && !oldDoc.equals(document))
            delete(oldDoc);
    }
}
