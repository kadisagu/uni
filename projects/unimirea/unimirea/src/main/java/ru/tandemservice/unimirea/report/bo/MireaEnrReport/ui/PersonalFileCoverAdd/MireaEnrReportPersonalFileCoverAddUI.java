/* $Id$ */
package ru.tandemservice.unimirea.report.bo.MireaEnrReport.ui.PersonalFileCoverAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.GlobalList.EnrReportBaseGlobalList;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimirea.report.bo.MireaEnrReport.MireaEnrReportManager;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
public class MireaEnrReportPersonalFileCoverAddUI extends UIPresenter
{
    private List<EnrEnrollmentCampaign> _enrollmentCampaignList;
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private List<EnrOrder> _enrollmentOrderList;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickApply()
    {
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(MireaEnrReportManager.instance().dao().createMassPersonalFileReport(this))
                        .fileName("EntrantPersonalFileReport.rtf")
                        .rtf(),
                false);

        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (MireaEnrReportPersonalFileCoverAdd.ENROLLMENT_ORDER_DS.equals(dataSource.getName()))
        {
            dataSource.put(MireaEnrReportPersonalFileCoverAdd.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
        }
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickCancel()
    {
        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public List<EnrOrder> getEnrollmentOrderList()
    {
        return _enrollmentOrderList;
    }

    public void setEnrollmentOrderList(List<EnrOrder> enrollmentOrderList)
    {
        _enrollmentOrderList = enrollmentOrderList;
    }
}