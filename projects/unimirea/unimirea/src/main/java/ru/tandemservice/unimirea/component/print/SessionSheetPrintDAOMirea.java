/*$Id$*/
package ru.tandemservice.unimirea.component.print;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.ISessionSheetPrintDAO;
import ru.tandemservice.unisession.print.SessionSheetPrintDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 25.11.2015
 */
public class SessionSheetPrintDAOMirea extends SessionSheetPrintDAO implements ISessionSheetPrintDAO
{
    @Override
    protected void addAdditionalData(RtfInjectModifier modifier, SessionSheetDocument sheet, SessionDocumentSlot slot, SessionMark mark)
    {
        final Student student = slot.getActualStudent();
        final OrgUnit orgUnit = sheet.getGroupOu();
        final EppRegistryElementPart registryElementPart = slot.getStudentWpeCAction().getStudentWpe().getRegistryElementPart();
        final EppRegistryElement registryElement = registryElementPart.getRegistryElement();
        final EppGroupTypeFCA type = slot.getStudentWpeCAction().getType();

        modifier.put("orgUnitTitle", orgUnit.getPrintTitle());
        switch (type.getCode())
        {
            case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
            case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
            case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
            case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
            {
                modifier.put("LTYPE", "ЗАЧЕТНЫЙ");
                break;
            }
            case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
            {
                modifier.put("LTYPE", "ЭКЗАМЕНАЦИОННЫЙ");
                break;
            }
            default:
                modifier.put("LTYPE", "");
        }

        modifier.put("department", registryElement.getOwner().getShortTitle());
        modifier.put("lnumber", student.getPersonalNumber());

        modifier.put("hours", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getSizeAsDouble()));
        modifier.put("ze", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getLaborAsDouble()));
    }
}
