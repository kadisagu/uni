/* $Id$ */
package ru.tandemservice.unimirea.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTabUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 30.09.2015
 */
public class EnrEntrantPubRequestTabExtUI extends UIAddon
{
    EnrEntrantPubRequestTabUI _presenter;
    public EnrEntrantPubRequestTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = getPresenter();
    }

    public boolean isCoverButtonActive()
    {
        EnrEntrantRequest currentEntrantRequest = _presenter.getCurrentEntrantRequest();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "com")
                .where(eq(property("com", EnrRequestedCompetition.request()), value(currentEntrantRequest)))
                .where(eq(property("com", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                .top(1);
        return IUniBaseDao.instance.get().existsEntity(builder.buildQuery());

    }
}