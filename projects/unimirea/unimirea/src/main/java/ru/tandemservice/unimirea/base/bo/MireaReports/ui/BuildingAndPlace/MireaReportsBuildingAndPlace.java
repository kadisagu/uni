/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.ui.BuildingAndPlace;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unimirea.base.bo.MireaReports.logic.*;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;

/**
 * @author Ekaterina Zvereva
 * @since 20.02.2015
 */
@Configuration
public class MireaReportsBuildingAndPlace extends BusinessComponentManager
{
    public static final String BUILDINGS_DS ="buildingDS";
    public static final String UNITS_DS ="unitDS";
    public static final String FLOORS_DS ="floorDS";
    public static final String PLACES_DS ="placeDS";
    public static final String ROOMS_DS ="roomDS";
    public static final String DISPOSAL_RIGHT_DS = "disposalRightDS";
    public static final Long AVALIABLE_RENT_TRUE = 0L;
    public static final Long AVALIABLE_RENT_FALSE = 1L;

    public static final Long STATUS_RENT_FREE = 0L;
    public static final Long STATUS_RENT_BUSY = 1L;




    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BUILDINGS_DS, buildingDSHandler()))
                .addDataSource(selectDS(UNITS_DS, unitsDSHandler()))
                .addDataSource(selectDS(FLOORS_DS, floorDSHandler()))
                .addDataSource(selectDS(PLACES_DS, placesDSHandler()))
                .addDataSource(selectDS(ROOMS_DS, roomsDSHandler()))
                .addDataSource(selectDS(DISPOSAL_RIGHT_DS, disposalRightHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> buildingDSHandler()
    {
        return new MireaReportBuildingDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> unitsDSHandler()
    {
        return new MireaReportUnitsDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> floorDSHandler()
    {
        return new MireaReportFloorDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> placesDSHandler()
    {
        return new MireaReportPlaceDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> roomsDSHandler()
    {
        return new MireaReportRoomDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> disposalRightHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesDisposalRight.class);
    }
}