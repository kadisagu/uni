/* $Id$ */
package ru.tandemservice.unimirea.component.extract.e1;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisp.component.extract.e1.StudentPaymentParagraphPrint;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 25.02.2016
 */
public class MireaStudentPaymentParagraphPrint extends StudentPaymentParagraphPrint
{
    @Override
    public RtfTableModifier createParagraphTableModifier(VirtualParagraph<StudentPaymentAppointmentExtract> paragraph)
    {
        DoubleFormatter formatter = new DoubleFormatter(2, true, true);
        RtfTableModifier tableModifier = new RtfTableModifier();

        List<String[]> paragraphDataLines = new ArrayList<>();
        for (StudentPaymentAppointmentExtract extract : paragraph.getExtractsList())
        {
            paragraphDataLines.add(new String[]{extract.getEntity().getPerson().getFullFio(), extract.getEntity().getPerNumber(),
                    extract.getEntity().getGroup().getTitle(), formatter.format(extract.getPaymentCostAsDouble())});
        }

        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));
        return tableModifier;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(VirtualParagraph<StudentPaymentAppointmentExtract> paragraph, int paragraphNumber)
    {
        StudentPaymentAppointmentExtract extract = paragraph.getFirstExtract();
        OrgUnit orgUnit = extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit();
        String qualification = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getQualificationTitleNullSafe();
        EducationLevelsHighSchool educationLevelsHighSchool = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
        EducationLevels educationLevels = educationLevelsHighSchool.getEducationLevel();
        EduProgramSubject programSubject = educationLevels.getEduProgramSubject();

        RtfInjectModifier injectModifier = new RtfInjectModifier()
                .put("paragraphNumber", String.valueOf(paragraphNumber))
                .put("formativeOrgUnit_G", orgUnit.getGenitiveCaseTitle() != null ? orgUnit.getGenitiveCaseTitle() : orgUnit.getPrintTitle())
                .put("qualification", !qualification.equals("") ? qualification.toLowerCase() + "ов" : "")
                .put("course", extract.getEntity().getCourse().getTitle())
                .put("programSubject", programSubject != null && !programSubject.getTitleWithCode().trim().isEmpty() ?
                        programSubject.getSubjectCode() + " «" + programSubject.getTitle() + "»" : educationLevelsHighSchool.getPrintTitle())
                .put("paymentBeginDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getPaymentStartDate()))
                .put("paymentEndDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getPaymentStopDate()));
        return injectModifier;
    }
}
