/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.mirea1.ListOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubModel;


/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class Controller extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderPub.Controller
{
    @Override
    protected void prepareListDataSource(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new SimpleColumn("Форма обучения", AbstractListOrderPubModel.DEVELOP_FORM_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", AbstractListOrderPubModel.COURSE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", AbstractListOrderPubModel.EDU_PROGRAM_SUBJECT).setClickable(false).setOrderable(false));
    }
}