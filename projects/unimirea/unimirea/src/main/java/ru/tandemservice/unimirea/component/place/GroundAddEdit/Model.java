/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundAddEdit;

import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.GroundAddEdit.Model
{
    private MireaSecondDisposalRight secondRight;
    private UniplacesDocument secondRightDocument;

    public UniplacesDocument getSecondRightDocument()
    {
        return secondRightDocument;
    }

    public void setSecondRightDocument(UniplacesDocument secondRightDocument)
    {
        this.secondRightDocument = secondRightDocument;
    }

    public MireaSecondDisposalRight getSecondRight()
    {
        return secondRight;
    }

    public void setSecondRight(MireaSecondDisposalRight secondRight)
    {
        this.secondRight = secondRight;
    }
}
