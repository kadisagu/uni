/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea1.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.likeUpper;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class DAO extends AbstractListParagraphAddEditDAO<MireaExcludeStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));

        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setGroupListModel(new GroupSelectModel(model, model, null, model.getParagraph().getOrder().getOrgUnit())
        {
            @Override
            protected boolean isNeedRequest()
            {
                if (getGroupModel().getCourse() == null) return false;
                if (getEduLevelModel().getDevelopForm() == null) return false;
                return getOrgUnit() != null || getEduLevelModel().getFormativeOrgUnit() != null;
            }

            protected DQLSelectBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, G_ALIAS).column(G_ALIAS);

                if (o != null)
                {
                    if (o instanceof Long) builder.where(eq(property(G_ALIAS + ".id"), value((Long) o)));
                    else if (o instanceof Collection) builder.where(in(property(G_ALIAS + ".id"), (Collection) o));
                }

                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().formativeOrgUnit()), value(getEduLevelModel().getFormativeOrgUnit())));
                builder.where(eq(property(G_ALIAS, Group.course()), value(getGroupModel().getCourse())));
                builder.where(eq(property(G_ALIAS, Group.educationOrgUnit().developForm()), value(getEduLevelModel().getDevelopForm())));
                builder.where(eq(property(G_ALIAS, Group.archival()), value(Boolean.FALSE)));
                builder.where(exists(new DQLSelectBuilder().fromEntity(Student.class, "st")
                                            .where(eq(property("st", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject()), value(model.getEduProgramSubject())))
                                            .where(eq(property("st", Student.archival()), value(Boolean.FALSE)))
                                             .where(eq(property("st", Student.group()), property(G_ALIAS))).buildQuery()));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(G_ALIAS, Group.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property(G_ALIAS, Group.P_TITLE), OrderDirection.asc);

                return builder;
            }
        });
        model.setEduProgramSubjectModel(new FullCheckSelectModel(EduProgramSubject.P_TITLE_WITH_CODE_INDEX_AND_GEN)
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                        .where(eq(property("s", Student.archival()), value(false)))
                        .where(eq(property("s", Student.educationOrgUnit().developForm()), value(model.getDevelopForm())))
                        .where(eq(property("s", Student.educationOrgUnit().formativeOrgUnit()), value(model.getFormativeOrgUnit())))
                        .joinPath(DQLJoinType.inner, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("s"), "eps")
                        .column(property("eps"))
                        .distinct();

                return new ListResult(builder.createStatement(getSession()).list());
            }

        });
        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model, false));

        ListStudentExtract firstExtract = model.getFirstExtract();
        if (firstExtract != null)
        {
            model.setEduProgramSubject(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject());
            model.setDevelopForm(firstExtract.getEntity().getEducationOrgUnit().getDevelopForm());

        }
        //заполняем поля сохраненными данными
        MireaExcludeStuListExtract extract = (MireaExcludeStuListExtract) model.getParagraph().getFirstExtract();
        if (model.getParagraphId() != null && extract != null)
        {
            model.setCourse(extract.getEntity().getCourse());
            model.setExcludeDate(extract.getExcludeDate());
            model.setGroupList(model.getParagraph().getExtractList().stream().map(e->((Student)e.getEntity()).getGroup()).collect(Collectors.toList()));
            model.setHolidayFrom(extract.getHolidayFrom());
            model.setHolidayTo(extract.getHolidayTo());

        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject(), model.getEduProgramSubject()))
                .add(MQExpression.eq(STUDENT_ALIAS, Student.course(), model.getCourse()))
                .add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), model.getFormativeOrgUnit()))
                .add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm(), model.getDevelopForm()))
                .add(MQExpression.eq(STUDENT_ALIAS, Student.status().active(), true));

        if (null != model.getGroupList() && !model.getGroupList().isEmpty())
               builder.add(MQExpression.in(STUDENT_ALIAS, Student.L_GROUP, model.getGroupList()));

        //Не включать в список тех, кто уже имеется в данном приказе
        MQBuilder subBuilder = new MQBuilder(ListStudentExtract.ENTITY_CLASS, "st", new String[]{ListStudentExtract.entity().id().s()})
                .add(MQExpression.eq("st", ListStudentExtract.paragraph().order().id().s(), model.getParagraph().getOrder().getId()))
                .add(MQExpression.notEq("st", ListStudentExtract.paragraph().id().s(), model.getParagraphId()));
        builder.add(MQExpression.notIn(STUDENT_ALIAS, Student.P_ID, subBuilder));

        builder.addOrder(STUDENT_ALIAS, Student.group().s());
        builder.addOrder(STUDENT_ALIAS, Student.person().identityCard().fullFio().s());

    }

    @Override
    protected MireaExcludeStuListExtract createNewInstance(Model model)
    {
        return new MireaExcludeStuListExtract();
    }

    @Override
    protected void fillExtract(MireaExcludeStuListExtract extract, Student student, Model model)
    {
        extract.setExcludeDate(model.getExcludeDate());
        extract.setHolidayFrom(model.getHolidayFrom());
        extract.setHolidayTo(model.getHolidayTo());
        extract.setStatusOld(student.getStatus());
        extract.setOrderText(model.getTextParagraph());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getHolidayFrom() != null)
        {
            if (model.getHolidayTo() == null)
            {
                errors.add("Период каникул должен иметь дату окончания.");
                return;
            }
            if (model.getHolidayFrom().after(model.getHolidayTo()))
                errors.add("Дата начала каникул должна быть раньше даты окончания.");
            if (model.getHolidayTo().after(model.getExcludeDate()))
                errors.add("Дата окончания каникул должна быть раньше даты отчисления.");
        }
        else if (model.getHolidayTo() != null)
            errors.add("Дата начала каникул должна быть заполнена.");
    }
}