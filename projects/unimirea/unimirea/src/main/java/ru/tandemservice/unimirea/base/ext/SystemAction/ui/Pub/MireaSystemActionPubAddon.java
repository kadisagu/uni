/* $Id$ */
package ru.tandemservice.unimirea.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unimirea.base.bo.MireaSettings.MireaSettingsManager;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
public class MireaSystemActionPubAddon extends UIAddon
{
    public MireaSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickGenStudentNumbers()
    {
       MireaSettingsManager.instance().dao().generateNewStudentPersonalNumber();
    }
}