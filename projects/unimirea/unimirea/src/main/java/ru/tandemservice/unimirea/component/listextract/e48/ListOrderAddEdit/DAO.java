/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.e48.ListOrderAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model;

/**
 * @author Ekaterina Zvereva
 * @since 07.02.2017
 */
public class DAO extends ru.tandemservice.movestudent.component.listextract.e48.ListOrderAddEdit.DAO
{
    public static final String TEXT_PARAGRAPH = "В соответствии с Временным положением о текущем контроле успеваемости и промежуточной аттестации по " +
                                                    "образовательным программам высшего образования - программам бакалавриата, программам специалитета" +
                                                    " и программам магистратуры СМКО МИРЭА 7.5.1/03.П.08-16";

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        //Если форма добавления
        if (model.getOrderId() == null)
        {
            model.getOrder().setTextParagraph(TEXT_PARAGRAPH);
            if (null != model.getOrder().getOrgUnit().getHead())
                model.setResponsibleEmployee((EmployeePost) model.getOrder().getOrgUnit().getHead());
        }
    }
}