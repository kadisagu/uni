/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e15.Pub;

import ru.tandemservice.unimirea.entity.MireaEduEnrAsTransferStuExtractExt;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2016
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e15.Pub.Model
{
    private MireaEduEnrAsTransferStuExtractExt _extractExt = new MireaEduEnrAsTransferStuExtractExt();

    public MireaEduEnrAsTransferStuExtractExt getExtractExt()
    {
        return _extractExt;
    }

    public void setExtractExt(MireaEduEnrAsTransferStuExtractExt extractExt)
    {
        _extractExt = extractExt;
    }
}