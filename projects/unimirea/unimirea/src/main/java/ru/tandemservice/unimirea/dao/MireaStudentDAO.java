/* $Id$ */
package ru.tandemservice.unimirea.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.dao.StudentDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimirea.entity.OrgUnitCodeForStudentNumber;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
public class MireaStudentDAO extends StudentDAO
{

    @Override
    public String generateNewPersonalNumber(Student student)
    {
        final String prefix = StringUtils.leftPad(String.valueOf(student.getEntranceYear() % 100), 2, '0');
        OrgUnitCodeForStudentNumber orgUnitCode = get(OrgUnitCodeForStudentNumber.class, OrgUnitCodeForStudentNumber.orgUnit(), student.getEducationOrgUnit().getFormativeOrgUnit());
        if (orgUnitCode == null)
            throw new ApplicationException("Для подразделения " + student.getEducationOrgUnit().getFormativeOrgUnit().getTitle() + " не задан код для генерации личных номеров студентов.");

        final int capacity = 4;
        final int minNumber = 1;
        final int maxNumber =  (int) Math.pow(10, capacity) - 1;
        return getFreePersonalNumber(prefix + StringUtils.capitalize(orgUnitCode.getCode()), minNumber, maxNumber, capacity);
    }


}