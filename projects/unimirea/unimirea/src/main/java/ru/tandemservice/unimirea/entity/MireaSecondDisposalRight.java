package ru.tandemservice.unimirea.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unimirea.entity.gen.*;

/** @see ru.tandemservice.unimirea.entity.gen.MireaSecondDisposalRightGen */
public class MireaSecondDisposalRight extends MireaSecondDisposalRightGen
{

    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    @Override
    @EntityDSLSupport(parts = {P_AREA})
    public Double getFractionalArea()
    {
        Long area = getArea();
        return (area != null) ? area / 100.0 : null;
    }

    @EntityDSLSupport(parts = {P_AREA})
    public void setFractionalArea(Double area)
    {
        setArea((area != null) ? Math.round(area * 100.0d) : null);
    }
}
