/* $Id$ */
package ru.tandemservice.unimirea.component.place.FloorPub;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.component.place.FloorPub.Model;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 20.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.FloorPub.DAO
{
    public static String PLACE_EXT = "extPlace";


    @Override
    public void prepareListDataSource(Model model)
    {
        prepareDocumentDataSource(model);
        DynamicListDataSource dataSource = model.getDataSource();
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesPlace.class, "place")
                .addAdditionalAlias(UniPlacesPlaceExt.class, "extPlace").addAdditionalAlias(UniMireaPlace.class, "mireaPlace");
        orderRegistry.setOrders(UniPlacesPlaceExt.placeMirea().title(), new OrderDescription("mireaPlace", UniMireaPlace.title()), new OrderDescription("place.number"));
        orderRegistry.setOrders(UniPlacesPlaceExt.placeMirea().number(), new OrderDescription("mireaPlace", UniMireaPlace.number()), new OrderDescription("place.number"));


        DQLSelectBuilder builder = orderRegistry.buildDQLSelectBuilder()
                .joinEntity("place", DQLJoinType.left, UniPlacesPlaceExt.class, "extPlace", eq(property("place.id"), property("extPlace", UniPlacesPlaceExt.place().id())))
                .joinPath(DQLJoinType.left, UniPlacesPlaceExt.placeMirea().fromAlias("extPlace"), "mireaPlace")
                .column(property("place"))
                .where(eq(property("place", UniplacesPlace.floor()), value(model.getFloor())));

        orderRegistry.applyOrderWithLeftJoins(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
        List<ViewWrapper<UniplacesPlace>> resultList= ViewWrapper.getPatchedList(dataSource);
        for(ViewWrapper<UniplacesPlace> item : resultList)
        {
            item.setViewProperty(PLACE_EXT, get(UniPlacesPlaceExt.class, UniPlacesPlaceExt.place(), item.getEntity()));
        }
    }
}
