/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.ui.BankCardPersonalData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 27.07.2015
 */
public class MireaReportsBankCardPersonalDataUI extends UIPresenter
{
    private boolean _showDateFrom;
    private boolean _showDateTo;
    private boolean _showCourse;
    private boolean _showEmployeeOrgUnit;
    private boolean _showFormativeOrgUnit;
    private boolean _showCitizenship;
    private boolean _showCompensationType;
    private boolean _showDevelopForm;

    private Date _dateFrom;
    private Date _dateTo;
    private List<Course> _courseList;
    private List<OrgUnit> _employeeOrgUnitList;
    private List<OrgUnit> _formativeOrgUnitList;
    private boolean _countInnerOrgUnit;
    private List<ICitizenship> _citizenshipList;
    private List<CompensationType> _compensationTypeList;
    private List<DevelopForm> _developFormList;

    private byte[] _report = null;

    private String _selectedTab = MireaReportsBankCardPersonalData.PERSON_TAB_PANEL;

    @Override
    public void onComponentRefresh()
    {
        setShowDateFrom(false);
        setShowDateTo(false);
        setShowCourse(false);
        setShowEmployeeOrgUnit(false);
        setShowFormativeOrgUnit(false);
        setShowCitizenship(false);
        setShowCompensationType(false);
        setShowDevelopForm(false);

        setDateFrom(null);
        setDateTo(null);
        setCourseList(null);
        setEmployeeOrgUnitList(null);
        setCountInnerOrgUnit(false);
        setFormativeOrgUnitList(null);
        setCitizenshipList(null);
        setCompensationTypeList(null);
        setDevelopFormList(null);
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (_report != null)
        {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Выгрузка персон для банка.xls").document(_report), false);
            _report = null;
        }
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public boolean isShowDateFrom()
    {
        return _showDateFrom;
    }

    public void setShowDateFrom(boolean showDateFrom)
    {
        _showDateFrom = showDateFrom;
    }

    public boolean isShowDateTo()
    {
        return _showDateTo;
    }

    public void setShowDateTo(boolean showDateTo)
    {
        _showDateTo = showDateTo;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public boolean isShowCourse()
    {
        return _showCourse;
    }

    public void setShowCourse(boolean showCourse)
    {
        _showCourse = showCourse;
    }

    public boolean isShowEmployeeOrgUnit()
    {
        return _showEmployeeOrgUnit;
    }

    public void setShowEmployeeOrgUnit(boolean showEmployeeOrgUnit)
    {
        _showEmployeeOrgUnit = showEmployeeOrgUnit;
    }

    public List<OrgUnit> getEmployeeOrgUnitList()
    {
        return _employeeOrgUnitList;
    }

    public void setEmployeeOrgUnitList(List<OrgUnit> employeeOrgUnitList)
    {
        _employeeOrgUnitList = employeeOrgUnitList;
    }

    public boolean isCountInnerOrgUnit()
    {
        return _countInnerOrgUnit;
    }

    public void setCountInnerOrgUnit(boolean countInnerOrgUnit)
    {
        _countInnerOrgUnit = countInnerOrgUnit;
    }

    public boolean isShowFormativeOrgUnit()
    {
        return _showFormativeOrgUnit;
    }

    public void setShowFormativeOrgUnit(boolean showFormativeOrgUnit)
    {
        _showFormativeOrgUnit = showFormativeOrgUnit;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isShowCitizenship()
    {
        return _showCitizenship;
    }

    public void setShowCitizenship(boolean showCitizenship)
    {
        _showCitizenship = showCitizenship;
    }

    public List<ICitizenship> getCitizenshipList()
    {
        return _citizenshipList;
    }

    public void setCitizenshipList(List<ICitizenship> citizenshipList)
    {
        _citizenshipList = citizenshipList;
    }

    public boolean isShowCompensationType()
    {
        return _showCompensationType;
    }

    public void setShowCompensationType(boolean showCompensationType)
    {
        _showCompensationType = showCompensationType;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public boolean isShowDevelopForm()
    {
        return _showDevelopForm;
    }

    public void setShowDevelopForm(boolean showDevelopForm)
    {
        _showDevelopForm = showDevelopForm;
    }

    //listener
    public void onClickApply()
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        checkDates(errors);
        if (errors.hasErrors()) return;

        ReportBankCardPersonDataExcelBuilder builder = new ReportBankCardPersonDataExcelBuilder(this);
        try
        {
            _report = builder.buildReport().toByteArray();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    //validators
    private void checkDates(ErrorCollector errors) throws ApplicationException
    {
        if (isShowDateFrom() && isShowDateTo() && getDateFrom().after(getDateTo()))
        {
            errors.add("\"Дата назначения на должность с\"  должна быть не позже \"Дата назначения на должность по\" .", "dateFrom", "dateTo");
        }
    }
}
