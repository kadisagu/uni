/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.*;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 24.02.2015
 */
public class ReportDataWrapper
{
    private boolean _hideGround;
    private boolean _hideBuilding;
    private boolean _hideUnit;
    private boolean _hideFloor;
    private boolean _hidePlace;
    private boolean _printParentData;
    private IdentifiableWrapper _availableRent;
    private IdentifiableWrapper _stateRent;
    private List<UniplacesGround> _groundList;
    private List<UniplacesBuilding> _buildingList;
    private List<UniplacesUnit> _unitList;
    private List<UniplacesFloor> _floorList;
    private List<UniMireaPlace> _placeList;
    private List<UniplacesPlace> _roomList;
    private List<UniplacesDisposalRight> _rightGroungList;
    private List<UniplacesDisposalRight> _rightBuildingList;

    public boolean isHideGround()
    {
        return _hideGround;
    }

    public void setHideGround(boolean hideGround)
    {
        _hideGround = hideGround;
    }

    public boolean isHideBuilding()
    {
        return _hideBuilding;
    }

    public void setHideBuilding(boolean hideBuilding)
    {
        _hideBuilding = hideBuilding;
    }

    public boolean isHideUnit()
    {
        return _hideUnit;
    }

    public void setHideUnit(boolean hideUnit)
    {
        _hideUnit = hideUnit;
    }

    public boolean isHideFloor()
    {
        return _hideFloor;
    }

    public boolean isHidePlace()
    {
        return _hidePlace;
    }

    public void setHideFloor(boolean hideFloor)
    {
        _hideFloor = hideFloor;
    }

    public void setHidePlace(boolean hidePlace)
    {
        _hidePlace = hidePlace;
    }

    public boolean isPrintParentData()
    {
        return _printParentData;
    }

    public void setPrintParentData(boolean printParentData)
    {
        _printParentData = printParentData;
    }

    public IdentifiableWrapper getAvailableRent()
    {
        return _availableRent;
    }

    public void setAvailableRent(IdentifiableWrapper availableRent)
    {
        _availableRent = availableRent;
    }

    public IdentifiableWrapper getStateRent()
    {
        return _stateRent;
    }

    public void setStateRent(IdentifiableWrapper stateRent)
    {
        _stateRent = stateRent;
    }

    public List<UniplacesGround> getGroundList()
    {
        return _groundList;
    }

    public void setGroundList(List<UniplacesGround> groundList)
    {
        _groundList = groundList;
    }

    public List<UniplacesBuilding> getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(List<UniplacesBuilding> buildingList)
    {
        _buildingList = buildingList;
    }

    public List<UniplacesUnit> getUnitList()
    {
        return _unitList;
    }

    public void setUnitList(List<UniplacesUnit> unitList)
    {
        _unitList = unitList;
    }

    public List<UniplacesFloor> getFloorList()
    {
        return _floorList;
    }

    public void setFloorList(List<UniplacesFloor> floorList)
    {
        _floorList = floorList;
    }

    public List<UniMireaPlace> getPlaceList()
    {
        return _placeList;
    }

    public void setPlaceList(List<UniMireaPlace> placeList)
    {
        _placeList = placeList;
    }

    public List<UniplacesPlace> getRoomList()
    {
        return _roomList;
    }

    public void setRoomList(List<UniplacesPlace> roomList)
    {
        _roomList = roomList;
    }

    public List<UniplacesDisposalRight> getRightGroungList()
    {
        return _rightGroungList;
    }

    public void setRightGroungList(List<UniplacesDisposalRight> rightGroungList)
    {
        _rightGroungList = rightGroungList;
    }

    public List<UniplacesDisposalRight> getRightBuildingList()
    {
        return _rightBuildingList;
    }

    public void setRightBuildingList(List<UniplacesDisposalRight> rightBuildingList)
    {
        _rightBuildingList = rightBuildingList;
    }

    public void clear()
    {
        getGroundList().clear();
        getBuildingList().clear();
        getUnitList().clear();
        getFloorList().clear();
        getPlaceList().clear();
        getRoomList().clear();
        setHideGround(false);
        setHideBuilding(false);
        setHideUnit(false);
        setHidePlace(false);
        setHideFloor(false);
        setAvailableRent(null);
        setStateRent(null);
        setPrintParentData(false);
        getRightGroungList().clear();
        getRightBuildingList().clear();
    }
}