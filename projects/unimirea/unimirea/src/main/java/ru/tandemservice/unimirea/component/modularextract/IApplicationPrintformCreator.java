/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 15.01.2016
 */
public interface IApplicationPrintformCreator
{
    RtfDocument createApplicationPrintForm(byte[] template, ModularStudentExtract extract, int number);
}