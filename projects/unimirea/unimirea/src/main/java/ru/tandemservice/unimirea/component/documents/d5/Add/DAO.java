package ru.tandemservice.unimirea.component.documents.d5.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by hgfhg on 21.07.2016.
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getCalculatedFIODeclination(model.getStudent().getPerson().getIdentityCard(), InflectorVariantCodes.RU_DATIVE));

        model.setAttestationList(Arrays.asList(
                new IdentifiableWrapper(0L, "промежуточной аттестации"),
                new IdentifiableWrapper(1L, "государственной итоговой аттестации"),
        new IdentifiableWrapper(2L, "итоговой аттестации"),
        new IdentifiableWrapper(3L, "подготовки и защиты ВПК и сдачи ИГЭ"),
        new IdentifiableWrapper(4L, "сдачи итоговых госэкзаменов (ИГЭ)")
         ));
        model.setAttestation(model.getAttestationList().get(0));
    }


}
