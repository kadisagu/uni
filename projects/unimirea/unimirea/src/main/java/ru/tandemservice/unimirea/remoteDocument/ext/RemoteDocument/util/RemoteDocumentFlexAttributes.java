/* $Id$ */
package ru.tandemservice.unimirea.remoteDocument.ext.RemoteDocument.util;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2015
 */
public interface RemoteDocumentFlexAttributes
{
    public static final String MIREA_PLACES_DOCUMENT_OWNER_OBJECT = "mireaPlacesDocumentOwnerObject";
    public static final String MIREA_PLACES_DOCUMENT_TITLE = "mireaPlacesDocumentTitle";
    public static final String MIREA_PLACES_DOCUMENT_START_DATE = "mireaPlacesDocumentStartDate";
    public static final String MIREA_PLACES_DOCUMENT_END_DATE = "mireaPlacesDocumentEndDate";
    public static final String MIREA_PLACES_DOCUMENT_COMMENT = "mireaPlacesDocumentComment";
    public static final String MIREA_PLACES_DOCUMENT_ACTIVE = "mireaPlacesDocumentActive";


}