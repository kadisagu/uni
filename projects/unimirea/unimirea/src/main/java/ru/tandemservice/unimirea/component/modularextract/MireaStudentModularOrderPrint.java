/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.RestorationStuExtractExt;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 15.01.2016
 */
public class MireaStudentModularOrderPrint extends ru.tandemservice.movestudent.component.modularextract.StudentModularOrderPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentModularOrder order)
    {
        RtfDocument orderDocument = super.createPrintForm(template, order);
        final List<ModularStudentExtract> extractList = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, false);

        int extractNumberInText = 1;
        List<IRtfElement> parList = new ArrayList<>();
        for (ModularStudentExtract extract : extractList)
        {
            if (extract instanceof RestorationStuExtractExt || extract instanceof EduEnrAsTransferStuExtract)
            {
                // создаем приложение
                byte[] applicationTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(extract.getType(), MireaMoveStudentDefines.INDIVIDUAL_ORDER_APPLICATION_CODE);

                String printName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
                IPrintFormCreator<ModularStudentExtract> componentPrint = CommonExtractPrint.getPrintFormCreator(printName);
                if (componentPrint instanceof IApplicationPrintformCreator)
                {

                    RtfDocument applicationDocument = ((IApplicationPrintformCreator) componentPrint).createApplicationPrintForm(applicationTemplate, extract, extractNumberInText);
                    if (applicationDocument != null)
                    {
                        extractNumberInText++;
                        // начинаем с новой страницы
                        parList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                        // подготавливаем ее для вставки в приказ
                        RtfUtil.modifySourceList(orderDocument.getHeader(), applicationDocument.getHeader(), applicationDocument.getElementList());

                        // дальше добавляем приложение
                        parList.addAll(applicationDocument.getElementList());
                    }
                }
            }

            // полученный список вставляем в конец
            if (!parList.isEmpty())
                orderDocument.getElementList().addAll(parList);
        }
        return orderDocument;

    }
}