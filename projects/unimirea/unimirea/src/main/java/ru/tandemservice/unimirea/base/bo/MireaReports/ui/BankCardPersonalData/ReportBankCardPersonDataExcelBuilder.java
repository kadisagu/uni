/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.ui.BankCardPersonalData;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Orientation;
import jxl.format.PageOrientation;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.base.entity.codes.AddressLevelCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimirea.base.bo.MireaReports.MireaReportsManager;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 28.07.2015
 */
public class ReportBankCardPersonDataExcelBuilder extends UniBaseDao
{
    MireaReportsBankCardPersonalDataUI _model;

    ReportBankCardPersonDataExcelBuilder(MireaReportsBankCardPersonalDataUI model)
    {
        _model = model;
    }

    public ByteArrayOutputStream buildReport() throws Exception
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        final WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        //стили и шрифты
        WritableFont times11bold = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        WritableFont times11 = new WritableFont(WritableFont.TIMES, 11);

        WritableCellFormat headerFormatBold = new WritableCellFormat(times11bold);
        headerFormatBold.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormatBold.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormatBold.setWrap(true);

        WritableCellFormat headerFormat = new WritableCellFormat(times11);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM, jxl.format.Colour.BLACK);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        WritableCellFormat headerVerticalFormat = new WritableCellFormat(times11);
        headerVerticalFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerVerticalFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM, jxl.format.Colour.BLACK);
        headerVerticalFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerVerticalFormat.setOrientation(Orientation.PLUS_90);
        headerVerticalFormat.setWrap(true);

        WritableCellFormat headerVerticalTopFormat = new WritableCellFormat(times11);
        headerVerticalTopFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerVerticalTopFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM, jxl.format.Colour.BLACK);
        headerVerticalTopFormat.setVerticalAlignment(VerticalAlignment.TOP);
        headerVerticalTopFormat.setOrientation(Orientation.PLUS_90);
        headerVerticalTopFormat.setWrap(true);


        prepareHeader(workbook, headerFormatBold, headerFormat, headerVerticalFormat, headerVerticalTopFormat);

        if (_model.getSelectedTab().equals(MireaReportsBankCardPersonalData.STUDENT_TAB))
        {
            fillStudentData(workbook.getSheet(0), _model, headerVerticalFormat);
        }
        else
        {
            fillEmployeePostData(workbook.getSheet(0), _model, headerVerticalFormat);
        }

        workbook.write();
        workbook.close();

        return out;
    }

    private void prepareHeader(WritableWorkbook workbook, WritableCellFormat headerFormatBold, WritableCellFormat headerFormat,
                               WritableCellFormat headerVerticalFormat, WritableCellFormat headerVerticalTopFormat) throws WriteException
    {


        WritableSheet sheet = workbook.createSheet("Выгрузка персон для банка", workbook.getSheets().length);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);

        setHeightAndWidth(sheet);

        sheet.mergeCells(3, 1, 25, 1);
        sheet.addCell(new Label(3, 1, "Список на выпуск банковских карт АКИБ «ОБРАЗОВАНИЕ» (ЗАО)", headerFormatBold));
        sheet.mergeCells(3, 2, 25, 2);
        sheet.addCell(new Label(3, 2, "для сотрудников/учащихся Организации  (в электронном виде формата MS Excel)", headerFormatBold));


        sheet.mergeCells(0, 4, 0, 8);
        sheet.addCell(new Label(0, 4, "№ п/п", headerVerticalFormat));

        sheet.mergeCells(1, 4, 1, 8);
        sheet.addCell(new Label(1, 4, "Код организации", headerVerticalFormat));

        sheet.mergeCells(2, 4, 9, 5);
        sheet.addCell(new Label(2, 4, "Сотрудник/учащийся", headerFormat));

        sheet.mergeCells(2, 6, 2, 8);
        sheet.addCell(new Label(2, 6, "Табельный номер", headerVerticalFormat));

        sheet.mergeCells(3, 6, 3, 8);
        sheet.addCell(new Label(3, 6, "Пол  М /Ж", headerVerticalFormat));

        sheet.mergeCells(4, 6, 6, 7);
        sheet.addCell(new Label(4, 6, "Кириллица", headerVerticalFormat));
        sheet.addCell(new Label(4, 8, "Фамилия", headerVerticalTopFormat));
        sheet.addCell(new Label(5, 8, "Имя", headerVerticalTopFormat));
        sheet.addCell(new Label(6, 8, "Отчество", headerVerticalTopFormat));

        sheet.mergeCells(7, 6, 7, 8);
        sheet.addCell(new Label(7, 6, "Резидентность (резидент-R; нерезидент-N)", headerVerticalFormat));

        sheet.mergeCells(8, 6, 9, 7);
        sheet.addCell(new Label(8, 6, "Латиница", headerVerticalFormat));
        sheet.addCell(new Label(8, 8, "Имя", headerVerticalTopFormat));
        sheet.addCell(new Label(9, 8, "Фамилия", headerVerticalTopFormat));

        sheet.mergeCells(10, 4, 10, 8);
        sheet.addCell(new Label(10, 4, "Эмбоссированная компания (латиница)", headerVerticalFormat));

        sheet.mergeCells(11, 4, 11, 8);
        sheet.addCell(new Label(11, 4, "Дата рождения (ЧЧ/ММ/ГГГГ)", headerVerticalFormat));

        sheet.mergeCells(12, 4, 12, 8);
        sheet.addCell(new Label(12, 4, "Страна рождения", headerVerticalFormat));

        sheet.mergeCells(13, 4, 13, 8);
        sheet.addCell(new Label(13, 4, "Место рождения", headerVerticalFormat));

        sheet.mergeCells(14, 4, 14, 8);
        sheet.addCell(new Label(14, 4, "Гражданство", headerVerticalFormat));

        sheet.mergeCells(15, 4, 15, 8);
        sheet.addCell(new Label(15, 4, "ИНН", headerVerticalFormat));

        sheet.mergeCells(16, 4, 26, 6);
        sheet.addCell(new Label(16, 4, "Адрес регистрации по месту регистрации в стандарте КЛАДР.\n" +
                "Если \"Страна\" адреса регистрации не Россия, то заполняется без жёсткого стандарта (без КЛАДР)", headerFormat));

        sheet.mergeCells(16, 7, 16, 8);
        sheet.addCell(new Label(16, 7, "Страна", headerVerticalFormat));

        sheet.mergeCells(17, 7, 17, 8);
        sheet.addCell(new Label(17, 7, "Регион", headerVerticalFormat));

        sheet.mergeCells(18, 7, 18, 8);
        sheet.addCell(new Label(18, 7, "Район", headerVerticalFormat));

        sheet.mergeCells(19, 7, 19, 8);
        sheet.addCell(new Label(19, 7, "Город", headerVerticalFormat));

        sheet.mergeCells(20, 7, 20, 8);
        sheet.addCell(new Label(20, 7, "Населенный пункт (село; поселок)", headerVerticalFormat));

        sheet.mergeCells(21, 7, 21, 8);
        sheet.addCell(new Label(21, 7, "Индекс", headerVerticalFormat));

        sheet.mergeCells(22, 7, 22, 8);
        sheet.addCell(new Label(22, 7, "Улица", headerVerticalFormat));

        sheet.mergeCells(23, 7, 23, 8);
        sheet.addCell(new Label(23, 7, "Дом", headerVerticalFormat));

        sheet.mergeCells(24, 7, 24, 8);
        sheet.addCell(new Label(24, 7, "Корпус", headerVerticalFormat));

        sheet.mergeCells(25, 7, 25, 8);
        sheet.addCell(new Label(25, 7, "Строение", headerVerticalFormat));

        sheet.mergeCells(26, 7, 26, 8);
        sheet.addCell(new Label(26, 7, "Квартира", headerVerticalFormat));

        sheet.mergeCells(27, 4, 37, 6);
        sheet.addCell(new Label(27, 4, "Адрес регистрации по месту жительства в стандарте КЛАДР", headerFormat));

        sheet.mergeCells(27, 7, 27, 8);
        sheet.addCell(new Label(27, 7, "Страна", headerVerticalFormat));

        sheet.mergeCells(28, 7, 28, 8);
        sheet.addCell(new Label(28, 7, "Регион", headerVerticalFormat));

        sheet.mergeCells(29, 7, 29, 8);
        sheet.addCell(new Label(29, 7, "Район", headerVerticalFormat));

        sheet.mergeCells(30, 7, 30, 8);
        sheet.addCell(new Label(30, 7, "Город", headerVerticalFormat));

        sheet.mergeCells(31, 7, 31, 8);
        sheet.addCell(new Label(31, 7, "Населенный пункт (село; поселок)", headerVerticalFormat));

        sheet.mergeCells(32, 7, 32, 8);
        sheet.addCell(new Label(32, 7, "Индекс", headerVerticalFormat));

        sheet.mergeCells(33, 7, 33, 8);
        sheet.addCell(new Label(33, 7, "Улица", headerVerticalFormat));

        sheet.mergeCells(34, 7, 34, 8);
        sheet.addCell(new Label(34, 7, "Дом", headerVerticalFormat));

        sheet.mergeCells(35, 7, 35, 8);
        sheet.addCell(new Label(35, 7, "Корпус", headerVerticalFormat));

        sheet.mergeCells(36, 7, 36, 8);
        sheet.addCell(new Label(36, 7, "Строение", headerVerticalFormat));

        sheet.mergeCells(37, 7, 37, 8);
        sheet.addCell(new Label(37, 7, "Квартира", headerVerticalFormat));

        sheet.mergeCells(38, 4, 39, 6);
        sheet.addCell(new Label(38, 4, "Телефон", headerFormat));

        sheet.mergeCells(38, 7, 38, 8);
        sheet.addCell(new Label(38, 7, "Домашний", headerVerticalFormat));

        sheet.mergeCells(39, 7, 39, 8);
        sheet.addCell(new Label(39, 7, "Мобильный", headerVerticalFormat));

        sheet.mergeCells(40, 4, 45, 6);
        sheet.addCell(new Label(40, 4, "Документ, удостоверяющий личность", headerFormat));

        sheet.mergeCells(40, 7, 40, 8);
        sheet.addCell(new Label(40, 7, "Вид документа (паспорт гражданина РФ- внести значение 21; паспорт иностранного гражданина- внести значение 31)", headerVerticalFormat));

        sheet.mergeCells(41, 7, 41, 8);
        sheet.addCell(new Label(41, 7, "Серия", headerVerticalFormat));

        sheet.mergeCells(42, 7, 42, 8);
        sheet.addCell(new Label(42, 7, "номер", headerVerticalFormat));

        sheet.mergeCells(43, 7, 43, 8);
        sheet.addCell(new Label(43, 7, "код подразделения", headerVerticalFormat));

        sheet.mergeCells(44, 7, 44, 8);
        sheet.addCell(new Label(44, 7, "Кем выдан", headerVerticalFormat));

        sheet.mergeCells(45, 7, 45, 8);
        sheet.addCell(new Label(45, 7, "Когда выдан (ЧЧ/ММ/ГГГГ)", headerVerticalFormat));

        sheet.mergeCells(46, 4, 48, 5);
        sheet.addCell(new Label(46, 4, "Служебные данные", headerFormat));

        sheet.mergeCells(46, 6, 46, 8);
        sheet.addCell(new Label(46, 6, "Личный e-mail", headerVerticalFormat));

        sheet.mergeCells(47, 6, 47, 8);
        sheet.addCell(new Label(47, 6, "Служебный телефон", headerVerticalFormat));

        sheet.mergeCells(48, 6, 48, 8);
        sheet.addCell(new Label(48, 6, "Должность", headerVerticalFormat));

    }

    private static void setHeightAndWidth(WritableSheet sheet) throws RowsExceededException
    {
        for (int i = 0; i < 40; i++)
        {
            // Задаем ширину колонок
            sheet.setColumnView(i, 5);
        }
        sheet.setColumnView(40, 15);

        for (int i = 41; i < 44; i++)
        {
            // Задаем ширину колонок
            sheet.setColumnView(i, 5);
        }

        sheet.setColumnView(44, 15);

        for (int i = 45; i < 49; i++)
        {
            // Задаем ширину колонок
            sheet.setColumnView(i, 5);
        }

        sheet.setRowView(4, 400);
        sheet.setRowView(5, 400);
        sheet.setRowView(6, 700);
        sheet.setRowView(7, 1200);
        sheet.setRowView(8, 2300);
        sheet.setRowView(8, 2400);
    }

    private static void fillStudentData(WritableSheet sheet, MireaReportsBankCardPersonalDataUI model, WritableCellFormat headerVerticalFormat)
    {

        List<Object[]> personList = MireaReportsManager.instance().dao().getPersonStudentList(model);
        fillPersonData(sheet, headerVerticalFormat, personList, true, null);

    }

    private static void fillEmployeePostData(WritableSheet sheet, MireaReportsBankCardPersonalDataUI model, WritableCellFormat headerVerticalFormat)
    {
        List<Object[]> personList = MireaReportsManager.instance().dao().getPersonEmployeeList(model); //person, identityCard, AddressReg
        Map<Person, List<EmployeePost>> personPosts = SafeMap.get(ArrayList.class);
        for (Object[] item : personList)
            personPosts.get((Person) item[0]).add((EmployeePost) item[5]);
        fillPersonData(sheet, headerVerticalFormat, personList, false, personPosts);


    }

    private static void fillPersonData(WritableSheet sheet, WritableCellFormat headerVerticalFormat, List<Object[]> personList, boolean isStudent, Map<Person, List<EmployeePost>> personPosts)
    {
        int rowIdx = 9;
        int rowNumber = 1;
        Set<Person> printedPersons = new HashSet<>(personList.size());
        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance();
        for (Object[] item : personList)
        {
            try
            {
                Person person = (Person) item[0];
                if (printedPersons.contains(person))
                {
                    continue;
                }
                else
                {
                    printedPersons.add(person);
                }

                IdentityCard card = (IdentityCard) item[1];
                String birthDate = card.getBirthDate() != null ? DateFormatUtils.format(card.getBirthDate(), "dd/MM/yyyy") : "";
                String birthCountry = (person.getBirthCountry() != null && person.getBirthCountry().getTitle() != null) ? person.getBirthCountry().getTitle() : "";
                AddressBase addressRegistration = (AddressBase) item[2];
                AddressDetailed addressRegDetailed = addressRegistration instanceof AddressDetailed ? (AddressDetailed) addressRegistration : null;
                AddressRu addressRegRu = addressRegDetailed instanceof AddressRu ? (AddressRu) addressRegDetailed : null;

                AddressBase addressFact = (AddressBase) item[3];
                AddressRu addressFactRu = addressRegDetailed instanceof AddressRu ? (AddressRu) addressRegDetailed : null;

                String resident = card.getCitizenship().getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE ? "R" : "N";
                String identityCardIssuanceDate = card.getIssuanceDate() != null ? DateFormatUtils.format(card.getIssuanceDate(), "dd/MM/yyyy") : "";

                sheet.setRowView(rowIdx, 2400);
                sheet.addCell(new Number(0, rowIdx, rowNumber++, headerVerticalFormat));
                sheet.addCell(new Label(1, rowIdx, StringUtils.trimToEmpty(topOrgUnit.getRegistryNumber()), headerVerticalFormat));
                sheet.addCell(new Label(2, rowIdx, getPersonalNumber(item[4], isStudent), headerVerticalFormat));
                sheet.addCell(new Label(3, rowIdx, StringUtils.trimToEmpty(card.getSex().getShortTitle()), headerVerticalFormat));
                sheet.addCell(new Label(4, rowIdx, card.getLastName(), headerVerticalFormat));
                sheet.addCell(new Label(5, rowIdx, card.getFirstName(), headerVerticalFormat));
                sheet.addCell(new Label(6, rowIdx, StringUtils.trimToEmpty(card.getMiddleName()), headerVerticalFormat));
                sheet.addCell(new Label(7, rowIdx, resident, headerVerticalFormat));

                sheet.addCell(new Label(8, rowIdx, getAmbossedFirstName(card).replace("'","").toUpperCase(), headerVerticalFormat));
                sheet.addCell(new Label(9, rowIdx, CoreStringUtils.transliterate(card.getLastName()).replace("'","").toUpperCase(), headerVerticalFormat));

                sheet.addCell(new Label(10, rowIdx, "MIREA", headerVerticalFormat));
                sheet.addCell(new Label(11, rowIdx, birthDate, headerVerticalFormat));
                sheet.addCell(new Label(12, rowIdx, birthCountry, headerVerticalFormat));
                sheet.addCell(new Label(13, rowIdx, StringUtils.trimToEmpty(card.getBirthPlace()), headerVerticalFormat));
                sheet.addCell(new Label(14, rowIdx, StringUtils.trimToEmpty(card.getCitizenship().getTitle()), headerVerticalFormat));
                sheet.addCell(new Label(15, rowIdx, StringUtils.trimToEmpty(person.getInnNumber()), headerVerticalFormat));

                sheet.addCell(new Label(16, rowIdx, StringUtils.trimToEmpty(getCountryTitle(addressRegistration)), headerVerticalFormat));
                sheet.addCell(new Label(17, rowIdx, StringUtils.trimToEmpty(getRegionTitle(addressRegistration)), headerVerticalFormat));
                sheet.addCell(new Label(18, rowIdx, StringUtils.trimToEmpty(getAreaTitle(addressRegistration)), headerVerticalFormat));
                sheet.addCell(new Label(19, rowIdx, StringUtils.trimToEmpty(getCityTitle(addressRegistration)), headerVerticalFormat));
                sheet.addCell(new Label(20, rowIdx, StringUtils.trimToEmpty(getLocalityTitle(addressRegistration)), headerVerticalFormat));
                sheet.addCell(new Label(21, rowIdx, StringUtils.trimToEmpty(getPostIndex(addressRegistration)), headerVerticalFormat));
                sheet.addCell(new Label(22, rowIdx, StringUtils.trimToEmpty(getStreetTitle(addressRegRu)), headerVerticalFormat));
                sheet.addCell(new Label(23, rowIdx, StringUtils.trimToEmpty(getHouseNumber(addressRegRu)), headerVerticalFormat));
                sheet.addCell(new Label(24, rowIdx, StringUtils.trimToEmpty(getHouseUnitNumber(addressRegRu)), headerVerticalFormat));
                sheet.addCell(new Label(25, rowIdx, StringUtils.trimToEmpty(getHouseUnitNumber(addressRegRu)), headerVerticalFormat));
                sheet.addCell(new Label(26, rowIdx, StringUtils.trimToEmpty(getFlatNumber(addressRegRu)), headerVerticalFormat));

                sheet.addCell(new Label(27, rowIdx, StringUtils.trimToEmpty(getCountryTitle(addressFact)), headerVerticalFormat));
                sheet.addCell(new Label(28, rowIdx, StringUtils.trimToEmpty(getRegionTitle(addressFact)), headerVerticalFormat));
                sheet.addCell(new Label(29, rowIdx, StringUtils.trimToEmpty(getAreaTitle(addressFact)), headerVerticalFormat));
                sheet.addCell(new Label(30, rowIdx, StringUtils.trimToEmpty(getCityTitle(addressFact)), headerVerticalFormat));
                sheet.addCell(new Label(31, rowIdx, StringUtils.trimToEmpty(getLocalityTitle(addressFact)), headerVerticalFormat));
                sheet.addCell(new Label(32, rowIdx, StringUtils.trimToEmpty(getPostIndex(addressFact)), headerVerticalFormat));
                sheet.addCell(new Label(33, rowIdx, StringUtils.trimToEmpty(getStreetTitle(addressFactRu)), headerVerticalFormat));
                sheet.addCell(new Label(34, rowIdx, StringUtils.trimToEmpty(getHouseNumber(addressFactRu)), headerVerticalFormat));
                sheet.addCell(new Label(35, rowIdx, StringUtils.trimToEmpty(getHouseUnitNumber(addressFactRu)), headerVerticalFormat));
                sheet.addCell(new Label(36, rowIdx, StringUtils.trimToEmpty(getHouseUnitNumber(addressFactRu)), headerVerticalFormat));
                sheet.addCell(new Label(37, rowIdx, StringUtils.trimToEmpty(getFlatNumber(addressFactRu)), headerVerticalFormat));

                sheet.addCell(new Label(38, rowIdx, StringUtils.trimToEmpty(person.getContactData().getPhoneFact()), headerVerticalFormat));
                sheet.addCell(new Label(39, rowIdx, getMobilePhoneNumber(StringUtils.trimToEmpty(person.getContactData().getPhoneMobile())), headerVerticalFormat));

                sheet.addCell(new Label(40, rowIdx, getIdentityCardTypeCode(card), headerVerticalFormat));
                sheet.addCell(new Label(41, rowIdx, getIdentityCardSeria(card), headerVerticalFormat));
                sheet.addCell(new Label(42, rowIdx, StringUtils.trimToEmpty(card.getNumber()), headerVerticalFormat));
                sheet.addCell(new Label(43, rowIdx, StringUtils.trimToEmpty(card.getIssuanceCode()), headerVerticalFormat));
                sheet.addCell(new Label(44, rowIdx, StringUtils.trimToEmpty(card.getIssuancePlace()), headerVerticalFormat));
                sheet.addCell(new Label(45, rowIdx, identityCardIssuanceDate, headerVerticalFormat));
                sheet.addCell(new Label(46, rowIdx, StringUtils.trimToEmpty(person.getContactData().getEmail()), headerVerticalFormat));
                if (isStudent)
                {
                    sheet.addCell(new Label(47, rowIdx, "", headerVerticalFormat));
                    sheet.addCell(new Label(48, rowIdx, "студент", headerVerticalFormat));
                }
                else
                {
                    List<EmployeePost> employeePostList = personPosts.get(person);
                    if (employeePostList.isEmpty())
                    {
                        sheet.addCell(new Label(47, rowIdx, "", headerVerticalFormat));
                        sheet.addCell(new Label(48, rowIdx, "", headerVerticalFormat));
                    }
                    else
                    {
                        String phones = CommonBaseStringUtil.join(employeePostList, EmployeePost.phone(), ", ");
                        String postes = CommonBaseStringUtil.join(employeePostList, EmployeePost.postRelation().postBoundedWithQGandQL().title(), ", ");
                        sheet.addCell(new Label(47, rowIdx, StringUtils.trimToEmpty(phones), headerVerticalFormat));
                        sheet.addCell(new Label(48, rowIdx, StringUtils.trimToEmpty(postes), headerVerticalFormat));
                    }
                }

                rowIdx++;
            }
            catch (WriteException e)
            {
                throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
            }
        }
    }

    //Utils
    public static String getRegionTitle(AddressBase addressBase)
    {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        String result = "";

        if (address != null && address.getSettlement() != null)
        {
            AddressItem item = address.getSettlement();
            while (item != null && AddressLevelCodes.REGION != item.getAddressType().getAddressLevel().getCode())
                item = item.getParent();

            result = item != null ?
                    item.getTitle() + " " + item.getAddressType().getShortTitle().replace(".", "")
                    : "";
        }
        return result;
    }

    public static String getCountryTitle(AddressBase addressBase)
    {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        String result = "";
        if (address != null)
        {
            result = address.getCountry().getTitle();
        }
        return result;
    }

    public static String getPostIndex(AddressBase addressBase)
    {
        AddressInter addressInter = addressBase instanceof AddressInter ? (AddressInter) addressBase : null;
        AddressRu addressRu = addressBase instanceof AddressRu ? (AddressRu) addressBase : null;
        String result = "";
        if (addressRu != null && addressRu.getInheritedPostCode() != null)
        {
            result = addressRu.getInheritedPostCode();
        }
        else if (addressInter != null && addressInter.getPostCode() != null)
        {
            result = addressInter.getPostCode();
        }
        return result;
    }

    public static String getAreaTitle(AddressBase addressBase)
    {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        if (address == null || address.getSettlement() == null)
            return null;

        AddressItem item = address.getSettlement();
        while (item != null && AddressLevelCodes.AREA != item.getAddressType().getAddressLevel().getCode())
            item = item.getParent();

        return item != null ?
                item.getTitle() + " " + item.getAddressType().getShortTitle()
                : null;
    }

    public static String getCityTitle(AddressBase addressBase)
    {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        List<Integer> cityList = Arrays.asList(103, 301);

        if (address == null || address.getSettlement() == null)
            return null;

        AddressItem item = address.getSettlement();
        while (item != null && !cityList.contains(item.getAddressType().getCode()))
            item = item.getParent();

        //ситуация город в городе
        if (item != null && item.getParent() != null && cityList.contains(item.getParent().getAddressType().getCode()))
            item = item.getParent();

        String result = item != null && cityList.contains(item.getAddressType().getCode()) ?
                item.getTitle() + " " + item.getAddressType().getShortTitle().replace(".", "")
                : null;
        String region = "";
        if (result != null)
        {
            region = StringUtils.trimToEmpty(getRegionTitle(addressBase));
        }
        return region.equals(result) ? null : result;
    }

    public static String getLocalityTitle(AddressBase addressBase)
    {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        List<Integer> cityList = Arrays.asList(103, 301);

        if (address == null || address.getSettlement() == null || address.getSettlement().getParent() == null)
            return null;
        AddressItem item = address.getSettlement();

        if (cityList.contains(item.getAddressType().getCode()))
        {
            if (item.getParent() != null && cityList.contains(item.getParent().getAddressType().getCode()))
            {
                //do nothing
            }
            else
                item = null;
        }

        return item != null && AddressLevelCodes.SETTLEMENT == item.getAddressType().getAddressLevel().getCode() ?
                item.getTitle() + " " + item.getAddressType().getShortTitle().replace(".", "")
                : null;
    }

    public static String getStreetTitle(AddressRu addressRu)
    {
        if (addressRu != null && addressRu.getStreet() != null)
        {
            return addressRu.getStreet().getTitle() + " " + addressRu.getStreet().getAddressType().getShortTitle().replace(".", "");
        }
        return "";
    }

    public static String getHouseNumber(AddressRu addressRu)
    {
        if (addressRu != null && addressRu.getStreet() != null)
        {
            return addressRu.getHouseNumber();
        }
        return "";
    }

    public static String getHouseUnitNumber(AddressRu addressRu)
    {
        if (addressRu != null && addressRu.getStreet() != null)
        {
            return addressRu.getHouseUnitNumber();
        }
        return "";
    }

    public static String getFlatNumber(AddressRu addressRu)
    {
        if (addressRu != null && addressRu.getStreet() != null)
        {
            return addressRu.getFlatNumber();
        }
        return "";
    }

    public static String getIdentityCardTypeCode(IdentityCard card)
    {
        if (card.getCardType().getCode().equals(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII))
        {
            return "21";
        }
        else if (card.getCardType().getCode().equals(IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA))
        {
            return "31";
        }
        return "";

    }


    public static String getIdentityCardSeria(IdentityCard card)
    {
        String seria = StringUtils.trimToEmpty(card.getSeria());
        if (card.getCardType().getCode().equals(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII) && seria.length() == 4)
        {
            seria = card.getSeria().substring(0, 2) + " " + card.getSeria().substring(2, 4);
        }
        return seria;
    }

    public static String getAmbossedFirstName(IdentityCard card)
    {
        String fistName = CoreStringUtils.transliterate(card.getFirstName());
        String lastAndFirstName = fistName + " " + CoreStringUtils.transliterate(card.getLastName());
        if (lastAndFirstName.length() > 20)
        {
            fistName = fistName.substring(0, 1) + ". ";
        }
        return fistName;
    }

    public static String getMobilePhoneNumber(String phoneNumber)
    {
        return phoneNumber.replace(" ", "").replace("+", "").replace("-", "").replace("(", "").replace(")", "");
    }

    public static String getPersonalNumber(Object emloyeeOrStudent, boolean isStudent)
    {

        if (isStudent)
        {
            return ((Student)emloyeeOrStudent).getPersonalNumber();
        }
        return ((Employee)emloyeeOrStudent).getEmployeeCode();
    }
}
