package ru.tandemservice.unimirea.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.OrganizationType;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аренда помещений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MireaPlacesRentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimirea.entity.MireaPlacesRent";
    public static final String ENTITY_NAME = "mireaPlacesRent";
    public static final int VERSION_HASH = 1656622434;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_PLACE = "place";
    public static final String L_CONTRACTOR = "contractor";
    public static final String L_CONTRACTOR_TYPE = "contractorType";
    public static final String L_CONTACTOR_PERSON = "contactorPerson";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";

    
    private UniplacesPlace _place;     // Комната - объект аренды
    private ExternalOrgUnit _contractor;     // Контрагент
    private OrganizationType _contractorType;     // Тип контрагента
    private ContactorPerson _contactorPerson;     // Контактное лицо
    private Date _startDate;     // Дата начала аренды
    private Date _endDate;     // Дата окончания аренды

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     *
     * Это формула.
     * @see {@link ru.tandemservice.unimirea.entity.MireaPlacesRent#getTitle}.
     */
    // @NotNull
    // @Length(max=767)
    public String getTitle()
    {
        String title = getContractor().getTitle();
        if (getStartDate() != null || getEndDate() != null)
            title = title + " (" + new org.tandemframework.core.view.formatter.DateFormatter().format(getStartDate()) + ", " + new org.tandemframework.core.view.formatter.DateFormatter().format(getEndDate()) + ")";
        return title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setTitle(String title)
    {
        // nothing
    }

    /**
     * @return Комната - объект аренды. Свойство не может быть null.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Комната - объект аренды. Свойство не может быть null.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Контрагент. Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getContractor()
    {
        return _contractor;
    }

    /**
     * @param contractor Контрагент. Свойство не может быть null.
     */
    public void setContractor(ExternalOrgUnit contractor)
    {
        dirty(_contractor, contractor);
        _contractor = contractor;
    }

    /**
     * @return Тип контрагента.
     */
    public OrganizationType getContractorType()
    {
        return _contractorType;
    }

    /**
     * @param contractorType Тип контрагента.
     */
    public void setContractorType(OrganizationType contractorType)
    {
        dirty(_contractorType, contractorType);
        _contractorType = contractorType;
    }

    /**
     * @return Контактное лицо.
     */
    public ContactorPerson getContactorPerson()
    {
        return _contactorPerson;
    }

    /**
     * @param contactorPerson Контактное лицо.
     */
    public void setContactorPerson(ContactorPerson contactorPerson)
    {
        dirty(_contactorPerson, contactorPerson);
        _contactorPerson = contactorPerson;
    }

    /**
     * @return Дата начала аренды.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала аренды.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания аренды.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания аренды.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MireaPlacesRentGen)
        {
            setTitle(((MireaPlacesRent)another).getTitle());
            setPlace(((MireaPlacesRent)another).getPlace());
            setContractor(((MireaPlacesRent)another).getContractor());
            setContractorType(((MireaPlacesRent)another).getContractorType());
            setContactorPerson(((MireaPlacesRent)another).getContactorPerson());
            setStartDate(((MireaPlacesRent)another).getStartDate());
            setEndDate(((MireaPlacesRent)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MireaPlacesRentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MireaPlacesRent.class;
        }

        public T newInstance()
        {
            return (T) new MireaPlacesRent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "place":
                    return obj.getPlace();
                case "contractor":
                    return obj.getContractor();
                case "contractorType":
                    return obj.getContractorType();
                case "contactorPerson":
                    return obj.getContactorPerson();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "contractor":
                    obj.setContractor((ExternalOrgUnit) value);
                    return;
                case "contractorType":
                    obj.setContractorType((OrganizationType) value);
                    return;
                case "contactorPerson":
                    obj.setContactorPerson((ContactorPerson) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "place":
                        return true;
                case "contractor":
                        return true;
                case "contractorType":
                        return true;
                case "contactorPerson":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "place":
                    return true;
                case "contractor":
                    return true;
                case "contractorType":
                    return true;
                case "contactorPerson":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "place":
                    return UniplacesPlace.class;
                case "contractor":
                    return ExternalOrgUnit.class;
                case "contractorType":
                    return OrganizationType.class;
                case "contactorPerson":
                    return ContactorPerson.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MireaPlacesRent> _dslPath = new Path<MireaPlacesRent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MireaPlacesRent");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     *
     * Это формула "contractor.title ||' (' || startDate || ', ' || endDate || ')'".
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Комната - объект аренды. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Контрагент. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getContractor()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> contractor()
    {
        return _dslPath.contractor();
    }

    /**
     * @return Тип контрагента.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getContractorType()
     */
    public static OrganizationType.Path<OrganizationType> contractorType()
    {
        return _dslPath.contractorType();
    }

    /**
     * @return Контактное лицо.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getContactorPerson()
     */
    public static ContactorPerson.Path<ContactorPerson> contactorPerson()
    {
        return _dslPath.contactorPerson();
    }

    /**
     * @return Дата начала аренды.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания аренды.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends MireaPlacesRent> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _contractor;
        private OrganizationType.Path<OrganizationType> _contractorType;
        private ContactorPerson.Path<ContactorPerson> _contactorPerson;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     *
     * Это формула "contractor.title ||' (' || startDate || ', ' || endDate || ')'".
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MireaPlacesRentGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Комната - объект аренды. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Контрагент. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getContractor()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> contractor()
        {
            if(_contractor == null )
                _contractor = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_CONTRACTOR, this);
            return _contractor;
        }

    /**
     * @return Тип контрагента.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getContractorType()
     */
        public OrganizationType.Path<OrganizationType> contractorType()
        {
            if(_contractorType == null )
                _contractorType = new OrganizationType.Path<OrganizationType>(L_CONTRACTOR_TYPE, this);
            return _contractorType;
        }

    /**
     * @return Контактное лицо.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getContactorPerson()
     */
        public ContactorPerson.Path<ContactorPerson> contactorPerson()
        {
            if(_contactorPerson == null )
                _contactorPerson = new ContactorPerson.Path<ContactorPerson>(L_CONTACTOR_PERSON, this);
            return _contactorPerson;
        }

    /**
     * @return Дата начала аренды.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(MireaPlacesRentGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания аренды.
     * @see ru.tandemservice.unimirea.entity.MireaPlacesRent#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(MireaPlacesRentGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return MireaPlacesRent.class;
        }

        public String getEntityName()
        {
            return "mireaPlacesRent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
