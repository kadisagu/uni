/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.Pub.MireaPlacesPub;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 12.02.2015
 */
public class MireaPlaceObjectDocumentHandler extends DefaultSearchDataSourceHandler
{
    public MireaPlaceObjectDocumentHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long placeId = context.get(MireaPlacesPub.PLACE_ID);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesObjectToDocumentRelation.class, "doc")
                .column(property("doc"))
            .where(eq(property("doc",UniplacesObjectToDocumentRelation.object()), value(placeId)));
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesObjectToDocumentRelation.class, "doc");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(orderRegistry).pageable(true).build();
    }
}