/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.impl.DialogComponentActivationBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.AddEdit.MireaPlacesAddEdit;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.UniplacesDocumentsManager;
import ru.tandemservice.uniplaces.base.bo.UniplacesDocuments.ui.AddEdit.UniplacesDocumentsAddEdit;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "placeId", required = true)
       })
public class MireaPlacesPubUI extends UIPresenter
{
    private Long _placeId;
    private UniMireaPlace _place;

    public Long getPlaceId()
    {
        return _placeId;
    }

    public void setPlaceId(Long placeId)
    {
        _placeId = placeId;
    }

    public UniMireaPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniMireaPlace place)
    {
        _place = place;
    }

    @Override
    public void onComponentRefresh()
    {
        _place = DataAccessServices.dao().getNotNull(UniMireaPlace.class, _placeId);
    }

    public void onClickEditPlace()
    {
        _uiActivation.asRegionDialog(MireaPlacesAddEdit.class).parameter("placeId", _placeId).activate();
    }

    public void onClickDeletePlace()
    {
        DataAccessServices.dao().delete(_placeId);
        deactivate();
    }

    public BaseSearchListDataSource getDataSource()
    {
        return _uiConfig.getDataSource(MireaPlacesPub.DOCUMENTS_DS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (MireaPlacesPub.DOCUMENTS_DS.equals(dataSource.getName()))
            dataSource.put(MireaPlacesPub.PLACE_ID, getPlaceId());

    }

    public void onClickPrintDocument()
    {
        UniplacesObjectToDocumentRelation document = getDataSource().getRecordById(getListenerParameterAsLong());
        UniplacesDocumentsManager.instance().dao().printDocumentScanCopy(document);
    }

    public void onClickEditDocument()
    {
        new DialogComponentActivationBuilder(UniplacesDocumentsAddEdit.class).parameter("documentId", getListenerParameterAsLong())
                .parameter("objectId", getPlaceId())
                .activate();
    }

    public void onClickDeleteDocument()
    {
        UniplacesDocumentsManager.instance().dao().deleteDocument(getListenerParameterAsLong());
    }

    public void onClickAddDocument()
    {
        new DialogComponentActivationBuilder(UniplacesDocumentsAddEdit.class)
                .parameter("objectId", getPlaceId())
                .activate();
    }

    public void onClickChangeArchiveProperty()
    {
        UniplacesDocumentsManager.instance().dao().changeActiveProperty(getListenerParameterAsLong());
    }
}