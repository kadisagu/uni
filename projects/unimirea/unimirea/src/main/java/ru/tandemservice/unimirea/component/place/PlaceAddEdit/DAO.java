/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.component.place.PlaceAddEdit.Model;
import ru.tandemservice.uniplaces.dao.IUniplacesDAO;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.uniplaces.util.UniplacesClassroomTypeSelectModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.PlaceAddEdit.DAO
{
    @Override
    public void prepare(final Model model)
    {
        UniplacesPlace place = model.getPlace();
        UniPlacesPlaceExt placeExt = getByNaturalId(new UniPlacesPlaceExt.NaturalId(model.getPlace()));
        final ru.tandemservice.unimirea.component.place.PlaceAddEdit.Model modelExt = (ru.tandemservice.unimirea.component.place.PlaceAddEdit.Model) model;


        if (model.isEditForm())
        {
            place = getNotNull(UniplacesPlace.class, place.getId());
            model.setPlace(place);
            modelExt.setFloor(place.getFloor());
            modelExt.setUnit(place.getFloor().getUnit());
            modelExt.setBuilding(place.getFloor().getUnit().getBuilding());
        }
        else if (model.getFloorId() != null)
        {
            UniplacesFloor floor = getNotNull(UniplacesFloor.class, model.getFloorId());
            place.setFloor(floor);
            modelExt.setFloor(floor);
            modelExt.setUnit(floor.getUnit());
            modelExt.setBuilding(floor.getUnit().getBuilding());
        }

        model.setOrgUnitList(IUniplacesDAO.instance.get().getResponsibleOrgUnitModel());
        model.setClassroomTypeModel(new UniplacesClassroomTypeSelectModel());
        model.setPurposeList(HierarchyUtil.listHierarchyNodesWithParents(getCatalogItemListOrderByCode(UniplacesPlacePurpose.class), true));
        model.setConditionList(getCatalogItemListOrderByCode(UniplacesPlaceCondition.class));

        if (placeExt == null)
            placeExt = new UniPlacesPlaceExt();
        else
        {
            modelExt.setPlaceMirea(placeExt.getPlaceMirea());
            modelExt.setResponsiblePerson(placeExt.getResponsiblePerson());
        }

        modelExt.setPlaceExt(placeExt);

        //модель выбора помещений для привязки комнаты
        modelExt.setPlacesModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (modelExt.getFloor() == null)
                    return ListResult.getEmpty();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniMireaPlace.class, "pl")
                        .where(eq(property("pl", UniMireaPlace.floor()), value(modelExt.getFloor())));
                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("pl", UniMireaPlace.title()), value(CoreStringUtils.escapeLike(filter))));

//                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        //модель выбора ответственного лица
        modelExt.setPersonModel(new BaseSingleSelectModel(EmployeePost.P_FULL_TITLE)
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if (primaryKey == null) return null;
                return get(EmployeePost.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e");
                builder.column("e");

                // фильтр по фио + должность + подр. + тип подр.
                if (StringUtils.isNotEmpty(filter))
                {
                    IDQLExpression expression = DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()),
                                                                    DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()),
                                                                                        DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.orgUnit().shortTitle().s()),
                                                                                                            DQLExpressions.property("e", EmployeePost.orgUnit().orgUnitType().title().s())
                                                                                        )));

                    builder.where(DQLExpressions.like(DQLFunctions.upper(expression), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
                }
                if (model.getPlace().getResponsibleOrgUnit() != null)
                        builder.where(eq(property("e", EmployeePost.orgUnit()), value(model.getPlace().getResponsibleOrgUnit())));

                // только активные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.postStatus().active().s()), DQLExpressions.value(Boolean.TRUE)));

                // не архивные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival().s()), DQLExpressions.value(Boolean.FALSE)));

                // сортируем по фио
                builder.order(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()));

                // показываем не больше 50
                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
                statement.setMaxResults(50);

                return new ListResult<EmployeePost>(statement.<EmployeePost>list(), count == null ? 0 : count.intValue());
            }
        });

        //модель выбора зданий
        modelExt.setBuildingModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesBuilding> findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesBuilding.class, "b")
                        .column(property("b"));
                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("b", UniplacesBuilding.title()), value(CoreStringUtils.escapeLike(filter))));

//                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
//                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
//                statement.setMaxResults(50);
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        //модель выбора блоков
        modelExt.setUnitModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesUnit> findValues(String filter)
            {
                if (modelExt.getBuilding() == null)
                    return ListResult.getEmpty();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesUnit.class, "u")
                        .column(property("u"));
                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("u", UniplacesUnit.title()), value(CoreStringUtils.escapeLike(filter))));
                builder.where(eq(property(UniplacesUnit.building().fromAlias("u")), value(modelExt.getBuilding())));

//                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
//                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
//                statement.setMaxResults(50);
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        //модель выбора этажа
        modelExt.setFloorModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String s)
            {
                if (modelExt.getUnit()== null)
                    return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesFloor.class, "f")
                        .column(property("f"));
                if (!StringUtils.isEmpty(s))
                    builder.where(likeUpper(property("f", UniplacesFloor.title()), value(CoreStringUtils.escapeLike(s))));
                builder.where(eq(property(UniplacesFloor.unit().fromAlias("f")), value(modelExt.getUnit())));

//                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
//                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
//                statement.setMaxResults(50);
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });
    }

    @Override
    public void update(Model model)
    {
        model.getPlace().setFloor(((ru.tandemservice.unimirea.component.place.PlaceAddEdit.Model)model).getFloor());
        super.update(model);
        fillAndSaveMireaPlaceExt(model);
    }

    private void fillAndSaveMireaPlaceExt(Model model)
    {
        ru.tandemservice.unimirea.component.place.PlaceAddEdit.Model modelExt = (ru.tandemservice.unimirea.component.place.PlaceAddEdit.Model) model;
        UniPlacesPlaceExt placeExt = modelExt.getPlaceExt();
        placeExt.setPlaceMirea(modelExt.getPlaceMirea());
        placeExt.setPlace(model.getPlace());
        placeExt.setResponsiblePerson(modelExt.getResponsiblePerson());
        saveOrUpdate(placeExt);

    }
}