/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaSettings.ui.OrgUnitCodes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unimirea.base.bo.MireaSettings.MireaSettingsManager;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
@Configuration
public class MireaSettingsOrgUnitCodes extends BusinessComponentManager
{
    public static String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(FORMATIVE_ORG_UNIT_DS, formativeDSColumn(), MireaSettingsManager.instance().createOrgUnitDataSource()))
                .create();
    }

    @Bean
    public ColumnListExtPoint formativeDSColumn()
    {
        return columnListExtPointBuilder(FORMATIVE_ORG_UNIT_DS)
                .addColumn(textColumn("orgUnit", OrgUnit.titleWithType()))
                .addColumn(blockColumn("orgUnitCode", "orgUnitCode"))
                .create();
    }
}