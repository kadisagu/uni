/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea1.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class Model extends AbstractListParagraphAddEditModel<MireaExcludeStuListExtract> implements IGroupModel, IEducationLevelModel
{
    private Course _course;
    private List<Group> _groupList;
    private EduProgramSubject _eduProgramSubject;
    private DevelopForm _developForm;

    private Date _excludeDate;
    private Date _holidayFrom;
    private Date _holidayTo;
    private StudentStatus _statusNew;
    private String _textParagraph;

    private ISelectModel _groupListModel;
    private ISelectModel _eduProgramSubjectModel;
    private ISelectModel _developFormModel;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    public void setExcludeDate(Date excludeDate)
    {
        _excludeDate = excludeDate;
    }

    public Date getHolidayFrom()
    {
        return _holidayFrom;
    }

    public void setHolidayFrom(Date holidayFrom)
    {
        _holidayFrom = holidayFrom;
    }

    public Date getHolidayTo()
    {
        return _holidayTo;
    }

    public void setHolidayTo(Date holidayTo)
    {
        _holidayTo = holidayTo;
    }

    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    public void setStatusNew(StudentStatus statusNew)
    {
        _statusNew = statusNew;
    }

    public EduProgramSubject getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    public void setEduProgramSubject(EduProgramSubject eduProgramSubject)
    {
        _eduProgramSubject = eduProgramSubject;
    }

    public ISelectModel getEduProgramSubjectModel()
    {
        return _eduProgramSubjectModel;
    }

    public void setEduProgramSubjectModel(ISelectModel eduProgramSubjectModel)
    {
        _eduProgramSubjectModel = eduProgramSubjectModel;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }


    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getParagraph().getOrder().getOrgUnit();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return null;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return null;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    public String getTextParagraph()
    {
        return _textParagraph;
    }

    public void setTextParagraph(String textParagraph)
    {
        _textParagraph = textParagraph;
    }
}