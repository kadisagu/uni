/* $Id$ */
package ru.tandemservice.unimirea.util;


import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
public class GeneratorIds
{
    private static AtomicLong lastId = new AtomicLong(System.currentTimeMillis());

    public static Long generateNewId()
    {
        return lastId.getAndIncrement();
    }
}