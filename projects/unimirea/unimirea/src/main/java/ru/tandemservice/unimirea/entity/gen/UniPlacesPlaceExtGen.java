package ru.tandemservice.unimirea.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности помещения для МИРЭА
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniPlacesPlaceExtGen extends EntityBase
 implements INaturalIdentifiable<UniPlacesPlaceExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimirea.entity.UniPlacesPlaceExt";
    public static final String ENTITY_NAME = "uniPlacesPlaceExt";
    public static final int VERSION_HASH = -281325157;
    private static IEntityMeta ENTITY_META;

    public static final String L_PLACE = "place";
    public static final String L_RESPONSIBLE_PERSON = "responsiblePerson";
    public static final String L_PLACE_MIREA = "placeMirea";
    public static final String P_MAY_RENTED = "mayRented";

    private UniplacesPlace _place;     // Помещение
    private EmployeePost _responsiblePerson;     // Ответственное лицо
    private UniMireaPlace _placeMirea;     // Ссылка на помещение (МИРЭА)
    private boolean _mayRented = false;     // Признак возможности сдачи в аренду

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Помещение. Свойство не может быть null и должно быть уникальным.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Ответственное лицо.
     */
    public EmployeePost getResponsiblePerson()
    {
        return _responsiblePerson;
    }

    /**
     * @param responsiblePerson Ответственное лицо.
     */
    public void setResponsiblePerson(EmployeePost responsiblePerson)
    {
        dirty(_responsiblePerson, responsiblePerson);
        _responsiblePerson = responsiblePerson;
    }

    /**
     * @return Ссылка на помещение (МИРЭА). Свойство не может быть null.
     */
    @NotNull
    public UniMireaPlace getPlaceMirea()
    {
        return _placeMirea;
    }

    /**
     * @param placeMirea Ссылка на помещение (МИРЭА). Свойство не может быть null.
     */
    public void setPlaceMirea(UniMireaPlace placeMirea)
    {
        dirty(_placeMirea, placeMirea);
        _placeMirea = placeMirea;
    }

    /**
     * @return Признак возможности сдачи в аренду. Свойство не может быть null.
     */
    @NotNull
    public boolean isMayRented()
    {
        return _mayRented;
    }

    /**
     * @param mayRented Признак возможности сдачи в аренду. Свойство не может быть null.
     */
    public void setMayRented(boolean mayRented)
    {
        dirty(_mayRented, mayRented);
        _mayRented = mayRented;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniPlacesPlaceExtGen)
        {
            if (withNaturalIdProperties)
            {
                setPlace(((UniPlacesPlaceExt)another).getPlace());
            }
            setResponsiblePerson(((UniPlacesPlaceExt)another).getResponsiblePerson());
            setPlaceMirea(((UniPlacesPlaceExt)another).getPlaceMirea());
            setMayRented(((UniPlacesPlaceExt)another).isMayRented());
        }
    }

    public INaturalId<UniPlacesPlaceExtGen> getNaturalId()
    {
        return new NaturalId(getPlace());
    }

    public static class NaturalId extends NaturalIdBase<UniPlacesPlaceExtGen>
    {
        private static final String PROXY_NAME = "UniPlacesPlaceExtNaturalProxy";

        private Long _place;

        public NaturalId()
        {}

        public NaturalId(UniplacesPlace place)
        {
            _place = ((IEntity) place).getId();
        }

        public Long getPlace()
        {
            return _place;
        }

        public void setPlace(Long place)
        {
            _place = place;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniPlacesPlaceExtGen.NaturalId) ) return false;

            UniPlacesPlaceExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getPlace(), that.getPlace()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPlace());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPlace());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniPlacesPlaceExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniPlacesPlaceExt.class;
        }

        public T newInstance()
        {
            return (T) new UniPlacesPlaceExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "place":
                    return obj.getPlace();
                case "responsiblePerson":
                    return obj.getResponsiblePerson();
                case "placeMirea":
                    return obj.getPlaceMirea();
                case "mayRented":
                    return obj.isMayRented();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "responsiblePerson":
                    obj.setResponsiblePerson((EmployeePost) value);
                    return;
                case "placeMirea":
                    obj.setPlaceMirea((UniMireaPlace) value);
                    return;
                case "mayRented":
                    obj.setMayRented((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "place":
                        return true;
                case "responsiblePerson":
                        return true;
                case "placeMirea":
                        return true;
                case "mayRented":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "place":
                    return true;
                case "responsiblePerson":
                    return true;
                case "placeMirea":
                    return true;
                case "mayRented":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "place":
                    return UniplacesPlace.class;
                case "responsiblePerson":
                    return EmployeePost.class;
                case "placeMirea":
                    return UniMireaPlace.class;
                case "mayRented":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniPlacesPlaceExt> _dslPath = new Path<UniPlacesPlaceExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniPlacesPlaceExt");
    }
            

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Ответственное лицо.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#getResponsiblePerson()
     */
    public static EmployeePost.Path<EmployeePost> responsiblePerson()
    {
        return _dslPath.responsiblePerson();
    }

    /**
     * @return Ссылка на помещение (МИРЭА). Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#getPlaceMirea()
     */
    public static UniMireaPlace.Path<UniMireaPlace> placeMirea()
    {
        return _dslPath.placeMirea();
    }

    /**
     * @return Признак возможности сдачи в аренду. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#isMayRented()
     */
    public static PropertyPath<Boolean> mayRented()
    {
        return _dslPath.mayRented();
    }

    public static class Path<E extends UniPlacesPlaceExt> extends EntityPath<E>
    {
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private EmployeePost.Path<EmployeePost> _responsiblePerson;
        private UniMireaPlace.Path<UniMireaPlace> _placeMirea;
        private PropertyPath<Boolean> _mayRented;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Ответственное лицо.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#getResponsiblePerson()
     */
        public EmployeePost.Path<EmployeePost> responsiblePerson()
        {
            if(_responsiblePerson == null )
                _responsiblePerson = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_PERSON, this);
            return _responsiblePerson;
        }

    /**
     * @return Ссылка на помещение (МИРЭА). Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#getPlaceMirea()
     */
        public UniMireaPlace.Path<UniMireaPlace> placeMirea()
        {
            if(_placeMirea == null )
                _placeMirea = new UniMireaPlace.Path<UniMireaPlace>(L_PLACE_MIREA, this);
            return _placeMirea;
        }

    /**
     * @return Признак возможности сдачи в аренду. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.UniPlacesPlaceExt#isMayRented()
     */
        public PropertyPath<Boolean> mayRented()
        {
            if(_mayRented == null )
                _mayRented = new PropertyPath<Boolean>(UniPlacesPlaceExtGen.P_MAY_RENTED, this);
            return _mayRented;
        }

        public Class getEntityClass()
        {
            return UniPlacesPlaceExt.class;
        }

        public String getEntityName()
        {
            return "uniPlacesPlaceExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
