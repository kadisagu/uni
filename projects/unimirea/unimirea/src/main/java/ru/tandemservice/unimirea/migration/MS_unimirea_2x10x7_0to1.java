/* $Id$ */
package ru.tandemservice.unimirea.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.io.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Igor Belanov
 * @since 09.09.2016
 */
public class MS_unimirea_2x10x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.7"),
                        new ScriptDependency("ru.tandemservice.nsiclient", "2.10.7"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.7")
                };
    }

    /**
     * имя, фамилия, отчество и группа принципала
     */
    private class PrincipalInfo
    {
        private final Long _principalId;
        private final String _lastName;
        private final String _firstName;
        private final String _middleName;
        private final String _groupTitle;
        private final Long _contactDataId;
        private List<String> _groupVariants;

        public PrincipalInfo(Long principalId, String lastName, String firstName, String middleName, String groupTitle, Long contactDataId)
        {
            this._principalId = principalId;
            this._lastName = lastName;
            this._firstName = firstName;
            this._middleName = middleName;
            this._groupTitle = groupTitle;
            this._contactDataId = contactDataId;

            _groupVariants = getVariants(_groupTitle);
        }

        public List<String> getVariants(String groupTitle)
        {
            List<String> groupVariants = new ArrayList<>();
            if (null != groupTitle)
            {
                String[] splittedGroupTitle = groupTitle.split("-");

                for (String segment : splittedGroupTitle)
                {
                    try
                    {
                        int intVal = Integer.parseInt(segment);
                        List<String> toAppend = new ArrayList<>();
                        for (int i = 0; i < groupVariants.size(); i++)
                        {
                            String part = groupVariants.get(i) + "-";
                            groupVariants.set(i, part + intVal);
                            toAppend.add(part + "0" + intVal);
                        }
                        groupVariants.addAll(toAppend);

                    } catch (NumberFormatException ex)
                    {
                        if (groupVariants.isEmpty()) groupVariants.add(segment);
                        else
                        {
                            for (int i = 0; i < groupVariants.size(); i++)
                            {
                                groupVariants.set(i, groupVariants.get(i) + "-" + segment);
                            }
                        }
                    }
                }
            }
            return groupVariants;
        }

        public Long getPrincipalId()
        {
            return _principalId;
        }

        public String getLastName()
        {
            return _lastName;
        }

        public String getFirstName()
        {
            return _firstName;
        }

        public String getMiddleName()
        {
            return _middleName;
        }

        public String getGroupTitle()
        {
            return _groupTitle;
        }

        public Long getContactDataId()
        {
            return _contactDataId;
        }

        public List<String> getGroupVariants()
        {
            return _groupVariants;
        }

        public void setGroupVariants(List<String> groupVariants)
        {
            _groupVariants = groupVariants;
        }
    }

    @Override
    public void run(DBTool dbTool) throws Exception
    {
        SQLSelectQuery selectPrincipalIdsWithInfo = new SQLSelectQuery()
                .from(SQLFrom.table("principal_t", "pri")
                        .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.principal_id=pri.id")
                        .innerJoin(SQLFrom.table("student_t", "s"), "s.id=pr.id")
                        .innerJoin(SQLFrom.table("person_t", "per"), "per.id=pr.person_id")
                        .innerJoin(SQLFrom.table("identityCard_t", "ic"), "ic.id=per.identitycard_id")
                        .innerJoin(SQLFrom.table("group_t", "g"), "g.id=s.group_id"))
                .column("pri.id")
                .column("ic.lastname_p")
                .column("ic.firstname_p")
                .column("ic.middlename_p")
                .column("g.title_p")
                .column("per.contactdata_id");

        Statement statement = dbTool.getConnection().createStatement();
        statement.execute(dbTool.getDialect().getSQLTranslator().toSql(selectPrincipalIdsWithInfo));
        ResultSet resultSet = statement.getResultSet();
        List<PrincipalInfo> principalInfoList = new ArrayList<>();
        while (resultSet.next())
        {
            Long principalId = resultSet.getLong(1);
            String lastName = StringUtils.trimToNull(resultSet.getString(2));
            String firstName = StringUtils.trimToNull(resultSet.getString(3));
            String middleName = StringUtils.trimToNull(resultSet.getString(4));
            String groupTitle = StringUtils.trimToNull(resultSet.getString(5));
            Long contactDataId = resultSet.getLong(6);
            principalInfoList.add(new PrincipalInfo(principalId, lastName, firstName, middleName, groupTitle, contactDataId));
        }

        String fileName = System.getProperty("app.install.path") + File.separator + "msp_reports_registered_students.csv";
        String csvSplitSym = ",";

        BufferedReader bufferedReader;
        Reader reader;
        try
        {
            // прочитаем прямо из файла данные, для импорта данных в базу
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
        } catch (FileNotFoundException exception)
        {
            dbTool.fatal("Data file for migration (" + fileName + ") is not found!");
            // кинем дальше, чтобы миграция не прошла
            throw exception;
        }

        dbTool.info("============= импорт логинов и email'ов из LDAP ============= ");

        // обновления для username в таблице principal_t
        MigrationUtils.BatchUpdater principalUsernameUpdater = new MigrationUtils.BatchUpdater("update principal_t set login_p = ? where id = ?", DBType.EMPTY_STRING, DBType.LONG);
        // обновления для email в personcontactdata_t
        MigrationUtils.BatchUpdater contactDataEmailUpdater = new MigrationUtils.BatchUpdater("update personcontactdata_t set email_p = ? where id = ?", DBType.EMPTY_STRING, DBType.LONG);

        int count = 0;
        int totalCount = 0;
        String line;
        while ((line = bufferedReader.readLine()) != null)
        {
            try
            {
                // возьмём значения из csv (убирая кавычки по бокам (если они есть))
                String[] splittedLine = line.split(csvSplitSym);
                String lastName = splittedLine[3].replaceAll("(^\")|(\"$)", "");
                String firstname = StringUtils.trimToNull(splittedLine[2].replaceAll("(^\")|(\"$)", ""));
                String middleName = StringUtils.trimToNull(splittedLine[4].replaceAll("(^\")|(\"$)", ""));
                String groupTitle = StringUtils.trimToNull(splittedLine[6].replaceAll("(^\")|(\"$)", ""));
                String email = StringUtils.trimToNull(splittedLine[8].replaceAll("(^\")|(\"$)", ""));
                String username = StringUtils.trimToNull(splittedLine[9].replaceAll("(^\")|(\"$)", ""));

                List<PrincipalInfo> result = principalInfoList.stream()
                        .filter(item -> isEqualStrings(item.getLastName(), lastName) &&
                                isEqualStrings(item.getFirstName(), firstname) &&
                                isEqualStrings(item.getMiddleName(), middleName) &&
                                isEqualGroupStrings(item.getGroupVariants(), item.getVariants(groupTitle))
                        )
                        .collect(Collectors.toList());

                if (result.isEmpty())
                {
                    dbTool.info("Студент \"" +
                            lastName + " " + firstname + " " + middleName + " (группа: " + groupTitle + ")\" не найден в базе");
                } else if (result.size() > 1)
                {
                    dbTool.info("В базе найдено несколько (" + result.size() + ") студентов \"" +
                            lastName + " " + firstname + " " + middleName + " (группа: " + groupTitle + ")\"");
                } else
                {
                    PrincipalInfo principalInfo = result.get(0);

                    // обновим логин
                    principalUsernameUpdater.addBatch(username, principalInfo.getPrincipalId());

                    // contactDataId - not null, поэтому можно сразу обновлять email
                    contactDataEmailUpdater.addBatch(email, principalInfo.getContactDataId());

//                dbTool.info("Login и email студента \"" + lastName + " " + firstname + " " +
//                        middleName + " (группа: " + groupTitle + ")\" импортированы");
                    count++;
                }
            } catch (Exception ex)
            {
                dbTool.info("Строка обработана с ошибкой: \"" + line + "\"");
                ex.printStackTrace();
            }
            totalCount++;
        }
        reader.close();

        principalUsernameUpdater.executeUpdate(dbTool);
        contactDataEmailUpdater.executeUpdate(dbTool);

        dbTool.info("Обновлены данные студентов: " + count + " из " + totalCount);
        dbTool.info("========================== SUCCESS ==========================");
    }

    private boolean isEqualStrings(String val1, String val2)
    {
        String val1Trimmed = StringUtils.trimToNull(val1);
        String val2Trimmed = StringUtils.trimToNull(val2);

        if (null == val1Trimmed && null == val2Trimmed) return true;
        if (null != val1Trimmed && null != val2Trimmed && val1Trimmed.equals(val2Trimmed)) return true;
        return false;
    }

    private boolean isEqualGroupStrings(List<String> val1, List<String> val2)
    {
        if ((null == val1 || val1.isEmpty()) && (null == val2 || val2.isEmpty())) return true;
        if ((null == val1 || val1.isEmpty()) || (null == val2 || val2.isEmpty())) return false;
        for (String val : val1) if (val2.contains(val)) return true;
        return false;
    }
}
