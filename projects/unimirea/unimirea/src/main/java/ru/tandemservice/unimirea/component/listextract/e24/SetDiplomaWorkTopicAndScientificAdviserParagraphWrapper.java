/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.e24;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 19.05.2016
 */
public class SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper implements Comparable<SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper>
{
    private final Course _course;
    private final Group _group;
    private final EducationLevelsHighSchool _educationLevels;
    private final OrgUnit _formativeOrgUnit;
    private final DevelopForm _developForm;
    private List<SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper> _studentsList;

    public SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper(Course course, Group group, EducationLevelsHighSchool educationLevels, OrgUnit formativeOrgUnit, DevelopForm developForm)
    {
        _course = course;
        _group = group;
        _educationLevels = educationLevels;
        _formativeOrgUnit = formativeOrgUnit;
        _developForm = developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public EducationLevelsHighSchool getEducationLevels()
    {
        return _educationLevels;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public List<SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper> getStudentsList()
    {
        if (null == _studentsList) _studentsList = new ArrayList<>();
        return _studentsList;
    }

    public SetDiplomaWorkTopicAndScientificAdviserStuListExtract getFirstExtract()
    {
        return getStudentsList().isEmpty() || null == getStudentsList().get(0).getExtract() ? null : getStudentsList().get(0).getExtract();
    }

    public List<OrgUnit> getCathedraList()
    {
        List<OrgUnit> result = new ArrayList<>();
        for (SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper wrapper : getStudentsList())
        {
            OrgUnit ou = wrapper.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit();
            if (null != ou && !result.contains(ou)) result.add(ou);
        }
        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper)) return false;

        SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper that = (SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper) o;

        return _course.equals(that.getCourse())
                && _group.equals(that.getGroup())
                && _educationLevels.equals(that.getEducationLevels())
                && _formativeOrgUnit.equals(that.getFormativeOrgUnit())
                && _developForm.equals(that.getDevelopForm());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode() & _educationLevels.hashCode() & _formativeOrgUnit.hashCode() & _developForm.hashCode();
    }

    @Override
    public int compareTo(SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper o)
    {
        if (_course.getIntValue() > o.getCourse().getIntValue()) return 1;
        else if (_course.getIntValue() < o.getCourse().getIntValue()) return -1;

        int result = _group.getTitle().compareTo(o.getGroup().getTitle());
        if(result != 0) return result;

        StructureEducationLevels thisLevels = _educationLevels.getEducationLevel().getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevels().getEducationLevel().getLevelType();

        if (thisLevels == null || thatLevels == null)
            return thisLevels == null ? (thatLevels == null ? 0 : 1) : -1;

        boolean isThisChild = thisLevels.isSpecialty() || thisLevels.isProfile();
        boolean isThatChild = thatLevels.isSpecialty() || thatLevels.isProfile();

        if (isThisChild != isThatChild) return isThisChild ? 1 : -1;

        result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());

        String formativeOrgUnitStr1 = null != _formativeOrgUnit.getNominativeCaseTitle() ? _formativeOrgUnit.getNominativeCaseTitle() : _formativeOrgUnit.getFullTitle();
        String formativeOrgUnitStr2 = null != o.getFormativeOrgUnit().getNominativeCaseTitle() ? o.getFormativeOrgUnit().getNominativeCaseTitle() : o.getFormativeOrgUnit().getFullTitle();

        if (result == 0) result = formativeOrgUnitStr1.compareTo(formativeOrgUnitStr2);
        if (result == 0) result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        return result;
    }
}