/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e17.Pub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.modularextract.e17.Pub.Model;
import ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt;

/**
 * @author Ekaterina Zvereva
 * @since 18.01.2016
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e17.Pub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        ru.tandemservice.unimirea.component.modularextract.e17.Pub.Model modelExt = (ru.tandemservice.unimirea.component.modularextract.e17.Pub.Model) model;
        MireaRestorationStuExtractExt extractExt = DataAccessServices.dao().get(MireaRestorationStuExtractExt.class, MireaRestorationStuExtractExt.restorationStuExtractExt(), model.getExtract());
        if (extractExt != null)
            modelExt.setExtractExt(extractExt);
    }
}