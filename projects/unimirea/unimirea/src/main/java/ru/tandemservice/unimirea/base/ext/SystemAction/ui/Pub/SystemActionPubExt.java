/* $Id$ */
package ru.tandemservice.unimirea.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String MIREA_SYSTEM_ACTION_PUB_ADDON_NAME = "mireaSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(MIREA_SYSTEM_ACTION_PUB_ADDON_NAME, MireaSystemActionPubAddon.class))
                .create();
    }
}