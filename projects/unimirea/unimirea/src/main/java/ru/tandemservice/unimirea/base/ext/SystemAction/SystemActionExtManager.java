/* $Id$ */
package ru.tandemservice.unimirea.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unimirea.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("unimirea_genStudentNumber", new SystemActionDefinition("unimirea", "genStudentNumbers", "onClickGenStudentNumbers", SystemActionPubExt.MIREA_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();

    }
}