/* $Id$ */
package ru.tandemservice.unimirea.component.place.BuildingPub;

import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.BuildingPub.Model
{
    private MireaSecondDisposalRight _secondRight;

    public MireaSecondDisposalRight getSecondRight()
    {
        return _secondRight;
    }

    public void setSecondRight(MireaSecondDisposalRight secondRight)
    {
        _secondRight = secondRight;
    }
}
