package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimirea_2x7x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность mireaSecondDisposalRight

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("mireaseconddisposalright_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_mireaseconddisposalright"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("registryrecord_id", DBType.LONG).setNullable(false),
                                      new DBColumn("disposalright_id", DBType.LONG).setNullable(false),
                                      new DBColumn("rightstartdate_p", DBType.DATE),
                                      new DBColumn("area_p", DBType.LONG),
                                      new DBColumn("document_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("mireaSecondDisposalRight");

        }


    }
}
