/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e15.AddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimirea.entity.MireaEduEnrAsTransferStuExtractExt;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2016
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.DAO
{

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.Model model)
    {
        super.prepare(model);

        Model modelExt = (Model) model;
        MireaEduEnrAsTransferStuExtractExt extractExt = DataAccessServices.dao().get(MireaEduEnrAsTransferStuExtractExt.class, MireaEduEnrAsTransferStuExtractExt.eduEnrAsTransferStuExtract(), model.getExtract());
        if (extractExt != null)
            modelExt.setExtractExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.Model model)
    {
        super.update(model);

        Model modelExt = (Model) model;
        modelExt.getExtractExt().setEduEnrAsTransferStuExtract(model.getExtract());
        if (model.getExtract().getCompensationTypeNew().isBudget())
        {
            modelExt.getExtractExt().setContractDate(null);
            modelExt.getExtractExt().setContractNumber(null);
        }
        saveOrUpdate(modelExt.getExtractExt());
    }
}