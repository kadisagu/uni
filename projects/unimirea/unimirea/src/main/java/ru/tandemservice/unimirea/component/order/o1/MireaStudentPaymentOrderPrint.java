/* $Id$ */
package ru.tandemservice.unimirea.component.order.o1;


import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimirea.component.order.MireaCommonStudentPaymentOrderPrint;
import ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.component.order.CommonStudentPaymentOrderPrint;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.component.order.o1.StudentPaymentOrderPrint;
import ru.tandemservice.unisp.dao.UnispPrintDAO;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 25.02.2016
 */
public class MireaStudentPaymentOrderPrint extends StudentPaymentOrderPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentPaymentsOrder order)
    {

        RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier injectModifier = CommonStudentPaymentOrderPrint.createListOrderInjectModifier(order);
        MireaStudentPaymentsOrderExt orderExt = DataAccessServices.dao().get(MireaStudentPaymentsOrderExt.class, MireaStudentPaymentsOrderExt.studentPaymentsOrder(), order);
        injectModifier.put("protocolDate", orderExt.getProtocolDate() == null ? "__________" : DateFormatter.DEFAULT_DATE_FORMATTER.format(orderExt.getProtocolDate()));
        injectModifier.put("protocolNumber", orderExt.getProtocolNumber() == null ? "__________" : orderExt.getProtocolNumber());
        insertHeaderData(order, injectModifier);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonStudentPaymentOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        List<VirtualParagraph> paragraphs = prepareOrderParagraphsData(order);
        MireaCommonStudentPaymentOrderPrint.injectParagraphs(document, paragraphs);
        injectModifier = new RtfInjectModifier();
        injectModifier.put("n", String.valueOf(paragraphs.size() + 1));
        injectModifier.modify(document);

        return document;
    }

    /**
     * Получаем виртуальные параграфы со студентам, сгруппированными
     * по курсу, направлению подготовки и сроку выплаты стипендии.
     *
     * @param order приказ в котором ищутся студенты
     * @return список сгруппированных студентов
     */
    @SuppressWarnings("unchecked")
    public List<VirtualParagraph> prepareOrderParagraphsData(StudentPaymentsOrder order)
    {
        List<VirtualParagraph> paragraphsList = new ArrayList<>();
        Map<MultiKey, VirtualParagraph<StudentPaymentAppointmentExtract>> paragraphsMap = new HashMap<>();

        for (IAbstractParagraph<StudentPaymentsOrder> paragraph : order.getParagraphList())
        {
            for (IAbstractExtract<Student, StudentPaymentParagraph> extract : paragraph.getExtractList())
            {
                StudentPaymentAppointmentExtract extr = (StudentPaymentAppointmentExtract) extract;
                Object[] keyArray = {extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(),
                        extract.getEntity().getCourse(),
                        extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(),
                        extr.getPaymentStartDate(),
                        extr.getPaymentStopDate()
                };

                MultiKey key = new MultiKey(keyArray);

                VirtualParagraph<StudentPaymentAppointmentExtract> virtParagraph = paragraphsMap.get(key);
                if (null == virtParagraph) virtParagraph = new VirtualParagraph<>(new ArrayList<>());
                virtParagraph.getExtractsList().add(extr);
                paragraphsMap.put(key, virtParagraph);
            }
        }

        paragraphsList.addAll(paragraphsMap.values());
        Collections.sort((List) paragraphsList, PARAGRAPHS_COMPARATOR);

        for (VirtualParagraph<StudentPaymentAppointmentExtract> paragraph : paragraphsList)
            Collections.sort(paragraph.getExtractsList(), UnispPrintDAO.EXTRACT_STUDENT_FIO_COMPARATOR);

        return paragraphsList;
    }

    public static Comparator<VirtualParagraph<StudentPaymentAppointmentExtract>> PARAGRAPHS_COMPARATOR = new Comparator<VirtualParagraph<StudentPaymentAppointmentExtract>>()
    {
        @Override
        public int compare(VirtualParagraph<StudentPaymentAppointmentExtract> par1, VirtualParagraph<StudentPaymentAppointmentExtract> par2)
        {
            StudentPaymentAppointmentExtract extr1 = par1.getFirstExtract();
            StudentPaymentAppointmentExtract extr2 = par2.getFirstExtract();

            if (!extr1.getEntity().getEducationOrgUnit().getFormativeOrgUnit().equals(extr2.getEntity().getEducationOrgUnit().getFormativeOrgUnit()))
                return extr1.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle().compareTo(extr2.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle());

            if (!extr1.getEntity().getCourse().equals(extr2.getEntity().getCourse()))
                return Integer.valueOf(extr1.getEntity().getCourse().getIntValue()).compareTo(extr2.getEntity().getCourse().getIntValue());

            if (!extr1.getPaymentStartDate().equals(extr2.getPaymentStartDate()))
                return extr1.getPaymentStartDate().compareTo(extr2.getPaymentStartDate());

            return extr1.getPaymentStopDate().compareTo(extr2.getPaymentStopDate());
        }
    };

    /**
     * Данные руководителя подразделения
     */
    private void insertHeaderData(StudentPaymentsOrder order, RtfInjectModifier modifier)
    {
        EmployeePost headerFormative = (EmployeePost) order.getOrgUnit().getHead();

        String fio_A = "";
        if (headerFormative != null)
        {
            IdentityCard identityCard = headerFormative.getPerson().getIdentityCard();
            String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.ACCUSATIVE, headerFormative.getPerson().isMale());
            StringBuilder str = new StringBuilder(lastName)
                    .append(' ');
            if (StringUtils.isNotEmpty(identityCard.getFirstName()))
            {
                str.append(Character.toTitleCase(identityCard.getFirstName().charAt(0))).append('.');
            }

            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            {
                str.append(Character.toTitleCase(identityCard.getMiddleName().charAt(0))).append('.');
            }
            fio_A = str.toString();
        }

        modifier.put("managerFIO_A", fio_A);
        StringBuilder postTitleBuilder = new StringBuilder();
        if (headerFormative != null)
            postTitleBuilder.append(CoreStringUtils.getLowerFirst(headerFormative.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle() == null ? "" :
                                                                          headerFormative.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle()));
        if (postTitleBuilder.length() > 0)
        {
            String orgUnitTitle = order.getOrgUnit().getGenitiveCaseTitle();
            postTitleBuilder.append(" ")
                    .append(StringUtils.capitalize(StringUtils.isEmpty(orgUnitTitle) ? order.getOrgUnit().getTitle() : orgUnitTitle));
        }
        modifier.put("managerPostTitle_A", postTitleBuilder.toString());
    }
}
