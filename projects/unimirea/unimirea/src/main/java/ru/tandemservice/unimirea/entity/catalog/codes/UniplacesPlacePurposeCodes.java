package ru.tandemservice.unimirea.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Назначение помещения"
 * Имя сущности : uniplacesPlacePurpose
 * Файл data.xml : unimirea.data.xml
 */
public interface UniplacesPlacePurposeCodes
{
    /** Константа кода (code) элемента : Коридор (title) */
    String KORIDOR = "1";
    /** Константа кода (code) элемента : Лестница (title) */
    String LESTNITSA = "2";
    /** Константа кода (code) элемента : Комната (title) */
    String KOMNATA = "3";
    /** Константа кода (code) элемента : Шахта (title) */
    String SHAHTA = "4";
    /** Константа кода (code) элемента : Аудитория общая (title) */
    String AUDITORIYA_OBTSHAYA = "5";
    /** Константа кода (code) элемента : Аудитория специализированная (title) */
    String AUDITORIYA_SPETSIALIZIROVANNAYA = "6";
    /** Константа кода (code) элемента : Лаборатория общая (title) */
    String LABORATORIYA_OBTSHAYA = "7";
    /** Константа кода (code) элемента : Лаборатория специализированная (title) */
    String LABORATORIYA_SPETSIALIZIROVANNAYA = "8";
    /** Константа кода (code) элемента : Вычислительный класс (title) */
    String VYCHISLITELNYY_KLASS = "9";
    /** Константа кода (code) элемента : Кабинет (title) */
    String KABINET = "10";
    /** Константа кода (code) элемента : Специализированный класс (title) */
    String SPETSIALIZIROVANNYY_KLASS = "11";
    /** Константа кода (code) элемента : Жилое (title) */
    String JILOE = "12";
    /** Константа кода (code) элемента : Административное (title) */
    String ADMINISTRATIVNOE = "13";
    /** Константа кода (code) элемента : Объект физкультуры и спорта (title) */
    String OBEKT_FIZKULTURY_I_SPORTA = "14";
    /** Константа кода (code) элемента : Пункт медицинского обслуживания (title) */
    String PUNKT_MEDITSINSKOGO_OBSLUJIVANIYA = "15";
    /** Константа кода (code) элемента : Пункт общественного питания (title) */
    String PUNKT_OBTSHESTVENNOGO_PITANIYA = "16";
    /** Константа кода (code) элемента : Библиотека (читальный зал) (title) */
    String BIBLIOTEKA_CHITALNYY_ZAL_ = "17";
    /** Константа кода (code) элемента : Хозяйственно-бытовое обслуживание (title) */
    String HOZYAYSTVENNO_BYTOVOE_OBSLUJIVANIE = "18";
    /** Константа кода (code) элемента : Санитарно-гигиеническое обслуживание (title) */
    String SANITARNO_GIGIENICHESKOE_OBSLUJIVANIE = "19";
    /** Константа кода (code) элемента : Досуг, быт и отдых (title) */
    String DOSUG_BYT_I_OTDYH = "20";
    /** Константа кода (code) элемента : Помещения социально-бытовой ориентировки (title) */
    String POMETSHENIYA_SOTSIALNO_BYTOVOY_ORIENTIROVKI = "21";
    /** Константа кода (code) элемента : Трудовое воспитание (title) */
    String TRUDOVOE_VOSPITANIE = "22";
    /** Константа кода (code) элемента : Общее (title) */
    String OBTSHEE = "23";
    /** Константа кода (code) элемента : Гостиница (title) */
    String GOSTINITSA = "24";
    /** Константа кода (code) элемента : Общежитие (title) */
    String OBTSHEJITIE = "25";
    /** Константа кода (code) элемента : Семейное (title) */
    String SEMEYNOE = "26";
    /** Константа кода (code) элемента : Мужское (title) */
    String MUJSKOE = "27";
    /** Константа кода (code) элемента : Женское (title) */
    String JENSKOE = "28";

    Set<String> CODES = ImmutableSet.of(KORIDOR, LESTNITSA, KOMNATA, SHAHTA, AUDITORIYA_OBTSHAYA, AUDITORIYA_SPETSIALIZIROVANNAYA, LABORATORIYA_OBTSHAYA, LABORATORIYA_SPETSIALIZIROVANNAYA, VYCHISLITELNYY_KLASS, KABINET, SPETSIALIZIROVANNYY_KLASS, JILOE, ADMINISTRATIVNOE, OBEKT_FIZKULTURY_I_SPORTA, PUNKT_MEDITSINSKOGO_OBSLUJIVANIYA, PUNKT_OBTSHESTVENNOGO_PITANIYA, BIBLIOTEKA_CHITALNYY_ZAL_, HOZYAYSTVENNO_BYTOVOE_OBSLUJIVANIE, SANITARNO_GIGIENICHESKOE_OBSLUJIVANIE, DOSUG_BYT_I_OTDYH, POMETSHENIYA_SOTSIALNO_BYTOVOY_ORIENTIROVKI, TRUDOVOE_VOSPITANIE, OBTSHEE, GOSTINITSA, OBTSHEJITIE, SEMEYNOE, MUJSKOE, JENSKOE);
}
