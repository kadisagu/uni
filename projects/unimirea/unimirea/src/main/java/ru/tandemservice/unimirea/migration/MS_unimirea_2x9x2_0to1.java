package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unimirea_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2"),
				 new ScriptDependency("ru.tandemservice.nsiclient", "2.9.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mireaRestorationStuExtractExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mrrstrtnstextrctext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ec0376c8"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("restorationstuextractext_id", DBType.LONG).setNullable(false), 
				new DBColumn("protocolnumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("protocoldate_p", DBType.DATE).setNullable(false), 
				new DBColumn("applicationdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("applicationnumber_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("contractdate_p", DBType.DATE),
				new DBColumn("contractnumber_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mireaRestorationStuExtractExt");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mireaEduEnrAsTransferStuExtractExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mredenrastrnsfrstextrctext_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_4d0f14d2"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("eduenrastransferstuextract_id", DBType.LONG).setNullable(false),
									  new DBColumn("protocolnumber_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("protocoldate_p", DBType.DATE).setNullable(false),
									  new DBColumn("applicationdate_p", DBType.DATE).setNullable(false),
									  new DBColumn("applicationnumber_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("contractdate_p", DBType.DATE),
									  new DBColumn("contractnumber_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mireaEduEnrAsTransferStuExtractExt");

		}


    }
}