/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPerson.ui.AccessRestrictionAddEdit;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unimirea.entity.MireaPrincipalToIPRelation;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */

@Input({
               @Bind(key = MireaPersonAccessRestrictionAddEditUI.PERSON_ID, binding = "personId", required = true),
               @Bind(key = MireaPersonAccessRestrictionAddEditUI.PRINCIPAL_TO_IP_RELATION_ID, binding = "principalToIPRelation.id")
       })
public class MireaPersonAccessRestrictionAddEditUI extends UIPresenter
{
    public static final String PERSON_ID = "personId";
    public static final String PRINCIPAL_TO_IP_RELATION_ID = "";

    private MireaPrincipalToIPRelation _principalToIPRelation = new MireaPrincipalToIPRelation();
    private String ipAddress;
    private Long _personId;
    private Principal _principal;
    private boolean _editForm;

    @Override
    public void onComponentRefresh()
    {
        setPrincipal(DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, getPersonId()).getPrincipal());
        if (getPrincipalToIPRelation().getId() != null)
        {
            setEditForm(false);
            setPrincipalToIPRelation(DataAccessServices.dao().get(MireaPrincipalToIPRelation.class,
                                                                  MireaPrincipalToIPRelation.P_ID, getPrincipalToIPRelation().getId()));

            setIpAddress(getPrincipalToIPRelation().getIpAddress());
        }
    }

    @Override
    public void onComponentRender()
    {
        ContextLocal.beginPageTitlePart(_uiConfig.getProperty(isEditForm() ? "ui.sticker.edit" : "ui.sticker.add"));
    }

    //listeners
    public void onClickApply()
    {
        getPrincipalToIPRelation().setPrincipal(getPrincipal());
        getPrincipalToIPRelation().setIpAddress(getIpAddress());
        DataAccessServices.dao().saveOrUpdate(getPrincipalToIPRelation());
        deactivate();
    }

    //Переводит ip-адрес в десятичную форму
    private Long ipToLong(String ipAddress)
    {
        String[] ipAddressInArray = ipAddress.split("\\.");
        long result = 0;
        for (int i = 0; i < ipAddressInArray.length; i++)
        {

            int power = 3 - i;
            int ip = Integer.parseInt(ipAddressInArray[i]);
            result += ip * Math.pow(256, power);
        }
        return result;
    }

    //validator
    //проверяет, что введен текст в формате xxx.xxx.xxx.xxx, где вместо xxx может присутствовать любое число от 0 до 255
    public BaseValidator getIpValidator()
    {
        return new BaseValidator()
        {
            @Override
            public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
            {
                boolean incorrectIP = false;
                String[] ipAddresses = ((String) object).replace(" ", "").split("-", 2);
                Long firstIpInDecimalFormat;
                Long secondIpInDecimalFormat = Long.MAX_VALUE;
                for (String ipAddress : ipAddresses)
                {
                    String[] ipParts = ipAddress.split("\\.", 4);
                    if (ipParts.length != 4)
                    {
                        incorrectIP = true;
                        break;
                    }
                    if (!incorrectIP)
                    {
                        for (String part : ipParts)
                        {
                            if (!StringUtils.isNumeric(part))
                            {
                                incorrectIP = true;
                                break;
                            }
                            else
                            {
                                try
                                {
                                    int numPart = Integer.parseInt(part);
                                    if (numPart < 0 || numPart > 255)
                                    {
                                        incorrectIP = true;
                                        break;
                                    }
                                }
                                catch (NumberFormatException e)
                                {
                                    incorrectIP = true;
                                }
                            }
                        }
                    }
                }
                if (!incorrectIP)
                {
                    firstIpInDecimalFormat = ipToLong(ipAddresses[0]);
                    if (ipAddresses.length>1)
                    {
                        secondIpInDecimalFormat = ipToLong(ipAddresses[1]);
                    }
                    if (firstIpInDecimalFormat > secondIpInDecimalFormat)
                    {
                        throw new ValidatorException("Первый IP-адрес диапазона должен быть меньше второго.");
                    }
                }
                if (incorrectIP)
                {
                    throw new ValidatorException("Введен некорректный IP-адрес.");
                }
            }
        };
    }

    //getters and setters
    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public Long getPersonId()
    {
        return _personId;
    }

    public void setPersonId(Long personId)
    {
        _personId = personId;
    }

    public Principal getPrincipal()
    {
        return _principal;
    }

    public void setPrincipal(Principal principal)
    {
        _principal = principal;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public MireaPrincipalToIPRelation getPrincipalToIPRelation()
    {
        return _principalToIPRelation;
    }

    public void setPrincipalToIPRelation(MireaPrincipalToIPRelation principalToIPRelation)
    {
        _principalToIPRelation = principalToIPRelation;
    }
}
