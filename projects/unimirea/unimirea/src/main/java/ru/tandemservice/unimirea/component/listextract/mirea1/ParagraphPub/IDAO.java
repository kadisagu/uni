/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea1.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public interface IDAO extends IAbstractListParagraphPubDAO<MireaExcludeStuListExtract, Model>
{
}
