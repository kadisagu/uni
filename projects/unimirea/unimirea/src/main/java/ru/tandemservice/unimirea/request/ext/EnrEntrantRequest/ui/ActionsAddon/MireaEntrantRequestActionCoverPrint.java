/* $Id$ */
package ru.tandemservice.unimirea.request.ext.EnrEntrantRequest.ui.ActionsAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unimirea.catalog.entity.codes.EnrScriptItemCodes;

/**
 * @author Ekaterina Zvereva
 * @since 30.09.2015
 */
public class MireaEntrantRequestActionCoverPrint extends NamedUIAction
{
    public MireaEntrantRequestActionCoverPrint(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.MIREA_ENTRANT_PERSONAL_FILE_COVER);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, (Long)presenter.getListenerParameter());
    }
}