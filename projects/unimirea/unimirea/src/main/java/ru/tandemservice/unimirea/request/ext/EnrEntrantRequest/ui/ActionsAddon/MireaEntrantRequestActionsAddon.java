/* $Id$ */
package ru.tandemservice.unimirea.request.ext.EnrEntrantRequest.ui.ActionsAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;

/**
 * @author Ekaterina Zvereva
 * @since 30.09.2015
 */
public class MireaEntrantRequestActionsAddon extends EnrEntrantRequestActionsAddon
{
    public MireaEntrantRequestActionsAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}