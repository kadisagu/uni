/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.PlaceAddEdit.Model
{
    private UniPlacesPlaceExt _placeExt;
    private ISelectModel _personModel;
    private ISelectModel _placesModel;
    private UniMireaPlace _placeMirea;
    private EmployeePost _responsiblePerson;
    private ISelectModel _buildingModel;
    private ISelectModel _unitModel;
    private ISelectModel _floorModel;

    private UniplacesFloor _floor;
    private UniplacesUnit _unit;
    private UniplacesBuilding _building;


    public EmployeePost getResponsiblePerson()
    {
        return _responsiblePerson;
    }

    public void setResponsiblePerson(EmployeePost responsiblePerson)
    {
        _responsiblePerson = responsiblePerson;
    }


    public UniMireaPlace getPlaceMirea()
    {
        return _placeMirea;
    }

    public void setPlaceMirea(UniMireaPlace placeMirea)
    {
        _placeMirea = placeMirea;
    }


    public ISelectModel getPlacesModel()
    {
        return _placesModel;
    }

    public void setPlacesModel(ISelectModel placesModel)
    {
        _placesModel = placesModel;
    }

    public ISelectModel getPersonModel()
    {
        return _personModel;
    }

    public void setPersonModel(ISelectModel personModel)
    {
        _personModel = personModel;
    }

    public UniPlacesPlaceExt getPlaceExt()
    {
        return _placeExt;
    }

    public void setPlaceExt(UniPlacesPlaceExt placeExt)
    {
        _placeExt = placeExt;
    }

    public ISelectModel getBuildingModel()
    {
        return _buildingModel;
    }

    public void setBuildingModel(ISelectModel buildingModel)
    {
        _buildingModel = buildingModel;
    }

    public ISelectModel getUnitModel()
    {
        return _unitModel;
    }

    public void setUnitModel(ISelectModel unitModel)
    {
        _unitModel = unitModel;
    }

    public ISelectModel getFloorModel()
    {
        return _floorModel;
    }

    public void setFloorModel(ISelectModel floorModel)
    {
        _floorModel = floorModel;
    }

    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        _floor = floor;
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }
}