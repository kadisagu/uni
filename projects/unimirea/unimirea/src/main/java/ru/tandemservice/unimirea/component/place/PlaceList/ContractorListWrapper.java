/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;

/**
 * @author Ekaterina Zvereva
 * @since 19.02.2015
 */
public class ContractorListWrapper extends IdentifiableWrapper<ExternalOrgUnit>
{
    public static String P_TITLE = "title";

    String _title;

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public ContractorListWrapper(Long id, String title)
    {
        super(id, title);

        StringBuilder formattedBuilder = new StringBuilder();
        formattedBuilder.append("<div style=\"padding-left:10px\">");
        formattedBuilder.append(title);
        formattedBuilder.append("</div>");
        _title = formattedBuilder.toString();
    }
}