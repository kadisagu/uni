/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceList;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.Pub.MireaPlacesPub;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.Collection;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class Controller extends ru.tandemservice.uniplaces.component.place.PlaceList.Controller
{
    @Override
    protected void prepareDataSource(IBusinessComponent component)
    {
        ru.tandemservice.unimirea.component.place.PlaceList.Model model = (ru.tandemservice.unimirea.component.place.PlaceList.Model)getModel(component);
        if (model.getPlaceDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<PlaceListWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new PublisherLinkColumn("Номер", "place."+ UniplacesPlace.P_NUMBER));
        dataSource.addColumn(new PublisherLinkColumn("Название","place."+ UniplacesPlace.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", "place." + UniplacesPlace.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Высота, м", "place." + UniplacesPlace.P_FRACTIONAL_HEIGHT, new DoubleFormatter(3, true)).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Длина, м", "place." + UniplacesPlace.P_FRACTIONAL_LENGTH, new DoubleFormatter(3, true)).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ширина, м", "place." + UniplacesPlace.P_FRACTIONAL_WIDTH, new DoubleFormatter(3, true)).setOrderable(false).setClickable(false));

        DefaultPublisherLinkResolver resolver = new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                OrgUnit o = ((PlaceListWrapper)entity).getPlace().getResponsibleOrgUnit();

                if (o != null)
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, o.getId());
                else
                    return null;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                OrgUnit o = ((PlaceListWrapper)entity).getPlace().getResponsibleOrgUnit();
                if (o != null)
                    return OrgUnitView.class.getSimpleName();
                else return null;
            }
        };
        dataSource.addColumn(new PublisherLinkColumn("Ответственное подразделение", "place." + UniplacesPlace.responsibleOrgUnit().title())
                                     .setResolver(resolver));
        dataSource.addColumn(new PublisherLinkColumn("Ответственное лицо", "placeExt." + UniPlacesPlaceExt.responsiblePerson().person().fio()).setResolver(resolver).setOrderable(false));

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Контрагенты", ContractorListWrapper.P_TITLE, "contractorList");
        linkColumn.setOrderable(false);
        linkColumn.setFormatter(RawFormatter.INSTANCE);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Object o = getDao().get(entity.getId());

                if (o instanceof ExternalOrgUnit)
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                else
                    return null;

            }

        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new BooleanColumn("Возможна аренда", "placeExt." + UniPlacesPlaceExt.mayRented()));

        DefaultPublisherLinkResolver placeResolver = new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                UniMireaPlace o = ((PlaceListWrapper)entity).getPlaceExt().getPlaceMirea();

                if (o != null)
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, o.getId());
                else
                    return null;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return MireaPlacesPub.class.getSimpleName();
            }
        };

        dataSource.addColumn(new PublisherLinkColumn("Помещение", "placeExt." + UniPlacesPlaceExt.placeMirea().title()).setResolver(placeResolver));
        dataSource.addColumn(new PublisherLinkColumn("Номер помещения", "placeExt." + UniPlacesPlaceExt.placeMirea().number()).setResolver(placeResolver));
        dataSource.addColumn(new PublisherLinkColumn("Этаж", "title","place." + UniplacesPlace.floor()).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Блок", "title", "place." + UniplacesPlace.floor().unit()).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Здание", "title","place." + UniplacesPlace.floor().unit().building()).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Земельный участок", "fullTitle", "place." + UniplacesPlace.floor().unit().building().ground()).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPlace"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePlace", "Удалить помещение {0}?", UniplacesPlace.title().s()));

        model.setPlaceDataSource(dataSource);
    }

    @Override
    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        DynamicListDataSource<PlaceListWrapper> dataSource = ((ru.tandemservice.unimirea.component.place.PlaceList.Model)getModel(component)).getPlaceDataSource();
        dataSource.showFirstPage();
        dataSource.refresh();
    }

    @Override
    public void onClickMassEditPlace(IBusinessComponent component)
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try  {
            final ru.tandemservice.unimirea.component.place.PlaceList.Model model = (ru.tandemservice.unimirea.component.place.PlaceList.Model) this.getModel(component);
            final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getPlaceDataSource().getColumn("select");
            final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
            if (ids.isEmpty()) { throw new ApplicationException("Не выбрано ни одно помещение."); }
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    UniplacesComponents.PLACE_MASS_EDIT, new ParametersMap().add("placeIds", ids)
            ));
        } finally {
            eventLock.release();
        }
    }


    @Override
    public void onClickMassDeletePlace(IBusinessComponent component)
    {
        final ru.tandemservice.unimirea.component.place.PlaceList.Model model = (ru.tandemservice.unimirea.component.place.PlaceList.Model) this.getModel(component);
        final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getPlaceDataSource().getColumn("select");
        final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
        if (ids.isEmpty()) { throw new ApplicationException("Не выбрано ни одно помещение."); }
        int itemsDeleted = getDao().deletePlacesList(ids);

        ContextLocal.getInfoCollector().add("Из выбранных помещений ("+ids.size()+") удалено: " + itemsDeleted + ", невозможно удалить: "+(ids.size() - itemsDeleted)+".");
        checkboxColumn.getSelectedObjects().clear();
    }

    public void onClickAddPlace(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                                           UniplacesComponents.PLACE_ADD_EDIT,
                                           new ParametersMap())
        );
    }
}