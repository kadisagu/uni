/* $Id:$ */
package ru.tandemservice.unimirea.component.listextract.e24.ParagraphAddEdit;

/**
 * @author Dmitry Seleznev
 * @since 27.05.2016
 */
public class Model extends ru.tandemservice.movestudent.component.listextract.e24.ParagraphAddEdit.Model
{
    @Override
    public boolean isDevelopConditionRequired()
    {
        return false;
    }
}