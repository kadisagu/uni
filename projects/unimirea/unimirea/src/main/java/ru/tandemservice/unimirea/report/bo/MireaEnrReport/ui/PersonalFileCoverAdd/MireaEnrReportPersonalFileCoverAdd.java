/* $Id$ */
package ru.tandemservice.unimirea.report.bo.MireaEnrReport.ui.PersonalFileCoverAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
@Configuration
public class MireaEnrReportPersonalFileCoverAdd extends BusinessComponentManager
{
    public static final String REPORT_KEY = "mireaEnr14ReportPersonalFileCover";
    public static final String ENROLLMENT_ORDER_DS = "enrollmentOrderDS";
    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENROLLMENT_ORDER_DS, enrollmentOrderDSHandler()).addColumn(EnrOrder.P_DATE_AND_NUMBER))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentOrderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrder.class)
                .where(EnrOrder.state().code(), (Object) OrderStatesCodes.FINISHED)
                .where(EnrOrder.type().code(), (Object) EnrOrderTypeCodes.ENROLLMENT)
                .where(EnrOrder.enrollmentCampaign(), ENROLLMENT_CAMPAIGN_PARAM)
                .filter(EnrOrder.number())
                .order(EnrOrder.number())
                .order(EnrOrder.commitDate())
                ;
    }
}