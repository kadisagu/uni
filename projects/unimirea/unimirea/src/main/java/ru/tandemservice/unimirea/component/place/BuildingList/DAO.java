/* $Id$ */
package ru.tandemservice.unimirea.component.place.BuildingList;

import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.component.place.BuildingList.Model;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.BuildingList.DAO
{
    public static String STRING_DISPOSAL_RIGHT_SECOND = "secongDisposalRight";

        @Override
        public void prepareListDataSource(Model model)
        {
            DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesBuilding.class, "building");
            orderRegistry.addAdditionalAlias(OrgUnitType.class, "orgUnitType").addAdditionalAlias(OrgUnit.class, "orgUnit");
            orderRegistry.addAdditionalAlias(MireaSecondDisposalRight.class, "secongDisposalRight");
            orderRegistry.addAdditionalAlias(UniplacesBuilding.class, "right");
            orderRegistry.setOrders(UniplacesBuilding.responsibleOrgUnit().fullTitle(), new OrderDescription("orgUnitType", OrgUnitType.title()), new OrderDescription("orgUnit", OrgUnit.title()));
            orderRegistry.setOrders(MireaSecondDisposalRight.disposalRight().title(), new OrderDescription("right", UniplacesDisposalRight.title()), new OrderDescription("building", UniplacesBuilding.number()));

            DynamicListDataSource<UniplacesBuilding> dataSource = model.getDataSource();

            DQLSelectBuilder builder = orderRegistry.buildDQLSelectBuilder().column("building")
                    .joinEntity("building", DQLJoinType.left, MireaSecondDisposalRight.class, "secongDisposalRight",
                                eq(property("building.id"), property(MireaSecondDisposalRight.registryRecord().fromAlias("secongDisposalRight"))))
                    .joinPath(DQLJoinType.left, UniplacesBuilding.responsibleOrgUnit().fromAlias("building"), "orgUnit")
                    .joinPath(DQLJoinType.left, UniplacesBuilding.address().fromAlias("building"), "address")
                    .joinPath(DQLJoinType.left, OrgUnit.orgUnitType().fromAlias("orgUnit"), "orgUnitType")
                    .joinPath(DQLJoinType.left, MireaSecondDisposalRight.disposalRight().fromAlias("secongDisposalRight"), "right");

            EntityOrder entityOrder = dataSource.getEntityOrder();
            if (entityOrder.getKey().equals(UniplacesBuilding.title().s()))
            {
                List<UniplacesBuilding> result = builder.createStatement(getSession()).list();
                Collections.sort(result, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, UniplacesBuilding.title().s()));
                if (entityOrder.getDirection() == OrderDirection.desc)
                {
                    Collections.reverse(result);
                }
                UniBaseUtils.createPage(model.getDataSource(), result);
            }
            else
            {
                orderRegistry.applyOrder(builder, entityOrder);
                UniBaseUtils.createPage(dataSource, builder, getSession());
            }


        List<ViewWrapper<UniplacesBuilding>> list = ViewWrapper.getPatchedList(model.getDataSource());
        for(ViewWrapper<UniplacesBuilding> item : list)
        {
            MireaSecondDisposalRight secondRight = get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.registryRecord(), item.getEntity());
            item.setViewProperty(STRING_DISPOSAL_RIGHT_SECOND, secondRight);
        }
    }
}
