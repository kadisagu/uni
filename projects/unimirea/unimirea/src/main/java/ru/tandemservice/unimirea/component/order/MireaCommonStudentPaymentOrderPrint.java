/* $Id$ */
package ru.tandemservice.unimirea.component.order;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unisp.component.order.CommonStudentPaymentOrderPrint;
import ru.tandemservice.unisp.component.order.VirtualParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 25.02.2016
 */
public class MireaCommonStudentPaymentOrderPrint
{

    public static void injectParagraphs(final RtfDocument document, List<VirtualParagraph> paragraphsList)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonStudentPaymentOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();
            int paragraphNumber = 1;
            for (VirtualParagraph paragraph : paragraphsList)
            {
                RtfDocument reportPart = CommonStudentPaymentOrderPrint.getParagraphPart(paragraph, paragraphNumber);
                RtfUtil.modifySourceList(document.getHeader(), reportPart.getHeader(), reportPart.getElementList());
                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(reportPart.getElementList());
                paragraphNumber++;
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}
