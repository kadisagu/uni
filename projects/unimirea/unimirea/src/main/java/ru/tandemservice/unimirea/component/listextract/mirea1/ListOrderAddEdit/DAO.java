/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.mirea1.ListOrderAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model;

/**
 * @author Ekaterina Zvereva
 * @since 27.03.2017
 */
public class DAO extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.DAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        //Если форма добавления
        if (model.getOrderId() == null && null != model.getOrder().getOrgUnit().getHead())
        {
                model.setResponsibleEmployee((EmployeePost) model.getOrder().getOrgUnit().getHead());
        }
    }
}