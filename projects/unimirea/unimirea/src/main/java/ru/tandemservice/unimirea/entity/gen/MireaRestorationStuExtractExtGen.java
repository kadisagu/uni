package ru.tandemservice.unimirea.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.RestorationStuExtractExt;
import ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки приказа о восстановлении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MireaRestorationStuExtractExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt";
    public static final String ENTITY_NAME = "mireaRestorationStuExtractExt";
    public static final int VERSION_HASH = -298914413;
    private static IEntityMeta ENTITY_META;

    public static final String L_RESTORATION_STU_EXTRACT_EXT = "restorationStuExtractExt";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_APPLICATION_DATE = "applicationDate";
    public static final String P_APPLICATION_NUMBER = "applicationNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_CONTRACT_NUMBER = "contractNumber";

    private RestorationStuExtractExt _restorationStuExtractExt;     // Выписка из приказа о восстановлении
    private String _protocolNumber;     // Номер протокола аттестационной комиссии
    private Date _protocolDate;     // Дата протокола аттестационной комиссии
    private Date _applicationDate;     // Дата заявления
    private String _applicationNumber;     // Номер заявления
    private Date _contractDate;     // Дата договора на обучение
    private String _contractNumber;     // Номер договора на обучение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из приказа о восстановлении. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RestorationStuExtractExt getRestorationStuExtractExt()
    {
        return _restorationStuExtractExt;
    }

    /**
     * @param restorationStuExtractExt Выписка из приказа о восстановлении. Свойство не может быть null и должно быть уникальным.
     */
    public void setRestorationStuExtractExt(RestorationStuExtractExt restorationStuExtractExt)
    {
        dirty(_restorationStuExtractExt, restorationStuExtractExt);
        _restorationStuExtractExt = restorationStuExtractExt;
    }

    /**
     * @return Номер протокола аттестационной комиссии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола аттестационной комиссии. Свойство не может быть null.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Дата протокола аттестационной комиссии. Свойство не может быть null.
     */
    @NotNull
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола аттестационной комиссии. Свойство не может быть null.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Дата заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getApplicationDate()
    {
        return _applicationDate;
    }

    /**
     * @param applicationDate Дата заявления. Свойство не может быть null.
     */
    public void setApplicationDate(Date applicationDate)
    {
        dirty(_applicationDate, applicationDate);
        _applicationDate = applicationDate;
    }

    /**
     * @return Номер заявления. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getApplicationNumber()
    {
        return _applicationNumber;
    }

    /**
     * @param applicationNumber Номер заявления. Свойство не может быть null.
     */
    public void setApplicationNumber(String applicationNumber)
    {
        dirty(_applicationNumber, applicationNumber);
        _applicationNumber = applicationNumber;
    }

    /**
     * @return Дата договора на обучение.
     */
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата договора на обучение.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Номер договора на обучение.
     */
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора на обучение.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MireaRestorationStuExtractExtGen)
        {
            setRestorationStuExtractExt(((MireaRestorationStuExtractExt)another).getRestorationStuExtractExt());
            setProtocolNumber(((MireaRestorationStuExtractExt)another).getProtocolNumber());
            setProtocolDate(((MireaRestorationStuExtractExt)another).getProtocolDate());
            setApplicationDate(((MireaRestorationStuExtractExt)another).getApplicationDate());
            setApplicationNumber(((MireaRestorationStuExtractExt)another).getApplicationNumber());
            setContractDate(((MireaRestorationStuExtractExt)another).getContractDate());
            setContractNumber(((MireaRestorationStuExtractExt)another).getContractNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MireaRestorationStuExtractExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MireaRestorationStuExtractExt.class;
        }

        public T newInstance()
        {
            return (T) new MireaRestorationStuExtractExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "restorationStuExtractExt":
                    return obj.getRestorationStuExtractExt();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "protocolDate":
                    return obj.getProtocolDate();
                case "applicationDate":
                    return obj.getApplicationDate();
                case "applicationNumber":
                    return obj.getApplicationNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "contractNumber":
                    return obj.getContractNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "restorationStuExtractExt":
                    obj.setRestorationStuExtractExt((RestorationStuExtractExt) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "applicationDate":
                    obj.setApplicationDate((Date) value);
                    return;
                case "applicationNumber":
                    obj.setApplicationNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "restorationStuExtractExt":
                        return true;
                case "protocolNumber":
                        return true;
                case "protocolDate":
                        return true;
                case "applicationDate":
                        return true;
                case "applicationNumber":
                        return true;
                case "contractDate":
                        return true;
                case "contractNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "restorationStuExtractExt":
                    return true;
                case "protocolNumber":
                    return true;
                case "protocolDate":
                    return true;
                case "applicationDate":
                    return true;
                case "applicationNumber":
                    return true;
                case "contractDate":
                    return true;
                case "contractNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "restorationStuExtractExt":
                    return RestorationStuExtractExt.class;
                case "protocolNumber":
                    return String.class;
                case "protocolDate":
                    return Date.class;
                case "applicationDate":
                    return Date.class;
                case "applicationNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "contractNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MireaRestorationStuExtractExt> _dslPath = new Path<MireaRestorationStuExtractExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MireaRestorationStuExtractExt");
    }
            

    /**
     * @return Выписка из приказа о восстановлении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getRestorationStuExtractExt()
     */
    public static RestorationStuExtractExt.Path<RestorationStuExtractExt> restorationStuExtractExt()
    {
        return _dslPath.restorationStuExtractExt();
    }

    /**
     * @return Номер протокола аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Дата протокола аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getApplicationDate()
     */
    public static PropertyPath<Date> applicationDate()
    {
        return _dslPath.applicationDate();
    }

    /**
     * @return Номер заявления. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getApplicationNumber()
     */
    public static PropertyPath<String> applicationNumber()
    {
        return _dslPath.applicationNumber();
    }

    /**
     * @return Дата договора на обучение.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Номер договора на обучение.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    public static class Path<E extends MireaRestorationStuExtractExt> extends EntityPath<E>
    {
        private RestorationStuExtractExt.Path<RestorationStuExtractExt> _restorationStuExtractExt;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<Date> _applicationDate;
        private PropertyPath<String> _applicationNumber;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<String> _contractNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из приказа о восстановлении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getRestorationStuExtractExt()
     */
        public RestorationStuExtractExt.Path<RestorationStuExtractExt> restorationStuExtractExt()
        {
            if(_restorationStuExtractExt == null )
                _restorationStuExtractExt = new RestorationStuExtractExt.Path<RestorationStuExtractExt>(L_RESTORATION_STU_EXTRACT_EXT, this);
            return _restorationStuExtractExt;
        }

    /**
     * @return Номер протокола аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(MireaRestorationStuExtractExtGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Дата протокола аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(MireaRestorationStuExtractExtGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getApplicationDate()
     */
        public PropertyPath<Date> applicationDate()
        {
            if(_applicationDate == null )
                _applicationDate = new PropertyPath<Date>(MireaRestorationStuExtractExtGen.P_APPLICATION_DATE, this);
            return _applicationDate;
        }

    /**
     * @return Номер заявления. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getApplicationNumber()
     */
        public PropertyPath<String> applicationNumber()
        {
            if(_applicationNumber == null )
                _applicationNumber = new PropertyPath<String>(MireaRestorationStuExtractExtGen.P_APPLICATION_NUMBER, this);
            return _applicationNumber;
        }

    /**
     * @return Дата договора на обучение.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(MireaRestorationStuExtractExtGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Номер договора на обучение.
     * @see ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(MireaRestorationStuExtractExtGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

        public Class getEntityClass()
        {
            return MireaRestorationStuExtractExt.class;
        }

        public String getEntityName()
        {
            return "mireaRestorationStuExtractExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
