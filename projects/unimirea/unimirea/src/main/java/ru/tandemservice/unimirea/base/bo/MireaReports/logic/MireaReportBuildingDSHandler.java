/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 24.02.2015
 */
public class MireaReportBuildingDSHandler extends DefaultComboDataSourceHandler
{
    public MireaReportBuildingDSHandler(String ownerId)
    {
        super(ownerId, UniplacesBuilding.class, UniplacesBuilding.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Object ground = ep.context.get(IMireaReportsDAO.GROUNDS);

        if (ground != null && !((Collection) ground).isEmpty())
            ep.dqlBuilder.where(in(property(UniplacesBuilding.ground().fromAlias("e")), (Collection) ground));
    }
}