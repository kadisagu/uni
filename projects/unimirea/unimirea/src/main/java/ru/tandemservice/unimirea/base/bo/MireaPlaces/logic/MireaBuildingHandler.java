/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class MireaBuildingHandler extends DefaultComboDataSourceHandler
{
    public MireaBuildingHandler(String ownerId)
    {
        super(ownerId, UniplacesBuilding.class, UniplacesBuilding.P_TITLE);
        setOrderByProperty(UniplacesBuilding.P_TITLE);
    }


}