/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaRent.ui.RenterAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
@Input({
        @Bind(key = "objectId", binding = "placeId", required = true),
        @Bind(key = "rentId", binding = "rentId")
})
public class MireaRentRenterAddEditUI extends UIPresenter
{
    private Long _placeId;
    private Long _rentId;
    private MireaPlacesRent _rentObject;
    private ISelectModel _contactorPersonModel;

    public ISelectModel getContactorPersonModel()
    {
        return _contactorPersonModel;
    }

    public void setContactorPersonModel(ISelectModel contactorPersonModel)
    {
        _contactorPersonModel = contactorPersonModel;
    }

    public Long getPlaceId()
    {
        return _placeId;
    }

    public void setPlaceId(Long placeId)
    {
        _placeId = placeId;
    }

    public Long getRentId()
    {
        return _rentId;
    }

    public void setRentId(Long rentId)
    {
        _rentId = rentId;
    }

    public MireaPlacesRent getRentObject()
    {
        return _rentObject;
    }

    public void setRentObject(MireaPlacesRent rentObject)
    {
        _rentObject = rentObject;
    }

    @Override
    public void onComponentRefresh()
    {
        if (_rentId != null)
            _rentObject = DataAccessServices.dao().getNotNull(MireaPlacesRent.class, _rentId);
        else _rentObject = new MireaPlacesRent();

        setContactorPersonModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return DataAccessServices.dao().get(ContactorPerson.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = null;
                if (getRentObject().getContractor() != null)
                    builder = new DQLSelectBuilder().fromEntity(JuridicalContactor.class, "p")
                            .where(eq(property("p", JuridicalContactor.externalOrgUnit()), value(getRentObject().getContractor())));
                else
                    builder = new DQLSelectBuilder().fromEntity(ContactorPerson.class, "p");

                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property(ContactorPerson.person().fio().asAlias("p")), value(CoreStringUtils.escapeLike(filter))));

                return new ListResult(DataAccessServices.dao().getList(builder));
            }
        });
    }

    public void onClickApply()
    {
        _rentObject.setPlace((UniplacesPlace)DataAccessServices.dao().get(_placeId));
        DataAccessServices.dao().saveOrUpdate(_rentObject);
        deactivate();
    }
}