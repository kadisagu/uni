/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unimirea.entity.UniMireaPlace;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
public class MireaPlacesHandler extends DefaultSearchDataSourceHandler
{
    public MireaPlacesHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Object building = context.get(IMireaPlacesDAO.BUILDING);
        Object unit = context.get(IMireaPlacesDAO.UNIT);
        Object floor = context.get(IMireaPlacesDAO.FLOOR);
        Object title = context.get(IMireaPlacesDAO.TITLE);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniMireaPlace.class, "pl")
                .column(property("pl"));
        if (building != null)
            builder.where(eq(property(UniMireaPlace.floor().unit().building().fromAlias("pl")), commonValue(building)));

        if (unit != null)
            builder.where(eq(property(UniMireaPlace.floor().unit().fromAlias("pl")), commonValue(unit)));

        if (floor != null)
            builder.where(eq(property(UniMireaPlace.floor().fromAlias("pl")), commonValue(floor)));
        if(!StringUtils.isEmpty((String)title))
            builder.where(likeUpper(property(UniMireaPlace.title().fromAlias("pl")), value(CoreStringUtils.escapeLike((String)title))));

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniMireaPlace.class, "pl");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(orderRegistry).pageable(true).build();
    }
}