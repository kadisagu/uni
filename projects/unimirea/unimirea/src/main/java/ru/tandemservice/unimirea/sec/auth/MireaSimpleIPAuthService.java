/* $Id$ */
package ru.tandemservice.unimirea.sec.auth;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.auth.SimpleAuthService;
import ru.tandemservice.unimirea.entity.MireaPrincipalToIPRelation;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */
public class MireaSimpleIPAuthService extends SimpleAuthService
{
    @Override
    public boolean authenticate(IPrincipal principal, String password)
    {
        boolean correctPassword = super.authenticate(principal, password);
        boolean correctIp = false;
        if (correctPassword)
        {
            DQLSelectBuilder ipListDql = new DQLSelectBuilder().fromEntity(MireaPrincipalToIPRelation.class, "p")
                    .column(property("p", MireaPrincipalToIPRelation.P_IP_ADDRESS), "ip")
                    .where(eqValue(property("p", MireaPrincipalToIPRelation.L_PRINCIPAL), principal));

            String currentIP = UserContext.getInstance().getUserIpAddress();
            List<String> ipList = DataAccessServices.dao().getList(ipListDql);
            correctIp = ipList.isEmpty() || isIpInList(currentIP, ipList);

        }
        if (correctPassword && !correctIp)
        {
            throw new ApplicationException("Невозможно выполнить вход с текущего IP-адреса");
        }
        return correctPassword;
    }

    /**
     * Ищет находится ли ip-адрес в списке ip-адресов и диапазонов ip-адресов
     *
     * @param currentIP искомый ip-адресс
     * @param ipList    список ip-адресов и диапазонов ip-адресов
     * @return находится ли ip-адрес в списке ip-адресов и диапазонов ip-адресов
     */
    private boolean isIpInList(String currentIP, List<String> ipList)
    {
        boolean correctIp = false;

        for (String ipAddress : ipList)
        {
            if (ipAddress.contains("-"))
            {
                if (isIpInRange(currentIP, ipAddress))
                {
                    correctIp = true;
                    break;
                }
            }
            else if (ipAddress.equals(currentIP))
            {
                correctIp = true;
                break;
            }
        }

        return correctIp;
    }

    //Ищет находится ли IP-адрес в заданном диапаозне вида xxxx.xxxx.xxxx.xxxx-xxxx.xxxx.xxxx.xxx
    private boolean isIpInRange(String currentIP, String ipRange)
    {
        String[] ipRangeEdges = ipRange.split("-", 2);
        long currentIpInDecimalFormat = ipToLong(currentIP);
        long fistIpInDecimalFormat = ipToLong(ipRangeEdges[0]);
        long secondIpInDecimalFormat = ipToLong(ipRangeEdges[1]);
        return currentIpInDecimalFormat >= fistIpInDecimalFormat && currentIpInDecimalFormat <= secondIpInDecimalFormat;
    }

    //Переводит ip-адрес в десятичную форму
    private Long ipToLong(String ipAddress)
    {
        String[] ipAddressInArray = ipAddress.split("\\.");
        long result = 0;
        for (int i = 0; i < ipAddressInArray.length; i++)
        {
            int power = 3 - i;
            int ip = Integer.parseInt(ipAddressInArray[i]);
            result += ip * Math.pow(256, power);
        }
        return result;
    }
}
