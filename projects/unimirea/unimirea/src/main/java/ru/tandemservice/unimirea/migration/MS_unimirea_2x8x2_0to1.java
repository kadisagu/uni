package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimirea_2x8x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность orgUnitCodeForStudentNumber

		// создана новая сущность
		if (!tool.tableExists("mirea_ou_code_stud_number_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("mirea_ou_code_stud_number_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_orgunitcodeforstudentnumber"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("orgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("orgUnitCodeForStudentNumber");

		}
    }
}