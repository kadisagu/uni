/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceList;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.Arrays;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 16.02.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.PlaceList.Model
{
    public static final Long AVALIABLE_RENT_TRUE = 0L;
    public static final Long AVALIABLE_RENT_FALSE = 1L;
    public static final List<IEntity> AVALIABLE_STATUS_LIST = Arrays.asList(new IEntity[]{new IdentifiableWrapper<>(AVALIABLE_RENT_TRUE, "Да"), new IdentifiableWrapper<>(AVALIABLE_RENT_FALSE, "Нет")});

    public static final Long STATUS_RENT_FREE = 0L;
    public static final Long STATUS_RENT_BUSY = 1L;
    public static final List<IEntity> STATUS_RENT_LIST = Arrays.asList(new IEntity[]{new IdentifiableWrapper<>(STATUS_RENT_FREE, "Свободно от аренды"), new IdentifiableWrapper<>(STATUS_RENT_BUSY, "Арендовано")});

    private DynamicListDataSource<PlaceListWrapper> _placeDataSource;

    private ISelectModel _contractorModel;
    private ISelectModel _responsiblePersonModel;
    private ISelectModel _mireaPlaceList;

    public ISelectModel getMireaPlaceList()
    {
        return _mireaPlaceList;
    }

    public void setMireaPlaceList(ISelectModel mireaPlaceList)
    {
        _mireaPlaceList = mireaPlaceList;
    }

    public List<IEntity> getAvaliableRentList()
    {
        return AVALIABLE_STATUS_LIST;
    }

    public List<IEntity> getStateRentList()
    {
        return STATUS_RENT_LIST;
    }


    public ISelectModel getResponsiblePersonModel()
    {
        return _responsiblePersonModel;
    }

    public void setResponsiblePersonModel(ISelectModel responsiblePersonModel)
    {
        _responsiblePersonModel = responsiblePersonModel;
    }

    public DynamicListDataSource<PlaceListWrapper> getPlaceDataSource()
    {
        return _placeDataSource;
    }

    public void setPlaceDataSource(DynamicListDataSource<PlaceListWrapper> placeDataSource)
    {
        _placeDataSource = placeDataSource;
    }

    public ISelectModel getContractorModel()
    {
        return _contractorModel;
    }

    public void setContractorModel(ISelectModel contractorModel)
    {
        _contractorModel = contractorModel;
    }
}