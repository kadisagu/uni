/* $Id$ */
package ru.tandemservice.unimirea.report.bo.MireaEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unimirea.report.bo.MireaEnrReport.ui.PersonalFileCoverAdd.MireaEnrReportPersonalFileCoverAddUI;

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
public interface IMireaEnrReportDao
{
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] createMassPersonalFileReport(MireaEnrReportPersonalFileCoverAddUI reportParams);
}