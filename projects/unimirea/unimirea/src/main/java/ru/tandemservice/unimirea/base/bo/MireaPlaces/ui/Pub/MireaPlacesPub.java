/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.MireaPlacesManager;
import ru.tandemservice.uniplaces.entity.place.UniplacesObjectToDocumentRelation;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
@Configuration
public class MireaPlacesPub extends BusinessComponentManager
{
    public static final String DOCUMENTS_DS="documentsDS";
    public static final String PLACE_ID = "mireaPlaceId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DOCUMENTS_DS, documentsDSColumn(), placesDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint documentsDSColumn()
    {
        return columnListExtPointBuilder(DOCUMENTS_DS)
                .addColumn(publisherColumn("title", UniplacesObjectToDocumentRelation.title()))
                .addColumn(textColumn("startDate", UniplacesObjectToDocumentRelation.startDate()))
                .addColumn(textColumn("endDate", UniplacesObjectToDocumentRelation.endDate()))
                .addColumn(textColumn("comment", UniplacesObjectToDocumentRelation.comment()))
                .addColumn(toggleColumn("active", UniplacesObjectToDocumentRelation.active()).toggleOnListener("onClickChangeArchiveProperty").
                        toggleOffListener("onClickChangeArchiveProperty").permissionKey("mireaPlaceChangeActiveDocument"))
                .addColumn(actionColumn("printer", new Icon("printer", "Печатать"), "onClickPrintDocument").permissionKey("printMireaPlaceDocument").create())
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditDocument").permissionKey("editMireaPlaceDocument"))
                .addColumn(actionColumn("delete",CommonDefines.ICON_DELETE, "onClickDeleteDocument").permissionKey("deleteMireaPlaceDocument"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> placesDSHandler() {
        return MireaPlacesManager.instance().createDocumentsDataSource();
    }

}