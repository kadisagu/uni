package ru.tandemservice.unimirea.component.documents.d5.Add;

/**
 * Created by hgfhg on 21.07.2016.
 */

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uni.util.formatters.PersonIofFormatter;

import java.util.Calendar;
import java.util.Date;


/**
 * @author vip_delete
 * @since 16.10.2009
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        boolean male = model.getStudent().getPerson().getIdentityCard().getSex().isMale();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();
        Calendar c = Calendar.getInstance();
         c.setTime(model.getFormingDate());

         String attType ="";
                switch (model.getAttestation().getId().intValue()){
                 case 0:
                     attType = "промежуточной аттестации";
                     break;
                    case 1:
                        attType = "государственной итоговой аттестации";
                        break;
                    case 2:
                        attType = "итоговой аттестации";
                        break;
                    case 3:
                        attType = "подготовки и защиты ВПК и сдачи ИГЭ";
                        break;
                    case 4:
                        attType ="сдачи итоговых госэкзаменов (ИГЭ)";
                        break;
                }

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_G", academy.getGenitiveCaseTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("birthDate", model.getStudent().getPerson().getBirthDateStr())
                .put("sexTitle", male ? "он" : "она")
                .put("studentCase", male ? "студентом" : "студенткой")
                .put("course", model.getStudent().getCourse().getTitle())
   //
                .put("departmentTitle", attType)
                .put("developFormTitle", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("orgUnitTitle", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle())
                .put("orgUnitTitleCase", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())
                .put("compensationTypeStr", model.getStudent().getCompensationType().isBudget() ? "бюджетной" : "договорной")
                .put("developPeriodStr", model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("term", model.getTerm() == null ? "" : model.getTerm().toString())
                .put("secretarTitle", model.getSecretarTitle())
                .put("phoneTitle", model.getPhone())
                .put("documentForTitle", model.getDocumentForTitle())
                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())
                .put("rectorAlt", academy.getHead() == null ? "" : PersonIofFormatter.INSTANCE.format((IdentityCard) academy.getHead().getEmployee().getPerson().getIdentityCard()));



        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "очной");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "заочной");
        else
            injectModifier.put("developFormTitle_G", "очно-заочной");
//
        //   период	действия справки
        Date begPeri = model.getEduFrom();
        String beginPeriod = RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(begPeri);
        int dayPeri = model.getCntDays();
        String dayPeriod = String.valueOf(dayPeri);
        Calendar dayend = Calendar.getInstance();
        dayend.setTime(model.getEduFrom()); //устанавливаем дату, с которой будет производить операции
        dayend.add(Calendar.DAY_OF_MONTH, dayPeri);// прибавляем дни к установленной дате
        Date endPeri = dayend.getTime(); // получаем измененную дату
        String endPeriod = RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(endPeri);

        return injectModifier;
    }
}
