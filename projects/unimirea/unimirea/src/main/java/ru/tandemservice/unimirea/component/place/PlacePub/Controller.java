/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlacePub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.activator.impl.DialogComponentActivationBuilder;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Pub.ExternalOrgUnitPub;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unimirea.base.bo.MireaRent.MireaRentManager;
import ru.tandemservice.unimirea.base.bo.MireaRent.ui.RenterAddEdit.MireaRentRenterAddEdit;
import ru.tandemservice.unimirea.base.bo.MireaRent.ui.RenterDocumentAddEdit.MireaRentRenterDocumentAddEdit;
import ru.tandemservice.unimirea.entity.MireaPlaceRentToDocumentRelation;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
public class Controller extends ru.tandemservice.uniplaces.component.place.PlacePub.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareRentDataSource(component);
    }

    protected void prepareRentDataSource(IBusinessComponent component)
    {
        if (((Model) getModel(component)).getRentDataSource() != null) return;
        DefaultPublisherLinkResolver resolver = new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ExternalOrgUnit o = ((MireaPlacesRent)entity.getProperty(Model.RENT_OBJECT)).getContractor();

                if (o != null)
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, o.getId());
                else
                    return null;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return ExternalOrgUnitPub.class.getSimpleName();
            }
        };

        DynamicListDataSource<DataWrapper> rentDataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) getDao()).prepareRentDataSource((Model) getModel(component1));
        });
        rentDataSource.addColumn(new PublisherLinkColumn("Контрагент", Model.RENT_OBJECT_EXT + MireaPlacesRent.contractor().title().s()).setResolver(resolver).setOrderable(false).setMergeRows(true)
                                         .setMergeRowIdResolver(entity -> ((MireaPlacesRent) entity.getProperty(Model.RENT_OBJECT)).getId().toString()));

        rentDataSource.addColumn(new PublisherLinkColumn("Контактное лицо", Model.RENT_OBJECT_EXT + MireaPlacesRent.contactorPerson().title().s()).setResolver(resolver).setOrderable(false).
                setMergeRows(true).setMergeRowIdResolver(entity -> ((MireaPlacesRent) entity.getProperty(Model.RENT_OBJECT)).getId().toString()));

        HeadColumn headPeriod = new HeadColumn("period", "Период аренды");
        headPeriod.setHeaderAlign("center");
        headPeriod.addColumn(new SimpleColumn("Дата начала", Model.RENT_OBJECT_EXT + MireaPlacesRent.P_START_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER)
                                     .setOrderable(false).setHeaderAlign("center").setClickable(false)
                                     .setMergeRows(true).setMergeRowIdResolver(entity -> ((MireaPlacesRent) entity.getProperty(Model.RENT_OBJECT)).getId().toString()));
        headPeriod.addColumn(new SimpleColumn("Дата окончания", Model.RENT_OBJECT_EXT + MireaPlacesRent.P_END_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setHeaderAlign("center").setWidth(1)
                                     .setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(entity -> ((MireaPlacesRent) entity.getProperty(Model.RENT_OBJECT)).getId().toString()));
        rentDataSource.addColumn(headPeriod);

        rentDataSource.addColumn(new ActionColumn("editContractor", ActionColumn.EDIT, "onClickEditContractor").setPermissionKey("editPlaceContractor").setMergeRows(true)
                                         .setMergeRowIdResolver(entity -> ((MireaPlacesRent) entity.getProperty(Model.RENT_OBJECT)).getId().toString()));
        rentDataSource.addColumn(new ActionColumn("deleteContractor", ActionColumn.DELETE, "onClickDeleteContractor").setPermissionKey("deletePlaceContractor").setMergeRows(true)
                                         .setMergeRowIdResolver(entity -> ((MireaPlacesRent) entity.getProperty(Model.RENT_OBJECT)).getId().toString()));

        HeadColumn documentColumn = new HeadColumn("documents", "Документы");
        documentColumn.setHeaderAlign("center");
        documentColumn.addColumn(new SimpleColumn("Наименование документа", Model.DOCUMENT_OBJECT_EXT + MireaPlaceRentToDocumentRelation.P_TITLE).setOrderable(false).setHeaderAlign("center"));
        documentColumn.addColumn(new SimpleColumn("Дата начала", Model.DOCUMENT_OBJECT_EXT + MireaPlaceRentToDocumentRelation.P_START_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        documentColumn.addColumn(new SimpleColumn("Дата окончания", Model.DOCUMENT_OBJECT_EXT + MireaPlaceRentToDocumentRelation.P_END_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        documentColumn.addColumn(new SimpleColumn("Примечание", Model.DOCUMENT_OBJECT_EXT + MireaPlaceRentToDocumentRelation.P_COMMENT).setOrderable(false).setHeaderAlign("center"));
        documentColumn.addColumn(new ToggleColumn("Активный", Model.DOCUMENT_OBJECT_EXT + MireaPlaceRentToDocumentRelation.active()).toggleOnListener("onClickChangeActiveProperty")
                                         .toggleOffListener("onClickChangeActiveProperty").setPermissionKey("changeActiveRentDocumentMirea").setClickable(true));
        documentColumn.addColumn(new BooleanColumn("Договор аренды", Model.DOCUMENT_OBJECT_EXT + MireaPlaceRentToDocumentRelation.P_RENT_CONTRACT).setHeaderAlign("center"));
        documentColumn.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintRentDocument", "Печать").setPermissionKey("printPlaceRentDocumentMirea").setDisabledProperty(Model.DISABLED));
        documentColumn.addColumn(new ActionColumn("edit", ActionColumn.EDIT, "onClickEditRentDocument").setPermissionKey("editPlaceRentDocumentMirea").setDisabledProperty(Model.DISABLED));
        documentColumn.addColumn(new ActionColumn("delete", ActionColumn.DELETE, "onClickDeleteRentDocument").setPermissionKey("deletePlaceRentDocumentMirea").setDisabledProperty(Model.DISABLED));
        rentDataSource.addColumn(documentColumn);

        ((Model) getModel(component)).setRentDataSource(rentDataSource);
    }


    public void onClickAddRent(IBusinessComponent component)
    {
        new DialogComponentActivationBuilder(MireaRentRenterAddEdit.class)
                .parameter("objectId", getModel(component).getObjectID())
                .activate();
    }

    public void onClickAddRentDocument(IBusinessComponent component)
    {
        new DialogComponentActivationBuilder(MireaRentRenterDocumentAddEdit.class)
                .parameter("objectId", getModel(component).getObjectID())
                .activate();
    }

    public void onClickChangeActiveProperty(IBusinessComponent component)
    {
        DataWrapper wrapper = ((Model) getModel(component)).getContractorsDocumentsMap().get((Long) component.getListenerParameter());
        MireaPlaceRentToDocumentRelation document = (MireaPlaceRentToDocumentRelation) wrapper.getProperty(Model.DOCUMENT_OBJECT);
        MireaRentManager.instance().dao().changeActiveProperty(document.getId());
    }

    public void onClickPrintRentDocument(IBusinessComponent component)
    {
        DataWrapper wrapper = ((Model) getModel(component)).getContractorsDocumentsMap().get((Long) component.getListenerParameter());
        MireaPlaceRentToDocumentRelation document = (MireaPlaceRentToDocumentRelation) wrapper.getProperty(Model.DOCUMENT_OBJECT);
        MireaRentManager.instance().dao().printDocumentScanCopy(document);
    }

    public void onClickDeleteRentDocument(IBusinessComponent component)
    {
        DataWrapper wrapper = ((Model) getModel(component)).getContractorsDocumentsMap().get((Long) component.getListenerParameter());
        MireaPlaceRentToDocumentRelation document = (MireaPlaceRentToDocumentRelation) wrapper.getProperty(Model.DOCUMENT_OBJECT);
        MireaRentManager.instance().dao().deleteDocument(document.getId());
    }

    public void onClickEditRentDocument(IBusinessComponent component)
    {
        DataWrapper wrapper = ((Model) getModel(component)).getContractorsDocumentsMap().get((Long) component.getListenerParameter());
        MireaPlaceRentToDocumentRelation document = (MireaPlaceRentToDocumentRelation) wrapper.getProperty(Model.DOCUMENT_OBJECT);
        new DialogComponentActivationBuilder(MireaRentRenterDocumentAddEdit.class).parameter("documentId", document.getId())
                .parameter("objectId", getModel(component).getObjectID())
                .activate();
    }

    public void onClickEditContractor(IBusinessComponent component)
    {
        DataWrapper wrapper = ((Model) getModel(component)).getContractorsDocumentsMap().get((Long) component.getListenerParameter());
        MireaPlacesRent rent = (MireaPlacesRent) wrapper.getProperty(Model.RENT_OBJECT);
        new DialogComponentActivationBuilder(MireaRentRenterAddEdit.class)
                .parameter("objectId", getModel(component).getObjectID())
                .parameter("rentId", rent.getId())
                .activate();
    }

    public void onClickDeleteContractor(IBusinessComponent component)
    {
        DataWrapper wrapper = ((Model) getModel(component)).getContractorsDocumentsMap().get((Long) component.getListenerParameter());
        MireaPlacesRent rent = (MireaPlacesRent) wrapper.getProperty(Model.RENT_OBJECT);
        getDao().delete(rent);
    }
}