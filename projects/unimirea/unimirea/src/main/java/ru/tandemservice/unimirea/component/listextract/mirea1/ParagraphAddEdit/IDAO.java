/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea1.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<MireaExcludeStuListExtract, Model>
{
}
