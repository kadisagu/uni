package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimirea_2x7x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность codesMSROrdersRelation

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("codesmsrordersrelation_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("extracttype_p", DBType.LONG).setNullable(false),
                                      new DBColumn("code_p", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("codesMSROrdersRelation");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность codesMSRStudentStatusRelation

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("cdsmsrstdntsttsrltn_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("studentstatusid_p", DBType.LONG).setNullable(false),
                                      new DBColumn("code_p", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("codesMSRStudentStatusRelation");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность codesMSRToFormativeOURelation

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("cdsmsrtfrmtvourltn_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("facultyid_p", DBType.LONG).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("codesMSRToFormativeOURelation");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность mireaMSRSettings

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("mireamsrsettings_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("codehighschool_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("codesender_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("fileversion_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("mireaMSRSettings");

        }



    }
}
