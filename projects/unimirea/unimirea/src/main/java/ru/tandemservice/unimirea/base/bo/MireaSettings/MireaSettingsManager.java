/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.logic.FormativeOrgUnitHandler;
import ru.tandemservice.unimirea.base.bo.MireaSettings.logic.IMireaSettingsDao;
import ru.tandemservice.unimirea.base.bo.MireaSettings.logic.MireaSettingsDao;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
@Configuration
public class MireaSettingsManager extends BusinessObjectManager
{

    public static MireaSettingsManager instance() {return instance(MireaSettingsManager.class);}

    @Bean
    public IMireaSettingsDao dao()
    {
        return new MireaSettingsDao();
    }

    @Bean
    public IDefaultSearchDataSourceHandler createOrgUnitDataSource()
    {
        return new FormativeOrgUnitHandler(getName());
    }
}