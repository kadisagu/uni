/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
public interface IMireaSettingsDao
{
    Map<OrgUnit, String> getMapCodesForFormativeOrgUnit();

    void saveOrUpdateCodesForFormativeOrgUnit(Map<OrgUnit, String> mapCodes);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void generateNewStudentPersonalNumber();
}