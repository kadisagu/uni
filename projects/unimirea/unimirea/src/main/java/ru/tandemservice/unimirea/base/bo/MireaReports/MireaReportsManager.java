/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimirea.base.bo.MireaReports.logic.IMireaReportsDAO;
import ru.tandemservice.unimirea.base.bo.MireaReports.logic.MireaReportsDAO;

/**
 * @author Ekaterina Zvereva
 * @since 20.02.2015
 */
@Configuration
public class MireaReportsManager extends BusinessObjectManager
{
    public static MireaReportsManager instance() {return instance(MireaReportsManager.class);}

    @Bean
    public IMireaReportsDAO dao()
    {
        return new MireaReportsDAO();
    }
}