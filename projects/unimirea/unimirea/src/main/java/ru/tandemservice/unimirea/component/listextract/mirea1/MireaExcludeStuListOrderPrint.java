/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.mirea1;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.MoveStudentPrintModifierManager;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractUtil;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.gen.AbstractStudentExtractGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;
import ru.tandemservice.unimirea.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Ekaterina Zvereva
 * @since 14.02.2017
 */
public class MireaExcludeStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    public static final String SUBPARAGRAPH_LABEL = "SUBPARAGRAPHS";

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        ListStudentExtract firstExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, firstExtract);


        if (firstExtract != null)
        {
            EduProgramSubject programSubject = firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
            injectModifier.put("lastParNumber", String.valueOf(order.getParagraphCount() + 1));
            UniRtfUtil.initEducationType(injectModifier, firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");
            CommonExtractPrint.initProgramSubjectLabels(injectModifier, programSubject, null, "");
        }

        //orderText
        if (firstExtract instanceof MireaExcludeStuListExtract || firstExtract instanceof MireaExcludeSuccessStuListExtract)
            injectModifier.put("orderText", (String) firstExtract.getProperty("orderText"));

        if (null != order.getResponsibleEmpl())
        {
            OrgUnit orgUnit = order.getResponsibleEmpl().getOrgUnit();
            Post post = order.getResponsibleEmpl().getPostRelation().getPostBoundedWithQGandQL().getPost();
            String respFioA = CommonExtractUtil.getModifiedFio(order.getResponsibleEmpl().getPerson(), GrammaCase.ACCUSATIVE, true);

            injectModifier.put("controllerPost_A", null != post.getAccusativeCaseTitle() ? post.getAccusativeCaseTitle().toLowerCase() : post.getTitle().toLowerCase());
            injectModifier.put("controllerOrgUnit_G", null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getTitle());
            injectModifier.put("controllerFio_A", respFioA);
        } else
        {
            injectModifier.put("controllerPost_A", "");
            injectModifier.put("controllerOrgUnit_G", "");
            injectModifier.put("controllerFio_A", "______________________________________________________________________________________________________________________________.");
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, order);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }


    protected void injectParagraphs(RtfDocument document, StudentListOrder order)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            List<IRtfElement> parList = new ArrayList<>();
            // Получаем шаблон параграфа

            // Шаблон подпараграфа
            byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.MIREA_EXCLUDE_LIST_EXTRACT), 3);
            RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

            for (StudentListParagraph paragraph : (List<StudentListParagraph>) order.getParagraphList())
            {
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, paragraph.getFirstExtract().getType().getCode()), 1);
                RtfDocument template = new RtfReader().read(paragraphTemplate);
                // Вносим необходимые метки
                final ListStudentExtract firstExtract = (ListStudentExtract) paragraph.getFirstExtract();
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(firstExtract.getParagraph(), firstExtract);

                Student firstStudent = firstExtract.getEntity();
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "orderOrgUnit", "");
                paragraphInjectModifier.put("excludeDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(((IExcludeExtract)firstExtract).getExcludeDate()));
                UniRtfUtil.initEducationType(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getEducationLevelHighSchool(), "");
                paragraphInjectModifier.put("levelTypeStr_D", getDiplomaLevelName(firstStudent.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getSafeQCode()));
                paragraphInjectModifier.put("qualification", firstStudent.getEducationOrgUnit().getEducationLevelHighSchool().getQualificationTitleNullSafe().toLowerCase());

                customModifier.listParagraphModifier(paragraphInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);

                paragraphInjectModifier.modify(template);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(template);

                // Вставляем список подпараграфов
                injectSubParagraphs(template, paragraph, paragraphPart.getClone());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(template.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraphTemplate, StudentListParagraph paragraph, RtfDocument subParagraphTemplate)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraphTemplate, SUBPARAGRAPH_LABEL);
        final IMoveStudentInjectModifier customModifier = MoveStudentPrintModifierManager.instance().modifier();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<ListStudentExtract> extracts = (List<ListStudentExtract>) paragraph.getExtractList();
            Map<CompensationType, List<ListStudentExtract>> subParagraphsMap = new HashMap<>();
            extracts.stream().forEach(e->SafeMap.safeGet(subParagraphsMap, e.getEntity().getCompensationType(),
                                                              key->new ArrayList<>()).add(e));

            List<IRtfElement> parList = new ArrayList<>();
            List<CompensationType> keySet = subParagraphsMap.keySet().stream()
                    .sorted(Comparator.comparing(e ->e.getCode())).collect(Collectors.toList());

            int subParCount=0;
            for (CompensationType item: keySet)
            {
                RtfDocument subParagraph = subParagraphTemplate.getClone();

                // Вносим необходимые метки
                final ListStudentExtract firstExtract = subParagraphsMap.get(item).get(0);
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, firstExtract);
                CommonListExtractPrint.injectCompensationType(paragraphPartInjectModifier, firstExtract.getEntity().getCompensationType(), "", false);
                paragraphPartInjectModifier.put("subParNumber", String.valueOf(firstExtract.getParagraph().getNumber()) + "." + String.valueOf(++subParCount));
                paragraphPartInjectModifier.put("compensationTypeMirea", firstExtract.getEntity().getCompensationType().isBudget()?
                        "на местах, финансируемых за счет бюджетных ассигнований федерального бюджета"
                        : "на местах с оплатой стоимости обучения физическими и (или) юридическими лицами");

                List<Student> studentList = subParagraphsMap.get(item).stream().map(AbstractStudentExtractGen::getEntity).collect(Collectors.toList());
                ArrayList<String[]> resultList = new ArrayList<>();
                studentList.stream().sorted(Comparator.comparing((Student s) -> s.getGroup().getTitle()).thenComparing(Student.FULL_FIO_AND_ID_COMPARATOR))
                        .forEach(e-> resultList.add(new String[] {e.getFullFio(), e.getPersonalNumber(), e.getGroup().getTitle()}));


                customModifier.listParagraphModifier(paragraphPartInjectModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                paragraphPartInjectModifier.modify(subParagraph);

                final RtfTableModifier tableModifier = new RtfTableModifier();
                tableModifier.put("T", resultList.toArray(new String[resultList.size()][]));
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(subParagraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(subParagraph.getElementList());
                parList.add(rtfGroup);
            }

            ListStudentExtract firstExtract = extracts.get(0);
            if (firstExtract.getProperty("holidayFrom") != null)
            {
                // Шаблон подпараграфа
                byte[] subParHolidayTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.MIREA_EXCLUDE_LIST_EXTRACT), 4);
                RtfDocument paragraphPart = new RtfReader().read(subParHolidayTemplate);
                RtfInjectModifier modifier = new RtfInjectModifier();
                RtfTableModifier tableModifier = new RtfTableModifier();
                modifier.put("subParNumber", String.valueOf(firstExtract.getParagraph().getNumber()) + "." + String.valueOf(++subParCount));
                modifier.put("holidayFromStr", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format((Date) firstExtract.getProperty("holidayFrom")));
                modifier.put("holidayToStr", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format((Date) firstExtract.getProperty("holidayTo")));

                List<Student> studentList = extracts.stream().map(AbstractStudentExtractGen::getEntity).collect(Collectors.toList());
                ArrayList<String[]> resultList = new ArrayList<>();
                studentList.stream().sorted(Comparator.comparing((Student s) -> s.getGroup().getTitle()).thenComparing(Student.FULL_FIO_AND_ID_COMPARATOR))
                        .forEach(e-> resultList.add(new String[] {e.getFullFio(), e.getPersonalNumber(), e.getGroup().getTitle()}));

                modifier.modify(paragraphPart);
                tableModifier.put("T", resultList.toArray(new String[resultList.size()][]));
                customModifier.listParagraphTableModifier(tableModifier, (StudentListParagraph) firstExtract.getParagraph(), firstExtract);
                tableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);

            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static String getDiplomaLevelName(String qualificationCode)
    {
        switch (qualificationCode)
        {
            case QualificationsCodes.BAKALAVR:
                return "бакалавра";
            case QualificationsCodes.MAGISTR:
                return "магистра";
           default:
                return "специалиста";
        }
    }


}