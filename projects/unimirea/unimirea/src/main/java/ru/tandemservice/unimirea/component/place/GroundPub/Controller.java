/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.UniplacesComponents;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;


/**
 * @author Ekaterina Zvereva
 * @since 18.03.2015
 */
public class Controller extends ru.tandemservice.uniplaces.component.place.GroundPub.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareBuildingDataSource(component);
    }

    private void prepareBuildingDataSource(IBusinessComponent component)
    {
        Model model = (Model)getModel(component);
        if (model.getDataSourceBuilding() != null)
        {
            return;
        }

        DynamicListDataSource<UniplacesBuilding> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new PublisherLinkColumn("Номер", UniplacesBuilding.fullNumber().s()));
        dataSource.addColumn(new SimpleColumn("Идентификатор", UniplacesBuilding.identifier().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Корпус", UniplacesBuilding.unit().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Название", UniplacesBuilding.title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Адрес расположения", UniplacesBuilding.address().titleWithFlat().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Право распоряжения", UniplacesBuilding.disposalRight().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Площадь, кв. м", UniplacesBuilding.P_FRACTIONAL_AREA, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditBuilding").setPermissionKey("editBuilding_list"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteBuilding", "Удалить здание №{0}?", UniplacesBuilding.fullNumber().s()).setPermissionKey("deleteBuilding_list"));
        model.setDataSourceBuilding(dataSource);
    }


    public void onClickEditBuilding(IBusinessComponent component)
    {

        component.createDefaultChildRegion(new ComponentActivator(
                UniplacesComponents.BUILDING_ADD_EDIT, new ParametersMap().add("buildingId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteBuilding(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}