/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
public class MireaPlacesDAO extends UniBaseDao implements IMireaPlacesDAO
{

    @Override
    public boolean existRelatedObjects(Long placeId)
    {
        return existsEntity(UniPlacesPlaceExt.class,UniPlacesPlaceExt.L_PLACE_MIREA, placeId);
    }
}