/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundPub;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.component.place.GroundPub.Model;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.GroundPub.DAO
{

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesBuilding.class, "b")
                .where(eq(property("b", UniplacesBuilding.ground()), value(model.getGround())));
        UniBaseUtils.createPage(((ru.tandemservice.unimirea.component.place.GroundPub.Model)model).getDataSourceBuilding(), builder, getSession());
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        ((ru.tandemservice.unimirea.component.place.GroundPub.Model)model).setSecondRight(get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.registryRecord(), model.getGround()));
    }
}
