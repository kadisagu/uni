/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.ui.BuildingAndPlace;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimirea.base.bo.MireaReports.logic.IMireaReportsDAO;
import ru.tandemservice.unimirea.base.bo.MireaReports.logic.MireaPlacesReportBuilder;
import ru.tandemservice.unimirea.base.bo.MireaReports.logic.ReportDataWrapper;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 20.02.2015
 */
public class MireaReportsBuildingAndPlaceUI extends UIPresenter
{

    private ReportDataWrapper _reportData;
    private static final List<IEntity> AVALIABLE_STATUS_LIST = Arrays.asList(new IEntity[]{new IdentifiableWrapper<>(MireaReportsBuildingAndPlace.AVALIABLE_RENT_TRUE, "Да"), new IdentifiableWrapper<>(MireaReportsBuildingAndPlace.AVALIABLE_RENT_FALSE, "Нет")});
    private static final List<IEntity> STATUS_RENT_LIST = Arrays.asList(new IEntity[]{new IdentifiableWrapper<>(MireaReportsBuildingAndPlace.STATUS_RENT_FREE, "Свободно от аренды"), new IdentifiableWrapper<>(MireaReportsBuildingAndPlace.STATUS_RENT_BUSY, "Арендовано")});

    private ISelectModel _groundModel;

    public ISelectModel getGroundModel()
    {
        return _groundModel;
    }

    public void setGroundModel(ISelectModel groundModel)
    {
        _groundModel = groundModel;
    }

    public ReportDataWrapper getReportData()
    {
        return _reportData;
    }

    public void setReportData(ReportDataWrapper reportData)
    {
        _reportData = reportData;
    }


    public List<IEntity> getAvaliableRentList()
    {
        return AVALIABLE_STATUS_LIST;
    }


    public List<IEntity> getStateRentList()
    {
        return STATUS_RENT_LIST;
    }

    public void onClickCreateReport()
    {
        try
        {
            ByteArrayOutputStream out = new MireaPlacesReportBuilder().createReport(getReportData());
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("ReportBuildingPlace.xls").document(out), false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onClickClearSettings()
    {
        getReportData().clear();
    }

    @Override
    public void onComponentRefresh()
    {
        if (_reportData == null)
            _reportData = new ReportDataWrapper();
        setGroundModel(new LazySimpleSelectModel<>(UniplacesGround.class, UniplacesGround.P_FULL_TITLE)
                               .setSortProperty(UniplacesGround.P_FULL_TITLE).setSearchProperty(UniplacesGround.P_FULL_TITLE));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(MireaReportsBuildingAndPlace.BUILDINGS_DS))
            dataSource.put(IMireaReportsDAO.GROUNDS, getReportData().getGroundList());
        else if (dataSource.getName().equals(MireaReportsBuildingAndPlace.UNITS_DS))
            dataSource.put(IMireaReportsDAO.BUILDINGS, getReportData().getBuildingList());
        else if (dataSource.getName().equals(MireaReportsBuildingAndPlace.FLOORS_DS))
            dataSource.put(IMireaReportsDAO.UNITS, getReportData().getUnitList());
        else if (dataSource.getName().equals(MireaReportsBuildingAndPlace.PLACES_DS))
            dataSource.put(IMireaReportsDAO.FLOORS, getReportData().getFloorList());
        else if (dataSource.getName().equals(MireaReportsBuildingAndPlace.ROOMS_DS))
        {
            dataSource.put(IMireaReportsDAO.FLOORS, getReportData().getFloorList());
            dataSource.put(IMireaReportsDAO.PLACES, getReportData().getPlaceList());
        }
    }
}