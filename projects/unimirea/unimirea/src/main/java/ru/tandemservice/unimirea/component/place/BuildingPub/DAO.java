/* $Id$ */
package ru.tandemservice.unimirea.component.place.BuildingPub;

import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.component.place.BuildingPub.Model;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.BuildingPub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        ((ru.tandemservice.unimirea.component.place.BuildingPub.Model)model).setSecondRight(get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.registryRecord(), model.getBuilding()));
    }
}
