package ru.tandemservice.unimirea.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Второе право распоряжения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MireaSecondDisposalRightGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimirea.entity.MireaSecondDisposalRight";
    public static final String ENTITY_NAME = "mireaSecondDisposalRight";
    public static final int VERSION_HASH = -1385244789;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_RECORD = "registryRecord";
    public static final String L_DISPOSAL_RIGHT = "disposalRight";
    public static final String P_RIGHT_START_DATE = "rightStartDate";
    public static final String P_AREA = "area";
    public static final String L_DOCUMENT = "document";
    public static final String P_FRACTIONAL_AREA = "fractionalArea";

    private UniplacesRegistryRecord _registryRecord;     // Сущность, для которой создается второе право
    private UniplacesDisposalRight _disposalRight;     // Право распоряжения
    private Date _rightStartDate;     // Дата начала действия прав
    private Long _area;     // Площадь
    private UniplacesDocument _document;     // Документ, подтверждающий право

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сущность, для которой создается второе право. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniplacesRegistryRecord getRegistryRecord()
    {
        return _registryRecord;
    }

    /**
     * @param registryRecord Сущность, для которой создается второе право. Свойство не может быть null и должно быть уникальным.
     */
    public void setRegistryRecord(UniplacesRegistryRecord registryRecord)
    {
        dirty(_registryRecord, registryRecord);
        _registryRecord = registryRecord;
    }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     */
    @NotNull
    public UniplacesDisposalRight getDisposalRight()
    {
        return _disposalRight;
    }

    /**
     * @param disposalRight Право распоряжения. Свойство не может быть null.
     */
    public void setDisposalRight(UniplacesDisposalRight disposalRight)
    {
        dirty(_disposalRight, disposalRight);
        _disposalRight = disposalRight;
    }

    /**
     * @return Дата начала действия прав.
     */
    public Date getRightStartDate()
    {
        return _rightStartDate;
    }

    /**
     * @param rightStartDate Дата начала действия прав.
     */
    public void setRightStartDate(Date rightStartDate)
    {
        dirty(_rightStartDate, rightStartDate);
        _rightStartDate = rightStartDate;
    }

    /**
     * @return Площадь.
     */
    public Long getArea()
    {
        return _area;
    }

    /**
     * @param area Площадь.
     */
    public void setArea(Long area)
    {
        dirty(_area, area);
        _area = area;
    }

    /**
     * @return Документ, подтверждающий право.
     */
    public UniplacesDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ, подтверждающий право.
     */
    public void setDocument(UniplacesDocument document)
    {
        dirty(_document, document);
        _document = document;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MireaSecondDisposalRightGen)
        {
            setRegistryRecord(((MireaSecondDisposalRight)another).getRegistryRecord());
            setDisposalRight(((MireaSecondDisposalRight)another).getDisposalRight());
            setRightStartDate(((MireaSecondDisposalRight)another).getRightStartDate());
            setArea(((MireaSecondDisposalRight)another).getArea());
            setDocument(((MireaSecondDisposalRight)another).getDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MireaSecondDisposalRightGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MireaSecondDisposalRight.class;
        }

        public T newInstance()
        {
            return (T) new MireaSecondDisposalRight();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryRecord":
                    return obj.getRegistryRecord();
                case "disposalRight":
                    return obj.getDisposalRight();
                case "rightStartDate":
                    return obj.getRightStartDate();
                case "area":
                    return obj.getArea();
                case "document":
                    return obj.getDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryRecord":
                    obj.setRegistryRecord((UniplacesRegistryRecord) value);
                    return;
                case "disposalRight":
                    obj.setDisposalRight((UniplacesDisposalRight) value);
                    return;
                case "rightStartDate":
                    obj.setRightStartDate((Date) value);
                    return;
                case "area":
                    obj.setArea((Long) value);
                    return;
                case "document":
                    obj.setDocument((UniplacesDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryRecord":
                        return true;
                case "disposalRight":
                        return true;
                case "rightStartDate":
                        return true;
                case "area":
                        return true;
                case "document":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryRecord":
                    return true;
                case "disposalRight":
                    return true;
                case "rightStartDate":
                    return true;
                case "area":
                    return true;
                case "document":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryRecord":
                    return UniplacesRegistryRecord.class;
                case "disposalRight":
                    return UniplacesDisposalRight.class;
                case "rightStartDate":
                    return Date.class;
                case "area":
                    return Long.class;
                case "document":
                    return UniplacesDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MireaSecondDisposalRight> _dslPath = new Path<MireaSecondDisposalRight>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MireaSecondDisposalRight");
    }
            

    /**
     * @return Сущность, для которой создается второе право. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getRegistryRecord()
     */
    public static UniplacesRegistryRecord.Path<UniplacesRegistryRecord> registryRecord()
    {
        return _dslPath.registryRecord();
    }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getDisposalRight()
     */
    public static UniplacesDisposalRight.Path<UniplacesDisposalRight> disposalRight()
    {
        return _dslPath.disposalRight();
    }

    /**
     * @return Дата начала действия прав.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getRightStartDate()
     */
    public static PropertyPath<Date> rightStartDate()
    {
        return _dslPath.rightStartDate();
    }

    /**
     * @return Площадь.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getArea()
     */
    public static PropertyPath<Long> area()
    {
        return _dslPath.area();
    }

    /**
     * @return Документ, подтверждающий право.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getDocument()
     */
    public static UniplacesDocument.Path<UniplacesDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getFractionalArea()
     */
    public static SupportedPropertyPath<Double> fractionalArea()
    {
        return _dslPath.fractionalArea();
    }

    public static class Path<E extends MireaSecondDisposalRight> extends EntityPath<E>
    {
        private UniplacesRegistryRecord.Path<UniplacesRegistryRecord> _registryRecord;
        private UniplacesDisposalRight.Path<UniplacesDisposalRight> _disposalRight;
        private PropertyPath<Date> _rightStartDate;
        private PropertyPath<Long> _area;
        private UniplacesDocument.Path<UniplacesDocument> _document;
        private SupportedPropertyPath<Double> _fractionalArea;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сущность, для которой создается второе право. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getRegistryRecord()
     */
        public UniplacesRegistryRecord.Path<UniplacesRegistryRecord> registryRecord()
        {
            if(_registryRecord == null )
                _registryRecord = new UniplacesRegistryRecord.Path<UniplacesRegistryRecord>(L_REGISTRY_RECORD, this);
            return _registryRecord;
        }

    /**
     * @return Право распоряжения. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getDisposalRight()
     */
        public UniplacesDisposalRight.Path<UniplacesDisposalRight> disposalRight()
        {
            if(_disposalRight == null )
                _disposalRight = new UniplacesDisposalRight.Path<UniplacesDisposalRight>(L_DISPOSAL_RIGHT, this);
            return _disposalRight;
        }

    /**
     * @return Дата начала действия прав.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getRightStartDate()
     */
        public PropertyPath<Date> rightStartDate()
        {
            if(_rightStartDate == null )
                _rightStartDate = new PropertyPath<Date>(MireaSecondDisposalRightGen.P_RIGHT_START_DATE, this);
            return _rightStartDate;
        }

    /**
     * @return Площадь.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getArea()
     */
        public PropertyPath<Long> area()
        {
            if(_area == null )
                _area = new PropertyPath<Long>(MireaSecondDisposalRightGen.P_AREA, this);
            return _area;
        }

    /**
     * @return Документ, подтверждающий право.
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getDocument()
     */
        public UniplacesDocument.Path<UniplacesDocument> document()
        {
            if(_document == null )
                _document = new UniplacesDocument.Path<UniplacesDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimirea.entity.MireaSecondDisposalRight#getFractionalArea()
     */
        public SupportedPropertyPath<Double> fractionalArea()
        {
            if(_fractionalArea == null )
                _fractionalArea = new SupportedPropertyPath<Double>(MireaSecondDisposalRightGen.P_FRACTIONAL_AREA, this);
            return _fractionalArea;
        }

        public Class getEntityClass()
        {
            return MireaSecondDisposalRight.class;
        }

        public String getEntityName()
        {
            return "mireaSecondDisposalRight";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getFractionalArea();
}
