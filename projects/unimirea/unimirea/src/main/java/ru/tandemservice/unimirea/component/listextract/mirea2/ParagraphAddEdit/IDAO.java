/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<MireaExcludeSuccessStuListExtract, Model>
{
}
