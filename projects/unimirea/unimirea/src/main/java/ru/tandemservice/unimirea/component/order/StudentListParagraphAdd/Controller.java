/* $Id$ */
package ru.tandemservice.unimirea.component.order.StudentListParagraphAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.component.order.list.StudentListParagraphAdd.Model;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dseleznev on 06.06.2016.
 */
public class Controller extends ru.tandemservice.movestudent.component.order.list.StudentListParagraphAdd.Controller
{
    @Override
    public void onChangeParagraphType(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(model.getOrder().getType().getCode().equals(StudentExtractTypeCodes.ADMIT_TO_PASS_STATE_ATTESTATION_LIST_ORDER)) //TODO rough hack for mirea
        {
            Set<String> extractTypesSet = new HashSet<>();
            List<AbstractStudentExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(model.getOrder(), true);
            for (AbstractStudentExtract extract : extracts)
            {
                extractTypesSet.add(extract.getType().getCode());
            }
            if (extractTypesSet.size() > 1 || (extractTypesSet.size() > 0 && !extractTypesSet.contains(model.getType().getCode())))
                throw new ApplicationException("В один приказ о допуске к государственной итоговой аттестации могут быть включены только параграфы одного типа");
        }


        super.onChangeParagraphType(component);
    }
}