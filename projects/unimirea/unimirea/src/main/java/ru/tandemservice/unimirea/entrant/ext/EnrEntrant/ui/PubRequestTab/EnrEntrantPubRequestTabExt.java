/* $Id$ */
package ru.tandemservice.unimirea.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTab;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unimirea.request.ext.EnrEntrantRequest.ui.ActionsAddon.MireaEntrantRequestActionCoverPrint;
import ru.tandemservice.unimirea.request.ext.EnrEntrantRequest.ui.ActionsAddon.MireaEntrantRequestActionsAddon;

/**
 * @author Ekaterina Zvereva
 * @since 30.09.2015
 */
@Configuration
public class EnrEntrantPubRequestTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "mireaEnrEntrantPubRequestTabExt";
    @Autowired
    private EnrEntrantPubRequestTab _requestPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_requestPub.presenterExtPoint())
                .replaceAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, MireaEntrantRequestActionsAddon.class))
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantPubRequestTabExtUI.class))
                .addAction(new MireaEntrantRequestActionCoverPrint("onClickPrintCover"))
                .create();
    }
}