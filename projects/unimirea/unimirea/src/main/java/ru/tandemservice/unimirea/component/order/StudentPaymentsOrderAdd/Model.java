/* $Id$ */
package ru.tandemservice.unimirea.component.order.StudentPaymentsOrderAdd;

import ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt;

/**
 * @author Andrey Avetisov
 * @since 26.02.2016
 */
public class Model extends ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd.Model
{
    private MireaStudentPaymentsOrderExt _paymentsOrderExt = new MireaStudentPaymentsOrderExt();

    public MireaStudentPaymentsOrderExt getPaymentsOrderExt()
    {
        return _paymentsOrderExt;
    }

    public void setPaymentsOrderExt(MireaStudentPaymentsOrderExt paymentsOrderExt)
    {
        _paymentsOrderExt = paymentsOrderExt;
    }
}
