/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimirea.entity.OrgUnitCodeForStudentNumber;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
public class MireaSettingsDao extends UniBaseDao implements IMireaSettingsDao
{

    @Override
    public Map<OrgUnit, String> getMapCodesForFormativeOrgUnit()
    {
        List<OrgUnitCodeForStudentNumber> list = getList(OrgUnitCodeForStudentNumber.class);
        Map<OrgUnit, String> result = new HashMap<>(list.size());
        for(OrgUnitCodeForStudentNumber item : list)
        {
            result.put(item.getOrgUnit(), item.getCode());
        }

        return result;
    }

    @Override
    public void saveOrUpdateCodesForFormativeOrgUnit(Map<OrgUnit, String> mapCodes)
    {
        new DQLDeleteBuilder(OrgUnitCodeForStudentNumber.class).createStatement(getSession()).execute();

        for (Map.Entry<OrgUnit, String> item : mapCodes.entrySet())
        {
            if (item.getValue() == null) continue;
            OrgUnitCodeForStudentNumber code = new OrgUnitCodeForStudentNumber();
            code.setOrgUnit(item.getKey());
            code.setCode(item.getValue());
            saveOrUpdate(code);
        }
    }

    @Override
    public void generateNewStudentPersonalNumber()
    {
        List<Student> studentList = getList(Student.class, Student.P_ID);
        for (Student student : studentList)
        {
            String newPersonNumber = IStudentDAO.instance.get().generateNewPersonalNumber(student);
            if (newPersonNumber == null)
                throw new IllegalArgumentException("Для " + student.getEntranceYear() + " года не осталось свободных персональных номеров.");
            student.setPersonalNumber(newPersonNumber);
            update(student);
            getSession().flush();

        }


    }
}