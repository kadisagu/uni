/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class Controller extends AbstractListParagraphAddEditController<MireaExcludeSuccessStuListExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }
}
