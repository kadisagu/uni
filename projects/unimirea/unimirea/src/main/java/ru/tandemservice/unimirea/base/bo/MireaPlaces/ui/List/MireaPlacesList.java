/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.List;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.MireaPlacesManager;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.Pub.MireaPlacesPub;
import ru.tandemservice.unimirea.entity.UniMireaPlace;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
@Configuration
public class MireaPlacesList extends BusinessComponentManager
{
    public static final String PLACES_DS = "placesSearchListDS";
    public static final String BUILDINGS_DS ="buildingDS";
    public static final String UNITS_DS ="unitDS";
    public static final String FLOORS_DS ="floorDS";



    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PLACES_DS, placesDSColumn(), placesDSHandler()))
                .addDataSource(selectDS(BUILDINGS_DS, buildingDSHandler()))
                .addDataSource(selectDS(UNITS_DS, unitsDSHandler()))
                .addDataSource(selectDS(FLOORS_DS, floorDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint placesDSColumn()
    {
        return columnListExtPointBuilder(PLACES_DS)
                .addColumn(publisherColumn("name", UniMireaPlace.title()).businessComponent(MireaPlacesPub.class).order())
                .addColumn(textColumn("number", UniMireaPlace.number()))
                .addColumn(textColumn("floor", UniMireaPlace.floor().title()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onEditPlaceClick").permissionKey("mireaPlaceEdit"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeletePlaceClick")
                                   .alert("message:placesSearchListDS.delete.alert")
                                   .permissionKey("mireaPlaceDelete"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> placesDSHandler() {
        return MireaPlacesManager.instance().createPlacesDataSource();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> buildingDSHandler()
    {
        return MireaPlacesManager.instance().createBuildingHandler();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> floorDSHandler()
    {
        return MireaPlacesManager.instance().createFloorHandler();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> unitsDSHandler()
    {
        return MireaPlacesManager.instance().createUnitHandler();
    }

}