/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import com.google.common.collect.Maps;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.*;

import java.util.Map;
import java.util.Set;

/**
 * @author Ekaterina Zvereva
 * @since 25.02.2015
 */
public class ReportInfo
{
    private Map<UniplacesGround, Set<UniplacesBuilding>> _groundToBuildingMap = Maps.newHashMap();
    private Map<UniplacesBuilding, Set<UniplacesUnit>> _buildingToUnitMap = Maps.newHashMap();
    private Map<UniplacesUnit, Set<UniplacesFloor>> _unitToFloorMap = Maps.newHashMap();
    private Map<UniplacesFloor, Set<UniMireaPlace>> _floorToPlaceMap = Maps.newHashMap();
    private Map<UniplacesFloor, Set<UniplacesPlace>> _floorToRoomMap = Maps.newHashMap();
    private Map<UniMireaPlace, Set<UniplacesPlace>> _placeToRoomMap = Maps.newHashMap();
    private Map<UniplacesPlace, UniPlacesPlaceExt> _roomToExtMap = Maps.newHashMap();

    public ReportInfo(Map<UniplacesGround, Set<UniplacesBuilding>> groundToBuildingMap, Map<UniplacesBuilding, Set<UniplacesUnit>> buildingToUnitMap,
                      Map<UniplacesUnit, Set<UniplacesFloor>> unitToFloorMap, Map<UniplacesFloor, Set<UniMireaPlace>> floorToPlaceMap, Map<UniplacesFloor, Set<UniplacesPlace>> floorToRoomMap,
                      Map<UniMireaPlace, Set<UniplacesPlace>> placeToRoomMap, Map<UniplacesPlace, UniPlacesPlaceExt> roomToExtMap)
    {
        _groundToBuildingMap = groundToBuildingMap;
        _buildingToUnitMap = buildingToUnitMap;
        _unitToFloorMap = unitToFloorMap;
        _floorToPlaceMap = floorToPlaceMap;
        _floorToRoomMap = floorToRoomMap;
        _placeToRoomMap = placeToRoomMap;
        _roomToExtMap = roomToExtMap;
    }

    public Map<UniplacesGround, Set<UniplacesBuilding>> getGroundToBuildingMap()
    {
        return _groundToBuildingMap;
    }

    public Map<UniplacesBuilding, Set<UniplacesUnit>> getBuildingToUnitMap()
    {
        return _buildingToUnitMap;
    }

    public Map<UniplacesUnit, Set<UniplacesFloor>> getUnitToFloorMap()
    {
        return _unitToFloorMap;
    }

    public Map<UniplacesFloor, Set<UniMireaPlace>> getFloorToPlaceMap()
    {
        return _floorToPlaceMap;
    }

    public Map<UniplacesFloor, Set<UniplacesPlace>> getFloorToRoomMap()
    {
        return _floorToRoomMap;
    }

    public Map<UniMireaPlace, Set<UniplacesPlace>> getPlaceToRoomMap()
    {
        return _placeToRoomMap;
    }

    public Map<UniplacesPlace, UniPlacesPlaceExt> getRoomToExtMap()
    {
        return _roomToExtMap;
    }

}