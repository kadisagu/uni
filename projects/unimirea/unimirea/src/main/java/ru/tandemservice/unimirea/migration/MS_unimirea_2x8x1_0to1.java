package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimirea_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
		};
    }

//    @Override
//    public ScriptDependency[] getAfterDependencies()
//    {
//        return new ScriptDependency[] {new ScriptDependency("movestudent", "2.8.1", 1)};
//    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        //TODO перенести настройки в продуктовые сущности прежде, чем удалять
		////////////////////////////////////////////////////////////////////////////////
		// сущность mireaMSRSettings
        {
            Statement requestStatement = tool.getConnection().createStatement();
            requestStatement.execute("select codehighschool_p, codesender_p, fileversion_p, userscriptexportstudent_p from mireamsrsettings_t");

            ResultSet requestResult = requestStatement.getResultSet();
            short entityCode = tool.entityCodes().get("exportMSRSettings");

            PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into exportmsrsettings_t (id, discriminator, codehighschool_p, codesender_p, fileversion_p, userscriptexportstudent_p) values (?, ?, ?, ?, ?,?)");
            insertRelSt.setShort(2, entityCode);

            while (requestResult.next())
            {
                insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insertRelSt.setString(3, requestResult.getString(1));
                insertRelSt.setString(4, requestResult.getString(2));
                insertRelSt.setString(5, requestResult.getString(3));
                insertRelSt.setString(6, requestResult.getString(4));
                insertRelSt.execute();

            }
        }
		// сущность была удалена
		{

			// удалить таблицу
			tool.dropTable("mireamsrsettings_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("mireaMSRSettings");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность counterMSRExportFile
        {
            Statement requestStatement = tool.getConnection().createStatement();
            requestStatement.execute("select currentDate_p, counter_p from countermsrexportfile_t");

            ResultSet requestResult = requestStatement.getResultSet();
            short entityCode = tool.entityCodes().get("counterMSRForExportFile");

            PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into countermsrforexportfile_t (id, discriminator, currentDate_p, counter_p) values (?, ?, ?, ?)");
            insertRelSt.setShort(2, entityCode);

            while (requestResult.next())
            {
                insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insertRelSt.setDate(3, requestResult.getDate(1));
                insertRelSt.setInt(4, requestResult.getInt(2));

                insertRelSt.execute();

            }
        }
		// сущность была удалена
		{

			// удалить таблицу
			tool.dropTable("countermsrexportfile_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("counterMSRExportFile");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность codesMSRToFormativeOURelation
        {
            Statement requestStatement = tool.getConnection().createStatement();
            requestStatement.execute("select facultyid_p, code_p from cdsmsrtfrmtvourltn_t");

            ResultSet requestResult = requestStatement.getResultSet();
            short entityCode = tool.entityCodes().get("codesMSRForFormativeOrgUnit");

            PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into codesmsrforformativeorgunit_t (id, discriminator, orgunit_id, code_p) values (?, ?, ?, ?)");
            insertRelSt.setShort(2, entityCode);

            while (requestResult.next())
            {
                insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insertRelSt.setLong(3, requestResult.getLong(1));
                insertRelSt.setString(4, requestResult.getString(2));

                insertRelSt.execute();

            }
        }
		// сущность была удалена
		{

			// удалить таблицу
			tool.dropTable("cdsmsrtfrmtvourltn_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("codesMSRToFormativeOURelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность codesMSRStudentStatusRelation
        {
            Statement requestStatement = tool.getConnection().createStatement();
            requestStatement.execute("select studentstatusid_p, code_p from cdsmsrstdntsttsrltn_t");

            ResultSet requestResult = requestStatement.getResultSet();
            short entityCode = tool.entityCodes().get("codesMSRForStudentStatus");

            PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into codesmsrforstudentstatus_t (id, discriminator, studentstatus_id, code_p) values (?, ?, ?, ?)");
            insertRelSt.setShort(2, entityCode);

            while (requestResult.next())
            {
                insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insertRelSt.setLong(3, requestResult.getLong(1));
                insertRelSt.setLong(4, requestResult.getLong(2));

                insertRelSt.execute();

            }
        }
		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("cdsmsrstdntsttsrltn_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("codesMSRStudentStatusRelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность codesMSROrdersRelation
        {
            Statement requestStatement = tool.getConnection().createStatement();
            requestStatement.execute("select extracttype_p, code_p from codesmsrordersrelation_t");

            ResultSet requestResult = requestStatement.getResultSet();
            short entityCode = tool.entityCodes().get("codesMSRForStudentOrders");

            PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into codesmsrforstudentorders_t (id, discriminator, extracttype_id, code_p) values (?, ?, ?, ?)");
            insertRelSt.setShort(2, entityCode);

            while (requestResult.next())
            {
                insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insertRelSt.setLong(3, requestResult.getLong(1));
                insertRelSt.setInt(4, (int) requestResult.getLong(2));

                insertRelSt.execute();

            }
        }
		// сущность была удалена
		{

			// удалить таблицу
			tool.dropTable("codesmsrordersrelation_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("codesMSROrdersRelation");

		}


    }
}
