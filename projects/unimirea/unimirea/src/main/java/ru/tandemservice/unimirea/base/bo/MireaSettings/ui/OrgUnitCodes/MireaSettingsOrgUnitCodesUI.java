/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaSettings.ui.OrgUnitCodes;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MovestudentMSRSettings.ui.CodesGupMsr.MovestudentMSRSettingsCodesGupMsr;
import ru.tandemservice.unimirea.base.bo.MireaSettings.MireaSettingsManager;
import org.apache.tapestry.form.validator.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
public class MireaSettingsOrgUnitCodesUI extends UIPresenter
{

    private Map<OrgUnit, String> _codesMap;
    final Pattern pattern = Pattern.compile("^[А-ЯЁ]");

    public Map<OrgUnit, String> getCodesMap()
    {
        return _codesMap;
    }

    public void setCodesMap(Map<OrgUnit, String> codesMap)
    {
        _codesMap = codesMap;
    }

    public String getOrgUnitCodeId()
    {
        return "codeId_" + getConfig().getDataSource(MovestudentMSRSettingsCodesGupMsr.FORMATIVE_ORG_UNIT_DS).getCurrentId();
    }

    public void onClickApply()
    {
        MireaSettingsManager.instance().dao().saveOrUpdateCodesForFormativeOrgUnit(_codesMap);
        deactivate();

    }

    public String getCurrentOrgUnitCode()
    {
        OrgUnit current = getConfig().getDataSource(MovestudentMSRSettingsCodesGupMsr.FORMATIVE_ORG_UNIT_DS).getCurrent();
        String code = _codesMap.get(current);
        return code;
    }

    public void setCurrentOrgUnitCode(String code)
    {
        OrgUnit current = getConfig().getDataSource(MovestudentMSRSettingsCodesGupMsr.FORMATIVE_ORG_UNIT_DS).getCurrent();
        _codesMap.put(current, code);
    }

    @Override
    public void onComponentRefresh()
    {
        _codesMap = MireaSettingsManager.instance().dao().getMapCodesForFormativeOrgUnit();
    }

    public BaseValidator getValidator()
    {
        return  new BaseValidator()
        {
            public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
            {
                String input = (String)object;
                if (input != null && !pattern.matcher(input).matches()) {
                    throw new ValidatorException("Код может содержать только один заглавный кириллический символ.");
                }
            }
        };

    }


}