/* $Id$ */
package ru.tandemservice.unimirea.component.place.FloorPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.Pub.MireaPlacesPub;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.Collection;

/**
 * @author Ekaterina Zvereva
 * @since 20.03.2015
 */
public class Controller extends ru.tandemservice.uniplaces.component.place.FloorPub.Controller
{

    @Override
    public void prepareDataSource(IBusinessComponent component)
    {
        super.prepareDataSource(component);
        DynamicListDataSource<UniplacesPlace> dataSource = getModel(component).getDataSource();
        if (dataSource.getColumn("place") == null)
        {
            DefaultPublisherLinkResolver resolverPlace = new DefaultPublisherLinkResolver()
            {
                @Override
                public Object getParameters(IEntity entity)
                {
                    UniMireaPlace o = ((UniPlacesPlaceExt) ((ViewWrapper<UniplacesPlace>) entity).getViewProperty(DAO.PLACE_EXT)).getPlaceMirea();

                    if (o != null)
                        return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, o.getId());
                    else
                        return null;
                }

                @Override
                public String getComponentName(IEntity entity)
                {
                    UniMireaPlace o = ((UniPlacesPlaceExt) ((ViewWrapper<UniplacesPlace>) entity).getViewProperty(DAO.PLACE_EXT)).getPlaceMirea();
                    if (o != null)
                        return MireaPlacesPub.class.getSimpleName();
                    else return null;
                }
            };

            HeadColumn headColumn = new HeadColumn("place", "Помещение");
            headColumn.setHeaderAlign("center");
            headColumn.addColumn(new PublisherLinkColumn("Название", DAO.PLACE_EXT + "." + UniPlacesPlaceExt.placeMirea().title()).setResolver(resolverPlace).setWidth("150px"));
            headColumn.addColumn(new SimpleColumn("Номер", DAO.PLACE_EXT +"."+ UniPlacesPlaceExt.placeMirea().number()).setClickable(false).setWidth("90px"));
            dataSource.addColumn(headColumn, 3);
        }


    }
}
