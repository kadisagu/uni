/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class MireaFloorHandler extends DefaultComboDataSourceHandler
{
    public MireaFloorHandler(String ownerId)
    {
        super(ownerId, UniplacesFloor.class, UniplacesFloor.P_TITLE);
        setOrderByProperty(UniplacesFloor.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Object unit = ep.context.get(IMireaPlacesDAO.UNIT);
        Object building = ep.context.get(IMireaPlacesDAO.BUILDING);

        if (unit != null)
            ep.dqlBuilder.where(eq(property(UniplacesFloor.unit().fromAlias("e")), commonValue(unit)));
        else if (building != null)
            ep.dqlBuilder.where(eq(property(UniplacesFloor.unit().building().fromAlias("e")), commonValue(building)));
    }
}