/*$Id$*/
package ru.tandemservice.unimirea.component.listextract.e24;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.common.base.entity.IPersistentPerson;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractUtil;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.DegreeToExtractRelation;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.movestudent.entity.StatusToExtractRelation;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2014
 */
public class SetDiplomaWorkTopicAndScientificAdviserStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<SetDiplomaWorkTopicAndScientificAdviserStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        List<DegreeToExtractRelation> degreeToExtractRelationList = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(DegreeToExtractRelation.class, "y").column(DQLExpressions.property("y"));
            dql.where(DQLExpressions.eq(DQLExpressions.property(DegreeToExtractRelation.extract().paragraph().order().id().fromAlias("y")), DQLExpressions.value(order.getId())));
            return dql.createStatement(session).list();
        });

        Map<Long, List<ScienceDegree>> scDegreeListMap = new HashMap<>(extracts.size());
        for (DegreeToExtractRelation rel : degreeToExtractRelationList)
        {
            List<ScienceDegree> scDegreeList = scDegreeListMap.get(rel.getExtract().getId());
            if (null == scDegreeList) scDegreeList = new ArrayList<>();
            scDegreeList.add(rel.getDegree());
            scDegreeListMap.put(rel.getExtract().getId(), scDegreeList);
        }

        List<StatusToExtractRelation> statusToExtractRelationList = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(StatusToExtractRelation.class, "y").column(DQLExpressions.property("y"));
            dql.where(DQLExpressions.eq(DQLExpressions.property(StatusToExtractRelation.extract().paragraph().order().id().fromAlias("y")), DQLExpressions.value(order.getId())));
            return dql.createStatement(session).list();
        });

        Map<Long, List<ScienceStatus>> scStatusListMap = new HashMap<>(extracts.size());
        for (StatusToExtractRelation rel : statusToExtractRelationList)
        {
            List<ScienceStatus> scStatusList = scStatusListMap.get(rel.getExtract().getId());
            if (null == scStatusList) scStatusList = new ArrayList<>();
            scStatusList.add(rel.getStatus());
            scStatusListMap.put(rel.getExtract().getId(), scStatusList);
        }


        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts, scDegreeListMap, scStatusListMap);
        Collections.sort(preparedParagraphsStructure);

        if (!extracts.isEmpty())
        {
            injectModifier.put("postParNum1", String.valueOf(preparedParagraphsStructure.size() + 1));
            injectModifier.put("postParNum2", String.valueOf(preparedParagraphsStructure.size() + 2));

            Set<OrgUnit> ouSet = new HashSet<>();
            for (SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper wrapper : preparedParagraphsStructure)
                ouSet.addAll(wrapper.getCathedraList());

            if (ouSet.isEmpty())
            {
                injectModifier.put("cathedraGList", "");
                injectModifier.put("cathedraHeadDList", "");
            } else
            {
                StringBuilder cathedraBuilder = new StringBuilder();
                StringBuilder headBuilder = new StringBuilder();

                for (OrgUnit ou : ouSet)
                {
                    cathedraBuilder.append(cathedraBuilder.length() > 0 ? ", " : "").append(null != ou.getGenitiveCaseTitle() ? ou.getGenitiveCaseTitle() : ou.getTitle());
                    if (null != ou.getHead())
                    {
                        IPersistentPerson head = ou.getHead().getEmployee().getPerson();
                        String headFioD = CommonExtractUtil.getModifiedFio((Person) head, GrammaCase.DATIVE, true);
                        headBuilder.append(headBuilder.length() > 0 ? ", " : "").append(headFioD);
                    }
                }

                injectModifier.put("cathedraGList", cathedraBuilder.toString());
                injectModifier.put("cathedraHeadDList", headBuilder.toString());
            }
        }

        if (null != order.getResponsibleEmpl())
        {
            OrgUnit orgUnit = order.getResponsibleEmpl().getOrgUnit();
            Post post = order.getResponsibleEmpl().getPostRelation().getPostBoundedWithQGandQL().getPost();
            String respFioA = CommonExtractUtil.getModifiedFio(order.getResponsibleEmpl().getPerson(), GrammaCase.ACCUSATIVE, true);

            injectModifier.put("controllerPost_A", null != post.getAccusativeCaseTitle() ? post.getAccusativeCaseTitle() : post.getTitle());
            injectModifier.put("controllerOrgUnit_G", null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getTitle());
            injectModifier.put("controllerFio_G", respFioA);
        } else
        {
            injectModifier.put("controllerPost_A", "");
            injectModifier.put("controllerOrgUnit_G", "");
            injectModifier.put("controllerFio_G", "____________________________________________________________________________________________________________________________________________________________________________________________.");
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);
        injectApplications(document, order, preparedParagraphsStructure);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper> prepareParagraphsStructure(
            List<SetDiplomaWorkTopicAndScientificAdviserStuListExtract> extracts,
            Map<Long, List<ScienceDegree>> scDegreeListMap,
            Map<Long, List<ScienceStatus>> scStatusListMap)
    {
        int ind;
        List<SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper> paragraphWrapperList = new ArrayList<>();

        if (extracts.isEmpty()) return paragraphWrapperList;

        for (SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract : extracts)
        {
            EducationOrgUnit educationOrgUnitOld = extract.getEntity().getEducationOrgUnit();

            /*EducationLevels eduLevelsNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();
            Qualifications qualification = eduLevelsNew.getQualification();

            if (null == qualification)
            {
                EducationLevels tmpLevel = eduLevelsNew.getParentLevel();
                while (null != tmpLevel && null == qualification)
                {
                    qualification = tmpLevel.getQualification();
                    tmpLevel = tmpLevel.getParentLevel();
                }
            }    */

            SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper paragraphWrapper = new SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper(
                    extract.getEntity().getCourse(),
                    extract.getEntity().getGroup(),
                    educationOrgUnitOld.getEducationLevelHighSchool(),
                    educationOrgUnitOld.getFormativeOrgUnit(),
                    educationOrgUnitOld.getDevelopForm());

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1) paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getStudentsList().add(new SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper(extract, scDegreeListMap, scStatusListMap));
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SET_DIPLOMA_WORK_TOPIC_AND_SCIENTIFIC_ADVISER_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_GF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_GF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_GF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_GF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_GF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                Student firstStudent = paragraphWrapper.getFirstExtract().getEntity();

                /*CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, firstStudent.getCompensationType(), "", false);*/
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "formativeOrgUnit", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                //CommonExtractPrint.initGroup(paragraphInjectModifier, "", paragraphWrapper.getGroup(), paragraphWrapper.getDevelopForm());
                UniRtfUtil.initEducationType(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), "");
                //CommonExtractPrint.initEducationType(paragraphInjectModifier, paragraphWrapper.getFirstExtract().getEducationOrgUnitOld(), "Old");
                addEducationLevelData(paragraphWrapper.getEducationLevels().getEducationLevel(), paragraphInjectModifier);

                // Получаем список студентов
/*                List<Person> personList = paragraphWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;
                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }
                // Вставляем список студентов
                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());
                paragraphInjectModifier.put("STUDENT_LIST", rtfString);*/
                paragraphInjectModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);

            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectApplications(RtfDocument document, StudentListOrder order, List<SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "APPLICATIONS");

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (SetDiplomaWorkTopicAndScientificAdviserParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.SET_DIPLOMA_WORK_TOPIC_AND_SCIENTIFIC_ADVISER_LIST_EXTRACT), 3);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());
                paragraphInjectModifier.put("orderDate", null != order.getCommitDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) : "_________");
                paragraphInjectModifier.put("orderNumber", null != order.getNumber() ? order.getNumber() : "_________");

                parNumber++;
                paragraphInjectModifier.put("appNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("appNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                Student firstStudent = paragraphWrapper.getFirstExtract().getEntity();
                OrgUnit orgUnit = firstStudent.getEducationOrgUnit().getFormativeOrgUnit();
                RtfTableModifier tableModifier = new RtfTableModifier();
                if (null != orgUnit.getHead())
                {
                    StringBuilder postStr = new StringBuilder();
                    IPersistentIdentityCard identityCard = orgUnit.getHead().getEmployee().getPerson().getIdentityCard();
                    List<EmployeePostPossibleVisa> possibleVisa = DataAccessServices.dao().getList(EmployeePostPossibleVisa.class, EmployeePostPossibleVisa.entity().s(), orgUnit.getHead());
                    if (possibleVisa.size() == 1)
                    {
                        postStr.append(possibleVisa.get(0).getTitle());
                        identityCard = possibleVisa.get(0).getEntity().getPerson().getIdentityCard();
                    } else
                    {
                        postStr.append(((EmployeePost) orgUnit.getHead()).getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
                        postStr.append(" ").append(null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getTitle());
                    }

                    String lastName = identityCard.getLastName();
                    String firstName = identityCard.getFirstName();
                    String middleName = identityCard.getMiddleName();

                    StringBuilder fioStr = new StringBuilder();
                    if (StringUtils.isNotEmpty(firstName))
                        fioStr.append(firstName.substring(0, 1).toUpperCase()).append(".");
                    if (StringUtils.isNotEmpty(middleName))
                        fioStr.append(middleName.substring(0, 1).toUpperCase()).append(".");
                    fioStr.append(" ").append(lastName);

                    tableModifier.put("FORM_OU_HEAD_VISA", new String[][]{{postStr.toString(), fioStr.toString()}});
                } else tableModifier.put("FORM_OU_HEAD_VISA", new String[][]{});

                // Получаем список студентов
                List<SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper> personList = paragraphWrapper.getStudentsList();
                Collections.sort(personList);

                List<String[]> studentList = new ArrayList<>(personList.size());
                for (SetDiplomaWorkTopicAndScientificAdviserStudParagraphWrapper wrapper : personList)
                {
                    studentList.add(new String[]{
                            wrapper.getExtract().getEntity().getPerson().getFullFio(),
                            wrapper.getFinalQualifyingWorkTheme(),
                            wrapper.getScientificAdvisor()
                    });
                }

                if (studentList.isEmpty()) tableModifier.put("T", new String[][]{});
                else tableModifier.put("T", studentList.toArray(new String[][]{}));

                paragraphInjectModifier.modify(paragraph);
                tableModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);

            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    private void addEducationLevelData(EducationLevels educationLevel, RtfInjectModifier modifier)
    {
        String levelTypeStr = "";

        if (educationLevel.getLevelType().isMiddle())
        {
            if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = " специальности";
            } else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = " специальности";
            }
        } else if (educationLevel.getLevelType().isSpecialty() || educationLevel.getLevelType().isSpecialization())
        {
            levelTypeStr = " специальности";
        } else if (educationLevel.getLevelType().isMaster())
        {
            levelTypeStr = "направлению подготовки магистров";
        } else if (educationLevel.getLevelType().isBachelor())
        {
            levelTypeStr = "направлению подготовки бакалавров";
        }

        modifier.put("levelTypeStr_D", levelTypeStr);
    }
}
