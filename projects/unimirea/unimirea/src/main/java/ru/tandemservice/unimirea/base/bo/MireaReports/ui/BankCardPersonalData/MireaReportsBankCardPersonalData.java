/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.ui.BankCardPersonalData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;

/**
 * @author Andrey Avetisov
 * @since 27.07.2015
 */
@Configuration
public class MireaReportsBankCardPersonalData extends BusinessComponentManager
{
    public static final String PERSON_TAB_PANEL = "personTabPanel";
    public static final String STUDENT_TAB = "studentTab";
    public static final String EMPLOYEE_TAB = "employeeTab";

    public static final String ORGUNIT_DS = "orgUnitDS";
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String CITIZENSHIP_DS = "citizenshipDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";

    @Bean
    public TabPanelExtPoint personTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(PERSON_TAB_PANEL)
                .addTab(htmlTab(STUDENT_TAB, MireaReportsBankCardPersonalData.class.getPackage() + ".StudentTab"))
                .addTab(htmlTab(EMPLOYEE_TAB, MireaReportsBankCardPersonalData.class.getPackage() + ".EmployeeTab"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(ORGUNIT_DS, orgUnitDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORGUNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(CITIZENSHIP_DS, citizenshipDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.P_SHORT_TITLE, CompensationType.P_SHORT_TITLE))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .create();
    }


    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }

    @Bean
    public IDefaultComboDataSourceHandler citizenshipDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), AddressCountry.class)
                .order(AddressCountry.title())
                .filter(AddressCountry.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CompensationType.class)
                .order(CompensationType.shortTitle())
                .filter(CompensationType.shortTitle());
    }
}
