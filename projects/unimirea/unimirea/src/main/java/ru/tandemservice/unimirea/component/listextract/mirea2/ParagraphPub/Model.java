/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2.ParagraphPub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class Model extends AbstractListParagraphPubModel<MireaExcludeSuccessStuListExtract>
{
    public String getHolidayPeriod()
    {
        if (getExtract().getHolidayFrom() != null && getExtract().getHolidayTo() != null)
        {
            StringBuilder builder = new StringBuilder("c ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getExtract().getHolidayFrom()))
                    .append(" по ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getExtract().getHolidayTo()));
            return builder.toString();

        }
        return null;
    }
}
