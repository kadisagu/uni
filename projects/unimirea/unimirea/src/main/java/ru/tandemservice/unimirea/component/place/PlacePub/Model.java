/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlacePub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.PlacePub.Model
{
    public static final String DOCUMENT_OBJECT_EXT = "document.";
    public static final String DOCUMENT_OBJECT = "document";
    public static final String RENT_OBJECT_EXT = "rentObject.";
    public static final String RENT_OBJECT = "rentObject";
    public static final String DISABLED = "disabled";

    private UniPlacesPlaceExt _placeExt;
    private DynamicListDataSource<DataWrapper> _rentDataSource;
    private Map<Long, DataWrapper> contractorsDocumentsMap = new HashMap<>();

    public Map<Long, DataWrapper> getContractorsDocumentsMap()
    {
        return contractorsDocumentsMap;
    }

    public void setContractorsDocumentsMap(Map<Long, DataWrapper> contractorsDocumentsMap)
    {
        this.contractorsDocumentsMap = contractorsDocumentsMap;
    }

    public DynamicListDataSource<DataWrapper> getRentDataSource()
    {
        return _rentDataSource;
    }

    public void setRentDataSource(DynamicListDataSource<DataWrapper> rentDataSource)
    {
        _rentDataSource = rentDataSource;
    }

    public UniPlacesPlaceExt getPlaceExt()
    {
        return _placeExt;
    }

    public void setPlaceExt(UniPlacesPlaceExt placeExt)
    {
        _placeExt = placeExt;
    }
}