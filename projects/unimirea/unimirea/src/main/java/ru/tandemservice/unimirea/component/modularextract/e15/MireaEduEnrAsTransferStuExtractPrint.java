/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e15;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.unimirea.component.modularextract.IApplicationPrintformCreator;
import ru.tandemservice.unimirea.component.modularextract.MireaMoveStudentDefines;
import ru.tandemservice.unimirea.entity.MireaEduEnrAsTransferStuExtractExt;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2016
 */
public class MireaEduEnrAsTransferStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e15.EduEnrAsTransferStuExtractPrint implements IApplicationPrintformCreator
{

    @Override
    public RtfDocument createPrintForm(byte[] template, EduEnrAsTransferStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        MireaEduEnrAsTransferStuExtractExt extractExt = DataAccessServices.dao().get(MireaEduEnrAsTransferStuExtractExt.class, MireaEduEnrAsTransferStuExtractExt.eduEnrAsTransferStuExtract(), extract);

        if (extract.getCompensationTypeNew().isBudget())
        {
            modifier.put("compensationTypeStrNew_A", "на место, финансируемое за счет бюджетных ассигнований федерального бюджета");
            modifier.put("contract", "");
        }
        else
        {
            modifier.put("compensationTypeStrNew_A", "на место с оплатой стоимости обучения физическими лицами");
            modifier.put("contract", " и договора на оказание платных образовательных услуг " + extractExt.getContractTitle());
        }

        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getEntranceDate()).modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);

        modifier.put("applicationTitle", "от " + extractExt.getApplicationTitle());
        modifier.put("protocolTitle", "от " + extractExt.getProtocolTitle());
        modifier.put("university", extract.getUniversity());

        insertHeaderData(extract, modifier);
        insertDebtsData(extract, document, modifier);

        insertEduDocumentData(extract, modifier);

        EducationLevels educationLevels = extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel();
        addEducationLevelData(educationLevels, modifier);
        modifier.put("eduProgram", extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel().getProgramSubjectTitleWithCode());

        modifier.modify(document);
        return document;
    }

    //Данные справки об обучении
    private void insertEduDocumentData(EduEnrAsTransferStuExtract extract, RtfInjectModifier modifier)
    {
        /**
         * Попросили завязаться на пользовательский элемент справочника "Виды документов о полученном образовании и (или) квалификации"
         * В Мирэа добавили элемент "Справка об обучении" с кодом "18"
         */
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonEduDocument.class, "ped")
                .where(eq(property("ped", PersonEduDocument.person()), value(extract.getEntity().getPerson())))
                .where(eq(property("ped", PersonEduDocument.eduDocumentKind().code()), value("18")))
                .order(property("ped", PersonEduDocument.issuanceDate()), OrderDirection.desc);
        List<PersonEduDocument> documentList = DataAccessServices.dao().getList(builder);
        if (!documentList.isEmpty())
        {
            PersonEduDocument doc = documentList.get(0);
            StringBuilder eduDocumentBuilder = new StringBuilder("справки об обучении ")
                    .append(documentList.get(0).getFullNumber());
            if (null != doc.getRegistrationNumber())
                eduDocumentBuilder.append(" регистрационный № ")
                        .append(documentList.get(0).getRegistrationNumber());
            if (null != doc.getIssuanceDate())
                eduDocumentBuilder.append(", выданной ")
                        .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(documentList.get(0).getIssuanceDate()));
            eduDocumentBuilder.append(", ").toString();

            modifier.put("eduDocumentStr", eduDocumentBuilder.toString());
        }
        else modifier.put("eduDocumentStr", "");
    }


    /**
     * Данные руководителя подразделения
     */
    private void insertHeaderData(EduEnrAsTransferStuExtract extract, RtfInjectModifier modifier)
    {
        EmployeePost headerFormative = (EmployeePost) extract.getEducationOrgUnitNew().getFormativeOrgUnit().getHead();

        String fio_A = "";
        if (headerFormative != null)
        {
            IdentityCard identityCard = headerFormative.getPerson().getIdentityCard();
            String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.ACCUSATIVE, headerFormative.getPerson().isMale());
            StringBuilder str = new StringBuilder(lastName)
                    .append(' ');
            if (StringUtils.isNotEmpty(identityCard.getFirstName()))
            {
                str.append(Character.toTitleCase(identityCard.getFirstName().charAt(0))).append('.');
            }

            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            {
                str.append(Character.toTitleCase(identityCard.getMiddleName().charAt(0))).append('.');
            }
            fio_A = str.toString();
        }

        modifier.put("managerFIO_A", fio_A);
        StringBuilder postTitleBuilder = new StringBuilder();
        if (headerFormative != null)
            postTitleBuilder.append(CoreStringUtils.getLowerFirst(headerFormative.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle() == null ? "" :
                                                                          headerFormative.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle()));
        if (postTitleBuilder.length() > 0)
        {
            String orgUnitTitle = extract.getEducationOrgUnitNew().getFormativeOrgUnit().getGenitiveCaseTitle();
            postTitleBuilder.append(" ")
                    .append(CoreStringUtils.getUpperFirst(StringUtils.isEmpty(orgUnitTitle) ? extract.getEducationOrgUnitNew().getFormativeOrgUnit().getTitle() : orgUnitTitle));
        }
        modifier.put("managerPostTitle_A", postTitleBuilder.toString());
    }

    /**
     * Данные о задолженности
     */
    protected void insertDebtsData(EduEnrAsTransferStuExtract extract, RtfDocument document, RtfInjectModifier modifier)
    {

        int number = 2;
        if (!extract.isHasDebts())
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Lists.newArrayList("debts", "ADDITIONAL"), false, false);
        else
        {
            number++;
            IdentityCard identityCard = extract.getEntity().getPerson().getIdentityCard();

            String deadlineDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline());

            String fio_D = PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_DATIVE);

            StringBuilder debtsBuilder = new StringBuilder("2. Утвердить студенту ")
                    .append(fio_D.toString())
                    .append(" индивидуальный план ликвидации академических задолженностей (Приложение) и установить срок их ликвидации: ")
                    .append(deadlineDate)
                    .append(".");


            modifier.put("debts", debtsBuilder.toString());

            //При печати выпискм добавляем в документ приложение
            addAdditionalToExtract(extract, document);
        }
        modifier.put("n", String.valueOf(number));

    }


    /**
     * Добавление приложения к выписке
     */
    private void addAdditionalToExtract(EduEnrAsTransferStuExtract extract, RtfDocument document)
    {
        // 1. ищем ключевое слово ADDITIONAL
        List<IRtfElement> elementList = null;
        int index = 0;
        boolean found = false;
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfParagraph)
            {
                index = 0;
                elementList = ((RtfParagraph) element).getElementList();
                while (index < elementList.size() && !(elementList.get(index) instanceof RtfField && "ADDITIONAL".equals(((RtfField) elementList.get(index)).getFieldName())))
                    index++;
                if (index < elementList.size())
                {
                    found = true;
                    break;
                }
            }
        }

        // 2. Если нашли, то вместо него вставляем все выписки
        if (found)
        {
            List<IRtfElement> parList = new ArrayList<>();
            // создаем приложение
            byte[] applicationTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(extract.getType(), MireaMoveStudentDefines.INDIVIDUAL_ORDER_APPLICATION_CODE);
            RtfDocument applicationDocument = createApplicationPrintForm(applicationTemplate, extract, 1);
            if (applicationDocument != null)
            {
                // начинаем с новой страницы
                parList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                // подготавливаем ее для вставки в приказ
                RtfUtil.modifySourceList(document.getHeader(), applicationDocument.getHeader(), applicationDocument.getElementList());

                // дальше добавляем приложение
                parList.addAll(applicationDocument.getElementList());

                elementList.remove(index);
                elementList.addAll(index, parList);
            }
        }
    }

    private void addEducationLevelData(EducationLevels educationLevel, RtfInjectModifier modifier)
    {
        String levelTypeStr = "";

        if (educationLevel.getLevelType().isMiddle())
        {
            if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = " специальности";
            }
            else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = " специальности";
            }
        }
        else if (educationLevel.getLevelType().isSpecialty() || educationLevel.getLevelType().isSpecialization())
        {
            levelTypeStr = " специальности";
        }
        else if (educationLevel.getLevelType().isMaster())
        {
            levelTypeStr = "направлению подготовки магистров";
        }
        else if (educationLevel.getLevelType().isBachelor())
        {
            levelTypeStr = "направлению подготовки бакалавров";
        }

        modifier.put("levelTypeStr", levelTypeStr);
    }

    @Override
    public RtfDocument createApplicationPrintForm(byte[] template, ModularStudentExtract extract, int number)
    {
        EduEnrAsTransferStuExtract extractTranf = (EduEnrAsTransferStuExtract) extract;
        final RtfDocument document = new RtfReader().read(template);
        RtfTableModifier table = new RtfTableModifier();
        if (!extractTranf.isHasDebts())
            return null;

        int i = 0;
        List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
        String dateStr = extractTranf.getDeadline() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extractTranf.getDeadline()) : "";
        String[][] tableData = new String[relsList.size()][];
        for (StuExtractToDebtRelation rel : relsList)
        {
            tableData[i++] = new String[]{String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), dateStr};
        }
        table.put("T_15", tableData);

        table.modify(document);
        RtfInjectModifier modifier = new RtfInjectModifier();
        AbstractStudentOrder order = null != extract.getParagraph() ? extract.getParagraph().getOrder() : null;
        modifier.put("orderNumber", order != null ? order.getNumber() : "");
        modifier.put("commitDate", order != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) : "");

        EmployeePost headerFormative = (EmployeePost) extractTranf.getEducationOrgUnitNew().getFormativeOrgUnit().getHead();

        modifier.put("managerFIO", headerFormative == null ? "" : headerFormative.getPerson().getIdentityCard().getIof());

        StringBuilder postTitleBuilder = new StringBuilder();
        if (headerFormative != null)
        {
            postTitleBuilder.append(StringUtils.capitalize(headerFormative.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()))
                    .append(" ");
            String orgUnitTitle = extractTranf.getEducationOrgUnitNew().getFormativeOrgUnit().getGenitiveCaseTitle();
            postTitleBuilder.append(StringUtils.capitalize(StringUtils.isEmpty(orgUnitTitle) ? extractTranf.getEducationOrgUnitNew().getFormativeOrgUnit().getTitle() : orgUnitTitle));
        }
        modifier.put("managerPostTitle", postTitleBuilder.toString());

        modifier.put("Num", number > 0 ? " № " + number : "");

        modifier.modify(document);

        return document;
    }
}