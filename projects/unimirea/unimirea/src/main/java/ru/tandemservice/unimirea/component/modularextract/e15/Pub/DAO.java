/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e15.Pub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimirea.entity.MireaEduEnrAsTransferStuExtractExt;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2016
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e15.Pub.DAO
{

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e15.Pub.Model model)
    {
        super.prepare(model);
        Model modelExt = (Model) model;
        MireaEduEnrAsTransferStuExtractExt extractExt = DataAccessServices.dao().get(MireaEduEnrAsTransferStuExtractExt.class, MireaEduEnrAsTransferStuExtractExt.eduEnrAsTransferStuExtract(), model.getExtract());
        if (extractExt != null)
            modelExt.setExtractExt(extractExt);
    }
}