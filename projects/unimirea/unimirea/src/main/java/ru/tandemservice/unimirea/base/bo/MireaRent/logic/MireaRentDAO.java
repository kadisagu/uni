/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaRent.logic;

import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimirea.entity.MireaPlaceRentToDocumentRelation;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;
import ru.tandemservice.unimirea.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.unimirea.remoteDocument.ext.RemoteDocument.util.RemoteDocumentFlexAttributes;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
public class MireaRentDAO extends UniBaseDao implements IMireaRentDAO
{
    public static final String REMOTE_DOCUMENT_MODULE = "mireaRentDocument";

    @Override
    public void saveOrUpdateDocument(MireaPlaceRentToDocumentRelation document, IUploadFile file)
    {
        checkDocumentForUniqueActiveContract(document);
        saveOrUpdate(document);
        if(document.getDocument() != null)
        {
            updateRemoteDocument(document, file);
        }
        else if(file != null)
        {
            createRemoteDocument(document, file);
        }
    }

    @Override
    public void printDocumentScanCopy(MireaPlaceRentToDocumentRelation document)
    {
        if(document.getDocument() != null)
        {
            try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.MIREA_DOCS_SERVICE_NAME))
            {

                RemoteDocumentDTO scanCopy = service.get(document.getDocument());
                if (scanCopy != null)
                {
                    BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(scanCopy.getFileType()).fileName(scanCopy.getFileName()).document(scanCopy.getContent()), true);
                }
            }
        }
    }

    @Override
    public void changeActiveProperty(Long documentId)
    {
        MireaPlaceRentToDocumentRelation document = get(MireaPlaceRentToDocumentRelation.class,documentId);
        document.setActive(!document.isActive());
        checkDocumentForUniqueActiveContract(document);
        update(document);
    }

    /**
     * Изменение прикрепленного файла документа
     */
    private void updateRemoteDocument(MireaPlaceRentToDocumentRelation document, IUploadFile scanCopy)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.MIREA_DOCS_SERVICE_NAME))
        {
            RemoteDocumentDTO documentDTO = documentService.get(document.getDocument());

            if(documentDTO == null)
            {
                createRemoteDocument(document, scanCopy);
                return;
            }

            if(scanCopy != null)
                documentDTO.setFileContent(scanCopy.getFileName(), IOUtils.toByteArray(scanCopy.getStream()));

            documentDTO.setFlexMap(fillFlexMap(document));

            documentService.update(documentDTO);
        } catch (IOException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }


    private void createRemoteDocument(MireaPlaceRentToDocumentRelation document, IUploadFile scanCopy)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.MIREA_DOCS_SERVICE_NAME))
        {
            RemoteDocumentDTO documentDTO = new RemoteDocumentDTO(null, "Скан-копия документа" + document.getTitle(), REMOTE_DOCUMENT_MODULE);
            documentDTO.setOwnerId(document.getId());

            if(scanCopy != null)
                documentDTO.setFileContent(scanCopy.getFileName(), IOUtils.toByteArray(scanCopy.getStream()));

            documentDTO.setFlexMap(fillFlexMap(document));

            Long attachmentId = documentService.insert(documentDTO);

            document.setDocument(attachmentId);
            update(document);
        } catch (IOException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    private Map<String, Object> fillFlexMap(MireaPlaceRentToDocumentRelation document)
    {
        final Map<String, Object> flexMap = Maps.newHashMap();
        flexMap.put(RemoteDocumentFlexAttributes.MIREA_PLACES_DOCUMENT_OWNER_OBJECT, document.getObject().getId());
        flexMap.put(RemoteDocumentFlexAttributes.MIREA_PLACES_DOCUMENT_TITLE, document.getTitle());
        if (document.getStartDate() != null)
        {
            Calendar startDate = Calendar.getInstance();
            startDate.setTime(document.getStartDate());
            flexMap.put(RemoteDocumentFlexAttributes.MIREA_PLACES_DOCUMENT_START_DATE, startDate);
        }

        if (document.getEndDate() != null)
        {
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(document.getStartDate());
            flexMap.put(RemoteDocumentFlexAttributes.MIREA_PLACES_DOCUMENT_END_DATE, endDate);
        }
        if (!StringUtils.isEmpty(document.getComment()))
            flexMap.put(RemoteDocumentFlexAttributes.MIREA_PLACES_DOCUMENT_COMMENT, document.getComment());
        flexMap.put(RemoteDocumentFlexAttributes.MIREA_PLACES_DOCUMENT_ACTIVE, document.isActive());
        return flexMap;
    }


    @Override
    public void deleteDocument(Long documentId)
    {
        MireaPlaceRentToDocumentRelation document = getNotNull(documentId);
        if (document.getDocument() != null)
        {
            try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.MIREA_DOCS_SERVICE_NAME))
            {

                RemoteDocumentDTO documentDTO = documentService.get(documentId);
                if (documentDTO != null)
                {
                    documentService.delete(documentDTO);
                }
            }
        }
        delete(documentId);
    }

    @Override
    public RemoteDocumentDTO getDocument(Long documentId)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.MIREA_DOCS_SERVICE_NAME))
        {

            RemoteDocumentDTO documentDTO = documentService.get(documentId);
            return documentDTO;
        }
    }

    /**
     * Проверка наличия активного договора аренды
     */
    private boolean isActiveContractExist(MireaPlacesRent rent, Long documentID)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(MireaPlaceRentToDocumentRelation.class, "doc")
            .where(eq(property("doc",MireaPlaceRentToDocumentRelation.object()), value(rent)))
            .where(eq(property("doc", MireaPlaceRentToDocumentRelation.rentContract()), value(true)))
            .where(eq(property("doc", MireaPlaceRentToDocumentRelation.active()), value(true)));
        if (documentID != null)
            builder.where(ne(property("doc.id"), value(documentID)));
        return existsEntity(builder.buildQuery());
    }

    /**
     * Проверка возможности сохранения документа
     * @param document
     */
    private void checkDocumentForUniqueActiveContract(MireaPlaceRentToDocumentRelation document)
    {
        if (document.isActive() && document.isRentContract() && isActiveContractExist(document.getObject(), document.getId()))
            throw new ApplicationException("Активный договор аренды может быть только один.");
    }


}