/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlacePub;


import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unimirea.entity.MireaPlaceRentToDocumentRelation;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.unimirea.util.GeneratorIds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.PlacePub.DAO implements IDAO
{
    @Override
    public void prepare(ru.tandemservice.uniplaces.component.place.PlacePub.Model model)
    {
        super.prepare(model);
        UniPlacesPlaceExt placeExt = getByNaturalId(new UniPlacesPlaceExt.NaturalId(model.getPlace()));
        ((ru.tandemservice.unimirea.component.place.PlacePub.Model)model).setPlaceExt(placeExt);
    }


    @Override
    public void prepareRentDataSource(ru.tandemservice.unimirea.component.place.PlacePub.Model model)
    {
        DynamicListDataSource<DataWrapper> dataSource = model.getRentDataSource();

        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();

        List<DataWrapper> wrappers = new ArrayList<>();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(MireaPlacesRent.class, "re")
                .joinEntity("re", DQLJoinType.left, MireaPlaceRentToDocumentRelation.class, "doc", eq(property("re.id"), property("doc", MireaPlaceRentToDocumentRelation.object().id())))
                .column(property("re"))
                .column(property("doc"))
                .where(eq(property("re", MireaPlacesRent.place()), value(model.getObjectID())))
                .order(property("re.id"))
                .order(property("doc.id"));

        final int count = getCount(builder);
        dataSource.setTotalSize(count);

        List<Object[]> resultList = getList(builder, (int) startRow, (int) countRow);
        Map<Long, DataWrapper> relationMap = model.getContractorsDocumentsMap();

        for(Object[] pair : resultList)
        {
            DataWrapper wrap = new DataWrapper();
            wrap.setId(GeneratorIds.generateNewId());
            MireaPlacesRent rent = (MireaPlacesRent) pair[0];
            wrap.setProperty(Model.RENT_OBJECT, rent);
            MireaPlaceRentToDocumentRelation doc = (MireaPlaceRentToDocumentRelation)pair[1];

            wrap.setProperty(Model.DOCUMENT_OBJECT, doc);
            wrap.setProperty(Model.DISABLED, doc == null);
            wrappers.add(wrap);
            relationMap.put(wrap.getId(), wrap);
        }

        dataSource.setCountRow(wrappers.size() < 3 ? 3:wrappers.size());
        dataSource.createPage(wrappers);
    }

}