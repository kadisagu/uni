package ru.tandemservice.unimirea.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ о назначении выплат студентам (МИРЭА)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MireaStudentPaymentsOrderExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt";
    public static final String ENTITY_NAME = "mireaStudentPaymentsOrderExt";
    public static final int VERSION_HASH = 1413625623;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_PAYMENTS_ORDER = "studentPaymentsOrder";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_PROTOCOL_DATE = "protocolDate";

    private StudentPaymentsOrder _studentPaymentsOrder;     // Приказ о назначении выплат студентам
    private String _protocolNumber;     // Номер протокола аттестационной комиссии
    private Date _protocolDate;     // Дата протокола аттестационной комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentPaymentsOrder getStudentPaymentsOrder()
    {
        return _studentPaymentsOrder;
    }

    /**
     * @param studentPaymentsOrder Приказ о назначении выплат студентам. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudentPaymentsOrder(StudentPaymentsOrder studentPaymentsOrder)
    {
        dirty(_studentPaymentsOrder, studentPaymentsOrder);
        _studentPaymentsOrder = studentPaymentsOrder;
    }

    /**
     * @return Номер протокола аттестационной комиссии.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола аттестационной комиссии.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Дата протокола аттестационной комиссии.
     */
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола аттестационной комиссии.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MireaStudentPaymentsOrderExtGen)
        {
            setStudentPaymentsOrder(((MireaStudentPaymentsOrderExt)another).getStudentPaymentsOrder());
            setProtocolNumber(((MireaStudentPaymentsOrderExt)another).getProtocolNumber());
            setProtocolDate(((MireaStudentPaymentsOrderExt)another).getProtocolDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MireaStudentPaymentsOrderExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MireaStudentPaymentsOrderExt.class;
        }

        public T newInstance()
        {
            return (T) new MireaStudentPaymentsOrderExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentPaymentsOrder":
                    return obj.getStudentPaymentsOrder();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "protocolDate":
                    return obj.getProtocolDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentPaymentsOrder":
                    obj.setStudentPaymentsOrder((StudentPaymentsOrder) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentPaymentsOrder":
                        return true;
                case "protocolNumber":
                        return true;
                case "protocolDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentPaymentsOrder":
                    return true;
                case "protocolNumber":
                    return true;
                case "protocolDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentPaymentsOrder":
                    return StudentPaymentsOrder.class;
                case "protocolNumber":
                    return String.class;
                case "protocolDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MireaStudentPaymentsOrderExt> _dslPath = new Path<MireaStudentPaymentsOrderExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MireaStudentPaymentsOrderExt");
    }
            

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt#getStudentPaymentsOrder()
     */
    public static StudentPaymentsOrder.Path<StudentPaymentsOrder> studentPaymentsOrder()
    {
        return _dslPath.studentPaymentsOrder();
    }

    /**
     * @return Номер протокола аттестационной комиссии.
     * @see ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Дата протокола аттестационной комиссии.
     * @see ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    public static class Path<E extends MireaStudentPaymentsOrderExt> extends EntityPath<E>
    {
        private StudentPaymentsOrder.Path<StudentPaymentsOrder> _studentPaymentsOrder;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Date> _protocolDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt#getStudentPaymentsOrder()
     */
        public StudentPaymentsOrder.Path<StudentPaymentsOrder> studentPaymentsOrder()
        {
            if(_studentPaymentsOrder == null )
                _studentPaymentsOrder = new StudentPaymentsOrder.Path<StudentPaymentsOrder>(L_STUDENT_PAYMENTS_ORDER, this);
            return _studentPaymentsOrder;
        }

    /**
     * @return Номер протокола аттестационной комиссии.
     * @see ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(MireaStudentPaymentsOrderExtGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Дата протокола аттестационной комиссии.
     * @see ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(MireaStudentPaymentsOrderExtGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

        public Class getEntityClass()
        {
            return MireaStudentPaymentsOrderExt.class;
        }

        public String getEntityName()
        {
            return "mireaStudentPaymentsOrderExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
