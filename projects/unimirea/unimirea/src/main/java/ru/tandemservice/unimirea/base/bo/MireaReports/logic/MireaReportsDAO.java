/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import com.google.common.collect.Maps;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unimirea.base.bo.MireaReports.ui.BankCardPersonalData.MireaReportsBankCardPersonalDataUI;
import ru.tandemservice.unimirea.base.bo.MireaReports.ui.BuildingAndPlace.MireaReportsBuildingAndPlace;
import ru.tandemservice.unimirea.entity.*;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 20.02.2015
 */
public class MireaReportsDAO extends UniBaseDao implements IMireaReportsDAO
{

    @Override
    public ReportInfo prepareReportData(ReportDataWrapper dataWrapper)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesGround.class, "ground")
                .joinEntity("ground", DQLJoinType.left, UniplacesBuilding.class, "building", eq(property("ground.id"), property("building", UniplacesBuilding.ground())))
                .joinEntity("building", DQLJoinType.left, UniplacesUnit.class, "unit", eq(property("building.id"), property("unit", UniplacesUnit.building())))
                .joinEntity("unit", DQLJoinType.left, UniplacesFloor.class, "floor", eq(property("unit.id"), property("floor", UniplacesFloor.unit())))
                .joinEntity("floor", DQLJoinType.left, UniplacesPlace.class, "room", eq(property("floor.id"), property("room", UniplacesPlace.floor())))
                .joinEntity("room", DQLJoinType.left, UniPlacesPlaceExt.class, "ext", eq(property("room.id"), property("ext", UniPlacesPlaceExt.place())))
                .joinEntity("ext", DQLJoinType.left, UniMireaPlace.class, "place", eq(property("place.id"), property("ext", UniPlacesPlaceExt.placeMirea())));

        if (dataWrapper.getRoomList() != null && !dataWrapper.getRoomList().isEmpty())
            builder.where(in(property("room.id"), dataWrapper.getRoomList()));
        else if (dataWrapper.getPlaceList() != null && !dataWrapper.getPlaceList().isEmpty())
            builder.where(in(property("place.id"), dataWrapper.getPlaceList()));
        else if (!dataWrapper.getFloorList().isEmpty())
            builder.where(in(property("floor.id"), dataWrapper.getFloorList()));
        else if (!dataWrapper.getUnitList().isEmpty())
            builder.where(in(property("unit.id"), dataWrapper.getUnitList()));
        else if (!dataWrapper.getBuildingList().isEmpty())
            builder.where(in(property("building.id"), dataWrapper.getBuildingList()));
        else if (!dataWrapper.getGroundList().isEmpty())
            builder.where(in(property("ground.id"), dataWrapper.getGroundList()));

        //Возможность аренды
        Object avaliableRent = dataWrapper.getAvailableRent();
        if (avaliableRent != null)
            builder.where(eq(property("ext", UniPlacesPlaceExt.mayRented()), value(((IEntity) avaliableRent).getId().equals(MireaReportsBuildingAndPlace.AVALIABLE_RENT_TRUE))));

        //Состояние аренды
        Object statusRent = dataWrapper.getStateRent();
        if (statusRent != null)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(MireaPlaceRentToDocumentRelation.class, "doc")
                    .column("doc.id")
                    .joinPath(DQLJoinType.inner, MireaPlaceRentToDocumentRelation.object().fromAlias("doc"), "rent")
                    .where(eq(property("rent", MireaPlacesRent.place()), property("room.id")))
                    .where(eq(property("doc", MireaPlaceRentToDocumentRelation.rentContract()), value(true)))
                    .where(eq(property("doc", MireaPlaceRentToDocumentRelation.active()), value(true)));
            if (((IEntity) statusRent).getId().equals(MireaReportsBuildingAndPlace.STATUS_RENT_BUSY))
                builder.where(exists(subBuilder.buildQuery()));
            else
            {
                builder.where(notExists(subBuilder.buildQuery()));
                builder.where(eq(property("ext", UniPlacesPlaceExt.mayRented()), value(true)));
            }
        }
        //право распоряжения зем.участком (доп.)
        List<UniplacesDisposalRight> rightGroundList = dataWrapper.getRightGroungList();
        if (rightGroundList != null && !rightGroundList.isEmpty())
        {
            builder.joinEntity("ground", DQLJoinType.left, MireaSecondDisposalRight.class, "rightGround", eq(property("ground.id"), property("rightGround", MireaSecondDisposalRight.registryRecord())));
            builder.where(in(property("rightGround", MireaSecondDisposalRight.disposalRight()), rightGroundList));
        }
        //право распоряжения зданием (доп)
        List<UniplacesDisposalRight> rightBuildingList = dataWrapper.getRightBuildingList();
        if (rightBuildingList != null && !rightBuildingList.isEmpty())
        {
            builder.joinEntity("building", DQLJoinType.left, MireaSecondDisposalRight.class, "rightBuilding", eq(property("building.id"), property("rightBuilding", MireaSecondDisposalRight.registryRecord())));
            builder.where(in(property("rightBuilding", MireaSecondDisposalRight.disposalRight()), rightBuildingList));
        }


        builder.order(property("ground", UniplacesGround.number()))
                .order(property("building", UniplacesBuilding.number()))
                .order(property("unit", UniplacesUnit.title()))
                .order(property("floor", UniplacesFloor.title()))
                .order(property("place", UniMireaPlace.title()))
                .order(property("room", UniplacesPlace.title()));


        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(builder);
        int roomCol = dql.column(property("room"));
        int placeCol = dql.column(property("place"));
        int floorCol = dql.column(property("floor"));
        int unitCol = dql.column(property("unit"));
        int buildingCol = dql.column(property("building"));
        int groundCol = dql.column(property("ground"));
        int roomExtCol = dql.column(property("ext"));
        List<Object[]> reportRows = dql.getDql().createStatement(getSession()).list();

        Map<UniplacesGround, Set<UniplacesBuilding>> groundToBuildingMap = Maps.newHashMap();
        Map<UniplacesBuilding, Set<UniplacesUnit>> buildingToUnitMap = Maps.newHashMap();
        Map<UniplacesUnit, Set<UniplacesFloor>> unitToFloorMap = Maps.newHashMap();
        Map<UniplacesFloor, Set<UniMireaPlace>> floorToPlaceMap = Maps.newHashMap();
        Map<UniplacesFloor, Set<UniplacesPlace>> floorToRoomMap = Maps.newHashMap();
        Map<UniMireaPlace, Set<UniplacesPlace>> placeToRoomMap = Maps.newHashMap();
        Map<UniplacesPlace, UniPlacesPlaceExt> roomToExtMap = Maps.newHashMap();

        for (Object[] row : reportRows)
        {
            UniplacesGround ground = (UniplacesGround) row[groundCol];
            UniplacesBuilding building = (UniplacesBuilding) row[buildingCol];
            UniplacesUnit unit = (UniplacesUnit) row[unitCol];
            UniplacesFloor floor = (UniplacesFloor) row[floorCol];
            UniplacesPlace room = (UniplacesPlace) row[roomCol];
            UniMireaPlace place = (UniMireaPlace) row[placeCol];
            UniPlacesPlaceExt roomExt = (UniPlacesPlaceExt) row[roomExtCol];

            if (ground != null)
                SafeMap.safeGet(groundToBuildingMap, ground, LinkedHashSet.class).add(building);
            if (building != null && unit != null)
                SafeMap.safeGet(buildingToUnitMap, building, LinkedHashSet.class).add(unit);
            if (unit != null && floor != null)
                SafeMap.safeGet(unitToFloorMap, unit, LinkedHashSet.class).add(floor);
            if (floor != null)
            {
                if (place != null)
                {
                    SafeMap.safeGet(floorToPlaceMap, floor, LinkedHashSet.class).add(place);
                    if (room != null)
                        SafeMap.safeGet(placeToRoomMap, place, LinkedHashSet.class).add(room);
                }
                else if (room != null)
                    SafeMap.safeGet(floorToRoomMap, floor, LinkedHashSet.class).add(room);
            }
            if (room != null && roomExt != null)
                roomToExtMap.put(room, roomExt);
        }

        return new ReportInfo(groundToBuildingMap, buildingToUnitMap, unitToFloorMap, floorToPlaceMap, floorToRoomMap, placeToRoomMap, roomToExtMap);
    }

    public List<Object[]> getPersonStudentList(MireaReportsBankCardPersonalDataUI model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .joinPath(DQLJoinType.left, Student.person().fromAlias("s"), "p")
                .joinPath(DQLJoinType.left, Person.identityCard().fromAlias("p"), "ic")
                .joinPath(DQLJoinType.left, IdentityCard.address().fromAlias("ic"), "regAdr")
                .joinPath(DQLJoinType.left, Person.address().fromAlias("p"), "factAdr")
                .column(property("p"))
                .column(property("ic"))
                .column(property("regAdr"))
                .column(property("factAdr"))
                .column(property("s"));

        if (model.isShowCourse())
        {
            dql.where(in(property("s", Student.course()), model.getCourseList()));
        }
        if (model.isShowFormativeOrgUnit())
        {
            dql.where(in(property("s", Student.educationOrgUnit().formativeOrgUnit()), model.getFormativeOrgUnitList()));
        }
        if (model.isShowCitizenship())
        {
            dql.where(in(property("ic", IdentityCard.citizenship()), model.getCitizenshipList()));
        }
        if (model.isShowCompensationType())
        {
            dql.where(in(property("s", Student.compensationType()), model.getCompensationTypeList()));
        }
        if (model.isShowDevelopForm())
        {
            dql.where(in(property("s", Student.educationOrgUnit().developForm()), model.getDevelopFormList()));
        }

        dql.where(eq(property("s", Student.status().active()), value(true)))
                .order(property("ic", IdentityCard.P_LAST_NAME))
                .order(property("ic", IdentityCard.P_FIRST_NAME))
                .order(property("ic", IdentityCard.P_MIDDLE_NAME));
        getSession().clear();
        return dql.createStatement(getSession()).list();
    }

    public List<Object[]> getPersonEmployeeList(MireaReportsBankCardPersonalDataUI model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Employee.class, "e")
                .joinEntity("e", DQLJoinType.left, EmployeePost.class, "ep", eq(property("e", Employee.id()), property("ep", EmployeePost.employee().id())))
                .joinPath(DQLJoinType.left, Employee.person().fromAlias("e"), "p")
                .joinPath(DQLJoinType.left, Person.identityCard().fromAlias("p"), "ic")
                .joinPath(DQLJoinType.left, IdentityCard.address().fromAlias("ic"), "regAdr")
                .joinPath(DQLJoinType.left, Person.address().fromAlias("p"), "factAdr")

                .column(property("p"))
                .column(property("ic"))
                .column(property("regAdr"))
                .column(property("factAdr"))
                .column(property("e"))
                .column(property("ep"));

        if (model.isShowDateFrom() || model.isShowDateTo())
        {
            FilterUtils.applyBetweenFilter(dql, "ep", EmployeePost.postDate().getPath(), model.getDateFrom(), model.getDateTo());
        }

        if (model.isShowEmployeeOrgUnit())
        {
            Set<OrgUnit> selectedOrgUnits = new HashSet<>();
            fillEmployeeOrgUnitSet(model.getEmployeeOrgUnitList(), model.isCountInnerOrgUnit(), selectedOrgUnits);
            dql.where(in(property("ep", EmployeePost.orgUnit()), selectedOrgUnits));
        }
        if (model.isShowCitizenship())
        {
            dql.where(in(property("ic", IdentityCard.citizenship()), model.getCitizenshipList()));
        }

        dql.where(eq(property("ep", EmployeePost.postStatus().active()), value(true)))
                .order(property("ic", IdentityCard.P_LAST_NAME))
                .order(property("ic", IdentityCard.P_FIRST_NAME))
                .order(property("ic", IdentityCard.P_MIDDLE_NAME));
        getSession().clear();
        return dql.createStatement(getSession()).list();
    }


    private static void fillEmployeeOrgUnitSet(List<OrgUnit> selectedOrgUnits, boolean isCountInnerOrgUnit, final Set<OrgUnit> result)
    {
        if (isCountInnerOrgUnit)
        {
            for (OrgUnit orgUnit : selectedOrgUnits)
            {
                result.add(orgUnit);
                List<OrgUnit> childOrgUnitList = orgUnit.getChildrenForOrgstructTree();

                if (!childOrgUnitList.isEmpty())
                {
                    fillEmployeeOrgUnitSet(childOrgUnitList, true, result);
                }
                else
                {
                    result.addAll(orgUnit.getChildrenForOrgstructTree());
                }
            }
        }
        else
        {
            result.addAll(selectedOrgUnits);
        }
    }
}