/*$Id$*/
package ru.tandemservice.unimirea.component.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 24.11.2015
 */
public class SessionBulletinPrintDAOMirea extends SessionBulletinPrintDAO implements ru.tandemservice.unisession.print.ISessionBulletinPrintDAO
{
    @Override
    protected void customizeSimpleTags(RtfInjectModifier modifier, BulletinPrintInfo printInfo)
    {
        final SessionBulletinDocument bulletin = printInfo.getBulletin();
        final EppRegistryElementPart registryElementPart = bulletin.getGroup().getActivityPart();
        final OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        final String termNumbers = printInfo.getContentMap().keySet().stream()
                .map(slot -> String.valueOf(slot.getStudentWpe().getTerm().getIntValue()))
                .distinct()
                .sorted()
                .collect(Collectors.joining(", "));

        modifier.put("orgUnitTitle", orgUnit.getPrintTitle());
        modifier.put("nterm", termNumbers);
        modifier.put("hours", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getSizeAsDouble()));
        modifier.put("ze", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getLaborAsDouble()));
    }

    @Override
    protected List<ColumnType> prepareColumnList(BulletinPrintInfo printInfo)
    {
        final EppControlActionType caType = printInfo.getBulletin().getGroup().getActionType();
        switch (caType.getCode())
        {
            case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF:
            case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT:
            case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK:
            case EppFControlActionTypeCodes.CONTROL_ACTION_EXAM:
                return getMireaColumnList(6);
            case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF:
                return getMireaColumnList(4);
            default:
                return super.prepareColumnList(printInfo);
        }
    }

    @Override
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
        final EppControlActionType caType = printInfo.getBulletin().getGroup().getActionType();
        switch (caType.getCode())
        {
            case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF:
            case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT:
            case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK:
            case EppFControlActionTypeCodes.CONTROL_ACTION_EXAM:
            case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF:
                return getMireaColumnList(6);

            default:
                return super.getTemplateColumnList(printInfo);
        }
    }

    @Override
    protected String[] fillTableRow(BulletinPrintRow printRow, List<ColumnType> columns, int rowNumber)
    {
        final List<String> result = new ArrayList<>();

        for (final ColumnType column : columns)
        {
            final SessionMarkCatalogItem mark = printRow.getMark() == null ? null : printRow.getMark().getValueItem();
            switch (column)
            {
                case EMPTY:
                    result.add("");
                    break;
                case FIO:
                    result.add(printRow.getSlot().getActualStudent().getPerson().getFio());
                    break;
                case BOOK_NUMBER:
                    result.add(StringUtils.trimToEmpty(printRow.getSlot().getActualStudent().getBookNumber()));
                    break;
                case GROUP:
                    result.add(printRow.getEduGroupRow() == null ? "" : StringUtils.trimToEmpty(printRow.getEduGroupRow().getStudentGroupTitle()));
                    break;
                case THEME:
                    result.add(printRow.getTheme() == null ? "" : StringUtils.trimToEmpty(printRow.getTheme().getTheme()));
                    break;
                case MARK:
                {
                    final SessionDocument document = printRow.getSlot().getDocument();
                    if (document instanceof SessionBulletinDocument)
                    {
                        final SessionBulletinDocument bulletinDocument = (SessionBulletinDocument) document;
                        final SessionObject session = bulletinDocument.getSessionObject();
                        final Student student = printRow.getSlot().getActualStudent();

                        final SessionStudentNotAllowedForBulletin notAllowedForBulletin = getByNaturalId(new SessionStudentNotAllowedForBulletin.NaturalId(bulletinDocument, student));
                        final SessionStudentNotAllowed notAllowedForSession = getByNaturalId(new SessionStudentNotAllowed.NaturalId(session, student));
                        if (isNotAllowed(notAllowedForBulletin) || isNotAllowed(notAllowedForSession))
                        {
                            result.add("н/д");
                            break;
                        }
                    }

                    result.add(mark == null ? "" : StringUtils.trimToEmpty(mark.getPrintTitle()));
                    break;
                }
                case NUMBER:
                    result.add(String.valueOf(rowNumber));
                    break;
                case CURRENT_RATING:
                    final Double currentRating = printRow.getSlotRatingData() == null ? null : printRow.getSlotRatingData().getFixedCurrentRating();
                    result.add(BrsRatingValueFormatter.instance.get().format(currentRating));
                    break;
                case POINT:
                    final Double points = printRow.getMark() == null ? null : printRow.getMark().getPoints();
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(points));
                    break;
                case SCORED_POINTS:
                    final Double scoredPoints = printRow.getMarkRatingData() == null ? null : printRow.getMarkRatingData().getScoredPoints();
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(scoredPoints));
                    break;
                case DATE:
                    result.add(printRow.getMark() == null || printRow.getMark().getPerformDate() == null ? "" : DATE_FORMATTER.format(printRow.getMark().getPerformDate()));
                    break;
                case PPS:
                    result.add(UniStringUtils.join(printRow.getCommission(), PpsEntry.person().identityCard().fio().s(), ", "));
                    break;
                case STUDENT_NUMBER:
                    result.add(printRow.getSlot().getActualStudent().getPersonalNumber());
                    break;
            }
        }
        return result.toArray(new String[result.size()]);
    }

    private List<ColumnType> getMireaColumnList(int n)
    {
        final List<ColumnType> columnTypes = new ArrayList<>(5 + n);
        columnTypes.add(ColumnType.NUMBER);
        columnTypes.add(ColumnType.FIO);
        columnTypes.add(ColumnType.STUDENT_NUMBER);
        columnTypes.add(ColumnType.DATE);
        columnTypes.add(ColumnType.MARK);
        for (int i = 0; i < n; i++)
            columnTypes.add(ColumnType.EMPTY);
        return columnTypes;
    }

    private boolean isNotAllowed(@Nullable ISessionStudentNotAllowed notAllowedDoc)
    {
        return notAllowedDoc != null && notAllowedDoc.getRemovalDate() == null;
    }
}