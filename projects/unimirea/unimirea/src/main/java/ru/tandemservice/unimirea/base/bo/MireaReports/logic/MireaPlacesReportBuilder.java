/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import com.google.common.collect.Maps;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.*;
import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.math.NumberUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimirea.base.bo.MireaReports.MireaReportsManager;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.*;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author Ekaterina Zvereva
 * @since 25.02.2015
 */
public class MireaPlacesReportBuilder
{
    private WritableCellFormat headerFormat;
    private WritableCellFormat rowFormat;

    private Map<String, Integer> _buildingPropertyMap;
    private Map<String, Integer> _groundPropertyMap;
    private Map<String, Integer> _unitPropertyMap;
    private Map<String, Integer> _floorPropertyMap;
    private Map<String, Integer> _placePropertyMap;
    private Map<String, Integer> _roomPropertyMap;
    private Map<String, Integer> _roomExtPropertyMap;

    int groundColNum;
    int buildingColNum;
    int unitColNum;
    int floorColNum;
    int placeColNum;
    int roomColNum;

    public WritableCellFormat getHeaderFormat()
    {
        return headerFormat;
    }

    public void setHeaderFormat(WritableCellFormat headerFormat)
    {
        this.headerFormat = headerFormat;
    }

    public int getGroundColNum()
    {
        return groundColNum;
    }

    public int getBuildingColNum()
    {
        return buildingColNum;
    }

    public int getUnitColNum()
    {
        return unitColNum;
    }

    public int getFloorColNum()
    {
        return floorColNum;
    }

    public int getPlaceColNum()
    {
        return placeColNum;
    }

    public int getRoomColNum()
    {
        return roomColNum;
    }

    public WritableCellFormat getRowFormat()
    {
        return rowFormat;
    }

    public void setRowFormat(WritableCellFormat rowFormat)
    {
        this.rowFormat = rowFormat;
    }

    public Map<String, Integer> getBuildingPropertyMap()
    {
        return _buildingPropertyMap;
    }

    public Map<String, Integer> getGroundPropertyMap()
    {
        return _groundPropertyMap;
    }

    public Map<String, Integer> getUnitPropertyMap()
    {
        return _unitPropertyMap;
    }

    public Map<String, Integer> getFloorPropertyMap()
    {
        return _floorPropertyMap;
    }


    public Map<String, Integer> getPlacePropertyMap()
    {
        return _placePropertyMap;
    }


    public Map<String, Integer> getRoomPropertyMap()
    {
        return _roomPropertyMap;
    }

    public Map<String, Integer> getRoomExtPropertyMap()
    {
        return _roomExtPropertyMap;
    }

    public ByteArrayOutputStream createReport(ReportDataWrapper reportData) throws Exception
    {
        ReportInfo reportInfo = MireaReportsManager.instance().dao().prepareReportData(reportData);

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        initFont();

        try
        {
            WritableSheet sheet = workbook.createSheet("отчет", 0);
            sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
            sheet.getSettings().setCopies(1);
            int excelRowNumber = 0;
            List<String> titleRow = new ArrayList<>();

            initReportTempInfo(titleRow, reportData);

            //Рисуем заголовки
            printTableTitle(titleRow, reportData, sheet, excelRowNumber);
            excelRowNumber += 2;

            boolean isPrintParentData = reportData.isPrintParentData();

            for (UniplacesGround ground : reportInfo.getGroundToBuildingMap().keySet())
            {
                if (!reportData.isHideGround())
                {
                    // выводим информацию о земельных участках
                    sheet.addCell(new Label(getGroundColNum(), excelRowNumber, ground.getFullTitle(), getRowFormat()));
                    for (String property : getGroundPropertyMap().keySet())
                        sheet.addCell(createRowCell(getGroundPropertyMap().get(property), excelRowNumber, ground.getProperty(property) == null ? null : String.valueOf(ground.getProperty(property))));
                    // Добавляем данные по второму праву
                    Integer maxCol = Collections.max(getGroundPropertyMap().values());
                    addAdditionalRecordProperty(ground, sheet, excelRowNumber, maxCol);
                    excelRowNumber++;
                }

                for (UniplacesBuilding building : reportInfo.getGroundToBuildingMap().get(ground))
                {
                    if (building == null) continue;
                    if (!reportData.isHideBuilding())
                    {
                        //информация о здании
                        if (isPrintParentData)
                            sheet.addCell(new Label(getGroundColNum(), excelRowNumber, ground.getFullTitle(), getRowFormat()));
                        addBuildingInfo(sheet, excelRowNumber, building);
                        excelRowNumber++;
                    }

                    if (reportInfo.getBuildingToUnitMap().containsKey(building))
                    {
                        for (UniplacesUnit unit : reportInfo.getBuildingToUnitMap().get(building))
                        {
                            if (!reportData.isHideUnit())
                            {
                                if (isPrintParentData)
                                {
                                    //выводим данные по верхним уровням иерархии
                                    sheet.addCell(new Label(getGroundColNum(), excelRowNumber, ground.getFullTitle(), getRowFormat()));
                                    sheet.addCell(new Label(getBuildingColNum(), excelRowNumber, building.getTitle(), getRowFormat()));
                                }
                                //информация о блоке
                                addUnitInfo(sheet, excelRowNumber, unit);
                                excelRowNumber++;
                            }
                            //Добавление данных об этаже
                            excelRowNumber = addFloorInfoToReport(reportInfo, sheet, excelRowNumber, unit, reportData);
                        }
                    }
                }
            }

        }
        catch (RowsExceededException e)
        {
            throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        workbook.write();
        workbook.close();

        return out;
    }

    private void initFont() throws WriteException
    {
        // создаем шрифты
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // шрифт для шапки отчета
        WritableCellFormat header = new WritableCellFormat(arial8bold);
        header.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        header.setAlignment(jxl.format.Alignment.CENTRE);
        header.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        header.setBackground(jxl.format.Colour.GRAY_25);
        header.setWrap(true);
        setHeaderFormat(header);

        // шрифт для строк отчета
        WritableCellFormat row = new WritableCellFormat(arial8);
        row.setBorder(jxl.format.Border.NONE, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        row.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        row.setWrap(true);
        setRowFormat(row);
    }

    private void printTableTitle(List<String> titleRow, ReportDataWrapper reportData, WritableSheet sheet, int rowNumber) throws WriteException
    {
        for (int i = 0; i < titleRow.size(); i++)
            sheet.addCell(new Label(i, rowNumber + 1, titleRow.get(i), getHeaderFormat()));

        sheet.mergeCells(0, rowNumber, getRoomColNum(), rowNumber);
        sheet.addCell(new Label(0, rowNumber, "Иерархия", getHeaderFormat()));

        int count = getRoomColNum();

        if (!reportData.isHideGround())
        {
            sheet.mergeCells(count+1, rowNumber, count + getGroundPropertyMap().size()+2, rowNumber);
            sheet.addCell(new Label(count+1, rowNumber, "Земельный участок", getHeaderFormat()));
            count += getGroundPropertyMap().size()+2;
        }
        if (!reportData.isHideBuilding())
        {
            sheet.mergeCells(count+1, rowNumber, count + getBuildingPropertyMap().size()+2, rowNumber);
            sheet.addCell(new Label(count+1, rowNumber, "Здание", getHeaderFormat()));
            count += getBuildingPropertyMap().size()+2;
        }

        if (!reportData.isHideUnit())
        {
            sheet.mergeCells(count+1, rowNumber, count + getUnitPropertyMap().size(), rowNumber);
            sheet.addCell(new Label(count+1, rowNumber, "Блок", getHeaderFormat()));
            count += getUnitPropertyMap().size();
        }

        if (!reportData.isHideFloor())
        {
            sheet.mergeCells(count+1, rowNumber, count + getFloorPropertyMap().size(), rowNumber);
            sheet.addCell(new Label(count+1, rowNumber, "Этаж", getHeaderFormat()));
            count += getFloorPropertyMap().size();
        }

        if (!reportData.isHidePlace())
        {
            sheet.mergeCells(count+1, rowNumber, count + getPlacePropertyMap().size(), rowNumber);
            sheet.addCell(new Label(count+1, rowNumber, "Помещение", getHeaderFormat()));
            count += getPlacePropertyMap().size();
        }

        sheet.mergeCells(count+1, rowNumber, count + getRoomPropertyMap().size() + getRoomExtPropertyMap().size(), rowNumber);
        sheet.addCell(new Label(count+1, rowNumber, "Комната", getHeaderFormat()));

    }

    //свойства здания
    private void addBuildingInfo(WritableSheet sheet, int excelRowNumber, UniplacesBuilding building) throws WriteException
    {
        sheet.addCell(new Label(getBuildingColNum(), excelRowNumber, building.getTitle(), getRowFormat()));
        for (String property : getBuildingPropertyMap().keySet())
            sheet.addCell(createRowCell(getBuildingPropertyMap().get(property), excelRowNumber, building.getProperty(property) == null ? null : String.valueOf(building.getProperty(property))));
        // Добавляем данные по второму праву
        Integer maxCol = Collections.max(getBuildingPropertyMap().values());
        addAdditionalRecordProperty(building, sheet, excelRowNumber, maxCol);
    }

    //свойства блока
    private void addUnitInfo(WritableSheet sheet, int excelRowNumber, UniplacesUnit unit) throws WriteException
    {
        sheet.addCell(new Label(getUnitColNum(), excelRowNumber, unit.getTitle(), getRowFormat()));
        for (String property : getUnitPropertyMap().keySet())
            sheet.addCell(createRowCell(getUnitPropertyMap().get(property), excelRowNumber, unit.getProperty(property) == null ? null : String.valueOf(unit.getProperty(property))));
    }

    // данные по этажу
    private int addFloorInfoToReport(ReportInfo reportInfo, WritableSheet sheet, int excelRowNumber, UniplacesUnit unit, ReportDataWrapper reportData) throws WriteException
    {
        if (reportInfo.getUnitToFloorMap().containsKey(unit))
        {
            for (UniplacesFloor floor : reportInfo.getUnitToFloorMap().get(unit))
            {
                if (!reportData.isHideFloor())
                {
                    if (reportData.isPrintParentData())
                    {
                        sheet.addCell(new Label(getGroundColNum(), excelRowNumber, unit.getBuilding().getGround().getFullTitle(), getRowFormat()));
                        sheet.addCell(new Label(getBuildingColNum(), excelRowNumber, unit.getBuilding().getTitle(), getRowFormat()));
                        sheet.addCell(new Label(getUnitColNum(), excelRowNumber, unit.getTitle(), getRowFormat()));
                    }
                    sheet.addCell(new Label(getFloorColNum(), excelRowNumber, floor.getTitle(), getRowFormat()));
                    for (String property : getFloorPropertyMap().keySet())
                        sheet.addCell(createRowCell(getFloorPropertyMap().get(property), excelRowNumber, floor.getProperty(property) == null ? null : String.valueOf(floor.getProperty(property))));
                    excelRowNumber++;
                }
                //добавляем в отчет комнаты, находящиеся вне помещений
                excelRowNumber = addRoomWithoutPlace(reportInfo, sheet, excelRowNumber, floor, reportData.isPrintParentData());

                //добавляем помещения
                excelRowNumber = addPlaceInfoToReport(reportInfo, sheet, excelRowNumber, floor, reportData);
            }
        }
        return excelRowNumber;
    }

    // инициализируем вспомогательную информацию
    private void initReportTempInfo(List<String> titleRow, ReportDataWrapper reportDataWrapper)
    {
        groundColNum = 0;
        buildingColNum = 1;
        unitColNum = 2;
        floorColNum = 3;
        placeColNum = 4;
        roomColNum = 5;
        titleRow.addAll(Arrays.asList("Земельный участок", "Здание", "Блок", "Этаж", "Помещение", "Комната"));

        boolean printParent = reportDataWrapper.isPrintParentData();
        if (reportDataWrapper.isHideGround() && !printParent)
        {
            buildingColNum--;
            unitColNum--;
            floorColNum--;
            placeColNum--;
            roomColNum--;
            titleRow.remove("Земельный участок");
        }

        if (reportDataWrapper.isHideBuilding() && !printParent)
        {
            unitColNum--;
            floorColNum--;
            placeColNum--;
            roomColNum--;
            titleRow.remove("Здание");
        }

        if (reportDataWrapper.isHideUnit() && !printParent)
        {
            floorColNum--;
            placeColNum--;
            roomColNum--;
            titleRow.remove("Блок");
        }

        if (reportDataWrapper.isHideFloor() && !printParent)
        {
            placeColNum--;
            roomColNum--;
            titleRow.remove("Этаж");
        }

        if (reportDataWrapper.isHidePlace() && !printParent)
        {
            roomColNum--;
            titleRow.remove("Помещение");
        }

        //Столбцы свойств земельного участка
        _groundPropertyMap = getPropertyMapForClassObject(UniplacesGround.class, titleRow, getRoomColNum(), reportDataWrapper.isHideGround());

        int posStart = getRoomColNum() + _groundPropertyMap.size()+2;
        //Столбцы свойств здания
        _buildingPropertyMap = getPropertyMapForClassObject(UniplacesBuilding.class, titleRow, posStart, reportDataWrapper.isHideBuilding());

        posStart += _buildingPropertyMap.size()+2 ;
        //Столбцы свойств блока
        _unitPropertyMap = getPropertyMapForClassObject(UniplacesUnit.class, titleRow, posStart, reportDataWrapper.isHideUnit());

        posStart += _unitPropertyMap.size();
        //Столбцы свойств этажа
        _floorPropertyMap = getPropertyMapForClassObject(UniplacesFloor.class, titleRow, posStart, reportDataWrapper.isHideFloor());

        posStart += _floorPropertyMap.size();
        //Столбцы свойств помещения
        _placePropertyMap = getPropertyMapForClassObject(UniMireaPlace.class, titleRow, posStart, reportDataWrapper.isHidePlace());

        posStart += _placePropertyMap.size();
        //Столбцы свойств комнаты
        _roomPropertyMap = getPropertyMapForClassObject(UniplacesPlace.class, titleRow, posStart, false);

        posStart += _roomPropertyMap.size();
        //Столбцы свойств комнаты
        _roomExtPropertyMap = getPropertyMapForClassObject(UniPlacesPlaceExt.class, titleRow, posStart, false);

    }


    // Добавление данных о комнатах, не входящих в помещения
    private int addRoomWithoutPlace(ReportInfo reportInfo, WritableSheet sheet, int excelRowNumber, UniplacesFloor floor, boolean printParent) throws WriteException
    {
        if (reportInfo.getFloorToRoomMap().containsKey(floor))
        {
            for (UniplacesPlace room : reportInfo.getFloorToRoomMap().get(floor))
            {
                if (printParent)
                {
                    sheet.addCell(new Label(getGroundColNum(), excelRowNumber, floor.getUnit().getBuilding().getGround().getFullTitle(), getRowFormat()));
                    sheet.addCell(new Label(getBuildingColNum(), excelRowNumber, floor.getUnit().getBuilding().getTitle(), getRowFormat()));
                    sheet.addCell(new Label(getUnitColNum(), excelRowNumber, floor.getUnit().getTitle(), getRowFormat()));
                    sheet.addCell(new Label(getFloorColNum(), excelRowNumber, floor.getTitle(), getRowFormat()));
                }
                addRoomInfoToReport(reportInfo, sheet, excelRowNumber, getRoomColNum(), room);
                excelRowNumber++;
            }
        }
        return excelRowNumber;
    }

    //Добавление в отчет данных о помещении
    private int addPlaceInfoToReport(ReportInfo reportInfo, WritableSheet sheet, int excelRowNumber, UniplacesFloor floor, ReportDataWrapper reportData) throws WriteException
    {
        if (reportInfo.getFloorToPlaceMap().containsKey(floor))
        {
            for (UniMireaPlace place : reportInfo.getFloorToPlaceMap().get(floor))
            {
                if (!reportData.isHidePlace())
                {
                    if (reportData.isPrintParentData())
                    {
                        sheet.addCell(new Label(getGroundColNum(), excelRowNumber, floor.getUnit().getBuilding().getGround().getFullTitle(), getRowFormat()));
                        sheet.addCell(new Label(getBuildingColNum(), excelRowNumber, floor.getUnit().getBuilding().getTitle(), getRowFormat()));
                        sheet.addCell(new Label(getUnitColNum(), excelRowNumber, floor.getUnit().getTitle(), getRowFormat()));
                        sheet.addCell(new Label(getFloorColNum(), excelRowNumber, floor.getTitle(), getRowFormat()));
                    }
                    sheet.addCell(new Label(getPlaceColNum(), excelRowNumber, place.getTitle(), getRowFormat()));
                    for (String property : getPlacePropertyMap().keySet())
                        sheet.addCell(createRowCell(getPlacePropertyMap().get(property), excelRowNumber, place.getProperty(property) == null ? null : String.valueOf(place.getProperty(property))));
                }

                for (UniplacesPlace room : reportInfo.getPlaceToRoomMap().get(place))
                {
                    excelRowNumber++;
                    if (reportData.isPrintParentData())
                    {
                        sheet.addCell(new Label(getGroundColNum(), excelRowNumber, floor.getUnit().getBuilding().getGround().getFullTitle(), getRowFormat()));
                        sheet.addCell(new Label(getBuildingColNum(), excelRowNumber, floor.getUnit().getBuilding().getTitle(), getRowFormat()));
                        sheet.addCell(new Label(getUnitColNum(), excelRowNumber, floor.getUnit().getTitle(), getRowFormat()));
                        sheet.addCell(new Label(getFloorColNum(), excelRowNumber, floor.getTitle(), getRowFormat()));
                        sheet.addCell(new Label(getPlaceColNum(), excelRowNumber, place.getTitle(), getRowFormat()));
                    }
                    addRoomInfoToReport(reportInfo, sheet, excelRowNumber, getRoomColNum(), room);
                }
            }
            excelRowNumber++;
        }
        return excelRowNumber;
    }

    //Добавление в отчет данных о комнате
    private void addRoomInfoToReport(ReportInfo reportInfo, WritableSheet sheet, int excelRowNumber, int roomColNum, UniplacesPlace room) throws WriteException
    {
        sheet.addCell(new Label(roomColNum, excelRowNumber, room.getTitle(), getRowFormat()));
        for (String property : getRoomPropertyMap().keySet())
            sheet.addCell(createRowCell(getRoomPropertyMap().get(property), excelRowNumber, room.getProperty(property) == null ? null : String.valueOf(room.getProperty(property))));
        UniPlacesPlaceExt roomExt = reportInfo.getRoomToExtMap().get(room);
        if (roomExt != null)
            for (String property : getRoomExtPropertyMap().keySet())
                sheet.addCell(createRowCell(getRoomExtPropertyMap().get(property), excelRowNumber, roomExt.getProperty(property) == null ? null : String.valueOf(roomExt.getProperty(property))));
    }

    // Создание ячейки нужного типа
    private WritableCell createRowCell(int col, int row, String value)
    {
        if (value == null)
            return new Blank(col, row, getRowFormat());
        if (NumberUtils.isNumber(value))
            return new Number(col, row, Double.parseDouble(value), getRowFormat());
        return new Label(col, row, value, getRowFormat());
    }


    // Получение свойств объекта, выводимых в отчет
    private Map<String, Integer> getPropertyMapForClassObject(Class entityClass, List<String> titleRow, int startColumn, boolean hide)
    {
        if (hide) return Maps.newHashMap();

        if (entityClass == UniplacesGround.class)
            return getPropertyMapForGround(titleRow, startColumn);
        else if (entityClass == UniplacesBuilding.class)
            return getPropertyMapForBuilding(titleRow, startColumn);
        else if (entityClass == UniplacesUnit.class)
            return getPropertyMapForUnit(titleRow, startColumn);
        else if (entityClass == UniplacesFloor.class)
            return getPropertyMapForFloor(titleRow, startColumn);
        else if (entityClass == UniMireaPlace.class)
            return getPropertyMapForPlace(titleRow, startColumn);
        else if (entityClass == UniplacesPlace.class)
            return getPropertyMapForRoom(titleRow, startColumn);
        else if (entityClass == UniPlacesPlaceExt.class)
            return getPropertyMapForRoomExt(titleRow, startColumn);

        return Maps.newHashMap();
    }

    //Получение свойств объекта "Земельный участок"
    private Map<String, Integer> getPropertyMapForGround(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;
        propertyMap.put("responsibleOrgUnit.title", startColumn + count++);
        titleRow.add("Ответственное подразделение");
        propertyMap.put("comment", startColumn + count++);
        titleRow.add("Комментарий");
        propertyMap.put("disposalRight.title", startColumn + count++);
        titleRow.add("Право распоряжения");
        propertyMap.put("cadastreRecordNumber", startColumn + count++);
        titleRow.add("Кадастровый номер");
        propertyMap.put("number", startColumn + count++);
        titleRow.add("Номер");
        propertyMap.put("location", startColumn + count++);
        titleRow.add("Местоположение");
        propertyMap.put("area", startColumn + count++);
        titleRow.add("Площадь кв. м");
        propertyMap.put("balanceValue", startColumn + count++);
        titleRow.add("Балансовая стоимость");
        propertyMap.put("rightStartDate", startColumn + count++);
        titleRow.add("Дата начала распоряжения");
        propertyMap.put("rightEndDate", startColumn + count);
        titleRow.add("Дата завершения распоряжения");
        titleRow.add("Право распоряжения зем.участком(доп.)");
        titleRow.add("Площадь по доп. праву");

        return propertyMap;
    }

    //Получение свойств объекта "Здание"
    private Map<String, Integer> getPropertyMapForBuilding(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;

        propertyMap.put("responsibleOrgUnit.title", startColumn + count++);
        titleRow.add("Ответственное подразделение");
        propertyMap.put("comment", startColumn + count++);
        titleRow.add("Комментарий");
        propertyMap.put("title", startColumn + count++);
        titleRow.add("Название");
        propertyMap.put("shortTitle", startColumn + count++);
        titleRow.add("Сокращенное название");
        propertyMap.put("number", startColumn + count++);
        titleRow.add("Порядковый номер");
        propertyMap.put("fullNumber", startColumn + count++);
        titleRow.add("Номер");
        propertyMap.put("unit", startColumn + count++);
        titleRow.add("Корпус");
        propertyMap.put("disposalRight.title", startColumn + count++);
        titleRow.add("Право распоряжения");
        propertyMap.put("purpose.title", startColumn + count++);
        titleRow.add("Назначение здания");
        propertyMap.put("address.shortTitleWithFlat", startColumn + count++);
        titleRow.add("Адрес");
        propertyMap.put("identifier", startColumn + count++);
        titleRow.add("Идентификатор");
        propertyMap.put("fractionalArea", startColumn + count++);
        titleRow.add("Площадь кв. м");
        propertyMap.put("rightStartDate", startColumn + count);
        titleRow.add("Дата начала распоряжения");
        titleRow.add("Право распоряжения зданием (доп.)");
        titleRow.add("Площадь по доп. праву");

        return propertyMap;
    }

    //Получение свойств объекта "Блок"
    private Map<String, Integer> getPropertyMapForUnit(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;

        propertyMap.put("responsibleOrgUnit.title", startColumn + count++);
        titleRow.add("Ответственное подразделение");
        propertyMap.put("comment", startColumn + count++);
        titleRow.add("Комментарий");
        propertyMap.put("title", startColumn + count++);
        titleRow.add("Название");
        propertyMap.put("purpose.title", startColumn + count++);
        titleRow.add("Назначение здания");
        propertyMap.put("identifier", startColumn + count++);
        titleRow.add("Идентификатор");
        propertyMap.put("fractionalArea", startColumn + count++);
        titleRow.add("Площадь кв. м");
        propertyMap.put("floorNumber", startColumn + count++);
        titleRow.add("Этажность");
        propertyMap.put("placeNumberingType.title", startColumn + count);
        titleRow.add("Тип нумерации помещений");

        return propertyMap;
    }

    //Получение свойств объекта "Этаж"
    private Map<String, Integer> getPropertyMapForFloor(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;

//        propertyMap.put("responsibleOrgUnit.title", startColumn + count++);
//        titleRow.add("Ответственное подразделение");
//        propertyMap.put("comment", startColumn + count++);
//        titleRow.add("Комментарий");
        propertyMap.put("title", startColumn + count++);
        titleRow.add("Название");
        propertyMap.put("number", startColumn + count);
        titleRow.add("Порядковый номер");

        return propertyMap;
    }

    //Получение свойств объекта "Помещение"
    private Map<String, Integer> getPropertyMapForPlace(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;

//        propertyMap.put("responsibleOrgUnit.title", startColumn + count++);
//        titleRow.add("Ответственное подразделение");
//        propertyMap.put("comment", startColumn + count++);
//        titleRow.add("Комментарий");
        propertyMap.put("title", startColumn + count++);
        titleRow.add("Название");
        propertyMap.put("number", startColumn + count);
        titleRow.add("Порядковый номер");

        return propertyMap;
    }

    //Получение свойств объекта "Комната"
    private Map<String, Integer> getPropertyMapForRoom(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;

        propertyMap.put("responsibleOrgUnit.title", startColumn + count++);
        titleRow.add("Ответственное подразделение");
        propertyMap.put("comment", startColumn + count++);
        titleRow.add("Комментарий");
        propertyMap.put("title", startColumn + count++);
        titleRow.add("Название");
        propertyMap.put("purpose.title", startColumn + count++);
        titleRow.add("Назначение здания");
        propertyMap.put("fractionalArea", startColumn + count++);
        titleRow.add("Площадь кв. м");
        propertyMap.put("number", startColumn + count++);
        titleRow.add("Номер");
        propertyMap.put("windowNumber", startColumn + count++);
        titleRow.add("Число окон");
        propertyMap.put("condition.title", startColumn + count++);
        titleRow.add("Состояние помещения");
        propertyMap.put("classroomType.title", startColumn + count++);
        titleRow.add("Тип учебного помещения");
        propertyMap.put("capacity", startColumn + count++);
        titleRow.add("Число посадочных мест");

        propertyMap.put("fractionalHeight", startColumn + count++);
        titleRow.add("Высота помещения, м");
        propertyMap.put("fractionalLength", startColumn + count++);
        titleRow.add("Длина помещения, м");
        propertyMap.put("fractionalWidth", startColumn + count);
        titleRow.add("Ширина помещения, м");

        return propertyMap;
    }

    //Получение свойств объекта "Комната(расширение)"
    private Map<String, Integer> getPropertyMapForRoomExt(List<String> titleRow, int startColumn)
    {
        Map<String, Integer> propertyMap = new HashMap<>();
        int count = 1;

        propertyMap.put("responsiblePerson.person.fio", startColumn + count++);
        titleRow.add("Ответственное лицо");
        propertyMap.put("mayRented", startColumn + count);
        titleRow.add("Признак возможности сдачи в аренду");

        return propertyMap;
    }

    private void addAdditionalRecordProperty(UniplacesRegistryRecord record, WritableSheet sheet, int excelRowNumber, int columnNum) throws WriteException
    {
        MireaSecondDisposalRight additionalRight = DataAccessServices.dao().get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.registryRecord(), record);
        sheet.addCell(createRowCell(++columnNum, excelRowNumber, additionalRight == null? null : additionalRight.getDisposalRight().getTitle()));
        sheet.addCell(createRowCell(++columnNum, excelRowNumber, additionalRight == null? null : (additionalRight.getFractionalArea() != null ? String.valueOf(additionalRight.getFractionalArea()) : null)));
    }


}