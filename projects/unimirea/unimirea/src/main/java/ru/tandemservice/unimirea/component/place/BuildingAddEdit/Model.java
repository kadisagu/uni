/* $Id$ */
package ru.tandemservice.unimirea.component.place.BuildingAddEdit;

import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.BuildingAddEdit.Model
{

    private MireaSecondDisposalRight secondRight;
    private UniplacesDocument secondRightDocument;
    private List<HSelectOption> _documentKindList;

    public List<HSelectOption> getDocumentKindList()
    {
        return _documentKindList;
    }

    public void setDocumentKindList(List<HSelectOption> documentKindList)
    {
        _documentKindList = documentKindList;
    }

    public UniplacesDocument getSecondRightDocument()
    {
        return secondRightDocument;
    }

    public void setSecondRightDocument(UniplacesDocument secondRightDocument)
    {
        this.secondRightDocument = secondRightDocument;
    }

    public MireaSecondDisposalRight getSecondRight()
    {
        return secondRight;
    }

    public void setSecondRight(MireaSecondDisposalRight secondRight)
    {
        this.secondRight = secondRight;
    }
}
