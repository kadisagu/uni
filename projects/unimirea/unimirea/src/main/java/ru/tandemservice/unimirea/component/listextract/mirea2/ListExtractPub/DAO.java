/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class DAO extends AbstractListExtractPubDAO<MireaExcludeSuccessStuListExtract, Model> implements IDAO
{
}
