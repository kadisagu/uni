/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e17.Pub;

import ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt;

/**
 * @author Ekaterina Zvereva
 * @since 18.01.2016
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e17.Pub.Model
{
    private MireaRestorationStuExtractExt _extractExt = new MireaRestorationStuExtractExt();

    public MireaRestorationStuExtractExt getExtractExt()
    {
        return _extractExt;
    }

    public void setExtractExt(MireaRestorationStuExtractExt extractExt)
    {
        _extractExt = extractExt;
    }
}