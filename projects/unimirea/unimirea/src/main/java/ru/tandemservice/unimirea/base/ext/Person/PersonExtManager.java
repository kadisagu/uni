/* $Id$ */
package ru.tandemservice.unimirea.base.ext.Person;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import ru.tandemservice.unimirea.base.ext.Person.logic.MireaPersonDao;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IPersonDao dao()
    {
        return new MireaPersonDao();
    }
}
