package ru.tandemservice.unimirea.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unimirea.entity.gen.*;

/** @see ru.tandemservice.unimirea.entity.gen.MireaEduEnrAsTransferStuExtractExtGen */
public class MireaEduEnrAsTransferStuExtractExt extends MireaEduEnrAsTransferStuExtractExtGen
{
    public String getProtocolTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getProtocolDate()) + " № " + getProtocolNumber() ;
    }

    public String getApplicationTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getApplicationDate()) + " № " + getApplicationNumber();
    }

    public String getContractTitle()
    {
        return new StringBuilder()
                .append(getContractNumber() != null? "№ " + getContractNumber() : "")
                .append(getContractDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractDate()) : "")
                .toString();
    }
}