/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPerson.ui.AccessRestrictionAddEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */

@Configuration
public class MireaPersonAccessRestrictionAddEdit extends BusinessComponentManager
{
}
