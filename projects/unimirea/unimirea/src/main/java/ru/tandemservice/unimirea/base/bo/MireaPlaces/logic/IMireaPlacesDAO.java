/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
public interface IMireaPlacesDAO
{
    public static final String BUILDING ="building";
    public static final String UNIT ="unit";
    public static final String FLOOR ="floor";
    public static final String TITLE ="title";

    boolean existRelatedObjects(Long placeId);
}