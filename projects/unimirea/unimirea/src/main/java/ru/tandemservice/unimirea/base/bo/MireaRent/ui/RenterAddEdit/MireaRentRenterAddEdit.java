/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaRent.ui.RenterAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;


/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
@Configuration
public class MireaRentRenterAddEdit extends BusinessComponentManager
{
    public static final String CONTRACTOR_DS = "contractorDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(CONTRACTOR_DS, createContractorDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler createContractorDS()
    {
        return new EntityComboDataSourceHandler(getName(), ExternalOrgUnit.class);
    }

}