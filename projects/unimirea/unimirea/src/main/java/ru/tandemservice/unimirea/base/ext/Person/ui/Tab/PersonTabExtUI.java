/* $Id$ */
package ru.tandemservice.unimirea.base.ext.Person.ui.Tab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */
public class PersonTabExtUI  extends UIAddon
{
    public PersonTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}
