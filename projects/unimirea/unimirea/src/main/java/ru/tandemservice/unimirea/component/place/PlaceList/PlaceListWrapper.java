/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 19.02.2015
 */
public class PlaceListWrapper extends IdentifiableWrapper<UniplacesPlace>
{

    private List<ContractorListWrapper> _contractorList;
    private UniplacesPlace _place;
    private UniPlacesPlaceExt _placeExt;

    public PlaceListWrapper(Long id, String title)
    {
        super(id, title);
    }

    public List<ContractorListWrapper> getContractorList()
    {
        return _contractorList;
    }

    public void setContractorList(List<ContractorListWrapper> contractorList)
    {
        _contractorList = contractorList;
    }

    public UniplacesPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniplacesPlace place)
    {
        _place = place;
    }

    public UniPlacesPlaceExt getPlaceExt()
    {
        return _placeExt;
    }

    public void setPlaceExt(UniPlacesPlaceExt placeExt)
    {
        _placeExt = placeExt;
    }
}