/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.text.SimpleDateFormat;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class MireaExcludeSuccessStuListExtractPrint implements IPrintFormCreator<MireaExcludeSuccessStuListExtract>, IListParagraphPrintFormCreator<MireaExcludeSuccessStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, MireaExcludeSuccessStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, MireaExcludeSuccessStuListExtract firstExtract)
    {
        CommonExtractPrint.initDevelopForm(modifier, firstExtract.getEntity().getGroup().getEducationOrgUnit(), "");
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, MireaExcludeSuccessStuListExtract currentExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, currentExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, MireaExcludeSuccessStuListExtract firstExtract)
    {
        return null;
    }
}