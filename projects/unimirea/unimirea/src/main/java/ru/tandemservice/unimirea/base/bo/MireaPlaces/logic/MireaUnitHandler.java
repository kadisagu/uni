/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class MireaUnitHandler extends DefaultComboDataSourceHandler
{
    public MireaUnitHandler(String ownerId)
    {
        super(ownerId, UniplacesUnit.class, UniplacesUnit.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Object building = ep.context.get(IMireaPlacesDAO.BUILDING);
        if (building != null)
            ep.dqlBuilder.where(eq(property(UniplacesUnit.building().fromAlias("e")), commonValue(building)));
    }
}