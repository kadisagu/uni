/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.logic.IMireaPlacesDAO;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.AddEdit.MireaPlacesAddEdit;


/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
public class MireaPlacesListUI extends UIPresenter
{

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(MireaPlacesList.PLACES_DS))
        {
            dataSource.put(IMireaPlacesDAO.BUILDING, getSettings().get(IMireaPlacesDAO.BUILDING));
            dataSource.put(IMireaPlacesDAO.UNIT, getSettings().get(IMireaPlacesDAO.UNIT));
            dataSource.put(IMireaPlacesDAO.FLOOR, getSettings().get(IMireaPlacesDAO.FLOOR));
            dataSource.put(IMireaPlacesDAO.TITLE, getSettings().get(IMireaPlacesDAO.TITLE));
        }
        else if (dataSource.getName().equals(MireaPlacesList.UNITS_DS))
        {
            dataSource.put(IMireaPlacesDAO.BUILDING, getSettings().get(IMireaPlacesDAO.BUILDING));
        }
        else if (dataSource.getName().equals(MireaPlacesList.FLOORS_DS))
        {
            dataSource.put(IMireaPlacesDAO.BUILDING, getSettings().get(IMireaPlacesDAO.BUILDING));
            dataSource.put(IMireaPlacesDAO.UNIT, getSettings().get(IMireaPlacesDAO.UNIT));
        }

    }

    @Override
    public String getSettingsKey()
    {
        return "MireaPlaceList.filter";
    }

    public void onEditPlaceClick()
    {
        _uiActivation.asRegionDialog(MireaPlacesAddEdit.class).parameter("placeId", getListenerParameterAsLong()).activate();
    }

    public void onClickAddMireaPlace()
    {
        _uiActivation.asRegionDialog(MireaPlacesAddEdit.class).activate();
    }

    public void onDeletePlaceClick()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickSearch()
    {
        getSettings().clear();
    }
}