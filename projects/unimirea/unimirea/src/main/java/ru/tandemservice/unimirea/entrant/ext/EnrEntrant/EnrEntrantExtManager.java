/* $Id$ */
package ru.tandemservice.unimirea.entrant.ext.EnrEntrant;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 30.09.2015
 */
@Configuration
public class EnrEntrantExtManager extends BusinessObjectExtensionManager
{
}