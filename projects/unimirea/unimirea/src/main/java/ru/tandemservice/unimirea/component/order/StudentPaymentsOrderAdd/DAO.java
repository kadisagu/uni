/* $Id$ */
package ru.tandemservice.unimirea.component.order.StudentPaymentsOrderAdd;

import ru.tandemservice.unimirea.entity.MireaStudentPaymentsOrderExt;
import ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd.Model;

/**
 * @author Andrey Avetisov
 * @since 26.02.2016
 */
public class DAO extends ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd.DAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        ru.tandemservice.unimirea.component.order.StudentPaymentsOrderAdd.Model modelExt = (ru.tandemservice.unimirea.component.order.StudentPaymentsOrderAdd.Model) model;
        if (null != modelExt.getOrder().getId())
        {
            MireaStudentPaymentsOrderExt paymentsOrderExt = get(MireaStudentPaymentsOrderExt.class, MireaStudentPaymentsOrderExt.studentPaymentsOrder().id(), model.getOrder().getId());
            if (paymentsOrderExt != null)
            {
                modelExt.setPaymentsOrderExt(paymentsOrderExt);
            }
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        ru.tandemservice.unimirea.component.order.StudentPaymentsOrderAdd.Model modelExt = (ru.tandemservice.unimirea.component.order.StudentPaymentsOrderAdd.Model) model;
        modelExt.getPaymentsOrderExt().setStudentPaymentsOrder(model.getOrder());
        getSession().saveOrUpdate(modelExt.getPaymentsOrderExt());
    }
}
