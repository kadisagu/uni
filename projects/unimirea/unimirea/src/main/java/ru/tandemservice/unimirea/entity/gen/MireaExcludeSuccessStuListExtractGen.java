package ru.tandemservice.unimirea.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об отчислении (вариант 3), диплом с отличием»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MireaExcludeSuccessStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract";
    public static final String ENTITY_NAME = "mireaExcludeSuccessStuListExtract";
    public static final int VERSION_HASH = 1747304794;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS_OLD = "statusOld";
    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String P_PREV_GRADUATE_SUCCESS_ORDER_DATE = "prevGraduateSuccessOrderDate";
    public static final String P_PREV_GRADUATE_SUCCESS_ORDER_NUMBER = "prevGraduateSuccessOrderNumber";
    public static final String P_PREV_EXCLUDE_ORDER_DATE = "prevExcludeOrderDate";
    public static final String P_PREV_EXCLUDE_ORDER_NUMBER = "prevExcludeOrderNumber";
    public static final String P_FINISHED_YEAR = "finishedYear";
    public static final String P_HOLIDAY_FROM = "holidayFrom";
    public static final String P_HOLIDAY_TO = "holidayTo";
    public static final String P_ORDER_TEXT = "orderText";

    private StudentStatus _statusOld;     // Статус до проведения приказа
    private Date _excludeDate;     // Дата отчисления
    private Date _prevGraduateSuccessOrderDate;     // Дата предыдущего приказа о выпуске (с отличием)
    private String _prevGraduateSuccessOrderNumber;     // Номер предыдущего приказа о выпуске (с отличием)
    private Date _prevExcludeOrderDate;     // Дата предыдущего приказа об отчислении
    private String _prevExcludeOrderNumber;     // Номер предыдущего приказа об отчислении
    private Integer _finishedYear;     // Год окончания на момент проведения приказа
    private Date _holidayFrom;     // Начало каникул
    private Date _holidayTo;     // Окончание каникул
    private String _orderText;     // Текст основания для приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Статус до проведения приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Статус до проведения приказа. Свойство не может быть null.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     */
    public Date getPrevGraduateSuccessOrderDate()
    {
        initLazyForGet("prevGraduateSuccessOrderDate");
        return _prevGraduateSuccessOrderDate;
    }

    /**
     * @param prevGraduateSuccessOrderDate Дата предыдущего приказа о выпуске (с отличием).
     */
    public void setPrevGraduateSuccessOrderDate(Date prevGraduateSuccessOrderDate)
    {
        initLazyForSet("prevGraduateSuccessOrderDate");
        dirty(_prevGraduateSuccessOrderDate, prevGraduateSuccessOrderDate);
        _prevGraduateSuccessOrderDate = prevGraduateSuccessOrderDate;
    }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     */
    @Length(max=255)
    public String getPrevGraduateSuccessOrderNumber()
    {
        initLazyForGet("prevGraduateSuccessOrderNumber");
        return _prevGraduateSuccessOrderNumber;
    }

    /**
     * @param prevGraduateSuccessOrderNumber Номер предыдущего приказа о выпуске (с отличием).
     */
    public void setPrevGraduateSuccessOrderNumber(String prevGraduateSuccessOrderNumber)
    {
        initLazyForSet("prevGraduateSuccessOrderNumber");
        dirty(_prevGraduateSuccessOrderNumber, prevGraduateSuccessOrderNumber);
        _prevGraduateSuccessOrderNumber = prevGraduateSuccessOrderNumber;
    }

    /**
     * @return Дата предыдущего приказа об отчислении.
     */
    public Date getPrevExcludeOrderDate()
    {
        initLazyForGet("prevExcludeOrderDate");
        return _prevExcludeOrderDate;
    }

    /**
     * @param prevExcludeOrderDate Дата предыдущего приказа об отчислении.
     */
    public void setPrevExcludeOrderDate(Date prevExcludeOrderDate)
    {
        initLazyForSet("prevExcludeOrderDate");
        dirty(_prevExcludeOrderDate, prevExcludeOrderDate);
        _prevExcludeOrderDate = prevExcludeOrderDate;
    }

    /**
     * @return Номер предыдущего приказа об отчислении.
     */
    @Length(max=255)
    public String getPrevExcludeOrderNumber()
    {
        initLazyForGet("prevExcludeOrderNumber");
        return _prevExcludeOrderNumber;
    }

    /**
     * @param prevExcludeOrderNumber Номер предыдущего приказа об отчислении.
     */
    public void setPrevExcludeOrderNumber(String prevExcludeOrderNumber)
    {
        initLazyForSet("prevExcludeOrderNumber");
        dirty(_prevExcludeOrderNumber, prevExcludeOrderNumber);
        _prevExcludeOrderNumber = prevExcludeOrderNumber;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    /**
     * @return Начало каникул.
     */
    public Date getHolidayFrom()
    {
        return _holidayFrom;
    }

    /**
     * @param holidayFrom Начало каникул.
     */
    public void setHolidayFrom(Date holidayFrom)
    {
        dirty(_holidayFrom, holidayFrom);
        _holidayFrom = holidayFrom;
    }

    /**
     * @return Окончание каникул.
     */
    public Date getHolidayTo()
    {
        return _holidayTo;
    }

    /**
     * @param holidayTo Окончание каникул.
     */
    public void setHolidayTo(Date holidayTo)
    {
        dirty(_holidayTo, holidayTo);
        _holidayTo = holidayTo;
    }

    /**
     * @return Текст основания для приказа.
     */
    @Length(max=255)
    public String getOrderText()
    {
        return _orderText;
    }

    /**
     * @param orderText Текст основания для приказа.
     */
    public void setOrderText(String orderText)
    {
        dirty(_orderText, orderText);
        _orderText = orderText;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof MireaExcludeSuccessStuListExtractGen)
        {
            setStatusOld(((MireaExcludeSuccessStuListExtract)another).getStatusOld());
            setExcludeDate(((MireaExcludeSuccessStuListExtract)another).getExcludeDate());
            setPrevGraduateSuccessOrderDate(((MireaExcludeSuccessStuListExtract)another).getPrevGraduateSuccessOrderDate());
            setPrevGraduateSuccessOrderNumber(((MireaExcludeSuccessStuListExtract)another).getPrevGraduateSuccessOrderNumber());
            setPrevExcludeOrderDate(((MireaExcludeSuccessStuListExtract)another).getPrevExcludeOrderDate());
            setPrevExcludeOrderNumber(((MireaExcludeSuccessStuListExtract)another).getPrevExcludeOrderNumber());
            setFinishedYear(((MireaExcludeSuccessStuListExtract)another).getFinishedYear());
            setHolidayFrom(((MireaExcludeSuccessStuListExtract)another).getHolidayFrom());
            setHolidayTo(((MireaExcludeSuccessStuListExtract)another).getHolidayTo());
            setOrderText(((MireaExcludeSuccessStuListExtract)another).getOrderText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MireaExcludeSuccessStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MireaExcludeSuccessStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new MireaExcludeSuccessStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return obj.getStatusOld();
                case "excludeDate":
                    return obj.getExcludeDate();
                case "prevGraduateSuccessOrderDate":
                    return obj.getPrevGraduateSuccessOrderDate();
                case "prevGraduateSuccessOrderNumber":
                    return obj.getPrevGraduateSuccessOrderNumber();
                case "prevExcludeOrderDate":
                    return obj.getPrevExcludeOrderDate();
                case "prevExcludeOrderNumber":
                    return obj.getPrevExcludeOrderNumber();
                case "finishedYear":
                    return obj.getFinishedYear();
                case "holidayFrom":
                    return obj.getHolidayFrom();
                case "holidayTo":
                    return obj.getHolidayTo();
                case "orderText":
                    return obj.getOrderText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "prevGraduateSuccessOrderDate":
                    obj.setPrevGraduateSuccessOrderDate((Date) value);
                    return;
                case "prevGraduateSuccessOrderNumber":
                    obj.setPrevGraduateSuccessOrderNumber((String) value);
                    return;
                case "prevExcludeOrderDate":
                    obj.setPrevExcludeOrderDate((Date) value);
                    return;
                case "prevExcludeOrderNumber":
                    obj.setPrevExcludeOrderNumber((String) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
                case "holidayFrom":
                    obj.setHolidayFrom((Date) value);
                    return;
                case "holidayTo":
                    obj.setHolidayTo((Date) value);
                    return;
                case "orderText":
                    obj.setOrderText((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                        return true;
                case "excludeDate":
                        return true;
                case "prevGraduateSuccessOrderDate":
                        return true;
                case "prevGraduateSuccessOrderNumber":
                        return true;
                case "prevExcludeOrderDate":
                        return true;
                case "prevExcludeOrderNumber":
                        return true;
                case "finishedYear":
                        return true;
                case "holidayFrom":
                        return true;
                case "holidayTo":
                        return true;
                case "orderText":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return true;
                case "excludeDate":
                    return true;
                case "prevGraduateSuccessOrderDate":
                    return true;
                case "prevGraduateSuccessOrderNumber":
                    return true;
                case "prevExcludeOrderDate":
                    return true;
                case "prevExcludeOrderNumber":
                    return true;
                case "finishedYear":
                    return true;
                case "holidayFrom":
                    return true;
                case "holidayTo":
                    return true;
                case "orderText":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "statusOld":
                    return StudentStatus.class;
                case "excludeDate":
                    return Date.class;
                case "prevGraduateSuccessOrderDate":
                    return Date.class;
                case "prevGraduateSuccessOrderNumber":
                    return String.class;
                case "prevExcludeOrderDate":
                    return Date.class;
                case "prevExcludeOrderNumber":
                    return String.class;
                case "finishedYear":
                    return Integer.class;
                case "holidayFrom":
                    return Date.class;
                case "holidayTo":
                    return Date.class;
                case "orderText":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MireaExcludeSuccessStuListExtract> _dslPath = new Path<MireaExcludeSuccessStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MireaExcludeSuccessStuListExtract");
    }
            

    /**
     * @return Статус до проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevGraduateSuccessOrderDate()
     */
    public static PropertyPath<Date> prevGraduateSuccessOrderDate()
    {
        return _dslPath.prevGraduateSuccessOrderDate();
    }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevGraduateSuccessOrderNumber()
     */
    public static PropertyPath<String> prevGraduateSuccessOrderNumber()
    {
        return _dslPath.prevGraduateSuccessOrderNumber();
    }

    /**
     * @return Дата предыдущего приказа об отчислении.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevExcludeOrderDate()
     */
    public static PropertyPath<Date> prevExcludeOrderDate()
    {
        return _dslPath.prevExcludeOrderDate();
    }

    /**
     * @return Номер предыдущего приказа об отчислении.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevExcludeOrderNumber()
     */
    public static PropertyPath<String> prevExcludeOrderNumber()
    {
        return _dslPath.prevExcludeOrderNumber();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    /**
     * @return Начало каникул.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getHolidayFrom()
     */
    public static PropertyPath<Date> holidayFrom()
    {
        return _dslPath.holidayFrom();
    }

    /**
     * @return Окончание каникул.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getHolidayTo()
     */
    public static PropertyPath<Date> holidayTo()
    {
        return _dslPath.holidayTo();
    }

    /**
     * @return Текст основания для приказа.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getOrderText()
     */
    public static PropertyPath<String> orderText()
    {
        return _dslPath.orderText();
    }

    public static class Path<E extends MireaExcludeSuccessStuListExtract> extends ListStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _statusOld;
        private PropertyPath<Date> _excludeDate;
        private PropertyPath<Date> _prevGraduateSuccessOrderDate;
        private PropertyPath<String> _prevGraduateSuccessOrderNumber;
        private PropertyPath<Date> _prevExcludeOrderDate;
        private PropertyPath<String> _prevExcludeOrderNumber;
        private PropertyPath<Integer> _finishedYear;
        private PropertyPath<Date> _holidayFrom;
        private PropertyPath<Date> _holidayTo;
        private PropertyPath<String> _orderText;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Статус до проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(MireaExcludeSuccessStuListExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Дата предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevGraduateSuccessOrderDate()
     */
        public PropertyPath<Date> prevGraduateSuccessOrderDate()
        {
            if(_prevGraduateSuccessOrderDate == null )
                _prevGraduateSuccessOrderDate = new PropertyPath<Date>(MireaExcludeSuccessStuListExtractGen.P_PREV_GRADUATE_SUCCESS_ORDER_DATE, this);
            return _prevGraduateSuccessOrderDate;
        }

    /**
     * @return Номер предыдущего приказа о выпуске (с отличием).
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevGraduateSuccessOrderNumber()
     */
        public PropertyPath<String> prevGraduateSuccessOrderNumber()
        {
            if(_prevGraduateSuccessOrderNumber == null )
                _prevGraduateSuccessOrderNumber = new PropertyPath<String>(MireaExcludeSuccessStuListExtractGen.P_PREV_GRADUATE_SUCCESS_ORDER_NUMBER, this);
            return _prevGraduateSuccessOrderNumber;
        }

    /**
     * @return Дата предыдущего приказа об отчислении.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevExcludeOrderDate()
     */
        public PropertyPath<Date> prevExcludeOrderDate()
        {
            if(_prevExcludeOrderDate == null )
                _prevExcludeOrderDate = new PropertyPath<Date>(MireaExcludeSuccessStuListExtractGen.P_PREV_EXCLUDE_ORDER_DATE, this);
            return _prevExcludeOrderDate;
        }

    /**
     * @return Номер предыдущего приказа об отчислении.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getPrevExcludeOrderNumber()
     */
        public PropertyPath<String> prevExcludeOrderNumber()
        {
            if(_prevExcludeOrderNumber == null )
                _prevExcludeOrderNumber = new PropertyPath<String>(MireaExcludeSuccessStuListExtractGen.P_PREV_EXCLUDE_ORDER_NUMBER, this);
            return _prevExcludeOrderNumber;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(MireaExcludeSuccessStuListExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

    /**
     * @return Начало каникул.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getHolidayFrom()
     */
        public PropertyPath<Date> holidayFrom()
        {
            if(_holidayFrom == null )
                _holidayFrom = new PropertyPath<Date>(MireaExcludeSuccessStuListExtractGen.P_HOLIDAY_FROM, this);
            return _holidayFrom;
        }

    /**
     * @return Окончание каникул.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getHolidayTo()
     */
        public PropertyPath<Date> holidayTo()
        {
            if(_holidayTo == null )
                _holidayTo = new PropertyPath<Date>(MireaExcludeSuccessStuListExtractGen.P_HOLIDAY_TO, this);
            return _holidayTo;
        }

    /**
     * @return Текст основания для приказа.
     * @see ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract#getOrderText()
     */
        public PropertyPath<String> orderText()
        {
            if(_orderText == null )
                _orderText = new PropertyPath<String>(MireaExcludeSuccessStuListExtractGen.P_ORDER_TEXT, this);
            return _orderText;
        }

        public Class getEntityClass()
        {
            return MireaExcludeSuccessStuListExtract.class;
        }

        public String getEntityName()
        {
            return "mireaExcludeSuccessStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
