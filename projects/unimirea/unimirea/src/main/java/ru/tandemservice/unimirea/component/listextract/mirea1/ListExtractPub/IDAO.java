/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.mirea1.ListExtractPub;


import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public interface IDAO extends IAbstractListExtractPubDAO<MireaExcludeStuListExtract, Model>
{
}
