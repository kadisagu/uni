/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.MireaPlacesManager;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.logic.IMireaPlacesDAO;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
@Input(
        @Bind(key = "placeId", binding = "placeId")
)
public class MireaPlacesAddEditUI extends UIPresenter
{
    private Long _placeId;
    private UniMireaPlace _place;
    private UniplacesBuilding _building;
    private UniplacesUnit _unit;
    private boolean _disabledChange;

    public boolean isDisabledChange()
    {
        return _disabledChange;
    }

    public Long getPlaceId()
    {
        return _placeId;
    }

    public void setPlaceId(Long placeId)
    {
        _placeId = placeId;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    public UniMireaPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniMireaPlace place)
    {
        _place = place;
    }

    @Override
    public void onComponentRefresh()
    {
        if (_placeId != null)
        {
            _place = DataAccessServices.dao().getNotNull(_placeId);
            _unit = _place.getFloor().getUnit();
            _building = _place.getFloor().getUnit().getBuilding();
            _disabledChange = MireaPlacesManager.instance().dao().existRelatedObjects(_placeId);
        }
        else
            _place = new UniMireaPlace();

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(MireaPlacesAddEdit.UNITS_DS))
        {
            dataSource.put(IMireaPlacesDAO.BUILDING, getBuilding());
        }
        else if (dataSource.getName().equals(MireaPlacesAddEdit.FLOORS_DS))
        {
            dataSource.put(IMireaPlacesDAO.BUILDING, getBuilding());
            dataSource.put(IMireaPlacesDAO.UNIT, getUnit());
        }
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(getPlace());
        deactivate();
    }
}