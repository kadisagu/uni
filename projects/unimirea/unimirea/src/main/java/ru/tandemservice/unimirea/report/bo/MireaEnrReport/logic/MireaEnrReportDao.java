/* $Id$ */
package ru.tandemservice.unimirea.report.bo.MireaEnrReport.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unimirea.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unimirea.report.bo.MireaEnrReport.ui.PersonalFileCoverAdd.MireaEnrReportPersonalFileCoverAddUI;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
public class MireaEnrReportDao extends UniBaseDao implements IMireaEnrReportDao
{
    @Override
    public byte[] createMassPersonalFileReport(MireaEnrReportPersonalFileCoverAddUI reportParams)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "e")
                .column("e.id")
                .where(eqValue(property("e", EnrEntrantRequest.entrant().enrollmentCampaign()), reportParams.getEnrollmentCampaign()))
                .where(eqValue(property("e", EnrEntrantRequest.entrant().state().code()), EnrEntrantStateCodes.ENROLLED))
                .joinPath(DQLJoinType.inner, EnrEntrantRequest.entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME));


        if (reportParams.getEnrollmentOrderList() != null && !reportParams.getEnrollmentOrderList().isEmpty())
        {
            dql.where(exists(EnrEnrollmentExtract.class,
                             EnrEnrollmentExtract.entity().request().s(), property("e"),
                             EnrEnrollmentExtract.paragraph().order().s(), reportParams.getEnrollmentOrderList()));
        }

        Iterator<Long> iterator = createStatement(dql).<Long>list().iterator();

        if (!iterator.hasNext())
            throw new ApplicationException("Данные для построения отчета отсутствуют.");

        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        final IRtfControl pageBreak = elementFactory.createRtfControl(IRtfData.PAGE);
        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.MIREA_ENTRANT_PERSONAL_FILE_COVER);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
        final RtfDocument mainDoc = (RtfDocument) scriptResult.get(IScriptExecutor.DOCUMENT);
        final List<IRtfElement> mainElementList = mainDoc.getElementList();
        while (iterator.hasNext())
        {
            mainElementList.add(pageBreak);
            scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
            RtfDocument nextDoc = (RtfDocument) scriptResult.get(IScriptExecutor.DOCUMENT);
            mainElementList.addAll(nextDoc.getElementList());
        }

        return RtfUtil.toByteArray(mainDoc);
    }
}