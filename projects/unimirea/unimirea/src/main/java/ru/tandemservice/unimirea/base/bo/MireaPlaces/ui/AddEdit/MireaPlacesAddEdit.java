/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.MireaPlacesManager;

/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
@Configuration
public class MireaPlacesAddEdit extends BusinessComponentManager
{
    public static final String BUILDINGS_DS ="buildingDS";
    public static final String UNITS_DS ="unitDS";
    public static final String FLOORS_DS ="floorDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BUILDINGS_DS, MireaPlacesManager.instance().createBuildingHandler()))
                .addDataSource(selectDS(UNITS_DS, MireaPlacesManager.instance().createUnitHandler()))
                .addDataSource(selectDS(FLOORS_DS, MireaPlacesManager.instance().createFloorHandler()))
                .create();
    }
}