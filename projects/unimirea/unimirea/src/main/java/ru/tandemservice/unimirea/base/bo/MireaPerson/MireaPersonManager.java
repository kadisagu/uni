/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */

@Configuration
public class MireaPersonManager extends BusinessObjectManager
{
    public static MireaPersonManager instance()
    {
        return instance(MireaPersonManager.class);
    }
}
