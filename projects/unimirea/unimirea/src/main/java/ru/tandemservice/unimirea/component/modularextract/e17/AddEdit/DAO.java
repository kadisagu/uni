/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract.e17.AddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.modularextract.e17.AddEdit.Model;
import ru.tandemservice.unimirea.entity.MireaRestorationStuExtractExt;

/**
 * @author Ekaterina Zvereva
 * @since 18.01.2016
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e17.AddEdit.DAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        ru.tandemservice.unimirea.component.modularextract.e17.AddEdit.Model modelExt = (ru.tandemservice.unimirea.component.modularextract.e17.AddEdit.Model) model;
        MireaRestorationStuExtractExt extractExt = DataAccessServices.dao().get(MireaRestorationStuExtractExt.class, MireaRestorationStuExtractExt.restorationStuExtractExt(), model.getExtract());
        if (extractExt != null)
            modelExt.setExtractExt(extractExt);
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        ru.tandemservice.unimirea.component.modularextract.e17.AddEdit.Model modelExt = (ru.tandemservice.unimirea.component.modularextract.e17.AddEdit.Model) model;
        modelExt.getExtractExt().setRestorationStuExtractExt(model.getExtract());
        if (model.getExtract().getCompensationTypeNew().isBudget())
        {
            modelExt.getExtractExt().setContractDate(null);
            modelExt.getExtractExt().setContractNumber(null);
        }
        saveOrUpdate(modelExt.getExtractExt());

    }
}