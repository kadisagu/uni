/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2;

import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class MireaExcludeSuccessStuListExtractDao extends UniBaseDao implements IExtractComponentDao<MireaExcludeSuccessStuListExtract>
{
    @Override
    public void doCommit(MireaExcludeSuccessStuListExtract extract, Map parameters)
    {
        //отключено, т.к. нет печатной формы выписки
//        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getExcludeOrderDate());
            extract.setPrevOrderNumber(orderData.getExcludeOrderNumber());
            extract.setPrevGraduateSuccessOrderDate(orderData.getGraduateSuccessDiplomaOrderDate());
            extract.setPrevGraduateSuccessOrderNumber(orderData.getGraduateSuccessDiplomaOrderNumber());
        }

        extract.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate()));

        orderData.setGraduateSuccessDiplomaOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setGraduateSuccessDiplomaOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setExcludeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setExcludeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(MireaExcludeSuccessStuListExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStatusOld());
        extract.getEntity().setFinishYear(extract.getFinishedYear());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        orderData.setExcludeOrderDate(extract.getPrevExcludeOrderDate());
        orderData.setExcludeOrderNumber(extract.getPrevExcludeOrderNumber());
        orderData.setGraduateSuccessDiplomaOrderDate(extract.getPrevGraduateSuccessOrderDate());
        orderData.setGraduateSuccessDiplomaOrderNumber(extract.getPrevGraduateSuccessOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}