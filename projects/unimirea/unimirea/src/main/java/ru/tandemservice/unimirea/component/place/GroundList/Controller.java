/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class Controller extends ru.tandemservice.uniplaces.component.place.GroundList.Controller
{
    @Override
    public void prepareDataSource(IBusinessComponent component)
    {
        super.prepareDataSource(component);
        DynamicListDataSource<UniplacesGround> dataSource = getModel(component).getDataSource();
        if (dataSource.getColumn(DAO.STRING_DISPOSAL_RIGHT_SECOND + "." + MireaSecondDisposalRight.disposalRight().title()) == null)
        {
            dataSource.addColumn(new SimpleColumn("Право распоряжения (дополнительное)", DAO.STRING_DISPOSAL_RIGHT_SECOND + "." + MireaSecondDisposalRight.disposalRight().title())
                                         .setClickable(false).setWidth("70px"), 7);
            dataSource.addColumn(new SimpleColumn("Площадь по дополнительному праву, кв.м", DAO.STRING_DISPOSAL_RIGHT_SECOND + "." + MireaSecondDisposalRight.fractionalArea(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED)
                                         .setClickable(false).setWidth("50px"), 8);
        }

    }
}
