/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPlaces;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.unimirea.base.bo.MireaPlaces.logic.*;


/**
 * @author Ekaterina Zvereva
 * @since 09.02.2015
 */
@Configuration
public class MireaPlacesManager extends BusinessObjectManager
{
    public static MireaPlacesManager instance() {return instance(MireaPlacesManager.class);}

    @Bean
    public IMireaPlacesDAO dao()
    {
        return new MireaPlacesDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler createPlacesDataSource()
    {
        return new MireaPlacesHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler createBuildingHandler()
    {
        return new MireaBuildingHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler createFloorHandler()
    {
        return new MireaFloorHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler createUnitHandler()
    {
        return new MireaUnitHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler createDocumentsDataSource()
    {
        return new MireaPlaceObjectDocumentHandler(getName());
    }
}