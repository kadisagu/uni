/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;


import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 24.02.2015
 */
public class MireaReportUnitsDSHandler extends DefaultComboDataSourceHandler
{
    public MireaReportUnitsDSHandler(String ownerId)
    {
        super(ownerId, UniplacesUnit.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Object building = context.get(IMireaReportsDAO.BUILDINGS);
        if (building == null || ((Collection) building).size() != 1)
            return ListOutputBuilder.get(input, Collections.emptyList()).build();
        return super.execute(input, context);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Object building = ep.context.get(IMireaReportsDAO.BUILDINGS);
        ep.dqlBuilder.where(in(property("e", UniplacesUnit.building()), (Collection) building));
    }

}