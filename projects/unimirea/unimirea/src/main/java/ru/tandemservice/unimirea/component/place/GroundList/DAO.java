/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.component.place.GroundList.Model;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesGround;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.GroundList.DAO
{
    public static String STRING_DISPOSAL_RIGHT_SECOND = "secongDisposalRight";

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<UniplacesGround> dataSource = model.getDataSource();

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesGround.class, "ground");
        orderRegistry.addAdditionalAlias(OrgUnitType.class, "orgUnitType").addAdditionalAlias(OrgUnit.class, "orgUnit");
        orderRegistry.addAdditionalAlias(MireaSecondDisposalRight.class, "secongDisposalRight");
        orderRegistry.addAdditionalAlias(UniplacesBuilding.class, "right");
        orderRegistry.setOrders(UniplacesBuilding.responsibleOrgUnit().fullTitle(), new OrderDescription("orgUnitType", OrgUnitType.title()), new OrderDescription("orgUnit", OrgUnit.title()));
        orderRegistry.setOrders(MireaSecondDisposalRight.disposalRight().title(), new OrderDescription("right", UniplacesDisposalRight.title()), new OrderDescription("building", UniplacesBuilding.number()));

        DQLSelectBuilder builder = orderRegistry.buildDQLSelectBuilder().column("ground")
                .joinEntity("ground", DQLJoinType.left, MireaSecondDisposalRight.class, "secongDisposalRight",
                            eq(property("ground.id"), property(MireaSecondDisposalRight.registryRecord().fromAlias("secongDisposalRight"))))
                .joinPath(DQLJoinType.left, UniplacesGround.responsibleOrgUnit().fromAlias("ground"), "orgUnit")
                .joinPath(DQLJoinType.left, OrgUnit.orgUnitType().fromAlias("orgUnit"), "orgUnitType")
                .joinPath(DQLJoinType.left, MireaSecondDisposalRight.disposalRight().fromAlias("secongDisposalRight"), "right");

        orderRegistry.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
        List<ViewWrapper<UniplacesGround>> list = ViewWrapper.getPatchedList(model.getDataSource());
        for(ViewWrapper<UniplacesGround> item : list)
        {
            MireaSecondDisposalRight secondRight = get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.registryRecord(), item.getEntity());
            item.setViewProperty(STRING_DISPOSAL_RIGHT_SECOND, secondRight);
        }
    }
}
