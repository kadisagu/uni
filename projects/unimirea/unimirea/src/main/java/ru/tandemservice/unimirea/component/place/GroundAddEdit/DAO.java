/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundAddEdit;

import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.component.place.GroundAddEdit.Model;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocument;
import ru.tandemservice.uniplaces.entity.place.UniplacesDocumentRight;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.GroundAddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        MireaSecondDisposalRight secondDisposalRight = get(MireaSecondDisposalRight.class, MireaSecondDisposalRight.L_REGISTRY_RECORD, model.getGround());
        if (secondDisposalRight == null)
            secondDisposalRight = new MireaSecondDisposalRight();
        ((ru.tandemservice.unimirea.component.place.GroundAddEdit.Model)model).setSecondRight(secondDisposalRight);
        ((ru.tandemservice.unimirea.component.place.GroundAddEdit.Model) model).setSecondRightDocument(secondDisposalRight.getDocument() == null ?
                                                                         new UniplacesDocumentRight() : secondDisposalRight.getDocument());
    }


    @Override
    public void update(Model model)
    {
        super.update(model);

        UniplacesDocument document = ((ru.tandemservice.unimirea.component.place.GroundAddEdit.Model)model).getSecondRightDocument();
        MireaSecondDisposalRight secondRight = ((ru.tandemservice.unimirea.component.place.GroundAddEdit.Model) model).getSecondRight();

        if (secondRight.getDisposalRight() == null)
        {
            if (secondRight.getId() != null)
                delete(secondRight);
            return;
        }

        UniplacesDocument oldDoc = secondRight.getDocument();
        if (document.getKind() != null)
        {
            saveOrUpdate(document);
            secondRight.setDocument(document);

        }
        else secondRight.setDocument(null);

        secondRight.setRegistryRecord(model.getGround());
        saveOrUpdate(secondRight);

        if (oldDoc != null && !oldDoc.equals(document))
            delete(oldDoc);
    }
}
