/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaRent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimirea.base.bo.MireaRent.logic.IMireaRentDAO;
import ru.tandemservice.unimirea.base.bo.MireaRent.logic.MireaRentDAO;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
@Configuration
public class MireaRentManager extends BusinessObjectManager
{
    public static MireaRentManager instance() {return instance(MireaRentManager.class);}

    @Bean
    public IMireaRentDAO dao()
    {
        return new MireaRentDAO();
    }

}