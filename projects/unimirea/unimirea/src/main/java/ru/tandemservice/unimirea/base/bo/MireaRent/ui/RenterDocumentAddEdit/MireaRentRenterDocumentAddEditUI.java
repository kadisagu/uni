/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaRent.ui.RenterDocumentAddEdit;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.BaseSelectModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unimirea.base.bo.MireaRent.MireaRentManager;
import ru.tandemservice.unimirea.entity.MireaPlaceRentToDocumentRelation;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
@Input({
               @Bind(key = "objectId", binding = "objectId", required = true),
               @Bind(key = "documentId", binding = "rentDocumentId")
       })
public class MireaRentRenterDocumentAddEditUI extends UIPresenter
{

    private Long _objectId;
    private MireaPlaceRentToDocumentRelation _rentDocument;
    private Long _rentDocumentId;
    private ISelectModel _contractorModel;
    private IUploadFile _uploadFile;
    private String _fileName;

    public boolean isEditForm()
    {
        return _rentDocumentId != null;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public String getFileName()
    {
        return _fileName;
    }

    public void setFileName(String fileName)
    {
        _fileName = fileName;
    }

    public ISelectModel getContractorModel()
    {
        return _contractorModel;
    }

    public void setContractorModel(ISelectModel contractorModel)
    {
        _contractorModel = contractorModel;
    }

    public Long getObjectId()
    {
        return _objectId;
    }

    public void setObjectId(Long objectId)
    {
        _objectId = objectId;
    }

    public MireaPlaceRentToDocumentRelation getRentDocument()
    {
        return _rentDocument;
    }

    public void setRentDocument(MireaPlaceRentToDocumentRelation rentDocument)
    {
        _rentDocument = rentDocument;
    }

    public Long getRentDocumentId()
    {
        return _rentDocumentId;
    }

    public void setRentDocumentId(Long rentDocumentId)
    {
        _rentDocumentId = rentDocumentId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (_rentDocumentId != null)
        {
            _rentDocument = DataAccessServices.dao().get(MireaPlaceRentToDocumentRelation.class, _rentDocumentId);
            if (_rentDocument != null && _rentDocument.getDocument() != null)
                _fileName = MireaRentManager.instance().dao().getDocument(_rentDocument.getDocument()).getFileName();
        }
        else
        {
            _rentDocument = new MireaPlaceRentToDocumentRelation();
        }

        setContractorModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return DataAccessServices.dao().get(MireaPlacesRent.class, (Long)primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(MireaPlacesRent.class, "re")
                        .column(property("re"))
                        .where(eq(property("re", MireaPlacesRent.place()), value(_objectId)));
                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("re", MireaPlacesRent.title()), value(CoreStringUtils.escapeLike(filter))));
                return new ListResult(DataAccessServices.dao().getList(builder));
            }
        });
    }

    public void onClickApply()
    {
        MireaRentManager.instance().dao().saveOrUpdateDocument(getRentDocument(), getUploadFile());
        deactivate();
    }
}