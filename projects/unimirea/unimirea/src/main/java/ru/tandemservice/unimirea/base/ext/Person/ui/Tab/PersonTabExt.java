/* $Id$ */
package ru.tandemservice.unimirea.base.ext.Person.ui.Tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTab;
import ru.tandemservice.unimirea.base.bo.MireaPerson.ui.AccessRestrictionList.MireaPersonAccessRestrictionList;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */
@Configuration
public class PersonTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unimirea" + PersonTabExtUI.class.getSimpleName();
    public static final String ACCESS_RESTRICTIONS_TAB = "accessRestrictionsTab";

    @Autowired
    private PersonTab _personTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_personTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonTabExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_personTab.personTabPanelExtPoint())
                .addTab(componentTab(ACCESS_RESTRICTIONS_TAB, MireaPersonAccessRestrictionList.class).permissionKey("ui:secModel.viewTabRestrictionAccessList"))
                .create();
    }


}
