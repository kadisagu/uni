/* $Id$ */
package ru.tandemservice.unimirea.component.modularextract;

/**
 * @author Ekaterina Zvereva
 * @since 15.01.2016
 */
public interface MireaMoveStudentDefines
{
    int INDIVIDUAL_ORDER_APPLICATION_CODE = 4; // код для приложения к сборному приказу
}