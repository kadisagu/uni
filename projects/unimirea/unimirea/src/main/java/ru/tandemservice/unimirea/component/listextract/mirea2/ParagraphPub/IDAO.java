/* $Id$ */

package ru.tandemservice.unimirea.component.listextract.mirea2.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unimirea.entity.MireaExcludeSuccessStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public interface IDAO extends IAbstractListParagraphPubDAO<MireaExcludeSuccessStuListExtract, Model>
{
}
