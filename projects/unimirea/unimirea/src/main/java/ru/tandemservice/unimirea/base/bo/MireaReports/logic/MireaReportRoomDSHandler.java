/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 24.02.2015
 */
public class MireaReportRoomDSHandler extends DefaultComboDataSourceHandler
{
    public MireaReportRoomDSHandler(String ownerId)
    {
        super(ownerId, UniplacesPlace.class);
        setOrderByProperty(UniplacesPlace.P_TITLE);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Object floors = context.get(IMireaReportsDAO.FLOORS);
        if (floors == null || ((Collection) floors).size() != 1)
            return ListOutputBuilder.get(input, Collections.emptyList()).build();
        return super.execute(input, context);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Object floors = ep.context.get(IMireaReportsDAO.FLOORS);
        Object places = ep.context.get(IMireaReportsDAO.PLACES);
        ep.dqlBuilder.where(in(property("e", UniplacesPlace.floor()), (Collection) floors));
        if (places != null)
            ep.dqlBuilder.joinEntity("e", DQLJoinType.left, UniPlacesPlaceExt.class, "ext", eq(property("e.id"), property("ext", UniPlacesPlaceExt.place())))
                    .where(in(property("ext", UniPlacesPlaceExt.placeMirea()), (Collection) places));

    }

}