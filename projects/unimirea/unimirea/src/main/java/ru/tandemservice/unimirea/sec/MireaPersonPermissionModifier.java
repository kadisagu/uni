/* $Id$ */
package ru.tandemservice.unimirea.sec;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.person.sec.IPersonRolePermissionMeta;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 03.04.2015
 */
public class MireaPersonPermissionModifier implements ISecurityConfigMetaMapModifier
{

    @Override
    @SuppressWarnings("unchecked")
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap) {

        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unimirea");
        config.setName("unimirea-sec-config");
        config.setTitle("");


        for (IPersonRolePermissionMeta meta : (List<IPersonRolePermissionMeta>) ApplicationRuntime.getBean("personRolePermissionMetaList")) {

            String name = meta.getName();
            String title = meta.getTitle();
            PermissionGroupMeta personRolePG = PermissionMetaUtil.createPermissionGroup(config, name + "PG", "Объект  «" + title + "»");
            PermissionGroupMeta tabPersonDataPG = PermissionMetaUtil.createPermissionGroup(personRolePG, "tabPassport_" + name + "PG", "Вкладка «Личные данные»");

            PermissionGroupMeta tabLoginPG = PermissionMetaUtil.createPermissionGroup(tabPersonDataPG, "tabRestrictionAccessList_" + name + "PG", "Вкладка «Ограничения доступа»");
            PermissionMetaUtil.createPermission(tabLoginPG, "viewTabRestrictionAccessList_" + name, "Просмотр вкладки «Ограничения доступа»");
            PermissionMetaUtil.createPermission(tabLoginPG, "addRestrictionAccess_" + name, "Добавление IP-адреса");
            PermissionMetaUtil.createPermission(tabLoginPG, "editRestrictionAccess_" + name, "Редактирование IP-адреса");
            PermissionMetaUtil.createPermission(tabLoginPG, "deleteRestrictionAccess_" + name, "Удаление IP-адреса");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
