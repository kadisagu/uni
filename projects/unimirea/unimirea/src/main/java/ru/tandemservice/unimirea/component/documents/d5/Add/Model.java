package ru.tandemservice.unimirea.component.documents.d5.Add;

import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hgfhg on 21.07.2016.
 */
public class Model extends DocumentAddBaseModel
{
    private Date _formingDate;
    private String _studentTitleStr;
    private List<Course> _courseList;
    private Course _course;
    private List<IdentifiableWrapper> _departmentList;
    private IdentifiableWrapper _department;

    private List<IdentifiableWrapper>  _attestationList;
    private IdentifiableWrapper _attestation;

    private String _documentForTitle;
    private String _managerPostTitle;
    private String _managerFio;
    private String _secretarTitle;
    private Integer _term;
    private String _phone;
    private Date _eduFrom;
    private Date _eduTo;
    private Integer _cntDays;

    // Getters & Setters

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }
    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<IdentifiableWrapper> getAttestationList()
    {
        return _attestationList;
    }

    public void setAttestationList(List<IdentifiableWrapper> attestationList)
    {
        _attestationList = attestationList;
    }

    public IdentifiableWrapper getAttestation()
    {
        return _attestation;
    }

    public void setAttestation(IdentifiableWrapper attestation)
    {
        _attestation = attestation;
    }


    public String getDocumentForTitle()
    {
        return _documentForTitle;
    }

    public void setDocumentForTitle(String documentForTitle)
    {
        _documentForTitle = documentForTitle;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }

    public String getSecretarTitle()
    {
        return _secretarTitle;
    }

    public void setSecretarTitle(String secretarTitle)
    {
        _secretarTitle = secretarTitle;
    }

    public Integer getTerm()
    {
        return _term;
    }

    public void setTerm(Integer term)
    {
        _term = term;
    }

    public String getPhone()
    {
        return _phone;
    }

    public void setPhone(String phone)
    {
        _phone = phone;
    }
    public Date getEduFrom()   { return _eduFrom; }
    public void setEduFrom(Date eduFrom) { _eduFrom = eduFrom; }
    public Date getEduTo() { return _eduTo; }
    public void setEduTo(Date eduTo)   {  _eduTo = eduTo; }
    public Integer getCntDays()
    {
        return _cntDays;
    }
    public void setCntDays(Integer cntDays)
    {
        _cntDays = cntDays;
    }

    public List<Validator> getValidatorDays()
    {
        List<Validator> list = new ArrayList<Validator>();
        list.add(new Required());
        list.add(new Min("min=14"));
        list.add(new Max("max=130"));
        return list;
    }
}
