package ru.tandemservice.unimirea.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unimirea.base.bo.MireaReports.ui.BankCardPersonalData.MireaReportsBankCardPersonalData;

/**
 * @author Andrey Avetisov
 * @since 27.07.2015
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
 /*       final IItemListExtensionBuilder<GlobalReportDefinition> itemListExtensionBuilder = itemListExtension(_globalReportManager.reportListExtPoint());
        return itemListExtensionBuilder
                .add("personSamplesReport", new GlobalReportDefinition("person", "bankCardPersonalData", MireaReportsBankCardPersonalData.class.getSimpleName()))
                .create();*/
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("personList", new GlobalReportDefinition("person", "bankCardPersonalData", MireaReportsBankCardPersonalData.class.getSimpleName()))
                  .create();
    }
}
