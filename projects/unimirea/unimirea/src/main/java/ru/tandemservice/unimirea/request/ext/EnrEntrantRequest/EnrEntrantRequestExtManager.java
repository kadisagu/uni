/* $Id$ */
package ru.tandemservice.unimirea.request.ext.EnrEntrantRequest;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 30.09.2015
 */
@Configuration
public class EnrEntrantRequestExtManager extends BusinessObjectExtensionManager
{
}