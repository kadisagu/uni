/* $Id$ */
package ru.tandemservice.unimirea.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unimirea.report.bo.MireaEnrReport.ui.PersonalFileCoverAdd.MireaEnrReportPersonalFileCoverAdd;

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager
{

    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
               .add(MireaEnrReportPersonalFileCoverAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(MireaEnrReportPersonalFileCoverAdd.REPORT_KEY, MireaEnrReportPersonalFileCoverAdd.class))
                .create();
    }
}