/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaPerson.ui.AccessRestrictionList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.PersonRoleSecModel;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unimirea.base.bo.MireaPerson.ui.AccessRestrictionAddEdit.MireaPersonAccessRestrictionAddEdit;
import ru.tandemservice.unimirea.base.bo.MireaPerson.ui.AccessRestrictionAddEdit.MireaPersonAccessRestrictionAddEditUI;

/**
 * @author Andrey Avetisov
 * @since 02.04.2015
 */
@Input({
               @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = "personRoleModel", required = true)
       })
public class MireaPersonAccessRestrictionListUI extends UIPresenter
{
    private ISecureRoleContext _personRoleModel;
    private Person _person;
    private PersonRoleSecModel _secModel;
    private Principal _principal;

    @Override
    public void onComponentRefresh()
    {
        setPerson(DataAccessServices.dao().getNotNull(Person.class, getPersonRoleModel().getPersonId()));
        setSecModel(PersonRoleSecModel.instance(getPerson(), getPersonRoleModel().getSecuredObject(), getPersonRoleModel().getSecuredPostfix()));
        setPrincipal(DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, getPersonRoleModel().getPersonId()).getPrincipal());
    }

    //listener
    public void onClickAddIPAddress()
    {
        _uiActivation.asRegionDialog(MireaPersonAccessRestrictionAddEdit.class)
                .parameter(MireaPersonAccessRestrictionAddEditUI.PERSON_ID, getPerson().getId())
                .activate();
    }

    public void onClickEditIPAddress()
    {
        _uiActivation.asRegionDialog(MireaPersonAccessRestrictionAddEdit.class)
                .parameter(MireaPersonAccessRestrictionAddEditUI.PERSON_ID, getPerson().getId())
                .parameter(MireaPersonAccessRestrictionAddEditUI.PRINCIPAL_TO_IP_RELATION_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(MireaPersonAccessRestrictionList.IP_ADDRESSES_DS))
            dataSource.put("principal", getPrincipal());
    }

    //getters & setters
    public ISecureRoleContext getPersonRoleModel()
    {
        return _personRoleModel;
    }

    public void setPersonRoleModel(ISecureRoleContext personRoleModel)
    {
        _personRoleModel = personRoleModel;
    }

    public PersonRoleSecModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(PersonRoleSecModel secModel)
    {
        _secModel = secModel;
    }

    public Person getPerson()
    {
        return _person;
    }

    public void setPerson(Person person)
    {
        _person = person;
    }

    public Principal getPrincipal()
    {
        return _principal;
    }

    public void setPrincipal(Principal principal)
    {
        _principal = principal;
    }
}
