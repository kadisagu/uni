/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaRent.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import ru.tandemservice.unimirea.entity.MireaPlaceRentToDocumentRelation;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
public interface IMireaRentDAO
{

    void saveOrUpdateDocument(MireaPlaceRentToDocumentRelation document, IUploadFile file);

    //печать приложенного документа
    void printDocumentScanCopy(MireaPlaceRentToDocumentRelation document);

    //Изменение признака активности документа
    void changeActiveProperty(Long documentId);

    //Удаление документа
    void deleteDocument(Long documentId);

    RemoteDocumentDTO getDocument(Long documentId);

}