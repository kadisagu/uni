/* $Id$ */
package ru.tandemservice.unimirea.events.single;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Ekaterina Zvereva
 * @since 17.08.2015
 */
public class MireaStudentSaveEventListener extends FilteredSingleEntityEventListener
{
    @Override
    public void onFilteredEvent(final ISingleEntityEvent event)
    {
        final Student student = (Student) event.getEntity();
        if (StringUtils.isNotBlank(student.getPersonalNumber()))
            return;

        final String freeNumber = IStudentDAO.instance.get().generateNewPersonalNumber(student);

        if (freeNumber == null) {
            throw new ApplicationException("Для " + student.getEntranceYear() + " года не осталось свободных персональных номеров..");
        }

        student.setPersonalNumber(freeNumber);
        student.setBookNumber(freeNumber);
        final Session session = event.getSession();
        session.flush();
    }
}