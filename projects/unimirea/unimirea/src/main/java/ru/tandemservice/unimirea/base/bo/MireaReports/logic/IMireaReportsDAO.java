/* $Id$ */
package ru.tandemservice.unimirea.base.bo.MireaReports.logic;

import ru.tandemservice.unimirea.base.bo.MireaReports.ui.BankCardPersonalData.MireaReportsBankCardPersonalDataUI;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 20.02.2015
 */
public interface IMireaReportsDAO
{
    public static final String GROUNDS = "grounds";
    public static final String BUILDINGS ="buildings";
    public static final String UNITS ="units";
    public static final String FLOORS ="floors";
    public static final String PLACES ="places";
    public static final String ROOMS = "rooms";

    ReportInfo prepareReportData(ReportDataWrapper dataWrapper);

    /**
     * Список студентов для отчета "Выгрузка персональных данных для банковских карт МИРЭА"
     * @param model модель с данными, необходимыми для фильтрации студентов.
     * @return массив объектов со следующими колонками - перосна, удостоврение личности, адрес регистрации, фактический адрес, студент
     */
    List<Object[]> getPersonStudentList(MireaReportsBankCardPersonalDataUI model);

    /**
     * Список сотрудников для отчета "Выгрузка персональных данных для банковских карт МИРЭА"
     * @param model модель с данными, необходимыми для фильтрации сотрудников.
     * @return массив объектов со следующими колонками - персона, удостоврение личности, адрес регистрации, фактический адрес, сотрудник, должность
     */
    List<Object[]> getPersonEmployeeList(MireaReportsBankCardPersonalDataUI model);
}