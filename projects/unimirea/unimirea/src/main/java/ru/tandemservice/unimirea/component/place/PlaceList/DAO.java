/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlaceList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unimirea.entity.MireaPlaceRentToDocumentRelation;
import ru.tandemservice.unimirea.entity.MireaPlacesRent;
import ru.tandemservice.unimirea.entity.UniMireaPlace;
import ru.tandemservice.unimirea.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.component.place.PlaceList.Model;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.PlaceList.DAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<PlaceListWrapper> dataSource = ((ru.tandemservice.unimirea.component.place.PlaceList.Model)model).getPlaceDataSource();
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesPlace.class, "place")
                .addAdditionalAlias(UniPlacesPlaceExt.class, "placeExt")
                .addAdditionalAlias(UniplacesPlace.class, "place");

        final DQLSelectBuilder builder = orderRegistry.buildDQLSelectBuilder()
        .joinPath(DQLJoinType.inner, UniplacesPlace.floor().fromAlias("place"), "floor")
        .joinPath(DQLJoinType.inner, UniplacesFloor.unit().fromAlias("floor"), "unit")
        .joinPath(DQLJoinType.inner, UniplacesUnit.building().fromAlias("unit"), "building")
        .joinEntity("place", DQLJoinType.left, UniPlacesPlaceExt.class, "placeExt", eq(property("place.id"), property("placeExt", UniPlacesPlaceExt.place().id())))
                .column(property("place"))
                .column(property("placeExt"));

        List<UniplacesPlacePurpose> purposes = model.getPurposes();
        if (purposes != null && !purposes.isEmpty())
        {
            builder.where(in(property("place", UniplacesPlace.purpose()), purposes));
        }

        List<UniplacesClassroomType> classroomTypes = model.getClassroomTypes();
        if(classroomTypes != null && !classroomTypes.isEmpty())
        {
            builder.where(in(property("place", UniplacesPlace.classroomType()), classroomTypes));
        }

        final UniplacesPlaceCondition condition = model.getCondition();
        if (condition != null)
        {
            builder.where(eq(property("place", UniplacesPlace.condition()), value(condition)));
        }

        if (model.getPlace() != null)
            builder.where(eq(property("place.id"), value(model.getPlace())));
        else if (model.getSettings().get("mireaPlace") != null)
            builder.where(eq(property("placeExt", UniPlacesPlaceExt.placeMirea()), commonValue(model.getSettings().get("mireaPlace"))));
        else if (model.getFloor() != null)
            builder.where(eq(property("place", UniplacesPlace.floor()), value(model.getFloor())));
        else if (model.getUnit() != null)
            builder.where(eq(property("floor", UniplacesFloor.unit()), value(model.getUnit())));
        else if (model.getBuilding() != null)
            builder.where(eq(property("unit", UniplacesUnit.building()), value(model.getBuilding())));

        if (model.getOrgUnit() != null)
            builder.where(eq(property("place", UniplacesPlace.responsibleOrgUnit()), value(model.getOrgUnit())));
        if (model.getAreaMax() != null)
            builder.where(le(property("place", UniplacesPlace.area()), value(model.getAreaMax())));
        if (model.getAreaMin() != null)
            builder.where(ge(property("place", UniplacesPlace.area()), value(model.getAreaMin())));
        if (model.getLengthMax()!= null)
            builder.where(le(property("place", UniplacesPlace.length()), value(model.getLengthMax())));
        if (model.getLengthMin()!= null)
            builder.where(ge(property("place", UniplacesPlace.length()), value(model.getLengthMin())));
        if (model.getWidthMax()!= null)
            builder.where(le(property("place", UniplacesPlace.width()), value(model.getWidthMax())));
        if (model.getWidthMin()!= null)
            builder.where(ge(property("place", UniplacesPlace.width()), value(model.getWidthMin())));
        if (model.getHeightMax()!= null)
            builder.where(le(property("place", UniplacesPlace.height()), value(model.getHeightMax())));
        if (model.getHeightMin()!= null)
            builder.where(ge(property("place", UniplacesPlace.height()), value(model.getHeightMin())));



        //Ответственный сотрудник
        Object responsiblePerson = model.getSettings().get("responsiblePerson");
        if (responsiblePerson != null)
            builder.where(eq(property("ext", UniPlacesPlaceExt.responsiblePerson()), commonValue(responsiblePerson)));

        //Возможность аренды
        Object avaliableRent = model.getSettings().get("availableRent");
        if (avaliableRent != null)
            builder.where(eq(property("placeExt", UniPlacesPlaceExt.mayRented()), value(((IEntity)avaliableRent).getId().equals(ru.tandemservice.unimirea.component.place.PlaceList.Model.AVALIABLE_RENT_TRUE))));

        //Состояние аренды
        Object statusRent = model.getSettings().get("stateRent");
        if (statusRent != null )
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(MireaPlaceRentToDocumentRelation.class, "doc")
                    .column("doc.id")
                    .joinPath(DQLJoinType.inner, MireaPlaceRentToDocumentRelation.object().fromAlias("doc"), "rent")
                    .where(eq(property("rent", MireaPlacesRent.place()), property("place.id")))
                    .where(eq(property("doc", MireaPlaceRentToDocumentRelation.rentContract()), value(true)))
                    .where(eq(property("doc", MireaPlaceRentToDocumentRelation.active()), value(true)));
            if (((IEntity)statusRent).getId().equals(ru.tandemservice.unimirea.component.place.PlaceList.Model.STATUS_RENT_BUSY))
              builder.where(exists(subBuilder.buildQuery()));
            else
            {
                builder.where(notExists(subBuilder.buildQuery()));
                builder.where(eq(property("placeExt", UniPlacesPlaceExt.mayRented()), value(true)));
            }
        }

        //Контрагент
        final Object contractor = model.getSettings().get("contractor");
        if (contractor != null)
        {
            DQLSelectBuilder subContractorBuilder = new DQLSelectBuilder().fromEntity(MireaPlacesRent.class, "cnt")
                    .where(eq(property("place.id"), property("cnt", MireaPlacesRent.place().id())))
                    .where(eq(property("cnt", MireaPlacesRent.contractor().id()), commonValue(contractor)));
            builder.where(exists(subContractorBuilder.buildQuery()));
        }

        orderRegistry.applyOrderWithLeftJoins(builder, dataSource.getEntityOrder());



        final int count = getCount(builder);
        dataSource.setTotalSize(count);

        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();
        List<Object[]> list = getList(builder, (int) startRow, (int) countRow);
        List<PlaceListWrapper> resultList= new ArrayList<>(list.size());
        List<Long> idsList = new ArrayList<>(list.size());

        for(Object[] row : list)
        {
            UniplacesPlace place = (UniplacesPlace) row[0];
            UniPlacesPlaceExt placeExt = (UniPlacesPlaceExt) row[1];
            PlaceListWrapper wrapper = new PlaceListWrapper(place.getId(), place.getTitle());
            wrapper.setPlace(place);
            wrapper.setPlaceExt(placeExt);
            idsList.add(place.getId());
            resultList.add(wrapper);
        }
        dataSource.createPage(resultList);

        final Map<Long, List<ContractorListWrapper>> contractorMap = new HashMap<>();

        BatchUtils.execute(idsList, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            DQLSelectBuilder contractorBuilder = new DQLSelectBuilder().fromEntity(MireaPlacesRent.class, "cnt")
                    .column(property("cnt"))
                    .where(in(property("cnt", MireaPlacesRent.place().id()), ids));
            if (contractor != null)
                    builder.where(eq(property("cnt", MireaPlacesRent.contractor().id()), commonValue(contractor)));

            for (MireaPlacesRent rent : contractorBuilder.createStatement(getSession()).<MireaPlacesRent>list())
            {
                List<ContractorListWrapper> cntList = contractorMap.get(rent.getPlace().getId());
                if (cntList == null)
                    cntList = new ArrayList<>();
                cntList.add(new ContractorListWrapper(rent.getContractor().getId(), rent.getTitle()));
                contractorMap.put(rent.getPlace().getId(), cntList);
            }
        });

        for (PlaceListWrapper wrapper : ((ru.tandemservice.unimirea.component.place.PlaceList.Model) model).getPlaceDataSource().getEntityList())
        {
            List<ContractorListWrapper> cntList = contractorMap.get(wrapper.getId());
            if (null == cntList) cntList = new ArrayList<>();
            wrapper.setContractorList(cntList);
        }
        flushClearAndRefresh();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        // модель выбора контрагента
        ((ru.tandemservice.unimirea.component.place.PlaceList.Model)model).setContractorModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(ExternalOrgUnit.class,(Long)primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(MireaPlacesRent.class, "rent")
                        .column(property("rent", MireaPlacesRent.contractor()))
                        .distinct();
                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("rent", MireaPlacesRent.contractor().title()), value(CoreStringUtils.escapeLike(filter))));
                return new ListResult<>(builder.createStatement(getSession()).<UniplacesPlace>list());
            }
        });

        //модель выбора ответственного лица
        ((ru.tandemservice.unimirea.component.place.PlaceList.Model) model).setResponsiblePersonModel(new BaseSingleSelectModel(EmployeePost.P_FULL_TITLE)
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(EmployeePost.class,(Long)primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e");
                builder.column("e");

                // фильтр по фио + должность + подр. + тип подр.
                if (StringUtils.isNotEmpty(filter))
                {
                    IDQLExpression expression = DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()),
                                                                    DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()),
                                                                                        DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.orgUnit().shortTitle().s()),
                                                                                                            DQLExpressions.property("e", EmployeePost.orgUnit().orgUnitType().title().s())
                                                                                        )));

                    builder.where(DQLExpressions.like(DQLFunctions.upper(expression), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
                }
                if (model.getSettings().get("orgUnit") != null)
                    builder.where(eq(property("e", EmployeePost.orgUnit()), commonValue(model.getSettings().get("orgUnit"))));

                // только активные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.postStatus().active().s()), DQLExpressions.value(Boolean.TRUE)));

                // не архивные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival().s()), DQLExpressions.value(Boolean.FALSE)));

                // сортируем по фио
                builder.order(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()));

                // показываем не больше 50
                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
                statement.setMaxResults(50);

                return new ListResult<>(statement.<EmployeePost>list(), count == null ? 0 : count.intValue());
            }
        });
        ((ru.tandemservice.unimirea.component.place.PlaceList.Model) model).setMireaPlaceList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniMireaPlace> findValues(String filter)
            {
                    if (null == model.getFloor())
                        return ListResult.getEmpty();
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniMireaPlace.class, "mireaPlace")
                            .column(property("mireaPlace"));
                    builder.where(eq(property("mireaPlace", UniMireaPlace.floor()), value(model.getFloor())));
                    if (StringUtils.isNotBlank(filter))
                        builder.where(likeUpper(property("mireaPlace", UniMireaPlace.title()), value(CoreStringUtils.escapeLike(filter))));
                    builder.order(property("mireaPlace", UniMireaPlace.title()));
                    
                    int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                    return new ListResult<>(builder.createStatement(getSession()).<UniMireaPlace>list(), count);
            }
        });

        //комнаты
        model.setPlaceList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesPlace> findValues(String filter)
            {
                if (null == model.getSettings().get("mireaPlace"))
                    return ListResult.getEmpty();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "pl")
                        .column(property("pl"))
                        .joinEntity("pl", DQLJoinType.inner, UniPlacesPlaceExt.class, "ext", eq(property("pl.id"), property("ext", UniPlacesPlaceExt.place())));
                builder.where(eq(property("ext", UniPlacesPlaceExt.placeMirea()), commonValue(model.getSettings().get("mireaPlace"))));
                if (StringUtils.isNotBlank(filter))
                    builder.where(likeUpper(property("pl", UniplacesPlace.title()), value(CoreStringUtils.escapeLike(filter))));
                builder.order(property("pl", UniplacesPlace.title()));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }

        });

    }
}