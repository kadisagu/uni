/* $Id$ */
package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Ekaterina Zvereva
 * @since 24.10.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unimirea_2x10x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.8"),
                        new ScriptDependency("ru.tandemservice.nsiclient", "2.10.8"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.8")
                };
    }

    @Override
    public void run(DBTool dbTool) throws Exception
    {
        // модуль uniepp_price отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if( ApplicationRuntime.hasModule("uniepp_price") )
                throw new RuntimeException("Module 'uniepp_price' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(dbTool, "uniepp_price");
    }
}