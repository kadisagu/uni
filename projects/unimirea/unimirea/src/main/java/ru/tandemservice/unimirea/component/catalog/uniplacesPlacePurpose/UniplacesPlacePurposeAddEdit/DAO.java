/* $Id$ */
package ru.tandemservice.unimirea.component.catalog.uniplacesPlacePurpose.UniplacesPlacePurposeAddEdit;

import ru.tandemservice.uniplaces.component.catalog.uniplacesPlacePurpose.UniplacesPlacePurposeAddEdit.Model;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;

/**
 * @author Ekaterina Zvereva
 * @since 18.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.catalog.uniplacesPlacePurpose.UniplacesPlacePurposeAddEdit.DAO
{
    @Override
    public void update(Model model)
    {
        final UniplacesPlacePurpose purpose = model.getCatalogItem();
        purpose.setChildsAllowed(true);
        super.update(model);
    }
}