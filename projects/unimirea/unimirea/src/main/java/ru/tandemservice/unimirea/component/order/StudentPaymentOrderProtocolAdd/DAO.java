/* $Id$ */
package ru.tandemservice.unimirea.component.order.StudentPaymentOrderProtocolAdd;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.unisession.dao.sessionObject.ISessionObjectDAO;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd.Model;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;

import java.text.DecimalFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * Created by Verun on 18.02.2016.
 */
public class DAO extends ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd.DAO
{


    @Override
    protected HashMap<Long, List<String>> getMarksInfo
            (
                    final List<Long> studentIds,
                    final Collection<EppWorkPlanRowKind> workPlanRowKinds
            )
    {
        final Map<Long, List<String>> studentSessionMarkSummaryMap = studentSessionMarkSummaryOur(studentIds, workPlanRowKinds);
        HashMap<Long, List<String>> result = new HashMap<>();
        for (Long student : studentIds)
        {
            List<String> markList = studentSessionMarkSummaryMap.get(student);
            if (markList.isEmpty())
            {
                result.put(student, Arrays.asList("0", "0", "0", "0", "0", "0"));
                continue;
            }

            int otl = 0;
            int hor = 0;
            int ud = 0;
            int neud = 0;
            int zach = 0;
            int nezach = 0;
            for (String markValueCode : markList)
            {
                if (SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(markValueCode))
                    otl++;
                if (SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(markValueCode))
                    hor++;
                if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(markValueCode))
                    ud++;
                if (SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO.equals(markValueCode))
                    neud++;
                if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(markValueCode))
                    zach++;
                if (SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO.equals(markValueCode))
                    nezach++;
            }
            result.put(student, Arrays.asList(String.valueOf(otl), String.valueOf(hor), String.valueOf(ud), String.valueOf(neud), String.valueOf(zach), String.valueOf(nezach)));
        }
        return result;
    }

    @Override
    protected Map<Long, String> getStudentsWithExtractsIds(Model model)
    {
        ArrayList<StudentPaymentAppointmentExtract> extraList = new ArrayList<>();
        MQBuilder builder = new MQBuilder(StudentPaymentAppointmentExtract.ENTITY_CLASS, "extract");
        builder.add(MQExpression.eq("extract", StudentPaymentExtract.paragraph().order().s(), model.getOrder()));
        extraList.addAll(builder.<StudentPaymentAppointmentExtract>getResultList(getSession()));
        Map<Long, String> result = new HashMap<>();
        for (StudentPaymentAppointmentExtract ext : extraList)
        {
            Long id = ext.getEntity().getId();
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            String secondNum = decimalFormat.format(ext.getPaymentCostAsDouble());
            result.put(id, secondNum);
        }
        return result;
    }

    @Override
    protected boolean printStudentsTable(Model model, RtfDocument page, List<Student> students, Map<Long, List<String>> marksInfoByStudentId, Map<Long, String> studentsWithExtracts)
    {
        List<String[]> tableContent = new ArrayList<>();
        final Set<Integer> markRowBold = new HashSet<>();
        int i = 0;
        boolean existStudentWithExtract = false;
        for (Student student : students)
        {
            String type = studentsWithExtracts.get(student.getId());
            final boolean extractExist = null != type;
            if (!model.getProtocol().isStudentsWithoutExtracts() && !extractExist)
                continue;
            if (UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()))
                continue;
            List<String> row = new ArrayList<>();
            row.add(String.valueOf(++i));
            row.add(student.getPerson().getFullFio());
            //   row.add(student.getBookNumber());
            //      row.add(UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()) ? "дог." : "");
            row.addAll(marksInfoByStudentId.get(student.getId()));
            if (extractExist)
            {
                markRowBold.add(i);
                existStudentWithExtract = true;
            }
            row.add("");
            row.add("");
            row.add(type == null ? "отказать" : "назначить");
            row.add(StringUtils.trimToEmpty(type));
            tableContent.add(row.toArray(new String[row.size()]));
        }
        if (!model.getProtocol().isGroupsWithoutExtracts() && !existStudentWithExtract)
            return false;
        RtfTableModifier modifier = new RtfTableModifier();
        final int rowCount = tableContent.size();
        modifier.put("T", tableContent.toArray(new String[rowCount][]));
        modifier.put("T", new RtfRowIntercepterBase()
        {

            @Override
            @SuppressWarnings("deprecation")
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = 0; i < rowCount; i++)
                    if (markRowBold.contains(i + 1))
                        UniRtfUtil.setRowBold(newRowList.get(startIndex + i));
            }
        });
        modifier.modify(page);
        return true;
    }

    @Override
    protected void fillHeader(RtfDocument page, Group group, Model model)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        String captain = "";
        if (null != group)
        {
            List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
            for (Student student : studentList)
            {
                captain += (captain.isEmpty() ? "" : ", ") + student.getPerson().getFio();
            }
        }

        //     String sesBeg = DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getSessionStartDate());
        //     String sesEnd = DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getSessionEndDate());
        String sesBeg = RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getProtocol().getSessionStartDate());
        String sesEnd = RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getProtocol().getSessionEndDate());
        String proDate8 = DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getProtocolDate());
        String sesEndYear = proDate8.substring(6);
        String sesEndMonth = proDate8.substring(3, 5);
        String selCourse = group != null ? group.getCourse().getTitle() : "";
        String sesName = "";
        String acadYear = "";
        String basePayment = "";
        String incPayment = "";
        if (StringUtils.trimToEmpty(sesEndYear).length() == 4)
        {
            int baseYear = Integer.parseInt(sesEndYear);
            sesName = "летней";
            acadYear = Integer.toString(baseYear - 1) + "/" + sesEndYear;
            //    if (sesEndMonth.equals("01") || sesEndMonth.equals("02"))
            String sWinter = "12 01 02 03";
            String sSpring = "04 05";
            if (sWinter.contains(sesEndMonth))
                sesName = "зимней";
            else if (sSpring.contains(sesEndMonth))
                sesName = "весенней";
        }
        ;

        if (selCourse.equals("1") && sesName.equals("летней"))
        {
            basePayment = "успешно прошедшим вступительные испытания при поступлении в университет";
        }
        else
        {
            basePayment = "по результатам " + sesName + " экзаменационной сессии " + acadYear + " учебного года, проходившей с " + sesBeg + " г. по " + sesEnd + " г";
            incPayment = " в увеличенном размере";
        }

        TopOrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit orgUnit = model.getOrder().getOrgUnit();
        String titleVar = StringUtils.uncapitalize(orgUnit.getGenitiveCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getGenitiveCaseTitle());
        String ouTitleG = titleVar.substring(0, 1).toUpperCase() + titleVar.substring(1);
        modifier.put("vuzTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        //      modifier.put("ouTitle", orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());
        modifier.put("protocolNum", StringUtils.trimToEmpty(model.getProtocol().getProtocolNumber()));
        modifier.put("protocolDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getProtocol().getProtocolDate()));
        modifier.put("orderPrNum", StringUtils.trimToEmpty(model.getProtocol().getCommOrderNumber()));
        modifier.put("orderPrDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getProtocol().getCommOrderDate()));
        modifier.put("acadYear", sesEndYear);
        // modifier.put("dayT", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getRetakeDate()));
        modifier.put("leaderGroup", model.isCaptainsFromGroup() ? captain : model.getCaptain() == null ? "" : model.getCaptain().getPerson().getFio());
        modifier.put("groupTitle", group != null ? group.getTitle() : "");
        //       modifier.put("ouTitleG", StringUtils.uncapitalize(orgUnit.getGenitiveCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getGenitiveCaseTitle()));
        modifier.put("ouTitleG", ouTitleG);
        modifier.put("course", group != null ? group.getCourse().getTitle() : "");
        modifier.put("incPayment", incPayment);
        modifier.put("basePayment", basePayment);
        modifier.put("acadYear", acadYear);


        modifier.modify(page);
    }

    /**
     * Итоговые оценки в сессию за последний семестр из набора актуальных МСРП-ФК основной обр. программы.
     *
     * @param studentIds список id студентов
     * @return [student.id -> список кодов оценок, всегда notNull ]
     */

    protected Map<Long, List<String>> studentSessionMarkSummaryOur(
            final Collection<Long> studentIds,
            final Collection<EppWorkPlanRowKind> workPlanRowKinds
    )
    {
        final Map<Long, Integer> lastTermNumberMap = ISessionObjectDAO.instance.get().lastTermNumberMap(studentIds);

        final Map<Long, List<String>> resultMarkMap = SafeMap.get(LinkedList.class);
        for (List<Long> elements : Iterables.partition(studentIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionMark.class, "m")
                    .column(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")))
                    .column(property(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("m")))
                    .column(property(SessionMark.cachedMarkValue().code().fromAlias("m")))
                    .where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")), elements))
                    .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(true)))
                    .where(isNull(property(SessionMark.slot().studentWpeCAction().studentWpe().removalDate().fromAlias("m"))))
                    .where(in(property(SessionMark.slot().document().id().fromAlias("m")),
                              new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "gb")
                                      .column(property(SessionStudentGradeBookDocument.id().fromAlias("gb")))
                                      .buildQuery())
                    );

            if (workPlanRowKinds != null && !workPlanRowKinds.isEmpty())
                builder.where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().sourceRow().kind().fromAlias("m")), workPlanRowKinds));

            final List<Object[]> list = builder.createStatement(getSession()).list();
            for (Object[] objects : list)
            {
                final Long studentId = (Long) objects[0];
                final Integer term = (Integer) objects[1];
                final String markValueCode = (String) objects[2];

                final Integer lastTerm = lastTermNumberMap.get(studentId);
                if (lastTerm != null && lastTerm.equals(term))
                {
                    resultMarkMap.get(studentId)
                            .add(markValueCode);
                }
            }
        }

        return resultMarkMap;
    }
}