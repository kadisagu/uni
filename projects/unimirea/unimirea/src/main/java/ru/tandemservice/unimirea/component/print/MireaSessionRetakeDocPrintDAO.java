/* $Id$ */
package ru.tandemservice.unimirea.component.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 17.12.2015
 */
public class MireaSessionRetakeDocPrintDAO extends SessionRetakeDocPrintDAO
{


    @Override
    protected void customizeSimpleTags(RtfInjectModifier modifier, BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating() || printInfo.isUsePoints())
            return;

        SessionRetakeDocument bulletin = printInfo.getBulletin();
        Map<EppStudentWpeCAction, BulletinPrintRow> contentMap = printInfo.getContentMap();

        EppRegistryElementPart registryElementPart = bulletin.getRegistryElementPart();

        OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        modifier.put("orgUnitTitle", orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());

        String documentType = "ВЕДОМОСТЬ";
        Set<String> caTypes = contentMap.values().stream()
                .map(row -> row.getSlot().getStudentWpeCAction().getType().getCode())
                .collect(Collectors.toSet());
        if (caTypes.size() == 1)
        {
            switch (caTypes.iterator().next())
            {
                case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
                case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM:
                    documentType = "ЭКЗАМЕНАЦИОННАЯ ВЕДОМОСТЬ";
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
                case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
                    documentType = "ЗАЧЕТНАЯ ВЕДОМОСТЬ";
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
                    documentType = "ВЕДОМОСТЬ НА КУРСОВУЮ РАБОТУ";
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
                    documentType = "ВЕДОМОСТЬ НА КУРСОВОЙ ПРОЕКТ";
                    break;
            }
        }
        modifier.put("documentType", documentType);

        final EppRegistryElement registryElement = bulletin.getRegistryElementPart().getRegistryElement();
        modifier.put("hours", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getSizeAsDouble()));
        modifier.put("ze", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getLaborAsDouble()));

        Iterator<BulletinPrintRow> rowIterator = contentMap.values().iterator();
        String term = rowIterator.hasNext() ?
                String.valueOf(rowIterator.next().getSlot().getStudentWpeCAction().getStudentWpe().getTerm().getIntValue()) : "";
        modifier.put("nterm", term);

    }

    @Override
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating())
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
                    ColumnType.CURRENT_RATING,
                    ColumnType.SCORED_POINTS,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.DATE);

        if (printInfo.isUsePoints())
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.DATE
            );

        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.STUDENT_NUMBER,
                ColumnType.DATE,
                ColumnType.MARK);
    }

    @Override
    protected List<ColumnType> prepareColumnList(final BulletinPrintInfo printInfo)
    {
        final Set<Group> groups = printInfo.getContentMap().values().stream()
                .map(row -> row.getSlot().getActualStudent().getGroup())
                .collect(Collectors.toSet());
        final List<ColumnType> columns = new ArrayList<>();
        columns.addAll(Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO));
        if (printInfo.isUseCurrentRating())
        {
            columns.add(ColumnType.BOOK_NUMBER);
            if (groups.size() > 1)
                columns.add(ColumnType.GROUP);
            columns.add(ColumnType.CURRENT_RATING);
            columns.add(ColumnType.SCORED_POINTS);
            columns.add(ColumnType.POINT);
        }
        else if (printInfo.isUsePoints())
        {
            columns.add(ColumnType.BOOK_NUMBER);
            if (groups.size() > 1)
                columns.add(ColumnType.GROUP);
            columns.add(ColumnType.POINT);
        }else{
            columns.add(ColumnType.STUDENT_NUMBER);
        }
        columns.add(ColumnType.DATE);
        columns.add(ColumnType.MARK);

        return columns;
    }

    @Override
    protected String[] fillTableRow(BulletinPrintRow printRow, List<ColumnType> columns, int rowNumber)
    {
        final List<String> result = new ArrayList<>();

        SessionMarkCatalogItem mark = printRow.getMark() == null ? null : printRow.getMark().getValueItem();
        Double points = printRow.getMark() == null ? null : printRow.getMark().getPoints();
        Double currentRating = printRow.getSlotRatingData() == null ? null : printRow.getSlotRatingData().getFixedCurrentRating();
        Double scoredPoints = printRow.getMarkRatingData() == null ? null : printRow.getMarkRatingData().getScoredPoints();

        for (final ColumnType column : columns)
        {
            switch (column)
            {
                case EMPTY:
                    result.add("");
                    break;
                case FIO:
                    result.add(printRow.getSlot().getActualStudent().getPerson().getFio());
                    break;
                case BOOK_NUMBER:
                    result.add(StringUtils.trimToEmpty(printRow.getSlot().getActualStudent().getBookNumber()));
                    break;
                case STUDENT_NUMBER:
                    result.add(StringUtils.trimToEmpty(printRow.getSlot().getActualStudent().getPersonalNumber()));
                    break;
                case GROUP:
                    result.add(printRow.getSlot().getActualStudent().getGroup() == null ? "" : printRow.getSlot().getActualStudent().getGroup().getTitle());
                    break;
                case THEME:
                    result.add(printRow.getTheme() == null ? "" : StringUtils.trimToEmpty(printRow.getTheme().getTheme()));
                    break;
                case MARK:
                    result.add(mark == null ? "" : StringUtils.trimToEmpty(mark.getPrintTitle()));
                    break;
                case NUMBER:
                    result.add(String.valueOf(rowNumber));
                    break;
                case CURRENT_RATING:
                    result.add(BrsRatingValueFormatter.instance.get().format(currentRating));
                    break;
                case POINT:
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(points));
                    break;
                case SCORED_POINTS:
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(scoredPoints));
                    break;
                case DATE:
                    result.add(printRow.getMark() == null ? "" : DATE_FORMATTER.format(printRow.getMark().getPerformDate()));
                    break;
                case PPS:
                    result.add(UniStringUtils.join(printRow.getCommission(), PpsEntry.person().identityCard().fio().s(), ", "));
                    break;
            }
        }
        return result.stream().toArray(String[]::new);
    }
}
