package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimirea_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность uniMireaPlace

		// создана новая сущность
		if (!tool.tableExists("unimireaplace_t"))
        {
			// создать таблицу
			DBTable dbt = new DBTable("unimireaplace_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("number_p", DBType.createVarchar(255)), 
				new DBColumn("floor_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("uniMireaPlace");

		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность mireaPlaceRentToDocumentRelation

        // создана новая сущность
        if (!tool.tableExists("mrplcrnttdcmntrltn_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("mrplcrnttdcmntrltn_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("object_id", DBType.LONG).setNullable(false),
                                      new DBColumn("document_p", DBType.LONG),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("startdate_p", DBType.DATE),
                                      new DBColumn("enddate_p", DBType.DATE),
                                      new DBColumn("comment_p", DBType.createVarchar(255)),
                                      new DBColumn("active_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("rentcontract_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("mireaPlaceRentToDocumentRelation");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность mireaPlacesRent

        // создана новая сущность
        if (!tool.tableExists("mireaplacesrent_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("mireaplacesrent_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("place_id", DBType.LONG).setNullable(false),
                                      new DBColumn("contractor_id", DBType.LONG).setNullable(false),
                                      new DBColumn("contractortype_id", DBType.LONG),
                                      new DBColumn("contactorperson_id", DBType.LONG),
                                      new DBColumn("startdate_p", DBType.DATE),
                                      new DBColumn("enddate_p", DBType.DATE)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("mireaPlacesRent");

        }

		////////////////////////////////////////////////////////////////////////////////
		// сущность uniPlacesPlaceExt

		// создана новая сущность
        if (!tool.tableExists("uniplacesplaceext_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("uniplacesplaceext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("place_id", DBType.LONG).setNullable(false), 
				new DBColumn("responsibleperson_id", DBType.LONG),
				new DBColumn("placemirea_id", DBType.LONG).setNullable(false),
                new DBColumn("mayrented_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("uniPlacesPlaceExt");

		}

    }
}