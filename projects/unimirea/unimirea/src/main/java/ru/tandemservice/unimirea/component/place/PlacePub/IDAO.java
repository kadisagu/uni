/* $Id$ */
package ru.tandemservice.unimirea.component.place.PlacePub;

/**
 * @author Ekaterina Zvereva
 * @since 18.02.2015
 */
public interface IDAO extends ru.tandemservice.uniplaces.component.place.PlacePub.IDAO
{
    void prepareRentDataSource(Model model);

}