/* $Id$ */
package ru.tandemservice.unimirea.component.listextract.mirea1.ListExtractPub;


/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unimirea.entity.MireaExcludeStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 13.02.2017
 */
public class Controller extends AbstractListExtractPubController<MireaExcludeStuListExtract, Model, IDAO>
{
}
