/* $Id$ */
package ru.tandemservice.unimirea.report.bo.MireaEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimirea.report.bo.MireaEnrReport.logic.IMireaEnrReportDao;
import ru.tandemservice.unimirea.report.bo.MireaEnrReport.logic.MireaEnrReportDao;

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
@Configuration
public class MireaEnrReportManager extends BusinessObjectManager
{
    public static MireaEnrReportManager instance()
    {
        return instance(MireaEnrReportManager.class);
    }

    @Bean
    public IMireaEnrReportDao dao()
    {
        return new MireaEnrReportDao();
    }
}