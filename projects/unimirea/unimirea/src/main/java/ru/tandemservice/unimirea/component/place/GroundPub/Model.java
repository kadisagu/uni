/* $Id$ */
package ru.tandemservice.unimirea.component.place.GroundPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimirea.entity.MireaSecondDisposalRight;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author Ekaterina Zvereva
 * @since 18.03.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.GroundPub.Model
{
    private DynamicListDataSource<UniplacesBuilding> _dataSourceBuilding;

   private MireaSecondDisposalRight _secondRight;

    public MireaSecondDisposalRight getSecondRight()
    {
        return _secondRight;
    }

    public void setSecondRight(MireaSecondDisposalRight secondRight)
    {
        _secondRight = secondRight;
    }

    public DynamicListDataSource<UniplacesBuilding> getDataSourceBuilding()
    {
        return _dataSourceBuilding;
    }

    public void setDataSourceBuilding(DynamicListDataSource<UniplacesBuilding> dataSourceBuilding)
    {
        _dataSourceBuilding = dataSourceBuilding;
    }
}
