package ru.tandemservice.unimirea.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unimirea_2x9x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.nsiclient", "2.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // модуль bpms отключен - удаляем все его сущности
        // убедиться, что модуль и в самом деле удален
        {
            if( ApplicationRuntime.hasModule("bpms") )
                throw new RuntimeException("Module 'bpms' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "bpms");


    }
}