package unimirea.scripts.common

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.rating.entity.gen.EnrRatingItemGen
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams

return new MireaEntrantLetterBoxPrint(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Ekaterina Zvereva
 * @since 28.04.2015
 */
class MireaEntrantLetterBoxPrint {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().entrant().id()}=${entrantRequest.entrant.id}
                order by ${EnrRequestedCompetition.request().regDate()}, ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillRequestedDirectionList(directions)
        fillInjectParameters()

        RtfDocument document = new RtfReader().read(template);

        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Титульный лист ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillRequestedDirectionList(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def rows = new ArrayList<String[]>()
        short i = 1

        for (EnrRequestedCompetition requestedCompetition : enrRequestedCompetitions) {
            if (EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY.equals(requestedCompetition.state?.code))
                continue

            // формируем строку из восьми столбцов
            String[] row = new String[8]

            row[0] = i++
            def programSet = requestedCompetition.competition.programSetOrgUnit.programSet
            def programForm = programSet.programForm
            def competitionType = requestedCompetition.competition.type
            def compensationType = competitionType.compensationType

            row[1] = programSet.title + "-" + programForm.shortTitle + "-" + compensationType.shortTitle.substring(0, 3)
            row[2] = requestedCompetition.request.stringNumber
            row[3] = DateFormatter.DEFAULT_DATE_FORMATTER.format(requestedCompetition.request.regDate)

            def choosenEntranceExam = DQL.createStatement(session, /
                    from ${EnrChosenEntranceExam.class.simpleName}
                    where ${EnrChosenEntranceExam.requestedCompetition().id()}=${requestedCompetition.id}
                    order by ${EnrChosenEntranceExam.discipline().id()}
                    /).<EnrChosenEntranceExam> list()

            def discipResult = new StringBuilder()
            def flag = true
            def hasNotMark = false
            def sum = 0
            def olymp = ""
            for (EnrChosenEntranceExam exam : choosenEntranceExam) {
                if (!flag)
                    discipResult.append("\n")
                discipResult.append(exam.discipline.discipline.title)
                if (null != exam.maxMarkForm && exam.maxMarkForm.markAsLong > 0) {
                    discipResult.append("-" + exam.maxMarkForm.markAsString)
                    sum += exam.maxMarkForm.markAsLong

                    if (exam.markSource instanceof EnrEntrantMarkSourceBenefit) {
                        olymp = ((EnrEntrantMarkSourceBenefit) exam.markSource).benefitCategory.shortTitle
                    }
                } else
                    hasNotMark = true
                flag = false
            }
            EnrRatingItem enrRatingItem = DataAccessServices.dao().getByNaturalId(new EnrRatingItemGen.NaturalId(enrRequestedCompetitions.get(0).competition, entrantRequest.entrant))
            if (enrRatingItem != null)
                sum += enrRatingItem.achievementMarkAsLong

            row[4] = discipResult as String
            row[5] = (!hasNotMark && sum > 0 ? String.valueOf(sum / 1000) : "")

            def coExcl = ""
            def competitionsExcl = requestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }.collect { e -> (EnrRequestedCompetitionExclusive) e }
            if (!competitionsExcl.isEmpty()) {
                coExcl = ((EnrRequestedCompetitionExclusive) competitionsExcl.get(0)).benefitCategory.shortTitle
            }
            def coNoEx = ""
            def competitionsNoExam = requestedCompetition.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }.collect { e -> (EnrRequestedCompetitionNoExams) e }
            if (!competitionsNoExam.isEmpty()) {
                coNoEx = ((EnrRequestedCompetitionNoExams) competitionsNoExam.get(0)).benefitCategory.shortTitle
            }
            row[6] = (!olymp.empty || !coExcl.empty || !coNoEx.empty) ? "Преим. " + [olymp, coExcl, coNoEx].toSet().toList().join(", ") : ""

            row[7] = (EnrCompetitionTypeCodes.MINISTERIAL.equals(competitionType.code) ? "" : competitionType.printTitle)
            rows.add(row)
        }
        tm.put("T", rows as String[][])
        // внедряем параграфы в 5 столбце
        tm.put("T", new RtfRowIntercepterBase() {
            @Override
            List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if (colIndex == 4) {
                    String[] valStr = value.split("\n")
                    RtfString newStr = new RtfString()
                    for (String item : valStr)
                        newStr.append(item).par()
                    return newStr.toList()
                }
            }
        })
    }

    def fillInjectParameters() {
        def entrant = entrantRequest.entrant
        def card = entrantRequest.identityCard

        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("lastName", card.lastName)
        im.put("entrantNumber", entrant.personalNumber)
        im.put("passport", card.title)
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("edEndYear", entrantRequest.eduDocument.yearEnd as String)

        PersonEduDocument eduDocument = entrantRequest.eduDocument;
        if (EduDocumentKindCodes.ATTESTAT_BASE.equals(eduDocument.eduDocumentKind.code) || EduDocumentKindCodes.ATTESTAT_COMPLETE.equals(eduDocument.eduDocumentKind.code))
            im.put('avgCertificateMark', "Средний балл аттестата: " + String.valueOf(eduDocument.avgMarkAsDouble))
        else im.put('avgCertificateMark', "")

        EnrRatingItem ratingItem = new DQLSelectBuilder().fromEntity(EnrRatingItem.class, 'eri')
                .top(1)
                .where(DQLExpressions.eq(DQLExpressions.property('eri', EnrRatingItem.requestedCompetition().request().entrant()), DQLExpressions.value(entrant)))
                .order(DQLExpressions.property('eri', EnrRatingItem.requestedCompetition().priority()))
                .createStatement(session)
                .uniqueResult()

        im.put('markIndivResult', DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ratingItem.getAchievementMarkAsDouble()))
        im.put('docType', entrantRequest.isEduInstDocOriginalHandedIn() ? "П" : "")

    }
}