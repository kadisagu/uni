package unimirea.scripts.common

/**
 * @author Ekaterina Zvereva
 * @since 29.09.2015
 */
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


return new EntrantPersonalFileCoverPrint(
        session: session,
        template: template,
        entrantRequest: session.get(EnrEntrantRequest.class, object)
).print()

class EntrantPersonalFileCoverPrint
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()

    def print() {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "com")
                                    .where(eq(property("com", EnrRequestedCompetition.request()), value(entrantRequest)))
                                    .where(eq(property("com", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)))
                                    .top(1)
        EnrRequestedCompetition enrolledDirection = builder.createStatement(session).uniqueResult();

        if (null != enrolledDirection) {

            EnrEnrollmentExtract extract = DataAccessServices.dao().getUnique(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity().s(), enrolledDirection);
            EnrEnrollmentParagraph paragraph = extract?.paragraph as EnrEnrollmentParagraph
            Student student = extract?.student;

//            im.put("orgUnit", paragraph?.formativeOrgUnit.shortTitle)
//            im.put("group", extract?.groupTitle);
            im.put("orgUnit", student? student.educationOrgUnit.formativeOrgUnit.shortTitle:"")
            im.put("group", student?.group?.title);
            im.put("code", student?.personalNumber)

            im.put("educationOrgUnit", paragraph? paragraph.programSubject.titleWithCode:"")    // .programSubject.title.toLowerCase()
            im.put("developForm", paragraph? paragraph.programForm.title:"")

        } else {
            im.put("orgUnit", "")
            im.put("educationOrgUnit", "")
            im.put("developForm", "")
            im.put("compensationType", "")
            im.put("group", "")
            im.put("code", "")
        }
        def card = entrantRequest.identityCard

        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("lastName", card.lastName)

        RtfDocument document = new RtfReader().read(template);

        im.modify(document)


        return [document: document,
                fileName: "Обложка личного дела ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }
}