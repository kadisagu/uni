package unimirea.scripts.common

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.core.view.formatter.YesNoFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfGroup
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.*
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.*
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintBS(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author oleyba
 * @since 5/13/13
 */
class EntrantRequestPrintBS {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() // метки, строки с которыми необходимо удалить из таблицы

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()} = ${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
        /).<EnrRequestedCompetition> list()

        fillCompetitionParallel(directions)
        fillCompetitionNoParallel(directions)
        fillCompetitionNoExams(directions)
        fillCompetitionExclusive(directions)
        fillEnrEntrantStateExam(directions)
        fillInternalStateExams(directions)
        fillInternalExams(directions)
        fillOlymps(directions)
        fillInjectParameters()
        fillNextOfKin()

        def fillRequestedPrograms = fillRequestedPrograms()
        def fillAcceptedContract = fillAcceptedContract()

        RtfDocument document = new RtfReader().read(template);
        if (fillRequestedPrograms) {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            byte[] template = IUniBaseDao.instance.get().getCatalogItem(ScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_REQUESTED_PROGRAMS).getCurrentTemplate()
            document.getElementList().addAll(new RtfReader().read(template).getElementList())
        }
        if (fillAcceptedContract) {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            byte[] template = IUniBaseDao.instance.get().getCatalogItem(ScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT).getCurrentTemplate()
            document.getElementList().addAll(new RtfReader().read(template).getElementList())
        }

        if (im != null)
            im.modify(document);
        if (tm != null)
            tm.modify(document);
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillOlymps(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def chosenExams = getChosenEntranceExamsWithMarkSourceBenefit(enrRequestedCompetitions)
        if (!chosenExams.isEmpty()) {
            Set<String> titles = chosenExams.collect { e -> e.discipline.title }
            im.put("Olymps", StringUtils.join(titles.sort(), ", "))

            def rtfString = new RtfString();
            def benefitDocTitles = chosenExams.collect { e -> benefitDocTitle(((EnrEntrantMarkSourceBenefit) e.markSource).mainProof) }.toSet().toList().sort()
            for (def bDoc : benefitDocTitles) {
                rtfString.append(bDoc)
                if (benefitDocTitles.indexOf(bDoc) != benefitDocTitles.size() - 1)
                    rtfString.par()
            }

            im.put("OlympStatements", rtfString)
        } else {
            deleteLabels.add("Olymps")
            deleteLabels.add("OlympStatements")
        }
    }

    static def olympDocTitle(EnrOlympiadDiploma doc) {
        doc.documentType.shortTitle + " " + StringUtils.trimToEmpty(doc.seriaAndNumber) + " (" + doc.olympiad.title + ") по предмету " + doc.subject.title.toLowerCase() + ", " + doc.honour.title.toLowerCase()
    }

    static def baseDocTitle(EnrEntrantBaseDocument doc) {
        doc.documentType.shortTitle + (doc.seria == null ? '' : ' ' + doc.seria) + (doc.number == null ? '' : ' ' + doc.number) + ", выдан: " +
                (StringUtils.isEmpty(doc.issuancePlace) ? "                                                            " : doc.issuancePlace) +
                (doc.issuanceDate == null ? '' : ' ' + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.issuanceDate)) +
                (doc.expirationDate == null ? '' : ', действителен до: ' + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.expirationDate))
    }

    static def String benefitDocTitle(IEnrEntrantBenefitProofDocument doc) {
        if (doc instanceof EnrEntrantBaseDocument)
        {
            if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code))
            {
                return olympDocTitle((EnrOlympiadDiploma)((EnrEntrantBaseDocument)doc).docRelation.document)
            }
            return baseDocTitle(doc)
        }

        return null == doc ? "" : doc.getDisplayableTitle();
    }

    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
        /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.isEmpty()) {
            List<String> titles = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.title }.toSet().toList().sort()
            im.put("internalExams", StringUtils.join(titles, ", "))
        } else {
            tm.remove('internalExams', 0, 1)
        }
    }

    def fillInternalStateExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().code()}='${EnrExamPassFormCodes.STATE_EXAM}'
        /).<EnrChosenEntranceExamForm> list()
        def subjectIds = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.stateExamSubject.id }.findAll().toSet().toList()
        def stateExamResultsPending = subjectIds.isEmpty() ? Collections.emptyList() : DQL.createStatement(session, /
                from ${EnrEntrantStateExamResult.class.simpleName}
                where ${EnrEntrantStateExamResult.subject().id()} in (${subjectIds.join(", ")})
                and ${EnrEntrantStateExamResult.entrant().id()} = ${entrantRequest.entrant.id}
                and ${EnrEntrantStateExamResult.secondWave()}=${true}
        /).<EnrEntrantStateExamResult> list()

        if (!stateExamResultsPending.isEmpty()) {
            List<String> titles = stateExamResultsPending.collect { e -> e.subject.title }.toSet().toList().sort()
            im.put("internalStateExams", StringUtils.join(titles, ", "))
        } else {
            tm.remove('internalStateExams', 0, 1)
        }
    }

    def fillEnrEntrantStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def results = getEnrEntrantMarkSourceStateExam(enrRequestedCompetitions)
        if (!results.isEmpty()) {
            def rows = Lists.newArrayList()

            // для каждого результата ЕГЭ
            for (def result : results) {
                // формируем строку из трех столбцов
                rows.add([
                        result.getSubject().getTitle(),
                        result.getMark(),
                        result.getYear()
                ])
            }

            tm.put('T3', rows as String[][])
        } else {
            tm.remove("T3", 0, 1)
        }
    }

    def getChosenEntranceExamsWithMarkSourceBenefit(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExam.class, "cee")
                .column(property("cee"))
                .where(DQLExpressions.in(property("cee", EnrChosenEntranceExam.requestedCompetition().id()), ids))
                .joinPath(DQLJoinType.inner, EnrChosenEntranceExam.maxMarkForm().markSource().fromAlias("cee"), "ms")
                .joinEntity("ms", DQLJoinType.inner, EnrEntrantMarkSourceBenefit.class, "msb", eq(property("msb", EnrEntrantMarkSourceBenefit.id()), property("ms", EnrEntrantMarkSource.id())))
                .order(property("cee", EnrChosenEntranceExam.discipline().discipline().title()))
                .createStatement(getSession()).<EnrChosenEntranceExam> list()
    }

    // результаты ЕГЭ
    def getEnrEntrantMarkSourceStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantStateExamResult.class, "ser")
                .where(exists(
                new DQLSelectBuilder()
                        .fromEntity(EnrEntrantMarkSourceStateExam.class, "ms")
                        .where(DQLExpressions.in(property("ms", EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()), ids))
                        .where(eq(property("ms", EnrEntrantMarkSourceStateExam.stateExamResult().id()), property("ser", EnrEntrantStateExamResult.id())))
                        .buildQuery()))
                .order(property("ser", EnrEntrantStateExamResult.subject().title()))

        return builder.createStatement(session).<EnrEntrantStateExamResult> list()
    }

    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def parallelCompetitions = enrRequestedCompetitions.findAll() { e -> e.parallel }
        if (!parallelCompetitions.isEmpty()) {
            def dirMap = getDirMap(enrRequestedCompetitions)

            def rows = new ArrayList<String[]>()
            for (EnrRequestedCompetition requestedCompetition : parallelCompetitions) {
                def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
                def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

                rows.add([
                        String.valueOf(requestedCompetition.priority),
                        programSubject,
                        requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                        getPlaces(requestedCompetition.competition.type),
                        EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle
                ] as String[])
            }

            tm.put('T2', rows as String[][])

        } else tm.remove("T2", 0, 1)
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        // пока нет признака параллельности считаем, что все не параллельные

        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)

        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort { e -> e.priority }

        // для каждого результата ЕГЭ
        for (def requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority

            def subject = requestedCompetition.competition.programSetOrgUnit.programSet
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            String programSubject = subject.title + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
            def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

            row[1] = programSubject
            row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
            row[3] = getPlaces(requestedCompetition.competition.type)
            row[4] = EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle
            rows.add(row)
        }

        tm.put('T1', rows as String[][])
    }

    static def getPlaces(EnrCompetitionType type) {
        if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "по договорам об оказании платных образовательных услуг"
        else if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) ||
                EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code))
            return "финансируемые из федерального бюджета"
        else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    def fillCompetitionExclusive(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def exclusiveCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }.collect { e -> (EnrRequestedCompetitionExclusive) e }
        if (!exclusiveCompetitions.isEmpty()) {
            def benefitCategories = exclusiveCompetitions.collect { e -> e.benefitCategory.shortTitle }.toSet().toList().sort()
            def benefitProofDocList = new ArrayList<>();
            for (def bpDocs : getBenefitProofDocuments(exclusiveCompetitions).values())
                benefitProofDocList.addAll(bpDocs)

            def rtfString = new RtfString();
            def benefitDocuments = benefitProofDocList.collect { e -> baseDocTitle(e) }.toSet().toList().sort()
            for (String doc : benefitDocuments) {
                rtfString.append(doc)
                if (benefitDocuments.indexOf(doc) != benefitDocuments.size() - 1)
                    rtfString.par()
            }

            im.put("benefitCategory", benefitCategories.join(", "))
            im.put("benefitStatement", rtfString)

            def category = entrantRequest.benefitCategory
            if (category == null) deleteLabels.add('preference')
            else im.put('preference', 'Имею преимущественное право зачисления, так как отношусь к категории граждан: ' + category.shortTitle + '.')

            im.put("usingBenefit", "Использование особого права, указанного в данном заявлении, только при подаче заявления в " + TopOrgUnit.instance.shortTitle + ", подтверждаю");
        } else {
            deleteLabels.add("benefitCategory")
            deleteLabels.add("usingBenefit")
            deleteLabels.add('preference')
        }
    }

    def fillCompetitionNoExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def noExamCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }.collect { e -> (EnrRequestedCompetitionNoExams) e }
        if (!noExamCompetitions.isEmpty()) {
            noExamCompetitions = noExamCompetitions.sort { e -> e.priority }

            def dirMap = getDirMap(noExamCompetitions)

            def item = DataAccessServices.dao().get(ScriptItem.class, ScriptItem.code(), EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_BENEFITS);
            def temp = item.getCurrentTemplate();
            def document = new RtfReader().read(temp)
            List<IRtfElement> parList = new ArrayList<>();

            def benefitProofDocs = getBenefitProofDocuments(noExamCompetitions)
            for (def noExamCompetition : noExamCompetitions) {
                def tempDoc = document.clone;
                def modifier = new RtfInjectModifier();
                def subject = noExamCompetition.competition.programSetOrgUnit.programSet.programSubject

                List<EduProgramSpecialization> specs = dirMap.get(noExamCompetition.id)

                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

                modifier.put("programSubject", programSubject)
                modifier.put("programForm", noExamCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase())
                modifier.put("places", getPlaces(noExamCompetition.competition.type))

                def docs = benefitProofDocs.get(noExamCompetition.id)
                if (docs.isEmpty())
                    modifier.put("benefitStatement", "")
                else {
                    def docTitles = docs.collect { e -> benefitDocTitle(e) }.toSet().toList().sort()
                    RtfString rtfString = new RtfString()
                    for (def docTitle : docTitles) {
                        rtfString.append(docTitle)
                        if (docTitles.indexOf(docTitle) != docTitles.size() - 1)
                            rtfString.par()
                    }

                    modifier.put("benefitStatement", rtfString)
                }

                modifier.modify(tempDoc)

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(tempDoc.getElementList());
                parList.add(rtfGroup);
            }
            im.put("CompetitionNoExams", parList)
        } else {
            im.put("CompetitionNoExams", "")
        }
    }

    boolean fillRequestedPrograms() {
        def requestedPrograms = new DQLSelectBuilder()
                .fromEntity(EnrRequestedProgram.class, "rc")
                .column(property("rc"))
                .fetchPath(DQLJoinType.inner, EnrRequestedProgram.requestedCompetition().competition().fromAlias("rc"), "c")
                .fetchPath(DQLJoinType.inner, EnrCompetition.type().compensationType().fromAlias("c"), "ct")
                .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("c"), "pf")
                .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias("c"), "ps")
                .where(eq(property("rc", EnrRequestedProgram.requestedCompetition().request()), value(entrantRequest)))
                .order(property("rc", EnrRequestedProgram.requestedCompetition().priority()))
                .order(property("rc", EnrRequestedProgram.priority()))
                .createStatement(session).<EnrRequestedProgram> list()

        if (requestedPrograms.isEmpty()) {
            return false;

        } else {
            List<String[]> rows = new ArrayList<>();
            for (EnrRequestedProgram requestedProgram : requestedPrograms) {
                def competition = requestedProgram.getRequestedCompetition().getCompetition()
                def program = requestedProgram.getProgramSetItem().getProgram()
                rows.add([
                        requestedProgram.getRequestedCompetition().getPriority(),
                        competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitle(),
                        competition.getType().getPrintTitle(),
                        competition.getProgramSetOrgUnit().getProgramSet().getProgramForm().getShortTitle(),
                        competition.getType().getCompensationType().getTitle(),
                        program.getProgramSpecialization().isRootSpecialization() ? "" : (program.getProgramSpecialization()).getTitle(),
                        program.getEduProgramTrait() == null ? "" : program.getEduProgramTrait().getShortTitle(),
                        requestedProgram.getPriority()
                ] as String[])
            }

            tm.put("T5", rows as String[][])

            return true;
        }
    }

    boolean fillAcceptedContract() {
        def acceptedContractCompetitions = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "rc")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().type().fromAlias("rc"), "ct")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().eduLevelRequirement().fromAlias("rc"), "elr")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().fromAlias("rc"), "ps")
                .column("rc")
                .where(eq(property("rc", EnrRequestedCompetition.request()), value(entrantRequest)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
                .order(property("rc", EnrRequestedCompetition.priority()))
                .createStatement(session).<EnrRequestedCompetition> list()

        if (acceptedContractCompetitions.isEmpty()) {
            return false;

        } else {
            def dirMap = getDirMap(acceptedContractCompetitions)

            List<String[]> rows = new ArrayList<>();
            for (EnrRequestedCompetition requestedCompetition : acceptedContractCompetitions) {
                def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
                def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

                rows.add([
                        requestedCompetition.getPriority(),
                        programSubject,
                        requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                        getPlaces(requestedCompetition.competition.type),
                        EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle,
                        YesNoFormatter.INSTANCE.format(requestedCompetition.isAcceptedContract())
                ] as String[])
            }

            tm.put("T6", rows as String[][])

            return true;
        }
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id };
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "ch")
        builder.where(DQLExpressions.in(property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))

        def programs = builder.createStatement(session).<EnrRequestedProgram> list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id;
            if (!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if (!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        dirMap
    }

    Map<Long, List<IEnrEntrantBenefitProofDocument>> getBenefitProofDocuments(List<? extends IEnrEntrantBenefitStatement> reqCompetitions) {
        def benefitProofs = new DQLSelectBuilder()
                .fromEntity(EnrEntrantBenefitProof.class, 'bp')
                .column(property('bp'))
                .where(DQLExpressions.in(property('bp', EnrEntrantBenefitProof.benefitStatement().id()), reqCompetitions.collect { e -> e.id }))
                .createStatement(this.getSession()).<EnrEntrantBenefitProof> list()

        Map<Long, List<IEnrEntrantBenefitProofDocument>> result = SafeMap.get(ArrayList.class)
        for (def benefitProof : benefitProofs)
            result.get(benefitProof.benefitStatement.id).add(benefitProof.document)

        return result
    }

    void fillInjectParameters() {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def sex = card.sex
        def personAddress = person.address

        def headers = getHeaderEmployeePostList(TopOrgUnit.instance)
        IdentityCard headCard = (headers != null && !headers.isEmpty()) ? headers.get(0).person.identityCard : null;

        im.put('regNumber', entrantRequest.stringNumber)
        im.put('highSchoolTitleShort', TopOrgUnit.instance.shortTitle)

        StringBuilder headIof = new StringBuilder();
        if (headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(' ').append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }

        im.put('rector_G', headIof.toString())
        im.put('FIO', card.fullFio)
        im.put('birthDate', card.birthDate?.format('dd.MM.yyyy'))
        im.put('birthPlace', card.birthPlace)
        im.put('sex', sex.title)
        im.put('citizenship', card.citizenship.fullTitle)
        im.put('identityCardTitle', card.shortTitle)
        im.put('identityCardPlaceAndDate', [card.issuancePlace, card.issuanceDate?.format('dd.MM.yyyy')].grep().join(', '))
        im.put('adressTitleWithFlat', personAddress != null ? personAddress.titleWithFlat : "")
        im.put('adressPhonesTitle', person.contactData.mainPhones)
        im.put('email', person.contactData.email)
        im.put('age', card.age as String)

        im.put('education', entrantRequest.eduDocument.eduLevel?.title)
        im.put("certificate", entrantRequest.eduDocument.title)
        //
        im.put('eduOrganization', entrantRequest.eduDocument.eduOrganization)
        im.put('eduOrganizationYearEnd', String.valueOf(entrantRequest.eduDocument.yearEnd))
        im.put('receiveEduLevelFirst', entrantRequest.receiveEduLevelFirst ? 'впервые' : 'не впервые')

        List<EnrEntrantAchievement> achievements = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'ea')
                .where(eq(property('ea', EnrEntrantAchievement.entrant().id()), value(entrant.id)))
                .order(property("ea", EnrEntrantAchievement.type().achievementKind().title()))
                .createStatement(session).list();

        def rtfString = new RtfString();
        for (EnrEntrantAchievement achievement : achievements) {
            rtfString.append(getAchivmentTitle(achievement))
            if (achievements.indexOf(achievement) != achievements.size() - 1)
                rtfString.par()
        }
        im.put('entrantAchievements', rtfString)
        //
        String foreignLanguage = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "pf")
                .column(property("pf", PersonForeignLanguage.language().title()))
                .where(eq(property("pf", PersonForeignLanguage.person()), value(person)))
                .where(eq(property("pf", PersonForeignLanguage.main()), value(Boolean.TRUE)))
                .createStatement(session).uniqueResult()

        im.put("foreignLanguages", StringUtils.trimToEmpty(foreignLanguage))
        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("additionalInfo", entrant.additionalInfo)

        im.put("wayOfProviding", entrantRequest.getOriginalSubmissionWay().getTitle())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())
        im.put("needSpecialConditions", entrant.isNeedSpecialExamConditions() ? "нуждаюсь" + (entrant.getSpecialExamConditionsDetails() == null ? "" : " (" + entrant.getSpecialExamConditionsDetails() + ")") : "не нуждаюсь")

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))


        im.put('surName', PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put('name', PersonManager.instance().declinationDao().getDeclinationLastName(card.firstName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put('secondName', PersonManager.instance().declinationDao().getDeclinationLastName(card.middleName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put('idPlaceCode', StringUtils.trimToEmpty(card.issuanceCode))
        im.put('addressFact', StringUtils.trimToEmpty(entrantRequest.entrant.person.address?.titleWithFlat))

        im.put('idTitle', card.cardType.title)
        im.put('idSeria', StringUtils.trimToEmpty(card.seria))
        im.put('idNumber', StringUtils.trimToEmpty(card.number))

        def eduDocument = entrantRequest.eduDocument

        im.put('numberOf5', eduDocument.mark5 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark5))
        im.put('numberOf4', eduDocument.mark4 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark4))
        im.put('numberOf3', eduDocument.mark3 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark3))

        def avgMark = eduDocument.avgMarkAsDouble
        im.put('avgMark', avgMark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(avgMark))

        def status = null
        def militaryStatusList = IUniBaseDao.instance.get().getList(PersonMilitaryStatus.class, PersonMilitaryStatus.person(), entrantRequest.entrant.person)
        if (!militaryStatusList.isEmpty())
            status = militaryStatusList.iterator().next()

        im.put('militaryCardNum', status == null ? "" : StringUtils.trimToEmpty(status.militaryNumber))
        im.put('beginArmy', entrant.beginArmy == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.beginArmy))
        im.put('endArmy', entrant.endArmy == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.endArmy))

        String crimeaEnrCampaignString = '';
        if(entrant.enrollmentCampaign.settings.acceptPeopleResidingInCrimea)
            crimeaEnrCampaignString = "Категория граждан, попадающая под действие ФЗ-64";
        im.put('crimeaEnrollmentCampaign', crimeaEnrCampaignString)
    }

    private String getAchivmentTitle(EnrEntrantAchievement achievement){
        double mark = achievement.getType().isMarked() ? achievement.getMark() : achievement.getType().getAchievementMark()
        return achievement.type.achievementKind.title
                .concat(' ')
                .concat(String.valueOf(mark))
    }
    def fillNextOfKin() {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put('father', father)
        else {
            im.put('father', "")
        }

        if (mother != null)
            im.put('mother', mother)
        else {
            im.put('mother', "")
        }

        if (tutor != null)
            im.put('tutor', tutor)
        else {
            deleteLabels.add("tutor")
        }
    }

    def getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
        /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(' '),
                            nextOfKin.phones
        ].grep().join(', ') : null;
    }

    def getHeaderEmployeePostList(OrgUnit orgUnit) {
        DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${orgUnit.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
        /).<EmployeePost> list()
    }
}
