package unimirea.scripts.common

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.unimirea.component.documents.d5.Add.Model
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import ru.tandemservice.uni.entity.employee.*
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.entity.catalog.*
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import org.tandemframework.core.view.formatter.DateFormatter

/**
 * @author ivcMITHT (MIREA)
 * @since 25.07.2016
 */

/* $Id:$ */

return new StudentCallPrint(
        session: session,
        template: template,
        model: object
).print();


class StudentCallPrint
{
    Session session
    byte[] template
    Model model

    RtfInjectModifier injectModifier = new RtfInjectModifier()


    def print() {
        boolean male = model.getStudent().getPerson().getIdentityCard().getSex().isMale()
        TopOrgUnit academy = TopOrgUnit.getInstance()
        AcademyData academyData = AcademyData.getInstance();
        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());
// 	причина вызова
         String attType ="";
                switch (model.getAttestation().getId().intValue()){
                 case 0:
                     attType = "промежуточной аттестации";
                     break;
                    case 1:
                        attType = "государственной итоговой аттестации";
                        break;
                    case 2:
                        attType = "итоговой аттестации";
                        break;
                    case 3:
                        attType = "подготовки и защиты ВПК и сдачи ИГЭ";
                        break;
                    case 4:
                        attType ="сдачи итоговых госэкзаменов (ИГЭ)";
                        break;
                }
         Student student = model.getStudent()
         OrgUnit orgUnit = student.educationOrgUnit.formativeOrgUnit
         EmployeePost head = (EmployeePost)orgUnit.getHead();
         String  headPost, headFam, headName  = ""
		 String studFio = student.getPerson().getFullFio()
         if (head != null){
            Person personHead = head.person
            headFam = StringUtils.trimToEmpty(personHead.getIdentityCard().lastName);
            //char nam = StringUtils.trimToEmpty(personHead.getIdentityCard().firstName).charAt(0);
            //char otc = StringUtils.trimToEmpty(personHead.getIdentityCard().middleName).charAt(0);
            headName = StringUtils.trimToEmpty(personHead.getIdentityCard().firstName).substring(0,1) +'.'+ StringUtils.trimToEmpty(personHead.getIdentityCard().middleName).substring(0,1)+'. '+headFam;
            headPost = head.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle();
          }
        String kampus = ""
        String prorectorFIO = ""
        String prorector = ""
        String Title = ""
        String TitleCase = ""
        String post = ""
        switch (orgUnit.getNominativeCaseTitle()) {
            case "Физико-технологический институт":
              	kampus = "МИРЭА"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
            case "Институт комплексной безопасности и специального приборостроения":
               	kampus = "МГУПИ"
				prorector = "Заместитель первого проректора"
                prorectorFIO = "Н.Б. Голованова"
                break
            /*case "Институт технической эстетики и дизайна":
            	kampus = "МГУПИ"
                prorector = "Проректор по методической работе"
                prorectorFIO = "В.В. Слепцов"
                break */
            case "Институт управления и стратегического развития организаций":
            	kampus = "МГУПИ"
				prorector = "Заместитель первого проректора"
                prorectorFIO = "Н.Б. Голованова"
                break
            case "Институт вечернего, заочного и дистанционного образования":
            	kampus = "МГУПИ"
				prorector = "Заместитель первого проректора"
                prorectorFIO = "Н.Б. Голованова"
                break
            /*case "Факультет заочного и дистанционного обучения":
            	kampus = "МГУПИ"
                prorector = "Проректор по методической работе"
                prorectorFIO = "В.В. Слепцов"
                break*/
            case "Институт кибернетики":
            	kampus = "МИРЭА"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
            case "Институт информационных технологий":
            	kampus = "МИРЭА"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
            /*case "Институт электроники":
            	kampus = "МИРЭА"
                prorector = "Проректор по учебной работе"
                prorectorFIO = "А.В. Тимошенко"
                break*/
            case "Институт радиотехнических и телекоммуникационных систем":
            	kampus = "МИРЭА"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
            case "Институт инновационных технологий и государственного управления":
                kampus = "МИРЭА"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
            case "Институт международного образования":
            	kampus = "МИРЭА"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
            case "Институт тонких химических технологий":
            	kampus = "МИТХТ"
                prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
                break
        	case "филиал федерального государственного бюджетного образовательного учреждения высшего образования «Московский технологический университет» в г. Серпухове":
    			prorector = "Заместитель первого проректора"
                prorectorFIO = "Н.Б. Голованова"
            	break
            case "филиал федерального государственного бюджетного образовательного учреждения высшего образования «Московский технологический университет» в г. Фрязино":
            	prorector = "Заместитель первого проректора"
                prorectorFIO = "А.В. Тимошенко"
            	break
            case "филиал федерального государственного бюджетного образовательного учреждения высшего образования «Московский технологический университет» в г. Ставрополе":
    			prorector = "Заместитель первого проректора"
                prorectorFIO = "Н.Б. Голованова"
				break
            case "филиал федерального государственного бюджетного образовательного учреждения высшего образования «Московский технологический университет» в г. Сергиевом Посаде":
    			prorector = "Заместитель первого проректора"
                prorectorFIO = "Н.Б. Голованова"
                break
}

// 	форма подготовки
       //student.educationOrgUnit.
        String code = student.getEducationOrgUnit().getDevelopForm().getCode();
		String developForm = ''
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
		    developForm ="очной"
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
		    developForm ="заочной"
        else
    	    developForm ="очно-заочной"
// направление подготовки
            String eduLevel = ""
            StructureEducationLevels levelType = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType()
            if (levelType.master)
                eduLevel = "направлению подготовки магистров"
            else {
                if (levelType.bachelor)
                    eduLevel = "направлению подготовки бакалавров"
                else {
                    if (levelType.specialty)
                        eduLevel = "специальности"
                    else {
                        if (levelType.specialization)
                            eduLevel = "специальности"
                    }
                }
            }
            String specNapr = student.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle()
            int poiScob = specNapr.indexOf("(", 1);
            specNapr = poiScob > 0 ? specNapr.substring(0, poiScob) : specNapr
//   период	действия справки
            Date begPeri = model.getEduFrom()
			String beginPeriod = RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(begPeri)
			int dayPeri = model.getCntDays()
			String dayPeriod = String.valueOf(dayPeri)
			Calendar dayend = Calendar.getInstance();
            dayend.setTime(model.getEduFrom()); //устанавливаем дату, с которой будет производить операции
            dayend.add(Calendar.DAY_OF_MONTH, dayPeri-1);// прибавляем дни к установленной дате
            Date endPeri = dayend.getTime(); // получаем измененную дату
			String endPeriod = RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(endPeri)



		//
        RtfDocument document = new RtfReader().read(template)

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_P", academy.getPrepositionalCaseTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("sexTitle", male ? "обучающемуся" : "обучающейся")
                .put("course", student.getCourse().getTitle())
                .put("departmentTitle", attType)
                .put("documentTitle", model.getDocumentForTitle())
                .put("rectorAlt", prorector)
                .put("rectorFio",prorectorFIO)
                .put ("eduFrom",beginPeriod)
                .put ("eduTo",endPeriod)
                .put("headName",headName)
                .put("headPost",headPost)
				.put ("levelType",eduLevel)
                //.put ("levelHighSchool",specNapr)
                .put ("levelHS",specNapr)
				.put("developFormTitle",developForm)
				.put("calendDay",dayPeriod)
                .put("certificateSeria",academyData.getCertificateSeria())
                .put("certificateRegNumber",academyData.getCertificateRegNumber())
                .put("certificateDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()))
			    .put("studentTitle",studFio)

                injectModifier.modify(document);
                return [document: RtfUtil.toByteArray(document),
                fileName: "Справка-вызов_${model.getStudent().fullFio}.rtf",
                rtf: document]
    }
}